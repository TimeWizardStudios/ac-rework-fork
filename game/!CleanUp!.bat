@del /S /Q *.rpyc *.rpymc
@rd /S /Q cache
@rd /S /Q saves
@del "..\log.txt"
@del "..\traceback.txt"
@del "..\save_dump.txt"
@del "..\errors.txt"
@del "..\project.json"
