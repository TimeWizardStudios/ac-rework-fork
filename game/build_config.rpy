﻿init -1 python:
  do_not_build_modules=[
    #"1100_xray_mode",
  ]

  def module_filter(mod):
    ## can add special cases here in case list is not enough
    return mod not in do_not_build_modules
