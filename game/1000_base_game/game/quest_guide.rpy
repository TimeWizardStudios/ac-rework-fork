﻿init python:
  def get_quest_guide_marker(loc,element_id,marker=None,marker_offset=(0,0),marker_sector="right_bottom"):
    if isinstance(loc,basestring):
      loc=game.locations[loc]
    if loc==game.location:
      elements=[]
      loc.scene(elements)
      loc.prepare_images(elements)
      for el in elements:
        if isinstance(el,(list,tuple)) and len(el)>2:
          el_pos=el[0]
          el_id=el[2]
          el_ofs=(0,0)
          if not isinstance(el_id,basestring):
            el_ofs=tuple(el_id[1:3])
            el_id=el_id[0]
          el_pos=tuple([v+el_ofs[n] for n,v in enumerate(el_pos)])
          if marker_offset:
            el_pos=tuple([v+marker_offset[n] for n,v in enumerate(el_pos)])
          if el_id==element_id:
            return marker,(el_pos,marker_sector)
    return None,None

  def get_quest_guide_path(target_location,**kwargs):
    if isinstance(target_location,basestring):
      target_location=game.locations[target_location]
    if game.location!=target_location:
      path=game.location.find_path(target_location)
      if path:
        if isinstance(path,basestring):
          path=[path,{}]
        if path[0]:
          path[1].update(kwargs)
          return get_quest_guide_marker(game.location.id,path[0],**path[1])
    return (None,None)

  def get_quest_guide_char(target_character,**kwargs):
    if isinstance(target_character,basestring):
      target_character=game.characters[target_character]
    ## special case: character at current location, any reason
    elements=[]
    loc=game.location
    loc.scene(elements)
    loc.prepare_images(elements)
    for el in elements:
      if len(el)>2:
        el=el[2]
        if not isinstance(el,basestring):
          el=el[0]
        if el==target_character.id:
          kwargs.pop("force_marker",None)
          return get_quest_guide_marker(loc.id,target_character.id,**kwargs)
    ## if character somewhere else ignore marker modifiers
    if not kwargs.pop("force_marker",False):
      kwargs={}
    ## check character main location
    loc=target_character.location
    ## if no main location, try to use first alternative location
    if not loc:
      for loc in sorted(target_character.alternative_locations):
        loc=game.locations[loc]
        break
    if loc:
      ## if we at same location point to character
      if game.location==loc:
        return get_quest_guide_marker(loc.id,target_character.id,**kwargs)
      ## if not then point to path to character location
      else:
        return get_quest_guide_path(loc.id,**kwargs)
    return (None,None)

  def get_quest_guide_hud(btn_id,**kwargs):
    marker_pos={
      "map": (175,125),
      "time": (400,125),
      "quest_guide": (1465,125),
      "game_mode": (1655,125),
      "inventory": (1710,125),
      "phone": (1815,125),
      "leyline_locator": (1490,970),
      }.get(btn_id,(0,0))
    marker_offset=kwargs.get("marker_offset",(0,0))
    if marker_offset:
      marker_pos=tuple([v+marker_offset[n] for n,v in enumerate(marker_pos)])
    return (kwargs.get("marker"),(marker_pos,kwargs.get("marker_sector")))

  def get_available_quest_guides():
    rv=[]
    rv.append(["Off",""])
    for quest_id in game.quest_started_order:
      quest=game.quests[quest_id]
      if quest.in_progress and not quest.hidden:
        rv.append([quest.title,quest.id])
    return rv

  class Event_quest_guide_system(GameEvent):
    ## disable quest guide when finished first quest and show message
    def on_quest_finished(event,quest,silent=False):
      if quest.id==game.quest_guide:
        game.quest_guide=None
    def on_quest_failed(event,quest,silent=False):
      if quest.id==game.quest_guide:
        game.quest_guide=None
    ## fix missing game.quest_guide on upgrade
    def on_after_load(event):
      if not hasattr(game,"quest_guide"):
        game.quest_guide=None

label select_quest_guide:
#  $game.quest_guide=renpy.display_menu(get_available_quest_guides())
  call screen quest_guide_select
  if _return!="$cancel":
    $game.quest_guide=_return
  return
