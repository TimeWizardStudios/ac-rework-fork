init python:
  def set_dialog_mode(mode="default",*args,**kwargs):
    game.ui.dialog_mode = mode
    game.ui.dialog_mode_args = args
    if mode == "contact_info":
      game.ui.can_close_phone = False
      kwargs.setdefault("phone_place",(0.5,0.5,0.5,0.5,0,0))
      kwargs.setdefault("return_to",None)
      renpy.show_screen("phone","contact_info",*args,**kwargs)
    elif mode == "phone_call":
      game.ui.can_close_phone = False
      kwargs.setdefault("phone_place",(0.5,0.5,1.0,0.5,0,0))
      kwargs.setdefault("return_to",None)
      renpy.show_screen("phone","call",*args,**kwargs)
    elif mode in ("phone_call_centered","phone_call_plus_textbox"):
      game.ui.can_close_phone = False
      kwargs.setdefault("phone_place",(0.5,0.5,0.5,0.5,0,0))
      kwargs.setdefault("return_to",None)
      renpy.show_screen("phone","call",*args,**kwargs)
    elif mode == "phone_message":
      game.ui.can_close_phone = False
      kwargs.setdefault("phone_place",(0.5,0.5,1.0,0.5,0,0))
      kwargs.setdefault("return_to",None)
      renpy.show_screen("phone","messages",*args,**kwargs)
    elif mode in ("phone_message_centered","phone_message_plus_textbox"):
      game.ui.can_close_phone = False
      kwargs.setdefault("phone_place",(0.5,0.5,0.5,0.5,0,0))
      kwargs.setdefault("return_to",None)
      renpy.show_screen("phone","messages",*args,**kwargs)
    elif mode in ("walkie_talkie","walkie_talkie_plus_textbox"):
      renpy.show_screen("walkie_talkie")
    elif mode in ("walkie_talkie_left_aligned","walkie_talkie_left_aligned_plus_textbox"):
      renpy.show_screen("walkie_talkie",xpos=534)
    else:
      renpy.hide_screen("phone")
      game.ui.can_close_phone = True

  def dialog_line_hook(who,what):
    if game.ui.dialog_mode in ("phone_message","phone_message_centered"):
      history_char = game.ui.dialog_mode_args[0]
      game.pc.add_message_to_history(history_char,who,what)
