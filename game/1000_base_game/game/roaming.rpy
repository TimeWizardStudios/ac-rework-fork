label goto_with_black(target_location):
  if mc["turn_off_location_transitions"]:
    $game.location=target_location
  else:
    scene black with Dissolve(.07)
    $game.location=target_location
    $renpy.pause(0.07)
    $renpy.get_registered_image("location").reset_scene()
    scene location
    if quest.jo_washed.in_progress:
      show misc flashback zorder 100
    with Dissolve(.5)
  $set_dialog_mode("")
  return

label roaming:
  python:
    process_event("update_state")
    process_event("enter_roaming_mode")
  window auto
  scene location
  if quest.jo_washed.in_progress:
    show misc flashback zorder 100
  while not game.events_queue:
    $game.ui.hide_hud=0
    $game.ui.hud_active=True
    call screen location
    $game.ui.hide_hud=0
    $game.ui.hud_active=False
    hide screen element_hint
    hide screen active_element
    $game.ui.hint_active_elements=False
    if isinstance(_return,(basestring,list,tuple)):
      return _return
  return
