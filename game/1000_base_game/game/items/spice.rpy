init python:
  class Item_spice(Item):
    icon="items spice"

    def title(item):
      return "Spice"

    def description(item):
      return "Taste is subjective. Who says you can't put cinnamon in a chili?"

    def actions(cls,actions):
      actions.append("?int_spice")
   
label int_spice(item):
  "Sumak, saffron, cardamom, and sugar. You always need sugar."
  return True
