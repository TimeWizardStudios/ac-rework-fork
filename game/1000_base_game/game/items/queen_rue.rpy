init python:
  class Item_queen_rue(Item):

    icon = "items queen_rue"

    def title(item):
      return "Queen's Rue"

    def description(item):
      return "It smells like death... I wonder what would happen if I ate it."

    def actions(cls,actions):
      actions.append("?queen_rue_interact")


label queen_rue_interact(item):
  "These flowers are already wilted..."
  "Did they die that quickly, or do they just come like that?"
  "Maybe a sexy bee could get them to perk right back up."
  return True
