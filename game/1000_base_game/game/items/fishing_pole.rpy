init python:
  class Item_fishing_pole(Item):

    icon = "items fishing_pole"

    def title(item):
      return "Fishing Pole"

    def description(item):
      # return "I bet [maxine] has used this pole to catch some crazy stuff."
      return "I bet [maxine] has used this pole\nto catch some crazy stuff."

    def actions(cls,actions):
      actions.append("?fishing_pole_interact")


label fishing_pole_interact(item):
  "I've got the hook, I've got the line. Time to sink her."
  return True
