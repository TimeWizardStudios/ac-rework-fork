init python:
  class Item_second_note_to_mrsl(Item):

    icon = "items second_note_to_mrsl"

    def title(item):
      return "Second Note to " + mrsl.name

    def description(item):
      # return "Not exactly a message in a bottle... but desperate times, and all that jazz."
      return "Not exactly a message in a\nbottle... but desperate times,\nand all that jazz."

    def actions(cls,actions):
      actions.append("?second_note_to_mrsl_interact")


label second_note_to_mrsl_interact(item):
  "{i}\"Art, gym — I do it all.\"{/}"
  "{i}\"I just need someone to really push me to the edge.\"{/}"
  return True
