init python:
  class Item_flora_pin(Item):

    icon="items flora_pin"

    def title(item):
      return flora.name + "'s Pin"

    def description(item):
      return "This pin is how [flora] signals her intentions to other squid fetishists."

    def actions(cls,actions):
      actions.append("?flora_pin_interact")


label flora_pin_interact(item):
  "What is [flora]'s favorite food?"
  "Ten-tacos! Extra slimy!"
  return True
