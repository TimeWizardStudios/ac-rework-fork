init python:
  class Item_lollipop_2(Item):
    icon="items lollipop_2"
    
    def title(item):
      return "Big Lollipop"
    
    def description(item):
      return "Diabetes on a stick. Yum."
    
    def actions(cls,actions):
      actions.append("?lollipop_2_interact")

label lollipop_2_interact(item):
  "This is the type of lollipop the girls practice on before spending a night with Chad."
  return True 