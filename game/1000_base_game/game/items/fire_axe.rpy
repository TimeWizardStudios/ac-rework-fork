init python:
  class Item_fire_axe(Item):

    icon = "items fire_axe"

    def title(item):
      return "Fire Axe"

    def description(item):
      return "A fire axe, but no extinguisher.\n\nPriorities."

    def actions(cls,actions):
      actions.append("?fire_axe_interact")

label fire_axe_interact(item):
  "..."
  "..."
  "..."
  "...and my axe!"
  "Ugh, why am I the dwarf even in my own made up scenario?"
  return True
