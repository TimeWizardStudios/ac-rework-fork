init python:
  class Item_note_from_mrsl(Item):

    icon = "items note_from_mrsl"

    def title(item):
      return "Note from " + mrsl.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?note_from_mrsl_interact")

  add_recipe("note_from_mrsl","pen","quest_mrsl_bot_back_and_forth_note_from_mrsl_pen_combine")
  add_recipe("note_from_mrsl","nurse_pen","quest_mrsl_bot_back_and_forth_note_from_mrsl_pen_combine")


label note_from_mrsl_interact(item):
  "{i}\"My, isn't this a clever little art project?\"{/}"
  "{i}\"I see you take your extracurricular activities very seriously.\"{/}"
  "..."
  "Is that it?"
  # "Damn. I better leave a reply so that she knows just how serious I am."
  "Damn. I better leave a reply so that she knows just how serious I am.{space=-20}"
  "I'll just write it on the back of this one..."
  $quest.mrsl_bot["note_from_mrsl_interacted_with"] = True
  return True
