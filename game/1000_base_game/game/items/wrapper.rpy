init python:
  class Item_wrapper(Item):
    
    icon="items candy_wrapper"
   
    def title(item):
      return "Candy Wrapper"
    
    def description(item):
      return "This isn't trash. They're an important part of any ASMR artist's toolbox."
    
    def actions(cls,actions):
      actions.append("?int_candy_wrapper")
      
label int_candy_wrapper(item):
  "The sickly sweet taste of candy residue. Sucking on these wrappers is the best."
  "Zero calories."
  return True
      
label wrapper_gotten_all:
  "Littering is a crime, and the janitor will be happy I helped her out."
  #$ria.love+=2 #To_Do
  return True    
