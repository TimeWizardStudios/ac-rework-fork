init python:
  class Item_tissues(Item):

    icon = "items tissues"
    consumable = True
    can_place_to_desk = True

    def title(item):
      return "Tissue"

    def description(item):
      return "These are from the bathroom. The ones on my desk need to stay there."

    def actions(cls,actions):
      actions.append("?int_tissues")


label int_tissues(item):
  "Do you need a tissue?"
  "No, I don't, Stacy."
  "I need my dignity back!"
  "These tears will dry, but my self-esteem will never fully recover!"
  return True
