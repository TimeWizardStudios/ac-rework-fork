init python:
  class Item_message_box(Item):

    icon = "items message_box"

    def title(item):
      return "Message Box"

    def description(item):
      # return "Not pretty enough for unboxing day, but it'll do."
      return "Not pretty enough for\nunboxing day, but it'll do."

    def actions(cls,actions):
      actions.append("?message_box_interact")


label message_box_interact(item):
  "If you put water in a box, it becomes the box..."
  return True
