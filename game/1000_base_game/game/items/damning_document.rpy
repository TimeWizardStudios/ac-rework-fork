init python:
  class Item_damning_document(Item):

    icon = "items damning_document"

    def title(item):
      return "Damning Document"

    def description(item):
      return "An official document from the Newfall Hospital."

    def actions(cls,actions):
      actions.append("?damning_document_interact")


label damning_document_interact(item):
  "Hmm..."
  if quest.nurse_aid["name_reveal"]:
    "According to this document, it looks like [amelia] has committed theft.{space=-30}"
  else:
    "According to this document, it looks like the [nurse] has committed theft.{space=-90}"
  "Stolen medical supplies..."
  "If this came to light, she'd never be able to practice medicine again."
  "She might even end up in jail."
  return True
