init python:
  class Character_lll(BaseChar):

    default_name = "{size=24}Ley Line\nLocator{/}"

style say_name_frame_lll is say_name_frame_guard:
  padding (30,10,30,16)


init python:
  class Item_leyline_locator(Item):

    icon="items ley_line_locator"
    retain_capital = True

    def title(item):
      return "Ley Line Locator"

    def description(item):
      if quest.maxine_lines["first_charge"]:
        return "Up, up, down, down, left, right, left, right, B, A, Start!\n\nHuh... nothing."
      else:
        return "There's a cable jack at the bottom. Perhaps for a charger?"

    def actions(cls,actions):
      if quest.maxine_lines in ("battery","charge","magnetic","finalnode"):
        if not quest.maxine_lines["first_charge"]:
          actions.append("?int_leyline_locator_charge")
        else:
          if mc.at("school_leyline_locator"):
            actions.append("int_leyline_locator_exit")
          else:
            if quest.maxine_lines["lll_charged"]:
              actions.append("goto_school_leyline_locator") ## locator already on 'belt' but interacting through inv
            else:
              actions.append("?int_leyline_locator_uncharged")
      elif quest.maxine_lines.in_progress: ## quest in progress but don't need locator right now
        actions.append("?int_leyline_locator_not_now")
      elif quest.maxine_lines.finished: ## quest over, but still have locator
        actions.append("?int_leyline_locator_finished")


image leyline disabled:
  yoffset -200
  zoom 0.8
  "misc leyline locator disabled"

label int_leyline_locator_charge(item):
  show leyline disabled with Dissolve(.5)
  "This thing is dead. It does seem to run on electricity, though... perhaps there's a way to charge it?"
  $mc["lll_dead"] = True
  hide leyline disabled with Dissolve(.5)
  return True

label int_leyline_locator_uncharged(item):
  show leyline disabled with Dissolve(.5)
  "This thing is out of juice. It almost needs a refill as much as I need a shot of strawberry J."
  hide leyline disabled with Dissolve(.5)
  return True

label int_leyline_locator_exit(item):
  call goto_with_black(quest.maxine_lines["leyline_location"])
  return

label int_leyline_locator_not_now(item):
  show leyline disabled with Dissolve(.5)
  "Okay, let's see if I've got any matches on Tinder... wait, this is not my phone."
  hide leyline disabled with Dissolve(.5)
  return True

label int_leyline_locator_finished(item):
  show leyline disabled with Dissolve(.5)
  "Man, I keep checking this thing more than my phone..."
  hide leyline disabled with Dissolve(.5)
  return True


init python:
  class Location_school_leyline_locator(Location):

    default_title="Leyline Locator"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      llocation = " "+quest.maxine_lines['leyline_location']
      scene.append([(0,0),"misc leyline background"+llocation])#,"locator_background"])

      if quest.maxine_lines['leyline_location']:
        if game.locations[quest.maxine_lines['leyline_location']]['magnet']:
          llocation+="_m"
        if game.locations[quest.maxine_lines['leyline_location']]['leyline_v2']:
          llocation+="_v2"
      if quest.maxine_lines["lll_charged"] == "off":
        scene.append([(580,140),"misc leyline screen"])
      else:
        scene.append([(751,215),"misc leyline screen"+llocation,"locator_screen"]) #(171,75) off from base location

      scene.append([(580,140),"misc leyline locator back"])
      # scene.append([(580,140),"misc leyline locator g3"])
      # scene.append([(580,140),"misc leyline locator g2"])
      # scene.append([(580,140),"misc leyline locator g1"])

      if quest.maxine_lines["lll_charged"] == "off":
        scene.append([(580,140),"misc leyline locator g1"])
        scene.append([(580,140),"misc leyline locator g2"])
        scene.append([(580,140),"misc leyline locator g3"])
      else:
        if quest.maxine_lines['leyline_light_3'] == "yellow":
          scene.append([(580,140),"misc leyline locator y3"])
        else:
          scene.append([(580,140),"misc leyline locator r3"])
        if quest.maxine_lines['leyline_light_2'] == "yellow":
          scene.append([(580,140),"misc leyline locator y2"])
        else:
          scene.append([(580,140),"misc leyline locator r2"])
        if quest.maxine_lines['leyline_light_1'] == "yellow":
          scene.append([(580,140),"misc leyline locator y1"])
        else:
          scene.append([(580,140),"misc leyline locator r1"])
      if quest.maxine_lines < "magnetic":
        scene.append([(580,140),"misc leyline locator b1",("locator_button_1",-180,520)])
      if quest.maxine_lines < "finalnode":
        scene.append([(580,140),"misc leyline locator b2",("locator_button_2",170,520)])
      scene.append([(1750,1000),"school ground_floor arrow_down","school_leyline_locator_exit"])

    def find_path(self,target_location):
      return "school_leyline_locator_exit"


init python:
  class Interactable_school_leyline_locator_exit(Interactable):

    def title(cls):
      return "Back"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["go","Back","school_leyline_locator_exit_use"])


label school_leyline_locator_exit_use:
  call goto_with_black(quest.maxine_lines['leyline_location'])
  return


init python:
  class Interactable_locator_button_1(Interactable):

    def title(cls):
      return "Blue Button"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("locator_button_1_interact")


label locator_button_1_interact:
  lll "{i}Locking in position...{/}"
  if quest.maxine_lines['leyline_location'] == "school_first_hall":
    $quest.maxine_lines['leyline_light_1'] = "yellow"
  elif quest.maxine_lines['leyline_location'] == "school_gym":
    $quest.maxine_lines['leyline_light_2'] = "yellow"
  elif quest.maxine_lines['leyline_location'] == "school_first_hall_west":
    $quest.maxine_lines['leyline_light_3'] = "yellow"
  $quest.maxine_lines['lll_charged']-=1
  lll "{i}Position successfully locked in!{/}"
  if quest.maxine_lines["lll_charged"] == 0:
    jump locator_out_of_fuel
  return


init python:
  class Interactable_locator_button_2(Interactable):

    def title(cls):
      return "Red Button"

    def description(cls):
      return ""

    def actions(cls,actions):
      if quest.maxine_lines == "magnetic":
        actions.append("locator_button_2_interact_final")
      else:
        actions.append("locator_button_2_interact")


label locator_button_2_interact_final:
  if school_first_hall["magnet"] and school_first_hall_west["magnet"] and school_gym["magnet"]:
    lll "{i}Triangulating positions...{/}"
    lll "{i}...{/}"
    lll "{i}Ley Line Pattern: Complete.{/}"
    lll "{i}...{/}"
    $school_first_hall["leyline_v2"] = True
    lll "{i}4th node successfully located.{/}"
    $school_secret_locker["panel"] = "loose"
    $quest.maxine_lines.advance("finalnode")
    if quest.maxine_lines["leyline_location"] == "school_first_hall":
      jump locator_screen_school_first_hall
  else:
    lll "{i}Triangulating positions...{/}"
    lll "{i}...{/}"
    lll "{i}Ley Line Pattern: Complete.{/}"
    lll "{i}...{/}"
    lll "{i}Error. Signal too weak.{/}"
    "Damn it. Maybe [maxine] is right... I need a magnet on each node."
  return

label locator_button_2_interact:
  if quest.maxine_lines['leyline_light_3'] == "yellow" and quest.maxine_lines['leyline_light_2'] == "yellow" and quest.maxine_lines['leyline_light_1'] == "yellow":
    lll "{i}Triangulating positions...{/}"
    lll "{i}...{/}"
    lll "{i}Ley Line Pattern: Complete.{/}"
    lll "{i}...{/}"
    lll "{i}Error. Signal too weak.{/}"
    "What the hell does that mean?"
    "Why is there an error? I'm sure I put in the right positions."
    "It said that the signal was too weak... maybe [maxine] knows what that means?"
    $quest.maxine_lines.advance("insight")
    jump locator_out_of_fuel_no_dialogue
  else:
    lll "{i}Insufficient positions locked in...{/}"
    $quest.maxine_lines["leyline_light_1"] = 0
    $quest.maxine_lines["leyline_light_2"] = 0
    $quest.maxine_lines["leyline_light_3"] = 0
    $quest.maxine_lines['lll_charged']-=1
    lll "{i}Resetting positions...{/}"
    lll "{i}Positions successfully reset.{/}"
    if quest.maxine_lines["lll_charged"] == 0:
      jump locator_out_of_fuel
  return

label locator_out_of_fuel:
  $quest.maxine_lines["lll_charged"] = "off"
  window hide
  pause(.25)
  window auto
  "Crap. This thing ran out of fuel... better go charge it."
  label locator_out_of_fuel_no_dialogue:
  call school_leyline_locator_exit_use
  $quest.maxine_lines["lll_charged"] = 0
  $school_first_hall["electric_smoke_now"] = False
  return


init python:
  class Interactable_locator_screen(Interactable):

    def title(cls):
      return "Leyline"

    def description(cls):
      if quest.maxine_lines['leyline_location'] == "school_art_class":
        return "These lines make up a figure\nof their own.\n\nWhat kind of figure?\nOnly [jacklyn] would know."
      elif quest.maxine_lines['leyline_location'] == "school_cafeteria":
        return "Who's usually in this spot? Hmm..."
      elif quest.maxine_lines['leyline_location'] == "school_english_class":
        return "These lines don't seem to connect to the ones outside.\n\nI'm not sure what that means..."
      elif quest.maxine_lines['leyline_location'] == "school_first_hall":
        if quest.maxine_lines == "finalnode":
          return "Is this the road to El Dorado?\nMy golden destiny!"
        else:
          return "The lines glow with arcane light.\n\nA very impressive filter on\nthis gadget!"
      elif quest.maxine_lines['leyline_location'] == "school_first_hall_east":
        return "This is nothing more than an intricate game. [maxine] is just messing with me..."
      elif quest.maxine_lines['leyline_location'] == "school_first_hall_west":
        return "This line connects to the one in the hall outside this corridor."
      elif quest.maxine_lines['leyline_location'] == "school_ground_floor_west":
        return "Great... a fucking pentagram.\n\nSurely, this is all a big hoax?"
      elif quest.maxine_lines['leyline_location'] == "school_gym":
        return "Are those balls cursed?\nThat would make a lot of sense..."
      elif quest.maxine_lines['leyline_location'] == "school_nurse_room":
        return "Did the lines just flicker?\n\nHm... probably just the device starting to run low on battery."
      elif quest.maxine_lines['leyline_location'] == "school_ground_floor" and quest.maxine_lines == "finalnode":
        return "Please don't tell me I need to find twelve different keys..."

    def actions(cls,actions):
      if quest.maxine_lines=="magnetic" and quest.maxine_lines['leyline_location'] in ("school_first_hall","school_first_hall_west","school_gym") and not game.locations[quest.maxine_lines['leyline_location']]['magnet']:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:maxine_lines,leyline|Use What?","quest_maxine_lines_use_magnet"])
      if quest.maxine_lines['leyline_location'] == "school_art_class":
        actions.append("?locator_screen_school_art_class")
      elif quest.maxine_lines['leyline_location'] == "school_cafeteria":
        actions.append("?locator_screen_school_cafeteria")
      elif quest.maxine_lines['leyline_location'] == "school_english_class":
        actions.append("?locator_screen_school_english_class")
      elif quest.maxine_lines['leyline_location'] == "school_first_hall":
        if quest.maxine_lines == "finalnode":
          actions.append("?locator_screen_school_first_hall")
        else:
          actions.append("?locator_screen_school_first_hall_final")
      elif quest.maxine_lines['leyline_location'] == "school_first_hall_east":
        actions.append("?locator_screen_school_first_hall_east")
      elif quest.maxine_lines['leyline_location'] == "school_first_hall_west":
        actions.append("?locator_screen_school_first_hall_west")
      elif quest.maxine_lines['leyline_location'] == "school_ground_floor_west":
        actions.append("?locator_screen_school_ground_floor_west")
      elif quest.maxine_lines['leyline_location'] == "school_gym":
        actions.append("?locator_screen_school_gym")
      elif quest.maxine_lines['leyline_location'] == "school_nurse_room":
        actions.append("?locator_screen_school_nurse_room")
      elif quest.maxine_lines['leyline_location'] == "school_ground_floor" and quest.maxine_lines == "finalnode":
        actions.append("?locator_screen_school_ground_floor")


label locator_screen_school_art_class:
  "It's hard to tell if someone drew these lines here with a luminescent marker..."
  return

label locator_screen_school_cafeteria:
  "If only these lines could point me in the direction of the best doughnuts..."
  "On second thought, that direction is the [guard]'s booth. Don't need no magic lines to tell me!"
  return

label locator_screen_school_english_class:
  "That looks a bit like an inverted gastrohedron, but without the hexatropinac flexpod..."
  "I'm just making up words."
  return

label locator_screen_school_first_hall:
  "Whoa! A new line has materialized, leading down the main stairs into the entrance hall..."
  $school_first_hall["new_line"] = True
  return

label locator_screen_school_first_hall_final:
  "The lines here converge into a node of some sort..."
  "One line leads off in the direction of each wing."
  return

label locator_screen_school_first_hall_east:
  "This line leads straight into the gym."
  "I'm not sure if following these is the smartest thing..."
  "Next thing you know, you're at the end of the rainbow, and a leprechaun is hitting you over the head with a gold ingot and cussing you out in Irish!"
  return

label locator_screen_school_first_hall_west:
  "It's hard to come full circle when it looks like this..."
  return

label locator_screen_school_ground_floor_west:
  "These lines are red instead of yellow... and they have a very distinct shape..."
  "Are they at all related to the yellow lines?"
  return

label locator_screen_school_gym:
  "This circle connects to the line in the corridor outside."
  "Does that mean what I think it does...?"
  "Yeah, no clue."
  return

label locator_screen_school_nurse_room:
  "The lines here are... what's the word?"
  "Right. There is no word to describe these shapes."
  return

label locator_screen_school_ground_floor:
  "Huh! The final circle surrounds these rows of lockers..."
  $school_ground_floor["final_circle"] = True
  return

label quest_maxine_lines_use_magnet(item):
  if item == 'magnet':
    if quest.maxine_lines['leyline_location'] == "school_first_hall":
      $game.locations[quest.maxine_lines['leyline_location']]['magnet'] = True
      $mc.remove_item(item)
      "Putting magnets in random locations is totally not crazy! [maxine] knows what she's talking about."
    elif quest.maxine_lines['leyline_location'] == "school_first_hall_west":
      $game.locations[quest.maxine_lines['leyline_location']]['magnet'] = True
      $mc.remove_item(item)
      "Not really sure why I'm doing this mumbo-jumbo..."
      "Maybe it's just the need to know?"
    elif quest.maxine_lines['leyline_location'] == "school_gym":
      $game.locations[quest.maxine_lines['leyline_location']]['magnet'] = True
      $mc.remove_item(item)
      "The thirst for the unknown drives me. Or maybe it's just the thirst for [maxine]? Either way..."
  else:
    if quest.maxine_lines['leyline_location'] == "school_first_hall":
      "Imbuing my [item.title_lower] with mysterious arcane properties seems like a quick way to attract a vengeful coven of witches."
      "Magically hot... but also dangerous."
      $quest.maxine_lines.failed_item("leyline", item)
    elif quest.maxine_lines['leyline_location'] == "school_first_hall_west":
      "My [item.title_lower] would make for a beautiful floor decoration, but [lindsey]'s probably just going to trip again."
      $quest.maxine_lines.failed_item("leyline", item)
    elif quest.maxine_lines['leyline_location'] == "school_gym":
      "Putting my [item.title_lower] in this circle could awaken some ancient spirit..."
      "And you can't fuck a spirit, so what's the point?"
      $quest.maxine_lines.failed_item("leyline", item)
  return


label goto_school_leyline_locator(item=None):
  if quest.maxine_lines < "finalnode":
    if game.location in ("school_clubroom","school_homeroom","school_entrance","school_computer_room","school_forest_glade","school_ground_floor"):
      jump quest_maxine_lines_locator_no_lines
  else:
    if game.location in ("school_clubroom","school_homeroom","school_entrance","school_computer_room","school_forest_glade"):
      jump quest_maxine_lines_locator_no_lines
  scene black with Dissolve(.07)
  $quest.maxine_lines['leyline_location'] = game.location.id
  $game.location = "school_leyline_locator"
  $renpy.pause(0.07)
  scene location with Dissolve(.5)
  $set_dialog_mode("")
  return

label quest_maxine_lines_locator_no_lines:
  $noline = renpy.random.randint(1,5)
  if noline == 1:
    "Man, I keep checking this thing more than my phone. Nothing here, though..."
  elif noline == 2:
    "This reminds me of my Game Boy Colour addiction! No lines drawn for either."
  elif noline == 3:
    "No lines here, and maybe that's a relief."
  elif noline == 4:
    "No lines here... whoever drew them must've run out of paint."
  elif noline == 5:
    "Okay, let's see if I've got any matches on Tinder... wait, this is not my phone. No lines either."
  return
