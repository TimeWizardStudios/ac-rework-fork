init python:
  class Item_spray_water(Item):
    icon="items bottle spray_water"
    bottled = True

    def title(item):
      return "Water"
   
    def description(item):
      return "Tides go in. Tides go out. Can't explain that! But you could bottle it."
    
    def actions(cls,actions):
      actions.append("?int_water_bottle")
      actions.append(["consume", "Consume","consume_spray_water"])

label consume_spray_water(item):
  "[flora] always tells me I'm too thirsty."
  "Let's see if this helps."
  $mc.remove_item("spray_water")
  $mc.add_item("spray_empty_bottle")
  "Nope. Just made it worse."
  return True