init python:
  class Item_sugar_cube(Item):
    icon="items sugar_cube"
    
    def title(item):
      return "Sugar Cube"
    
    def description(item):
      return "Say that you'll be mine. Baby, all the time. Sugar cube."
    
    def actions(cls,actions):
      actions.append("?int_sugar_cube")
      #actions.append(["consume", "Consume", "?sugar_cube_consume"])

label int_sugar_cube(item):
  "Why aren't there salt cubes? If only they put me in charge of society, we'd have more useful things."
  return True
