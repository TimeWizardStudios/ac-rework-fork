init python:
  class Item_doughnut(Item):
    icon="items doughnut"
    
    def title(item):
      return "Doughnut"
    
    def description(item):
      return "The [guard]'s favorite snack. Also my favorite snack. Perfect with the famed strawberry juice."
    
    def actions(cls,actions):
      actions.append("?int_doughnut")
      
label int_doughnut(item):
  "The round shape with a hole in the middle tells a secret..."
  "There's more in the cafeteria."
  return True
