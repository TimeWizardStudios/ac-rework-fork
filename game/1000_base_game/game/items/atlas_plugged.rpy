init python:
  class Item_atlas_plugged(Item):
    icon="items book atlas_plugged"
    retain_capital = True
    def title(item):
      return "\"Atlas Plugged, a Tale of Rails, Fails, and Fluffy Tails\""
    
    def description(item):
      return "If you like muscley men turned into obedient kittens, this is the book for you."
    
    def actions(cls,actions):
      actions.append("?int_atlas_plugged")
   
label int_atlas_plugged(item):
  "{i}\"Atlas stood under the heavy sky, sweat dotting his brow and broad shoulders, the massive plug firmly lodged in his—\"{/}"
  "All right! That's... that's enough of that!"
  return True
