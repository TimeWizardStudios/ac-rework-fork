init python:
  class Item_chopin_music_score(Item):

    icon = "items chopin_music_score"

    def title(item):
      return "Chopin Music Score"

    def description(item):
      return "[kate] is both a C-flat and a B major, if you know what I'm saying..."

    def actions(cls,actions):
      actions.append("?chopin_music_score_interact")


label chopin_music_score_interact(item):
  "If it weren't [isabelle], I'd G-flat out refuse to play."
  return True
