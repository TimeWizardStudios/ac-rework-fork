init python:
  class Item_stick(Item):

    icon="items stick_1"

    def title(item):
      return "Stick"

    def description(item):
      return "So, no one told you life was gonna be this way? Stick, stick, stick, stick."

    def actions(cls,actions):
      actions.append("?stick_interact")


init python:
  class Item_stick_two(Item):

    icon="items stick_2"

    def title(item):
      return "Two Sticks"

    def description(item):
      return "Never before has a pair of such fabulous elongated cellulose been seen on this side of the train tracks."

    def actions(cls,actions):
      actions.append("?stick_two_interact")
      actions.append(["consume", "Consume", "stick_two_consume"])


init python:
  class Item_stick_three(Item):

    icon="items stick_3"

    def title(item):
      return "Three Sticks"

    def description(item):
      return "Tying these three sticks together is a lot of work for potentially no payoff. However, [lindsey]'s butt."

    def actions(cls,actions):
      actions.append("?stick_three_interact")
      actions.append(["consume", "Consume", "stick_three_consume"])

  add_recipe("stick","stick","stick_two_combine")
  add_recipe("stick","stick_two","stick_three_combine")
  add_recipe("stick_two","stick_two","stick_x4_combine")


label stick_two_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("stick_two",silent=True)
  $game.notify_modal(None,"Combine","{image= items stick_2}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Two Sticks{/}.",10.0)
  "Two thirds of the way there. Just need one more stick now."
  return

label stick_three_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("stick_three",silent=True)
  $game.notify_modal(None,"Combine","{image= items stick_3}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Three Sticks{/}.",10.0)
  "Perfect! Now I just need to tie them all together."
  return

label stick_x4_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("stick",4,silent=True)
  $game.notify_modal(None,"Combine","{image= items stick_1}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Stick{/}x{color=#900}4{/}.",10.0)
  return

label stick_interact(item):
  "Carrot or stick — neither ever worked on me to get shit done. The woes of being a lazy bum."
  return True

label stick_two_interact(item):
  "We had two sticks and a cock for the whole platoon. And we had to share the cock."
  "Hmm... suddenly gay. Unless it was the amazonian guard or something. Yum."
  return True

label stick_three_interact(item):
  "What's made of wood and is better than two sticks?"
  "A lot of things probably."
  return True

label stick_two_consume(item):
  $mc.remove_item("stick_two")
  $mc.add_item("stick",2)
  return True

label stick_three_consume(item):
  $mc.remove_item("stick_three")
  $mc.add_item("stick",3)
  return True
