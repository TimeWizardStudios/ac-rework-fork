init python:
  class Item_duct_tape(Item):

    icon = "items duct_tape"

    def title(item):
      return "Duct Tape"

    def description(item):
      # return "Perfect for fun and games of every kind."
      return "Perfect for fun and games\nof every kind."

    def actions(cls,actions):
      actions.append("?duct_tape_interact")


label duct_tape_interact(item):
  "Maybe I'll save a strip for when [flora] gets extra annoying?"
  return True
