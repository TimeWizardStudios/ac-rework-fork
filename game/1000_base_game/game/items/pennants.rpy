init python:
  class Item_pennants(Item):
    
    icon="items pennants"
    
    def title(item):
      return "Pennants"
    
    def description(item):
      return "Tiny brightly colored flags. Reminds me of knights, birthday parties, and Nepal."

    def actions(cls,actions):
      actions.append("?pennants_interact")
  
  add_recipe("pennants","glass_shard","combine_pennants_glass_shard")

label pennants_interact(item):
  "[lindsey] would love these. They're right down her alley. She'd probably prefer them baby blue, though."
  return True
