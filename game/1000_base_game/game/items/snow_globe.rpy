init python:
  class Item_snow_globe(Item):

    icon = "items snow_globe"

    def title(item):
      return "Snow Globe"

    def description(item):
      return "A snow globe that appears\nto have an inhabitant."

    def actions(cls,actions):
      actions.append("?snow_globe_interact")

  add_recipe("locked_box_forest","rusty_key","snow_globe_combine")


label snow_globe_interact(item):
  "A single glowing spider has made this globe its home."
  "Oddly enough, there appears to be no cracks in the glass..."
  return True

label snow_globe_combine(item,item2):
  $mc.remove_item("rusty_key")
  "There's no way this key is going to fit—"
  play sound "lock_click"
  "..."
  "No way..."
  "The lock popped right open."
  $mc.remove_item("locked_box_forest")
  $mc.add_item("snow_globe")
  return
