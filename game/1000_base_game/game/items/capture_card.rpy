init python:
  class Item_capture_card(Item):
    
    icon="items capture_card"
    
    def title(item):
      return "Capture Card"
    
    def description(item):
      return "Doesn't look like a card to me. But then again, hard drives don't drive."
    
    def actions(cls,actions):
      actions.append("?capture_card_interact")

label capture_card_interact(item):
  "The link between the past and the future. The gadget of gadgets."
  return True
