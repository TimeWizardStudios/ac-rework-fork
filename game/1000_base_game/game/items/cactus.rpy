init python:
  class Item_cactus(Item):

    icon = "items cactus"

    def title(item):
      return "Cactus"

    def description(item):
      return "What's prickly, strong,\nand goes all night long?\n\nA saguaro."

    def actions(cls,actions):
      actions.append("?cactus_interact")

  add_recipe("cactus","paint_jar","cactus_paint_jar_combine")


label cactus_interact(item):
  "Hmm... I feel like something's missing..."
  "Maybe a bigger pot? There's plenty of old student projects left behind in the art classroom."
  return True

label cactus_paint_jar_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $process_event("items_combined",item1,item2,items_by_id["cactus_in_a_pot"])
  $mc.add_item("cactus_in_a_pot",silent=True)
  "Looking sharp."
  $quest.jo_day["cactus_in_a_pot_crafted"] = True
  return
