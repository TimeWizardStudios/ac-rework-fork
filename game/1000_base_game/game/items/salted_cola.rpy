init python:
  class Item_salted_cola(Item):
   
    icon="items bottle salted_cola"
   
    can_place_to_desk = True
    bottled = True
    consumable = True

    def title(item):
      return "Salted Cola"
  
    def description(item):
      return "There's like globs of white stuff floating around in the cola. Probably nothing to worry about."
  
    def actions(cls,actions):
      actions.append("?int_salted_cola")
      if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
        actions.append(["consume", "Consume","consume_salted_cola_maxine"])
      else:
        actions.append(["consume","Consume","consume_salted_cola"])
      
label int_salted_cola(item):
  "Like drinking carbonated semen. Not that I've tried that or anything..."
  return True

label consume_salted_cola(item):
  $mc.remove_item(item) 
  $mc.add_item("empty_bottle")
  "That's just... fucking hell, that's bad!"
  #$ Inverse love/lust gains for the next hour tbd temp stat change
  return True

label consume_salted_cola_maxine(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my drink?"
  $mc.add_item("letter_from_maxine")
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  return True