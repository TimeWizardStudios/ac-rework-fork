init python:
  class Item_tob_le_bone(Item):

    icon = "items tob_le_bone"

    def title(item):
      return "Tob-le-bone"

    def description(item):
      return "This sweet, tasty, dick-shaped chocolate will make any girl drool."

    def actions(cls,actions):
      actions.append("?tob_le_bone_interact")


label tob_le_bone_interact(item):
  # "Is that a Tob-le-bone in your pocket, or are you just happy to see me?"
  "Is that a Tob-le-bone in your pocket, or are you just happy to see me?{space=-35}"
  return True
