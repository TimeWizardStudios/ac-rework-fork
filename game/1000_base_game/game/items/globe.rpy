init python:
  class Item_globe(Item):
    icon="items globe"
    
    def title(item):
      return "Globe"
    
    def description(item):
      return "Not sure what Atlas complained about. This thing weighs almost nothing."
    
    def actions(cls,actions):
      actions.append("?int_globe")
      
label int_globe(item):
  "Google Maps probably put the producers of these out of business. It's scary when you think about it."
  "Actual world-makers defeated by the digital age."
  return True
      
      
