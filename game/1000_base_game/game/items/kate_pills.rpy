init python:
  class Item_kate_pills(Item):

    icon = "items kate_pills"

    def title(item):
      return kate.name + "'s Pills"

    def description(item):
      return "A bottle of pills that may or\nmay not belong to [kate]."

    def actions(cls,actions):
      actions.append("?kate_pills_interact")


label kate_pills_interact(item):
  "Hmm... Xynoparazyl-something..."
  "It sounds expensive. I guess I should return them."
  return True
