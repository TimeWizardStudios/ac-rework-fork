init python:
  class Item_high_tech_lockpick(Item):
    icon="items high_tech_lock"
    
    def title(item):
      return "High-Tech Lockpick"
    
    def description(item):
      return "Cutting edge technology built from exclusive materials. Never before have locks been such a tiny obstacle."
    
    def actions(cls,actions):
      actions.append("?int_high_tech_lockpick")

  add_recipe("safety_pin","ball_of_yarn","high_tech_lockpick")
  
label int_high_tech_lockpick(item):
  "With the new product from Doors R' Gross, picking locks has never been easier!"
  "Made from stainless titanium and the finest cashmere yarn, this lock pick will change your life."
  "Get it now from your local grocer!"
  "Batteries not included."
  return True
      
      
