init python:
  class Item_bolt(Item):

    icon="items clean_bolt"

    def title(item):
      return "Bolt"

    def description(item):
      return "The sharp point and fine threads allow for very deep and accurate penetration."

    def actions(cls,actions):
      actions.append("?bolt_interact")

  add_recipe("greasy_bolt","tissues","bolt_combine")


label bolt_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("bolt",silent=True)
  $game.notify_modal(None,"Combine","{image= items clean_bolt}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Bolt{/}.",10.0)
  "Well, that's clean as shit now. Never seen a bolt so pristine."
  return

label bolt_interact(item):
  "Hard as steel. Long sleek design. Perfect to screw holes with."
  return True
