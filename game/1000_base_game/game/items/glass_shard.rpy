init python:
  class Item_glass_shard(Item):
    icon="items glass_shard"
    
    def title(item):
      return "Glass Shard"
    
    def description(item):
      return "Very sharp. Very deadly. Not so useful."
    
    def actions(cls,actions):
      actions.append("?int_glass_shard")
      
label int_glass_shard(item):
  "Not exactly cutting edge technology, but maybe it's enough."
  return True
      
      
