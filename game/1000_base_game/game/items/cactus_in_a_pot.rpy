init python:
  class Item_cactus_in_a_pot(Item):

    icon = "items cactus_in_a_pot"

    def title(item):
      return "Cactus in a Pot"

    def description(item):
      return "This cactus is exceptionally succulent."

    def actions(cls,actions):
      actions.append("?cactus_in_a_pot_interact")


label cactus_in_a_pot_interact(item):
  "I saw a cursed video on the dark web about a cactus once..."
  "Never again."
  return True
