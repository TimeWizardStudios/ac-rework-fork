init python:
  class Item_shoebox(Item):

    icon = "items shoebox"

    def title(item):
      return "Shoebox"

    def description(item):
      return "It smells like foot sweat... or maybe it's just my imagination."

    def actions(cls,actions):
      actions.append("?shoebox_interact")

  add_recipe("shoebox","beaver_carcass","quest_mrsl_bot_shoebox")
  add_recipe("shoebox","beaver_pelt","quest_mrsl_bot_shoebox")
  add_recipe("shoebox","fire_axe","quest_mrsl_bot_shoebox")
  add_recipe("shoebox","glass_shard","quest_mrsl_bot_shoebox")
  add_recipe("shoebox","machete","quest_mrsl_bot_shoebox")
  add_recipe("shoebox","nurse_pen","quest_mrsl_bot_shoebox")


label shoebox_interact(item):
  "It's a box. It holds shoes. It's a shoebox!"
  "There is beauty in simplicity."
  return True
