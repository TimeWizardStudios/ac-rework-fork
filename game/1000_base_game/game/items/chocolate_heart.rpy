init python:
  class Item_chocolate_heart(Item):

    icon = "items chocolate_heart"

    def title(item):
      return "Chocolate Heart"

    def description(item):
      return "A sweet heart for a sweetheart."

    def actions(cls,actions):
      actions.append("?chocolate_heart_interact")


label chocolate_heart_interact(item):
  "It looks tasty, but I can't eat the evidence."
  return True
