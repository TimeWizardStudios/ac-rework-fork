init python:
  class Item_kate_fashion_poster(Item):

    icon = "items kate_fashion_poster"

    def title(item):
      return "Kate's Fashion Poster"

    def description(item):
      return "Why does someone so evil\nget to be so pretty?"

    def actions(cls,actions):
      actions.append("?kate_fashion_poster_interact")


label kate_fashion_poster_interact(item):
  "This poster is pure cocaine to the male brain."
  return True
