init python:
  class Item_flora_poster(Item):

    icon="items flora_poster"

    def title(item):
      return flora.name + "'s Poster"

    def description(item):
      return "Judging by this poster, [flora]'s internship might be more exciting than expected."

    def actions(cls,actions):
      actions.append("?flora_poster_interact")


label flora_poster_interact(item):
  "{i}\"Do you have what it takes?\"{/}"
  "{i}\"We're looking for the best and brightest science students.\"{/}"
  "{i}\"The Foundation offers a highly exciting work environment with new experiences and stimulating tasks.\"{/}"
  "{i}\"Welcome to the open house day at the Foundation!\"{/}"
  "{i}\"Hazmat suits and protective gear will be provided by us.\"{/}"
  return True
