init python:
  class Item_string(Item):
    
    icon="items string"
    
    def title(item):
      return "String"
    
    def description(item):
      return "Not exactly a g-string, but I'm still learning the ropes."
    
    def actions(cls,actions):
      actions.append("?string_interact")

  add_recipe("string","pennants","combine_string_pennants")

label string_interact(item):
  "Always wanted to try out bondage, but this string isn't nearly strong enough for the type of quarry it needs to hold."
  return True
