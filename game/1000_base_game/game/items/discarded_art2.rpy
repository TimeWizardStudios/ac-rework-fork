init python:
  class Item_discarded_art2(Item):
    
    icon="items discarded_art2"
    
    def title(item):
      return "Discarded Art"
    
    def description(item):
      return "Practice makes perfect, but this shit still requires a lot of practice."
    
    def actions(cls,actions):
      actions.append("?discarded_art2_interact")
  
  add_recipe("discarded_art2", "paint_buckets", "poolside_story_making_decor")

label discarded_art2_interact(item):
  "Making old art into new pennants. I should receive the Nobel Prize in recycling."
  return True
