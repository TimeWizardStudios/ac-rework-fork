init python:
  class Item_baseball_bat(Item):

    icon="items baseball_bat"

    def title(item):
      return "Baseball Bat"

    def description(item):
      return "Baseballs, snitches, zombies, wooden boats — this baby can smash anything into a homerun."

    def actions(cls,actions):
      actions.append("?baseball_bat_interact")

label baseball_bat_interact(item):
  "I'm rubber, you're glue. Whatever you say bounces off me and makes a bat-sized dent in you."
  return True
