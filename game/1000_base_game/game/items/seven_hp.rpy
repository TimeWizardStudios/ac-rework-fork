init python:
  class Item_seven_hp(Item):
    icon="items bottle seven_hp"
    can_place_to_desk = True
    bottled = True
    consumable = True

    def title(item):
      return "Seven HP"

    def description(item):
      return "Bottled loser tears — that's what the jocks call it."

    def actions(cls,actions):
      actions.append("?int_seven_hp")
      if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
        actions.append(["consume", "Consume","consume_seven_hp_maxine"])
      elif quest.maxine_lines == "shot":
        actions.append(["consume", "Consume","consume_seven_hp_maxine_lines"])
      else:
        actions.append(["consume","Consume","consume_seven_hp"])

label int_seven_hp(item):
  "Sparkling water, except there's nothing sparkly or watery about the taste."
  return True

label consume_seven_hp(item):
  #if quest.tide_pod_special.in_progress:     tbd, tidepod special not in yet
  #  $mc.remove_item("seven_hp")
  #  $mc.add_item("empty_bottle")
  #  "That hit the spot!"
  #  "Man, no wonder the jocks drink this shit. It's easily the most refreshing drink I've had."
  #  "It's like my eyes have been cleared of their fog for the first time."
  #  #All the girls use nude models and sprites until TidePodQuest01 = "Complete"
  #else:
  $mc.remove_item("seven_hp")
  $mc.add_item("empty_bottle")
  "God. If refreshing and disgusting had a lovechild, this would be it."
  #$ +Athletics (temporary: one day)
  #$ -Smarts (temporary: one day) tbd temp stats
  return True

label consume_seven_hp_maxine_lines(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  $quest.maxine_lines['doped_up_now'] = True
  "{i}*Gulp, gulp, gulp*{/}"
  "Ah, that hit the sport! Spot. That hit the spot."
  "Surely, no athletic endeavors are beyond my reach now!"
  return True

label consume_seven_hp_maxine(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my drink?"
  $mc.add_item("letter_from_maxine")
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  return True
