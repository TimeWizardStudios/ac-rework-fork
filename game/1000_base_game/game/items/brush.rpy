init python:
  class Item_brush(Item):
    
    icon="items brush"
    
    def title(item):
      return "Paint Brush"
    
    def description(item):
      return "It takes a certain artistic refinement to truly appreciate a brush of mammoth hair. Fortunately, this one's made from the hair of a dead beaver, which of course anyone can appreciate."

    def actions(cls,actions):
      actions.append("?brush_interact")
     
label brush_interact(item):
  "They say that a single stroke can change the course of history."
  "A great handjob does tend to have that effect."
  return True
