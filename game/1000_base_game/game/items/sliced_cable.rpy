init python:
  class Item_sliced_cable(Item):
    icon="items sliced_cable"
    
    def title(item):
      return "Sliced Cable"
    
    def description(item):
      return "The satisfying perfection that is unique to makeshift cable jobs."

    
    def actions(cls,actions):
      actions.append("?int_sliced_cable")
  
label int_sliced_cable(item):
  "This thing will either give me unlimited power or burn down the school."
  "A win-win situation."
  return True
      