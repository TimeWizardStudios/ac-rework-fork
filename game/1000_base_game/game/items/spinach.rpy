init python:
  class Item_spinach(Item):

    icon = "items spinach"

    def title(item):
      return spinach.name

    def description(item):
      return maxine.name +"'s pussy."

    def actions(cls,actions):
      actions.append("?spinach_interact")


label spinach_interact(item):
  show spinach neutral with Dissolve(.5)
  spinach neutral "Meow?"
  mc "Shhh!"
  mc "Stay in my backpack, girl. I'll give you a sardine later."
  spinach tongueout "Meow!"
  hide spinach with Dissolve(.5)
  return True
