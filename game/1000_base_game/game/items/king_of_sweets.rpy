init python:
  class Item_king_of_sweets(Item):

    icon = "items king_of_sweets"

    def title(item):
      return "King of Sweets"

    def description(item):
      return "It's not only tasteful, it is tasty."

    def actions(cls,actions):
      actions.append("?king_of_sweets_interact")


label king_of_sweets_interact(item):
  "A culinary magnum opus. Unique, some say. Luckily, others say."
  return True
