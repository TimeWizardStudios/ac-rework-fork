init python:
  class Item_pepelepsi(Item):
    
    icon="items bottle pepelepsi"
    
    can_place_to_desk = True
    bottled = True
    consumable = True
    
    def title(item):
      return "Pepelepsi"
    
    def description(item):
      return "If motor oil and amphetamine put their dicks in a bottle of cola at the same time, Pepelepsi would be the outcome."
    
    def actions(cls,actions):
      actions.append("?int_pepelepsi")
      if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
        actions.append(["consume", "Consume","consume_pepelepsi_maxine"])
      elif not (quest.flora_cooking_chilli.in_progress and quest.flora_cooking_chilli >= "get_milk"):
        actions.append(["consume","Consume","consume_pepelepsi"])

label consume_pepelepsi_maxine(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my drink?"
  $mc.add_item("letter_from_maxine")
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  return True
        
label int_pepelepsi(item):
  "With ingredients scraped off the bottom of toxic waste barrels, the industrial flavor has become synonymous with the Pepelepsi brand."
  "The Devil in a can."
  return True

label consume_pepelepsi(item):
  $mc.remove_item(item) 
  $mc.add_item("empty_bottle")
  "Mmm. The taste is fine, but it always makes me feel really hyper after a while."
  return True
