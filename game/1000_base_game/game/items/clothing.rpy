init python:

  def advance_day():
    while game.hour != 7:
      game.advance()

  def test_items():
    check = False
    characters = ["flora","isabelle","jacklyn","jo","kate","maxine","mrsl","nurse","lindsey"]
    for item in items_by_id:
      for character in characters:
        if character not in item:
          check = True
        else:
          check = False
          break
      if check:
        #print(item)
        mc.add_item(item)
        check = False

  def fill_inventory(count=1):
    for item in items_by_id:
      if items_by_id[item].slot is None:
        mc.add_item(item,count=count,silent=True)
    mc.inv.sort(key=lambda item:items_by_id[item[0]].title().replace('"',''))
    return "Your inventory has been filled!"

  def clear_inventory(count=None):
    for item in items_by_id:
      if items_by_id[item].slot is None:
        if count:
          mc.remove_item(item,count=count,silent=True)
        else:
          mc.remove_item(item,count=mc.owned_item_count(item),silent=True)
    return "Your inventory has been cleared!"


#General clothes:
#mrsl brooch?

#mc_clothes:


#character clothes:
#flora
  class Item_flora_hat(Item):
    slot="hat"
    state_suffix="_hat"
  class Item_flora_sun_hat(Item):
    slot="hat"
    state_suffix="_sun_hat"
  class Item_flora_referee_cap(Item):
    slot="hat"
    state_suffix="_referee_cap"
  class Item_flora_referee_shirt(Item):
    slot="blindfold"
    state_suffix="_referee_shirt"
  class Item_flora_armor(Item):
    slot="shirt"
    state_suffix="_armor"
  class Item_flora_trench_coat(Item):
    slot="shirt"
    state_suffix="_trench_coat"
  class Item_flora_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_flora_business_shirt(Item):
    slot="shirt"
    state_suffix="_business_shirt"
  class Item_flora_blouse(Item):
    slot="shirt"
    state_suffix="_blouse"
  class Item_flora_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_flora_purple_bra(Item):
    slot="bra"
    state_suffix="_purple_bra"
  class Item_flora_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_flora_skirt(Item):
    slot="pants"
    state_suffix="_skirt"
  class Item_flora_dress(Item):
    slot="pants"
    state_suffix="_dress"
  class Item_flora_panties(Item):
    slot="panties"
    state_suffix="_panties"
  class Item_flora_striped_panties(Item):
    slot="panties"
    state_suffix="_striped_panties"
  class Item_flora_vines(Item):
    slot="vines"
    state_suffix="_vines"
  class Item_flora_cum(Item):
    slot="cum"
    state_suffix="_cum"
  class Item_flora_blindfold(Item):
    slot="blindfold"
    state_suffix="_blindfold"
#isabelle
  class Item_isabelle_glasses(Item):
    slot="hat"
    state_suffix="_glasses"
  class Item_isabelle_tiara(Item):
    slot="hat"
    state_suffix="_tiara"
  class Item_isabelle_collar(Item):
    slot="collar"
    state_suffix="_collar"
  class Item_isabelle_jacket(Item):
    slot="jacket"
    state_suffix="_jacket"
  class Item_isabelle_party_dress(Item):
    slot="shirt"
    state_suffix="_party_dress"
  class Item_isabelle_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_isabelle_top(Item):
    slot="shirt"
    state_suffix="_top"
  class Item_isabelle_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_isabelle_green_bra(Item):
    slot="bra"
    state_suffix="_green_bra"
  class Item_isabelle_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_isabelle_skirt(Item):
    slot="pants"
    state_suffix="_skirt"
  class Item_isabelle_panties(Item):
    slot="panties"
    state_suffix="_panties"
  class Item_isabelle_green_panties(Item):
    slot="panties"
    state_suffix="_green_panties"
#jacklyn
  class Item_jacklyn_chained_choker(Item):
    slot="choker"
    state_suffix="_chained_choker"
  class Item_jacklyn_squidpin(Item):
    slot="pin"
    state_suffix="_squidpin"
  class Item_jacklyn_dress(Item):
    slot="shirt"
    state_suffix="_dress"
  class Item_jacklyn_jacket(Item):
    slot="shirt"
    state_suffix="_jacket"
  class Item_jacklyn_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_jacklyn_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_jacklyn_orange_bra(Item):
    slot="bra"
    state_suffix="_orange_bra"
  class Item_jacklyn_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_jacklyn_shorts(Item):
    slot="pants"
    state_suffix="_shorts"
  class Item_jacklyn_high_waist_fishnet(Item):
    slot="fishnet"
    state_suffix="_high_waist_fishnet"
  class Item_jacklyn_thigh_high_fishnet(Item):
    slot="fishnet"
    state_suffix="_thigh_high_fishnet"
  class Item_jacklyn_panties(Item):
    slot="panties"
    state_suffix="_panties"
  class Item_jacklyn_orange_panties(Item):
    slot="panties"
    state_suffix="_orange_panties"
#jo
  class Item_jo_glasses(Item):
    slot="glasses"
    state_suffix="_glasses"
  class Item_jo_coat(Item):
    slot="coat"
    state_suffix="_coat"
  class Item_jo_blazer(Item):
    slot="shirt"
    state_suffix="_blazer"
  class Item_jo_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_jo_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_jo_bikini_top(Item):
    slot="bra"
    state_suffix="_bikini_top"
  class Item_jo_white_bra(Item):
    slot="bra"
    state_suffix="_white_bra"
  class Item_jo_skirt(Item):
    slot="pants"
    state_suffix="_skirt"
  class Item_jo_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_jo_panties(Item):
    slot="panties"
    state_suffix="_panties"
  class Item_jo_white_panties(Item):
    slot="panties"
    state_suffix="_white_panties"
  class Item_jo_bikini_bottom(Item):
    slot="panties"
    state_suffix="_bikini_bottom"
#kate
  class Item_kate_necklace(Item):
    slot="necklace"
    state_suffix="_necklace"
  class Item_kate_choker(Item):
    slot="necklace"
    state_suffix="_choker"
  class Item_kate_angel_costume(Item):
    slot="shirt"
    state_suffix="_angel_costume"
  class Item_kate_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_kate_cheerleader_top(Item):
    slot="shirt"
    state_suffix="_cheerleader_top"
  class Item_kate_bardot_top(Item):
    slot="shirt"
    state_suffix="_bardot_top"
  class Item_kate_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_kate_cheerleader_bra(Item):
    slot="bra"
    state_suffix="_cheerleader_bra"
  class Item_kate_blue_bra(Item):
    slot="bra"
    state_suffix="_blue_bra"
  class Item_kate_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_kate_cheerleader_skirt(Item):
    slot="pants"
    state_suffix="_cheerleader_skirt"
  class Item_kate_skirt(Item):
    slot="pants"
    state_suffix="_skirt"
  class Item_kate_panties(Item):
    slot="panties"
    state_suffix="_panties"
  class Item_kate_cheerleader_panties(Item):
    slot="panties"
    state_suffix="_cheerleader_panties"
  class Item_kate_blue_panties(Item):
    slot="panties"
    state_suffix="_blue_panties"
  class Item_kate_knee_high_boots(Item):
    slot="boots"
    state_suffix="_knee_high_boots"
#casey
  class Item_casey_cheerleader_top(Item):
    slot="top"
    state_suffix="_cheerleader_top"
  class Item_casey_cheerleader_bra(Item):
    slot="bra"
    state_suffix="_cheerleader_bra"
  class Item_casey_cheerleader_skirt(Item):
    slot="skirt"
    state_suffix="_cheerleader_skirt"
  class Item_casey_cheerleader_panties(Item):
    slot="panties"
    state_suffix="_cheerleader_panties"
#lacey
  class Item_lacey_cheerleader_top(Item):
    slot="top"
    state_suffix="_cheerleader_top"
  class Item_lacey_cheerleader_bra(Item):
    slot="bra"
    state_suffix="_cheerleader_bra"
  class Item_lacey_cheerleader_skirt(Item):
    slot="skirt"
    state_suffix="_cheerleader_skirt"
  class Item_lacey_cheerleader_panties(Item):
    slot="panties"
    state_suffix="_cheerleader_panties"
#stacy
  class Item_stacy_cheerleader_top(Item):
    slot="top"
    state_suffix="_cheerleader_top"
  class Item_stacy_cheerleader_bra(Item):
    slot="bra"
    state_suffix="_cheerleader_bra"
  class Item_stacy_cheerleader_skirt(Item):
    slot="skirt"
    state_suffix="_cheerleader_skirt"
  class Item_stacy_cheerleader_panties(Item):
    slot="panties"
    state_suffix="_cheerleader_panties"
#lindsey
  class Item_lindsey_hat(Item):
    slot="hat"
    state_suffix="_hat"
  class Item_lindsey_towel(Item):
    slot="shirt"
    state_suffix="_towel"
  class Item_lindsey_mummy_costume(Item):
    slot="shirt"
    state_suffix="_mummy_costume"
  class Item_lindsey_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_lindsey_tshirt(Item):
    slot="shirt"
    state_suffix="_tshirt"
  class Item_lindsey_mcshirt(Item):
    slot="shirt"
    state_suffix="_mcshirt"
  class Item_lindsey_cropped_hoodie(Item):
    slot="shirt"
    state_suffix="_cropped_hoodie"
  class Item_lindsey_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_lindsey_skirt(Item):
    slot="pants"
    state_suffix="_skirt"
  class Item_lindsey_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_lindsey_sweatpants(Item):
    slot="pants"
    state_suffix="_sweatpants"
  class Item_lindsey_panties(Item):
    slot="panties"
    state_suffix="_panties"
#mrsl
  class Item_mrsl_ponytail(Item):
    slot = "hairstyle"
    state_suffix = "_ponytail"
  class Item_mrsl_panties(Item):
    slot = "panties"
    state_suffix = "_panties"
  class Item_mrsl_black_panties(Item):
    slot = "panties"
    state_suffix = "_black_panties"
  class Item_mrsl_workout_panties(Item):
    slot = "panties"
    state_suffix = "_workout_panties"
  class Item_mrsl_leggings(Item):
    slot = "pants"
    state_suffix = "_leggings"
  class Item_mrsl_bra(Item):
    slot = "bra"
    state_suffix = "_bra"
  class Item_mrsl_black_bra(Item):
    slot = "bra"
    state_suffix = "_black_bra"
  class Item_mrsl_workout_bra(Item):
    slot = "bra"
    state_suffix = "_workout_bra"
  class Item_mrsl_dress(Item):
    slot = "dress"
    state_suffix = "_dress"
  class Item_mrsl_bikini(Item):
    slot = "dress"
    state_suffix = "_bikini"
  class Item_mrsl_red_dress(Item):
    slot = "dress"
    state_suffix = "_red_dress"
  class Item_mrsl_coat(Item):
    slot = "coat"
    state_suffix = "_coat"
#nurse
  class Item_nurse_sun_hat(Item):
    slot="hat"
    state_suffix="_sun_hat"
  class Item_nurse_hat(Item):
    slot="hat"
    state_suffix="_hat"
  class Item_nurse_pendant(Item):
    slot="pendant"
    state_suffix="_pendant"
  class Item_nurse_hiking_gear(Item):
    slot="outfit"
    state_suffix="_hiking_gear"
  class Item_nurse_outfit(Item):
    slot="outfit"
    state_suffix="_outfit"
  class Item_nurse_nice_dress(Item):
    slot="shirt"
    state_suffix="_nice_dress"
  class Item_nurse_sophisticated_dress(Item):
    slot="shirt"
    state_suffix="_sophisticated_dress"
  class Item_nurse_revealing_dress(Item):
    slot="shirt"
    state_suffix="_revealing_dress"
  class Item_nurse_dress(Item):
    slot="shirt"
    state_suffix="_dress"
  class Item_nurse_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_nurse_black_bra(Item):
    slot="bra"
    state_suffix="_black_bra"
  class Item_nurse_green_bra(Item):
    slot="bra"
    state_suffix="_green_bra"
  class Item_nurse_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_nurse_black_panties(Item):
    slot="panties"
    state_suffix="_black_panties"
  class Item_nurse_green_panties(Item):
    slot="panties"
    state_suffix="_green_panties"
  class Item_nurse_pantys(Item):
    slot="panties"
    state_suffix="_pantys"
  class Item_nurse_masturbating(Item):
    slot="outfit"
    state_suffix="_masturbating"
#maxine
  class Item_maxine_boat_captain_hat(Item):
    slot="hat"
    state_suffix="_boat_captain_hat"
  class Item_maxine_hat(Item):
    slot="hat"
    state_suffix="_hat"
  class Item_maxine_necklace(Item):
    slot="necklace"
    state_suffix="_necklace"
  class Item_maxine_snorkel(Item):
    slot="glasses"
    state_suffix="_snorkel"
  class Item_maxine_glasses(Item):
    slot="glasses"
    state_suffix="_glasses"
  class Item_maxine_hazmat_suit(Item):
    slot="shirt"
    state_suffix="_hazmat_suit"
  class Item_maxine_armor(Item):
    slot="shirt"
    state_suffix="_armor"
  class Item_maxine_boat_captain_uniform(Item):
    slot="shirt"
    state_suffix="_boat_captain_uniform"
  class Item_maxine_diving_suit(Item):
    slot="shirt"
    state_suffix="_diving_suit"
  class Item_maxine_sweater(Item):
    slot="shirt"
    state_suffix="_sweater"
  class Item_maxine_shirt(Item):
    slot="shirt"
    state_suffix="_shirt"
  class Item_maxine_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_maxine_black_bra(Item):
    slot="bra"
    state_suffix="_black_bra"
  class Item_maxine_pants(Item):
    slot="pants"
    state_suffix="_pants"
  class Item_maxine_skirt(Item):
    slot="pants"
    state_suffix="_skirt"
  class Item_maxine_panties(Item):
    slot="panties"
    state_suffix="_panties"
  class Item_maxine_black_panties(Item):
    slot="panties"
    state_suffix="_black_panties"
#maya
  class Item_maya_hair_towel(Item):
    slot="hat"
    state_suffix="_hair_towel"
  class Item_maya_armor(Item):
    slot="jacket"
    state_suffix="_armor"
  class Item_maya_jacket(Item):
    slot="jacket"
    state_suffix="_jacket"
  class Item_maya_dress(Item):
    slot="dress"
    state_suffix="_dress"
  class Item_maya_towel(Item):
    slot="dress"
    state_suffix="_towel"
  class Item_maya_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_maya_panties(Item):
    slot="panties"
    state_suffix="_panties"
#ria
  class Item_ria_necklace(Item):
    slot="necklace"
    state_suffix="_necklace"
  class Item_ria_coverall(Item):
    slot="shirt"
    state_suffix="_coverall"
  class Item_ria_bra(Item):
    slot="bra"
    state_suffix="_bra"
  class Item_ria_panties(Item):
    slot="panties"
    state_suffix="_panties"
