init python:
  class Item_specialized_fuse_cable_replacement_tool(Item):

    icon="items specialized_fuse_cable_replacement_tool"

    def title(item):
      return "Specialized Fuse Cable Replacement Tool"

    def description(item):
      return "A contraption with a unique purpose. Could be worth a fortune if patented."

    def actions(cls,actions):
      actions.append("?specialized_fuse_cable_replacement_tool_interact")

  add_recipe("plastic_fork", "lollipop_2", "specialized_fuse_cable_replacement_tool_combine")


label specialized_fuse_cable_replacement_tool_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("specialized_fuse_cable_replacement_tool",silent=True)
  $game.notify_modal(None,"Combine","{image= items specialized_fuse_cable_replacement_tool}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Specialized Fuse Cable Replacement Tool{/}.",10.0)
  "It sure as hell ain't pretty, but hopefully it's not useless."
  "Ugly and useless would be a shitty combo. I should know."
  return

label specialized_fuse_cable_replacement_tool_interact(item):
  "Leonardo Da Vinci looks silly now, doesn't he?"
  return True
