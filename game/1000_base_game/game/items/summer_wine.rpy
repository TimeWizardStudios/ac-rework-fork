init python:
  class Item_summer_wine(Item):

    icon="items summer_wine"

    def title(item):
      return "Summer Wine"

    def description(item):
      return "Never been a fan of wine, but this tastes like an angel's kiss."

    def actions(cls,actions):
      actions.append("?summer_wine_interact")

label summer_wine_interact(item):
  "Let the wine fill our stomachs and hearts with courage and tranquility!"
  "Or at the very least quench our thirst for truth and meaning — in vino veritas!"
  return True
