init python:
  class Item_note_to_mrsl(Item):

    icon = "items note_to_mrsl"

    def title(item):
      return "Note to " + mrsl.name

    def description(item):
      # return "Not exactly a message in a bottle... but desperate times, and all that jazz."
      return "Not exactly a message in a\nbottle... but desperate times,\nand all that jazz."

    def actions(cls,actions):
      actions.append("?note_to_mrsl_interact")


label note_to_mrsl_interact(item):
  "{i}\"These buns are still in need of toasting.\"{/}"
  "{i}\"Please allow me to use your grill.\"{/}"
  return True
