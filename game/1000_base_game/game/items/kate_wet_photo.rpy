init python:
  class Item_kate_wet_photo(Item):

    icon="items kate_wet_photo"

    def title(item):
      return kate.name + "'s Soaked Photo"

    def description(item):
      return "[kate] in her natural wet state."

    def actions(cls,actions):
      actions.append("?kate_wet_photo_interact")


label kate_wet_photo_interact(item):
  show misc kate_soaked with Dissolve(.5)
  "Man, I  wish I could soak her every day."
  "Alas, I'm not Chad."
  hide misc kate_soaked with Dissolve(.5)
  return True
