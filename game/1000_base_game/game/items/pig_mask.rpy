init python:
  class Item_pig_mask(Item):

    icon = "items pig_mask"

    def title(item):
      return "Pig Mask"

    def description(item):
      return "This thing is straight out of\na nightmare..."

    def actions(cls,actions):
      actions.append("?pig_mask_interact")


label pig_mask_interact(item):
  "Silence of the Pigs. The real horror story."
  return True
