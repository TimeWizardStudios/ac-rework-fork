init python:
  class Item_permission_slip(Item):
    icon="items permission_slip"
    
    def title(item):
      return "Permission Slip"
    
    def description(item):
      return "Just a regular locker permission slip. Nothing out of the ordinary."
   
    def actions(cls,actions):
      actions.append("?int_permission_slip")
      
label int_permission_slip(item):
    "Her handwriting is a bit hard to read, but at least it's in the school's official red ink."
    return True
  