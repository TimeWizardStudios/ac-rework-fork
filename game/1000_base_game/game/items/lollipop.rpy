init python:
  class Item_lollipop(Item):
   
    icon="items lollipop_1"
    consumable = True
    can_place_to_desk = True
    
    def title(item):
      return "Lollipop"
    
    def description(item):
      return "With a taste of cucumber and gourmet grass. No wonder it's unpopular."
    
    def actions(cls,actions):
      actions.append("?lollipop_interact")
      actions.append(["consume", "Consume", "?lollipop_consume"])
     
label lollipop_interact(item):
  "Suckers come in all shapes and forms. My unshapely ass should know."
  return True

label lollipop_consume(item):
  "Unappetizing, but also alluring. Anyone sucking on this baby is going to feel sexy."
  $mc.charisma+=1
  $mc.remove_item("lollipop")
  $mc.add_item("wrapper")
  return True  
