init python:
  class Item_mop(Item):

    icon="items mop"

    def title(item):
      return "Janitor's Mop"

    def description(item):
      return "The tip smells a bit funky. Olfactorily, it's a combination of heaven and hell."

    def actions(cls,actions):
      actions.append("?mop_interact")


label mop_interact(item):
  "If anyone tries to mess with me now, I'll mop the floor with them."
  "...or get it shoved up my butt. Probably more plausible."
  return True
