init python:
  class Item_fourth_note_to_mrsl(Item):

    icon = "items second_note_to_mrsl"

    def title(item):
      return "Fourth Note to " + mrsl.name

    def description(item):
      # return "Not exactly a message in a bottle... but desperate times, and all that jazz."
      return "Not exactly a message in a\nbottle... but desperate times,\nand all that jazz."

    def actions(cls,actions):
      actions.append("?fourth_note_to_mrsl_interact")


label fourth_note_to_mrsl_interact(item):
  "{i}\"Popular box for a popular guy.\"{/}"
  "{i}\"I'm not giving up hope just yet.\"{/}"
  return True
