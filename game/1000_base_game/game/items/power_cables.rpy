init python:
  class Item_power_cable_red(Item):

    icon = "items power_cable_red"

    def title(item):
      return "Red Cable"

    def description(item):
      return "A color-coded cable. What's the code? No one knows."

    def actions(cls,actions):
      actions.append("?power_cable_red_interact")


label power_cable_red_interact(item):
  "A regularly sized cable..."
  "Okay, maybe it's a bit under average."
  return True


init python:
  class Item_power_cable_blue(Item):

    icon = "items power_cable_blue"

    def title(item):
      return "Blue Cable"

    def description(item):
      return "A color-coded cable. What's the code? No one knows."

    def actions(cls,actions):
      actions.append("?power_cable_blue_interact")


label power_cable_blue_interact(item):
  "When you don't look at this cable, it appears slightly larger."
  "But in reality, it's not that big."
  return True


init python:
  class Item_power_cable_yellow(Item):

    icon = "items power_cable_yellow"

    def title(item):
      return "Yellow Cable"

    def description(item):
      return "A color-coded cable. What's the code? No one knows."

    def actions(cls,actions):
      actions.append("?power_cable_yellow_interact")


label power_cable_yellow_interact(item):
  "When you talk about this cable, it's so large!"
  "But when you look at it... not so much."
  return True
