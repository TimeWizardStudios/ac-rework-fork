init python:
  class Item_umbrella(Item):
    icon="items umbrella"
    
    def title(item):
      return "Umbrella"
    
    def description(item):
      return "She probably thinks of me too sometimes." 
    
    def actions(cls,actions):
      actions.append("?int_umbrella")

label int_umbrella(item):
  "Told you I'll be here forever. That restraining order's clever."
  "Said I'll always be outside. Took an oath I'ma stick it out to the end."
  "Now that it's raining more than ever."
  "Know that I want us together."
  "I'll just wait under your umbrella, ella, eh eh." 
  return True 
