init python:
  class Item_makeshift_easel(Item):
    icon="items makeshift_easel"
    
    def title(item):
      return "Makeshift Easel"
    
    def description(item):
      return "To capture true art, you need to look inside yourself, search for that spark of inspiration to guide your brush... and also pray that the rickety legs of this piece of shit holds steady."
    
    def actions(cls,actions):
      actions.append("?makeshift_easel_interact")

  add_recipe("stick_three","ball_of_yarn","makeshift_easel_combine")

   
label makeshift_easel_interact(item):
  "The easel — the painter's best friend..."
  "Apart from the paint... and the brush... and the canvas... and inspiration."
  "The easel — the painter's kinda annoying and rickety friend, who falls over from the mildest gust of wind."
  return True

label makeshift_easel_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("makeshift_easel",silent=True)
  $game.notify_modal(None,"Combine","{image= items makeshift_easel}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Makeshift Easel{/}.",10.0)
  if quest.lindsey_book=="easel":
    "That's ugly at best, but hopefully it can hold a canvas without falling apart..."
    "[lindsey]'s going to be happy."
    $quest.lindsey_book.advance("easel_created")
  else:
    "I don't know why I made this..." #TBD proper dialogue line
  return
