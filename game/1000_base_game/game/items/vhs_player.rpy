init python:
  class Item_vhs_player(Item):

    icon="items vhs_player" 

    def title(item):
      return "VHS Player"

    def description(item):
      return "Busting a nut or busting a block... the difference is in the movie genre."

    def actions(cls,actions):
      actions.append("?vhs_player_interact")


label vhs_player_interact(item):
  "Before the time of man, dinosaurs roamed the earth."
  "Despite its antique design, this VHS player was in fact not a jurassic creation."
  "Unless you count the plastic bits."
  return True
