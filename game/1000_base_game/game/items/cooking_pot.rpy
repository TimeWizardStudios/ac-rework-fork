init python:
  class Item_cooking_pot(Item):

    icon="items cooking_pot"

    def title(item):
      return "Cooking Pot"

    def description(item):
      return "Load with food. Load with explosives. Either way, muck-BANG!"
   
    def actions(cls,actions):
      actions.append("?int_cooking_pot")
      
label int_cooking_pot(item):
  "Aptly nicknamed Big Bertha, this behemoth can be operated both as a cooking pot and a muzzle-loaded mortar platform."
  return True