init python:
  class Item_big_pile_of_wood(Item):

    icon = "items big_pile_of_wood"

    def title(item):
      return "Wood"

    def description(item):
      # return "Driftwood, redwood, my wood, hardwood."
      return "Driftwood, redwood,\nmy wood, hardwood."

    def actions(cls,actions):
      actions.append("?big_pile_of_wood_interact")


label big_pile_of_wood_interact(item):
  "This was once a living flourishing tree. Unlucky, bitch."
  return True
