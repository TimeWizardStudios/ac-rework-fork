init python:
  class Item_tide_pods(Item):
    icon="items tide_pods"

    can_place_to_desk=True
    consumable = True
    
    def title(item):
      return "Laundry Detergent"
   
    def description(item):
      return "A light snack that makes teeth brushing obsolete."
    
    def actions(cls,actions):
      actions.append("?int_tide_pods")
      actions.append(["consume","Consume","?consume_tide_pods"])
      
label int_tide_pods(item):
  "Tiny pods of highly concentrated chemicals. Perfect for consumption."
  return True

label consume_tide_pods(item):
  $mc.remove_item(item)
  "Mmm. Tastes like synthetic death. Pungent. Like daggers in my throat."
  "Oh god. This wasn't at all..."
  #Fade to black
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  "Is this what it feels like to be washed up?"
  "Ugh, my insides..."
  hide black with fadehold
  $set_dialog_mode()
  "God, my head is pounding..."
  "The thirst is unbearable..."
  "Need to find something to drink fast."
  return True
