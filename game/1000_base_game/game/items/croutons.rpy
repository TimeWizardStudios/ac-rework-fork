init python:
  class Item_croutons(Item):
    
    icon="items croutons"
    consumable = True
    
    def title(item):
      return "Croutons"
    
    def description(item):
      return "Fancy crumbs. The cornerstone of any Roman salad."
    
    def actions(cls,actions):
      actions.append("?croutons_interact")
      actions.append(["consume", "Consume", "?consume_croutons"])
     
label croutons_interact(item):
  "This is what little Nero and little Julius tossed out behind them so that they'd find their way out of the Colosseum."
  return True

label consume_croutons(item):
  "Probably shouldn't eat these..."
  "..."
  "{i}*Crunch, crunch, crunch*{/}" #italics
  "..."
  "Probably best to save some at least for the salad."
  "..."
  "{i}*Crunch, crunch, crunch*{/}" #italics
  "..."
  "Huh. Only one little crouton left."
  "Well, no use saving it."
  "{i}*Crunch, crunch, crunch*{/}" #italics
  $mc.remove_item(item)
  $home_kitchen["croutons_eaten"] = True
  $home_kitchen["croutons_activated"] = False
  $home_kitchen["croutons_taken"] = False
  return True
