init python:
  class Item_buried_box(Item):
    pass

  class Item_locked_box_forest(Item):

    icon = "items locked_box"

    def title(item):
      return "Locked Box"

    def description(item):
      # return "Wrapped in chains and with a rusty busty lock."
      return "Wrapped in chains and\nwith a rusty busty lock."

    def actions(cls,actions):
      actions.append("?locked_box_forest_interact")


label locked_box_forest_interact(item):
    "Whoever locked this box didn't want anyone getting in..."
    "...or worse, getting out..."
    return True
