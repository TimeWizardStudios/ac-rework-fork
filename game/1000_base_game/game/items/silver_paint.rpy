init python:
  class Item_silver_paint(Item):
    
    icon="items silver_paint"
    
    def title(item):
      return "Silver Paint"
    
    def description(item):
      return "With the help of this bucket of paint, I can finally realize my life long dream.\n\nBecoming budget Midas."
    
    def actions(cls,actions):
      actions.append("?silver_paint_interact")
   
label silver_paint_interact(item):
  "Underneath the shiny exterior, we're all just looking for a heart."
  "That's the silver lining."
  return True
