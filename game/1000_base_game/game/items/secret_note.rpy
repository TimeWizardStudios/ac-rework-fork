init python:
  class Item_secret_note(Item):

    icon="items secret_note"

    def title(item):
      return "Secret Note"

    def description(item):
      return "Hmmm... there's no big red X to mark the spot."

    def actions(cls,actions):
      actions.append("secret_note_interact")

  add_recipe("secret_note","water_bottle","secret_note_revealed")
  add_recipe("secret_note","spray_water","secret_note_revealed")


label secret_note_interact(item):
  show misc secret_note_1 with Dissolve(.5)
  pause
  hide misc secret_note_1 with Dissolve(.5)
  return True

label secret_note_revealed(item, item2):
  show misc secret_note_1 with Dissolve(.5)
  show misc tsn as secret_note_reveal with Dissolve(2)
  pause
  hide secret_note_reveal
  hide misc secret_note_1
  with Dissolve(.5)
  return
