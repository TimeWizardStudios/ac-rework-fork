init python:
  class Item_hex_vex_and_texmex(Item):

    icon = "items book hex_vex_and_texmex"
    retain_capital = True

    def title(item):
      return "\"Hex, Vex, and TexMex: The Beginner Spellweaver's\nTasty Guide and Recipe Book\" by Nicksy Steve"

    def description(item):
      return "I did say I should start\nreading more..."

    def actions(cls,actions):
      if quest.maya_spell == "book":
        actions.append("quest_maya_spell_book_hex_vex_and_texmex")
      actions.append("?hex_vex_and_texmex_interact")


label hex_vex_and_texmex_interact(item):
  "Well, would you look at that? The perfect green chili enchilada recipe!"
  return True
