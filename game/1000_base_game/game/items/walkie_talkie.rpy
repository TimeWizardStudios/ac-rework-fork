init python:
  class Item_walkie_talkie(Item):

    icon = "items walkie_talkie"

    def title(item):
      return "Walkie-Talkie"

    def description(item):
      return "{i}Kch!{/}"

    def actions(cls,actions):
      if quest.nurse_venting == "bottle" and (game.location == "school_entrance" or game.location.id.startswith("home_")):
        actions.append("?quest_nurse_venting_walkie_talkie_home")
      elif "stuck" > quest.nurse_venting > "ready":
        actions.append("quest_nurse_venting_walkie_talkie")
      else:
        actions.append("?walkie_talkie_interact")


label walkie_talkie_interact(item):
  "I've got two walkie-talkies, but no one to talk to. Fuck my life."
  return True
