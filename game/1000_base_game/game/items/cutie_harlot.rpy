init python:
  class Item_cutie_harlot(Item):

    icon = "items cutie_harlot"

    def title(item):
      return "Scarlet Starlet's Cutie-Harlot"

    def description(item):
      return "Double layered full grain leather. Every girl's dream skirt."

    def actions(cls,actions):
      actions.append("?cutie_harlot_interact")


label cutie_harlot_interact(item):
  "[flora] will worship me after she gets this skirt."
  "At least, that's the plan..."
  "Disclaimer. Plans aren't my strong suit."
  return True
