init python:
  class Item_kettle(Item):
    icon="items kettle"
    
    def title(item):
      return "Kettle"
    
    def description(item):
      return "Tea time is such a dying concept. Why don't we have pepelepsi time?"
    
    def actions(cls,actions):
      actions.append("?int_kettle")
      #actions.append(["consume", "Consume", "?kettle_consume"])

label int_kettle(item):
  "The kettle's calling me an incel. A case of utmost sanc-tea-mony."
  return True
