init python:
  class Item_monkey_wrench(Item):
    icon="items monkey_wrench"
    
    def title(item):
      return "Monkey Wrench"
    
    def description(item):
      return "Even a fucking baboon would be able to use a tool this simple. Hence the name."
    
    def actions(cls,actions):
      actions.append("?int_monkey_wrench")
      
label int_monkey_wrench(item):
  "Tighten it enough around your finger and you can stop the blood flow. This way you can give someone the purple finger."
  "Truly, this is one of the most useful tools."
  return True
      
      
