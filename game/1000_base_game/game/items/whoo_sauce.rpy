init python:
  class Item_whoo_sauce(Item):
    
    icon="items whoo_sauce"
    consumable = True
    
    def title(item):
      return "Whooshster™ Sauce"
    
    def description(item):
      return "The recipe is the Vatican's most guarded secret. It is said that this sauce alone made the Last Supper a culinary success."
    
    def actions(cls,actions):
      actions.append("?whoo_sauce_interact")
      actions.append(["consume", "Consume", "?consume_whoo_sauce"])
     
label whoo_sauce_interact(item):
  "Fitted with a built in combination lock and child-resistant packaging. Few people ever get to taste the magnificence that is the Whooshster™ Sauce."
  return True

label consume_whoo_sauce(item):
  $mc.remove_item(item)
  "Now to chase it, a shot of soy sauce."
  return True