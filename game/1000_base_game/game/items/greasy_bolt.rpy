init python:
  class Item_greasy_bolt(Item):

    icon="items greasy_bolt"

    def title(item):
      return "Greasy Bolt"

    def description(item):
      return "One day, I hope to do some screwing. Becoming a carpenter of love is one of my life goals."

    def actions(cls,actions):
      actions.append("?int_greasy_bolt")


label int_greasy_bolt(item):
  "Coated with something sticky. Grease, motor oil, or maybe sperm. Hard to tell."
  return True
