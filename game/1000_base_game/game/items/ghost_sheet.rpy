init python:
  class Item_ghost_sheet(Item):

    icon = "items ghost_sheet"

    def title(item):
      return "Ghost Sheet"

    def description(item):
      return "Simple yet effective."

    def actions(cls,actions):
      actions.append("?ghost_sheet_interact")


label ghost_sheet_interact(item):
  "With this thing on, I'll be the Ghost of Christmas Sins."
  return True
