init python:
  class Item_second_note_from_mrsl(Item):

    icon = "items note_from_mrsl"

    def title(item):
      return "Second Note from " + mrsl.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?second_note_from_mrsl_interact")

  add_recipe("second_note_from_mrsl","pen","quest_mrsl_bot_back_and_forth_second_note_from_mrsl_pen_combine")
  add_recipe("second_note_from_mrsl","nurse_pen","quest_mrsl_bot_back_and_forth_second_note_from_mrsl_pen_combine")


label second_note_from_mrsl_interact(item):
  "{i}\"You are very persistent, I will grant you.\"{/}"
  "{i}\"Though, I am sure you're capable of reaching the edge all on your own.\"{/}"
  "..."
  "Oh, well. At least she hasn't stopped replying."
  "So, neither will I."
  $quest.mrsl_bot["second_note_from_mrsl_interacted_with"] = True
  return True
