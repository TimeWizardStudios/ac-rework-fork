init python:
  class Item_saucer_eyed_doll(Item):

    icon = "items saucer_eyed_doll"

    def title(item):
      return "The Saucer-Eyed Doll"

    def description(item):
      return "It's like I hear a thousand voices screaming when I touch it. This thing is truly cursed."

    def actions(cls,actions):
      actions.append("?int_saucer_eyed_doll")


label int_saucer_eyed_doll(item):
  "I can't believe this thing is real..."
  "How the hell did [maxine] even know about it?"
  "..."
  "Unless she planted it..."
  "I need answers."
  return True
