init python:
  class Item_grapes(Item):

    icon = "items grapes"

    def title(item):
      return "Grapes"

    def description(item):
      # return "A perfect cluster of grapes. In grapeus veritas."
      return "A perfect cluster of grapes.\nIn grapeus veritas."

    def actions(cls,actions):
      actions.append("?grapes_interact")


label grapes_interact(item):
  "When in Rome... grapes and wine."
  return True
