init python:
  class Item_dirty_laundry(Item):
    icon="items dirty_laundry"
   
    def title(item):
      return "Dirty Laundry"
    
    def description(item):
      return "Sweat, perfume, and other raunchier bodily odors. Smelling things you pick up is only natural."
    
    def actions(cls,actions):
      actions.append("?int_dirty_laundry")
      
label int_dirty_laundry(item):
  "Are these [jo]'s or [flora]'s?"
  "Not knowing is arousing."
  return True