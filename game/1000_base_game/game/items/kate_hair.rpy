init python:
  class Item_kate_hair(Item):

    icon = "items kate_hair"

    def title(item):
      return kate.name + "'s Hair"

    def description(item):
      return "Smells like roses and disdain."

    def actions(cls,actions):
      actions.append("?kate_hair_interact")


label kate_hair_interact(item):
  "Goldilocks and the three Bs."
  "Bitchy, bossy, and... bitchy."
  return True
