init python:
  class Item_banana_milk(Item):
    
    icon="items bottle banana_milk"
   
    can_place_to_desk = True
    bottled = True
    consumable = True
    
    def title(item):
      return "Banana Milk"
   
    def description(item):
      return "Why is the name so weirdly sexual?"
   
    def actions(cls,actions):
      actions.append("?int_banana_milk")
      if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
        actions.append(["consume", "Consume","consume_banana_milk_maxine"])
      else:
        actions.append(["consume", "Consume", "consume_banana_milk"])
      

label int_banana_milk(item):  
  "Good for your throat, and for your skin, and for your face, and for your boobs..."
  "The doctors hate this drink."
  return True

label consume_banana_milk(item):
  $mc.remove_item(item) 
  $mc.add_item("empty_bottle")
  "Yuck! Why do I do this to myself? Probably going to throw up now..."
  return True

label consume_banana_milk_maxine(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my drink?"
  $mc.add_item("letter_from_maxine")
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  return True