init python:
  class Item_armor(Item):

    icon = "items armor"

    def title(item):
      return "Armor"

    def description(item):
      return "I nearly lost all of my hard work when [jo] found the cardboard and went to throw it away..."

    def actions(cls,actions):
      actions.append("?armor_interact")


label armor_interact(item):
  "Dress for how you feel on the inside, right?"
  "I guess trash is fitting, in more ways than one."
  "..."
  window hide
  $mc.remove_item("armor")
  $quest.maxine_dive.advance("dive")
  return
