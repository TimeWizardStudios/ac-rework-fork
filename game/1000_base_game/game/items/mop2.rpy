init python:
  class Item_mop2(Item):

    icon="items mop"

    def title(item):
      return "Mop"

    def description(item):
      return "In the dim lights of my bedroom, she's the shadow of a woman."

    def actions(cls,actions):
      actions.append("?mop2_interact")


label mop2_interact(item):
  "Nobody puts Baby in the supply closet."
  "Yes, she was my first kiss."
  return True
