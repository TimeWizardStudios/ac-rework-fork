init python:
  class Item_mug(Item):

    icon = "items mug"

    def title(item):
      return "Mug"

    def description(item):
      return "They don't call me \"Sticky\nFingers [mc]\" for nothing."

    def actions(cls,actions):
      actions.append("?mug_interact")


label mug_interact(item):
  "This is a step in the right direction, but it's going to take more than a stolen mug to win [jo] back."
  return True
