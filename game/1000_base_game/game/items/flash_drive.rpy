init python:
  class Item_flash_drive(Item):
    icon="items flash_drive"
    
    def title(item):
      return "Flash Drive"
    
    def description(item):
      return "Megabytes, gigabytes, terabytes — size matters."
    
    def actions(cls,actions):
      actions.append("?int_flash_drive")
      
label int_flash_drive(item):
  "They say that life's a flash in the pan..."
  "That's especially true if [jo] finds the content of this flash drive."
  return True
      
