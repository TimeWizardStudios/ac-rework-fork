init python:
  class Item_keycard(Item):

    icon = "items keycard"

    def title(item):
      return "Keycard"

    def description(item):
      return "I've never been a magician, but this card will do the trick."

    def actions(cls,actions):
      actions.append("?keycard_interact")


label keycard_interact(item):
  "Not the key to [lindsey]'s heart, but at least it's the key to her room...{space=-5}"
  "It's a start!"
  return True
