init python:
  class Item_bippity_boppity_bitch(Item):

    icon = "items book bippity_boppity_bitch"
    retain_capital = True

    def title(item):
      return "\"Bippity Boppity, Bitch: How to Spell\nfor Dummies\" by Glinda B. Goode"

    def description(item):
      return "Spell... spell. Heh."

    def actions(cls,actions):
      if quest.maya_spell == "book":
        actions.append("quest_maya_spell_book_bippity_boppity_bitch")
      actions.append("?bippity_boppity_bitch_interact")


label bippity_boppity_bitch_interact(item):
  "Maybe studying this will turn me into the likes of Merlin or Gandalf?"
  "Just imagine what I could do then..."
  return True
