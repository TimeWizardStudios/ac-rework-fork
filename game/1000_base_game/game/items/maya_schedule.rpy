init python:
  class Item_maya_schedule(Item):

    icon = "items maya_schedule"

    def title(item):
      return maya.name + "'s Schedule"

    def description(item):
      return "Every day, rigorously structured. Just like prison."

    def actions(cls,actions):
      actions.append("?maya_schedule_interact")


label maya_schedule_interact(item):
  "It feels dirty knowing [maya]'s whereabouts, but this time it's for the greater good."
  if quest.maya_witch == "schedule":
    "Let's see here..."
    "..."
    "Okay, she's in English class right now. I better hurry."
    $quest.maya_witch.advance("english_class")
  return True
