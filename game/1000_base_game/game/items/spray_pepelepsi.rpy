init python:
  class Item_spray_pepelepsi(Item):
    icon="items bottle spray_pepelepsi"
    bottled = True
    
    def title(item):
      return "Pepelepsi"
    
    def description(item):
      return "If motor oil and amphetamine put their dicks in a bottle of cola at the same time, Pepelepsi would be the outcome."
    
    def actions(cls,actions):
      actions.append("?int_pepelepsi")
      actions.append(["consume","Consume","consume_spray_pepelepsi"])

label consume_spray_pepelepsi(item):
  $mc.remove_item(item)
  $mc.add_item("spray_empty_bottle")
  "Mmm. The taste is fine, but it always makes me feel really hyper after a while."
  return True