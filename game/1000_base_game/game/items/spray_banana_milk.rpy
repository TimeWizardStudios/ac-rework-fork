init python:
  class Item_spray_banana_milk(Item):
    icon="items bottle spray_banana_milk"
    bottled = True
    
    def title(item):
      return "Banana Milk"
   
    def description(item):
      return "Why is the name so weirdly sexual?"
   
    def actions(cls,actions):
      actions.append("?int_banana_milk")
      actions.append(["consume", "Consume", "consume_spray_banana_milk"])

label consume_spray_banana_milk(item):
  $mc.remove_item(item) 
  $mc.add_item("spray_empty_bottle")
  "Yuck! Why do I do this to myself? Probably going to throw up now..."
  return True