init python:
  class Item_paint_jar(Item):

    icon = "items paint_jar"

    def title(item):
      return "Paint Jar"

    def description(item):
      return "It's a matte pink. Minimalist design. Very Scandinavian."

    def actions(cls,actions):
      actions.append("?paint_jar_interact")

  add_recipe("paint_jar","cactus","paint_jar_cactus_combine")


label paint_jar_interact(item):
  "There's still a few scraps of dried paint, but that should be extra nourishing for the cactus."
  return True

label paint_jar_cactus_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $process_event("items_combined",item1,item2,items_by_id["cactus_in_a_pot"])
  $mc.add_item("cactus_in_a_pot",silent=True)
  "Looking sharp."
  $quest.jo_day["cactus_in_a_pot_crafted"] = True
  return
