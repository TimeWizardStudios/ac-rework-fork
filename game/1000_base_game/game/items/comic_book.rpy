init python:
  class Item_comic_book(Item):

    icon = "items comic_book"

    def title(item):
      return "Comic Book"

    def description(item):
      # return "I can't wait for the movie to come out!"
      return "I can't wait for the movie\nto come out!"

    def actions(cls,actions):
      actions.append("?comic_book_interact")


label comic_book_interact(item):
  "They say a picture is worth a thousand words..."
  "That's the best way to read."
  return True
