init python:
  class Item_conductor_baton(Item):

    icon = "items conductor_baton"

    def title(item):
      return "Conductor's Baton"

    def description(item):
      return "Hold this stick to run the show.\n\nA reversed talking-stick."

    def actions(cls,actions):
      actions.append("?conductor_baton_interact")


label conductor_baton_interact(item):
  "It's not the girth or length of the baton, it's how aggressively you flick the wrist."
  return True
