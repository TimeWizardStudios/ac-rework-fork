init python:
  class Item_dungeons_and_damsels(Item):

    icon = "items dungeons_and_damsels"
    retain_capital = True

    def title(item):
      return "Dungeons and Damsels"

    def description(item):
      return "Warriors like this one promote unrealistic body expectations."

    def actions(cls,actions):
      actions.append("?dungeons_and_damsels_interact")


label dungeons_and_damsels_interact(item):
  "High flying, sword swashingbuckling, epic fantasy, here I come!"
  return True
