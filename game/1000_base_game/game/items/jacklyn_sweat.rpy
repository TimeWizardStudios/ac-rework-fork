init python:
  class Item_jacklyn_sweat(Item):

    icon = "items jacklyn_sweat"

    def title(item):
      return jacklyn.name + "'s Sweat"

    def description(item):
      return "To the window, to the wall...\nthe sweat right in my bottle."

    def actions(cls,actions):
      actions.append("?jacklyn_sweat_interact")


label jacklyn_sweat_interact(item):
  "Careful, I don't want to drink that by accident..."
  "..."
  "..."
  "..."
  "...or do I?"
  return True
