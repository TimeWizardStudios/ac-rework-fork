init python:
  class Item_plant(Item):

    icon = "items plant"

    def title(item):
      return "Plant"

    def description(item):
      return "A regular, completely normal butterfly attractor."

    def actions(cls,actions):
      actions.append("?plant_interact")


label plant_interact(item):
  "The flowers are tiny, but the scent is quite strong. Just like my dic...tionary."
  "Because of the, uh, old paper and stuff. Quite pungent."
  return True
