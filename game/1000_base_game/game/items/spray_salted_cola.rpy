init python:
  class Item_spray_salted_cola(Item):
    icon="items bottle spray_salted_cola"
    bottled = True

    def title(item):
      return "Salted Cola"
  
    def description(item):
      return "There's like globs of white stuff floating around in the cola. Probably nothing to worry about."
  
    def actions(cls,actions):
      actions.append("?int_salted_cola")
      actions.append(["consume","Consume","consume_spray_salted_cola"])

label consume_spray_salted_cola(item):
  $mc.remove_item(item) 
  $mc.add_item("spray_empty_bottle")
  "That's just... fucking hell, that's bad!"
  return True