init python:
  class Item_spoon(Item):
    icon="items spoon"
    
    def title(item):
      return "Spoon"
    
    def description(item):
      return "The most horrible murder weapon is a spoon. It just takes so painfully long to kill someone with it. Especially if you use the flat side."
    
    def actions(cls,actions):
      actions.append("?int_spoon")
   
label int_spoon(item):
  "As much as I want to believe that it's all in my head and that there is no spoon, I can't."
  return True
