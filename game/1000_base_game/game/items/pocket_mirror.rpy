init python:
  class Item_pocket_mirror(Item):

    icon = "items pocket_mirror"

    def title(item):
      return "Pocket Mirror"

    def description(item):
      return "[flora] doesn't need this. She's cute enough without makeup."

    def actions(cls,actions):
      actions.append("?int_pocket_mirror")


label int_pocket_mirror(item):
  "For when you need a quick cosmetic pick-me-up!"
  "Or in my case... smackdown."
  return True
