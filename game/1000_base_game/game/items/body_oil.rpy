init python:
  class Item_body_oil(Item):

    icon = "items body_oil"

    def title(item):
      return "Body Oil"

    def description(item):
      return "Only a little left.\n[jacklyn]'s been busy."

    def actions(cls,actions):
      actions.append("?body_oil_interact")


label body_oil_interact(item):
  "Massages are professional foreplay. I'm looking to go pro."
  return True
