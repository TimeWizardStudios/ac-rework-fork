init python:
  class Item_empty_can(Item):
    icon="items empty_can"
    
    def title(item):
      return "Empty Can"
    
    def description(item):
      return "Sharp edges. Good for cutting soft things, like earth. Attach it to stick and start digging."
    
    def actions(cls,actions):
      actions.append("?int_empty_can")

label int_empty_can(item):
  "Catch a can canner as he does the cancan, and you've caught a can-canning can-canning can canner!"
  return True
