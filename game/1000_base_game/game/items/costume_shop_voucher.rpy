init python:
  class Item_costume_shop_voucher(Item):

    icon = "items costume_shop_voucher"

    def title(item):
      return "Costume Shop Voucher"

    def description(item):
      return "A voucher for a costume shop\non the web."

    def actions(cls,actions):
      actions.append("?costume_shop_voucher_interact")


label costume_shop_voucher_interact(item):
  "I better check out the site before this voucher expires..."
  return True
