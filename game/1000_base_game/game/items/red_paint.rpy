init python:
  class Item_red_paint(Item):

    icon = "items red_paint"

    def title(item):
      return "Red Paint"

    def description(item):
      if quest.mrsl_bot == "diversion":
        return "It's about to be Carrie at the prom."
      else:
        return "One bucket of revenge."

    def actions(cls,actions):
      if quest.mrsl_bot == "diversion":
        actions.append("?quest_mrsl_bot_diversion_red_paint")
      else:
        actions.append("?red_paint_interact")


label red_paint_interact(item):
  "I see a black door and I want to paint it red."
  return True
