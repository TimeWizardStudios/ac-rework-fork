init python:
  class Item_charger_cable(Item):
    icon="items charger_cable"

    def title(item):
      return "Charger Cable"

    def description(item):
      return "A tier 3 type cable. Solid gold. Very low resistance."

    def actions(cls,actions):
      actions.append("?charger_cable_interact")

  add_recipe("charger_cable","glass_shard","charger_cable_combine_glass_shard")
  add_recipe("charger_cable","beaver_pelt","charger_cable_combine_beaver")
  add_recipe("charger_cable","beaver_carcass","charger_cable_combine_beaver")


label charger_cable_interact(item):
  "This thing has a plug that will fit a normal jack, but a normal jack would never have enough energy to power the locator device."
  return True

label charger_cable_combine_glass_shard(item,item2):
  $mc.remove_item("charger_cable")
  "Just a quick incision..."
  "Cut off the head..."
  "Slice along the spine..."
  "Peel off the skin..."
  "And done!"
  $mc.add_item("sliced_cable",silent=True)
  $game.notify_modal(None,"Combine","{image= items sliced_cable}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Sliced Cable{/}.",10.0)
  $mc["crafted_sliced_cable"] = True
  return

label charger_cable_combine_beaver(item,item2):
  $mc.remove_item("charger_cable")
  "Sharper than any knife."
  "These deadly teeth strike fear in even the sturdiest of cables."
  $mc.add_item("sliced_cable",silent=True)
  $game.notify_modal(None,"Combine","{image= items sliced_cable}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Sliced Cable{/}.",10.0)
  $mc["crafted_sliced_cable"] = True
  return
