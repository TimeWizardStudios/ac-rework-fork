init python:
  class Item_wet_tissue(Item):

    icon = "items wet_tissue"
    consumable = True

    def title(item):
      return "Wet Tissue"

    def description(item):
      return "This tissue is wetter than [isabelle] in a Scottish bookshop."

    def actions(cls,actions):
      actions.append("?wet_tissue_interact")

  add_recipe("tissues","water_bottle","wet_tissue_combine")
  add_recipe("tissues","spray_water","wet_tissue_combine")


label wet_tissue_interact(item):
  "Next time someone's crying and asks for a tissue, I'll give them this."
  "You're not the only one in pain."
  return True

label wet_tissue_combine(item, item2):
  if quest.mrsl_table == "laundry" and mc.owned_item_count("tissues") == 1 and not quest.mrsl_table["piss_puddles_removed"]:
    "As much as I like being the cause of wetness, I still need a dry one for the piss puddles."
  else:
    $mc.remove_item("tissues")
    $mc.add_item("wet_tissue",silent=True)
    $game.notify_modal(None,"Combine","{image= items wet_tissue}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Wet Tissue{/}.",10.0)
    "That'll do it."
    "The application of a moderate amount of water."
  return
