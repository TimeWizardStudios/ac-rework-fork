init python:
  class Item_locked_box_cave(Item):

    icon = "items locked_box"

    def title(item):
      return "Locked Box"

    def description(item):
      return "A box wrapped in chains, and that's not a euphemism."

    def actions(cls,actions):
      actions.append("?locked_box_cave_interact")


label locked_box_cave_interact(item):
    "Sturdy and unbreakable despite its apparent age."
    return True
