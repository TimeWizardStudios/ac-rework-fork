init python:
  class Item_picnic_blanket(Item):

    icon = "items picnic_blanket"

    def title(item):
      return "Picnic Blanket"

    def description(item):
      return "This pairs great with a bottle\nof Rosé and a sexy woman\nto drink with."

    def actions(cls,actions):
      actions.append("?picnic_blanket_interact")


label picnic_blanket_interact(item):
  "Soft and comfortable. A barrier between my ass cheeks and the cold, hard ground."
  return True
