init python:
  class Item_beaver_pelt(Item):

    icon = "items beaver_pelt2"
    tiertwo = True

    def title(item):
      return "Beaver Pelt"

    def description(item):
      return "Smells of death and destruction, and of malintent and an unquenchable hatred for mankind."

    def actions(cls,actions):
      if berb["scalped"]:
        actions.append("?beaver_pelt_interact")
      else:
        actions.append("?beaver_pelt_interact2")


label beaver_pelt_interact(item):
  "This thing still radiates an aura of pure evil. Perhaps burying it would've been the right call."
  return True

label beaver_pelt_interact2(item):
  "A luxury doormat, previously owned by J. J. Timberlake."
  return True
