init python:
  class Item_meds(Item):
    icon="items meds"
    
    def title(item):
      return "Unassorted Meds"
    
    def description(item):
      return "A cocktail of fun at any party."
    
    def actions(cls,actions):
      actions.append("?int_meds")
      #actions.append(["consume", "Consume", "?meds_consume"])

label int_meds(item):
  "This is how you get girls with just one spiked drink."
  return True
