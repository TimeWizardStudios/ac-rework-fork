init python:
  class Item_rock(Item):
    icon="items rock"
    
    def title(item):
      return "Rock"
    
    def description(item):
      return "A rock without any redeeming qualities."
    
    def actions(cls,actions):
      actions.append("?int_rock")
      
label int_rock(item):
  "When your mother doesn't trust you with a real pet..."
  "..."
  "I still miss Pebbles McShoepain. Rest in peace, little buddy."
  return True
      
      
