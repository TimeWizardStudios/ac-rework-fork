init python:
  class Item_lindsey_bag(Item):

    icon="items lindsey_bag"

    def title(item):
      return lindsey.name + "'s Bag"

    def description(item):
      return "Smells vaguely of sweat, shampoo, and princess."

    def actions(cls,actions):
      actions.append("?lindsey_bag_interact")


label lindsey_bag_interact(item):
  "The question on everyone's lips..."
  "The one that you can't get around..."
  "The reason you're scratching your head..."
  "How do you get the cat out of the bag?"
  return True
