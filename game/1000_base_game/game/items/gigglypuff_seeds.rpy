init python:
  class Item_gigglypuff_seeds(Item):
    icon="items gigglypuff_seeds"
    
    def title(item):
      return "Gigglypuff Seeds"
    
    def description(item):
      return "Cute and puffy little poisonous seeds that smell like bubblegum." 
    
    def actions(cls,actions):
      actions.append("?int_gigglypuff_seeds")
      #actions.append(["consume", "Consume", "?gigglypuff_seeds_consume"])

label int_gigglypuff_seeds(item):
  "Eating these may cause severe vomiting, a bloody nose, randomly breaking into song, and fits of unending giggles."
  return True
