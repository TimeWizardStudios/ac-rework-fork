init python:
  class Item_soup_can(Item):
    icon="items soupcan"
    def title(item):
      return "Can of Soup"
    def description(item):
      return "Water and soggy veggies in a can. My favorite dessert."
    def actions(cls,actions):
      actions.append("?int_soup_can")
      actions.append(["consume", "Consume", "?soup_can_consume"])

label int_soup_can(item):
  "One spoon each and we'll save the rest for dinner. We can't be wasteful with our last can of soup."
  return True

label soup_can_consume(item):
  "Ah, that'll clean out the pipes!"
  $mc.remove_item(item)
  $mc.add_item("empty_can")
  return True