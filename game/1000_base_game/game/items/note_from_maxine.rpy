init python:
  class Item_note_from_maxine(Item):

    icon = "items note_from_maxine"

    def title(item):
      if quest.mrsl_bot["note_from_maxine_interacted_with"]:
        return "Note from " + maxine.name
      else:
        return "Third Note from " + mrsl.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?note_from_maxine_interact")


label note_from_maxine_interact(item):
  "{i}\"The craftsmanship of this ghost trap leaves things to be desired.\"{/}"
  "{i}\"I'm prepared to pay a handsome fee in pitcoin to reduce its flaws.\"{/}"
  "{i}\"That is, coin from the underground pits.\"{/}"
  "{i}\"I expect one day its monetary value will return.\"{/}"
  "{i}\"If you accept, send word by carrier pigeon. It will know where to find me.\"{/}"
  "{i}\"P.S. Don't touch the specimen in Area 12. It's still not a mailbox.\"{/}"
  "..."
  "Damn it, [maxine]! Stay out of my box!"
  "And nothing from [mrsl], either..."
  "Back to waiting, I guess."
  window hide
  $quest.mrsl_bot["note_from_maxine_interacted_with"] = True
  $mc.remove_item("note_from_maxine")
  return
