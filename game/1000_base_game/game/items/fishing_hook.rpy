init python:
  class Item_fishing_hook(Item):

    icon = "items fishing_hook"

    def title(item):
      return "Fishing Hook"

    def description(item):
      return "Ho-o-o-o-o-ok! Hook me up!"

    def actions(cls,actions):
      actions.append("?fishing_hook_interact")


label fishing_hook_interact(item):
  "Sharp, but not that sharp."
  "Hooked, but not that hooked."
  "Serves its purpose, but not much more."
  return True
