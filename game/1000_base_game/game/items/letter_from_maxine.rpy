init python:
  class Item_letter_from_maxine(Item):

    icon="items letter_from_maxine"

    def title(item):
      return "Letter from " + maxine.name

    def description(item):
      return "A seemingly normal, albeit wet, paper slip."

    def actions(cls,actions):
      actions.append("?int_letter_from_maxine")


label int_letter_from_maxine(item):
  "{i}\"Dear person with poor understanding of basic spy protocol,\"{/}"
  "{i}\"Do NOT use the specimen in Area 12 as a mailbox.\"{/}"
  "{i}\"That is highly unethical within at least five systems of thought.\"{/}"
  "{i}\"Nonetheless, the answer you seek has been provided in your personal food container.\"{/}"
  "{i}\"Do NOT make contact again through these means.\"{/}"
  "{i}\"Yours truly, M.\"{/}"
  "{i}\"P.S. this letter will burn upon reading.\"{/}"
  "{i}\"P.P.S. and if not, that means the self-combustion misfired and that you should burn it.\"{/}"
  "{i}\"Remember, I'm watching.\"{/}"
  $quest.isabelle_stolen.advance("maxineclue")
  return True
