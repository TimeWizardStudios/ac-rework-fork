init python:
  class Item_strawberry_juice(Item):

    icon="items bottle strawberry_juice"

    can_place_to_desk = True
    consumable = True
    bottled = True

    def title(item):
      return "Strawberry Juice"

    def description(item):
      return "The red gold. The nectar of heaven. One of the few truly good things left in this world."

    def actions(cls,actions):
      actions.append("?int_strawberry_juice")
      if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
        actions.append(["consume", "Consume","consume_strawberry_juice_maxine"])
      elif quest.maxine_hook == "glade":
        actions.append(["consume", "Consume","quest_maxine_hook_glade"])
      elif quest.isabelle_tour.finished and not quest.nurse_photogenic.started:
        actions.append(["consume","Consume","quest_nurse_photogenic_strawberry_juice"])
      else:
        actions.append(["consume","Consume","consume_strawberry_juice"])


label int_strawberry_juice(item):
  "Rolls over your tongue like ruby silk. The only thing better is licking a pussy, or so I've heard."
  return True

label consume_strawberry_juice(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  mc "Ah, the lifeblood courses through my veins once more!"
  #$ +Smarts (temporary: one day)
  #$ -Charm (temporary: one day) #tbd
  return True

label consume_strawberry_juice_maxine(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my drink?"
  $mc.add_item("letter_from_maxine")
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  return True
