init python:
  class Item_lab_coat(Item):

    icon = "items lab_coat"

    def title(item):
      return "Lab Coat"

    def description(item):
      return "Coat of white, night by night. Hands of blue, two by two."

    def actions(cls,actions):
      actions.append("?lab_coat_interact")


label lab_coat_interact(item):
  "No need to lab coat it. I can handle the truth."
  return True
