init python:
  class Item_cat_tail(Item):

    icon = "items cat_tail"

    def title(item):
      return "Cat Tail"

    def description(item):
      return "It's so cute and innocent-looking!\nI wonder who it belongs to..."

    def actions(cls,actions):
      actions.append("?cat_tail_interact")


label cat_tail_interact(item):
  "A fake tail of some sort with a metal bulb."
  "It probably attaches by use of magnets."
  return True
