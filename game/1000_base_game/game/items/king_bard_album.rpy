init python:
  class Item_king_bard_album(Item):

    icon = "items king_bard_album"

    def title(item):
      return "King's Bard Album"

    def description(item):
      return "Fantasy music, the one place where nerds and princesses intersect."

    def actions(cls,actions):
      actions.append("?king_bard_album_interact")

label king_bard_album_interact(item):
  "{i}\"Riffs and rafts in a thunderous storm. Lightning enlightens our minds and swords.\"{/}"
  "{i}\"Warriors spring to battle. Fair maidens sing and prattle.\"{/}"
  "{i}\"Dragon fights a knightly lord, eats a dick upon his hoard!\"{/}"
  "Yeah, the lyrics are as cheesy as ever."
  return True
