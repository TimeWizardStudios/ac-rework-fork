init python:
  class Item_tubes_of_paint(Item):

    icon = "items tubes_of_paint"

    def title(item):
      return "Tubes of Paint"

    def description(item):
      return "Fifty shades of color.\nNothing dirty about it."

    def actions(cls,actions):
      actions.append("?tubes_of_paint_interact")


label tubes_of_paint_interact(item):
  "I'm more of a bucket guy myself, but I guess these tubes will have to do..."
  return True
