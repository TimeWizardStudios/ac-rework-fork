init python:
  class Item_haggis_lunchbox(Item):

    icon="items haggis_lunchbox"

    def title(item):
      return "Haggis Lunch Box"

    def description(item):
      return "A mix of sheep's pluck, minced onion, oatmeal, suet, and other tasty things, encased in the sheep's stomach while cooking."

    def actions(cls,actions):
      actions.append("?int_haggis_lunchbox")


label int_haggis_lunchbox(item):
  "The smell is to die for. No literally, you might die from the stench alone."
  return True
