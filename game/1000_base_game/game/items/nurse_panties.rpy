init python:
  class Item_nurse_panties(Item):

    icon="items nurse_panties"
    tiertwo = True

    def title(item):
      return nurse.name + "'s Panties"

    def description(item):
      return "The fantasy is that she only has this one pair, and is now forced to go commando."

    def actions(cls,actions):
      actions.append("?int_nurse_panties")


label int_nurse_panties(item):
  if quest.nurse_aid["name_reveal"]:
    "I could smell these all day. They smell like [amelia], but much more intensely."
  else:
    "I could smell these all day. They smell like the [nurse], but much more intensely."
  return True
