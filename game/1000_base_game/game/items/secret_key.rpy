init python:
  class Item_secret_key(Item):
    icon="items secret_key"
    
    def title(item):
      return "Secret Key"
    
    def description(item):
      return "A key found in the Secret Locker, which in reality is just another locker and not really secret at all."
    
    def actions(cls,actions):
      actions.append("?int_secret_key")
   
label int_secret_key(item):
  "Whoever left this here... has to have been a psychopath. There's literally no other option."
  return True
