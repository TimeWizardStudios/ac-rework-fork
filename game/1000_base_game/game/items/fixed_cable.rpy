init python:
  class Item_fixed_cable(Item):

    icon="items fixed_cable" 

    def title(item):
      return "Fixed Cable"

    def description(item):
      return "This cable is utterly complete. Never before has such craftsmanship been seen in the cable business."

    def actions(cls,actions):
      actions.append("?fixed_cable_interact")

  add_recipe("broken_cable","electrical_tape","fixed_cable_combine")


label fixed_cable_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("fixed_cable",silent=True)
  $game.notify_modal(None,"Combine","{image= items fixed_cable}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Fixed Cable{/}.",10.0)
  "It's a bit of a botched job, but it should do the trick now."
  return

label fixed_cable_interact(item):
  "To whoever said that turning your life around is impossible: how do you like me now?"
  "And err... I guess, how do you like my cable?"
  return True
