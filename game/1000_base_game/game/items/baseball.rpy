init python:
  class Item_baseball(Item):
    icon="items baseball"
    
    def title(item):
      return "Baseball"
    
    def description(item):
      return "Fits right in your hand. Like one big testicle. Or like ten regularly sized testicles bunched up into one big ball."
   
    def actions(cls,actions):
      actions.append("?int_baseball")
      
label int_baseball(item):
    "Oh, it has a signature..."
    "$5 to the person that can hit [mc] in the face."
    return True
  

