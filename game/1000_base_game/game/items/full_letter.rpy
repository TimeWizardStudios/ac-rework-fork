init python:
  class Item_full_letter(Item):
    icon="items full_letter"
    
    def title(item):
      return "A Letter and a Half"
    
    def description(item):
      return "A letter of insatiable hunger."
    
    def actions(cls,actions):
      actions.append("?int_full_letter")

  add_recipe("half_letter_2","half_letter_1","full_letter")

      
label int_full_letter(item):
  "{i}\"Use me if you get hungry.\"{/}"
  "That's an odd request, but I guess it's different strokes for different folks.  "      
  return True
      
      
