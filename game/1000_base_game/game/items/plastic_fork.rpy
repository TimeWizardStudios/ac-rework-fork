init python:
  class Item_plastic_fork(Item):
    icon="items plastic_fork" 

    def title(item):
      return "Plastic Fork"

    def description(item):
      return "This fork is just begging to end up in the ocean. There's something highly pollutable about it."

    def actions(cls,actions):
      actions.append("?int_plastic_fork")

label int_plastic_fork(item):
  "Fork this forking fork. Yeah, I said it."
  return True
