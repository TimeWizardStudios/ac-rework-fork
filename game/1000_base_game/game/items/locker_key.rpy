init python:
  class Item_locker_key(Item):
    icon="items locker_key"
    
    def title(item):
      return "Locker Key"
    
    def description(item):
      return "The key to the hollow heart of a steel locker. Mine."
    
    def actions(cls,actions):
      actions.append("?int_locker_key")
      
label int_locker_key(item):
  "You sign your soul away for a key."
  "From here on out, your life is meant to fit into a tiny box."
  "Sit in a classroom box. Work in a cubicle at the box office. Eat from a happy meal box."
  "Live in a concrete box, die in a casket box."
  "It's all connected."
  return True
      
