init python:
  class Item_poisoned_apple(Item):
    
    icon="items poisonedapple"
    consumable = True
    
    def title(item):
      return "Poisoned Apple"
    
    def description(item):
      return "Laced with chemicals and nerve toxins, this apple has enough poison to kill a hundred fully grown Snow Whites."
    
    def actions(cls,actions):
      actions.append("?poisoned_apple_interact")

  add_recipe("tide_pods","apple","poisoned_apple_recipe")    

label poisoned_apple_recipe(item, item2):
  $mc.remove_item("tide_pods")
  $mc.remove_item("apple")
  "Just like the evil queen in Snow White. An apple laced with poison."
  $mc.add_item("poisoned_apple")
  return True
     
label poisoned_apple_interact(item):
  "On the outside, it looks like a normal apple. On the inside, death incarnate. "
  return True
