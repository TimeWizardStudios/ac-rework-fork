init python:
  class Item_venus_vagenis(Item):
    icon="items venus_vagenis"
    retain_capital = True
    
    def title(item):
      return "Venus Vagenis"
    
    def description(item):
      return "Rarest of the rare — this herb is said to grow only where female vaginal fluids seeped into the soil."
    
    def actions(cls,actions):
      actions.append("?int_venus_vagenis")
      
label int_venus_vagenis(item):
  "The musky scent of passionate love-making accompanies this herb."
  return True
      
