init python:
  class Item_machete(Item):
    
    icon="items machete"
    
    def title(item):
      return "Machete"
    
    def description(item):
      return "Perfect for picking your teeth and hacking apart criminals."
    
    def actions(cls,actions):
      actions.append("?machete_interact")
     
  add_recipe("machete","colored_paper","combine_colored_paper_machete")   
     
label machete_interact(item):
  "So shiny and sharp, the reflection is—"
  "Oh, god Jason Voorheese is real!"
  "Wait, that's just me."
  return True
