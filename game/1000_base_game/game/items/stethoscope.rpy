init python:
  class Item_stethoscope(Item):
    icon="items stethoscope"
    
    def title(item):
      return "Stethoscope"
    
    def description(item):
      return "[flora] always says I don't listen to her. Maybe this will help."
    
    def actions(cls,actions):
      actions.append("?int_stethoscope")
      #actions.append(["consume", "Consume", "?stethoscope_consume"])

label int_stethoscope(item):
  "Hmm, interesting..."
  "The self-examination points toward a raging heart-on."
  return True
