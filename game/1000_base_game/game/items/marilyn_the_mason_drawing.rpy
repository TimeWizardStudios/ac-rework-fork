init python:
  class Item_marilyn_the_mason_drawing(Item):
    icon="items marilyn_the_mason_drawing"
    retain_capital = True
    
    def title(item):
      return "Marilyn the Mason Drawing"
    
    def description(item):
      return "A rare drawing of Marilyn the Mason — the satanic brick-laying icon of the 1400s. Throughout history, very few construction workers have attracted such widespread hatred."
    
    def actions(cls,actions):
      actions.append("?int_marilyn_the_mason_drawing")
      
label int_marilyn_the_mason_drawing(item):
  "Marilyn the Mason is best known for his controversial brick structures, which were accused of being satanic in nature and a negative influence on young people."
  "His most famed work, the Anti-chalk Superstar, has modern architects scratching their heads."
  return True
      
