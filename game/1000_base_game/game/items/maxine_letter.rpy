init python:
  class Item_maxine_letter(Item):

    icon="items polaroidback_text"

    def title(item):
      return "Letter to " + maxine.name

    def description(item):
      return "A finely composed letter for the queen of weird."

    def actions(cls,actions):
      actions.append("?maxine_letter_interact")

  add_recipe("pen","piece_of_paper","maxine_letter_combine")


label maxine_letter_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  "[maxine] isn't exactly the easiest person to deal with."
  "Writing to her requires a special touch. A certain skill. Hmm..."
  menu(side="middle"):
    extend ""
    "\"Dear protector of the Sacred Plant...\"":
      $maxine.lust-=1
      $maxineletterbegin = "Dear protector of the Sacred Plant,"
    "\"Dear [spinach] enthusiast...\"":
      $maxine.love+=1
      $maxineletterbegin = "Dear " + spinach.name + " enthusiast,"
    "\"My fellow seeker of truths...\"":
      $maxine.lust+=1
      $maxineletterbegin = "My fellow seeker of truths,"
    "\"My fellow keeper of secrets...\"":
      $maxine.love-=1
      $maxineletterbegin = "My fellow keeper of secrets,"
  "{i}\"[maxineletterbegin]\"{/}"
  "{i}\"In this time of uncertainty and turmoil, I come to you with a request.\"{/}"
  "{i}\"A crime has been committed, and with it, all our safety hangs in the balance.\"{/}"
  "{i}\"I beseech your guidance... and sort of hope that you've seen something, or know where to start or whatever?\"{/}"
  "{i}\"Sorry about the ending, I ran out of cool words.\"{/}"
  menu(side="middle"):
    extend ""
    "\"Yours truly, [mc].\"":
      $maxine.lust-=1
      $maxineletterend = "Yours truly, " + mc.name + "."
    "\"An anonymous vigilante.\"":
      $maxine.lust+=1
      $maxineletterend =  "An anonymous vigilante."
    "\"Kind regards, your future friend.\"":
      $mc.charisma+=1
      $maxineletterend = "Kind regards, your future friend."
    "\"Signed only, V.\"":
      $mc.intellect+=1
      $maxineletterend = "Signed only, V."
  "{i}\"[maxineletterend]\"{/}"
  $mc.add_item("maxine_letter",silent=True)
  $game.notify_modal(None,"Combine","{image= items polaroidback_text}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Letter to [maxine]{/}.",10.0)
  "That should do it."
  "Now, this letter just needs to be posted in [maxine]'s mailbox — the potted plant."
  return

label maxine_letter_interact(item):
  "{i}\"[maxineletterbegin]\"{/}"
  "{i}\"In this time of uncertainty and turmoil, I come to you with a request.\"{/}"
  "{i}\"A crime has been committed, and with it, all our safety hangs in the balance.\"{/}"
  "{i}\"I beseech your guidance... and sort of hope that you've seen something, or know where to start or whatever?\"{/}"
  "{i}\"Sorry about the ending, I ran out of cool words.\"{/}"
  "{i}\"[maxineletterend]\"{/}"
  return True
