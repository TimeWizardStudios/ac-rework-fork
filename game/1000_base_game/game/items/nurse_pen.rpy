init python:
  class Item_nurse_pen(Item):

    icon="items nurse_pen"

    def title(item):
      return nurse.name + "'s Pen"

    def description(item):
      return "A sharp and deadly weapon, especially when used to write."

    def actions(cls,actions):
      actions.append("?int_nurse_pen")


label int_nurse_pen(item):
  "Stabbing someone with words might hurt as much as using the sharp tip."
  return True
