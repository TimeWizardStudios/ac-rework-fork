init python:
  class Item_garlic(Item):
    
    icon="items garlic"
    consumable = True
    can_place_to_desk = True


    def title(item):
      return "Garlic"
    
    def description(item):
      return "Garlic. Perfect against vampires and untasty meals."
    
    def actions(cls,actions):
      actions.append("?garlic_interact")
      actions.append(["consume", "Consume", "?consume_garlic"])
     
label garlic_interact(item):
  "Just a clove to chew on for the sake of good breath."
  return True

label consume_garlic(item):
  "Not like anybody talks to me anyways, might as well indulge."
  $mc.remove_item(item)
  "My breath already smells. Eating a few cloves of raw garlic would probably improve it."
  return True