init python:
  class Item_cowboy_hat(Item):

    icon = "items cowboy_hat"

    def title(item):
      return "Cowboy Hat"

    def description(item):
      return "Howdy, pardner!"

    def actions(cls,actions):
      actions.append("?cowboy_hat_interact")


label cowboy_hat_interact(item):
  if mc.name.lower().startswith(("a","e","i","o","u")):
    "You know what they say — save a horse, ride an [mc]."
  else:
    "You know what they say — save a horse, ride a [mc]."
  return True
