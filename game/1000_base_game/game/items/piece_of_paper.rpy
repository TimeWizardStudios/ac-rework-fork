init python:
  class Item_piece_of_paper(Item):
    icon="items polaroidback"
    
    def title(item):
      return "Piece of Paper"
    
    def description(item):
      return "A sentimental piece of personal history, from a time before [jo] gave up hope on me."
    
    def actions(cls,actions):
      actions.append("?int_piece_of_paper")
      
label int_piece_of_paper(item):
  "One side is perfect to write on. The other is perfect to cry over. A unique balance of emotions."
  return True 
      
      