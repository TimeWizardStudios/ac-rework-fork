init python:
  class Item_shrimp_and_lobster(Item):

    icon = "items shrimp_and_lobster"

    def title(item):
      return "Shrimp and Lobster"

    def description(item):
      # return "The Rolls Royce of the dinner table."
      return "The Rolls Royce of\nthe dinner table."

    def actions(cls,actions):
      actions.append("?shrimp_and_lobster_interact")


label shrimp_and_lobster_interact(item):
  "The vermin of the sea. Truly a delicacy."
  return True
