init python:
  class Item_paint_buckets(Item):
    
    icon="items paint_buckets"
    
    def title(item):
      return "Paint Buckets"
    
    def description(item):
      return "The school's pride. The colors of autumn. Yeah, I'm not convinced."
    
    def actions(cls,actions):
      actions.append("?paint_buckets_interact")
  
label paint_buckets_interact(item):
  "The taste isn't great. Not sure why I stuck my entire hand in."
  return True
