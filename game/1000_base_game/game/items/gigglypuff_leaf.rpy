init python:
  class Item_gigglypuff_leaves(Item):
    icon="items gigglypuffleaves"
    
    def title(item):
      return "Gigglypuff Leaves"
    
    def description(item):
      return "Do not smoke. Bad for your health, and bad for the leaves' health."
    
    def actions(cls,actions):
      actions.append("?int_gigglypuff_leaves")

label int_gigglypuff_leaves(item):
  "Boil 'em, mash 'em, stick 'em in your bum."
  "There are many ways to enjoy these little leaves."
  return True
