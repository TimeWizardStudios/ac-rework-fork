init python:
  class Item_package_gigglypuff_seeds(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_gigglypuff_seeds_interact")
      actions.append(["consume","Consume","?package_gigglypuff_seeds_consume"])


label package_gigglypuff_seeds_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_gigglypuff_seeds_consume(item):
  $mc.remove_item(item)
  $mc.add_item("gigglypuff_seeds")
  $quest.jo_potted.advance("plow")
  return True




init python:
  class Item_package_magnet(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_magnet_interact")
      actions.append(["consume","Consume","?package_magnet_consume"])


label package_magnet_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_magnet_consume(item):
  $mc.remove_item(item)
  $mc.add_item("magnet")
  $home_kitchen["magnet_package_consumed"] = True
  return True




init python:
  class Item_package_ball_of_yarn(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_ball_of_yarn_interact")
      actions.append(["consume","Consume","?package_ball_of_yarn_consume"])


label package_ball_of_yarn_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_ball_of_yarn_consume(item):
  $mc.remove_item("package_ball_of_yarn")
  $mc.add_item("ball_of_yarn",5)
  return True




init python:
  class Item_package_stick(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_stick_interact")
      actions.append(["consume","Consume","?package_stick_consume"])


label package_stick_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_stick_consume(item):
  $mc.remove_item("package_stick")
  $mc.add_item("stick")
  return True




init python:
  class Item_package_mrsl_brooch(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_mrsl_brooch_interact")
      actions.append(["consume","Consume","?package_mrsl_brooch_consume"])


label package_mrsl_brooch_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_mrsl_brooch_consume(item):
  $mc.remove_item("package_mrsl_brooch")
  $mc.add_item("mrsl_brooch")
  "Hold on a minute, isn't that [mrsl]'s brooch?"
  return True




init python:
  class Item_package_nymphoria_heartus(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_nymphoria_heartus_interact")
      actions.append(["consume","Consume","?package_nymphoria_heartus_consume"])


label package_nymphoria_heartus_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_nymphoria_heartus_consume(item):
  $mc.remove_item("package_nymphoria_heartus")
  $mc.add_item("nymphoria_heartus")
  return True




init python:
  class Item_package_venus_vagenis(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_venus_vagenis_interact")
      actions.append(["consume","Consume","?package_venus_vagenis_consume"])


label package_venus_vagenis_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_venus_vagenis_consume(item):
  $mc.remove_item("package_venus_vagenis")
  $mc.add_item("venus_vagenis")
  return True




init python:
  class Item_package_bag_of_weed(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_bag_of_weed_interact")
      actions.append(["consume","Consume","?package_bag_of_weed_consume"])


label package_bag_of_weed_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_bag_of_weed_consume(item):
  $mc.remove_item("package_bag_of_weed")
  $mc.add_item("bag_of_weed")
  return True




init python:
  class Item_package_beaver_pelt(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_beaver_pelt_interact")
      actions.append(["consume","Consume","?package_beaver_pelt_consume"])


label package_beaver_pelt_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_beaver_pelt_consume(item):
  $mc.remove_item("package_beaver_pelt")
  $mc.add_item("beaver_pelt")
  return True




init python:
  class Item_package_baseball_bat(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_baseball_bat_interact")
      actions.append(["consume","Consume","?package_baseball_bat_consume"])


label package_baseball_bat_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_baseball_bat_consume(item):
  $mc.remove_item("package_baseball_bat")
  $mc.add_item("baseball_bat")
  return True




init python:
  class Item_package_sleeping_pills(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_sleeping_pills_interact")
      actions.append(["consume","Consume","?package_sleeping_pills_consume"])


label package_sleeping_pills_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_sleeping_pills_consume(item):
  $mc.remove_item("package_sleeping_pills")
  $mc.add_item("sleeping_pills")
  return True




init python:
  class Item_package_cutie_harlot(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_cutie_harlot_interact")
      actions.append(["consume","Consume","?package_cutie_harlot_consume"])


label package_cutie_harlot_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_cutie_harlot_consume(item):
  $mc.remove_item("package_cutie_harlot")
  $mc.add_item("cutie_harlot")
  return True




init python:
  class Item_package_pig_mask(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_pig_mask_interact")
      actions.append(["consume","Consume","?package_pig_mask_consume"])


label package_pig_mask_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_pig_mask_consume(item):
  $mc.remove_item("package_pig_mask")
  $mc.add_item("pig_mask")
  if False: ## Placeholder ## The player has started Last Breath ##
#   "This thing is scary for sure, but [lindsey] will know it's me if the lights are on..."
#   "I need to figure out a way to short circuit the fuse box without getting caught on the surveillance camera."
#   $quest.lindsey_breath.advance("fuse")
    pass
  return True




init python:
  class Item_package_hex_vex_and_texmex(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_hex_vex_and_texmex_interact")
      actions.append(["consume","Consume","?package_hex_vex_and_texmex_consume"])


label package_hex_vex_and_texmex_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_hex_vex_and_texmex_consume(item):
  $mc.remove_item("package_hex_vex_and_texmex")
  $mc.add_item("hex_vex_and_texmex")
  pause 0.25
  "When I thought the hunt for pussy would be magical, this isn't what I had in mind..."
  "But if it helps [maya], I guess I don't really have a choice."
  "..."
  "Not tonight, though."
  "She probably already thinks I'm a stalker... and for some reason,\nI don't want to push her away."
  "I better give her some space while I figure this shit out."
  window hide
  $quest.maya_witch.finish()
  return True




init python:
  class Item_package_hearshey_shesay(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_hearshey_shesay_interact")
      actions.append(["consume","Consume","?package_hearshey_shesay_consume"])


label package_hearshey_shesay_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_hearshey_shesay_consume(item):
  "I can smell the chocolatey sweetness wafting from the packaging."
  window hide
  $mc.remove_item("package_hearshey_shesay")
  $mc.add_item("hearshey_shesay")
  pause 0.25
  window auto
  "All right, I have all the pieces for the perfect apology."
  "Now, I just need to find the courage to face [isabelle]..."
  $quest.isabelle_gesture.advance("apology")
  return True




init python:
  class Item_package_tob_le_bone(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_tob_le_bone_interact")
      actions.append(["consume","Consume","?package_tob_le_bone_consume"])


label package_tob_le_bone_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_tob_le_bone_consume(item):
  "I can smell the chocolatey sweetness wafting from the packaging."
  window hide
  $mc.remove_item("package_tob_le_bone")
  $mc.add_item("tob_le_bone")
  pause 0.25
  window auto
  "All right, I have all the pieces for the perfect apology."
  "Now, I just need to find the courage to face [isabelle]..."
  $quest.isabelle_gesture.advance("apology")
  return True




init python:
  class Item_package_dick_wonky(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_dick_wonky_interact")
      actions.append(["consume","Consume","?package_dick_wonky_consume"])


label package_dick_wonky_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_dick_wonky_consume(item):
  "I can smell the chocolatey sweetness wafting from the packaging."
  window hide
  $mc.remove_item("package_dick_wonky")
  $mc.add_item("dick_wonky")
  pause 0.25
  window auto
  "All right, I have all the pieces for the perfect apology."
  "Now, I just need to find the courage to face [isabelle]..."
  $quest.isabelle_gesture.advance("apology")
  return True




init python:
  class Item_package_milk(Item):

    icon = "items package"

    def title(item):
      return "Mysterious Package"

    def description(item):
      return "Very light for its size.\nLuckily, weight doesn't always translate into worth."

    def actions(cls,actions):
      actions.append("?package_milk_interact")
      actions.append(["consume","Consume","?package_milk_consume"])


label package_milk_interact(item):
  "Smells a bit like paper and tape. Odd."
  return True

label package_milk_consume(item):
  "I better unpack this before it starts to curdle."
  # "[jo] would not be happy if my backpack started smelling like spoiled milk."
  "[jo] would not be happy if my backpack started smelling like spoiled milk.{space=-85}"
  window hide
  $mc.remove_item("package_milk")
  $mc.add_item("milk")
  pause 0.25
  window auto
  $quest.maxine_dive.advance("trade")
  return True
