init python:
  class Item_spray_cap(Item):
    icon="items bottle spray_cap" #TBD find spray cap image
    
    def title(item):
      return "Spray Cap"
   
    def description(item):
      return "Fill it, spray it, run — fun for everyone."
  
    def actions(cls,actions):
      actions.append("?int_spray_cap")
  
  #recipes
  add_recipe("spray_cap","water_bottle","spray_water")
  add_recipe("spray_cap","banana_milk","spray_banana_milk")
  add_recipe("spray_cap","empty_bottle","spray_empty_bottle")
  add_recipe("spray_cap","pepelepsi","spray_pepelepsi")
  add_recipe("spray_cap","salted_cola","spray_salted_cola")
  add_recipe("spray_cap","seven_hp","spray_seven_hp")
  add_recipe("spray_cap","strawberry_juice","spray_strawberry_juice")

label int_spray_cap(item):
  "I can hear the ocean inside it." 
  "..."
  "It's telling me that my ear needs cleaning."
  return True