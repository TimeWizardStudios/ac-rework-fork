init python:
  class Item_fishing_line(Item):

    icon = "items fishing_line"

    def title(item):
      return "Fishing Line"

    def description(item):
      return "Invisi-line — straighten out\nyour fishing today!"

    def actions(cls,actions):
      actions.append("?fishing_line_interact")


label fishing_line_interact(item):
  "First person to cross the fishing line is crowned the tripper!"
  return True
