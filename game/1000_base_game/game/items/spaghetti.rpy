init python:
  class Item_spaghetti(Item):

    icon = "items spaghetti"

    def title(item):
      return "Spaghetti"

    def description(item):
      # return "What's a romantic dinner without some spags?"
      return "What's a romantic dinner\nwithout some spags?"

    def actions(cls,actions):
      actions.append("?spaghetti_interact")


label spaghetti_interact(item):
  "A true lady and the tramp dinner."
  return True
