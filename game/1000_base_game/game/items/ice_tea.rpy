init python:
  class Item_ice_tea(Item):

    icon = "items ice_tea"

    def title(item):
      return "Ice Tea"

    def description(item):
      return "This is a trophy and a reward.\nNot for drinking."

    def actions(cls,actions):
      actions.append("?ice_tea_interact")

  add_recipe("ice_tea","sleeping_pills","ice_tea_sleeping_pills_combine")


label ice_tea_interact(item):
  "When an English girl gives you her favorite tea, it means that she trusts and secretly has the hots for you."
  "...at least, that's what I choose to believe."
  return True

label ice_tea_sleeping_pills_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  "Just pop a few of these bad boys into the drink and we'll have a perfect party cocktail."
  $process_event("items_combined",item1,item2,items_by_id["spiked_ice_tea"])
  $mc.add_item("spiked_ice_tea",silent=True)
  if quest.kate_stepping == "spiked_drink":
    $quest.kate_stepping.advance("isabelle")
  return
