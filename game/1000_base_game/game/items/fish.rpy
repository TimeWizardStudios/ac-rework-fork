init python:
  class Item_fish(Item):

    icon = "items fish"

    def title(item):
      return "Fish"

    def description(item):
      return "A locally grown fish without\nany added preservatives,\nhence the smell."

    def actions(cls,actions):
      actions.append("?fish_interact")


label fish_interact(item):
  "Today's catch, and I'm talking about the price."
  return True
