init python:
  class Item_half_letter_1(Item):
    icon="items half_letter_1"
    
    def title(item):
      return "Half a Letter"
    
    def description(item):
      return "A letter ripped in half."
    
    def actions(cls,actions):
      actions.append("?int_half_letter_1")

      
label int_half_letter_1(item):
  "{i}\"Use me...\"{/}"
  "Hot. I wonder what the rest of the letter says."
  return True
      
      
