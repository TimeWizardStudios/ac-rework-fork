init python:
  class Item_telescope(Item):

    icon = "items telescope"

    def title(item):
      return "Telescope"

    def description(item):
      return "The only thing missing from this beauty is a slot for coins."

    def actions(cls,actions):
      actions.append("?telescope_interact")


label telescope_interact(item):
  "Anything is better than being short-sighted."
  return True
