init python:
  class Item_note_from_flora(Item):

    icon = "items note_from_flora"

    def title(item):
      if quest.mrsl_bot["note_from_flora_interacted_with"]:
        return "Note from " + flora.name
      else:
        return "Fifth Note from " + mrsl.name + "?"

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?note_from_flora_interact")


label note_from_flora_interact(item):
  "{i}\"Is this, like, one of those anonymous 'Ask' advice columns?\"{/}"
  "{i}\"I read those all the time in The Cosmololitan!\"{/}"
  # "{i}\"Anyway, dear secret advice giver, how does one make a murder look like an accident so they can be an only child?\"{/}"
  "{i}\"Anyway, dear secret advice giver, how does one make a murder look{space=-25}\nlike an accident so they can be an only child?\"{/}"
  "{i}\"I'm just asking for a friend.\"{/}"
  "..."
  "Ugh! [flora]!"
  "She thinks she's so funny..."
  window hide
  $quest.mrsl_bot["note_from_flora_interacted_with"] = True
  $mc.remove_item("note_from_flora")
  pause 0.25
  window auto
  return True
