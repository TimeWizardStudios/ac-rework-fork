init python:
  class Item_power_plower(Item):
    icon="items plow"
    retain_capital = True
    
    def title(item):
      return "Power-Plower 3000"
    
    def description(item):
      return "This thing sure is worth the cheap price of $999, especially since you get {i}ANOTHER(!){/} Power-Plower 3000 if you order within the next hour!"
    
    def actions(cls,actions):
      actions.append("?int_power_plower")

label int_power_plower(item):
  "Fitting this thing into my backpack sure wasn't easy..."
  "Luckily, the hydraulics-powered mold-board compressor, the compound landside heel-frog-brace, and the foldable wooden beam make it manageable."
  "Ever since I ordered the Power-Plower 3000, keeping my fields nice and plowed has never been easier!"
  "I don't know where I'd be today if it weren't for the Power-Plower 3000!"
  return True
