init python:
  class Item_shovel(Item):
    icon="items shovel"
    
    def title(item):
      return "Shovel"
    
    def description(item):
      return "Six feet under, or under sexy feet. The difference is in the type of dirt." 
    
    def actions(cls,actions):
      actions.append("?int_shovel")
      #actions.append(["consume", "Consume", "?shovel_consume"])

  add_recipe("empty_can","stick","shovel_combination")
  

label shovel_combination(item, item2):
  $mc.remove_item("empty_can")
  $mc.remove_item("stick")
  "It ain't tryna be pretty, just useful."
  $mc.add_item("shovel")
  return True

label int_shovel(item):
  "The first shovel I've ever owned."
  "My ace of spades, if you will."
  return True
