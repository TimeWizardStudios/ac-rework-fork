init python:
  class Item_book_of_the_dammed(Item):

    icon = "items book book_of_the_dammed"
    retain_capital = True

    def title(item):
      return "\"Book of the Dammed: the Beaver\nEncyclopedia\" by J.J. Timberlake"

    def description(item):
      return "According to the backside blurb, the author lost his leg while hunting beavers and spent months trapped inside a beaver dam."

    def actions(cls,actions):
      actions.append("?book_of_the_dammed_interact")


label book_of_the_dammed_interact(item):
  "{i}\"Contains foul language and dangerous information. Read with extreme caution.\"{/}"
  "Why is there a disclaimer on the first page of this book?"
  return True
