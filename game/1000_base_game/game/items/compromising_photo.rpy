init python:
  class Item_compromising_photo(Item):

    icon="items compromising_photo"

    def title(item):
      return nurse.name + "'s Compromising Photo"

    def description(item):
      return "In case of my death, copies have been spread out and will be mailed to everyone. Heh, not really, but it would be cool."

    def actions(cls,actions):
      actions.append("?int_compromising_photo")


label int_compromising_photo(item):
  if quest.nurse_aid["name_reveal"]:
    # "Evidence for a blackmailing scheme. Poor [amelia]. She won't have an easy year, that's for sure."
    "Evidence for a blackmailing scheme. Poor [amelia]. She won't have an{space=-15}\neasy year, that's for sure."
  else:
    "Evidence for a blackmailing scheme. The poor [nurse]. She won't have an easy year, that's for sure."
  return True
