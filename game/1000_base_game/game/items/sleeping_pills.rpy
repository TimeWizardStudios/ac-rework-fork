init python:
  class Item_sleeping_pills(Item):

    icon = "items sleeping_pills"

    def title(item):
      return "Sleeping Pills"

    def description(item):
      return "{i}\"For a regular nap, take one pill. For a dirt nap, one bottle.\"{/}"

    def actions(cls,actions):
      actions.append("?sleeping_pills_interact")

  add_recipe("sleeping_pills","ice_tea","sleeping_pills_ice_tea_combine")


label sleeping_pills_interact(item):
  "{i}\"Side effects include dizziness, anal cravings, and a good night's sleep.\"{/}{space=-85}"
  return True

label sleeping_pills_ice_tea_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  "Just pop a few of these bad boys into the drink and we'll have a perfect party cocktail."
  $process_event("items_combined",item1,item2,items_by_id["spiked_ice_tea"])
  $mc.add_item("spiked_ice_tea",silent=True)
  if quest.kate_stepping == "spiked_drink":
    $quest.kate_stepping.advance("isabelle")
  return
