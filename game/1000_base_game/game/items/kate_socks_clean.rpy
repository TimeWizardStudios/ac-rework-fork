init python:
  class Item_kate_socks_clean(Item):

    icon = "items kate_socks_clean"

    def title(item):
      return kate.name + "'s Clean Socks"

    def description(item):
      return "The only thing softer than these socks are [kate]'s feet."

    def actions(cls,actions):
      actions.append("?kate_socks_clean_interact")


label kate_socks_clean_interact(item):
  "Washing a girl's clothes for her — chivalrous or pathetic?"
  "Probably both..."
  "Positive thinking."
  return True
