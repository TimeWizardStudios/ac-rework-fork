init python:
  class Item_nymphoria_heartus(Item):
    icon="items nymphoria_heartus"
    retain_capital = True
    
    def title(item):
      return "Nymphoria Heartus"
    
    def description(item):
      return "Said to have the power to enamor even the bitterest of forest nymphs, this herb only grows in the presence of true love."
    
    def actions(cls,actions):
      actions.append("?int_nymphoria_heartus")
      
label int_nymphoria_heartus(item):
  "Heart-shaped leaves have long attracted biologists and love-sick teenagers alike."
  return True
      
