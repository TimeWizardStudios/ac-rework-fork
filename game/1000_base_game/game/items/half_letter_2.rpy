init python:
  class Item_half_letter_2(Item):
    icon="items half_letter_2"
    
    def title(item):
      return "Other Half of the Letter"
    
    def description(item):
      return "A letter tworn in twain."
    
    def actions(cls,actions):
      actions.append("?int_half_letter_2")

      
label int_half_letter_2(item):
  "{i}\"...if you get hungry.\"{/}"
  "Damn. What does that mean?"
  return True 
      
      
