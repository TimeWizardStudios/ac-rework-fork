init python:
  class Item_sandwich(Item):

    icon = "items sandwich"

    def title(item):
      return "Sandwich"

    def description(item):
      # return "Bread and stuff in between. An ageless classic."
      return "Bread and stuff in between.\nAn ageless classic."

    def actions(cls,actions):
      actions.append("?sandwich_interact")


label sandwich_interact(item):
  "Who doesn't like a good sandwich? Especially, girls."
  return True
