init python:
  class Item_note_from_maya(Item):

    icon = "items note_from_maya"

    def title(item):
      if quest.mrsl_bot["note_from_maya_interacted_with"]:
        return "Note from " + maya.name
      else:
        return "Third Note from " + mrsl.name + "?"

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?note_from_maya_interact")


label note_from_maya_interact(item):
  "{i}\"Aw, how cute! Passing notes?\"{/}"
  "{i}\"I remember when I was five, too.\"{/}"
  "{i}\"Now, if you could start leaving something of use, that would be cool.\"{/}"
  "{i}\"Like a dildo, cocaine, or ball of yarn.\"{/}"
  "..."
  "Ugh! Seriously, [maya]?!"
  "Of course she would be curious about a box..."
  "Surely, I have to get a reply from [mrsl] tomorrow, right?"
  window hide
  if "further_notes_to_mrsl" in quest.mrsl_bot._failed_items and "ball_of_yarn" in quest.mrsl_bot._failed_items["further_notes_to_mrsl"]:
    $quest.mrsl_bot._failed_items["further_notes_to_mrsl"].remove("ball_of_yarn")
  $quest.mrsl_bot["note_from_maya_interacted_with"] = True
  $mc.remove_item("note_from_maya")
  return
