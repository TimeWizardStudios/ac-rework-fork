init python:
  class Item_hearshey_shesay(Item):

    icon = "items hearshey_shesay"

    def title(item):
      return "Hearshey Shesay"

    def description(item):
      # return "This chocolate is said to have been created outside the chocolate factory."
      return "This chocolate is said to\nhave been created outside\nthe chocolate factory."

    def actions(cls,actions):
      actions.append("?hearshey_shesay_interact")


label hearshey_shesay_interact(item):
  "Some say this is the finest chocolate in all of Newfall, but others say otherwise."
  "The court is still out on this one."
  return True
