init python:
  class Item_flora_phone(Item):

    icon="items flora_phone"

    def title(item):
      return flora.name + "'s Phone"

    def description(item):
      return "[flora]'s only possession that isn't pink or adorable in some way. She likes to keep a professional appearance." 

    def actions(cls,actions):
      actions.append("?int_flora_phone")


label int_flora_phone(item):
  "Locked with both fingerprint and code... she must be hiding something."
  return True
