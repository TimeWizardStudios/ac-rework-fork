init python:
  class Item_confetti(Item):
    
    icon="items confetti"
    
    def title(item):
      return "Confetti"
    
    def description(item):
      return "[jacklyn] was right."
    
    def actions(cls,actions):
      actions.append("?confetti_interact")

label confetti_interact(item):
  "Little bits of garbage."
  return True
