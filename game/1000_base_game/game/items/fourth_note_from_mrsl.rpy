init python:
  class Item_fourth_note_from_mrsl(Item):

    icon = "items note_from_mrsl"

    def title(item):
      return "Fourth Note from " + mrsl.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      if quest.mrsl_bot["note_from_flora_interacted_with"]:
        actions.append("?fourth_note_from_mrsl_interact")
      else:
        actions.append("?note_from_flora_interact")


label fourth_note_from_mrsl_interact(item):
  # "{i}\"I am afraid your little game has not quite had the effect you desired.\"{/}"
  "{i}\"I am afraid your little game has not quite had the effect you desired.\"{/}{space=-40}"
  "{i}\"Most of my movements are too closely watched, still.\"{/}"
  "{i}\"Especially for something like that little workout.\"{/}"
  "..."
  "Damn it! This did help us communicate a bit, but I guess I'm going to need something bigger now..."
  "Maybe some kind of diversion to distract whoever is keeping tabs on [mrsl]?"
  "Hmm... who do I know that is good at stirring up talk?"
  window hide
  $quest.mrsl_bot["fourth_note_from_mrsl_interacted_with"] = True
  $mc.remove_item("fourth_note_from_mrsl", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_mrsl}{space=50}|Lost\n{color=#900}Fourth Note from\n[mrsl]{/}",5.0)
  $quest.mrsl_bot.advance("help_2_electric_boogaloo")
  return
