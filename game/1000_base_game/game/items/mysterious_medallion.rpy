init python:
  class Item_mysterious_medallion(Item):

    icon="items mysterious_medallion"

    def title(item):
      return "Mysterious Medallion"

    def description(item):
      return "One of my most prized possessions, also known as \"the Pussy Magnet.\"\n\nOnly the repelling end seems to be working, though."

    def actions(cls,actions):
      actions.append("?mysterious_medallion_interact")

  add_recipe("mysterious_coin","ball_of_yarn","mysterious_medallion_combine")


label mysterious_medallion_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $mc.add_item("mysterious_medallion",silent=True)
  $game.notify_modal(None,"Combine","{image= items mysterious_medallion}{space=60}|You combine {color=#900}"+str(item1)+"{/} with {color=#900}"+str(item2)+"{/} and make {color=#900}Mysterious Medallion{/}.",10.0)
  $achievement.putting_things_together.unlock()
  return

label mysterious_medallion_interact(item):
  "The Medallion."
  return True
