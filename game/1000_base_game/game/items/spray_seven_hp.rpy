init python:
  class Item_spray_seven_hp(Item):
    icon="items bottle spray_seven_hp"
    bottled = True
    
    def title(item):
      return "Seven HP"
    
    def description(item):
      return "Bottled loser tears — that's what the jocks call it."
    
    def actions(cls,actions):
      actions.append("?int_seven_hp")
      actions.append(["consume","Consume","consume_spray_seven_hp"])

label consume_spray_seven_hp(item):
  $mc.remove_item("spray_seven_hp")
  $mc.add_item("spray_empty_bottle")
  "God. If refreshing and disgusting had a lovechild, this would be it."
  return True