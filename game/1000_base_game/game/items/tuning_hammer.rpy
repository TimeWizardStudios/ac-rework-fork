init python:
  class Item_tuning_hammer(Item):

    icon = "items tuning_hammer"

    def title(item):
      return "Tuning Hammer"

    def description(item):
      return "Not exactly Mjolnir,\nbut it's not too shabby..."

    def actions(cls,actions):
      actions.append("?tuning_hammer_interact")


label tuning_hammer_interact(item):
  "Together with a tuning fork, this thing could make any old lady sing.{space=-5}"
  return True
