init python:
  class Item_diary_page(Item):

    icon="items ripped_diary_page"

    def title(item):
      return "Secret Diary Page"

    def description(item):
      return "An old page torn from a diary."

    def actions(cls,actions):
      actions.append("?diary_page_interact")

  add_recipe("diary_page","water_bottle","diary_page_revealed")
  add_recipe("diary_page","spray_water","diary_page_revealed")


label diary_page_interact(item):
  show misc secret_diary with Dissolve(.5)
  pause
  hide misc secret_diary with Dissolve(.5)
  return True

label diary_page_revealed(item, item2):
  show misc secret_diary with Dissolve(.5)
  show misc secret_diary_2 with Dissolve(2)
  pause
  hide secret_diary
  hide misc secret_diary_2
  with Dissolve(.5)
  return
