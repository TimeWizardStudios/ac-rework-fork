init python:
  class Item_letter_for_jo(Item):

    icon = "items letter_for_jo"

    def title(item):
      return "Letter for " + jo.name

    def description(item):
      return "Mailman during the day, crime fighter at night — I'm Devil Letterman."

    def actions(cls,actions):
      actions.append("?letter_for_jo_interact")


label letter_for_jo_interact(item):
  "Why does it feel like there's a living creature just on the other side of the paper?"
  "Wait, that's just my finger."
  return True
