init python:
  class Item_gigglypuff_tea(Item):
    
    icon="items tea"
    can_place_to_desk = True    
    consumable = True
    
    def title(item):
      return "Gigglypuff Tea"
    
    def description(item):
      return "Smells like burning rubber and gorgonzola. A killer combination."

    def actions(cls,actions):
      actions.append("?gigglypuff_tea_interact")
     
label gigglypuff_tea_interact(item):
  "If a war was ever fought over this hallowed beverage, I'm sure there'd be lots of casualteas."
  return True

