init python:
  class Item_isabelle_glasses_inv(Item):

    icon = "items isabelle_glasses"

    def title(item):
      return isabelle.name + "'s Glasses"

    def description(item):
      return "It's a pair of Olivia Peoples nonprescription redwood-framed glasses.\n\nShit... is her last name Bateman?"


    def actions(cls,actions):
      actions.append("?isabelle_glasses_interact")


label isabelle_glasses_interact(item):
  "Whoa! When I put these on, I can finally see!"
  "Is that a pixie?!"
  "Ah, no — it's just [flora]."
  return True
