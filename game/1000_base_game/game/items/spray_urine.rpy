init python:
  class Item_spray_urine(Item):

    icon="items bottle spray_urine"
    bottled = True

    def title(item):
      return "Spray Bottle of Urine"

    def description(item):
      return "A weapon of sorts. A loaded gun. No one's safe."

    def actions(cls,actions):
      actions.append("?int_spray_urine")
      actions.append(["consume","Consume","consume_spray_urine"])


label int_spray_urine(item):
  "For when the cat slept with your girlfriend or ate the last of the ice cream."
  return True

label consume_spray_urine(item):
  $mc.remove_item(item)
  $mc.add_item("spray_empty_bottle")
  "Bear Grylls would've been proud of me."
  "Anyone else... probably just disgusted."
  return True
