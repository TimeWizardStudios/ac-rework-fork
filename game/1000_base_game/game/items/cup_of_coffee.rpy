init python:
  class Item_cup_of_coffee(Item):
    icon="items coffee"
    
    def title(item):
      return "Cup of Coffee"
    
    def description(item):
      return "Blackest of the black, darker than night. That's how I like both my coffee and my horror punk."
    
    def actions(cls,actions):
      actions.append("?int_cup_of_coffee")
      actions.append(["consume", "Consume", "consume_cup_of_coffee"])
      
label int_cup_of_coffee(item):
  "They call it the black gold, but what about Pepelepsi? That's the real heavy metal of the beverage world."
  return True
      
label consume_cup_of_coffee(item):
  $mc.remove_item(item)
  $mc.add_item("coffee_cup")
  "A bean-juice container. Sweet!"
  return True
