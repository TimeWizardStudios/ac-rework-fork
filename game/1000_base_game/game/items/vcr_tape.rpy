init python:
  class Item_vcr_tape(Item):

    icon="items vcr_tape"

    def title(item):
      return "HOT Box Set"

    def description(item):
      return "\"High-intensity Omni-flex Training,\" also known as \"HOT.\""

    def actions(cls,actions):
      actions.append("?vcr_tape_interact")


label vcr_tape_interact(item):
  "{i}\"Due to the difficulty and intensity, exercises are best conducted without the hindrance of clothing.\"{/}"
  "Sounds like my kind of workout."
  return True
