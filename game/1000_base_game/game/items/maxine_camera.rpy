init python:
  class Item_maxine_camera(Item):

    icon="items maxine_camera"

    def title(item):
      return maxine.name + "'s Camera"

    def description(item):
      return "An heirloom passed down through generations. At least, that's what the label says."

    def actions(cls,actions):
      if quest.nurse_venting == "bottle" and (game.location == "school_entrance" or game.location.id.startswith("home_")):
        actions.append("?quest_nurse_venting_maxine_camera_home")
      elif "stuck" > quest.nurse_venting > "help":
        actions.append("quest_nurse_venting_maxine_camera")
      else:
        actions.append("?maxine_camera_interact")


label maxine_camera_interact(item):
  "How do you even take selfies with this thing?"
  "No wonder social media wasn't big in the 1800s."
  return True
