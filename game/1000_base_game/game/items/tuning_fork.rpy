init python:
  class Item_tuning_fork(Item):

    icon = "items tuning_fork"

    def title(item):
      return "Tuning Fork"

    def description(item):
      return "Not the best way to eat, but it's not a competition."

    def actions(cls,actions):
      actions.append("?tuning_fork_interact")


label tuning_fork_interact(item):
  "Combined with a tuning hammer, you basically have the whole cutlery set."
  return True
