init python:
  class Item_frog(Item):

    icon = "items frog"

    def title(item):
      return "Frog"

    def description(item):
#     return "Will you turn into a princess if I kiss you?\n\n...\n\nWorth a shot."
      return "Will you turn into a princess\nif I kiss you?\n\n...\n\nWorth a shot."

    def actions(cls,actions):
      actions.append("?frog_interact")


label frog_interact(item):
  $nurse.default_name = "Frog"
  nurse "Rubbit! Rubbit!"
  mc "Rub it? If you insist..."
  nurse "Rubbit! Rubbit!"
  mc "You like that?"
  nurse "{i}Gurgle...{/}"
  $nurse.default_name = "Nurse"
  return True
