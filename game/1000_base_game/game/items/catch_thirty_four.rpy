init python:
  class Item_catch_thirty_four(Item):
    icon="items book catch_34"
    retain_capital = True
    def title(item):
      return "\"Catch 34, a Porno-Political Conflict\""
    
    def description(item):
      return "Nothing's more classy than vintage erotica."
    
    def actions(cls,actions):
      actions.append("?int_catch_thirty_four")
   
label int_catch_thirty_four(item):
  "A dilemma or difficult circumstance from which there is no escape because of the sheer lewdness."
  return True
