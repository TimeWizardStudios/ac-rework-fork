init python:
  class Item_isabelle_panties_inv(Item):

    icon = "items isabelle_panties"

    def title(item):
      return isabelle.name + "'s Panties"

    def description(item):
      return "[isabelle] is going commando\nright now. What a thought!"


    def actions(cls,actions):
      actions.append("?isabelle_panties_interact")


label isabelle_panties_interact(item):
  "[isabelle] has entrusted me with one of her most private possessions."
  "Sniffing these would be an unforgivable sin."
  if mc.at("school_*") and not quest.isabelle_locker["sniff_panties"]:
    menu(side="middle"):
      extend ""
      "Sniff them":
        "..."
        $mc.lust+=1
        "{i}*Sniff, sniff*{/}"
        "Damn, that's potent!"
        "Smells like tea and biscuits and pussy..."
        "{i}*Sniff, sniff*{/}"
        "Oh, hell yeah!"
        "I can't believe she smells so good!"
        show isabelle afraid at appear_from_right
        isabelle afraid "!!!" with vpunch
        mc "Err..."
        isabelle cringe "What the hell, [mc]?!"
        show isabelle cringe at move_to(.75)
        menu(side="left"):
          extend ""
          "\"It's not what it looks like...\"":
            show isabelle cringe at move_to(.5)
            mc "It's not what it looks like..."
            isabelle cringe "I trusted you..."
            mc "I'm sorry, I—"
            isabelle angry "You're unbelievable!"
            isabelle sad "We buried the box together... shared a moment and a kiss..."
            $isabelle.love-=5
            $isabelle.lust-=3
            isabelle sad "I think I'm going to be sick..."
            isabelle annoyed "Give them back!"
            show isabelle annoyed with vpunch:
              linear .25 zoom 1.5 yoffset 750
            show isabelle annoyed_holdingpanties:
              linear .25 zoom 1 yoffset 0
            $mc.remove_item("isabelle_panties_inv")
            isabelle annoyed_holdingpanties "I'll steal [maxine]'s cat on my own."
            isabelle angry_holdingpanties "Screw you, you twat."
            show isabelle angry_holdingpanties at disappear_to_right
            $isabelle.equip("isabelle_panties")
            $isabelle["romance_disabled"] = True
            $game.notify_modal(None,"Love or Lust","Romance with [isabelle]:\nDisabled.",wait=5.0)
            hide isabelle
            "Well, that was probably the biggest mistake in my life..."
            "Shit."
            $quest.isabelle_locker.fail()
          "?mc.charisma>=10@[mc.charisma]/10|{image=stats cha}|\"Don't get your knickers in a twist just yet.\"":
            show isabelle cringe at move_to(.5)
            mc "Don't get your knickers in a twist just yet."
            isabelle cringe "..."
            mc "I mean, not that you can right now, anyway..."
            isabelle skeptical "Funny."
            isabelle skeptical "Why were you sniffing my panties?"
            mc "[spinach] had trouble picking up your scent, so I just wanted to check if there was any."
            isabelle concerned "Really?"
            mc "Absolutely! My motives were pure and scientific."
            isabelle concerned "Well, what did you conclude?"
            mc "That the scent of your panties are sufficient."
            isabelle neutral "Sufficient?"
            mc "Also, heavenly."
            isabelle excited "Is that your scientific opinion?"
            mc "It's entirely objective and peer reviewed."
            isabelle skeptical "Peer reviewed...?"
            mc "Don't worry, that part was a joke."
            isabelle laughing "Let's just steal the cat."
            mc "Oh, I have nothing but that sweet pussy on my mind!"
            $isabelle.lust+=2
            $isabelle.love-=1
            isabelle eyeroll "I'll wait outside..."
            show isabelle eyeroll at disappear_to_right
            $achievement.cat_whisperer.unlock()
      "Don't be a creep":
        $mc.love+=1
        "As a pervert in recovery, passing this test of will was a great step forward."
    $quest.isabelle_locker["sniff_panties"] = True
  return True
