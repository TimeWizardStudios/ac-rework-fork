init python:
  class Item_dog_collar(Item):

    icon = "items dog_collar"

    def title(item):
      return "Dog Collar"

    def description(item):
      return "An item made dirty\nby pop culture alone."

    def actions(cls,actions):
      actions.append("?dog_collar_interact")


label dog_collar_interact(item):
  "Nothing but the finest budget nylon for my bitch."
  return True
