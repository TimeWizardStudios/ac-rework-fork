init python:
  class Item_love_potion_droplet(Item):

    icon = "items love_potion_droplet"

    def title(item):
      return "Love Potion Droplet"

    def description(item):
      return "Smells like paint thinner,\nbut in a good way."

    def actions(cls,actions):
      actions.append("?love_potion_droplet_interact")


label love_potion_droplet_interact(item):
  "I told her that I was a flop with chicks."
  "I'd been this way since 1956."
  "She looked at my palm and she made a magic sign."
  "She said, \"what you need is Love Potion No. 9.\""
  return True
