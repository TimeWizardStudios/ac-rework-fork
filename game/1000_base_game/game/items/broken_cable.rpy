init python:
  class Item_broken_cable(Item):

    icon="items frayed_cable" 

    def title(item):
      return "Broken Cable"

    def description(item):
      return "Fixing this cable will require prayers and copious amounts of electrical tape."

    def actions(cls,actions):
      actions.append("?int_broken_cable")


label int_broken_cable(item):
  "Broken, frayed, and all around miserable. Much like this cable."
  return True
