init python:
  class Item_maxine_other_nude(Item):

    icon="items maxine_other_nude"

    def title(item):
      return maxine.name + "'s Other Nude"

    def description(item):
      return "I miss her first nude. Maybe I have a thing for frail wrists?"

    def actions(cls,actions):
      actions.append("?maxine_other_nude_interact")


label maxine_other_nude_interact(item):
  show misc maxine_other_nude with Dissolve(.5)
  "There's no doubt that [maxine]'s a little crazy."
  "There's also no doubt that [maxine]'s crazy hot."
  hide misc maxine_other_nude with Dissolve(.5)
  return True
