init python:
  class Item_gigglypuff_leaves_corrupted(Item):
    icon="items corrupted_gigglypuffleaves"
    
    def title(item):
      return "Corrupted Gigglypuff Leaves"
    
    def description(item):
      return "They're thrumming with dark energies, threatening to eclipse the world in a new dawn of evil."
    
    def actions(cls,actions):
      actions.append("?int_gigglypuff_leaves_corrupted")
      #actions.append(["consume", "Consume", "?gigglypuff_leaves_corrupted_consume"])

label int_gigglypuff_leaves_corrupted(item):
  "It's just leaves. Nothing sinister about them. Regular organic and honest-to-god leaves."
  return True
