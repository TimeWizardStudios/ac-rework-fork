init python:
  class Item_frog_cage(Item):

    icon = "items frog_cage"

    def title(item):
      return "Frog Cage"

    def description(item):
      return "I hope these don't get mistaken for a science project..."

    def actions(cls,actions):
      actions.append("?frog_cage_interact")


label frog_cage_interact(item):
  "Let's see here..."
  "We have Timmy... Jimmy... Slimmy... Tammy... and Frank."
  "Yep! Gang's all here!"
  return True
