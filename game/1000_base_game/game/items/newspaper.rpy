init python:
  class Item_newspaper(Item):

    icon = "items newspaper"

    def title(item):
      return "Newspaper"

    def description(item):
      return "[maxine]'s latest issue —\na Peter Parker meets Frankenstein{space=-5}\nsort of creation."

    def actions(cls,actions):
      actions.append("?newspaper_interact")


label newspaper_interact(item):
  show misc newspaper5 with Dissolve(.5)
  pause
  hide misc newspaper5 with Dissolve(.5)
  return True
