init python:
  class Item_mysterious_coin(Item):
    icon="items mysterious_coin"
    
    def title(item):
      return "Mysterious Coin"
    
    def description(item):
      return "Strange symbols line the surface of this brass coin. The hole in the middle is large enough for a string."
    
    def actions(cls,actions):
      actions.append("?int_mysterious_coin")
      
label int_mysterious_coin(item):
  "A coin of mystical powers. Flipping it could have astronomical consequences."
  menu(side="middle"):
    extend ""
    "Flip the coin":
      "Tails."
      "Of course. Never lucky. Seriously, even in coin flips I lose."
      "The coin is probably rigged somehow."
      "Honestly, the weather conditions for the flip sucked."
      "Plus I wasn't taking the flip seriously."
    "Gambling is dangerous":
      "This is the wise decision."
      "Who knows what could've happened?"
      "Poverty? Disease? Death? Or worse still... losing?"
      "Definitely matrixed a bullet there."
  return True
      
