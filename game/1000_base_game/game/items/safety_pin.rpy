init python:
  class Item_safety_pin(Item):
    icon="items safety_pin"

    def title(item):
      return "Safety Pin"

    def description(item):
      return "Just a regular safety pin of the most standard sort. It's so mundane that it must have magical properties."

    def actions(cls,actions):
      actions.append("?int_safety_pin")


label int_safety_pin(item):
  "Ouch, my finger! I really needed that drop of blood."
  return True 
