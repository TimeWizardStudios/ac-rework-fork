init python:
  class Item_candle(Item):
    
    icon="items candle"
    
    def title(item):
      return "Candle"
    
    def description(item):
      return "A light in the black... or just a candle without matches."
    
    def actions(cls,actions):
      actions.append("?candle_interact")

label candle_interact(item):
  "All that's missing is matches and lube."
  return True
