init python:
  class Item_independence_day_decorations(Item):
    
    icon="items independence_day_decorations"
    
    def title(item):
      return "Independence Day Decorations"
    
    def description(item):
      return "The good thing about decorations on a string is... hmm... ... Back to the studio!"

    def actions(cls,actions):
      actions.append("?independence_day_decorations_interact")
  
label independence_day_decorations_interact(item):
  "St. Mercuria would've been proud."
  "Freeing Newfall from the debauchery of the upper class."
  "A true hero of her time." 
  return True
