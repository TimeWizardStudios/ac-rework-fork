init python:
  class Item_key_jo_room(Item):

    icon="items joskey"

    def title(item):
      return "Key to " + jo.name + "'s Room"

    def description(item):
      return "Having the key doesn't mean I can just enter. [jo] has surveillance cameras, trip wires, and a sixth sense."

    def actions(cls,actions):
      actions.append("?int_key_jo_room")


label int_key_jo_room(item):
  "The key to the chamber of secrets."
  "And I'm not talking about [jo]'s pussy."
  "Okay, fine. I am."
  return True
