init python:
  class Item_fifth_note_from_mrsl(Item):

    icon = "items note_from_mrsl"

    def title(item):
      return "Fifth Note from " + mrsl.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?fifth_note_from_mrsl_interact")


label fifth_note_from_mrsl_interact(item):
  "{i}\"I'm not sure what you did, but I am no longer being watched so closely.\"{/}"
  "{i}\"If you truly wish to follow through with this, then meet me in the gym after school.\"{/}"
  if quest.mrsl_bot == "aftermath":
    "..."
    "Holy crap! It worked!"
    # "I knew a little patience and creativity would work out in the long run..."
    "I knew a little patience and creativity would work out in the long run...{space=-30}"
    "After school, huh? Oh, boy! Okay!"
    $quest.mrsl_bot["fifth_note_from_mrsl_interacted_with"] = True
    $quest.mrsl_bot.advance("meeting_2_electric_boogaloo")
  return True
