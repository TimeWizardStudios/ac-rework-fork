init python:
  class Item_ball_of_yarn(Item):

    icon = "items ball_of_yarn"

    def title(item):
      return "Ball of Yarn"

    def description(item):
      return "The String of Fate. Cutting it may change the course of history."

    def actions(cls,actions):
      actions.append("?int_ball_of_yarn")


label int_ball_of_yarn(item):
  "One of my least interesting possessions. A ball of yawn."
  return True


init python:
  class Item_ball_of_yarn_pink(Item):

    icon = "items ball_of_yarn_pink"

    def title(item):
      return "Pink Ball of Yarn"

    def description(item):
      return "This yarn is looking a little tense, like it just needs to... unwind."

    def actions(cls,actions):
      actions.append("?ball_of_yarn_pink_interact")


label ball_of_yarn_pink_interact(item):
  "I'll be counting balls of yarn in my sleep tonight..."
  return True


init python:
  class Item_ball_of_yarn_red(Item):

    icon = "items ball_of_yarn_red"

    def title(item):
      return "Red Ball of Yarn"

    def description(item):
      return "This yarn is looking a little tense, like it just needs to... unwind."

    def actions(cls,actions):
      actions.append("?ball_of_yarn_red_interact")


label ball_of_yarn_red_interact(item):
  "I'll be counting balls of yarn in my sleep tonight..."
  return True


init python:
  class Item_ball_of_yarn_gray(Item):

    icon = "items ball_of_yarn_gray"

    def title(item):
      return "Gray Ball of Yarn"

    def description(item):
      return "This yarn is looking a little tense, like it just needs to... unwind."

    def actions(cls,actions):
      actions.append("?ball_of_yarn_gray_interact")


label ball_of_yarn_gray_interact(item):
  "I'll be counting balls of yarn in my sleep tonight..."
  return True


init python:
  class Item_ball_of_yarn_yellow(Item):

    icon = "items ball_of_yarn_yellow"

    def title(item):
      return "Yellow Ball of Yarn"

    def description(item):
      return "This yarn is looking a little tense, like it just needs to... unwind."

    def actions(cls,actions):
      actions.append("?ball_of_yarn_yellow_interact")


label ball_of_yarn_yellow_interact(item):
  "I'll be counting balls of yarn in my sleep tonight..."
  return True


init python:
  class Item_ball_of_yarn_blue(Item):

    icon = "items ball_of_yarn_blue"

    def title(item):
      return "Blue Ball of Yarn"

    def description(item):
      return "This yarn is looking a little tense, like it just needs to... unwind."

    def actions(cls,actions):
      actions.append("?ball_of_yarn_blue_interact")


label ball_of_yarn_blue_interact(item):
  "I'll be counting balls of yarn in my sleep tonight..."
  return True
