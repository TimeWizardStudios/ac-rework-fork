init python:
  class Item_paper(Item):
    
    icon="items paper"
    
    def title(item):
      return "Paper"
    
    def description(item):
      return "On the surface, just a regular white paper."
    
    def actions(cls,actions):
      actions.append("?paper_interact")
  
label paper_interact(item):
  "Not all white papers are racist. Just this one."
  return True
