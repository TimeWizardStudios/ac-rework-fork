init python:
  class Item_computer(Item):

    icon = "items computer"

    def title(item):
      return "Computer"

    def description(item):
      # return "Zeroes and ones. Nothing in between."
      return "Zeroes and ones.\nNothing in between."

    def actions(cls,actions):
      actions.append("?computer_interact")


label computer_interact(item):
  "Inter Pantsium 4."
  "Triple-core ASSUS GPU with a built-in exhaust pipe."
  "Runs on diesel."
  return True
