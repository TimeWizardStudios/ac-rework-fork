init python:
  class Item_rusty_key(Item):

    icon = "items rusty_key"

    def title(item):
      return "Rusty Key"

    def description(item):
      return "Medieval in design and size."

    def actions(cls,actions):
      actions.append("?rusty_key_interact")


label rusty_key_interact(item):
  "Whose heart does this key unlock?"
  return True
