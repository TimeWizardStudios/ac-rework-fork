init python:
  class Item_eraser(Item):
    icon="items eraser"
   
    def title(item):
      return "Eraser"
    
    def description(item):
      return "The best tool to undo mistakes. At least, the literary kind."
    
    def actions(cls,actions):
      actions.append("?int_eraser")
      
label int_eraser(item):
  "Erase the past, erase yourself. Nothing matters if no one knows of your existence."
  return True
      
      
