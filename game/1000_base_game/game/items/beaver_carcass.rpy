init python:
  class Item_beaver_carcass(Item):
   
    icon="items beaver_carcass"
    tiertwo = True
    
    def title(item):
      return "Beaver Carcass"
   
    def description(item):
      return "It's already starting to rot. The smell is unbearable."
   
    def actions(cls,actions):
      actions.append("?int_beaver_carcass")
      
label int_beaver_carcass(item):
  "Who the hell carries around a dead beaver in their backpack? Ugh, probably just me."
  return True
      
      
