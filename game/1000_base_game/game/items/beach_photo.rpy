init python:
  class Item_beach_photo(Item):

    icon = "items picture_frame"

    def title(item):
      return "Beach Photo"

    def description(item):
      return "As always, the camera man is missing from the shot."

    def actions(cls,actions):
      actions.append("?beach_photo_interact")


label beach_photo_interact(item):
  show misc beach_photo with Dissolve(.5)
  "I had forgotten how cute they looked that day."
  "Well, [flora] is cute. [jo] is more... sexy? Is that weird to think?"
  "..."
  "The smell of sand and ocean water, bodies sweating in the sun..."
  # "[flora] doing that little shimmy towel dance to get out of her clothes..."
  "[flora] doing that little shimmy towel dance to get out of her clothes...{space=-15}"
  "[jo] wrapping her lips tight around a popsicle..."
  "Me trying to hide my raging erection by lying on my stomach..."
  "[jo] rubbing sunscreen on my back and legs..."
  "Man, what a time to be alive."
  window hide
  hide misc beach_photo with Dissolve(.5)
  return True
