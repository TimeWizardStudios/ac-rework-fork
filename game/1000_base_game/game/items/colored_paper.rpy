init python:
  class Item_colored_paper(Item):

    icon="items colored_paper"

    def title(item):
      return "Colored Papers"

    def description(item):
      return "Vibrant like a forest dressed for autumn! Shit. I'm starting to sound like [isabelle]."

    def actions(cls,actions):
      actions.append("?colored_paper_interact")

  add_recipe("colored_paper","glass_shard","combine_colored_paper_glass_shard")
  add_recipe("colored_paper","beaver_pelt","combine_colored_paper_beaver")
  add_recipe("colored_paper","beaver_carcass","combine_colored_paper_beaver")


label colored_paper_interact(item):
  "They say that the best season in Newfall is the fall."
  "Who could've guessed that?"
  return True
