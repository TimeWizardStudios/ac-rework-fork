init python:
  class Item_empty_bottle(Item):
    icon="items bottle empty_bottle"
    
    def title(item):
      return "Empty Bottle"
   
    def description(item):
      return "Just fill her up again — recycling in its purest form."
  
    def actions(cls,actions):
      actions.append("?int_empty_bottle")
      
label int_empty_bottle(item):
  mc "A bottle is never truly empty. Right now, it's filled with all sorts of nasty gases. Oxygen is just one of them."
  return True
