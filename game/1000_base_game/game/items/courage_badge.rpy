init python:
  class Item_courage_badge(Item):
   
    icon="items courage_badge"
    
    def title(item):
      return "Courage Badge"
   
    def description(item):
      return "It's like the metal itself has caught some of [mrsl]'s sizzling heat."
  
    def actions(cls,actions):
      actions.append("?int_courage_badge")
      
label int_courage_badge(item):
  "The badge is meant to inspire courage, but whenever I look at it, my thoughts drift to [mrsl]'s cleavage.."
  return True