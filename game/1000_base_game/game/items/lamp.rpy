init python:
  class Item_lamp(Item):
    icon="items lamp"
    
    def title(item):
      return "Lamp"
    
    def description(item):
      return "If lamps weren't a thing, our eyes would surely have developed night vision by now. Good job, science."
    
    def actions(cls,actions):
      actions.append("?int_lamp")
      #actions.append(["consume", "Consume", "?lamp_consume"])

label int_lamp(item):
  "Before the lightbulb's invention, did no one have any bright ideas?"
  "Must've been a dark time, indeed."
  return True
