init python:
  class Item_dish_brush(Item):
    icon="items dishbrush"
    tiertwo = True    
    
    def title(item):
      return "Dish Brush"
    
    def description(item):
      return "To a plateophile, doing the dishes has an entirely different meaning."
    
    def actions(cls,actions):
      actions.append("?int_dish_brush")
      #actions.append(["consume", "Consume", "?dish_brush_consume"])

label int_dish_brush(item):
  "Sharp hard bristles. Unyielding polyvinyl-chloride handle. Ribbed anti-slide grip."
  "The ultimate cleaning tool."
  return True
