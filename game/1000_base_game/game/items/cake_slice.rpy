init python:
  class Item_cake_slice(Item):
    
    icon="items cake_slice" 

    def title(item):
      return "Slice of Cake"
    
    def description(item):
      return "This cocaine cake is more addictive than... well, cocaine."
    
    def actions(cls,actions):
      actions.append("?cake_slice_interact")
     
label cake_slice_interact(item):
  "[flora] always finds her recipes on the dark web."
  return True
