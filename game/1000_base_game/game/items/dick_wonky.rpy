init python:
  class Item_dick_wonky(Item):

    icon = "items dick_wonky"

    def title(item):
      return "Dick Wonky's Chocolate Bar"

    def description(item):
      # return "What girl would say no to a bit of chocolate?"
      return "What girl would say no\nto a bit of chocolate?"

    def actions(cls,actions):
      actions.append("?dick_wonky_interact")


label dick_wonky_interact(item):
  "I'm feeling a little hypoglycemic..."
  "I better give this to [isabelle] before I get hangry and eat it."
  return True
