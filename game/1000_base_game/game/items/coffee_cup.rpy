init python:
  class Item_coffee_cup(Item):
    icon="items coffee_cup"
    def title(item):
      return "Coffee Cup"
    def description(item):
      return "Porcelain. Zero magical properties."
    def actions(cls,actions):
      actions.append("?int_coffee_cup")
      
label int_coffee_cup(item):
  "Smears of lipstick on the rim. [mrsl] clearly doesn't like drinking coffee through a straw."
  return True
      
      
