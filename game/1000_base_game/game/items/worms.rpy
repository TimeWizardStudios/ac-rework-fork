init python:
  class Item_worms(Item):

    icon = "items worms"

    def title(item):
      return "Worms"

    def description(item):
      return "Nature's potato chip."

    def actions(cls,actions):
      actions.append("?worms_interact")


label worms_interact(item):
  "Here's hoping we can catch a sweet, juicy fish with these!"
  return True
