init python:
  class Item_third_note_from_mrsl(Item):

    icon = "items note_from_mrsl"

    def title(item):
      return "Third Note from " + mrsl.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?third_note_from_mrsl_interact")

  add_recipe("third_note_from_mrsl","pen","quest_mrsl_bot_back_and_forth_third_note_from_mrsl_pen_combine")
  add_recipe("third_note_from_mrsl","nurse_pen","quest_mrsl_bot_back_and_forth_third_note_from_mrsl_pen_combine")


label third_note_from_mrsl_interact(item):
  "{i}\"It appears your little box is quite the hit.\"{/}"
  "{i}\"Not quite the means of communication for which you were hoping, though.\"{/}"
  "{i}\"No toasted buns after all, it seems.\"{/}"
  "..."
  "Damn it! I'm losing her!"
  "I need to reel her back in..."
  $quest.mrsl_bot["third_note_from_mrsl_interacted_with"] = True
  return True
