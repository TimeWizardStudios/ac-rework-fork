init python:
  class Item_isabelle_hair(Item):

    icon = "items isabelle_hair"

    def title(item):
      return isabelle.name + "'s Hair"

    def description(item):
#     return "Smells like cinnamon and degradation."
      return "Smells like cinnamon\nand degradation."

    def actions(cls,actions):
      actions.append("?isabelle_hair_interact")


label isabelle_hair_interact(item):
  "This strand contains enough DNA to create an [isabelle] clone."
  "That's plan B."
  return True
