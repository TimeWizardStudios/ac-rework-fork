init python:
  class Item_bag_of_weed(Item):

    icon="items bag_of_weed"

    def title(item):
      return "Bag of Weed"

    def description(item):
      return "Sex, drugs, and rock 'n' roll — one out of three is a good start."

    def actions(cls,actions):
      if home_hall["hole_bag"]:
        actions.append("?bag_of_weed_interact")
      else:
        actions.append("?bag_of_weed_interact2")


label bag_of_weed_interact(item):
  "[jo] used to smoke weed to reduce stress."
  "Not sure why she'd keep it hidden up here, though..."
  "Both [flora] and I knew about it."
  return True

label bag_of_weed_interact2(item):
  "Weeds pulled out of the neighbor's lawn. Classic black market produce."
  return True
