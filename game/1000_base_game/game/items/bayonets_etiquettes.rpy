init python:
  class Item_bayonets_etiquettes(Item):
    icon="items book bayonets_etiquettes"
    retain_capital = True
    def title(item):
      return "\"Bayonets & Etiquettes\""
   
    def description(item):
      return "Polish your manners like you'd polish your blade — with grease and good posture." 
    
    def actions(cls,actions):
      actions.append("?int_bayonets_etiquettes")
      
label int_bayonets_etiquettes(item):
  if not quest.isabelle_haggis["got_wrench"]:
    "Oh, the middle pages of the book have been cut out to form a secret compartment."
    "Whoever did this probably thought no one would be bored enough to read through the chapter called \"Burette & Pincette: the Famous French Lab-Cooked Omelette.\""
    "Well, that chapter would certainly have been useful, but maybe this will be too?"
    $mc.add_item("monkey_wrench")
    $quest.isabelle_haggis["got_wrench"] = True
  else:
    "{i}\"Bayonets, Etiquette, and Other Things the French Made Dull, Vol. 983\"{/}"
    "The French tracked down the author of this book, but couldn't stab him to death. Their stilettos weren't sharp enough."
    "He arrived at the hospital with minor bruising and a smell of freshly baked baguettes."
  return True
      
      
