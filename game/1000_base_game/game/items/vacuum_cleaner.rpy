init python:
  class Item_vacuum_cleaner(Item):

    icon = "items vacuum_cleaner"

    def title(item):
      return "Vacuum Cleaner"

    def description(item):
      return "Few women can compete with the unyielding suction of this fine lady.\n\nAnd that was before I added\nthe turbo."

    def actions(cls,actions):
      actions.append("?vacuum_cleaner_interact")

label vacuum_cleaner_interact(item):
  "[jo] always gets suspicious when I start vacuuming in the middle of the night."
  "She doesn't believe me when I tell her that things get quite dirty in here.{space=-80}"
  return True
