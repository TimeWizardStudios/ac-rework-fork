init python:
  class Item_pen(Item):

    icon = "items ballpen"

    def title(item):
      return "Pen"

    def description(item):
      if game.season == 1:
        # return "Those that say that the pen is mightier than the sword have never run out of ink."
        return "Those that say that the pen\nis mightier than the sword have\nnever run out of ink."
      elif game.season == 2:
        # return "You're looking a little pent up... Time to release some ink."
        return "You're looking a little {i}pent{/} up...\n\nTime to release some ink."

    def actions(cls,actions):
      if game.season == 1:
        actions.append("?int_pen")
      elif game.season == 2:
        actions.append("?quest_mrsl_bot_message_box_pen")

  add_recipe("paper","pen","quest_mrsl_bot_message_box")
  add_recipe("paper","nurse_pen","quest_mrsl_bot_message_box")


label int_pen(item):
  "This pen smells like [isabelle]... mmmm..."
  if quest.nurse_aid["name_reveal"]:
    "Shit, I better put it away before it disappears up my nose and I end up in the nurse's office."
  else:
    "Shit, I better put it away before it disappears up my nose and I end up in the [nurse]'s office."
  "That sometimes happens."
  return True
