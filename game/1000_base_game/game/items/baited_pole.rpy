init python:
  class Item_baited_pole(Item):

    icon = "items baited_pole"

    def title(item):
      return "Baited Pole"

    def description(item):
      # return "Living life as a worm would be so simple..."
      return "Living life as a worm\nwould be so simple..."

    def actions(cls,actions):
      actions.append("?baited_pole_interact")

  add_recipe("fishing_pole","worms","quest_nurse_aid_bait")


label baited_pole_interact(item):
  "The rod is ready. I just need to get it wet."
  return True
