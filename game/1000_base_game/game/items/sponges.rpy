init python:
  class Item_sponges(Item):

    icon = "items sponges"

    def title(item):
      return "Sponges"

    def description(item):
      return "No one likes a suck-up,\nexcept in certain very specific\nand wet cases."

    def actions(cls,actions):
      actions.append("?sponges_interact")


label sponges_interact(item):
  "I was like this sponge that day — just soaking it in."
  return True
