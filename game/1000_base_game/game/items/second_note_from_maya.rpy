init python:
  class Item_second_note_from_maya(Item):

    icon = "items note_from_maya"

    def title(item):
      return "Second Note from " + maya.name

    def description(item):
      # return "It's in rare times like these when a good handwriting is more important than a good handjob."
      return "It's in rare times like these\nwhen a good handwriting is more important than a good handjob."

    def actions(cls,actions):
      actions.append("?second_note_from_maya_interact")


label second_note_from_maya_interact(item):
  "{i}\"Okay, I've been looking for my yarn since the first day of school.\"{/}"
  "{i}\"And don't think I don't know my own yarn.\"{/}"
  "{i}\"I'm not sure where you found it, or if you're the thief that stole it from me, but... thank you!\"{/}"
  "{i}\"This yarn is special to me. I've had it since I was a little girl.\"{/}"
  "{i}\"So, I guess I owe you a blowjob now or whatever...\"{/}"
  "{i}\"...unless you stole it, in which case, you owe me one.\"{/}"
  if not quest.mrsl_bot["second_note_from_maya_interacted_with"]:
    $maya.love+=3
    $quest.mrsl_bot["second_note_from_maya_interacted_with"] = True
  return True
