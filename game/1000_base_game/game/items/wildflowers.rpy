init python:
  class Item_wildflowers(Item):

    icon = "items wildflowers"

    def title(item):
      return "Wildflowers"

    def description(item):
      # return "These smell so good. Almost as good as [isabelle]."
      return "These smell so good.\nAlmost as good as [isabelle]."

    def actions(cls,actions):
      actions.append("?wildflowers_interact")


label wildflowers_interact(item):
  "I need to get these to [isabelle] quickly so that she can put them in some water..."
  "...and also forgive me."
  return True
