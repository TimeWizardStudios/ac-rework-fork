init python:
  class Item_car_wash_sign(Item):

    icon = "items car_wash_sign"

    def title(item):
      return "Car Wash Sign"

    def description(item):
      return "This was a sign that things\nwere about to get good."

    def actions(cls,actions):
      actions.append("?car_wash_sign_interact")


label car_wash_sign_interact(item):
  "{i}\"Welcome to Newfall High's car wash fundraiser!\"{/}"
  "{i}\"Roll up your windows and prepare to get wet!\"{/}"
  return True
