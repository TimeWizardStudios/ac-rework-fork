init python:
  class Item_jo_secret_wine(Item):

    icon = "items jo_secret_wine"

    def title(item):
      return jo.name + "'s Secret Wine"

    def description(item):
      return "A full-bodied Chardonnay?\n[jo] has good taste..."

    def actions(cls,actions):
      actions.append("?jo_secret_wine_interact")


label jo_secret_wine_interact(item):
  "If only I was older again, I could've bought this myself."
  return True
