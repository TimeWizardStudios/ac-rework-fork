init python:
  class Item_handcuffs(Item):

    icon = "items handcuffs"

    def title(item):
      return "Handcuffs"

    def description(item):
      return "Stainless steel. Escape proof. [isabelle]'s worst enemy."

    def actions(cls,actions):
      actions.append("?handcuffs_interact")


label handcuffs_interact(item):
  "It's surprising that [kate] doesn't have a pair of her own."
  "Perhaps she just wants me to run errands for her..."
  return True
