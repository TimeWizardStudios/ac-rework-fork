init python:
  class Item_ancient_chastity_belt(Item):

    icon = "items ancient_chastity_belt"

    def title(item):
      return "Ancient Chastity Belt"

    def description(item):
      return "How did a device of purity turn into a device of debauchery?"

    def actions(cls,actions):
      actions.append("?ancient_chastity_belt_interact")


label ancient_chastity_belt_interact(item):
  # "Crafted in some sort of alloy of rust-free metal, this is a masterpiece of sexual submission."
  "Crafted in some sort of alloy of rust-free metal, this is a masterpiece{space=-20}\nof sexual submission."
  return True
