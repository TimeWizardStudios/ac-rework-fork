init python:
  class Item_old_artwork(Item):
    
    icon="items old_artwork"
    
    def title(item):
      return "Old Artwork"
    
    def description(item):
      return "[jacklyn] was right."
    
    def actions(cls,actions):
      actions.append("?old_artwork_interact")
  
  add_recipe("old_artwork", "beaver_pelt", "poolside_story_making_confetti")
  add_recipe("old_artwork", "beaver_carcass", "poolside_story_making_confetti")
  add_recipe("old_artwork", "glass_shard", "poolside_story_making_confetti_glass_shard")
  add_recipe("old_artwork", "locker_key", "poolside_story_making_confetti_glass_shard")

label old_artwork_interact(item):
  "The linework... the color combinations... the motif!"
  "This is what failure on a canvas looks like."
  return True
