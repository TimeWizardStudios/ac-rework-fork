init python:
  class Item_bottle_of_soap(Item):

    icon = "items bottle_of_soap"

    def title(item):
      return "Bottle of Soap"

    def description(item):
      return "God, I wish I never spoke..."

    def actions(cls,actions):
      actions.append("?bottle_of_soap_interact")


label bottle_of_soap_interact(item):
  "Do soap goblins eat soap, or are they made of it?"
  "I have to ask [maxine] at some point."
  return True
