init python:
  class Item_spray_empty_bottle(Item):
    icon="items bottle spray_empty_bottle"
    
    def title(item):
      return "Empty Bottle"
   
    def description(item):
      return "Just fill her up again — recycling in its purest form."
  
    def actions(cls,actions):
      actions.append("?int_water_bottle")
      
