init python:
  class Item_water_bottle(Item):
    icon="items bottle water_bottle"

    can_place_to_desk = True
    bottled = True
    consumable = True
       
    def title(item):
      return "Water"
   
    def description(item):
      return "Tides go in. Tides go out. Can't explain that! But you could bottle it."
    
    def actions(cls,actions):
      actions.append("?int_water_bottle")
      if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
        actions.append(["consume", "Consume","consume_water_bottle_maxine"])
      else:
        actions.append(["consume", "Consume","consume_water_bottle"])

      
label int_water_bottle(item):
  "A bottle of liquid H2O molecules. Don't need to be a chemist to see the danger in that."
  return True

label consume_water_bottle(item):
  "[flora] always tells me I'm too thirsty."
  "Let's see if this helps."
  $mc.remove_item("water_bottle")
  $mc.add_item("empty_bottle")
  "Nope. Just made it worse."
  return True

label consume_water_bottle_maxine(item):
  $mc.remove_item(item)
  $mc.add_item("empty_bottle")
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my drink?"
  $mc.add_item("letter_from_maxine")
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  return True