init python:
  class Item_spray_strawberry_juice(Item):

    icon="items bottle spray_strawberry_juice"
    consumable = True

    def title(item):
      return "Strawberry Juice"

    def description(item):
      return "The red gold. The nectar of heaven. One of the few truly good things left in this world."

    def actions(cls,actions):
      actions.append("?int_strawberry_juice")
      if quest.maxine_hook == "glade":
        actions.append(["consume", "Consume","quest_maxine_hook_glade"])
      elif quest.isabelle_tour.finished and not quest.nurse_photogenic.started:
        actions.append(["consume","Consume","quest_nurse_photogenic_strawberry_juice"])
      else:
        actions.append(["consume","Consume","consume_spray_strawberry_juice"])


label consume_spray_strawberry_juice(item):
  $mc.remove_item(item)
  $mc.add_item("spray_empty_bottle")
  mc "Ah, the lifeblood courses through my veins once more!"
  return True
