init python:
  class Item_spoonful_of_spice(Item):
    icon="items spoonful_of_spice"
    
    def title(item):
      return "Spoonful of Spice"
    
    def description(item):
      return "Measuring is for beginners. A master chef always goes by taste."
    
    def actions(cls,actions):
      actions.append("?int_spoonful_of_spice")

  add_recipe("spoon","spice","spoonful_of_spice")

label int_spoonful_of_spice(item):
  "Sugar and spice, and everything nice."
  "Where the fuck did I put Chemical X?"
  "Ugh, no chilipuff girls for me."
  return True
