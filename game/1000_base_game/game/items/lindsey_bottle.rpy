init python:
  class Item_lindsey_bottle(Item):

    icon="items lindsey_bottle"

    def title(item):
      return lindsey.name + "'s Bottle"

    def description(item):
      return "Cute and girly, just like [lindsey]. The only thing missing is a bit of lace."

    def actions(cls,actions):
      actions.append("?lindsey_bottle_interact")


label lindsey_bottle_interact(item):
  "Cute girls putting their lips on you."
  "Sucking the liquid right out of your tip."
  "Oh, to be a water bottle."
  return True
