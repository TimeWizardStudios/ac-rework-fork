init python:
  class Item_cardboard(Item):

    icon = "items cardboard"

    def title(item):
      return "Cardboard"

    def description(item):
      # return "Forged in the fires of grease and desperation."
      return "Forged in the fires of grease\nand desperation."

    def actions(cls,actions):
      actions.append("?cardboard_interact")

  add_recipe("cardboard","duct_tape","quest_maxine_dive_armor_cardboard_duct_tape_combine")


label cardboard_interact(item):
  "Light armor has some advantages, too!"
  "Like mobility and, err... yeah, that's probably it."
  return True
