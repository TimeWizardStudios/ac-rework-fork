init python:
  class Item_onion_slice(Item):

    icon="items onion_slice"
    consumable = True
    
    def title(item):
      return "Sliced Onion"

    def description(item):
      return "Sliced to perfection. Possibly the best sliced onion since Napoleon conquered Russia in the great onion war."
   
    def actions(cls,actions):
      actions.append("?int_onion_slice")
      
label int_onion_slice(item):
  "Ahh, my eyes! Onion is the worst! "
  "Probably shouldn't put it in my eyes next time."
  return True
