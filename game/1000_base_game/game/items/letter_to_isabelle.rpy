init python:
  class Item_letter_to_isabelle(Item):

    icon = "items letter_to_isabelle"

    def title(item):
      return "Letter to " + isabelle.name

    def description(item):
      return "I need to lett'er know how I feel."

    def actions(cls,actions):
      actions.append("?letter_to_isabelle_interact")


label letter_to_isabelle_interact(item):
  "{i}\"Dear [isabelle].\"{/}"
  "{i}\"I know that I messed up big time, and that I really hurt you.\"{/}"
  "{i}\"If I could take it back, I would.\"{/}"
  "{i}\"Because hurting you is the last thing I ever want.\"{/}"
  "{i}\"I would always rather make you smile.\"{/}"
  "{i}\"Because your smile is like a fresh rose, touched by a breeze of la belle air, in the sunlight of your garden.\"{/}"
# "{i}\"When I'm near you, I open my mouth, but the words won't come out.\"{/}"
  "{i}\"When I'm near you, I open my mouth, but the words won't come out.\"{/}{space=-45}"
  "{i}\"And I'm choking, how? You probably think I'm joking now.\"{/}"
  "{i}\"So, I will show you and hope that you can find it in your heart to forgive me.\"{/}"
# "{i}\"A heart I know beats for justice, truth, kindness... and forgiveness.\"{/}"
  "{i}\"A heart I know beats for justice, truth, kindness... and forgiveness.\"{/}{space=-15}"
  "{i}\"A heart of which I want to be worthy.\"{/}"
  "{i}\"Sincerely, [mc].\"{/}"
  return True
