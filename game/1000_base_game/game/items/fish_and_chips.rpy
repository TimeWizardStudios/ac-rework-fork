init python:
  class Item_fish_and_chips(Item):

    icon = "items fish_and_chips"

    def title(item):
      return "Fish and Chips"

    def description(item):
      return "Fish and chips — the cornerstone of English cuisine."

    def actions(cls,actions):
      actions.append("?fish_and_chips_interact")


label fish_and_chips_interact(item):
  "Yum! Tasty and healthy."
  "Unless you count the chips, the batter, and the sauce..."
  "..."
  "Okay, fine. Just tasty."
  return True
