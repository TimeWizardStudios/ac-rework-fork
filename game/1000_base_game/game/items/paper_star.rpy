init python:
  class Item_paper_star_blue(Item):

    icon = "items paper_star_blue"

    def title(item):
      return "Blue Star"

    def description(item):
      return "A paper star with a message scribbled on the back."

    def actions(cls,actions):
      actions.append("?paper_star_blue_interact")


label paper_star_blue_interact(item):
  "{i}\"I'm big and I'm long.\"{/}"
  "{i}\"I let you take a ride on me.\"{/}"
  "{i}\"I take you along...\"{/}"
  "{i}\"To the place you least want to be.\"{/}"
  $quest.kate_desire["blue_star_interacted"] = True
  return True


init python:
  class Item_paper_star_red(Item):

    icon = "items paper_star_red"

    def title(item):
      return "Red Star"

    def description(item):
      return "A paper star with a message scribbled on the back."

    def actions(cls,actions):
      actions.append("?paper_star_red_interact")


label paper_star_red_interact(item):
  "{i}\"Touch me once, I'll make a sound.\"{/}"
  "{i}\"Push my buttons. I don't mind.\"{/}"
  "{i}\"Finger me. Oh, do it right.\"{/}"
  "{i}\"I'll sing for you. I'll sing all night.\"{/}"
  $quest.kate_desire["red_star_interacted"] = True
  return True


init python:
  class Item_paper_star_green(Item):

    icon = "items paper_star_green"

    def title(item):
      return "Green Star"

    def description(item):
      return "A paper star with a message scribbled on the back."

    def actions(cls,actions):
      actions.append("?paper_star_green_interact")


label paper_star_green_interact(item):
  "{i}\"I'm hard.\"{/}"
  "{i}\"I'm strong.\"{/}"
  "{i}\"I'm inside you all life long.\"{/}"
  $quest.kate_desire["green_star_interacted"] = True
  return True


init python:
  class Item_paper_star_black(Item):

    icon = "items paper_star_black"

    def title(item):
      return "Black Star"

    def description(item):
      return "A paper star with a message scribbled on the back."

    def actions(cls,actions):
      actions.append("?paper_star_black_interact")


label paper_star_black_interact(item):
  "{i}\"I'm where no man dares to tread.\"{/}"
  "{i}\"A land of bowls and pipes of lead.\"{/}"
  $quest.kate_desire["black_star_interacted"] = True
  return True


init python:
  class Item_paper_star_gold(Item):

    icon = "items paper_star_gold"

    def title(item):
      return "Gold Star"

    def description(item):
      return "A paper star with a message scribbled on the back."

    def actions(cls,actions):
      actions.append("?paper_star_gold_interact")


label paper_star_gold_interact(item):
  "{i}\"To the victor goes the spoils.\"{/}"
  return True
