init python:
  class Item_milk(Item):
    
    icon="items bottle milk"
    
    can_place_to_desk = True
    bottled = True
    consumable = True
    
    def title(item):
      return "Milk"
    
    def description(item):
      return "Milk from the cow. Milk from the bull. Is there really a difference?"
    
    def actions(cls,actions):
      actions.append("?int_milk")
      #actions.append("consume","Consume","consume_milk")#can't consume
      
label int_milk(item):
  "Tasty and healthy! At least, that's what it used to be. Who knows anymore?"
  return True
  
