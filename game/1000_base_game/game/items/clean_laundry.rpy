init python:
  class Item_clean_laundry(Item):
    icon="items nurse_panties"
   
    def title(item):
      return "Clean Laundry"
   
    def description(item):
      return "Lace, silk, and Component X... which is sex appeal. Lots of Component X here."
    
    def actions(cls,actions):
      actions.append("?int_clean_laundry")
      
label int_clean_laundry(item):
  "Spring flowers and flowing meadows. Somehow, the dirty laundry smelled better."
  return True
