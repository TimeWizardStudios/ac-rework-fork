init python:
  class Item_isabelle_box(Item):

    icon="items isabelle_box"

    def title(item):
      return isabelle.name + "'s Box"

    def description(item):
      return "An empty heart. Feels familiar somehow."

    def actions(cls,actions):
      actions.append("?isabelle_box_interact")


label isabelle_box_interact(item):
  "Hey!"
  "Wait!"
  "I've got a new complaint."
  "Yeah, you guessed it. It's out of chocolate."
  return True
