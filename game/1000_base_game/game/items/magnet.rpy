init python:
  class Item_magnet(Item):

    icon = "items magnet"

    def title(item):
      return "Magnet"

    def description(item):
      return "The repelling side seems to be working. The girls are definitely keeping their distance."

    def actions(cls,actions):
      actions.append("?magnet_interact")

label magnet_interact(item):
  "They say that opposites attract — that'd be my only way of getting with a hot babe."
  return True
