init python:
  class Item_grappling_hook(Item):
    icon="items grappling_hook"
    
    def title(item):
      return "Grappling Hook"
    
    def description(item):
      return "Throw. Pull. Climb. The simple way of committing crimes."
    
    def actions(cls,actions):
      actions.append("?int_grappling_hook")
      
label int_grappling_hook(item):
  "Unimaginable heights could be achieved with this thing."
  "Well, at least heights up to ten feet."
  return True
      
      
