init python:
  class Item_flora_bracelet(Item):

    icon="items flora_bracelet"

    def title(item):
      return flora.name + "'s Bracelet"

    def description(item):
      return "Probably the first thing that [flora] has ever given to me that holds any sort of sentimental value to her. A big step in the right direction."

    def actions(cls,actions):
      actions.append("?int_flora_bracelet")


label int_flora_bracelet(item):
  "[flora]'s friendship bracelet. She's worn it for years. It still smells of her strawberry perfume and sass."
  #tbd If Flora's Bracelet = Inventory: +2 LoveMom (temporary)    
  return True
