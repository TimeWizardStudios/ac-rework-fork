init python:
  class Item_apple(Item):
    
    icon="items apple"
    
    def title(item):
      return "Apple"
    
    def description(item):
      return "The biggest problem with an apple is that it doesn't go very well with other things. Some might call that a design flaw; others, a business model."
    
    def actions(cls,actions):
      actions.append("?apple_interact")
     
label apple_interact(item):
  "Earbuds not included. Charger not included. Battery not included."
  "Goes bad after a short while."
  "In other words, a classic apple."
  "Luckily, you can always get a new and fresh one."
  return True
