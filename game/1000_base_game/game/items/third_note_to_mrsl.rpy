init python:
  class Item_third_note_to_mrsl(Item):

    icon = "items second_note_to_mrsl"

    def title(item):
      return "Third Note to " + mrsl.name

    def description(item):
      # return "Not exactly a message in a bottle... but desperate times, and all that jazz."
      return "Not exactly a message in a\nbottle... but desperate times,\nand all that jazz."

    def actions(cls,actions):
      actions.append("?third_note_to_mrsl_interact")


label third_note_to_mrsl_interact(item):
  "{i}\"I am really enjoying talking to you.\"{/}"
  "{i}\"You push me to be better.\"{/}"
  return True
