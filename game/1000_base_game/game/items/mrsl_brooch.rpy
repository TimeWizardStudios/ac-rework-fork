init python:
  class Item_mrsl_brooch(Item):

    icon="items mrsl_brooch"
    tiertwo = True    

    def title(item):
      return "Mrs. Lichtenstein's Brooch"

    def description(item):
      return "A brooch of dubious origin. Possibly from a strip club or cult."

    def actions(cls,actions):
      actions.append("?int_mrsl_brooch")


label int_mrsl_brooch(item):
  "Feels hot to the touch, as if it's pulsating with a tiny heart of its own."
  return True
