init python:
  class Item_spiked_ice_tea(Item):

    icon = "items spiked_ice_tea"

    def title(item):
      return "Spiked Ice Tea"

    def description(item):
      return "A sleeping beauty DIY kit."

    def actions(cls,actions):
      actions.append("?spiked_ice_tea_interact")


label spiked_ice_tea_interact(item):
  "A single drop is enough to put ten ants to sleep, and the leg of an elephant."
  return True
