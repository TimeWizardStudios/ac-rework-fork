init python:
  class Item_kate_socks_dirty(Item):

    icon = "items kate_socks_dirty"

    def title(item):
      return kate.name + "'s Dirty Socks"

    def description(item):
      return "Despite being in my mouth the whole night, they still smell vaguely of [kate]'s feet..."

    def actions(cls,actions):
      actions.append("?kate_socks_dirty_interact")


label kate_socks_dirty_interact(item):
  "A true beta would clean them and return them."
  "Humiliating, but [kate] would like that."
  return True
