init python:
  class Item_hotdog(Item):

    icon = "items hotdog"

    def title(item):
      return "Hotdog"

    def description(item):
      return "Cheap, tasty, and most importantly, cheap."

    def actions(cls,actions):
      actions.append("?hotdog_interact")


label hotdog_interact(item):
  "Deepthroat contest, anyone?"
  return True
