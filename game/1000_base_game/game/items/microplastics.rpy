init python:
  class Item_microplastics(Item):

    icon = "items book microplastics"
    retain_capital = True

    def title(item):
      return "\"Microplastics: How Ocean Pollution\nAffects Cthulhu\" by Luna Lovecraft"

    def description(item):
      return "One of [flora]'s favorite books. Not so strange when you think about it."

    def actions(cls,actions):
      actions.append("?microplastics_interact")


label microplastics_interact(item):
  "{i}\"That is not biodegradable which can eternal lie.\"{/}"
  "{i}\"And with polluted oceans, even death may die.\"{/}"
  return True
