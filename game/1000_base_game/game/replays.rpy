﻿init python:
  replays_by_order=[]

  def register_replay(replay_id,replay_title,replay_icon,replay_label):
    replays_by_order.append((replay_id,replay_title,replay_icon,replay_label))

  def is_replay_unlocked(replay_id):
    unlocked_replays=persistent.unlocked_replays
    if isinstance(unlocked_replays,set):
      return replay_id in unlocked_replays
    return False

  def unlock_replay(replay_id):
    unlocked_replays=persistent.unlocked_replays
    if not isinstance(unlocked_replays,set):
      unlocked_replays=set()
    unlocked_replays.add(replay_id)
    persistent.unlocked_replays=unlocked_replays

  def lock_replay(replay_id):
    unlocked_replays=persistent.unlocked_replays
    #if not isinstance(unlocked_replays,set):
    #  unlocked_replays=set()
    unlocked_replays.remove(replay_id)
    persistent.unlocked_replays=unlocked_replays



  class Event_phoneapp_replay(GameEvent):
    def on_after_load(event):
      if not mc.phone_app("replay"):
        mc.install_phone_app("replay",silent=True)
