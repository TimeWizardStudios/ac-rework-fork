init python:
  register_replay("isabelle_blackmail","Isabelle's Blackmail","replays isabelle_blackmail","replay_isabelle_blackmail")

label replay_isabelle_blackmail:
  show isabelle stream_cleanup with fadehold
# "She wades into the oil slicked water with the filters, placing them up and down the length of the stream."
  "She wades into the oil slicked water with the filters, placing them up{space=-5}\nand down the length of the stream."
  flora "That's it! Keep it moving!"
  mc "Yeah! Make sure you get all of the trash when you're finished there!"
  isabelle stream_cleanup "O-okay..."
  "Man, there's something about watching others work."
  "Especially if it's a hot girl that's usually so prim and proper."
  "Usually so clean and pristine."
  "..."
  "I do kind of feel bad for not helping her, though."
  "Especially since it's my fault the stream looks like this..."
# "Unfortunately for [isabelle], to see her get down and dirty, in the most literal sense of the word, it does something to me."
  "Unfortunately for [isabelle], to see her get down and dirty, in the most{space=-15}\nliteral sense of the word, it does something to me."
  "And [flora] watches over her like some sort of stern supervisor."
  "I didn't know that she had a domineering side to her, but I guess it makes sense."
  "If you don't push back at her, she'll do whatever she wants."
  "And [isabelle] definitely doesn't push back."
  "She just cleans dutifully. Sweat pours down her brow and back."
  "Shadows of strain fall across her face, but in her eyes there's something else."
  "A smoldering flame."
# "Perhaps a sign of determination, or maybe... something else entirely."
  "Perhaps a sign of determination, or maybe... something else entirely.{space=-10}"
# "She cleans and cleans until the water starts to run clear and the sun begins to set."
  "She cleans and cleans until the water starts to run clear and the sun{space=-10}\nbegins to set."
  "Naked the whole time as the sludgy oil water runs off of her."
  scene location with fadehold
  return
