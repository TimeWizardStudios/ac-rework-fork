init python:
  register_replay("lindsey_hypno_somnambulism","Lindsey's Hypno-somnambulism","replays lindsey_hypno_somnambulism","replay_lindsey_hypno_somnambulism")

label replay_lindsey_hypno_somnambulism:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show lindsey painting_date_laughing with fadehold
  lindsey painting_date_laughing "Finally, right? I can't believe a beaver stole the easel you made for me!{space=-45}"
  mc "Neither can I..."
  lindsey painting_date_smile "Well, these are a bit sturdier. I don't think we'll have that problem again."
  lindsey painting_date_smile "Anyway, what happened to that beaver?"
  "Crap. That's not the question I was hoping for."
  menu(side="middle"):
    extend ""
    "\"It chewed up the easel, but eventually escaped into the forest.\"":
      mc "It chewed up the easel, but eventually escaped into the forest."
      lindsey painting_date_laughing "Well, it'll make for a great story for many years to come."
      "As long as she doesn't learn what really happened..."
      mc "Yeah, it was definitely stranger than fiction."
    "\"I drowned it in the aquarium.\"":
      mc "I drowned it in the aquarium."
      lindsey painting_date_laughing "You're joking!"
      mc "I tracked the beast down and fought it at the top of the mountain."
      lindsey painting_date_smile "Did you, now?"
      mc "Yeah, it was a fierce battle... vile fangs gnashing against steel and courage..."
      mc "The thought of you is what kept me going in my darkest hour."
      mc "Like a patron saint of light and hope, you scattered darkness and sharpened my blade."
      mc "And with your name on my lips, I vanquished my foe."
      lindsey painting_date_blush "That is quite the story!"
      lindsey painting_date_blush "But what about the aquarium?"
      mc "Oh, uh. That's what I call my sword."
      lindsey painting_date_laughing "Oh, really? You'll have to show it to me some time!"
      mc "You want to see my sword?"
      lindsey painting_date_pouting "Hey!"
      mc "What?"
      lindsey painting_date_pouting "You made a face."
      mc "I'm sorry, this is just how I look."
      lindsey painting_date_laughing "You know what you did."
      mc "Nothing wrong with liking swords."
      lindsey painting_date_pouting "There it is again!"
      mc "No, this is just a paint brush. My sword is way bigger."
      lindsey painting_date_laughing "Stop it!"
      mc "Heh, fine."
    "\"Let's not dwell on the past.\"":
      mc "Let's not dwell on the past."
      lindsey painting_date_laughing "You're probably right."
      lindsey painting_date_smile "I always look to the future. I have so many plans."
      mc "You're going to go far."
      mc "Scholarship at some prestigious college. Taking the athlete world by storm."
      mc "National championships. The Olympic Games. Fame and fortune!"
      lindsey painting_date_blush "Aw! You're too nice!"
      mc "I'm also right."
  lindsey painting_date_smile "So, what are you going to paint?"
  mc "Oh, I don't know... maybe you?"
  lindsey painting_date_blush "Me?"
  mc "Yeah, I've been looking for a motif since we got here, but none of the flowers here can compare to you."
  lindsey painting_date_laughing "You total cheeseball!"
  mc "I mean it!"
  show lindsey painting_date_blush with dissolve2
  mc "I haven't felt this way before. Whenever I'm with you, I feel so light and happy."
  mc "I really like you, [lindsey]."
  lindsey painting_date_blank "..."
  mc "[lindsey]?"
  lindsey painting_date_blank "What...?"
  mc "Are you okay?"
  lindsey painting_date_blank "Um..."
  "What's happening?"
  lindsey painting_date_blank "..."
  "Is this the hypno-somnambulism that [maxine] talked about?"
  "Man, I really need to find a way to detox her somehow..."
  "...or maybe I should try giving her a suggestion?"
  mc "[lindsey]..."
  menu(side="middle"):
    extend ""
    "\"Show me your boobs.\"":
      mc "Show me your boobs."
      lindsey painting_date_blank "Um... okay..."
      window hide
      show lindsey painting_date_boobs_blank with Dissolve(.5)
      window auto
      mc "Very nice, [lindsey]!"
      "Hold up, what if she suddenly wakes up?"
      "Uhhh..."
      mc "[lindsey], jump in the water!"
      lindsey painting_date_boobs_blank "Okay..."
      window hide
      play sound "<from 0.25>water_splash2" volume 0.5
      show lindsey painting_date_water with vpunch
      window auto
      "Oh, wow! She jumped right in, no questions asked!"
      lindsey painting_date_water "..."
      mc "Err, you can come out of the water again!"
      lindsey painting_date_water "Okay..."
      window hide
      show lindsey painting_date_boobs_wet_blank with Dissolve(.5)
      window auto
      "Perfect! This way, I can just tell her she fell into the pond if she suddenly wakes up."
      "Hmm... what next? So many possibilities..."
      mc "[lindsey]..."
      menu(side="middle"):
        extend ""
        "\"Bark like a dog.\"":
          mc "Bark like a dog."
          lindsey painting_date_boobs_wet_blank "Woof. Woof."
          mc "Good girl!"
          "That's oddly hot..."
          mc "Moo like a cow."
          lindsey painting_date_boobs_wet_blank "Moo."
          mc "Again!"
          lindsey painting_date_boobs_wet_blank "Moo."
          "Okay, I should probably get the [nurse] now."
          mc "Wait here, [lindsey]."
        "\"Give me the money in your wallet.\"":
          mc "Give me the money in your wallet."
          lindsey painting_date_boobs_wet_blank "Okay..."
          window hide
          show lindsey painting_date_boobs_wet_armless_blank with Dissolve(.5)
          pause 0.25
          show lindsey painting_date_boobs_wet_wallet_blank with Dissolve(.5)
          window auto
          lindsey painting_date_boobs_wet_wallet_blank "..."
          "This feels so wrong, but I really need the money."
          "She'll be fine, though. She has like eleven scholarships to choose from next year."
          "Damn, she's loaded!"
          "..."
          "Was loaded."
          "Okay, I should probably get the [nurse] now."
          mc "Wait here, [lindsey]."
        "\"Take your skirt and panties off.\"":
          mc "Take your skirt and panties off."
          lindsey painting_date_boobs_wet_blank "Okay..."
          window hide
          pause 0.25
          $lindsey.unequip("lindsey_skirt")
          pause 1.0
          $lindsey.unequip("lindsey_panties")
          pause 0.75
          window auto
          "Now, that's what I'm talking about!"
          "God, I love her tan lines..."
          "Most girls try to get rid of them, but [lindsey] is a little worker bee."
          "Always running in the sun."
          "And the skin of her tits and pussy is so creamy and white."
          "I bet it's the softest part of her."
          "Fuck..."
          mc "Say, \"Fondle me, daddy!\""
          lindsey painting_date_boobs_wet_blank "Fondle me, daddy..."
          "Well, the monotony in her voice kind of ruins it..."
          "Still, it was worth a shot."
          "Okay, I should probably get the [nurse] now."
          mc "Wait here, [lindsey]."
        "\"Wait here while I get help.\"":
          mc "Wait here while I get help."
    "\"Tell me your darkest secret.\"":
      mc "Tell me your darkest secret."
      lindsey painting_date_blank "I once cheated on a science test..."
      lindsey painting_date_blank "I sometimes still have nightmares about it..."
      "That's it? God, she's such a perfect princess."
      "Hmm..."
      mc "Tell me your darkest sexual fantasy."
      lindsey painting_date_blank "I'm on my knees in a rusty cage..."
      lindsey painting_date_blank "My dress is torn and ragged..."
      "Jesus Christ."
      lindsey painting_date_blank "Darkness lies thick..."
      lindsey painting_date_blank "But something is moving out of sight..."
      lindsey painting_date_blank "The sound of steel plates clinking and rustling over the rock floor..."
      lindsey painting_date_blank "A massive eye opens... glowing like a furnace... watching me... amused at my cries for help..."
      "Man, this is some dark shit. I had no idea [lindsey] was into horror porn."
      lindsey painting_date_blank "But then, a beam of silvery light shines into the massive cave, and a knight steps forth..."
      lindsey painting_date_blank "Flames burn in the reflections of his armor and shield..."
      lindsey painting_date_blank "His sword sings as he pulls it from his scabbard..."
      "Ah, of course. I should've seen this coming."
      lindsey painting_date_blank "The dragon roars and flaps its giant wings... jagged teeth... torrents of liquid fire—"
      mc "Yeah, and then the knight chops off the head of the dragon and saves you. Great stuff, [lindsey]."
      lindsey painting_date_blank "..."
      "I should probably get the [nurse]."
      mc "Wait here, [lindsey]."
    "\"Wait here while I get help.\"":
      mc "Wait here while I get help."
  mc "I'll be right back."
  if renpy.showing("lindsey painting_date_blank"):
    lindsey painting_date_blank "..."
  elif renpy.showing("lindsey painting_date_boobs_wet_wallet_blank"):
    lindsey painting_date_boobs_wet_wallet_blank "..."
  else:
    lindsey painting_date_boobs_wet_blank "..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Okay, this date didn't quite turn out the way I had planned—"
  play sound "<from 0.25>water_splash2" volume 0.25
  with vpunch
  "Huh?"
  mc "[lindsey]?"
  pause 0.25
  show lindsey painting_date_float
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Oh, god. What is she doing?"
  mc "[lindsey], are you okay?"
  window hide
  show lindsey painting_date_sink with Dissolve(.5)
  window auto
  "Oh, fuck. She disappeared below the surface. That's not good."
  "Man, I'm really not cut out to be a hero..."
  mc "[lindsey]!!!"
  "..."
  "Fuck it! I'm going in!"
  window hide
  play sound "<from 0.25>water_splash2"
  show black onlayer screens zorder 100 with vpunch
  pause 0.25
  play ambient "audio/sound/water_drown.ogg" volume 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Oh, god! She's at the bottom already..."
  "..."
  "Ugh, I should have paid more attention to the swim instructor in kindergarten..."
  "Come on, [lindsey]..."
  stop ambient
  pause 0.25
  show lindsey making_out_limp
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "[lindsey]!"
  lindsey making_out_limp "..."
  mc "Come on, wake up!"
  lindsey making_out_limp "..."
  mc "Wake up, [lindsey]!"
  lindsey making_out_dazed "Mmm..."
  mc "Oh, thank god! You're alive!"
  lindsey making_out_surprised "W-what happened...?"
  mc "You fell into the pond..."
  lindsey making_out_surprised "Last thing I remember I was painting..."
  lindsey making_out_surprised "How did I end up in the water? And where did my clothes go?"
  mc "Err... I don't know, but I'm glad you're okay."
  lindsey making_out_surprised "Did you also fall in?"
  mc "Not exactly. Someone had to pull you out."
  lindsey making_out_adore "You saved me?"
  mc "I guess I did..."
  lindsey making_out_adore "You're like my guardian angel!"
  mc "I mean, I wouldn't' go that far..."
  lindsey making_out_adore "It feels like you're always there!"
  lindsey making_out_adore "You carried me to the [nurse] when I crashed into you..."
  lindsey making_out_adore "You caught me when I was about to fall down the stairs..."
  lindsey making_out_adore "You helped me get dry clothes..."
  mc "I guess—"
  lindsey making_out_adore "I don't."
  window hide
  show lindsey making_out_blush with Dissolve(.5)
  window auto
  mc "!!!"
  "Whenever our lips meet, it's like an electrical current surges through me."
  "It's like getting a cold glass of water after walking through the desert."
  "Like jumping into a rejuvenating oasis."
  "Hopelessness, fear, self-doubt melt away."
  "And for a moment, her energy is mine. Her excitement, strength, and determination — a rush of life from her lips to mine."
  "It's one of those things that never gets old..."
  window hide
  show lindsey making_out_workaround_hand_adore with Dissolve(.5)
  window auto
  lindsey making_out_workaround_hand_adore "In fact, I'm quite sure."
  lindsey making_out_workaround_hand_adore "Kiss me again."
  mc "You don't have to say that twice."
  window hide
  show lindsey making_out_blush with Dissolve(.5)
  window auto
  "I read somewhere that human lips have over a million nerve endings..."
  "...and whenever I kiss [lindsey], they all fire at once."
  "A million volts, right into my lips. A million stars, lighting up my brain."
  "To think that [lindsey] called me her guardian angel... maybe I'm not as useless as I thought."
  "But was it really heroics and bravado that brought me here?"
  "Maybe I'm the villain of the story..."
  window hide
  show lindsey making_out_boob_blush with Dissolve(.5)
  show lindsey making_out_boob_surprised2 with Dissolve(.5)
  window auto
  lindsey making_out_boob_surprised2 "Hey!" with vpunch
  mc "Sorry, um..."
  lindsey making_out_boob_blush "No, it's okay..."
  lindsey making_out_boob_blush "I don't mind. I was just surprised."
  "Whoa! I wasn't expecting that!"
  window hide
  show lindsey making_out_boob_hand_blush with Dissolve(.5)
  window auto
  lindsey making_out_boob_hand_blush "Mmmm..."
  "Her lips and mouth taste like cotton candy."
  "Very wet cotton candy."
  "Making out is a lot sloppier than I thought... but it's sloppy in a good way."
  "It's nothing like those hentai clips where strings of saliva stretch like telephone lines between the mouths of the kissers."
  "In fact, kissing [lindsey] is nothing like porn."
  "It's sweet and gentle, and her playful tongue engages mine in a shy game of peekaboo."
  "Her stiff nipple pokes at my palm as I squeeze her breast."
  "I can't believe she's allowing me to grab her like this. To feel her up completely."
  "But her muffled moaning says it all."
  menu(side="middle"):
    extend ""
    "Move your hand lower":
      lindsey making_out_nervous "N-no..."
      "Crap. She didn't like that."
      mc "I thought, um..."
      lindsey making_out_nervous "I can't..."
      mc "Why?"
      lindsey making_out_nervous "It's too soon..."
      mc "I thought you liked it..."
      lindsey making_out_nervous "I did... but I'm saving myself for marriage..."
      mc "Oh, sorry..."
      mc "That's not what I had in mind, I was just..."
      lindsey making_out_nervous "It's fine. Can you let me up, please?"
      lindsey making_out_nervous "I think it's time to go back."
      "God, why did I have to be greedy?"
      "I always fuck these things up..."
      "[lindsey] looked like she enjoyed it, but I should've known better."
      "She's a princess, and if I want to have a real chance with her, I need to start treating her as one."
    "Don't push your luck":
      lindsey making_out_adore2 "I like you, [mc]."
      "The adoration in her eyes is like a healing balm for my wretched soul."
      "Having someone like [lindsey] look at me like that..."
      "Like I'm actually worth something."
      "Maybe even a lot."
      "Well, nothing can really compare."
      "Even with my hand on her breast and my mouth sweet with her taste, it's the validation that sets me free."
      mc "I like you too, [lindsey]."
      window hide
      show lindsey making_out_boob_hand_blush with Dissolve(.5)
      window auto
      lindsey making_out_boob_hand_blush "Mmm..."
      "I could stay like this forever..."
      "Dream my life away with her."
      "But the truth is, [lindsey] has a life to get back to."
      "But maybe that's not the worst thing..."
      "I could see myself following her to running events all over the world."
      "I'm sure Chad couldn't stand not being the number one in the relationship."
      "But I don't mind being the supporting boyfriend if that's what it takes to win her heart."
      "I would make sure she'd be the best runner in the world."
      "I would be her fan, her lover, and her guardian angel."
      "Nothing bad would ever happen to her."
      "But for now, I'll just enjoy the moment..."
  scene location with fadehold
  $lindsey.equip("lindsey_panties")
  $lindsey.equip("lindsey_skirt")

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
