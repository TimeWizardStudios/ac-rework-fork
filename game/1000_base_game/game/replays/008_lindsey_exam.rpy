init python:
  register_replay("lindsey_exam","Lindsey's Exam","replays lindsey_exam","replay_lindsey_exam")

label replay_lindsey_exam:
  scene nurse clinic_nurse_confused 
  show lindsey clinic_lindsey_neutral 
  with Dissolve(.5)
  nurse "Okay, let's get that bra off, so I can listen to your heart."
  show lindsey clinic_lindsey_neutral_topless with Dissolve(.75)
  lindsey "Okay."
  show nurse clinic_nurse_surprised with Dissolve(.25)
  nurse "Huh?"
  show lindsey clinic_lindsey_scared_topless with Dissolve(.75)
  lindsey "Aaah!"
  "Some prophets may call this a heavy risk, but there it finally is — the prize."
  "People would kill to see her tits. There's a big chance that no man has ever seen her naked."
  "That's what makes this so special."
  "Judging by the tan lines, even the sun's been deprived of the soft flesh of her chest."
  "The unblemished snow-white skin. A sharp but enticing contrast to the soft pink of her areolas."
  "Some girls sunbathe to get an even tan, but she clearly doesn't care about that."
  "As long as her sports bra keeps her boobs from bouncing."
  "If only she'd allow me to reach out, weigh them in my hands, squeeze them gently..."
  "Or roughly... whatever, she prefers! Twist her nipples... or give them kind and gentle kisses."
  "There are so many possibilities! Play with them. Nibble them. Suck them."
  "So soft and supple — so forbidden — joy-sized perfection!"
  nurse "What in god's name?!"
  menu(side="far_right"):
    extend ""
    "\"Just claiming my reward for helping you.\"":
      mc "Just claiming my reward for helping you."
      lindsey clinic_lindsey_angry_topless "That's sick! Go away!"
      nurse clinic_nurse_angry "That is outrageous, [mc]!"
      nurse "Wait until [jo] hears about this."
    "\"I'm sorry, but I figured I'd only have one chance at seeing an angel the way god made her...\"":
      mc "I'm sorry, but I figured I'd only have one chance at seeing an angel the way god made her..."
      lindsey clinic_lindsey_blush_topless  "That's oddly nice..."
      nurse clinic_nurse_angry "No, it's inappropriate, and I'm reporting it."
      nurse clinic_nurse_angry "Get out of here, [mc]."
    "\"I was going to see them sooner or later, anyway.\"":
      mc "I was going to see them sooner or later, anyway."
      lindsey clinic_lindsey_laughing_topless "Oh my god!"
      mc "I agree. Two-fold."
      lindsey clinic_lindsey_blush_topless "Oh my god!"
      nurse clinic_nurse_confused "Young love, huh? Okay, pull back the curtain now."
      mc "Who said anything about love?"
      lindsey clinic_lindsey_annoyed_topless "Oh my god!"
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
