init python:
  register_replay("flora_cereal","Flora's Breakfast","replays flora_cereal","replay_flora_cereal")

label replay_flora_cereal:
  show flora kitchen_cereals_1 with fadehold
  "When you haven't had a single positive female interaction in years... it's hard not to peep, even though it's wrong."
  "Her frilly little panties always seemed so alluring."
  "How they follow the contours of her butt so perfectly. Hiding and showing everything at the same time."
  "She's so casual in her skimpy ways. Coming down to breakfast in her underwear."
  "Sometimes showering with the door unlocked. Forgetting to bring a towel. Thinking that her naked tip-toeing past my door goes unnoticed."
  "Sucking on a popsicle, half-dressed on her bed. Playing with her hair or legs, without a care in the world."
  "Maybe it's the taboo of it that gets me?"
  show flora kitchen_cereals_2 with Dissolve(0.5) 
  flora "What are you looking at?"
  "Huh? Crap..."
  menu(side="right"):
    extend ""
    "\"Nothing!\"":
      mc "Nothing!"
      flora kitchen_cereals_1 "Ugh, perv."
    "\"Looked like you needed some help reaching...\"":
      "Looked like you needed some help reaching..."
      flora kitchen_cereals_1 "Oh, it's okay. I got it."
    "\"Don't touch my cereal!\"":
      mc "Don't touch my cereal!"
      flora kitchen_cereals_2 "Try to stop me!"
      mc "..."
      flora kitchen_cereals_1 "Yeah, didn't think so."
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
