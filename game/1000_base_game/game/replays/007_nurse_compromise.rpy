init python:
  register_replay("nurse_compromise","Nurse's Compromise","replays nurse_compromise","replay_nurse_compromise")

label replay_nurse_compromise:
  $x = game.location
  $game.location = school_nurse_room
  $nurse.equip("nurse_masturbating")
  show nurse afraid with Dissolve(.5)
  nurse afraid "[mc]?! What in god's name?!"
  "Look at her all embarrassed. The heat of her lust still clinging to her cheeks."
  "To catch someone in such a compromising position has always been a fantasy of mine."
  "To see the terror and desire in her eyes. Her fingers still glistening with her juices."
  "Beads of sweat on her forehead, so close to reaching her climax, but unable now."
  "Denied."
  "And then the repercussions start filling her head. Shame. Regret. Fear. But also excitement."
  "It still lingers as her body disagrees with her mind."
  "Her blush deepens."
  "How will she get out of this? Will I let her?"
  "The loss of power. The exposure. We're both feeling it."
  nurse blush "This is not what it looks like!"
  show nurse blush at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Huh! It looks exactly like you were having a very wet daydream about the English teacher.\"":
      show nurse blush at move_to(.5)
      mc "Huh! It looks exactly like you were having a very wet daydream about the English teacher."
      nurse concerned "I... I..."
      mc "Don't worry, I won't tell anyone."
      nurse concerned  "Really?"
      mc "Of course not! It'll be our little secret."
      nurse sad "Thank you. I can't lose this job..."
      mc "You're welcome!"
      show nurse sad at move_to(.75)
      menu(side="left"):
        "\"Just one for the yearbook, okay?\"":
          show nurse sad at move_to(.5)
          mc "Just one for the yearbook, okay?"
          show white with Dissolve(.15)
          play sound "camera_snap"
          show nurse afraid
          hide white with Dissolve(.15)
          nurse afraid "W-what?"
          mc "Your job and reputation mean everything to you, right?"
          nurse concerned "Please, this can't get out!"
          mc  "If you play your cards right, nobody will see this."
          nurse afraid "This is blackmail!"
          mc "Yes, and I've got nothing to lose. You do."
          mc "You know how easy it is to print up a few of these?"
          mc "Or just report you to [jo]?"
          nurse sad "Please! I'll be ruined!"
          mc "You'll be all right. Just don't do anything stupid."
          nurse thinking "Like what?"
          mc "Like snitching. No snitching!"
          nurse afraid "I won't say anything!"
          nurse afraid "Just please don't show anyone!"
          mc "You have my word."
          "This could all come crashing down on me, but I never took risks in the past."
          "Here's my chance to get something back from the place that took everything from me."
          "Just got to be smart about this."
        "\"It's okay, I know how hard you work.\"":
          show nurse sad at move_to(.5)
          mc "It's okay, I know how hard you work."
          nurse blush "I appreciate that. It won't happen again!"
          mc "And if it does, just make sure you lock the door first."
          nurse sad "Okay, I promise."
          "She seems relieved, but there's something else in her eyes."
          "Disappointment? Nah, that can't be. Must be my imagination."
    "\"I'm so sorry! I should've knocked!\"":
      show nurse blush at move_to(.5)
      mc "I'm so sorry! I should've knocked!"
      nurse afraid "Oh my lord. This can't be happening..."
      mc "Hey, it could've been worse. At least you look great."
      nurse blush "Do you really think so?"
      nurse annoyed "Err, I mean... this is widely inappropriate!"
      mc "Maybe what you were doing. I'm just complimenting you."
      nurse sad "This is the most embarrassing moment of my life..."
      mc "Don't beat yourself up. It's really not a big deal."
      nurse concerned "Don't tell anyone, okay?"
      mc "My lips are sealed."
    "\"Smile for the camera!\"":
      show nurse blush at move_to(.5)
      mc "Smile for the camera!"
      #SFX: Camera shutter sound / Camera Flash

      ## Flash original implementation
      # show white with fade
      # hide white with Dissolve(.25)

      ## The camera's flash and sound, together with the nurse's pose
      ## that is the same as the quest's image.
      show white with Dissolve(.15)
      play sound "camera_snap"
      show nurse afraid
      hide white with Dissolve(.15)

      nurse afraid "D-did you just...?"
      mc "You have a lot to lose, don't you?"
      nurse concerned "Oh my... this... this can't get out!"
      mc  "If you follow my instructions, it won't."
      nurse afraid "Y-you're going to blackmail me?"
      mc "We'll see, I guess."
      mc "Maybe I'll just print up a bunch of these and post them all over the school."
      mc "Maybe I'll give the photo anonymously to the principal."
      nurse afraid "Oh, please! Please, you can't! My life will be over!"
      mc "Maybe I won't. It all depends on your actions.."
      nurse thinking "What do you mean?"
      mc "If you're quiet about it, no one will know. But if you decide to tell people..."
      nurse afraid "I'll keep quiet! I swear!"
      nurse sad "Please, don't ruin me..."
      mc "Okay, that's good. We'll see what happens, then. For now, you're safe."
      mc "If you do decide to tell people, just know that my life isn't worth anything..."
      mc "And the only loser in that scenario will be you."
      nurse afraid "I won't! I promise!"
      "This could all come crashing down on me, but I never took risks in the past."
      "Here's my chance to get something back from the place that took everything from me."
      "Just got to be smart about this."
    "\"Relax, there's no shame in this, and I won't tell anyone.\"":
      show nurse blush at move_to(.5)
      mc "Relax, there's no shame in this, and I won't tell anyone."
      nurse concerned  "Really?"
      mc "Yes, really."
      "Getting caught masturbating is a very relatable fear."
      "If it ever happened to me, I'd like it to be by someone kind and understanding."
      mc "Everyone needs to take the edge off. Just lock the door next time."
      nurse blush "I'm such an idiot. I'm so sorry you had to see it."
      nurse concerned "I've been so stressed out, lately. I'm so ashamed."
      mc "Don't worry about it!"
      "She seems relieved, but there's something else in her eyes, as well..."
      "Disappointment? Nah, that can't be. Must be my imagination."
  hide nurse
  $nurse.equip("nurse_outfit")
  $game.location = x
  #scene x with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
