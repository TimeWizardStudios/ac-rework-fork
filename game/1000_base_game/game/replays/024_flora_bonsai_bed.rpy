init python:
  register_replay("flora_bonsai_bed","Flora's Sleep ","replays flora_bonsai_bed","replay_flora_bonsai_bed")

label replay_flora_bonsai_bed:
  show flora bed neutral with fadehold
  "It always used to be a fantasy of mine to have [flora] sleeping by my side."
  "But those fantasies were all dirty in nature. This is different."
  "A few minutes ago, she was shaking and sweating, but now she seems more content."
  "Old [flora] would never have agreed to this, but maybe she's changing?"
  "Maybe I'm the one changing?"
  "We always give each other a tough time, but maybe we're starting to trust each other more."
  "At the end of the day, this is what I want."
  "Her soft breath on my neck. The familiar scent of strawberries and sass."
  "I could grow old like this."
  "Why chase other girls when the perfect one's been here all along?"
  "Seeing her like this makes me regret all the bad things I've done to her in the past."
  flora bed neutral "[mc]? Are you awake?"
  mc "Hmm, yeah?"
  flora bed sad "I want cake."
  mc "I know. I was addicted for far too many years. I know the struggle."
  flora bed sad "I didn't fully grasp the magnitude of this cake's power."
  mc "It's okay, I figured you'd be better off knowing. It's a lot of responsibility."
  flora bed smile "You make it sound silly."
  show flora bed content with Dissolve(.5)
  mc "I'm doing my best not to. This cake is evil."
  flora bed sad "I'm burning the recipe tomorrow."
  menu(side="far_right"):
    extend ""
    "\"You don't have to do that. It really might come in handy someday.\"":
      mc "You don't have to do that. It really might come in handy someday."
      flora bed flirty "That's what you said about the neighbor's old magazines."
      "Oh, they've come in handy a lot. Sometimes multiple times per day."
      show flora bed content with Dissolve(.5)
      mc "Right, but this is different."
      flora bed content "Fine, I'll keep the recipe in case of a cake emergency!"
      mc "Yeah, because it's easier to bake your way to the top than... using your mouth."
      flora bed flirty "As if I need to take shortcuts to be successful."
      show flora bed content with Dissolve(.5)
      mc "I mean... if [jo] got her throat problems from climbing the corporate ladder..."
    "\"That's probably for the best. I'm all about disarming WMDs.\"":
      mc "That's probably for the best. I'm all about disarming WMDs."
      flora bed neutral "Since when do you care about that?"
      mc "Since I saw what that cake did to you."
      mc "I don't want you to get like this again, or anyone else for that matter."
      flora bed content "Aww."
      mc "Besides, real fighting should be done using your fists."
    "\"Make sure you also stake it and put salt around the grave.\"":
      mc "Make sure you also stake it and put salt around the grave."
      #incest patch
      #flora bed flirty "Being your [roommate/sister] has taught me everything I need to know about purifying unholy taint."
      flora bed flirty "Being your roommate has taught me everything I need to know about purifying unholy taint."
      show flora bed content with Dissolve(.5)
      mc "Hilarious..."
      flora bed content "You're not all bad. You know that, right?"
      mc "You really are high on cake."
      flora bed neutral "I'm serious!"
  flora bed neutral "You know, you can be a real dick sometimes, and annoying, and a creep, and you rile me up like no one else..."
  mc "Is there a \"but\" coming soon?"
  flora bed smile "But!"
  flora bed content "I know that deep down you care about me."
  flora bed content "I think you've shown that in the last few weeks."
  flora bed content "Give me your hand."
  mc "Why? What are you going to do with it?"
  flora bed flirty "Just shut up and do it!"
  "Her cake addiction might cause her to bite it off..."
  "But you only live twice, right?"
  show flora bed content with Dissolve(.5)
  "Blindly, her fingertips fumble down my arm, searching for something..."
  "Maybe it's approval? Maybe it's comfort."
  "Sweeping across my wrists, her hand finally finds mine and her thin fingers squeeze me tight."
  show flora bed content hand with Dissolve(.5)
  "Luckily, she can't see the surprise and stupid grin on my face."
  "I never thought holding her hand would feel so intimate and pure at the same time."
  "Unlike so many other things between us, no one can judge us for holding hands."
  "There's nothing wrong about it, and she knows it too."
  mc "This feels... right."
  flora bed smile hand "Goodnight, [mc]."
  show flora bed content with Dissolve(.5)
  "Who knows what will happen tomorrow?"
  "Maybe we'll go back to our usual ways."
  "But for now, I don't care. I just want to feel her squeezing my hand."
  "I want nothing more than for this moment to last."
  "Both of us pretending to sleep, feeling the soothing heat, the comfort in each other's presence."
  "Both of us wondering what could be? What if things were different?"
  "Maybe we'll never know, but that doesn't matter right now."
  "..."
  "What matters is this... this feeling. Is it happiness? Bliss?"
  "All I know is that I want this."
  "Her hand in mine."
  hide flora with Dissolve(.5)
  return
