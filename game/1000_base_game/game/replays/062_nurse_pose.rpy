init python:
  register_replay("nurse_pose","Nurse's Pose","replays nurse_pose","replay_nurse_pose")

label replay_nurse_pose:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  $x = game.location
  $y = school_cafeteria["exclusive"]
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_cafeteria"
  $school_cafeteria["exclusive"] = ("flora","isabelle","lindsey","maxine")
  $quest.isabelle_dethroning["kitchen_shenanigans_replay"] = True
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  nurse "B-but, Miss [kate]! They're right outside!"
  kate "Yes, that's perfect for you. Someone could walk in here at any moment.{space=-65}"
  nurse "Mhm..."
  kate "Make the round, slut. I will not repeat myself again."
  nurse "Yes, Miss [kate]..."
  window hide
  show nurse kitchen_crawling
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  kate "Come on, crawl."
  kate "If you're not quick, I'll make you eat that carrot afterward."
  "Holy shit! The [nurse] is crawling after [kate] in broad daylight...\nright at the party..."
  if quest.kate_desire["stakes"] == "release":
    "This is really hot..."
    "But [kate] broke her promise."
    mc "What are you doing, [kate]?"
  nurse kitchen_crawling "Oh my goodness!"
  kate "Look, we have company! It's my least favorite nerd!"
  if quest.kate_desire["stakes"] != "release":
    mc "Err... I was just looking for ice cream..."
  nurse kitchen_crawling "Oh, no... Oh, god..."
  kate "Please, I know you love this. And it looks like [mc] here brought\na camera!"
  if quest.kate_desire["stakes"] == "release":
    mc "You broke your promise to leave her alone."
    kate "I didn't, actually. She asked me to do this."
    kate "Our deal was only to stop blackmailing her."
    mc "I won't allow this, [kate]."
    kate "Don't get your panties in a twist. This is the last time, okay?"
    kate "She just wanted to say goodbye... the right way."
    mc "Is this true, [nurse]?"
    nurse kitchen_crawling "Y-yes..."
    kate "See?"
  kate "Come on, slut! Pose for the camera!"
  nurse kitchen_crawling "My life is over..."
  kate "Don't be such a drama queen."
  kate "This is only for the school paper. No one reads it."
  window hide
  show nurse kitchen_posing
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  nurse kitchen_posing "Oh my god..."
  kate "Take the picture, [mc]."
  mc "I guess it'll look great on the cover..."
  mc "All right, smile!"
  nurse kitchen_posing "Oh my god..."
  window hide
  pause 0.25
  show white onlayer screens zorder 100 with Dissolve(.125)
  play sound "camera_snap"
  hide white onlayer screens with Dissolve(.125)
  pause 0.25
  window auto
  "Despite looking spooked, the [nurse] seems to have enjoyed that. She's practically gushing."
  kate "Keep crawling, slut. We're not done yet."
  nurse kitchen_posing "Yes, Miss [kate]..."
  kate "I'm starting to think you do want to eat that carrot."
  kate "Maybe I should make you eat it at the table?"
  nurse kitchen_posing "Oh! Please, no!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = x
  $school_cafeteria["exclusive"] = y
  $quest.isabelle_dethroning["kitchen_shenanigans_replay"] = False
  pause 1.0
  hide nurse kitchen_posing
  hide black onlayer screens
  with Dissolve(.5)
  window auto

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
