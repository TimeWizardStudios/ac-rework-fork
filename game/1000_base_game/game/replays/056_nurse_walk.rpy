init python:
  register_replay("nurse_walk","Nurse's Walk","replays nurse_walk","replay_nurse_walk")

label replay_nurse_walk:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show nurse dog_mouth_closed with fadehold
  "There's something supremely satisfying about the [nurse] crawling after me, doing my bidding."
  "Even better is the fact that [kate] is here, watching me use her little pet to meet my own needs."
  show nurse dog_kate_surprised with Dissolve(.5)
  kate "What the hell is this?"
  menu(side="middle"):
    extend ""
    "\"What do you mean? It's just\nme walking my dog.\"":
      mc "What do you mean? It's just me walking my dog."
      show nurse dog_kate_skeptical with Dissolve(.5)
      kate "Very funny."
      mc "I don't see anything funny here. Just me going about my day."
      kate "Well, go about your day somewhere else."
      mc "I will, right after I get my gym grade."
    "\"Did you think you were the\nonly one capable of this?\"":
      mc "Did you think you were the only one capable of this?"
      show nurse dog_kate_excited with Dissolve(.5)
      kate "What? Am I supposed to be impressed or something?"
      kate "She's the easiest one I've met."
      mc "Hey, be nice to my dog!"
      show nurse dog_kate_skeptical with Dissolve(.5)
      kate "Don't tell me what to do, else I'll make {i}you{/} my dog."
      mc "As interesting as that would be, I have a gym grade to get right now.{space=-10}"
    "\"I'm looking for another bitch.\nAre you interested?\"":
      mc "I'm looking for another bitch. Are you interested?"
      show nurse dog_kate_excited with Dissolve(.5)
      kate "You sure have a lively fantasy."
      mc "Are you blushing a little? Maybe that's secretly {i}your{/} fantasy?"
      show nurse dog_kate_skeptical with Dissolve(.5)
      kate "You need to get your eyes checked, you dumb clown."
      kate "Get the fuck out of here before I call the girls over."
      mc "Sorry, no can do. I actually have a gym grade to get."
  mc "Please, step out of the way."
  show nurse dog_kate_excited with Dissolve(.5)
  kate "I knew you were slow, but this is ridiculous."
  kate "The gym is closed. C-l-o-s-e-d. Got it?"
  mc "Not according to the Newfall Constitution on Animal Rights, Chapter 87, subsection D."
  show nurse dog_kate_neutral with Dissolve(.5)
  kate "What are you talking about?"
  mc "Oh, could it be that you're not familiar with the county rules?"
  mc "Please, do look it up. I'll wait."
  show nurse dog_kate_skeptical with Dissolve(.5)
  kate "Seriously?"
  mc "Seriously."
  show nurse dog_kate_neutral with Dissolve(.5)
  kate "..."
  kate "..."
  "Man, I sure hope [maxine] didn't make this up..."
  show nurse dog_kate_surprised with Dissolve(.5)
  kate "What the fuck?"
  mc "See? You're legally required to let me in."
  show nurse dog_kate_skeptical with Dissolve(.5)
  kate "This is not a dog, though! She doesn't even bark!"
  show nurse dog_mouth_open
  nurse dog_mouth_open "Woof! Woof!" with vpunch
  show nurse dog_kate_neutral with Dissolve(.5)
  kate "..."
  mc "Move or I'll get the [guard]. I know he's been looking to bust you ever since you put that special glaze on his doughnuts."
  show nurse dog_kate_annoyed with Dissolve(.5)
  kate "This is not over, [mc]!"
  kate "And you... you're in big trouble."
  window hide
  show expression "nurse avatar events dog kate_background" as background behind nurse
  show nurse dog_kate_annoyed_backgroundless at disappear_to_left
  pause 1.0
  hide background
  show nurse dog_mouth_closed_right at center
  with Dissolve(.5)
  window auto
  mc "Good girl! You did great."
  mc "People really need to stand up to [kate] more often."
  "And speaking of meeting needs... I think I'll get a little more use out of this situation."
  scene location with fadehold

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
