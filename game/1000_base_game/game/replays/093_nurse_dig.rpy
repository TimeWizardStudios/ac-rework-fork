init python:
  register_replay("nurse_dig","Nurse's Dig","replays nurse_dig","replay_nurse_dig")

label replay_nurse_dig:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show nurse worm_digging with fadehold
  # "Unsurprisingly, she does as I say, even if she pretends to be indignant about it."
  "Unsurprisingly, she does as I say, even if she pretends to be indignant{space=-40}\nabout it."
  "She looks right at home down there on her hands and knees."
  "Down in the dirt with the literal worms."
  mc "That's it. Make sure you get plenty."
  nurse worm_digging "Oh, gracious..."
  "Her face turns a darker shade of red, but even down on the ground, she seems to relax a little."
  "Just being told what to do and how to do it must have this effect on her."
  # "I can definitely see how [kate] got carried away with this kind of power."
  "I can definitely see how [kate] got carried away with this kind of power.{space=-35}"
  "The [nurse] gives in so easily. She really needs someone who won't abuse it."
  mc "Okay, I think that's plenty. Nice job."
  nurse worm_digging "Th-thank you..."
  scene location with fadehold

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
