init python:
  register_replay("maya_consolation","Maya's Consolation","replays maya_consolation","replay_maya_consolation")

label replay_maya_consolation:
  window hide None
  hide screen interface_hider
  show maya looking_down concerned
  show teary_eyes2 as half_open_eyes:
    ysize 1080
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "The hit knocks me out cold."
  "For a moment, I drift over a landscape of burning trees."
  "Fires roaring down the hills."
  "Ashes and smoke billow into the dark sky."
  show black onlayer screens zorder 100
  pause 0.5
  $game.ui.hide_hud = True
  hide flora
  hide black
  hide black onlayer screens
  with open_eyes
  window auto
  $set_dialog_mode("")
  "Then, my senses start to return, and I realize that this is one of the biggest mistakes of my life."
  "The room spins around me as the heat consumes me from within."
  window hide
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  pause 0.25
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  window auto
  "My old life seems to flash in rapid bursts behind my eyelids."
  "Alienating everyone."
  "Ending up alone and lonely."
  "I'm reminded of all of my regrets, big and small."
  # "I choke on them, like I choke on this fire scorching through my mouth and throat."
  "I choke on them, like I choke on this fire scorching through my mouth{space=-35}\nand throat."
  flora "[mc]! Talk to me!!"
  flora "Oh, god. We killed him."
  mc "Mmm...?"
  window hide
  $game.ui.hide_hud = False
  show expression Flatten("maya looking_down sarcastic1") as workaround behind half_open_eyes:
    alpha 0.0
    pause 0.25
    ease 0.75 alpha 1.0
  hide half_open_eyes with open_eyes
  $renpy.transition(Dissolve(.25))
  show screen interface_hider
  window auto show
  show maya looking_down sarcastic1 with {"master": Dissolve(.5)}
  maya looking_down sarcastic1 "That may be the most tragic thing I have ever seen."
  maya looking_down sarcastic1 "It even tops cult demon sacrifice tragic."
  hide workaround
  "Huh? Am I still dreaming?"
  # "Seeing [maya] in front of me, completely naked from the top up, I must be, right?"
  "Seeing [maya] in front of me, completely naked from the top up,\nI must be, right?"
  maya looking_down squeeze3 "Maybe this will wake you..."
  window hide
  pause 0.125
  show maya looking_down squeeze2 with Dissolve(.125)
  show maya looking_down squeeze1 with Dissolve(.125)
  pause 0.25
  show maya looking_down squeeze2 with Dissolve(.0625)
  show maya looking_down squeeze3 with Dissolve(.0625)
  show maya looking_down squeeze4 with Dissolve(.0625)
  show maya looking_down squeeze3 with Dissolve(.125)
  pause 0.5
  show maya looking_down squeeze2 with Dissolve(.125)
  show maya looking_down squeeze1 with Dissolve(.125)
  pause 0.25
  show maya looking_down squeeze2 with Dissolve(.0625)
  show maya looking_down squeeze3 with Dissolve(.0625)
  show maya looking_down squeeze4 with Dissolve(.0625)
  show maya looking_down squeeze3 with Dissolve(.125)
  pause 0.5
  show maya looking_down squeeze2 with Dissolve(.125)
  show maya looking_down squeeze1 with Dissolve(.125)
  pause 0.25
  show maya looking_down squeeze2 with Dissolve(.0625)
  show maya looking_down squeeze3 with Dissolve(.0625)
  show maya looking_down squeeze4 with Dissolve(.0625)
  show maya looking_down squeeze3 with Dissolve(.125)
  pause 0.25
  window auto
  # "She squeezes her tits together and then shrugs, letting them bounce back to their unbelievably perky state."
  "She squeezes her tits together and then shrugs, letting them bounce{space=-20}\nback to their unbelievably perky state."
  mc "Err, what happened?"
  maya looking_down confident "I took my turn while you were moaning and squirming on the floor."
  maya looking_down confident "It was pretty hot. So, I decided to cool down."
  show maya looking_down sarcastic2 with {"master": Dissolve(.5)}
  maya looking_down sarcastic2 "The sauce, I mean. Definitely not your moaning."
  mc "Ugh, god... just let me die here..."
  "Even a topless [maya] is little consolation for that absolute embarrassment..."
  scene location with fadehold
  return
