init python:
  register_replay("flora_shower","Flora's Shower","replays flora_shower","replay_flora_shower")

label replay_flora_shower:
  $quest.flora_squid["shower_stamp"] =  quest.flora_squid["shower"]
  show black onlayer screens zorder 4 with fadehold
  play ambient "audio/sound/shower_loop.ogg" volume 0.5
  $set_dialog_mode("default_no_bg")
  "Ah! The flow of water finally washing away the smell of the river!"
  "Hopefully, it'll get rid of the ink too—"
  mc "Hey! Stop pushing me."
  flora "Well, you're in the way..."
  "She slides up in front of me, the warm skin of her back pressing against my stomach."
# "I'm showering with my sister! I can't see anything, but still..." ## Incest patch ##
# "This is all sorts of wrong." ## Incest patch ##
  mc "Couldn't you have waited your turn?"
  flora "Nope!"
  mc "..."
  menu(side="middle"):
    extend ""
    "Pull her into a hug":
      $quest.flora_squid["shower"] = "hug"
      flora "Oh!" with vpunch
      "For the first time in my life, it feels right."
      "[flora] squirms weakly in my arms, her wet skin sliding beneath my touch."
      "And then she stops moving."
      "It's like we both know this is how it's supposed to be."
#     "Even though it's wrong in the eyes of society, it's right for us." ## Incest patch ##
      flora "Thanks for saving my squid..."
      mc "Anytime."
      "She turns in my arms to face me."
      "Her wet fingers wipe away the ink from my face, from my eyes."
      window hide None
      show flora shower_hug
      hide black onlayer screens
      with Dissolve(1.0)
      window auto
      $set_dialog_mode("")
      "I never thought we'd end up in a situation like this..."
      "Her perky breasts tight against my chest."
      "[flora] is usually a total brat, but there is a soft feminine side to her as well."
      "A caring side."
      mc "I'd like to know you."
      flora shower_hug "What do you mean?"
      mc "The other side of you."
      flora shower_hug "And what side is that?"
      mc "This side... Your gentle side."
      flora shower_hug "Maybe if you treat me well, I'll let you..."
      mc "I'll do my best."
      flora shower_hug "Try it now."
      mc "Okay..."
      window hide
      show flora shower_kiss with Dissolve(1.0)
      window auto
      "Our lips meet for the first time."
      "The kiss is much wetter than in my fantasies."
      "She has a faint sweet taste, nothing more."
      "Not at all a venomous acid taste like her words would sometimes suggest."
      "..."
#     "I never thought I'd know the taste of my sister's lips..." ## Incest patch ##
      "I do like her bratty side in moderation, but this is all new to me."
      "At first, it's like a thought in the back of my mind — a craving of sorts.{space=-50}"
      "Then it grows..."
      "I need to have her."
      "It would be a lie to say it's not a carnal desire, but there's definitely something more..."
      "Something that has been growing in my chest for years."
      "A yearning of the heart."
#     "Most people would say they love their sister, but for me it's a different kind of love." ## Incest patch ##
      "You never really understand how much you love someone until you're close to losing them."
      "Or in my case, until you push them away, and they stop talking to you...{space=-65}"
      "And then you spend the rest of your life thinking what an idiot and coward you were."
      "To never hold her in my arms, her tender form, skin-to-skin... so much regret."
      flora shower_hug "Are you crying?"
      mc "No... it's just the ink..."
      "She doesn't know how it felt to see her in the arms of another man."
      "The pictures of her in a blazing white wedding dress..."
      "God. It hurts to even think about it."
      "..."
      "Don't be a fucking coward, man. Not again."
      "But maybe she doesn't want this? Maybe she just likes the intimacy?{space=-20}"
      "Fuck."
      mc "[flora]..."
      flora shower_hug "What?"
      mc "You need to tell me to stop."
      mc "I'm serious. This is wrong."
      flora shower_hug "Stop what?"
      mc "What I'm about to do..."
      window hide
      play sound "fast_whoosh"
      show flora shower_sex_anticipation with Dissolve(.5): ## This gives the image both the dissolve (transition) and vpunch (transform) effects
        block:
          linear 0.05 xoffset 15
          linear 0.05 xoffset -15
          repeat 3
        linear 0.05 xoffset 0
      hide flora
      show flora shower_sex_anticipation ## This is just to avoid the screen shake if the player is skipping
      window auto
      flora shower_sex_anticipation "Oh!"
      flora shower_sex_anticipation "We can't do this, you know..."
      mc "I know. Tell me to stop."
      flora shower_sex_anticipation "..."
      mc "[flora], please..."
      flora shower_sex_anticipation "Nope!"
      mc "Oh, god."
      "Instead of saying stop, she takes my hand and leans against the wall for support."
      "And it feels right, even though it's not."
#     "Against our better judgment, and the condemnation of society." ## Incest patch ##
      window hide
      show flora shower_sex_penetration1 with Dissolve(.5)
      window auto
      "Our palms meet and our fingers intertwine."
      "We're going to hell together."
      flora shower_sex_penetration1 "Oww, be careful!"
      mc "Sorry!"
      show flora shower_sex_penetration2 with dissolve2
      "[flora] cries out as I part her nether lips and push inside her."
      "She's so soft, so sensitive..."
      "Not at all the tough exterior she puts on."
      "I really do need to be gentle with her."
      show flora shower_sex_penetration3 with dissolve2
      "Her pussy finally relaxes a bit, allowing me to push deeper."
      "[flora] seems nervous, but no longer in pain."
      "Maybe she's regretting it already..."
      mc "Are you okay?"
      flora shower_sex_penetration3 "Y-yeah..."
      mc "You can still tell me to stop..."
      flora shower_sex_penetration3 "..."
      "She says nothing."
      "And maybe she's waiting for me to back out... but I've been waiting years for this."
      "I need to make her mine, even if it's just once."
      window hide
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.15)
      window auto
      "Her thighs and pussy squeeze me hard."
      "Goosebumps explode over my arms and back."
      "The heat of her pussy is like nothing I've ever felt before."
#     "It's a perfect fit — two organs from the same mold." ## Incest patch ##
      window hide
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.5)
      window auto
      flora shower_sex_penetration2 "Oh, my god!"
      mc "Shhh! [jo] will hear you!"
      show flora shower_sex_penetration6 with dissolve2
      flora shower_sex_penetration6 "Sorry..."
      window hide
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      window auto
      "God, she feels so good!"
#     "My sister has the tightest pussy in all of Newfall." ## Incest patch ##
#     "That's a messed up thing to think... but damn it all..." ## Incest patch ##
      flora shower_sex_penetration7 "Don't stop!"
      mc "I won't... I've waited thirty years for this..."
      show flora shower_sex_penetration3 with dissolve2
      flora shower_sex_penetration3 "W-what?"
      mc "Uh, nothing!"
      window hide
      window hide
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.1)
      show flora shower_sex_penetration4 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      window auto
      "I just need to keep my mouth shut and enjoy the moment..."
      "Enjoy every inch of her pussy around my cock."
      "Enjoy every spasm and moan."
      "Take it all in."
      "Take it all."
      "Take it!"
      window hide
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_flora_cum1 with vpunch
      window auto
      flora shower_sex_flora_cum1 "Oh, fuck!"
      show flora shower_sex_flora_cum2 with dissolve2
      "With a gasp, the orgasm rips through her."
      "Every muscle in her body goes rigid, and then starts spasming."
      "The muscles of her pussy expel me as her eyes roll back."
      "And the blush of pleasure on her freckled cheeks sends me over the edge."
      window hide
      show flora shower_sex_mc_cum1 with Dissolve(.5)
      show flora shower_sex_mc_cum2 with vpunch
      show flora shower_sex_mc_aftermath with Dissolve(.5)
      window auto
      "[flora] slumps against the wall, in a post-orgasmic haze."
      "Her eyes still glazed over from pure bliss."
    "Touch her ass":
      $quest.flora_squid["shower"] = "ass"
      flora "Hey!" with vpunch
      mc "Sorry, blind man here."
      flora "..."
      flora "Your hand is still on my butt..."
      mc "So it seems."
      flora "..."
      mc "..."
      mc "You haven't told me to remove it yet..."
      flora "So it seems."
      mc "You're a brat."
      flora "You're an idiot."
      mc "..."
      flora "..."
      mc "You have a nice butt."
      flora "Shut up."
      "God, the soft curve of her ass in my hand..."
      "It feels so wrong to touch her like this... but she's okay with it..."
#     "Fuck me. What's wrong with us?" ## Incest patch ##
      flora "Move back."
      flora "And wipe that grin off your face."
      mc "This is Apollo 11; we've lost contact with the moon! I repeat; we've lost contact with the moon!"
      flora "Just be quiet for once in your life..."
      mc "I'm afraid I can't do—"
      mc "[flora]!!!"
      window hide None
      show flora shower_blowjob
      show flora shower_blowjob_blurred as blurred:
        pause 0.25
        easeout 0.75 alpha 0.0
      hide black onlayer screens with open_eyes
      window auto
      pause 0.25
      $set_dialog_mode("")
      "This is one of those feelings where you just get floored in an instant.{space=-20}"
      "[flora]'s lips wrapped around the tip of my dick..."
#     "My very own sister..." ## Incest patch ##
      "Her mischievous eyes looking up at me..."
      "Fuck me, I can't!"
      mc "[flora]! W-what, hnngh.... are you doing?"
      flora shower_blowjob "You had some ink in this hole..."
      flora shower_blowjob "I have to suck it out like venom."
      mc "Oh, fuck!"
      "The suction of her mouth increases."
      "It's like my entire soul and spirit is about to get sucked out of my dick.{space=-50}"
      "Stars dance before my eyes as the wet heat of her mouth envelops me.{space=-65}"
#     "My sister's mouth..." ## Incest patch ##
      "How did we end up here?"
      "Did I corrupt her? Did she corrupt me?"
      "Or was it always a flaw on both our parts?"
      "Fuck. I can't think straight."
      window hide
      show flora shower_blowjob_blurred as blurred:
        alpha 0.0
        easein 0.75 alpha 1.0
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      show flora shower_blowjob_blurred as blurred:
        pause 0.25
        easeout 0.75 alpha 0.0
      hide black onlayer screens with open_eyes
      pause 0.25
      show flora shower_blowjob_blurred as blurred:
        alpha 0.0
        easein 0.75 alpha 1.0
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      show flora shower_blowjob_blurred as blurred:
        pause 0.25
        easeout 0.75 alpha 0.0
      hide black onlayer screens with open_eyes
      window auto
      flora shower_blowjob "Stay still... I need to get all the ink out."
      mc "[flora], you're killing me..."
      window hide
      show flora shower_blowjob_blurred as blurred:
        alpha 0.0
        easein 0.75 alpha 1.0
      show black onlayer screens zorder 100 with close_eyes
      pause 1.5
      play sound "fast_whoosh"
      hide black onlayer screens
      hide blurred
      show flora shower_sex_anticipation
      with hpunch
      window auto
      flora shower_sex_anticipation "Oh!"
      "In a daze of pleasure and guilt, I pull her up and push her against the shower wall."
      "Her mouth has made me hard. This is her fault."
      "Little devilish vixen."
      "She giggles, acting all innocent... as if she doesn't know what she's doing to me."
      "Years and years of teasing has finally unleashed a beast in me."
      flora shower_sex_anticipation "What are you going to do with that?"
      "Only a grunt of frustration passes over my lips."
      "This is so wrong, but I can't help myself."
      mc "You little succubus..."
      window hide
      show flora shower_sex_penetration1 with vpunch
      window auto
      flora shower_sex_penetration1 "Owie!"
      "In a flash, my fingers catch her nipple."
      "And just like that, my cock split her open."
      show flora shower_sex_penetration6 with dissolve2
      mc "Isn't this what you wanted?"
      "The heat of her vagina almost knocks me out; only sheer will keeps me standing."
      "She doesn't answer, but her hand slips between her legs, her fingers teasing her clitoris."
      "What a fucked up pair we are..."
#     "Brother and sister." ## Incest patch ##
      "To do this in the shower, with only thin walls separating us from [jo]."
      "What would she think if she saw us now?"
#     "Would she sneer in disgust or just want us to be happy?" ## Incest patch ##
      show flora shower_sex_penetration3 with dissolve2
      "Guilt flickers through me, and I can see it in [flora]'s face — she's feeling it too."
      "The sheer wrongness of what's happening."
      "Still I push myself deeper inside her tight pussy."
      "Still she keeps rubbing herself."
      show flora shower_sex_penetration8 with dissolve2
      "She gasps as I bottom out inside her."
      "My fingers keep playing with her nipple."
      "It's so soft, and somehow still firm."
      "Deliciously erotic to the touch."
      window hide
      show flora shower_sex_penetration6 with Dissolve(.25)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.15)
      pause 0.5
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.5)
      window auto
      mc "God, you're hot!"
      flora shower_sex_penetration2 "Ohhh!"
      "Her moans are loud enough to cut through the rushing water, but I don't care."
      "I want [flora] to cry out in pleasure."
      window hide
      show flora shower_sex_penetration3 with Dissolve(.1)
      show flora shower_sex_penetration4 with Dissolve(.25)
      show flora shower_sex_penetration3 with Dissolve(.1)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      window auto
      "This might be a once in a lifetime thing."
#     "Fucking my sister without any inhibition..." ## Incest patch ##
      "Maybe we'll both pay for it later, but this moment belongs to us alone.{space=-40}"
      "And I'm going to take it all in."
      "Take it all."
      "Take it!"
      window hide
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_flora_cum1 with vpunch
      window auto
      flora shower_sex_flora_cum1 "Oh, my god!"
      show flora shower_sex_flora_cum2 with dissolve2
      "A shiver runs through her body as she goes rigid."
      "Her pussy tightens around my dick, squeezing it with every wave of her orgasm."
      "She's gripping my dick so tightly, I almost can't pull it out..."
      "But I can't cum inside her!"
      window hide
      show flora shower_sex_mc_cum1 with Dissolve(.5)
      show flora shower_sex_mc_cum2 with vpunch
      show flora shower_sex_mc_aftermath with Dissolve(.5)
      window auto
      "With a plop, my dick dislodges and sprays my load all over her."
      "That was way too fucking close..."
      "Exhausted, [flora] slumps against the wall, her brain seemingly gone with her orgasm."
  "..."
  "Fuck. What have we done?"
  window hide
  stop ambient
  scene location with fadehold
  $quest.flora_squid["shower"] = quest.flora_squid["shower_stamp"]
  $quest.flora_squid["shower_stamp"] = None
  return
