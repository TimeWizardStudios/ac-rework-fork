init python:
  register_replay("floras_dare","Flora's Dare","replays floras_dare","replay_floras_dare")

label replay_floras_dare:
  scene flora FloraIsabelleKiss with dissolve
  "It's probably wrong to be turned on by what many courts would consider an assault, but you only live once, right?"
  "[isabelle]'s surprise adds extra spice. A hint of non-con, but it's still rather playful."
  "[flora]'s usually never one to back out of a dare, but she seems to be enjoying it."
  "There are things about her that even I don't know..."
  "Two straight girls kissing, though..."
  "And the way their boobs squish together..."
  "Hot!"
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
