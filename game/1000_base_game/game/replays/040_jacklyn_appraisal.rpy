init python:
  register_replay("jacklyn_appraisal","Jacklyn's Appraisal","replays jacklyn_appraisal","replay_jacklyn_appraisal")

label replay_jacklyn_appraisal:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show jacklyn king_of_sweets_inspection with fadehold
  mc "..."
  jacklyn king_of_sweets_approval "What the fuck? This is not bad!"
  mc "Do you really think so?"
  jacklyn king_of_sweets_approval "I'm always honest when it comes to art. White lies hurt artists more than harsh truths."
  jacklyn king_of_sweets_approval "It's a decent piece of art, amigo."
  jacklyn king_of_sweets_inspection "Although..."
  jacklyn king_of_sweets_inspection "The technique is baller, but it's missing a centerpiece..."
  mc "Well, I've still to make the final touches."
  jacklyn king_of_sweets_inspection "Yeah?"
  show jacklyn king_of_sweets_zipper with Dissolve(.5)
  mc "Yeah... take a good look."
  play sound "<to 0.75>nylon_rip"
  show jacklyn king_of_sweets_dick_out
  mc "There!" with hpunch
  jacklyn king_of_sweets_amazed "Whoa! Johnny fucking Bravo!"
  jacklyn king_of_sweets_amazed "I'm totally catching your electrons right now, you baby genius!"
  mc "It's a comment on society."
  jacklyn king_of_sweets_amazed "Damn straight it is!"
  mc "I call it, the \"King of Sweets.\""
  jacklyn king_of_sweets_lip_bite "Of course! All the junk food in the world got nothing on the old king!{space=-15}"
  menu(side="middle"):
    extend ""
    "\"Could you take a photo for Newfall Culinary?\"":
      mc "Could you take a photo for Newfall Culinary?"
      jacklyn king_of_sweets_lip_bite "Sure thing, babylegs."
      jacklyn king_of_sweets_camera "..."
      jacklyn king_of_sweets_camera "Okay, let's film-roll this bad boy!"
      window hide
      show jacklyn king_of_sweets_camera_flash with Dissolve(.15)
      show white with Dissolve(.15)
      play sound "camera_snap"
      show jacklyn king_of_sweets_camera
      hide white
      with Dissolve(.15)
      window auto
      "I never thought it would feel this good to have your junk photographed..."
      "It's a bit like being a celebrity, in front of the paparazzi."
      "All celebrities are dicks anyway, so it's fitting."
      window hide
      show jacklyn king_of_sweets_camera_flash with Dissolve(.15)
      show white with Dissolve(.15)
      play sound "camera_snap"
      show jacklyn king_of_sweets_camera
      hide white
      with Dissolve(.15)
      window auto
      "Maybe that would be an idea for a new project?"
      "What if I could make a name for myself as the dick-artist?"
      "The girls would surely come flocking."
      window hide
      show jacklyn king_of_sweets_camera_flash with Dissolve(.15)
      show white with Dissolve(.15)
      play sound "camera_snap"
      show jacklyn king_of_sweets_camera
      hide white
      with Dissolve(.15)
      window auto
      "..."
      "I think [jacklyn]'s exhibitionism is rubbing off on me..."
      "If only she would rub me off..."
      window hide
      show jacklyn king_of_sweets_camera_flash with Dissolve(.15)
      show white with Dissolve(.15)
      play sound "camera_snap"
      show jacklyn king_of_sweets_camera
      hide white
      with Dissolve(.15)
      window auto
      "Another time, perhaps — those who wait for something good, and all that jazz."
      "She's a great mentor. Maybe you just don't fuck your mentor?"
      "That's surely a code or something."
      window hide
      show jacklyn king_of_sweets_camera_flash with Dissolve(.15)
    "\"Would you like to taste the King of Sweets?\"":
      mc "Would you like to taste the King of Sweets?"
      jacklyn king_of_sweets_lip_bite "How could I possibly refuse...?"
      mc "All right, then — dig in."
      jacklyn king_of_sweets_realization "Actually, maybe a grade is par for the course first..."
      jacklyn king_of_sweets_looking_up "Do you think I'm a fair teacher, [mc]?"
      mc "I guess?"
      jacklyn king_of_sweets_shirt_lift1 "How about now?"
      mc "Oh, wow! Pretty fair..."
      mc "But this took me a {i}lot{/} of effort!"
      jacklyn king_of_sweets_shirt_lift2 "You're slap right in the bullseye on that."
      jacklyn king_of_sweets_shirt_lift2 "True raw murder spares..."
      $jacklyn.unequip("jacklyn_bra")
      extend " nothing."
      jacklyn king_of_sweets_stroke1 "Every stroke of the brush not only carries the paint."
      window hide
      show jacklyn king_of_sweets_stroke3 with Dissolve(.2)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.2)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
      pause 0.3
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.2)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
      window auto
      mc "Mmmm..."
      jacklyn king_of_sweets_stroke2 "Every single stroke..."
      window hide
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke5 with Dissolve(.2)
      pause 0.3
      window auto
      jacklyn king_of_sweets_stroke5 "...from the mind of the artist..."
      window hide
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.2)
      pause 0.3
      window auto
      jacklyn king_of_sweets_stroke2 "...down the whole arm..."
      window hide
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.2)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
      pause 0.2
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke5 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.2)
      window auto
      mc "Ohhh!"
      jacklyn king_of_sweets_stroke2 "...to the flick of the wrist..."
      window hide
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke5 with Dissolve(.2)
      pause 0.3
      window auto
      jacklyn king_of_sweets_stroke5 "...out to the tip of the brush..."
      window hide
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.2)
      pause 0.3
      window auto
      jacklyn king_of_sweets_stroke2 "...and on to the canvas..."
      window hide
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke5 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke5 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke4 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.2)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.1)
      show jacklyn king_of_sweets_stroke4 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_stroke3 with Dissolve(.2)
      show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
      pause 0.2
      show jacklyn king_of_sweets_stroke3 with Dissolve(.3)
      pause 0.3
      show jacklyn king_of_sweets_stroke2 with Dissolve(.3)
      pause 0.2
      show jacklyn king_of_sweets_stroke3 with Dissolve(.3)
      pause 0.3
      show jacklyn king_of_sweets_stroke1 with Dissolve(.3)
      window auto
      mc "Hnnng!"
      jacklyn king_of_sweets_stroke1 "...is part of the masterpiece."
      jacklyn king_of_sweets_stroke1 "And all we can do is admire it."
      mc "K-keep going! I'm close to—"
      jacklyn king_of_sweets_stroke6 "Hold on, big boy. I haven't had a taste of it yet..."
      show jacklyn king_of_sweets_admiration with Dissolve(.5)
      "[jacklyn] eyes my dick with delight and admiration."
      "And it's like the clouds break and a beam of clarity fills my mind."
      "It's all about context!"
      "It's a dick like any other, except now it's something more than just a rod of flesh."
      "It's a symbol."
      "People crave all sorts of sweets — pastries, soda, snacks, and candy.{space=-35}"
      "But at the end of the day, they're not playing in the same league as the king."
      "The symbol of life itself."
      "Of course, someone like [jacklyn] appreciates that."
      jacklyn king_of_sweets_taste1 "Mmmm!"
      window hide
      show jacklyn king_of_sweets_taste2 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste1 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste1 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste1 with Dissolve(.4)
      window auto
      mc "Oooh..."
      "Her wrapping her lips around the tip and sucking it vigorously?"
      "That, too, is a symbol..."
      "There's nothing dirty about her tongue massaging the length of my shaft."
      "Nothing dirty about her coating me in hot saliva. Adding suction. Sucking me deeper into her mouth."
      window hide
      show jacklyn king_of_sweets_taste2 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste1 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste1 with Dissolve(.4)
      pause 0.6
      show jacklyn king_of_sweets_taste3 with Dissolve(.4)
      pause 0.6
      show jacklyn king_of_sweets_taste2 with Dissolve(.4)
      pause 0.2
      show jacklyn king_of_sweets_taste3 with Dissolve(.4)
      window auto
      "Letting the tip of my dick paint every nook and cranny of her mouth in sticky pre-cum."
      "And then guiding my dick toward the back of her mouth. Teasing me with the opening of her throat."
      "This is not even a blowjob... this is a statement."
      "This is what art is about."
      window hide
      show jacklyn king_of_sweets_taste2 with Dissolve(.1)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.3)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.1)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.3)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.3)
      show jacklyn king_of_sweets_taste3 with Dissolve(.4)
      pause 0.3
      show jacklyn king_of_sweets_taste2 with Dissolve(.3)
      show jacklyn king_of_sweets_taste3 with Dissolve(.4)
      pause 0.3
      show jacklyn king_of_sweets_taste2 with Dissolve(.3)
      show jacklyn king_of_sweets_taste3 with Dissolve(.4)
      pause 0.3
      show jacklyn king_of_sweets_taste2 with Dissolve(.3)
      show jacklyn king_of_sweets_taste3 with Dissolve(.4)
      window auto
      mc "F-fuck!"
      jacklyn king_of_sweets_taste1 "Mmmm..."
      "This is so much better than in my dream with [kate]..."
      "That was good and felt real, but..."
      "[jacklyn] really has a talent for sucking cock."
      "Her eyes tear up, but she doesn't budge. Doesn't expel me."
      "She just keeps me deep inside her hot throat."
      window hide
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.5)
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.4
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.4
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.4
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.3)
      window auto
      "Maybe art isn't her true calling..."
      "The way she takes me down her throat and flexes her muscles repeatedly to swallow really makes me feel welcome in her mouth."
      "Like she wants my dick to stay down there."
      "Like she enjoys it filling up her throat."
      window hide
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      window auto
      "I've always dreamed of a teacher getting on her knees for me."
      "[jacklyn] has such a strong personality, but she also has no inhibitions.{space=-40}"
      "It's a deadly combination when it comes to sex."
      "Did she learn to suck dick like this in art school?"
      "Because, shit, she's the fucking Picasso of blowjobs."
      window hide
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.1)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.1)
      pause 0.4
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      pause 0.4
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.1)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.1)
      pause 0.4
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      pause 0.2
      show jacklyn king_of_sweets_deepthroat2 with Dissolve(.3)
      pause 0.1
      show jacklyn king_of_sweets_deepthroat1 with Dissolve(.3)
      window auto
      "Although, this is of course not sexual."
      "Not dirty in the slightest."
      "This is art."
      "And in her case... appreciation for art."
      "You don't fault people for admiring the Birth of Venus."
      "So, you can't really fault her for enjoying the King of Sweets either."
      "And damn, does she enjoy it to the fullest! Down to the last inch!"
      window hide
      show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.5)
      pause 0.2
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.1)
      pause 0.1
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.1)
      pause 0.1
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.1)
      pause 0.1
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.1)
      pause 0.1
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
      show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.1)
      pause 0.1
      show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
      show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.2)
      window auto
      "If someone walked in on us now, [jacklyn] would have a lot of explaining to do..."
      "Her sticky lips. Her smudged makeup. Her cock-breath."
      "She's not exactly a Mona Lisa right now... but she probably would take it in stride."
      "Maybe even invite the intruder to enjoy the King of Sweets with her?{space=-10}"
      "God, that would be so hot..."
      python:
        girl_lust = [flora.lust,isabelle.lust,jo.lust,kate.lust,lindsey.lust,maxine.lust,mrsl.lust,nurse.lust]
        girl_name = [flora.name,isabelle.name,jo.name,kate.name,lindsey.name,maxine.name,mrsl.name,nurse.name]
        most_lust = girl_name[girl_lust.index(max(girl_lust))]
        most_lust = ("the " if most_lust == nurse.name else "") + most_lust
      "Imagine if [most_lust] got on her knees right next to [jacklyn]..."
      "Like two art connoisseurs in the Louvre."
      "And it wouldn't be dirty. It would just be raw and artsy."
      jacklyn king_of_sweets_taste1 "Fwah!"
      window hide
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      pause 0.2
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      pause 0.1
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste1 with Dissolve(.2)
      show jacklyn king_of_sweets_taste2 with Dissolve(.2)
      show jacklyn king_of_sweets_taste3 with Dissolve(.2)
      window auto
      "Uhhh! I'm about to Jackson Pollock all over this bitch!"
      mc "H-hey, [jacklyn]?"
      jacklyn king_of_sweets_cum_on_face1 "Hmm? Is it time for the king to take the throne?"
      mc "Y-yeah..."
      jacklyn king_of_sweets_cum_on_face1 "You're the artist now — tell me where you want the final splash of color!{space=-85}"
      menu(side="middle"):
        extend ""
        "\"I think a face painting is in order...\"":
          mc "I think a face painting is in order..."
          jacklyn king_of_sweets_stroke7 "That's photogenic as fuck."
          jacklyn king_of_sweets_stroke3 "All right... hit me, shooter!"
          window hide
          show jacklyn king_of_sweets_stroke4 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.1)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
          pause 0.1
          show jacklyn king_of_sweets_stroke3 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke4 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.1)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.1)
          pause 0.1
          show jacklyn king_of_sweets_stroke3 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke4 with Dissolve(.05)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.1)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke5 with Dissolve(.05)
          pause 0.1
          show jacklyn king_of_sweets_stroke4 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke3 with Dissolve(.025)
          show jacklyn king_of_sweets_stroke2 with Dissolve(.05)
          window auto
          mc "H-here it comes!"
          window hide
          show jacklyn king_of_sweets_cum_on_face1 with Dissolve(.5)
          show jacklyn king_of_sweets_cum_on_face2 with vpunch
          window auto
          jacklyn king_of_sweets_cum_on_face2 "Mmmm!"
          "Man, there's something special about a teacher wearing your cum on her face..."
          "[jacklyn] isn't even embarrassed, which makes it even hotter."
          show jacklyn king_of_sweets_selfie_cum with Dissolve(.5)
        "\"Swallow it!\"":
          mc "Swallow it!"
          jacklyn king_of_sweets_cum_on_face1 "Great art always fills the body and soul."
          window hide
          show jacklyn king_of_sweets_taste2 with Dissolve(.3)
          pause 0.1
          show jacklyn king_of_sweets_taste1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_taste2 with Dissolve(.3)
          pause 0.1
          show jacklyn king_of_sweets_taste1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_taste2 with Dissolve(.3)
          pause 0.1
          show jacklyn king_of_sweets_taste1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_taste2 with Dissolve(.3)
          pause 0.1
          show jacklyn king_of_sweets_taste1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_assisted_taste3 with Dissolve(.3)
          window auto
          mc "H-here it comes!"
          window hide
          show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.4)
          show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.3)
          pause 0.2
          show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
          show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
          show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.3)
          pause 0.1
          show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
          show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
          show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.3)
          pause 0.1
          show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.1)
          show jacklyn king_of_sweets_assisted_deepthroat1 with Dissolve(.2)
          pause 0.1
          show jacklyn king_of_sweets_assisted_deepthroat2 with Dissolve(.2)
          show jacklyn king_of_sweets_assisted_deepthroat3 with Dissolve(.3)
          show jacklyn king_of_sweets_assisted_deepthroat3_cum with hpunch
          pause 0.5
          show jacklyn king_of_sweets_assisted_deepthroat3_cum with vpunch
          pause 0.5
          show jacklyn king_of_sweets_cum_in_mouth with Dissolve(.5)
          window auto
          jacklyn king_of_sweets_cum_in_mouth "Mmmm... thanks for the meal, hot shot."
          "I feel like I came twice the usual load, but [jacklyn] swallowed it all like a pro."
          "She's even licking her lips..."
          show jacklyn king_of_sweets_selfie with Dissolve(.5)
      jacklyn "And now for the crowning moment..."
      jacklyn "A clean-filthy shot for the magazine!"
      window hide
  play sound "camera_snap"
  scene location with Fade(0.5,1.0,0.5,color="#fff")
  $jacklyn.equip("jacklyn_bra")

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
