init python:
  register_replay("maxine_experiment","Maxine's Experiment","replays maxine_experiment","replay_maxine_experiment")

label replay_maxine_experiment:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  python:
    school_hook_up_table["cable"] = False
    school_hook_up_table["watermelon_cut"] = False
    school_hook_up_table["watermelon_slice"] = False
    school_hook_up_table["ass_object"] = False
    school_hook_up_table["vr_goggles"] = False
    school_hook_up_table["router_connected"] = False
  show maxine hooking_up_replay_smile with fadehold
  maxine hooking_up_replay_smile "Listen closely. This morning I put a micro-transmitter in my cereal."
  maxine hooking_up_replay_smile "Throughout the day, that transmitter has traveled through my digestive system."
  mc "...You ate electronics for breakfast?"
  maxine hooking_up_replay_angry "Yes! Try to keep up!"
  maxine hooking_up_replay_smile "The transmitter is now situated somewhere in my large intestine."
  maxine hooking_up_replay_smile "And with the help of the equipment here, we're going to hook me up to the internet."
  menu(side="left"):
    extend ""
    "\"This makes absolutely no sense...\"":
      mc "This makes absolutely no sense..."
      maxine hooking_up_replay_angry "I told you to listen! Listen and you will understand."
      mc "I did listen, but it still makes no sense."
      mc "How are you even going to get electricity to the transmitter?"
      maxine hooking_up_replay_angry "Do you really think I haven't thought of that?"
      mc "Honestly, who knows at this point..."
      maxine hooking_up_replay_smile "You underestimate my meticulousness. Everything has been planned in great detail."
    "\"How about we hook up instead?\"":
      mc "How about we hook up instead?"
      maxine hooking_up_replay_flirty "Would you really like to?"
      mc "I mean, sure? Yeah! That's a definitely."
      maxine hooking_up_replay_flirty "I didn't bring a transmitter for you, but maybe I have one somewhere...{space=-50}"
      mc "No, I mean..."
      mc "Forget it."
      maxine hooking_up_replay_afraid "And here I got all excited for nothing..."
      mc "Sorry, I guess."
    "\"Can I have a slice of watermelon?\"":
      mc "Can I have a slice of watermelon?"
      maxine hooking_up_replay_afraid "No! That's out of the question!"
      mc "Why?"
      maxine hooking_up_replay_afraid "It's a vital part of the process."
      mc "I mean, surely you don't need all of it..."
      maxine hooking_up_replay_angry "If I didn't need all of it, then I wouldn't have gotten all of it."
      mc "Fair enough, I guess."
  maxine hooking_up_replay_smile "Let's proceed now."
  maxine hooking_up_replay_thinking "First off, we have this power supply..."
  maxine hooking_up_replay_thinking "I'm lubricating this end for better conductivity."
  maxine hooking_up_replay_thinking "You might have to lubricate it yourself, so look closely."
  mc "I'm not putting a power cable in my mouth."
  maxine hooking_up_replay_excited "That's fine! Any bodily fluid will suffice."
  mc "Uh... what other fluids are there?"
  maxine hooking_up_replay_thinking "You could use pre-ejaculation fluid... or even tears."
  mc "That's a combination I'm all too familiar with..."
  maxine hooking_up_replay_excited "Good! Let's get started!"
  maxine hooking_up_replay_excited "Just follow my instructions closely and no one will get electrocuted!{space=-5}"
  mc "Electrocuted?!"
  maxine hooking_up_replay_flirty_cable_insertion "Y-yes, hnngh... but don't be afraid..."
  mc "Whoa there!"
  "If anyone would just pull their pants down and show their ass for science, it would be [maxine]."
  "The power cable slid right into her ass..."
  mc "What are you doing?"
  maxine hooking_up_replay_orgasm_cable_insertion "I need electricity... for the transmitter..."
  maxine hooking_up_replay_orgasm_cable_insertion "I could have also swallowed the cable... but that's a choking hazard...{space=-20}"
  maxine hooking_up_replay_concerned_cable_insertion "Now... I just need to..."
  maxine hooking_up_replay_concerned_cable_insertion "Get it..."
  maxine hooking_up_replay_concerned_cable_insertion "Right..."
  maxine hooking_up_replay_afraid_cable_insertion "Damn it! I can't get it to attach to the transmitter!"
  mc "I wish I was more surprised..."
  maxine hooking_up_replay_smile_cable_offer "Can you help me out here?"
  mc "Sure, I guess."
  maxine hooking_up_replay_cable_flirty_hands_behind_back "Okay, try not to waste electricity!"
  mc "That's what you're worried about in this scenario...?"

  maxine hooking_up_replay_cable_concerned_hands_behind_back "Hey, don't touch that cable!"
  mc "Don't worry, I'm something of a tech guy myself."
  $school_hook_up_table["cable"] = True
  show maxine hooking_up_replay_cable_afraid_hands_behind_back with Dissolve(.25)
  mc "..."
  maxine hooking_up_replay_cable_afraid_hands_behind_back "I believe you've got me tangled up by mistake..."
  mc "It looks fine to me."

  mc "How about a snack?"
  $school_hook_up_table["watermelon_cut"] = True
  mc "..."
  $school_hook_up_table["watermelon_slice"] = True
  with vpunch
  maxine hooking_up_replay_cable_afraid_hands_behind_back "Mmmph!"
  mc "What's that? You want the watermelon in your butt?"

  $school_hook_up_table["watermelon_slice"] = False
  $school_hook_up_table["ass_object"] = "watermelon_slice"
  show maxine hooking_up_replay_cable_ahegao_hands_behind_back
  with vpunch
  maxine hooking_up_replay_cable_ahegao_hands_behind_back "Hnngh!"
  mc "What? We need the electricity to reach the transmitter."
  mc "Electricity travels well through liquids like watermelon juice."
  maxine hooking_up_replay_cable_out_of_it_hands_behind_back "You're... ugh... right..."

  maxine hooking_up_replay_cable_out_of_it_hands_behind_back "How's... it looking?"
  mc "I think we've got plenty of conductivity now."
  maxine hooking_up_replay_cable_afraid_hands_behind_back "Well, then you—"
  $school_hook_up_table["ass_object"] = False
  $school_hook_up_table["watermelon_slice"] = True
  show maxine hooking_up_replay_cable_concerned_hands_behind_back
  with vpunch
  maxine hooking_up_replay_cable_concerned_hands_behind_back "Mmmph!"
  mc "I agree. Too much talking."

  mc "Is this thing even on?"
  maxine hooking_up_replay_cable_smile_hands_behind_back "Mmmph!"
  mc "Well, on it goes!"
  $school_hook_up_table["vr_goggles"] = True
  "..."
  "That looks almost like a blindfold..."

  mc "Okay, I think we can plug this in now..."
  $school_hook_up_table["ass_object"] = "power_supply_cable"
  show maxine hooking_up_replay_concerned_hands_behind_back
  with vpunch
  mc "..."

  "Well, everything is in place. I just need to turn on the power now."
  "If [maxine] gets electrocuted in her ass, that's on her..."
  mc "Okay, are you ready?"
  maxine hooking_up_replay_concerned_hands_behind_back "Mmmph!"
  mc "I'll take that as a yes..."
  mc "Here we go."
  window hide
  show white with Dissolve(.25)
  $school_hook_up_table["router_connected"] = True
  $set_dialog_mode("default_no_bg")
  maxine hooking_up_replay_concerned_hands_behind_back "Mmmmmaaah!" with vpunch
  hide white with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Holy shit! That must've hurt..."
  "..."
  mc "Wait... it seems like the router is actually connected to something?"
  mc "I guess the transmitter does have power somehow..."
  maxine hooking_up_replay_concerned_hands_behind_back "Mmmph!"
  "I have no idea what she's saying..."
  menu(side="middle"):
    extend ""
    "Remove the watermelon slice":
      $school_hook_up_table["watermelon_slice"] = False
      maxine hooking_up_replay_orgasm_hands_behind_back "Oh! That was something else!"
      maxine hooking_up_replay_orgasm_hands_behind_back "I think I saw something!"
      mc "Really? What did you see?"
      maxine hooking_up_replay_smile_hands_behind_back "It was beautiful! It must've been a glimpse of cyberspace!"
      mc "Great."
      maxine hooking_up_replay_flirty_hands_behind_back "Turn the power on again!"
      mc "Seriously?"
      maxine hooking_up_replay_flirty_hands_behind_back "Yes! Higher voltage!"
      mc "If you insist..."
      window hide
      show white with Dissolve(.25)
      $set_dialog_mode("default_no_bg")
      show maxine hooking_up_replay_ahegao_hands_behind_back
      maxine hooking_up_replay_ahegao_hands_behind_back "Aaaaaah!" with vpunch
      hide white with Dissolve(.5)
      window auto
      $set_dialog_mode("")
    "Keep it in":
      "I kinda prefer [maxine] gagged, to be honest."
      "She's less annoying that way."
      maxine hooking_up_replay_concerned_hands_behind_back "Mmmph!"
      mc "It's time to increase the voltage!"
      window hide
      show white with Dissolve(.25)
      $set_dialog_mode("default_no_bg")
      maxine hooking_up_replay_concerned_hands_behind_back "Mmmmmmmmmph!" with vpunch
      hide white with Dissolve(.5)
      window auto
      $set_dialog_mode("")
  "Who knew science could be so erotic?"
  "The way her sphincter clenches around the power cable is something deeply perverted."
  "The inner walls of her asshole contract to electrical currents that pulsate through her."
  "I don't know if [maxine] is seeing anything at all in those goggles, but one thing is for sure..."
  "I'm seeing her for who she really is for the first time."
  "Underneath that conspiracy-nutter-exterior is a girl who is deeply dedicated to her work."
  "Someone who will stop at nothing to find the truth, even if it means taking electricity up your ass."
  "That's admirable in some ways, and quite hot."
  "Few people are as driven as [maxine], and perhaps if I spend more time with her it'll rub off on me."
  "...or I'll just become as crazy as her."
  "Either way, I wonder if she'd let me put my dick in her ass if I attached an electrode to it..."
  "That thought alone is worth sticking around for."
  "..."
  "I should probably check if she's okay..."
  $school_hook_up_table["vr_goggles"] = False
  $school_hook_up_table["watermelon_slice"] = False
  maxine hooking_up_replay_out_of_it_hands_behind_back "Uhhhh...."
  mc "Are you okay?"
  maxine hooking_up_replay_out_of_it_hands_behind_back "I... I saw it..."
  mc "What did you see?"
  maxine hooking_up_replay_out_of_it_hands_behind_back "The truth..."
  mc "Yeah? Did you see the light?"
  maxine hooking_up_replay_out_of_it_hands_behind_back "No..."
  maxine hooking_up_replay_out_of_it_hands_behind_back "It was a person..."
  mc "A person?"
  maxine hooking_up_replay_out_of_it_hands_behind_back "Yes... I saw through... the narrative lens..."
  mc "This again?"
  maxine hooking_up_replay_out_of_it_hands_behind_back "It's just a thin screen... separating us... from the truth..."
  mc "What are you talking about?"
  maxine hooking_up_replay_out_of_it_hands_behind_back "We're being watched..."
  mc "You always say that..."
  maxine hooking_up_replay_out_of_it_hands_behind_back "No... I mean really..."
  maxine hooking_up_replay_out_of_it_hands_behind_back "This very moment... there's a person... watching us..."
  mc "I don't understand anything."
  maxine hooking_up_replay_orgasm_hands_behind_back "That's... okay... You never will..."
  maxine hooking_up_replay_orgasm_hands_behind_back "The world... is a stage... and everyone's a player..."
  maxine hooking_up_replay_orgasm_hands_behind_back "But there is... more than one world... and I've seen our audience..."
  maxine hooking_up_replay_orgasm_hands_behind_back "And I now know... what the narrative lens is... or who..."
  "[maxine] seems completely out of it. I guess the electricity fried her brain or something."
  "And what is this narrative lens she keeps talking about?"
  "I should probably take her to the [nurse]'s office..."
  scene location with fadehold
  python:
    school_hook_up_table["cable"] = False
    school_hook_up_table["watermelon_cut"] = False
    school_hook_up_table["ass_object"] = False
    school_hook_up_table["router_connected"] = False

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
