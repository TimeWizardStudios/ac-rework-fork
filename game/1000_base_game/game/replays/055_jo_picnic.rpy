init python:
  register_replay("jo_picnic","Jo's Picnic","replays jo_picnic","replay_jo_picnic")

label replay_jo_picnic:
  show jo picnic_smile_holding_wine with fadehold
  mc "I cleaned my room. Everything's sparkling, so you don't have to lift a finger."
  jo picnic_smile_holding_wine "Wow! Color me impressed. I can't remember the last time you did that.{space=-50}"
  mc "Probably never. I've always taken everything for granted."
  mc "But not anymore. I'll try to do better. I want to turn things around."
  "She doesn't know that I've already seen the future that awaits me\nif I don't..."
  jo picnic_smile_holding_wine "I'm glad you're taking up more responsibility. It's an important part of your life as an adult."
  jo picnic_blush_smile_holding_wine "You've grown up so fast. Faster than I ever wanted."
  jo picnic_blush_smile_holding_wine "I'm proud to see the man you've become."
# jo picnic_blush_smile_holding_wine "Your father would be proud to see the man you've become." ## Incest patch ##
  "[jo] is never proud of me. I guess I really must've impressed her\nthis time."
# "[jo] never talks about Dad. I guess I really must've impressed her\nthis time." ## Incest patch ##
  jo picnic_flirty_holding_wine "Now that you're a grown-up, maybe it's time to start treating you like one..."
  mc "I'd like that. How's the wine?"
  jo picnic_blush_flirty_holding_wine "It's delicious. It goes down so... smoothly, too."
  jo picnic_blush_flirty_holding_wine "Mmm, so delectable..."
  jo picnic_blush_flirty_offering_wine "Did you try some?"
  mc "I'm okay, thanks. It's all yours, you deserve it."
  mc "You work so hard, and do so much for me and [flora]."
  jo picnic_smile_holding_wine"You really are something today, [mc]! I'm not sure what's gotten into you."
  jo picnic_smile_holding_wine "Not that I'm complaining!"
  mc "Just trying to be better..."
  window hide
  show jo picnic_surprised_spilling_wine with Dissolve(.5)
  window auto
  jo picnic_surprised_spilling_wine "Oop!"
  jo picnic_surprised_looking_down_spilling_wine "Oh, no, I've spilled some on myself! Clumsy me..."
  jo picnic_blush_smile_spilling_wine "I suppose I should take this thing off, keep it from staining too badly.{space=-20}"
  window hide
  show jo picnic_blush_smile_neutral_unbuttoning_shirt with Dissolve(.5)
  pause 0.25
  show jo picnic_blush_smile_removing_shirt with Dissolve(.5)
  window auto
  jo picnic_blush_smile_removing_shirt "There, that's better already."
  jo picnic_blush_smile_removing_shirt "Don't you think?"
  mc "Much better..."
  jo picnic_blush_flirty_removing_shirt "I should probably slow down!"
  "God, please don't slow down..."
  "Wait, what am I thinking? It doesn't matter, I can't look away..."
  window hide
  show jo picnic_blush_smile_holding_wine_shirtless with Dissolve(.5)
  window auto
  jo picnic_blush_smile_holding_wine_shirtless "Oooh, this wine has me feeling woozy..."
  jo picnic_blush_smile_holding_wine_shirtless "It was so good that I drank it a little too quickly."
  jo picnic_blush_smile_holding_wine_shirtless "Just a little more won't hurt, though..."
  mc "It's okay to have some more. I'm here for you."
  jo picnic_flirty_holding_wine_shirtless "You're going to take responsibility, right?"
  mc "Of course!"
  jo picnic_blush_flirty_holding_wine_shirtless "Such a mature, grown man you're becoming..."
  jo picnic_blush_flirty_holding_wine_shirtless "I'm so proud of you... taking care of me like this..."
  jo picnic_blush_flirty_holding_wine_shirtless "How did I get soo lucky?"
  "Damn, she might actually be drunk now."
  window hide
  show jo picnic_flirty_pouring_wine with Dissolve(.5)
  window auto
  jo picnic_flirty_pouring_wine "Oh!"
  jo picnic_blush_flirty_pouring_wine "Would you look at that...? I've gone and spilled some more all over my bra..."
  jo picnic_blush_flirty_pouring_wine "What a clumsy thing I am today."
  jo picnic_blush_flirty_pouring_wine "Thank goodness I have you here, looking after me..."
  window hide
  show jo picnic_blush_flirty_removing_bra with Dissolve(.5)
  window auto
  jo picnic_blush_flirty_removing_bra "I should probably take this off though, don't you think?"
  menu(side="middle"):
    extend ""
    "\"Err, yes... just in case!\"":
      mc "Err, yes... just in case!"
    # "\"Actually, [jo], I don't think you would feel good about it later.\"":
    "\"Actually, [jo], I don't think you\nwould feel good about it later.\"":
      mc "Actually, [jo], I don't think you would feel good about it later."
      jo picnic_surprised_removing_bra "Ah, yes. You're right. That would be inappropriate."
      mc "It's okay. You'll survive with a stain on it for just a bit."
      window hide
      show jo picnic_neutral_looking_down_resting_hands_bra_on with Dissolve(.5)
      window auto
      jo picnic_neutral_looking_down_resting_hands_bra_on "Sorry, I've really had too much to drink..."
      jo picnic_blush_smile_resting_hands_bra_on "I was just so happy that you put this thing together for me."
      jo picnic_blush_smile_resting_hands_bra_on "You're such a sweet... sweet boy..."
      "Despite her drunken state, it does feel good hearing [jo] say these things."
      mc "I'm trying to do better this time. To be a better person."
      jo picnic_blush_smile_resting_hands_bra_on "This time?"
      show teary_eyes1 behind teary_eyes2:
        alpha 0.0 yalign 1.0
        pause 0.4
        easein 0.5 alpha 1.0
      show teary_eyes2 with {"master": close_eyes}:
        ysize 1080
      mc "Yeah... it's really rare to get second chances like this..."
      jo picnic_surprised_resting_hands_bra_on "My darling boy, are you crying?"
      "Damn. I actually am."
      mc "Sorry... it just brings me so much joy seeing you happy..."
      mc "When [flora] left... and I didn't do anything with my life..."
      mc "I think it really crushed you..."
      jo picnic_surprised_resting_hands_bra_on "When [flora] left? You have me all sorts of confused, [mc]."
      jo picnic_surprised_resting_hands_bra_on "Maybe it's the wine..."
      "[jo] turned into a gray shell of her former self, only ever lighting up during the holidays when [flora] visited."
      "And it was all my fault for being such a lazy and selfish piece of shit."
      "No one is perfect, but I still feel like it was my fault that she became depressed."
      show teary_eyes1:
        easeout 0.5 alpha 0.0
      hide teary_eyes2 with {"master": open_eyes}
      mc "Err, yes. It must be the wine."
      mc "I'm really glad we did this, [jo]."
      jo picnic_smile_resting_hands_bra_on "Aww! Me too, me too."
      jo picnic_smile_resting_hands_bra_on "The school year has barely started and I already felt drained before this..."
      mc "I'll try to make things easier for you this year, I promise."
      mc "I know I've been difficult in the past."
      jo picnic_flirty_resting_hands_bra_on "[mc]!"
      jo picnic_flirty_resting_hands_bra_on "Such a big heart on my big man..."
      mc "Anyway, I think we should head home now. It's already getting dark."
      jo picnic_smile_resting_hands_bra_on "Oh? I guess you're right."
      jo picnic_blush_smile_resting_hands_bra_on "There's only one problem... the ground is a bit unstable... or maybe that's just me..."
      mc "Don't worry. I've got you."
      window hide
      show black onlayer screens zorder 100
      show jo sunset_walk
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      "There is a certain feeling that you can only get by being selfless."
      # "It starts as a lightness in your chest, and then spreads to your shoulder and face, relaxing your posture and tickling your lips into a smile."
      "It starts as a lightness in your chest, and then spreads to your shoulder{space=-75}\nand face, relaxing your posture and tickling your lips into a smile."
      "This feeling is almost like a drug that lifts your spirits without any of the bad side effects."
      "It sparks a hope inside you that maybe, just maybe, you're really a bad person."
      "There was some goodness all along, and all you had to do was act on it."
      "And you wonder why you didn't act on it sooner."
      "I've never believed in karma, but maybe I just didn't understand what it was until now?"
      "It's not some cosmic force of justice."
      "It's just you feeling better about yourself, and in turn the world around you feels a little more positive, and you pay more attention to when good things happen to you."
      "..."
      "Or maybe I'm just high on positivity right now, and I'll crash and burn before I know it..."
      "But at least [jo] is happy and proud of me, and maybe that's enough."
      scene location with fadehold
      return
  jo picnic_flirty_removing_bra "Mmm, exactly... just in case..."
  window hide
  show jo picnic_blush_flirty_squeezing_boobs with Dissolve(.5)
  window auto
  jo picnic_blush_flirty_squeezing_boobs "God, that's muuuch better. So... freeing."
  "Wow! Her breasts are right there, ready for the taking!"
  "..."
  "Why am I looking at [jo] this way? Maybe I'm buzzed, too."
  mc "I don't know if it's the way the sun's hitting you this late in the day, but you look astonishing."
  jo picnic_blush_smile_resting_hands "Aww! You're making me blush!"
  mc "I can't imagine how anyone could be mean to you."
  mc "You're just so beautiful. How could they?"
  mc "I bet the person who wrote that email feels terrible about it now."
  jo picnic_blush_smile_resting_hands "Oh, I doubt it... but I don't want to think about that right now..."
  jo picnic_blush_smile_resting_hands "I just want to enjoy this relaxing moment."
  mc "I know, but I'm kind of glad that she got you so fired up. If she hadn't, we wouldn't be enjoying this moment right now."
  jo picnic_flirty_resting_hands "It does feel good to just slow down and enjoy this time with you..."
  mc "It does. And you worked so hard to get to where you are now."
  mc "I hope that all of your ambition rubbed off on me."
  mc "[flora] and I really don't know how lucky we are to have someone like you taking care of us."
  jo picnic_blush_flirty_resting_hands "Sweetie, you're being too generous. An old lady like me?"
  mc "You're so vibrant and full of life. You're hardly old."
  mc "I mean, just look at your breasts! They're so perky compared to other women your age."
  jo picnic_surprised_squeezing_boobs "[mc]... that's... that's so inappropriate!"
  mc "But it's true!"
  jo picnic_surprised_squeezing_boobs "That's not the point! You shouldn't even have eyes wandering down there!"
  mc "How could I not? They're huge. When they knock against each other,{space=-10}\nI can't help it..."
  mc "Besides, if you didn't want me to look, you wouldn't have them out."
  mc "Yet here they are. I'm staring right at them, and you're not stopping me.{space=-70}"
  jo picnic_neutral_looking_down_squeezing_boobs "It's just that they've been constricted all day... and then I spilled that wine on, on accident..."
  mc "On accident, of course."
  jo picnic_blush_smile_squeezing_boobs "Yesss, of course. Accidents happen and that's... that's okay."
  mc "I know one thing..."
  mc "...if there is a God, the way he created you definitely wasn't an accident."
  jo picnic_smile_resting_hands "Well, isn't that a thing to say!"
  jo picnic_smile_resting_hands "How very sweet of you, [mc]."
  jo picnic_smile_resting_hands "I think I raised you right, after all."
  mc "You certainly did."
  jo picnic_blush_smile_resting_hands "Goodness, but that wine is so strong..."
  jo picnic_blush_smile_resting_hands "And on an empty stomach. It's really gotten to my head, I think."
  jo picnic_blush_smile_resting_hands "How irresponsible!"
  mc "It's okay to just let loose sometimes."
  jo picnic_blush_flirty_resting_hands "So loose..."
  mc "Remember, you deserve it."
  mc "You should lie back and relax. Let the evening breeze wash over your skin."
  jo picnic_blush_flirty_resting_hands "Oh, but that does sound so wonderful..."
  jo picnic_blush_flirty_resting_hands "Maybe just for a moment..."
  window hide
  show black onlayer screens zorder 100
  show jo boobjob_smile_lying_down
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  jo boobjob_smile_lying_down "Mmmm... that does feel good..."
  jo boobjob_smile_lying_down "That wine has really rushed to my head... makes it hard to th-think too clearly."
  "She keeps saying that, but I can see how much she's enjoying this."
  mc "Don't think too much, just keep relaxing."
  jo boobjob_flirty_lifting_boob "Ohhh... that feels good, and really helps me relax. Sometimes at night, all on my own, well..."
  mc "Yeah?"
  jo boobjob_laughing_lifting_boob "Oh! Never mind, you naughty boy."
  mc "I could help relax you some more, you know."
  jo boobjob_flirty_lifting_boob "Mmmm, yeah? Please..."
  window hide
  show jo boobjob_surprised_looking_down_dick_reveal with vpunch
  window auto
  jo boobjob_surprised_looking_down_dick_reveal "Ohh!"
  jo boobjob_flirty_looking_down_grabbing_dick "It's been so long since someone showed me a cock like yours..."
  jo boobjob_flirty_looking_down_grabbing_dick "..."
  jo boobjob_concerned_grabbing_dick "Wait, did I just say that out loud?"
  mc "What are you gonna do about it?"
  jo boobjob_smile_grabbing_dick "Well... there was one thing my husband loved to do..."
# jo boobjob_smile_grabbing_dick "Well... there was one thing your father loved to do..." ## Incest patch ##
  jo boobjob_flirty_looking_down_grabbing_dick "He liked to stick it right between..."
  jo boobjob_flirty_looking_down_dick_top "Yes... just like that..."
  window hide
  #1
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #2
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #3
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #4
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #5
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  window auto
  jo boobjob_flirty_looking_down_dick_top "Oh, my! What a big man you've grown into..."
  jo boobjob_flirty_looking_down_dick_top "And I do mean big!"
  window hide
  #1
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #2
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #3
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #4
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #5
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #6
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #7
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  window auto
  jo boobjob_flirty_looking_down_dick_top "Shove it right between them and really make them bounce!"
  jo boobjob_flirty_looking_down_dick_top "Up and down! Up and down!"
  window hide
  #1
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #2
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #3
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #4
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #5
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.5)
  window auto
  jo boobjob_drunk_dick_top "Keep going! You're doing great!"
  jo boobjob_drunk_dick_top "Don't you dare stop!"
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #4
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #5
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #6
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  window auto
  "Her boobs feel so perfect, like they were made just for me..."
  "There's no way I could stop now, even if I wanted to!"
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #4
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #5
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #6
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #7
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  window auto
  jo boobjob_drunk_dick_top "Come on, sweetheart! You can do it!"
  jo boobjob_drunk_dick_top "Show me how it's done!"
# jo boobjob_drunk_dick_top "Show momma how it's done!" ## Incest patch ##
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #4
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #5
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #6
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #7
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #8
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  window auto
  "Fuck! She's squeezing them against me with everything she's got!"
  "I can't believe I'm gonna blow my load right on [jo]'s face!"
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  show jo boobjob_drunk_cum with vpunch
  pause 0.25
  show jo boobjob_flirty_aftermath with Dissolve(.5)
  window auto
  jo boobjob_flirty_aftermath "Mmmm, yes! Just like that!"
  mc "Goddamn... you really drained it all out of me..."
  jo boobjob_smile_aftermath "That's what a woman with experience is supposed to do, honey."
  scene location with fadehold
  return
