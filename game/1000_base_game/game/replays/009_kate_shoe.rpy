init python:
  register_replay("kate_shoe","Kate's Kiss","replays kate_shoe","replay_kate_shoe")

label replay_kate_shoe:
  scene kate shoekiss_shoe with qdis
  "This is the most humiliating thing I've ever experienced, but being so close to [kate] makes it all worth it. Even if it's just her foot."
  kate shoekiss_shoe "Don't drool."
  mc "I won't. I promise."
  kate shoekiss_shoe "Okay, go ahead."
  mc "Thank you, ma'am!"
  kate shoekiss_shoe "..."
  "Seeing her from this angle truly puts the meaning of dominance into perspective."
  "The shackling sharpness of her eyes is enough to send anyone to their knees."
  "Maybe people like her are meant to be admired from below. She does look even more beautiful towering over you."
  "Her posture says it all. This is how it's meant to be."
  "She doesn't care if anyone sees us. She doesn't care that her shirt rides up her midriff, or that her pants stretch over her thighs."
  "Would she have put me through my paces this way if she was wearing her cheerleader uniform?"
  "What kind of panties hide beneath her pants? Strict black ones like her soul... or girly ones with little ribbons?"
  "God, the almost imperceptible scent of her feet makes my head swim..."
  "I just need to experience it fully..."
  menu(side="far_right"):
    "\"Please, ma'am... could you... um... maybe, take off your shoe?\"":
      mc "Please, ma'am... could you... um... maybe, take off your shoe?"
      kate shoekiss_shoe "That's disgusting. You're disgusting."
      kate shoekiss_shoe "Why would I do that?"
      mc "I... I'd just really like it, ma'am."
      kate shoekiss_shoe "Well... I suppose you have been a good boy lately."
      kate shoekiss "But don't get any funny ideas."
      kate shoekiss "I do not want to feel your mouth or tongue slobbering all over my toes. Understood?"
      mc "Yes, ma'am! Thank you!"
      kate shoekiss "Go on, then. Have a whiff, you creep."
      "Oh man, the aroma..."
      "The leather of her shoes... a tiny hint of sweat and salt... and that distinctive yet subtle fragrance of feet..."
      "Even her toes smell heavenly. Those dainty little things."
      "If only she'd allow me to stick my tongue out. Clean her toes and in between them."
      "Run it up and down the length of her sole."
      "Taste the wrinks of her arches."
      "Kiss the rough flesh of her heel. Have her step on my face."
      kate shoekiss "I told you not to drool."
      mc "Sorry, ma'am."
      kate shoekiss "At least, you didn't get it on me. But I think that's enough."
      mc "Please, ma'am! I'll keep my mouth away."
      kate shoekiss "You're pathetic."
      kate shoekiss "..."
      "God, she really knows how to degrade people..."
      "Hopefully, there's another reward waiting for me!"
    "\"Thank you for showing me my place, ma'am.\"":
      mc "Thank you for showing me my place, ma'am."
      kate shoekiss_shoe "You're very welcome, pet."
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
