init python:
  register_replay("jo_embarrassment","Jo's Embarrassment","replays jo_embarrassment","replay_jo_embarrassment")

label replay_jo_embarrassment:
  $x = quest.jo_washed["car_wash_volunteers"]
  $quest.jo_washed["car_wash_volunteers"] = {"maxine","kate"}
  show flora car_wash flora excited_right
  show misc flashback zorder 1
  with fadehold
  flora car_wash flora excited_right "Thanks for waiting, everyone!"
  flora car_wash flora excited_right "Welcome to the Newfall High car wash fundraiser!"
  flora car_wash flora excited_right "You'll get a scrub and a rinse!"
  flora car_wash flora excited_right "All donations will go to our school dance at the end of the year!"
  "The funny thing was, it was just the two of us there, and a line of cars longer than [kate]'s strap on."
  if "maxine" in quest.jo_washed["car_wash_volunteers"]:
    "But then a miracle happened..."
    maxine "That's a lot of cars. I didn't know car washes were so in demand."
    window hide
    show flora car_wash flora excited_right maxine neutral with Dissolve(.5)
    window auto
    mc "[maxine]?"
    "I truly didn't expect her to show up."
    "Especially not in... {i}that.{/}"
    show flora car_wash flora excited_right maxine surprised with Dissolve(.5)
    maxine "Has no one taught you to expect the unexpected?"
    "Not when a zombie hunting weirdo comes dressed in a hot bikini."
    flora car_wash flora excited_right maxine surprised "[maxine]! We could really use a hand! Thanks for coming!"
    "I always wished she'd get that excited when seeing me. Alas."
    $guard.default_name = "Driver"
    guard "Make sure to get the windshield real good!"
    guard "Lots of dead bugs splattered on there."
    show flora car_wash flora excited_right maxine smile with Dissolve(.5)
    maxine "Their bodies will be exhumed, and your windshield will shine again."
    guard "Uh... okay."
    $guard.default_name = "Guard"
    "That guy looked completely shocked, but I was already enjoying myself... and the view."
  if "kate" in quest.jo_washed["car_wash_volunteers"]:
    kate "I thought there would be nicer cars..."
    "And then, of course, [kate] showed up."
    window hide
    show flora car_wash flora excited_right maxine smile kate annoyed hands_on_hips with Dissolve(.5)
    window auto
    "But it wasn't all bad. [kate] in a bikini is a treasured memory."
    kate "I'm just here to get some sun and watch others work."
    show flora car_wash flora excited_right maxine smile kate smile hands_on_hips with Dissolve(.5)
    kate "Oh, and to make sure you don't wash off that marker, [mc]."
    mc "..."
    flora car_wash flora surprised maxine smile kate smile hands_on_hips "I wasn't going to say anything, but..."
    flora car_wash flora surprised maxine smile kate smile hands_on_hips "That's a bit weird, [mc]."
    mc "It's not like I wanted this, okay?"
    flora car_wash flora smile maxine smile kate smile hands_on_hips "So, since you're [kate]'s property, does that mean you won't be coming home?"
    kate "I do have an empty dog house."
    mc "No way! I'm definitely coming home!"
    mc "If I don't, call the cops and say I've been kidnapped."
    show flora car_wash flora smile maxine smile kate neutral with Dissolve(.5)
    kate "Don't flatter yourself."
    flora car_wash flora smile maxine smile kate neutral "You'd probably have to pay the kidnappers for that to happen, [mc].{space=-5}"
    mc "Ugh..."
    "If the fundraiser had taken place in the new timeline, I bet [isabelle] would've been there to back me up."
  else:
    "If the fundraiser had taken place in the new timeline, I bet [isabelle] would've been there as well."
  "It would probably have gone down like this..."
  isabelle "The cavalry is here!"
  window hide
  if "kate" in quest.jo_washed["car_wash_volunteers"]:
    show flora car_wash flora smile maxine smile kate neutral isabelle neutral with Dissolve(.5)
  else:
    show flora car_wash flora excited_right maxine smile kate neutral isabelle neutral with Dissolve(.5)
  window auto
  isabelle "I heard you needed some help."
  "She'd be showing off that perfect milky white skin, in an appropriately{space=-40}\nrevealing bikini."
  flora car_wash flora excited_right maxine smile kate neutral isabelle neutral "Thanks for showing up, [isabelle]! It's a huge help."
  show flora car_wash flora excited_right maxine smile kate neutral isabelle smile with Dissolve(.5)
  isabelle "Of course, love! Where should I start?"
  flora car_wash flora smile maxine smile kate neutral isabelle smile "You can start by sucking [mc]'s dick."
  isabelle "Perfect! Let me just get my lips wet..."
  "Okay, maybe that's a bit over the top."
  "Although, what if [jacklyn] showed up as well?"
  "That would've been something."
  jacklyn "Wicked wheels over here."
  window hide
  show flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn annoyed with Dissolve(.5)
  window auto
  jacklyn "They could use some artistic twang, though."
  "She'd be cool and confident as ever, probably only wearing a bikini to stop others from getting flustered."
  "Although, that clearly wouldn't have worked on [flora]."
  flora car_wash flora surprised_left maxine smile kate neutral isabelle neutral jacklyn annoyed "J-[jacklyn]?!"
  "She would have blushed so hard seeing [jacklyn] like this."
  show flora car_wash flora surprised_left maxine smile kate neutral isabelle neutral jacklyn smile_left with Dissolve(.5)
  jacklyn "What's swinging, sister?"
  jacklyn "Let's take this car to school, yeah?"
  flora car_wash flora smile_left maxine smile kate neutral isabelle neutral jacklyn smile_left "Y-yeah, let's do it!"
  flora car_wash flora smile_left maxine smile kate neutral isabelle neutral jacklyn smile_left "T-thanks for helping us out, [jacklyn]."
  "You should be thanking me for bringing this fantasy to life."
  "...or, well, maybe I should thank myself."
  "But back to the real memory for a bit..."
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile "We need some water over here, [mc]!"
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile "Can you man the hose?"
  "The air was so hot that day, she probably couldn't wait to get wet."
  window hide
  show flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_anticipation with Dissolve(.5)
  window auto
  mc "All right, here it goes!"
  "Ready..."
  "..aim..."
  show flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_spraying
  "...fire!" with vpunch
  show flora car_wash flora embarrassed maxine surprised kate neutral isabelle surprised jacklyn smile hose_everyhwere wet
  flora car_wash flora embarrassed maxine surprised kate neutral isabelle surprised jacklyn smile hose_everyhwere wet "Ahhh!" with hpunch
  mc "That's right, soak it up!"
  "No matter what happens, I'll always have the mental image of her dripping in front of me."
  "And it's one I really like to take my time with and relive."
  flora car_wash flora annoyed maxine surprised kate neutral isabelle surprised jacklyn smile hose_dripping wet "[mc], come on!"
  flora car_wash flora annoyed maxine surprised kate neutral isabelle surprised jacklyn smile hose_dripping wet "At least try to hit the car!"
  mc "How about I try to hit your open mouth instead?"
  flora car_wash flora cringe maxine surprised kate neutral isabelle surprised jacklyn smile hose_dripping wet "Don't you even—"
  show flora car_wash flora embarrassed_down maxine smile kate neutral isabelle neutral jacklyn smile hose_flora_mouth wet
  flora car_wash flora embarrassed_down maxine smile kate neutral isabelle neutral jacklyn smile hose_flora_mouth wet "Eeeek!" with vpunch
  "Watching [flora] running around, trying to escape the cold spray of the water, did something to me."
  "Her youthful laughter and looks that could kill."
  "Man, I really lost everything when high school ended."
  flora car_wash flora annoyed maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "[mc]! Stop it!"
  "I could think of her running around in that soaked bikini all day..."
  "But there was one thing missing."
  mc "..."
  mc "Where's [jo]?"
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "I'm not sure..."
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "She said she'd be here, but I think we can manage."
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "[jo] taking a breather is fine. She deserves it."
  "Well, I deserve to see her in a bikini."
  show flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet with Dissolve(.5)
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "It is a little strange, though."
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "She was supposed to be here."
  mc "I'll go look for her."
  "I remember being a bit grumpy about having to leave the girls..."
  "...but the day didn't feel complete without [jo]."
  mc "Any idea where she might've gone?"
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "Sorry, no clue."
  flora car_wash flora excited_right maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "But! I'll be happy to take over the hose until you get back!"
  "She said, mischievously."
  mc "Fine, but treat her like you would treat my—"
  flora car_wash flora annoyed maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "Why do you have to make everything weird?"
  window hide
  show flora car_wash flora hose maxine smile kate neutral isabelle neutral jacklyn smile wet new_cars
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "Give me the hose back!"
  flora car_wash flora hose maxine smile kate neutral isabelle neutral jacklyn smile wet new_cars "Saying please wouldn't hurt, you know?"
  mc "Pretty please with melons on top."
  flora car_wash flora hose maxine smile kate neutral isabelle neutral jacklyn smile wet new_cars "Ugh! Just shut up and spray..."
  window hide
  show flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet new_cars with Dissolve(.5)
  window auto
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet new_cars "What took you so long, anyway?"
  mc "I had to search the school for [jo]."
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet new_cars "Was something wrong?"
  mc "Well..."
  jo "Everything is fine, sweetie!"
  window hide
  show flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile jo smile hose_dripping wet new_cars with Dissolve(.5)
  window auto
  jo "Just, err... some administrative stuff I had to take care of!"
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo smile hose_dripping wet new_cars "What the hell are—"
  mc "...we doing just standing around?!"
  mc "Let's get this show on the road!"
  show flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo excited hose_dripping wet new_cars with Dissolve(.5)
  jo "Yes!"
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo excited hose_dripping wet new_cars "..."
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo excited hose_dripping wet new_cars "Okay, then..."
  "Mission accomplished."
  "And [jo] fit right in."
  "All she needed was to get a little more wet..."
  "Luckily, I was in the perfect position to help her out."
  window hide
  show jo car_wash jo smile hose_dripping
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide flora
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  mc "I see some dirt over to the right, [jo]!"
  jo car_wash jo surprised hose_dripping "Huh? Where?"
  jo car_wash jo smile bending_over hose_dripping "Oh, I see it! Thank you, honey!"
  "For a moment, I thought that her ass would snap that bikini\nbottom off."
  "I mean, who designed that thing anyway?"
  "Must've been a true genius in string theory."
  "Even when she bent over slightly, I could see her labia peeking out."
  "Those perfect folds of heavenly flesh..."
  "Ripples set by the gods themselves..."
  "The ultimate forbidden fruit."
  "And then I got one of my best ideas ever."
  "It was a little mean, but..."
  "Sometimes, you just have to be mean!"
  "I had all of this power in my hands, what kind of person would I have been if I squandered it?"
  mc "You're doing great, [jo]!"
  mc "Here's a bit more water!"
  show jo car_wash jo surprised bending_over hose_spraying wet
  jo car_wash jo surprised bending_over hose_spraying wet "Oh!" with vpunch
  jo car_wash jo laughing bending_over hose_dripping wet "The water feels great! Thank you, [mc]!"
  mc "You're very welcome!"
  "There wasn't quite enough water yet, but I was making progress."
  "And I thought, maybe a direct hit would do the trick..."
  show jo car_wash jo afraid bending_over hose_jo_panties wet
  jo car_wash jo afraid bending_over hose_jo_panties wet "Ah!" with vpunch
  jo car_wash jo afraid exposed hose_jo_pussy wet "Oh, no!"
  "Oh, yes!"
  "The bikini bottom went straight off. I felt like Chris Kyle."
  "Not that it was covering much... but it sure made a difference in\nher posture."
  jo car_wash jo embarrassed covering_up hose_jo_pussy wet "[mc], please!"
  mc "This hose is out of control!"
  mc "It has a mind of its own!"
  "I could see the goosebumps exploding on her skin."
  "The droplets of water rolling down her ass and falling from her pussy..."
  "The streaks wrapping around her legs..."
  "That look of desperation..."
  "Visual poetry."
  jo car_wash jo embarrassed covering_up hose_jo_pussy wet "Mmmm!"
  "And the best part, [jo] was getting turned on."
  "The water pressure must've been high enough on her vulva."
  jo car_wash jo blush covering_up hose_jo_pussy wet "I... I can't..."
  "She was just about to come, right there in front of the entire town!"
  $guard.default_name = "Driver"
  guard "Hell yeah, lady! Keep it going!"
  guard "I'll give ya everything I got, just don't stop!"
  $guard.default_name = "Guard"
  jo car_wash jo blush covering_up hose_jo_pussy wet "Oooh..."
  "[jo] shifted slightly toward the beam."
  "It was a glorious thing to witness."
  "Getting a woman off from ten feet away... surely, I was breaking some sort of obscure record?"
  "Either way, if it went on for much longer, my dick was going to rip through my pants."
  "Then I'd have two hoses, and no standing army could stop me."
  jo car_wash jo embarrassed covering_up hose_jo_pussy wet "I can't! It's too much!"
  "She was cumming!"
  show jo car_wash jo orgasm covering_up hose_jo_pussy wet
  jo car_wash jo orgasm covering_up hose_jo_pussy wet "Oh, god! Ohhh!" with vpunch
  "I was right there with her."
  "The hose water's falling down her back and mixing with her cum..."
  "It must've felt amazing."
  jo car_wash jo orgasm covering_up hose_dripping wet "Huff... puff..."
  "Her knees were wobbling. She was barely able to keep herself up after that orgasm."
  mc "Are you okay, [jo]?"
  jo car_wash jo aftermath covering_up hose_dripping wet "I... I think so..."
  jo car_wash jo aftermath covering_up hose_dripping wet "This is... very inappropriate..."
  mc "Here, let's get you inside and wrapped up in a towel, all right?"
  jo car_wash jo aftermath covering_up hose_dripping wet "T-thank you... sweetie..."
  jo car_wash jo aftermath covering_up hose_dripping wet "But... there are still... more cars..."
  mc "Don't worry about them. We can manage from here."
  jo car_wash jo aftermath covering_up hose_dripping wet "Okay... good..."
  "More cars, and more time to soak girls in bikinis."
  "Even though I never got a cent's worth out of the fundraiser, I felt like a very rich man."
  scene location with fadehold
  $quest.jo_washed["car_wash_volunteers"] = x
  return
