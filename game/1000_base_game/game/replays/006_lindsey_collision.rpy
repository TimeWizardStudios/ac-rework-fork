init python:
  register_replay("lindsey_collision","Lindsey's Collision","replays lindsey_collision","replay_lindsey_collision")

label replay_lindsey_collision:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show lindsey lindseypose01 with hpunch
  lindsey lindseypose01 "Oww..."
  "Damn, that took the wind out of me. [lindsey]'s always been fast, but luckily for me, she's tiny."
  "That's the second time she's crashed today. She never seemed like the clumsy kind. Maybe her eye-sight is getting bad."
  "In any case, mine isn't, and that's a relief. Else I wouldn't be able to feast my eyes on her."
  "It does feel a little bad, but everyone else is staring too. Who can blame us when she spreads her legs like that?"
  "It's like her dazed state has shattered her inhibitions. Her body is calling out for someone to... just give it to her."
  "Like she's longing to wrap her legs around your waist as you slam into her. Maybe she has a thing for slamming into stuff?"
  "Maybe she likes it rough and saw the opportunity to get physical with me?"
  "Oh, who am I kidding? That's just me projecting my fantasies — push her down, grab her by the throat, thrust into her repeatedly until her eyes roll back."
  "Might be a trick of the light, but isn't there a wet spot... just in the right spot?"
  "The contour of her labia against her panties... oh man... "
  show isabelle at right
  lindsey lindseypose01 "Who put a wall there...?"
  isabelle concerned_left  "Hey, are you okay?"#(looking left)
  lindsey lindseypose01 "My head..."
  isabelle afraid "[mc], she looks really hurt!"
  menu(side="far_left"):
    extend ""
    "\"Wait here. I'll go get the [nurse].\"":
      mc "Wait here. I'll go get the [nurse]."
      isabelle afraid "Okay, but hurry!"
    "\"Help me get her to the [nurse]'s office.\"":
      mc "Help me get her to the [nurse]'s office."
      "[lindsey] probably never cared about my existence until it hit her in the face, but this is what second chances are about."
      isabelle concerned_left "Hey, what's your name?"#(looking left)
      lindsey lindseypose01 "[lindsey]..."
      isabelle smile_left "Okay, [lindsey], do you think you can stand if we help you?" #(looking left)
      lindsey lindseypose01 "Maybe..."
      isabelle smile_left "Okay, put one arm around each of our necks. We'll take you to the nurse."#(looking left)
    "\"She should've paid more attention.\"":
      mc "She should've paid more attention."
      isabelle angry "What's wrong with you?"
      mc "Why should I care about someone who has never given two shits about me?"
      isabelle cringe "Because she's hurt. Don't you have any compassion?"
      mc "She would probably have laughed if I ran into a beefy football player."
      isabelle annoyed "Here's your chance to set things right then. Be the bigger person."
    "Carry [lindsey]":
      hide isabelle with Dissolve(.25)
      show lindsey LindseyPose02a with fade
      lindsey LindseyPose02a "What happened?"
      mc "You had an accident. I'm taking you to the [nurse]."
      "This is probably the closest I've ever been to a girl, and it's really not what I expected."
      "I just want to hold her, breathe in the scent of her hair, the flowery shampoo and natural oils."
      "All those dirty thoughts feel wrong now. "
      "She's so light in my arms. So soft and fragile. Like porcelain."
      "It's a good feeling — a pure and selfless one — to care for someone in need."
      "She's trusting me with her vulnerability. That's the best part."
      "Makes me want to kiss her on the forehead and comfort her. Be a gentleman and a hero!"
      "Perhaps, that's what this feeling is — a taste of heroism."
      lindsey LindseyPose02b "Everything's spinning..."
      menu(side="right"):
        extend ""
        "\"Don't worry, I've got you.\"":
          mc "Don't worry, I've got you."
          lindsey LindseyPose02a "Thank you... I think I hit my head."
          mc "You did."
          mc "Smacked right into my chest."
          lindsey LindseyPose02b "Oh... did I... did I hurt you?"
          mc "No, you're good. Sorry I couldn't soften the blow."
          lindsey LindseyPose02a "It's good that you're strong enough to carry me."
          lindsey LindseyPose02a "I don't think I could walk right now."
          mc "No worries. I'll carry you right back to the gym as soon as the [nurse] clears you."
          lindsey LindseyPose02c "I bet you would!"
        "\"Never seen anyone run so fast!\"":
          mc "Never seen anyone run so fast!"
          lindsey LindseyPose02a "Chivalry and flattery? What's next?"
          mc "Hopefully, the [nurse] saying that it's a mild concussion and you'll be okay in a couple of days."
          mc "You have a sprinting competition to win this year."
          lindsey LindseyPose02a "Hey, thanks for taking me there. That's so nice of you."
          "I've never seen a girl look at me like this before."
          "Who knew admiration would feel so fulfilling?"
          "She must've hit her head really hard."
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
