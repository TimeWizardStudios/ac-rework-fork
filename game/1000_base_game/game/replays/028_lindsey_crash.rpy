init python:
  register_replay("lindsey_crash","Lindsey's Crash","replays lindsey_crash","replay_lindsey_crash")

label replay_lindsey_crash:
  $x = game.location
  $game.location = school_ground_floor
  show lindsey lindseyfall
  $renpy.transition(Dissolve(0.5))
  pause(.5)
  lindsey lindseyfall "Eeep!"
  show lindsey lindseycrashed_eyesclosed with fadehold: #this gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 18
    linear 0.05 yoffset 0
  hide lindsey
  show lindsey lindseycrashed_eyesclosed #this is just to avoid the screen shake if the player is skipping
  "That right there is perk-fection!"
  "Boobery at its very finest."
  "Titillating to say the least."
  "[lindsey]'s crashes always put her in the best positions."
  "Maybe she's trying to show me her hot little body."
  "Fuck. I can watch her like this all day long."
  lindsey lindseycrashed_waking "Ouch..."
  menu(side="far_right"):
    extend ""
    "\"Are you okay?\"":
      mc "Are you okay?"
      lindsey lindseycrashed_angry "Yeah... I think."
      lindsey lindseycrashed_angry "Probably just a few bruises..."
      lindsey lindseycrashed_angry "I need to tie my shoelaces better."
      "She looked embarrassed again."
      "I guess it doesn't look good when the star athlete keeps tripping over her own feet."
      mc "You need to be more careful. Next time you might not be so lucky."
      lindsey lindseycrashed_waking "Nngh."
      lindsey lindseycrashed_waking "I need to get to practice..."
    "Start filming her":
      "Okay, this should be good..."
      lindsey lindseycrashed_waking_rec "Oww..."
      mc "Here we have the so-called star athlete of Newfall High, once again tripping over her own feet."
      mc "For all that praise, she can't seem to remain upright."
      mc "Perhaps that prestigious scholarship should be reconsidered?"
      lindsey lindseycrashed_angry_rec "Are you filming me?"
      mc "Just happened to have the phone out."
      lindsey lindseycrashed_angry_rec "You're totally filming me."
      mc "People need to know the truth."
      lindsey lindseycrashed_angry_rec "The truth?"
      mc "Yeah, that no one is perfect."
      lindsey lindseycrashed_angry_rec "I never claimed to be perfect! What's wrong with you?"
      mc "Yet everyone thinks you are. Well, now I have proof."
      lindsey lindseycrashed_angry_rec "I don't even know what to say... you're truly awful!"
  hide lindsey
  $game.location = x
  $renpy.transition(Dissolve(0.5))
  return
