init python:
  register_replay("kate_scare","Kate's Scare","replays kate_scare","replay_kate_scare")

label replay_kate_scare:
  show kate chase with fadehold
  kate chase "Aeeeyeeeee!"
  "Holy shit, [isabelle] is scary in that thing!"
  isabelle "Ooooooooink, oink, oink, oink!"
  kate chase "Aaaaaaaah! Help!"
  show kate trip
  kate trip "Eeep!" with vpunch
  window hide
  play sound "falling_thud"
  play ambient "<from 0.25>audio/sound/paint_splash.ogg" volume 0.75 noloop
  show black onlayer screens zorder 100 with vpunch
  hide black onlayer screens
  scene location
  with fadehold
  window auto
  return
