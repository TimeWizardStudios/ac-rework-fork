init python:
  register_replay("maxine_adventure","Maxine's Adventure","replays maxine_adventure","replay_maxine_adventure")

label replay_maxine_adventure:
  window hide
  hide screen interface_hider
  show maxine cave_fight earthworm_blur
  show black
  show black onlayer screens zorder 100
  with Fade(0.5, 0.375, 0.0)
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "The earth shivers again."
  "A slithering tremor rolling through the rock..."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  $set_dialog_mode("")
  window auto show
  show maxine cave_fight earthworm with {"master": Dissolve(2.0)}
  # $set_dialog_mode("")
  # "...and then, from beneath that rock, bursts the most hideous creature I have ever seen."
  "...and then, from beneath that rock, bursts the most hideous creature{space=-30}\nI have ever seen."
  # "A giant, pale, fat worm wriggles out of the shattered rock and whips its head around."
  "A giant, pale, fat worm wriggles out of the shattered rock and whips{space=-5}\nits head around."
  "Obviously blind. Obviously searching for a meal."
  mc "I can't believe I'm saying this, [maxine], but grab your weapon!"
  window hide
  # show maxine cave_fight first_strike
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  play sound "fire_axe_hit"
  hide black onlayer screens
  show maxine cave_fight first_strike: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  with Dissolve(.5)
  hide maxine
  show maxine cave_fight first_strike ## This is just to avoid the screen shake if the player is skipping
  window auto
  "I hardly need to tell her, though, because she already has her axe in hand, and is hacking away at the beast."
  # "However, it seems to do little more than tickle it and anger it further."
  "However, it seems to do little more than tickle it and anger it further.{space=-15}"
  "Dread and horror shakes me at my core. Do I run? Do I fight?"
  "I can't leave [maxine] behind, even though this was her stupid idea."
  window hide
  pause 0.125
  show maxine cave_fight second_strike_incoming with Dissolve(.5)
  play sound "machete_hit"
  show maxine cave_fight second_strike with vpunch
  pause 0.25
  window auto
  mc "Aaaaaaaah! [maxine]!"
  maxine cave_fight second_strike "Fight or get eaten, [mc]!"
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike with vpunch
  show maxine cave_fight third_strike_aftermath with Dissolve(.5)
  pause 0.25
  window auto
  "Oh, god. It's like cutting through fat."
  "How the fuck did I end up here, battling a giant worm?"
  window hide
  pause 0.125
  play sound "machete_hit"
  show maxine cave_fight fourth_strike with vpunch
  pause 0.25
  window auto
  "It coils around the cavern, hideously slithering along the walls."
  "It bites blindly, threatening to swallow anything that falls into its vile mouth."
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike_aftermath with vpunch
  pause 0.25
  window auto
  "This is it! I'm going to die down here!"
  window hide
  pause 0.125
  play sound "machete_hit"
  show maxine cave_fight fourth_strike with vpunch
  pause 0.25
  window auto
  "I'm ready to run and hide, but somewhere in the chaos, I catch a glimpse of [maxine]'s face."
  "The fearless determination in her eyes invigorates me."
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike_aftermath with vpunch
  pause 0.25
  window auto
  "In a dream-like state, my body moves on its own, dodging, weaving, hacking and slashing."
  "This is not how my second chance at life ends. Not inside the belly of this freak of nature."
  window hide
  pause 0.125
  play sound "machete_hit"
  show maxine cave_fight fourth_strike with vpunch
  pause 0.25
  window auto
  # "Knees weak, arms heavy, as I chop, and chop, and chop, and chop..."
  "Knees weak, arms heavy, as I chop, and chop, and chop, and chop...{space=-5}"
  "...until my face and eyes sting with sweat, tears, and worm juices."
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike_aftermath with vpunch
  pause 0.25
  window auto
  # "[maxine] is nearby, breathing heavily, crying out as she fights like hell."
  "[maxine] is nearby, breathing heavily, crying out as she fights like hell.{space=-5}"
  "As she fights to the death."
  window hide
  pause 0.125
  show maxine cave_fight fifth_strike_incoming with Dissolve(.5)
  pause 0.25
  hide screen interface_hider
  play sound "fire_axe_hit"
  play audio "machete_hit"
  show expression LiveComposite((1920,1080), (0,0),"school park cave background", (854,229),"school park cave glowing_rune_door", (1574,482),"school park cave ship_wheel", (556,106),"school park cave columns", (798,506),"school park cave crown", (1152,443),"school park cave ancient_chests", (1207,363),"school park cave portrait", (1164,446),"school park cave coins", (1105,617),"school park cave ancient_book", (1368,680),"school park cave ship_propellor", (14,0),"school park cave foreground", (304,800),"school park cave ancient_chastity_belt") as makeshift_location behind maxine
  $maxine["outfit_stamp"] = maxine.outfit
  $maxine.outfit = {"glasses":"maxine_glasses", "panties":"maxine_black_panties", "bra":"maxine_black_bra", "shirt":"maxine_armor"}
  show maxine afraid_blood
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "In a final chomp that barely misses my head, the worm collapses on the cave floor."
  "Leaking its vile insides onto the rock."
  "Breaths rattle through my mouth, sucking in the fetid air."
  "My hair and skin drenched in worm blood."
  "But I'm alive in glorious victory."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "What... the fuck... was that?!"
  maxine afraid_blood "A giant earthworm."
  mc "But why?!"
  maxine thinking_blood "Are you asking why was it an earthworm?"
  mc "Why was it huge, [maxine]?!"
  maxine sad_blood "Oh. I am not sure."
  maxine sad_blood "I have several hypotheses, but without the necessary control—"
  mc "You know what? Nevermind."
  mc "I'm just glad we're okay. I was worried about you."
  maxine concerned_blood "..."
  "She goes uncharacteristically silent."
  "We stare at each other for a moment."
  "Both of us covered in the ichor of the worm."
  "The adrenaline from the fight still pumping through me."
  "This feels like one of those moments in the movies, when the lead character just leans in and kisses the girl."
  window hide
  show maxine cave_kiss
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide makeshift_location
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "It all happens so fast. He pulls her in, and then presses his lips against hers."
  "And the audience breathes a sigh of relief. It's the moment they've all been waiting for."
  "Not the action or fighting scenes. The simple kiss of victory."
  "At first, [maxine] is stiff."
  "But then she melts into me a little more and begins to kiss me back."
  "The adrenaline of the fight and the discovery is probably doing something to her, too."
  "Her salted caramel lips taste like heaven and block out the stench of the dead worm."
  "My shaking hands explore her soft hips and waist."
  "And then she's really giving into the passion of the moment."
  "Her lips push hard against mine. Her tongue darting out to tease."
  "She leans into me, making me support her weight."
  "Pressing her armored chest against me."
  window hide
  hide screen interface_hider
  play sound "falling_thud"
  $maxine.outfit = maxine["outfit_stamp"]
  show maxine cave_sex anticipation
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "The force of her kiss overwhelms me in my weakened state, and we tumble onto the floor."
  "Our eyes meet for a moment, and then we're tearing at each other's clothes and armor."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "My eyes and hands fully take [maxine] in."
  "Her soft body beneath the armor... her smooth legs... her warm crotch..."
  window hide
  pause 0.125
  show maxine cave_sex insertion1 with Dissolve(.5)
  pause 0.25
  window auto
  "She lifts herself up, and slowly impales herself on my dick."
  "Sliding her slick outer lips back and forth on it."
  mc "Ready for our next adventure?"
  show maxine cave_sex insertion1 smile with dissolve2
  "She smiles knowingly down at me..."
  window hide
  pause 0.125
  show maxine cave_sex insertion2 with Dissolve(.25)
  show maxine cave_sex insertion3 with vpunch
  pause 0.25
  window auto
  mc "Oh, f-fuck!"
  # "The weight of her coming down on me feels even better as I fill her up."
  "The weight of her coming down on me feels even better as I fill\nher up."
  "Inch by inch, she adjusts herself around my throbbing shaft, until her ass hits my thighs."
  show maxine cave_sex insertion3 flirty with dissolve2
  "She then sits there for a moment."
  "Her tight pussy squeezing me, clenching and unclenching as it gets accustomed to the invasion."
  # maxine cave_sex insertion3 flirty "The heat of battle really aroused you. I didn't think you had it in you."
  maxine cave_sex insertion3 flirty "The heat of battle really aroused you. I didn't think you had it in you.{space=-10}"
  maxine cave_sex insertion3 flirty "It will make an interesting addition to your fi—"
  mc "Yes, yes, [maxine]!"
  mc "Just ride me, will you?"
  maxine cave_sex insertion3 flirty "A well earned outcome for your valor."
  "I can feel the pressure mounting in my balls..."
  "...driving me insane as she sits there, hugging my dick tightly..."
  window hide
  pause 0.125
  show maxine cave_sex penetration1 with Dissolve(.5)
  pause 0.25
  show maxine cave_sex penetration2 with Dissolve(.175)
  show maxine cave_sex penetration3 with Dissolve(.25)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.175)
  show maxine cave_sex penetration1 with Dissolve(.25)
  pause 0.5
  show maxine cave_sex penetration2 with Dissolve(.175)
  show maxine cave_sex penetration3 with Dissolve(.25)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.175)
  show maxine cave_sex penetration1 with Dissolve(.25)
  pause 0.5
  show maxine cave_sex penetration2 with Dissolve(.175)
  show maxine cave_sex penetration3 with Dissolve(.25)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.175)
  show maxine cave_sex penetration1 with Dissolve(.25)
  pause 0.25
  window auto
  "...but thankfully, she does exactly as I ask."
  "She places her hands on my chest and begins to grind on my dick."
  "Slowly at first, gyrating her hips back and forth."
  mc "Oh, god. That's it! Just like that!"
  window hide
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration3 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration1 with Dissolve(.175)
  pause 0.25
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration3 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration1 with Dissolve(.175)
  pause 0.25
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration3 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration1 with Dissolve(.175)
  pause 0.25
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration3 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration1 with Dissolve(.175)
  pause 0.25
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration3 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration2 with Dissolve(.125)
  show maxine cave_sex penetration1 with Dissolve(.175)
  pause 0.25
  window auto show
  show maxine cave_sex penetration1 aroused with dissolve2
  "Who knew that eccentric, cryptic, strange [maxine] could ride a cock so well?"
  "Like everything she does, she goes about it in a methodical way."
  "Squeezing and releasing me at intervals."
  # "Rocking back and forth while she gains momentum, before suddenly stopping."
  "Rocking back and forth while she gains momentum, before suddenly{space=-15}\nstopping."
  "After she does, she holds me there for a minute."
  window hide
  pause 0.125
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration3 aroused with Dissolve(.175)
  pause 0.5
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration1 aroused with vpunch
  pause 0.5
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration3 aroused with Dissolve(.175)
  pause 0.25
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration1 aroused with vpunch
  pause 0.25
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration3 aroused with Dissolve(.175)
  pause 0.175
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration1 aroused with vpunch
  pause 0.175
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration3 aroused with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration2 aroused with Dissolve(.125)
  show maxine cave_sex penetration1 aroused with vpunch
  pause 0.25
  window auto
  # "Before she starts to bounce again, going up and down on my length."
  "Before she starts to bounce again, going up and down on my length.{space=-15}"
  "Each time she comes down is harder than the last."
  "And she grinds her clit down into me."
  mc "Y-yes! Goddamn, [maxine]!"
  "If this is how the weirdos fuck, the rest of society is definitely missing out."
  window hide
  pause 0.125
  show maxine cave_sex penetration4 with Dissolve(.375)
  pause 0.125
  show maxine cave_sex penetration5 with Dissolve(.125)
  show maxine cave_sex penetration6 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration5 with Dissolve(.125)
  show maxine cave_sex penetration4 with vpunch
  pause 0.125
  show maxine cave_sex penetration5 with Dissolve(.125)
  show maxine cave_sex penetration6 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration5 with Dissolve(.125)
  show maxine cave_sex penetration4 with vpunch
  pause 0.125
  show maxine cave_sex penetration5 with Dissolve(.125)
  show maxine cave_sex penetration6 with Dissolve(.175)
  pause 0.125
  show maxine cave_sex penetration5 with Dissolve(.125)
  show maxine cave_sex penetration4 with vpunch
  pause 0.25
  window auto
  "I grasp her thighs and start to piston up into her sopping wet folds."
  maxine cave_sex penetration4 "N-no, don't!"
  maxine cave_sex penetration4 "I am close to peak arousal, and I know how best to achieve it if you lie still."
  mc "..."
  "It's not exactly sexy talk, but somehow I just seem to stiffen even more within her."
  "My own climax cresting."
  mc "F-fine, but I'm about to—"
  window hide
  pause 0.125
  show maxine cave_sex penetration7 with Dissolve(.375)
  pause 0.125
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration9 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration7 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration9 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration7 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration9 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration7 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration9 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration7 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration9 with Dissolve(.125)
  pause 0.0625
  show maxine cave_sex penetration8 with Dissolve(.1)
  show maxine cave_sex penetration7 with Dissolve(.125)
  pause 0.25
  window auto
  mc "...oooooh!"
  "Before I can even finish speaking, she's back to maneuvering my dick exactly how she wants."
  "Going faster and faster, back and forth on my dick."
  "Sweat rolling down her pale skin."
  "Between her bouncing tits, whose nipples are hard as diamonds."
  show maxine cave_sex penetration7 aroused with dissolve2
  maxine cave_sex penetration7 aroused "..."
  "Suddenly, she stops."
  # "Her legs clenching against my hips, her pussy locking up around me."
  "Her legs clenching against my hips, her pussy locking up around me.{space=-15}"
  window hide
  pause 0.125
  show maxine cave_sex orgasm with vpunch
  pause 0.25
  window auto
  maxine cave_sex orgasm "Mmmmmmmmm!"
  # "She shudders and closes her eyes as the orgasm hits her full force."
  "She shudders and closes her eyes as the orgasm hits her full\nforce."
  "Her mouth hangs open, tremors cascading throughout her entire body."
  "As she sits there, holding me inside of her, I feel my own tidal wave hitting."
  mc "[maxine], I-I'm about to—"
  maxine cave_sex orgasm concerned "Inside! You have to ejaculate inside!"
  menu(side="middle"):
    extend ""
    "Do as she says":
      # "She doesn't have to tell me twice. Even if I wanted to hold back, I couldn't."
      "She doesn't have to tell me twice. There's no better feeling than cumming inside of a warm, wet pussy."
      "Even if I wanted to hold back, I couldn't."
      window hide
      pause 0.125
      show maxine cave_sex penetration10 with Dissolve(.375)
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration12 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration10 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration12 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration10 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration12 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration10 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration12 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration10 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration12 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration11 with Dissolve(.1)
      show maxine cave_sex penetration10 with Dissolve(.125)
      show maxine cave_sex cum_inside with vpunch
      pause 0.25
      window auto
      "An upward torrent of seed gushes into her waiting womb."
      "Her pussy seems to drink it up, milking me for every last drop."
      maxine cave_sex cum_inside flirty "Yes! Good! T-that's very good."
      mc "Mmm... it really is..."
      maxine cave_sex cum_inside flirty "We can't leave any behind, or else the crab rocks will consume it and multiply."
      maxine cave_sex cum_inside flirty "We don't want an infestation, do we?"
      mc "..."
      mc "Err, right. Of course not."
    "Pull out":
      "[maxine] is hot in a crazy kind of way..."
      "...but I am not getting trapped if she gets pregnant or something."
      window hide
      show maxine cave_sex stroke1
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      pause 0.125
      show maxine cave_sex stroke2 with Dissolve(.175)
      show maxine cave_sex stroke1 with Dissolve(.25)
      pause 0.03125
      show maxine cave_sex stroke2 with Dissolve(.175)
      show maxine cave_sex stroke1 with Dissolve(.25)
      pause 0.03125
      show maxine cave_sex stroke2 with Dissolve(.175)
      show maxine cave_sex stroke1 with Dissolve(.25)
      pause 0.03125
      show maxine cave_sex stroke2 with Dissolve(.175)
      show maxine cave_sex stroke1 with Dissolve(.25)
      pause 0.03125
      show maxine cave_sex stroke2 with Dissolve(.175)
      show maxine cave_sex cum_outside with hpunch
      show maxine cave_sex cum_outside_aftermath with Dissolve(.5)
      window auto
      "She gasps and looks up at the cave ceiling as my dick pops out of her pussy with a slurping sound."
      "Her eyes glaze over from the orgasm as my seed coats her belly."
      mc "Mmm, yes! Fuck, that felt good!"
      show maxine cave_sex cum_outside_aftermath annoyed with dissolve2
      maxine cave_sex cum_outside_aftermath annoyed "Did I not tell you to stay inside?"
      mc "Sorry, that's hot and all, but—"
      show maxine cave_sex cum_outside_aftermath angry with dissolve2
      maxine cave_sex cum_outside_aftermath angry "Now the crab rocks will consume it and multiply!"
      maxine cave_sex cum_outside_aftermath angry "There will be an infestation!"
      mc "Oh. Whoops."
  scene location with fadehold
  return
