init python:
  register_replay("nurse_catch","Nurse's Catch","replays nurse_catch","replay_nurse_catch")

label replay_nurse_catch:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show nurse fishing wait with fadehold
  mc "There you go! Just like that."
  nurse fishing wait "Gracious... it's not as easy as it looks..."
  mc "Just make sure you hold it firmly, okay?"
  nurse fishing wait "Oh, um. O-okay."
  nurse fishing wait "Like this?"
  mc "Yes, just like that!"
  nurse fishing wait "..."
  "We stand there together in silence for a moment."
  "Enjoying the soft fall breeze and the lapping of the water against the shore."
  "It's actually kind of peaceful. I get now why people do this."
  "Nothing but nature and the slowing down of thoughts."
  "It's weird how standing still and just waiting can make everything else feel insignificant."
  "..."
  mc "So, are you having fun?"
  nurse fishing wait "It's okay, I suppose."
  nurse fishing wait "However, once the pole is in the water, it's kind of, well, boring?"
  nurse fishing wait "You have lots of time to think..."
  mc "I guess you do."
  mc "Are you thinking about [kate]?"
  nurse fishing wait "Err, a bit... and work, too."
  mc "Just enjoy the fresh air, will you? Relax a bit."
  mc "The point of fishing is to forget about those things."
  nurse fishing wait "Right. You're right."
  nurse fishing wait "It's just... easier said than done."
  mc "Hey, I get it."
  window hide
  pause 0.125
  show nurse fishing wait with vpunch
  pause 0.25
  window auto
  nurse fishing wait "Oh! Did you feel that?!"
  mc "It feels like you got a bite!"
  mc "Try reeling it in!"
  nurse fishing wait "Will you, um, help me?"
  mc "Gladly."
  window hide
  pause 0.125
  play sound "<from 0.5>fishing_reel"
  show nurse fishing reeling_in with Dissolve(.5)
  pause 0.25
  window auto
  nurse fishing reeling_in "Goodness! Should it be this hard?!"
  "I don't want to admit it to her, but when the pole bends downwards, the muscles in my arms definitely strain with effort to hold on."
  mc "Err, it must be a r-really big one..."
  mc "L-let me try doing it on my own, okay?"
  nurse fishing reeling_in "Yes, please!"
  window hide
  hide screen interface_hider
  show nurse fishing maxine_reveal
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  play sound "<from 0.5>fishing_reel"
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "What the hell kind of fish do they keep stocked in this stream?!"
  "The harder I try to reel it in, the heavier it seems to get, fighting against me, against the current of the water."
  "It's fully bending the pole, now."
  "Maybe I should just give up and let it win?"
  "Maybe it's—"
  show black onlayer screens zorder 100
  pause 0.5
  play sound "<from 0.2>water_splash2"
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "[maxine]?! What the hell are you doing?"
  if quest.isabelle_hurricane.actually_finished:
    maxine "I'm searching for my homunculus."
    maxine "...and also the portal to the underworld."
    mc "Whoops. Sorry."
  else:
    maxine "I'm preparing."
  maxine "So, if you could throw me back, that would be great."
  mc "You do realize this is probably not a good place to swim, don't you?"
  if school_forest_glade["pollution"]:
    mc "The pollution can't be good for your health."
  else:
    mc "You might be ruining the ecosystem."
  maxine "There are worse things than me below the surface."
  "I somehow doubt that..."
  "At least, after I got rid of that damned beaver."
  mc "That's not even the point I was trying to make."
  nurse fishing maxine_reveal "Goodness gracious, [maxine]! Are you all right?!"
  maxine "I would be better if you would allow me to continue my work."
  menu(side="middle"):
    extend ""
    "Throw her back in":
      mc "I suppose what you're doing down there is not really my business..."
      mc "Very well. I'll release you."
      nurse fishing maxine_reveal "Oh, um. Are you certain, [mc]?"
      mc "I'm sure she'll be fine."
      nurse fishing maxine_reveal "Well, let me at least unhook her and make sure she's all right first, will you?"
    "Keep her":
      mc "I'm sorry, [maxine], but I'm not letting you go back down there."
      # mc "Congratulations on being our biggest — and only — catch of the day."
      mc "Congratulations on being our biggest — and only — catch of the day.{space=-20}"
      maxine "You disturbed the process."
      mc "What process?"
      # nurse fishing maxine_reveal "Please, just come on out of there and let me make sure you're all right, will you, [maxine]?"
      nurse fishing maxine_reveal "Please, just come on out of there and let me make sure you're all right,{space=-60}\nwill you, [maxine]?"
      "Man, the [nurse] really is too nice..."
      # "Instead of being annoyed by [maxine]'s antics, she just wants to help her."
      "Instead of being annoyed by [maxine]'s antics, she just wants to help her.{space=-75}"
      "I don't think I have ever met anyone kinder."
      nurse fishing maxine_reveal "Would you please help her up, [mc]?"
      mc "You got it."
  scene location with fadehold

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
