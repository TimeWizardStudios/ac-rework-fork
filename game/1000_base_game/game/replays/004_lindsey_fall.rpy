﻿init python:
  register_replay("lindsey_fall","Lindsey's Trip","replays lindsey_fall","replay_lindsey_fall")

label replay_lindsey_fall:
  scene lindsey LindseyPose00 with fade: #this gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 8
    linear 0.05 yoffset 0
  hide lindsey
  show lindsey LindseyPose00 #this is just to avoid the screen shake if the player is skipping
  lindsey "Eeep!"
  "She tripped over the wet-floor sign. Where's the sign with a warning about hazardous signs?"
  "For once in my life, I happened to be at the right place at the right time."
  "It's hard to imagine a top athlete like [lindsey] ever misstepping, but maybe this is that one blue-moon occurrence."
  "Hmm... this is more of a beige-pink moon, though... a pristine one, without a single blemish or crater..."
  "One might even call it a full one, if it weren't partially obscured by a sports thong."
  lindsey LindseyPose00b "Effing shit! I swear to god!"
  menu(side="far_left"):
    "Help her up":
      mc "Are you okay?"
      show lindsey LindseyPose00 with Dissolve(.5)
      lindsey LindseyPose00 "..."
      "Hmm... no thanks? Well, she did look really embarrassed."
      "She probably just wanted to get the hell out of here and hide her face. Been there, can relate."
    "Enjoy the view":
      "Clumsy girls are my favorite."
      "Especially, angry clumsy girls in skirts and thongs."
      "Girls who do everything they can to keep their modesty and good reputation."
      "Who'd hate to have their pert bottoms exposed to half the school. "
      "Their silky unblemished buttcheeks... split by a pair of risqué panties that only hide their most intimate parts..."
      "Just a thin layer of fabric between me and that wet squishy pussy."
      "Between that tight tight rose of forbidden lust and the unworthy..."
      show lindsey LindseyPose00 with Dissolve(.5)
      lindsey "Piece of crap scum sign!"
      show lindsey LindseyPose00b with Dissolve(.5)
      lindsey "Ugh..."
      "Should be illegal to look that good!"
      "Hours at the gym, thousands of squats. Her falling over is a public service."
      "Should be a weekly thing with tickets and popcorn. There's profit to be made here."
      "If only girls weren't so protective of their bodies..."
  ## restore normal scene
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
