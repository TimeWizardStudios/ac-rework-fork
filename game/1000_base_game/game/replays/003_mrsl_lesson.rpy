init python:
  register_replay("mrsl_lesson","Mrs. L's Lesson","replays mrsl_lesson","replay_mrsl_lesson")

label replay_mrsl_lesson:
  scene mrsl write1 with fade
  "Oh, shit. Okay, definitely time to take a seat!"
  "Who knew she hid such a tight body underneath those layers of fabric. It's hard not to be vulgar because {i}damn{/}... that ass!"
  "And that strip of skin at the edge of her dress is like a glimpse into the second circle of Hell..."
  "The firm roundness of her cheeks..."
  "The belt that caresses her stripper waist..."
  "The outline of her freaking nipple piercings earlier!"
  "She's like sex on legs... long perfectly toned legs..."
  show mrsl write2
  with dissolve
  mrsl "Can everyone see well?"
  mrsl "Okay, perfect!"
  show mrsl write1
  with dissolve
  mrsl "Starting today, each of you will be in charge of your own schedule."
  mrsl "That means you have to sign up for three classes you want to focus on. There'll be a list in each classroom."
  mrsl "To graduate at the end of the year, you need to acquire one hundred points in your focus classes and twenty points in your regular classes."
  mrsl "This means that there won't be scheduled classes for you this year and that you're more responsible for your education."
  show mrsl write2
  with dissolve
  mrsl "Did everyone get that? Some of you seem a bit distracted."
  mrsl "In case you're confused, the details are on the blackboard."
  mrsl "Any questions?"
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
