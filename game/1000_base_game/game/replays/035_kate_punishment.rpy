init python:
  register_replay("kate_punishment","Kate's Punishment","replays kate_punishment","replay_kate_punishment")

label replay_kate_punishment:
  show kate cheerleader_feet with fadehold
  kate cheerleader_feet "Down where you belong."
  kate cheerleader_feet "Did you really think you could be disrespectful and get away with it?{space=-10}"
  lacey "He looks like a maggot!"
  stacy "He probably is one."
  casey "Is there maggot DNA in you?"
  mc "..."
  kate cheerleader_feet "Answer her."
  mc "No..."
  kate cheerleader_feet "No, what?"
  menu(side="middle"):
    extend ""
    "\"No, ma'am...\"":
      mc "No, ma'am..."
      kate cheerleader_feet "Better."
      casey "You're so good at getting him in line, Katie!"
      kate cheerleader_feet "Hopefully, he's learning."
      stacy "Are you learning, little maggot?"
      mc "Yes, ma'am..."
      lacey "Queen!"
      kate cheerleader_feet "All right, we're going to get back to practice. You stay put, [mc]."
      show kate cheerleader_feet_nofeet with Dissolve(.5)
      "Nothing to do but staying put, I guess."
      "Damn, these ropes are tight..."
    "\"No, you psychotic bitch.\"":
      mc "No, you psychotic bitch."
      lacey "{i}Gasp!{/}" with vpunch
      stacy "My god."
      casey "How dare..."
      kate cheerleader_feet_kate_neutral "For someone in your position, that was a very stupid thing to say."
      kate cheerleader_feet_kate_laughing "Luckily, I know exactly how to deal with unruly prisoners!"
      window hide
      show kate cheerleader_feet_kate_shoeless_smile with Dissolve(.5)
      pause 0.5
      show kate cheerleader_feet_kate_shoeless_sockless_holdingsock_smile with Dissolve(.5)
      window auto
      mc "Wait just one minute!"
      kate cheerleader_feet_kate_shoeless_sockless_holdingsock_smile "What's wrong?"
      lacey "I think it's time to open wide, [mc]."
      casey "Yeah, you pervert! Say, \"aaaah!\""
      kate cheerleader_feet_kate_shoeless_sockless_holdingsock_neutral "They're going into your mouth whether you like it or not."
      kate cheerleader_feet_kate_shoeless_sockless_holdingsock_neutral "We're tired of your insolence."
      "Fuck! There's no way she's putting her dirty socks in my mouth..."
      "What do I do?!"
      "I can tell they're all itching to give me a wedgie if I don't comply..."
      "There's no winning here, only different levels of losing."
      menu(side="middle"):
        extend ""
        "Keep your mouth shut":
          mc "..."
          kate cheerleader_feet_kate_shoeless_sockless_holdingsock_neutral "Girls. You know what to do."
          stacy "You asked for this, loser!"
          window hide
          show black with Dissolve(.5)
          pause 1.0
          play sound "<from 1.0>nylon_rip"
          $set_dialog_mode("default_no_bg")
          mc "Ow, fuck!" with vpunch
          "It's been so long... I'd forgotten how painful that wedgie is..."
          lacey "Let's take it to the next level, yeah?"
          mc "Fuck me! You're tearing me—"
          mc "{i}Mmmph!{/}" with vpunch
          hide black
          show kate cheerleader_feet_kate_shoeless_sockless_laughing
          with Dissolve(.5)
          window auto
          $set_dialog_mode("")
          kate cheerleader_feet_kate_shoeless_sockless_laughing "There you go! I told you those socks would go into your mouth..."
          mc "{i}Muhgugh!{/}"
          kate cheerleader_feet_kate_shoeless_sockless_confident "Didn't quite catch that."
        "Surrender":
          mc "Aaaah..."
          kate cheerleader_feet_kate_shoeless_sockless_holdingsock_smile "Finally."
          window hide
          show black with Dissolve(.5)
          pause 1.0
          hide black
          show kate cheerleader_feet_kate_shoeless_sockless_confident
          with Dissolve(.5)
          window auto
          kate cheerleader_feet_kate_shoeless_sockless_confident "There's a good boy! That ought to shut you up..."
          mc "{i}Mmmph.{/}"
      "The taste of feet is overwhelming. Sweat, old and new."
      "Kate's feet..."
      "Fuck, why am I getting turned on all of a sudden?"
      "If they flip me over, I'll never hear the end of it."
      kate cheerleader_feet_kate_shoeless_sockless_smile "We're going to practice now."
      kate cheerleader_feet_kate_shoeless_sockless_smile "If you behave, we'll let you go once we're done."
      kate cheerleader_feet_kate_shoeless_sockless_neutral "If not, we'll let the janitor find you in a bathroom stall at the end of the day."
      kate cheerleader_feet_shoeless_sockless "Let's go, girls!"
      show kate cheerleader_feet_nofeet with Dissolve(.5)
      "..."
      "The upside is that I have a first row seat to a group of hot girls in skimpy outfits."
      "The downside is that I have [kate]'s socks in my mouth, and my dick is about to burst out of my pants."
      "Fuck me... I better reduce the swelling before they're done."
      window hide
      show black with Dissolve(.5)
      pause 1.5
      hide black with Dissolve(.5)
      window auto
      "..."
      "Damn it. And I don't even have a good view of them..."
      "Their long legs, dancing across the gym floor... Their short skirts, flapping in the wind..."
      "How long has it been since my imprisonment? Days? weeks?"
      "The only good thing about this is that I'm hard as a rock."
      "My dick keeps the blood circulation going."
      window hide
      show black with Dissolve(.5)
      pause 1.5
      hide black with Dissolve(.5)
      window auto
      "..."
      "Shit, they're coming over."
      show kate cheerleader_feet with Dissolve(.5)
      kate cheerleader_feet "Enjoying yourself down there?"
      mc "{i}Mmmph.{/}"
      kate cheerleader_feet "See, [lacey]? This is why I always keep an extra pair of gym socks."
      lacey "Your wisdom is a marvel to behold..."
      kate cheerleader_feet "Bitch, don't roll your eyes at me."
      lacey "Sorry, hah!"
      kate cheerleader_feet "We're halfway through practice, but we thought we'd take a break to inspect our prisoner."
      kate cheerleader_feet "[casey], check his ropes."
      casey "Why me? I don't want to touch him!"
      kate cheerleader_feet "No one wants to, okay?"
      lacey "Can't we make him check his own ropes?"
      stacy "Babe, you're doing that thing again..."
      lacey "What thing?"
      stacy "Being stupid."
      lacey "Okay, miss smartypants. You check his ropes, then."
      stacy "I'm not touching that!"
      kate cheerleader_feet "You know what? He wouldn't dare to escape."
      kate cheerleader_feet "I'm sure he'll stay just like this until we say otherwise."
      "The world has always been at the feet of beautiful women."
      "That's more clear to me than ever."
      "For some reason, there's a sort of calming clarity in letting go and submitting."
      "The bulge between my legs isn't something I control, but perhaps the lack of control is the real turn on."
      "Not sure if I'm ready to face such levels of humiliation, though..."
      "Maybe certain things should be left unsaid."
      menu(side="middle"):
        extend ""
        "Roll over":
          window hide
          show kate cheerleader_feet:
            xalign 0.5 yalign 0.5
            linear 0.1 rotate -5
            parallel:
              linear 0.5 rotate 185
              linear 0.1 rotate 180
            parallel:
              linear 0.25 zoom 2.5
              linear 0.25 zoom 1
          pause 1.0
          hide kate cheerleader_feet
          show kate cheerleader_feet:
            zoom -1
          window auto
          casey "..."
          lacey "..."
          stacy "..."
          kate cheerleader_feet "Well, there's the proof."
          kate cheerleader_feet "What kind of slimy perverted dog gets horny in such a situation?"
          casey "He's truly one of a kind, this one."
          lacey "Ewwwwww!"
          stacy "Disgusting."
          kate cheerleader_feet "Turn back over. No one wants to see that."
          "Not sure if I should be happy that they even noticed the bulge, or just completely ashamed..."
          window hide
          show kate cheerleader_feet:
            xalign 0.5 yalign 0.5
            linear 0.1 rotate 5
            parallel:
              linear 0.5 rotate -185
              linear 0.1 rotate -180
            parallel:
              linear 0.25 zoom -2.5
              linear 0.25 zoom -1
          pause 1.0
          hide kate cheerleader_feet
          show kate cheerleader_feet
          window auto
          kate cheerleader_feet "Well, if this turns you on, wait until you learn what else we have in store for you this year."
          "In a weird way, I'm both terrified and excited..."
          "Damn it, I need to get these thoughts out of my head."
          "..."
          "But maybe the only way is to embrace it?"
          kate cheerleader_feet "Come on, girls! Let's finish up practice."
          show kate cheerleader_feet_nofeet with Dissolve(.5)
        "Stay put":
          "There's no telling what would happen by showing [kate] that this kind of treatment is turning me on."
          "Why increase her hold over me?"
          kate cheerleader_feet "Come on, girls! Let's finish up practice."
          show kate cheerleader_feet_nofeet with Dissolve(.5)
          "Phew! Somehow made it through unnoticed..."
  window hide
  show black with Dissolve(.5)
  pause 2.0
  hide black with Dissolve(.5)
  window auto
  "..."
  "When I woke up this morning, I never thought I'd end up in bondage."
  "Hmm... does this actually count as bondage?"
  "If so, does that mean I just had sex with all the cheerleaders at once?"
  "No one except Chad could pull that off!"
  "..."
  "I can't believe they practice for this long..."
  "Maybe I should try to get loose?"
  "Hmm... probably not worth the risk."
  "..."
  "Huh, the music finally stopped. Maybe they're done?"
  show kate cheerleader_feet with Dissolve(.5)
  kate cheerleader_feet "We're done with practice for today."
  kate cheerleader_feet "We were supposed to go the whole day, but we've decided to take the rest of the day off."
  kate cheerleader_feet "I'm treating the girls to a night at the Newfall spa."
  lacey "Queen!"
  kate cheerleader_feet "Unfortunately for you, your fate will have to wait until the morning."
  kate cheerleader_feet "Let's go, girls!"
  show kate cheerleader_feet_nofeet with Dissolve(.5)
  "What the hell? Are they really going to leave me here overnight?!"
  "Fuck..."
  "And they said they had the gym booked for the rest of the day..."
  "..."
  "Huh? Someone's coming."
  show kate cheerleader_feet_noretinue with Dissolve(.5)
  pause 0.5
  kate cheerleader_feet_nofeet_kate_confident "Had a few more things to tell you before I go."
  kate cheerleader_feet_nofeet_kate_laughing "Those ropes look good on you! I like how they dig into your skin and keep you in a docile little package..."
  kate cheerleader_feet_nofeet_kate_confident "I've studied Japanese rope techniques such as shibari and kinbaku. Those ropes are expertly tied and won't be coming off until I say so.{space=-5}"
  kate cheerleader_feet_nofeet_kate_smile "And I say they stay on until the morning."
  kate cheerleader_feet_nofeet_kate_smile "I spoke to [jo], and told her you're staying over at my place tonight."
  "What the fuck. No way."
  kate cheerleader_feet_nofeet_kate_laughing "She sounded surprised, but happy..."
  kate cheerleader_feet_nofeet_kate_laughing "She didn't even question it! Just told me to have fun."
  kate cheerleader_feet_nofeet_kate_smile "I know that look, but don't worry. I quite like keeping you under my heel, and I wouldn't want anything serious to happen to you."
  "Somehow, I don't buy that for a second..."
  kate cheerleader_feet_nofeet_kate_smile "In case of an emergency, I've put my number into your phone. Here you go."
  kate cheerleader_feet_nofeet_kate_phoneground_smile "So, just press call with your nose or something."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "But keep in mind that abusing this rare privilege would have dire consequences."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "Nod if you understand."
  menu(side="middle"):
    extend ""
    "Nod":
      window hide
      show layer master:
        xalign 0.5 yalign 0.5
        parallel:
          linear 0.25 yoffset -50
          linear 0.25 yoffset 50
          linear 0.25 yoffset -50
          linear 0.25 yoffset 50
          linear 0.25 yoffset 0
        parallel:
          linear 0.25 zoom 1.1
          pause 0.75
          linear 0.25 zoom 1.0
      pause 1.5
      show layer master
      show kate cheerleader_feet_nofeet_kate_phoneground_confident with Dissolve(.5)
      window auto
      kate cheerleader_feet_nofeet_kate_phoneground_confident "That's a good pet!"
      kate cheerleader_feet_nofeet_kate_phoneground_confident "I love obedience. I can already tell you'll be very obedient."
      kate cheerleader_feet_nofeet_kate_phoneground_smile "Keep it up, and this might be a good year for you..."
    "Shake head":
      window hide
      show layer master:
        xalign 0.5 yalign 0.5
        parallel:
          linear 0.25 xoffset -50
          linear 0.25 xoffset 50
          linear 0.25 xoffset -50
          linear 0.25 xoffset 50
          linear 0.25 xoffset 0
        parallel:
          linear 0.25 zoom 1.1
          pause 0.75
          linear 0.25 zoom 1.0
      pause 1.5
      show layer master
      window auto
      kate cheerleader_feet_nofeet_kate_phoneground_neutral "Bad pet! I see you require some training still..."
      kate cheerleader_feet_nofeet_kate_phoneground_confident "Luckily, we have all year and by the end of it, you're going to be my little bitch."
      kate cheerleader_feet_nofeet_kate_phoneground_confident "You're going to do everything I say. Even bark on command."
      kate cheerleader_feet_nofeet_kate_phoneground_smile "All in good time, though."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "Oh, and one more thing — no cumming."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "I'll check in the morning, so don't you dare try."
  kate cheerleader_feet_nofeet_kate_phoneground_laughing "Sleep well!"
  show kate cheerleader_feet_nofeet_phoneground with Dissolve(.5)
  "Man, I can't believe [kate] is actually going to go through with it..."
  "Never thought this is how I'd get her number either."
  "In fact, I never thought I'd get her number period."
  "In a weird way, by leaving her number she's showing that she's not rotten to the core."
  "Although, knowing her, it might just be a fake number to crush my hope one last time during an emergency."
  "Might as well try to get some sleep..."
  scene location with fadehold
  return
