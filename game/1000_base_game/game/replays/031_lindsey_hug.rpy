init python:
  register_replay("lindsey_hug","Lindsey's Hug","replays lindsey_hug","replay_lindsey_hug")

label replay_lindsey_hug:
  $x = game.location
  $game.location = school_art_class
  $lindsey.unequip("lindsey_panties")
  $lindsey.unequip("lindsey_bra")
  $lindsey.unequip("lindsey_skirt")
  $lindsey.unequip("lindsey_shirt")
  $lindsey.equip("lindsey_mcshirt")
  show lindsey laughing
  $renpy.transition(Dissolve(0.5))
  pause(.5)
  lindsey laughing "It's a bit big on me, don't you think?"
  mc "I think it looks absolutely perfect."
  lindsey flirty "Do you really think so?"
  mc "Without a doubt."
  lindsey smile "Aw, come here!"
  show lindsey hugging_mcshirt with Dissolve(.5)
  "Not how I expected my day to turn out when I woke up this morning."
  "Maybe that's the trick. Having no expectations and just doing the right thing."
  "Feeling [lindsey]'s nimble body pressing against me... it's hard not to think that's the case."
  "Her wet hair smells like spring rain."
  "Living in loneliness for so long... I sure missed out on life itself."
  "'Cause her arms around me is life. It fills me with new hope. With energy."
  "Even if I wake up now or get sent back to my old life, I'll remember this moment."
  "I'll remember her on her tippy toes, wearing my shirt."
  "I'll remember it riding up her cute butt. And I'll remember her breath on my neck."
  "Nothing will ever erase this moment."
  "..."
  show lindsey smile with Fade(.5,3,.5)
  lindsey smile "Thank you. And thanks for being so nice to me."
  mc "You're one of the first people I think of when I hear the phrase \"good people.\""
  mc "And this year, I've decided to be better as well."
  lindsey smile "Well, it's certainly working. I hope everyone sees that!"
  lindsey smile "Anyway, can you close your eyes for a moment so I can change?"
  mc "Of course."
  hide lindsey
  $game.location = x
  $renpy.transition(Dissolve(0.5))
  $lindsey.unequip("lindsey_mcshirt")
  $lindsey.equip("lindsey_panties")
  $lindsey.equip("lindsey_bra")
  $lindsey.equip("lindsey_skirt")
  $lindsey.equip("lindsey_shirt")
  return
