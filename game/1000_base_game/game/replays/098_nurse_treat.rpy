init python:
  register_replay("nurse_treat","Nurse's Treat","replays nurse_treat","replay_nurse_treat")

label replay_nurse_treat:
  show nurse date_pancakes with fadehold
  nurse date_pancakes "This is lovely. Thank you again."
  mc "It really is! I'm glad we stayed for dinner."
  nurse date_pancakes "Well, I did get all dressed up..."
  mc "Heh. Not only you did, but you look great, too."
  nurse date_pancakes "Thanks to {i}your{/} help, anyway."
  mc "Was it? I mean, you could make even a potato sack look hot."
  nurse date_pancakes "Oh, stop it, you!"
  mc "I'm serious!"
  nurse date_pancakes "It {i}is{/} nice to get out of those scrubs and into something pretty every now and then..."
  nurse date_pancakes "And these pancakes are delicious, by the way!"
  mc "They are, aren't they?"
  mc "I guess the night wasn't a total waste, after all."
  nurse date_pancakes "Not at all!"
  nurse date_pancakes "I'm glad you're the one I'm sharing this moment with, to be honest."
  nurse date_pancakes "Did you know I was one of the popular ones in high school?"
  mc "Oh, really? I had no idea."
  nurse date_pancakes "It's true! Well, I was friends with the popular girls, at least."
  nurse date_pancakes "Although, I got along with everyone, really."
  mc "Now, that part doesn't surprise me one bit."
  # nurse date_pancakes "I just wish I had been better about being kinder to everyone back then, and not going along with what everyone else did."
  nurse date_pancakes "I just wish I had been better about being kinder to everyone back then,{space=-50}\nand not going along with what everyone else did."
  nurse date_pancakes "I suppose that's why I try harder these days."
  "I wonder if [kate] will ever reflect back on her high school years like this..."
  mc "Well, it's never too late to start trying to be a better person, right?"
  nurse date_pancakes "Exactly!"
  nurse date_pancakes "And, um, I think you're certainly one of the good ones, [mc]."
  mc "Likewise, [nurse]..."
  nurse date_pancakes "..."
  nurse date_pancakes "Goodness, those pancakes go down easy with that ice cream, don't they?"
  mc "They really do. I'm glad I agreed to be your moral support!"
  nurse date_pancakes "Heh! So am I."
  scene location with fadehold
  return
