init python:
  register_replay("nurse_first_strike","Nurse's First Strike","replays nurse_first_strike","replay_nurse_first_strike")

label replay_nurse_first_strike:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  $x = game.location
  show nurse neutral
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $game.location = "school_nurse_room"
  $nurse["outfit_stamp"] = nurse.outfit
  if game.season == 1:
    $nurse.outfit = {"outfit":"nurse_outfit", "shirt":"nurse_shirt", "bra":"nurse_bra", "panties":"nurse_pantys"}
  elif game.season == 2:
    $nurse.outfit = {"hat":"nurse_hat", "shirt":"nurse_dress", "bra":"nurse_green_bra", "panties":"nurse_green_panties"}
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  nurse neutral "Gosh, I'm not sure you should be looking through that book\nright now..."
  mc "Hmm? This little book? And why is that?"
  nurse annoyed "Because... it's inappropriate, and there's people just outside."
  mc "But they don't know what these strikes mean, right?"
  mc "They're our little secret."
  nurse annoyed "Oh, I don't know. I get really nervous when you flip through\nthose pages."
  mc "I can imagine."
  mc "At any time, I could redeem a strike..."
  nurse afraid "Oh, but I don't think that's necessary! Anyone could walk in!"
  mc "But that's part of the fun, isn't it?"
  mc "As a matter of fact, I'm redeeming one strike right now."
  nurse surprised "You're joking!"
  mc "Does it look like I'm joking? Look, I already erased one strike."
  nurse afraid "Please, don't! Not right now!"
  mc "It's done. I'm not really in the mood for an argument right now."
  mc "But you know I have those photos."
  nurse sad "But—"
  mc "No buts except your butt."
  mc "Strip."
  nurse concerned "..."
  mc "You're spending the rest of the day naked in your office. End of argument."
  nurse concerned "Oh, my goodness..."
  nurse concerned "Gosh, okay, but what if someone comes in?"
  window hide
  pause 0.25
  if game.season == 1:
    $nurse.unequip("nurse_shirt")
  elif game.season == 2:
    $nurse.unequip("nurse_hat")
    $nurse.unequip("nurse_dress")
  pause 0.75
  window auto show
  show nurse thinking with dissolve2
  nurse thinking "This is enough, right?"
  mc "Keep going."
  nurse afraid "But I'll be naked!"
  mc "That's the point!"
  nurse annoyed "Oh, um. Very well. This is making me really nervous."
  window hide
  pause 0.25
  if game.season == 1:
    $nurse.unequip("nurse_bra")
  elif game.season == 2:
    $nurse.unequip("nurse_green_bra")
  pause 0.75
  window auto show
  show nurse neutral with dissolve2
  nurse neutral "I don't know what I'll do if someone comes in..."
  nurse neutral "Sorry, but this is..."
  mc "It's pretty hot, right?"
  mc "Stripping on command."
  nurse thinking "I don't know about that..."
  mc "Well, give me your panties and I'll have a look. I'm sure they're soaked."
  nurse concerned "That's not true..."
  mc "Let's find out."
  nurse concerned "Okay, okay. Gosh."
  window hide
  pause 0.25
  if game.season == 1:
    $nurse.unequip("nurse_pantys")
  elif game.season == 2:
    $nurse.unequip("nurse_green_panties")
  pause 0.75
  window auto show
  show nurse sad with dissolve2
  nurse sad "This must be enough, right?"
  mc "Well, you're completely naked, so I'd say so."
  mc "Unless you want to take a lap around the school?"
  nurse afraid "No, no! I'm fine, thank you!"
  mc "Keep your clothes locked in one of those drawers for the rest of\nthe day."
  mc "And don't you dare to put them on. I could come in at any moment."
  nurse annoyed "Okay, all right. I promise."
  window hide
  pause 0.5
  play sound "open_door"
  pause 0.25
  show nurse afraid with Dissolve(.5)
  show nurse afraid at move_to(.25,1.0)
  show maxine neutral at appear_from_right(.75)
  pause 0.5
  window auto
  maxine neutral "Did the mites eat your clothes too? That is fascinating."
  nurse afraid "Mites? Err, I..."
  mc "She just likes going naked."
  nurse surprised "That is not true!"
  maxine smile "Very well. I need seven gallons of malic acid, thank you."
  nurse annoyed "I don't think I can help you with that, [maxine]..."
  maxine neutral "That is unfortunate. Do you know where I can find crab apples?"
  nurse annoyed "..."
  "[maxine] doesn't seem fazed by the [nurse]'s nudity at all."
  "The [nurse], on the other hand, is sweating and squirming."
  "If her pussy wasn't glistening with her juices, one could think she didn't enjoy the situation."
  nurse annoyed "I don't know..."
  maxine neutral "What about you, [mc]?"
  mc "What about me?"
  maxine skeptical "Your crab apples. Hand them over."
  mc "I don't have any crab apples."
  maxine skeptical "You seem like someone who would hide crab apples."
  mc "Well, I don't."
  maxine annoyed "Would you submit yourself to a cavity search?"
  mc "...no?"
  maxine concerned "Very well."
  maxine concerned "If either of you see anyone in possession of crab apples, inform me immediately."
  window hide
  show nurse annoyed at move_to(.5,1.25)
  show maxine concerned at disappear_to_right
  pause 0.5
  show nurse concerned with Dissolve(.5)
  window auto
  nurse concerned "My goodness. I hope she doesn't report me."
  mc "I don't think [maxine] has much credibility with the principal. You could{space=-25}\njust deny it."
  nurse thinking "God, I hope so..."
  nurse sad "Would you please let me get dressed now?"
  mc "Absolutely not. You still have hours and hours left."
  mc "I'll be back to check on you."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $nurse.outfit = nurse["outfit_stamp"]
  $game.location = x
  pause 1.0
  hide nurse
  hide black onlayer screens
  with Dissolve(.5)
  window auto

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
