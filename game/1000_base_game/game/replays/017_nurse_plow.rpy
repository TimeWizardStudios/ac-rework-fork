init python:
  register_replay("nurse_plow","Nurse's Garden","replays nurse_plow","replay_nurse_plow")

label replay_nurse_plow:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  $x = game.location
  $game.location = school_forest_glade
  show nurse annoyed with Dissolve (.5)
  #nurse annoyed "What is this place?"
  #mc "It's my little farm... sort of."
  #nurse thinking "It doesn't look like a farm to me."
  #mc "Yeah, it still requires a bit of work, but that's why I brought you here."
  nurse thinking "I still don't know what you have in mind..."
  mc "See that patch of grass over there? That needs plowing."
  nurse smile "How are you going to do that?"
  mc "I got this plow and now I got the cattle to pull it."
  nurse annoyed "I don't see any cattle..."
  mc "Well, I took a picture of her earlier. Have a look."
  nurse neutral "..."
  nurse surprised "That's me!"
  mc "Right. You're going to be my little plow-cow today."
  nurse afraid "You can't be serious..."
  mc "Well, it's up to you really. Do you want to keep your job and reputation?"
  nurse sad "I do, but—"
  mc "There's no two ways about it. And you're already here, so let's get you naked and ready for the plow."
  nurse surprised "Naked?!"
  mc "I mean, unless you want your scrubs and coat to get all dirty."
  nurse sad "..."
  mc "Come on, we're wasting precious daylight."
  mc "Nothing to be embarrassed about out here anyway. Just forest critters."
  nurse sad "I... I suppose..."
  #$nurse.unequip("nurse_outfit")
  mc "It's fine, you can give me your clothes, I'll make sure they're safe."
  $nurse.unequip("nurse_shirt")
  nurse annoyed "Now what?"
  mc "Are you giving me attitude?"
  nurse afraid "No! I wasn't!"
  mc "Well, then. What are you waiting for?"
  nurse thinking "Waiting for?"
  mc "Take off the rest. Now."
  nurse afraid "..."
  $nurse.unequip("nurse_bra")
  mc "That wasn't so hard, was it?"
  $nurse.unequip("nurse_pantys")
  nurse annoyed "I... I guess not..."
  mc "Well then, Daisy. Let's get you harnessed up for the plow."
  #Fade to black (blackscreen until fade in)
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  mc "..."
  #SFX: rustle of chains, creaking of leather
  mc "Okay, there we go."
  mc "Are you ready?"
  nurse "Y-yes..."
  mc "Pull!"
  show nurse plowcow
  hide black with Dissolve (.5)
  $set_dialog_mode()
  mc "Pull, my little moo-cow!"
  nurse "..."
  "Watching her muscles strain, her thighs flexing against the weight of the plow, lights something dark within me."
  "It's in the way her eyes crackle with determination, while her cheeks burn in utter shame."
  "I definitely struck gold that day in the [nurse]'s office. Whenever that photo enters my mind, it fuels a primal sadism."
  "And it's a perfect match because deep down, she wants this too. I can see the submissiveness in her downcast eyes."
  "She has urges that might be darker than mine, which is so refreshing when it comes to women."
  "Vanilla is the lamest form of sexual desires. It's like eating a bowl of pure rice. Sure, it satisfies the needs, but there's no flavor."
  "If only I had a larger field to plow, I'd let her work in the sun for hours. Driving her trembling legs on with the harsh crack of my whip."
  "Seeing the sweat drip down her back, following her spine before seeping into her asscrack."
  "Smelling the trailing scent of her pussy as it weeps in humiliation."
  "There's nothing quite like it."
  $nurse.equip("nurse_shirt")
  $nurse.equip("nurse_bra")
  $nurse.equip("nurse_pantys")
  $game.location = x
  hide nurse with Dissolve(.5)

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
