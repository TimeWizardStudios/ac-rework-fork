init python:
  register_replay("mrsl_workout","Mrs. L's Workout","replays mrsl_workout","replay_mrsl_workout")

label replay_mrsl_workout:
  show expression LiveComposite((1920,1080),(-275,0),"school gym gym",(1057,271),"school gym door_right",(1304,181),"school gym banner",(0,349),"school gym bleachers",(0,204),"school gym window_left_night",(745,289),"school gym window_right_night",(388,247),"school gym score_board",(432,290),"school gym score_board_text",(0,259),"school gym fence",(156,477),"school gym door_left",(237,531),"school gym signup",(93,591),"school gym bin",(0,515),"school gym vending_machine",(1472,364),"school gym ladder",(1617,0),"school gym backboard_top",(1739,75),"school gym backboard_bottom",(106,0),"school gym ring_left",(937,473),"school gym net",(17,875),"school gym hoops",(1250,704),"school gym ball_5",(1073,925),"school gym ball_3",(175,700),"school gym ball_2",(67,700),"school gym ball_1",(1445,768),"school gym table",(1593,814),"school gym shorts",(1725,813),"school gym paddles",(843,55),"school gym broken_light",(1824,741),"school gym book",(0,0),"school gym overlay") as makeshift_location
  show expression LiveComposite((569,1080),(97,116),"mrsl avatar body2_ponytail",(128,176),"mrsl avatar face_excited",(152,736),"mrsl avatar b2workout_panties",(96,358),"mrsl avatar b2workout_bra",(135,681),"mrsl avatar b2leggings",(80,379),"mrsl avatar b2arm2_n",(145,624),"mrsl avatar b2workout_bra_fix") as makeshift_mrsl
  with fadehold
  mrsl "First, we must stretch."
  # mrsl "It's very important to make sure you're plenty {i}limber{/} and, well... loose."
  mrsl "It's very important to make sure you're plenty {i}limber{/} and, well... loose.{space=-45}"
  # "The way those words leave her mouth, all slow and sensual, automatically accelerates my heart rate."
  "The way those words leave her mouth, all slow and sensual, automatically{space=-120}\naccelerates my heart rate."
  mc "Very important, indeed..."
  mrsl "Come a little closer and help me out, will you?"
  window hide
  show mrsl bent_forward
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide makeshift_location
  hide makeshift_mrsl
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "Just as she says this, she bends forward, reaching down to touch her toes."
  "Goddamn! It looks like she's still just as flexible as in that video."
  mc "That's, err... very impressive."
  mrsl bent_forward "Oh, you haven't seen anything yet!"
  "Mmm... I really want to see it all..."
  mrsl bent_forward "Can you help me stretch my thighs?"
  mrsl bent_forward "Just put your hands on them and apply some pressure, okay?"
  mc "O-okay."
  window hide
  pause 0.125
  show mrsl bent_forward mc_help with Dissolve(.5)
  pause 0.125
  window auto
  "This is exhilarating."
  # "Coming this close, smelling her fiery scent and placing my hands on the backs of her thighs while she continues to bend forward."
  "Coming this close, smelling her fiery scent and placing my hands on{space=-5}\nthe backs of her thighs while she continues to bend forward."
  "Her round, firm ass resting right there in my face."
  mrsl bent_forward mc_help "That's it, [mc]!"
  mrsl bent_forward mc_help "Mmm! That feels good!"
  "God, it really does..."
  "The feel of her muscular, toned legs shifting beneath my fingers..."
  "Her ass so close I could just slide my hand up to have a touch..."
  mrsl bent_forward mc_help "Okay! It's your turn, now."
  mrsl bent_forward mc_help "Lie back, will you?"
  window hide
  show mrsl leg_stretch
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  # "With my back flat on the floor, she towers over me, like some sort of sex goddess above her devout follower."
  "With my back flat on the floor, she towers over me, like some sort of{space=-15}\nsex goddess above her devout follower."
  "And then she moves in, pressing one of my legs back."
  "I have never been particularly flexible, but her presence seems to bolster me, somehow."
  "A smile plays on her lips as her crimson nails dig into the skin of my leg."
  "Despite my best efforts, a groan of pleasure escapes me."
  "It's like her hands have a magical current of electrical discharge into my body, and my mind starts to wander."
  "And my dick immediately stiffens in my shorts."
  mrsl leg_stretch "That's better already."
  mrsl leg_stretch "You're very tight. Too much strain in that young body of yours."
  mc "Err, right..."
  mrsl leg_stretch "That's exactly what this is for, though!"
  mrsl leg_stretch "Are you feeling better?"
  mc "Much better..."
  mrsl leg_stretch "Perfect! Up you get, then."
  window hide
  show expression LiveComposite((1920,1080),(-275,0),"school gym gym",(1057,271),"school gym door_right",(1304,181),"school gym banner",(0,349),"school gym bleachers",(0,204),"school gym window_left_night",(745,289),"school gym window_right_night",(388,247),"school gym score_board",(432,290),"school gym score_board_text",(0,259),"school gym fence",(156,477),"school gym door_left",(237,531),"school gym signup",(93,591),"school gym bin",(0,515),"school gym vending_machine",(1472,364),"school gym ladder",(1617,0),"school gym backboard_top",(1739,75),"school gym backboard_bottom",(106,0),"school gym ring_left",(937,473),"school gym net",(17,875),"school gym hoops",(1250,704),"school gym ball_5",(1073,925),"school gym ball_3",(175,700),"school gym ball_2",(67,700),"school gym ball_1",(1445,768),"school gym table",(1593,814),"school gym shorts",(1725,813),"school gym paddles",(843,55),"school gym broken_light",(1824,741),"school gym book",(0,0),"school gym overlay") as makeshift_location
  show expression LiveComposite((569,1080),(97,116),"mrsl avatar body2_ponytail",(128,176),"mrsl avatar face_excited",(152,736),"mrsl avatar b2workout_panties",(96,358),"mrsl avatar b2workout_bra",(135,681),"mrsl avatar b2leggings",(80,379),"mrsl avatar b2arm2_n",(145,624),"mrsl avatar b2workout_bra_fix") as makeshift_mrsl
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Oh, boy! It's finally time!"
  "All my hard work has led me to this workout."
  # "But not just any workout. A workout with the hottest teacher in school."
  "But not just any workout. A workout with the hottest teacher in school.{space=-60}"
  mrsl "Ready?"
  mc "I have never been more ready."
  mrsl "Okay! Let's hot your bottom!"
  window hide
  show mrsl workout knee_up_jumps
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide makeshift_location
  hide makeshift_mrsl
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "She takes me through a much more grueling regimen than I expected."
  "But watching her do all the moves makes all the sweat and pain worthwhile."
  "To see [mrsl] there across from me, boobs bouncing in her tight, sweaty top..."
  "Beads of sweat clinging to her heaving chest."
  show mrsl workout jumping_jacks with dissolve2
  mrsl workout jumping_jacks "You're doing great, [mc]!"
  mrsl workout jumping_jacks "So strong! And determined!"
  mc "Th-thanks!"
  show mrsl workout side_lunges with dissolve2
  "The word wheezes out past my lips as her movements turn into a blur."
  "She moves with an otherworldly grace that leaves my mouth open and watering."
  "Like some sort of goddess, her body moves in impossible ways."
  show mrsl workout leg_raise with dissolve2
  "She bends in ways I never knew a person could bend."
  "She's in complete control of her own body."
  "Moving like a lithe, graceful dancer. Sensual, sexy, dangerous."
  show mrsl workout squats with dissolve2
  "Her powerful ass and thighs leave nothing to the imagination in that skin-tight outfit."
  "It makes my dick ache in my shorts even as I sweat from the routine."
  "Sweat from her fiery eyes watching me just as I watch her."
  show mrsl workout downward_facing_dog with dissolve2
  mrsl workout downward_facing_dog "One last push, [mc]! We're nearly done!"
  mrsl workout downward_facing_dog "You're going to have the hottest bottom, all right!"
  "I had no idea a body could be as flexible as hers..."
  "Never in my decades of porn abuse have I ever come across something so incredible."
  "Her body truly is a work of art, and a dizzying passion overcomes me as I marvel at her divine perfection."
  window hide
  show expression LiveComposite((1920,1080),(-275,0),"school gym gym",(1057,271),"school gym door_right",(1304,181),"school gym banner",(0,349),"school gym bleachers",(0,204),"school gym window_left_night",(745,289),"school gym window_right_night",(388,247),"school gym score_board",(432,290),"school gym score_board_text",(0,259),"school gym fence",(156,477),"school gym door_left",(237,531),"school gym signup",(93,591),"school gym bin",(0,515),"school gym vending_machine",(1472,364),"school gym ladder",(1617,0),"school gym backboard_top",(1739,75),"school gym backboard_bottom",(106,0),"school gym ring_left",(937,473),"school gym net",(17,875),"school gym hoops",(1250,704),"school gym ball_5",(1073,925),"school gym ball_3",(175,700),"school gym ball_2",(67,700),"school gym ball_1",(1445,768),"school gym table",(1593,814),"school gym shorts",(1725,813),"school gym paddles",(843,55),"school gym broken_light",(1824,741),"school gym book",(0,0),"school gym overlay") as makeshift_location
  show expression LiveComposite((569,1080),(97,116),"mrsl avatar body2_ponytail",(128,176),"mrsl avatar face_excited",(152,736),"mrsl avatar b2workout_panties",(96,358),"mrsl avatar b2workout_bra",(135,681),"mrsl avatar b2leggings",(80,379),"mrsl avatar b2arm2_n",(145,624),"mrsl avatar b2workout_bra_fix") as makeshift_mrsl
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Finally, just when I think I can't take anymore, we stop."
  mrsl "Well? What did you think?"
  mc "That was... {i}huff...{/} quite the... {i}puff...{/} workout..."
  "She just laughs and beams..."
  show expression LiveComposite((569,1080),(97,116),"mrsl avatar body2_ponytail",(157,210),"mrsl avatar face_concerned",(152,736),"mrsl avatar b2workout_panties",(96,358),"mrsl avatar b2workout_bra",(135,681),"mrsl avatar b2leggings",(131,631),"mrsl avatar b2arm1_n") as makeshift_mrsl with dissolve2
  extend " then turns serious."
  mrsl "Lie back on the mat."
  mrsl "I will massage out your thighs so that they don't cramp."
  mc "Err, okay."
  window hide
  hide screen interface_hider
  # show mrsl clothed_titjob anticipation
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Oh, man. Lying down and relaxing my muscles is such a blessing after that murderous workout..."
  "Eyes closed, my ragged breath slowly returns to normal."
  # "But my mind keeps swimming in a cocktail of endorphins, dopamine, and perhaps something else entirely."
  "But my mind keeps swimming in a cocktail of endorphins, dopamine,{space=-15}\nand perhaps something else entirely."
  "The world keeps spinning behind my eyes."
  "They say that workout is a powerful drug, but this is nothing like I've ever felt before."
  "Then, [mrsl]'s hands start to massage my thigh."
  "Her fingers dig in. So very close to my groin."
  mrsl "Does that feel good?"
  mc "Mmm... yes..."
  mc "It feels so good... and relaxing..."
  "It puts my mind and body at ease."
  "A calm, post-workout fog settles over me."
  "The world moves slowly, drifts past me. I'm drifting."
  "My shorts roll down my legs."
  "My semi-erect dick springs free, hardening under her gentle touch."
  mc "Wha—"
  show black onlayer screens zorder 100
  pause 0.5
  show mrsl clothed_titjob anticipation: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  pause 0.0
  hide makeshift_location
  hide makeshift_mrsl
  hide black
  hide black onlayer screens
# show screen interface_hider
  with Dissolve(.5)
  hide mrsl
  show mrsl clothed_titjob anticipation ## This is just to avoid the screen shake if the player is skipping
  window auto
  $set_dialog_mode("")
  mc "Oh, f-fuck!"
  # "In a haze of blurry passion, she lifts her top, taking my dick between her breasts."
  "In a haze of blurry passion, she lifts her top, taking my dick between{space=-10}\nher breasts."
  "They envelop me in a valley of silky bliss, softer than anything I've ever experienced."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob1 with Dissolve(.5)
  pause 0.25
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  # "With her hands on the sides of her boobs, she pushes them together around my cock."
  "With her hands on the sides of her boobs, she pushes them together{space=-20}\naround my cock."
  # "They sweat from the workout mixes with my sticky precum, creating a perfect lubrication."
  "The sweat from the workout mixes with my sticky precum, creating{space=-5}\na perfect lubrication."
  "The sensation sends a new hit of euphoria and released tension kindling through me."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  mc "Oooooooh!"
  mc "That really... does feel... so gooood!"
  "The last word leaves me in a desperate groan."
  "The sensation almost knocks me out."
  "My head keeps spinning and my eyes water."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  # "It feels like a dream when she starts to slide her boobs up and down my shaft."
  "It feels like a dream when she starts to slide her boobs up and down{space=-15}\nmy shaft."
  "Pushing them together even more to create the feeling of a vagina."
  "But it feels nothing like one."
  # "It's almost like fucking warm jelly... warm perfect jelly that fits perfectly around me."
  "It's almost like fucking warm jelly... warm perfect jelly that fits perfectly{space=-70}\naround me."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  mc "God... y-yes..."
  mc "This is just... like my dream..."
  mrsl clothed_titjob titjob1 "Hmm?"
  mc "Err, nothing! Don't stop!"
  window hide
  pause 0.125
  show mrsl clothed_titjob lick1 with Dissolve(.5)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.25
  window auto
  "Thankfully, she does the exact opposite of stopping."
  "She twirls her tongue around the tip of my dick, teasing the hole with her tongue."
  "Tasting the clear sticky fluid that oozes out."
  "She licks and sucks it up as it comes out, slurping loudly."
  "Sounds I've never heard before. Not even in porn."
  window hide
  pause 0.125
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.25
  window auto
  "Sweat rolls down my brow, burning into my eyes."
  "This feels too good to be true."
  "It's like some sort of perfect wet dream, but one where you don't wake up just before finishing."
  "It almost feels like a compulsion. I need to finish."
  "I need to finish before I wake up!"
  window hide
  pause 0.125
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob orgasm with Move((0,15), (0,-15), .075, bounce=True, repeat=True, delay=.275)
  pause 0.25
  window auto
  mc "Ohhhhhhh!"
  "After everything... the workout... this..."
  "It sends me into outer fucking space."
  "Stars explode across my eyes, and I explode in her mouth."
  "Shooting my cum straight onto her tongue."
  "She tastes my hot, sticky semen. Savors it."
  show mrsl clothed_titjob titjob3 with dissolve2
  "Swirls it around in her mouth, before finally swallowing."
  "Careful to drink every last drop."
  mc "Oh... my god..."
  mc "That was... amazing..."
  scene location with fadehold
  return
