init python:
  register_replay("jacklyn_statement_date","Jacklyn's Meal","replays jacklyn_statement_date","replay_jacklyn_statement_date")

label replay_jacklyn_statement_date:
  show jacklyn jacklyn_date with Dissolve(.5)
  jacklyn "Cozy."
  mc "I know it's not much, but I've always found overblown romantic gestures kinda cringe."
  show jacklyn jacklyn_date_nervous with Dissolve(.5)
  jacklyn "Romanticism is a bygone era."
  "..."
  show jacklyn jacklyn_date with Dissolve(.5)
  mc  "So... err."
  "Crap. What do you even say on dates?"
  "My mind's totally blank."
  mc "Did you like the food?"
  if quest.jacklyn_statement["all_ingredients"]:
    show jacklyn jacklyn_date_flirty with Dissolve(.5)
    jacklyn "It's pussy dew!"
    show jacklyn jacklyn_date_laughing with Dissolve(.5)
    jacklyn "Four out of four toasters!"
    mc "Heh, thanks!"
    show jacklyn jacklyn_date_smile with Dissolve(.5)
    mc "What did you think of the Whooshster™ Sauce?"
    show jacklyn jacklyn_date_flirty with Dissolve(.5)
    jacklyn "Blew me away. Totally made me straight!"
    mc "Thought you were straight?"
    show jacklyn jacklyn_date_laughing with Dissolve(.5)
    jacklyn "Oh, baby boy. I meant it woke me the fuck up."
    show jacklyn jacklyn_date_smile with Dissolve(.5)
    jacklyn "You know I don't dabble."
    mc "Ah! Got confused there for a second!"
  else:
    show jacklyn jacklyn_date_laughing with Dissolve(.5)
    jacklyn "It's a pass over the bar."
    mc "Great."
  "Hmm... need to think of a good topic."
  menu(side="far_left"):
    extend ""
    "\"What's your favorite game?\"":
      mc "What's your favorite game?"
      show jacklyn jacklyn_date_smile with Dissolve(.5)
      jacklyn "I only play love games."
      show jacklyn jacklyn_date_smile with Dissolve(.5)
      mc "Err... same. Video games are for nerds."
      show jacklyn jacklyn_date_flirty with Dissolve(.5)
      jacklyn "If that's your passion, then shake that virtual ass."
      mc "Ah, yeah. Definitely. I do play some video games. I really like Castlevania."
      show jacklyn jacklyn_date_neutral with Dissolve(.5)
      jacklyn "Can't say I've heard of it."
    "\"Why do you want to become an art teacher?\"":
      mc "Why do you want to become an art teacher?"
      show jacklyn jacklyn_date_neutral with Dissolve(.5)
      jacklyn "Art is my weeping heart. Always has been."
      show jacklyn jacklyn_date_smile with Dissolve(.5)
      jacklyn "Plus it's chill as fuck. Just watch a bunch of students splash color on a canvas all day."
      mc "Never thought of that. Getting a chill job is pretty legit."
      show jacklyn jacklyn_date_laughing with Dissolve(.5)
      jacklyn "Yeah, and I can dress however the fuck I want and call it part of the image."
      mc "I do like the way you dress. It's a very strong statement."
      show jacklyn jacklyn_date_flirty with Dissolve(.5)
      jacklyn "You're totally rubbing my clit, right now."
      mc "Err... you're welcome?"
    "\"Would you like to hear a funny story?\"":
      mc "Would you like to hear a funny story?"
      show jacklyn jacklyn_date_neutral with Dissolve(.5)
      jacklyn "Hit me, shooter."
      mc "Okay, here goes..."
      mc "I once had a nightmare that felt like years."
      mc "I watched my life pass before my eyes. It slipped between my fingers." 
      mc "[flora] got engaged to some douchebag with a BMW."
      mc "I dropped out of college due to depression, and started working some dead end job."
      mc "Each night was just me playing video games and watching porn."
      mc "And then I woke up, and life seems entirely different."
      mc "In my dream, Mrs. Lichtenstein was this introverted loner. Mrs. Bloomer an old hag."
      mc "No girls ever wanted anything to do with me."
      mc "I haven't told anyone this, but I'm so relieved I woke up. It almost feels like I traveled back in time."
      mc "Now you're here, and honestly, you're a splash of vibrant color in my life."
      show jacklyn jacklyn_date_laughing with Dissolve(.5)
      jacklyn "That's smooch as hell."
      show jacklyn jacklyn_date_flirty with Dissolve(.5)
      jacklyn "Excuse me for sucking your cock, but that's quite amazing."
      show jacklyn jacklyn_date_smile with Dissolve(.5)
      jacklyn "It's like one of those origin stories. What a relief."
      mc "Happy you liked it."
  show jacklyn jacklyn_date_surprised with Dissolve(.5)
  jacklyn "What was that?"
  mc "Huh?"
  show jacklyn jacklyn_date_surprised with Dissolve(.5)
  jacklyn "Is someone else here?"
  mc "No, [jo]'s at work and [flora] is at her internship."
  show jacklyn jacklyn_date_nervous with Dissolve(.5)
  jacklyn "That's pretty damn freddy kruger if you ask me..."
  mc "Wait here, I'll go have a look."
  show jacklyn jacklyn_date_neutral with Dissolve(.5)
  jacklyn "That's how you get kebabbed."
  mc "It's probably nothing. [flora] sometimes leaves the window open and the neighbor's cat climbs in."
  mc "But err... be ready to call the cops."
  hide jacklyn with Dissolve(.5)
  return
