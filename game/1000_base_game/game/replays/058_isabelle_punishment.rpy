init python:
  register_replay("isabelle_punishment","Isabelle's Punishment","replays isabelle_punishment","replay_isabelle_punishment")

label replay_isabelle_punishment:
  show kate paddling_arrival_isabelle_defiant_ballgag with fadehold
  isabelle "Mmmph!"
  "Holy crap."
  "[kate] wasn't joking about putting [isabelle] in her place..."
  "She's completely helpless, her arms and legs folded and bound."
  "And even if she somehow managed to get free, they're four on one."
  "Still, there's fire in her eyes."
  "If only I knew where [maxine]'s secret popcorn dispenser is..."
  "Oh, well. I'm just going to sit back and enjoy the show."
  "I wonder if they'll be able to break her?"
  "It's going to be a battle of wills for sure, but [isabelle] seems to be at a slight disadvantage."
  kate paddling_arrival_isabelle_defiant_ballgag "She's still being defiant, I see."
  isabelle "Mmmph!"
  casey "Quiet, bitch."
  lacey "Yeah! No one told you to speak!"
  "God, they're so strict with her already..."
  "But maybe that's what she needs?"
  kate paddling_arrival_isabelle_defiant_ballgag "Listen carefully, bitch. You lost, and I'm going to enjoy breaking you.{space=-5}"
  kate paddling_arrival_isabelle_defiant_ballgag "I have been planning for this moment for weeks."
  kate paddling_arrival_isabelle_defiant_ballgag "You're exactly the type of girl I want as a pet."
  kate paddling_arrival_isabelle_defiant_ballgag "Dumb and ignorant subs are boring... but you're the opposite."
  kate paddling_arrival_isabelle_defiant_ballgag "You're fiery, headstrong, independent, and intelligent."
  kate paddling_arrival_isabelle_defiant_ballgag "You'll truly understand and appreciate every facet of humiliation that I have to offer."
  kate paddling_arrival_isabelle_defiant_ballgag "Now, what do you say when you get a compliment?"
  isabelle "..."
  stacy "Thank her, bitch!"
  lacey "Yeah! Thank her!"
  casey "We told you what happens if you're rude."
  "It's astonishing to watch the power struggle."
  "Despite being tied up, [isabelle] is proving a tough opponent."
  "She's just not submissive yet. Quite the opposite."
  "It's like watching the superbowl of domination, with the two top teams facing off."
  isabelle "Maammk-uuuh."
  kate paddling_arrival_isabelle_defiant_ballgag "Was that a \"thank you,\" or a \"fuck you?\""
  menu(side="middle"):
    extend ""
    "\"That was definitely a 'fuck you,'\nno doubt about it.\"":
      mc "That was definitely a \"fuck you,\" no doubt about it."
      "[isabelle] is a lot tougher than they think."
      kate paddling_arrival_isabelle_defiant_ballgag "I think you're right."
      kate paddling_arrival_isabelle_defiant_ballgag "She always was a rude cunt."
      lacey "Yeah, always the worst!"
      stacy "Total hoebag."
      casey "She doesn't know what respect is."
      kate paddling_arrival_isabelle_defiant_ballgag "We'll teach her, then."
      lacey "Queen!"
    "\"That sounded like a 'thank you' to me.\"":
      mc "That sounded like a \"thank you\" to me."
      "They're being really harsh with [isabelle]. I feel a bit like Judas now..."
      kate paddling_arrival_isabelle_defiant_ballgag "Let's find out, then, shall we?"
      kate paddling_arrival_isabelle_defiant_ballgag "Would you unmuzzle the bitch for a bit, [lacey]?"
      lacey "With pleasure."
      window hide
      show kate paddling_lacey_ballgag_isabelle_defiant with Dissolve(.5)
      window auto
      kate paddling_lacey_ballgag_isabelle_defiant "Now, [isabelle], repeat what you just said."
      isabelle "Fuck you, you psychotic slag!"
      show kate paddling_lacey_hair_pull_stacy_foot_casey_leash_pull
      isabelle "Hnnngh!" with vpunch
      lacey "Bad bitch!"
      casey "Try again!"
      "Wow, they're really showing her who's in charge..."
      isabelle "T-thank... y-you..."
      stacy "Thank you, what?"
      lacey "Yeah! Show some respect!"
      isabelle "Thank you... ma'am..."
      kate paddling_lacey_hair_pull_stacy_foot_casey_leash_pull "What do you think, ladies? Was that sincere enough?"
  casey "I think she needs to be taught a real lesson."
  kate "You're absolutely right."
  kate "I normally warm my pets up with a flogger, but I'm not feeling particularly charitable today."
  kate "[lacey], give her ten hard strokes with the paddle."
  if renpy.showing("kate paddling_arrival_isabelle_defiant_ballgag"):
    kate "And take the ballgag out, I want to hear her scream."
  else:
    kate "Leave the ballgag out, I want to hear her scream."
  lacey "Yes!"
  window hide
  if renpy.showing("kate paddling_arrival_isabelle_defiant_ballgag"):
    show kate paddling_lacey_ballgag_isabelle_defiant with Dissolve(.5)
    pause 0.25
  show kate paddling_lacey_paddle1_isabelle_defiant with Dissolve(.5)
  window auto
  stacy "Should we make her count?"
  kate paddling_lacey_paddle1_isabelle_defiant "Nah, I just want to hear her beg."
  kate paddling_lacey_paddle1_isabelle_defiant "Are you going to beg for me, [isabelle]?"
  isabelle "..."
  kate paddling_lacey_paddle1_isabelle_defiant "How about you try right now?"
  kate paddling_lacey_paddle1_isabelle_defiant "Beg for twenty strokes instead of ten."
  casey "Beg, bitch!"
  isabelle defiant "Forget it."
  kate paddling_lacey_paddle1_isabelle_defiant "Fine. Give her ten, [lacey]."
  kate paddling_lacey_paddle1_isabelle_defiant "Then, we'll see if she's ready to beg for more."
  lacey "Yes!"
  "God, [lacey] looks hot wielding that paddle."
  "I'm glad it's not my ass..."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_pain with Dissolve(.175)
  window auto
  isabelle "Mmmmph."
  casey "That was one."
  show kate paddling_lacey_paddle1_red_isabelle_defiant with dissolve2
  stacy "Nine more until we let you beg again."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_pain with Dissolve(.175)
  pause 0.25
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_pain with Dissolve(.175)
  window auto
  isabelle "Hnnngh!"
  "Wow, [isabelle] is tough."
  "Her face is turning as red as her ass, but she's holding back the screams."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_more_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_more_pain with Dissolve(.175)
  pause 0.0
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_more_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_more_pain with Dissolve(.175)
  window auto
  isabelle "Fffff! Oh, g-god!"
  kate paddling_lacey_paddle1_red_isabelle_more_pain "Looks like you got her good with those."
  lacey "Haha! I love it!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Owwwwwww! Ahhh, noo!"
  kate paddling_lacey_paddle1_red_isabelle_crying "All you have to do is beg for more, [isabelle]."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Nooo! Stop!"
  casey "That's not begging."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaah! Please stop!"
  kate paddling_lacey_paddle1_red_isabelle_crying "Wrong. Beg for more."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaaaah! Oh, my god!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Noooo! Aaaah!"
  kate paddling_lacey_paddle1_red_isabelle_crying "That was ten."
  kate paddling_lacey_paddle1_red_isabelle_crying "Are you ready to beg for ten more, or should we keep going?"
  show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
  isabelle "..."
  isabelle "Please... more..."
  kate paddling_lacey_paddle1_red_isabelle_pleading "You can do better than that."
  kate paddling_lacey_paddle1_red_isabelle_pleading "Be respectful."
  isabelle "P-please, ma'am... can I have ten more strokes?"
  kate paddling_lacey_paddle1_red_isabelle_pleading "You absolutely can. Go ahead, [lacey]."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  pause 0.15
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Ayeeeeee!"
  isabelle "God! Oh, god!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  pause 0.0
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaah!"
  "They're really being ruthless..."
  "I guess that's what it takes to tame someone like [isabelle]."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  pause 0.2
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Ooooh my goood! Pleeease, nooo mooore!"
  casey "Take it, bitch!"
  lacey "Yeah, take it!"
  stacy "You asked for this, remember?"
  isabelle "Nooo!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  kate paddling_lacey_paddle1_red_isabelle_crying "Yes, you did."
  kate paddling_lacey_paddle1_red_isabelle_crying "Now ask for five more, or we start over."
  show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
  isabelle "C-can I... please have... five more... ma'am?"
  kate paddling_lacey_paddle1_red_isabelle_pleading "God, you're pathetic."
  kate paddling_lacey_paddle1_red_isabelle_pleading "You thought you could stand up to me. Now look at you begging to be punished."
  show kate paddling_lacey_paddle1_red_isabelle_blush with Dissolve(.5)
  isabelle "Screw you... ma'am..."
  kate paddling_lacey_paddle1_red_isabelle_blush "Haha! God, I love that you're still defiant."
  kate paddling_lacey_paddle1_red_isabelle_blush "But I'll let that slide because you were polite in your rudeness."
  kate paddling_lacey_paddle1_red_isabelle_blush "I'm really looking forward to this year with you at my feet, [isabelle]."
  kate paddling_lacey_paddle1_red_isabelle_blush "Maybe the girls and I will bring you along to my skiing cabin during Christmas break."
  lacey "We could walk you naked in the snow!"
  kate paddling_lacey_paddle1_red_isabelle_blush "Doesn't that sound like fun?"
  kate paddling_lacey_paddle1_red_isabelle_blush "You could be our pet bitch for two weeks straight!"
  "Fuck, I suddenly have an urge to learn how to ski..."
  kate paddling_lacey_paddle1_red_isabelle_blush "Now, ask for five more like a good bitch."
  isabelle "P-please, ma'am... Can I have... five more...?"
  kate paddling_lacey_paddle1_red_isabelle_blush "Haha! You sure can."
  kate paddling_lacey_paddle1_red_isabelle_blush "But since you're so eager, perhaps we should give you more than five? What do you think, [mc]?"
  menu(side="middle"):
    extend ""
    "\"She does seem eager\nfor more punishment.\"":
      mc "She does seem eager for more punishment."
      kate paddling_lacey_paddle1_red_isabelle_blush "She does, doesn't she?"
      kate paddling_lacey_paddle1_red_isabelle_blush "I've always wanted a pain slut. Bitch, ask for ten more."
      "[isabelle] looks conflicted."
      "It's an unfair game, but it's not like she has a choice."
      isabelle "Um... can I please... have ten more instead, ma'am?"
      kate paddling_lacey_paddle1_red_isabelle_blush "Yes, you may. [lacey], go ahead."
      lacey "With pleasure..."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.25
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.15
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Aaaaaaaaaah! Ooooh!"
      "The paddle hits [isabelle] square across her buttcheeks, sending shockwaves through her ass."
      "For such a dimwit, [lacey] sure is wielding that paddle like a pro."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.2
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Ayeeeeeeh! Nooo!"
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Ooooh gooood!"
    "\"You're the expert, [kate].\"":
      mc "You're the expert, [kate]."
      kate paddling_lacey_paddle1_red_isabelle_blush "That's true."
      kate paddling_lacey_paddle1_red_isabelle_blush "I would suggest another forty, but then she probably wouldn't feel anything after."
      kate paddling_lacey_paddle1_red_isabelle_blush "And I'm far from done with her..."
      kate paddling_lacey_paddle1_red_isabelle_blush "[lacey], go ahead and give [isabelle] the last five."
      lacey "Yes!"
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Oooooooh, god!"
      kate paddling_lacey_paddle1_red_isabelle_crying "I like hearing you scream, but I think we can improve it."
      kate paddling_lacey_paddle1_red_isabelle_crying "For the next one, I want you to scream my name. Am I making myself clear?"
      show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
      isabelle "Y-yes... ma'am..."
      kate paddling_lacey_paddle1_red_isabelle_pleading "Good bitch."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Oooooooh, [kate]!"
      kate paddling_lacey_paddle1_red_isabelle_crying "Haha, yes! That has a better ring to it."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Ayeeeeeeeeh!"
  kate paddling_lacey_paddle1_red_isabelle_crying "All right, you only have one more, [isabelle]."
  kate paddling_lacey_paddle1_red_isabelle_crying "After [lacey] has delivered it, you're going to kiss the paddle and thank her. Understood?"
  show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
  isabelle "Yes... ma'am..."
  kate paddling_lacey_paddle1_red_isabelle_pleading "Go ahead, [lacey]."
  lacey "This one is going to hurt."
  lacey "Are you ready?"
  isabelle "Oh, god..."
  casey "She asked you a question, bitch."
  isabelle "I'm... I'm ready, ma'am..."
  lacey "We'll see about that."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaaaaaoooh!"
  lacey "There we go!"
  casey "Now kiss the paddle."
  stacy "Show the proper respect."
  window hide
  show kate paddling_lacey_paddle_kiss with Dissolve(.5)
  window auto
  isabelle "Thank you... Miss [lacey]..."
  isabelle "Thank you... Miss [casey]..."
  isabelle "Thank you... Miss [stacy]..."
  isabelle "Thank you... Miss [kate]..."
  kate paddling_lacey_paddle_kiss "Good girl."
  "It's weird seeing [isabelle] so humbled and docile..."
  "But I can't deny the boner in my pants."
  "It's not every day you get to see [kate] dominate someone who isn't submissive in nature."
  kate paddling_lacey_paddle_kiss "I've been fantasizing about this for a while now."
  kate paddling_lacey_paddle_kiss "You, down there, helpless and submissive."
  kate paddling_lacey_paddle_kiss "Accepting your punishment."
  kate paddling_lacey_paddle_kiss "And now that you understand your place a bit more, I think it's\ntime to... drive it home."
  kate paddling_lacey_paddle_kiss "[lacey], can you gag her again?"
  lacey "Of course!"
  window hide
  show kate paddling_lacey_ballgag_red_isabelle_blush with Dissolve(.5)
  pause 0.25
  show kate paddling_arrival_red_isabelle_blush_ballgag with Dissolve(.5)
  window auto
  kate paddling_arrival_red_isabelle_blush_ballgag "Be right back."
  window hide
  show kate paddling_kateless_isabelle_blush_ballgag with Dissolve(.5)
  window auto
  "What else does [kate] have in store?"
  "Knowing her, this was just the warm up."
  "And [isabelle]'s ass does look warm. It's practically glowing."
  "[lacey] sure did a number on her. She won't be sitting for the\nnext week."
  window hide
  show black onlayer screens zorder 100
  show kate strap_on_anticipation
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  kate strap_on_anticipation "A while back, I promised the girls that I would take your ass, [isabelle].{space=-25}"
  kate strap_on_anticipation "And, well... I never go back on my promises."
  kate strap_on_anticipation "I don't know if you've taken it in the ass before, but I guess there's a first time for everything."
  isabelle "Mmmph!"
  kate strap_on_anticipation "Don't be so upset. We did warm your ass up a bit with the paddle."
  kate strap_on_anticipation "I won't ask if you're ready; I can assure you that you're not."
  casey "Give it to her!"
  stacy "Fuck her ass!"
  lacey "Queen!"
  kate strap_on_anticipation "Here it comes, bitch."
  window hide
  show kate strap_on_poke with vpunch
  window auto
  isabelle "Mmmmph! Mmmph!"
  kate strap_on_poke "I know it hurts. That's the point."
  isabelle "Mmmph!"
  kate strap_on_poke "Good girl. Take it. Scream for me, and take it."
  window hide
  show kate strap_on_penetration1_isabelle_pain with vpunch
  window auto
  isabelle "Mmmph!"
  kate strap_on_penetration1_isabelle_pain "I'm going to get every last inch of it inside you."
  kate strap_on_penetration1_isabelle_pain "And then, I'm going to fuck you until you come."
  kate strap_on_penetration1_isabelle_pain "And then, if you ask nicely, I'm going to let you clean it with your mouth."
  window hide
  show kate strap_on_penetration2_isabelle_pain with vpunch
  window auto
  isabelle "Mmmmph!"
  kate strap_on_penetration2_isabelle_pain "Let me in, bitch."
  lacey "Make room for the queen!"
  "Fitting that whole thing inside her ass seems impossible, but [kate] seems very determined."
  "And unfortunately for [isabelle], [kate] usually gets what she wants..."
  window hide
  show kate strap_on_penetration3_isabelle_pain with vpunch
  window auto
  isabelle "Mmmmph! Mmmmph!"
  kate strap_on_penetration3_isabelle_pain "Hold her down!"
  show kate strap_on_penetration3_isabelle_pain_casey_leash_pull
  casey "Be still and take it, bitch." with vpunch
  stacy "Do as you're told!"
  lacey "Give in!"
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "[isabelle], I'm going to give you two more inches now."
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "Are you going to be a good girl for me?"
  isabelle "Uhmph-umph..."
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "No? You want to be my bad girl, then?"
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "I'm okay with that."
  window hide
  show kate strap_on_penetration4_isabelle_pain with vpunch
  window auto
  isabelle "Mmmph! Mmmph! Mmmph!"
  kate strap_on_penetration4_isabelle_pain "Almost there..."
  kate strap_on_penetration4_isabelle_pain "Considering the size of the stick you usually keep up your ass, I'm surprised you're even protesting."
  stacy "Come on, [kate], put her in her place!"
  casey "Make her take it all!"
  kate strap_on_penetration4_isabelle_pain "Oh, I'm planning on it. I just want to enjoy the moment."
  kate strap_on_penetration4_isabelle_pain "I find it so funny that she walked up to me that day, trying to\nstart shit."
  kate strap_on_penetration4_isabelle_pain "And now she's here, with my strap-on up her ass."
  kate strap_on_penetration4_isabelle_pain "That's a powerful statement if I ever saw one!"
  kate strap_on_penetration4_isabelle_pain "Anyway, are you ready for the last inches, bitch?"
  show kate strap_on_penetration4_isabelle_head_down with Dissolve(.5)
  isabelle "Mmmmph..."
  kate strap_on_penetration4_isabelle_head_down "Hey, don't look so crestfallen. That's no fun."
  kate strap_on_penetration4_isabelle_head_down "I want you to submit as I fuck your ass."
  kate strap_on_penetration4_isabelle_head_down "Here it comes..."
  window hide
  show kate strap_on_penetration5_isabelle_crying with vpunch
  window auto
  isabelle "Mmmmmmmmmmmmmmmmph!"
  kate strap_on_penetration5_isabelle_crying "There it is. Perfection."
  "I can't believe it. [kate] somehow got the entire length of that monster inside [isabelle]'s ass."
  isabelle "Mmmph! Mmmph!"
  kate strap_on_entire_length "God, it must be all the way up in your colon."
  kate strap_on_entire_length "Tell me, how does it feel to be so thoroughly owned?"
  kate strap_on_entire_length "You're sweating and crying from the pain, but how does it feel on a psychological level?"
  isabelle "..."
  kate strap_on_entire_length "Yeah, I don't imagine you have much to say now."
  kate strap_on_entire_length "You saw me as a rival. As your equal, in a way."
  kate strap_on_entire_length "But we're not equals, are we?"
  show kate strap_on_penetration5_isabelle_head_down with Dissolve(.5)
  isabelle "..."
  casey "No way, you're not!"
  stacy "One is a bitch, the other is a queen."
  lacey "Yeah! You're the bitch, [isabelle]! [kate]'s the queen!"
  kate strap_on_penetration5_isabelle_head_down "I'm going to master you, [isabelle]. And you will submit to me."
  kate strap_on_penetration5_isabelle_head_down "Tell me, are you ready to submit?"
  isabelle "Mmmmph..."
  kate strap_on_penetration5_isabelle_head_down "I think you're ready, but I'm still going to make sure..."
  window hide
  show kate strap_on_penetration4_isabelle_head_down with Dissolve(.2)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  pause 0.2
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.2)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.2)
  pause 0.15
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  window auto
  isabelle "Mmmmmph!"
  "Each thrust from that cock sends a shock wave of pain and pleasure through [isabelle]'s core."
  "No one has probably ever been as fucked as she is now..."
  window hide
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  pause 0.1
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  pause 0.2
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  window auto
  isabelle "Mmmmmph!"
  kate strap_on_penetration1_isabelle_pain "Do you know what I like the most about this?"
  isabelle "..."
  kate strap_on_penetration1_isabelle_pain "I'm fucking you, but there's zero pleasure for you."
  kate strap_on_penetration1_isabelle_pain "And there's almost zero physical pleasure for me."
  kate strap_on_penetration1_isabelle_pain "It's all about me asserting my dominance over your ass, and you submitting to it."
  kate strap_on_penetration1_isabelle_pain "It's kind of twisted, but the real pleasure is in the power exchange."
  window hide
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  pause 0.15
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.1
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  window auto
  isabelle "Mmmmmph!"
  "Wow, [kate] is really giving it to her..."
  "She moves her hips back, adding more power to each thrust."
  "Whenever I watch porn, girls always struggle to thrust their hips right while fucking someone with a strap-on."
  "But [kate] is an expert at it. She's as skilled at fucking people as\nany guy."
  "Which I guess shouldn't come as a surprise..."
  "Fucking people, in every sense of the word, is one of her favorite hobbies."
  window hide
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.15
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.2
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.1
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.1
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  window auto
  isabelle "Mmmmmph!"
  kate strap_on_penetration1_isabelle_crying "Come on, [isabelle], don't hold back."
  kate strap_on_penetration1_isabelle_crying "I want to hear you scream."
  isabelle "..."
  casey "Scream for us, bitch!"
  lacey "Yeah, scream!"
  window hide
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with hpunch
  pause 0.2
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with hpunch
  pause 0.2
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with hpunch
  window auto
  isabelle "Mmmmmph!"
  kate strap_on_penetration5_isabelle_crying "Maybe it's time to remove the gag..."
  kate strap_on_penetration5_isabelle_crying "Will you be on your best behavior if I take it out?"
  isabelle "..."
  stacy "Bitch, answer her!"
  isabelle "Mmmphmmm."
  casey "Sounds like she'll be a good girl for us now."
  kate strap_on_penetration5_isabelle_crying "It does, indeed."
  kate strap_on_penetration5_isabelle_crying "Now, when I unmuzzle you, I want to hear gratitude."
  window hide
  show kate strap_on_entire_length with Dissolve(.5)
  pause 0.25
  show kate strap_on_entire_length_no_ballgag with Dissolve(.5)
  pause 0.25
  window auto
  kate strap_on_entire_length_no_ballgag "What do you say, [isabelle]?"
  isabelle "Thank... you... ma'am..."
  kate strap_on_entire_length_no_ballgag "For what?"
  isabelle "For... setting me straight..."
  kate strap_on_entire_length_no_ballgag "It was about time someone did, right?"
  isabelle "Y-yes... ma'am..."
  kate strap_on_entire_length_no_ballgag "I already like your attitude better."
  kate strap_on_entire_length_no_ballgag "You're going to be obedient now, aren't you?"
  isabelle "Yes, ma'am..."
  kate strap_on_entire_length_no_ballgag "Good girl."
  if quest.kate_stepping["fuck_isabelle_too"]:
    kate strap_on_entire_length_no_ballgag "Unfortunately for you, I've promised [mc] something."
    isabelle "..."
    kate strap_on_entire_length_no_ballgag "I know. It's a bit gross, but he did play a part in your capture."
    kate strap_on_entire_length_no_ballgag "And just like Judas, he'll get paid in full."
    kate strap_on_entire_length_no_ballgag "[mc], come on over here."
    "Finally, the moment I've been waiting for!"
  window hide
  show black onlayer screens zorder 100
  show kate spitroast_strap_on_isabelle_nervous
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  if quest.kate_stepping["fuck_isabelle_too"]:
    kate spitroast_strap_on_isabelle_nervous "You can fuck her in the ass as well."
    kate spitroast_strap_on_isabelle_nervous "I haven't decided yet if I'll let her take dicks in her pussy again."
    kate spitroast_strap_on_isabelle_nervous "Maybe she'll be an ass-only girl from now on."
    isabelle "..."
    "As long as I get to fuck her, I'm happy."
    "Buggers can't be choosers, as the gay men say in England."
    kate spitroast_strap_on_isabelle_nervous "Just one more thing before we start."
    kate spitroast_strap_on_isabelle_nervous "I usually don't make first-time pets do this..."
    kate spitroast_strap_on_isabelle_nervous "But I think you deserve to taste your own ass, [isabelle]."
    kate spitroast_strap_on_isabelle_nervous "Wouldn't you agree?"
    isabelle "Y-yes... ma'am..."
    kate spitroast_strap_on_isabelle_nervous "Good girl. Open your mouth."
    window hide
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.5)
    pause 0.1
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.2
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.15
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    window auto
    kate spitroast_blowjob1_isabelle_eyes_closed "Now, isn't that the taste of utter defeat?"
    "Fuck, that's twisted, but also stupid hot..."
    mc "Can I fuck her now?"
    kate spitroast_blowjob1_isabelle_eyes_closed "Yeah, go ahead. Claim your prize."
    window hide
    show kate spitroast_blowjob1_anticipation_squeeze1_isabelle_eyes_closed with Dissolve(.5)
    window auto
    "Heat radiates off [isabelle]'s ass from the harsh spanking."
    "Her sphincter, winking and red from [kate]'s strap-on, beckons me."
    "It is indeed a prize, one that most can only dream of."
    window hide
    show kate spitroast_blowjob1_penetration1_squeeze1_isabelle_looking_up with Dissolve(.5)
    window auto
    "At first, a shock tightens her back as the tip of my dick meets her abused asshole."
    show kate spitroast_blowjob1_penetration2_squeeze2_isabelle_looking_up with dissolve2
    "A gargle escapes her mouth as the strap-on domesticates her throat."
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with dissolve2
    "The girls hold her down, making sure she knows her place."
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with dissolve2
    "And my brain completely floods with lust chemicals as I break through her backdoor."
    window hide
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    window auto
    "Her ass tightens around my cock, trying to push it out..."
    "But that only makes me want to go deeper."
    "It's a losing battle for [isabelle], and slowly but surely her defiance melts away."
    "She expected to get fucked by [kate], but she didn't think I would also have my way with her."
    "Nevertheless, she has little choice but to accept me into the hottest, most forbidden place in her body."
    window hide
    show kate spitroast_blowjob1_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.5)
    window auto
    "My hips slam into her bruised ass, forcing the strap-on deeper down her throat."
    "[kate] isn't letting up, and neither am I."
    "She pushes [isabelle] back onto my cock, and I push her forward onto hers."
    "After a while, our paces start to match."
    "We push into [isabelle] at the same time."
    window hide
    show kate spitroast_blowjob1_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.5)
    window auto
    "Her teary eyes bulge, she squirms, but the cheerleaders keep her\nin place."
    lacey "Stay, bitch!"
    casey "Yeah! Stay!"
    window hide
    show kate spitroast_penetration1_squeeze1_strap_on_stacy_backfoot_isabelle_crying with hpunch
    window auto
    "[kate] just smiles smugly, and smacks [isabelle] in the face with\nthe strap-on."
    kate spitroast_penetration1_squeeze1_strap_on_stacy_backfoot_isabelle_crying "Are you enjoying yourself?"
    "It's hard to say who the question is pointed at. Maybe it's me, maybe it's [isabelle]."
    window hide
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.5)
    show kate spitroast_blowjob1_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    window auto
    "Her juices drip in strings down onto the gym floor, creating a small puddle."
    "Her pussy is flexing, and so is her ass. She's close to orgasming,\nI can feel it..."
    "And me..."
    "All at once the pleasure rips through me, this is going to be my best—{space=-40}"
    window hide None
    play sound "falling_thud"
    show black onlayer screens zorder 100
    show kate spitroast_strap_on_isabelle_shock
    with vpunch
    pause 0.5
    show black onlayer screens zorder 4
    $set_dialog_mode("default_no_bg")
    mc "Ouch..."
    "Just as orgasm hits me, [lacey] pushes me back."
    "My dick erupts, but it's less than a whimper of the titanic eruption I was about to release..."
    "Less satisfying."
    show black onlayer screens zorder 100
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    $set_dialog_mode("")
  else:
    kate spitroast_strap_on_isabelle_nervous "Now, I usually don't make first-time pets do this..."
    kate spitroast_strap_on_isabelle_nervous "But I think you deserve to taste your own ass, [isabelle]."
    kate spitroast_strap_on_isabelle_nervous "Wouldn't you agree?"
    isabelle "Y-yes... ma'am..."
    kate spitroast_strap_on_isabelle_nervous "Good girl. Open your mouth."
    window hide
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.5)
    pause 0.1
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.2
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.15
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    window auto
    kate spitroast_blowjob1_isabelle_eyes_closed "Isn't that the taste of utter defeat?"
    "Fuck, that's twisted, but also stupid hot..."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_eyes_closed with Dissolve(.5)
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with hpunch
    pause 0.25
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    window auto
    isabelle "Mmmmmm!"
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "You like that vibrator?"
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "I thought it would be fitting for you to orgasm while tasting your ass.{space=-20}"
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "So, go on, enjoy it."
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "It might be your last orgasm in the foreseeable future."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    window auto
    "Despite her total degradation, [isabelle]'s body is responding to the touch of the vibrator."
    "Eyes downcast in shame, she cleans her ass off of [kate]'s strap-on."
    "Her cheeks burn, while waves of pleasure start to roll through her."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.5)
    window auto
    "It must be so humiliating to get dominated like this, and then be forced to enjoy it..."
    "She bobs her head up and down, grimacing from the taste, but also from the building lust."
    "Her abdomen spasms hard, her pussy leaks down her thighs."
    "It's only a matter of time before she tips over the edge."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.5)
    window auto
    "Her hips move on instinct, trying to hump the vibrator..."
    "Her breath quickens..."
    "It builds and it builds..."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    window auto
    "Then suddenly, her eyes open wide, her muscles go stiff..."
    "A final gasp fills her lungs..."
    window hide
    show kate spitroast_blowjob1_isabelle_orgasm with Dissolve(.5)
    window auto
    "Before [lacey] calmly turns off the vibrator."
    show kate spitroast_strap_on_isabelle_shock with Dissolve(.5)
  isabelle "Noooooooooo!"
  if quest.kate_stepping["fuck_isabelle_too"]:
    "[isabelle] isn't as lucky."
    "The stimulus of my dick is lost at the very brink of her orgasm."
    "She struggles against her bonds in utter frustration. The cheerleaders{space=-45}\njust laugh."
  else:
    "[isabelle] struggles against her bonds in utter frustration. The cheerleaders just laugh."
  "The cruelty has reached a new high..."
  kate spitroast_strap_on_isabelle_crying "Silly girl! Did you really think we'd let you orgasm?"
  "Before she can fully gather herself from the denied orgasm, [stacy] pulls out a new item."
  window hide
  show kate spitroast_strap_on_stacy_buttplug_isabelle_pleading with Dissolve(.5)
  window auto
  stacy "The final piece of your costume."
  "[isabelle] just bows her head in submission. There's no fight left\nin her."
  "It's almost like she presents her ass for [stacy]."
  window hide
  show kate spitroast_strap_on_isabelle_pleading_buttplug with Dissolve(.5)
  window auto
  "As the plug slides home, and her face turns even redder, the girls all cheer."
  kate spitroast_strap_on_isabelle_pleading_buttplug "Moving forward, will you bark for me like a dog, or purr for me like a kitten?"
  kate spitroast_strap_on_isabelle_pleading_buttplug "Those are your options, and I'm excited to see what kind of pet you'll be for me."
  show kate spitroast_strap_on_isabelle_head_down_buttplug with Dissolve(.5)
  isabelle "..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "I know it's a hard choice. You're both catty and a total bitch."
  isabelle "..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "Make your choice."
  isabelle "A... dog..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "What was that?"
  isabelle "A dog... ma'am..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "Oh, a good choice!"
  kate spitroast_strap_on_isabelle_head_down_buttplug "Let's hear it, then. Bark for me."
  isabelle "..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "Bark for me, or we'll bring out the paddle again."
  show kate spitroast_strap_on_isabelle_shock_buttplug with Dissolve(.5)
  isabelle "Woof!"
  kate spitroast_strap_on_isabelle_shock_buttplug "Again."
  show kate spitroast_strap_on_isabelle_crying_buttplug with Dissolve(.5)
  isabelle "Woof! Woof!"
  kate spitroast_strap_on_isabelle_crying_buttplug "Heel, whore."
  scene location with fadehold
  return
