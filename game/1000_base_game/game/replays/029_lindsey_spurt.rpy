init python:
  register_replay("lindsey_spurt","Lindsey's Spurt","replays lindsey_spurt","replay_lindsey_spurt")

label replay_lindsey_spurt:
  show lindsey soaked_gushed with fadehold: ## This gives the image both the fadehold (transition) and hpunch (transform) effects
    block:
      linear 0.05 xoffset 15
      linear 0.05 xoffset -15
      repeat 18
    linear 0.05 xoffset 0
  hide lindsey
  show lindsey soaked_gushed ## This is just to avoid the screen shake if the player is skipping
  lindsey soaked_gushed "Aaaaaah!"
  lindsey soaked_gushed "{i}*Gurgle*{/}"
  window hide
  pause 0.125
  show lindsey soaked_annoyed with Dissolve(.5)
  pause 0.25
  window auto
  lindsey soaked_annoyed "Oh my god!"
  "Took the words right out of my mouth..."
  "The dripping hair and blush of indignation is nothing short of adorable."
  "She wipes her face on her sleeve, eyelashes sparkling with tiny wet diamonds."
  "I always thought those wet t-shirt contests were for sluts without self-esteem or dignity..."
  "...but [lindsey] is the opposite of that, and maybe that's what makes this so special."
  menu(side="left"):
    extend ""
    "\"Are you okay?\"":
      mc "Are you okay?"
      lindsey soaked_sad "Yeah, I'm just absolutely drenched."
      lindsey soaked_sad "Oh, god. My skirt! It looks like I peed myself!"
      lindsey soaked_sad "What's wrong with the fountain?"
      mc "Err, I don't know."
      mc "I did try to warn you, but you moved too fast, as usual."
      lindsey soaked_blush "Aw, thanks for being nice about it!"
      lindsey soaked_blush "I should've realized something was wrong when I saw the hoodie."
    "\"You look super cute right now.\"":
      mc "You look super cute right now."
      lindsey soaked_skeptical "I don't feel very cute."
      mc "Dry, wet — you're always adorable and you know it!"
      lindsey soaked_flirty "Do you really think so?"
      mc "Of course! You're the fastest {i}and{/} the cutest!"
      lindsey soaked_blush "Aw, you always know how to turn a bad situation around!"
    "\"Sorry for making you wet.\"":
      mc "Sorry for making you wet."
      lindsey soaked_flirty "Let me guess... it was bound to happen at some point?"
      mc "Oh my lord! That's absolutely raunchy! What a thing to say!"
      lindsey soaked_blush "Stop pretending!"
      lindsey soaked_blush "I know that's exactly where you were going with that!"
      mc "I don't know what you're talking about!"
      lindsey soaked_flirty "Okay, Reverend [mc]. I totally believe you!"
  mc "Do you have anything you can change into?"
  lindsey soaked_annoyed "I do. In the women's locker room."
  "Crap. Someone's coming up the stairs."
  lindsey soaked_sad "People are coming! Oh, god! I'll die if they see me like this!"
  mc "Quick! Hide in the art classroom!"
  scene location with fadehold
  return
