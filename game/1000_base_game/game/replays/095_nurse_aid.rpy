init python:
  register_replay("nurse_aid","Nurse's Aid","replays nurse_aid","replay_nurse_aid")

label replay_nurse_aid:
  play sound ["<silence 1.5>", "falling_thud"]
  show nurse hiking_help mc_down with fadehold: ## This gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 18
    linear 0.05 yoffset 0
  hide nurse
  show nurse hiking_help mc_down ## This is just to avoid the screen shake if the player is skipping
  mc "Ow! Fuck!"
  nurse hiking_help mc_down "Oh, dear! Are you okay?"
  mc "I-I think I twisted my ankle..."
  nurse hiking_help mc_down "Goodness, I'm so sorry."
  nurse hiking_help mc_down "Let me take a look, will you?"
  window hide
  pause 0.125
  show nurse hiking_help ankle_hold with Dissolve(.5)
  pause 0.25
  window auto
  mc "O-ouch..."
  "Somehow, it hurts a little less when she's looking over me."
  "Cradling my injured foot in her soft, caring hands."
  nurse hiking_help ankle_hold "There's just a bit of swelling, okay?"
  nurse hiking_help ankle_hold "Let me wrap it for you, and you should be able to get back up."
  mc "Okay... thank you..."
  window hide
  pause 0.125
  show nurse hiking_help ankle_wrap with Dissolve(.5)
  pause 0.25
  window auto
  # "It's like a fire ignites in her eyes when she gets to work on my ankle."
  "It's like a fire ignites in her eyes when she gets to work on my ankle.{space=-5}"
  "A determination and purpose, which I've only seen in [lindsey]'s eyes while blazing across the gym."
  "In a way, I envy such passion."
  "How do you find it?"
  "She wraps my ankle until it's tight and stiff, and most of the pain is already fading."
  mc "Thank you, [nurse]. That's much better."
  show nurse hiking_help ankle_wrap smile with dissolve2
  nurse hiking_help ankle_wrap smile "I'm glad. Do you think you can walk?"
  mc "Well, it's not like I have much choice, do I?"
  nurse hiking_help ankle_wrap smile "We can stop as often as you need, okay?"
  mc "Thanks..."
  scene location with fadehold
  return
