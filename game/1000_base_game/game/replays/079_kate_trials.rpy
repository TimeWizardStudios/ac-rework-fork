init python:
  register_replay("kate_trials","Kate's Trials","replays kate_trials","replay_kate_trials")

label replay_kate_trials:
  show kate looking_down with fadehold
  kate looking_down "Listen carefully, because I'll only say this once."
  kate looking_down "You do not speak without permission."
  kate looking_down "You do not touch me without permission."
  kate looking_down "You don't even look at me without permission."
  kate looking_down "Am I making myself clear?"
  mc "Y-yes..."
  kate looking_down "Yes, what?"
  menu(side="middle"):
    extend ""
    "\"Yes, ma'am.\"":
      mc "Yes, ma'am."
      kate looking_down "Good boy."
      kate looking_down "However, I think today we'll go for something different."
      kate looking_down "This is a step up, don't you think?"
    "\"Yes, Miss [kate].\"":
      mc "Yes, Miss [kate]."
      kate looking_down "Good bitch."
      kate looking_down "That's how you will address me today."
    "\"Yes, mistress.\"":
      mc "Yes, mistress."
      kate looking_down "Wrong. I am not your mistress."
      kate looking_down "This is simply a tryout. I've not yet decided if you're worthy."
      mc "I'm sorry..."
    "\"Yes, goddess.\"":
      mc "Yes, goddess."
      kate looking_down "Ha! Compared to you, that's exactly what I am."
      kate looking_down "However, it's a bit pompous for my taste."
  kate looking_down "Call me Miss [kate]."
  mc "Yes, Miss [kate]."
  kate looking_down "That does have a certain ring to it..."
  kate looking_down "...but basic obedience is just the start."
  kate looking_down "We'll see if you can hack the rest, though I doubt it."
  "As she speaks, my eyes remain downcast."
  "Careful not to look at her, as instructed."
# kate looking_down "Being a good bitch is all about showing the proper respect and reverence."
  kate looking_down "Being a good bitch is all about showing the proper respect and reverence.{space=-120}"
  kate looking_down "Kiss my shoes."
  "Her cool, calm voice snaps like a whip and stutters my heart."
  window hide
  show kate feet_worship right_shoe_kiss
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
# "As my lips meet the rough texture of her toe, a shiver runs through me."
  "As my lips meet the rough texture of her toe, a shiver runs through me.{space=-70}"
  "A hint of sweat and leather. A somewhat salty taste."
  kate feet_worship right_shoe_kiss "And the other one."
  window hide
  pause 0.125
  show kate feet_worship left_shoe_kiss with Dissolve(.5)
  pause 0.25
  window auto
  "As if on submissive autopilot, my lips brush over her other shoe."
  kate feet_worship left_shoe_kiss "Dirty little freak! You have no idea where those have even been."
  kate feet_worship left_shoe_kiss "I bet you would like to clean them for me, though, wouldn't you?"
  mc "Yes, Miss [kate]."
  kate feet_worship left_shoe_kiss "Take them off, then."
  window hide
  pause 0.125
  show kate feet_worship left_shoe_removal with Dissolve(.5)
  pause 0.25
  window auto
  "It feels like a privilege to even touch her."
  "For so long I've been building her up in my mind, putting her on a throne-like pedestal."
  show kate feet_worship left_shoe_removed with dissolve2
  "She wiggles her toes in my grasp."
  "Touching her... smelling her... being this close to any part of her makes me flush with excitement."
  kate feet_worship left_shoe_removed "Now the other one."
  window hide
  pause 0.125
  show kate feet_worship right_shoe_removal with Dissolve(.5)
  pause 0.25
  window auto
  "She doesn't have to tell me twice."
  "Being bossed around like this has been on my mind for decades."
  "Always a guilty pleasure thought late at night."
  show kate feet_worship right_shoe_removed with dissolve2
  "In one swift movement, she now stands before me barefoot."
  kate feet_worship right_shoe_removed "Kiss my feet, bitch."
  "Telling people what to do comes so naturally to her."
  "It's like she was born to rule."
  window hide
  pause 0.125
  show kate feet_worship right_foot_kiss with Dissolve(.5)
  pause 0.25
  window auto
  "Reverently, my lips meet her feet in a humiliating kiss."
  "First one, then the other."
  window hide
  pause 0.125
  show kate feet_worship left_foot_kiss with Dissolve(.5)
  pause 0.25
  window auto
# "My mouth lingers against her skin, tasting the sweat of her, breathing in the faint smell of soap and salt."
  "My mouth lingers against her skin, tasting the sweat of her, breathing{space=-30}\nin the faint smell of soap and salt."
  kate feet_worship left_foot_kiss "God, my feet are aching after such a long day..."
  kate feet_worship left_foot_kiss "Massage them like a good boy, will you?"
  window hide
  pause 0.125
  show kate feet_worship right_foot_massage with Dissolve(.5)
  pause 0.25
  window auto
  "And that's all I want to be."
  "I want her approval more than anything."
  "Even if it'll cost me my dignity."
  kate feet_worship right_foot_massage "Now with the tongue."
  mc "Yes, Miss [kate]."
  window hide
  pause 0.125
  show kate feet_worship right_foot_lick with Dissolve(.5)
  pause 0.25
  window auto
  "It's a demeaning order, but I'm completely under her spell."
  "I'm a slave to her whims, to her desires, and to my own."
  "Slowly, up to her toes."
  "Her foot trembles in my hand from the sensation and fills me with a sense of delight and pride."
  "Despite the degrading act, I just want to please her."
  "To make her feel good."
  window hide
  pause 0.125
  show kate feet_worship right_toe_suck with Dissolve(.5)
  pause 0.25
  window auto
  "Suddenly, she slips her big toe into my open mouth."
  "Making me get a full taste."
  kate feet_worship right_toe_suck "Suck it."
  "And I do. God, I do."
  "[kate] laughs, her voice full of derision."
  kate feet_worship right_toe_suck "Oh, my god! You really are disgusting!"
  kate feet_worship right_toe_suck "Make sure you get them all, you pathetic foot bitch."
  "My face burns, the shame makes me sweat."
  window hide
  pause 0.125
  show kate feet_worship right_toe_choke with Dissolve(.5)
  pause 0.25
  window auto
  "She applies more force, pushing her foot inside my mouth."
  "She wants more. Deeper."
  "She keeps on pushing until the toes hit the back of my throat."
  "Tears well up as I do my best not to choke."
  "And yet... the treatment makes my dick twitch and rise higher."
  kate feet_worship right_toe_choke "You're totally getting off on this, aren't you?"
# kate feet_worship right_toe_choke "You're probably going to cream all over the floor because I let you choke on my foot."
  kate feet_worship right_toe_choke "You're probably going to cream all over the floor because I let you choke{space=-85}\non my foot."
  window hide
  pause 0.125
  show kate feet_worship aftermath_confident with Dissolve(.5)
  pause 0.25
  window auto
  "Abruptly, she slides her foot out of my mouth."
  kate feet_worship aftermath_confident "Isn't that right? Aren't you loving this?"
  "Like a rising wave, the emotions threaten to swallow me whole."
  "The shame... the degradation... the arousal..."
  show kate feet_worship aftermath_skeptical with dissolve2
  kate feet_worship aftermath_skeptical "Answer me."
  mc "Y-yes, Miss [kate]..."
  kate feet_worship aftermath_skeptical "Be more specific, bitch."
  mc "Yes, Miss [kate], I love choking on your foot."
  show kate feet_worship aftermath_laughing with dissolve2
  kate feet_worship aftermath_laughing "Interesting, but not surprising."
  kate feet_worship aftermath_laughing "I always knew you were a little worm desperate to crawl at my feet."
  mc "Yes, Miss [kate]. Thank you..."
  show kate feet_worship aftermath_skeptical with dissolve2
  kate feet_worship aftermath_skeptical "That's enough talk from you."
  kate feet_worship aftermath_skeptical "It's time for the next trial. Get down on all fours and turn around."
  window hide
  show kate spanking_trial kateless
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
# "The dryness of my throat... the sweat pouring down my back and legs..."
  "The dryness of my throat... the sweat pouring down my back and legs...{space=-70}"
  "Never before have I felt so stepped on. So completely owned."
  "My body moves on its own. Like an obedient zombie driven by pure lust and need."
  "..."
  "Is this it already?"
  "Am I about to be fucked in the ass by [kate]?"
  "She moves behind me, her wet feet padding against the carpet..."
  window hide
  pause 0.125
  show kate spanking_trial anticipation with Dissolve(.5)
  pause 0.25
  window auto
  "...but moments later, she returns."
  "She comes up behind me, a smile in her voice when she talks."
  kate spanking_trial anticipation "Let's see how well you handle this."
  "Something swishes through the air, and suddenly all the hair on my neck stands up."
  kate spanking_trial anticipation "Do you know this sound?"
  "Another loud swish right next to my ear makes me freeze."
  kate spanking_trial anticipation "Oh, I see you do."
  kate spanking_trial anticipation "But I wonder if you know the feeling..."
  "She takes a step back, and for a moment, her words hang in the air as the anticipation builds."
  window hide
  pause 0.125
  show kate spanking_trial workaround spanking1 with Dissolve(.5)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2 with Dissolve(.125)
  show kate spanking_trial spanking3 with Dissolve(.075)
  show kate spanking_trial spanking4 with Dissolve(.25)
  pause 0.25
  window auto
  mc "Owww!"
# "And when the cane comes whipping down and cracks across my bare ass, pain engulfs every nerve in my body."
  "And when the cane comes whipping down and cracks across my bare{space=-30}\nass, pain engulfs every nerve in my body."
  "Reflexively, my ass clenches and I flinch away from the pain that sears through me."
  kate spanking_trial spanking4 "Oh, that little tap? That's just one of many."
  kate spanking_trial spanking4 "Are you ready for some more?"
  mc "Yes, Miss [kate]!"
  kate spanking_trial spanking4 "What was that?"
  kate spanking_trial spanking4 "I don't have time for an ill-mannered bitch, you know."
  mc "Y-yes, please, Miss [kate]!"
  window hide
  pause 0.125
  show kate spanking_trial spanking1 with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2 with Dissolve(.125)
  show kate spanking_trial spanking3 with Dissolve(.075)
  show kate spanking_trial spanking4 with Dissolve(.25)
  pause 0.25
  window auto
  mc "Hnnnng!"
  "In response, she brings the cane down again."
  "Snapping it with practiced speed and grace on my asscheek."
  "She's an artist of pain with a simple twist of her wrist."
  "It's terrifying and intoxicating all at once."
  "And it makes my dick quiver, hardening even more."
  window hide
  pause 0.125
  show kate spanking_trial spanking1 with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2 with Dissolve(.125)
  show kate spanking_trial spanking3 with Dissolve(.075)
  show kate spanking_trial spanking4 with Dissolve(.25)
  pause 0.25
  window auto show
  show kate spanking_trial spanking4_red with dissolve2
  mc "Puuuh!"
  "The cane whistles down again, this time on my other cheek."
  kate spanking_trial spanking4_red "What's that? Are you going to beg for it?"
  mc "Yes, please, Miss [kate]!"
  kate spanking_trial spanking4_red "Then beg, bitch."
  mc "P-please... I would really like some more, Miss [kate]..."
  kate spanking_trial spanking4_red "Do you want me to hit you?"
  mc "Please!"
  kate spanking_trial spanking4_red "Do you like being my pain slut?"
  mc "Yes, Miss [kate]!"
  kate spanking_trial spanking4_red "Keep groveling, then."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_red with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_red with Dissolve(.125)
  show kate spanking_trial spanking3_red with Dissolve(.075)
  show kate spanking_trial spanking4_red with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_red with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_red with Dissolve(.125)
  show kate spanking_trial spanking3_red with Dissolve(.075)
  show kate spanking_trial spanking4_red with Dissolve(.25)
  pause 0.25
  window auto
  mc "Ayyyeeee!"
  "Her strokes get harder. Lashing across my skin."
  "The harsh, stinging pain makes me feel like I am being flayed."
  "Ropes of fire burning across my flesh."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_red with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_red with Dissolve(.125)
  show kate spanking_trial spanking3_red with Dissolve(.075)
  show kate spanking_trial spanking4_red with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_red with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_red with Dissolve(.125)
  show kate spanking_trial spanking3_red with Dissolve(.075)
  show kate spanking_trial spanking4_red with Dissolve(.25)
  pause 0.25
  window auto show
  show kate spanking_trial spanking4_redder with dissolve2
# "Every fiber in my body screams at me to get away, and yet somehow I manage to remain still."
  "Every fiber in my body screams at me to get away, and yet somehow{space=-15}\nI manage to remain still."
  "Shaking... but still."
# "[kate]'s breathing increases. Perhaps in excitement, or perhaps because she's putting more force into each stroke."
  "[kate]'s breathing increases. Perhaps in excitement, or perhaps because{space=-55}\nshe's putting more force into each stroke."
  kate spanking_trial spanking4_redder "Look at you moaning and screaming like a pussy..."
  kate spanking_trial spanking4_redder "...and your pathetic little dick getting harder."
  kate spanking_trial spanking4_redder "Is that all you've got? Had enough yet?"
  mc "No, Miss [kate]!"
  "She pauses for a moment."
  kate spanking_trial spanking4_redder "Do you want some more?"
  kate spanking_trial spanking4_redder "Do you really think you have what it takes to be my bitch?"
  mc "Yes, Miss [kate]! I do!"
  kate spanking_trial spanking4_redder "Very well. You better buckle up, then."
  kate spanking_trial spanking4_redder "Here comes the actual pain."
  "Oh, god..."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.25
  window auto
  "Her voice rings through the room with unrestrained delight."
  "And then she makes good on her promise."
  "She rains down blow after blow."
# "The cane singing through the air and reaching its crescendo on my skin."
  "The cane singing through the air and reaching its crescendo on\nmy skin."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.25
  window auto
  "The pain ravages my nerves and screams in my head, and yet somehow it feels like I'm approaching my biggest orgasm ever."
  "My body shakes uncontrollably."
  "Desperate to crawl away from her."
  "To escape the torment that is burning through the very core of my being."
# "Yet each strike sends shockwaves through my stomach, humming into my rock hard dick."
  "Yet each strike sends shockwaves through my stomach, humming into{space=-55}\nmy rock hard dick."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.0
  show kate spanking_trial spanking1_redder with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_redder with Dissolve(.125)
  show kate spanking_trial spanking3_redder with Dissolve(.075)
  show kate spanking_trial spanking4_redder with Dissolve(.25)
  pause 0.25
  window auto show
  show kate spanking_trial spanking4_reddest with dissolve2
  mc "Ahhhh!"
  "It escapes me in screams and ragged pants."
  "An inhale of air, an exhaled scream."
  "Again and again and again."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_reddest with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_reddest with Dissolve(.125)
  show kate spanking_trial spanking3_reddest with Dissolve(.075)
  show kate spanking_trial spanking4_reddest with Dissolve(.25)
  pause 0.25
  window auto
  kate spanking_trial spanking4_reddest "That's."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_reddest with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_reddest with Dissolve(.125)
  show kate spanking_trial spanking3_reddest with Dissolve(.075)
  show kate spanking_trial spanking4_reddest with Dissolve(.25)
  pause 0.25
  window auto
  extend " A."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_reddest with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_reddest with Dissolve(.125)
  show kate spanking_trial spanking3_reddest with Dissolve(.075)
  show kate spanking_trial spanking4_reddest with Dissolve(.25)
  pause 0.25
  window auto
  extend " Good."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_reddest with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_reddest with Dissolve(.125)
  show kate spanking_trial spanking3_reddest with Dissolve(.075)
  show kate spanking_trial spanking4_reddest with Dissolve(.25)
  pause 0.25
  window auto
  extend " Bitch."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_reddest with Dissolve(.125)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2_reddest with Dissolve(.125)
  show kate spanking_trial spanking3_reddest with Dissolve(.075)
  show kate spanking_trial spanking4_reddest with Dissolve(.25)
  pause 0.25
  window auto
  "She punctuates each word with a smack."
  "Tears sting my eyes and run down my cheeks."
  "I am incapable of actual speech."
  "Nothing but pain and sweat and tears."
  "My cock aching for release as the pressure builds in my balls."
  window hide
  pause 0.125
  show kate spanking_trial spanking1_reddest with Dissolve(.125)
  show kate spanking_trial spanking2_reddest with Dissolve(.125)
  play sound "slap"
  play ambient "audio/sound/line_snapping.ogg" noloop
  $renpy.transition(Move((0,20), (0,-20), 0.10, bounce=True, repeat=True, delay=0.275))
  hide screen interface_hider
  show kate spanking_trial aftermath
  show black
  show black onlayer screens zorder 100
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  mc "Aaaaaaaah!"
  "Until [kate] brings the cane down one last time and a new sound bounces off my eardrums..."
  "...a loud snap as the cane breaks against my reddened buttcheeks."
  "And then silence."
  "Only the sound of our pants clogging the sweat drenched room."
  "My whole body trembles in pain and fatigue."
  "Ready to collapse on the floor."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  kate spanking_trial aftermath "Would you look at that? Not only did you take it, but you broke my cane too."
  kate spanking_trial aftermath "Which, of course, you'll have to replace."
  kate spanking_trial aftermath "But I have to admit, I didn't think you would last."
  mc "T-thank you... Miss... [kate]..."
  kate spanking_trial aftermath "There's a good bitch."
  scene location with fadehold
  return
