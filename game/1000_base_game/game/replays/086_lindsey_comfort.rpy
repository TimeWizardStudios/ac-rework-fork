init python:
  register_replay("lindsey_comfort","Lindsey's Comfort","replays lindsey_comfort","replay_lindsey_comfort")

label replay_lindsey_comfort:
  show lindsey hospital_room asleep with fadehold
  "Oh? She's asleep. Maybe I shouldn't disturb her."
  "She looks so peaceful. My very own Briar Rose."
  "I wish I could do something for her..."
  lindsey hospital_room drowsy "[mc]?"
  mc "I'm sorry! I didn't mean to wake you!"
  lindsey hospital_room drowsy "That's okay..."
  "She doesn't even look up. Her blue eyes, distant."
  "I know that look."
  "I saw it in the mirror in my old life."
  "It's the look of someone that is losing hope."
  mc "So, how have you been?"
  lindsey hospital_room sad2 "It's whatever."
  mc "Hey..."
  "It's hard to see [lindsey] so down and depressed."
  "The cheerful girl who was always so bubbly and happy."
  "She's still in there somewhere, right? She must be."
  # "But what can I really do to scatter the dark clouds for someone that has lost so much?"
  "But what can I really do to scatter the dark clouds for someone that{space=-5}\nhas lost so much?"
  "..."
  "I wish I could turn back time..."
  "I wish that I could have stopped this from ever happening."
  "And I wish I could heal her. To make her walk and run again."
  "But I can't."
  # "All I can do is ask her these hollow questions that I already know the answer to."
  "All I can do is ask her these hollow questions that I already know the{space=-15}\nanswer to."
  "\"How are you doing? How have you been?\""
  "\"Bad.\" The answer is \"bad.\""
  "She doesn't have to say it, but I know. Of course I know."
  mc "I care about you, okay?"
  lindsey hospital_room sad2 "..."
  "It's hard to say if she's even listening to me, and I don't blame her."
  # "Nothing I, or anyone else, can say will restore her life to what it used to be."
  "Nothing I, or anyone else, can say will restore her life to what it used{space=-10}\nto be."
  "All I can do is be there for her, if she lets me."
  window hide
  show lindsey hospital_bed_angel embraced
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "All I can do is put my arm around her, and try to comfort her with actions."
  "That's what I'll do if words mean nothing."
  "I'll keep her safe and warm."
  "She snuggles up against my arm, trying to get comfortable in spite of all the pain."
  "Having her so near, like this, makes my heart ache with tenderness."
  "If she lets me, I'll hold on forever."
  "I'll heal her through sheer willpower if I have to."
  window hide
  show expression LiveComposite((1920,1080),(0,-50),"home bedroom bedroom",(0,0),"home bedroom colored_walls",(197,150),"home bedroom painting",(943,332),"home bedroom closet_clean",(1187,238),"home bedroom closet2_clean",(255,742),"home bedroom carpet_clean",(736,406),"home bedroom door",(315,539),"home bedroom bed_clean",(1419,658),"home bedroom drawers",(135,359),"home bedroom bookshelve_left",(148,628),"home bedroom tv_clean",(271,801),"home bedroom controller_clean",(1330,32),"home bedroom bookshelves_right",(1060,621),"home bedroom desk_clean",(316,612),"home bedroom rooster_crowing",(1554,498),"home bedroom table_lamp",(1143,485),"home bedroom small_pc",(1159,600),"home bedroom chair",(77,909),"home bedroom sports_magazine",(307,435),"home bedroom badge",(99,725),"home bedroom potted_plant",(0,0),"home bedroom overlay") as makeshift_location
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  play sound "rooster_crowing"
  pause 2.25
  hide lindsey
  hide black onlayer screens
  with Dissolve(.5)
  window auto show
  show expression LiveComposite((1920,1080),(0,-50),"home bedroom bedroom",(0,0),"home bedroom colored_walls",(197,150),"home bedroom painting",(943,332),"home bedroom closet_clean",(1187,238),"home bedroom closet2_clean",(255,742),"home bedroom carpet_clean",(736,406),"home bedroom door",(315,539),"home bedroom bed_clean",(1419,658),"home bedroom drawers",(135,359),"home bedroom bookshelve_left",(148,628),"home bedroom tv_clean",(271,801),"home bedroom controller_clean",(1330,32),"home bedroom bookshelves_right",(1060,621),"home bedroom desk_clean",(315,615),"home bedroom rooster",(1554,498),"home bedroom table_lamp",(1143,485),"home bedroom small_pc",(1159,600),"home bedroom chair",(77,909),"home bedroom sports_magazine",(307,435),"home bedroom badge",(99,725),"home bedroom potted_plant",(0,0),"home bedroom overlay") as makeshift_location with {"master": Dissolve(.25)}
  "H-huh?"
  "Is that... a rooster?"
  "Where's my alarm clock? What time is it?"
  "..."
  "Shit, it's almost night!"
  "I'm going to be late for the Independence Day dinner!"
  "Oh, man, I better hurry. It's only the biggest night of the year..."
  window hide
  show expression LiveComposite((1920,1080),(-12,-119),"school ground_floor background",(881,426),"school ground_floor window_night",(232,273),"school ground_floor computer_room",(741,486),"school ground_floor main_stairs",(1656,273),"school ground_floor science_class",(558,175),"school ground_floor roof_stairs_left",(269,297),"school ground_floor independence_day_pennants",(874,148),"school ground_floor independence_day_banner",(450,484),"school ground_floor west_hall",(583,556),"school ground_floor lockersback",(552,561),"school ground_floor lockersfront",(324,551),"school ground_floor lockersleft",(406,546),"school ground_floor missing_locker",(402,585),"school ground_floor tape",(418,608),"school ground_floor permit",(1306,447),"school ground_floor guard_booth",(1140,513),"school ground_floor cafeteria",(1562,547),"school ground_floor janitor_door",(1459,547),"school ground_floor board",(483,551),"school ground_floor homeroom",(412,730),"school ground_floor sign_down",(693,691),"school ground_floor couch",(0,0),"school ground_floor night_overlay",(1171,545),"school ground_floor cafeteria_doors_light",(933,1000),"school ground_floor arrow_down") as makeshift_location
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Crap. It seems like they've gotten started already."
  "Hopefully, I haven't missed dinner. I'm starv—"
  window hide
  show expression LiveComposite((1920,1080),(-12,-119),"school ground_floor background",(881,426),"school ground_floor window_night",(232,273),"school ground_floor computer_room",(741,486),"school ground_floor main_stairs",(1656,273),"school ground_floor science_class",(558,175),"school ground_floor roof_stairs_left",(269,297),"school ground_floor independence_day_pennants",(874,148),"school ground_floor independence_day_banner",(450,484),"school ground_floor west_hall",(583,556),"school ground_floor lockersback",(552,561),"school ground_floor lockersfront",(324,551),"school ground_floor lockersleft",(406,546),"school ground_floor missing_locker",(402,585),"school ground_floor tape",(418,608),"school ground_floor permit",(1306,447),"school ground_floor guard_booth",(1140,513),"school ground_floor cafeteria",(1171,545),"school ground_floor cafeteria_doors",(1562,547),"school ground_floor janitor_door",(1459,547),"school ground_floor board",(483,551),"school ground_floor homeroom",(412,730),"school ground_floor sign_down",(693,691),"school ground_floor couch",(0,0),"school ground_floor night_overlay",(933,1000),"school ground_floor arrow_down") as makeshift_location with {"master": Dissolve(.25)}
  play audio "light_switch"
  play sound "screaming_long"
  pause 2.0
  window auto
  "What the hell?"
  "That doesn't sound like a scream of delight..."
  "What is going on in the cafeteria?"
  window hide
  play sound "screaming_short"
  show expression LiveComposite((642,1080),(193,155),"lindsey avatar body3",(245,265),"lindsey avatar face_afraid",(205,437),"lindsey avatar b3bra",(216,810),"lindsey avatar b3panty",(190,800),"lindsey avatar b3skirt",(139,347),"lindsey avatar b3arm2_n",(202,407),"lindsey avatar b3jacket",(132,346),"lindsey avatar b3arm2_c") as makeshift_lindsey:
    xanchor 0.075 xpos 1.0 yalign 1.0
    linear 1.0 xanchor 0.925 xpos 0.0
  pause 0.25
  show misc saucer_eyed_doll as saucer_eyed_doll:
    xanchor 0.0 xpos 1.0 yalign 1.0
    linear 2.0 xanchor 1.0 xpos 0.0
  pause 1.0
  window auto
  "And what the fuck?!"
  mc "[lindsey]? [lindsey]!"
  "Without as much as a glance, she passes right by me."
  "Her eyes oddly vacant."
  "She doesn't slow down, she doesn't look back. She just goes up the stairs to the roof."
  mc "[lindsey]!!"
  "Damn, that girl is fast..."
  window hide
  show expression LiveComposite((1920,1080),(0,0),"school roof_landing background",(0,528),"school roof_landing plant",(77,447),"school roof_landing wall_sign",(0,0),"school roof_landing painting",(227,0),"school roof_landing web",(411,30),"school roof_landing door",(809,316),"school roof_landing calendar",(1308,816),"school roof_landing locker",(1378,712),"school roof_landing cans",(1526,801),"school roof_landing toilet_paper",(1608,94),"school roof_landing fire_axe",(0,0),"school roof_landing overlay_night",(904,921),"school roof_landing exit_arrow") as makeshift_location
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 1.0
  hide makeshift_lindsey
  hide saucer_eyed_doll
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  show expression LiveComposite((642,1080),(193,155),"lindsey avatar body3",(245,265),"lindsey avatar face_afraid",(205,437),"lindsey avatar b3bra",(216,810),"lindsey avatar b3panty",(190,800),"lindsey avatar b3skirt",(139,347),"lindsey avatar b3arm2_n") as makeshift_lindsey:
    xanchor 0.075 xpos 1.0 yalign 1.0
    linear 1.0 xanchor 0.925 xpos 0.0
  show expression LiveComposite((642,1080),(202,407),"lindsey avatar b3jacket",(132,346),"lindsey avatar b3arm2_c") as lindsey_shirt:
    parallel:
      xanchor 0.075 xpos 1.0 yalign 1.0
      linear 1.0 xanchor 0.925 xpos 0.0
    parallel:
      alpha 1.0
      pause 0.25
      linear 0.5 alpha 0.0
  pause 1.0
  play sound "<from 9.2 to 10.2>door_breaking_in"
  show expression LiveComposite((1920,1080),(0,0),"school roof_landing background",(0,528),"school roof_landing plant",(77,447),"school roof_landing wall_sign",(0,0),"school roof_landing painting",(227,0),"school roof_landing web",(411,30),"school roof_landing door",(809,316),"school roof_landing calendar",(1308,816),"school roof_landing locker",(1378,712),"school roof_landing cans",(1526,801),"school roof_landing toilet_paper",(1608,94),"school roof_landing fire_axe",(0,0),"school roof_landing overlay_night",(904,921),"school roof_landing exit_arrow") as makeshift_location
  hide lindsey_shirt
  show expression LiveComposite((1920,1080),(1294,537),"school roof_landing lindsey_hoodie",(1294,537),AlphaMask(Crop((1294,537,1920,1080),"school roof_landing overlay_night"),"school roof_landing lindsey_hoodie")) as lindsey_shirt:
    alpha 0.0
    easein 0.25 alpha 1.0
  with hpunch
  pause 0.25
  "I'm close to catching her... but not quite fast enough."
  "The door slams in my face."
  mc "[lindsey]!!!"
  "No, no, no... not like this..."
  "In case of emergency..."
  window hide
  play sound "<from 0.3>glass_break"
  hide lindsey_shirt
  show expression LiveComposite((1920,1080),(0,0),"school roof_landing background",(0,528),"school roof_landing plant",(77,447),"school roof_landing wall_sign",(0,0),"school roof_landing painting",(227,0),"school roof_landing web",(411,30),"school roof_landing door",(1294,537),"school roof_landing lindsey_hoodie",(809,316),"school roof_landing calendar",(1308,816),"school roof_landing locker",(1378,712),"school roof_landing cans",(1526,801),"school roof_landing toilet_paper") as makeshift_location
  show expression LiveComposite((1920,1080),(1608, 94), "school roof_landing no_fire_axe") as no_fire_axe
  show expression LiveComposite((1920,1080),(1608,94),"school roof_landing fire_axe") as fire_axe:
    alpha 1.0
    easein 0.25 alpha 0.0
  show expression LiveComposite((1920,1080),(0,0),"school roof_landing overlay_night",(904,921),"school roof_landing exit_arrow") as overlay_night
  with hpunch
  pause 0.25
  window auto
  "The glass breaks against the impact of my elbow."
  "Next, the axe slams into the door in tandem with my thundering heartbeat."
  window hide
  play sound "fire_axe_impact"
  hide screen interface_hider
  show lindsey roof_door doorless wings
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  # "My muscles burn as the axe falls again and again and again and again."
  "My muscles burn as the axe falls again and again and again and again.{space=-45}"
  "It's just a door."
  "Nothing but a weak door, bending and shredding, and giving way to my strength and determination."
  "And finally, with a scream, it breaks wide open and I tumble into the summer night."
  show black onlayer screens zorder 100
  pause 0.5
  hide makeshift_location
  hide no_fire_axe
  hide fire_axe
  hide overlay_night
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "A single word passes through my mind."
  "NO!"
  mc "[lindsey], don't!!!"
  window hide
  pause 0.25
  show lindsey roof_door doorless step with Dissolve(.5)
  window auto
  "But she doesn't listen."
  "Just lifts a foot and hangs it out in open space, her arms pigeoning as she teeters."
  "Time itself seems to slow to a crawl."
  "The impossible distance, like a chasm opening up between us..."
  "A looming darkness that could ruin everything."
  "This can't be happening. Not again!"
  # "The sky warps above me, my mind and body turning into a hazy blur."
  "The sky warps above me, my mind and body turning into a hazy blur.{space=-15}"
  "It's so far..."
  "My entire being protests as I break at the seams, stretching myself like rubber..."
  "Reaching... reaching... it's so far..."
  "But I will not... cannot... let it happen..."
  "It's so far..."
  window hide
  hide screen interface_hider
  # show lindsey roof_floor scared
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "...and then my arms wrap around her midriff..."
  "...dragging her backward..."
  "...away from the ledge."
  show black onlayer screens zorder 100
  pause 0.5
  play sound "falling_thud"
  show lindsey roof_floor scared: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  hide lindsey
  show lindsey roof_floor scared ## This is just to avoid the screen shake if the player is skipping
  window auto
  $set_dialog_mode("")
  "We crash onto the hard floor of the roof."
  # "[lindsey] lands on top of me, knocking the air from my burning lungs."
  "[lindsey] lands on top of me, knocking the air from my burning lungs.{space=-10}"
  "My body lies broken and mangled, stretched beyond proportions."
  "She blinks several times, her eyes refocusing as she looks down into my own."
  lindsey roof_floor scared "Oh, my god..."
  mc "You're... okay now..."
  "My words are nothing more than a wheeze."
  mc "I've... got you..."
  lindsey roof_floor scared "I... I was about to..."
  mc "I know... I know..."
  mc "I've got you..."
  lindsey roof_floor crying "You..."
  "Tears spring to her eyes, her lip wobbling."
  lindsey roof_floor crying "You saved me..."
  mc "Thank god..."
  lindsey roof_floor admiring "You're my hero... my knight in shining armor..."
  lindsey roof_floor admiring "I don't know what I would do without you."
  lindsey roof_floor admiring "Every time I fall, you catch me."
  "Those words... they break my heart."
  "Because deep down, I know I failed her."
  "This isn't real."
  "And yet..."
  "As her heart beats strong against my chest..."
  "As her pupils dilate..."
  # "As the tears dry on her cheeks and she looks at me with admiration..."
  "As the tears dry on her cheeks and she looks at me with admiration...{space=-30}"
  "She's still here."
  "She's in my arms. Her soft skin, her beaming smile."
  "Still alive."
  "Nothing else matters... except that she's here."
  mc "[lindsey]..."
  window hide
  hide screen interface_hider
  show lindsey hospital_bed_voluntary awake
  show black
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  lindsey hospital_bed_voluntary awake "[mc]?"
  mc "I got you, [lindsey]..."
  lindsey hospital_bed_voluntary awake "Wake up, [mc]."
  mc "Mmm...?"
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  # "The lights from the hospital machine glow behind [lindsey] like a halo."
  "The lights from the hospital machine glow behind [lindsey] like a halo.{space=-25}"
  "And despite her condition, she looks like a real life angel."
  "How could this happen to someone like her?"
  "I knew it was a dream, but that's how it should've been."
  "If only I could trade my stretched out and broken body for hers..."
  "If only it would've turned out differently..."
  "But it didn't. And I failed her."
  mc "I'm sorry..."
  lindsey hospital_bed_voluntary awake "You were dreaming."
  mc "Err, yeah..."
  mc "I'm sorry if I woke you up."
  lindsey hospital_bed_voluntary awake "It's okay. It's just nice having you here with me."
  "And for a moment, the blank hopelessness leaves her eyes."
  "Big and blue, and filled with concern, she looks at me."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary mc_chest with Dissolve(.5)
  pause 0.25
  window auto
  lindsey hospital_bed_voluntary mc_chest "Your heart is beating so fast..."
  "It's the adrenaline from the dream... and perhaps her touch itself."
  "Yet the gentle pressure is calming."
  mc "I was trying to save you..."
  lindsey hospital_bed_voluntary mc_chest "You know it's not your fault, right?"
  mc "I could have been faster... I could have been there for you..."
  mc "Watching you... in that moment..."
  mc "It was like all the air had been sucked out of the universe."
  mc "And all of the light, too."
  mc "I thought... I thought you were..."
  lindsey hospital_bed_voluntary mc_chest "But I'm not. I'm right here, right now."
  lindsey hospital_bed_voluntary mc_chest "And you're here, too. That counts for more than you know."
  # "I'm the one that should be comforting her, but somehow it's the other way around..."
  "I'm the one that should be comforting her, but somehow it's the other{space=-35}\nway around..."
  "Even in her broken and depressed state, [lindsey] has kindness and compassion left for other people."
  mc "The dream just felt so real..."
  lindsey hospital_bed_voluntary mc_chest "Well, {i}this{/} is real."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary lindsey_chest with Dissolve(.5)
  pause 0.25
  window auto
  lindsey hospital_bed_voluntary lindsey_chest "Do you feel that?"
  mc "I do..."
  "The steady thread of her beating heart, thrumming against my palm like the blinking gaze of a lighthouse."
  "The soft skin of her collarbone beneath my fingertips."
  "Here. Alive. Real."
  "We could've lost everything."
  lindsey hospital_bed_voluntary lindsey_chest "I might be broken... but I'm still here."
  mc "You're not broken, [lindsey]. You're still beautiful in all the ways that matter."
  mc "Your heart is so strong, and so gentle at the same time."
  "But still, the dream lingers like smoke from a dying fire..."
  mc "Can I, um, listen to it? Just to make sure this is really real?"
  "She gives me a small smile, her hand still over mine."
  lindsey hospital_bed_voluntary lindsey_chest "Okay..."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary vitals_check with Dissolve(.5)
  pause 0.25
  window auto
  # "It thuds against my eardrums. Her strong runner's heart, cooling my fevered mind."
  "It thuds against my eardrums. Her strong runner's heart, cooling my{space=-10}\nfevered mind."
  "It makes me feel closer to her than ever."
  "Gentle, calm, consistent."
  mc "Err, everything sounds good."
  mc "Great, even. That is a very strong heartbeat you have there."
  lindsey hospital_bed_voluntary vitals_check "Oh? Is that your professional opinion, doctor?"
  mc "Heh. It absolutely is."
  "She exhales and her chest expands beneath my fingertips."
  "She smiles, and for a few moments, seems to forget."
  "And maybe that's the best I can do for her right now."
  "Keep her mind off of things."
  mc "I'm really sorry for waking you..."
  lindsey hospital_bed_voluntary vitals_check "It's okay..."
  mc "You should get some more rest."
  "[lindsey] doesn't respond, but I can sense that she's more at ease."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary forehead_kiss with Dissolve(.5)
  pause 0.25
  window auto
  "She smiles as my lips touch her forehead."
  "Perhaps, I was being stupid when I thought that words matter..."
  "Sometimes, they really don't."
  "Sometimes, a touch, a hug, and a kiss conveys everything that is important."
  lindsey hospital_bed_voluntary forehead_kiss "Thank you. For being here."
  mc "Likewise..."
  mc "Good night, [lindsey]."
  lindsey hospital_bed_voluntary forehead_kiss "Good night, [mc]."
  scene location with fadehold
  return
