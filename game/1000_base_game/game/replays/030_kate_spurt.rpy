init python:
  register_replay("kate_spurt","Kate's Spurt","replays kate_spurt","replay_kate_spurt")

label replay_kate_spurt:
  show kate soaked_wet with fadehold: ## This gives the image both the fadehold (transition) and hpunch (transform) effects
    block:
      linear 0.05 xoffset 15
      linear 0.05 xoffset -15
      repeat 18
    linear 0.05 xoffset 0
  hide kate
  show kate soaked_wet ## This is just to avoid the screen shake if the player is skipping
  kate soaked_wet "Aaaaaaaaah!"
  "Oh, wow! [kate] looks like a wet cat! A soaked pussy!"
  # "It's bold of her to not wear a bra, but I guess she's confident enough in her nipple game."
  "It's bold of her to not wear a bra, but I guess she's confident enough{space=-15}\nin her nipple game."
  "With standards as high as hers, she doesn't have to worry about autonomous body responses."
  "She's honestly pretty cute like that..."
  "Maybe that's the cure — drown the witch in her."
  "Finally, something bad happened to [kate]!"
  # "It's kinda hot seeing her all upset and shivering. She almost appears human."
  "It's kinda hot seeing her all upset and shivering. She almost appears{space=-5}\nhuman."
  "..."
  "I should take a picture of this. It might be useful at some point."
  window hide
  pause 0.125
  show white with Dissolve(.15)
  play sound "camera_snap"
  hide white with Dissolve(.15)
  pause 0.25
  window auto
  "Perfect!"
  scene location with fadehold
  return
