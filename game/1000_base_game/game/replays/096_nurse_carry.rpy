init python:
  register_replay("nurse_carry","Nurse's Carry","replays nurse_carry","replay_nurse_carry")

label replay_nurse_carry:
  play sound ["<silence 1.5>", "falling_thud"]
  show nurse hiking_hurt with fadehold: ## This gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 18
    linear 0.05 yoffset 0
  hide nurse
  show nurse hiking_hurt ## This is just to avoid the screen shake if the player is skipping
  nurse hiking_hurt "Ahhh! Owww!"
  mc "Oh, shit! Are you okay?"
  nurse hiking_hurt "My... my ankle... I twisted it..."
  mc "I see. Don't move it."
  mc "I'll just carry you back, all right?"
  nurse hiking_hurt "I, um... I don't know..."
  mc "It's okay. I got you."
  window hide
  show nurse hiking_carried
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "When I pick her up and hold her in my arms, I can feel her heart beating wildly against my own."
  "She looks up at me with her round, soft eyes and her breath tickles my chin when she speaks."
  nurse hiking_carried "You're, um, much stronger than you look."
  mc "I have put a little more effort into maintaining my physique."
  nurse hiking_carried "You make me feel like I actually weigh less than you..."
  mc "I think you feel great."
  nurse hiking_carried "I sometimes have a hard time laying off the sweets, and get a little discouraged by the scale..."
  mc "I think you're perfect the way you are. Really."
  nurse hiking_carried "Thank you... and thank you for carrying me..."
  mc "I got you, [nurse]."
  # "She smiles sweetly and rests her head against my shoulder, relaxing into me."
  "She smiles sweetly and rests her head against my shoulder, relaxing{space=-10}\ninto me."
  "It's clear that she's starting to feel more and more at ease around me, and that feels great."
  "In the past, many people have looked at me like I'm some kind of creep, but there's none of that in her eyes."
  scene location with fadehold
  return
