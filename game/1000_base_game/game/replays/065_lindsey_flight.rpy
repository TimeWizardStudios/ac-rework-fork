init python:
  register_replay("lindsey_flight","Lindsey's Flight","replays lindsey_flight","replay_lindsey_flight")

label replay_lindsey_flight:
  show lindsey roof_ground_still with fadehold
  "Oh, wow. Half the school is still here."
  "But what are they all looking at? The fireworks?"
  window hide
  show lindsey roof_ground_still:
    ease 3.0 yalign 0.0
  pause 3.0
  hide lindsey
  show lindsey roof_ground_still:
    yalign 0.0
  window auto
  "Holy shit."
  mc "[lindsey]!!"
  mc "Get away from the edge!!"
  "There are too many people shouting..."
  "Some are even chanting for her to jump..."
  "Luckily, she probably can't hear a thing up there."
  "Where is [jo]?"
  "..."
  "[lindsey] looks completely out of it."
  "A gust of wind could knock her right off the roof."
  "..."
  "Where the hell is [jo]?!"
  window hide
  $set_dialog_mode("phone_call_plus_textbox","jo")
  pause 0.5
  $guard.default_name = "Phone"
  window auto
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","jo")
  jo "Hello?"
  mc "[jo], how long until\nyou're here?"
  jo "It'll be another fifteen\nminutes or so, honey."
  jo "I just got into the car."
  mc "That's too long..."
  jo "I'll be there as\nfast as I can—"
  play sound "end_call"
  $set_dialog_mode("")
  pause 0.5
  window auto
  menu(side="middle"):
    "\"Fuck it, I'm not waiting\nfor [lindsey] to fall down!\"":
      "Fuck it, I'm not waiting for [lindsey] to fall down!"
      "I'm going up there even if I have to break down the door!"
      window hide
      show lindsey roof_door_still
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 2.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Oh, my god..."
      "She seems to be in a complete trance. She probably doesn't realize how close she is to the edge of the roof."
      mc "[lindsey]!!"
      lindsey roof_door_still "Run! Run!"
      mc "What are you saying?! Why are you up here?!"
      "She's totally out of it..."
      mc "[lindsey], get away from the edge!!"
      "..."
      "No reaction. She doesn't seem to hear me."
      "Maybe the wind is to blame, or the shouting below..."
      window hide
      show lindsey roof_door_wings with Dissolve(.5)
      window auto
      "What the hell is she doing?"
      mc "[lindsey]!! Listen to me!!"
      "For a moment, she looks back at me, but her gaze is empty."
      "It's like she's not there at all."
      "Then she turns away."
      lindsey roof_door_wings "Fly!"
      mc "No!! Stop!!"
      lindsey roof_door_wings "Fly to the moon!"
      window hide
      show lindsey roof_door_step with Dissolve(.5)
      window auto
      "And just like that, your heart stops as she steps over the edge."
      "For a moment, she hangs in the balance."
      "Like an angel about to take flight."
      "And in that split second you think that maybe if you pray hard enough..."
      "Maybe if you believe it with all your might..."
      "Maybe if you wish for it with every penny and birthday candle\nyou have..."
      "Then just maybe, maybe wings of blazing white will unfold on\nher back."
      "Wings that catch the updraft and send her soaring into the sky."
      window hide
      show expression LiveComposite((1920,1080),(0,0),"lindsey LindseyPose02c",(0,0),"misc flashback") onlayer screens zorder 4 as flashback
      show white onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide lindsey
      hide white onlayer screens
      with Dissolve(.5)
      window auto
      "With each wingbeat, memories flood your mind..."
      window hide
      show expression LiveComposite((1920,1080),(0,0),"nurse clinic_nurse_confused",(0,0),"lindsey clinic_lindsey_laughing",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      $set_dialog_mode("default_no_bg")
      ""
      show expression LiveComposite((1920,1080),(0,0),"lindsey lindseycatch_smile",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      ""
      show expression LiveComposite((1920,1080),(0,0),"lindsey soaked_flirty",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      ""
      show expression LiveComposite((1920,1080),(0,0),"lindsey hugging_mcshirt",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      ""
      show expression LiveComposite((1920,1080),(0,0),"lindsey dancing_with_the_mc_background",(0,0),"lindsey dancing_with_the_mc_admiring",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      ""
      show expression LiveComposite((1920,1080),(0,0),"lindsey kiss_kitchen",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      ""
      show expression LiveComposite((1920,1080),(0,0),"lindsey painting_date_laughing",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      "You could've had it all..."
      window hide
      show lindsey roof_door_fall
      show white onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide flashback onlayer screens
      hide white onlayer screens
      with Dissolve(.5)
      window auto
      "But then reality creeps back in."
      "And in the back of your mind you know... she isn't really an angel."
      "No amount of wishing will save her from the pull of gravity."
      "She's just a girl."
      "Just [lindsey]."
      play sound "falling_thud"
      show lindsey roof_door_lindseyless with vpunch2
      "And then she falls."
    "\"Oh, well. I've done what I could.\"":
      "Oh, well. I've done what I could."
      "She'll probably snap out of it and come back down, right?"
      "I'm not sure why I'm all that worried."
      "Sure, she's been a bit clumsy lately... but surely she's not going to fall off the roof."
      "It's weird that she's up there, but what can you do?"
      "It's not like anyone else is lifting a finger, anyway."
      "At least I called [jo] and she'll be here in a few minutes."
      window hide
      show lindsey roof_ground_wings with Dissolve(.5)
      window auto
      "Oh, good! She's finally moving!"
      "..."
      "Wait a minute, what is she doing?"
      "No, no, no, no—"
      "[lindsey], what are you—"
      window hide
      show lindsey roof_ground_fall with Dissolve(.5)
      window auto
      "And then it just happens."
      "As if time stops, she hangs suspended in the air for a moment."
      "And in that moment — that fraction of a second — you feel regret."
      "Regret that you didn't try harder to help her."
      "Regret that you didn't get to know her better."
      "Regret for things said, and unsaid."
      "Things that could have been different."
      "It's all too easy to get caught up in life. Even when you have another chance."
      "Because sometimes the bigger picture gets clouded by impulses and desires."
      "Sometimes, reality itself is blinding."
      "Your mind fills with glimpses of past conversations."
      "Visions of could-have-beens and unfulfilled dreams."
      "Then the moment passes..."
      "...gravity takes hold..."
      play sound "falling_thud"
      show lindsey roof_ground_lindseyless with vpunch2
      "...and she falls."
  scene location with fadehold
  return
