init python:
  register_replay("flora_secret","Flora's Secret","replays flora_secret","replay_flora_secret")

label replay_flora_secret:
  show flora bedroom_hug with fadehold
  "Before she can continue doubting this, doubting herself, doubting us...{space=-55}\nI step in close and pull her to me."
  "Nothing sexual, nothing scandalous. Just a hug."
  "A warm embrace as I fold my arms around her and squeeze her\nto me."
  "At first, there's some tension in her body... but then she relaxes into me."
  "Her cheek pressed against my chest."
  flora bedroom_hug "You actually smell kind of nice..."
  mc "Don't sound too surprised."
  "She giggles softly and melts into me."
  flora bedroom_hug "You feel nice too..."
  mc "Eh, I guess you feel all right."
  window hide
  pause 0.125
  show flora bedroom_hug laughing with Dissolve(.5)
  pause 0.125
  window auto
  flora bedroom_hug laughing "You're a real comedian."
  show flora bedroom_hug blush with dissolve2
  "She pulls away slightly and looks up at me."
  "Her eyes shine with worry, but more than that, they're alight with something tender."
  "Something I rarely get to see, but will always treasure."
  "Especially in these safe, hidden moments."
  "Just the two of us with no pretense."
  "No walls and no lies."
  "Just the soft feel of her in my arms, and that look in her eyes."
  "The truth of what we really feel for one another."
  "Her cheeks are flushed, lips slightly parted as her breathing quickens."
  "I can feel her practically trembling in my arms."
  mc "It's okay. I got you."
  flora bedroom_hug blush "O-okay."
  window hide
  pause 0.125
  show flora bedroom_hug kiss with Dissolve(.5)
  pause 0.125
  window auto
  "And with that, the last of her resolve melts away."
  "I'm kissing her, and she's kissing me back."
  if quest.flora_squid["shower"] not in ("hug","ass"):
    "I can hardly believe it — [flora], kissing me!"
    "Giving into what we're told by society is wrong... but we feel is right.{space=-5}"
    "I feel dizzy and elated all at once."
  "Her lips soft, sweet, and the polar opposite of her usual sharp and bratty self."
  window hide
  pause 0.125
  show flora bedroom_hug concerned with Dissolve(.5)
  pause 0.125
  window auto
  "But just as I start to deepen the kiss, she pulls away."
  mc "What's wrong?"
  if quest.maya_sauce["bedroom_taken_over"]:
    flora bedroom_hug concerned "We should go to your room..."
    "She's right. Out here we're doomed if anyone walks up the stairs."
    mc "What about [maya]?"
    flora bedroom_hug concerned "I'll just text her some excuse."
    mc "Why not do it in your room instead?"
    flora bedroom_hug surprised "You're still banned, remember?"
    # "She acts all coy when she says it, but there's also a hint of seriousness in her voice."
    "She acts all coy when she says it, but there's also a hint of seriousness{space=-65}\nin her voice."
    mc "Err, fine."
    mc "You text her, and I'll try to get the door open in the meantime."
  else:
    flora bedroom_hug concerned "We should lock the door..."
    menu(side="middle"):
      extend ""
      "\"It's fine. I need you right now.\"":
        mc "It's fine. I need you right now."
        mc "I don't care about anything or anyone else."
        flora bedroom_hug surprised "[mc], I—"
        window hide
        pause 0.125
        show flora bedroom_hug kiss with Dissolve(.5)
        pause 0.125
        window auto
        "Once again, my hungry mouth swallows her words."
        "With my hands on her hips, I walk her backwards to my bed."
      "\"Good idea. We can't be too careful.\"":
        mc "Good idea. We can't be too careful."
        flora bedroom_hug concerned "R-right."
        play sound "lock_click"
        "In a moment of rare excellence and sex-driven efficiency, I manage to lock the door behind me without ever letting go of her."
        flora bedroom_hug blush "Thank you..."
        mc "Of course. Whatever makes you most comfortable."
        window hide
        pause 0.125
        show flora bedroom_hug kiss with Dissolve(.5)
        pause 0.125
        window auto
        "She smiles at me, then presses her lips to mine and pulls me toward the bed."
  window hide
  hide screen interface_hider
  show flora bedroom_sex anticipation
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  if quest.maya_sauce["bedroom_taken_over"]:
    "A few moments later, we're in my old room, which now smells like some kind of girly mountain flowers or something."
    "[flora] smiles at me, then presses her lips to mine again and pulls me toward the bed."
  "We become a tangle of limbs and fevered kisses."
  if quest.maya_sauce["bedroom_taken_over"]:
    "All I know is I need to be as close to her as possible."
  else:
    "All I know is I need to be as close to [flora] as possible."
  "She's rarely so soft, so vulnerable, and I feel privileged to have this moment right now."
  "I want everything off, our hearts laid bare."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "She looks up at me with bright, expectant eyes."
  flora bedroom_sex anticipation "Not too hard, okay...?"
  "Her words come out in a whisper and it melts my heart anew."
  mc "I'll be gentle."
  "From one heartbeat to the next, with our pulses beating in time and our breath mingling..."
  show flora bedroom_sex insertion with dissolve2
  "...I slowly push my way into her."
  "She gives a tiny gasp and her head instantly rocks back against\nthe pillow."
  show flora bedroom_sex penetration1 with dissolve2
  flora bedroom_sex penetration1 "Ooooh..."
  "Fuck. She fits me like a glove. So tight and slick."
  "I'm not even halfway in yet and already her pussy is constricting around me."
  mc "Are you okay?"
  flora bedroom_sex penetration1 "Y-yes... keep going..."
  show flora bedroom_sex penetration2 with dissolve2
  "Glad that none of this seems to be too much for her, I happily do as she says."
  "With my hands braced on the mattress on either side of her, I work my way deeper inside."
  show flora bedroom_sex wrapped_arms penetration3 with dissolve2
  "She lets go of her white-knuckled hold of the sheets and wraps her arms around me, nails digging into the back of my neck."
  mc "God, [flora]... you feel so good..."
  show flora bedroom_sex wrapped_arms penetration4 with dissolve2
  flora bedroom_sex wrapped_arms penetration4 "Mmm, yes..."
  mc "I'm going to start moving now, okay?"
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.3
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.3
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.3
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms penetration4 "Oh, fuck!"
  mc "You like that?"
  flora bedroom_sex wrapped_arms penetration4 "Mmm, yes! P-please, don't stop!"
  mc "I got you, remember?"
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.175
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.175
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms penetration4 "Mhmmmm..."
  "She lets out a little whine and bites her bottom lip and it undoes\nme completely."
  "Having [flora] here, beneath me. Taking her like this."
  "Wholesome, tender... yet a secret we have to keep."
  "How can anything this good possibly be wrong?"
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms penetration4 "Goodness, [mc]..."
  "She goes rigid beneath me and calls out desperately."
  "It's like her words unleash something within me, and I thrust my hips harder until I'm bottoming out against her cervix."
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.5)
  pause 0.125
  window auto
  mc "F-fuck, [flora]!"
  "When she wraps her ankles around my hips and uses her legs to press my ass harder against her, I unleash a groan."
  "We both want the same thing, her feeling every last inch of me."
  "Each thrust with my whole weight behind it."
  "Hammering home that there is no going back from this."
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.125
  window auto
  "Our bodies slick with sweat. The sweet smell of her pussy juices coating my sheets."
  "And her taking my dick, her flower spread open for me."
  "Her entire being needing me, holding me tight as she begs me\nfor more."
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms wrapped_legs penetration4 "Ahhh! [mc]! I'm about to—"
  mc "Cum with me, [flora]!"
  flora bedroom_sex wrapped_arms wrapped_legs penetration4 "God! Oh, god! P-please!"
  "The sound of her pleading for my cum, my semen, my hard cock, finally unravels me."
  menu(side="middle"):
    extend ""
    "Cum inside":
      mc "I-I'm cumming too!"
      window hide
      pause 0.125
      show flora bedroom_sex cum with hpunch
      pause 0.125
      window auto
      flora bedroom_sex cum "Ohhhhh!"
      "With one final pump I unleash my hot, sticky load deep within her."
      "She arches against me, her feet still digging into my ass."
      window hide
      pause 0.125
      show flora bedroom_sex aftermath with Dissolve(.5)
      pause 0.125
      window auto
      "There really is no going back from this."
      "It's just the two of us on this road now."
      "And whether it's to hell or heaven... I don't care."
      "All that matters is [flora] senseless with lust beneath me."
      "Her soft little mewls still escaping her parted lips."
      window hide
      show flora bedroom_aftermath tired
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "With a shudder of pleasure, my knees go weak, and I collapse against her."
      "Together, we catch our breath. Still coming down from what we've just done."
      "From what {i}I've{/} just done."
      flora bedroom_aftermath tired "Jesus, I'm a mess..."
      flora bedroom_aftermath tired "I can't believe you came inside!"
      mc "Heh! It will be okay..."
      "...and even if it isn't, maybe we could make it work somehow?"
      flora bedroom_aftermath tired "I thought we were going to be careful."
      mc "Well, they have... stuff, just in case."
      mc "I was just so caught up in the moment..."
      mc "And with the way you were squeezing me, I couldn't pull out even if I wanted to."
      flora bedroom_aftermath blush "Oh, my god!"
      flora bedroom_aftermath blush "I, um... I should go get cleaned up..."
      mc "Right..."
      flora bedroom_aftermath blush "And remember, not a word of this to anyone."
      mc "My lips are sealed."
      flora bedroom_aftermath blush "They better be..."
      window hide
      pause 0.125
      show flora bedroom_aftermath with Dissolve(.5)
      pause 0.125
      window auto
      "And just like that, she's gone."
      "But I can still feel her in my arms."
      "I just hope she doesn't regret what we've done..."
      "I know I don't."
      "..."
      "Man, I'm pretty tired after that workout..."
      "Maybe I could just sleep for a bit and skip my first classes?"
    "Pull out":
      "There's no going back from this path we're on now... but we can still be careful about it."
      "Our little secret."
      "A cool oasis for just the two of us to drink each other in."
      window hide
      show flora bedroom_aftermath blush dick
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Not a second too soon, I pull my dick out of her sopping pussy."
      "It squelches as it reluctantly releases me."
      window hide
      pause 0.125
      show flora bedroom_aftermath blush cumming with vpunch
      show flora bedroom_aftermath blush dick cum with Dissolve(.5)
      pause 0.125
      window auto
      "Load after load coat her heaving stomach."
      "Her eyes flutter with exhaustion, a faint smile on her lips."
      flora bedroom_aftermath blush dick cum "Oh, my god..."
      window hide
      pause 0.125
      show flora bedroom_aftermath tired cum with Dissolve(.5)
      pause 0.125
      window auto
      "In the aftermath, we just lie there together, soaking it all in."
      "Catching our breath, our spent limbs sprawled across the mattress."
      mc "That was..."
      flora bedroom_aftermath tired cum "Yeah..."
      "She gives a little giggle."
      mc "Fuck, you're adorable when you want to be."
      flora bedroom_aftermath blush cum "When I'm not being a brat, you mean?"
      mc "You said it, not me."
      flora bedroom_aftermath blush cum "Mm-hmm."
      flora bedroom_aftermath blush cum "Thanks for, err... pulling out."
      mc "We can't be too careful, right?"
      flora bedroom_aftermath blush cum "Right."
      window hide
      pause 0.125
      show flora bedroom_aftermath asleep cum with Dissolve(.5)
      pause 0.125
      window auto
      "And then, in an even bigger shock to my system, [flora] snuggles up to me and shuts her eyes."
      flora bedroom_aftermath asleep cum "I'm exhausted..."
      flora bedroom_aftermath asleep cum "Maybe we could just sleep for a bit and skip our first classes?"
      "I swallow past the giddy lump in my throat."
      if quest.maya_sauce["bedroom_taken_over"]:
        "Pleased that she wants to stay like this in [maya]'s bed with me for a little while longer."
      else:
        "Pleased that she wants to stay like this in my bed with me for a little while longer."
      "Pleased I have had this effect."
      "Plus, I too am pretty tired after that workout."
      mc "Sleep well, [flora]."
      flora bedroom_aftermath asleep cum "You too, [mc]..."
  scene location with fadehold
  return
