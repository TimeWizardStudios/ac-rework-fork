init python:
  register_replay("flora_distraction","Flora's Distraction","replays flora_distraction","replay_flora_distraction")

label replay_flora_distraction:
  show flora streaking1 with fadehold
  "[flora] looks nervous."
  "I mean, probably less nervous than I would be in her place..."
  "I'd be shaking, most likely... maybe even crying."
  "But she's always been braver than I."
  "Always been in charge of her own destiny."
  show flora streaking2 with dissolve2
  "She looks left, right, then just goes for it, boobs bouncing!"
  "Right by the [guard]'s booth!"
  "Oh, shit! He saw her, despite his doughnut-preoccupation..."
  show flora streaking3 with dissolve2
  "Run, [flora], run!"
  "..."
  "God, he's slow. It's like he's not even running."
  "He's like... shuffling forward..."
  "Weak legs are heavy, there's doughnut on his sweater already."
  "Anyway, I should probably—"
  guard "Hey, miss!"
  "Oh, wow! The chase is on! I wish I'd brought popcorn..."
  "One man chasing a naked girl."
  "Illegal if it weren't for his uniform."
  "Strange how clothes make all the difference."
  "Or lack there of, in some cases..."
  "[flora] actually looks worried now, glancing over her shoulder."
  "She's given up on covering herself, and allows herself to be on display for the entire entrance hall."
  "How many times have I not dreamed about having her body close\nto mine..."
  "Her hot skin, burning against my own..."
  "Her nipples hard, her pussy wet..."
  "Shit, I really need to stop thinking of her like that."
  "But... it's hard. It gets me hard. She's so cute and annoying at the same time..."
  "It's her fire that makes her so attractive to me... and perhaps the forbidden nature."
  "I'm a total hypocrite for not wanting other guys to look her way, and then making her streak through the school."
  "But somehow... she's doing it for me, and that makes it okay in a weird way."
  "God, my feelings for her are so twisted..."
  "Everything about it is twisted... and somehow, she's okay with it..."
  "Or at least pretends to be..."
  "..."
  "No, she's not pretending. That can't be."
  "She wouldn't get naked for anyone but me..."
  "It's like an unspoken, forbidden, urge inside us both."
  "It's been there for a while, festering in the back of our minds."
  "She can expose her body to the world, but never her mind."
  "That's what's sad and hot at the same time."
  "The world will always keep us apart, but even now her eye catches mine."
  "And there's a twinkle of that dark passion that only I get to see..."
  "Last time around, I ignored it. Pretended it wasn't there."
  "Conforming to society's expectations."
  "But really, what did society ever do for me?"
  "This time around, I will hold her, love her. Make her mine."
  "The consequences be damned."
  "Strange how I decided that just now, in the middle of a chase..."
  "But it doesn't matter. Nothing matters if I don't take risks."
  "Nothing matters if I don't get to have her—"
  show flora streaking4
  flora streaking4 "Eeep!" with vpunch
  window hide
  window auto
  play sound "falling_thud"
  show black onlayer screens zorder 100 with vpunch
  hide black onlayer screens
  scene location
  with fadehold
  return
