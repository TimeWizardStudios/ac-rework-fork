init python:
  register_replay("lindsey_foot_bath","Lindsey's Foot Bath","replays lindsey_foot_bath","replay_lindsey_foot_bath")

label replay_lindsey_foot_bath:
  show minigames simple_stepping background as minigame_background
  show minigames simple_stepping foreground as minigame_foreground:
    xpos 0 ypos 0
  with fadehold
  lindsey "Hey, guys!"
  kate "[lindsey]! Glad you could make it!"
  lindsey "What's going on here?"
  kate "We're just playing with the mystery box."
  lindsey "What's inside it?"
  kate "That's a secret..."
  lindsey "Hehe! I guess it wouldn't be a mystery box otherwise..."
  kate "Exactly!"
  lindsey "So, how does it work?"
  kate "You take of your shoes and stick your leg in."
  lindsey "Oh, um... I don't know about that..."
  kate "It'll be fun! Trust me, it's like a pedicure."
  kate "Considering how important your feet are to you, you deserve\nsome pampering!"
  lindsey "Err, okay..."
  window hide
  show minigames simple_stepping feet lindsey_idle as minigame_lindsey behind minigame_foreground:
    xoffset 1200 yoffset -50 zoom 0.75
    parallel:
      easeout 2.0 zoom 1.0
    parallel:
      easein_circ 2.5 xoffset 818
    parallel:
      easein_expo 2.5 yoffset 402
  pause 2.5
  window auto
  "Uh-oh. This might not turn out well..."
  hide minigame_lindsey
  show minigames simple_stepping feet lindsey_idle as minigame_lindsey behind minigame_foreground:
    xpos 818 ypos 401
  if lindsey["romance_disabled"]:
    "Maybe I'll cut her foot off and ruin her dreams?"
    "That would be fitting."
    "But on the other hand, I could keep this over her head..."
    "Watching [lindsey] go down slowly is a better punishment."
  else:
    "It's one thing to worship the feet of those bitches, but [lindsey]\nis too pure for this."
    "It'd feel like taking advantage of her innocence. I'd feel like a creep."
    "On the other hand, it might be my only chance to touch her beautiful feet..."
    menu(side="middle"):
      extend ""
      "Worship":
        "God, she smells amazing... I've been waiting for this moment."
        "I'll make her love my mouth so much, she'll remember it forever."
      "Refuse":
        "I'd feel weird keeping this a secret from [lindsey]."
        "If I do something like this with her, I want it to be consensual."
        lindsey "Um... is anything supposed to happen?"
        kate "Yeah, don't you feel anything?"
        lindsey "Not really..."
        kate "Okay, let me handle this."
        "[kate] leans in closer, her voice but a whisper."
        kate "{i}Bitch, do your job.{/}"
        mc "..."
        kate "{i}I swear to god, you won't be able to walk for a month.{/}"
        mc "..."
        lindsey "Still nothing..."
        kate "Sorry, sweetie. We'll try again later, okay?"
        lindsey "All right! No worries!"
        window hide
        hide minigame_lindsey
        show minigames simple_stepping feet lindsey_curled as minigame_lindsey behind minigame_foreground:
          xoffset 818
          yoffset 402
          parallel:
            easeout 1.0 xoffset 1200
          parallel:
            easein 1.0 yoffset -500
        pause 0.5
        window auto
        "Their steps fade away, and the beat of the music takes over."
        "The taste of their feet still linger on my lips."
        "So very humiliating, and a night I'll never forget."
        scene location with fadehold
        return
  "As I lean forward, mere inches from her wiggling toes, the scent hits me in full."
  "Cotton candy plus a light hint of feminine sweat and shoe leather — an intoxicating combination."
  "My breath tickles her skin and she jumps in surprise."
  lindsey "W-what's in there?!"
  kate "It's okay! Just enjoy."
  lindsey "But..."
  extend " eeek!" with vpunch
  window hide
  play sound "falling_thud"
  show black onlayer screens zorder 100 with vpunch
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "My tongue makes contact with her toes for the first time, and she almost pulls out her legs."
  "The kick hits me in the nose, and I suddenly see stars."
  if lindsey["romance_disabled"]:
    "Fucking bitch..."
  else:
    "But it's fine, she's skittish. I can't blame her."
  show black onlayer screens zorder 100
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  kate "Calm down, [lindsey]! It's a foot bath!"
  lindsey "B-but I felt a tongue or something..."
  kate "Yes, it's a tongue-foot bath."
  lindsey "What?!"
  lindsey "Who is in there?!"
  kate "Don't worry about it! They want to do it."
  lindsey "Are... are you sure?"
  kate "I'm positive."
  lindsey "Um... whoever is in there... are you okay with this?"
  "I drag my tongue across her sole, from heel to toe, to show her\nthat I'm down."
  lindsey "Ooooh!"
  lindsey "I... I don't know if I like this..."
  kate "It'll grow on you."
  "The sole of her foot is coarse against my tongue."
  "It's a well-used sole, a runner girl's sole."
  if not lindsey["romance_disabled"]:
    "[lindsey] is a princess, but she's also an athlete."
    "And her feet and legs are where it shows the most."
  "Slowly, taking my time, I slip my tongue between her toes."
  "She squeals in surprise and delight."
  if lindsey["romance_disabled"]:
    "Always so fake in the way she acts..."
    "I almost feel an urge to bite her."
    "Instead, I nibble on her toes, and again she acts scandalized."
    "As if all girls aren't born whores."
  else:
    "When it comes to her, it doesn't feel like an act of submission\nto me."
    "She's so inexperienced, so pure... It almost feels wrong to do\nthis to her."
    "I nibble on her toes, and she makes the cutest noises."
  "As I start to kiss my way up the arch of her foot, her squirming abates."
  show minigames simple_stepping feet lindsey_curled as minigame_lindsey with dissolve2
  "A small moan escapes her lips, and she spreads her toes."
  "This invitation is all I need to put my tongue deeper between her toes, and apply some suction."
  "A shiver prickles the skin of her leg."
  "Her breathing increases."
  lindsey "Oh, my goodness..."
  "I can't see [lindsey] face, but she's clearly blushing at the sensation."
  "The taste of her foot fills me with hunger."
  "I need to have her. All of her."
  "But I'm locked inside this box..."
  "Instead, my lips increase their ravenous pace."
  "My hand strokes my cock."
  window hide
  hide minigame_lindsey
  show minigames simple_stepping feet lindsey_curled as minigame_lindsey behind minigame_foreground:
    xoffset 818
    yoffset 402
    parallel:
      easeout 1.0 xoffset 1200
    parallel:
      easein 1.0 yoffset -500
  pause 0.5
  window auto
  "And just as I'm about to come... she pulls her foot out."
  "I'm left licking my lips, her taste still thick in my mouth."
  if lindsey["romance_disabled"]:
    "Fucking tease! I knew it..."
    "Never a good time with her."
  else:
    "Damn it! Perhaps I went too hard or too fast on her?"
    "In the future, I need to be more gentle with [lindsey] if I want to\nhave a chance..."
  "Anyway..."
  "Still hard as a rock, I give my cock a few more strokes and cum\nall over the floor of the box."
  "I suppose things could've gone much worse."
  scene location with fadehold
  return
