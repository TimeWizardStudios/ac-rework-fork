init python:
  register_replay("flora_bonsai_hug","Flora's Hug ","replays flora_bonsai_hug","replay_flora_bonsai_hug")

label replay_flora_bonsai_hug:
  show flora hugging_flora_sad_bandaid with Dissolve(.5)
  mc "It's okay. You're safe now."
  "Feels weird hugging her like this, but also... right somehow."
  "We've always been at each other's throats, but this is nice."
  "She's not the only one relieved. Not sure what I would've done if that thing had dragged her off into the forest."
  "Sometimes it's hard to admit, but I do care about her so much."
  show flora hugging_flora_openmouth_bandaid with Dissolve(.5) 
  flora "Can you help me take the bandaid off? I don't wanna be blind anymore."
  show flora hugging_flora_sad_bandaid with Dissolve(.5) 
  mc "Okay. I think it's been almost a week, right?"
  $flora.unequip("flora_blindfold")
  show flora hugging_flora_admiring with Dissolve(.5)
  "Seeing her looking up at me like this... it feels less real than the tree-monster itself."
  "Somehow, for the first time ever, admiration and relief fill her eyes."
  "Her whole body still trembles as she wraps her arms around me and scoots closer."
  "Guess I always figured that she'd be into this sort of thing, but maybe the reality is very different from the fantasy."
  show flora hugging_flora_admiring with Dissolve(.5) 
  flora "I'm glad you were around."
  mc "I'm glad too."
  "Knowing her, she'll have forgotten about this by the start of next week, but for once it's nice to be a hero in her eyes and not some worthless loser."
  "And maybe it's wrong to feel this way about her, but if I really tried my best, maybe I could make her happy?"
  show flora hugging_flora_content with Dissolve(.5) 
  flora "Just hold me."
  "The urge to troll her is always going to be strong, but this seems like a good moment to just... be there for her."
  "She pulls me tighter and her heart thuds against my chest."
  "Maybe we can be more than this?"
  "Would she be okay with the world despising her? Probably not."
  mc "Whatever happens, I'll be there for you."
  "She just smiles, and that's all that matters." 
  "I just need her to smile."
  hide flora with Dissolve(.5)
  return

