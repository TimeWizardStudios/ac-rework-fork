init python:
  register_replay("lindsey_solo_dance","Lindsey's Solo Dance","replays lindsey_solo_dance","replay_lindsey_solo_dance")

label replay_lindsey_solo_dance:
  stop music fadeout 2.0
  show lindsey dancing_on_her_own5 with fadehold
  play music "<from 33.7>epic_medieval" noloop
  queue music "epic_medieval"
  "[lindsey] is fearless. Dancing on her own like nobody's watching."
  show lindsey dancing_on_her_own7 with dissolve2
  "I wish I had half of her confidence..."
  show lindsey dancing_on_her_own8 with dissolve2
  "The way she rocks her hips, her waist, her perfect ass."
  show lindsey dancing_on_her_own6 with dissolve2
  "Her moves are so soft, so gentle and delicate!"
  show lindsey dancing_on_her_own4 with dissolve2
  "She truly is one of a kind. A real life princess."
  show lindsey dancing_on_her_own2 with dissolve2
  "So precious..."
  show lindsey dancing_on_her_own1 with dissolve2
  "I just want to keep her safe. Protect her from the world."
  show lindsey dancing_on_her_own3 with dissolve2
  "Let her live in a world where nothing bad will ever happen to her."
  show lindsey dancing_on_her_own5 with dissolve2
  "..."
  show lindsey dancing_on_her_own7 with dissolve2
  "She probably wouldn't like it if I took a picture of her, but this is a once in a lifetime moment..."
  menu(side="right"):
    extend ""
    "Take a picture":
      show lindsey dancing_on_her_own8 with dissolve2
      "What the hell... I'll probably regret it later if I don't."
      window hide
      show lindsey dancing_on_her_own6 with dissolve2
      show expression "lindsey avatar events fall crashed lindseycrashed_camera" as camera
      with Dissolve(.5)
      pause 0.5
      show white onlayer screens zorder 100 with Dissolve(.15)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.15)
      pause 0.5
      show lindsey dancing_on_her_own4
      hide camera
      with Dissolve(.5)
      window auto
      "Phew, [lindsey] didn't seem to notice! She's too caught up in the music.{space=-55}"
      window hide
      show lindsey dancing_on_her_own2 with Dissolve(.5)
    "Let her dance in peace":
      show lindsey dancing_on_her_own8 with dissolve2
      "[lindsey] looks perfect like this... No need to risk it."
      show lindsey dancing_on_her_own6 with dissolve2
      "Besides, this will be the screensaver of my mind for a while."
      window hide
      show lindsey dancing_on_her_own4 with Dissolve(.5)
  window auto
  stop music fadeout 2.0
  scene location with fadehold
  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"
  else:
    stop music
  return
