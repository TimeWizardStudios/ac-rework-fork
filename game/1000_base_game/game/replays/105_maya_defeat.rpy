init python:
  register_replay("maya_defeat","Maya's Defeat","replays maya_defeat","replay_maya_defeat")

label replay_maya_defeat:
  window hide None
  hide screen interface_hider
  show maya sink bending_over
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Then, she pushes me out of the way, those beautiful, perfect tits bouncing..."
  "...before running straight for the sink."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "She bends over, and it's beautiful."
  window hide
  show maya sink spitting with vpunch
  window auto
  "In an instant, she retches and spits out the sauce."
  "Goddamn!"
  "That view right there has made me a winner already."
  flora "Well, I guess that does it..."
  flora "I'm shocked to say it, but [mc], you are the winner."
  mc "There's no need to sound so surprised."
  mc "But thank you, [flora]."
  # mc "Hey, [maya]? I guess I'll be signing you up for that talent contest, after all."
  mc "Hey, [maya]? I guess I'll be signing you up for that talent contest, after all.{space=-85}"
  maya sink looking_back "...!"
  "For a moment, it looks like she's about to cry, but maybe that's just the tears from the hot sauce."
  "She looks so pretty standing there all red-faced and vulnerable."
  "But after a moment of catching her breath, she finally straightens and comes back over to us."
  scene location with fadehold
  return
