init python:
  register_replay("kate_girlfriend","Kate's Girlfriend","replays kate_girlfriend","replay_kate_girlfriend")

label replay_kate_girlfriend:
  show kate isabelle_kiss with fadehold
  "Whoa... I didn't expect that..."
  "In fact, I didn't expect any of this."
  "I thought that they were somehow messing with me."
# "I seriously can't believe that [isabelle] would date [kate] after what she put her through."
  "I seriously can't believe that [isabelle] would date [kate] after what she{space=-5}\nput her through."
  "I thought there would be a huge lawsuit."
  "And even as the scene unfolds before my very eyes..."
  "...as the lipsticks of opposite worlds meet..."
  "...as vanilla frosting meets icy mint..."
  "...as cinnamon meets rose thorn..."
  "...I struggle to believe that this is real."
  "Surely, [isabelle]'s dad would've crushed [kate] in court?"
  "Unless there's something preventing legal action..."
  "..."
  "[kate] did say she'd have an airtight alibi, so maybe that's it?"
  "But that still doesn't explain why they're kissing and being friendly..."
  if quest.nurse_aid["name_reveal"]:
    # "Perhaps there's also a blackmail angle? [kate] certainly didn't have any issues doing that to [amelia]."
    "Perhaps there's also a blackmail angle? [kate] certainly didn't have any{space=-35}\nissues doing that to [amelia]."
  else:
    # "Perhaps there's also a blackmail angle? [kate] certainly didn't have any issues doing that to the [nurse]."
    "Perhaps there's also a blackmail angle? [kate] certainly didn't have any{space=-35}\nissues doing that to the [nurse]."
  "And honestly, I didn't think [kate] would take it this far with [isabelle]..."
  scene location with fadehold
  return
