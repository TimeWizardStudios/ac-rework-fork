init python:
  register_replay("lindsey_kiss","Lindsey's Kiss","replays lindsey_kiss","replay_lindsey_kiss")

label replay_lindsey_kiss:
  show lindsey kiss_kitchen with fadehold
  mc "!!!"
  "[lindsey] gets up on her tippy-toes and leans in."
  "Our lips meet for a moment, and time seems to stop."
  "It sounds cliché, but it's what happens when a moment gets seared into memory for the rest of your life."
  "So soft, so sweet, on my upper lip..."
  "Misaligned, but still perfect. Still absolutely electric."
  "Then she moves down, taking my bottom lip between hers."
  "She blushes, smiles through the kiss, then gets it right."
  "Three kisses in one!"
  "It's one of those moments you'll never forget..."
  scene location with fadehold
  return
