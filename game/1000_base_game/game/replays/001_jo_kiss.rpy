init python:
  register_replay("jo_kiss","Jo's Kiss","replays jo_kiss","replay_jo_kiss")

label replay_jo_kiss:
  show jo JoMorningKiss with fadehold
  "[jo] showing affection? That's new."
  "Well, not really new."
  "All those years of slowly losing her glow. Man, it's good to see her so perky again."
  "And to feel those forbidden sensations that only the most deprived and sexually starved person can ever experience."# from their mother."
  "The sweet familiar scent of her mango-pine perfume, with just a hint of sweat."
  "The skirt hugging the full curves of her mature figure, straining to contain her hips."
  "The way she sometimes rocks her blazer without a bra, just for the thrill of it. Just to feel young and hot again!"
  "The wet tingle on my forehead after the kiss."
  "This time around, being good should probably be one of my priorities."
  mc "Mmm... thanks, [jo]! I needed this."
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
