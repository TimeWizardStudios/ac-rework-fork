init python:
  register_replay("jo_assignment","Jo's Assignment","replays jo_assignment","replay_jo_assignment")

label replay_jo_assignment:
  show jo under_desk_closed_legs with fadehold
  jo under_desk_closed_legs "[maya]? What are you doing here today? School's closed!"
  maya "Um... I just..."
  jo under_desk_half_open_legs "Sweetheart, are you okay?"
  maya "I-forgot-my-lunch-money-the-other-day-and... umm..."
  jo under_desk_half_open_legs "Slow down! Have you been crying?"
  maya "N-no! I don't cry! Never!"
  jo under_desk_half_open_legs "Well, then... tell me slowly what happened."
  maya "I, um... did something bad. I was bad."
  maya "[flora] told me to, um... talk to you to get my p-punishment..."
  jo under_desk_open_legs "She did, did she?"
  maya "Y-yes. She said, um... I was a bad kitty."
  "What the hell is going on?"
  jo under_desk_open_legs "Well, were you a bad kitty?"
  "Excuse me, what?!"
  maya "Very bad, madam-principal. Very."
  jo under_desk_open_legs "I see... then you know what to do."
  jo under_desk_open_legs "Down on all fours. Bad kitties don't walk on two legs."
  maya "Yes, madam-principal."
  jo under_desk_open_legs "Okay, no more talking now! No more words for you, bad kitten."
  "Okay, this is getting out of hand!"
  menu(side="middle"):
    extend ""
    "Question it":
      mc "What the hell is going on?!"
      $quest.mrsl_table["jo_assignment"] = "questioned_it"
      maya "Meep!"
      jo under_desk_look "[mc]?! What in god's name do you think you're doing?"
      mc "I could ask you the very same—"
      jo under_desk_look "I did not tell you to talk, boy. Focus on your task at hand."
      mc "W-what?"
      jo under_desk_open_legs "I want my pantyhose ripped and my pussy licked."
      jo under_desk_open_legs "Get to it, or you're grounded!"
      maya "Meow!"
      menu(side="middle"):
        "\"You've gone insane, [jo]!\"":
          mc "You've gone insane, [jo]!"
          mc "Move so I can get out!"
          jo under_desk_open_legs "I was worried you'd act like this..."
          jo under_desk_open_legs "That's unfortunate. Very unfortunate."
          jo under_desk_closed_legs "I guess there's only one option, then."
          mc "..."
          mc "What option...?"
          jo under_desk_closed_legs "[maya]."
          jo under_desk_closed_legs "Kill."
          show jo under_desk_closed_legs_maya
          maya "Shhh! Don't scream!" with vpunch
          mc "Aaaaah!"
          scene location with fadehold
          return
        "\"Whatever you say, [jo]...\"":
          mc "Whatever you say, [jo]..."
          mc "I just want you to love me like you love [flora]..."
          jo under_desk_open_legs "None of that sniveling, [mc]."
          mc "Yes, [jo]."
          show jo under_desk_grab_stockings with Dissolve(.5)
          "God, the warmth of her leg pulsating through the thin layer of nylon makes my fingertips tingle."
          "She did tell me to rip it..."
          play sound "nylon_rip"
          show jo under_desk_ripped_stockings with hpunch
          "Like a bandaid! A pussy bandaid."
          show jo under_desk_grab_panties with Dissolve(.5)
    "Keep listening":
      "In other words, just the way I like it..."
      $quest.mrsl_table["jo_assignment"] = "kept_listening"
      maya "Meow."
      jo under_desk_open_legs "That's better."
      jo under_desk_open_legs "Now, touch yourself for me, [maya]."
      maya "..."
      jo under_desk_open_legs "That's a good kitten."
      jo under_desk_open_legs "Goodness, it's getting hot in here..."
      "[jo]'s right. I'm sweating like a pig in here, but damn it if the view isn't great!"
      "If only I could get a closer look..."
      jo under_desk_grab_stockings "Huh?"
      "She certainly felt my hand on her leg... the question is, does she recognize it?"
      maya "Meow?"
      jo under_desk_grab_stockings "Keep going..."
      "Well, that was ambiguous at best..."
      "Did she mean me or [maya]?"
      "..."
      "Only one way to find out."
      play sound "nylon_rip"
      show jo under_desk_ripped_stockings
      jo under_desk_ripped_stockings "{i}Gasp!{/}" with hpunch
      jo under_desk_grab_panties "..."
      jo under_desk_grab_panties "Go on... get in there!"
      maya "Yes, madam-principal!"
      "She was totally talking to me, wasn't she?"
      "God, she's so wet! That dark spot on her panties... she's leaking through!"
      "There's no doubt she wants this."
  "I don't know if I should be sickened or delighted about [jo] spreading her legs for me..."
  "Maybe this is what's been missing from our relationship all these years?"
  "The scent of her leg-moisturizer mixes with the fumes of her pussy in the small space under the table."
  "And for a moment, time stops."
  "It's a crossroads of sorts, where each choice could change your life forever."
  "Repercussions ricochet through my mind..."
  "What would people think of us?"
  "I see [jo] being forced to step down, losing her job."
  "I see [flora] shutting us both out, taking off with some douchebag in a BMW never to be seen again."
  "But I also see [jo]'s hairy pussy... moist and squishy with juices and arousal..."
  "And then the choice is simple! The animalistic urges take over, and I lose myself in the passion of the moment."
  show jo under_desk_ripped_panties with Dissolve(.5)
  "There it is... the pride of a woman ripened with age and wisdom."
  show jo under_desk_grab_pussy with Dissolve(.5)
  "Arousal glistening on her pubic stubble — she shaves, but not as often as girls in their prime."
  "Years of sexual experience has taught her that the stubble adds a certain feeling to the sex."
  "She's confident in her appearance. A natural pussy, some would call it..."
  "Stringy glistening secretion oozes out of her pussy."
  "It's like a banquet of succulent goodness just waiting to be devoured."
  show jo under_desk_tongue with Dissolve(.5)
  "..."
  "A quick taste never hurt nobody, right?"
  "Just a lick..."
  "Just..."
  show jo under_desk_tongue1 with Dissolve(.05)  ## 1
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  jo under_desk_tongue5 "Oh... my god..."
  "Yum!"
  "Salty with just a hint of acid."
  show jo under_desk_tongue1 with Dissolve(.05)  ## 1
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.1)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 2
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 3
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.15)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 4
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  "Damn, that's a meal and a half! Who knew that the tastiest pussy would be [jo]'s?"
  "Probably only my homeboy Oedipus."
  show jo under_desk_tongue1 with Dissolve(.05)  ## 1
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 2
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  jo under_desk_tongue5 "Fuck!"
  "Don't swear, [jo]."
  show jo under_desk_tongue1 with Dissolve(.05)  ## 1
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.1)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 2
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.15)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 3
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  "God, she's trying to drown me in pussy juices..."
  "I won't stop until she floods my mouth."
  show jo under_desk_tongue1 with Dissolve(.05)  ## 1
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 2
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.1)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 3
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 4
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  pause(.15)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 5
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  jo under_desk_tongue5 "Oh, god! Oh, fuck!"
  "Here it comes... the most unholy orgasm of her life!"
  show jo under_desk_tongue1 with Dissolve(.05)  ## 1
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 2
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 3
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 4
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 5
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_tongue1 with Dissolve(.05)  ## 6
  show jo under_desk_tongue2 with Dissolve(.05)
  show jo under_desk_tongue3 with Dissolve(.05)
  show jo under_desk_tongue4 with Dissolve(.05)
  show jo under_desk_tongue5 with Dissolve(.05)
  show jo under_desk_orgasm
  jo under_desk_orgasm "!!!" with vpunch
  "Fuck, I'm so hard I might explode in my pants!"
  if quest.mrsl_table["jo_assignment"] == "kept_listening":
    jo under_desk_orgasm2 "Oh, god..."
    "Yeah, that's one way of putting it..."
    jo under_desk_orgasm2 "Oh, god."
    jo under_desk_look_ripped_panties "Oh, god!"
    "Oh, no!"
    jo under_desk_look_ripped_panties "[mc]?! What have you done?!"
    jo under_desk_look_ripped_panties "What have {i}I{/} done?!"
    "[jo] looks like she's going to have a stroke..."
    "Fuck! This is not how I imagined it would end!"
    "This isn't how it was supposed to—"
    "I'll never be able to—"
  elif quest.mrsl_table["jo_assignment"] == "questioned_it":
    jo under_desk_orgasm2 "Goodness gracious... that was something..."
    jo under_desk_orgasm2 "My old heart is trying to rip through my chest..."
    "What a rush! What a damn rush!"
    jo under_desk_orgasm2 "[maya], stop touching yourself."
    maya "Meow..."
    jo under_desk_orgasm2 "You get to orgasm when I allow it. Today is not that day."
    jo under_desk_orgasm2 "Hands off your naughty pussy!"
    maya "Yes, madam-principal."
    "That's rough... but she did ruin our bathroom..."
    jo under_desk_orgasm2 "[mc], stay under the table while I call [flora] and tell her I've found a new pussy-licker."
    mc "W-what?"
    jo under_desk_orgasm2 "I'm kicking her out. I don't have any use of her anymore."
    mc "But—"
    jo under_desk_look_ripped_panties "Don't fucking argue with me!"
    jo under_desk_look_ripped_panties "I'm going to keep you under my office desk at all times now. You'll only eat my pussy from now on."
    jo under_desk_look_ripped_panties "That's what you wanted, isn't it?"
    "She's gone mad..."
    jo under_desk_look_ripped_panties "Isn't it?!"
    jo under_desk_look_ripped_panties "Answer me, you little shit."
    jo under_desk_look_ripped_panties "Answer me!"
  scene location with fadehold
  return
