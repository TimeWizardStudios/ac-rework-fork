init python:
  register_replay("isabelle_justice","Isabelle's Justice","replays isabelle_justice","replay_isabelle_justice")

label replay_isabelle_justice:
  show isabelle locker_push with Fade(0.5,1.0,0.0)
  play sound "fast_whoosh"
  with hpunch
  "Whoa! [maxine] sure didn't see that coming."
  "[isabelle] is fast when she wants to be... and strong..."
  "But maybe that's just her adrenaline and hatred fueling her."
  window hide
  play sound "LockerBump"
  show isabelle locker_lie with vpunch
  window auto
  isabelle locker_lie "Not so fun now, is it?"
  isabelle locker_lie "Are you bloody laughing now?"
  maxine "I would never laugh about the misuse of a priceless—"
  isabelle locker_lie "Oh, piss off!"
  "Perhaps I should step in and try to put a stop to this?"
  "But then [isabelle] would probably never forgive me..."
  "And [maxine] did steal her locker..."
  window hide
  play audio "lock_click"
  show isabelle locker_lock with Dissolve(.5)
  window auto
  isabelle locker_lock "Now, this is what poetic justice looks like!"
  isabelle locker_lock "The bully trapped in their locker."
  maxine "This is highly dangerous..."
  isabelle locker_lock "Oh, don't be so wet. There's plenty of holes to breathe through."
  "It's kind of hot seeing her so fired up. So... dominant."
  "Very few people would ever try to go up against [maxine]."
  "She's simply too much to handle."
  "But [isabelle] has her exactly where she wants her."
  "She's completely in her control."
  window hide
  show isabelle locker_sit with Dissolve(.5)
  window auto
  isabelle locker_sit "Maybe I'll let you out tomorrow. Maybe I'll let you rot in there for\na week."
  maxine "May I remind you—"
  isabelle locker_sit "No, you bloody well may not!"
  isabelle locker_sit "You may be quiet and think over your actions!"
  mc "..."
  isabelle locker_sit "What?"
  mc "You look absolutely stunning right now."
  mc "You're the very incarnation of Lady Justice."
  isabelle locker_sit "Thank you! I couldn't have done it without you."
  isabelle locker_sit "Finally, some things set right in this place..."
  mc "You were incredible. That's one bully locked up where she belongs."
  mc "I could kiss you right now."
  isabelle locker_sit "Go on, then."
  mc "Or maybe I'll do more than just kiss you..."
  isabelle locker_sit "I said go on, then!"
  window hide
  show black onlayer screens zorder 100
  show isabelle locker_invitation
  with Dissolve(.5)
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "We roll in the grass, tearing each other's clothes off."
  "Her nails dig into my skin, leaving trails of passion down my back."
  "It's the kind of unbridled lust they show in movies."
  "Raw. Skin to skin. Sweat. Hunger. Victory."
  "We both wrestle for control, both wanting to devour the other whole.{space=-15}"
  "I lift her up against the wayward locker."
  "Her mouth paints my neck with lipstick marks. Love bites with a hint of teeth."
  "Her sweet taste fills my mouth, my veins... and it burns through me like the fiery streams down an erupting volcano."
  "Her words come out breathless. Feverish."
  show black onlayer screens zorder 100
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  isabelle locker_invitation "Go on, shag me..."
  "It's one of those things you've always dreamt about hearing."
  "It's more than just a request — it's validation that you're not unlovable. Not unfuckable."
  "[isabelle] wants me. She wants me bad."
  show isabelle locker_anticipation with dissolve2
  "She wraps her legs around my waist, pulling me closer."
  "Her labia opens like a rose in bloom, kissed by the morning dew."
  "Wet, ready, and absolutely gorgeous."
  "Our eyes meet for a moment. Desire and triumph burn in her gaze."
  "This is our victory lap."
  window hide
  show isabelle locker_penetration1 with Dissolve(.5)
  pause 0.2
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.2)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.2)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  window auto
  isabelle locker_moan_penetration4 "Let her hear it! Let her feel it!"
  "[isabelle] gasps as I slam into her with force, every last inch of me pinning her against the locker."
  "I hold her there for a moment."
  "So deep inside her. Deeper than I ever thought possible."
  "The heat of her almost sends me over the edge. Her pulsating muscles squeeze me hard."
  window hide
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.1
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.1
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  pause 0.4
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  pause 0.4
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  window auto
  maxine "Be careful. This is a priceless artifact."
  isabelle locker_moan_penetration4 "Oooh... S-shut up..."
  "Oh, god... The fact that we're doing it right on top of [maxine]..."
  "Only a thin layer of metal separating us..."
  "She probably feels our heat. She's probably sweating inside."
  "Our little bully prisoner."
  window hide
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.25)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  pause 0.1
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  pause 0.1
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.25)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  window auto
  mc "F-fuck! I'm going to—"
  menu(side="middle"):
    extend ""
    "Come inside":
      show isabelle locker_cum_inside
      isabelle locker_cum_inside "Oh, my god!" with vpunch
      "Her pussy grips me hard as we come in unison."
      "The tight muscles milk me for every drop I'm worth."
      "It's the end of a perfect storm."
    "Pull out":
      show isabelle locker_cum_outside2 with dissolve2
      "There's a special place this load going..."
      window hide
      show isabelle locker_cum_outside1 with vpunch
      show isabelle locker_cum_outside2 with Dissolve(.25)
      window auto
      maxine "That's wet..."
      mc "Does it taste good?"
      isabelle locker_cum_outside2 "Oh, my god! I can't believe you did that!"
      mc "She deserves it."
      isabelle locker_cum_outside2 "You're absolutely right."
      menu(side="middle"):
        extend ""
        "\"We should let her soak in it\nfor a few hours.\"":
          mc "We should let her soak in it for a few hours."
          isabelle locker_cum_outside2 "I'm inclined to keep her in there over the weekend, at least!"
          mc "As long as I get to fuck you on top of her again..."
        "\"Your turn?\"":
          mc "Your turn?"
          isabelle locker_cum_outside2 "I have another idea..."
          mc "Oh, yeah?"
          isabelle locker_cum_outside2 "But maybe you should look away."
          mc "Why would I do that?"
          mc "As far as I'm concerned, she deserves it all."
          isabelle locker_cum_outside2 "Bloody right, she does!"
          isabelle locker_cum_outside2 "It's because of people like you that my sister is dead."
          isabelle locker_pee1 "I've been wanting to do this for years..."
          window hide
          show isabelle locker_pee2 with Dissolve(.5)
          window auto
          maxine "Pfft!"
          isabelle locker_pee2 "Yeah? Drink it up!"
          "Oh, wow! I can't believe [isabelle] did that!"
          "Is this really the girl that stands for justice and what's right?"
          "This has gone way further than I ever thought it would..."
          "I can't say I hate it... but I'm also not sure I like it."
  scene location with fadehold
  return
