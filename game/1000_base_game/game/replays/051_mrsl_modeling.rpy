init python:
  register_replay("mrsl_modeling","Mrs. L's Modeling","replays mrsl_modeling","replay_mrsl_modeling")

label replay_mrsl_modeling:
  show mrsl painting_date_lindsey_smile with fadehold
  lindsey "Okay, I guess it was worth being picky. This is a great spot!"
  "She was starting to get annoyed, so I had to settle here..."
  mc "I told you so."
  lindsey "I'm sorry I ever doubted you!"
  window hide
  show mrsl painting_date_smile_lindsey_smile with Dissolve(.5)
  window auto
  mrsl painting_date_smile_lindsey_smile "Are you guys ready to get started?"
  mc "More than ready!"
  show mrsl painting_date_smile_lindsey_worried with Dissolve(.5)
  lindsey "I guess..."
  mrsl painting_date_smile_lindsey_worried "So, what pose should I do?"
  mc "What do you think, [lindsey]?"
  lindsey "Err... I don't know..."
  mc "Maybe we should have her take a seat and spread her legs apart?"
  lindsey "N-no, let's just paint..."
  mc "But we need a good pose!"
  mc "How about having her bend over?"
  show mrsl painting_date_flirty_lindsey_skeptical with dissolve2
  lindsey worried "No! Definitely not that!"
  show mrsl painting_date_smile_lindsey_skeptical with dissolve2
  mc "Okay, okay."
  show mrsl painting_date_smile_ with Dissolve(.5)
  lindsey "I'm just going to start..."
  mc "Well, I'm not quite happy with the pose just yet."
  mc "[jacklyn] has actually taught me a few things."
  show mrsl painting_date_smile_lindsey_worried with Dissolve(.5)
  lindsey "Okay, what is it?"
  mc "[mrsl]..."
  menu(side="middle"):
    extend ""
    "\"Can you put your hands\nbehind your head?\"":
      mc "Can you put your hands behind your head?"
      window hide
      show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
      window auto
      mrsl painting_date_arms_behind_head_lindsey_worried "Like this?"
      mc "Oh, yeah. I think that's a good pose."
      show mrsl painting_date_arms_behind_head_lindsey_blush with Dissolve(.5)
      lindsey "Yes, this is good."
      mc "You're doing great, [mrsl]. Standing so perfectly still."
      mrsl painting_date_arms_behind_head_lindsey_blush "Thank you... I care deeply about the progress of my students."
      mc "Can you show me some more of that passion you have for your students?"
      show mrsl painting_date_arms_behind_head_lindsey_worried with dissolve2
      mrsl painting_date_arms_behind_head_lindsey_worried "Trying to capture the whole essence of your object?"
      "The object of my desires..."
      mc "Something like that."
      mrsl painting_date_arms_behind_head_lindsey_worried "Mhmmm..."
    "\"Can you turn around slightly?\"":
      mc "Can you turn around slightly?"
      mrsl painting_date_smile_lindsey_worried "Certainly."
      window hide
      show mrsl painting_date_standing_sideways_lindsey_worried with Dissolve(.5)
      window auto
      "She gives me a playful little smile, then does as she's told."
      "[lindsey] averts her eyes as [mrsl] slides her right hand along her naked body..."
      "...swirls a finger around her nipple briefly..."
      "...before continuing down along her stomach, circling her belly button.{space=-45}"
      "Then, she stops just above her pussy."
      show mrsl painting_date_standing_sideways_lindsey_skeptical with dissolve2
      lindsey "Hands on your head! No more moving about!" with vpunch
      show mrsl painting_date_sad_lindsey_skeptical with dissolve2
      "A sudden panicked order from [lindsey] makes [mrsl] freeze\nmid-motion."
      window hide
      show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
      window auto
      mrsl painting_date_arms_behind_head_lindsey_worried "As you wish..."
      "But then, she blinks away the surprise and submissively assumes the position."
      "There's something highly erotic about that short exchange."
      "Something old and backwards..."
      "Like the church punishing the sinners."
      "Old, young. Naked, clothed. Sinner, saint."
      "A smirk plays on [mrsl]'s lips. She's turned on."
      "And [lindsey]... well, she tries to block it all out."
    "\"Can you lift your leg up a bit?\"":
      mc "Can you lift your leg up a bit?"
      show mrsl painting_date_flirty_lindsey_skeptical with Dissolve(.5)
      lindsey concerned "[mc]..."
      window hide
      show mrsl painting_date_right_leg_up_lindsey_embarrassed with Dissolve(.5)
      window auto
      "With uncanny grace, [mrsl] throws her leg up ballerina-style, exposing her bare pussy."
      "She cocks her head to the side, eyes bright and questioning."
      mc "Yes... just like that..."
      lindsey horrified "[mc]!"
      mc "Don't worry. It's fine."
      "I hold my paintbrush up and measure her shapely proportions, then give a nod of approval."
      mrsl painting_date_right_leg_up_lindsey_embarrassed "How... intrepid of you, [mc]."
      mc "All in the name of art."
      mrsl painting_date_right_leg_up_lindsey_embarrassed "Mhmmm, of course."
      lindsey "Oh, my god... I don't know about any of this..."
      "[lindsey]'s fidgety embarrassment is kind of cute, but my eyes are on [mrsl]."
      "In my mind, she spreads her pussy with her fingers, pushing the lips apart and exposing her dripping pink flower."
      "Goddamn, this is why art exists in the mind of the painter! How else can one possibly capture the beauty of such a thing?"
      show mrsl painting_date_right_leg_up_lindsey_worried with Dissolve(.5)
      lindsey "Can we try a different pose, please?"
      mc "Fine, fine, fine."
      window hide
      show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
      window auto
      mrsl painting_date_arms_behind_head_lindsey_worried "How do you like this one, [lindsey]?"
      show mrsl painting_date_arms_behind_head_lindsey_blush with Dissolve(.5)
      lindsey "This is better."
  if renpy.showing("mrsl painting_date_arms_behind_head_lindsey_blush"):
    mrsl painting_date_arms_behind_head_lindsey_blush "What about you, [mc]? Are you getting what you need?"
    "I'm getting what I need and then some..."
    mc "You're such a cooperative model, [mrsl]."
    mrsl painting_date_arms_behind_head_lindsey_blush "All in the name of good grades, isn't that right?"
    mc "I take my education very seriously."
    mrsl painting_date_arms_behind_head_lindsey_blush "Oh, I know you do."
    show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
  else:
    mrsl painting_date_arms_behind_head_lindsey_worried "How's that? Are you getting what you need?"
    "I'm getting what I need and then some..."
    mc "You're such a cooperative model, [mrsl]."
    mrsl painting_date_arms_behind_head_lindsey_worried "All in the name of good grades, isn't that right?"
    mc "I take my education very seriously."
    mrsl painting_date_arms_behind_head_lindsey_worried "Oh, I know you do."
  lindsey "Are you sure this is... appropriate?"
  mc "Why not? It's art. Just paint her like the French."
  lindsey "Okay..."
  show mrsl painting_date_arms_behind_head with dissolve2
  "We paint in silence for a while — [lindsey] red as a tomato, at first, but then getting more into it."
  show mrsl painting_date_arms_behind_head_canvas_head with dissolve2
  mc "That's perfect, [mrsl]. Stay just like that."
  mrsl "As you wish... you are the artist, after all."
  show mrsl painting_date_arms_behind_head_canvas_arms with dissolve2
  mc "Exactly right..."
  "Once again, I take her measure with my paintbrush."
  show mrsl painting_date_arms_behind_head_canvas_torso with dissolve2
  "I hold it up in front of my face, squint my eyes, and focus on getting her perfect proportions just right."
  "Her firm, round ass... her legs spread so wide her pussy glistens in the sun... her lips swollen and wet..."
  show mrsl painting_date_arms_behind_head_canvas_full with dissolve2
  "The imitation on the canvas can hardly do justice to what's in front of me."
  show mrsl painting_date_arms_behind_head_canvas_full_lindsey_blush with Dissolve(.5)
  lindsey "I think I'm done..."
  mc "It looks great!"
  window hide
  show mrsl painting_date_smile_canvas_full_lindsey_blush with Dissolve(.5)
  window auto
  mrsl painting_date_smile_canvas_full_lindsey_blush "You two are quite the artists. I must say I'm very impressed."
  mc "Thank you!"
  lindsey "Thanks..."
  mrsl painting_date_flirty_canvas_full_lindsey_blush "Let me know if I can be of further use to your... academic careers... in the future."
  mc "Oh, we will."
  scene location with fadehold
  return
