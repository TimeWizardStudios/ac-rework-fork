image spaghetti_small = LiveComposite((60,60), (-10,-10),Transform("items spaghetti", zoom=0.85))
image sandwich_small = LiveComposite((60,60), (-10,-10),Transform("items sandwich", zoom=0.85))
image grapes_small = LiveComposite((60,60), (-10,-10),Transform("items grapes", zoom=0.85))
image fish_and_chips_small = LiveComposite((60,60), (-10,-10),Transform("items fish_and_chips", zoom=0.85))
image shrimp_and_lobster_small = LiveComposite((60,60), (-10,-10),Transform("items shrimp_and_lobster", zoom=0.85))
image hotdog_small = LiveComposite((60,60), (-10,-10),Transform("items hotdog", zoom=0.85))


init python:
  register_replay("isabelle_night","Isabelle's Night","replays isabelle_night","replay_isabelle_night")

label replay_isabelle_night:
  show isabelle stargazing_bonfire smile with fadehold
  "As soon as she's beside me, all of my nerves seem to steady."
  "And I am left with just a feeling of calm and contentment."
  "[isabelle]'s presence is more warming than the fire."
  # "With that subtle vanilla smell, her beautiful smile, and kind eyes, so full of wonderment that someone would do something like this for her."
  "With that subtle vanilla smell, her beautiful smile, and kind eyes, so full{space=-65}\nof wonderment that someone would do something like this for her."
  "Except it's not even much. I would do this and more."
  mc "I don't get outside as much as I should, but this really is nice."
  mc "And we have some time before the meteor shower gets good."
  mc "..."
  mc "What, err... what do you think of this?"
  isabelle stargazing_bonfire smile "I think it's lovely."
  mc "Were you surprised?"
  isabelle stargazing_bonfire smile "Utterly gobsmacked."
  mc "Smacked by god? Sounds pretty good."
  isabelle stargazing_bonfire laughing "Haha! Close enough."
  show isabelle stargazing_bonfire blush with dissolve2
  isabelle stargazing_bonfire blush "And it is good. Thanks for doing this, [mc]."
  isabelle stargazing_bonfire blush "Nobody has ever done something like this for me before."
  mc "Have you dated much?"
  isabelle stargazing_bonfire thinking "Not really."
  isabelle stargazing_bonfire thinking "It was always kind of difficult, moving around so much."
  isabelle stargazing_bonfire thinking "It was hard to make significant connections."
  isabelle stargazing_bonfire thinking "And when I tried, it seems I had a habit of choosing total wankers."
  mc "Ah."
  "I need to make sure I'm not the next wanker in line..."
  isabelle stargazing_bonfire blush "But luckily, I seem to have finally found a decent guy."
  menu(side="middle"):
    extend ""
    "\"Who is he?! I'll kill him!\"":
      mc "Who is he?! I'll kill him!"
      isabelle stargazing_bonfire laughing "Very funny."
      show isabelle stargazing_bonfire smile with dissolve2
      isabelle stargazing_bonfire smile "I don't think you have to worry about much competition..."
      mc "Excellent. I Stockholmed you first."
      isabelle stargazing_bonfire laughing "Is that what you've done? Brilliant!"
      mc "Heh. Thank you. I try."
    "\"Thanks for saying that. I try to be someone who could deserve you.\"":
      mc "Thanks for saying that. I try to be someone who could deserve you."
      isabelle stargazing_bonfire smile "I don't think you give yourself enough credit, [mc]."
      isabelle stargazing_bonfire smile "You're a good guy, and I'm glad to have met you."
      isabelle stargazing_bonfire smile "Besides, it's not about deserving."
      isabelle stargazing_bonfire smile "I want to be with you, and you want to be with me."
      isabelle stargazing_bonfire smile "And that's all that matters."
      mc "I suppose you're right."
      # mc "I wasn't always the best person, but I realized I need to do some things differently."
      mc "I wasn't always the best person, but I realized I need to do some things{space=-60}\ndifferently."
      mc "I'm glad it's working... and I'm glad to have met you, too."
      isabelle stargazing_bonfire blush "I'm really happy to hear that, [mc]."
      # "Maybe [isabelle] being in my life this time around is positive karma from the universe or something?"
      "Maybe [isabelle] being in my life this time around is positive karma from{space=-45}\nthe universe or something?"
      "A driving force to make me and the world a better place."
      "..."
      "Or at the very least, {i}my{/} world."
      "And that's enough for me."
    "\"It's a good thing you came to Newfall.\"":
      mc "It's a good thing you came to Newfall."
      isabelle stargazing_bonfire smile "And why is that?"
      mc "Superior menfolk."
      # isabelle stargazing_bonfire thinking "I think people in general can be pretty shitty no matter where you go."
      isabelle stargazing_bonfire thinking "I think people in general can be pretty shitty no matter where you go.{space=-20}"
      mc "True..."
      mc "That's why we say fuck the world."
      mc "It's you and me, and that's all that matters."
      isabelle stargazing_bonfire flirty "Is that so?"
      mc "Like Bonnie and Clyde, baby."
      isabelle stargazing_bonfire flirty "You do know they died, right?"
      mc "Well, we're not criminals, yet..."
      mc "...and our passion won't die!"
      isabelle stargazing_bonfire flirty "Smooth!"
      mc "Heh. Thanks."
  mc "So, anyway, how about some dinner?"
  isabelle stargazing_bonfire smile "Sounds delightful."
  isabelle stargazing_bonfire smile "What are we having?"
  menu(side="middle"):
    "{image=spaghetti_small}|Spaghetti":
      mc "My specialty."
      window hide
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["spaghetti"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["spaghetti"].title()+"{/}", 0.25)
      show isabelle stargazing_bonfire surprised spaghetti with dissolve2
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["spaghetti"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["spaghetti"].title()+"{/}", 4.75, False)
      pause 0.25
      window auto
      isabelle stargazing_bonfire surprised spaghetti "Oh, wow! It looks tasty!"
      isabelle stargazing_bonfire surprised spaghetti "I am impressed, [mc]! I didn't realize you could cook!"
      mc "Heh. Well, maybe you should try it first?"
      isabelle stargazing_bonfire laughing spaghetti "Fair dos."
      show isabelle stargazing_bonfire thinking spaghetti with dissolve2
      isabelle stargazing_bonfire thinking spaghetti "..."
      isabelle stargazing_bonfire smile spaghetti "Wow."
      mc "Good wow?"
      isabelle stargazing_bonfire flirty spaghetti "Amazing wow!"
      mc "Seriously?"
      isabelle stargazing_bonfire flirty spaghetti "What did you put in this?"
      mc "I actually don't remember. I would need to have a taste myself."
      isabelle stargazing_bonfire flirty spaghetti "You—"
      window hide
      show isabelle stargazing_spaghetti taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      pause 0.25
      show isabelle stargazing_spaghetti kiss with Dissolve(.5)
      pause 0.25
      window auto
      "It's a spur of the moment decision, but thankfully, [isabelle] doesn't break our noodly connection."
      "Instead, she goes all in with me and we slurp our way to the middle of the noodle together, until our lips meet."
      "We kiss gently, until she breaks away, laughing."
      window hide
      show isabelle stargazing_bonfire laughing
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      isabelle stargazing_bonfire laughing "Oh, my god! I did not expect that!"
      # mc "Heh, sorry. I know it's cheesy, but you were just too cute with the noodle hanging out of your mouth like that."
      mc "Heh, sorry. I know it's cheesy, but you were just too cute with the noodle{space=-90}\nhanging out of your mouth like that."
      isabelle stargazing_bonfire blush "You continue to surprise me, [mc]."
      isabelle stargazing_bonfire blush "I really like it."
    "{image=sandwich_small}|Sandwich":
      mc "My specialty."
      window hide
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["sandwich"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["sandwich"].title()+"{/}", 0.25)
      show isabelle stargazing_bonfire laughing sandwich with dissolve2
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["sandwich"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["sandwich"].title()+"{/}", 4.75, False)
      pause 0.25
      window auto
      mc "Your dinner, my British crumpet."
      isabelle stargazing_bonfire laughing sandwich "Haha! Thank you!"
      show isabelle stargazing_bonfire smile sandwich with dissolve2
      isabelle stargazing_bonfire smile sandwich "Sandwiches, eh? How very practical."
      mc "Practical is my middle name."
      isabelle stargazing_bonfire thinking sandwich "..."
      isabelle stargazing_bonfire blush sandwich "Oh, my goodness."
      mc "Do you like it?"
      # isabelle stargazing_bonfire blush sandwich "Do I like it? This is the first sandwich I've had since moving here that's actually good!"
      isabelle stargazing_bonfire blush sandwich "Do I like it? This is the first sandwich I've had since moving here that's{space=-50}\nactually good!"
      isabelle stargazing_bonfire blush sandwich "Is that mayonnaise, even?"
      mc "Indeed, it is."
      isabelle stargazing_bonfire blush sandwich "The idea has always disgusted me... but this... this just works..."
      isabelle stargazing_bonfire blush sandwich "Mmm..."
      # mc "Finally, I have shown you and converted you to the tasty Newfall ways!"
      mc "Finally, I have shown you and converted you to the tasty Newfall ways!{space=-55}"
      isabelle stargazing_bonfire laughing sandwich "Hah! Don't get too far ahead of yourself!"
      show isabelle stargazing_bonfire smile sandwich with dissolve2
      isabelle stargazing_bonfire smile sandwich "But this? Keep it up and I'll be sticking around."
      mc "Excellent! My plan is working! In through the stomach!"
      mc "...and maybe later in through somewhere else..."
      isabelle stargazing_bonfire laughing sandwich "[mc]!"
      window hide
      pause 0.125
      show isabelle stargazing_bonfire blush with Dissolve(.5)
      pause 0.25
      window auto
    "{image=grapes_small}|Grapes":
      mc "My specialty."
      window hide
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["grapes"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["grapes"].title()+"{/}", 5.0)
      pause 0.25
      window auto
      mc "Something light and simple for us before the show begins."
      isabelle stargazing_bonfire smile "Interesting choice."
      mc "I am the interesting choice, baby."
      isabelle stargazing_bonfire laughing "Hah! Very true."
      mc "Besides, I thought I could feed you these grapes?"
      isabelle stargazing_bonfire flirty "Oh, really? Is that what you thought?"
      mc "I promise I'll be gentle..."
      isabelle stargazing_bonfire flirty "Very well. Go on, then."
      "Hell yeah. Sexy, sensual, tasty."
      "I knew this was a good move."
      mc "As you wish."
      window hide
      show isabelle stargazing_grapes feeding
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      # "When I decided on this, I had no idea it could actually be so intimate."
      "When I decided on this, I had no idea it could actually be so intimate.{space=-15}"
      "But the way each round grape slowly goes past her mouth..."
      # "...her eyes on mine while she accepts the fruit from my own fingers..."
      "...her eyes on mine while she accepts the fruit from my own fingers...{space=-20}"
      "...it does something to me."
      window hide
      pause 0.125
      show isabelle stargazing_grapes caressing with Dissolve(.5)
      pause 0.25
      window auto
      "As I touch her soft, sensitive lips, a shiver seems to tremble silently through her."
      # "In return, the look she gives me sends a swarm of butterflies fluttering through my chest."
      "In return, the look she gives me sends a swarm of butterflies fluttering{space=-60}\nthrough my chest."
      isabelle stargazing_grapes caressing "Mmm..."
      mc "Good?"
      "I'm surprised my voice sounds so calm to my own ears."
      isabelle stargazing_grapes caressing "Very...."
      window hide
      pause 0.125
      show isabelle stargazing_grapes licking with Dissolve(.5)
      pause 0.25
      window auto
      "She giggles softly and licks the juice from her lips."
      "I better stop this now before I can't control myself..."
      "She has no idea how easily she undoes me."
      window hide
      show isabelle stargazing_bonfire blush
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
    "{image=fish_and_chips_small}|Fish and Chips":
      mc "I ordered out for us tonight."
      isabelle stargazing_bonfire flirty "You did? Oh, wow!"
      isabelle stargazing_bonfire flirty "Pulling out all the stops, aren't you?"
      mc "Only the best for you."
      # mc "And I thought it would be extra nice to bring something to give you a taste of home."
      mc "And I thought it would be extra nice to bring something to give you a{space=-15}\ntaste of home."
      window hide
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["fish_and_chips"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["fish_and_chips"].title()+"{/}", 5.0)
      pause 0.25
      window auto show
      show isabelle stargazing_bonfire surprised with dissolve2
      isabelle stargazing_bonfire surprised "Oh! That's so thoughtful of you, [mc]..."
      isabelle stargazing_bonfire surprised "Thank you. Truly."
      mc "My pleasure."
      mc "Plus, it sounded good, too."
      isabelle stargazing_bonfire thinking "..."
      isabelle stargazing_bonfire blush "Mmm! It really is the best."
      isabelle stargazing_bonfire blush "And so are you."
    "{image=shrimp_and_lobster_small}|Shrimp and Lobster":
      mc "I ordered out for us tonight."
      isabelle stargazing_bonfire flirty "You did? Oh, wow!"
      isabelle stargazing_bonfire flirty "Pulling out all the stops, aren't you?"
      mc "Only the best for you."
      mc "And I wanted you to have the very best."
      mc "So, please enjoy this shrimp and lobster straight from the sea."
      window hide
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["shrimp_and_lobster"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["shrimp_and_lobster"].title()+"{/}", 5.0)
      pause 0.25
      window auto show
      show isabelle stargazing_bonfire surprised with dissolve2
      isabelle stargazing_bonfire surprised "Wow! [mc], you shouldn't have!"
      isabelle stargazing_bonfire surprised "I feel so spoiled right now..."
      mc "Tonight is your night. Away from all the shit and worry."
      mc "So, I want you to extra enjoy it."
      isabelle stargazing_bonfire blush "So far, I definitely am."
      mc "Good. Then, dig in, baby."
      isabelle stargazing_bonfire blush "Mmm! Delicious!"
    "{image=hotdog_small}|Hotdog":
      mc "I ordered out for us tonight."
      isabelle stargazing_bonfire flirty "You did? Oh, wow!"
      isabelle stargazing_bonfire flirty "Pulling out all the stops, aren't you?"
      mc "Only the best for you."
      mc "But I still wanted to keep it simple. So, I went with some hotdogs."
      window hide
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["hotdog"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["hotdog"].title()+"{/}", 5.0)
      pause 0.25
      window auto show
      show isabelle stargazing_bonfire surprised with dissolve2
      isabelle stargazing_bonfire surprised "Oh."
      "Crap. She looks kind of disappointed."
      "Maybe hotdogs weren't the best choice for a romantic date beneath the stars, after all..."
      isabelle stargazing_bonfire smile "Thank you, anyway."
      mc "Err, of course."
      isabelle stargazing_bonfire smile "And thank you for setting this up."
      mc "No worries..."
  if renpy.showing("isabelle stargazing_bonfire blush"):
    mc "..."
    isabelle stargazing_bonfire blush "..."
  else:
    show isabelle stargazing_bonfire blush with dissolve2
    isabelle stargazing_bonfire blush "..."
    mc "..."
  "We sit together in silence for a little bit as we finish our food and stare up at the vast expanse of black sky."
  "Luckily for us, the moon is waning and the weather is decent."
  "So far, tonight could hardly have gone better."
  "And when I look over at [isabelle] and see the soft smile playing on her lips, it makes me feel like oxygen is filling my lungs for the very first time."
  # isabelle stargazing_bonfire blush "You know, there was a time when I was little when I wanted to travel to space."
  isabelle stargazing_bonfire blush "You know, there was a time when I was little when I wanted to travel{space=-5}\nto space."
  # isabelle stargazing_bonfire blush "There's something so awe-inspiring about the unknown and discovery."
  isabelle stargazing_bonfire blush "There's something so awe-inspiring about the unknown and discovery.{space=-50}"
  # isabelle stargazing_bonfire blush "And I had already been all over the country and Europe by that point."
  isabelle stargazing_bonfire blush "And I had already been all over the country and Europe by that point.{space=-20}"
  isabelle stargazing_bonfire blush "So, I thought, why not make space my last frontier?"
  mc "Heh, that's kind of cute. I had no idea you were a space nerd."
  isabelle stargazing_bonfire laughing "Hah! Well, I wasn't the best at science. Decent, but not the best."
  # isabelle stargazing_bonfire laughing "Plus, I then discovered my love of literature, and kept my nose more firmly in books."
  isabelle stargazing_bonfire laughing "Plus, I then discovered my love of literature, and kept my nose more{space=-10}\nfirmly in books."
  mc "That's adorable."
  mc "What about your sister? What kind of things did she like?"
  isabelle stargazing_bonfire sad "..."
  "Her smile flickers and a stone of guilt pits in my stomach."
  mc "Err, sorry. We don't have to talk about her if you don't want to."
  isabelle stargazing_bonfire sad "No, no. It's okay."
  isabelle stargazing_bonfire sad "It is difficult, but talking about her helps keep her alive."
  isabelle stargazing_bonfire sad "And it's sweet of you to ask."
  mc "I know how important she was to you."
  isabelle stargazing_bonfire sad "Yes, she was."
  isabelle stargazing_bonfire sad "She liked video games, drawing, and music."
  isabelle stargazing_bonfire sad "So many obscure bands I had never heard of..."
  isabelle stargazing_bonfire sad "She was kind of quiet and shy. Basically, the exact opposite of me."
  isabelle stargazing_bonfire sad "But it worked for us."
  isabelle stargazing_bonfire sad "..."
  isabelle stargazing_bonfire blush "She would have loved something like this, too."
  mc "I bet she would."
  mc "But if there is something more after life... well, maybe she's looking over us and enjoying it just the same?"
  isabelle stargazing_bonfire blush "That's a nice thought."
  mc "I'm sure all she would want for you is to be happy."
  isabelle stargazing_bonfire blush "And I'm trying to find a little bit of that every day."
  # isabelle stargazing_bonfire blush "It varies day to day, of course, but if you hold on to the little moments of contentment or joy, they do tend to add up and make life a little better."
  isabelle stargazing_bonfire blush "It varies day to day, of course, but if you hold on to the little moments of{space=-95}\ncontentment or joy, they do tend to add up and make life a little better.{space=-60}"
  mc "That's a nice way of looking at it. Just focusing on the positive."
  isabelle stargazing_bonfire blush "Exactly..."
  isabelle stargazing_bonfire blush "That's why tonight was a much needed reset, I think."
  isabelle stargazing_bonfire blush "And a nice one at that."
  mc "Right. I'm really happy we did this."
  isabelle stargazing_bonfire blush "Me too."
  mc "Should we enjoy the show, then?"
  isabelle stargazing_bonfire blush "Let's do it!"
  window hide
  show isabelle stargazing_embrace
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "With my arm around her, we look into the telescope, ready for a dazzling display."
  # "As it gets later, the night continues to get colder, but with the fire to our backs we manage to stay pretty warm while we look up toward the infinite sky."
  "As it gets later, the night continues to get colder, but with the fire to{space=-5}\nour backs we manage to stay pretty warm while we look up toward the infinite sky."
  "There's also a different kind of warmth."
  # "Not in the crackle and pop of flames jumping in the air, but a quieter, hotter kind building up within me."
  "Not in the crackle and pop of flames jumping in the air, but a quieter,{space=-15}\nhotter kind building up within me."
  # "Just standing next to [isabelle] like this, the two of us out here marveling at the vast expanse of the universe and our small place in it."
  "Just standing next to [isabelle] like this, the two of us out here marveling{space=-75}\nat the vast expanse of the universe and our small place in it."
  # "It's like a comforting embrace heating up through my belly and chest."
  "It's like a comforting embrace heating up through my belly and chest.{space=-30}"
  "Until the mere presence of her makes my cheeks burn."
  "I'm not sure what I did to deserve her in this universe or timeline or whatever this is..."
  "...but instead of feeling small beneath the vast ocean of stars, I feel bigger and more seen than I ever have before."
  "[isabelle] sees the real me and doesn't shrink from it."
  "She stands out in the dark void with me, and helps me pick out the bright spots as they streak across a blackened sky."
  "And it reminds me of what she just said. About finding the good in each day."
  "The streaks of light in the darkness."
  "She's one of those bright, burning lights in my sky."
  isabelle stargazing_embrace "Ooooh!"
  "Every time a meteor streaks by, she gasps and laughs."
  isabelle stargazing_embrace "There's one!"
  mc "I see it!"
  "And each time she does it, I can't help but grin."
  "We both know it's coming, but it still fills her with such wonder."
  isabelle stargazing_embrace "That one was really bright!"
  mc "It was, wasn't it?"
  "Yet nothing can really compare to the brightness of her."
  "She knows exactly who she is, and she's not afraid to burn hot."
  "She doesn't care what anyone thinks if she believes she's right."
  "Looking at her, with her face pressed to the telescope, it feels as if the stars are shooting through my veins."
  # "[isabelle] burns me up like the sun, and I'm struck by how easily I could be consumed by her gravitational pull."
  "[isabelle] burns me up like the sun, and I'm struck by how easily I could{space=-40}\nbe consumed by her gravitational pull."
  scene location with fadehold
  return
