init python:
  register_replay("jacklyn_kiss","Jacklyn's Kiss","replays jacklyn_kiss","replay_jacklyn_kiss")

label replay_jacklyn_kiss:
  show jacklyn marina_kiss kiss with fadehold:
    xalign 0.0
  # "Thrumming with the confidence the night has given me, my feet take me closer to [jacklyn]."
  "Thrumming with the confidence the night has given me, my feet take{space=-15}\nme closer to [jacklyn]."
  "My blood rushes hot and heavy and my heart feels ready to burst."
  "My hands find her face, and my mouth finds her lips."
  "Her mouth twists in a little half-smile beneath mine."
  "For a moment, I worry I've overstepped, but then she leans into it."
  "Her lips, warm and soft, explore without reserve."
  "She bites my bottom lip, and a quiet gasp escapes my throat."
  "Her tongue suddenly touches mine, plays with it."
  "It's a hungry, passionate kiss. One that could last forever."
  if quest.jacklyn_romance["flora_sex"]:
    "But something behind her catches my eye..."
    window hide
    show jacklyn marina_kiss kiss:
      ease 3.0 xalign 1.0
    pause 3.0
    hide jacklyn
    show jacklyn marina_kiss kiss:
      xalign 1.0
    window auto
    "Fuck."
    # "Never once did I think my life, once so lonely and seemingly hopeless, could become so complicated."
    "Never once did I think my life, once so lonely and seemingly hopeless,{space=-40}\ncould become so complicated."
    # "Never did I think that the dream of making out with a girl like [jacklyn] could become real..."
    "Never did I think that the dream of making out with a girl like [jacklyn]{space=-15}\ncould become real..."
    "...or that doing so would risk my relationship with [flora], and send my heart into a panicked stumble."
    "Life is all about choices. Some are harder than others."
    "Especially when you can't see the road ahead."
    "All you can do is make a choice, hoping it's the right one."
    jacklyn marina_kiss kiss "[mc]?"
    window hide
    show jacklyn marina_kiss stare:
      xalign 1.0
      ease 3.0 xalign 0.0
    pause 3.0
    hide jacklyn
    show jacklyn marina_kiss stare:
      xalign 0.0
    window auto
    mc "Huh?"
    jacklyn marina_kiss stare "You prada, spaceman?"
    mc "I, err... I don't know..."
    mc "[flora]—"
    jacklyn marina_kiss stare "It's [jacklyn]."
    mc "No, I mean she's—"
    window hide
    show jacklyn marina_kiss floraless:
      xalign 0.0
      ease 3.0 xalign 1.0
    pause 3.0
    hide jacklyn
    show jacklyn marina_kiss floraless:
      xalign 1.0
    window auto
    "...gone."
  else:
    # "Never did I think that the dream of making out with a girl like [jacklyn] could become real..."
    "Never did I think that the dream of making out with a girl like [jacklyn]{space=-15}\ncould become real..."
  scene location with fadehold
  return
