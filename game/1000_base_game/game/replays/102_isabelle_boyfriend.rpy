init python:
  register_replay("isabelle_boyfriend","Isabelle's Boyfriend","replays isabelle_boyfriend","replay_isabelle_boyfriend")

label replay_isabelle_boyfriend:
  show isabelle stargazing_kiss with fadehold: ## This gives the image both the fadehold (transition) and hpunch (transform) effects
    block:
      linear 0.05 xoffset 15
      linear 0.05 xoffset -15
      repeat 18
    linear 0.05 xoffset 0
  hide isabelle
  show isabelle stargazing_kiss ## This is just to avoid the screen shake if the player is skipping
  "She nearly falls off balance when I pull her close."
  "Her waist fits perfectly in my hands, and her cloudlike, vanilla lips fit even better on mine."
  "It's a slow and sensual kiss."
  "As I deepen the kiss and tease her tongue with my own, she melts into me a little more."
  # "The taste of her, the feel of her small, yet full frame crushed beneath my hands, elicits a primal growl from deep within my throat."
  "The taste of her, the feel of her small, yet full frame crushed beneath{space=-25}\nmy hands, elicits a primal growl from deep within my throat."
  "It makes [isabelle] gasp into my mouth, and I can feel her heart hitch against my own chest."
  window hide
  hide screen interface_hider
  show expression LiveComposite((1920,1080), (-469,-56),"school park sky_night", (0,275),"school park back_grass_autumn", (155,106),"school park back_trees_autumn", (0,514),"school park grass_autumn", (1218,64),"school park kite", (528,543),"school park no_swimming_sign", (493,233),"school park left_tree_autumn", (746,602),"school park left_bench", (0,704),"school park pond", (964,578),"school park back_leaves_autumn", (1532,459),"school park telescope", (1444,692),"school park blanket", (850,779),"school park left_grass_autumn", (1000,826),"school park left_bush_autumn", (1390,666),"school park right_bench", (1651,919),"school park right_bush_autumn", (0,0),"school park front_tree_autumn", (319,526),"school park butterflies", (232,0),"school park swing", (1784,583),"school park front_bush_autumn", (0,586),"school park front_leaves_autumn", (1609,970),"school park football", (0,0),"school park overlay_night", (904,921),"school park exit_arrow", (1703,568),"school park big_bonfire", (1390,666),"school park right_bench_night") as makeshift_location behind isabelle
  $isabelle["outfit_stamp"] = isabelle.outfit
  $isabelle.outfit = {"panties":"isabelle_green_panties", "bra":"isabelle_green_bra", "pants":"isabelle_skirt", "shirt":"isabelle_top", "jacket":"isabelle_jacket", "hat":"isabelle_tiara"}
  show isabelle afraid
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "But just as I'm about to slip my hands up under her shirt, she pulls back and gasps again."
  "It sounds less sexy and more frightened, and it momentarily breaks the fog quickly creeping into my brain."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "Sorry! Did I hurt you?"
  isabelle afraid "No, look! There's someone in the window!"
  mc "Err, what?"
  isabelle cringe "In the school! There!"
  mc "All right, hang on!"
  "The last thing I want to do right now is think about school, but she looks genuinely afraid..."
  window hide
  show misc window_silhoutte as window
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide makeshift_location
  hide isabelle
  $isabelle.outfit = isabelle["outfit_stamp"]
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  mc "What the hell?!"
  isabelle "I bloody told you!"
  "There was a part of me that worried [isabelle] was making it up to get out of kissing me."
  "I'm almost glad to see someone up there."
  "..."
  "But I can't quite tell who it is..."
  "If only the school could afford a better telescope."
  window hide
  pause 0.125
  show misc window as window with Dissolve(.5)
  pause 0.25
  window auto
  mc "They left."
  scene location with fadehold
  return
