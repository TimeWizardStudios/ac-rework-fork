init python:
  register_replay("lindsey_pleasure","Lindsey's Pleasure","replays lindsey_pleasure","replay_lindsey_pleasure")

label replay_lindsey_pleasure:
  show lindsey hospital_room neutral with fadehold
  "Oh, my god..."
  "I don't know what I was expecting, but damn..."
  "God, it really hurts to see her like this."
  "Reduced from an energetic athlete to this pitiful state."
  "She can barely move at all."
  lindsey hospital_room surprised "Huh? Is someone there?"
  "Hearing her voice again fills me with both sadness and relief."
  "You never truly know how important someone is before you're close to losing them."
  lindsey hospital_room surprised "Who is it?"
  mc "It's [mc]..."
  lindsey hospital_room sad "..."
  "Can you even ask how she's doing when she looks like this?"
  mc "I'm really sorry..."
  lindsey hospital_room sad "Um..."
  "What can you even say to someone who was robbed of their destiny?{space=-30}"
  mc "I figured you could use some company..."
  lindsey hospital_room skeptical "Did they put you up to this?"
  mc "What? Who?"
  lindsey hospital_room skeptical "The doctors... my parents... I don't know."
  mc "No, I just wanted to see you!"
  lindsey hospital_room sad "Even though I look like this?"
  mc "You're beautiful to me no matter what."
  mc "I... really missed you..."
  lindsey hospital_room afraid "The doctors say I may never run again..."
  mc "I'm sure they're wrong."
  lindsey hospital_room crying "My legs..."
  mc "I know. I'm so sorry."
  mc "I wish we could trade places. I really do."
  lindsey hospital_room crying "I wish I could remember what happened..."
  lindsey hospital_room crying "They say it was a... a..."
  "Her voice cracks and breaks, and my heart with it."
  "Coming here, I never imagined it would be this hard..."
  "And I can't just stand here and watch anymore."
  window hide
  show lindsey hospital_bed_angel crying
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Tears stream down her cheeks as I sit down beside her, giving her the gentlest hug I can manage."
  "She sobs softly into my shoulder, and for a moment I just hold her."
  "It's sometimes easy to forget that [lindsey] comes from a religious family."
  "The guilt must be overwhelming on top of her injuries."
  mc "I know what they said."
  mc "And I also know they're wrong."
  lindsey hospital_bed_angel surprised "What do you mean...?"
  mc "It wasn't a suicide attempt."
  lindsey hospital_bed_angel surprised "..."
  mc "Right?"
  lindsey hospital_bed_angel sad "I... I don't know..."
  lindsey hospital_bed_angel sad "I don't remember..."
  "How do you even begin to explain that a doll did this to her?"
  "It would sound crazy, and it's probably not what she needs to hear right now."
  mc "What's the last thing you remember?"
  lindsey hospital_bed_angel sad "I... don't want to think about that day..."
  menu(side="middle"):
    extend ""
    "\"That's okay. Just rest.\"":
      mc "That's okay. Just rest."
      mc "You just need to let your body heal."
      lindsey hospital_bed_angel surprised "But what's the point?"
      mc "You survived, and if that's not divine intervention, I don't know what is."
      mc "The universe or god has decided that you have life left to live."
      mc "So, the point is living it."
      lindsey hospital_bed_angel surprised "I don't know what to do if I can't run again..."
      lindsey hospital_bed_angel surprised "It was my entire life..."
      mc "It was a big part of it, true. But it wasn't everything."
      mc "You can still enjoy King's Bard, for example."
      lindsey hospital_bed_angel sad "I guess..."
      mc "You can still read spicy novels, eat good food, give hugs..."
      mc "I, for one, think you give the best hugs in the country."
      mc "Maybe there's a championship for that?"
      lindsey hospital_bed_angel closed_eyes "That's sweet..."
      "She sighs and leans her head against my shoulder."
      "It's a small gesture of appreciation, but it means a lot."
      "Maybe I've lit the spark of hope for her... or at the very least, taken away the pain for a moment."
    "\"I know it's difficult...\nbut I'm here to help.\"":
      mc "I know it's difficult... but I'm here to help."
      lindsey hospital_bed_angel surprised "Help? How?"
      lindsey hospital_bed_angel sad "I've already lost everything..."
      mc "It might feel that way right now."
      mc "In a way, I also lost my life that day."
      mc "Watching you fall, unable to stop it... I just couldn't deal with it."
      mc "My world collapsed when I saw them roll you into the ambulance."
      mc "You're stronger than you think, and by pushing through, you didn't just save yourself... you also saved me."
      mc "And I promise you, as long as I live, you have my sword and shield."
      lindsey hospital_bed_angel closed_eyes "..."
      "She doesn't say anything, but a hint of pink shade touches her pale cheeks."
  "We lie in silence for a bit, listening to each other's breathing."
  "Hers, more labored than usual."
  lindsey hospital_bed_angel sad "I can't..."
  mc "Hmm?"
  lindsey hospital_bed_angel sad "I keep seeing it..."
  lindsey hospital_bed_angel sad "As soon as I close my eyes, the abyss opens up below me..."
  lindsey hospital_bed_angel sad "I'm teetering on the edge, doing my best not to fall..."
  lindsey hospital_bed_angel sad "But gravity takes hold and my stomach drops, and I with it..."
  lindsey hospital_bed_angel sad "I want to scream, but nothing comes out..."
  lindsey hospital_bed_angel sad "I gasp for air as I fall... and fall... and fall... all the way down..."
  lindsey hospital_bed_angel sad "And then it's just darkness... I'm alone in the void... but only for a short while..."
  lindsey hospital_bed_angel sad "Pain blinds me when I wake up... each beat of my heart rips me apart..."
  lindsey hospital_bed_angel sad "My arms won't move... my legs lie crumpled like paper..."
  lindsey hospital_bed_angel sad "I can't breathe, my lungs won't fill..."
  lindsey hospital_bed_angel sad "I start to fade again..."
  lindsey hospital_bed_angel sad "But someone stands over me, radiating light... white, gold, and pink...{space=-30}"
  lindsey hospital_bed_angel sad "And then my lungs fill with blessed air, my heart beats anew..."
  lindsey hospital_bed_angel sad "And I just sleep... for so long..."
  mc "I'm really sorry..."
  lindsey hospital_bed_angel sad "I'm afraid to close my eyes... I don't know if I'll wake up again..."
  lindsey hospital_bed_angel sad "I don't know if this is all a dream..."
  lindsey hospital_bed_angel sad "What if I died that day?"
  mc "Maybe you did die for a moment, but someone was clearly watching over you."
  mc "And you're alive now."
  lindsey hospital_bed_angel surprised "Promise?"
  mc "I promise. I'll stay by your side until you wake."
  lindsey hospital_bed_angel closed_eyes "Thank you..."
  "And somehow, she trusts me enough to close her eyes despite her fears."
  show lindsey hospital_bed_angel sleeping with dissolve2
  "Her breaths go softer and softer as she drifts off, falling asleep in my arms."
  "The beeps and whirrs of the machines are an ever present soundtrack{space=-45}\nin the background."
  "Not exactly the most romantic setting..."
  "If things were different, I would have real, romantic music... maybe some candles..."
  "But at least [lindsey] is here, alive."
  "And I get to be with her, just the two of us here in the dark."
  "..."
  "I silently count each breath she takes."
  "With each one, my heart trembles with relief."
  "Every beat of her heart is precious, every moment invaluable."
  "I know that now more than ever."
  "And I don't want to take it for granted."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel sleeping hand_against_chest with Dissolve(.5)
  pause 0.125
  window auto
  "Gently, I reach out and place the palm of my hand against her chest.{space=-20}"
  "Just to feel the steady rhythm of her heart."
  "It's strong. Just like she's always been."
  lindsey hospital_bed_angel sleepy hand_against_chest "[mc]...?"
  mc "I'm sorry! Did I wake you?"
  lindsey hospital_bed_angel sleepy hand_against_chest "It's okay..."
  lindsey hospital_bed_angel sleepy hand_against_chest "Thanks for staying with me..."
  mc "They'll have to kick me out if they want me to leave."
  show lindsey hospital_bed_angel smile hand_against_chest with dissolve2
  "She giggles softly, her breath tickling my cheek like butterfly wings."
  "The sound of her laughter sends joy bubbling through me."
  lindsey hospital_bed_angel smile hand_against_chest "I like you... you're cute."
  mc "I'm serious! I will fight them."
  mc "..."
  mc "When I thought you were gone... and then when they weren't sure if you would wake up..."
  "My voice cracks as a lump of sorrow fills my throat."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel admiring fingertips_against_cheek with Dissolve(.5)
  pause 0.125
  window auto
  "[lindsey] reaches out and brushes her fingertips against my cheek."
  mc "I'm so thankful..."
  "As I say the words, my eyes meet hers."
  "So blue and pure and sincere."
  "She would never hurt a soul, and to see her broken is the worst feeling in the world."
  "A swell of affection and regret fills my chest."
  "A desire to protect her and keep her safe, no matter what."
  "I lean in close, my lips inches from hers."
  lindsey hospital_bed_angel kiss fingertips_against_cheek "Go on..."
  window hide
  hide screen interface_hider
  show lindsey hospital_bed_angel admiring fingertips_against_cheek
  show black
  show black onlayer screens zorder 100
  with close_eyes
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Our lips meet in the softest kiss imaginable."
  "Unlike the gray autumn outside, it's like spring when we touch."
  "A beam of light breaking through the dark clouds."
  "An instant flood of warmth and happiness blossoming in my veins."
  "She kisses me back slowly, her sweet, cotton candy lips giving way under mine."
  "The kiss deepens, and I'm completely lost in her."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with open_eyes
  window auto
  $set_dialog_mode("")
  "As our lips part, [lindsey] looks up at me, giving me the most heart-melting look."
  # "Through her chest, the pulse of her heart quickens against my palm."
  "Through her chest, the pulse of her heart quickens against my palm.{space=-15}"
  mc "Just relax, okay? I'll make you feel better."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel blush cupping_breast with Dissolve(.5)
  pause 0.125
  window auto
  "Slowly, my hand moves lower, cupping her breast."
  "The jiggly softness makes me dizzy with desire."
  "The squeeze is light, but she still moans."
  "Distantly, her heart rate monitor starts beeping faster."
  "The sound fills me with a certain pride and giddiness."
  "Emboldened, my hand leaves her chest, traveling down her ribcage and stomach."
  "Steadily, slowly, descending."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel blush hand_under_panties with Dissolve(.5)
  pause 0.125
  window auto
  "[lindsey] gasps as I get closer to her sex, but doesn't stop me."
  "She just looks at me, eyes full of innocence and surprise, but also desire."
  "Even as my hand slips under the waistband of her panties, her eyes remain on me."
  "The soft skin of her mound against my palm makes me shiver."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel blush fully_covered_pussy with Dissolve(.5)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.5)
  pause 0.125
  window auto
  lindsey hospital_bed_angel aroused fully_covered_pussy "[mc]..."
  "She gasps as I fully cover her pussy with my hand."
  "Her words are a mere whisper, and despite her broken limbs, she tries to spread her legs for me."
  mc "Just relax."
  "She bites her lip, her cheeks turning crimson."
  show lindsey hospital_bed_angel aroused sliding_hand_further with dissolve2
  "As I say this, I slide my hand further into her panties."
  "Caressing her vulva."
  "Gliding my fingers between her already slick lips."
  "She squirms slightly beneath me, pressing her head back into her pillow."
  lindsey hospital_bed_angel aroused sliding_hand_further "Oh... oh my..."
  "My thumb touches her clit and she stiffens, holding her breath."
  "Her pussy leaks into my hand, throbbing beneath my touch."
  lindsey hospital_bed_angel aroused sliding_hand_further "[mc]..."
  "The sound of her whispering my name spurs me on."
  show lindsey hospital_bed_angel aroused sliding_finger_inside with dissolve2
  "My fingers separate her nether lips, opening her up enough to slide a finger inside."
  "Her eyelashes flutter, her pupils dilate, and her lips produce a single moan."
  "She pushes against my hand, and I return the pressure."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.15
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.15
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.075
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.075
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.075
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.225
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.0
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.0
  show expression Crop((960,0,960,1080), Flatten("lindsey hospital_bed_angel ahegao sliding_finger_inside")) as lindsey_expression:
    alpha 0.0 xpos 960
    linear 0.5 alpha 1.0
  pause 0.0
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
  pause 0.0
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
  pause 0.0
  hide lindsey_expression
  pause 0.0
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
  pause 0.125
  window auto
  "Her head tilts back, her eyes on the ceiling, mouth open in a silent gasp."
  "It's like I'm controlling her entire body with the tips of my fingers."
  "Like a puppet master, I make her gasp and sigh, moan and shudder."
  "More pressure, deeper penetration, faster stimulation of her clitoris.{space=-5}"
  window hide
  pause 0.125
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
  pause 0.15
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
  pause 0.075
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
  pause 0.075
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
  pause 0.225
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.075
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.075
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.125
  window auto
  "She cries out, her orgasm building."
  mc "That's it, baby..."
  mc "You deserve to feel good after everything you've been through."
  mc "Let me make you feel good."
  lindsey hospital_bed_angel ahegao sliding_finger_inside "Mmmm... it really does!"
  window hide
  pause 0.125
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.0
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.0
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.15
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.075
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.15
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.0
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.0
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.0
  show expression Crop((960,0,960,1080), Flatten("lindsey hospital_bed_angel pleading sliding_finger_inside")) as lindsey_expression:
    alpha 0.0 xpos 960
    linear 0.5 alpha 1.0
  pause 0.0
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
  show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
  pause 0.075
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.0
  hide lindsey_expression
  pause 0.075
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.075
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.075
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.125
  window auto
  "Her toned stomach muscles flex and relax, flex and relax — the dam about to break."
  "Panting, she looks up at me, eyes pleading."
  "The cutest, most desperate mewls fill the hospital room."
  "She's so close, her pussy's gripping my fingers, trying to pull me in."
  "Her swollen, sensitive clitoris an endless focus of my thumb."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.075
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.075
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.075
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.0
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.0
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.0
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.0
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  pause 0.0
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
  show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
  window auto show None
  show lindsey hospital_bed_angel squirt sliding_finger_inside with dissolve2
  lindsey hospital_bed_angel squirt sliding_finger_inside "Ohhhhhh!" with hpunch
  show lindsey hospital_bed_angel puddle sliding_finger_inside with dissolve2
  "Finally, she gives in and explodes around my fingers."
  "A near silent, but oh so powerful orgasm."
  "Juices flow freely from her pussy, soaking my hand and her panties,{space=-5}\neven her bed."
  "Her heart monitor is going crazy."
  lindsey hospital_bed_angel pleased sliding_finger_inside "Oh, my god... that..."
  mc "That."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel pleased squirt_into_hand with Dissolve(.5)
  pause 0.125
  window auto
  "It's hard to keep the smile off my face..."
  "I just made [lindsey] squirt right into my hand!"
  "She relaxes back against the pillow, a smile of relief flitting across her rosy lips."
  lindsey hospital_bed_angel pleased squirt_into_hand "Amazing..."
  "And it truly is."
  "To give someone pleasure is such a beautiful thing."
  "Especially when she can't do it herself."
  "...not that she would ever admit to doing something like that,\nof course."
  window hide
  pause 0.125
  show lindsey hospital_bed_angel pleased napkin_dab with Dissolve(.5)
  show lindsey hospital_bed_angel embarrassed napkin_dab with Dissolve(.5)
  pause 0.125
  window auto
  "She looks at me in surprise and embarrassment when I grab a napkin, and gently dab her pussy."
  "She blushes deeply, but knows that she can't clean herself up."
  "And I refuse to just let her soak in her juices."
  lindsey hospital_bed_angel blush napkin_dab "Thank you..."
  mc "Don't mention it!"
  lindsey hospital_bed_angel blush napkin_dab "You can't either... no one can know..."
  mc "Don't worry about it. My lips are—"
  window hide
  play sound "open_door"
  pause 0.75
  window auto
  $nurse.default_name = "Doctor"
  nurse "What's going on in here? The visiting hours ended a long time ago."
  show lindsey hospital_bed_angel embarrassed with dissolve2
  nurse "Time to go."
  $nurse.default_name = "Nurse"
  scene location with fadehold
  return
