init python:
  register_replay("jacklyn_string","Jacklyn's Search","replays jacklyn_string","replay_jacklyn_string")

label replay_jacklyn_string:
  scene jacklyn jacklynpose01 with fade
  "Okay, that's... a good selling point for her class... not going to lie."
  "That has to be against some kind of school policy! But well, who's actually going to complain?"
  "They say that self-expression is the best art, and [jacklyn] is certainly making a point of it."
  "Sure takes confidence to wear a skirt that short. And bending over in it is like an invitation."
  "Is she the type who likes to be spanked? "
  "Hand or paddle?"
  "Hmm... she seems tough, so probably the paddle."
  "And those fishnet stockings... didn't even know I had a leg fetish until now."
  "This goes to show that not only airhead bimbos can pull off slutty."
  "It's honestly even hotter that she's highly intelligent and still chooses to dress this way."
  "Math and science used to be my forte... but her string theory seems way more alluring."
  ## restore normal scene
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
