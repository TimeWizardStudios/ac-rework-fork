init python:
  register_replay("lindsey_catch","Lindsey's Catch","replays lindsey_catch","replay_lindsey_catch")

label replay_lindsey_catch:
  $x = game.location
  $game.location = school_ground_floor
  show lindsey lindseyfall
  $renpy.transition(Dissolve(0.5))
  pause(.5)
  lindsey lindseyfall "Eeep!"
  show lindsey lindseycatch with fadehold: #this gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 18
    linear 0.05 yoffset 0
  hide lindsey
  show lindsey lindseycatch #this is just to avoid the screen shake if the player is skipping
  lindsey lindseycatch "Oof!"
  mc "I got you."
  "That was too close."
  lindsey lindseycatch_look "[mc]? Where did you come from?"
  menu(side="far_left"):
    extend ""
    "\"I've been here all along.\"":
      mc "I've been here all along."
      mc "Maybe if you slowed down you'd notice the people around you."
      lindsey lindseycatch_look "I'm late for practice... thanks for catching me, though!"
      mc "You're going to hurt yourself if you keep being careless."
      lindsey lindseycatch_smile "I slipped on the stairs, that's all! I'll watch my step in the future."
      mc "This is the third time you've been in an accident this year."
      mc "Are you sure you're okay?"
      lindsey lindseycatch_smile "Never been better!"
    "\"Where did {i}you{/} come from?\"":
      mc "Where did {i}you{/} come from?"
      mc "I was just walking along and you fell out of the sky."
      mc "Are you a bird? A plane? Maybe an angel?"
      lindsey lindseycatch_smile "Hey, smooth!"
      mc "I thought you were a runner. What's with the flight maneuvers?"
      lindsey lindseycatch_smile "My lifelong dream is actually to become a pilot."
      mc "Really?"
      lindsey lindseycatch_smile "No, silly! I just slipped."
      lindsey lindseycatch_smile "Thanks for catching me."
      mc "That's it? I just saved your life."
      lindsey lindseycatch_look "..."
      mc "I'm kidding. Glad you're okay."
      lindsey lindseycatch_smile "I thought I'd eat it for sure."
      mc "Me too. I thought for sure that—"
      show lindsey lindseycatch_hug with Dissolve(.5)
      "Oh."
      "Okay, then..."
      "..."
      "God, she's so warm."
      "I used to dream about hugging her, but even in my fantasy, she never hugged me back."
      "Never thought it would be so different from [jo]'s hugs."
      "When she wraps her arms around me, it feels like she's putting everything she has into it."
      "Maybe that's unique for her, being a star athlete. Maybe she puts all her effort into everything she does."
      lindsey lindseycatch_hug "Thanks so much for catching me!"
      "So, this is what true appreciation feels like."
      "Who would've thought it'd be so soft?"
    "\"We have to stop meeting like this.\"":
      mc "We have to stop meeting like this."
      lindsey lindseycatch_smile "Oh, hello there!"
      lindsey lindseycatch_smile "I don't mind it all too much."
      mc "You could seriously have hurt yourself!"
      mc "What if I wasn't around?"
      lindsey lindseycatch_look "You're serious..."
      mc "Of course I'm serious!"
      mc "I can't stand the thought of something bad happening to you."
      lindsey lindseycatch_smile "But I'm okay!"
      mc "Yes, but—"
      lindsey lindseycatch_kiss "Mwwah!"
      "It's hard to argue with a pair of lips against your cheek."
      "Her carelessness is the main reason we're even talking."
      "It has led us to this moment. So, maybe it's wrong of me to be upset with her."
      "But she's just so precious. And I don't say that just 'cause she's kissing my cheek."
      lindsey lindseycatch_hug "Thanks for catching me."
      "Okay, maybe I don't mind it that much."
      "Not when her hair carries the scent of spring — that mild breeze that heralds the summer."
      "And her arms around me whisk away the mental anguish from years of misery and self-doubt."
      "Not when her soft chest presses into me, and our hearts beat in tandem."
      "Not when she whispers words that only I can hear."
      lindsey lindseycatch_hug "My hero!"
      "And she means it too. Not a trace of sarcasm in her voice."
      "If only we could stay like this forever."
      "If only..."
  hide lindsey
  $game.location = x
  $renpy.transition(Dissolve(0.5))
  return
