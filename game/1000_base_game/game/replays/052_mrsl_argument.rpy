init python:
  register_replay("mrsl_argument","Mrs. L's Argument","replays mrsl_argument","replay_mrsl_argument")

label replay_mrsl_argument:
  show mrsl under_desk with fadehold
  "Ugh, my knees and back hurt already."
  "The things I do in search of the truth..."
  "..."
  "..."
  "..."
  "Man, it's getting late."
  "The guard is probably going to start locking up soon..."
  window hide
  play sound "<from 2.7 to 3.3>door_breaking_in"
  pause 0.75
  play sound "<from 0.4>bedroom_door"
  pause 0.75
  window auto
  "Oh, shit! Someone's coming!"
  play sound "<to 2.2>high_heels"
  "..."
  "Wait, why are they coming over here?"
  "Are my feet sticking out or something?"
  "Fuck, they're right by the desk—"
  window hide
  show mrsl under_desk_closed_legs with Dissolve(.5)
  window auto
  "Crap, crap, crap! Not good! Not good at all!"
  "What is [mrsl] doing here?!"
  "God, her legs smell so good... like cigarettes and sex..."
  "..."
  "Wait, could it be that [mrsl] is Mer?"
  "Surely not, right?"
  "Why would she make [lindsey] steal the chocolate hearts? That makes zero sense."
  "Mer must be someone who would want to hurt [isabelle]. Nothing else really makes sense."
  window hide
  show mrsl under_desk_half_open_legs with Dissolve(.5)
  window auto
  "Whoa!"
  "You naughty bitch, [mrsl]! Where are your panties?"
  "God, her pussy is like a work of art. And that piercing... fuck!"
  "The things I would do to that pussy..."
  window hide
  play sound "<from 1.7 to 2.4>door_knock"
  pause 0.75
  play sound "<from 0.4>bedroom_door"
  pause 0.75
  show mrsl under_desk_closed_legs with Dissolve(.5)
  window auto
  mrsl under_desk_closed_legs "What do {i}you{/} want?"
  "???" "Why are you here?"
  "Huh? Who is that?"
  "I feel like I've heard her voice before."
  mrsl under_desk_closed_legs "Have you lost your last marbles, you crazy old bitch?"
  mrsl under_desk_closed_legs "I'm the homeroom teacher. This is the homeroom."
  "???" "Watch your mouth, you fiend."
  "I can't believe what I'm hearing..."
  "This is an entirely different side of [mrsl]."
  mrsl under_desk_closed_legs "You never set foot inside my classroom. Why now?"
  "???" "..."
  "???" "That does not concern you."
  "From the sound of it, this person is Mer... and they expected to find [lindsey] here, not [mrsl]."
  mrsl under_desk_closed_legs "Oh, secrets! Now, that's exciting!"
  mrsl under_desk_closed_legs "You're not breaking the rules, are you?"
  "???" "That is rich coming from you."
  mrsl under_desk_closed_legs "I don't know what you're talking about."
  window hide
  show mrsl under_desk_half_open_legs with Dissolve(.5)
  window auto
  "Oh, god."
  "Her pussy is so wet it glistens."
  "Why is she so aroused all of a sudden?"
  "Her pussy is inches away from my face... I can almost taste it..."
  menu(side="middle"):
    extend ""
    "Have a taste":
      "[mrsl] is in the middle of a serious argument with someone she hates..."
      "There is no way she would admit that there's a student under her desk, eating her pussy."
      "Right...?"
      window hide
      show mrsl under_desk_tongue6 with Dissolve(.5)
      window auto
      mrsl under_desk_tongue6 "..."
      "???" "What now?"
      mrsl under_desk_tongue6 "Oh... nothing..."
      mrsl under_desk_tongue6 "I was just... thinking about how ridiculous you look..."
      "???" "You really need to take a look in the mirror."
      "She reacted, but didn't say anything..."
      "That's practically an invitation, right?"
      window hide
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue6 with Dissolve(.05)
      window auto
      mrsl "Oooh!"
      "God, [mrsl] tastes like the seven deadly sins."
      "And she's already wetter than [flora] at the squid aquarium."
      mrsl "Mmm... society is perfectly fine with how I dress... and so is the school board..."
      if quest.lindsey_motive["mrsl_model"]:
        "???" "What about your naked escapade earlier? That must be against\nthe rules."
        mrsl "Would you really deprive these young minds... mmm... of their education in art and anatomy...?"
        "???" "Debauchery! That is what it is!"
        window hide
        show mrsl under_desk_tongue1 with Dissolve(.05)
        show mrsl under_desk_tongue2 with Dissolve(.05)
        show mrsl under_desk_tongue3 with Dissolve(.05)
        show mrsl under_desk_tongue4 with Dissolve(.05)
        show mrsl under_desk_tongue5 with Dissolve(.05)
        pause 0.1
        show mrsl under_desk_tongue1 with Dissolve(.05)
        show mrsl under_desk_tongue2 with Dissolve(.05)
        show mrsl under_desk_tongue3 with Dissolve(.05)
        show mrsl under_desk_tongue4 with Dissolve(.05)
        show mrsl under_desk_tongue5 with Dissolve(.05)
        show mrsl under_desk_tongue6 with Dissolve(.05)
        window auto
        mrsl "Mmmhmm..."
        mrsl "Well, it's part of the curriculum..."
      "???" "Outrageous."
      mrsl "No... what is outrageous... mmm... is what you've been up to lately..."
      mrsl "It doesn't take a genius to figure out... that you're stirring the pot..."
      window hide
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue6 with Dissolve(.05)
      window auto
      mrsl "{i}Gasp!{/}" with vpunch
      "Oh, I hit the spot."
      mrsl "What I don't understand... is why you're doing me a favor..."
      mrsl "Could it be that you're... mmm... trying to lose...? Are you secretly a masochist...?"
      window hide
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      pause 0.15
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue6 with Dissolve(.05)
      window auto
      mrsl "Hnnngh..."
      "???" "Do not look at me like that."
      $quest.lindsey_motive["taste_replay"] = True
    "Don't be stupid":
      "Oh, well. You can't just lick someone's pussy and not get caught."
      mrsl "Society is perfectly fine with how I dress, and so is the school board.{space=-20}"
      if quest.lindsey_motive["mrsl_model"]:
        "???" "What about your naked escapade earlier? That must be against\nthe rules."
        mrsl "Would you really deprive these young minds of their education in art{space=-10}\nand anatomy?"
        "???" "Debauchery! That is what it is!"
        mrsl "Well, it's part of the curriculum!"
      "???" "Outrageous."
      mrsl "No, what is outrageous is what you've been up to lately."
      mrsl "It doesn't take a genius to figure out that you're stirring the pot."
      mrsl "What I don't understand is why you're doing me a favor."
      mrsl "Could it be that you're trying to lose? Are you secretly a masochist?{space=-5}"
  "???" "I admit, things did get out of hand."
  if quest.isabelle_stolen["lindsey_slaped"]:
    "???" "That slap should never have happened."
    if quest.lindsey_motive["taste_replay"]:
      mrsl "Oh... but it was inevitable..."
    else:
      mrsl "Oh, but it was inevitable!"
  if quest.lindsey_motive["taste_replay"]:
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "Mmmm..."
    mrsl "What exactly was your plan...? You know you can't... mmm.... influence the boy..."
  else:
    mrsl "What exactly was your plan? You know you can't influence the boy."
  "...the boy?"
  if quest.isabelle_locker.finished:
    if quest.isabelle_stolen["lindsey_slaped"]:
      "???" "Oh, please."
      "???" "Apart from the slap, and the fact that someone stole the girl's entire locker, it turned out quite well."
    else:
      "???" "Oh, please."
      "???" "Apart from the fact that someone stole the girl's entire locker,\nit turned out quite well."
  elif quest.isabelle_stolen["lindsey_slaped"]:
    "???" "Oh, please."
    "???" "Apart from the slap, it turned out quite well."
  else:
    "???" "Oh, please. It turned out quite well."
  "???" "Someone steals a precious item from the girl. The boy successfully plays the hero, and they fall in love."
  "???" "Beautiful, isn't it?"
  "Wait, is she talking about me and [isabelle]?"
  mrsl "..."
  mrsl under_desk_closed_legs "You cheating bitch!"
  "???" "Absolutely untrue. The rules specifically say, no {i}direct{/} influence."
  "???" "The boy made the choice to help the girl. He could've stared at your oversized chest all day, but he did not."
  mrsl under_desk_closed_legs "Oh, is this how you want to play it? By all means."
  mrsl under_desk_closed_legs "Now get the fuck out of my classroom!"
  "???" "Oh, stop pouting. It's going to ruin your lipstick."
  window hide
  play sound "<from 8.2 to 10>door_breaking_in" volume 0.5
  pause 1.5
  window auto
  "Seems like they left..."
  "What the hell is going on? Did this person really make [lindsey] steal [isabelle]'s chocolate hearts, knowing that I'd help her?"
  "That is so messed up."
  "But also thanks, I guess? It really did help me get closer to [isabelle]."
  "I just don't understand why..."
  "What's the motive in all of—"
  if quest.lindsey_motive["taste_replay"]:
    window hide
    show mrsl under_desk_open_legs with Dissolve(.5)
    window auto
    "God, I was so caught up in the mystery that I almost forgot that there's a gorgeous pussy just a few inches away from my face..."
    "Does [mrsl] want me to finish what I started?"
    "..."
    "Don't mind if I do..."
    window hide
    show mrsl under_desk_tongue6 with Dissolve(.5)
    pause 0.15
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "Mmmm! Yes!"
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.1
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.15
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "F-fuck!"
    "You have a filthy mouth, [mrsl]."
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.1
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.15
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "Fuck! Fuck! Fuck!"
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_squirt1 with vpunch
    window auto
    mrsl under_desk_squirt1 "Ohhh!"
    show mrsl under_desk_squirt2 with dissolve2
    "Damn, I actually made her climax!"
    "I'll add that to my CV."
    mrsl under_desk_squirt2 "..."
  window hide
  show mrsl under_desk_look with Dissolve(.5)
  window auto
  mrsl under_desk_look "This never happened."
  window hide
  show mrsl under_desk_closed_legs with Dissolve(.5)
  pause 0.25
  show mrsl under_desk with Dissolve(.5)
  play sound "<to 2.2>high_heels"
  pause 1.5
  play sound "<from 8.2 to 10>door_breaking_in" volume 0.5
  pause 1.5
  window auto
  "Huh..."
  "She just got up and left."
  if quest.lindsey_motive["taste_replay"]:
    "I thought for sure she'd send me to the gulags for that..."
    "I don't know what's more bizarre — [mrsl] letting me eat her pussy or that weird conversation."
  "I don't know what to make of this..."
  "What a strange day..."
  $del quest.lindsey_motive.flags["taste_replay"]
  scene location with fadehold
  return
