init python:
  register_replay("flora_ambush","Flora's Ambush","replays flora_ambush","replay_flora_ambush")

label replay_flora_ambush:
  show flora dnd_bandits held_hostage: ## This gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 18
    linear 0.05 yoffset 0
  show fantasy_feeling
  with fadehold
  hide flora
  show flora dnd_bandits held_hostage behind fantasy_feeling ## This is just to avoid the screen shake if the player is skipping
  # mc "He rips the hem of your undergarment, exposing your soft, supple thigh."
  mc "He rips the hem of your undergarment, exposing your soft, supple thigh.{space=-90}"
  flora dnd_bandits held_hostage "I kill him!"
  mc "You can't use your crossbow in close combat!"
  maya "Should have picked the sword, eh?"
  flora dnd_bandits held_hostage "Whatever."
  mc "Anyway, you let out a scream as he tries to push you down onto the moss laden ground."
  # mc "Your heart quickens at his violent touch, you feel his manhood straining against your—"
  mc "Your heart quickens at his violent touch, you feel his manhood straining{space=-75}\nagainst your—"
  flora dnd_bandits held_hostage "Hold on, [mc]! What are you trying to do?"
  mc "What? It's just part of the story."
  flora dnd_bandits held_hostage "Well, I don't like the direction this is going."
  maya "It's cool, [flora]. I got you."
  window hide
  pause 0.125
  show flora dnd_bandits stabbed with hpunch
  pause 0.25
  window auto
  maya "I whip out my massive... sword, and fuck that guy's shit up."
  # maya "Stab him right in the back. Impale him. Then, ask him how he likes it."
  maya "Stab him right in the back. Impale him. Then, ask him how he likes it.{space=-15}"
  maya "He can't answer, of course, because he's choking on his own blood."
  maya "Afterward, I compose a song about it."
  maya "Naturally, it's an instant hit single."
  mc "..."
  mc "Roll for it."
  maya "..."
  maya "20."
  mc "Jesus."
  scene location with fadehold
  return
