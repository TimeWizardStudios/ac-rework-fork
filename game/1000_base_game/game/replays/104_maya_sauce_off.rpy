init python:
  register_replay("maya_sauce_off","Maya's Sauce-Off","replays maya_sauce_off","replay_maya_sauce_off")

label replay_maya_sauce_off:
  $flora["outfit_stamp"] = flora.outfit
  $flora.outfit = {"panties":"flora_striped_panties", "bra":"flora_purple_bra", "pants":"flora_dress", "shirt":"flora_blouse", "blindfold":"flora_referee_shirt", "hat":"flora_referee_cap"}
  $maya["outfit_stamp"] = maya.outfit
  $maya.outfit = {"panties":"maya_panties", "bra":"maya_bra", "dress":"maya_dress", "jacket":"maya_jacket"}
  show expression LiveComposite((1920,1080), (0,0),"home kitchen kitchen", (241,832),"home kitchen carpets", (1028,524),"home kitchen furniture_03", (1432,183),"home kitchen closet", (1457,728),"home kitchen d3", (1597,684),"home kitchen d1", (1507,261),"home kitchen s1", (966,325),"home kitchen cupboard", (1009,327),"home kitchen c1", (175,197),"home kitchen door", (497,227),"home kitchen stove", (805,93),"home kitchen lights", (362,227),"home kitchen furniture_01", (444,667),"home kitchen d2", (831,669),"home kitchen mf", (689,272),"home kitchen clock", (591,301),"home kitchen squid", (575,593),"home kitchen counter", (857,476),"home kitchen various_back", (1223,626),"home kitchen barstool_2", (1087,639),"home kitchen barstool_1", (691,521),"home kitchen kettle", (424,482),"home kitchen various_front", (1087,545),"home kitchen sauce_left", (908,544),"home kitchen sauce_fahrenheit", (956,544),"home kitchen sauce_bust_a_ghost", (1008,544),"home kitchen sauce_taco_hell", (933,545),"home kitchen sauce_wench", (986,544),"home kitchen sauce_carolina", (1058,517),"home kitchen plant", (0,0),"home kitchen stairs") as makeshift_location
  show flora neutral at Transform(xalign=.25)
  show maya dramatic at Transform(xalign=.75)
  with fadehold
  flora neutral "Anyway, here we go!"
  flora neutral "No spitting, no food, no drinks. Else you lose."
  mc "Got it."
  maya dramatic "Naturally."
  flora excited "Okay, then. Bottoms up!"
  maya sarcastic "Oh? I didn't know it was going to be that kind of contest."
  maya sarcastic "Good thing I wore a skirt. I'm so ready to get my ass scorched!"
  flora angry "[maya]!"
  mc "Yeah, [maya], behave."
  maya cringe "The death of self-expression! Woe is me!"
  mc "Whatever. I guess I'll go first."
  mc "..."
  show flora angry at move_to("left")
  show maya cringe at move_to("right")
  menu(side="middle"):
    extend ""
    "Channel your inner hothead":
      show flora angry at move_to(.25)
      show maya cringe at move_to(.75)
      "A mild burning sensation hits my mouth."
      "It's not bad. I might start using this one."
      mc "Easy as pie! Tastes like it, too."
      maya sarcastic "You swallow like a champ, [mc]."
      mc "Very cute. I bet you hear that one a lot."
      maya dramatic "I do! My fans love to chant it during the cock-sucking contests."
      maya dramatic "...where I'm also the champion, by the way."
      mc "Just take your turn, will you?"
      window hide
      show maya hot_sauce_competition taco_hell_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide makeshift_location
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "[maya] shrugs and takes the bottle from me."
      "Her eyes meet mine as she tips it back, lathering her tongue in the sauce."
      show maya hot_sauce_competition taco_hell_aftermath licking with {"master": Dissolve(.5)}
      "She swallows, then closes her eyes and licks her lips."
      show maya hot_sauce_competition taco_hell_aftermath bored with {"master": Dissolve(.5)}
      maya hot_sauce_competition taco_hell_aftermath bored "Like a champ."
      flora "Well done, both of you!"
      # flora "Take a moment to let it die down, and then we'll move to the next one."
      flora "Take a moment to let it die down, and then we'll move to the next one.{space=-45}"
    "Give it your best shot":
      show flora angry at move_to(.25)
      show maya cringe at move_to(.75)
      "Instantly, the heat of regret washes through me."
      "My tongue ignites into a fiery inferno."
      "Beads of sweat immediately spring to my forehead."
      show flora concerned
      show maya sarcastic
      with {"master": Dissolve(.5)}
      mc "Pfft!" with vpunch
      "It's just too much. I'm forced to spit it out."
      "My face on fire from the heat and the shame."
      maya sarcastic "Hah! I knew you were a spitter."
      window hide
      show maya hot_sauce_competition taco_hell_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide makeshift_location
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      pause 0.25
      show maya hot_sauce_competition taco_hell_aftermath licking with Dissolve(.5)
      window auto
      # "She takes the bottle from me, pours the sauce right onto her tongue and then swallows."
      "She takes the bottle from me, pours the sauce right onto her tongue{space=-10}\nand then swallows."
      show maya hot_sauce_competition taco_hell_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition taco_hell_aftermath confident "Yum! Piece of cake."
      "Fuck. She didn't even break a sweat."
      scene location with fadehold
      $flora.outfit = flora["outfit_stamp"]
      $maya.outfit = maya["outfit_stamp"]
      return
  window hide
  show expression LiveComposite((1920,1080), (0,0),"home kitchen kitchen", (241,832),"home kitchen carpets", (1028,524),"home kitchen furniture_03", (1432,183),"home kitchen closet", (1457,728),"home kitchen d3", (1597,684),"home kitchen d1", (1507,261),"home kitchen s1", (966,325),"home kitchen cupboard", (1009,327),"home kitchen c1", (175,197),"home kitchen door", (497,227),"home kitchen stove", (805,93),"home kitchen lights", (362,227),"home kitchen furniture_01", (444,667),"home kitchen d2", (831,669),"home kitchen mf", (689,272),"home kitchen clock", (591,301),"home kitchen squid", (575,593),"home kitchen counter", (857,476),"home kitchen various_back", (1223,626),"home kitchen barstool_2", (1087,639),"home kitchen barstool_1", (691,521),"home kitchen kettle", (424,482),"home kitchen various_front", (1087,545),"home kitchen sauce_left", (908,544),"home kitchen sauce_fahrenheit", (956,544),"home kitchen sauce_bust_a_ghost", (1008,544),"home kitchen sauce_taco_hell", (933,545),"home kitchen sauce_wench", (986,544),"home kitchen sauce_carolina", (1058,517),"home kitchen plant", (0,0),"home kitchen stairs") as makeshift_location behind maya
  show flora excited at Transform(xalign=.25)
  show maya bored flip at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora excited "Okay, contestants! We are now moving onto sauce number two."
  flora excited "Prepare your tongues and secure your taste buds..."
  flora excited "Next up, we have {i}The Saucy Wench!{/}"
  maya bored flip "Just like my noisy ex-roommate."
  mc "Or [flora]."
  flora angry "Hey! Don't harass the judge!"
  maya skeptical flip "Yeah! He should get points deducted!"
  flora angry "I'm very tempted to straight up disqualify him."
  mc "That's the only way [maya] is going to win."
  maya confident flip "Nevermind. Let him play. I want to beat him."
  flora concerned "Tsk! Fine."
  flora concerned "Go ahead, [mc]."
  mc "Don't mind if I do..."
  "As the lid comes off, a sharp smell assaults my nose."
  "It makes my eyes water, but I can't show weakness now."
  mc "It smells... delicious."
  maya bored flip "Don't just stare at it. Eat it."
  mc "Careful, or I'll make you swallow the entire thing."
  maya blush flip "Don't start sweet talking me now!"
  mc "Heh. I knew you would like that."
  "Everyone is confident before the sauce hits their tongue."
  "And boy, it does."
  mc "..."
  show flora concerned at move_to("left")
  show maya blush flip at move_to("right")
  menu(side="middle"):
    extend ""
    # "Stronger sauces have treated me worse":
    "Stronger sauces have\ntreated me worse":
      show flora concerned at move_to(.25)
      show maya blush flip at move_to(.75)
      "A pulse of fire rushes through my mouth."
      "It's hot, all right... but not enough to make me quit."
      # "A wink at [maya]. A drop of sauce in my mouth. Fearless and cocksure."
      "A wink at [maya]. A drop of sauce in my mouth. Fearless and cocksure.{space=-30}"
      mc "You were saying?"
      maya bored flip "Oh, please. That's an easy one."
      maya bored flip "Hand it over."
      window hide
      show maya hot_sauce_competition wench_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide makeshift_location
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "She snatches it out of my hands, and instantly knocks it back, as if downing a shot."
      show maya hot_sauce_competition wench_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition wench_aftermath confident "Mmm! Refreshing!"
      mc "Yeah, yeah."
      mc "What else have you got, [flora]?"
      flora "We are about to enter masochism territory now."
      flora "Are you guys ready?"
      show maya hot_sauce_competition wench_aftermath dramatic with {"master": Dissolve(.5)}
      maya hot_sauce_competition wench_aftermath dramatic "Alas, what is pain but a reminder that you are alive?"
      mc "Bring it on."
      flora "Very well!"
    "Try not to cry":
      show flora concerned at move_to(.25)
      show maya blush flip at move_to(.75)
      "Fuck, fuck, fuck!"
      "That's way hotter than I was expecting..."
      "I'll never live it down if I puss out, but—"
      show maya confident flip with {"master": Dissolve(.5)}
      mc "Pfft!" with vpunch
      "The pain overwhelms me, my eyes water, and the spit-sauce mix dribbles out of my mouth."
      maya confident flip "He spits, he quits."
      flora concerned "Really, [mc]? That was lowkey pathetic."
      mc "Sh-shut up... [maya] still has to try it..."
      maya confident flip "That won't be a problem."
      window hide
      show maya hot_sauce_competition wench_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide makeshift_location
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      # "She takes the bottle from me and squirts it straight onto her tongue."
      "She takes the bottle from me and squirts it straight onto her tongue.{space=-15}"
      # "She holds it there for what feels like forever, before finally swallowing."
      "She holds it there for what feels like forever, before finally swallowing.{space=-45}"
      show maya hot_sauce_competition wench_aftermath bored with {"master": Dissolve(.5)}
      maya hot_sauce_competition wench_aftermath bored "Easier than a load of cum."
      maya hot_sauce_competition wench_aftermath bored "Tastier, too."
      mc "Ugh! No way!"
      flora "Don't be a sore loser, [mc]."
      flora "You did it, [maya]!"
      scene location with fadehold
      $flora.outfit = flora["outfit_stamp"]
      $maya.outfit = maya["outfit_stamp"]
      return
  window hide
  show expression LiveComposite((1920,1080), (0,0),"home kitchen kitchen", (241,832),"home kitchen carpets", (1028,524),"home kitchen furniture_03", (1432,183),"home kitchen closet", (1457,728),"home kitchen d3", (1597,684),"home kitchen d1", (1507,261),"home kitchen s1", (966,325),"home kitchen cupboard", (1009,327),"home kitchen c1", (175,197),"home kitchen door", (497,227),"home kitchen stove", (805,93),"home kitchen lights", (362,227),"home kitchen furniture_01", (444,667),"home kitchen d2", (831,669),"home kitchen mf", (689,272),"home kitchen clock", (591,301),"home kitchen squid", (575,593),"home kitchen counter", (857,476),"home kitchen various_back", (1223,626),"home kitchen barstool_2", (1087,639),"home kitchen barstool_1", (691,521),"home kitchen kettle", (424,482),"home kitchen various_front", (1087,545),"home kitchen sauce_left", (908,544),"home kitchen sauce_fahrenheit", (956,544),"home kitchen sauce_bust_a_ghost", (1008,544),"home kitchen sauce_taco_hell", (933,545),"home kitchen sauce_wench", (986,544),"home kitchen sauce_carolina", (1058,517),"home kitchen plant", (0,0),"home kitchen stairs") as makeshift_location behind maya
  show flora flirty at Transform(xalign=.25)
  show maya dramatic at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora flirty "Then, I present to you... {i}Bust a Ghost!{/}"
  maya sarcastic "Oh! Just like [mc] last night!"
  mc "True, I did hear someone moaning."
  maya cringe "If you're implying what I think you're implying, it wasn't me."
  maya sarcastic "I take it in silence like a good girl."
  mc "Heh. I bet you do."
  # "[maya] is probably just messing around, but now I can't get the image of her struggling to stay quiet while getting railed out of my head..."
  "[maya] is probably just messing around, but now I can't get the image of{space=-55}\nher struggling to stay quiet while getting railed out of my head..."
  flora worried "I sincerely hope you're drooling because of the hot sauce."
  mc "Err, yes! Let's have a taste, shall we?"
  maya dramatic "Sorry, I'm off the clock."
  mc "Maybe next time, then?"
  flora worried "..."
  "Uh-oh. [flora] seems to be getting annoyed by the banter."
  "I better tone it down if I don't want to jeopardize my chances of winning this contest."
  mc "I meant, err... a taste of the sauce, of course! [flora]?"
  flora thinking "Here you go."
  flora thinking "Let's see if you can handle it, [mc]."
  mc "Happily."
  "Except when I grab this one, my hand shakes slightly."
  "Probably anticipation for the spice that's about to hit."
  mc "..."
  show flora thinking at move_to("left")
  show maya dramatic at move_to("right")
  menu(side="middle"):
    extend ""
    "Be like hot sauce":
      show flora thinking at move_to(.25)
      show maya dramatic at move_to(.75)
      "It can flow, it can crash, and it can hit you right in the face."
      "And hit it certainly does."
      # "Like a wildfire raging through my mouth and sinuses, making me sweat on the inside."
      "Like a wildfire raging through my mouth and sinuses, making me sweat{space=-65}\non the inside."
      # "My face goes red, my ears thud, my eyes bleed... but I fight through it and then swallow."
      "My face goes red, my ears thud, my eyes bleed... but I fight through it{space=-30}\nand then swallow."
      "Even pulling off a weak smile at the end."
      mc "D-done."
      show maya sarcastic
      flora worried "Are you okay?"
      mc "I have... never... been better..."
      maya sarcastic "Still hanging in there, are we?"
      "As she takes the bottle from me, my hoodie practically falls off my shoulders by itself."
      "The kitchen suddenly feels a lot warmer."
      maya sarcastic "Whoa, there! I don't remember ordering a stripper."
      mc "Don't pretend like you don't enjoy the show..."
      maya dramatic "Be still, my beating heart!"
      window hide
      show maya hot_sauce_competition bust_a_ghost_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide makeshift_location
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      pause 0.25
      show maya hot_sauce_competition bust_a_ghost_aftermath licking with Dissolve(.5)
      window auto
      "After she has a taste, her own hoodie comes off."
      mc "Oh, my god! Look! Naked arms!"
      show maya hot_sauce_competition bust_a_ghost_aftermath dramatic with {"master": Dissolve(.5)}
      maya hot_sauce_competition bust_a_ghost_aftermath dramatic "I'm so sorry! I know this much flesh scandalizes you terribly!"
      "Seeing [maya] like that does do something to me, all right."
      "The way the clothes cling to her moist skin and accentuates her petite curves..."
      "Her chest heaving from the heat of the moment..."
      "She's so tiny... very much unlike her personality."
      flora "You could definitely say things are heating up now!"
      flora "Clothes coming off and fists coming out!"
      flora "Let's see how you handle this next one..."
    "Believe in yourself":
      show flora thinking at move_to(.25)
      show maya dramatic at move_to(.75)
      "And the hit sends me straight to my knees." with vpunch
      # "For a moment, my spirit leaves my body, hovering over the young man in pain."
      "For a moment, my spirit leaves my body, hovering over the young man{space=-50}\nin pain."
      "He writhes on the floor, coughing, gasping, eyes and nose leaking."
      flora worried "[mc]? Are you okay?"
      # "When I return to my body, my head is pounding, my vision is blurred, and my throat is on fire."
      "When I return to my body, my head is pounding, my vision is blurred,{space=-20}\nand my throat is on fire."
      maya dramatic "He looks like he's going to explode."
      mc "F-fuck! Move!"
      window hide
      play sound "fast_whoosh"
      show flora worried at move_to("left")
      show maya dramatic at move_to("right")
      pause 0.0
      hide screen interface_hider
      show maya hot_sauce_competition bust_a_ghost_taste
      show black
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "The dash to the fridge feels like a marathon."
      "The fumbling search for the milk is like a desperate robbery."
      "Finally, it hits my tongue, blessedly soothing the raging fire."
      flora "Disqualified!"
      show black onlayer screens zorder 100
      pause 0.5
      hide makeshift_location
      hide flora
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      pause 0.25
      show maya hot_sauce_competition bust_a_ghost_aftermath licking with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      flora "Damn, [maya]! Look at you go!"
      flora "That's a real winner, right there!"
      # "When the burning finally subsides, [maya] is standing there in her skirt and top, her jacket discarded."
      "When the burning finally subsides, [maya] is standing there in her skirt{space=-25}\nand top, her jacket discarded."
      "There's a faint bit of sweat clinging to her neck, making the already tight top stick to her even more."
      "It leaves very little to the imagination."
      show maya hot_sauce_competition bust_a_ghost_aftermath bored with {"master": Dissolve(.5)}
      maya hot_sauce_competition bust_a_ghost_aftermath bored "It's getting toasty, isn't it?"
      maya hot_sauce_competition bust_a_ghost_aftermath bored "Except I managed to keep it down without turning into a little bitch."
      mc "You should... keep going..."
      "At least give me some sort of consolation prize..."
      show maya hot_sauce_competition bust_a_ghost_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition bust_a_ghost_aftermath confident "Never do more work than you absolutely have to."
      flora "True! She's our winner with that one."
      scene location with fadehold
      $flora.outfit = flora["outfit_stamp"]
      $maya.outfit = maya["outfit_stamp"]
      return
  window hide
  $maya.unequip("maya_jacket")
  show expression LiveComposite((1920,1080), (0,0),"home kitchen kitchen", (241,832),"home kitchen carpets", (1028,524),"home kitchen furniture_03", (1432,183),"home kitchen closet", (1457,728),"home kitchen d3", (1597,684),"home kitchen d1", (1507,261),"home kitchen s1", (966,325),"home kitchen cupboard", (1009,327),"home kitchen c1", (175,197),"home kitchen door", (497,227),"home kitchen stove", (805,93),"home kitchen lights", (362,227),"home kitchen furniture_01", (444,667),"home kitchen d2", (831,669),"home kitchen mf", (689,272),"home kitchen clock", (591,301),"home kitchen squid", (575,593),"home kitchen counter", (857,476),"home kitchen various_back", (1223,626),"home kitchen barstool_2", (1087,639),"home kitchen barstool_1", (691,521),"home kitchen kettle", (424,482),"home kitchen various_front", (1087,545),"home kitchen sauce_left", (908,544),"home kitchen sauce_fahrenheit", (956,544),"home kitchen sauce_bust_a_ghost", (1008,544),"home kitchen sauce_taco_hell", (933,545),"home kitchen sauce_wench", (986,544),"home kitchen sauce_carolina", (1058,517),"home kitchen plant", (0,0),"home kitchen stairs") as makeshift_location behind maya
  show flora confident at Transform(xalign=.25)
  show maya bored flip at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora confident "It's the infamous {i}Carolina Jeepers Creeper!{/}"
  flora confident "It is said to have once killed a group of teenagers during a bus ride!"
  flora confident "Take a crack at it, if you dare!"
  mc "Oh, I most certainly do."
  mc "I'm sure it's nothing more than a scarecrow. Hand it over."
  flora excited "Enjoy it, big boy!"
  # "My throat still burns and my tongue is starting to feel numb, but I can't show any weakness."
  "My throat still burns and my tongue is starting to feel numb, but I can't{space=-60}\nshow any weakness."
  # "Instead, the cap comes off with a pop, and the sauce hits the back of my throat."
  "Instead, the cap comes off with a pop, and the sauce hits the back of{space=-25}\nmy throat."
  mc "..."
  show flora excited at move_to("left")
  show maya bored flip at move_to("right")
  menu(side="middle"):
    extend ""
    # "The flesh is weak, but the mind endures":
    "The flesh is weak,\nbut the mind endures":
      show flora excited at move_to(.25)
      show maya bored flip at move_to(.75)
      "It hits me like a fire truck, right in the face."
      "Slamming into my taste buds and screaming through my sinuses."
      "My brain vibrates in my head, my eyes and nose start to gush."
      mc "Hoooly—"
      show flora neutral
      maya confident flip "You're looking a little red there, [mc]."
      maya confident flip "Do you need some water?"
      "Y-yes!"
      mc "N-no..."
      mc "That was... nothing..."
      maya blush flip "Are you sure? Because it looks like you're about to shit your pants."
      mc "If I do, I'll let you clean them after I win."
      # maya bored flip "Bitch, please. Hand that over and watch what a shitless win looks like."
      maya bored flip "Bitch, please. Hand that over and watch what a shitless win looks like.{space=-45}"
      mc "Good luck. You'll need it."
      window hide
      show maya hot_sauce_competition carolina_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide makeshift_location
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "She takes the bottle from me and douses her tongue with the fiery hell pepper."
      show maya hot_sauce_competition carolina_aftermath thinking with {"master": Dissolve(.5)}
      maya hot_sauce_competition carolina_aftermath thinking "Oh? I detect a hint of... is that semen?"
      maya hot_sauce_competition carolina_aftermath thinking "Yum. Delicious."
      mc "What? No, you don't!"
      show maya hot_sauce_competition carolina_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition carolina_aftermath confident "It's okay to like the sauce! No homo."
      "[maya] plays it perfectly cool, but even as she speaks, she removes her top."
      "As if it's the most casual thing in the world."
      "My mouth opens and then closes, and then opens again, but the words fail me."
      "She just stands there with her perky tits on display."
      "Her skin flushed, dewy beads of sweat subtly clinging to her."
      "The fire in my mouth is replaced with a fire somewhere much lower."
      maya hot_sauce_competition carolina_aftermath confident "What's the matter? Pepper got your tongue?"
      mc "Err, nope! I'm just preparing for the next one..."
      "...and whatever it is she plans on removing next."
      flora "Very well! Last sauce coming right up!"
      flora "Are you two ready?"
      maya hot_sauce_competition carolina_aftermath confident "Bring the heat, baby."
      mc "Hell yeah!"
    "Do your damndest":
      show flora excited at move_to(.25)
      show maya bored flip at move_to(.75)
      "And it's one of the biggest mistakes of this new life of mine."
      window hide
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      hide black onlayer screens with open_eyes
      pause 0.25
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      hide black onlayer screens with open_eyes
      window auto
      "The world seems to spin and blink around me."
      "I don't know why I agreed to this."
      "What was I even trying to prove?"
      "That I'm somehow better than [maya]?"
      "That I'm worthy?"
      window hide
      hide screen interface_hider
      show black
      show black onlayer screens zorder 100
      with close_eyes
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "That's something the old me would've done."
      "The new me shouldn't be trying so hard to prove something to the world."
      flora "Hellooo? [mc]?"
      maya "I think he's going to faint."
      show black onlayer screens zorder 100
      pause 0.5
      hide black onlayer screens
      show screen interface_hider
      scene location
      with Dissolve(.5)
      $flora.outfit = flora["outfit_stamp"]
      $maya.outfit = maya["outfit_stamp"]
      $set_dialog_mode("")
      return
  window hide
  $maya.unequip("maya_dress")
  show expression LiveComposite((1920,1080), (0,0),"home kitchen kitchen", (241,832),"home kitchen carpets", (1028,524),"home kitchen furniture_03", (1432,183),"home kitchen closet", (1457,728),"home kitchen d3", (1597,684),"home kitchen d1", (1507,261),"home kitchen s1", (966,325),"home kitchen cupboard", (1009,327),"home kitchen c1", (175,197),"home kitchen door", (497,227),"home kitchen stove", (805,93),"home kitchen lights", (362,227),"home kitchen furniture_01", (444,667),"home kitchen d2", (831,669),"home kitchen mf", (689,272),"home kitchen clock", (591,301),"home kitchen squid", (575,593),"home kitchen counter", (857,476),"home kitchen various_back", (1223,626),"home kitchen barstool_2", (1087,639),"home kitchen barstool_1", (691,521),"home kitchen kettle", (424,482),"home kitchen various_front", (1087,545),"home kitchen sauce_left", (908,544),"home kitchen sauce_fahrenheit", (956,544),"home kitchen sauce_bust_a_ghost", (1008,544),"home kitchen sauce_taco_hell", (933,545),"home kitchen sauce_wench", (986,544),"home kitchen sauce_carolina", (1058,517),"home kitchen plant", (0,0),"home kitchen stairs") as makeshift_location behind maya
  show flora excited at Transform(xalign=.25)
  show maya bored_skirt flip at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora excited "Okay, this is the one!"
  flora excited "The sauce of all sauces!"
  flora excited "The pepper that will get your goat!"
  flora excited "The—"
  mc "Can we please just get on with it?"
  flora concerned "Where is your sense of pageantry?"
  maya confident_skirt flip "It must've died with his sense of style."
  mc "Hey! I'm just ready to kick your ass, is all."
  maya confident_skirt flip "Kiss my ass, you mean?"
  mc "You'd like that, wouldn't you? Especially once my lips are on fire."
  flora angry "Okay, that's enough!"
  flora angry "I don't want to hear about your gross fantasies."
  # flora confident "Instead, are you ready for the one and only... {i}Fahrenheit-551{/} hot sauce?!"
  flora confident "Instead, are you ready for the one and only... {i}Fahrenheit-551{/} hot sauce?!{space=-90}"
  flora confident "It contains actual kerosene!"
  flora confident "[mc]. Your move."
  # "A deep breath to steel my nerves and my bowels is all that I can afford."
  "A deep breath to steel my nerves and my bowels is all that I can afford.{space=-65}"
  "The bottle feels heavy in my hand."
  "This is a path where there is no turning back."
  "Life and choices, right?"
  "This is one I have to live with."
  "Death sauce straight to the tongue."
  mc "..."
  show flora confident at move_to("left")
  show maya confident_skirt flip at move_to("right")
  menu(side="middle"):
    extend ""
    "Transcend":
      show flora confident at move_to(.25)
      show maya confident_skirt flip at move_to(.75)
      "As soon as it hits, it tears right through me."
      "Like a current of superheated electricity."
      window hide
      hide screen interface_hider
      show maya hot_sauce_competition fahrenheit_taste
      show black
      show black onlayer screens zorder 100
      with close_eyes
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "My eyes burn, and my nose starts to run."
      "This heat is on a psychological level."
      "My mind reels as the desperation hits me."
      "It's like touching snow for too long, or holding your breath."
      "It's a primal sort of suffering."
      "..."
      "The only escape is to think of happy things."
      "Of [maya] topless. Of [flora] topless."
      "And somehow... it's working."
      show black onlayer screens zorder 100
      pause 0.5
      hide makeshift_location
      hide flora
      hide black
      hide black onlayer screens
      show screen interface_hider
      with open_eyes
      window auto
      $set_dialog_mode("")
      "Holy shit."
      "Am I dreaming, or...?"
      show maya hot_sauce_competition fahrenheit_aftermath with {"master": Dissolve(.5)}
      "[maya] stands there completely topless."
      "Her face is red, sweat dripping down her forehead."
      "She looks ready to explode."
      "Her small breasts heave as she pants."
      window hide
      play sound "fast_whoosh"
      hide screen interface_hider
      show black
      show black onlayer screens zorder 100
      with Move((45, 0), (-45, 0), .10, bounce=True, repeat=True, delay=.275)
      pause 0.5
      hide black onlayer screens
      show screen interface_hider
      scene location
      with Dissolve(.5)
      $flora.outfit = flora["outfit_stamp"]
      $maya.outfit = maya["outfit_stamp"]
      return
    "You've come this far...":
      show flora confident at move_to(.25)
      show maya confident_skirt flip at move_to(.75)
      "So far, I don't—"
      window hide
      play sound "falling_thud"
      hide screen interface_hider
      show black
      show black onlayer screens zorder 100
      with Move((0,30), (0,-30), .10, bounce=True, repeat=True, delay=.275)
      pause 0.5
      hide black onlayer screens
      show screen interface_hider
      scene location
      with Dissolve(.5)
      $flora.outfit = flora["outfit_stamp"]
      $maya.outfit = maya["outfit_stamp"]
      return
