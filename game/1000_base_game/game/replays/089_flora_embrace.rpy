init python:
  register_replay("flora_embrace","Flora's Embrace","replays flora_embrace","replay_flora_embrace")

label replay_flora_embrace:
  show flora bathroom_hug with fadehold
  if quest.jacklyn_romance["i_wont_go"]:
    "[flora] wipes the tears from her cheek as I take a step closer to her."
    "She rebels against me at first, trying to pull away. To escape."
    "Nonetheless, time seems to slow and the world shrinks to nothing but this bathroom."
  else:
    "She wipes the tears from her cheek and takes a step closer to me."
    # "Time seems to slow and the world shrinks to nothing but this bathroom."
    "Time seems to slow and the world shrinks to nothing but this bathroom.{space=-85}"
  "Just [flora] and me."
  "Nothing else matters. No one else matters."
  "Not when there's a chance for us."
  "A chance born out of a miraculous jump back in time."
  "But it's real, and it's here. And nothing can take that away."
  "..."
  "God, I can't believe how badly I messed up in my old life..."
  "Hugging her feels so right, even though it's wrong."
  "She's always been reluctant to show her true feelings, but her walls finally come down as she returns my embrace."
  "She curls into my chest, her arms still wrapped around herself."
  "Squeezing her tight, the scent of her shampoo fills my senses."
  "Her heartbeat against my chest, finally slowing down."
  "It won't be easy... but what good thing ever was?"
  flora bathroom_hug "Never let me go, okay?"
  scene location with fadehold
  return
