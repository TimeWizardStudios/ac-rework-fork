init python:
  register_replay("kate_prize","Kate's Prize","replays kate_prize","replay_kate_prize")

label replay_kate_prize:
  show kate spanking_bend_over with fadehold
  kate spanking_bend_over "Now, isn't this so much better?"
  kate spanking_bend_over "Finally, at our feet where you belong."
  "Crap. This is not good."
  stacy "Answer her!"
  mc "Yes, ma'am. This is better..."
  casey "Oh, he agrees! You've trained him well!"
  lacey "Queen!"
  "God, this is so humiliating... I'm completely at their mercy."
  kate spanking_bend_over "So, girls, how many swats does he deserve?"
  lacey "Like, ten at least!"
  casey "Come on, [lacey]."
  lacey "What?"
  "That's probably as high as she can count..."
  kate spanking_bend_over "The answer is... all of them."
  "Shit. Fuck. Cock."
  kate "Don't you agree, [mc]?"
  mc "Yes, ma'am. All of them..."
  stacy "Oh, this is exciting! He's such a little bitch boy!"
  kate spanking_bend_over "Oh, he will be a total bitch after this, won't he?"
  lacey "He better be!"
  kate spanking_bend_over "Pants down, bitch."
  "I better do as they say..."
  window hide
  pause 0.5
  show kate spanking_pants_down with Dissolve(.5)
  window auto
  lacey "He totally did it!"
  kate spanking_pants_down "Of course he did. He knows he's not the one wearing the pants in this scenario."
  casey "Are you ready for your forfeit, [mc]?"
  mc "Yes, ma'am..."
  kate spanking_pants_down "Then, why aren't your shorts down too?"
  "Oh, god. They'll see everything!"
  stacy "Do as you're told, bitch."
  "Crap, this is not the right time to freeze up..."
  kate spanking_pants_down "I think he's nervous..."
  kate spanking_pants_down "[stacy], can you hold him?"
  stacy "With pleasure!"
  show kate spanking_chair with Dissolve(.5)
  kate spanking_chair "Are you nervous, [mc]?"
  mc "Y-yes..."
  kate spanking_chair "Yes, what?"
  mc "Yes, ma'am..."
  kate spanking_chair "Don't forget it again or we'll be here all night."
  mc "Sorry, ma'am."
  kate spanking_chair "Now, pull those shorts down. I won't ask you again."
  window hide
  pause 0.5
  show kate spanking_kate1 with Dissolve(.5)
  window auto
  "Oh, fuck. This is happening."
  "On some level I thought it was a joke, but clearly they mean business..."
  kate spanking_kate1 "Look at that juicy ass, just begging for some good discipline."
  mc "..."
  lacey "He looks scared!"
  casey "He should be!"
  kate spanking_kate1 "You don't mind if I hit as hard as I can, right?"
  mc "Um..."
  stacy "Speak clearly!"
  mc "No, ma'am!"
  kate spanking_kate2 "We'll see about that..."
  window hide
  show kate spanking_kate3 with Dissolve(.05)
  show kate spanking_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate7 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  window auto
  mc "Fuck me!"
  stacy "Not likely!"
  window hide
  show kate spanking_kate3 with Dissolve(.05)
  show kate spanking_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_kate3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate7 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_kate3 with Dissolve(.05)
  show kate spanking_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate7 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.5)
  window auto
  "Each hit of the paddles sends shockwaves through my core..."
  "Instant pain. Lingering heat. An odd pleasure."
  window hide
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  window auto
  "Stacy sits on my back, holding me down, while [kate] brings the pain.{space=-5}"
  "The heat from my ass flows through my skin and hits my face as well.{space=-40}"
  "But perhaps that's just the embarrassment of being spanked by a group of hot cheerleaders?"
  show kate spanking_stacy_up_next with Dissolve(.5)
  kate spanking_stacy_up_next "How's that, [mc]?"
  mc "It... burns..."
  lacey "He forgot again!"
  casey "Address us with respect, you maggot!"
  mc "I'm sorry, ma'am!"
  mc "C-can I go now?"
  kate spanking_stacy_up_next "Are you being serious right now?"
  kate spanking_stacy_up_next "No, you can't go!"
  kate spanking_stacy_up_next "But what you can do is ask for more..."
  "Oh, god..."
  menu(side="middle"):
    extend ""
    "\"Please, ma'am, can I have some more?\"":
      mc "Please, ma'am, can I have some more?"
      kate spanking_stacy_up_next "You absolutely can."
      stacy "My turn, then..."
    "\"Please, no more...\"":
      mc "Please, no more..."
      casey "That is not what any of us want to hear."
      lacey "Nuh-uh! I haven't had my turn yet."
      kate spanking_stacy_up_next "Maybe he'll be more agreeable after another round?"
      stacy "I think you're right."
  stacy "Would you mind holding him down for me, [kate]?"
  kate spanking_stacy_up_next "With pleasure!"
  show kate spanking_red_stacy1 with Dissolve(.5)
  stacy "The summer is way too long... I always miss putting losers back in their place."
  window hide
  show kate spanking_red_stacy2 with Dissolve(.5)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  window auto
  mc "Ow! Ow! Ow!"
  "God, that hurt! She's not hitting as hard as [kate], but her technique is painfully good..."
  "My ass is on fire!"
  "But the shame is much worse than the pain..."
  stacy "You won't be able to sit for a week after this!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy4 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy4 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.5)
  window auto
  "Each stroke of the paddle puts me further and further under their control..."
  "The pain fuels the lust of submission..."
  window hide
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  window auto
  mc "Ouch!"
  "If I don't put a stop to this, I'll be their plaything for the rest of the year...{space=-90}"
  show kate spanking_redder_casey_up_next with Dissolve(.5)
  kate spanking_redder_casey_up_next "You got him really good, [stacy]!"
  stacy "As good as he deserves."
  lacey "Look at that red ass! Say something, [mc]!"
  mc "O-oww..."
  kate spanking_redder_casey_up_next "Not animal noises. Thank [stacy] for setting you straight with the paddle.{space=-75}"
  mc "T-thanks... for setting me straight, ma'am..."
  kate spanking_redder_casey_up_next "That's a good boy. We'll teach you some manners yet."
  kate spanking_redder_casey_up_next "Now, ask [casey] for more."
  menu(side="middle"):
    extend ""
    "\"Please, Miss [casey], can I have some more?\"{space=-35}":
      mc "Please, Miss [casey], can I have some more?"
      casey "Oh, I like the sound of that. Very polite."
      casey "I'll happily oblige."
    "\"This is humiliating enough! I won't ask for more...\"{space=-120}":
      mc "This is humiliating enough! I won't ask for more..."
      lacey "Wow, rude! That's rude!"
      kate spanking_redder_casey_up_next "Yeah, he still has a lot to learn it seems."
      kate spanking_redder_casey_up_next "Maybe we'll have to introduce weekly spankings this year..."
  kate spanking_redder_casey_up_next "Go ahead, [casey]."
  show kate spanking_redder_casey1 with Dissolve(.5)
  casey "I'll enjoy this..."
  window hide
  show kate spanking_redder_casey2 with Dissolve(.5)
  pause 0.1
  show kate spanking_redder_casey3 with Dissolve(.05)
  show kate spanking_redder_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey7 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  window auto
  "Fuck, that hurt!"
  "Each of them has their own style of spanking..."
  "[kate] is brutal and merciless."
  "[stacy] is skilled and precise."
  "[casey] is—"
  window hide
  show kate spanking_redder_casey3 with Dissolve(.05)
  show kate spanking_redder_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_casey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey7 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_casey3 with Dissolve(.05)
  show kate spanking_redder_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey7 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.5)
  window auto
  mc "Oww!"
  "—harsh and uncaring..."
  casey "Quiet, bitch boy."
  mc "Sorry—"
  window hide
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  window auto
  "Fuck!"
  casey "I said quiet."
  show kate spanking_reddest_lacey_up_next with Dissolve(.5)
  casey "You need to learn how to listen."
  casey "I don't care what you have to say."
  "She's cruel, goddamn..."
  lacey "My turn, my turn!"
  kate spanking_reddest_lacey_up_next "[mc], ask [lacey] for more."
  menu(side="middle"):
    extend ""
    "\"Can I please have some more, Miss [lacey]?\"{space=-30}":
      mc "Can I please have some more, Miss [lacey]?"
      lacey "Ah! I feel like a queen!"
      casey "You are a queen, babe!"
      stacy "He should worship the ground you walk on."
      kate spanking_reddest_lacey_up_next "Damn straight."
    "\"This has gone way too far!\"":
      mc "This has gone way too far!"
      kate spanking_reddest_lacey_up_next "No one asked for your opinion."
      lacey "Yeah!"
  stacy "Come on, [lacey]. Give it to him."
  lacey "Oh, I'm planning on it!"
  show kate spanking_reddest_lacey1 with Dissolve(.5)
  "Out of these four, [lacey] worries me the most..."
  "She's unpredictable and slightly unhinged."
  lacey "I've been looking forward to this..."
  "She strokes my ass gently with the paddle. Teasing me."
  "There's malice in her touch."
  "She lowers her voice to a whisper."
  lacey "{size=25}{i}I'm going to hurt you.{/}"
  window hide
  show kate spanking_reddest_lacey2 with Dissolve(.5)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.5)
  window auto
  mc "Fuck!"
  "The searing pain rips through my asscheeks."
  "Red hot like burning coals."
  "A sob escapes my throat, and suddenly the gym is silent."
  window hide
  pause 1.0
  window auto
  "Then, [lacey] giggles."
  lacey "He's crying! I made him cry!"
  lacey "Ask for more!"
  kate spanking_tomato_lacey2 "Do it, bitch!"
  stacy "Beg!"
  casey "Beg like a dog!"
  "I try to speak, but another sob comes out instead."
  lacey "Good enough!"
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.1)
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.1)
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  window auto
  "She alternates between hard swats and playful teasing."
  "Hot tears stream down my face, but she's not done..."
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  window auto
  lacey "More?"
  stacy "I'd say so!"
  casey "Absolutely!"
  kate spanking_tomato_lacey2 "Definitely more!"
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  window auto
  "My throat is screwed shut."
  "More sobs roll out when I try to plead for mercy."
  "But there is no mercy to be had..."
  "They're fully in control of my punishment."
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  window auto
  stacy "Look at that red ass! It's like two ripe tomatoes!"
  kate spanking_tomato_lacey2 "How does it feel, [mc]?"
  kate spanking_tomato_lacey2 "Actually, I don't care."
  kate spanking_tomato_lacey2 "What matters is you know your place."
  "[kate]'s voice suddenly takes a darker tone."
  kate spanking_tomato_lacey2 "Now, beg me to finish you off."
  kate spanking_tomato_lacey2 "Show me you're a true bitch — beg me to bust your balls."
  menu(side="middle"):
    extend ""
    "\"P-please...\"":
      mc "P-please..."
      kate spanking_tomato_lacey2 "That's not really it, but I'll take it."
      show kate spanking_tomato_step_back with Dissolve(.5)
      "Huh? What's happening?"
      kate spanking_tomato_step_back "I've been looking forward to this..."
      window hide
      pause 0.5
      show kate spanking_tomato_kick with Dissolve(.5)
      pause 0.5
    "\"No!\"":
      mc "No!"
      show kate spanking_tomato_fight_back
      kate spanking_tomato_fight_back "Huh?" with vpunch
      window hide
  play sound "falling_thud"
  show black with vpunch
  window auto
  scene location with fadehold
  return
