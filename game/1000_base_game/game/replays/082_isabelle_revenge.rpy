init python:
  register_replay("isabelle_revenge","Isabelle's Revenge","replays isabelle_revenge","replay_isabelle_revenge")

label replay_isabelle_revenge:
  $x = quest.isabelle_gesture["masturbated"]
  $quest.isabelle_gesture["masturbated"] = 0
  show isabelle door_peek confrontation with fadehold
  kate "Whatever. I'm busy. I don't have time to properly roast you."
  "There's a hint of [kate]'s old fire crackling in her voice."
  "No matter what happens next, it doesn't seem like she's going to just roll over for [isabelle]."
  isabelle door_peek confrontation "Well, so am I."
  isabelle door_peek confrontation "I'm doing a little ghost hunting."
  kate "..."
  "That seems to deflate [kate] some."
  "She's quiet, the air heavy with a breathless tension."
  kate "Why would you say that?"
  isabelle door_peek confrontation "I heard the school is haunted. I just thought I would see for myself."
  kate "I don't need this..."
  window hide
  pause 0.125
  show isabelle door_peek step_around1 with Dissolve(.5)
  pause 0.25
  show isabelle door_peek step_around2 with Dissolve(.5)
  pause 0.25
  window auto
  "[kate] tries to step around [isabelle], but [isabelle] sidesteps in front of her, blocking her way to the door."
  kate "What are you doing, freak? Let me out!"
  isabelle door_peek step_around2 "I wanted to ask you something first."
  kate "Well, too bad."
  isabelle door_peek step_around2 "Why are you always such a twat?"
  kate "Ugh! Just leave me alone!"
  "The old [kate] probably would have had an actual comeback to that..."
  "...but the new one clearly just wants to get away."
  window hide
  pause 0.125
  show isabelle door_peek other_direction1 with Dissolve(.5)
  pause 0.25
  show isabelle door_peek other_direction2 with Dissolve(.5)
  pause 0.25
  window auto
  "She attempts a maneuver around [isabelle] in the other direction, but is blocked again."
  isabelle door_peek other_direction2 "Is it because that's the only way you feel anything?"
  isabelle door_peek other_direction2 "Or are you just a sociopath?"
  "These words seem to reignite [kate]'s temper."
  window hide
  pause 0.125
  show isabelle door_peek arm_swing1 with Dissolve(.5)
  pause 0.25
  window auto
  "She straightens up and cocks her arm back, only to swing it back around, aiming her hand at [isabelle]'s face."
  window hide
  pause 0.125
  show isabelle door_peek arm_swing2 with hpunch
  pause 0.25
  window auto
  "However, [isabelle] is quick to react."
  "She probably saw the fire raging in [kate]'s eyes."
  "She catches [kate]'s wrist before she can make contact, then holds it tight for a moment as they stare each other down."
  window hide
  pause 0.125
  show isabelle door_peek kiss1 with Dissolve(.5)
  pause 0.25
  window auto
  "And then, to [kate]'s surprise and mine, [isabelle] presses her lips to [kate]'s, disarming her for a moment."
  if isabelle.love >= 10:
    "And it's as if a crack opens in my heart."
    "Tears roll down my cheeks."
    "How did I ruin it with [isabelle] so badly?"
  # "It's hard to tell if it's in revenge for the attempted slap, or something else entirely..."
  "It's hard to tell if it's in revenge for the attempted slap, or something{space=-15}\nelse entirely..."
  "If it was an accident of passion or intentional."
  if isabelle.love >= 10:
    "A part of me hopes that this is all for show."
    "And even then, it hurts to watch."
  window hide
  pause 0.125
  show isabelle door_peek kiss2 with Dissolve(.5)
  pause 0.25
  window auto
  "With her one hand still clasped in [isabelle]'s grip, [kate] shoves at her weakly with the other, breaking the kiss."
  "[isabelle] doesn't seem fazed, though."
  "Only an unwavering determination fills her every move."
  window hide
  pause 0.125
  show isabelle door_peek kiss3 with Dissolve(.5)
  pause 0.25
  window auto
  kate "Owww!"
  "Instead, she uses her other hand to reach up and grab [kate] roughly by the hair."
  "She yanks [kate]'s head back, keeping a tight hold on the blonde tresses, before kissing her again."
  "Harder this time, locking [kate] in her embrace."
  window hide
  pause 0.125
  show isabelle door_peek grope1 with hpunch
  pause 0.25
  window auto
  # "Then, she relentlessly pushes forward until [kate]'s back hits the wall."
  "Then, she relentlessly pushes forward until [kate]'s back hits the wall.{space=-15}"
  "Wrist pinned, lips touching, she just holds her there."
  "And that seems to undo something in [kate]."
  "Her posture slackens and she starts to kiss [isabelle] back."
  if isabelle.love >= 10:
    "And that's when I can't take it anymore."
    "This is the worst day of my life."
    "Losing what I had with [isabelle] thanks to my own stupidity and selfishness..."
  else:
    "Hesitant at first, then with more force."
    "I can hardly believe what I'm seeing..."
    "[isabelle], not taking any of [kate]'s crap and woman-handling her into submission like that..."
    "Their soft, full lips going at each other, filling the room with the luscious wet sound..."
    "..."
    "My dick takes on a mind of its own, getting harder and harder at the sight."
    "The kissing... the roughness of it..."
    "Fuck me."
  window hide
  pause 0.125
  show isabelle door_peek grope2 with Dissolve(.5)
  pause 0.25
  window auto
  "[isabelle]'s hand travels down [kate]'s body, stopping at her heaving chest."
  "Feeling her up as they kiss each other with fast, desperate lips."
  "As if both are fighting to come out on top."
  if isabelle.love >= 10:
    "And maybe they both come out on top."
    "Maybe I'm the only loser in all of this..."
  "Low moans escape from [kate]'s mouth as [isabelle] works her tongue inside."
  "And [kate] just lets it happen, growing weaker and weaker against the unrelenting force that is [isabelle]."
  window hide
  pause 0.125
  show isabelle door_peek grope3 with Dissolve(.5)
  pause 0.25
  window auto
  "Even as [isabelle] trails her hand lower, down from [kate]'s chest to her skirt."
  "Their lips never break apart."
  if isabelle.love >= 10:
    "It's the most crushing thing I've seen."
    jump replay_isabelle_revenge_just_watch
  else:
    "It's the most confusing and hot thing I've ever seen."
  menu(side="middle"):
    extend ""
    "Masturbate":
      "I can't stand it anymore."
      "If I don't get some release, I might actually explode."
      window hide
      pause 0.125
      show isabelle door_peek stroke1 grope3 with Dissolve(.5)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.2
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.15
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.25
      window auto
      "Mmm... watching the two girls continue to go at it..."
      "The way [isabelle] takes control, holding [kate] in place with her fingers up her skirt..."
      "Fuuuuck."
      window hide
      pause 0.125
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.15
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.15
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.25
      window auto
      "Watching these two together..."
      "Bitter enemies, engaging in a kissing match..."
      "And [kate] somehow losing..."
      "It's like nothing I've ever seen before."
      $quest.isabelle_gesture["masturbated"] = True
    "Just watch":
      label replay_isabelle_revenge_just_watch:
        if isabelle.love >= 10:
          "As much as I want to scream for them to stop... I can do nothing but watch it unfold."
          "Part of me probably knows that I deserve to suffer for what I did."
          "And the cracks spread across my heart."
          "Breaking it apart with every moment that passes."
          "And it makes me realize how much I really like [isabelle]."
        else:
          "But as hard as my dick is... I can only stand there and stare."
          "And as hot as the fantasy is, I do find it difficult to watch [isabelle] attempting to fuck someone else."
          "And it makes me realize how much I really like her."
        "How stupid I would be to ruin things with her."
        "Especially over someone like [kate]."
  window hide
  pause 0.125
  if quest.isabelle_gesture["masturbated"]:
    show isabelle door_peek stroke2 grope4 with Dissolve(.5)
  else:
    show isabelle door_peek grope4 with Dissolve(.5)
  pause 0.25
  window auto
  "Despite [isabelle]'s grip and hungry passion, [kate] finally comes to her senses and shoves her away."
  if isabelle.love >= 10:
    "Thank god..."
  "Both of them pant for air, confusion lingering in the silence."
  window hide
  pause 0.125
  if quest.isabelle_gesture["masturbated"]:
    show isabelle door_peek stroke2 push_away with hpunch
  else:
    show isabelle door_peek push_away with hpunch
  pause 0.25
  if quest.isabelle_gesture["masturbated"]:
    show isabelle door_peek stroke2 run_away with Dissolve(.5)
  else:
    show isabelle door_peek run_away with Dissolve(.5)
  pause 0.25
  window auto
  "Then, [kate] seems to snap back to reality."
  "Quickly, she pushes past [isabelle] and runs for the door."
  "[isabelle] just lets it happen, standing there in stunned silence."
  "Clearly, not even sure herself of what just happened."
  if quest.isabelle_gesture["masturbated"]:
    "Crap. I better get out of here before [kate] sees—"
    mc "Fuck, fuck, fuuuuuck! It's too late to stop now!"
    window hide
    pause 0.125
    show isabelle door_peek stroke1 run_away with Dissolve(.25)
    show isabelle door_peek stroke2 run_away with Dissolve(.25)
    pause 0.1
    show isabelle door_peek stroke1 run_away with Dissolve(.25)
    show isabelle door_peek cum run_away with hpunch
    pause 0.25
  scene location with fadehold
  $quest.isabelle_gesture["masturbated"] = x
  return
