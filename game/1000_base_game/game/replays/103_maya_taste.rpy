init python:
  register_replay("maya_taste","Maya's Taste","replays maya_taste","replay_maya_taste")

label replay_maya_taste:
  show maya chili flora_smile_right maya_chew with fadehold
  flora "So, what do you think, [maya]?"
  mc "It's [flora]'s culinary masterpiece."
  mc "...or so she says, at least."
  maya chili flora_smile_right maya_thinking_left "It's good!"
  maya chili flora_smile_right maya_thinking_left "I haven't had anything this delicious in a long, long time."
  show maya chili flora_excited_right maya_thinking_left with {"master": Dissolve(.5)}
  flora "Really?!"
  show maya chili flora_excited_right maya_flirty_left with {"master": Dissolve(.5)}
  maya chili flora_excited_right maya_flirty_left "Fuck yeah! The last good thing I remember eating was pussy."
  "God, that's even hotter than the chili..."
  "Assuming [maya] is being serious, that is."
  mc "Oh? Whose?"
  maya chili flora_skeptical maya_chew_half "Sorry, I don't munch and tell."
  mc "Mm-hmm..."
  flora "Can you cut it out, [mc]?"
  flora "Seriously. At least during dinner."
  mc "But [maya] started it!"
  show maya chili flora_smile_right maya_chew_half with {"master": Dissolve(.5)}
  flora "[maya] is our guest."
  mc "So? Does that just excuse her?"
  maya chili flora_smile_right maya_flirty_empty "Them's the rules!"
  mc "That you just made up."
  show maya chili flora_smile_right maya_smile_left_empty with {"master": Dissolve(.5)}
  maya chili flora_smile_right maya_smile_left_empty "...maybe."
  maya chili flora_smile_right maya_smile_left_empty "Anyway, can I get some more of that chili, [flora]?"
  show maya chili flora_excited_right maya_smile_left_empty with {"master": Dissolve(.5)}
  flora "Really? Oh, wow! I'm amazed you can handle seconds!"
  flora "Most people take a few bites, and then reach for the milk."
  window hide
  pause 0.125
  show maya chili flora_excited_right maya_smile_left mc_half with Dissolve(.5)
  pause 0.25
  window auto
  mc "Not me! I don't quit!"
  maya chili flora_neutral maya_thinking mc_half "Oh? I always did peg you for a swallower, [mc]."
  maya chili flora_neutral maya_thinking mc_half "Still, I'm pretty confident I can take the heat better than anyone I know.{space=-60}"
  "Now, that's a challenge if I ever saw one..."
  menu(side="middle"):
    extend ""
    "\"You don't know me too well, then.\"":
      mc "You don't know me too well, then."
      maya chili flora_neutral maya_thinking mc_half "Really? Do you think you can handle heat better than me?"
      maya chili flora_neutral maya_thinking mc_half "I would have thought you like it bland, [mc]."
      mc "See? What did I just say about you not knowing me?"
      mc "I like all kinds of unexpected things."
      maya chili flora_neutral maya_flirty mc_half "Like a dick in the ass?"
      show maya chili flora_skeptical_right_half maya_flirty mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_right_half maya_flirty mc_half "Sometimes, that's unexpected."
      mc "Very funny. I meant in my mouth."
      maya chili flora_skeptical_right_half maya_dramatic mc_half "Ah, I see!"
      show maya chili flora_skeptical_right_half maya_smile_chest mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_right_half maya_smile_chest mc_half "You like dicks in your mouth. That tracks."
      show maya chili flora_sarcastic_right_half maya_smile_chest mc_half with {"master": Dissolve(.5)}
      flora "It kind of does, doesn't it?"
      mc "No! Food! Stop twisting my words!"
      maya chili flora_sarcastic_right_half maya_chew_half mc_half "No twisting necessary on my part."
      maya chili flora_sarcastic_right_half maya_chew_half mc_half "You did that all on your own like some kind of perverted pretzel."
      show maya chili flora_smile_half maya_chew_half mc_half with {"master": Dissolve(.5)}
      flora "As amusing as it is to watch [mc] be this embarrassed..."
      flora "...you guys do realize there's only one way to settle this, right?"
    "\"Pfft! From the cops, maybe.\"":
      mc "Pfft! From the cops, maybe."
      maya chili flora_neutral maya_dramatic mc_half "How very dare you bring up my record, sir?!"
      mc "What? Handcuffs are probably your favorite accessory."
      show maya chili flora_skeptical_half maya_dramatic mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_dramatic mc_half "Gasp!" with vpunch
      show maya chili flora_skeptical_half maya_neutral_left mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_neutral_left mc_half "..."
      show maya chili flora_skeptical_half maya_smile_chest mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_smile_chest mc_half "Actually, they would be my second favorite."
      maya chili flora_skeptical_half maya_smile_chest mc_half "Right after accessory to murder."
      mc "Whose?"
      maya chili flora_skeptical_half maya_chew_half mc_half "If you keep this up? Yours."
      mc "Is this why you're so fiery all the time? Your diet?"
      show maya chili flora_skeptical_half maya_thinking_half mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_thinking_half mc_half "If you're not burping brimstone, are you really trying?"
      mc "Heh! So, that's why the house smells of sulfur."
      show maya chili flora_eyeroll_half maya_thinking_half mc_half with {"master": Dissolve(.5)}
      flora "Oh, my god! Gross!"
      flora "Stop flirting, you two."
      maya chili flora_eyeroll_half maya_dramatic_left_half mc_half "Oh, sweetheart. That wasn't flirting."
      show maya chili flora_eyeroll_half maya_smile_left_half mc_half with {"master": Dissolve(.5)}
      maya chili flora_eyeroll_half maya_smile_left_half mc_half "You'll definitely know when I'm doing that."
      mc "Knowing [maya], it's probably about as subtle as a hand on the dick..."
      maya chili flora_eyeroll_half maya_flirty_chest_half mc_half "Aww! You really do know me!"
      show maya chili flora_skeptical_half maya_flirty_chest_half mc_half with {"master": Dissolve(.5)}
      flora "Okay! Point taken! Moving on!"
      flora "You guys do realize there's only one way to settle this, right?"
    "\"Weird brag, but okay...\"":
      mc "Weird brag, but okay..."
      maya chili flora_neutral maya_thinking mc_half "I'm not bragging. Just stating facts."
      mc "I'm just saying, if I were you, I'd be bragging about my singing voice instead."
      # maya chili flora_skeptical_half maya_bored mc_half "Bring that up again, and I'll break your throat and cut your hair. I mean it."
      maya chili flora_skeptical_half maya_bored mc_half "Bring that up again, and I'll break your throat and cut your hair.\nI mean it."
      mc "Threats in my own home?! Hallelujah!"
      maya chili flora_skeptical_half maya_bored mc_half "I can threaten you outside, if you'd prefer? It makes no difference to me."
      mc "Err, I'm good, thanks."
      show maya chili flora_annoyed_half maya_chew_half mc_half with Dissolve(.5)
      flora "What did I tell you about being nice, [mc]?"
      mc "Me? She's the one making threats!"
      flora "Well, you earned them."
      mc "Your betrayal wounds me, [flora]."
      show maya chili flora_skeptical_half maya_chew_half mc_half with {"master": Dissolve(.5)}
      flora "Stop being dramatic, will you?"
      "Oh, but it's fine when [maya] does it? Girls..."
      # flora "Anyway, you guys do realize there's only one way to settle this, right?"
      flora "Anyway, you guys do realize there's only one way to settle this, right?{space=-35}"
  if renpy.showing("maya chili flora_smile_half maya_chew_half mc_half"):
    maya chili flora_smile_half maya_flirty_left_half mc_empty "A gun duel, obviously."
    maya chili flora_smile_half maya_flirty_left_half mc_empty "Loser deep-throats the barrel."
  elif renpy.showing("maya chili flora_skeptical_half maya_flirty_chest_half mc_half"):
    maya chili flora_skeptical_half maya_thinking_left_bowl_half mc_empty "A gun duel, obviously."
    maya chili flora_skeptical_half maya_thinking_left_bowl_half mc_empty "Loser deep-throats the barrel."
  elif renpy.showing("maya chili flora_skeptical_half maya_chew_half mc_half"):
    maya chili flora_skeptical_half maya_flirty_left_half mc_empty "A gun duel, obviously."
    maya chili flora_skeptical_half maya_flirty_left_half mc_empty "Loser deep-throats the barrel."
  mc "That sounds a little extreme... and very dirty."
  if renpy.showing("maya chili flora_smile_half maya_flirty_left_half mc_empty"):
    show maya chili flora_excited_right_half maya_flirty_left_half mc_empty with {"master": Dissolve(.5)}
  elif renpy.showing("maya chili flora_skeptical_half maya_thinking_left_bowl_half mc_empty"):
    show maya chili flora_excited_right_half maya_thinking_left_bowl_half mc_empty with {"master": Dissolve(.5)}
  elif renpy.showing("maya chili flora_skeptical_half maya_flirty_left_half mc_empty"):
    show maya chili flora_excited_right_half maya_flirty_left_half mc_empty with {"master": Dissolve(.5)}
  flora "No, silly! A sauce-off!"
  maya chili flora_excited_right_half maya_dramatic_left_chest_half mc_empty "But my sauce is shy!"
  mc "A sauce-off?"
  show maya chili flora_smile_half maya_dramatic_left_chest_half mc_empty with {"master": Dissolve(.5)}
  flora "A hot sauce competition to decide which one of you is tougher."
  maya chili flora_smile_half maya_smile_half mc_empty "We already know who's tougher."
  maya chili flora_smile_half maya_smile_half mc_empty "I don't mind putting [mc] in his place, though."
  mc "We'll see about that. All right, deal."
  show maya chili flora_excited_half maya_smile_half mc_empty with {"master": Dissolve(.5)}
  flora "Great! I'll be the referee."
  maya chili flora_excited_half maya_thinking_left_bowl_empty mc_empty "You can be anything your little heart desires, sweetheart."
  maya chili flora_excited_half maya_thinking_left_bowl_empty mc_empty "Except not tonight, because I still have to go make a living on the corner."
  mc "How about tomorrow at high noon, then?"
  maya chili flora_excited_empty maya_flirty_empty mc_empty "You've got yourself a deal there, pardner!"
  scene location with fadehold
  return
