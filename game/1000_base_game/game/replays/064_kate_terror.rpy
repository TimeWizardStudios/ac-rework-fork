init python:
  register_replay("kate_terror","Kate's Terror","replays kate_terror","replay_kate_terror")

label replay_kate_terror:
  $x = quest.isabelle_dethroning["kate_spanking"]
  $y = quest.isabelle_dethroning["kate_sex"]
  $z = quest.isabelle_dethroning["kate_cum"]
  $quest.isabelle_dethroning["kate_spanking"] = quest.isabelle_dethroning["kate_sex"] = quest.isabelle_dethroning["kate_cum"] = 0
  show kate cowering with fadehold
  "Wow, look at her... so small and fearful."
  "That's not something you see every day... or ever, really."
  "Her face is completely covered, her eyes glued shut by the\ndrying paint."
  if mc.owned_item("pig_mask"):
    "The pig mask is pretty useless in her current state."
  else:
    "Maybe I should call [isabelle]... but that would give away my voice."
  "The most important thing right now is to not speak."
  "If I talk, [kate] will know it's me."
  "..."
  "But perhaps I could oink to scare her?"
  "Or maybe..."
  menu(side="middle"):
    extend ""
    "Oink":
      pass
    "Grunt":
      mc "Uh...?"
      kate cowering "Chad, is that you?!"
      mc "Uh-huh."
      kate cowering "Oh, thank god! You won't believe what I just saw!"
      "Chad must really be a man of few words. I can't believe she thinks I'm him."
      kate cowering "I swear I just saw a ghost pig!"
      kate cowering "I was freaking out until you got here!"
      mc "Mhm..."
      kate cowering "Haha, I'm being serious!"
      mc "Uh-huh."
      kate cowering "Hihihi, why are you always teasing me?"
      "It's odd to see this side of [kate] up close."
      "She's always such a bitch to me, it feels totally out of character..."
      kate cowering "Can you help me get this stuff out of my eyes?"
      mc "Nuh-uh."
      kate cowering "What? You think I look cute like this?"
      mc "Uh-huh."
      kate cowering "My god, you're such a charmer!"
      "Ugh, it's disgusting how she fawns over him."
      "She's always such an ice queen, but with Chad, she's so... girly."
      "Life's so unfair sometimes..."
      "But maybe I can use this to my advantage?"
      menu(side="middle"):
        extend ""
        "Grab her":
          window hide
          show black onlayer screens zorder 100 with Dissolve(.5)
          pause 0.5
          play sound "fast_whoosh"
          show kate hate_sex_bent_over:
            block:
              linear 0.05 xoffset 25
              linear 0.05 xoffset -25
              repeat 2
            linear 0.05 xoffset 0
          hide black onlayer screens
          with Dissolve(.5)
          hide kate
          show kate hate_sex_bent_over
          window auto
          kate hate_sex_bent_over "Oh, Chad! You're in a mood today!"
          "You have no idea, bitch."
          mc "Uh-huh."
          kate hate_sex_bent_over "What about [stacy]?"
          mc "Uh-uh."
          kate hate_sex_bent_over "Oh? You like me more now?"
          mc "Mmmm."
          kate hate_sex_bent_over "Oh, you! You can date us both if you want to!"
          mc "Mhm..."
          kate hate_sex_bent_over "This stuff is really burning my eyes, though..."
          mc "Huh."
          kate hate_sex_bent_over "What is it? I can't see anything!"
          mc "Mhm."
          kate hate_sex_bent_over "You're kinky! My god!"
          "As if [kate] really thinks that. God, she's so fake."
          window hide
          show kate hate_sex_smile_spanking1 with Dissolve(.5)
          show kate hate_sex_smile_spanking2 with Dissolve(.075)
          play sound "slap"
          $quest.isabelle_dethroning["kate_spanking"]+=1
          $renpy.transition(vpunch, layer="master")
          show kate hate_sex_smile_spanking3 with Dissolve(.05)
          show kate hate_sex_spanking4 with Dissolve(.05)
          show kate hate_sex_spanking1 with Dissolve(.125)
          window auto
          kate hate_sex_spanking1 "Ohhh!"
          mc "Mhm."
          window hide
          show kate hate_sex_spanking2 with Dissolve(.075)
          play sound "slap"
          $quest.isabelle_dethroning["kate_spanking"]+=1
          $renpy.transition(vpunch, layer="master")
          show kate hate_sex_spanking3 with Dissolve(.05)
          show kate hate_sex_spanking4 with Dissolve(.05)
          show kate hate_sex_spanking1 with Dissolve(.125)
          window auto show
          show kate hate_sex_smile_spanking1 with dissolve2
          kate hate_sex_smile_spanking1 "Ow! Hehe! What am I getting punished for?"
          "For being a total and eternal bitch."
          window hide
          show kate hate_sex_smile_spanking2 with Dissolve(.075)
          play sound "slap"
          $quest.isabelle_dethroning["kate_spanking"]+=1
          $renpy.transition(vpunch, layer="master")
          show kate hate_sex_smile_spanking3 with Dissolve(.05)
          show kate hate_sex_spanking4 with Dissolve(.05)
          show kate hate_sex_spanking1 with Dissolve(.125)
          window auto
          kate hate_sex_spanking1 "Owww! Be careful!"
          kate hate_sex_spanking1 "You're really strong, you know?"
          menu(side="middle"):
            extend ""
            "Spank her harder":
              window hide
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              pause 0.1
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              window auto
              kate hate_sex_spanking1 "Owwwww! That really hurt!"
              mc "Mhm!"
              kate hate_sex_spanking1 "I'm all for fooling around and being kinky, but..."
              window hide
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              pause 0.05
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              window auto show
              show kate hate_sex_tearing_up_spanking1 with dissolve2
              kate hate_sex_tearing_up_spanking1 "Owwww! Stop! That's way too hard!"
              menu(side="middle"):
                extend ""
                "Spank her harder":
                  window hide
                  show kate hate_sex_tearing_up_spanking2 with Dissolve(.075)
                  play sound "slap"
                  $quest.isabelle_dethroning["kate_spanking"]+=1
                  $renpy.transition(vpunch, layer="master")
                  show kate hate_sex_tearing_up_spanking3 with Dissolve(.05)
                  show kate hate_sex_spanking4 with Dissolve(.05)
                  show kate hate_sex_spanking1 with Dissolve(.125)
                  pause 0.0
                  show kate hate_sex_spanking2 with Dissolve(.075)
                  play sound "slap"
                  $quest.isabelle_dethroning["kate_spanking"]+=1
                  $renpy.transition(vpunch, layer="master")
                  show kate hate_sex_spanking3 with Dissolve(.05)
                  show kate hate_sex_spanking4 with Dissolve(.05)
                  show kate hate_sex_spanking1 with Dissolve(.125)
                  window auto show
                  show kate hate_sex_tearing_up_spanking1 with dissolve2
                  kate hate_sex_tearing_up_spanking1 "Noooo! Stop!"
                  kate hate_sex_tearing_up_spanking1 "This hurts too much!"
                  "So? Never seen you care about that before."
                  window hide
                  show kate hate_sex_tearing_up_spanking2 with Dissolve(.075)
                  play sound "slap"
                  $quest.isabelle_dethroning["kate_spanking"]+=1
                  $renpy.transition(vpunch, layer="master")
                  show kate hate_sex_tearing_up_spanking3 with Dissolve(.05)
                  show kate hate_sex_spanking4 with Dissolve(.05)
                  show kate hate_sex_spanking1 with Dissolve(.125)
                  window auto show
                  show kate hate_sex_crying_spanking1 with dissolve2
                  kate hate_sex_crying_spanking1 "Chaaaaaaaad! Pleaseeeeee!"
                  mc "Mhm."
                  kate hate_sex_crying_spanking1 "You're hurting me, babe..."
                  "Duh. That's the whole point."
                  mc "Uh-huh."
                  kate hate_sex_crying_spanking1 "Why? What did I do?"
                  "God, she's such a whiner..."
                  "You torment people for fun, but can't take a little spanking? Pathetic.{space=-20}"
                  menu(side="middle"):
                    extend ""
                    "Spank her harder":
                      window hide
                      show kate hate_sex_crying_spanking2 with Dissolve(.075)
                      play sound "slap"
                      $quest.isabelle_dethroning["kate_spanking"]+=1
                      $renpy.transition(vpunch, layer="master")
                      show kate hate_sex_crying_spanking3 with Dissolve(.05)
                      show kate hate_sex_spanking4 with Dissolve(.05)
                      show kate hate_sex_spanking1 with Dissolve(.125)
                      pause 0.0
                      show kate hate_sex_spanking2 with Dissolve(.075)
                      play sound "slap"
                      $quest.isabelle_dethroning["kate_spanking"]+=1
                      $renpy.transition(vpunch, layer="master")
                      show kate hate_sex_spanking3 with Dissolve(.05)
                      show kate hate_sex_spanking4 with Dissolve(.05)
                      show kate hate_sex_spanking1 with Dissolve(.125)
                      pause 0.0
                      show kate hate_sex_spanking2 with Dissolve(.075)
                      play sound "slap"
                      $quest.isabelle_dethroning["kate_spanking"]+=1
                      $renpy.transition(vpunch, layer="master")
                      show kate hate_sex_spanking3 with Dissolve(.05)
                      show kate hate_sex_spanking4 with Dissolve(.05)
                      show kate hate_sex_spanking1 with Dissolve(.125)
                      window auto show
                      show kate hate_sex_orgasm_spanking1 with dissolve2
                      kate hate_sex_orgasm_spanking1 "Mmmhmmm..."
                      "Huh? She liked that?"
                      "That sounded almost like an orgasm..."
                      "..."
                      "There's no way [kate] is secretly a masochist, right?"
                      "Maybe she complained so much because she knew she would\nget off on it?"
                      "Or maybe she just fainted. It's hard to say with the paint covering her face."
                      "Either way, now it's time to claim my prize."
                      menu(side="middle"):
                        extend ""
                        "Fuck her pussy":
                          $quest.isabelle_dethroning["kate_sex"] = "pussy"
                        "Fuck her ass":
                          $quest.isabelle_dethroning["kate_sex"] = "ass"
                    "Fuck her pussy":
                      $quest.isabelle_dethroning["kate_sex"] = "pussy"
                    "Fuck her ass":
                      $quest.isabelle_dethroning["kate_sex"] = "ass"
                "Fuck her pussy":
                  $quest.isabelle_dethroning["kate_sex"] = "pussy"
                "Fuck her ass":
                  $quest.isabelle_dethroning["kate_sex"] = "ass"
            "Fuck her pussy":
              $quest.isabelle_dethroning["kate_sex"] = "pussy"
            "Fuck her ass":
              $quest.isabelle_dethroning["kate_sex"] = "ass"
          window hide
          if renpy.showing("kate hate_sex_spanking1"):
            show kate hate_sex_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_pulling_panties2 with Dissolve(.5)
          elif renpy.showing("kate hate_sex_tearing_up_spanking1"):
            show kate hate_sex_tearing_up_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_tearing_up_pulling_panties2 with Dissolve(.5)
          elif renpy.showing("kate hate_sex_crying_spanking1"):
            show kate hate_sex_crying_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_crying_pulling_panties2 with Dissolve(.5)
          elif renpy.showing("kate hate_sex_orgasm_spanking1"):
            show kate hate_sex_orgasm_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_orgasm_pulling_panties2 with Dissolve(.5)
          window auto
          if quest.isabelle_dethroning["kate_sex"] == "pussy":
            "Wow, look at that moist pussy."
            "I can't believe [kate] is this wet already."
            if quest.isabelle_dethroning["kate_spanking"] == 13:
              "I wonder if it's from the spanking?"
            "Probably just the fact that she thinks I'm Chad."
            "It's seriously weird how some women work."
            "Let's see if she can tell the difference between our cocks..."
            window hide
            show kate hate_sex_dick_reveal with Dissolve(.5)
            window auto
            kate hate_sex_dick_reveal "Chad, come on! Stop teasing me!"
            "So eager. So thirsty for popular dudes. Typical cheerleader behavior.{space=-10}"
            mc "Ugh..."
            kate hate_sex_dick_reveal "What's wrong? Don't you like what you see?"
            mc "Nuh-uh."
            kate hate_sex_dick_reveal "What do you mean? Is it the stuff on my face?"
            mc "Mhm."
            kate hate_sex_dick_reveal "Well, I asked you to remove it, but you—"
            window hide
            show kate hate_sex_vaginal4 with hpunch
            window auto
            kate hate_sex_vaginal4 "Ohhhhhh!"
            "God, she's so hot."
            "Even drenched in paint, she has the perfect body. The perfect pussy."
            "It fits the exact measurements of my dick."
            "It's almost as if we were genetically made for each other..."
            "Of course, she'll never know that."
            kate hate_sex_vaginal4 "Hmm? What's wrong?"
            "What is she on about now?"
            kate hate_sex_vaginal4 "Am I not pleasing to you?"
            "God, she's pathetic when it comes to alpha dudes."
            mc "Uh..."
            kate hate_sex_vaginal4 "Don't I usually get you a lot harder?"
            "Ah, great. I see what this is about."
            "Chad clearly has a much bigger dick than me, so [kate] thinks\nhe's not fully hard yet."
            mc "Nuh-uh."
            kate hate_sex_vaginal4 "Oh my god... that's so mean..."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            window auto
            kate hate_sex_vaginal4 "Ohhh! Cha-aad!"
            kate hate_sex_vaginal4 "You're making me feel soooo good!"
            "She might be faking it, but she sounds almost relieved that\nit's not Chad's thunder-cock banging her pussy..."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            window auto
            kate hate_sex_vaginal4 "Oh, fuck yes! Keep going, you stud!"
            "It does feel nice to be able to pleasure someone like [kate]."
            "She probably has the highest standards in the whole school."
            "Who would've thought me and my loser-dick could pull that off?"
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            window auto
            kate hate_sex_vaginal4 "God, you're such an animal!"
            kate hate_sex_vaginal4 "Fuck me! Fuck me harder!"
            mc "Mmhh! Mmhh!"
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Ohhhh! If you want to... dump [stacy]..."
            kate hate_sex_vaginal4 "I'm... down for a fling..."
            "No, bitch. All I want is a one night stand."
            "You'd make a horrible girlfriend."
            mc "Nuh-uh."
            kate hate_sex_vaginal4 "Ohhh... okay!"
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Oh, my god! You fuck me so good!"
            kate hate_sex_vaginal4 "Are you... sure? We could be..."
            kate hate_sex_vaginal4 "The new... ohhh... power couple!"
            mc "Nuh-uh."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Ohhh! Screw you... Chad... Haha!"
            kate hate_sex_vaginal4 "You know... we'd be great..."
            mc "Uh-uh."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Oh, god! I'm going to—"
            show kate hate_sex_squirt
            kate hate_sex_squirt "Ahhhhhhhh!" with vpunch
            "Her pussy squeezes my dick the tightest I've ever felt."
            "In an instant, she pushes me out and starts to shake in a\npowerful orgasm."
            "Damn, I even made her squirt!"
            "Guess I'll just finish myself off..."
            window hide
            show kate hate_sex_jerking_off4 with Dissolve(.5)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_cum_outside with vpunch
            $quest.isabelle_dethroning["kate_cum"] = "outside"
            show kate hate_sex_aftermath with Dissolve(.5)
            window auto
            "Ahhhh.... there it is..."
            "Bitch just got marked."
            "I still can't believe I fucked her to orgasm."
            "That in itself crosses out most of the nasty things she's said\nabout me in the past."
            "I bet she didn't think a loser like me could make her orgasm,\nmuch less squirt!"
          elif quest.isabelle_dethroning["kate_sex"] == "ass":
            "There it is... the holy grail of assholes."
            if quest.isabelle_dethroning["kate_spanking"] == 13:
              "Her ass is really glowing from the spanking..."
              "And her pussy is dripping..."
            "She's perfect and ripe for the taking."
            window hide
            show kate hate_sex_reluctance with Dissolve(.5)
            window auto
            "As I move closer, the tip of my dick grazes [kate]'s asshole."
            "It twitches, she twitches, and lets out a tiny gasp."
            kate hate_sex_reluctance "No, you know that won't work! You're way too big!"
            mc "Uh-uh."
            kate hate_sex_reluctance "Come on, it won't fit! You know this!"
            mc "Uh-uh."
            kate hate_sex_reluctance "Just put it in my pussy instead, okay? It's much—"
            window hide
            show kate hate_sex_anal1 with hpunch
            window auto
            kate hate_sex_anal1 "Oww! I told you—"
            mc "Mmmm."
            "She gasps for air, her breathing suddenly shallow."
            "My dick isn't nearly as big as Chad's, but it probably hurts to\ntake it in the butt regardless."
            "Her fingers curl into fists, and she bites her lip to ease the pain."
            "She's probably trying to come to terms with the fact that Chad\ngets what Chad wants."
            "And right now... Chad wants anal."
            window hide
            show kate hate_sex_anal2 with hpunch
            window auto
            kate hate_sex_anal2 "Owww! Wait! It really hurts!"
            "That's the point, bitch."
            "How does it feel to get sodomized?"
            "For me, it's heaven."
            "She's so tight and squirmy, but I'm not letting my dick out of\nher ass now."
            "I've almost got her exactly where I want her."
            "Just a few more inches..."
            window hide
            show kate hate_sex_anal3 with hpunch
            window auto
            kate hate_sex_anal3 "Oh, my god! That's sooooo deep!"
            "[kate] is probably proud of herself for taking Chad in the ass."
            "Girls take pride in the weirdest things."
            "Although, to be fair, no one else in the school would probably survive Chad's cock up the ass..."
            window hide
            show kate hate_sex_anal4 with hpunch
            window auto
            kate hate_sex_anal4 "Ooooooh!"
            "God damn! That's as deep as it goes."
            "It feels good to finally put that bitch in her place."
            "And to do it with my cock up her ass, that's daydream stuff."
            window hide
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            window auto
            kate hate_sex_anal4 "Oh, god! Your dick in my ass feels so good, Chad!"
            "Wow, it sounds like [kate] is enjoying almost as much as I am."
            "If only she knew..."
            window hide
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            window auto
            kate hate_sex_anal4 "Ohhhhhh! I can feel you in my tummy!"
            "Slamming into her again and again..."
            "Burying myself in her ass..."
            "That's what every guy dreams of."
            window hide
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            window auto
            "To thaw the fucking ice queen..."
            "Make her a docile kitten with my dick..."
            "Make her take the whole length..."
            "Make her enjoy an ass pounding..."
            "That's a life achievement!"
            window hide
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            window auto
            kate hate_sex_anal4 "Oh, my god!"
            mc "Mmmm!"
            "God, going all the way in, pulling out, then bottoming out again..."
            "Fuck! It's too good!"
            "I'm going to come!"
            menu(side="middle"):
              extend ""
              "Pull out":
                window hide
                show kate hate_sex_pain_cum_outside with vpunch
                $quest.isabelle_dethroning["kate_cum"] = "outside"
                show kate hate_sex_aftermath with Dissolve(.5)
                window auto
                "That's a pretty picture..."
                "[kate]'s ass coated in my cum."
                kate hate_sex_aftermath "You wipe that off, buddy!"
                mc "Nuh-uh."
                kate hate_sex_aftermath "Haha! You're so bad!"
                kate hate_sex_aftermath "Fine, I'll clean myself up..."
              "Fill her ass":
                window hide
                show kate hate_sex_cum_inside with hpunch
                window auto
                kate hate_sex_cum_inside "Ooooh!"
                kate hate_sex_cum_inside "You naughty boy... you came inside me!"
                mc "Uh-uh."
                window hide
                $quest.isabelle_dethroning["kate_cum"] = "inside"
                show kate hate_sex_aftermath with Dissolve(.5)
                window auto
                kate hate_sex_aftermath "I'll keep it inside me for the rest of the day."
                kate hate_sex_aftermath "Would you like that?"
                mc "Mmmm..."
                kate hate_sex_aftermath "Okay, then!"
            kate "Can you help me get this stuff off my face now?"
            mc "Nuh-uh."
            kate "Please!"
            "No, you keep it on."
            "Show everyone what you are."
            "Nothing but a dethroned queen."
            "The laughing stock of the undesirables."
          "And now it's time to..."
          menu(side="middle"):
            extend ""
            "...give her a heart attack.":
              mc "Oink."
              kate hate_sex_scare "W-what?"
              mc "..."
              kate hate_sex_scare "Chad?"
              "She probably can't believe her ears."
              kate hate_sex_scare "Chad?!"
              "I wonder what thoughts rush through her bewildered mind..."
              "Does she think Chad abandoned her?"
              "Or that Chad is the ghost pig?"
              "Did the ghost pig just fuck her?"
              "In the end, it doesn't matter."
              "Only her trauma matters."
              mc "Oink! Oink! Oink!"
              kate hate_sex_scare "Aaaaaaayee!"
              window hide
              show kate hate_sex_kateless with Dissolve(.5)
              pause 0.25
              show expression LiveComposite((1920,1080),(0,0),"kate avatar events hate_sex background",(873,76),"kate avatar events hate_sex door_closed") as kate
            "...make my escape.":
              "I've pushed my luck twice today, and it's time to dip."
              if quest.isabelle_dethroning["kate_sex"] == "pussy":
                "I came for [kate]'s fear, and got her pussy as well."
              elif quest.isabelle_dethroning["kate_sex"] == "ass":
                "I came for [kate]'s fear, and got her ass as well."
              "She can crawl around like a blind maggot for a while. That's\nmore than deserved."
              "Now, I have a dinner to get back to."
              $quest.isabelle_dethroning["kate_spanking"] = x
              $quest.isabelle_dethroning["kate_sex"] = y
              $quest.isabelle_dethroning["kate_cum"] = z
              scene location with fadehold
              return
        "Oink":
          pass
  if renpy.showing("kate cowering"):
    mc "Oink!"
    kate cowering "N-noooo! Go away!"
    mc "Ooooink, oink, oink, oink!"
    kate cowering "Aaaaaaaaaah! Help!"
    "Her face twisted in fear is all I've ever dreamed of."
    "To take back some of the power."
    "To show her that she's not the queen of the world."
    "She's just a trembling girl, scared out of her mind."
    window hide
    show kate cowering_kateless with Dissolve(.5)
    pause 0.25
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "She crawls across the floor in abject horror, hitting her head on chairs and easels."
  "I follow her as she cries and pleads, oinking in her ear."
  "Tears flow down her cheeks. Streams of piss down her legs."
  "She screams in fear."
  "Calls out for her dad."
  "Screams again."
  "Finally, she reaches the corner of the room, and just cowers."
  "Her body shakes in complete mind-numbing terror."
  "And that's how I leave her."
  "A whimpering mess on the floor."
  "Justice served, bitch."
  $set_dialog_mode("")
  $quest.isabelle_dethroning["kate_spanking"] = x
  $quest.isabelle_dethroning["kate_sex"] = y
  $quest.isabelle_dethroning["kate_cum"] = z
  show black onlayer screens zorder 100
  with None
  hide black onlayer screens
  scene location
  with fadehold
  window auto
  return
