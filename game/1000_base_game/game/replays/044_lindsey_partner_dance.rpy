init python:
  register_replay("lindsey_partner_dance","Lindsey's Partner Dance","replays lindsey_partner_dance","replay_lindsey_partner_dance")

label replay_lindsey_partner_dance:
  stop music fadeout 2.0
  show lindsey dancing_with_the_mc_background as background behind lindsey:
    xalign 0.5 yalign 0.5 zoom 1.125
  show lindsey dancing_with_the_mc_smile
  with fadehold
  play music "epic_medieval"
  lindsey dancing_with_the_mc_smile "Good, just like that!"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "[lindsey] smells so nice up close... Cotton candy and spring. Her waist is tiny!"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_smile "Take it slow! Lead me around the room to the music."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Okay, this isn't too hard. As long as I don't step on her toes..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "This is nice. Thanks for inviting me over."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "My pleasure..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Uh-oh, did that sound dirty?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  mc "I mean... I enjoyed it too! You coming over, that is."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Crap, I'm making it worse."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_smile "Do you ever feel like life's too short?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Hmm..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "When you're stuck for years in the same rut..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "With no will to break the cycle..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "No ambitions. No dreams. No hope."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "When you have no energy to fulfill anything but the most basic of needs..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "Eat. Sleep. Get off."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Rinse and repeat. Day in, day out."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "When you don't even shower, cut your nails, or brush your teeth anymore..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Then no... life's not too short."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "I don't think so."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_blush with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "I sometimes feel like I'm getting outrun by life."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "What do you mean?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "I practice so much that I don't have time for other things..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Oh..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "Everyone else is living, you know?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  mc "It feels like you're watching from the sideline?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_admiring with dissolve2:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_admiring "Exactly!"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_admiring:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Yeah, I know that feeling."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_admiring:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_admiring "Did you know I've never even had a boyfriend?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_admiring:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "You could have your pick..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "Perhaps, but I don't really have time for it."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "And my family... well, they wouldn't like it."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Pretty religious?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_smile with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_smile "Yeah..."
  menu(side="left"):
    extend ""
    "\"They probably just want to protect you.\"":
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_smile:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "They probably just want to protect you."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_smile:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      mc "There's nothing wrong with waiting."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_blush with dissolve2:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_blush "Do you really think so?"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_blush:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "Absolutely. I'm sure you'll find the right one."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_admiring with dissolve2:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_admiring "That's so sweet..."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_admiring:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_admiring "My mom would like you."
    "\"Maybe you should start deciding things for yourself?\"":
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_smile:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "Maybe you should start deciding things for yourself?"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_blush with dissolve2:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_blush "I'm not very good at that."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_blush:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "Well, then you need to find someone who can take care of you."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_blush:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      mc "Someone who will stand up for you and protect you, no matter what.{space=-15}"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_admiring with dissolve2:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_admiring "Like the dragon slayer?"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_admiring:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      mc "Yes. He's chivalrous and pure of heart."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_admiring:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_admiring "That would be the dream..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed with dissolve2:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "[lindsey] closes her eyes and lets me guide her to the music."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "She makes leading easy, matching my every move."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Her athletic body close to mine, the rise and fall of her chest..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "It's more intimate than a hug."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "My hand on the small of her back, mere inches away from her butt."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "It would be so easy to just slide it down and feel her up..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "Perhaps it's that temptation that makes it special?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Her trust that I won't abuse my position."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "It's strange how good that feels."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "That she trusts me to treat her right."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "Maybe that's my calling?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "To look after [lindsey] and protect her from the world."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Nothing will ever hurt her as long as I'm around."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "I'll be her biggest supporter, and her knight in shining armor."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Even if she doesn't see me as more than a friend."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "She is a princess, after all... and that's what she deserves."
  stop music fadeout 2.0
  scene location with fadehold
  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"
  else:
    stop music
  return
