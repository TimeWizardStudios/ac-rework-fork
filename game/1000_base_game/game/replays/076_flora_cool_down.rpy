init python:
  register_replay("flora_cool_down","Flora's Cool Down","replays flora_cool_down","replay_flora_cool_down")

label replay_flora_cool_down:
  show flora pond flora_pulled with fadehold: ## This gives the image both the fadehold (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 18
    linear 0.05 yoffset 0
  hide flora
  show flora pond flora_pulled ## This is just to avoid the screen shake if the player is skipping
  flora pond flora_pulled "Bad [cuthbert]! He pulled me in!"
  mc "Do you need help getting out?"
  show flora pond mc_help with dissolve2
  flora pond mc_help "Y-yeah."
  mc "Okay, hold on to my hand."
  flora pond mc_help "..."
  mc "[flora], what are you—"
  window hide
  play sound "falling_thud"
  play ambient "<from 0.25>audio/sound/paint_splash.ogg" volume 0.75 noloop
  show flora pond mc_pulled with vpunch
  window auto
  flora pond mc_pulled "Hehe!"
  mc "Oh, my god! That's cold!"
  show flora pond smile with dissolve2
  flora pond smile "You looked like you needed some cooling down."
  mc "True. I'm all sorts of hot for you."
  show flora pond embarrassed with dissolve2
  flora pond embarrassed "That's... that's..."
  mc "Don't worry. I won't tell anyone."
  flora pond embarrassed "You better not!"
  mc "So, how do we explain this to [jo]?"
  show flora pond smile with dissolve2
  flora pond smile "I'll let you come up with something."
  flora pond smile "After all, lying is one of your specialties."
  mc "That's rude."
  flora pond smile "Maybe, but it's also true."
  mc "Brat."
  flora pond smile "Idiot."
  "I guess some things never change..."
  "And I don't think either of us want them to."
  flora pond smile "Come on, [cuthbert], swim into the bucket. We're going back home."
  scene location with fadehold
  return
