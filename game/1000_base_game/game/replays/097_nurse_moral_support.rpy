init python:
  register_replay("nurse_moral_support","Nurse's Moral Support","replays nurse_moral_support","replay_nurse_moral_support")

label replay_nurse_moral_support:
  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Nurse"

  show nurse date_spying date with fadehold
  "Damn, the [nurse] pulled out all the stops for tonight!"
  "She looks beautiful."
  "And that dress accentuates her best features and contrasts nicely with her skin tone."
  "Mr. Brown really is a lucky son of a bitch tonight..."
  nurse date_spying date "Such a lovely evening, don't you think?"
  mrbrown "It's fine, I guess."
  nurse date_spying date "Err, yeah..."
  nurse date_spying date "So, um, did you have a nice day?"
  mrbrown "It was fine."
  "What a great conversationalist for an English teacher..."
  nurse date_spying date "Oh. That's good."
  mrbrown "Anyway, I'm going to have the wine."
  "Is he not even going to ask her about her day? Rude."
  "Even I had better manners than that in my old life."
  "The [nurse] seems a little put out, but persistent."
  "Honestly, good for her."
  # nurse date_spying date "I, err... I think I'm going to have the shrimp pancakes with ice cream..."
  nurse date_spying date "I, err... I think I'm going to have the shrimp pancakes with ice cream...{space=-25}"
  mrbrown "Huh. Are you sure?"
  mrbrown "There are some healthier options, you know."
  "Seriously, dude?"
  nurse date_spying date sad "Oh, um. Right. You're right."
  nurse date_spying date sad "Maybe I'll try the seaweed crepe instead..."
  mrbrown "So, you got all dolled up for me tonight."
  mrbrown "Does this mean you will be putting out?"
  show nurse date_spying date annoyed with dissolve2
  nurse date_spying date annoyed "Oh, my goodness!" with vpunch
  menu(side="middle"):
    extend ""
    "Intervene":
      "All right, that's enough of that."
      window hide
      pause 0.125
      show nurse date_spying confrontation with Dissolve(.5)
      pause 0.25
      window auto
      mc "Sir, you can't just talk to her like that."
      mrbrown "Excuse me? This is none of your business, kid."
      mc "I just couldn't help but overhear."
      # mc "And I really think you should treat women with more respect than that."
      mc "And I really think you should treat women with more respect than that.{space=-55}"
      mrbrown "Now, look here, you little—" with vpunch
      nurse date_spying confrontation sad "Oh, my... please don't make a scene, you two..."
      # nurse date_spying confrontation sad "I'm sorry, Mr. Brown, but I really don't think this is going to work out..."
      nurse date_spying confrontation sad "I'm sorry, Mr. Brown, but I really don't think this is going to work out...{space=-35}"
      mrbrown "Seriously? The audacity to ask me out and then treat me like this!"
      # mrbrown "And I bet you expected me to pay for dinner without anything in return, didn't you?"
      mrbrown "And I bet you expected me to pay for dinner without anything in return,{space=-65}\ndidn't you?"
      mc "Dates aren't a transactional affair..."
      mrbrown "Tsk! I knew I shouldn't have wasted my time with this nonsense."
      mrbrown "Don't expect me to ever give you another chance."
      window hide
      pause 0.125
      show nurse date_spying aftermath with Dissolve(.5)
      pause 0.25
      window auto
      nurse date_spying aftermath "Oh, goodness... that was awful..."
      mc "Good grief. What an asshole."
      mc "I'm sorry you had to put up with that."
      nurse date_spying aftermath "It's all right..."
      mc "No, it's not. You deserve more respect than that."
      nurse date_spying aftermath blush "Well, I do appreciate you standing up for me."
      nurse date_spying aftermath blush "I'm not so sure I could have done it on my own..."
      mc "I bet you could, but it was nothing, really."
      nurse date_spying aftermath blush "No, I mean it. Thank you, [mc]."
      mc "Don't mention it!"
    "Let the [nurse] handle it":
      "Mr. Brown really is something else outside the classroom, isn't he?"
      "That's not exactly gentlemanly of him... but she's an independent woman capable of dealing with this on her own."
      nurse date_spying date annoyed "What a terribly rude thing to say."
      mrbrown "Come on. We both know you'll do it."
      nurse date_spying date annoyed "I just wanted a nice evening out with some good company..."
      nurse date_spying date annoyed "...but I guess that was too much to ask for."
      "Go on, girl! You tell him!"
      mrbrown "Seriously? No need to be a bitch about it."
      nurse date_spying date annoyed "..."
      nurse date_spying date annoyed "I'm sorry, Mr. Brown, but I think we're done here."
      mrbrown "Tsk. Whatever. What a waste of time."
      window hide
      pause 0.125
      show nurse date_spying aftermath with Dissolve(.5)
      pause 0.25
      window auto
      mc "Good grief. What an asshole."
      mc "I'm sorry you had to put up with that."
      nurse date_spying aftermath "That wasn't how I envisioned tonight going..."
      mc "You did handle it really well, though. I'm impressed."
      mc "You didn't even need me!"
      nurse date_spying aftermath blush "Err, your presence might have just given me the courage I needed..."
      mc "I don't know about that. You have been getting better all week."
      nurse date_spying aftermath blush "Oh, um. Thank you."
  mc "Anyway, since Mr. Brown left..."
  menu(side="middle"):
    extend ""
    # "\"...let me buy you dinner instead, will you?\"":
    "\"...let me buy you dinner instead, will you?\"{space=-10}":
      # extend " let me buy you dinner instead, will you?"
      extend " let me buy you dinner instead, will you?{space=-55}"
      nurse date_spying aftermath "Oh, no! I couldn't possibly let you do that!"
      # mc "Why not? It's the least I can do after your date turned out to be an asshole."
      mc "Why not? It's the least I can do after your date turned out to be\nan asshole."
      nurse date_spying aftermath "That's not your fault..."
      mc "Still, I want to apologize on behalf of all men."
      # mc "Now, go ahead and order those shrimp pancakes with ice cream to your heart's content. My treat!"
      mc "Now, go ahead and order those shrimp pancakes with ice cream\nto your heart's content. My treat!"
      nurse date_spying aftermath blush "Oh, all right! Thank you, [mc]."
    "\"...let's take our leave, too, shall we?\"":
      extend " let's take our leave, too, shall we?"
      nurse date_spying aftermath "Yes, please."
      nurse date_spying aftermath "I am getting rather tired after the week we just had."
      mc "But it was fun, wasn't it?"
      nurse date_spying aftermath blush "It really was!"
      nurse date_spying aftermath blush "Thank you for getting me out of my own head. Sincerely."
      mc "Anytime! I know what it's like to feel stuck and hopeless."
      nurse date_spying aftermath blush "Exactly..."
  scene location with fadehold

  if quest.nurse_aid["name_reveal"]:
    $nurse.default_name = "Amelia"
  return
