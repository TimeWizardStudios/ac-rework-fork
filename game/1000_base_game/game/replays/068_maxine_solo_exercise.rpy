init python:
  register_replay("maxine_solo_exercise","Maxine's Solo Exercise","replays maxine_solo_exercise","replay_maxine_solo_exercise")

label replay_maxine_solo_exercise:
  show maxine electrodes_run smile with fadehold
  "When I woke up this morning, I never thought I'd be doing physical labor in exchange for curse help."
  "Life really is full of unexpected twists sometimes."
  "Hopefully, this can help [maya] and I won't keel over before then."
  maxine electrodes_run smile "Whenever you're ready."
  mc "Okay, here goes nothing..."
  window hide
  pause 0.125
  show maxine electrodes_run smile jog with Dissolve(.5)
  pause 0.125
  window auto
  "I start with a light jog, my heart rate spiking as the generator gives a little hiss of life."
  maxine electrodes_run annoyed jog "Faster!"
  mc "All right, all right!"
  show maxine electrodes_run annoyed run with dissolve2
  "Against the weak protests of my aching lungs, I take the jog to a run.{space=-25}"
  "The sweat gathers at my temples and drips down my head."
  window hide
  pause 0.125
  show maxine electrodes_run annoyed run generator_lights with Dissolve(.5)
  pause 0.125
  window auto
  "The generator kicks into gear then and [maxine]'s computer flickers to life."
  mc "Why... am I... doing this... again...?"
  show maxine electrodes_run neutral run generator_lights with dissolve2
  maxine electrodes_run neutral run generator_lights "To generate power, of course."
  mc "But... why not... use... the school... computers...?"
  maxine electrodes_run neutral run generator_lights "Technology can never be trusted."
  mc "Whatever... you say..."
  show maxine electrodes_run neutral faster_run generator_lights with dissolve2
  "I pant from the excursion, my out of shape body screaming in protest.{space=-40}"
  "My skinny legs burn, but I know I'm running towards something."
  "I'm running towards [maya], towards being a better, more selfless person."
  "I would run up any hill, power any weird [maxine] experiment to help a friend."
  maxine electrodes_run excited faster_run generator_lights "That's it! Keep going!"
  mc "I am... {i}huff...{/} trying..."
  window hide
  pause 0.125
  show maxine electrodes_run excited faster_run lights with Dissolve(.5)
  pause 0.125
  window auto
  "I pant through the pain in my lungs, and watch as her computer actually blinks awake."
  mc "It's... actually... {i}huff...{/} working...?"
  maxine electrodes_run excited faster_run lights "Of course it's working! Run, [mc], run!"
  show maxine electrodes_run excited fastest_run lights with dissolve2
  mc "I'm going... {i}huff...{/} as fast... {i}puff...{/} as I can...!"
  maxine electrodes_run neutral fastest_run lights "Your physical stamina is a bit worrisome. You should probably have that checked."
  mc "Gee... {i}huff...{/} thanks..."
  maxine electrodes_run smile fastest_run lights "You're welcome."
  "As I go, the generator picks up speed and the computer lights up\nin full."
  mc "I can't... {i}huff...{/} go... any... {i}puff...{/} more...!"
  maxine electrodes_run smile fastest_run lights "That will be sufficient for today."
  mc "Thank... {i}huff...{/} god..."
  scene location with fadehold
  return
