init python:
  register_replay("mrsl_dance","Mrs. L's Dance","replays mrsl_dance","replay_mrsl_dance")

label replay_mrsl_dance:
  #scene mrsl events LichYoga1_naked with Dissolve(.5)
  show mrsl surveillance_one with Dissolve (.5)
  "Always wondered why they put the surveillance camera in this spot, but it makes sense now. It's the perfect place to see the whole room."
  "Just people walking around. Seniors headed for the homeroom, everyone else going to the football field."
  "..."
  #SFX: rewind sound
  #SFX: rewind distortion effect
  "Here comes [lindsey] at full speed..."
  show mrsl surveillance_two with Dissolve (.5)
  "Look out!"
  show black with hpunch
  $set_dialog_mode("default_no_bg")
  "Ouch. That hurts. Damn slippery floors."
  "Her butt looked even better up close, but honestly, this is decent fapping material..."
  $set_dialog_mode()
  hide black with Dissolve (.5) 
  show mrsl surveillance_three with Dissolve (.5)
  "Clipped."
  "..."
  #SFX: rewind sound
  #SFX: rewind distortion effect
  show mrsl surveillance_four with Dissolve (.5)
  "Hey, there's [isabelle]. She's looking lost."
  "Is that a dildo in her hand? Ah, no. Just her phone."
  "She stood there for a long time, didn't she?"
  "Daydreaming about the Scottish highland perhaps."
  "..."
  "Hmm... what else is on here..."
  #SFX: rewind sound
  show mrsl surveillance_five with Dissolve (.5)
  "Ah, there's [mrsl]!"
  "As usual, flirting with the poor [guard]. Teasing him with her body."
  "Looks like the glass to the [guard]'s booth is getting all steamy."
  "She drew something on the glass. A heart? A pair of luscious lips?"
  "Hmm... seems like a longer message..."
  #$quest.mrsl_number.start() #hidden #TBD add quest?
  show mrsl surveillance_three with Dissolve (.5)
  "Okay, she left. Let's fast forward a bit..."
  "No point looking if her ass is no longer in the picture."
  #SFX: rewind sound
  #SFX: rewind distortion effect
  "..."
  show mrsl surveillance_five with Dissolve (.5)
  "Why, hello again there, [mrsl]!"
  "It's late in the day and you're still over there, bothering the [guard]."
  "He probably likes the attention, though. I know I would."
  "..."
  "The [guard] left to go upstairs for his usual rounds. Sweeping the school for potential stragglers and putting them in detention."
  "But, [mrsl]? What do you get up to after the school closes?"
  "..."
  show mrsl surveillance_three with Dissolve (.5)
  "She's walking over to the couch with a confident sway in her hips, grabbing a mop from the janitor's closet on the way."
  "..."
  "What... wait, what is she doing?"
  "Why is she stripping out of her dress?"
  show mrsl mrsl_pole_ready_frame_one with Dissolve(.5)
  show mrsl mrsl_pole_ready_frame_two with Dissolve(2)
  show mrsl mrsl_pole_ready_frame_three with Dissolve(2)
  show mrsl mrsl_poleready with Dissolve (.5)
  "..."
  "What the hell?"
  "..."
  "All right, time to pause and unzip."
  show mrsl mrsl_poletop with Dissolve (.5)
  "With the mop firmly planted on the floor, she grinds it, pole dancing around it."
  "A mischievous smile touches her lips as she slides mop up and down between her buttcheeks."
  show mrsl mrsl_poletop with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_poletop with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_poletop with Dissolve (.5)
  "Like a stripper she rides that mop, the muscles in her thighs and stomach flexing."
  "Rising to her tippy toes before plunging deep into a crouch."
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_poletop with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  "Teasing her asshole and pussy lips, leaving a sheen of her juices along the pole."
  "Maybe this is why the homeroom smells like it does?"
  "It's almost like she's gripping the shaft, squeezing it, teasing it with her most intimate areas."
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_poletop with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  "Her hair whips back and forth while her hips sway to an unheard tune."
  "Hypnotic."
  "Up and down the pole, dancing with her whole body. Pleasuring herself with the stick."
  "Licking her lips."
  "..."
  "Oh snap, the [guard] is returning."
  show mrsl mrsl_pole_bot_frame_three with Dissolve(.5)
  show mrsl mrsl_pole_bot_frame_two with Dissolve(.5)
  show mrsl mrsl_pole_bot_frame_one with Dissolve(.5)
  show mrsl surveillance_three with Dissolve (.5)
  "She saw it too, and hurried off just in time."
  "Close call..."
  #SFX: rewind sound
  #SFX: rewind distortion effect
  "All right. Next day."
  "Let's just skip forward a bit more."
  #SFX: rewind sound
  #SFX: rewind distortion effect
  show mrsl surveillance_four with Dissolve (.5)
  "There's [isabelle] again."
  "She's heading toward her locker."
  show mrsl surveillance_three with Dissolve (.5)
  "She just put her purse in and took out her sports bag."
  "All right, she left for the gym..."
  "Let's see who our thief is..."
  "..."
  "..."
  "Come on... show yourself..."
  "..."
  show mrsl lindsey_stealing1 with Dissolve (.5)
  "[lindsey]?!"
  "No way..."
  "She's the perfect princess. There's no way..."
  "How did she open [isabelle]'s locker?"
  show mrsl lindsey_stealing2 with Dissolve (.5)
  "Is she one of those rich kids who gets a kick out of stealing stuff?"
  "She has such a bright future ahead of her. Why would she risk it with something so stupid?"
  show mrsl lindsey_stealing1 with Dissolve (.5)
  "Maybe her dad has one of those harvey-specter-lawyers, so it doesn't matter if she is caught."
  "Huh. I didn't even see what she took."
  show mrsl surveillance_three with Dissolve (.5)
  "I've always felt like an outsider looking in."
  "Peeping on people has become a hobby of mine."
  "They go about their days, unknowingly watched and judged."
  "It's fascinating what you notice if you're just people-watching."
  "Girls adjusting their thongs when they think no one's looking."
  "Brad checking his wallet to make sure he's still got dough."
  "Tanner picking his nose and eating the booger."
  "Chad... ugh, he still looks like a Greek god even when no one's watching..."
  #SFX: rewind sound
  #SFX: rewind distortion effect
  "Not much happening..."
  "Just people rushing to their classes. To the cafeteria."
  "The [guard] doing his final rounds."
  "[mrsl] doing her... wait a minute..."
  show mrsl mrsl_pole_ready_frame_one with Dissolve(.5)
  show mrsl mrsl_pole_ready_frame_two with Dissolve(2)
  show mrsl mrsl_pole_ready_frame_three with Dissolve(2)        
  show mrsl mrsl_poleready with Dissolve (.5)
  "And she's got that mop again. If the janitor finds out... well, I'd like to see that conversation."
  show mrsl mrsl_poletop with Dissolve (.5)
  "She moves so casually yet sexy. Does she do this every night?"
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_polebot with Dissolve (.5)
  show mrsl mrsl_polemid with Dissolve (.5)
  show mrsl mrsl_poletop with Dissolve (.5)
  "Last time she was interrupted, but now... now she settles on the couch."
  show mrsl mrsl_couchmastpre with Dissolve (.5)
  "Slides into position. Spreading her legs like she wants it."
  "The contrast of missionary position and her sinful underwear is something out of a wet dream."
  "The way she teases the camera; it's almost like she knows someone's watching."
  "Playfully tugging at her underwear. Playfully winking at the camera?"
  show mrsl mrsl_couchmastready with Dissolve (.5)
  "Her fingers pull her panties to the side, exposing her flesh flower."
  "The clit piercing steals the show as she rubs it between her fingertips."
  "Biting her lip. Roughly flicking her little bean. Dark passion building inside her."
  "With neon embers burning in her eyes, she lines the tip of the mop up with her pussy."
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  "An inaudible gasp escapes her lips as the rough tip settles against her soft flesh."
  "A shiver of anticipation pricks the skin of her open thighs."
  "The muscles in her abdomen twitch, ready for the inevitable penetration."
  "Ready to take the shaft deep."
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  "Eyes closed, she pushes the tip inside."
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  "Her vagina provides the required lubrication... or perhaps it doesn't, but she likes the slight friction tearing at her walls."
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  "Slowly, with a steady rhythm she pushes the shaft deeper, only to let it back out again."
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  "Her hand finds her nipple, squeezing it between sharp nails, sending jolts of cruel pain and heightened pleasure into her brain."
  "Fingers tugging at her piercings, bullying the tortured nub."
  "Hey eyes light up, as the sensations of the masochism rolls through her, connecting with the sharp depravity of the sadistic act."
  "Of course, someone like her would enjoy both ends of the kink spectrum."
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  "Deeper. As deep as the shaft will go. Teasing the entrance of her cervical canal."
  "A gasp of pain undoubtedly shatters the silence of the entrance hall."
  "Perhaps that was a little too deep?"
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  "But perhaps... perhaps it was just a warmup?"
  "She wants another try. To feel full. To grip the shaft with the muscles in her pussy. Squeeze it. Milk it."
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  "Beads of sweat glitter on her skin." 
  "The blush on her cheeks spreads like wildfire through her body."
  "Her labia shudders like jelly."
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  "Deep strokes. Fingers playing with her clit and nipple piercings."    
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  "She's taking her time, despite rapidly approaching her climax."
  "Enjoying the janitor's mop to the fullest."
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasttip_eyes with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasthalf_nipple with Dissolve (.5)
  show mrsl mrsl_couchmastfull with Dissolve (.5)
  show mrsl mrsl_couchmasttip with Dissolve (.5)
  show mrsl mrsl_couchmasthalf with Dissolve (.5)
  "Suddenly, her entire body tenses up. Her eyes roll back, as the waves of pleasure roll through her."
  "Shudders. Muscles spasming to contain the raw sexual energy trying to rip out of her."
  show mrsl mrsl_couchmastsquirt with Dissolve (.5)
  "Her chest heaves as the juices squirt out of her, spraying the couch dark with her sin."
  "Wave after wave. Gasp after gasp. Each shuddering spasm tossing her body around."
  "Completely spent, she lies there, feeling the pleasure ebb out of her body."
  "Never thought I'd see something like this outside of Pornhub..."
  "It's like a dream come true. Only it's not a dream. At least I hope not."
  "Fuck. I wouldn't mind watching that again, but..."
  "[flora]'s always snooping, and I could get into some serious trouble if anyone finds out I stole the school's surveillance footage."
  hide mrsl mrsl_couchmastsquirt with Dissolve (.5)
  "I'll just clip the part with [lindsey] stealing and put it on my phone for now."
  "The rest of the tape has to go..." 
  "What a truly painful loss of what can only be considered art."
  scene location with fadehold
#  ## OR close phone and scene will be restored automatically
#  $game.ui.current_phone_app=None
  return
