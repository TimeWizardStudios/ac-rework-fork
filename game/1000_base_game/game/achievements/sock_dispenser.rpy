init python:
  class Achievement_sock_dispenser(Achievement):

    def title(self):
      if self.value:
        return "Sock Dispenser"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Socks for all. No more barefoot servants."
      else:
        return "???"

    def unlocked_message(self):
      return ""
