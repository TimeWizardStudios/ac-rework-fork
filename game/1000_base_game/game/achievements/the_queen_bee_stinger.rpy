init python:
  class Achievement_the_queen_bee_stinger(Achievement):

    def title(self):
      if self.value:
        return "The Queen Bee's Stinger"
      else:
        return "???"

    def description(self):
      if self.value:
        return "The poison enters the bloodstream, corrupting the senses one by one."
      else:
        return "???"
