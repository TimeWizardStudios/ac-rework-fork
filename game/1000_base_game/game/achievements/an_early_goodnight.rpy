init python:
  class Achievement_an_early_goodnight(Achievement):

    def title(self):
      if self.value:
        return "An Early Goodnight"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Does anyone need a mop? Maybe it's too early to tell..."
      else:
        return "???"
