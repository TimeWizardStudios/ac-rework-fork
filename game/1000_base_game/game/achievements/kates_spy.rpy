init python:
  class Achievement_kates_spy(Achievement):
    def title(self):
      if self.value:
        return "[kate]'s Spy"
      else:
        return "???"
    def description(self):
      if self.value:
        return "You're not proud of what you've done. No one is."
      else:
        return "???"
