init python:
  class Achievement_the_mark_of_shame(Achievement):
    def title(self):
      if self.value:
        return "The Mark of Shame"
      else:
        return "???"
    def description(self):    
      if self.value:
        return "So either you cheated or you have way too much time on your hands. Both are disgraceful. Wear your mark of shame proudly!"
      else:
        return "???"       
