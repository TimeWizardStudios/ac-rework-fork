init python:
  class Achievement_offer_refused(Achievement):

    def title(self):
      if self.value:
        return "Offer Refused"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Can't barter without cash. You've refused the offer of the Codfather."
      else:
        return "???"
