init python:
  class Achievement_standing_up(Achievement):
    def title(self):
      if self.value:
        return "Standing up"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Your spine is developing nicely. Soon, you'll be able to stand upright like other Homosapien."
      else:
        return "???"
