init python:
  class Achievement_hand_of_fate(Achievement):

    def title(self):
      if self.value:
        return "Hand of Fate"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Violence is oftentimes the big changer of fates."
      else:
        return "???"
