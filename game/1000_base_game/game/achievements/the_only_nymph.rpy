init python:
  class Achievement_the_only_nymph(Achievement):

    def title(self):
      if self.value:
        return "The Only Nymph"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Alone in the forest, the nymph threw shy glances over his shoulder."
      else:
        return "???"
