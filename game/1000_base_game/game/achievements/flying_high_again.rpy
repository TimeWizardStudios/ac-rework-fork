init python:
  class Achievement_flying_high_again(Achievement):
    def title(self):
      if self.value:
        return "Flying High Again"
      else:
        return "???"
    def description(self):
      if self.value:
        return "If you could be inside my head, you'd see that black and white is red."
      else:
        return "???"
