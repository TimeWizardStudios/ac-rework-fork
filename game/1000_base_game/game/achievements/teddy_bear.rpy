init python:
  class Achievement_teddy_bear(Achievement):
    def title(self):
      if self.value:
        return "Teddy Bear"
      else:
        return "???"
    def description(self):
      if self.value:
        return "People call you Teddy because you're so huggable. Or maybe it's because you're somewhat skimpy and perverted."
      else:
        return "???"
    def unlocked_message(self):
      return ""
