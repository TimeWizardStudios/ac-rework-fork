init python:
  class Achievement_the_queen_bee_decree(Achievement):

    def title(self):
      if self.value:
        return "The Queen Bee's Decree"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Down at the Ninth Circle, right next to Mr. Iscariot."
      else:
        return "???"
