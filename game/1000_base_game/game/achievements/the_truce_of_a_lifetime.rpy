init python:
  class Achievement_the_truce_of_a_lifetime(Achievement):

    def title(self):
      if self.value:
        return "The Truce of a Lifetime"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Some battles should only exist in the mind."
      else:
        return "???"
