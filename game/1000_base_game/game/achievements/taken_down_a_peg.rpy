init python:
  class Achievement_taken_down_a_peg(Achievement):
    def title(self):
      if self.value:
        return "Taken Down a Peg"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Humiliating, but also hot. Taking it deep for [jacklyn]."
      else:
        return "???"
