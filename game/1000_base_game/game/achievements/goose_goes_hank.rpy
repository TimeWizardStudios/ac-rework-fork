init python:
  class Achievement_goose_goes_hank(Achievement):
    def title(self):
      if self.value:
        return "Goose Goes, Hank!"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Your name is actually Hank. You didn't restart the game just to unlock this achievement. You're that one golden goose. No hanky-panky."
      else:
        return "???"
    def unlocked_message(self):
      return ""
