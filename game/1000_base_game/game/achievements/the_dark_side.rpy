init python:
  class Achievement_the_dark_side(Achievement):
    def title(self):
      if self.value:
        return "The Dark Side"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Some philosophers may call it morally questionable... but [kate] is way hotter than those nerds."
      else:
        return "???"
