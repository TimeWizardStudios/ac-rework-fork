init python:
  class Achievement_call_me_beep_me(Achievement):
    def title(self):
      if self.value:
        return "Call Me, Beep Me!"
      else:
        return "???"
    def description(self):
      if self.value:
        return "In danger and trouble, I'm there on the double. Inside the air duct, that is."
      else:
        return "???"
