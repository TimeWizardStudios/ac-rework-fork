init python:
  class Achievement_riddle_me_this(Achievement):

    def title(self):
      if self.value:
        return "Riddle Me This"
      else:
        return "???"

    def description(self):
      if self.value:
        return "You never considered it before, but now you know. Tseb eht si enixam."
      else:
        return "???"
