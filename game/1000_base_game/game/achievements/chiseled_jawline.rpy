init python:
  class Achievement_chiseled_jawline(Achievement):

    def title(self):
      if self.value:
        return "Chiseled Jawline"
      else:
        return "???"

    def description(self):
      if self.value:
#       return "Marbleous! Monumental GigaChad moment."
        return "Marbleous! Monumental GigaChad{space=-10}\nmoment."
      else:
        return "???"
