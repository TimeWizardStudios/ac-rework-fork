init python:
  class Achievement_leave_in_peace(Achievement):
    def title(self):
      if self.value:
        return "We Leave in Peace"
      else:
        return "???"
    def description(self):
      if self.value:
        return "I'm not saying it was aliens, but..."
      else:
        return "???"