init python:
  class Achievement_putting_things_together(Achievement): 
    def title(self):
      if self.value:
        return "Putting Things Together"
      else:
        return "???"
    def description(self):
      if self.value:
        return "It's like peanut butter and jelly, cookies and milk, yo' mama and all of the above. Some things just work well together."
      else:
        return "???"
