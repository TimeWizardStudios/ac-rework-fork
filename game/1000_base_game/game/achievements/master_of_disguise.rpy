init python:
  class Achievement_master_of_disguise(Achievement):

    def title(self):
      if self.value:
        return "Master of Disguise"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Fool you once, fool you twice, fool you thrice. Always the fool."
      else:
        return "???"
