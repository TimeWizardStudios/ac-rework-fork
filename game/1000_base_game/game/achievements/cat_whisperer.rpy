init python:
  class Achievement_cat_whisperer(Achievement):

    def title(self):
      if self.value:
        return "Cat Whisperer"
      else:
        return "???"

    def description(self):
      if self.value:
        return "A sense of smell and a silver tongue? Felines moisten in your presence."
      else:
        return "???"
