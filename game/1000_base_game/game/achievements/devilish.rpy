init python:
  class Achievement_devilish(Achievement):
    def title(self):
      if self.value:
        return "Devilish"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Even with the alarm blaring in your ears, you went straight for the porn magazine. That's dedication."
      else:
        return "???"
