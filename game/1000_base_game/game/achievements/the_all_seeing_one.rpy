init python:
  class Achievement_the_all_seeing_one(Achievement):

    def title(self):
      if self.value:
        return "The All-Seeing One"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Nothing slips past you. Not even [maxine]'s nefarious plans."
      else:
        return "???"
