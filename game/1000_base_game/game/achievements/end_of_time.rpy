init python:
  class Achievement_end_of_time(Achievement):
    def title(self):
      if self.value:
        return "End of Time"
      else:
        return "???"
    def description(self):
      if self.value:
        return "They say that time eventually runs out for everyone, but you've certainly made a point of crippling it."
      else:
        return "???"
    def unlocked_message(self):
      return ""
