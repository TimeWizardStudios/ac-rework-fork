init python:
  class Achievement_lord_of_the_stones(Achievement):

    def title(self):
      if self.value:
        return "Lord of the Stones"
      else:
        return "???"

    def description(self):
      if self.value:
        return "So smooth, the rest of the world is crooked and uneven."
      else:
        return "???"
