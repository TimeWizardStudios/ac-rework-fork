init python:
  class Achievement_inspired(Achievement):
    def title(self):
      if self.value:
        return "Inspired"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Inspiration is a fickle mistress. But you're just happy to be in the same sentence as the word \"mistress.\""
      else:
        return "???"
