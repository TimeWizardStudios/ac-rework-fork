init python:
  class Achievement_philanthropist(Achievement):
    def title(self):
      if self.value:
        return "Philanthropist"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Who knew you were such a do-gooder? Now, what's the ulterior motive?"
      else:
        return "???"
    def unlocked_message(self):
      return ""
