init python:
  class Achievement_the_right_kind_of_bust(Achievement):
    def title(self):
      if self.value:
        return "The Right Kind of Bust"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Art is life, and you're living proof."
      else:
        return "???"
    def unlocked_message(self):
      return ""
