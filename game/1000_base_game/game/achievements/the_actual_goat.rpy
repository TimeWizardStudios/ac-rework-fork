init python:
  class Achievement_the_actual_goat(Achievement):

    def title(self):
      if self.value:
        return "The Actual Goat"
      else:
        return "???"

    def description(self):
      if self.value:
        return "Definitely not a sheep. You can touch it as much as you like. It won't explode."
      else:
        return "???"
