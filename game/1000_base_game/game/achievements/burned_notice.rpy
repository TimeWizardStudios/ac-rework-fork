init python:
  class Achievement_burned_notice(Achievement):
    def title(self):
      if self.value:
        return "Burned Notice"
      else:
        return "???"
    def description(self):
      if self.value:
        return "No guard rails, wholesales, or paper trails. You're the spy that didn't get burned."
      else:
        return "???"
    def unlocked_message(self):
      return ""
