init python:
  class Achievement_top_hat(Achievement):
    def title(self):
      if self.value:
        return "Top Hat"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Is that a hat or a hot air balloon? Either way, it's a perfect fit for your head."
      else:
        return "???"
    def unlocked_message(self):
      return ""
