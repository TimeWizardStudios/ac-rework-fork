init python:
  class Achievement_breaking_all_the_rules(Achievement):
    def title(self):
      if self.value:
        return "Breaking All the Rules"
      else:
        return "???"
    def description(self):
      if self.value:
        return "Stop in the name of love! Even if it's filthy and taboo."
      else:
        return "???"
