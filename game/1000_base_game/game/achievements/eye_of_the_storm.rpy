init python:
  class Achievement_eye_of_the_storm(Achievement):
    def title(self):
      if self.value:
        return "Eye of the Storm"
      else:
        return "???"
    def description(self):
      if self.value:
        return "In the typhoon that is the chaos of life, you're the eye of the storm. Calm and centered."
      else:
        return "???"
    def unlocked_message(self):
      return ""
