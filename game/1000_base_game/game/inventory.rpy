init python:
  def combine_filter(item1,item2):
    if isinstance(item1,basestring):
      item1=items_by_id[item1]
    if isinstance(item2,basestring):
      item2=items_by_id[item2]
    recipe=(item1.id,item2.id)
    if recipe in game.failed_recipes:
      if recipe not in recipes_by_ingredients:
        return "gray"
      if item1==item2 and mc.owned_item_count(item1)==1:
        return "grey"

label equip_item(item,char):
  $char.equip(item)
  return

label unequip_item(item,char):
  $char.unequip(item)
  return

label combine_items(item1,item2,silent=False,return_to_inventory=True):
  python:
    if isinstance(item1,basestring):
      item1=items_by_id[item1]
    if isinstance(item2,basestring):
      item2=items_by_id[item2]
    combine_result=recipes_by_ingredients.get((item1.id,item2.id))
  if item1==item2 and mc.owned_item_count(item1)==1:
    $msg="Combining {color=#900}[item1]{/color} with itself would break the metaphysical laws that govern the universe. It's too early in the day for that."
    $game.failed_recipes.add((item1.id,item2.id))
  elif combine_result is None:
    $msg="Combining {color=#900}[item1]{/color} with {color=#900}[item2]{/color} is not only futile, it is dangerous."
    $game.failed_recipes.add((item1.id,item2.id))
    $game.failed_recipes.add((item2.id,item1.id))
  elif renpy.has_label(combine_result):
    call expression combine_result pass (item1,item2) from _call_expression_1
    $msg=None
  else:
    python:
      combine_result=items_by_id[combine_result]
      mc.remove_item(item1)
      mc.remove_item(item2)
      process_event("items_combined",item1,item2,combine_result,silent)
      mc.add_item(combine_result, silent = True)
      msg=None
  if msg:
    $game.notify_modal(None,"Combine",msg,10.0)
  if return_to_inventory:
    return "inventory"
  return

label select_inventory_item(title,on_select_label,on_select_label_args=None,return_result=None,*args):
  call screen inventory(title)
  hide screen element_hint
  hide screen active_element
  if isinstance(_return,basestring):
    $_return=items_by_id[_return]
    if return_result:
      return [on_select_label,tuple(on_select_label_args or tuple())+(_return,)]
    else:
      call expression on_select_label pass (*(on_select_label_args or tuple())+(_return,)) from _call_expression_2
  return _return

label inventory:
  while True:
    $game.ui.hide_hud=0
    $game.ui.hud_active=False
    call screen inventory
    hide screen element_hint
    hide screen active_element
    if isinstance(_return,basestring):
      call expression _return from _call_expression_3
    elif isinstance(_return,(list,tuple)):
      call expression _return[0] pass (*_return[1]) from _call_expression_4
    else:
      return _return
    if isinstance(_return,basestring):
      call expression _return from _call_expression_5
    elif isinstance(_return,(list,tuple)):
      call expression _return[0] pass (*_return[1]) from _call_expression_6
    elif _return==True:
      jump inventory
    return
