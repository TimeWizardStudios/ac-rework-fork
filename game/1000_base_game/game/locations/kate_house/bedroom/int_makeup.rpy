init python:
  class Interactable_kate_house_bedroom_makeup(Interactable):

    def title(cls):
      return "Makeup"

    def description(cls):
#     return "Products to conceal horns, tails, and red skin."
      return "Products to conceal horns,\ntails, and red skin."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_makeup_interact")


label kate_house_bedroom_makeup_interact:
  "[kate] doesn't need makeup, but uses it to flex on the less attractive masses."
  if not kate_house_bedroom["makeup_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["makeup_interacted"] = True
  return
