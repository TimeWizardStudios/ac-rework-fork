init python:
  class Interactable_kate_house_bedroom_dresser(Interactable):

    def title(cls):
      return "Dresser"

    def description(cls):
      return "This must be where [kate] keeps the souls of all her victims."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_dresser_interact")


label kate_house_bedroom_dresser_interact:
  "Strange, unfamiliar, and possibly evil smells hang heavy over this dresser — roses, sweet fruits, and dread."
  "...although, the last one might be me."
  if not kate_house_bedroom["dresser_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["dresser_interacted"] = True
  return
