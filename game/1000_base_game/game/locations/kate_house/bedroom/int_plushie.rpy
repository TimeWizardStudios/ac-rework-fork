init python:
  class Interactable_kate_house_bedroom_plushie(Interactable):

    def title(cls):
      return "Plushie"

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_plushie_interact")


label kate_house_bedroom_plushie_interact:
  $kate_house_bedroom["plushie_moved"] = True
  $kate_house_bedroom["interactables"]+=1
  if kate_house_bedroom["interactables"] == 8:
    pause 0.5
  return
