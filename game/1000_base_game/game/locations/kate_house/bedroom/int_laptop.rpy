init python:
  class Interactable_kate_house_bedroom_laptop(Interactable):

    def title(cls):
      return "Laptop"

    def description(cls):
      return "An instrument of online cruelty and cyber bullying."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_laptop_interact")


label kate_house_bedroom_laptop_interact:
  "The constant pings of new messages and friend requests really makes you feel unpopular in comparison..."
  if not kate_house_bedroom["laptop_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["laptop_interacted"] = True
  return
