init python:
  class Interactable_kate_house_bedroom_framed_picture(Interactable):

    def title(cls):
      return "Framed Picture"

    def description(cls):
      return "A picture of a young Satan playing with Cerberus at the Gates of Hell."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_framed_picture_interact")


label kate_house_bedroom_framed_picture_interact:
  "Everyone has a facade that they show the world."
  "This picture is living proof of that."
  if not kate_house_bedroom["framed_picture_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["framed_picture_interacted"] = True
  return
