init python:
  class Interactable_kate_house_bedroom_bed(Interactable):

    def title(cls):
      return "Bed"

    def description(cls):
#     return "Even her bed smells nice. And by \"nice,\" I mean \"evil.\""
      return "Even her bed smells nice.\nAnd by \"nice,\" I mean \"evil.\""

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_bed_interact")


label kate_house_bedroom_bed_interact:
# "I wonder if [kate] ever sleeps with guys here or if this is like her private sanctum..."
  "I wonder if [kate] ever sleeps with guys here or if this is like her private{space=-35}\nsanctum..."
  "What if I'm the first guy to ever set foot in here?"
  if not kate_house_bedroom["bed_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["bed_interacted"] = True
  return
