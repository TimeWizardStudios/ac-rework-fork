init python:
  class Interactable_kate_house_bedroom_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
#     return "Even her door is perfect and looks expensive..."
      return "Even her door is perfect\nand looks expensive..."

    def actions(cls,actions):
      actions.append(["go","Leave","?kate_house_bedroom_door_go"])


label kate_house_bedroom_door_go:
  "I've come this far. No chickening out now."
  if not kate_house_bedroom["door_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["door_interacted"] = True
  return
