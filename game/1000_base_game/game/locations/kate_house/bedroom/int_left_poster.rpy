init python:
  class Interactable_kate_house_bedroom_left_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
      return "A lesbian modern day cult film."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_left_poster_interact")


label kate_house_bedroom_left_poster_interact:
# "When Beatrice Le Biddo has an embarrassing orgasm at the altar on her wedding day at the hands of the viciously beautiful Wilhelmina \"Bill\" Fresh, there's only one thing left to do..."
  "When Beatrice Le Biddo has an embarrassing orgasm at the altar on{space=-10}\nher wedding day at the hands of the viciously beautiful Wilhelmina \"Bill\" Fresh, there's only one thing left to do..."
  "{i}Jill Bill.{/}"
  if not kate_house_bedroom["left_poster_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["left_poster_interacted"] = True
  return
