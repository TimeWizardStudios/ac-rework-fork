init python:
  class Interactable_kate_house_bedroom_lamp(Interactable):

    def title(cls):
      return "Lamp"

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_lamp_interact")


label kate_house_bedroom_lamp_interact:
  $kate_house_bedroom["lamp_moved"] = True
  $kate_house_bedroom["interactables"]+=1
  if kate_house_bedroom["interactables"] == 8:
    pause 0.5
  return
