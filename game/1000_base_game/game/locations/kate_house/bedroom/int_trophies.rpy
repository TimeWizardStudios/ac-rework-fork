init python:
  class Interactable_kate_house_bedroom_trophies(Interactable):

    def title(cls):
      return "Trophies"

    def description(cls):
      return "Countless trophies from cheerleading contests. Countless."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_trophies_interact")


label kate_house_bedroom_trophies_interact:
  "Yep, [kate] is the best at everything she does."
  "She doesn't even need these trophies to prove it."
  if not kate_house_bedroom["trophies_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["trophies_interacted"] = True
  return
