init python:
  class Location_kate_house_bedroom(Location):

    default_title = "Kate's Room"

    def scene(self,scene):
      scene.append([(0,0),"kate_house bedroom background"])

      if game.season == 1:
        scene.append([(0,0),"kate_house bedroom balcony_door",("kate_house_bedroom_balcony_door",112,1000)])
      elif game.season == 2:
        scene.append([(0,0),"kate_house bedroom balcony_door_autumn",("kate_house_bedroom_balcony_door",112,1000)])
      scene.append([(0,0),"#kate_house bedroom curtain"])

      scene.append([(830,228),"kate_house bedroom left_poster","kate_house_bedroom_left_poster"])
      scene.append([(1014,201),"kate_house bedroom mid_left_poster","kate_house_bedroom_mid_left_poster"])
      scene.append([(743,465),"kate_house bedroom dresser","kate_house_bedroom_dresser"])
      scene.append([(764,443),"kate_house bedroom trophies","kate_house_bedroom_trophies"])
      scene.append([(912,531),"kate_house bedroom laptop",("kate_house_bedroom_laptop",26,0)])
      scene.append([(1023,558),"kate_house bedroom makeup",("kate_house_bedroom_makeup",16,0)])
      scene.append([(1296,322),"kate_house bedroom door","kate_house_bedroom_door"])
      scene.append([(1680,663),"kate_house bedroom back_nightstand"])
      if kate_house_bedroom["lamp_moved"]:
        scene.append([(1552,108),"kate_house bedroom right_poster_alt",("kate_house_bedroom_right_poster",-10,620)])
        scene.append([(1716,501),"kate_house bedroom lamp_moved"])
      else:
        scene.append([(1552,108),"kate_house bedroom right_poster",("kate_house_bedroom_right_poster",-10,620)])
        scene.append([(1793,501),"kate_house bedroom lamp","kate_house_bedroom_lamp"])
      scene.append([(1862,643),"kate_house bedroom alarm"])

      scene.append([(783,677),"kate_house bedroom footstool"])
      scene.append([(907,659),"kate_house bedroom bed","kate_house_bedroom_bed"])
      if kate_house_bedroom["plushie_moved"]:
        scene.append([(1564,813),"kate_house bedroom plushie_moved"])
      else:
        scene.append([(1556,743),"kate_house bedroom plushie","kate_house_bedroom_plushie"])
      if kate_house_bedroom["slippers_moved"]:
        scene.append([(985,1037),"kate_house bedroom slippers_moved"])
      else:
        scene.append([(920,1018),"kate_house bedroom slippers","kate_house_bedroom_slippers"])
      scene.append([(1669,987),"kate_house bedroom front_nightstand"])
      scene.append([(1740,898),"kate_house bedroom framed_picture",("kate_house_bedroom_framed_picture",-29,0)])

      scene.append([(0,0),"#kate_house bedroom overlay"])
      scene.append([(59,699),"#kate_house bedroom window_light"])


label goto_kate_house_bedroom:
  call goto_with_black(kate_house_bedroom)
  return
