init python:
  class Interactable_kate_house_bedroom_mid_left_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
#     return "These posters all show the same actress. She must be [kate]'s biggest idol."
      return "These posters all show\nthe same actress. She must be\n[kate]'s biggest idol."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_mid_left_poster_interact")


label kate_house_bedroom_mid_left_poster_interact:
  "{i}The Devil Wears Nada.{/}"
  "A critically acclaimed masterpiece."
  if not kate_house_bedroom["mid_left_poster_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["mid_left_poster_interacted"] = True
  return
