init python:
  class Interactable_kate_house_bedroom_slippers(Interactable):

    def title(cls):
      return "Slippers"

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_slippers_interact")


label kate_house_bedroom_slippers_interact:
  $kate_house_bedroom["slippers_moved"] = True
  $kate_house_bedroom["interactables"]+=1
  if kate_house_bedroom["interactables"] == 8:
    pause 0.5
  return
