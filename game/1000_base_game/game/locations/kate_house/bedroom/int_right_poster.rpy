init python:
  class Interactable_kate_house_bedroom_right_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
#     return "The actress is beautiful and confident. In this movie, a femme fatale of sorts."
      return "The actress is beautiful\nand confident. In this movie,\na femme fatale of sorts."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_right_poster_interact")


label kate_house_bedroom_right_poster_interact:
  "{i}Legally Boned.{/}"
  "Damn. Signed by the lead actress."
  if not kate_house_bedroom["right_poster_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["right_poster_interacted"] = True
  return
