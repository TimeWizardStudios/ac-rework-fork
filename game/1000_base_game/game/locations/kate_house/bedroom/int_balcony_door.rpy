init python:
  class Interactable_kate_house_bedroom_balcony_door(Interactable):

    def title(cls):
      return "Balcony Door"

    def description(cls):
      return "The Newfall Bay stretches out into the distance. Nothing but the endless ocean and sky."

    def actions(cls,actions):
      actions.append("?kate_house_bedroom_balcony_door_interact")


label kate_house_bedroom_balcony_door_interact:
  "With a view like this, how could you ever be unhappy?"
  if not kate_house_bedroom["balcony_door_interacted"]:
    $kate_house_bedroom["interactables"]+=1
    $kate_house_bedroom["balcony_door_interacted"] = True
  return
