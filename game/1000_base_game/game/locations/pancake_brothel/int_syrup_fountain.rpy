init python:
  class Interactable_pancake_brothel_syrup_fountain(Interactable):

    def title(cls):
      return "Syrup Fountain"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "{i}\"Caution: Hot, sticky mess.\"\n\n\"The syrup, too.\"{/}"


    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_syrup_fountain_interact")


label pancake_brothel_syrup_fountain_interact:
  "A fountain of syrup."
  "It's the foundation of fondue."
  "The pinnacle of pancakes."
  "The waterfall of waffles."
  "A beacon of modern genius and deliciousness."
  $quest.jacklyn_town["interactables"].add("syrup_fountain")
  return
