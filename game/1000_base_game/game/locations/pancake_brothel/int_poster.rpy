init python:
  class Interactable_pancake_brothel_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        # return "Yummy in every sense of the word, since 1798."
        return "Yummy in every sense of\nthe word, since 1798."

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_poster_interact")


label pancake_brothel_poster_interact:
  # "The ingenious idea of mixing sweet food with sweet women has been passed down through generations."
  "The ingenious idea of mixing sweet food with sweet women has been{space=-25}\npassed down through generations."
  # "The firstborn daughter of each generation is named Tam and is fated to uphold the tradition."
  "The firstborn daughter of each generation is named Tam and is fated{space=-20}\nto uphold the tradition."
  return
