init python:
  class Interactable_pancake_brothel_mystery_woman(Interactable):

    def title(cls):
      return "Mystery Woman"

    def actions(cls,actions):
      if quest.jacklyn_town == "pancake_brothel" and all(interactable in quest.jacklyn_town["interactables"] for interactable in ("door","syrup_fountain","bar_counter","handcuffs","employee_sign","pink_feather_boa")):
        actions.append(["quest","Quest","pancake_brothel_mystery_woman_interact"])


label pancake_brothel_mystery_woman_interact:
  "Oh? That's a pretty hat on a pretty la—"
  "..."
  "Hang on a second..."
  "I know that pretty lady!"
  window hide
  $flora["outfit_stamp"] = flora.outfit
  $flora.outfit = {"panties":"flora_striped_panties", "bra":"flora_purple_bra", "shirt":"flora_trench_coat", "hat":"flora_sun_hat"}
  show flora sad with Dissolve(.5)
  window auto show
  show flora afraid with dissolve2
  flora afraid "Aaaah!" with vpunch
  mc "[flora]?! What the hell are you doing here?"
  flora embarrassed "Ugh, how did you even find me?"
  mc "You're not the most inconspicuous in that hat."
  mc "But that's not the point. Answer the question!"
  flora skeptical "Shouldn't you be with your date?"
  mc "Stop answering my questions with questions!"
  if quest.jacklyn_romance["flora_sex"]:
    mc "And anyway, it's not even a date, okay?"
    if quest.jacklyn_romance["i_wont_go"]:
      mc "I promise I'm just here for the art."
      mc "I'm trying to be more worldly, but I still didn't want to upset you."
      mc "I'm really sorry it backfired."
    else:
      mc "I told you, I'm here for the art."
    flora skeptical "A likely story."
    flora skeptical "I saw the way you've been ogling [jacklyn] in that dress all night."
    "Uh-oh. Have I?"
    # "[jacklyn] does look pretty stunning right now... and I'm a man with eyes..."
    "[jacklyn] does look pretty stunning right now... and I'm a man with eyes...{space=-80}"
    "I can't help it if they linger every now and then, right?"
    mc "[flora], are you jealous?"
    if quest.jacklyn_romance["i_wont_go"]:
      flora angry "Of course I'm jealous!"
      flora angry "You're a dirty liar, [mc], and I won't ever forgive you for this!"
      "Crap. She sounds serious."
      mc "[flora], please..."
      flora concerned "Whatever."
      flora concerned "Maybe I just wanted to get out and enjoy some art, too."
    else:
      flora angry "No, I'm not!"
      flora angry "I just wanted to get out and enjoy some art too, okay?"
  else:
    mc "And stop spying on my date. Got it?"
    flora concerned "This is a public place."
    flora concerned "I can go wherever I please and look at whatever I please."
    mc "Right. Namely, [jacklyn]."
    flora confident "Pfft! You don't know what you're talking about."
    mc "I'm pretty sure I know exactly what I'm talking about."
    flora angry "I just wanted to get out and enjoy some art too, okay?"
  mc "Come on, now. I know a lie when I see one."
  flora angry "Ugh! I don't need to stand here and take this!"
  window hide
  show flora angry at disappear_to_left(.75)
  pause 0.75
  play sound "<from 9.2 to 10.2>door_breaking_in"
  show location with hpunch
  pause 0.25
  window auto
  if quest.jacklyn_romance["i_wont_go"]:
    "Welp. I may have well and truly fucked it up with [flora]."
  else:
    "Jesus. What the hell was that all about?"
  "She flew out of here like she'd literally been burned..."
  "And were those tears in her eyes?"
  $quest.jacklyn_town["interactables"].add("mystery_woman")
  return
