init python:
  class Interactable_pancake_brothel_pink_feather_boa(Interactable):

    def title(cls):
      return "Pink Feather Boa"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        # return "Boa constrict-her... makes all the sense."
        return "{i}Boa constrict-her...{/}\n\nIt makes all the sense."

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_pink_feather_boa_interact")


label pancake_brothel_pink_feather_boa_interact:
  # "Oh? I wonder what this classy piece of women's wear from a bygone era is doing here?"
  "Oh? I wonder what this classy piece of women's wear from a bygone{space=-15}\nera is doing here?"
  $quest.jacklyn_town["interactables"].add("pink_feather_boa")
  return
