init python:
  class Interactable_pancake_brothel_handcuffs(Interactable):

    def title(cls):
      return "Handcuffs"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "Even Houdini would have a hard time slipping out of these."

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_handcuffs_interact")


label pancake_brothel_handcuffs_interact:
  "Err, did someone get arrested here recently, or...?"
  $quest.jacklyn_town["interactables"].add("handcuffs")
  return
