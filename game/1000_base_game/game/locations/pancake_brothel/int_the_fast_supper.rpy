init python:
  class Interactable_pancake_brothel_the_fast_supper(Interactable):

    def title(cls):
      return "\"The Fast Supper\""

    def description(cls):
      if quest.jacklyn_town == "gallery":
        # return "I could really go for some of this mouthwatering goodness now...\n\nPussy or actual taco, I'm not picky."
        return "I could really go for some of this mouthwatering goodness now...\n\nPussy or actual taco,\nI'm not picky."


    def actions(cls,actions):
      if quest.jacklyn_town == "gallery":
        actions.append("?pancake_brothel_the_fast_supper_interact")


label pancake_brothel_the_fast_supper_interact:
  show misc the_fast_supper with Dissolve(.5)
  pause 0.125
  "Okay, now this one looks my speed!"
  "With those juicy tacos, burgers, and pizzas..."
  "The way these dudes are just stuffing their faces..."
  "Grease running down their chins..."
  mc "I really like this piece."
  jacklyn "It is pretty lush."
  jacklyn "Just look at the way they're salting the pork."
  mc "Pork tacos, you think?"
  mc "Mmm... I'm getting hungry."
  jacklyn "Your sense of humor is a split and a half, [mc]!"
  mc "Err, thanks?"
  "I think? Although I was being serious..."
  # "A bunch of dudes going to town on plates of fast food is making my mouth water."
  "A bunch of dudes going to town on plates of fast food is making my{space=-5}\nmouth water."
  jacklyn "The statement in this one is a spicy cumshot."
  jacklyn "Except I don't know if I hardcore agree with it, you know?"
  jacklyn "The supposed crude culture, or lack thereof, to the artist and the euphemism."
  # jacklyn "Reducing the beauty of the human body to nothing more than something to be consumed."
  jacklyn "Reducing the beauty of the human body to nothing more than something{space=-95}\nto be consumed."
  mc "What? I mean, yeah! Totally."
  mc "That is quite the, err... viewpoint."
  mc "I first thought this piece was a statement about consumerism and the all-consuming greed of capitalism."
  jacklyn "That's a hot take. I respect that."
  jacklyn "I don't know about you, but I love me a good taco."
  "Oh? Does she now?"
  jacklyn "A juicy... succulent... taco."
  "Right..."
  mc "Agreed. Sometimes, that's all it is."
  jacklyn "Slick. Now my stomach is jawing, too!"
  mc "I told you!"
  window hide
  hide misc the_fast_supper with Dissolve(.5)
  $quest.jacklyn_town["paintings"].add("the_fast_supper")
  return
