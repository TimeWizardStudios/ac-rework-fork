init python:
  class Interactable_pancake_brothel_neon_girl(Interactable):

    def title(cls):
      return "Neon Sign"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "Nothing hotter than a poorly maintained LED sign."

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_neon_girl_interact")


label pancake_brothel_neon_girl_interact:
  # "An accurate depiction of the first cock-cooker, as the stripper-waitress-chefs here are called."
  "An accurate depiction of the first cock-cooker, as the stripper-waitress-{space=-70}\nchefs here are called."
  return
