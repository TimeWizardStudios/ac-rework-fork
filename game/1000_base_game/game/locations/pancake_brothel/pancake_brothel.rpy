init python:
  class Location_pancake_brothel(Location):

    default_title = "  Tam's Pancake Brothel  "

    def scene(self,scene):
      scene.append([(0,0),"pancake_brothel background"])

      if quest.jacklyn_town in ("marina","gallery","pancake_brothel"):
        scene.append([(16,40),"pancake_brothel the_cream",("pancake_brothel_the_cream",0,606)])
        scene.append([(372,204),"pancake_brothel the_moan_aaah_lisa",("pancake_brothel_the_moan_aaah_lisa",0,50)])
        scene.append([(730,264),"pancake_brothel the_birth_of_penis","pancake_brothel_the_birth_of_penis"])
        scene.append([(970,285),"pancake_brothel the_furry_night","pancake_brothel_the_furry_night"])
        scene.append([(1541,230),"pancake_brothel the_fast_supper",("pancake_brothel_the_fast_supper",22,44)])
      else:
        scene.append([(-10,36),"pancake_brothel neon_girl",("pancake_brothel_neon_girl",55,725)])
        scene.append([(405,260),"pancake_brothel poster",("pancake_brothel_poster",0,20)])
        scene.append([(716,250),"pancake_brothel neon_tams"])
        scene.append([(1711,242),"pancake_brothel picture_of_owner",("pancake_brothel_picture_of_owner",0,350)])

      scene.append([(594,325),"pancake_brothel door","pancake_brothel_door"])
      scene.append([(1075,215),"pancake_brothel miscellaneous"])
      scene.append([(1404,361),"pancake_brothel employee_sign","pancake_brothel_employee_sign"])
      scene.append([(1422,194),"pancake_brothel pink_feather_boa",("pancake_brothel_pink_feather_boa",40,350)])

      scene.append([(965,407),"pancake_brothel bar_counter","pancake_brothel_bar_counter"])
      scene.append([(1026,333),"pancake_brothel syrup_fountain","pancake_brothel_syrup_fountain"])

      ## Flora ##
      if ((not pancake_brothel["exclusive"] or "flora" in pancake_brothel["exclusive"])
      and flora.at("pancake_brothel","spying")
      and not flora.talking):
        scene.append([(1108,353),"pancake_brothel flora","pancake_brothel_mystery_woman"])

      if quest.jacklyn_town in ("marina","gallery","pancake_brothel"):
        scene.append([(740,0),"pancake_brothel removed_poles"])
      else:
        scene.append([(597,0),"pancake_brothel poles"])

      ## Jacklyn ##
      if ((not pancake_brothel["exclusive"] or "jacklyn" in pancake_brothel["exclusive"])
      and jacklyn.at("pancake_brothel","standing")
      and not jacklyn.talking):
        scene.append([(204,230),"pancake_brothel jacklyn"])

      scene.append([(0,479),"pancake_brothel tables"])
      scene.append([(1024,648),"pancake_brothel handcuffs","pancake_brothel_handcuffs"])

      scene.append([(0,0),"#pancake_brothel overlay"])

    def find_path(self,target_location):
      return "pancake_brothel_door", dict(marker_sector = "left_top", marker_offset = (70,170))


label goto_pancake_brothel:
  call goto_with_black(pancake_brothel)
  return
