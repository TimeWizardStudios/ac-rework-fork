init python:
  class Interactable_pancake_brothel_picture_of_owner(Interactable):

    def title(cls):
      return "Picture of Owner"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "That bust is a work of art itself..."

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_picture_of_owner_interact")


label pancake_brothel_picture_of_owner_interact:
  # "The owner looks like she could smackdown the toughest customers and still get paid."
  "The owner looks like she could smackdown the toughest customers{space=-5}\nand still get paid."
  return
