init python:
  class Interactable_pancake_brothel_the_furry_night(Interactable):

    def title(cls):
      return "\"The Furry Night\""

    def description(cls):
      if quest.jacklyn_town == "gallery":
        # return "Now, {i}this{/} is the work of a mad genius."
        return "Now, {i}this{/} is the work\nof a mad genius."

    def actions(cls,actions):
      if quest.jacklyn_town == "gallery":
        actions.append("?pancake_brothel_the_furry_night_interact")


label pancake_brothel_the_furry_night_interact:
  show misc the_furry_night with Dissolve(.5)
  pause 0.125
  mc "This one has some nice colors throughout."
  mc "And yet... it's kind of haunting, in a way."
  mc "Like some kind of weirdo passing the viewer by in a blur."
  # mc "If you blink, you could miss the sheer depravity, and how little someone's life choices, if questionable, matter in the grand scheme of things."
  mc "If you blink, you could miss the sheer depravity, and how little someone's{space=-110}\nlife choices, if questionable, matter in the grand scheme of things."
  # mc "It makes you feel small under the giant, endless darkness of the heavens."
  mc "It makes you feel small under the giant, endless darkness of the heavens.{space=-110}"
  jacklyn "That's a hot shot of depresso you're frothing, Modesty Blues."
  mc "Sorry, I guess this one just kind of spoke to me..."
  jacklyn "You're blossoming for the art, [mc]."
  # jacklyn "Keep letting it happen. Your petals are going to be dewey in no time."
  jacklyn "Keep letting it happen. Your petals are going to be dewey in no time.{space=-5}"
  mc "To be honest, I never realized how impactful art could be."
  mc "I never really took it seriously before this."
  mc "Thank you for bringing me here tonight, [jacklyn]."
  jacklyn "That sort of speak just floods my heart dam!"
  mc "Heh! It's true!"
  jacklyn "I actually know the artist of this one."
  # jacklyn "He got slizzard hard, put on a costume, and decided to paint his backyard."
  jacklyn "He got slizzard hard, put on a costume, and decided to paint his backyard.{space=-130}"
  mc "Oh."
  # jacklyn "It doesn't make your interpretation or heartspeak any less money, though!"
  jacklyn "It doesn't make your interpretation or heartspeak any less money, though!{space=-120}"
  mc "Err, maybe not."
  window hide
  hide misc the_furry_night with Dissolve(.5)
  $quest.jacklyn_town["paintings"].add("the_furry_night")
  return
