init python:
  class Interactable_pancake_brothel_bar_counter(Interactable):

    def title(cls):
      return "Bar Counter"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "Solid oak finish. Classy."

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_bar_counter_interact")


label pancake_brothel_bar_counter_interact:
  "Hmm... I wonder if they'll serve me some alcohol..."
  $quest.jacklyn_town["interactables"].add("bar_counter")
  return
