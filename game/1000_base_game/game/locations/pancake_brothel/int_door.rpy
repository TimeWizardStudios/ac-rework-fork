init python:
  class Interactable_pancake_brothel_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "I'm surprised there's not a bouncer keeping watch here..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "pancake_brothel":
            actions.append("?pancake_brothel_door_interact")
            return
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append(["go","Leave","goto_marina"])


label pancake_brothel_door_interact:
  "Not yet. It would be rude to ditch this joint without [jacklyn]."
  $quest.jacklyn_town["interactables"].add("door")
  return
