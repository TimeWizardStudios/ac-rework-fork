init python:
  class Interactable_pancake_brothel_employee_sign(Interactable):

    def title(cls):
      return "Employee Sign"

    def description(cls):
      if quest.jacklyn_town >= "pancake_brothel":
        return "{i}\"All employees must wear protective gear when serving customers.\"{/}"

    def actions(cls,actions):
      if quest.jacklyn_town >= "pancake_brothel":
        actions.append("?pancake_brothel_employee_sign_interact")


label pancake_brothel_employee_sign_interact:
  "Smart. Staying gloved for best hygiene practices."
  $quest.jacklyn_town["interactables"].add("employee_sign")
  return
