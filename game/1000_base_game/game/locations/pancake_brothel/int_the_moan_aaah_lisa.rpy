init python:
  class Interactable_pancake_brothel_the_moan_aaah_lisa(Interactable):

    def title(cls):
      return "\"The Moan-aaah Lisa\""

    def description(cls):
      if quest.jacklyn_town == "gallery":
        return "I can practically hear the moans coming from the canvas."

    def actions(cls,actions):
      if quest.jacklyn_town == "gallery":
        actions.append("?pancake_brothel_the_moan_aaah_lisa_interact")


label pancake_brothel_the_moan_aaah_lisa_interact:
  show misc the_moan_aaah_lisa with Dissolve(.5)
  pause 0.125
  "Goddamn, she's gorgeous."
  "And it's like she's staring... straight into my soul... even as she..."
  jacklyn "That's orgasmic bliss right there!"
  mc "Heh. Literally."
  jacklyn "This one flatters my scarjo and unrolls the red carpet."
  "I bet it does..."
  jacklyn "There's real artistic license in the female orgasm, don't you think?"
  mc "I do, now. She's definitely hitting her peak in this one."
  mc "And so is the artist."
  jacklyn "It's Buddha in the woods. Enlightenment for the sexes."
  mc "Sounds deep."
  mc "Looks deep, too."
  jacklyn "Hitting all the right places!"
  window hide
  hide misc the_moan_aaah_lisa with Dissolve(.5)
  $quest.jacklyn_town["paintings"].add("the_moan_aaah_lisa")
  return
