init python:
  class Interactable_marina_exit_arrow(Interactable):

    def title(cls):
      return "Back"

    def description(cls):
      return "Some take the bus to school,\nbut this is a shortcut."

    def actions(cls,actions):
#     actions.append(["go","Return","marina_exit_arrow_go"])
      actions.append(["go","Return","goto_school_park"])


#label marina_exit_arrow_go:
# menu(side="middle"):
#   "Where should I go?"
#   "Go to school":
#     jump goto_school_entrance
#   "Go home":
#     jump goto_home_kitchen
#   "Nevermind":
#     return
