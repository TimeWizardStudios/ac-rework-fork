init python:
  class Interactable_marina_codfather(Interactable):

    def title(cls):
      return "The Codfather"

    def description(cls):
      return "{i}\"Fish, one dollar. An offer\nyou can't refuse.\"{/}"

    def actions(cls,actions):
      if mc.money >= 1:
        actions.append("marina_codfather_interact_buy")
      else:
        actions.append("?marina_codfather_interact_refuse")


label marina_codfather_interact_buy:
  "I'm not a cook, and my backpack will probably start smelling..."
  "...but I can't pass up on an offer this good!"
  window hide
  $mc.money-=1
  $mc.add_item("fish")
  return

label marina_codfather_interact_refuse:
  "No one has ever refused the offer of the Codfather before."
  "Maybe I'm the first one to tread these waters?"
  window hide
  $achievement.offer_refused.unlock()
  return
