init python:
  class Interactable_marina_prawn_star(Interactable):

    def title(cls):
      return "The Prawn Star"

    def description(cls):
      return "The smell of cooking oil and seafood always welcomes you to the Newfall Marina."

    def actions(cls,actions):
      if quest.nurse_aid == "fishing_pole" and (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished):
        actions.append("?quest_nurse_aid_fishing_pole_marina_prawn_star")
        return
      if quest.isabelle_stars == "takeout":
        actions.append("quest_isabelle_stars_takeout")
      actions.append("?marina_prawn_star_interact")


label marina_prawn_star_interact:
  "The Prawn Star's locally famous fish 'n' shrimps is not only a cheeky{space=-15}\ntake on the traditional dish..."
  "...it is also the reigning champion of the Newfall Food Fair Famine Challenge."
  return
