init python:
  class Interactable_marina_hospital(Interactable):

    def title(cls):
      return "Newfall Hospital"

    def description(cls):
      if hospital["unlocked"]:
        return "A place dedicated to the endless struggle of life."

    def actions(cls,actions):
      if hospital["unlocked"]:
        if mc["focus"]:
          if mc["focus"] == "lindsey_angel":
            if quest.lindsey_angel == "hospital" and mc.owned_item("lab_coat"):
              actions.append(["go","Newfall Hospital","quest_lindsey_angel_hospital"])
        else:
          if quest.fall_in_newfall.finished and (quest.lindsey_angel.finished and not quest.lindsey_angel["finished_this_week"]) and quest.maya_witch.finished and not quest.lindsey_voluntary.started:
            actions.append(["go","Newfall Hospital","quest_lindsey_voluntary_start"])
        actions.append(["go","Newfall Hospital","goto_hospital"])
