init python:
  class Interactable_marina_pancake_brothel(Interactable):

    def title(cls):
      return "Tam's Pancake Brothel"

    def description(cls):
      return "The place where \"sugar\" and \"hot stuff\" have both literal and metaphorical meanings."

    def actions(cls,actions):
      if quest.nurse_aid == "fishing_pole" and (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished):
        actions.append("?quest_nurse_aid_fishing_pole_marina_pancake_brothel")
      elif quest.jacklyn_town.finished:
        actions.append(["go","Tam's Pancake Brothel","goto_pancake_brothel"])
      else:
        actions.append("?marina_pancake_brothel_interact")


label marina_pancake_brothel_interact:
  "Born from the extramarital affair between a chef and a hooker, Tam set out to combine the best of both worlds."
  return
