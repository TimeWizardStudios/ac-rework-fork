init python:
  class Interactable_marina_police_department(Interactable):

    def title(cls):
      return "Newfall Police Department"

    def description(cls):
      return "Unsmooth criminals tend to\nend up here."

    def actions(cls,actions):
      actions.append("?marina_police_department_interact")


label marina_police_department_interact:
  "Newfall has one of the lowest crime rates in the country."
  "Probably because there's nothing really worth stealing..."
  return
