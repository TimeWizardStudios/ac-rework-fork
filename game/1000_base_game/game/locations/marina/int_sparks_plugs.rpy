init python:
  class Interactable_marina_sparks_plugs(Interactable):

    def title(cls):
      return "Sparks & Plugs"

    def description(cls):
      return "Second or third hand electronics."

    def actions(cls,actions):
      if quest.lindsey_voluntary == "new_computer":
        actions.append("quest_lindsey_voluntary_new_computer")
      else:
        actions.append("?marina_sparks_plugs_interact")


label marina_sparks_plugs_interact:
  "The only electronics store in all of Newfall."
  "That's how they survive — total market monopoly."
  return
