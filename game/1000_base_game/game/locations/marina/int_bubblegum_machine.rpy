init python:
  class Interactable_marina_bubblegum_machine(Interactable):

    def title(cls):
      return "Bubblegum Machine"

    def description(cls):
      return "Duke's Assgum Nukes."

    def actions(cls,actions):
      actions.append("?marina_bubblegum_machine_interact")


label marina_bubblegum_machine_interact:
  "This is where [maya] gets her gum."
  "Not because I used to follow her here to look at her legs or anything...{space=-40}"
  "I'm just a bubblegum connoisseur."
  return
