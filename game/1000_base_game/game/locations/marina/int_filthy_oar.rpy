init python:
  class Interactable_marina_filthy_oar(Interactable):

    def title(cls):
      return "Filthy Oar"

    def description(cls):
      return "The only rent-a-boat\nin the marina."

    def actions(cls,actions):
      if quest.nurse_aid == "fishing_pole" and (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished):
        actions.append("?quest_nurse_aid_fishing_pole_marina_filthy_oar")
      else:
        actions.append("?marina_filthy_oar_interact")


label marina_filthy_oar_interact:
  "When it blows, raise the mast and get the seamen ready!"
  return
