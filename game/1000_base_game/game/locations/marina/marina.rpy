init python:
  class Location_marina(Location):

    default_title = "Newfall Marina"

    def scene(self,scene):
      ## Background ##
      if game.season == 1:
        scene.append([(-26,0),"marina background"])
      elif game.season == 2:
        scene.append([(-26,0),"marina background_autumn"])
      if (quest.lindsey_angel == "hospital"
      or quest.jacklyn_town in ("marina","pancake_brothel")
      or (quest.nurse_aid == "date" and game.hour == 20)):
        scene.append([(0,0),"marina background_night"])
      scene.append([(532,12),"marina miscellaneous"])

      ## Buildings ##
      scene.append([(691,192),"marina hospital",("marina_hospital",25,50)])
      if (quest.lindsey_angel == "hospital"
      or quest.jacklyn_town in ("marina","pancake_brothel")
      or (quest.nurse_aid == "date" and game.hour == 20)):
        scene.append([(790,257),"marina hospital_lights",("marina_hospital",-10,-15)])
      scene.append([(889,441),"marina tailor_aquarium","marina_tailor_aquarium"])
      scene.append([(658,237),"marina pancake_brothel",("marina_pancake_brothel",0,50)])
      scene.append([(740,297),"marina police_department",("marina_police_department",0,35)])
      scene.append([(236,0),"marina sparks_plugs",("marina_sparks_plugs",110,320)])
      scene.append([(0,0),"marina prawn_star",("marina_prawn_star",0,415)])

      ## Boats ##
      scene.append([(1301,656),"marina filthy_oar","marina_filthy_oar"])
      scene.append([(1401,721),"marina codfather",("marina_codfather",0,50)])
      scene.append([(1512,913),"marina naughtilus"])
      scene.append([(1765,905),"marina crab",("crab",-30,-10)])

      ## Greenery ##
      if game.season == 1:
        scene.append([(215,543),"marina pots"])
      elif game.season == 2:
        scene.append([(215,543),"marina pots_autumn"])
      scene.append([(399,573),"marina bubblegum_machine","marina_bubblegum_machine"])
      scene.append([(220,656),"marina tables"])
      if game.season == 1:
        scene.append([(194,637),"marina plants"])
        scene.append([(1133,0),"marina tree"])
      elif game.season == 2:
        scene.append([(194,637),"marina plants_autumn"])
        scene.append([(1133,0),"marina tree_autumn"])

      ## Boardwalk ##
      scene.append([(0,597),"marina boardwalk"])
      if game.season == 2:
        scene.append([(528,724),"marina pile_of_leaves"])
      scene.append([(-6,420),"marina i_scream"])
#     scene.append([(991,426),"marina ice_cream_cart"])

      ## Leaves ##
      if game.season == 1:
        scene.append([(882,-695),"marina leaves"])
      elif game.season == 2:
        scene.append([(1112,219),"marina leaves_autumn"])

      ## Overlay ##
      scene.append([(0,0),"#marina overlay"])
      if (quest.lindsey_angel == "hospital"
      or quest.jacklyn_town in ("marina","pancake_brothel")
      or (quest.nurse_aid == "date" and game.hour == 20)):
        scene.append([(0,0),"#marina overlay_night"])

      ## Arrow ##
      scene.append([(907,924),"marina exit_arrow","marina_exit_arrow"])

    def find_path(self,target_location):
      if target_location == "hospital":
        return "marina_hospital", dict(marker_sector="left_bottom", marker_offset=(300,-70))
      else:
        return "marina_exit_arrow", dict(marker_offset=(15,10))


label goto_marina:
  call goto_with_black(marina)
  return
