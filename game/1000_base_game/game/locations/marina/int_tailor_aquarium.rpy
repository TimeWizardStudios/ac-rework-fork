init python:
  class Interactable_marina_tailor_aquarium(Interactable):

    def title(cls):
      return "Newfall Tailor & Aquarium"

    def description(cls):
      return "A strange combination to be sure, but they're one of the most popular tourist attractions here."

    def actions(cls,actions):
      if quest.jacklyn_romance.in_progress:
        actions.append(["go","Newfall Tailor & Aquarium","?quest_jacklyn_romance_suit"])
        return
      actions.append("?marina_tailor_aquarium_interact")


label marina_tailor_aquarium_interact:
  "[flora]'s favorite place in the entire world."
  "Where deep sea meets deep seams."
  "Where shoreline meets hemline."
  "Where krill meets frill."
  return
