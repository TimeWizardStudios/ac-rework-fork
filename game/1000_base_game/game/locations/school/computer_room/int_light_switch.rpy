init python:
  class Interactable_school_computer_room_light_switch(Interactable):
    def title(cls):
      return "Light Switch"
    def description(cls):
      if school_computer_room["dark"]:
        return "And darkness was upon the face of the deep..."
      else:
        return "\"We can easily forgive a child who is afraid of the dark; the real tragedy of life is when men are afraid of the light.\" — Plato"
    def actions(cls,actions):
      actions.append("school_computer_room_light_switch_interact")

label school_computer_room_light_switch_interact:
  $school_computer_room["dark"] = not school_computer_room["dark"]
  return
