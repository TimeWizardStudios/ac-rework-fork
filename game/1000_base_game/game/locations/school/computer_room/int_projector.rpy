init python:
  class Interactable_school_computer_room_projector(Interactable):
    def title(cls):
      return "Projector"
    def description(cls):
      return "Dimmed lights. Movie and popcorn. My arm around a girl.\n\nSo many unfulfilled fantasies in this room."

    def actions(cls,actions):
      actions.append("?school_computer_room_projector_interact")

label school_computer_room_projector_interact:
  "There's something special about the whirr when it heats up."
  "Just need something worth showing on the big screen."
  return
