init python:
  class Interactable_school_computer_room_dollar(Interactable):

    def title(cls):
      return "Money"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["take","Yoink","school_computer_room_dollar_take"])


label school_computer_room_dollar_take:
  $school_computer_room["dollar_taken_today"] = True
  $mc.money+=20
  return
