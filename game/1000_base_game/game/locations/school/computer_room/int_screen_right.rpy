init python:
  class Interactable_school_computer_room_screen_right(Interactable):

    def title(cls):
      return "School Computer"

    def description(cls):
      return "What's the opposite of an interface? 'Cause this piece of crap has one."

    def actions(cls,actions):
      actions.append("school_computer_room_screen_right_interact")

label school_computer_room_screen_right_interact:
  $school_computer_room["screen4"] = not school_computer_room["screen4"]
  return
