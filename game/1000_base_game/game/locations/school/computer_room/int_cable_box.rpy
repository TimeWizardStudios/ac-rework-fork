init python:
  class Interactable_school_computer_room_cable_box(Interactable):

    def title(cls):
      return "Box of Cables"

    def description(cls):
      if quest.maxine_lines.started:
        return "The electronic graveyard. Did any of these cables receive a proper electric funeral?"
      elif quest.isabelle_piano.started:
        return "Like a can of electronic worms, this box gives me nightmares."
      else:
        return "Filled with old cables that don't fit anywhere.\n\nThere's an exact box like this in my closet."

    def actions(cls,actions):
      if quest.maxine_lines == "battery" and not school_computer_room["charger_cable_taken"]:
        actions.append(["take","Take","?school_computer_room_cable_box_interact_maxine_lines_take"])
      elif quest.maxine_lines == "magnetic" and not school_computer_room["box_lines_magnet"]:
        actions.append(["take","Take","?school_computer_room_cable_box_interact_maxine_lines_take_magnet"])
      elif quest.isabelle_piano == "printer" and not (school_computer_room["red_cable_taken"] == school_computer_room["blue_cable_taken"] == school_computer_room["yellow_cable_taken"] == school_computer_room["candle_taken"] == True):
        actions.append(["take","Take","quest_isabelle_piano_printer_cable_box_take"])
      elif not school_computer_room["candle_taken"]:
        actions.append(["take","Take","school_computer_room_cable_box_take"])
      if quest.maxine_lines == "battery":
        actions.append("?school_computer_room_cable_box_interact_maxine_lines")
      elif quest.isabelle_piano == "printer" and (school_computer_room["red_cable_taken"] == school_computer_room["blue_cable_taken"] == school_computer_room["yellow_cable_taken"] == school_computer_room["candle_taken"] == True):
        actions.append("?quest_isabelle_piano_printer_cable_box_interact")
      else:
        actions.append("?school_computer_room_cable_box_interact")


label school_computer_room_cable_box_interact_maxine_lines_take_magnet:
  "The box of cables is always a sure way to find random items."
  $school_computer_room["box_lines_magnet"] = True
  $mc.add_item("magnet")
  return

label school_computer_room_cable_box_take:
  "The most interesting thing here is this fat candle. Imagine that."
  "Might as well keep it..."
  $school_computer_room["candle_taken"] = True
  $mc.add_item("candle")
  return

label school_computer_room_cable_box_interact:
  "Obviously not a single net cable or micro USB."
  "But enough VGA cables to power an entire 90s hacker den."
  if not school_computer_room["box_lines_magnet"]:
    "Also, a decently sized magnet."
  return

label school_computer_room_cable_box_interact_maxine_lines_take:
  "Hmm... lots of different cables here, but this one looks like a charger cable."
  $school_computer_room["charger_cable_taken"] = True
  $mc.add_item("charger_cable")
  return

label school_computer_room_cable_box_interact_maxine_lines:
  "Items you think you might need at some point in the future but never do. Except that one time."
  return
