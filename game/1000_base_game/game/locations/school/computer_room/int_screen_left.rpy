init python:
  class Interactable_school_computer_room_screen_left(Interactable):

    def title(cls):
      return "School Computer"

    def description(cls):
      if school_computer_room["burns"]:
        return "If it works, it ain't broken.\n\n...at least, that's the school policy."
      elif quest.isabelle_piano.started:
        return "What's the difference between a school computer and a toaster?\n\nThe toaster can make bread."
      else:
        return "There's something about always picking the same computer, even at school.\n\nIt becomes trusted. A friend for the lonesome."

    def actions(cls,actions):
      if quest.maxine_eggs == "cyberia":
        actions.append(["quest","Quest","quest_maxine_eggs_computer"])
      elif quest.maxine_eggs == "plugpc":
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:maxine_eggs,screen_left_flash_drive|Use What?","quest_maxine_eggs_screen_left_flash_drive"])
        actions.append("?quest_maxine_eggs_screen_left_interact")
      elif quest.maxine_lines == "magnetic" and not home_bedroom["lines_magnet"]:
        actions.append("home_bedroom_computer_interact_maxine_lines_take_magnet")
      elif quest.isabelle_piano == "computer":
        actions.append("quest_isabelle_piano_computer")
      elif quest.lindsey_motive == "google":
        actions.append("quest_lindsey_motive_google")
      elif quest.kate_stepping == "photoshop":
        actions.append("?quest_kate_stepping_photoshop_wrong_computer")
      else:
        actions.append("school_computer_room_screen_left_interact")


label quest_maxine_eggs_screen_left_flash_drive(item):
  if item == "flash_drive":
    "Okay, it's working again."
    "Time to make a simple appointment system for [maxine]."
    "Shouldn't take more than an hour."
    $game.advance()
    with Fade(.5,3,.5)
    "All right, done!"
    mc "Thanks for the help, [cyberia]!"
    "..."
    "Maybe she fell asleep?"
    $quest.maxine_eggs["released_cyberia"] = True
    $quest.maxine_eggs.advance("deliver")
  else:
    "Imagine if I could store data on my [item.title_lower]."
    $quest.maxine_eggs.failed_item("screen_left_flash_drive",item)
  return

label quest_maxine_eggs_screen_left_interact:
  "What was my password again?"
  "Favorite pet? Mother's maiden name?"
  "Hmm... how about \"[cyberia]HasAThiccMainframe\"?"
  return

label school_computer_room_screen_left_interact:
  $school_computer_room["screen1"] = not school_computer_room["screen1"]
  return
