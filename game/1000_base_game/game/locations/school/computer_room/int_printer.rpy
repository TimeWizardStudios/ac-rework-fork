init python:
  class Interactable_school_computer_room_printer(Interactable):

    def title(cls):
      return "Printer"

    def description(cls):
      if quest.isabelle_piano.started:
        return "This is where most of the school budget is spent. Printer ink — the devil in liquid form."
      else:
        return "Ink and a nice rack to hold the paper. What more can you ask for?"

    def actions(cls,actions):
      if quest.isabelle_piano == "printer":
        if quest.isabelle_piano["missing_cable"]:
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_piano,printer|Use What?","quest_isabelle_piano_printer_use_item"])
        actions.append("?quest_isabelle_piano_printer_interact")
      elif quest.kate_stepping == "photoshop" and quest.kate_stepping["print_it_out"]:
        actions.append("quest_kate_stepping_photoshop_printer")
      else:
        actions.append("?school_computer_room_printer_interact")


label school_computer_room_printer_interact:
  "Wonder how many butts this thing has scanned over the years..."
  "Probably a shitload."
  return
