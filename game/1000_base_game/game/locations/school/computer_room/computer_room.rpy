init python:
  class Location_school_computer_room(Location):

    default_title="Computer Room"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,0),"school computer_room wall_ceiling_floor"])
      scene.append([(134,0),"school computer_room lamptop"])
      scene.append([(1647,200),"school computer_room door",("school_computer_room_door",-90,300)])
      scene.append([(1620,178),"school computer_room doorframe"])
      scene.append([(0,147),"school computer_room posterleft"])
      scene.append([(344,296),"school computer_room boardmid"])
      scene.append([(710,229),"school computer_room clock"])
      scene.append([(0,319),"school computer_room board_left"])
      scene.append([(1470,366),"school computer_room paper_wall_right"])
      scene.append([(202,349),"school computer_room paper_wall_left"])
      scene.append([(1576,481),"school computer_room light_switch","school_computer_room_light_switch"])
      scene.append([(166,505),"school computer_room teacher_chair"])
      scene.append([(105,561),"school computer_room teacher_table"])
      scene.append([(1111,296),"school computer_room cable_box","school_computer_room_cable_box"])
      scene.append([(1113,356),"school computer_room cabinet_right"])
      scene.append([(1121,383),"school computer_room old_machine"])
      scene.append([(0,622),"school computer_room shelf_left"])
#     scene.append([(16,797),"school computer_room mouse"])
#     scene.append([(0,722),"school computer_room nomousecage"])
      scene.append([(0,722),"school computer_room mousecage"])
      scene.append([(1229,319),"school computer_room server"])
      scene.append([(1412,371),"school computer_room servercable"])
      scene.append([(0,558),"school computer_room printer","school_computer_room_printer"])

      if school_computer_room["dollar_spawned_today"] == True and not school_computer_room["dollar_taken_today"]:
        scene.append([(99,577),"school computer_room dollar","school_computer_room_dollar"])

      scene.append([(0,618),"school computer_room stationary"])
      scene.append([(1402,574),"school computer_room tablettable"])
      scene.append([(1440,506),"school computer_room tablet","cyberia"])
      scene.append([(738,0),"school computer_room projector",("school_computer_room_projector",0,400)])
      scene.append([(267,653),"school computer_room table"])

      #Maxine
      if not school_computer_room["dark"]:
        if maxine.at("school_computer_room","stethoscope"):
          if not maxine.talking:
            scene.append([(1171,395),"school computer_room maxine","maxine"])

      if school_computer_room["sparks"]:
        scene.append([(331,476),"school computer_room sparks"])
      if school_computer_room["smoke"]:
        scene.append([(319,394),"school computer_room smoke"])
      elif school_computer_room["smoke_spread"]:
        scene.append([(198,100),"school computer_room smoke_spread"])
      if school_computer_room["fire"]:
        scene.append([(361,466),"school computer_room fire"])
      elif school_computer_room["fire_spread"]:
        scene.append([(361,429),"school computer_room fire_spread"])
      if school_computer_room["ashes"]:
        scene.append([(377,558),"school computer_room ashes"])

      if maya.at("school_computer_room","sitting"):
        scene.append([(361,570),"school computer_room screen1",("maya",-30,44)])
      else:
        scene.append([(361,570),"school computer_room screen1","school_computer_room_screen_left"])
      scene.append([(599,561),"school computer_room screen2","school_computer_room_screen_mid_left"])
      scene.append([(794,554),"school computer_room screen3","school_computer_room_screen_mid_right"])
      scene.append([(966,547),"school computer_room screen4","school_computer_room_screen_right"])

      if not school_computer_room["dark"]:
        if school_computer_room["screen1"] or (maya.at("school_computer_room","sitting") and not maya.talking):
          if school_computer_room["screen1"] == "google":
            scene.append([(376,580),"school computer_room screen1_google"])
          elif school_computer_room["screen1"] == "wikipedia":
            scene.append([(376,580),"school computer_room screen1_wikipedia"])
          elif maya.at("school_computer_room","sitting"):
            scene.append([(377,580),"school computer_room screen1_on",("maya",-30,34)])
          else:
            scene.append([(377,580),"school computer_room screen1_on",("school_computer_room_screen_left",0,-10)])
        if school_computer_room["screen2"]:
          scene.append([(612,571),"school computer_room screen2_on",("school_computer_room_screen_mid_left",-1,-10)])
        if school_computer_room["screen3"]:
          scene.append([(812,563),"school computer_room screen3_on",("school_computer_room_screen_mid_right",-4,-9)])
        if school_computer_room["screen4"]:
          scene.append([(981,556),"school computer_room screen4_on",("school_computer_room_screen_right",-3,-9)])
        #school_computer_room["screen_content"] == x:
      # scene.append([(377,580),"school computer_room video1","school_computer_room_video1"])
      # scene.append([(377,580),"school computer_room video2","school_computer_room_video2"])
      # scene.append([(377,580),"school computer_room video3","school_computer_room_video3"])
      # scene.append([(377,580),"school computer_room videooverlay","school_computer_room_videooverlay"])

      if school_computer_room["burns"]:
        if maya.at("school_computer_room","sitting"):
          scene.append([(363,570),"school computer_room screen1_burns",("maya",-31,44)])
        else:
          scene.append([(363,570),"school computer_room screen1_burns",("school_computer_room_screen_left",-1,0)])
        scene.append([(599,562),"school computer_room screen2_burns",("school_computer_room_screen_mid_left",0,-1)])

      #Cyberia
      if cyberia.talking:
        scene.append([(1425,488),"school computer_room cyberia"])

      scene.append([(332,673),"school computer_room keyboardmouse"])
      if not school_computer_room["dark"]:
        scene.append([(278,628),"school computer_room chair"])

      if school_computer_room["pee"]:
        scene.append([(355,542),"school computer_room pee"])

      #Maya
      if not school_computer_room["dark"]:
        if maya.at("school_computer_room","sitting"):
          if not maya.talking:
            scene.append([(317,614),"school computer_room maya_autumn","maya"])

      if school_computer_room["dark"]:
        scene.append([(0,0),"#school computer_room overlay_night"])
        scene.append([(1206,304),"school computer_room server_night"])

        if maxine.at("school_computer_room","stethoscope"):
          if not maxine.talking:
            scene.append([(1171,395),"school computer_room maxine_night","maxine"])

        scene.append([(278,628),"school computer_room chair_night"])

        if school_computer_room["screen1"] or (maya.at("school_computer_room","sitting") and not maya.talking):
          if school_computer_room["screen1"] == "google":
            scene.append([(376,580),"school computer_room screen1_google"])
          elif school_computer_room["screen1"] == "wikipedia":
            scene.append([(376,580),"school computer_room screen1_wikipedia"])
          elif maya.at("school_computer_room","sitting"):
            scene.append([(294,494),"school computer_room screen1on_night",("maya",-31,120)])
          else:
            scene.append([(294,494),"school computer_room screen1on_night",("school_computer_room_screen_left",-1,91)])
        if school_computer_room["screen2"]:
          scene.append([(501,483),"school computer_room screen2on_night",("school_computer_room_screen_mid_left",14,78)])
        if school_computer_room["screen3"]:
          scene.append([(708,458),"school computer_room screen3on_night",("school_computer_room_screen_mid_right",-3,96)])
        if school_computer_room["screen4"]:
          scene.append([(921,493),"school computer_room screen4on_night",("school_computer_room_screen_right",-8,54)])

        if maya.at("school_computer_room","sitting"):
          if not maya.talking:
            scene.append([(317,614),"school computer_room maya_autumn_night","maya"])

    def find_path(self,target_location):
      return "school_computer_room_door", dict(marker_sector="right_top", marker_offset=(110,70))


label goto_school_computer_room:
  if school_computer_room.first_visit_today:
    $school_computer_room["dollar_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_computer_room)
  return
