init python:
  class Interactable_school_computer_room_screen_mid_left(Interactable):

    def title(cls):
      return "School Computer"

    def description(cls):
      if school_computer_room["burns"]:
        return "Burned DVDs or computer screens, what's really\nthe difference?"
      else:
        return "Windows ME. Enough said."

    def actions(cls,actions):
      actions.append("school_computer_room_screen_mid_left_interact")


label school_computer_room_screen_mid_left_interact:
  $school_computer_room["screen2"] = not school_computer_room["screen2"]
  return
