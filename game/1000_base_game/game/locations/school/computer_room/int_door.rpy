init python:
  class Interactable_school_computer_room_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.maya_witch in ("phone_call","wait"):
        return "Nope. Still locked."
      else:
        return "Another goddamn door... always so goddamn fascinating."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "maya_witch":
          if quest.maya_witch == "locked":
            actions.append(["go","Entrance Hall","quest_maya_witch_locked"])
          elif quest.maya_witch in ("phone_call","wait"):
            actions.append(["go","Entrance Hall","?quest_maya_witch_phone_call_school_computer_room_door"])
            return
      actions.append(["go","Entrance Hall","goto_school_ground_floor"])
