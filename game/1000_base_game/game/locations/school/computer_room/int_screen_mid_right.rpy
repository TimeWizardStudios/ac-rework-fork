init python:
  class Interactable_school_computer_room_screen_mid_right(Interactable):

    def title(cls):
      return "School Computer"

    def description(cls):
      return "The modem dial-up noise... that's a sound I'll know till my dying day."

    def actions(cls,actions):
      actions.append("school_computer_room_screen_mid_right_interact")


label school_computer_room_screen_mid_right_interact:
  $school_computer_room["screen3"] = not school_computer_room["screen3"]
  return
