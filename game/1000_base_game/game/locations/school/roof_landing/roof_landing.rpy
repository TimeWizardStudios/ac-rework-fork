init python:
  class Location_school_roof_landing(Location):

    default_title = "Roof Landing"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,0),"school roof_landing background"])
      scene.append([(0,528),"school roof_landing plant",("school_roof_landing_plant",25,25)])
      scene.append([(77,447),"school roof_landing wall_sign"])
      scene.append([(0,0),"school roof_landing painting"])
      scene.append([(227,0),"school roof_landing web",("school_roof_landing_spider_web",0,300)])
      scene.append([(411,30),"school roof_landing door",("school_roof_landing_door",330,700)])
      if (quest.isabelle_dethroning in ("revenge_done","panik","fly_to_the_moon")
      or quest.kate_stepping == "fly_to_the_moon"
      or (quest.lindsey_voluntary == "dream" and quest.lindsey_voluntary["door_slam"])):
        scene.append([(1294,537),"school roof_landing lindsey_hoodie","school_roof_landing_lindsey_hoodie"])
      if (quest.jo_washed.in_progress
      or quest.lindsey_voluntary == "dream"):
        scene.append([(809,316),"school roof_landing calendar","school_roof_landing_calendar"])
      elif (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik","fly_to_the_moon")
      or quest.kate_stepping == "fly_to_the_moon"
      or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        if school_roof_landing["axe_hole"] == 1:
          scene.append([(810,317),"school roof_landing axe_hole1_night"])
        elif school_roof_landing["axe_hole"] == 2:
          scene.append([(810,317),"school roof_landing axe_hole2_night"])
        elif school_roof_landing["axe_hole"] == 3:
          scene.append([(882,300),"school roof_landing axe_hole3_night",("school_roof_landing_door",297,430)])
        else:
          scene.append([(809,316),"school roof_landing calendar","school_roof_landing_calendar"])
      else:
        if school_roof_landing["axe_hole"] == 1:
          scene.append([(809,316),"school roof_landing axe_hole1"])
        elif school_roof_landing["axe_hole"] == 2:
          scene.append([(809,316),"school roof_landing axe_hole2"])
        elif school_roof_landing["axe_hole"] == 3:
          scene.append([(882,300),"school roof_landing axe_hole3",("school_roof_landing_door",297,430)])
        else:
          scene.append([(809,316),"school roof_landing calendar","school_roof_landing_calendar"])
      scene.append([(1308,816),"school roof_landing locker"])
      scene.append([(1378,712),"school roof_landing cans","school_roof_landing_cans"])
      scene.append([(1526,801),"school roof_landing toilet_paper","school_roof_landing_toilet_paper"])

      if (school_roof_landing["fire_axe_taken"]
      and not (quest.jo_washed.in_progress
      or (quest.lindsey_voluntary == "dream" and not quest.lindsey_voluntary["emergency"]))):
        scene.append([(1608,94),"school roof_landing no_fire_axe",("school_roof_landing_fire_axe",-15,510)])
      else:
        scene.append([(1608,94),"school roof_landing fire_axe",("school_roof_landing_fire_axe",-15,510)])

      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik","fly_to_the_moon")
      or quest.kate_stepping == "fly_to_the_moon"
      or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")
      or quest.lindsey_voluntary == "dream"):
        scene.append([(0,0),"#school roof_landing overlay_night"])

      scene.append([(904,921),"school roof_landing exit_arrow","school_roof_landing_exit_arrow"])

    def find_path(self,target_location):
      if target_location == "school_roof":
        return "school_roof_landing_door", dict(marker_sector="left_bottom", marker_offset=(550,0))
      else:
        return "school_roof_landing_exit_arrow"


label goto_school_roof_landing:
  call goto_with_black(school_roof_landing)
  return
