init python:
  class Interactable_school_roof_landing_cans(Interactable):

    def title(cls):
      return "Cans"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The traces of one or more\nfuture alcoholics."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_roof_landing_cans_interact")


label school_roof_landing_cans_interact:
  "Remains of the latest drink-a-thon — a weekly event for those too popular to get detention."
  return
