init python:
  class Interactable_school_roof_landing_spider_web(Interactable):

    def title(cls):
      return "Spider Web"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "If you get bitten by a\nradioactive man, do you become\nthe Man-man?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_roof_landing_spider_web_interact")


label school_roof_landing_spider_web_interact:
  "Man, these spider webs seem to be everywhere these days..."
  "Where's the janitor when you need her?"
  return
