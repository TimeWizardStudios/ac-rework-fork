init python:
  class Interactable_school_roof_landing_exit_arrow(Interactable):

    def title(cls):
      return "Entrance Hall"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_voluntary == "dream":
        return "Too late to go back now."
      else:
        return "Such a good view over the entrance hall from up here!"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Entrance Hall","goto_school_ground_floor"])
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Entrance Hall","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
      else:
        if (quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started:
          actions.append(["go","Leave School","quest_jo_washed_not_started_bedroom"])
      actions.append(["go","Entrance Hall","goto_school_ground_floor"])
