init python:
  class Interactable_school_roof_landing_lindsey_hoodie(Interactable):

    def title(cls):
      return "[lindsey]'s Hoodie"

    def description(cls):
      if quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Only one girl smells like\ncotton candy...\n\n...and that's definitely her hoodie."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_roof_landing_lindsey_hoodie_interact")


label school_roof_landing_lindsey_hoodie_interact:
  "Why is [lindsey]'s hoodie stuck in the door?"
  "I don't like this one bit..."
  "What is she even doing on the roof?"
  return
