init python:
  class Interactable_school_roof_landing_plant(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "\"Plantae Intoxicaticus\" — a rare species potted-as-fuck plant."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.lindsey_motive == "lure":
          actions.append("?quest_lindsey_motive_lure_roof_landing_plant_interact")
      actions.append("?school_roof_landing_plant_interact")


label school_roof_landing_plant_interact:
  "The only reason this thing is alive is due to all the beer poured into the pot."
  return
