init python:
  class Interactable_school_roof_landing_fire_axe(Interactable):

    def title(cls):
      return "Fire Axe Case" if (school_roof_landing["fire_axe_taken"] and not quest.lindsey_voluntary == "dream") else "Fire Axe"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stars.started:
        return "If a lumberjack could jack wood, how much would a lumberjack jack off?"
      elif quest.maxine_dive.started:
        # return "This is what you call axidental theft."
        return "This is what you call\naxidental theft."
      else:
        return "Safety is very important... but not as important as the salary of the board members."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon" and not school_roof_landing["fire_axe_taken"]:
            actions.append(["take","Take","quest_isabelle_dethroning_fly_to_the_moon_fire_axe"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "fly_to_the_moon" and not school_roof_landing["fire_axe_taken"]:
            actions.append(["take","Take","quest_isabelle_dethroning_fly_to_the_moon_fire_axe"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("quest_lindsey_voluntary_dream_roof_landing_fire_axe")
      else:
        if quest.flora_squid == "weapon":
          actions.append(["take","Take","quest_flora_squid_weapon_fire_axe"])
        if quest.maxine_dive == "weapons" and not school_roof_landing["fire_axe_taken"]:
          actions.append(["take","Take","quest_maxine_dive_weapons_fire_axe"])
        if quest.isabelle_stars == "fire_axe":
          actions.append(["take","Take","quest_isabelle_stars_fire_axe"])
      if school_roof_landing["fire_axe_taken"]:
        actions.append("?school_roof_landing_no_fire_axe_interact")
      else:
        actions.append("?school_roof_landing_fire_axe_interact")


label school_roof_landing_fire_axe_interact:
  "Put an axe into a case, and it becomes a fire axe!"
  "Set an axe on fire, and it becomes burnt!"
  "Logic."
  return

label school_roof_landing_no_fire_axe_interact:
  "No extinguisher or fire axe..."
  "Here at Newfall High, we burn with compassion and in solidarity."
  return
