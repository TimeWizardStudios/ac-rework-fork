init python:
  class Interactable_school_roof_landing_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_dethroning > "revenge":
        return "This is probably the worst door in the entire school. Bottom three for sure."
      else:
        return "The roof, the roof, the roof is...\n\n...not on fire yet, but\nmaybe one of these days."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            if mc.owned_item("fire_axe"):
              actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_dethroning,fire_axe|Use What?","quest_isabelle_dethroning_fly_to_the_moon_roof_door"])
            actions.append(["go","Roof","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Roof","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "fly_to_the_moon" and mc.owned_item("fire_axe"):
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_dethroning,fire_axe|Use What?","quest_isabelle_dethroning_fly_to_the_moon_roof_door"])
          if quest.isabelle_dethroning in ("trap","revenge","fly_to_the_moon"):
            actions.append(["go","Roof","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning in ("revenge_done","panik"):
            actions.append(["go","Roof","?quest_isabelle_dethroning_revenge_done_roof_landing_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Roof","goto_school_roof"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Roof","?quest_lindsey_angel_keycard_school_roof_landing_door"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Roof","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if (quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started:
          actions.append(["go","Roof","?quest_isabelle_dethroning_trap_locked"])
          return
#     actions.append("?school_roof_landing_door_interact")
      actions.append(["go","Roof","goto_school_roof"])


label school_roof_landing_door_interact:
  "People generally aren't allowed onto the roof."
  "But there are ways around it... if you know the right person."
  return
