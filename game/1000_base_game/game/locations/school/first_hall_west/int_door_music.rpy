init python:
  class Interactable_school_first_hall_west_door_music(Interactable):

    def title(cls):
      return "Music Classroom"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "If \"computer keyboard\" were an instrument, I would've gotten an A in music, but alas..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart":
            actions.append(["go","Music Classroom","?first_hall_west_door_music_lindseyart"])
            return
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append(["go","Music Classroom","?school_first_hall_west_door_music_kate_fate"])
            return
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "holdup":
            actions.append(["go","Music Classroom","quest_kate_desire_holdup_leave"])
        elif mc["focus"] == "isabelle_piano":
          if quest.isabelle_piano in ("concert","score"):
            actions.append(["go","Music Classroom","?quest_isabelle_piano_concert_music_class"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "phone_call":
            actions.append(["go","Music Classroom","?quest_kate_stepping_phone_call_exit"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "phone_call":
            actions.append(["go","Music Classroom","?quest_kate_wicked_phone_call_exit"])
            return
          elif quest.kate_wicked == "school" and game.hour == 19:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:kate_wicked,key_music_class|Use What?","quest_kate_wicked_school_key_music_class"])
            actions.append(["go","Music Classroom","?quest_kate_wicked_school_music_class"])
            return
          elif quest.kate_wicked == "costume":
            actions.append(["go","Music Classroom","?quest_kate_wicked_costume_music_class"])
            return
          elif quest.kate_wicked == "party":
            actions.append(["go","Music Classroom","quest_kate_wicked_party"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge","revenge_done","panik"):
            actions.append(["go","Music Classroom","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Music Classroom","goto_school_music_class"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Music Classroom","?quest_lindsey_angel_keycard_school_first_hall_west_door_music"])
            return
      else:
        if quest.isabelle_tour < "gym":
         actions.append(["go","Music Classroom","?school_first_hall_west_door_music_interact"])
         return
      actions.append(["go","Music Classroom","goto_school_music_class"])


label school_first_hall_west_door_music_kate_fate:
  "Shit. Locked."
  return

label school_first_hall_west_door_music_interact:
  "Locked... the music teacher has always had a bad habit of being late.{space=-20}"
  return

label first_hall_west_door_music_lindseyart:
  "They'll write songs about me if I don't help [lindsey]. And not the flattering kind."
  return
