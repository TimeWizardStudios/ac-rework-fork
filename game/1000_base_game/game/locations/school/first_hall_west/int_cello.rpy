init python:
  class Interactable_school_first_hall_west_cello(Interactable):

    def title(cls):
      return "Sidney Starling's Cello"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Sidney Starling's first cello — the school's only claim to real fame. Right after graduating, Starling made it big as a beatboxing, moonwalking cellist."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_cello")
      actions.append("?school_first_hall_west_cello_interact")


label school_first_hall_west_cello_interact:
  "Bulletproof glass. Built-in alarm system. A teacher with an unhealthy Starling-obsession."
  "Stealing this would be close to impossible; almost as hard as getting into [maya]'s pants."
  return

label isabelle_quest_stolen_maxine_paper_cello:
  "Aha! There's a paper slip on the backside of the cello."
  "Too bad it's inside a case of bulletproof glass..."
  return
