init python:
  class Interactable_school_first_hall_west_back(Interactable):

    def title(cls):
      return "Back"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "It's called the 1st Floor Hall, but it's technically the hall on the second floor. A British linguistic relic, I'm sure. Very posh."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append(["go","First Floor Hall","quest_kate_fate_wrong_int"])
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "holdup":
            actions.append(["go","First Floor Hall","quest_kate_desire_holdup_leave"])
        elif mc["focus"] == "isabelle_piano":
          if quest.isabelle_piano in ("concert","score"):
            actions.append(["go","First Floor Hall","?quest_isabelle_piano_concert_exit_arrow"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "phone_call":
            actions.append(["go","First Floor Hall","?quest_kate_stepping_phone_call_exit"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "phone_call":
            actions.append(["go","First Floor Hall","?quest_kate_wicked_phone_call_exit"])
            return
          elif quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"]:
            actions.append(["go","First Floor Hall","?quest_kate_wicked_school_exit"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "revenge_done" and not school_first_hall_west["scorch_marks"]:
            actions.append(["go","First Floor Hall","quest_isabelle_dethroning_revenge_done_arts_wing_exit"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","First Floor Hall","goto_school_first_hall"])
      actions.append(["go","First Floor Hall","goto_school_first_hall"])
