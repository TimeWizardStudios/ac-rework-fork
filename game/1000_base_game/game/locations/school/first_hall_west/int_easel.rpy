init python:
  class Interactable_school_first_hall_west_easel(Interactable):

    def title(cls):
      return "Easel"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "This is the art teacher's way of encouraging spontaneous creativity. In other words, she loves vulgarities and drawings of dicks."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_easel")
          return
      actions.append("?school_first_hall_west_easel_interact")


label school_first_hall_west_easel_interact:
  "This is like photoshop but without any layers, tools, or cool effects."
  "This easel needs a software update."
  return

label isabelle_quest_stolen_maxine_paper_easel:
  "Here's some paper, but it's too large for a letter..."
  "I don't have a knife to slice it and I sure as hell ain't risking a papercut without having medical insurance."
  return
