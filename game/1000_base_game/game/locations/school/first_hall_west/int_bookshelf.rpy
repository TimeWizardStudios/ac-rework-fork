init python:
  class Interactable_school_first_hall_west_bookshelf(Interactable):

    def title(cls):
      return "Bookcase"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Books — the poor man's source of entertainment."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.berb_fight.in_progress and not quest.berb_fight["book_retrieved"]:
          actions.append("school_first_hall_west_bookshelf_berb_fight")
      actions.append("?school_first_hall_west_bookshelf_interact")


label school_first_hall_west_bookshelf_interact:
  "This bookcase is brimming with classics."
  "{i}\"Food for Thought & Words for Food (Please!)\" by Edgar Allan Hobo{/}"
  "{i}\"Book of the Dammed: the Beaver Encyclopedia\" by J. J. Timberlake{/}"
  "{i}\"So, You Want to Burn Stuff? — the Pyromaniac's Guide to Safe Fires\"{/}"
  return

label school_first_hall_west_bookshelf_berb_fight:
  "Ah, the famous encyclopedia by the beaverologist and humanitarian J. J. Timberlake."
  "Some say it's the Art of War 2.0."
  "Others say it's the last hope of mankind."
  "Either way, it's widely acclaimed for its insights on evil as a force of nature."
  $mc.add_item("book_of_the_dammed")
  $quest.berb_fight["book_retrieved"] = True
  return
