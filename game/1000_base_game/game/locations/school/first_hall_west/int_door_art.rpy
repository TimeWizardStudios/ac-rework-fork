init python:
  class Interactable_school_first_hall_west_door_art(Interactable):

    def title(cls):
      return "Art Classroom"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_piano.started:
          return "How come we never learn body painting? That would make this class much more interesting..."
      else:
        return "The art classroom used to be one of my favorite places. Not that art was ever my strong suit, but seeing jocks failing miserably always filled me with a special kind of joy."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append(["go","Art Classroom","?school_first_hall_west_door_art_kate_fate"])
            return
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "holdup":
            actions.append(["go","Art Classroom","quest_kate_desire_holdup_leave"])
        elif mc["focus"] == "isabelle_piano":
          if quest.isabelle_piano in ("concert","score"):
            actions.append(["go","Art Classroom","?quest_isabelle_piano_concert_art_class"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "phone_call":
            actions.append(["go","Art Classroom","?quest_kate_stepping_phone_call_exit"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "phone_call":
            actions.append(["go","Art Classroom","?quest_kate_wicked_phone_call_exit"])
            return
          elif quest.kate_wicked in ("school","costume","party") and game.hour == 19:
            actions.append(["go","Art Classroom","?quest_kate_wicked_school_art_class"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "revenge":
            if quest.isabelle_dethroning["pea_brains"]:
              actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_dethroning,lockpicking|Use What?","quest_isabelle_dethroning_revenge_art_door_lockpicking"])
            actions.append(["go","Art Classroom","?quest_isabelle_dethroning_revenge_art_door"])
            return
          elif quest.isabelle_dethroning in ("revenge_done","panik"):
            actions.append(["go","Art Classroom","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Art Classroom","goto_school_art_class"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Art Classroom","?quest_lindsey_angel_keycard_school_first_hall_west_door_art"])
            return
      else:
        if quest.isabelle_tour.in_progress and quest.isabelle_tour < "art_class":
          actions.append(["go","Art Classroom","?school_first_hall_west_door_art_interact"])
          return
      actions.append(["go","Art Classroom","goto_school_art_class"])


label school_first_hall_west_door_art_kate_fate:
  "It's fucking locked!"
  return

label school_first_hall_west_door_art_interact:
  "Odd that the door is still locked. Mrs. Bloomer is usually the first teacher to enter the school each morning."
  return
