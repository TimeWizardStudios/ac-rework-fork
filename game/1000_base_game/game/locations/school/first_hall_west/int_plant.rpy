init python:
  class Interactable_school_first_hall_west_plant(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      if quest.lindsey_motive == "lure":
        return "A plant with tiny white flowers."
      elif quest.jo_day == "supplies":
        return "What a dirty little plant! Bet you like that thick soil buried over your roots, don't you?"

    def actions(cls,actions):
      if quest.lindsey_motive == "lure":
        actions.append(["take","Take","quest_lindsey_motive_lure_first_hall_west_plant_take"])
        actions.append("?school_first_hall_west_plant_interact")
      elif quest.jo_day == "supplies":
        actions.append("?quest_jo_day_supplies_plant_five")


label school_first_hall_west_plant_interact:
  call quest_lindsey_motive_lure_first_hall_west_plant_interact
  return
