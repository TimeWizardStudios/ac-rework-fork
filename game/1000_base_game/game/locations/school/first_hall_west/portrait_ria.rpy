init python:
  class Interactable_school_first_hall_west_portrait_ria(Interactable):

    def title(cls):
      return "Portrait"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A hero of the old days — a paladin of love and purity — standing triumphantly over her vanquished foe."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?first_hall_portrait_ria_interact")


label first_hall_portrait_ria_interact:
  "Sure was easier back then. Just slay a dragon or demon. Save the princess. Get laid. No bullshit."
  return
