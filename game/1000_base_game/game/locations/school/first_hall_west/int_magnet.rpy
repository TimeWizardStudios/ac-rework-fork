init python:
  class Interactable_school_first_hall_west_magnet(Interactable):

    def title(cls):
      return "Magnet"

    def description(cls):
      return "There it sits in its lonesome. Desperately trying to attract other magnets — or hell, any kind of metal..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
      else:
        actions.append(["take","Take","school_first_hall_west_magnet_take"])
        actions.append("?school_first_hall_west_magnet_interact")


label school_first_hall_west_magnet_take:
  $school_first_hall_west["magnet"] = False
  $mc.add_item("magnet")
  return

label school_first_hall_west_magnet_interact:
  "They say that a grateful heart is a magnet for miracles."
  "I'm still waiting."
  return
