init python:
  class Interactable_school_first_hall_west_piano(Interactable):

    def title(cls):
      return "Piano"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.piano_tuning.finished:
        return "Playing the piano has always been seen as classy, but everyone knows that true sophistication and excellence can only be achieved with the triangle."
      else:
        return "This piano is so out of tune that even Beethoven would be embarrassed to play for Elise."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "stargreen":
            actions.append("quest_kate_desire_piano_interact")
        elif mc["focus"] == "isabelle_piano":
          if quest.isabelle_piano == "score":
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_piano,score|Use What?","quest_isabelle_piano_score_use_item"])
            actions.append("quest_isabelle_piano_score_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if game.season == 1:
          if quest.piano_tuning.in_progress:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:piano_tuning,tuning|Use What?","school_first_hall_west_piano_tuning"])
          else:
            actions.append(["quest","Quest","school_first_hall_west_piano_quest"])
      actions.append("?school_first_hall_west_piano_interact")


label school_first_hall_west_piano_interact:
  "Allegedly, some girls like it rough, but the abuse of this fine lady has only left her sad and untuned."
  return

label school_first_hall_west_piano_tuning(item):
  if item == "tuning_hammer" and school_first_hall_west["tuning_hammer"]:
    "I've already used this tuning hammer. I need a tuning fork now."
    $quest.piano_tuning.failed_item("tuning",item)
  elif item == "tuning_fork" and school_first_hall_west["tuning_fork"]:
    "I've already used this tuning fork. I need a tuning hammer now."
    $quest.piano_tuning.failed_item("tuning",item)
  elif item in ("tuning_hammer","tuning_fork"):
    $mc.remove_item(item)
    if school_first_hall_west["tuning_hammer"] or school_first_hall_west["tuning_fork"]:
      "There we go! This lady will sing once more."
      $quest.piano_tuning.finish()
    else:
      "Thanks to PornHub, anyone can learn to fuck."
      "Thanks to YouTube, anyone can learn how to tune a piano."
      "The internet is great!"
      "..."
    $mc.add_item(item)
    if item == "tuning_hammer":
      $school_first_hall_west["tuning_hammer"] = True
      if not school_first_hall_west["tuning_fork"]:
        "Okay, I just need a tuning fork now..."
    elif item == "tuning_fork":
      $school_first_hall_west["tuning_fork"] = True
      if not school_first_hall_west["tuning_hammer"]:
        "Okay, I just need a tuning hammer now..."
  else:
    "Tuning the piano with my [item.title_lower] would be an excellent idea if..."
    "Hmm... no. I can't think of a situation where it would be an excellent idea.{space=-100}"
    $quest.piano_tuning.failed_item("tuning",item)
  return

label school_first_hall_west_piano_quest:
  if quest.piano_tuning.finished:
    "Hmm... what to play, what to play..."
    menu(side="middle"):
      extend ""
      "?quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and (not quest.isabelle_locker['time_for_a_change'] or quest.isabelle_hurricane.finished)) and (not quest.isabelle_piano.started or quest.isabelle_piano == 'piano' or quest.isabelle_piano.finished)|Play something sweet and romantic" if (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed):
        if quest.isabelle_piano.finished:
          jump quest_isabelle_piano_sex_repeatable
        else:
          jump quest_isabelle_piano_start
      "?quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and (not quest.isabelle_piano.started or quest.isabelle_piano == 'piano')|Play something sweet and romantic" if (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and not quest.isabelle_piano.finished:
        if quest.isabelle_piano.finished:
          jump quest_isabelle_piano_sex_repeatable
        else:
          jump quest_isabelle_piano_start
      "?quest.lindsey_piano == 'piano' or (quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.lindsey_wrong.finished and (quest.piano_tuning.finished or quest.lindsey_piano['failed']))|Play something fast and exciting" if not (lindsey["romance_disabled"] or quest.lindsey_wrong.failed) and quest.lindsey_piano < "jo_talk":
        jump quest_lindsey_piano_play
      "?quest.isabelle_tour.finished|Play something dark and dramatic" if quest.kate_wicked < "phone_call":
        jump quest_kate_wicked_start
      "?False|Play something playful and spontaneous\n{space=152}{color=#C00}(not yet available){/}": ## Maya requirements
        pass
      "Just press the keys randomly":
        "Making the world a little bit worse, one bleeding ear at the time."
  else:
    "Girls love it when you play the guitar, and a piano is a level above that.{space=-50}"
    "It's in the perfect position to draw attention from both the hall and the closest classrooms."
    "My piano skills have definitely improved, so maybe it's time to test it out?"
    "But tuning this piano requires a tuning hammer and a tuning fork..."
    if ((quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.finished)) and not quest.isabelle_piano.started)
    or (quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and not quest.isabelle_piano.started)) and not game.quest_guide == "kate_wicked":
      $quest.isabelle_piano.start("piano")
    elif quest.isabelle_tour.finished:
      $quest.kate_wicked.start()
    if not quest.piano_tuning.started:
      $quest.piano_tuning.start()
    $quest.piano_tuning["out_of_tune"] = True
  return
