init python:
  class Interactable_school_first_hall_west_portrait_headmistress(Interactable):

    def title(cls):
      return "Portrait"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "This is the portrait of Newfall High's first headmistress. She looks a bit familiar, somehow."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append("quest_kate_fate_wrong_int")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?first_hall_portrait_headmistress_interact")


label first_hall_portrait_headmistress_interact:
  "The old headmistress was an absolute babe, that's for sure. Perhaps a bit on the mature side, but still sizzling hot."
  return
