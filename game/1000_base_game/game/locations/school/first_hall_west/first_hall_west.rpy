init python:
  class Location_school_first_hall_west(Location):

    default_title="Arts Wing"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,0),"school first_hall_west 1fwcorridor"])

      if ((quest.kate_wicked in ("school","costume","party") and game.hour == 19)
      or quest.isabelle_dethroning in ("revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(1405,116),"school first_hall_west night_window"])

      #Paintings
      scene.append([(526,227),"school first_hall_west paintings"])
      scene.append([(1084,270),"school first_hall_west headmistress","school_first_hall_west_portrait_headmistress"])
      scene.append([(234,134),"school first_hall_west painting_top",("school_first_hall_west_portrait_ria",0,100)])
      scene.append([(209,371),"school first_hall_west painting_bottom"])

      #Doors
      scene.append([(906,232),"school first_hall_west library_sign",("school_first_hall_west_door_library",0,56)])
      scene.append([(845,288),"school first_hall_west library","school_first_hall_west_door_library"])
      if school_first_hall_west["scorch_marks"] and not quest.jo_washed.in_progress:
        scene.append([(901,359),"school first_hall_west scorch_marks",("school_first_hall_west_door_library",10,-71)])
      scene.append([(646,306),"school first_hall_west music","school_first_hall_west_door_music"])
      scene.append([(648,227),"school first_hall_west music_sign",("school_first_hall_west_door_music",-2,79)])
      scene.append([(405,264),"school first_hall_west english","school_first_hall_west_door_english"])
      scene.append([(411,136),"school first_hall_west english_sign",("school_first_hall_west_door_english",-1,128)])
      scene.append([(0,194),"school first_hall_west art","school_first_hall_west_door_art"])
      scene.append([(0,12),"school first_hall_west art_sign",("school_first_hall_west_door_art",51,182)])
      if school_first_hall["paint_splash"]:
        scene.append([(141,940),"#school first_hall_west paint_splash"])

      #Kate in her sexy angel costume
      if kate.at("school_first_hall_west","waiting"):
        if not kate.talking:
          if not school_first_hall_west["exclusive"] or "kate" in school_first_hall_west["exclusive"]:
            scene.append([(751,316),"school first_hall_west angel_kate","kate"])

      #Interactables
      scene.append([(580,347),"school first_hall_west cello","school_first_hall_west_cello"])
      scene.append([(525,340),"#school first_hall_west cello_box"])
      scene.append([(1129,319),"school first_hall_west easel","school_first_hall_west_easel"])
      scene.append([(1106,495),"school first_hall_west stool"])

      #Maya
      if maya.at("school_first_hall_west","sitting"):
        if not maya.talking:
          if not school_first_hall_west["exclusive"] or "maya" in school_first_hall_west["exclusive"]:
            scene.append([(1099,360),"school first_hall_west maya_autumn","maya"])

      #More Interactables
      scene.append([(1215,294),"school first_hall_west bookshelf","school_first_hall_west_bookshelf"])
      scene.append([(1220,312),"school first_hall_west books",("school_first_hall_west_bookshelf",44,-18)])
      scene.append([(1240,481),"school first_hall_west sofa_back"])

      #Kate sleeping
      if kate.at("school_first_hall_west","sleeping"):
        if not school_first_hall_west["exclusive"] or "kate" in school_first_hall_west["exclusive"]:
          if not kate.talking:
            scene.append([(1268,418),"school first_hall_west kate_sleeping","kate"])

      scene.append([(1330,569),"school first_hall_west table"])
      if (not school_first_hall_west["plant_taken"] or quest.jo_washed.in_progress or game.season > 1):
        if quest.lindsey_motive == "lure" or quest.jo_day == "supplies":
          scene.append([(1369,482),"school first_hall_west plant","school_first_hall_west_plant"])
        else:
          scene.append([(1369,482),"school first_hall_west plant"])
      scene.append([(1376,541),"school first_hall_west sofa_front"])

      #Isabelle
      if isabelle.at("school_first_hall_west","standing"):
        if not isabelle.talking:
          if not school_first_hall_west["exclusive"] or "isabelle" in school_first_hall_west["exclusive"]:
            scene.append([(1395,283),"school first_hall_west isabelle",("isabelle",-36,36)])

      #Piano
      scene.append([(1531,183),"school first_hall_west piano",("school_first_hall_west_piano",0,50)])
      if school_first_hall_west["music_score"] == "chopin" and not quest.jo_washed.in_progress:
        scene.append([(1793,507),"school first_hall_west chopin_music_score",("school_first_hall_west_piano",-131,-274)])
      scene.append([(1450,804),"school first_hall_west piano_stool"])

      #Spinach
      if spinach.at("school_first_hall_west","running") and not spinach.talking:
        if quest.kate_search_for_nurse["spinach_first_int"]:
          scene.append([(1303,792),"school first_hall_west close_cat","spinach"])

      if not quest.kate_search_for_nurse["kate_found_wrappers"]:
        if quest.kate_search_for_nurse.in_progress and quest.kate_search_for_nurse=="start":
          if not school_first_hall_west["candy9_taken"]:
            scene.append([(1043,889),"school first_hall_west candy9","school_first_hall_west_candy9"])
          if not school_first_hall_west["candy8_taken"]:
            scene.append([(994,814),"school first_hall_west candy8","school_first_hall_west_candy8"])
          if not school_first_hall_west["candy7_taken"]:
            scene.append([(906,799),"school first_hall_west candy7","school_first_hall_west_candy7"])
          if not school_first_hall_west["candy6_taken"]:
            scene.append([(797,838),"school first_hall_west candy6","school_first_hall_west_candy6"])
          if not school_first_hall_west["candy5_taken"]:
            scene.append([(728,898),"school first_hall_west candy5","school_first_hall_west_candy5"])
          if not school_first_hall_west["candy4_taken"]:
            scene.append([(558,887),"school first_hall_west candy4","school_first_hall_west_candy4"])
          if not school_first_hall_west["candy3_taken"]:
            scene.append([(402,874),"school first_hall_west candy3","school_first_hall_west_candy3"])
          if not school_first_hall_west["candy2_taken"]:
            scene.append([(274,898),"school first_hall_west candy2","school_first_hall_west_candy2"])
          if not school_first_hall_west["candy1_taken"]:
            scene.append([(141,950),"school first_hall_west candy1","school_first_hall_west_candy1"])

      if not quest.jo_washed.in_progress:
        if school_first_hall_west["dollar1_spawned_today"] == True and not school_first_hall_west["dollar1_taken_today"]:
          scene.append([(858,524),"school first_hall_west dollar1","school_first_hall_west_dollar1"])
        if school_first_hall_west["dollar2_spawned_today"] == True and not school_first_hall_west["dollar2_taken_today"] and not (game.season == 1 and school_first_hall_west["plant_taken"]):
          scene.append([(1368,501),"school first_hall_west dollar2","school_first_hall_west_dollar2"])
        if school_first_hall_west["dollar3_spawned_today"] == True and not school_first_hall_west["dollar3_taken_today"]:
          scene.append([(1792,439),"school first_hall_west dollar3","school_first_hall_west_dollar3"])
        if not school_first_hall_west["book_taken"]:
          scene.append([(1251,284),"school first_hall_west book","school_first_hall_west_book"])

      if school_first_hall_west['magnet']:
        scene.append([(570,798),"school first_hall_west magnet","school_first_hall_west_magnet"])

      #Mrs. L
      if mrsl.at("school_first_hall_west","standing"):
        if not mrsl.talking:
          if not school_first_hall_west["exclusive"] or "mrsl" in school_first_hall_west["exclusive"]:
            if game.season == 1:
              scene.append([(693,296),"school first_hall_west mrsl","mrsl"])
            elif game.season == 2:
              scene.append([(692,296),"school first_hall_west mrsl_autumn","mrsl"])

      #Kate
      if kate.at("school_first_hall_west","standing"):
        if not kate.talking:
          if not school_first_hall_west["exclusive"] or "kate" in school_first_hall_west["exclusive"]:
            if kate.equipped_item("kate_cheerleader_top") and kate.equipped_item("kate_cheerleader_skirt"):
              scene.append([(1149,253),"school first_hall_west kate","kate"])
            else:
              scene.append([(1149,253),"school first_hall_west kate_undies","kate"])

      if ((quest.kate_wicked in ("school","costume","party") and game.hour == 19)
      or quest.isabelle_dethroning in ("revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(0,0),"#school first_hall_west night_overlay"])

      if quest.isabelle_dethroning in ("revenge_done","panik") and not school_first_hall_west["scorch_marks"]:
        scene.append([(900,314),"school first_hall_west glowing_spiders",("school_first_hall_west_glowing_spiders",0,150)])

      #Arrow Back
      scene.append([(904,921),"school first_hall_west exit_arrow","school_first_hall_west_back"])

    def find_path(self,target_location):
      if target_location.id == "school_music_class":
        return "school_first_hall_west_door_music",dict(marker_offset=(50,0),marker_sector="left_bottom")
      elif target_location.id == "school_english_class":
        return "school_first_hall_west_door_english",dict(marker_offset=(0,0),marker_sector="right_bottom")
      elif target_location.id == "school_art_class":
        return "school_first_hall_west_door_art",dict(marker_offset=(80,80),marker_sector="left_bottom")
      else:
        return "school_first_hall_west_back",dict(marker_offset=(0,0),marker_sector="mid_mid")


label goto_school_first_hall_west:
  if school_first_hall_west.first_visit_today:
    $school_first_hall_west["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_first_hall_west["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_first_hall_west["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_first_hall_west)
  return
