init python:
  class Interactable_school_first_hall_west_door_library(Interactable):

    def title(cls):
      return "Library"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif school_first_hall_west["scorch_marks"]:
        return "Vandals. This is why\nwe can't have nice doors."
      else:
        return "The school library is a research hotspot for historians. It's the only real place of significance in Newfall."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart":
            actions.append(["go","Library","?first_hall_west_door_library_lindseyart"])
            return
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt2":
            actions.append(["go","Library","quest_kate_fate_wrong_int"])
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "holdup":
            actions.append(["go","Library","quest_kate_desire_holdup_leave"])
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "phone_call":
            actions.append(["go","Library","?quest_kate_stepping_phone_call_exit"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "phone_call":
            actions.append(["go","Library","?quest_kate_wicked_phone_call_exit"])
            return
          elif quest.kate_wicked in ("school","costume","party") and game.hour == 19:
            actions.append(["go","Library","?quest_kate_wicked_school_library"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge","revenge_done","panik"):
            actions.append(["go","Library","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Library","?quest_lindsey_angel_keycard_school_first_hall_west_door_library"])
            return
      if school_first_hall_west["scorch_marks"]:
        actions.append(["go","Library","?school_first_hall_west_door_library_interact_scorch_marks"])
      else:
        actions.append(["go","Library","?school_first_hall_west_door_library_interact"])


label school_first_hall_west_door_library_interact:
  "Right, the library is being renovated."
  "Last time around, it didn't open until Christmas..."
  return

label first_hall_west_door_library_lindseyart:
  "Dry dusty books or a soaking wet [lindsey]? The choice is simple."
  return

label school_first_hall_west_door_library_interact_scorch_marks:
  "Someone really did a number on this door..."
  return
