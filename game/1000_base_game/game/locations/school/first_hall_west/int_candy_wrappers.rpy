init python:
  class Interactable_school_first_hall_west_candy9(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy9_interact")

label school_first_hall_west_candy9_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy9_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy8(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy8_interact")

label school_first_hall_west_candy8_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy8_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy7(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy7_interact")

label school_first_hall_west_candy7_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy7_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy6(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy6_interact")

label school_first_hall_west_candy6_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy6_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy5(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy5_interact")

label school_first_hall_west_candy5_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy5_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy4(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy4_interact")

label school_first_hall_west_candy4_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy4_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy3(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy3_interact")

label school_first_hall_west_candy3_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy3_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy2(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy2_interact")

label school_first_hall_west_candy2_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy2_taken"] = True
  return

init python:
  class Interactable_school_first_hall_west_candy1(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_first_hall_west_candy1_interact")

label school_first_hall_west_candy1_interact:
  $mc.add_item("wrapper")
  $school_first_hall_west["candy1_taken"] = True
  return
