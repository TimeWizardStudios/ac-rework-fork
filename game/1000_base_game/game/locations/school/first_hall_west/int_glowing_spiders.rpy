init python:
  class Interactable_school_first_hall_west_glowing_spiders(Interactable):

    def title(cls):
      return "Glowing Spiders"

    def description(cls):
      return "Spiderman or radiation poisoning? Flip a coin."

    def actions(cls,actions):
      actions.append("?school_first_hall_west_glowing_spiders_interact")


label school_first_hall_west_glowing_spiders_interact:
  "What the fuck..."
  "Those spiders are all over the door... it's like they crawled out of the library..."
  "And even more disturbing... [maxine] was right!"
  "..."
  "Ugh, I don't have time to investigate this further! I got to find [lindsey]!{space=-45}"
  return
