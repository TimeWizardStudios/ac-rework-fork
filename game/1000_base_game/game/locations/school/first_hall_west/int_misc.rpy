init python:
  class Interactable_school_first_hall_west_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_west_dollar1_take"])


label school_first_hall_west_dollar1_take:
  $school_first_hall_west["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_west_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_west_dollar2_take"])


label school_first_hall_west_dollar2_take:
  $school_first_hall_west["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_west_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_west_dollar3_take"])


label school_first_hall_west_dollar3_take:
  $school_first_hall_west["dollar3_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_west_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_first_hall_west_book_interact"])


label school_first_hall_west_book_interact:
  $school_first_hall_west["book_taken"] = True
  "This sounds like an interesting read..."
  "{i}\"The Benefits of Turning Your Sister Into a Nuclear Reactor\" by Alberto Uno-roca{/}"
  $mc.intellect+=1
  return
