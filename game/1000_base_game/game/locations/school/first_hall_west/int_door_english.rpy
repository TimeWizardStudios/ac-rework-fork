init python:
  class Interactable_school_first_hall_west_door_english(Interactable):

    def title(cls):
      return "English Classroom"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_piano.started:
        return "English... each essay is like carving out a bit of your soul and smearing it over the paper."
      else:
        return "English... isn't it ironic that the man who said that brevity is the soul of wit has a name that is eighteen letters long?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart":
            actions.append(["go","English Classroom","?first_hall_west_door_english_lindseyart"])
            return
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "holdup":
            actions.append(["go","English Classroom","quest_kate_desire_holdup_leave"])
        elif mc["focus"] == "isabelle_piano":
          if quest.isabelle_piano in ("concert","score"):
            actions.append(["go","English Classroom","?quest_isabelle_piano_concert_english_class"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "phone_call":
            actions.append(["go","English Classroom","?quest_kate_stepping_phone_call_english"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "phone_call":
            actions.append(["go","English Classroom","?quest_kate_wicked_phone_call_exit"])
            return
          elif quest.kate_wicked in ("school","costume","party") and game.hour == 19:
            actions.append(["go","English Classroom","?quest_kate_wicked_school_english_class"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge_done","panik"):
            actions.append(["go","English Classroom","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","English Classroom","goto_school_english_class"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","English Classroom","?quest_lindsey_angel_keycard_school_first_hall_west_door_english"])
            return
        elif mc["focus"] in ("kate_moment","isabelle_gesture"):
          if quest.isabelle_gesture == "wait":
            actions.append(["go","English Classroom","?quest_isabelle_gesture_revenge_early"])
            return
          elif quest.isabelle_gesture == "revenge":
            actions.append(["go","English Classroom","quest_isabelle_gesture_revenge"])
      else:
        if quest.isabelle_red == "black_note" and not quest.isabelle_red["english_class_empty"]:
          actions.append(["go","English Classroom","quest_isabelle_red_black_note_english_door"])
      actions.append(["go","English Classroom","goto_school_english_class"])


label first_hall_west_door_english_lindseyart:
  "A wet girl in the art room is better than a dry one in the bean bag. Yeah, that's a saying."
  return
