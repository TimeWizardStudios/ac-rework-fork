init python:
  class Interactable_school_music_class_triangle(Interactable):

    def title(cls):
      return "Triangle"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Those who fear the Bermuda Triangle have never met a\ntriangle musician."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_red == "triangle":
          actions.append("quest_isabelle_red_triangle")
      actions.append("?school_music_class_triangle_interact")


label school_music_class_triangle_interact:
  "I can never resist the urge..."
  window hide
  play sound "<from 4.0>triangle"
  window auto
  return
