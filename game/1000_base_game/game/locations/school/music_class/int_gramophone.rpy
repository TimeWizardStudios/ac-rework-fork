init python:
  class Interactable_school_music_class_gramophone(Interactable):

    def title(cls):
      return "Gramophone"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Compared to the stereo system, this old thing looks like a horse cart parked next to a sports car."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_music_class_gramophone_interact")


label school_music_class_gramophone_interact:
  "Without a vinyl, turning the crank won't do anything."
  "Just like talking to girls who aren't into you."
  return
