init python:
  class Interactable_school_music_class_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_piano == "listen_music_class":
        return "I don't want to let [lindsey] catch me looking at the door. The balance between disinterest and attention is crucial — too much of either will turn her off.\n\nGirls are weird."
      else:
        return "The best way to annoy the art and English teacher is to stand really close to the door, using my secret weapon — the triangle."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "listen_music_class":
            actions.append(["go","Fine Arts Wing","?quest_lindsey_piano_listen_music_class_door"])
            return
        elif mc["focus"] == "lindsey_piano":
          if quest.jo_washed.in_progress:
            actions.append(["go","Fine Arts Wing","goto_school_first_hall_west"])
      actions.append(["go","Fine Arts Wing","goto_school_first_hall_west"])
