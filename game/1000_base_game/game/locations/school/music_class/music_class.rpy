init python:
  class Location_school_music_class(Location):

    default_title = "Music Class"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      if quest.kate_wicked == "party":
        scene.append([(0,0),"school music_class background_party"])
      else:
        scene.append([(0,0),"school music_class background"])
        scene.append([(456,548),"school music_class platform"])
        scene.append([(587,188),"school music_class speakers"])
        if quest.piano_tuning > "cabinet" and not (quest.isabelle_red == "cabinet" or quest.jo_washed.in_progress):
          scene.append([(258,151),"school music_class cabinet"])
          scene.append([(383,214),"school music_class top_compartment",("school_music_class_top_compartment",0,50)])
          scene.append([(383,337),"school music_class middle_compartment",("school_music_class_middle_compartment",0,50)])
          scene.append([(383,449),"school music_class bottom_compartment",("school_music_class_bottom_compartment",0,50)])
        else:
          scene.append([(258,151),"school music_class cabinet",("school_music_class_cabinet",54,113)])
        scene.append([(927,398),"school music_class podium","school_music_class_teacher_podium"])
        scene.append([(1718,155),"school music_class door",("school_music_class_door",0,110)])
        scene.append([(1703,578),"school music_class glass_case",("school_music_class_glass_case",-200,350)])
        scene.append([(1310,524),"school music_class table"])
        scene.append([(1433,516),"school music_class stereo_system","school_music_class_stereo_system"])
        scene.append([(1316,467),"school music_class gramophone","school_music_class_gramophone"])
        scene.append([(1452,596),"school music_class violin","school_music_class_violin"])
        scene.append([(1011,174),"school music_class guitar"])
        scene.append([(793,0),"school music_class disco_ball"])
        scene.append([(840,0),"school music_class triangle",("school_music_class_triangle",0,300)])
        scene.append([(6,111),"school music_class bookcase"])
        scene.append([(0,519),"school music_class chairs"])
        scene.append([(846,520),"school music_class michael_jackson_statue",("school_music_class_statue",-30,0)])

        ## Lindsey best girl
        if lindsey.at("school_music_class","standing"):
          if not lindsey.talking:
            scene.append([(1158,748),"school music_class lindsey","lindsey"])
            scene.append([(1150,930),"school music_class lindsey_chair"])

    def find_path(self,target_location):
      return "school_music_class_door"


label goto_school_music_class:
  call goto_with_black(school_music_class)
  return
