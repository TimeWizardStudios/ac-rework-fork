init python:
  class Interactable_school_music_class_statue(Interactable):

    def title(cls):
      return "Statue"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A statue of an unknown man touching his crotch, longing to just {i}beat it.{/}"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if game.season == 1:
          if quest.isabelle_tour.finished and quest.jacklyn_art_focus.finished and quest.jacklyn_broken_fuse.finished:
            actions.append(["flirt","Flirt","school_music_class_statue_flirt"])
      actions.append("?school_music_class_statue_interact")


label school_music_class_statue_interact:
  "This statue used to give me comfort..."
  "A short man rocking a tall man's world. Truly iconic."
  return

label school_music_class_statue_flirt:
  menu(side="middle"):
    "?mc.lust>=10@[mc.lust]/10|{image=stats lust}|Beat It":
      if school_music_class["masturbation_jacklyn_today"]:
        "Getting caught masturbating by [jacklyn] was enough thrill for today.{space=-5}"
        "Thankfully, she didn't seem to mind it. But who knows how other people would've reacted to it?"
      else:
        "I've always wondered what it would be like to touch my crotch\nlike, MJ..."
        "Like..."
        mc "Woooaauw!"
        "Like..."
        mc "Hee-hee!"
        "Damn, I actually get it!"
        "A sort of weird narcissistic boner..."
        "Like... yeah, I'm hot. I don't care what anyone else says."
        "And it feels damn good."
        "Imagine blowing a load on Sidney Starling's glass case, like..."
        mc "Pow! Yeeh-hee!"
        "They'd probably think it was Chad and laugh it off."
        "That guy is always whipping his dick around..."
        "..."
        "Well, no one's here, so..."
        window hide
        show black onlayer screens zorder 100
        show jacklyn music_class_masturbation_stroke2
        with Dissolve(.5)
        pause 1.0
        hide black onlayer screens with Dissolve(.5)
        window auto
        "God, I feel like I haven't gotten off in a week."
        "There's so much stress, so much nagging..."
        "A man has to find ways to relieve the pressure."
        window hide
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        pause 0.05
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        pause 0.1
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        pause 0.1
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        window auto
        "A hot mouth or pussy is of course great..."
        "But sometimes, your own hand feels just right."
        window hide
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        pause 0.05
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        pause 0.1
        show expression "jacklyn avatar events music_class_masturbation shadow":
          xpos 1127 ypos 21 alpha 0.0
          easein 0.5 alpha 1.0
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        show expression "jacklyn avatar events music_class_masturbation shadow":
          easeout 0.5 alpha 0.0
        pause 0.05
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.125)
        window auto
        "Imagination is a powerful thing, too."
        "Sometimes, it's better than porn."
        "Even if I'd scour the internet, I'd never find [flora] kissing [isabelle]..."
        window hide
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.175)
        pause 0.1
        show expression "jacklyn avatar events music_class_masturbation shadow":
          easein 0.5 alpha 1.0
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.175)
        show expression "jacklyn avatar events music_class_masturbation shadow":
          easeout 0.5 alpha 0.0
        pause 0.1
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.175)
        window auto
        "Or [mrsl] eating out [kate]..."
        "Damn, I wonder who would be the top there..."
        "Maybe [kate] would be the one licking pussy in that scenario..."
        "I can't decide which is hotter..."
        window hide
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        pause 0.1
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        pause 0.05
        show expression "jacklyn avatar events music_class_masturbation shadow":
          easein 0.5 alpha 1.0
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        pause 0.1
        show jacklyn music_class_masturbation_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_stroke2 with Dissolve(.15)
        window auto
        "Or maybe [jacklyn]—"
        window hide
        play sound "<from 0.1>open_door"
        pause 0.75
        window auto
        "Shit. Fuck. Cock."
        hide expression "jacklyn avatar events music_class_masturbation shadow"
        show jacklyn music_class_masturbation_jacklyn_stand_hide
        with dissolve2
        "Please, don't notice me! Please, please, please..."
        jacklyn music_class_masturbation_jacklyn_stand_hide "[mc]? I put my bloodhounds on your trail earlier."
        mc "Uh, I'm a bit busy! Let's talk later, okay?"
        window hide
        show jacklyn music_class_masturbation_jacklyn_excited_sit_hide with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_excited_sit_hide "This will be a flasher."
        "Fuck me! It absolutely will!"
        jacklyn music_class_masturbation_jacklyn_smile_down_sit_hide "I just wanted to flip the kinks of your latest—"
        jacklyn music_class_masturbation_jacklyn_surprised_down_sit_hide "..."
        "Crap."
        "That's the expression of impending doom..."
        jacklyn music_class_masturbation_jacklyn_excited_sit_hide "As I was saying, I just wanted to flip the kinks of your latest\nart piece."
        "..."
        "What's happening?"
        jacklyn music_class_masturbation_jacklyn_smile_sit_hide "You down or nah?"
        mc "For... flipping kinks?"
        jacklyn music_class_masturbation_jacklyn_smile_sit_hide "You know, going over the deets."
        mc "Ah, err... I'm a bit busy..."
        jacklyn music_class_masturbation_jacklyn_flirty_down_sit_hide "I can see that."
        jacklyn music_class_masturbation_jacklyn_flirty_down_sit_hide "But I don't need your spankers for this, just your juice box."
        mc "Um..."
        jacklyn music_class_masturbation_jacklyn_flirty_sit_hide "No need for roses now. Just keep riding that skin-elevator."
        mc "T-the what?"
        jacklyn music_class_masturbation_jacklyn_smile_sit_hide "I'm not going to wear the wig and gavel, bro."
        "God, is she saying what I think she's saying?"
        "[jacklyn] wants me to keep going in front of her?"
        "Fuck me, I never thought a nervous boner was a thing..."
        "But somehow... this is oddly hot."
        mc "You're not going to give me detention?"
        jacklyn music_class_masturbation_jacklyn_excited_sit_hide "This ain't my court. Art and passion are above the law."
        "Man, this is so weird, but..."
        window hide
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.5)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        pause 0.15
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 "So, topic reset. Your latest piece is stars."
        mc "T-thanks..."
        window hide
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        pause 0.15
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_neutral_sit_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_neutral_sit_stroke2 "It's stars, but there's a billion stars, comprende?"
        jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 "It can always get brighter. I want Sirius! I want Venus!"
        mc "I'll work... harder..."
        window hide
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        pause 0.15
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke3 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke1 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 with Dissolve(.15)
        show jacklyn music_class_masturbation_jacklyn_annoyed_sit_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_annoyed_sit_stroke2 "Damn straight you will!"
        jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 "And I'll be there to guide you."
        window hide
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        pause 0.15
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke3 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke1 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 with Dissolve(.175)
        show jacklyn music_class_masturbation_jacklyn_flirty_sit_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_flirty_sit_stroke2 "Just soak up the juices of inspiration."
        window hide
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 "I'll be your muse."
        window hide
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.1
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        pause 0.05
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 "But the art itself is in your hands."
        jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 "Every stroke... art is at your fingertips."
        window hide
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke3 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke1 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_flirty_down_reveal_stroke2 with Dissolve(.125)
        show jacklyn music_class_masturbation_jacklyn_blush_reveal_stroke2 with Dissolve(.5)
        window auto
        jacklyn music_class_masturbation_jacklyn_blush_reveal_stroke2 "So, grip it hard, and squeeze out perfection!"
        mc "F-fuck! I'm going to—"
        menu(side="middle"):
          extend ""
          "Cum openly":
            "There comes a time in your life when you have to make a choice."
            "A choice between right and wrong..."
            "Between good and evil..."
            "But today, the only choice is to shoot my load proudly into the air."
            "Today, the choice is to rise out of sexual repression."
            "Today—"
            window hide
            show jacklyn music_class_masturbation_jacklyn_blush_reveal_cum_proud with vpunch
            pause 0.25
            show jacklyn music_class_masturbation_jacklyn_blush_reveal_stroke2 with Dissolve(.5)
            window auto
            jacklyn music_class_masturbation_jacklyn_blush_reveal_stroke2 "Scheiße Shimamoto!"
            jacklyn music_class_masturbation_jacklyn_blush_reveal_stroke2 "It's Pompeii all over again!"
            mc "Sorry..."
            jacklyn music_class_masturbation_jacklyn_annoyed_reveal_stroke2 "What are you wringing your hands about? That was aces!"
            "She's right. Why should I be ashamed for doing what I came here\nto do?"
            "I'll paint the walls with my seed if I have to! Nothing will stand in\nmy way!"
            jacklyn music_class_masturbation_jacklyn_smile_sit_stroke2 "Anyway, I have to dip now."
            jacklyn music_class_masturbation_jacklyn_excited_sit_stroke2 "Remember, I want Venus!"
            window hide
            show jacklyn music_class_masturbation_stroke2 with Dissolve(.5)
            window auto
            "Crap, I forgot what that was."
            "But god damn..."
            "I can't believe I cleared that level without a year-long detention."
            "I wish [jacklyn] was here the first time around..."
          "Be discreet":
            "Oh, god... this is too embarrassing..."
            "I... I can't..."
            window hide
            show jacklyn music_class_masturbation_jacklyn_blush_reveal_cum_embarrassed with vpunch
            pause 0.25
            show jacklyn music_class_masturbation_jacklyn_surprised_reveal_hide with Dissolve(.5)
            window auto
            jacklyn music_class_masturbation_jacklyn_surprised_reveal_hide "What happened, shooter?"
            jacklyn music_class_masturbation_jacklyn_surprised_reveal_hide "You played the show, but hid during the ovations."
            mc "I guess I wasn't ready..."
            jacklyn music_class_masturbation_jacklyn_confident_reveal_hide "In time you will be, baby bird."
            jacklyn music_class_masturbation_jacklyn_confident_reveal_hide "Keep spreading those wings."
            mc "Uh... thanks?"
            jacklyn music_class_masturbation_jacklyn_excited_sit_hide "Anyway, time for me to scooch and smooch. Peace!"
            window hide
            show jacklyn music_class_masturbation_hide with Dissolve(.5)
            window auto
            "Well, that was weird. Not sure how I feel about it..."
            "But hey, at least I didn't get detention."
        window hide
        if not school_music_class["masturbation_jacklyn"]:
          $game.notify_modal(None, "Love or Lust", "You have unlocked\na repeatable scene\nwith [jacklyn]!", wait=5.0)
          pause 0.25
        show black onlayer screens zorder 100 with Dissolve(.5)
        pause 1.0
        hide jacklyn
        hide black onlayer screens
        with Dissolve(.5)
        $unlock_replay("jacklyn_interruption")
        $school_music_class["masturbation_jacklyn"]+=1
        $school_music_class["masturbation_jacklyn_today"] = True
        window auto
    "?False@[mc.lust]/15|{image=stats lust}|{space=71}Thriller\n{color=#C00}(not yet available){/}": ## Placeholder ## Repeatable Sex Scene ##
      pass
    "Not now":
      pass
  return
