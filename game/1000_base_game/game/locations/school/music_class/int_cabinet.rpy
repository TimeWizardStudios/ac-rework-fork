init python:
  class Interactable_school_music_class_cabinet(Interactable):

    def title(cls):
      return "Cabinet"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "There's a reason the music teacher never opens this cabinet.\n\n{i}Virtuoso vibrato catastrofico de musica...{/} or whatever you call a massive collapse in musical terms."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_red == "cabinet":
          actions.append("quest_isabelle_red_cabinet")
      actions.append("?school_music_class_cabinet_interact")


label school_music_class_cabinet_interact:
  "This cabinet is more stuffed than a turkey at Thanksgiving."
  "Thousands of dollars worth in music equipment just waiting to come crashing down..."
  "Okay, let's see here..."
  "A tuning fork is stuck in the top, but getting it out would require a replacement item of the same size."
  "Hmm... nothing really of use in the middle..."
  "Ah, there's a tuning hammer in the bottom!"
  "B-but, hnngh... the door is... stuck..."
  "Seems like it'll only open if the door to the middle compartment can also be opened."
  "However, opening it now would cause everything inside to spill out in an avalanche of musical instruments and equipment..."
  "..."
  "Hmm... a long thin item could potentially be pushed into a key position to keep all the items in check..."
  if quest.piano_tuning == "cabinet":
    $quest.piano_tuning.advance("baton")
  return
