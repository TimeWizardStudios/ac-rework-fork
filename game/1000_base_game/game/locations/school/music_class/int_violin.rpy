init python:
  class Interactable_school_music_class_violin(Interactable):

    def title(cls):
      return "Disassembled Violin"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The parts are all there, but putting it together requires both musical knowledge and intelligence. Not to mention the right tools."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_music_class_violin_interact")


label school_music_class_violin_interact:
  "The music teacher has promised that anyone who can assemble the violin will get an A on their next test..."
  "Not sure how many points that would translate into with the new system.{space=-100}"
  return
