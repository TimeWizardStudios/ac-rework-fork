init python:
  class Interactable_school_music_class_teacher_podium(Interactable):

    def title(cls):
      return "Teacher's Podium"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The music teacher has a good voice, but she has other assets too, which are the exact opposite of what you would call C-flat."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_music_class["got_conductor_baton"]:
          actions.append(["take","Take","school_music_class_teacher_podium_take"])
      actions.append("?school_music_class_teacher_podium_interact")


label school_music_class_teacher_podium_take:
  "I always thought the conductor's baton looked a bit like a wand. Maybe I can use it to cast a love-spell on someone?"
  "Hello, desperation, my old friend..."
  $school_music_class["got_conductor_baton"] = True
  $mc.add_item("conductor_baton")
  return

label school_music_class_teacher_podium_interact:
  "Things typically aren't as bland as they seem... unless you're talking about a teacher's podium. An irredeemable piece of furniture."
  return
