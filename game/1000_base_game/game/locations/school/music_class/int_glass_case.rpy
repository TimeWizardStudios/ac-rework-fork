init python:
  class Interactable_school_music_class_glass_case(Interactable):

    def title(cls):
      return "Sidney Starling's Debut Album"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Titled \"A Tiny Box of Moonlight,\" Sidney Starling's debut album took the world by storm."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_music_class_glass_case_interact")


label school_music_class_glass_case_interact:
  "The last music teacher retired shortly after Starling's graduation."
  "This vinyl copy was signed and delivered in person as a thanks."
  "The current music teacher has since tried to find a star-talent student of their own, without much success."
  return
