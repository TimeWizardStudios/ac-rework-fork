init python:
  class Interactable_school_music_class_stereo_system(Interactable):

    def title(cls):
      return "Stereo System"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A stereotypical stereo system. Sounds like all the other stereo systems."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "listen_music_class":
            if not quest.lindsey_piano["stereo_system"]:
              actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_piano,stereo_system|Use What?","quest_lindsey_piano_listen_music_class_stereo_system_use_item"])
            actions.append("?quest_lindsey_piano_listen_music_class_stereo_system_interact")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_music_class_stereo_system_interact")


label school_music_class_stereo_system_interact:
  $guard.default_name = "Speakers" ## Workaround to use the gray textbox for the "Speakers" character
  guard "Welcome to music class, dear Newfall student."
  guard "As you may know, the changes to the curriculum are now in full effect.{space=-55}"
  guard "This means that I, your humble music teacher, am no longer obligated to teach in person."
  guard "Lectures will perhaps be supplied over the year. Maybe. And if not, I'm sure you'll be able to find educational videos online."
  guard "Don't worry, though! The final examination will be as thorough as ever.{space=-50}"
  guard "So, while you sweat and work your asses off, I'll be enjoying a tinnitus-free pina colada on the beach."
  guard "That's a joke, by the way, if this recording somehow ends up in the administration's hands..."
  guard "..."
  guard "...no, I don't want to buy a seashell necklace! Do I look like a hippie to you?"
  guard "..."
  guard "Right, sorry about that. Now, if you'll excuse me, I have to go surfing... uh, the web? Surf the web, yes! For school-related stuff!"
  guard "Don't destroy the classroom while I'm away!"
  guard "Or do. It's not like I care."
  guard "So long, suckers."
  $guard.default_name = "Guard"
  return
