init python:
  class Interactable_school_music_class_top_compartment(Interactable):

    def title(cls):
      return "Cabinet (top compartment)"

    def description(cls):
      return "There's a reason the music teacher never opens this cabinet.\n\n{i}Virtuoso vibrato catastrofico de musica...{/} or whatever you call a massive collapse in musical terms."

    def actions(cls,actions):
      if school_music_class["bottom_compartment"]:
        if school_music_class["top_compartment"] == "tuning_fork":
          if not school_first_hall_west["tuning_fork"]:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:piano_tuning,top_compartment1|Use What?","school_music_class_top_compartment_use_item1"])
          actions.append("?school_music_class_top_compartment_interact1")
        elif school_music_class["top_compartment"] == "conductor_baton":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:piano_tuning,top_compartment2|Use What?","school_music_class_top_compartment_use_item2"])
          actions.append("?school_music_class_top_compartment_interact3")
      else:
        actions.append("?school_music_class_top_compartment_interact2")

label school_music_class_top_compartment_interact1:
  "A tuning fork is stuck in this compartment, but getting it out would require a replacement item of the same size."
  if school_first_hall_west["tuning_fork"]:
   "It would also require me to have another need for it."
  return

label school_music_class_top_compartment_interact2:
  "Opening this door right now would cause the apocalypse..."
  "Without the tuning hammer in the bottom compartment holding everything together, it's just too risky."
  return

label school_music_class_top_compartment_use_item1(item):
  if item == "conductor_baton":
    $school_music_class["top_compartment"] = "conductor_baton"
    $mc.remove_item("conductor_baton")
    "Baton cum fork. Alea iacta est."
    $mc.add_item("tuning_fork")
    if quest.piano_tuning in ("baton","hammer"):
      $quest.piano_tuning.advance("fork")
  else:
    "Much like the piano outside, my choice of items need some fine tuning.{space=-60}"
    $quest.piano_tuning.failed_item("top_compartment1",item)
  return

label school_music_class_top_compartment_interact3:
  "One instrument goes in, one comes out. Musical economy, baby!"
  return

label school_music_class_top_compartment_use_item2(item):
  if item == "tuning_fork":
    $school_music_class["top_compartment"] = "tuning_fork"
    $mc.remove_item("tuning_fork")
    "Return to sender... or in this case, holder."
    $mc.add_item("conductor_baton")
    if quest.piano_tuning == "fork" and school_first_hall_west["tuning_fork"]:
      $quest.piano_tuning.advance("hammer")
  else:
    "Suppose putting my [item.title_lower] to use like this. Suppose it would work."
    "Supposed wrong."
    $quest.piano_tuning.failed_item("top_compartment2",item)
  return


init python:
  class Interactable_school_music_class_middle_compartment(Interactable):

    def title(cls):
      return "Cabinet (middle compartment)"

    def description(cls):
      return "Boxes and items stacked on top of each other. When catastrophe strikes, the cacophony shall be heard across the entirety of the mortal plane... and probably most of the school."

    def actions(cls,actions):
      if not school_music_class["middle_compartment"]:
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:piano_tuning,middle_compartment|Use What?","school_music_class_middle_compartment_use_item"])
        actions.append("?school_music_class_middle_compartment_interact1")
      else:
        actions.append(["take","Take","school_music_class_middle_compartment_take"])
        actions.append("?school_music_class_middle_compartment_interact2")


label school_music_class_middle_compartment_interact1:
  "If I open this door now, everything inside will spill out in an avalanche of musical instruments and equipment."
  "A long thin item could potentially be pushed into a key position to keep all the items in check..."
  return

label school_music_class_middle_compartment_use_item(item):
  if item == "conductor_baton":
    $school_music_class["middle_compartment"] = "conductor_baton"
    $mc.remove_item("conductor_baton")
    "Reversed jackstraw. Revolutionary in porn."
    if quest.piano_tuning in ("baton","fork"):
      $quest.piano_tuning.advance("hammer")
  elif item == "tuning_fork":
    "This tuning fork is long, but not thin enough. What's even thinner?"
    $quest.piano_tuning.failed_item("middle_compartment",item)
  else:
    "My [item.title_lower] won't fit. Not every day that happens to me."
    $quest.piano_tuning.failed_item("middle_compartment",item)
  return

label school_music_class_middle_compartment_interact2:
  "Everything behind the door balances on a thin stick, but at least it can be opened."
  return

label school_music_class_middle_compartment_take:
  "I believe this belongs to me... or the music teacher, but who's keeping track?"
  $school_music_class["middle_compartment"] = None
  $mc.add_item("conductor_baton")
  return


init python:
  class Interactable_school_music_class_bottom_compartment(Interactable):

    def title(cls):
      return "Cabinet (bottom compartment)"

    def description(cls):
      return "Like Atlas carried the sky on his shoulders, the bottom compartment of this cabinet strains under the weight of its bloated siblings."

    def actions(cls,actions):
      if school_music_class["bottom_compartment"]:
        if school_music_class["middle_compartment"] and school_music_class["top_compartment"] and not school_first_hall_west["tuning_hammer"]:
          actions.append(["take","Take","school_music_class_bottom_compartment_take"])
        else:
          actions.append("?school_music_class_bottom_compartment_interact1")
      else:
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:piano_tuning,bottom_compartment|Use What?","school_music_class_bottom_compartment_use_item"])
        actions.append("?school_music_class_bottom_compartment_interact2")


label school_music_class_bottom_compartment_interact1:
  "There's a tuning hammer in this compartment, but its door is stuck."
  if school_first_hall_west["tuning_hammer"]:
    "Luckily, I don't have another need for it."
  else:
    "It'll only open if the door to the middle compartment can also be opened.{space=-100}"
  return

label school_music_class_bottom_compartment_take:
  "Okay, now this door opens!"
  "The only item that can be removed without causing widespread ruination and calamity is exactly the tuning hammer. Lucky me!"
  $school_music_class["bottom_compartment"] = None
  $mc.add_item("tuning_hammer")
  return

label school_music_class_bottom_compartment_interact2:
  "Removing anything else would cause widespread ruination and calamity. It's too early in the day for that."
  return

label school_music_class_bottom_compartment_use_item(item):
  if item == "tuning_hammer":
    $school_music_class["bottom_compartment"] = "tuning_hammer"
    $mc.remove_item("tuning_hammer")
    "There and back again — a striking, smashing, tuning journey."
    if quest.piano_tuning == "hammer" and school_first_hall_west["tuning_hammer"]:
      $quest.piano_tuning.advance("fork")
  else:
    "Putting my [item.title_lower] inside this cabinet would be like putting a straw into a haystack."
    $quest.piano_tuning.failed_item("bottom_compartment",item)
  return
