init python:
  class Interactable_school_gym_signup_list(Interactable):

    def title(cls):
      return "Sign-Up List"

    def description(cls):
      if quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Tanner, obviously. Brad, yup. Chad? Where is Chad? Oh, there he is. Yeah, if I ever made a hit list, this is what it would look like."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_gym_signup_list_interact")


label school_gym_signup_list_interact:
  menu(side="middle"):
    "Sign up for gym focus classes":
      #If 3 other focus classes:
      #  Three focus classes. Gym is not one of them. Small victories.
      #If already signed up:
      if quest.isabelle_tour["gym_signed"]:
        "Where's the I-changed-my-mind-list?"
        "The only upside of this is that I can't sign up twice."
      else:
        "This is probably one of the dumbest things I've ever done, but girls dig muscles, so..."
        $quest.isabelle_tour["gym_signed"] = True
    "I'd rather take a fall down a flight of stairs":
      if quest.isabelle_tour["gym_signed"]:
        "I already signed my life away..."
      else:
        #"Sports is for idiots."
        return
  return
