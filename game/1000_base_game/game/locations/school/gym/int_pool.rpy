init python:
  class Interactable_school_gym_pool(Interactable):

    def title(cls):
      return "Kiddie Pool"

    def description(cls):
      return "An inflatable pool made for children aged 3-5. Great."

    def actions(cls,actions):
      actions.append("?school_gym_pool_interact")


label school_gym_pool_interact:
  "They talk about being tossed into the deep end."
  "This is the opposite of that."
  return

