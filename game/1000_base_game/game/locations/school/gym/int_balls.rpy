init python:
  class Interactable_school_gym_balls(Interactable):

    def title(cls):
      return "Ball"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.maxine_lines == "shot":
        return "Never seen the jocks hit the shot either... because it's usually from behind.\n\nMy poor clapping asscheeks..."
      elif quest.mrsl_HOT.started:
        return "Being tall isn't just good for playing basketball, it's apparently also vital for getting a girlfriend."
      elif quest.lindsey_nurse.ended:
        return "Would you rather have one ball-sized nut, or two nut-sized balls?\n\nYour answer may kill someone with an allergy."
      else:
        return "With balls this big, one could really score."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.maxine_lines == "shot":
          actions.append("school_gym_balls_interact_maxine_lines")
      if quest.mrsl_HOT.started:
        actions.append("?school_gym_balls_interact_mrsl_hot")
      if quest.lindsey_nurse.ended:
        actions.append("?school_gym_balls_lindsey_nurse")
      actions.append("?school_gym_balls_interact")


label school_gym_balls_interact_maxine_lines:
  menu(side="middle"):
    "Take the shot":
      #If the mc Consumed 7 HP within the last hour:
      if quest.maxine_lines['doped_up_now']:
        $school_gym['ball_3_hidden'] = True
        $school_gym['ball_7_shown'] = False
        "Aim: locked in like a laser."
        "Power: as hard as Zeus's dick at the zoo."
        "And... fire!"
        window hide
        #cut to black
        $process_event("update_state")
        show school gym ball_throw at gym_basketball_hit
        #show ball staying wherever it landed until advance/leave (now) ball_7
        #always use same ball
        pause 1.0
        show black with Dissolve(.5)
        #First hoop then bounce
        play sound "bball_hoop"
        pause 1.0
        play sound "bball_bounce"
        $set_dialog_mode("default_no_bg")
        "..."
        hide school gym ball_throw
        $school_gym['ball_7_shown'] = "six_pointer"
        $process_event("update_state")
        hide black with Dissolve(.5)
        $set_dialog_mode("")
        window auto
        "Score!"
        "That was at least a six pointer!"
        show maxine skeptical with Dissolve(.5)
        maxine skeptical "...How did you do that?"
        mc "Skills! What else?"
        maxine thinking flip "Are you telling the truth?"
        mc "Of course."
        maxine sad flip "Very well... follow me."
        show maxine sad flip at disappear_to_right
        "Not sure where [maxine] is going, but I better keep up."
        $quest.maxine_lines.advance("reward")
      else:
        $school_gym['ball_3_hidden'] = True
        $school_gym['ball_7_shown'] = False
        $process_event("update_state")
        pause(.5)
        show school gym ball_throw at gym_basketball_miss
        play sound "bball_bounce"
        "He shoots! He sco—"
        $school_gym['ball_7_shown'] = True
        $process_event("update_state")
        hide school gym ball_throw
        "Miss. Always miss."
        "I need to find another way..."
        "Maybe doping of some sort?"
        $quest.maxine_lines["maybe_doping"] = True
    "Admit defeat":
      "This is never going to happen..."
      "I need to find another way."
      "Maybe doping of some sort?"
      $quest.maxine_lines["maybe_doping"] = True
  return

label school_gym_balls_lindsey_nurse:
  menu(side="middle"):
    "Take the shot":
      "Oh! It's a..."
      "It's a..."
      "Miss."
    "Pussy out":
      return
  return

label school_gym_balls_interact:
  menu(side="middle"):
    "Take the shot":
      "..."
      "Miss."
    "Don't embarrass yourself":
      return
  return

label school_gym_balls_interact_mrsl_hot:
  menu(side="middle"):
    "Take the shot":
      "All right, here goes nothing..."
      "..."
      "Score!"
      "Did anyone see that?"
      "..."
      "Wait, that was my own hoop."
    "Pussy out":
      return
  return
