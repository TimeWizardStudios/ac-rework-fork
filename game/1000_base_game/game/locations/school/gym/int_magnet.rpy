init python:
  class Interactable_school_gym_magnet(Interactable):
    def title(cls):
      return "Magnet"
    def description(cls):
      return "Peace was never an option."
    def actions(cls,actions):
      actions.append(["take","Take","school_gym_magnet_take"])
      actions.append("?school_gym_magnet_interact")

label school_gym_magnet_take:
  $school_gym["magnet"] = False
  $mc.add_item("magnet")
  return

label school_gym_magnet_interact:
  "The thing that no one understands is that there are no villains or heroes."
  "..."
  "Not sure where I was going with that..."
  return
