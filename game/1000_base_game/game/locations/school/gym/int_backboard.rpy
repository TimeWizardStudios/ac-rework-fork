init python:
  class Interactable_school_gym_backboard(Interactable):

    def title(cls):
      return "Basketball Hoop"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.mrsl_HOT.started:
        return "Video games are so much better than sports. It's not just good genetics that decide the winner."
      elif quest.lindsey_nurse.ended:
        return "Why do some people have to jump through hoops to get a good education, while others receive scholarships for chasing balls?"
      else:
        return "Three points? Two points? Really, what's the point?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.mrsl_HOT.started:
        actions.append("?school_gym_backboard_interact_mrsl_hot")
      if quest.lindsey_nurse.ended:
        actions.append("?school_gym_backboard_lindsey_nurse")
      actions.append("?school_gym_backboard_interact")


label school_gym_backboard_lindsey_nurse:
  "Life's not about scoring or winning..."
  "It's about missing shots and losing."
  return

label school_gym_backboard_interact:
  "The only thing that matters here is hitting the shot."
  "If you miss, everyone will know you're a loser."
  return

label school_gym_backboard_interact_mrsl_hot:
  "There's this special feeling about being picked last for sports. Like no one wants you on their team or something."
  "Odd because I'm very experienced at keeping the bench warm."
  return
