init python:
  class Interactable_school_gym_ladder(Interactable):

    def title(cls):
      return "Ladder"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_haggis.started:
        return "Reaching the light is but a short ladder away. At least, if we're talking about changing light bulbs."
      elif quest.mrsl_HOT.started:
        return "With so many lightbulbs exposed to balls and other projectiles, it's probably wise to keep the ladder here."
      elif quest.lindsey_nurse.ended:
        return "They call it the social ladder. Jocks and cheerleaders at the top, normies in the middle, me at the bottom."
      else:
        return "The only way I'll ever dunk is with the help of a ladder."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis == "escape":
            actions.append(["take","Take","school_gym_ladder_isabelle_haggis"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.mrsl_HOT.started:
        actions.append("?school_gym_ladder_interact_mrsl_hot")
      if quest.lindsey_nurse.ended:
        actions.append("?school_gym_ladder_lindsey_nurse")
      actions.append("?school_gym_ladder_interact")


label school_gym_ladder_lindsey_nurse:
  "You could see the world from up there... but what's the point of that?"
  return

label school_gym_ladder_interact:
  "Stray balls, man's worst enemy."
  "As soon as you sit down, there's a risk of instant pain."
  "Why make myself a more visible target, though?"
  return

label school_gym_ladder_isabelle_haggis:
  "With the risk of the janitor killing me... well, she isn't using it right now."
  "This ladder is perfect for climbing through the homeroom window."
  #Hide Ladder
  $school_gym["ladder_taken"] = True
  "Now to make the sweat drip out of every pore..."
  show black with fadehold
  call goto_school_entrance
  "Oh god, I feel like I've stood before the lord."
  "Bleeding, bleeding, bleeding. Life is suffering."
  $school_entrance["window_shortcut"] = True
  "At least, it's done now."
  return

label school_gym_ladder_interact_mrsl_hot:
  "There'll be a folding ladder reaching down when the janitor comes around."
  return
