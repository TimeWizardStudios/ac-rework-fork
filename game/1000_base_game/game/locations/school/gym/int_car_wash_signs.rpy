init python:
  class Interactable_school_gym_car_wash_signs(Interactable):

    def title(cls):
      return "Car Wash Signs"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
      if quest.jo_washed.in_progress:
        if not school_gym["car_wash_sign_taken"]:
          actions.append(["take","Take","quest_jo_washed_supplies_car_wash_signs"])
        actions.append(random.choice(quest_jo_washed_interact_lines))
