init python:
  class Interactable_school_gym_door_left(Interactable):

    def title(cls):
      return "Left Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "I have no interest in mysterious doors."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Left Door","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["go","Left Door","?school_gym_door_left_interact"])


label school_gym_door_left_interact:
  "Why can't opening the door fulfill my exercise quota?"
  return
