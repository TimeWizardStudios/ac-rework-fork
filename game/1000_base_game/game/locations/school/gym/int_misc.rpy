init python:
  class Interactable_school_gym_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_gym_dollar1_take"])


label school_gym_dollar1_take:
  $school_gym["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_gym_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_gym_dollar2_take"])


label school_gym_dollar2_take:
  $school_gym["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_gym_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_gym_book_interact"])


label school_gym_book_interact:
  $school_gym["book_taken"] = True
  "Holy shit, it's Chad's diary! I had no idea he could write."
  "All the ancient secrets and techniques to become the perfect male shall finally be mine!"
  "Fuck, it's heavy..."
  "And... all the pages are blank..."
  "Oh well, at least I got my daily workout."
  $mc.strength+=1
  return
