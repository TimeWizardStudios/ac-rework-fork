init python:
  class Interactable_school_gym_hula_hoop(Interactable):

    def title(cls):
      return "Hula Hoop"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.mrsl_HOT.started:
        return "Someone should take a hula hoop to space and beat the world record. Hmm... need to become an astronaut and patent this idea quickly."
      elif quest.lindsey_nurse.ended:
        return "Hips do lie. Hula hoops are the proof of that."
      else:
        return "Girls have an unfair advantage rocking these. Girls have an unfair advantage, period. Except during their period."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.mrsl_HOT.started:
        actions.append("?school_gym_hula_hoop_interact_mrsl_hot")
      if quest.lindsey_nurse.ended:
        actions.append("?school_gym_hula_hoop_lindsey_nurse")
      actions.append("?school_gym_hula_hoop_interact")


label school_gym_hula_hoop_lindsey_nurse:
  "Just move your hips like this. Side to side. Back and forth."
  "Shit. Probably shouldn't practice without the hula hoop. People might get the wrong idea."
  return

label school_gym_hula_hoop_interact:
  "Hmm... always wondered who the best hula hooper is. Time to put it to the test!"
  mc "Anyone wanna give it a try? I'm trying to find the best hula hooper in the school!"
  "..."
  "Okay, maybe another time."
  return

label school_gym_hula_hoop_interact_mrsl_hot:
  "The best girl at hula hooping is probably..."
  menu(side="middle"):
    extend ""
    "[lindsey]":
      if not school_gym["best_hula_chosen"]:
        $school_gym["best_hula_chosen"] = True
        $lindsey.love+=1
      extend " [lindsey]."
      "After all, she is the star athlete."
    "[isabelle]":
      if not school_gym["best_hula_chosen"]:
        $school_gym["best_hula_chosen"] = True
        $isabelle.love+=1
      extend " [isabelle]."
      "She's probably done in-depth reading about hula hooping and knows all the techniques."
    "[flora]":
      if not school_gym["best_hula_chosen"]:
        $school_gym["best_hula_chosen"] = True
        $flora.love+=1
      extend " [flora]."
      "She's insufferably good at everything she does."
    "[kate]":
      if not school_gym["best_hula_chosen"]:
        $school_gym["best_hula_chosen"] = True
        $kate.lust+=1
      extend " [kate]."
      "Cheer captain: Check."
      "Natural hip-sway: Check."
      "Would kill the competition: Check."
      "No one would even dare to challenge her."
  return
