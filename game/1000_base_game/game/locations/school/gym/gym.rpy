init python:
  class Location_school_gym(Location):

    default_title="Gym"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(-275,0),"school gym gym"])
      scene.append([(1057,271),"school gym door_right","school_gym_exit"])
      scene.append([(1304,181),"school gym banner"])
      scene.append([(0,349),"school gym bleachers","school_gym_bleachers"])

      if quest.mrsl_bot == "dream":
        scene.append([(47,763),"school gym puddles"])

      if quest.jacklyn_romance == "hide_and_seek" and not school_gym["ball_of_yarn_taken"]:
        scene.append([(738,681),"school gym ball_of_yarn","school_gym_ball_of_yarn"])

      if (school_gym["lights_off"]
      or (quest.mrsl_bot == "meeting_2_electric_boogaloo" and game.hour == 19)):
        scene.append([(0,204),"school gym window_left_night","school_gym_window"])
        scene.append([(745,289),"school gym window_right_night","school_gym_window"])
      else:
        scene.append([(0,204),"school gym window_left","school_gym_window"])
        scene.append([(745,289),"school gym window_right","school_gym_window"])
      scene.append([(388,247),"school gym score_board","school_gym_score_board"])
      if quest.maya_witch >= "gym_class":
        scene.append([(432,290),"school gym score_board_text",("school_gym_score_board",-8,-43)])
      scene.append([(0,259),"school gym fence"])
      scene.append([(156,477),"school gym door_left","school_gym_door_left"])
      if not quest.jo_washed.in_progress:
        scene.append([(237,531),"school gym signup","school_gym_signup_list"])
      scene.append([(93,591),"school gym bin",("school_gym_trash_bin",30,0)])
      scene.append([(0,515),"school gym vending_machine",("school_gym_vending_machine",120,0)])

      #Ladder
      if not school_gym["ladder_taken"] or quest.jo_washed.in_progress:
        scene.append([(1472,364),"school gym ladder","school_gym_ladder"])

      scene.append([(1617,0),"school gym backboard_top"])
      scene.append([(1739,75),"school gym backboard_bottom",("school_gym_backboard",-30,230)])
      scene.append([(106,0),"school gym ring_left"])
      scene.append([(937,473),"school gym net","school_gym_volley_net"])
      scene.append([(17,875),"school gym hoops","school_gym_hula_hoop"])
      if not quest.jo_washed.in_progress:
        if school_gym["ball_7_shown"] == "six_pointer":
          scene.append([(1789,672),"school gym ball_7","school_gym_balls"])
        elif school_gym['ball_7_shown']:
          scene.append([(1637,629),"school gym ball_7","school_gym_balls"])
      #scene.append([(1824,707),"school gym ball_6","school_gym_balls"])
      scene.append([(1250,704),"school gym ball_5","school_gym_balls"])
      #scene.append([(1641,1042),"school gym ball_4","school_gym_balls"])
      if not school_gym["ball_3_hidden"] or quest.jo_washed.in_progress:
        scene.append([(1073,925),"school gym ball_3","school_gym_balls"])
      scene.append([(175,700),"school gym ball_2","school_gym_balls"])
      scene.append([(67,700),"school gym ball_1","school_gym_balls"])

      #Nurse
      if nurse.at("school_gym","standing"):
        if not nurse.talking:
          if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "nurse":
            scene.append([(1506,463),"school gym nurse","nurse"])

      scene.append([(1445,768),"school gym table"])

      #Pool
      if quest.poolside_story == "tryout":
        scene.append([(431,676),"school gym pool","school_gym_pool"])
        scene.append([(455,707),"school gym water",("school_gym_pool",-9,-31)])

      scene.append([(1593,814),"school gym shorts","school_gym_short"])
      scene.append([(1725,813),"school gym paddles",("school_gym_ping_pong_paddle",-60,0)])

      #Light
      if school_gym["lights_off"]:
        scene.append([(158,33),"#school gym lights_off"])
      if school_gym["light_smashed"] and not quest.jo_washed.in_progress:
        scene.append([(843,55),"school gym broken_light",("school_gym_light",0,150)])
      else:
        scene.append([(843,57),"school gym light",("school_gym_light",0,150)])

      #Car Wash Signs
      if quest.jo_washed["kate_location"]:
        if school_gym["car_wash_sign_taken"]:
          scene.append([(717,814),"school gym car_wash_signs2","school_gym_car_wash_signs"])
        else:
          scene.append([(524,814),"school gym car_wash_signs1",("school_gym_car_wash_signs",96,0)])

      #Kate
      if not kate.talking:
        if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "kate":
          if kate.at("school_gym","standing"):
            if ((kate.equipped_item("kate_cheerleader_top") and kate.equipped_item("kate_cheerleader_skirt"))
            or quest.maya_witch == "gym_class"):
              scene.append([(1263,467),"school gym kate_cheerleader","kate"])
            elif game.season == 1:
              scene.append([(1263,467),"school gym kate","kate"])
            elif game.season == 2:
              scene.append([(1263,467),"school gym kate_autumn","kate"])
          elif kate.at("school_gym","car_wash_signs"):
            scene.append([(536,488),"school gym kate_car_wash_signs","kate"])

      #Isabelle
      if isabelle.at("school_gym","standing"):
        if not isabelle.talking:
          if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "isabelle":
            if game.season == 1:
              scene.append([(253,505),"school gym isabelle","isabelle"])
            elif game.season == 2:
              scene.append([(253,505),"school gym isabelle_autumn","isabelle"])
              if isabelle.equipped_item("isabelle_collar"):
                scene.append([(269,540),"school gym isabelle_collar",("isabelle",19,-35)])

      #Lindsey
      if lindsey.at("school_gym","running"):
        if not lindsey.talking:
          if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "lindsey":
            scene.append([(737,497),"school gym lindsey","lindsey"])

      #MrsL
      if mrsl.at("school_gym","sitting"):
        if not mrsl.talking:
          if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "mrsl":
            if game.season == 1:
              scene.append([(522,474),"school gym mrsl","mrsl"])
            elif game.season == 2:
              scene.append([(522,474),"school gym mrsl_autumn","mrsl"])

      #Maxine
      if maxine.at("school_gym","standing"):
        if not maxine.talking:
          if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "maxine":
            if game.season == 1:
              scene.append([(467,514),"school gym maxine","maxine"])
            elif game.season == 2:
              scene.append([(467,514),"school gym maxine_autumn","maxine"])

      #Maya
      if maya.at("school_gym","sitting"):
        if not maya.talking:
          if school_gym["exclusive"] is 0 or school_gym["exclusive"] == "maya":
            scene.append([(861,350),"school gym maya_autumn","maya"])

      if not (quest.kate_blowjob_dream == "school" or quest.jo_washed.in_progress):
        if school_gym["dollar1_spawned_today"] == True and not school_gym["dollar1_taken_today"]:
          scene.append([(132,686),"school gym dollar1","school_gym_dollar1"])
        if school_gym["dollar2_spawned_today"] == True and not school_gym["dollar2_taken_today"]:
          scene.append([(1859,822),"school gym dollar2","school_gym_dollar2"])
        if not school_gym["book_taken"]:
          scene.append([(1824,741),"school gym book","school_gym_book"])

      if school_gym["magnet"]:
        scene.append([(171,767),"school gym magnet","school_gym_magnet"])

      scene.append([(0,0),"#school gym overlay"])
      if school_gym["lights_off"]:
        scene.append([(0,0),"#school gym overlay_night"])

    def find_path(self,target_location):
      return "school_gym_exit"


label goto_school_gym:
  if school_gym.first_visit_today:
    $school_gym["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_gym["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_gym)
  return
