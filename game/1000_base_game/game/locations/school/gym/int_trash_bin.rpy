init python:
  class Interactable_school_gym_trash_bin(Interactable):

    def title(cls):
      return "Trash Bin"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.mrsl_HOT.started:
        return "It's all just stinking useless garbage. Oh, and then there's also this trash can and its content."
      elif quest.lindsey_nurse.ended:
        return "The filth of society, gathered in one place. It's almost like my bedroom."
      else:
        return "Drawn to the trash. Drawn to the filth. Not even the longest of showers can ever truly wash away the dirt."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_gym["stick_interacted"]:
          actions.append(["take","Take","school_gym_trash_bin_interact_lindsey_book"])
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_gym_bin")
      if quest.lindsey_nurse.ended:
        actions.append("?school_gym_trash_bin_lindsey_nurse")
      actions.append("?school_gym_trash_bin_interact")


label school_gym_trash_bin_lindsey_nurse:
  "Paper towels, an old shoe, a half-eaten sandwich... wait, is that gold?"
  "Ah, no. Just a magnum condom wrapper. Of course, the jocks are hung as fuck."
  return

label school_gym_trash_bin_interact:
  "Old pieces of gum, candy wrappers, soda cans, and some kind of goo that looks like an alien brain."
  return

label school_gym_trash_bin_interact_lindsey_book:
  "Here we go again. Digging through the trash. Oh, look, an elongated wooden object!"
  $mc.add_item("stick")
  $school_gym["stick_interacted"] = True
  return

label isabelle_quest_stolen_maxine_paper_gym_bin:
  "Oh, a paper!"
  "..."
  "Wait, it's drowned yoghurt. [maxine] hates yoghurt."
  return
