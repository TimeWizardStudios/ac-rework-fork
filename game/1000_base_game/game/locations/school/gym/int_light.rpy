init python:
  class Interactable_school_gym_light(Interactable):

    def title(cls):
      if school_gym["light_smashed"] and not quest.jo_washed.in_progress:
        return "Broken Light"
      else:
        return "Light"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif school_gym["light_smashed"]:
        if quest.mrsl_HOT.started:
          return "Not exactly the brightest of the bunch, but still a part of the bunch. That's what matters."
        elif quest.isabelle_tour.finished:
          return "This is where the fear starts to set in. One shattered light. Darkness skulking closer. It's only a matter of time now."
        elif quest.lindsey_nurse.started:
          return "To this day, the crunch of the glass still rings true in my ears. A moment of true glory."
        else:
          return "Darkness and asymmetry. My trail of destruction."
      else:
        if quest.mrsl_HOT.started:
          return "They all look the exact same. It's like they were made in a factory. No individuality whatsoever."
        elif quest.lindsey_book.started:
          return "A light like any other, without any sort of significance."
        elif quest.isabelle_tour.finished:
          return "9001 watts of blinding technology. The reason my shots always miss the hoops."
        else:
          return "What did the lightbulb say to the switch?\n\nBaby, you turn me on!"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if mc.owned_item(("rock","monkey_wrench","soup_can","baseball","baseball_bat","umbrella")) and not school_gym["light_smashed"]:
          actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:act_one,gym_light|Use What?","school_gym_light_use"])
      if school_gym["light_smashed"]:
        if quest.jacklyn_broken_fuse.started:
          actions.append("?school_gym_light_interact_jacklyn_broken_fuse_smashed")
        if quest.lindsey_book.started:
          actions.append("?school_gym_light_interact_lindsey_book_smashed")
        if quest.isabelle_tour.finished:
          actions.append("?school_gym_light_interact_isabelle_tour_smashed")
        if quest.lindsey_nurse.started:
          actions.append("?school_gym_light_interact_lindsey_nurse_smashed")
        actions.append("?school_gym_light_interact")
      else:
        if quest.mrsl_HOT.started:
          actions.append("?school_gym_light_interact_mrsl_HOT")
        if quest.lindsey_book.started:
          actions.append("?school_gym_light_interact_lindsey_book")
        if quest.isabelle_tour.finished:
          actions.append("?school_gym_light_interact_isabelle_tour")
        actions.append("?school_gym_light_interact")


label school_gym_light_use(item):
  if item in ("rock","monkey_wrench","soup_can","baseball","baseball_bat","umbrella"):
    play sound "glass_break"
    $school_gym["light_smashed"] = True
    $mc.remove_item(item)
    "Always wanted to do that. Those lamps are just begging to be smashed. Vandalism at its finest!"
    $mc.add_item("glass_shard")
    $mc.add_item(item)
  else:
    "Tossing my [item.title_lower] at the lamp is fun, but ultimately useless."
    "Nonetheless..."
    "..."
    "Miss."
    $quest.act_one.failed_item("gym_light", item)
  return

label school_gym_light_interact:
  "It's just a lamp. Nothing to worry about. It's not like it could fall down at any moment and hit you on the head."
  "It's not like you'll pass out from the impact and pee yourself for all the school to see. It's just a lamp."
  return

label school_gym_light_interact_jacklyn_broken_fuse_smashed:
  "Extinguished. Like the hopes and dreams of people with... well, extinguished hopes and dreams"
  return

label school_gym_light_interact_lindsey_nurse_smashed:
  "For better or worse, it's all about making a change."
  return

label school_gym_light_interact_lindsey_book_smashed:
  "A perfect example of how something broken can stand out from the rest of the flock."
  "A harsh reminder that there are two sides of extraordinary."
  return

label school_gym_light_interact_mrsl_HOT:
  "Like a beacon of hope in a forest of beacons of hope. Yeah, they come in great numbers here."
  return

label school_gym_light_interact_lindsey_book:
  "When Brad hit me in the face with a basketball, this was the light at the end of the tunnel."
  return

label school_gym_light_interact_isabelle_tour_smashed:
  "Who cares if one more light goes out on the ceiling of a hundred lamps?"
  "Only janitors and Linkin Park fans is my guess."
  return

label school_gym_light_interact_isabelle_tour:
  "Shine while you can, little light. Who knows how much time you have left before you burn out?"
  return
