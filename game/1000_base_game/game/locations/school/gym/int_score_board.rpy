init python:
  class Interactable_school_gym_score_board(Interactable):

    def title(cls):
      return "Score Board"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.maya_witch >= "gym_class":
        return "This can't be true, surely.\nJust a big — or small — attempt\nat bullying."
      else:
        return "This piece of technology discriminates against the losers of the world."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.maya_witch >= "gym_class":
        actions.append("?quest_maya_witch_gym_class_school_gym_score_board")
      else:
        actions.append("?school_gym_score_board_interact")


label school_gym_score_board_interact:
  "The score?"
  "Attractive people — all the points."
  "Me — zero."
  return
