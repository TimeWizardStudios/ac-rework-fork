init python:
  class Interactable_school_gym_short(Interactable):

    def title(cls):
      return "Gym Shorts"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The simpleton sniffs panties. The scholar huffs gym shorts."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_gym_short_interact")


label school_gym_short_interact:
  "I hope their owner wasn't an ugly girl."
  "That would really kill my vibe."
  return
