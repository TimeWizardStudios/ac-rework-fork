init python:
  class Interactable_school_gym_volley_net(Interactable):

    def title(cls):
      return "Volleyball Net"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Cute girls getting irrevocably tied up in the volleyball net — my favorite hentai plot point."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_gym_volley_net_interact")


label school_gym_volley_net_interact:
  "Nyet!"
  return
