init python:
  class Interactable_school_gym_ping_pong_paddle(Interactable):

    def title(cls):
      return "Ping Pong Paddle"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.mrsl_HOT.started:
        return "Speed, accuracy, coordination, finesse — things that my body seems to lack."
      elif quest.lindsey_nurse.ended:
        return "Ping pong or beer pong, two horrible alternatives."
      else:
        return "When I look at these paddles, the only thing I can think of is [mrsl]'s ass.\n\nWouldn't mind bending her over, and giving her a ping pong lesson."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.mrsl_HOT.started:
        actions.append("?school_gym_ping_pong_paddle_interact_mrsl_hot")
      if quest.lindsey_nurse.ended:
        actions.append("?school_gym_ping_pong_paddle_lindsey_nurse")
      actions.append("?school_gym_ping_pong_paddle_interact")


label school_gym_ping_pong_paddle_lindsey_nurse:
  "What do you call a girl lying in the middle of a ping pong table?"
  "Annette."
  return

label school_gym_ping_pong_paddle_interact:
  "There's only one proper use for these paddles..."
  "Hard and fast. "
  "Mercilessly bringing the pain."
  "Like, slap, slap, slap, slap!"
  "Hitting that ball over the net! Yeah, ping pong is serious business."
  return

label school_gym_ping_pong_paddle_interact_mrsl_hot:
  "Slap a ball across the net or a girl across her ass; one sport is more interesting than the other."
  return
