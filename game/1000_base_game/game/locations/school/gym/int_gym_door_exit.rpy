init python:
  class Interactable_school_gym_exit(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return "Too late to go back now."
      elif quest.mrsl_HOT.started:
        return "In case of emergency, cover your face from stray balls and run this way."
      else:
        return "The coast is clear."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","East Hall","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "mrsl@poolside_story":
          if quest.poolside_story == "tryout":
            actions.append(["go","East Hall","?school_gym_door_poolside_story_tryout"])
            return
        elif mc["focus"] == "kate_trick":
          if quest.kate_trick == "workout":
            actions.append(["go","East Hall","?quest_kate_trick_workout_exit"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Leave School","quest_isabelle_dethroning_panik_outside"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","East Hall","goto_school_first_hall_east"])
      actions.append(["go","East Hall","goto_school_first_hall_east"])


label school_gym_door_poolside_story_tryout:
  "After all the work that went into getting this tryout, bailing isn't an option."
  return
