init python:
  class Interactable_school_gym_ball_of_yarn(Interactable):

    def title(cls):
      return "Blue Ball of Yarn"

    def description(cls):
      return "If all balls were this soft,\nsports would be more fun."

    def actions(cls,actions):
      actions.append(["take","Take","school_gym_ball_of_yarn_take"])
      actions.append("?school_gym_ball_of_yarn_interact")


label school_gym_ball_of_yarn_interact:
  "Once this is all done, maybe [maya] will have some spare yarn and could knit the cheerleaders some skimpier uniforms?"
  return

label school_gym_ball_of_yarn_take:
  if ("school_gym_ball_of_yarn_interact",()) not in game.seen_actions:
    call school_gym_ball_of_yarn_interact
    window hide
  $quest.jacklyn_romance["balls_of_yarn_found"]+=1
  $school_gym["ball_of_yarn_taken"] = True
  $mc.add_item("ball_of_yarn_blue",silent=True)
  $game.notify_modal(None,"Inventory","{image=items ball_of_yarn_blue}{space=50}|Gained\n{color=#900}Blue Ball of Yarn{/color}\n("+str(quest.jacklyn_romance["balls_of_yarn_found"])+"/5)",5.0)
  return
