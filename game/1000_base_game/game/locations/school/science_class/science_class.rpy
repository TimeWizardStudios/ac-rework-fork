init python:
  class Location_school_science_class(Location):

    default_title = "Science Class"

    def scene(self,scene):
      scene.append([(0,-42),"school science_class background"])
      scene.append([(6,442),"school science_class posters"])
      scene.append([(1407,474),"school science_class max_planck","school_science_class_poster_of_max_planck"])
      if (quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")
      or (quest.maya_spell in ("midnight","busted","statue") and game.hour == 20)
      or quest.maya_spell == "spell"):
        scene.append([(1553,178),"school science_class windows_night"])
      scene.append([(1518,611),"school science_class aquarium","school_science_class_aquarium"])

      ## Left wall ##
      scene.append([(231,368),"school science_class closet",("school_science_class_supply_cabinet",40,0)])
      scene.append([(0,595),"school science_class sink"])
      scene.append([(0,323),"school science_class cabinet"])
      scene.append([(113,340),"school science_class bookcase","school_science_class_bookcase"])
      scene.append([(626,435),"school science_class door","school_science_class_door"])
      if ((quest.maya_spell not in ("midnight","busted","paint","statue","spell") and not school_science_class["lab_coat_taken"])
      or (quest.maya_spell in ("midnight","busted","paint","statue","spell") and not (mc.owned_item("lab_coat") or quest.maya_spell["lab_coat_taken"]))):
        scene.append([(571,502),"school science_class lab_coat","school_science_class_lab_coat"])

      ## Far wall ##
      scene.append([(858,483),"school science_class skeleton"])
      if cyberia.talking:
        scene.append([(904,375),"school science_class speakers_active"])
      else:
        scene.append([(929,388),"school science_class speakers",("school_science_class_speakers",-7,66)])
      scene.append([(938,454),"school science_class blackboard","school_science_class_blackboard"])
      scene.append([(1025,411),"school science_class roll_up"])
      scene.append([(1288,548),"school science_class tv"])

      ## Middle stuff ##
      if school_science_class["additional_hardware"]:
        scene.append([(847,563),"school science_class teacher_desk_improved"])
        scene.append([(1105,629),"school science_class calculator","school_science_class_calculator"])
      else:
        scene.append([(847,565),"school science_class teacher_desk"])
        scene.append([(1067,629),"school science_class calculator","school_science_class_calculator"])
      scene.append([(1213,597),"school science_class abacus","school_science_class_abacus"])
      scene.append([(1166,621),"school science_class papers"])
      scene.append([(103,668),"school science_class desks"])
      scene.append([(357,713),"school science_class left_terrarium","school_science_class_left_terrarium"])
      scene.append([(1039,676),"school science_class right_terrarium","school_science_class_right_terrarium"])
      scene.append([(866,769),"school science_class solar_system","school_science_class_solar_system_model"])

      ## Maxine ##
      if ((not school_science_class["exclusive"] or "maxine" in school_science_class["exclusive"])
      and maxine.at("school_science_class","concocting")
      and not maxine.talking):
        scene.append([(539,488),"school science_class maxine","maxine"])

      ## Overlay ##
      scene.append([(1,889),"#school science_class overlay"])
      if (quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")
      or (quest.maya_spell in ("midnight","busted","statue") and game.hour == 20)
      or quest.maya_spell == "spell"):
        scene.append([(0,0),"#school science_class overlay_night"])

    def find_path(self,target_location):
      return "school_science_class_door", dict(marker_offset=(0,0))


label goto_school_science_class:
  call goto_with_black(school_science_class)
  return
