init python:
  class Interactable_school_science_class_solar_system_model(Interactable):

    def title(cls):
      return "Solar System Model"

    def description(cls):
      return "In the grand scheme of the universe, my life sucked astronomically. Now, slight improvement."

    def actions(cls,actions):
      actions.append("?school_science_class_solar_system_model_interact")


label school_science_class_solar_system_model_interact:
  "Which one is Venus?"
  "It's supposed to be right next to Mercury..."
  "Why is Venus always so hard to find?"
  return
