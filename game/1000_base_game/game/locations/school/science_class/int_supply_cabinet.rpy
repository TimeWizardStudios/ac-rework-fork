init python:
  class Interactable_school_science_class_supply_cabinet(Interactable):

    def title(cls):
      return "Supply Cabinet"

    def description(cls):
      return "This cabinet is filled with all sorts of cool stuff, but it's school policy to keep it under lock and key."

    def actions(cls,actions):
      actions.append("?school_science_class_supply_cabinet_interact")


label school_science_class_supply_cabinet_interact:
  "Burettes, pipettes, box cutters, flasks, and chemicals. Need some chemistry?"
  "Yes... sorely."
  return
