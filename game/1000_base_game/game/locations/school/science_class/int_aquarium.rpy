init python:
  class Interactable_school_science_class_aquarium(Interactable):

    def title(cls):
      return "Aquarium"

    def description(cls):
      return "Frogs and fish living in perfect harmony. If only the world was more like an aquarium."

    def actions(cls,actions):
      actions.append("?school_science_class_aquarium_interact")


label school_science_class_aquarium_interact:
  "[lindsey] once dropped her energy bar into the aquarium."
  "Nothing has been the same in the underwater kingdom since that taste of freedom."
  return
