init python:
  class Interactable_school_science_class_blackboard(Interactable):

    def title(cls):
      return "Blackboard"

    def description(cls):
      return "A helpful and enlightening message from our dear science teacher."

    def actions(cls,actions):
      actions.append("?school_science_class_blackboard_interact")


label school_science_class_blackboard_interact:
  "{i}\"Follow the instructions in the textbook. —Mr. Clarence\"{/}"
  return
