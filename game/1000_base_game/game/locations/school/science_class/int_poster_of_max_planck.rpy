init python:
  class Interactable_school_science_class_poster_of_max_planck(Interactable):

    def title(cls):
      return "Poster of Max Planck"

    def description(cls):
      return "Mr. Clarence says he was destined to go into theoretical physics, but due to budget cuts and bad luck he got stuck teaching high school science."

    def actions(cls,actions):
      actions.append("?school_science_class_poster_of_max_planck_interact")


label school_science_class_poster_of_max_planck_interact:
  "With a head like that, no wonder he was smart."
  "With a mustache like that, no wonder he got with the ladies."
  "At least, that's what Mr. Clarence says..."
  "Also, that they were drinking buddies. Not sure either is true."
  return
