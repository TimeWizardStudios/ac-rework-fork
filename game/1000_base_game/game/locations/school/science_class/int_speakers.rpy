init python:
  class Interactable_school_science_class_speakers(Interactable):

    def title(cls):
      return "Speakers"

    def description(cls):
      return "Technology of a bygone era. No one knows how, but it just works."

    def actions(cls,actions):
      actions.append("?school_science_class_speakers_interact")


label school_science_class_speakers_interact:
  "These speakers are part of the school's public address system."
  return
