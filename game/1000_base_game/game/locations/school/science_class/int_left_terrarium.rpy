init python:
  class Interactable_school_science_class_left_terrarium(Interactable):

    def title(cls):
      return "Terrarium"

    def description(cls):
      return "Empty and dead, just like\nmy life and heart."

    def actions(cls,actions):
      actions.append("?school_science_class_left_terrarium_interact")


label school_science_class_left_terrarium_interact:
  "This terrarium is divided into four little glass cubicles, each with its own landscape."
  "But it has been empty for a while."
  return
