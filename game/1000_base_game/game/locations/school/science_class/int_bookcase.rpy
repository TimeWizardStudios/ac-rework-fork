init python:
  class Interactable_school_science_class_bookcase(Interactable):

    def title(cls):
      return "Bookcase"

    def description(cls):
      return "Beats the English literature, but that's where the positives end."

    def actions(cls,actions):
      actions.append("?school_science_class_bookcase_interact")


label school_science_class_bookcase_interact:
  "Science books have the most boring titles..."
  "Why would anyone want to read about {i}\"Cone-Shaped Asteroids and How They Affect Uranus?\"{/}"
  return
