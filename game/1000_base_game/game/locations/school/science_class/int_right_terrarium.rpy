init python:
  class Interactable_school_science_class_right_terrarium(Interactable):

    def title(cls):
      return "Terrarium"

    def description(cls):
      return "Jabba the slug —\ntruly one of his kind."

    def actions(cls,actions):
      actions.append("?school_science_class_right_terrarium_interact")


label school_science_class_right_terrarium_interact:
  "The only resident in this classroom that isn't a fish."
  "This overgrown slug."
  return
