init python:
  class Interactable_school_science_class_abacus(Interactable):

    def title(cls):
      return "Abacus"

    def description(cls):
      return "My girlfriend is the square root\nof -100.\n\nA perfect 10, but entirely imaginary.{space=-10}"

    def actions(cls,actions):
      actions.append("?school_science_class_abacus_interact")


label school_science_class_abacus_interact:
  "Sometimes, the calculator runs out of batteries. Then, Mr. Clarence uses this antiquity."
  return
