init python:
  class Interactable_school_science_class_lab_coat(Interactable):

    def title(cls):
      return "Lab Coat"

    def description(cls):
      return "Stainless and probably unused. Not many experiments\nhappening here."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "lab_coat":
            actions.append(["take","Take","school_science_class_lab_coat_take"])
            actions.append("?school_science_class_lab_coat_interact_alt")
            return
      else:
        actions.append("?school_science_class_lab_coat_interact")


label school_science_class_lab_coat_interact:
  "No need to lab coat it. I can handle the truth."
  return

label school_science_class_lab_coat_interact_alt:
  "The perfect disguise for getting into the hospital."
  "Just another sexy doctor. Like on Greg's Anatomy."
  return

label school_science_class_lab_coat_take:
  if ("school_science_class_lab_coat_interact_alt",()) not in game.seen_actions:
    call school_science_class_lab_coat_interact_alt
    window hide
  $school_science_class["lab_coat_taken"] = True
  $mc.add_item("lab_coat")
  pause 0.25
  window auto
  "Perfect! Okay, time to bail."
  $quest.lindsey_angel.advance("escape")
  return

