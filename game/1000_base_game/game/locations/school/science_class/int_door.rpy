init python:
  class Interactable_school_science_class_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      return "Tanner once broke his ankle trying to kick this door open.\n\nGood door."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "lab_coat" and not school_science_class["lab_coat_taken"]:
            actions.append(["go","Entrance Hall","?quest_lindsey_angel_lab_coat_school_science_class_door"])
            return
      actions.append(["go","Entrance Hall","goto_school_ground_floor"])
