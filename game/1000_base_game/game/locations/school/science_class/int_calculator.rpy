init python:
  class Interactable_school_science_class_calculator(Interactable):

    def title(cls):
      return "Mr. Clarence's Calculator"

    def description(cls):
      return "Back in the 70s, this calculator was cutting edge. Now, it's duller than a roman blade... numeral... ugh, whatever."

    def actions(cls,actions):
      actions.append("?school_science_class_calculator_interact")


label school_science_class_calculator_interact:
  "Mr. Clarence's prized calculator."
  "It's the only machine — or human — that he trusts."
  return
