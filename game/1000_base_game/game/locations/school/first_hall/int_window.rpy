init python:
  class Interactable_school_first_hall_window(Interactable):

    def title(cls):
      return "Window"

    def description(cls):
      if quest.lindsey_motive in ("distraction","keys"):
        return "Ah, the great outdoors! Birds, bees, and butterflies!"
      elif quest.lindsey_motive in ("window_open","lure"):
        return "Some fresh air has never\nhurt anyone.\n\nExcept that one guy who was about to snort some lines.\n\nPoor coke addicts."

    def actions(cls,actions):
      if quest.lindsey_motive in ("distraction","keys"):
        actions.append("?school_first_hall_window_interact")
      elif quest.lindsey_motive in ("window_open","lure"):
        if quest.lindsey_motive == "lure":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_motive,butterfly_attractor|Use What?","quest_lindsey_motive_lure_window"])
        actions.append("?quest_lindsey_motive_window_open")


label school_first_hall_window_interact:
  call quest_lindsey_motive_distraction_window
  return
