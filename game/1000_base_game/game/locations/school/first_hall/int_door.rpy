init python:
  class Interactable_school_first_hall_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Wouldn't surprise me if this door was installed just to annoy the curious. Probably a concrete wall behind it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Door","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Door","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik"):
            actions.append(["go","Door","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Door","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.isabelle_hurricane == "short_circuit" and (quest.isabelle_hurricane["distraction"] or quest.isabelle_hurricane["backpack"]) and not quest.isabelle_hurricane["scratching"]:
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_hurricane,distraction|Use What?","quest_isabelle_hurricane_short_circuit_door"])
      actions.append(["go","Door","?school_first_hall_door_interact"])


label school_first_hall_door_interact:
  "No reason to enter any door unless court-martialed to."
  return
