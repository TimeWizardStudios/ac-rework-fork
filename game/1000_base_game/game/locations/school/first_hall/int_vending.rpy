init python:
  class Interactable_school_first_hall_vending(Interactable):

    def title(cls):
      return "Vending Machine"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "When are they going to introduce vending machines for sex? That should be a human right."
      elif quest.kate_search_for_nurse.started:
        return "They taunt you with a glass window full of goodies, then extort you for money. Just like girls and capitalism."
      else:
        return "This is a machine that allows you to exchange money for food — a food whore."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "trap" and not school_first_hall["red_paint"]:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_dethroning,vending_machine|Use What?","quest_isabelle_dethroning_trap_vending_machine"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if mc.owned_item("full_letter"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:act_one,vending_machine_letter|Use What?","vending_machine_letter_use"])
        if quest.kate_search_for_nurse.in_progress:
          actions.append("?school_first_hall_vending_interact_kate_search_for_nurse")
      actions.append("school_first_hall_vending_interact")


label school_first_hall_vending_interact:
  "This vending machine hasn't been stocked yet. It currently only offers lollipops and water bottles."
  menu(side="middle"):
    "?mc.money>=1@[mc.money]/1|{image=ui hud icon_money}|{image=items lollipop_1 }|Lollipop":
      $mc.money-=1
      $mc.add_item("lollipop")
      "Nothing in life is free."
    "?mc.money>=50@[mc.money]/50|{image=ui hud icon_money}|{image=items lollipop_2 }|Big Lollipop":
      $mc.money-=50
      $mc.add_item("lollipop_2")
      "Who would spend their life savings on a lollipop?"
    "?mc.money>=100@[mc.money]/100|{image=ui hud icon_money}|{image=items bottle water_bottle }|Bottle of Water":
      $mc.money-=100
      $mc.add_item("water_bottle")
      "Back in the day, my granny bought a twelve acre farm and a three story villa for this price."
      "After the Great War, water bottles became a luxury item."
      "Affording one is not only a staple of wealth, it is a social badge of refinement."
    "Nothing for now":
      return
  return

label school_first_hall_vending_interact_kate_search_for_nurse:
  "Someone bought up half the lollipop stock."
  "The wrappers are everywhere..."
  "Everyone knows the [nurse] is a hopeless stress eater."
  return
