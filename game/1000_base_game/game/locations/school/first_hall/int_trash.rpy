init python:
  class Interactable_school_first_hall_bin(Interactable):

    def title(cls):
      return "Trash Bin"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_tour.finished:
        return "It's trash can, not trash can't. That's always a comforting thought."
      else:
        return "Who put a mirror here?"

    def actions(cls,actions):
      if not school_first_hall["trash_bin_interact"]:
        actions.append(["take","Take","?school_first_hall_bin_interact_first"])
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            if not school_first_hall["trash_bin_interact"]:
              actions.remove(["take","Take","?school_first_hall_bin_interact_first"])
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            if not school_first_hall["trash_bin_interact"]:
              actions.remove(["take","Take","?school_first_hall_bin_interact_first"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            if not school_first_hall["trash_bin_interact"]:
              actions.remove(["take","Take","?school_first_hall_bin_interact_first"])
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_first_hall_bin")
        if quest.isabelle_buried == "boxhunt":
          actions.append("?isabelle_quest_buried_boxhunt_first_hall_bin")
      actions.append("?school_first_hall_bin_interact")


label school_first_hall_bin_interact:
  "One man's trash, another man's treasure. "
  "Not sure if that's true for this banana peel, though."
  return

label school_first_hall_bin_interact_first:
  "Looking through the trash is always really popular. It makes you seem both hygienic and sound of mind."
  "Prejudice by people who don't have this..."
  window hide
  $mc.add_item("rock")
  pause 0.25
  window auto
  "...rock. "
  "Damn it! I had hoped for at least a piece of gum or a broken hairpin."
  $school_first_hall["trash_bin_interact"] = True
  return

label isabelle_quest_stolen_maxine_paper_first_hall_bin:
  "No paper here."
  return

label isabelle_quest_buried_boxhunt_first_hall_bin:
  "Any heart-shaped boxes in here? Or well, any box really."
  "..."
  "Nope, but there's other things here... which means the janitor didn't throw it out."
  "..."
  "Hmm... the surveillance camera might've caught whoever did it, but I doubt the [guard] will fall for the same trick twice."
  "Luckily, there's one person who always keeps the school under a microscope."
  "[maxine]."
  $quest.isabelle_buried.advance("buried_maxine")
  return
