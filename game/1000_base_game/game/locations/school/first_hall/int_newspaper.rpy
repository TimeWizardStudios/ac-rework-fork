init python:
  class Interactable_school_first_hall_newspaper(Interactable):

    def title(cls):
      return "Newspaper Stand"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "Some fools call [maxine]'s paper a source of misinformation and fake news... and they're probably right."
      elif quest.kate_search_for_nurse.started:
        return "The ever-so-popular school newspaper. Brad usually buys a column every issue and leaves it blank, just to brag about his money."
      else:
        return "No one really reads the school newspaper despite all the effort [maxine] puts in. Probably for the best."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_newspaper_interact")


label school_first_hall_newspaper_interact:
  if quest.isabelle_dethroning >= "newspaper":
    show misc newspaper5 with Dissolve(.5)
    if school_first_hall["newspaper_taken"]:
      pause
      hide misc newspaper5 with Dissolve(.5)
    else:
      "Wow, [maxine] really does work fast!"
      "Damn, that shit looks scary..."
      "..."
      "How do I get [kate] to read it, though?"
      window hide
      hide misc newspaper5 with Dissolve(.5)
      $school_first_hall["newspaper_taken"] = True
      $mc.add_item("newspaper")
      window auto
  elif quest.kate_stepping.finished:
    show misc newspaper4 with Dissolve(.5)
    pause
    hide misc newspaper4 with Dissolve(.5)
  elif quest.maxine_eggs.finished:
    if quest.maxine_eggs["lindsey_photo"]:
      show misc newspaper3 with Dissolve(.5)
      "Well, that's odd..."
      hide misc newspaper3 with Dissolve(.5)
    else:
      show misc newspaper2 with Dissolve(.5)
      pause
      hide misc newspaper2 with Dissolve(.5)
  else:
    show misc newspaper1 with Dissolve(.5)
    pause
    hide misc newspaper1 with Dissolve(.5)
  return
