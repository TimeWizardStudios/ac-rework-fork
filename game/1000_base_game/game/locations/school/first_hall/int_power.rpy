init python:
  class Interactable_school_first_hall_power(Interactable):

    def title(cls):
      return "Fuse Box"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.maxine_lines.started:
        return "The security camera could potentially see me fidgeting with the fuse box.\n\nHopefully, the guard is too busy stuffing his face with doughnuts to notice..."
      elif quest.isabelle_stolen.started:
        return "1.21 gigawatts? Is this thing powered with plutonium?"
      elif quest.jacklyn_broken_fuse == "investigate_fusebox":
        return "Is this a fuse box? It's hard to say. Do fuse boxes have cables? Hmm. Well, it has something to do with the wiring of the school."
      elif quest.kate_search_for_nurse.started:
        return "Fuses, wires, capacitors — everything you need to bring light. God was an electrician."
      else:
        return "All the school's electricity runs through this very box. Understandably, it's heavily guarded with a flimsy plastic cover."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.jacklyn_broken_fuse.in_progress and quest.jacklyn_broken_fuse < "reward":
          if quest.jacklyn_broken_fuse == "investigate_fusebox":
            actions.append("?school_first_hall_power_interact_jacklyn_broken_fuse")
          elif quest.jacklyn_broken_fuse in ("find_fork","reach","fix"):
            actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jacklyn_broken_fuse,fusebox|Use What?","school_first_hall_power_interact_jacklyn_broken_fuse_use"])
        if quest.maxine_lines > "reward" and quest.maxine_lines.in_progress:
          if quest.maxine_lines == "battery" and not school_first_hall["personal_charging_station"]:
            actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:maxine_lines,fusebox_cable|Use What?","school_first_hall_power_interact_maxine_lines_item"])
          elif quest.maxine_lines in ("battery","charge","magnetic","finalnode") and school_first_hall["personal_charging_station"]:
            actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:maxine_lines,fusebox_charge|Use What?","school_first_hall_power_interact_maxine_lines_item_charge"])
          actions.append("?school_first_hall_power_interact_maxine_lines_interact")
        if quest.isabelle_hurricane == "short_circuit":
          if quest.isabelle_hurricane["scratching"]:
            actions.append("?quest_isabelle_hurricane_short_circuit_fuse_box2")
          else:
            actions.append("?quest_isabelle_hurricane_short_circuit_fuse_box1")
        if quest.lindsey_motive == "power_cut":
          actions.append("quest_lindsey_motive_power_cut")
      if quest.kate_search_for_nurse.started:
        actions.append("?school_first_hall_power_interact_kate_search_for_nurse")
      if quest.isabelle_stolen.started:
        actions.append("?school_first_hall_power_interact_two")
      actions.append("?school_first_hall_power_interact")


label school_first_hall_power_interact_two:
  "My tongue in that hole... two fingers pumping that hole... my dick in this hole..."
  "I call this sexual position \"The Circuit Breaker.\""
  return

label school_first_hall_power_interact:
  "If you think that you're thinking outside the box, it just means that you're thinking inside another bigger box."
  return

label school_first_hall_power_interact_jacklyn_broken_fuse:
  "Last time around, the same thing happened with the light in the art classroom."
  "Instead of fixing the problem, the old art teacher gave everyone a worse grade."
  "Maybe there's a way to avoid it this time..."
  "Last time, a wire needed replacing. [jo] ranted for weeks about how such an easy fix ended up costing [flora] her A+ in art."
  "Hmm..."
  "There's definitely a frayed cable in there that looks broken."
  "The only problem is that it's behind a panel and I can't reach it without a tool of some sort."
  "Needs to be a plastic tool as well; I don't feel like getting electrocuted."
  $quest.jacklyn_broken_fuse.advance("find_fork")
  return

label school_first_hall_power_interact_jacklyn_broken_fuse_use(item):
  if item == "plastic_fork":
    jump school_first_hall_power_interact_jacklyn_broken_fuse_fork
  elif item == "specialized_fuse_cable_replacement_tool" and quest.jacklyn_broken_fuse<="reach":
    jump school_first_hall_power_interact_jacklyn_broken_fuse_special_tool
  elif item == "fixed_cable":
    jump school_first_hall_power_interact_jacklyn_broken_fuse_fixed_cable
  else:
    "At this point, it'd be better to close my eyes and start shoving fingers in there. Still better than my [item.title_lower]."
    $quest.jacklyn_broken_fuse.failed_item("fusebox", item)
  return

label school_first_hall_power_interact_jacklyn_broken_fuse_fork:
  "This fork could be the right tool for the job, but it's too short to reach inside. Need to find a way to make it longer."
# $quest.jacklyn_broken_fuse.advance("reach")
  return

label school_first_hall_power_interact_jacklyn_broken_fuse_special_tool:
  "..."
  "Come on, come on..."
  $mc.add_item("broken_cable")
  "Aha! Got the broken cable out!"
  "Now to find a way to repair it... "
  $quest.jacklyn_broken_fuse.advance("fix")
  return

label school_first_hall_power_interact_jacklyn_broken_fuse_fixed_cable:
  $mc.remove_item("fixed_cable")
  "It wasn't easy to plug in. Luckily, my specialized fuse cable replacement tool saved the day once more."
  "Maybe there's a career for me in engineering?"
  "Anyway, now that this is fixed, [jacklyn] should be so grateful."
  $quest.jacklyn_broken_fuse.advance()
  return

label school_first_hall_power_interact_kate_search_for_nurse:
  "The weakest spot in the school's defenses. This is where any marauder would start."
  return

label school_first_hall_power_interact_maxine_lines_interact:
  "This seems like a good place to charge [maxine]'s strange device..."
  "I need to find a fitting cable, though."
  return

label school_first_hall_power_interact_maxine_lines_item(item):
  if item == "sliced_cable":
    $mc.remove_item(item)
    "I'm something of a hobby electrician myself."
    "..."
    "Please don't explode or fry the school's electrical grid..."
    "..."
    "Bingo!"
    "My personal charging station."
    $school_first_hall["personal_charging_station"] = True
  elif item == "charger_cable":
    $quest.maxine_lines.failed_item("fusebox_cable",item)
    "This cable fits into the locator device's port, but not into the fuse box."
    "I might need to conduct some cable surgery..."
    $school_first_hall["cable_surgery"] = True
  else:
    $quest.maxine_lines.failed_item("fusebox_cable",item)
    "Sticking my [item.title_lower] would possibly provide me with a free session of electroshock therapy."
    "Just put it in like—"
    window hide
    show white with Dissolve(.25)
    $set_dialog_mode("default_no_bg")
    mc "Aasdasg-ashsdhs-gdgeehh!" with vpunch
    hide white with Dissolve(.5)
    window auto
    $set_dialog_mode("")
    "Ah, yes... that hit the spot..."
  return

label school_first_hall_power_interact_maxine_lines_item_charge(item):
  if item == "leyline_locator":
    if not quest.maxine_lines['first_charge']:
      "Just plug it in and—"
      window hide
      show white with Dissolve(.25)
      $school_first_hall['electric_smoke_now'] = True
      $process_event("update_state")
      with vpunch
      pause(.5)
      hide white with Dissolve(1)
      window auto
      "Son of a bitch! It... worked?"
      "Smells like burnt plastic, but some of the buttons on the device lit up..."
      "Hopefully, no one comes sniffing."
      $quest.maxine_lines['first_charge'] = True
      $quest.maxine_lines['lll_charged'] = 4
      $quest.maxine_lines.advance("charge")
    else:
      if quest.maxine_lines['lll_charged']:
        "Supercharging it would be cool, but the school is on a tight budget."
        "I don't want to rack up their electricity bill unnecessarily."
      else:
        "The good thing about having a personal charging station is that you can conveniently charge your mysterious devices."
        "The bad thing about it is that you never know if you'll get zapped and die on the spot."
        "Life is sometimes full of excitement..."
        window hide
        show white with Dissolve(.25)
        $school_first_hall['electric_smoke_now'] = True
        with vpunch
        pause(.5)
        hide white with Dissolve(1)
        window auto
        $quest.maxine_lines['lll_charged'] = 4
  else:
    "If only I could charge my [item.title_lower] here, I could change the world. If only..."
    $quest.maxine_lines.failed_item("fusebox_charge",item)
  return
