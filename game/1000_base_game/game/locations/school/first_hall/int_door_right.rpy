init python:
  class Interactable_school_first_hall_door_right(Interactable):

    def title(cls):
      return "Sports Wing"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "The corridor of physical inferiority. Next to Chad, my chicken legs and t-rex arms aren't exactly a good look."
      elif quest.kate_search_for_nurse.started:
        return "Sports just makes me want to gag. Without all the fit babes, this place wouldn't be worth visiting."
      else:
        return "There should be a sign here saying, \"Warning for Rabid Jocks.\"\n\nEntering is like playing Russian roulette, but with a bully in each chamber."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","East Corridor","goto_school_first_hall_east"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart":
            actions.append(["go","East Corridor","?first_hall_door_right_lindsey_wrong_lindseyart"])
            return
          elif quest.lindsey_wrong in ("verywet","guard","mopforkate","cleanforkate"):
            actions.append(["go","East Corridor","?first_hall_door_east_lindsey_wrong_verywet"])
            return
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate in ("hunt","rescue","run"):
            actions.append(["go","East Corridor","?first_hall_door_right_kate_fate_interact"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "trap":
            actions.append(["go","East Corridor","?quest_isabelle_dethroning_trap_leave_first_hall"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","East Corridor","goto_school_first_hall_east"])
      else:
        if quest.isabelle_tour == "english_class" and quest.kate_over_isabelle == "the_winning_team":
          actions.append(["go","East Corridor","?first_hall_door_right_isabelle_tour_side_kate"])
          return
        elif quest.isabelle_tour in ("first_hall_meetup","confrontation_side_kate","confrontation_side_isabelle","english_class"):
          actions.append(["go","East Corridor","?first_hall_door_right_isabelle_tour_first_hall_meetup"])
          return
      actions.append(["go","East Corridor","goto_school_first_hall_east"])


label first_hall_door_right_kate_fate_interact:
  "Running into the arms of my pursuer?"
  "This is a thriller, not a romance."
  return

label first_hall_door_right_isabelle_tour_first_hall_meetup:
  "Hell no. That's where you don't go."
  return

label first_hall_door_right_isabelle_tour_side_kate:
  "Entering that corridor seems like a terrible idea, especially now that [kate] is fuming."
  return

label first_hall_door_right_lindsey_wrong_lindseyart:
  "[lindsey]'s counting on me. Abandoning her now would ruin all my chances."
  return

label first_hall_door_east_lindsey_wrong_verywet:
  if quest.lindsey_wrong == "verywet":
    "It's better to get wet than get detention for the rest of the year."
  elif quest.lindsey_wrong in ("guard", "mopforkate"):
    "Running into Chad right now would not be good. He'd use my hair as a mop."
  else:
    show kate neutral with Dissolve(.5)
    kate neutral "Where do you think you're going?"
    kate neutral "Back to work."
    hide kate neutral with Dissolve(.5)
  return
