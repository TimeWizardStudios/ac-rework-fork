init python:
  class Interactable_school_first_hall_water_puddle_6(Interactable):

    def title(cls):
      return "Puddle"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,puddle|Use What?","lindsey_wrong_puddle_use_mop",("6",)])


init python:
  class Interactable_school_first_hall_water_puddle_5(Interactable):

    def title(cls):
      return "Puddle"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,puddle|Use What?","lindsey_wrong_puddle_use_mop",("5",)])


init python:
  class Interactable_school_first_hall_water_puddle_4(Interactable):

    def title(cls):
      return "Puddle"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,puddle|Use What?","lindsey_wrong_puddle_use_mop",("4",)])


init python:
  class Interactable_school_first_hall_water_puddle_3(Interactable):

    def title(cls):
      return "Puddle"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,puddle|Use What?","lindsey_wrong_puddle_use_mop",("3",)])


init python:
  class Interactable_school_first_hall_water_puddle_2(Interactable):

    def title(cls):
      return "Puddle"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,puddle|Use What?","lindsey_wrong_puddle_use_mop",("2",)])


init python:
  class Interactable_school_first_hall_water_puddle_1(Interactable):

    def title(cls):
      return "Puddle"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,puddle|Use What?","lindsey_wrong_puddle_use_mop",("1",)])


init python:
  class Interactable_school_first_hall_water_puddles_squid(Interactable):

    def title(cls):
      return "Puddles"

    def description(cls):
      return "Perhaps the doings of a delinquent squid?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_squid":
          if quest.flora_squid == "search":
            actions.append("?school_first_hall_water_puddles_squid_interact")


label school_first_hall_water_puddles_squid_interact:
  call quest_flora_squid_search_puddles
  return
