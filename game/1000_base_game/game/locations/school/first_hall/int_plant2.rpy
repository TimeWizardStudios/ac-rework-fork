init python:
  class Interactable_school_first_hall_plant2(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      if quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "With all these butterflies orbiting around it, this plant must feel like the [kate] of the flora."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_first_hall_plant2_interact")


label school_first_hall_plant2_interact:
  "If only I was a plant, maybe I could also have a harem of my own. Alas."
  return
