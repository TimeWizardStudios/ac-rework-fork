init python:
  class Interactable_school_first_hall_door_left(Interactable):

    def title(cls):
      return "Fine Arts Wing"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "The corridor of intellectual inadequacy. Knowing code and macs apparently isn't the same as knowing Cormac."
      elif quest.kate_search_for_nurse.started:
        return "They say that beauty is in the eye of the beholder. Like an eyelash or speck of dust in this case."
      else:
        return "They call this the Fine Arts Wing, but it's little more than a corridor, and the art isn't particularly fine."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_nurse":
          if quest.lindsey_nurse == "fetch_the_nurse":
            actions.append(["go","West Corridor","?first_hall_door_west_lindesy_fetch_nurse"])
            return
        elif mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","West Corridor","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("verywet","guard","mopforkate","cleanforkate"):
            actions.append(["go","West Corridor","?first_hall_door_west_lindsey_wrong_verywet"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "trap":
            actions.append(["go","West Corridor","?quest_isabelle_dethroning_trap_leave_first_hall"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","West Corridor","goto_school_first_hall_west"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","West Corridor","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.isabelle_tour < "english_class":
          actions.append(["go","West Corridor","?first_hall_door_west_isabelle_tour"])
          return
        elif quest.isabelle_tour == "english_class" and quest.kate_over_isabelle == "the_winning_team":
          actions.append(["go","West Corridor","?first_hall_door_west_isabelle_tour_side_kate"])
          return
      actions.append(["go","West Corridor","goto_school_first_hall_west"])


label first_hall_door_west_isabelle_tour:
  "Leave no man behind... or woman, in this case. [isabelle] needs her tour, and she's going to get it."
  return

label first_hall_door_west_lindesy_fetch_nurse:
  "The [nurse] might be fine, but she isn't in the Fine Arts wing."
  return

label first_hall_door_west_isabelle_tour_side_kate:
  "[isabelle] already left. Why did staying with [kate] seem like a good idea? Okay, I better fix this."
  return

label first_hall_door_west_lindsey_wrong_verywet:
  if quest.lindsey_wrong == "verywet":
    "There's only one way out of this, and it's a very wet way."
  elif quest.lindsey_wrong in ("guard", "mopforkate"):
    "Art is all fine and dandy, but it won't help a man required to do manual labor."
  else:
    show kate annoyed with Dissolve(.5)
    kate annoyed "Naughty maid!"
    kate annoyed "You have lots of mopping left to do before I'll set you free."
    hide kate with Dissolve(.5)
  return
