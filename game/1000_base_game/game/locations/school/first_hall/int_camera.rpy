
init python:
  class Interactable_school_first_hall_camera(Interactable):

    def title(cls):
      return "Camera"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.lindsey_motive >= "distraction":
        return "Always watching. Always waiting."
      else:
        return "Big Mother is watching."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if "lure" >= quest.lindsey_motive >= "distraction":
          actions.append("?quest_lindsey_motive_distraction_camera")
        elif quest.lindsey_motive == "power_cut":
          actions.append("?quest_lindsey_motive_power_cut_camera")
      actions.append("?school_first_hall_camera_interact")


label school_first_hall_camera_interact:
  "Ever watched a video of yourself and realized how truly unattractive you are?"
  "Handsome people can't relate."
  return
