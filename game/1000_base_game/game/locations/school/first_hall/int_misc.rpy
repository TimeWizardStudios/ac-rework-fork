init python:
  class Interactable_school_first_hall_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_dollar1_take"])


label school_first_hall_dollar1_take:
  $school_first_hall["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_dollar2_take"])


label school_first_hall_dollar2_take:
  $school_first_hall["dollar2_taken_today"] = True
  $mc.money+=20
  return
