init python:
  class Interactable_school_first_hall_red_paint(Interactable):

    def title(cls):
      return "Red Paint"

    def description(cls):
      return "The first piece of this\nmost lethal trap."

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_dethroning,red_paint|Use What?","quest_isabelle_dethroning_trap_red_paint"])
      actions.append("?school_first_hall_red_paint_interact")


label school_first_hall_red_paint_interact:
  "Shit, it almost fell over me!"
  "That would surely have been the outcome last time..."
  return
