init python:
  class Interactable_school_first_hall_plant(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "It is said that anyone foolish enough to vandalize this plant wakes up in a dumpster, wearing nothing but garden overalls and a grin of insanity."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.jo_day == "supplies":
          actions.append("?quest_jo_day_supplies_plant_four")
        if quest.lindsey_motive == "lure":
          actions.append("?quest_lindsey_motive_lure_first_hall_plant_interact")
      actions.append("?school_first_hall_plant_interact")


label school_first_hall_plant_interact:
  "On the surface, a completely normal plant."
  "Under closer inspection, however..."
  "Still a completely normal plant."
  return

