init python:
  class Interactable_school_first_hall_mop(Interactable):
    def title(cls):
      return "Janitor's Mop"
    def description(cls):
      return "A harsh reminder of the hardships of labor."
    def actions(cls,actions):
      actions.append("?school_first_hall_mop_interact")

label school_first_hall_mop_interact:
  "I vowed to turn my life around..."
  "But some dirt never goes away. No matter how hard you scrub."
  return
