init python:
  class Interactable_school_first_hall_board(Interactable):
    def title(cls):
      return "Bulletin Board"
    def actions(cls,actions):
      actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:maxine_wine,flora_poster|Use What?","school_first_hall_board_maxine_wine_locker_use_item"])

label school_first_hall_board_maxine_wine_locker_use_item(item):
  if item == "flora_poster":
    $quest.maxine_wine["1f_hall_poster"] = True
    $mc.remove_item("flora_poster")
    "Okay, that looks great there. Hope no one realizes I used gum to put it there. Damn global shortage of pins."
    if quest.maxine_wine["entrance_hall_poster"] + quest.maxine_wine["homeroom_poster"] + quest.maxine_wine["admin_wing_poster"] + quest.maxine_wine["1f_hall_poster"] + quest.maxine_wine["sports_wing_poster"] == 5:
      "That's all of them. Praise the lord Jesus hallelujah. My ass is sweating more than that hipster up on the cross."
      $quest.maxine_wine.advance("florareward")
  else:
    "My [item.title_lower] would look great nailed to this board, but that would probably get me expelled."
    $quest.maxine_wine.failed_item("flora_poster",item)
  return
