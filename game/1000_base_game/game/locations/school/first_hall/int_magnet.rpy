init python:
  class Interactable_school_first_hall_magnet(Interactable):
    def title(cls):
      return "Magnet"
    def description(cls):
      return "Surely, no one will find a random magnet on the floor suspicious."
    def actions(cls,actions):
      actions.append(["take","Take","school_first_hall_magnet_take"])
      actions.append("?school_first_hall_magnet_interact")

label school_first_hall_magnet_take:
  $school_first_hall["magnet"] = False
  $mc.add_item("magnet")
  return

label school_first_hall_magnet_interact:
  "In perfect position... or just randomly on the floor. Who knows at this point?"
  return
