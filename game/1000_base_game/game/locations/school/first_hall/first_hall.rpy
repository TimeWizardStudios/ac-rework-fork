init python:
  class Location_school_first_hall(Location):

    default_title="First Hall"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,0),"school first_hall background"])
      scene.append([(1637,658),"school first_hall walk_right","school_first_hall_door_right"])
      scene.append([(0,657),"school first_hall walk_left","school_first_hall_door_left"])
      scene.append([(1276,20),"school first_hall cctv",("school_first_hall_camera",0,200)])
      scene.append([(1754,265),"school first_hall electric",("school_first_hall_power",-75,100)])
      if school_first_hall['electric_smoke_now']:
        scene.append([(1758,314),"school first_hall smoke"])
      scene.append([(1780,610),"school first_hall stairs",("school_first_hall_stairs",-95,0)])
      scene.append([(194,427),"school first_hall door","school_first_hall_door"])
      if quest.isabelle_hurricane["scratching"] and not quest.isabelle_hurricane["darkness"]:
        scene.append([(213,591),"school first_hall spinach_scratching",("school_first_hall_door",-2,-164)])
      if quest.maxine_wine == "locker" and mc.owned_item("flora_poster") and not quest.maxine_wine["1f_hall_poster"]:
        scene.append([(66,443),"school first_hall board","school_first_hall_board"])
      else:
        scene.append([(66,443),"school first_hall board"])
        if game.season == 1 and not quest.jo_washed.in_progress:
          if quest.maxine_wine["1f_hall_poster"]:
            scene.append([(104,484),"school first_hall flora_poster"])
      scene.append([(77,599),"school first_hall bin",("school_first_hall_bin",30,0)])

      #Water Fountain
      if game.season == 1 and not quest.jo_washed.in_progress:
        if quest.lindsey_wrong["fountain_state"] in (1,2,3,4):
          scene.append([(0,482),"school first_hall water_fountain_gushing"])
        elif quest.lindsey_wrong["fountain_state"] == 5:
          #scene.append([(15,552),"school first_hall mcjacket","school_first_hall_mcjacket"])
          scene.append([(0,542),"school first_hall water_fountain_hoodie",("school_first_hall_water_fountain_hoodie",170,0)])
        else:
          scene.append([(0,525),"school first_hall water",("school_first_hall_water_fountain",120,0)])
      else:
        scene.append([(0,525),"school first_hall water",("school_first_hall_water_fountain",120,0)])

      scene.append([(1727,520),"school first_hall plant",("school_first_hall_plant",-70,0)])

      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        if game.season == 1 and not quest.jo_washed.in_progress:
          if quest.lindsey_motive >= "power_cut":
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window"])
            scene.append([(526,293),"school first_hall window_open"])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn"])
#             scene.append([(526,293),"school first_hall window_autumn_open"])
            scene.append([(504,273),"school first_hall window_night"])
            scene.append([(526,293),"school first_hall window_night_open"])
            scene.append([(547,770),"school first_hall plant2","school_first_hall_plant2"])
            if school_first_hall["butterflies_inside"]:
              scene.append([(551,721),"school first_hall butterflies1",("school_first_hall_plant2",1,49)])
            else:
              scene.append([(531,440),"school first_hall butterflies2"])
          elif quest.lindsey_motive in ("window_open","lure"):
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window",("school_first_hall_window",0,15)])
            scene.append([(526,293),"school first_hall window_open",("school_first_hall_window",363,-19)])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn",("school_first_hall_window",0,15)])
#             scene.append([(526,293),"school first_hall window_autumn_open",("school_first_hall_window",363,-19)])
            scene.append([(504,273),"school first_hall window_night"])
            scene.append([(526,293),"school first_hall window_night_open"])
            scene.append([(531,440),"school first_hall butterflies2",("school_first_hall_window",365,-166)])
          elif quest.lindsey_motive in ("distraction","keys"):
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window",("school_first_hall_window",0,15)])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn",("school_first_hall_window",0,15)])
            scene.append([(504,273),"school first_hall window_night"])
            scene.append([(531,440),"school first_hall butterflies3",("school_first_hall_window",365,-166)])
          else:
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window"])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn"])
            scene.append([(504,273),"school first_hall window_night"])
            scene.append([(531,440),"school first_hall butterflies3"])
        else:
#         if game.season == 1:
#           scene.append([(489,257),"school first_hall window"])
#         elif game.season == 2:
          scene.append([(489,257),"school first_hall window_autumn"])
          scene.append([(504,273),"school first_hall window_night"])
          scene.append([(531,440),"school first_hall butterflies3"])
      else:
        if game.season == 1 and not quest.jo_washed.in_progress:
          if quest.lindsey_motive >= "power_cut":
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window"])
            scene.append([(526,293),"school first_hall window_open"])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn"])
#             scene.append([(526,293),"school first_hall window_autumn_open"])
            scene.append([(547,770),"school first_hall plant2","school_first_hall_plant2"])
            if school_first_hall["butterflies_inside"]:
              scene.append([(551,721),"school first_hall butterflies1",("school_first_hall_plant2",1,49)])
            else:
              scene.append([(531,440),"school first_hall butterflies2"])
          elif quest.lindsey_motive in ("window_open","lure"):
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window",("school_first_hall_window",0,15)])
            scene.append([(526,293),"school first_hall window_open",("school_first_hall_window",363,-19)])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn",("school_first_hall_window",0,15)])
#             scene.append([(526,293),"school first_hall window_autumn_open",("school_first_hall_window",363,-19)])
            scene.append([(531,440),"school first_hall butterflies2",("school_first_hall_window",365,-166)])
          elif quest.lindsey_motive in ("distraction","keys"):
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window",("school_first_hall_window",0,15)])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn",("school_first_hall_window",0,15)])
            scene.append([(531,440),"school first_hall butterflies3",("school_first_hall_window",365,-166)])
          else:
#           if game.season == 1:
            scene.append([(489,257),"school first_hall window"])
#           elif game.season == 2:
#             scene.append([(489,257),"school first_hall window_autumn"])
            scene.append([(531,440),"school first_hall butterflies3"])
        else:
#         if game.season == 1:
#           scene.append([(489,257),"school first_hall window"])
#         elif game.season == 2:
          scene.append([(489,257),"school first_hall window_autumn"])
          scene.append([(531,440),"school first_hall butterflies3"])

      if not maya.talking:
        if maya.at("school_first_hall","sitting"):
          scene.append([(1458,180),"school first_hall maya_autumn","maya"])

      scene.append([(232,529),"school first_hall newspaper","school_first_hall_newspaper"])
      if school_first_hall["kate_fashion_poster"] and not quest.jo_washed.in_progress:
        scene.append([(297,271),"school first_hall kate_fashion_poster","school_first_hall_kate_poster"])
      scene.append([(1422,378),"school first_hall vending","school_first_hall_vending"])
      if not quest.jo_washed.in_progress:
        if quest.isabelle_dethroning in ("revenge_done","panik") and not school_first_hall["rusty_key_taken"]:
          scene.append([(1350,789),"school first_hall backpack","school_first_hall_backpack"])
        else:
          scene.append([(1350,789),"school first_hall backpack"])
      if school_first_hall["red_paint"]:
        scene.append([(1654,293),"school first_hall red_paint",("school_first_hall_red_paint",0,-25)])
      if school_first_hall["fishing_hook"]:
        scene.append([(1681,279),"school first_hall fishing_hook",("school_first_hall_red_paint",-14,-11)])
      if school_first_hall["fishing_line"]:
        scene.append([(1703,314),"school first_hall fishing_line",("school_first_hall_red_paint",-86,-46)])
      if school_first_hall["paint_splash"]:
        scene.append([(68,689),"#school first_hall paint_splash"])

      if not quest.lindsey_wrong in ("mop","doughnuts","clean","splash","verywet","guard","mopforkate","cleanforkate"):

        if not spinach.talking:
          if spinach.at("school_first_hall","running"):
            scene.append([(1721,750),"school first_hall spinach","spinach"])

        if not maxine.talking:
          if maxine.at("school_first_hall", "vending"):
            if game.season == 1:
              scene.append([(1498,711),"school first_hall maxine","maxine"])
            elif game.season == 2:
              scene.append([(1498,721),"school first_hall maxine_autumn","maxine"])

        if not quest.kate_search_for_nurse["kate_found_wrappers"]:
          if quest.kate_search_for_nurse.in_progress and quest.kate_search_for_nurse=="start":
            if not school_first_hall["candy10_taken"]:
              scene.append([(1325,1031),"school first_hall candy10","school_first_hall_candy10"])
            if not school_first_hall["candy9_taken"]:
              scene.append([(1473,987),"school first_hall candy9","school_first_hall_candy9"])
            if not school_first_hall["candy8_taken"]:
              scene.append([(1566,903),"school first_hall candy8","school_first_hall_candy8"])
            if not school_first_hall["candy7_taken"]:
              scene.append([(1500,895),"school first_hall candy7","school_first_hall_candy7"])
            if not school_first_hall["candy5_taken"]:
              scene.append([(847,871),"school first_hall candy5","school_first_hall_candy5"])
            if not school_first_hall["candy4_taken"]:
              scene.append([(587,917),"school first_hall candy4","school_first_hall_candy4"])
            if not school_first_hall["candy2_taken"]:
              scene.append([(248,931),"school first_hall candy2","school_first_hall_candy2"])
            if not school_first_hall["candy1_taken"]:
              scene.append([(183,881),"school first_hall candy1","school_first_hall_candy1"])

        if not isabelle.talking:
          if isabelle.at("school_first_hall", "standing"):
            if game.season == 1:
              scene.append([(1199,466),"school first_hall isabelle","isabelle"])
            elif game.season == 2:
              scene.append([(1199,466),"school first_hall isabelle_autumn","isabelle"])
              if isabelle.equipped_item("isabelle_collar"):
                scene.append([(1261,538),"school first_hall isabelle_collar",("isabelle",6,-72)])

      if not kate.talking:
        if kate.at("school_first_hall", "standing"):
          if game.season == 1:
            scene.append([(581,436),"school first_hall kate","kate"])
          elif game.season == 2:
            scene.append([(581,436),"school first_hall kate_autumn","kate"])

      #Dollars
      if not (quest.kate_blowjob_dream == "school" or quest.jo_washed.in_progress or quest.mrsl_bot == "dream"):
        if school_first_hall["dollar1_spawned_today"] == True and not school_first_hall["dollar1_taken_today"]:
          scene.append([(104,684),"school first_hall dollar1","school_first_hall_dollar1"])
        if school_first_hall["dollar2_spawned_today"] == True and not school_first_hall["dollar2_taken_today"]:
          scene.append([(1453,879),"school first_hall dollar2","school_first_hall_dollar2"])

      if game.season == 1 and not quest.jo_washed.in_progress:
        if school_first_hall["mop_shown"]:
          scene.append([(469,478),"school first_hall mop","school_first_hall_mop"])

      if game.season == 1 and not quest.jo_washed.in_progress:
        if quest.lindsey_wrong["fountain_state"] in (4,5):
          if not quest.lindsey_wrong["puddle_6_clean"]:
            scene.append([(145,914),"school first_hall water_puddle_6","school_first_hall_water_puddle_6"])
          if not quest.lindsey_wrong["puddle_5_clean"]:
            scene.append([(16,796),"school first_hall water_puddle_5","school_first_hall_water_puddle_5"])
        if quest.lindsey_wrong["fountain_state"] in (3,4,5):
          if not quest.lindsey_wrong["puddle_4_clean"]:
            scene.append([(138,792),"school first_hall water_puddle_4","school_first_hall_water_puddle_4"])
          if not quest.lindsey_wrong["puddle_3_clean"]:
            scene.append([(203,695),"school first_hall water_puddle_3","school_first_hall_water_puddle_3"])
        if quest.lindsey_wrong["fountain_state"] in (2,3,4,5):
          if not quest.lindsey_wrong["puddle_2_clean"]:
            scene.append([(117,699),"school first_hall water_puddle_2","school_first_hall_water_puddle_2"])
          if not quest.lindsey_wrong["puddle_1_clean"]:
            scene.append([(21,727),"school first_hall water_puddle_1","school_first_hall_water_puddle_1"])

      if quest.flora_squid >= "search" and quest.flora_squid.in_progress:
        scene.append([(145,914),"school first_hall water_puddle_6",("school_first_hall_water_puddles_squid",-16,-172)])
        scene.append([(16,796),"school first_hall water_puddle_5",("school_first_hall_water_puddles_squid",108,-54)])
        scene.append([(138,792),"school first_hall water_puddle_4",("school_first_hall_water_puddles_squid",0,-50)])
        scene.append([(203,695),"school first_hall water_puddle_3",("school_first_hall_water_puddles_squid",-65,47)])
        scene.append([(117,699),"school first_hall water_puddle_2",("school_first_hall_water_puddles_squid",16,43)])
        scene.append([(21,727),"school first_hall water_puddle_1",("school_first_hall_water_puddles_squid",86,15)])

      if school_first_hall['magnet']:
        scene.append([(889,930),"school first_hall magnet","school_first_hall_magnet"])

      scene.append([(0,0),"#school first_hall overlay"])

      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(0,0),"#school first_hall overlay_night"])

      if quest.isabelle_hurricane == "short_circuit" and quest.isabelle_hurricane["darkness"]:
        scene.append([(0,0),"black"])

    def find_path(self,target_location):
      if target_location.id in ("school_english_class","school_art_class","school_music_class","school_first_hall_west"):
        return "school_first_hall_door_left",dict(marker_offset=(125,125),marker_sector="right_top")
      elif target_location.id in ("school_gym","school_bathroom","school_locker_room","school_first_hall_east"):
        return "school_first_hall_door_right",dict(marker_offset=(150,150),marker_sector="left_top")
      else:
        return ("school_first_hall_stairs",dict(marker_offset=(150,0),marker_sector="right_bottom"))


label goto_school_first_hall:
  if school_first_hall.first_visit_today:
    $school_first_hall["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_first_hall["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_first_hall)
  return
