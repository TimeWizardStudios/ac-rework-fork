init python:
  class Interactable_school_first_hall_kate_poster(Interactable):

    def title(cls):
      return "[kate]'s Fashion Poster"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Why does someone so evil\nget to be so pretty?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_first_hall_kate_poster_interact")


label school_first_hall_kate_poster_interact:
  "The prettiest girl in Newfall — that's what everyone is thinking."
  "And I made it happen."
  return
