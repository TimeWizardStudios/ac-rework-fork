init python:
  class Interactable_school_first_hall_water_fountain(Interactable):

    def title(cls):
      return "Water Fountain"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Why does it taste different every day?"

    def actions(cls,actions):
      if mc.owned_item(("diary_page","secret_note","empty_bottle","spray_empty_bottle","lindsey_bottle","kate_socks_dirty")):
        actions.append(["use_item", "Use item", "select_inventory_item","$quest_item_filter:act_one,water_fountain|Use What?","school_first_hall_water_fountain_use_item"])
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            if mc.owned_item(("diary_page","secret_note","empty_bottle","spray_empty_bottle","lindsey_bottle","kate_socks_dirty")):
              actions.remove(["use_item", "Use item", "select_inventory_item","$quest_item_filter:act_one,water_fountain|Use What?","school_first_hall_water_fountain_use_item"])
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            if mc.owned_item(("diary_page","secret_note","empty_bottle","spray_empty_bottle","lindsey_bottle","kate_socks_dirty")):
              actions.remove(["use_item", "Use item", "select_inventory_item","$quest_item_filter:act_one,water_fountain|Use What?","school_first_hall_water_fountain_use_item"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            if mc.owned_item(("diary_page","secret_note","empty_bottle","spray_empty_bottle","lindsey_bottle","kate_socks_dirty")):
              actions.remove(["use_item", "Use item", "select_inventory_item","$quest_item_filter:act_one,water_fountain|Use What?","school_first_hall_water_fountain_use_item"])
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
          actions.append("school_first_hall_water_fountain_interact_askmaxine")
      actions.append("?school_first_hall_water_fountain_interact")


label school_first_hall_water_fountain_interact_askmaxine:
  "{i}*Cough, cough*{/}"
  "What the hell?"
  "How did this piece of paper end up in my throat?"
  $quest.isabelle_stolen["letter_from_maxine_recieved"] = True
  $mc.add_item("letter_from_maxine")
  return

label school_first_hall_water_fountain_interact:
  "Life giving sustenance."
  "Who needs it?"
  return

label school_first_hall_water_fountain_use_item(item):
  if item == "diary_page":
    show misc secret_diary with Dissolve(.5)
    show misc secret_diary_2 with Dissolve(2)
    pause
    hide secret_diary
    hide misc secret_diary_2
    with Dissolve(.5)
  elif item == "lindsey_bottle":
    call school_first_hall_water_fountain_use_item_lindsey_wrong(item)
  elif item == "kate_socks_dirty":
    "I wonder what [kate] will think when I return her socks perfectly clean...{space=-45}"
    "It's weird, but I can't wait to feel that shame she'll undoubtedly pour on."
    "..."
    $mc.remove_item("kate_socks_dirty")
    $mc.add_item("kate_socks_clean")
  elif item == "secret_note":
    show misc secret_note_1 with Dissolve(.5)
    show misc tsn as secret_note_reveal with Dissolve(2)
    pause
    hide secret_note_reveal
    hide misc secret_note_1
    with Dissolve(.5)
  elif item in ("empty_bottle","spray_empty_bottle"):
    $mc.remove_item(item)
    if item == "empty_bottle":
      $mc.add_item("water_bottle")
    elif item == "spray_empty_bottle":
      $mc.add_item("spray_water")
    if quest.lindsey_wrong == "fountain":
      "It's good to stay hydrated."
      "[lindsey] wanted water in her own bottle, though."
    else:
      "Even though this water is free, I still like to imagine I'm getting one over on the establishment."
  else:
    "I sprinkle a few drops of water on my [item.title_lower]."
    "Then I stand around like an idiot as if something was supposed to happen."
    $quest.act_one.failed_item("water_fountain",item)
  return

label school_first_hall_lindsey_wrong_verywet:
  menu(side="middle"):
    "Remove the hoodie":
      show lindsey soaked_bg_jacket1 with Dissolve(.5)
      "All right... here we go, I guess..."
      show lindsey soaked_bg_jacket2_splash with hpunch
      $quest.lindsey_wrong["mc_verywet"] = True
      show lindsey soaked_bg_jacket2_splash with fadehold
      "Ugh. Feels like my internal organs just got filled with water!"
      "Oh, god. Better put the hoodie back before the school gets flooded."
      show lindsey soaked_bg_jacket1 with Dissolve(.5)
      "Damn. I'm going to be in so much trouble."
      hide lindsey
      show kate laughing
      with Dissolve(.5)
      kate laughing "Haha! What a look! Like a wet puppy!"
      mc "..."
      kate neutral "Are you my wet puppy?"
      mc "Ugh..."
      kate confident "Well, you better go find a mop quickly. I'll get the principal if I get bored."
      mc "Yes, ma'am..."
      kate sad "Poor wet puppy..."
      kate gushing "Fetch!"
      "How utterly humiliating..."
      "Well, there's probably a mop in the janitor's closet."
      hide kate with Dissolve(.5)
      $quest.lindsey_wrong.advance("guard")
    "Do nothing":
      "Not sure how long I can stall. [kate]'s impatience is growing."
  return


init python:
  class Interactable_school_first_hall_water_fountain_hoodie(Interactable):

    def title(cls):
      return "Broken Water Fountain"

    def description(cls):
      return "The well of life, shattered by negligence and disrepair."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "verywet":
            actions.append("school_first_hall_lindsey_wrong_verywet")
      actions.append("?school_first_hall_water_fountain_hoodie_interact")


label school_first_hall_water_fountain_hoodie_interact:
  "It was a health risk anyway. I practically did everyone a favor."
  return
