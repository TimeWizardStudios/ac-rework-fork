init python:
  class Interactable_school_first_hall_stairs(Interactable):

    def title(cls):
      return "Stairs"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return "Too late to go back now."
      elif quest.kate_search_for_nurse.started:
        return "Sometimes, the way down is the only way out. Holds true in most cases of architecture and despair."
      else:
        return "Identifying stairs...\n\nYep, it's stairs"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Ground Floor","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart":
            actions.append(["go","Ground Floor","?first_hall_stairs_lindsey_wrong_lindseyart"])
            return
          elif quest.lindsey_wrong in ("verywet","cleanforkate"):
            actions.append(["go","Ground Floor","?first_hall_stairs_lindsey_wrong_verywet"])
            return
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate in ("rescue","run"):
            actions.append(["go","Ground Floor","quest_kate_fate_rescue_stairs_interact"])
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume" and not quest.kate_wicked["ass_downstairs"]:
            actions.append(["go","Ground Floor","quest_kate_wicked_costume_first_hall_stairs"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "trap":
            actions.append(["go","Ground Floor","?quest_isabelle_dethroning_trap_leave_first_hall"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Ground Floor","goto_school_ground_floor"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Ground Floor","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
      else:
        if quest.isabelle_tour == "english_class" and quest.kate_over_isabelle == "the_winning_team":
          actions.append(["go","Ground Floor","?quest_kate_over_isabelle_talk_kate"])
          return
      actions.append(["go","Ground Floor","goto_school_ground_floor"])

    #def circle (cls,actions,x,y):
    #  rv=super().circle(actions,x,y)
    #  rv.start_angle+=120
    #  rv.angle_per_icon= 32
    #  rv.center=(150,50)
    #  return rv


label quest_kate_fate_rescue_stairs_interact:
  if quest.kate_fate == "rescue":
    "When [isabelle] looks up from her book, you know you've made an impression."
    "Too bad bolting past her isn't the same as impressing [lindsey] with my speed..."
  call goto_school_ground_floor
  "Phew! Made it to safety."
  "Not even [kate] would try to abduct me in front of the [guard]'s booth."
  $quest.kate_fate.advance("escaped")
  return

label quest_kate_over_isabelle_talk_kate:
  "As much as I'd like to run, appeasing [kate] is more important. If I don't resolve this, she'll make my life even more miserable."
  return

label first_hall_stairs_lindsey_wrong_lindseyart:
  "Not exactly chivalrous to run off when the damsel needs me the most."
  return

label first_hall_stairs_lindsey_wrong_verywet:
  if quest.lindsey_wrong == "cleanforkate":
    show kate skeptical with Dissolve(.5)
    kate skeptical "Go ahead, take one step down those stairs."
    kate skeptical "No, really. See what happens."
    "..."
    kate excited "Didn't think so."
    hide kate with Dissolve(.5)
  else:
    "No use running, [kate]'s got me exactly where she wants me."
  return
