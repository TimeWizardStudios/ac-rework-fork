init python:
  class Interactable_school_first_hall_backpack(Interactable):

    def title(cls):
      return "Backpack"

    def description(cls):
      return "I always thought someone left it here every day, but who doesn't have a locker?"

    def actions(cls,actions):
      actions.append(["take","Take","school_first_hall_backpack_take"])


label school_first_hall_backpack_take:
  "I've always wondered what's inside this backpack."
  "Perhaps this is my only chance..."
  "..."
  $school_first_hall["rusty_key_taken"] = True
  $mc.add_item("rusty_key")
  return
