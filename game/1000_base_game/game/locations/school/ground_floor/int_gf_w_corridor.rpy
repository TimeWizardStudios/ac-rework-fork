init python:
  class Interactable_school_ground_floor_gf_w_corridor(Interactable):

    def title(cls):
      return "Admin Wing"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs >= "steal":
        return "The Administration Wing, the dullest of all. Like that of a featherless bird."
      else:
        return "The Corridor of Pain and Punishment. The place where all the criminals and wounded end up."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","West Corridor","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("guard","mopforkate","cleanforkate"):
            actions.append(["go","West Corridor","?school_ground_floor_gf_w_corridor_lindsey_wrong_verywet"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","West Corridor","quest_mrsl_table_morning"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","West Corridor","?quest_flora_handcuffs_steal_gf_w_corridor"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","West Corridor","?quest_isabelle_dethroning_fly_to_the_moon_corridor"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "fly_to_the_moon":
            actions.append(["go","West Corridor","?quest_isabelle_dethroning_fly_to_the_moon_corridor"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","West Corridor","goto_school_ground_floor_west"])
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","West Corridor","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","West Corridor","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.day1_take2.in_progress:
          actions.append(["go","West Corridor","?school_ground_floor_gf_w_corridor_interact_day1_take2"])
          return
      actions.append(["go","West Corridor","goto_school_ground_floor_west"])


label school_ground_floor_gf_w_corridor_interact_day1_take2:
  "To willingly enter this domain would be reckless and foolish. Besides, only first-year students get lollipops at the [nurse]'s office."
  return

label school_ground_floor_gf_w_corridor_lindsey_wrong_verywet:
  "Nope. No mop here."
  return
