init python:
  class Interactable_school_ground_floor_beaver(Interactable):
    def title(cls):
      return "Beaver"
    def description(cls):
      return "It's gobbling wood chips as we speak."
    def actions(cls,actions):
      actions.append("?school_ground_floor_beaver_interact")

label school_ground_floor_beaver_interact:
  mc "Where are you going to go now?"
  #SFX: Beaver noises
  ## Beaver sound
  play sound "<to 0.7>squeaking_animal"
  $quest.lindsey_book["beaver"] = 5
  "It ran into the administration wing."
  "There was definitely fear in those tiny black eyes."
  return
