init python:
  class Interactable_school_ground_floor_janitor_door(Interactable):

    def title(cls):
      return "Janitor's Closet"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_tour.started:
        return "The cleanest place in the school lies beyond this door. Few have seen it. Fewer still have lived to tell the glorious tale."
      elif quest.day1_take2.started:
        return "Locked. The janitor has a vendetta against horny students seeking a hiding spot."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Janitor's Closet","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("mop","guard","mopforkate","cleanforkate"):
            actions.append(["go","Janitor's Closet","?school_ground_floor_janitor_closet_lindsey_wrong_mop"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Janitor's Closet","quest_mrsl_table_morning"])
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Janitor's Closet","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Janitor's Closet","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik","fly_to_the_moon"):
            actions.append(["go","Janitor's Closet","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed == "supplies" and not mc.owned_item("bottle_of_soap"):
            actions.append(["go","Janitor's Closet","quest_jo_washed_supplies_janitor_closet"])
          elif quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Janitor's Closet","?quest_lindsey_angel_keycard_school_ground_floor_janitor_door"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Janitor's Closet","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Janitor's Closet","kate_blowjob_dream_random_interact_locked_location"])
      if quest.isabelle_tour.started:
        actions.append(["go","Janitor's Closet","?school_ground_floor_janitor_closet_interact_two"])
      actions.append(["go","Janitor's Closet","?school_ground_floor_janitor_closet_interact_day1_take2"])


label school_ground_floor_janitor_closet_interact_two:
  "Knock, knock! Anyone home?"
  "The janitor is usually out and about. She's the only one who does anything around here."
  return

label school_ground_floor_janitor_closet_interact_day1_take2:
  "The janitor always seemed kind of lonely. She was one of the few people here who never looked at me like I was dirt."
  "Go figure."
  return

label school_ground_floor_janitor_closet_lindsey_wrong_mop:
  "Well, it's locked. No real surprise there."
  "Maybe the [guard] could help?"
  return
