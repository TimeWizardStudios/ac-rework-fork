init python:
  class Interactable_school_ground_floor_missing_locker(Interactable):

    def title(cls):
      return "Missing Locker"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Looks like the entire locker was uprooted. Not even the lock remains."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.isabelle_buried == "missinglocker":
          actions.append("?school_ground_floor_missing_locker_interact_isabelle_bury")
      actions.append(["interact","Read Permit","?school_ground_floor_missing_locker_interact"])


label school_ground_floor_missing_locker_interact:
  show misc lockerremoval with Dissolve(.5)
  pause
  hide misc lockerremoval with Dissolve(.5)
  return
