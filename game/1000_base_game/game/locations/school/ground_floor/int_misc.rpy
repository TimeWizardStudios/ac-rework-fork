init python:
  class Interactable_school_ground_floor_dollar(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_ground_floor_dollar_take"])


label school_ground_floor_dollar_take:
  $school_ground_floor["dollar_taken_today"] = True
  $mc.money+=20
  return
