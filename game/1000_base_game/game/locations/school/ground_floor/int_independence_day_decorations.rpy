init python:
  class Interactable_school_ground_floor_independence_day_decorations(Interactable):

    def title(cls):
      return "Independence Day Decorations"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Ah, freedom!\n\nIronic that such a concept is celebrated in this place."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if (quest.kate_stepping == "independence_day"
        or quest.isabelle_dethroning == "independence_day"):
          actions.append("school_ground_floor_independence_day_decorations_interact1")
      actions.append("?school_ground_floor_independence_day_decorations_interact2")


label school_ground_floor_independence_day_decorations_interact1:
  "The decorations are already in place? Man, the Independence Day sure is coming up fast."
  "I can't believe a quarter of the school year has already passed..."
  "Well, might as well keep trucking along. It's not like it can get\nany worse."
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  menu(side="middle"):
    "Proceed":
      if quest.kate_stepping == "independence_day":
        "Tonight is the big takedown of [isabelle]. I can't wait."
        if mc.owned_item("spiked_ice_tea"):
          "Time to give her the sleeping potion..."
        else:
          "Time to get the sleeping potion in order..."
        $quest.kate_stepping.advance("spiked_drink")
      elif quest.isabelle_dethroning == "independence_day":
        "Tonight is the big night when the queen gets dethroned. I can't wait."
        "[flora] mentioned that [isabelle] wants to host the Independence Day dinner this year."
        "Perhaps, I should go and check out her announcement and make sure she doesn't get egged..."
        $quest.isabelle_dethroning.advance("announcement")
    "I better get my act together quick":
      pass
  return

label school_ground_floor_independence_day_decorations_interact2:
  "The decorations are already in place? Man, the Independence Day sure is coming up fast."
  "I can't believe a quarter of the school year has already passed..."
  return
