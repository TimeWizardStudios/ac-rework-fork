init python:
  class Interactable_school_ground_floor_lockers_front_right(Interactable):

    def title(cls):
      return "Lockers"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.the_key.started:
        return "My base of operations. The strategy table, if you will."
      elif quest.isabelle_tour.started:
        return "This locker always smells a bit funky. The owner should clean it out more often. But there are more important things in life and he knows that."
      elif quest.day1_take2.started:
        return "There's a locked lock on my locker. That's... unlocky."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if school_ground_floor["locker_unlocked"]:
          actions.append(["go","Open Locker","goto_school_locker"])
        else:
          actions.append(["use_item","Open Locker","select_inventory_item","$quest_item_filter:act_one,locker|Use What?","school_ground_floor_lockers_front_right_use_item"])
      if quest.day1_take2.started:
        actions.append("?school_ground_floor_lockers_front_right_interact_day1_take2")
      actions.append("?school_ground_floor_lockers_front_right_interact")


label school_ground_floor_lockers_front_right_use_item(item):
  if item.id == "locker_key":
    "Ah, my own personal oasis!"
    $school_ground_floor["locker_unlocked"] = True
    jump goto_school_locker
  else:
    "Try as I might, smushing my [item.title_lower] into the locker keyhole doesn't seem to work."
    $quest.act_one.failed_item("locker",item)
  return

label school_ground_floor_lockers_front_right_interact_day1_take2:
  "Every new school year, there's always a chance that a cute girl gets a locker next to mine."
  "It happened in my sophomore year, but she asked to change it a week later."
  "That heartbreak still stings."
  return

label school_ground_floor_lockers_front_right_interact:
  "Just the right size to fit one awkward, nerdy teen."
  "Not that I'm speaking from experience or anything..."
  return
