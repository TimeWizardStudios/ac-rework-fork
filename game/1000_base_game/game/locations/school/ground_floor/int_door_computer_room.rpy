init python:
  class Interactable_school_ground_floor_computer_room_door(Interactable):

    def title(cls):
      return "Computer Room"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs >= "steal":
        return "The computer classroom. The only place worth a visit here."
      else:
        return "The school's harbor of information. Also its port—porn. Also its porn harbor."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Computer Room","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "vines":
            actions.append(["go","Computer Room","?school_ground_floor_computer_room_door_escape"])
            return
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("guard","mopforkate","cleanforkate"):
            actions.append(["go","Computer Room","?school_ground_floor_computer_room_door_escape"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Computer Room","quest_mrsl_table_morning"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","Computer Room","?quest_flora_handcuffs_steal_computer_room_door"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Computer Room","?quest_isabelle_dethroning_fly_to_the_moon_computer_room_door"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Computer Room","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik"):
            actions.append(["go","Computer Room","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "fly_to_the_moon":
            actions.append(["go","Computer Room","?quest_isabelle_dethroning_fly_to_the_moon_computer_room_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Computer Room","?quest_lindsey_angel_keycard_school_ground_floor_computer_room_door"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Computer Room","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Computer Room","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.maya_witch == "computer_class":
          actions.append(["go","Computer Room","quest_maya_witch_computer_class"])
      if school_computer_room["unlocked"]:
        actions.append(["go","Computer Room","goto_school_computer_room"])
      else:
        actions.append(["go","Computer Room","?school_ground_floor_computer_room_door_locked"])


label school_ground_floor_computer_room_door_locked:
  "This computer room is a digital oasis in the analog desert that is Newfall High."
  return

label school_ground_floor_computer_room_door_escape:
  "Now is not the time for virtual escape... or any other type of escape, for that matter."
  return
