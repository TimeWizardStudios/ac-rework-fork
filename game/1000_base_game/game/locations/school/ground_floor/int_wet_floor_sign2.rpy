init python:
  class Interactable_school_ground_floor_wet_floor_sign_down(Interactable):

    def title(cls):
      return "Wet Floor Sign"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The plastic idol lies fallen on the scene of the crime. To some, the culprit. To others, a hero."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_ground_floor_wet_floor_sign")


label school_ground_floor_wet_floor_sign:
  $school_ground_floor["sign_fallen"] = False
  return
