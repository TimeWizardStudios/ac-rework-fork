init python:
  class Interactable_school_ground_floor_stairs(Interactable):

    def title(cls):
      return "First Floor"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs >= "steal":
        return "Rejection and staircases are the two leading causes of heartbreak."
      else:
        return "There's a lady who's sure, all that glitters is gold... but this is definitely not the stairway she bought."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Upstairs","goto_school_first_hall"])
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "vines":
            actions.append(["go","Upstairs","?school_ground_floor_stairs_flora_bonsai"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Upstairs","quest_mrsl_table_morning"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","Upstairs","?quest_flora_handcuffs_steal_stairs"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Upstairs","?quest_isabelle_dethroning_fly_to_the_moon_stairs"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume" and not quest.kate_wicked["own_costume"]:
            actions.append(["go","Upstairs","quest_kate_wicked_costume_entrance_hall_stairs"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "fly_to_the_moon":
            actions.append(["go","Upstairs","?quest_isabelle_dethroning_fly_to_the_moon_stairs"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Upstairs","goto_school_first_hall"])
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Upstairs","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.day1_take2 == "lindsey_fall" and quest.isabelle_tour == "go_upstairs" and quest.the_key.finished:
          actions.append(["go","Upstairs","quest_day1_take2_lindsey_fall"])
        elif quest.day1_take2.in_progress:
          actions.append(["go","Upstairs","?school_ground_floor_stairs_day1_take2"])
          return
        elif quest.isabelle_tour.in_progress:
          actions.append(["go","Upstairs","?school_ground_floor_stairs_isabelle_tour"])
          return
        if quest.lindsey_wrong == "fall":
          actions.append(["go","Upstairs","school_ground_floor_stairs_quest_lindsey_wrong_fall"])
      actions.append(["go","Upstairs","goto_school_first_hall"])


label school_ground_floor_stairs_flora_bonsai:
  "Maybe after [flora] has gotten her pads, I'll make her walk up the stairs. That should be entertaining."
  return

label school_ground_floor_stairs_day1_take2:
  "School is going to be an uphill battle. No point in starting early."
  return

label school_ground_floor_stairs_isabelle_tour:
  if quest.the_key.finished and not quest.isabelle_tour=="beginning":
      jump goto_school_first_hall
  else:
    if quest.isabelle_tour=="beginning":
      "Ditching [isabelle] would be a bad idea. It could lead to ostracization, social outrage, and possibly even detention!"
    if not quest.the_key.finished:
      "Hmm... still need to pick up that locker key at the [guard]'s booth."
    return
