init python:
  class Interactable_school_ground_floor_notice_board(Interactable):

    def title(cls):
      return "Bulletin Board"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.maxine_wine == "locker":
        return "This bulletin board completely lacks moderation... and that's the charm of it!"
      elif quest.isabelle_tour.started:
        return "Someone's selling unused baby shoes again. Probably one of those slutty cheerleaders."
      elif quest.day1_take2.started:
        return "The school's very own Craigslist. Any important information usually gets buried by the end of the first week."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.poolside_story == "petition":
          actions.append("?school_ground_floor_notice_board_poolside_story_petition")
        elif quest.poolside_story == "waitpet":
          actions.append("school_ground_floor_notice_board_poolside_story_isadec")
        if quest.maxine_wine == "locker":
          if mc.owned_item("flora_poster") and not quest.maxine_wine["entrance_hall_poster"]:
            actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:maxine_wine,flora_poster|Use What?","school_ground_floor_notice_board_maxine_wine_locker_use_item"])
          actions.append("?school_ground_floor_notice_board_maxine_wine_locker_interact")
        if quest.maya_sauce == "school":
          actions.append("quest_maya_sauce_school")
      if quest.isabelle_tour.in_progress:
        actions.append("?school_ground_floor_notice_board_interact_two")
      actions.append("?school_ground_floor_notice_board_interact_day1_take2")


label school_ground_floor_notice_board_interact_two:
  "{i}\"Make a real queen rule the prom this year. Vote for [kate]!\"{/}"
  "Oh, boy. They're starting early this year."
  return

label school_ground_floor_notice_board_interact_day1_take2:
  "{i}\"Welcome to Newfall High and the first day of the school year!\"{/}"
  "{i}\"Freshmen, sophomores, and juniors — please, gather on the field behind the school.\"{/}"
  "{i}\"Seniors, come to the homeroom.\"{/}"
  return

label school_ground_floor_notice_board_poolside_story_petition:
  "There's simply no way that people would sign a petition that I'm in charge of."
  "Better ask [isabelle] for help. She's a professional at this."
  return

label school_ground_floor_notice_board_poolside_story_isadec:
  if game.day > quest.poolside_story["isadecday"] or game.hour - quest.poolside_story["isadechour"] >=3:
    show misc petition4 with Dissolve(.5)
    "Oh, [maxine] put her signature down... that's odd."
    "She's not the type to care about these things..."
    "She probably has an ulterior motive for wanting the pool opened."
    "Very well. Time to change the world."
    #$mc.add_item("Pool Petition")
    $quest.poolside_story.advance("convince")
    hide misc petition4 with Dissolve(.5)
  elif game.hour - quest.poolside_story["isadechour"] ==2:
    show misc petition3 with Dissolve(.5)
    "Okay, now we're getting somewhere!"
    "Let's give it one more hour, then I'll bring it to [mrsl]."
    hide misc petition3 with Dissolve(.5)
  elif game.hour - quest.poolside_story["isadechour"] ==1:
    show misc petition2 with Dissolve(.5)
    "It looks like [kate] put her signature on it and then erased it."
    "Typical [kate]..."
    hide misc petition2 with Dissolve(.5)
  elif game.hour - quest.poolside_story["isadechour"] ==0:
    show misc petition1 with Dissolve(.5)
    "Hmm... I probably need to wait for more signatures."
    hide misc petition1 with Dissolve(.5)
  $quest.poolside_story["more_signatures"] = True
  return

label school_ground_floor_notice_board_maxine_wine_locker_interact:
  "Okay, let's see what the current gossip is..."
  "{i}\"To whoever stole my ball of yarn, I feed your kidneys to my rabbit. —[maya]\"{/}"
  "{i}\"The top floor is currently being renovated. Classes there are suspended until further notice. —Your principal\"{/}"
  "{i}\"Interested in buying fat candles.\"{/}"
  "The last one forgot to add a name."
  return

label school_ground_floor_notice_board_maxine_wine_locker_use_item(item):
  if item == "flora_poster":
    $quest.maxine_wine["entrance_hall_poster"] = True
    $mc.remove_item("flora_poster")
    "The board is full, but it's tradition to cover someone else's shit with your own..."
    "There. Perfection."
    if quest.maxine_wine["entrance_hall_poster"] + quest.maxine_wine["homeroom_poster"] + quest.maxine_wine["admin_wing_poster"] + quest.maxine_wine["1f_hall_poster"] + quest.maxine_wine["sports_wing_poster"] == 5:
      "Okay, that's all of them. Praise the lord Jesus hallelujah. My ass is sweating more than that hipster up on the cross."
      $quest.maxine_wine.advance("florareward")
  else:
    "It wouldn't be entirely outlandish to nail my [item.title_lower] to the board and watch a sudden outbreak of confusion."
    $quest.maxine_wine.failed_item("flora_poster",item)
  return
