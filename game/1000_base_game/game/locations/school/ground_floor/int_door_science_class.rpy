init python:
  class Interactable_school_ground_floor_science_class_door(Interactable):

    def title(cls):
      return "Science Classroom"

    def description(cls):
      if (quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif game.season > 1:
        return "The phrase \"doing it for science\" has more than one meaning here."

    def actions(cls,actions):
      if game.season > 1:
        if mc["focus"]:
          if mc["focus"] == "lindsey_angel":
            if quest.lindsey_angel in ("keycard","off_limits"):
              actions.append(["go","Science Classroom","?quest_lindsey_angel_keycard_school_ground_floor_science_class_door"])
              return
            elif quest.lindsey_angel == "phone_book":
              actions.append(["go","Science Classroom","?quest_lindsey_angel_phone_book_school_ground_floor_science_class_door"])
              return
          elif mc["focus"] == "lindsey_voluntary":
            if quest.lindsey_voluntary == "dream":
              actions.append(["go","Science Classroom","kate_blowjob_dream_random_interact_locked_location"])
          elif mc["focus"] == "mrsl_bot":
            if quest.mrsl_bot == "dream":
              actions.append(["go","Science Classroom","kate_blowjob_dream_random_interact_locked_location"])
        actions.append(["go","Science Classroom","goto_school_science_class"])
