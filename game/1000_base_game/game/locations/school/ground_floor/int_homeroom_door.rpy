init python:
  class Interactable_school_ground_floor_homeroom_door(Interactable):

    def title(cls):
      return "Homeroom"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs >= "steal":
        return "[mrsl]'s abode. Entry may cause sudden light-headedness and cramped pants."
      elif quest.isabelle_haggis.started:
        return "The Door of Dread. That's the official name. I once doused it with holy water and baptized it."
      elif quest.isabelle_tour.started:
        return "The stench of sulphur fills the air... or perhaps that's just my anxiety."
      elif quest.day1_take2.started:
        return "And behind door number one... Honestly, a goat wouldn't be so bad."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Homeroom","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "puzzle" and quest.isabelle_haggis.in_progress:
            actions.append(["go","Homeroom","?school_ground_floor_homeroom_door_interact_isabelle_haggis"])
            return
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "vines":
            actions.append(["go","Homeroom","?school_ground_floor_homeroom_door_flora_bonsai"])
            return
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("guard","mopforkate","cleanforkate"):
            actions.append(["go","Homeroom","?school_ground_floor_homeroom_door_lindsey_wrong_verywet"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Homeroom","quest_mrsl_table_morning"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","Homeroom","?quest_flora_handcuffs_steal_homeroom_door"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Homeroom","?quest_isabelle_dethroning_fly_to_the_moon_homeroom_door"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party") and not quest.kate_wicked["safer_somehow"]:
            actions.append(["go","Homeroom","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik"):
            actions.append(["go","Homeroom","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "fly_to_the_moon":
            actions.append(["go","Homeroom","?quest_isabelle_dethroning_fly_to_the_moon_homeroom_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Homeroom","goto_school_homeroom"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Homeroom","?quest_lindsey_angel_keycard_school_ground_floor_homeroom_door"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Homeroom","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Homeroom","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.day1_take2 == "entered":
          actions.append(["go","Homeroom","school_ground_floor_homeroom_door_interact_day1_take2"])
      actions.append(["go","Homeroom","goto_school_homeroom"])


label school_ground_floor_homeroom_door_flora_bonsai:
  "[flora] is counting on me. Maybe today is the day I meet her expectations and earn her eternal gratitude?"
  return

label school_ground_floor_homeroom_door_interact_day1_take2:
  $quest.day1_take2.advance("homeroom")
  "Okay, here goes nothing."
  jump goto_school_homeroom

label school_ground_floor_homeroom_door_interact_isabelle_haggis:
  "Yeah, this door is locked. [mrsl] wasn't kidding when she said we needed to solve the conflict to get out."
  "That also goes for getting back in. I need to find another way."
  return

label school_ground_floor_homeroom_door_lindsey_wrong_verywet:
  "This room doesn't get mopped by the janitor, so the odds of finding one is nil."
  return
