init python:
  class Interactable_school_ground_floor_lockers_back(Interactable):

    def title(cls):
      return "Secret Locker"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Somewhere in the chaotic time between freshman and senior year, you learn the owner of each locker.\n\n I've never seen anyone claim or open this one."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      else:
        if quest.lindsey_voluntary == "discovery":
          actions.append(["go","Open Locker","quest_lindsey_voluntary_discovery"])
      if school_ground_floor["secret_locker_found"]:
        actions.append(["go","Open Locker","goto_school_secret_locker"])
      else:
        actions.append(["go","Open Locker","school_ground_floor_lockers_back_interact_first_time"])
      #elif quest.isabelle_tour.started:
      #  actions.append("?school_ground_floor_lockers_back_interact_isabelle_tour")
      #else:
      #  actions.append("?school_ground_floor_lockers_back_interact")


label school_ground_floor_lockers_back_interact_isabelle_tour:
  "The key to a healthy secret life is... not in my possession right now."
  return

label school_ground_floor_lockers_back_interact_first_time:
  "Somebody left their locker open..."
  "Looks unused apart from the note and the key."
  "Maybe it's a love letter and the key to her heart... obviously, not meant for me."
  "Being nosy is one of my worst traits."
  "Okay, not really. My list of bad traits is very long."
  "Nobody's looking, though..."
  $school_ground_floor["secret_locker_found"] = True
  jump goto_school_secret_locker

label school_ground_floor_lockers_back_interact:
  "Nothing but disappointment here."
  return
