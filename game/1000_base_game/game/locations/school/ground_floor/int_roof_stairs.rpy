init python:
  class Interactable_school_ground_floor_roof_stairs(Interactable):

    def title(cls):
      return "Roof Landing"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs >= "steal":
        return "The stairs to the roof. The least exciting staircase in the school."
      else:
        return "The roof landing is where punks go to have a smoke, drink, or fuck. Super classy."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Roof Landing","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Roof Landing","quest_mrsl_table_morning"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","Roof Landing","?quest_flora_handcuffs_steal_roof_stairs"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Roof Landing","goto_school_roof_landing"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Roof Landing","kate_blowjob_dream_random_interact_locked_location"])
      actions.append(["go","Roof Landing","goto_school_roof_landing"])
