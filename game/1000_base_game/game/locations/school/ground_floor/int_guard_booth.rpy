init python:
  class Interactable_school_ground_floor_guard_booth(Interactable):

    def title(cls):
      return "[guard]'s Booth"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs == "steal":
        return "Empty, for now. Here's my chance."
      elif quest.isabelle_stolen.started:
        return "What a rotten fate. Locked behind a glass window, watching hoards of excited girls bounce by. Knowing he'll never have any of them."
      elif quest.day1_take2.started:
        return "Someone once asked for their locker key during the coffee break. Not making that mistake."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("doughnuts","mopforkate"):
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_wrong,guard_booth|Use What?","school_ground_floor_guard_booth_lindsey_wrong_doughnuts"])
          if quest.lindsey_wrong in ("mop","guard"):
            actions.append(["quest","Quest","?school_ground_floor_guard_booth_lindsey_wrong_mop"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append("quest_flora_handcuffs_steal")
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append("?quest_isabelle_dethroning_trap_locked")
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","revenge_done","fly_to_the_moon"):
            actions.append("?quest_isabelle_dethroning_trap_locked")
            return
          elif quest.isabelle_dethroning == "panik":
            actions.append("?quest_isabelle_dethroning_panik_entrance_hall_guard_booth")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append("?quest_lindsey_angel_keycard_school_ground_floor_guard_booth")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if game.season == 1:
          if quest.isabelle_tour.finished and quest.jacklyn_art_focus.finished and quest.jacklyn_statement.finished and not quest.jacklyn_sweets.started:
            actions.append(["quest","Quest","quest_jacklyn_sweets_start"])
        if quest.mrsl_number.in_progress and mc.owned_item("cup_of_coffee"):
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_stolen|Use What?","school_ground_floor_guard_booth_interact_mrsl_number"])
        if quest.isabelle_stolen == "breakin" and mc.owned_item("flash_drive"):
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_stolen,guard_booth|Use What?","school_ground_floor_guard_booth_interact_isabelle_stolen_flash_drive"])
        if quest.jacklyn_sweets == "feed":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jacklyn_sweets,guard_booth|Use What?","quest_jacklyn_sweets_feed"])
        if not quest.the_key.finished:
          actions.append("school_ground_floor_guard_booth_interact_the_key")
        if quest.isabelle_stolen == "noluck":
          actions.append("isabelle_quest_stolen_noluck")
        elif quest.isabelle_stolen =="guard":
          actions.append("isabelle_quest_stolen_guard")
        elif quest.isabelle_stolen == "nonuts":
          actions.append("isabelle_quest_stolen_entrance_guard_nonuts")
        elif quest.isabelle_stolen == "breakin":
          actions.append("?school_ground_floor_guard_booth_interact_isabelle_stolen")
        if quest.isabelle_hurricane == "guard_booth":
          actions.append("?quest_isabelle_hurricane_guard_booth")
        elif quest.isabelle_hurricane == "blowjob" and quest.isabelle_hurricane["cocksucker"]:
          actions.append("?quest_isabelle_hurricane_blowjob_guard_booth")
        if quest.lindsey_motive == "keys" and not quest.lindsey_motive["scram"]:
          actions.append("quest_lindsey_motive_keys_guard")
      if quest.day1_take2.finished:
        actions.append("?school_ground_floor_guard_booth_interact_day1_take2")
      if quest.isabelle_stolen.started:
        actions.append("?school_ground_floor_guard_booth_interact_two")
      actions.append("?school_ground_floor_guard_booth_interact_default")


label school_ground_floor_guard_booth_interact_default:
  "The [guard] seems to be asleep. "
  "I wonder if he's dreaming about big boobies or little boobies."
  return

label school_ground_floor_guard_booth_interact_day1_take2:
  "There's a smell of coffee and freshly baked pastries coming from inside."
  "Must be nice to have a job where your only real duty is locking up the school at night."
  return

label school_ground_floor_guard_booth_interact_the_key:
  mc "Hi, I'd like to pick up my locker key."
  guard "Did you get the permission slip from your homeroom teacher?"
  menu(side="middle"):
    extend ""
    "?quest.the_key.started@|{image=mrsl contact_icon}|\"Yes!\"":
      mc "Yes!"
      $mc.remove_item("permission_slip")
      guard "..."
      guard "Please, sign here."
      $mc.add_item("locker_key")
      $quest.the_key.finish()
    "\"No.\"":
      mc "No."
      guard "A senior who doesn't know how things work around here?"
      guard "I've got my eye on you."
  return

label school_ground_floor_guard_booth_interact_isabelle_stolen:
  "The [guard]'s booth is empty save for a few crumbs of aged doughnuts."
  "The [guard] left his post to embark on a journey for sustenance."
  "Here's my chance to get that surveillance footage. Just need to use my flash drive."
  return

label school_ground_floor_guard_booth_interact_isabelle_stolen_flash_drive(item):
  if item=="flash_drive" and quest.isabelle_stolen=="breakin":
    "While the [guard] is out, the cyber criminals dance on the table."
    "Downloading..."
    "Downloading..."
    "Come on..."
    "..."
    "Aha!"
    "The surveillance footage is all mine."
    "Oh, and I should probably delete the original file, so that he can't see that I went in here."
    "Bye, little file."
    $quest.isabelle_stolen.advance("footage")
  elif quest.mrsl_number.in_progress:
    call school_ground_floor_guard_booth_interact_mrsl_number(item)
  else:
    "Need to be quick about this, can't be sticking my dick or my [item.title_lower] into the security console."
    $quest.isabelle_stolen.failed_item("guard_booth",item)
  return

label school_ground_floor_guard_booth_interact_mrsl_number(item):
  if item=="cup_of_coffee":
    $mc.remove_item(item)
    $mc.add_item("coffee_cup")
    "The steam from the hot coffee slowly reveals a message on the glass."
    "It's a number... a phone number!"
    "Please, say it's hers..."
    $mc.add_phone_contact(mrsl,silent=False)
    $quest.mrsl_number.finish()
  else:
    "Not sure why I thought that putting my [item.title_lower] next to the booth window would reveal [mrsl]'s hidden message."
    "Probably need something steaming hot."
    $quest.mrsl_number.failed_item(item)
  return

label school_ground_floor_guard_booth_interact_two:
  guard "ZzzZzz."
  "The [guard] is out cold. Probably some sort of doughnut induced coma."
  return

label school_ground_floor_guard_booth_lindsey_wrong_mop:
  mc "Hey, I need a mop."
  guard "What do I look like, the janitor?"
  mc "No, she's pretty cute."
  guard "Great, more insults. Good luck finding a mop."
  mc "Well, I soaked the hall upstairs, and I know you'd rather have a cup of coffee and a doughnut than help clean up."
  guard "What are you suggesting, exactly?"
  mc "A deal. I'll get you a doughnut and you'll get me the mop from the janitor's closet."
  guard "Two doughnuts."
  mc "Done."
  guard "Very well."
  if quest.lindsey_wrong == "guard":
    $quest.lindsey_wrong.advance("mopforkate")
  else:
    $quest.lindsey_wrong.advance("doughnuts")
  return

label school_ground_floor_guard_booth_lindsey_wrong_doughnuts(item):
  if item == "doughnut":
    $doughnuts = mc.owned_item_count("doughnut")
    if doughnuts<2 and not quest.lindsey_wrong["first_doughnut"]:
        $mc.remove_item(item)
        guard "Where's the other one?"
        $quest.lindsey_wrong["first_doughnut"]=True
        return
    else:
      if quest.lindsey_wrong["first_doughnut"]:
        $mc.remove_item(item)
      else:
        $mc.remove_item(item,2)
      mc "Here you go, sir. The best doughnuts in all of Newfall!"
      guard "I'll be the judge of that."
      guard "Mmmm..."
      guard "Mmmm, yes... that hit the spot."
      mc "What about the mop?"
      guard "Ugh. Never any peace in this place..."
      guard "Fine. Wait here."
      "There he goes. Heavy in the step. Saggy in the posture. Unwanted by the general populace."
      "Like a garbage bag rolling on the side of the highway."
      guard "Okay, here's the damn mop. Don't bother me again."
      $mc.add_item("mop")
      if quest.lindsey_wrong == "mopforkate":
        $quest.lindsey_wrong.advance("cleanforkate")
      else:
        $quest.lindsey_wrong.advance("clean")
      return
  else:
    guard "What do you want me to do with this?"
    guard "This isn't some pawn shop."
    $quest.lindsey_wrong.failed_item("guard_booth",item)
  return
