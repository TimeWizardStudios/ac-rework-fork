init python:
  class Interactable_school_ground_floor_couch(Interactable):

    def title(cls):
      return "Couch"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Maybe the couch was here all along, hiding in plain sight?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_ground_floor_couch_interact_day1_take2")


label school_ground_floor_couch_interact_day1_take2:
  "This couch... where the hell did it come from?"
  "Last time around, there was no couch!"
  "..."
  "Who put it here, and why?"
  "Perhaps all the answers I seek are between the pillows?"
  "..."
  "Nope. Just dust and doughnut crumbs."
  "Yum!"
  return
