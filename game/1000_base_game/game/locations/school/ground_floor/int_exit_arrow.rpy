init python:
  class Interactable_school_ground_floor_exit_arrow(Interactable):

    def title(cls):
      return "Entrance"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return "Too late to go back now."
      elif quest.flora_handcuffs >= "steal":
        return "They say that love will make a way out of no way, but this is the way out for those with no love."
      elif quest.day1_take2.started:
        return "The only exit... and I'm all out of breadcrumbs."
      else:
        return "Outside... who'd ever know I'd yearn for it so."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Leave School","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "vines":
            actions.append(["go","Leave School","?school_ground_floor_exit_arrow_flora_bonsai"])
            return
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("verywet","guard","mopforkate","cleanforkate"):
            actions.append(["go","Leave School","?school_ground_floor_exit_arrow_interact_lindsey_wrong"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Leave School","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","Leave School","?quest_flora_handcuffs_steal_exit_arrow"])
            return
        elif mc["focus"] == "nurse_venting":
          if (quest.nurse_venting in ("vents","lost","puzzle")
          or (quest.nurse_venting == "bottle" and home_kitchen["water_bottle_taken"])):
            actions.append(["go","Leave School","?quest_nurse_venting_ground_floor_exit_arrow_go1"])
            return
          elif quest.nurse_venting in ("stuck","not_stuck"):
            actions.append(["go","Leave School","?quest_nurse_venting_ground_floor_exit_arrow_go2"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Leave School","?quest_isabelle_dethroning_fly_to_the_moon_leave_school"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Leave School","quest_kate_wicked_costume_entrance_hall_exit"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge"):
            actions.append(["go","Leave School","?quest_isabelle_dethroning_trap_leave_school"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","Leave School","?quest_isabelle_dethroning_revenge_done_leave_school"])
            return
          elif quest.isabelle_dethroning == "panik":
            if quest.isabelle_dethroning["gravity_of_the_situation"]:
              actions.append(["go","Leave School","quest_isabelle_dethroning_panik_outside"])
            else:
              actions.append(["go","Leave School","?quest_isabelle_dethroning_panik_leave_school"])
              return
          elif quest.isabelle_dethroning == "fly_to_the_moon":
            actions.append(["go","Leave School","?quest_isabelle_dethroning_fly_to_the_moon_leave_school"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Leave School","goto_school_entrance"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat"):
            actions.append(["go","Leave School","?quest_lindsey_angel_keycard_school_ground_floor_exit_arrow"])
            return
        elif mc["focus"] == "maya_spell":
          if quest.maya_spell in ("paint","statue"):
            actions.append(["go","Leave School","?quest_maya_spell_paint_exit_arrow"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Leave School","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Leave School","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
      else:
        if quest.day1_take2.in_progress:
          actions.append(["go","Leave School","?school_ground_floor_exit_arrow_interact_day1_take2"])
          return
        if ((quest.isabelle_tour.in_progress  and quest.isabelle_tour <= "gym")
        or quest.lindsey_nurse.in_progress):
          actions.append(["go","Leave School","?school_ground_floor_exit_arrow_lindesy_fetch_nurse"])
          return
        if quest.lindsey_motive in ("date_lindsey","date_mrsl"):
          actions.append(["go","Leave School","quest_lindsey_motive_date_lindsey_door"])
      actions.append(["go","Leave School","goto_school_entrance"])

    #def circle(cls,actions,x,y):
    #    rv =super().circle(actions,x,y)
    #    rv.start_angle=340
    #    rv.angle_per_icon= 40
    #    return rv


label school_ground_floor_exit_arrow_flora_bonsai:
  "Even though it would be funny to let [flora] run around blindly, girls with PMS should be handled with utmost care."
  return

label school_ground_floor_exit_arrow_interact_lindsey_wrong:
  "Escape is not an option. Only suffering will satisfy [kate]."
  return

label school_ground_floor_exit_arrow_interact_day1_take2:
  "The compulsion to escape eclipses my senses, and this is only the first day."
  return

label school_ground_floor_exit_arrow_lindesy_fetch_nurse:
  "Coward."
  "Go ahead. Run away from everything difficult like you always do."
  return
