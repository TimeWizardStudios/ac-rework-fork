init python:
  class Interactable_school_ground_floor_ball_of_yarn(Interactable):

    def title(cls):
      return "Pink Ball of Yarn"

    def description(cls):
      return "I have a yarnin' for you, pardner."

    def actions(cls,actions):
      actions.append(["take","Take","school_ground_floor_ball_of_yarn_take"])
      actions.append("?school_ground_floor_ball_of_yarn_interact")


label school_ground_floor_ball_of_yarn_interact:
  "Huh? I wonder what the [guard] would want with this..."
  "Maybe he's taken up knitting to kick his doughnut habit?"
  "..."
  "Doubtful. Once an addict, always an addict."
  return

label school_ground_floor_ball_of_yarn_take:
  if ("school_ground_floor_ball_of_yarn_interact",()) not in game.seen_actions:
    call school_ground_floor_ball_of_yarn_interact
    window hide
  $quest.jacklyn_romance["balls_of_yarn_found"]+=1
  $school_ground_floor["ball_of_yarn_taken"] = True
  $mc.add_item("ball_of_yarn_pink",silent=True)
  $game.notify_modal(None,"Inventory","{image=items ball_of_yarn_pink}{space=50}|Gained\n{color=#900}Pink Ball of Yarn{/color}\n("+str(quest.jacklyn_romance["balls_of_yarn_found"])+"/5)",5.0)
  return
