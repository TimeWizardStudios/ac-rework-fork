init python:
  class Location_school_ground_floor(Location):

    default_title = "Entrance Hall"

    def __init__(self):
      super().__init__()

    @property
    def cafeteria_open(self):
      return False
      #return game.dow in (0,1,2,3,4) and game.hour in (8,9,10,11,12,13,14)

    def scene(self,scene):
      ###########################Objects####################
      scene.append([(-12,-119),"school ground_floor background"])

      if game.season == 2 and not quest.lindsey_voluntary == "dream":
        scene.append([(881,426),"school ground_floor window_autumn"])
      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik","fly_to_the_moon")
      or quest.kate_stepping == "fly_to_the_moon"
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")
      or quest.maya_spell == "spell"
      or quest.lindsey_voluntary == "dream"
      or (quest.mrsl_bot == "meeting" and mc["focus"] == "mrsl_bot")):
        scene.append([(881,426),"school ground_floor window_night"])

      scene.append([(232,273),"school ground_floor computer_room","school_ground_floor_computer_room_door"])
      scene.append([(741,486),"school ground_floor main_stairs","school_ground_floor_stairs"])
      scene.append([(1656,273),"school ground_floor science_class","school_ground_floor_science_class_door"])

      #Staircases to the roof
      if quest.isabelle_tour < "cafeteria":
        scene.append([(558,175),"school ground_floor roof_stairs_left"])
      else:
        scene.append([(558,175),"school ground_floor roof_stairs_left",("school_ground_floor_roof_stairs",0,75)])
#     scene.append([(1208,175),"school ground_floor roof_stairs_right"])

      #Independence Day Banner
      if ((school_ground_floor["independence_day_banner"] and game.day >= 21 and game.season == 1)
      or quest.lindsey_voluntary == "dream"):
        scene.append([(269,297),"school ground_floor independence_day_pennants",("school_ground_floor_independence_day_decorations",-2,171)])
        scene.append([(874,148),"school ground_floor independence_day_banner",("school_ground_floor_independence_day_decorations",0,320)])

      #GFW Corridor
      scene.append([(450,484),"school ground_floor west_hall","school_ground_floor_gf_w_corridor"])
      if quest.flora_squid >= "trail" and quest.flora_squid.in_progress:
        scene.append([(218,692),"school ground_floor water_puddle"])

      #Pentagram
      if school_ground_floor["pentagram"] and quest.mrsl_bot.in_progress:
        scene.append([(258,734),"school ground_floor pentagram"])

      #Lockers
      scene.append([(583,556),"school ground_floor lockersback","school_ground_floor_lockers_back"])
      scene.append([(552,561),"school ground_floor lockersfront","school_ground_floor_lockers_front_right"])

      #Left Lockers
      scene.append([(324,551),"school ground_floor lockersleft"])
      if (quest.isabelle_buried >= "missinglocker" or quest.isabelle_buried.ended) and not quest.jo_washed.in_progress:
        scene.append([(406,546),"school ground_floor missing_locker","school_ground_floor_missing_locker"])
        scene.append([(402,585),"school ground_floor tape",("school_ground_floor_missing_locker",5,-39)])
        scene.append([(418,608),"school ground_floor permit",("school_ground_floor_missing_locker",1,-62)])

      scene.append([(1306,447),"school ground_floor guard_booth","school_ground_floor_guard_booth"])
      if quest.jacklyn_romance == "hide_and_seek" and not school_ground_floor["ball_of_yarn_taken"]:
        scene.append([(1391,619),"school ground_floor ball_of_yarn","school_ground_floor_ball_of_yarn"])

      #Cafeteria
      scene.append([(1140,513),"school ground_floor cafeteria"])
      if (quest.isabelle_dethroning not in ("trap","revenge")
      and not (quest.lindsey_voluntary == "dream" and not quest.lindsey_voluntary["lights_off"])):
        scene.append([(1171,545),"school ground_floor cafeteria_doors","school_ground_floor_cafeteria_doors"])

      scene.append([(1562,547),"school ground_floor janitor_door","school_ground_floor_janitor_door"])
      scene.append([(1459,547),"school ground_floor board","school_ground_floor_notice_board"])
      if game.season == 1 and not quest.jo_washed.in_progress:
        if quest.maxine_wine["entrance_hall_poster"]:
          scene.append([(1472,599),"school ground_floor flora_poster",("school_ground_floor_notice_board",21,-52)])
      scene.append([(483,551),"school ground_floor homeroom","school_ground_floor_homeroom_door"])

      #Crowd
      if quest.maya_sauce == "school":
        scene.append([(1436,566),"school ground_floor crowd"])
        # scene.append([(1441,561),"school ground_floor retinue"])

      #Spinach
      if not spinach.talking:
        if not school_ground_floor["exclusive"] or "spinach" in school_ground_floor["exclusive"]:
          if spinach.at("school_ground_floor","running"):
            if quest.kate_search_for_nurse["spinach_first_repeat"]:
              scene.append([(540,646),"school ground_floor spinach_left","spinach"])
            else:
              scene.append([(1385,713),"school ground_floor spinach_right","spinach"])

      #Beaver
      if quest.lindsey_book["beaver"] == 4:
        scene.append([(460,666),"school ground_floor beaver","school_ground_floor_beaver"])

      ###########################Characters####################

      #Lindsey
      if not lindsey.talking:
        if not school_ground_floor["exclusive"] or "lindsey" in school_ground_floor["exclusive"]:
          if lindsey.at("school_ground_floor","running"):
            scene.append([(568,573),"school ground_floor lindsey","lindsey"])
          elif lindsey.at("school_ground_floor","stairs"):
            scene.append([(959,407),"school ground_floor lindseystairs"])

      #Isabelle
      if not isabelle.talking:
        if isabelle.at("school_ground_floor","standing"):
          if not school_ground_floor["exclusive"] or "isabelle" in school_ground_floor["exclusive"]:
            if game.season == 1:
              scene.append([(1129,529),"school ground_floor isabelle","isabelle"])
            elif game.season == 2:
              scene.append([(1129,529),"school ground_floor isabelle_autumn","isabelle"])
              if isabelle.equipped_item("isabelle_collar"):
                scene.append([(1169,572),"school ground_floor isabelle_collar",("isabelle",-8,-43)])

      #MrsL
      if not mrsl.talking:
        if mrsl.at("school_ground_floor","standing"):
          if not school_ground_floor["exclusive"] or "mrsl" in school_ground_floor["exclusive"]:
            if game.season == 1:
              scene.append([(1337,581),"school ground_floor mrsl","mrsl"])
            elif game.season == 2:
              scene.append([(1337,581),"school ground_floor mrsl_autumn","mrsl"])

      #Guard
      if not guard.talking:
        if guard.at("school_ground_floor","standing"):
          if not school_ground_floor["exclusive"] or "guard" in school_ground_floor["exclusive"]:
            scene.append([(1706,582),"school ground_floor guard"])

      #Wet Floor Sign
      if ((game.season == 1 and not quest.jo_washed.in_progress)
      or quest.lindsey_voluntary == "dream"):
        if school_ground_floor["sign_fallen"]:
          scene.append([(412,730),"school ground_floor sign_down","school_ground_floor_wet_floor_sign_down"])
        else:
          scene.append([(512,680),"school ground_floor sign","school_ground_floor_wet_floor_sign_up"])

      #Dollar
      if school_ground_floor["dollar_spawned_today"] == True and not school_ground_floor["dollar_taken_today"] and not (quest.kate_blowjob_dream == "school" or quest.mrsl_table == "morning" or quest.jo_washed.in_progress or quest.lindsey_voluntary == "dream" or quest.mrsl_bot == "dream"):
        scene.append([(1076,975),"school ground_floor dollar","school_ground_floor_dollar"])

      #Couch
      if not quest.jo_washed.in_progress:
        scene.append([(693,691),"school ground_floor couch","school_ground_floor_couch"])

      #Maxine
      if not maxine.talking:
        if maxine.at("school_ground_floor","sploot"):
          if not school_ground_floor["exclusive"] or "maxine" in school_ground_floor["exclusive"]:
            if game.season == 1:
              scene.append([(839,654),"school ground_floor maxine",("maxine",0,39)])
            elif game.season == 2:
              scene.append([(839,654),"school ground_floor maxine_autumn","maxine"])

      #Maya
      if ((not school_ground_floor["exclusive"] or "maya" in school_ground_floor["exclusive"])
      and maya.at("school_ground_floor","sitting")
      and not maya.talking):
        scene.append([(999,708),"school ground_floor maya_autumn","maya"])

      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik","fly_to_the_moon")
      or quest.kate_stepping == "fly_to_the_moon"
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")
      or quest.maya_spell == "spell"
      or quest.lindsey_voluntary == "dream"
      or (quest.mrsl_bot == "meeting" and mc["focus"] == "mrsl_bot")):
        scene.append([(0,0),"#school ground_floor night_overlay"])

      if quest.isabelle_hurricane == "short_circuit" and quest.isabelle_hurricane["darkness"]:
        scene.append([(0,0),"black"])

      #Cafeteria Lights
      if (quest.isabelle_dethroning in ("trap","revenge")
      or (quest.lindsey_voluntary == "dream" and not quest.lindsey_voluntary["lights_off"])):
        scene.append([(1171,545),"school ground_floor cafeteria_doors_light",("school_ground_floor_cafeteria_doors",-26,0)])

      #Exit Arrow
      scene.append([(933,1000),"school ground_floor arrow_down","school_ground_floor_exit_arrow"])

    def find_path(self,target_location):
      if target_location=="school_locker":
        return "school_ground_floor_lockers_front_right"
      elif target_location=="school_secret_locker":
        return "school_ground_floor_lockers_back"
      elif target_location in ("school_homeroom","school_clubroom"):
        return "school_ground_floor_homeroom_door"
      elif target_location=="school_cafeteria":
        return "school_ground_floor_cafeteria_doors",dict(marker_offset=(60,30),marker_sector="mid_bottom")
      elif target_location in ("school_first_hall","school_english_class","school_art_class","school_music_class","school_first_hall_west","school_gym","school_bathroom","school_locker_room","school_first_hall_east"):
        return "school_ground_floor_stairs",dict(marker_offset=(220,0),marker_sector="mid_bottom")
      elif target_location in ("school_ground_floor_west","school_nurse_room","school_principal_office"):
        return "school_ground_floor_gf_w_corridor",dict(marker_offset=(50,50),marker_sector="right_mid") # Offset used to be (130,30)
      elif target_location == "school_computer_room":
        return "school_ground_floor_computer_room_door",dict(marker_offset=(-15,65),marker_sector="right_mid")
      elif target_location in ("school_roof_landing","school_roof"):
        return "school_ground_floor_roof_stairs",dict(marker_offset=(25,25))
      elif target_location == "school_science_class":
        return "school_ground_floor_science_class_door",dict(marker_offset=(-15,-5))
      else:
        return "school_ground_floor_exit_arrow",dict(marker_offset=(-10,-15),marker_sector="right_bottom")


label goto_school_ground_floor:
  if school_ground_floor.first_visit_today:
    $school_ground_floor["dollar_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_ground_floor)
  if quest.isabelle_dethroning not in ("revenge_done","panik","fly_to_the_moon") and quest.kate_stepping != "fly_to_the_moon":
    play music "school_theme" if_changed
  return
