init python:
  class Interactable_school_ground_floor_wet_floor_sign_up(Interactable):

    def title(cls):
      return "Wet Floor Sign"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "When I asked for a sign, this is not what I meant."
      elif quest.day1_take2.finished:
        return "Another trap set. Now to wait for the next girl to come along."
      elif quest.isabelle_tour.started:
        return "A wet girl sign would be more useful. Less guesswork involved."
      elif quest.day1_take2.started:
        return "This is the only thing that ever shows up when I pray for a sign."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if quest.day1_take2.finished:
        actions.append("?school_ground_floor_wet_floor_sign_interact_day1_take2_finished")
      if quest.isabelle_tour.started:
        actions.append("?school_ground_floor_wet_floor_sign_interact_isabelle_tour")
      if quest.day1_take2.started:
        actions.append("?school_ground_floor_wet_floor_sign_interact_day1_take2")
      if quest.isabelle_tour.finished:
        actions.append("?school_ground_floor_wet_floor_sign_interact")


label school_ground_floor_wet_floor_sign_interact_day1_take2:
  "It should say \"Caution: Ugly Reflection\" instead."
  "That's the real danger of wet floors."
  return

label school_ground_floor_wet_floor_sign_interact_isabelle_tour:
  "Has a wet floor ever caused someone to fall? I honestly doubt it."
  return

label school_ground_floor_wet_floor_sign_interact_day1_take2_finished:
  "Caution: Hazardous Sign."
  return

label school_ground_floor_wet_floor_sign_interact:
  "When I asked for wet pussies, this is not..."
  "Hold on, if I dip my dick in the soap, maybe it's not needed."
  return
