init python:
  class Interactable_school_ground_floor_cafeteria_doors(Interactable):

    def title(cls):
      return "Cafeteria"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_handcuffs >= "steal" and quest.nurse_aid["name_reveal"]:
        # return "Someone once drank five cups of Pepelepsi in protest. Had a seizure and ended up in the nurse's office.Sadly, he survived."
        return "Someone once drank five cups of Pepelepsi in protest. Had a seizure and ended up in the nurse's office.{space=-5}\n\nSadly, he survived."
      elif quest.flora_handcuffs >= "steal":
        # return "Someone once drank five cups of Pepelepsi in protest. Had a seizure and ended up in the [nurse]'s office.Sadly, he survived."
        return "Someone once drank five cups of Pepelepsi in protest. Had a seizure and ended up in the [nurse]'s office.{space=-5}\n\nSadly, he survived."
      else:
        return "Closed until noon. The strawberry juice has to wait."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Cafeteria","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "vines":
            actions.append(["go","Cafeteria","?school_ground_floor_cafeteria_doors_flora_bonsai"])
            return
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("guard","cleanforkate"):
            actions.append(["go","Cafeteria","?school_ground_floor_cafeteria_doors_lindsey_wrong_verywet"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Cafeteria","quest_mrsl_table_morning"])
        elif mc["focus"] == "flora_handcuffs":
          if quest.flora_handcuffs == "steal":
            actions.append(["go","Cafeteria","?quest_flora_handcuffs_steal_cafeteria_doors"])
            return
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "fly_to_the_moon":
            actions.append(["go","Cafeteria","?quest_isabelle_dethroning_fly_to_the_moon_cafeteria_doors"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Cafeteria","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge"):
            actions.append(["go","Cafeteria","?quest_isabelle_dethroning_trap_cafeteria_doors"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","Cafeteria","?quest_isabelle_dethroning_revenge_done_cafeteria_doors"])
            return
          elif quest.isabelle_dethroning == "panik":
            actions.append(["go","Cafeteria","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "fly_to_the_moon":
            actions.append(["go","Cafeteria","?quest_isabelle_dethroning_fly_to_the_moon_cafeteria_doors"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Cafeteria","goto_school_cafeteria"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Cafeteria","?quest_lindsey_angel_keycard_school_ground_floor_cafeteria_doors"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Cafeteria","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Cafeteria","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if (quest.day1_take2.in_progress
        or (quest.isabelle_tour.in_progress and quest.isabelle_tour < "cafeteria")):
          actions.append(["go","Cafeteria","?school_ground_floor_cafeteria_doors_interact_day1_take2"])
          return
        if quest.isabelle_dethroning == "announcement":
          actions.append(["go","Cafeteria","quest_isabelle_dethroning_announcement_cafeteria_doors"])
      actions.append(["go","Cafeteria","goto_school_cafeteria"])


label school_ground_floor_cafeteria_doors_flora_bonsai:
  "Finally, [flora] could do some blind tasting. Maybe of banana milk. She hates that."
  return

label school_ground_floor_cafeteria_doors_interact_day1_take2:
  "The food sucks, but the strawberry juice..."
  "It's like an orgasm in your mouth!"
  "I think..."
  return

label school_ground_floor_cafeteria_doors_lindsey_wrong_verywet:
  "Manual labor sure builds an appetite, but [kate] doesn't allow lunch breaks."
  return
