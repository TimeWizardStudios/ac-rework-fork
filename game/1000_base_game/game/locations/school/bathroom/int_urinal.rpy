init python:
  class Interactable_school_bathroom_urinal(Interactable):

    def title(cls):
      return "Urinal"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.flora_squid == "bathroom" or quest.lindsey_motive in ("steal","hide"):
        return "Inclusion has really come\na long way..."
      else:
        return "Urinal snorkeling is a new low. Really need that meeting, though..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_squid":
          if quest.flora_squid == "bathroom":
            actions.append("?quest_flora_squid_bathroom_urinal")
            return
        elif mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "steal":
            actions.append("?quest_lindsey_motive_steal_urinal")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.lindsey_motive == "hide":
          actions.append("?quest_lindsey_motive_steal_urinal")
          return
      actions.append("?school_bathroom_urinal_interact")


label school_bathroom_urinal_interact:
  "Please no pads or tampons..."
  "..."
  "Thank god. But no spider eggs either."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["urinal_searched"]:
      $school_bathroom["urinal_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
