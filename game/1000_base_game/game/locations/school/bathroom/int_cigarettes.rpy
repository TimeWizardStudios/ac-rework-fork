init python:
  class Interactable_school_bathroom_cigarettes(Interactable):

    def title(cls):
      return "Cigarettes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Long Dong Silver — for when you want to suck on something long and smoky."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_bathroom_cigarettes_interact")


label school_bathroom_cigarettes_interact:
  "Who the hell just leaves—"
  "Oh, it actually says, \"property of [maya]\" on the back."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["cigarettes_searched"]:
      $school_bathroom["cigarettes_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
