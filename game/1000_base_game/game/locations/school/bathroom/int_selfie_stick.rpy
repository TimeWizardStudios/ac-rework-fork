init python:
  class Interactable_school_bathroom_selfie_stick(Interactable):

    def title(cls):
      return "Selfie Stick"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A selfie stick. An integral part of any classy bathroom."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_bathroom_selfie_stick_interact")


label school_bathroom_selfie_stick_interact:
  "After we go to the bathroom, can we go investigate the time travel thing?"
  "I really need to figure out what's going on here."
  "But first..."
  "Let me take a selfie."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["selfie_stick_searched"]:
      $school_bathroom["selfie_stick_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
