init python:
  class Interactable_school_bathroom_fork(Interactable):
    def title(cls):
      return "Fork"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","school_bathroom_fork_take"])

label school_bathroom_fork_take:
  "There you are, you sneaky piece of cutlery!"
  "Trying to blend into the background, were you?"
  $school_bathroom["fork_taken"] = True
  $mc.add_item("plastic_fork")
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["fork_searched"]:
      $school_bathroom["fork_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
