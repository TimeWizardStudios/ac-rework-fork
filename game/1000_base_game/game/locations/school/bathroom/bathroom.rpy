init python:
  class Location_school_bathroom(Location):

    default_title = "Bathroom"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,0),"school bathroom bg"])
      scene.append([(1254,204),"school bathroom shower1",("school_bathroom_shower1",0,38)])
      scene.append([(1405,102),"school bathroom shower2",("school_bathroom_shower2",0,140)])
      scene.append([(1447,446),"school bathroom urinal",("school_bathroom_urinal",30,50)])
      scene.append([(1638,640),"school bathroom sink"])
      scene.append([(1274,340),"school bathroom selfie_stick","school_bathroom_selfie_stick"])
      scene.append([(351,98),"school bathroom stall_doors"])
      scene.append([(1003,275),"school bathroom door","school_bathroom_door"])
      if quest.maxine_eggs == "bathroom":
        scene.append([(1528,471),"school bathroom broken_snow_globe","school_bathroom_broken_snow_globe"])
      scene.append([(1545,938),"school bathroom shoes","school_bathroom_shoes"])
      #scene.append([(1658,680),"school bathroom soap"])
      #scene.append([(424,1004),"school bathroom condom"])
      #scene.append([(1373,749),"school bathroom face_shield"])
      scene.append([(845,982),"school bathroom cigarettes","school_bathroom_cigarettes"])
      #scene.append([(368,528),"school bathroom mask"])
      #scene.append([(1578,15),"school bathroom lizard"])
      #scene.append([(942,697),"school bathroom basket"])
      scene.append([(0,0),"school bathroom painting",("school_bathroom_painting",0,250)])
      if not school_bathroom["fork_taken"] or quest.jo_washed.in_progress:
        if quest.jo_washed.in_progress:
          scene.append([(44,289),"school bathroom fork",("school_bathroom_painting",-9,-39)])
        else:
          scene.append([(44,289),"school bathroom fork","school_bathroom_fork"])
      scene.append([(624,273),"school bathroom lockers"])
      if quest.maxine_eggs == "bathroom" or quest.lindsey_motive in ("steal","hide"):
        scene.append([(631,279),"school bathroom locker1","school_bathroom_locker1"])
        scene.append([(754,279),"school bathroom locker2","school_bathroom_locker2"])
        scene.append([(876,279),"school bathroom locker3","school_bathroom_locker3"])
      scene.append([(621,210),"school bathroom hot_air_dryer"])
      scene.append([(742,7),"school bathroom sinks_columns"])
      scene.append([(743,593),"school bathroom sinks","school_bathroom_sinks"])
      scene.append([(1525,217),"school bathroom mirror"])

      if quest.flora_squid == "bathroom":
        if not flora.talking:
          scene.append([(220,440),"school bathroom flora","flora"])
        scene.append([(772,805),"school bathroom water_bottle","school_bathroom_water_bottle"])

      #Night
      if ((game.hour >= 20 and not lindsey.talking)
      or quest.lindsey_motive in ("steal","hide")):
        scene.append([(0,0),"#school bathroom nightoverlay"])

    def find_path(self,target_location):
      return "school_bathroom_door"


label goto_school_bathroom:
  call goto_with_black(school_bathroom)
  return
