init python:
  class Interactable_school_bathroom_shower1(Interactable):

    def title(cls):
      return "Shower"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "What is locker room talk like for girls? Do they brag about sexual conquests and compare boobs?\n\nMaybe they just fawn over\nTanner, Brad, and Chad."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_squid":
          if quest.flora_squid == "bathroom":
            actions.append("?quest_flora_squid_bathroom_shower1")
            return
        elif mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "steal":
            actions.append("?quest_lindsey_motive_steal_shower1")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.lindsey_motive == "hide":
          actions.append("?quest_lindsey_motive_steal_shower1")
          return
      actions.append("?school_bathroom_shower1_interact")


label school_bathroom_shower1_interact:
  "Who doesn't like when girls get wet?"
  "..."
  "Spiders, apparently."
  "None here."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["shower1_searched"]:
      $school_bathroom["shower1_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return


init python:
  class Interactable_school_bathroom_shower2(Interactable):

    def title(cls):
      return "Shower"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.flora_squid == "bathroom"
      or quest.lindsey_motive in ("steal","hide")):
        return "For some reason, this shower is the setting of many wet fantasies."
      else:
        return "Nothing but mold and sadness and sad mold here."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_squid":
          if quest.flora_squid == "bathroom" and not quest.flora_squid["scrooge_mcduck"]:
            actions.append("quest_flora_squid_bathroom_shower2")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_bathroom_shower2_interact")


label school_bathroom_shower2_interact:
  "This place must be a marvel to behold after gym class."
  "Soap flowing freely, water splashing everywhere. Bodies slick and shiny.{space=-80}"
  "Two girls on a wild chase... one screaming and laughing... the other whipping a wet towel at the first..."
  "Okay, relax. I got no time for a cold shower right now."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["shower2_searched"]:
      $school_bathroom["shower2_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
