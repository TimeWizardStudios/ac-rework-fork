init python:
  class Interactable_school_bathroom_sinks(Interactable):

    def title(cls):
      return "Sinks"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "No matter how many times I wash my hands, I'll never be cleansed of my sins."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_bathroom_sinks_interact")


label school_bathroom_sinks_interact:
  "No one uses the left sink because it belongs to the school's resident ghost."
  "She was once the German chancellor before her career in adult entertainment."
  "Now she's only known by her stage name — Moaning Merkel."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["sinks_searched"]:
      $school_bathroom["sinks_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
