init python:
  class Interactable_school_bathroom_water_bottle(Interactable):

    def title(cls):
      return "[lindsey]'s Water Bottle"

    def description(cls):
      return "[lindsey]'s lips touch this daily. At this point, taking a sip would basically be kissing her."

    def actions(cls,actions):
      actions.append("?school_bathroom_water_bottle_interact")


label school_bathroom_water_bottle_interact:
  call quest_flora_squid_bathroom_water_bottle
  return
