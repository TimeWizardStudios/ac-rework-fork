init python:
  class Interactable_school_bathroom_locker1(Interactable):

    def title(cls):
      return "Locker"

    def description(cls):
      if quest.lindsey_motive in ("steal","hide"):
        return "Life's too short.\n\nLock your problems away.\n\nAvoid, avoid, avoid."
      else:
        return "This could very well be the wardrobe that leads to Narnia. Or it's just a regular locker. Probably like fifty-fifty."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "steal" and not school_bathroom["baseball_bat_taken"]:
            actions.append(["take","Take","quest_lindsey_motive_steal_locker1"])
      else:
        if not school_bathroom["baseball_bat_taken"]:
          if quest.lindsey_motive == "hide":
            actions.append(["take","Take","quest_lindsey_motive_steal_locker1"])
          else:
            actions.append(["take","Take","school_bathroom_locker1_take"])
      actions.append("?school_bathroom_locker1_interact")


label school_bathroom_locker1_take:
  "Any itsy bitsy spiders here?"
  "Hmm... if there are, they sure as hell aren't glowing."
  "This baseball bat could come in handy, though."
  "Especially if there's a sudden apocalypse of baseball-tossing zombies... or if something else needs smashing."
  $school_bathroom["baseball_bat_taken"] = True
  $mc.add_item("baseball_bat")
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["locker1_searched"]:
      $school_bathroom["locker1_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return

label school_bathroom_locker1_interact:
  "Not even a trace of cobwebs here. A+ janitor work."
  return


init python:
  class Interactable_school_bathroom_locker2(Interactable):

    def title(cls):
      return "Locker"

    def description(cls):
      if quest.lindsey_motive in ("steal","hide"):
        return "What's the point of putting your stuff in a locker without a lock?"
      else:
        return "Nobody has ever examined this locker so closely before."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "steal":
            actions.append("quest_lindsey_motive_steal_locker2")
      else:
        if quest.lindsey_motive == "hide":
          actions.append("?quest_lindsey_motive_steal_locker2")
          return
      actions.append("?school_bathroom_locker2_interact")


label school_bathroom_locker2_interact:
  "Any spiders here?"
  "Nope."
  if not school_bathroom["locker_looted"]:
    "Just this wallet..."
    menu(side="middle"):
      extend ""
      "Loot it":
        $mc.lust+=1
        "If they wanted the money, they wouldn't have left their wallet here."
        $mc.money+=100
      "Leave it be":
        $mc.love+=2
        "What's mine is mine. What's yours is yours."
    $school_bathroom["locker_looted"] = True
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["locker2_searched"]:
      $school_bathroom["locker2_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return


init python:
  class Interactable_school_bathroom_locker3(Interactable):

    def title(cls):
      return "Locker"

    def description(cls):
      if quest.lindsey_motive in ("steal","hide"):
        return "The [guard] was asked to install locks here three years ago.\n\nHe's still working on it."
      else:
        return "Every little scratch has a story to tell. Every dent is a mark of the past."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "steal":
            if quest.lindsey_motive["grave_mistake"]:
              actions.append("?quest_lindsey_motive_steal_locker3")
            else:
              actions.append(["take","Take","quest_lindsey_motive_steal_locker3"])
            return
      else:
        if quest.lindsey_motive == "hide":
          if quest.lindsey_motive["grave_mistake"]:
            actions.append("?quest_lindsey_motive_steal_locker3")
          else:
            actions.append(["take","Take","quest_lindsey_motive_steal_locker3"])
          return
      actions.append("?school_bathroom_locker3_interact")


label school_bathroom_locker3_interact:
  "This locker's history could date back to time immemorial."
  "It could be the very locker Queen Victoria II changed in before the Battle of Waterloo."
  "Or the locker that Marie Antoinette hid in during the French Revolution."
  "Who knows, really?"
  "Maybe it would've been smart to look up what spider eggs actually look like?"
  "Nothing here."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["locker3_searched"]:
      $school_bathroom["locker3_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
