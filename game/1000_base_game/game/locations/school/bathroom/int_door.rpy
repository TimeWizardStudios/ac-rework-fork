init python:
  class Interactable_school_bathroom_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "It looks so mundane from the inside... nothing mystical about it. Just a doorway like any other."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "maxine_eggs":
          if quest.maxine_eggs == "bathroom":
            actions.append(["go","East Hall","?quest_maxine_eggs_leave_bathroom"])
            return
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "bathroom":
            actions.append(["go","East Hall","?quest_flora_squid_bathroom_door"])
            return
        elif mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "steal":
            actions.append(["go","East Hall","?quest_lindsey_motive_steal_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","East Hall","goto_school_first_hall_east"])
      actions.append(["go","East Hall","goto_school_first_hall_east"])


label quest_maxine_eggs_leave_bathroom:
  "Let's be honest, there probably aren't any glowing spider eggs here."
  "Still a couple of places left to look in, though... Might as well be thorough."
  return
