init python:
  class Interactable_school_bathroom_broken_snow_globe(Interactable):
    def title(cls):
      return "Broken Snow Globe"
    def description(cls):
      return "Some hints of cobwebs in here, but no spider."
    def actions(cls,actions):
      actions.append("?school_bathroom_broken_snow_globe_interact")

label school_bathroom_broken_snow_globe_interact:
  "A broken snow globe with a tiny house."
  "If a spider ever lived here, it is now long gone."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["broken_snow_globe_searched"]:
      $school_bathroom["broken_snow_globe_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
