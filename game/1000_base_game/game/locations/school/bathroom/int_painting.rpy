init python:
  class Interactable_school_bathroom_painting(Interactable):

    def title(cls):
      return "Painting"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Impressionism meets modern art. The result is only suitable for a bathroom wall."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_bathroom_painting_interact")


label school_bathroom_painting_interact:
  "Art is a window into the artist's mind."
  "In this case, a {i}very{/} disturbed mind."
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["painting_searched"]:
      $school_bathroom["painting_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
