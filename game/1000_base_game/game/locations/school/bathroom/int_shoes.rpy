init python:
  class Interactable_school_bathroom_shoes(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Why are Chad's shoes in the women's bathroom?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_bathroom_shoes_interact")


label school_bathroom_shoes_interact:
  mc "Bleh!"
  "Gross. Sniffing them probably wasn't the best idea."
  "..."
  "One more time?"
  if quest.maxine_eggs == "bathroom":
    if not school_bathroom["shoes_searched"]:
      $school_bathroom["shoes_searched"] = True
      $quest.maxine_eggs["bathroom_interactables"] += 1
    if quest.maxine_eggs["bathroom_interactables"] == 6:
      jump quest_maxine_eggs_bathroom_encounter
  return
