init python:
  class Interactable_school_park_exit_arrow(Interactable):

    def title(cls):
      return "School Entrance"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The road back. Negative connotation."

    def actions(cls,actions):
      actions.append(["go","Return","goto_school_entrance"])
