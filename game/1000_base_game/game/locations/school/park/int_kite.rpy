init python:
  class Interactable_school_park_kite(Interactable):

    def title(cls):
      return "Kites"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Flying a kite, or running a kite?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_kite_interact")


label school_park_kite_interact:
  "Stuck in a tree top, these kites fly at the whim of the wind."
  "That's not a metaphor."
  return
