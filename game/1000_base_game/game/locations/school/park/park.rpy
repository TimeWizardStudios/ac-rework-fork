init python:
  class Location_school_park(Location):

    default_title = "Park"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      if (quest.lindsey_angel == "hospital"
      or (quest.maxine_dive in ("line","cave") and game.hour == 19)
      or quest.isabelle_stars == "stargaze"):
        scene.append([(-469,-56),"school park sky_night"])
      elif game.season == 1:
        scene.append([(0,-5),"school park sky"])
      elif game.season == 2:
        scene.append([(-137,-90),"school park sky_autumn"])
      if game.season == 1:
        scene.append([(0,275),"school park back_grass",("school_park_road_to_newfall",77,-19)])
        scene.append([(155,106),"school park back_trees",("school_park_road_to_newfall",0,150)])
        scene.append([(0,514),"school park grass"])
      elif game.season == 2:
        scene.append([(0,275),"school park back_grass_autumn",("school_park_road_to_newfall",77,-19)])
        scene.append([(155,106),"school park back_trees_autumn",("school_park_road_to_newfall",0,150)])
        scene.append([(0,514),"school park grass_autumn"])
      scene.append([(1218,64),"school park kite",("school_park_kite",0,300)])
      scene.append([(528,543),"school park no_swimming_sign","school_park_no_swimming_sign"])
      if game.season == 1:
        scene.append([(493,233),"school park left_tree",("school_park_left_tree",0,23)])
      elif game.season == 2:
        scene.append([(493,233),"school park left_tree_autumn",("school_park_left_tree",0,23)])
      scene.append([(746,602),"school park left_bench","school_park_bench"])
      scene.append([(0,704),"school park pond","school_park_pond"])

      if game.season == 2:
        scene.append([(964,578),"#school park back_leaves_autumn"])

      ## Cuthbert
      if school_park["cuthbert"]:
        scene.append([(749,758),"school park cuthbert"])

      ## Diving bell
      if quest.maxine_hook.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and quest.maya_spell.finished and not quest.maxine_dive.finished:
        if school_park["diving_bell"]:
          scene.append([(1066,342),"school park open_box"])
          scene.append([(1111,384),"school park diving_bell","school_park_diving_bell"])
        else:
          scene.append([(1066,342),"school park unopened_box"])

      ## Telescope
      if quest.isabelle_stars == "stargaze" and school_park["telescope"]:
        scene.append([(1532,459),"school park telescope"])

      ## Blanket
      if quest.isabelle_stars == "stargaze" and school_park["blanket"]:
        scene.append([(1444,692),"school park blanket"])

      if game.season == 1:
        scene.append([(850,779),"school park left_grass"])
        scene.append([(1000,826),"school park left_bush","school_park_left_bush"])
      elif game.season == 2:
        scene.append([(850,779),"school park left_grass_autumn"])
        scene.append([(1000,826),"school park left_bush_autumn","school_park_left_bush"])
      scene.append([(1390,666),"school park right_bench","school_park_bench"])
      if game.season == 1:
        scene.append([(1651,919),"school park right_bush"])
        scene.append([(0,0),"school park front_tree"])
      elif game.season == 2:
        scene.append([(1651,919),"school park right_bush_autumn"])
        scene.append([(0,0),"school park front_tree_autumn"])
      scene.append([(319,526),"school park butterflies","school_park_butterflies"])
      scene.append([(232,0),"school park swing"])
      if game.season == 1:
        scene.append([(1784,583),"school park front_bush"])
      elif game.season == 2:
        scene.append([(1784,583),"school park front_bush_autumn"])

      if game.season == 2:
        scene.append([(0,586),"#school park front_leaves_autumn"])

      ## Flora
      if ((not school_park["exclusive"] or "flora" in school_park["exclusive"])
      and flora.at("school_park","standing")
      and not flora.talking):
        if game.season == 1:
          if flora.equipped_item("flora_skirt"):
            scene.append([(1134,446),"school first_hall_east flora_skirt","flora"])
          else:
            scene.append([(1134,446),"school first_hall_east flora","flora"])
        elif game.season == 2:
          scene.append([(1066,455),"school park flora_autumn","flora"])

      ## Maxine
      if ((not school_park["exclusive"] or "maxine" in school_park["exclusive"])
      and maxine.at("school_park","standing")
      and not maxine.talking):
        scene.append([(859,445),"school park maxine_autumn","maxine"])
      elif ((not school_park["exclusive"] or "maxine" in school_park["exclusive"])
      and maxine.at("school_park","armor")
      and not maxine.talking):
        scene.append([(856,471),"school park maxine_armor","maxine"])

      ## Nurse
      if ((not school_park["exclusive"] or "nurse" in school_park["exclusive"])
      and nurse.at("school_park","standing")
      and not nurse.talking):
        scene.append([(1235,441),"school park nurse_hiking_gear","nurse"])

      scene.append([(1609,970),"school park football","school_park_football"])

      if (quest.lindsey_angel == "hospital"
      or (quest.maxine_dive in ("line","cave") and game.hour == 19)
      or quest.isabelle_stars == "stargaze"):
        scene.append([(0,0),"#school park overlay_night"])
        if quest.maxine_dive in ("line","cave") and game.hour == 19:
          scene.append([(1066,342),"#school park open_box_night"])
      else:
        scene.append([(0,-25),"#school park overlay"])
      scene.append([(904,921),"school park exit_arrow","school_park_exit_arrow"])

      ## Bonfire
      if quest.isabelle_stars == "stargaze":
        if school_park["small_pile_of_wood"]:
          scene.append([(1717,686),"school park small_bonfire"])
        elif school_park["medium_pile_of_wood"]:
          scene.append([(1717,638),"school park medium_bonfire"])
        elif school_park["big_pile_of_wood"]:
          scene.append([(1703,568),"school park big_bonfire"])
        scene.append([(1390,666),"school park right_bench_night"])

    def find_path(self,target_location):
      if target_location in ("marina","hospital"):
        return "school_park_road_to_newfall", dict(marker_offset=(1025,275))
      else:
        return "school_park_exit_arrow"


label goto_school_park:
  call goto_with_black(school_park)
  return
