init python:
  class Interactable_school_park_pond(Interactable):

    def title(cls):
      return "Pond"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Wetter than [isabelle] at\nan antiquarium."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_pond_interact")


label school_park_pond_interact:
  "This pond is deeper than it looks..."
  "There's an urban legend that it leads to an underground cave system."
  return
