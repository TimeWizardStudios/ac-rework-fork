init python:
  class Location_school_mushroom_spot(Location):

    default_title = "Mushroom Spot"

    def scene(self,scene):
      scene.append([(0,0),"school park mushroom_spot sky"])
      scene.append([(0,481),"school park mushroom_spot mountains"])
      scene.append([(0,607),"school park mushroom_spot paths"])
      scene.append([(0,0),"school park mushroom_spot trees"])
      scene.append([(0,436),"school park mushroom_spot bushes"])
      scene.append([(206,755),"school park mushroom_spot rocks"])
      scene.append([(37,667),"school park mushroom_spot mushrooms"])
      scene.append([(0,0),"school park mushroom_spot overlay"])


label goto_school_mushroom_spot:
  call goto_with_black(school_mushroom_spot)
  return
