init python:
  class Interactable_school_park_bench(Interactable):

    def title(cls):
      return "Park Bench"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A seat for some, a bed for others."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_bench_interact")


label school_park_bench_interact:
  "I always assumed I'd end up on one of these..."
  return
