init python:
  class Interactable_school_park_left_tree(Interactable):

    def title(cls):
      return "Tree"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "[kate]'s tree."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_left_tree_interact")


label school_park_left_tree_interact:
  "This one was stolen from the Newfall Botanic Garden and planted here."
  return
