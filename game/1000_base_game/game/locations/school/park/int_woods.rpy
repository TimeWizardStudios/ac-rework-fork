init python:
  class Location_school_woods(Location):

    default_title = "Woods"

    def scene(self,scene):
      scene.append([(0,0),"school park woods sky"])
      scene.append([(0,0),"school park woods trees_back"])
      scene.append([(437,358),"school park woods birds"])
      scene.append([(0,432),"school park woods path"])
      scene.append([(0,0),"school park woods trees_front"])


label goto_school_woods:
  call goto_with_black(school_woods)
  return
