init python:
  class Interactable_school_park_butterflies(Interactable):

    def title(cls):
      return "Butterflies"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Beautiful without even knowing it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_butterflies_interact")


label school_park_butterflies_interact:
  "They say that happiness is a butterfly..."
  "Impossible to catch without breaking its wings."
  return
