init python:
  class Interactable_school_park_road_to_newfall(Interactable):

    def title(cls):
      return "Road to Newfall"

    def description(cls):
      if game.season > 1:
        if quest.jo_washed.in_progress:
          return random.choice(quest_jo_washed_investigate_lines)
        else:
          return "Bliss is for those that don't look beneath the surface."

    def actions(cls,actions):
      if game.season > 1:
        if mc["focus"]:
          if mc["focus"] == "jo_washed":
            if quest.jo_washed.in_progress:
              actions.append(random.choice(quest_jo_washed_interact_lines))
        if game.season > 1:
          actions.append(["go","Newfall Marina","goto_marina"])
        else:
          actions.append("?school_park_road_to_newfall_interact")


label school_park_road_to_newfall_interact:
  "Beyond this park lies the land of the rich..."
  "A gated community of toothpick tycoons and pickle kingpins."
  return
