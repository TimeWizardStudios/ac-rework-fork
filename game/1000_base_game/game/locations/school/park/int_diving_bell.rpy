init python:
  class Interactable_school_park_diving_bell(Interactable):

    def title(cls):
      return "Diving Bell"

    def description(cls):
      # return "Diving bell or dumbbell? Both descriptions fit."
      return "Diving bell or dumbbell?\nBoth descriptions fit."

    def actions(cls,actions):
      actions.append("?school_park_diving_bell_interact")


label school_park_diving_bell_interact:
  "I'm not exactly an expert on diving gear, but this one is a ringer."
  return
