init python:
  class Interactable_school_park_no_swimming_sign(Interactable):

    def title(cls):
      return "\"No Swimming\" Sign"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "No swimming, swinging,\nor crawling allowed."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_no_swimming_sign_interact")


label school_park_no_swimming_sign_interact:
  "This is the best way of getting students to lemming train into the water. Reversed psychosis."
  return
