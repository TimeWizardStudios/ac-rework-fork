init python:
  class Location_school_cave(Location):

    default_title = "Cave"

    def scene(self,scene):
      scene.append([(0,0),"school park cave background"])

      ## Glowing Rune Door ##
      scene.append([(854,229),"school park cave glowing_rune_door","school_cave_glowing_rune_door"])

      ## Ship Wheel ##
      scene.append([(1574,482),"school park cave ship_wheel","school_cave_ship_wheel"])

      scene.append([(556,106),"school park cave columns"])

      ## Crown ##
      scene.append([(798,506),"school park cave crown","school_cave_crown"])

      ## Ancient Chests ##
      scene.append([(1152,443),"school park cave ancient_chests","school_cave_ancient_chests"])

      ## Portrait ##
      scene.append([(1207,363),"school park cave portrait","school_cave_portrait"])

      scene.append([(1164,446),"school park cave coins"])

      ## Ancient Book ##
      scene.append([(1105,617),"school park cave ancient_book","school_cave_ancient_book"])

      ## Maxine ##
      if ((not school_cave["exclusive"] or "maxine" in school_cave["exclusive"])
      and maxine.at("school_cave","standing")
      and not maxine.talking):
        scene.append([(413,352),"school park cave maxine","maxine"])

      ## Locked Box ##
      if not school_cave["locked_box_taken"]:
        scene.append([(491,832),"school park cave locked_box","school_cave_locked_box"])

      scene.append([(1368,680),"school park cave ship_propellor"])
      scene.append([(14,0),"school park cave foreground"])

      ## Ancient Chastity Belt ##
      if not school_cave["ancient_chastity_belt_taken"]:
        scene.append([(304,800),"school park cave ancient_chastity_belt","school_cave_ancient_chastity_belt"])


label goto_school_cave:
  call goto_with_black(school_cave)
  return


init python:
  class Interactable_school_cave_glowing_rune_door(Interactable):

    def title(cls):
      return "Glowing Rune Door"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        # return "Knock, knock, knockin'... on this random-ass door."
        return "Knock, knock, knockin'...\n\n...on this random-ass door."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        actions.append("?school_cave_glowing_rune_door_interact")


label school_cave_glowing_rune_door_interact:
  if "glowing_rune_door" in quest.maxine_dive["cave_interactables"]:
    "Imagine if these runes are the link to a certain Dick Nastley song..."
    "That's everything I aspire to be, in terms of trolling."
  else:
    "{i}\"Speak dirty and enter...\"{/}"
    "Heh. That's what the runes to [maya]'s opening would say."
    "..."
    "I have no idea what's actually written here, though."
    "And no idea how to get inside of either opening."
    mc "What do you make of this door, [maxine]?"
    window hide
    show maxine sad_map with Dissolve(.5)
    window auto
    maxine sad_map "Nothing. It appears to be unmovable."
    mc "What? Really?"
    mc "What about the hieroglyphs or whatever?"
    maxine thinking_map "Those are runes, and I would have to look up the type."
    mc "How do they glow like that?"
    maxine skeptical "It seems to be some sort of bio-or-not-bio-luminescence."
    mc "Can you get us through?"
    # maxine skeptical "Those blocks weigh more than a car, and I don't think I have the upper body strength for that."
    maxine skeptical "Those blocks weigh more than a car, and I don't think I have the upper{space=-50}\nbody strength for that."
    mc "Err, fair enough."
    mc "Any idea what's on the other side of the door?"
    maxine sad_map "The map doesn't say."
    mc "Welp. Great."
    window hide
    hide maxine with Dissolve(.5)
    if ("school_cave_glowing_rune_door_interact",()) in game.seen_actions:
      $game.seen_actions.remove(("school_cave_glowing_rune_door_interact",()))
    $quest.maxine_dive["cave_interactables"].add("glowing_rune_door")
  return


init python:
  class Interactable_school_cave_ancient_chastity_belt(Interactable):

    def title(cls):
      return "Ancient Chastity Belt"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        return "Here's the real booty."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        if "ancient_chastity_belt" in quest.maxine_dive["cave_interactables"]:
          actions.append(["take","Take","school_cave_ancient_chastity_belt_take"])
        actions.append("?school_cave_ancient_chastity_belt_interact")


label school_cave_ancient_chastity_belt_interact:
  # "It looks like treasure isn't the only thing that's been locked up for ages..."
  "It looks like treasure isn't the only thing that's been locked up\nfor ages..."
  "I bet medieval chicks were horny as fuck."
  $quest.maxine_dive["cave_interactables"].add("ancient_chastity_belt")
  return

label school_cave_ancient_chastity_belt_take:
  "This could be useful one day..."
  window hide
  $school_cave["ancient_chastity_belt_taken"] = True
  $mc.add_item("ancient_chastity_belt")
  return


init python:
  class Interactable_school_cave_crown(Interactable):

    def title(cls):
      return "Crown"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        # return "A crown of some sort. It looks expensive."
        return "A crown of some sort.\nIt looks expensive."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        actions.append("?school_cave_crown_interact")


label school_cave_crown_interact:
  "It's not exactly what I'd find in my grandmother's jewelry box..."
  "...probably because she was a peasant."
  $quest.maxine_dive["cave_interactables"].add("crown")
  return


init python:
  class Interactable_school_cave_ancient_chests(Interactable):

    def title(cls):
      return "Ancient Chests"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        # return "Ancient chests, but I don't discriminate."
        return "Ancient chests,\nbut I don't discriminate."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        actions.append("?school_cave_ancient_chests_interact")


label school_cave_ancient_chests_interact:
  "Whatever is inside these chests smells a bit odd."
  "Like... molding cloth and all sorts of I-don't-want-to-open them."
  $quest.maxine_dive["cave_interactables"].add("ancient_chests")
  return


init python:
  class Interactable_school_cave_ancient_book(Interactable):

    def title(cls):
      return "Ancient Book"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        # return "Look at me exploring this cave and finding books! I'm a tome reader."
        return "Look at me exploring this\ncave and finding books!\n\nI'm a tome reader."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        actions.append("?school_cave_ancient_book_interact")


label school_cave_ancient_book_interact:
  "{i}\"Booby Traps and How to Properly Trap Booty: A Survival Guide\" by Danny Boonslayer.{/}"
  $quest.maxine_dive["cave_interactables"].add("ancient_book")
  return


init python:
  class Interactable_school_cave_portrait(Interactable):

    def title(cls):
      return "Portrait"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        # return "His royal majesty of the underworld."
        return "His royal majesty of\nthe underworld."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        actions.append("?school_cave_portrait_interact")


label school_cave_portrait_interact:
  if "portrait" in quest.maxine_dive["cave_interactables"]:
    "I'm not sure what [maxine] sees in this guy... apart from being a total 12th century Chad."
  else:
    "Majestic... and pretty much everything every man wants to be..."
    mc "What do you make of this portrait, [maxine]?"
    window hide
    show maxine concerned with Dissolve(.5)
    window auto
    maxine concerned "..."
    maxine concerned "I don't know..."
    "Huh. She seems confused and sad at the same time."
    mc "Who even is this king, anyway?"
    maxine concerned "No idea... I have no idea..."
    "Damn. That's probably the first time I've ever heard her say those words."
    "She just keeps staring at the painting as if it holds all the answers to the universe."
    mc "Well, then..."
    window hide
    hide maxine with Dissolve(.5)
    if ("school_cave_portrait_interact",()) in game.seen_actions:
      $game.seen_actions.remove(("school_cave_portrait_interact",()))
    $quest.maxine_dive["cave_interactables"].add("portrait")
  return


init python:
  class Interactable_school_cave_ship_wheel(Interactable):

    def title(cls):
      return "Ship Wheel"

    def description(cls):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        # return "An old ship's wheel, seemingly in decent condition."
        return "An old ship's wheel,\nseemingly in decent condition."

    def actions(cls,actions):
      if "maxine" in quest.maxine_dive["cave_interactables"]:
        actions.append("?school_cave_ship_wheel_interact")


label school_cave_ship_wheel_interact:
  "How the hell did this end up in a pond...?"
  $quest.maxine_dive["cave_interactables"].add("ship_wheel")
  return


init python:
  class Interactable_school_cave_locked_box(Interactable):

    def title(cls):
      return "Locked Box"

    def description(cls):
      if all(interactable in quest.maxine_dive["cave_interactables"] for interactable in ("ancient_book","ancient_chastity_belt","ancient_chests","crown","glowing_rune_door","maxine","portrait","ship_wheel")):
        # return "I'm getting some serious déjà vu..."
        return "I'm getting some serious\ndéjà vu..."

    def actions(cls,actions):
      if all(interactable in quest.maxine_dive["cave_interactables"] for interactable in ("ancient_book","ancient_chastity_belt","ancient_chests","crown","glowing_rune_door","maxine","portrait","ship_wheel")):
        actions.append(["take","Take","school_cave_locked_box_take"])


label school_cave_locked_box_take:
  "Hang on a second..."
  "This looks just like the box from the forest glade."
  "How the hell did it end up in the bottom of the pond?"
  window hide
  $school_cave["locked_box_taken"] = True
  $mc.add_item("locked_box_cave")
  pause 0.25
  window auto
  jump quest_maxine_dive_cave_upon_exploring
