init python:
  class Interactable_school_park_football(Interactable):

    def title(cls):
      return "Football"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Airhead-cocks chasing an egg filled with air. Almost poetic."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_park_football_interact")


label school_park_football_interact:
  "This is clearly a trap."
  "There's a jock hiding in a bush somewhere, just waiting to tackle anyone who touches it."
  return
