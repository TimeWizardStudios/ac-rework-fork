init python:
  class Interactable_school_cafeteria_cooking_pot(Interactable):

    def title(cls):
      return "Cooking Pot"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.flora_cooking_chilli == "cooking_pot":
        return "Aptly nicknamed Big Bertha, this behemoth can be operated both as a cooking pot and a muzzle-loaded mortar platform."
      elif quest.maxine_dive.started:
        # return "Why are potholes called the way they are?"
        return "Why are potholes called\nthe way they are?"
      else:
        return "I love BBCs — Big, bulky, cooking pots."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.flora_cooking_chilli == "cooking_pot":
          actions.append(["take","Take","school_cafeteria_cooking_pot_take_flora_cooking_chilli"])
        if quest.maya_spell == "container":
          actions.append(["take","Take","quest_maya_spell_container"])
        if quest.maxine_dive == "shield":
          actions.append(["take","Take","quest_maxine_dive_shield"])
      actions.append("?school_cafeteria_cooking_pot_interact")


label school_cafeteria_cooking_pot_interact:
  "{i}\"If a man does not have the sauce, then he is lost. But the same man can be lost in the sauce.\"{/}"
  "They must have been talking about bolognese."
  return

label school_cafeteria_cooking_pot_take_flora_cooking_chilli:
  mc "Hello, mind if I borrow a big pot?"
  mc "..."
  "No answer. They're too busy brewing their witch broths."
  "Oh well, they probably won't miss it."
  $mc.add_item("cooking_pot")
  $school_cafeteria["cooking_pot_taken"]=True
  $ quest.flora_cooking_chilli.advance("bring_pot")
  return
