init python:
  class Location_school_cafeteria(Location):

    default_title="Cafeteria"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      scene.append([(1662,203),"school cafeteria security",("school_cafeteria_exit",-50,50)])
      scene.append([(0,0),"school cafeteria wall"])

      #Night
      if (quest.isabelle_dethroning == "dinner" and game.hour == 19) or quest.isabelle_dethroning in ("trap","revenge_done") or quest.isabelle_dethroning["kitchen_shenanigans_replay"]:
        scene.append([(0,197),"school cafeteria night_windows"])

      scene.append([(1396,270),"school cafeteria door"])
      if quest.isabelle_dethroning["kitchen_shenanigans"] and not quest.isabelle_dethroning["dinner_picture"]:
        scene.append([(436,219),"school cafeteria kitchen_wall",("school_cafeteria_kitchen",0,32)])
        scene.append([(416,310),"school cafeteria kitchen",("school_cafeteria_kitchen",36,-60)])
      else:
        scene.append([(416,310),"school cafeteria kitchen"])
      scene.append([(1380,498),"school cafeteria counterright"])
      scene.append([(1423,432),"school cafeteria coffee","school_cafeteria_coffee"])
      scene.append([(1491,429),"school cafeteria bread"])
      scene.append([(1449,473),"school cafeteria plate"])
      scene.append([(1483,523),"school cafeteria salad"])

      #Drink Machine
      scene.append([(1563,420),"school cafeteria drink","school_cafeteria_drink_machine"])

      if school_cafeteria["fire"]:
        scene.append([(1692,456),"school cafeteria fire"])
      elif school_cafeteria["fire_spread"]:
        scene.append([(1565,370),"school cafeteria fire_spread"])
      if school_cafeteria["burns"]:
        scene.append([(1642,444),"school cafeteria burns",("school_cafeteria_drink_machine",-17,-24)])
      if school_cafeteria["smoke_this_scene"]:
        scene.append([(1532,279),"#school cafeteria smoke"])

      scene.append([(434,454),"school cafeteria counterleft","school_cafeteria_counter_left"])
      scene.append([(499,410),"school cafeteria table"])

      #Pastries
      scene.append([(433,364),"school cafeteria pastries","school_cafeteria_pastries"])
      if quest.isabelle_stolen in ("outnuts", "nonuts"):
        scene.append([(435,404),"school cafeteria no_donuts"])

      #Maya
      if maya.at("school_cafeteria","kicking"):
        if not maya.talking:
          if not school_cafeteria["exclusive"] or "maya" in school_cafeteria["exclusive"]:
            scene.append([(1028,386),"school cafeteria maya_autumn", "maya"])

      #Vending Machines
      scene.append([(882,317),"school cafeteria vendingback","school_cafeteria_vending_back"])
      scene.append([(164,268),"school cafeteria vendingleft","school_cafeteria_vending_left"])

      #Isabelle in her party dress
      if isabelle.at("school_cafeteria","dinner"):
        if not isabelle.talking:
          if not school_cafeteria["exclusive"] or "isabelle" in school_cafeteria["exclusive"]:
            scene.append([(1365,348),"school cafeteria isabelle_dinner","isabelle"])

      #Furniture
      scene.append([(798,446),"school cafeteria chair_h2"])
      scene.append([(691,467),"school cafeteria chair_h1"])
      scene.append([(755,479),"school cafeteria table_h"])
      scene.append([(720,482),"school cafeteria chair_q2"])
      scene.append([(572,516),"school cafeteria chair_q1"])
      scene.append([(663,533),"school cafeteria table_q"])
      scene.append([(617,538),"school cafeteria chair_f1"])
      scene.append([(394,583),"school cafeteria chair_f2"])
      scene.append([(530,608),"school cafeteria table_f"])
      scene.append([(451,632),"school cafeteria chair_e2"])
      scene.append([(136,703),"school cafeteria chair_e1"])
      scene.append([(329,739),"school cafeteria table_e"])
      scene.append([(1089,467),"school cafeteria chair_d2"])
      scene.append([(1012,479),"school cafeteria table_d"])
      scene.append([(1062,480),"school cafeteria chair_d1"])
      scene.append([(1154,516),"school cafeteria chair_c2"])
      scene.append([(1041,533),"school cafeteria table_c"])
      scene.append([(1108,535),"school cafeteria chair_c1"])
      scene.append([(1239,583),"school cafeteria chair_b1"])
      scene.append([(1079,608),"school cafeteria table_b"])
      scene.append([(1173,607),"school cafeteria chair_b2"])
      scene.append([(1398,703),"school cafeteria chair_a2"])
      scene.append([(1153,739),"school cafeteria table_a"])
      scene.append([(1280,729),"school cafeteria chair_a1"])

      #Cooking Pot
      if ((game.season == 1 and not school_cafeteria["cooking_pot_taken"])
      or quest.jo_washed.in_progress
      or (quest.maya_spell == "container" and not quest.flora_cooking_chilli.actually_finished)
      or (quest.maxine_dive == "shield" and (not quest.flora_cooking_chilli.actually_finished or quest.maya_spell.finished))):
          scene.append([(517,350),"school cafeteria pot","school_cafeteria_cooking_pot"])

      #Guard
      if guard.at("school_cafeteria","standing"):
        if not guard.talking:
          if not school_cafeteria["exclusive"] or "guard" in school_cafeteria["exclusive"]:
            scene.append([(511,334),"school cafeteria guard","guard"])

      #Nurse
      if nurse.at("school_cafeteria","standing"):
        if not nurse.talking:
          if not school_cafeteria["exclusive"] or "nurse" in school_cafeteria["exclusive"]:
            if game.season == 1:
              scene.append([(473,348),"school cafeteria nurse","nurse"])
            elif game.season == 2:
              scene.append([(475,344),"school cafeteria nurse_autumn","nurse"])

      #Jo
      if jo.at("school_cafeteria","coffee"):
        if not jo.talking:
          if not school_cafeteria["exclusive"] or "jo" in school_cafeteria["exclusive"]:
            if game.season == 1:
              scene.append([(1389,325),"school cafeteria jo","jo"])
            elif game.season == 2:
              scene.append([(1391,329),"school cafeteria jo_autumn","jo"])

      #Kate
      if kate.at("school_cafeteria","sitting"):
        if not kate.talking:
          if not school_cafeteria["exclusive"] or "kate" in school_cafeteria["exclusive"]:
            if game.season == 1:
              scene.append([(1248,586),"school cafeteria kate","kate"])
            elif game.season == 2:
              scene.append([(1261,590),"school cafeteria kate_autumn","kate"])

      #Isabelle
      if isabelle.at("school_cafeteria","sitting"):
        if not isabelle.talking:
          if not school_cafeteria["exclusive"] or "isabelle" in school_cafeteria["exclusive"]:
            if game.season == 1:
              scene.append([(1115,420),"school cafeteria isabelle","isabelle"])
            elif game.season == 2:
              scene.append([(1122,424),"school cafeteria isabelle_autumn","isabelle"])
              if isabelle.equipped_item("isabelle_collar"):
                scene.append([(1165,466),"school cafeteria isabelle_collar",("isabelle",-10,-42)])

      #Flora
      if flora.at("school_cafeteria","eating"):
        if not flora.talking:
          if not school_cafeteria["exclusive"] or "flora" in school_cafeteria["exclusive"]:
            if game.season == 1:
              if flora.equipped_item("flora_skirt"):
                scene.append([(463,502),"school cafeteria flora_skirt","flora"])
              else:
                scene.append([(463,502),"school cafeteria flora","flora"])
            elif game.season == 2:
              scene.append([(474,509),"school cafeteria flora_autumn","flora"])

      #Lindsey
      if lindsey.at("school_cafeteria","walking"):
        if not lindsey.talking:
          if not school_cafeteria["exclusive"] or "lindsey" in school_cafeteria["exclusive"]:
            scene.append([(879,354),"school cafeteria lindsey", "lindsey"])
            if school_cafeteria["mmm_doughnuts"] == True:
              scene.append([(930,0),"school cafeteria mmm_doughnuts",("lindsey",6,354)])

      #Maxine
      if maxine.at("school_cafeteria","sitting"):
        if not maxine.talking:
          if not school_cafeteria["exclusive"] or "maxine" in school_cafeteria["exclusive"]:
            scene.append([(1234,551),"school cafeteria maxine","maxine"])
      elif maxine.at("school_cafeteria","newspaper"):
        if not maxine.talking:
          if not school_cafeteria["exclusive"] or "maxine" in school_cafeteria["exclusive"]:
            scene.append([(798,417),"school cafeteria maxine_newspaper","maxine"])

      #Spinach
      if spinach.at("school_cafeteria","sitting"):
        if not spinach.talking:
          if not school_cafeteria["exclusive"] or "spinach" in school_cafeteria["exclusive"]:
            scene.append([(1122,554),"school cafeteria spinach",("spinach",-35,0)])

      #Jacklyn
      if jacklyn.at("school_cafeteria","sitting"):
        if not jacklyn.talking:
          if not school_cafeteria["exclusive"] or "jacklyn" in school_cafeteria["exclusive"]:
            if game.season == 1:
              scene.append([(196,543),"school cafeteria jacklyn","jacklyn"])
            elif game.season == 2:
              scene.append([(196,543),"school cafeteria jacklyn_autumn","jacklyn"])

      if not quest.jo_washed.in_progress:
        if school_cafeteria["dollar1_spawned_today"] == True and not school_cafeteria["dollar1_taken_today"]:
          scene.append([(1497,482),"school cafeteria dollar1","school_cafeteria_dollar1"])
        if school_cafeteria["dollar2_spawned_today"] == True and not school_cafeteria["dollar2_taken_today"]:
          scene.append([(375,484),"school cafeteria dollar2","school_cafeteria_dollar2"])

      #Independence Day Decorations
      if (quest.isabelle_dethroning == "dinner" and game.hour == 19) or quest.isabelle_dethroning in ("trap","revenge_done") or quest.isabelle_dethroning["kitchen_shenanigans_replay"]:
        scene.append([(0,0),"#school cafeteria independence_day_banners"])
        scene.append([(21,372),"#school cafeteria independence_day_balloons"])

      #Night
      if quest.isabelle_dethroning == "revenge_done" and quest.isabelle_dethroning["fly_to_the_moon"]:
        scene.append([(0,0),"#school cafeteria night_overlay"])

    def find_path(self,target_location):
      return "school_cafeteria_exit",dict(marker_offset=(130,60),marker_sector="right_bottom")


label goto_school_cafeteria:
  if school_cafeteria.first_visit_today:
    $school_cafeteria["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_cafeteria["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  if lindsey.at("school_cafeteria","walking"):
    $school_cafeteria["mmm_doughnuts"] = renpy.random.randint(1,20)
  call goto_with_black(school_cafeteria)
  return
