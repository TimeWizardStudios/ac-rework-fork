image strawberry_juice_small = LiveComposite((60,60),(-3,-9),Transform("items bottle strawberry_juice",zoom=0.85))
image spray_strawberry_juice_small = LiveComposite((60,60),(-3,-19),Transform("items bottle spray_strawberry_juice",zoom=0.75))
image banana_milk_small = LiveComposite((60,60),(-3,-9),Transform("items bottle banana_milk",zoom=0.85))
image spray_banana_milk_small = LiveComposite((60,60),(-3,-19),Transform("items bottle spray_banana_milk",zoom=0.75))
image seven_hp_small = LiveComposite((60,60),(-3,-9),Transform("items bottle seven_hp",zoom=0.85))
image spray_seven_hp_small = LiveComposite((60,60),(-3,-19),Transform("items bottle spray_seven_hp",zoom=0.75))
image salted_cola_small = LiveComposite((60,60),(-3,-9),Transform("items bottle salted_cola",zoom=0.85))
image spray_salted_cola_small = LiveComposite((60,60),(-3,-19),Transform("items bottle spray_salted_cola",zoom=0.75))
image pepelepsi_small = LiveComposite((60,60),(-3,-9),Transform("items bottle pepelepsi",zoom=0.85))
image spray_pepelepsi_small = LiveComposite((60,60),(-3,-19),Transform("items bottle spray_pepelepsi",zoom=0.75))


init python:
  class Interactable_school_cafeteria_drink_machine(Interactable):

    def title(cls):
      return "Soda Machine"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "All the flavors of the rainbow.\n\nThe rainbow over a toxic lake next to an industrial waste dump."

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:act_one,soda_machine|Use What?","school_cafeteria_drink_machine_use_item"])
      if mc["focus"]:
        if mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19 and "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
            actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_soda_fountain"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.remove(["use_item","Use item","select_inventory_item","$quest_item_filter:act_one,soda_machine|Use What?","school_cafeteria_drink_machine_use_item"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_cafeteria_drink_machine_interact")


label school_cafeteria_drink_machine_interact:
  "This thing would be more interesting if it worked more like an actual fountain, spraying soda into the ceiling..."
  return

label school_cafeteria_drink_machine_use_item(item):
  if item in ("empty_bottle","spray_empty_bottle"):
    if mc.money >= 1:
      $mc.remove_item(item)
      "All right... the choice is simple, but..."
      menu(side="middle"):
        extend ""
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=spray_strawberry_juice_small}|{vspace=5}Strawberry Juice" if item == "spray_empty_bottle":
          jump school_cafeteria_drink_machine_use_item_strawberry_juice
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=strawberry_juice_small}|{vspace=5}Strawberry Juice" if item == "empty_bottle":
          label school_cafeteria_drink_machine_use_item_strawberry_juice:
            "Yes! At long last!"
            $school_cafeteria["drink_choice"] = "spray_strawberry_juice" if item == "spray_empty_bottle" else "strawberry_juice"
            if quest.maxine_hook == "juice":
              jump quest_maxine_hook_juice
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=spray_banana_milk_small}|{vspace=5}Banana Milk" if item == "spray_empty_bottle":
          jump school_cafeteria_drink_machine_use_item_banana_milk
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=banana_milk_small}|{vspace=5}Banana Milk" if item == "empty_bottle":
          label school_cafeteria_drink_machine_use_item_banana_milk:
            "The weird smell and sickly yellow color put most people off."
            "The milky banana taste takes care of the rest."
            $school_cafeteria["drink_choice"] = "spray_banana_milk" if item == "spray_empty_bottle" else "banana_milk"
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=spray_seven_hp_small}|{vspace=5}Seven HP" if item == "spray_empty_bottle":
          jump school_cafeteria_drink_machine_use_item_seven_hp
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=seven_hp_small}|{vspace=5}Seven HP" if item == "empty_bottle":
          label school_cafeteria_drink_machine_use_item_seven_hp:
            "The jocks always drink this crap. Tastes like watered-down Sprite."
            $school_cafeteria["drink_choice"] = "spray_seven_hp" if item == "spray_empty_bottle" else "seven_hp"
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=spray_salted_cola_small}|{vspace=5}Salted Cola" if item == "spray_empty_bottle":
          jump school_cafeteria_drink_machine_use_item_salted_cola
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=salted_cola_small}|{vspace=5}Salted Cola" if item == "empty_bottle":
          label school_cafeteria_drink_machine_use_item_salted_cola:
            "Tastes like cold semen. [mrsl] is the only person who likes this\nfoul liquid."
            $school_cafeteria["drink_choice"] = "spray_salted_cola" if item == "spray_empty_bottle" else "salted_cola"
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=spray_pepelepsi_small}|{vspace=5}Pepelepsi" if item == "spray_empty_bottle":
          jump school_cafeteria_drink_machine_use_item_pepelepsi
        "[mc.money]/1|{image=ui hud icon_money}|{space=20}{image=pepelepsi_small}|{vspace=5}Pepelepsi" if item == "empty_bottle":
          label school_cafeteria_drink_machine_use_item_pepelepsi:
            "There was an article in the school paper about this drink once — said it contained traces of amphetamine."
            "Not sure if that was ever confirmed..."
            $school_cafeteria["drink_choice"] = "spray_pepelepsi" if item == "spray_empty_bottle" else "pepelepsi"
      $mc.money-=1
      $mc.add_item(school_cafeteria["drink_choice"])
    else:
      "Right, this thing isn't free. Damn it."
  elif item in ("strawberry_juice","spray_strawberry_juice","banana_milk","spray_banana_milk","seven_hp","spray_seven_hp","salted_cola","spray_salted_cola","pepelepsi","spray_pepelepsi"):
    "If I want to refill this bottle with a different liquid, I should probably empty it first..."
  else:
    "While pouring soda on my [item.title_lower] seems like a good idea in theory, it's a bit less effective in practice."
    $quest.act_one.failed_item("soda_machine",item)
  return
