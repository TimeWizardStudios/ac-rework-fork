init python:
  class Interactable_school_cafeteria_exit(Interactable):

    def title(cls):
      return "Exit"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The [guard] looks at everyone with disinterest... except for me.\n\nHatred."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "maxine_hook":
          if quest.maxine_hook == "interrogation":
            actions.append(["go","Entrance Hall","?quest_maxine_hook_interrogation_school_cafeteria_exit"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19:
            actions.append(["go","Entrance Hall","?quest_isabelle_dethroning_dinner_exit"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Entrance Hall","goto_school_ground_floor"])
      actions.append(["go","Entrance Hall","goto_school_ground_floor"])
