init python:
  class Interactable_school_cafeteria_coffee(Interactable):

    def title(cls):
      return "Coffee Machine"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Steaming hot, just like [mrsl]'s... well, all of her, really."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if mc.owned_item("coffee_cup"):
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:act_one,refill_coffee|Use What?","school_cafeteria_coffee_use_item"])
      actions.append("?school_cafeteria_coffee_interact")


label school_cafeteria_coffee_use_item(item):
  if item == "coffee_cup":
    $mc.remove_item("coffee_cup")
    $mc.add_item("cup_of_coffee")
    "Mmm... a nice cup of bean juice."
  else:
    "Ouch! The school coffee is way too hot for my [item.title_lower]."
    $quest.act_one.failed_item("refill_coffee",item)
  return

label school_cafeteria_coffee_interact:
  "The coffee here sucks, but at least it's hot and free. A deadly combination."
  if not school_cafeteria["got_coffee"]:
    $mc.add_item("cup_of_coffee")
    "Mmm... a nice cup of bean juice."
    $school_cafeteria["got_coffee"] = True
  return
