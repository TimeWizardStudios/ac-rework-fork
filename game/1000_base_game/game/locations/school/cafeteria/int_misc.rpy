init python:
  class Interactable_school_cafeteria_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_cafeteria_dollar1_take"])


label school_cafeteria_dollar1_take:
  $school_cafeteria["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_cafeteria_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_cafeteria_dollar2_take"])


label school_cafeteria_dollar2_take:
  $school_cafeteria["dollar2_taken_today"] = True
  $mc.money+=20
  return
