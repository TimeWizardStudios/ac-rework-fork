init python:
  class Interactable_school_cafeteria_counter_left(Interactable):

    def title(cls):
      return "Food Counter"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.started:
        return "The only worse cook than these ladies is probably [flora], but at least she'd make the meals more interesting."
      else:
        return "They say that too many cooks spoil the broth... but despite running the kitchen solo, the lunch lady here always seems to get it wrong."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis == "escape" and not mc.owned_item("haggis_lunchbox"):
            actions.append(["take","Take","school_cafeteria_counter_left_isabelle_haggis_seconds"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("verywet","guard","mopforkate","cleanforkate"):
            actions.append("school_cafeteria_counter_left_cleanforkate")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if game.season == 1:
          if quest.isabelle_tour >= "cafeteria" and not quest.isabelle_haggis.started:
            actions.append(["quest","Quest","school_cafeteria_counter_left_isabelle_haggis_start"])
        #elif quest.jacklyn_statement == "ingredients":
        #  actions.append("school_cafeteria_counter_left_jacklyn_statement_ingredients")
        if quest.jacklyn_statement.in_progress:
          actions.append("school_cafeteria_counter_left_jacklyn_statement_ingredients")
        else:
          actions.append("?school_cafeteria_counter_left_jacklyn_statement_ingredients")
      if quest.isabelle_tour.started:
         actions.append("?school_cafeteria_counter_left_interact_two")
      actions.append("?school_cafeteria_counter_left_interact")

      #if quest.kate_blowjob_dream == "school":
      #  actions.append("kate_blowjob_dream_random_interact")
      #elif quest.isabelle_tour.started:
      #  if quest.isabelle_haggis.started:
      #    if quest.isabelle_haggis=="escape" and not mc.owned_item("haggis_lunchbox") and quest.isabelle_haggis.in_progress:
      #      actions.append("school_cafeteria_counter_left_isabelle_haggis_seconds") #seconds
      #    else:
      #      actions.append("school_cafeteria_counter_left_interact") #post-seconds/post-haggis
      #  else:
      #    actions.append("school_cafeteria_counter_left_isabelle_haggis_start") #start haggis
      #else:
      #  actions.append("school_cafeteria_counter_left_interact") #pre-isabelle_tour


label school_cafeteria_counter_left_interact_two:
  "Entering the kitchen is against school regulations."
  "They're worried unruly students will mess with the food."
  "To be honest, it would probably make it better."
  return

label school_cafeteria_counter_left_interact:
  "Haggis? No thanks."
  return

label school_cafeteria_counter_left_isabelle_haggis_start:
  "Think I'll skip lunch today. There's haggis on the menu."
  "On second thought... [isabelle] says she loves Scotland, and this is their national dish..."
  "Would be fun to watch her squirm."
  $mc.add_item("haggis_lunchbox")
  $quest.isabelle_haggis.start()
  return

label school_cafeteria_counter_left_isabelle_haggis_seconds:
  mc "Hey, do you guys have any haggis left?"
  mc "..."
  "No one's answering, but there's still a big pot left. Not entirely surprising."
  $mc.add_item("haggis_lunchbox")
  return

label school_cafeteria_counter_left_cleanforkate:
  "I can't eat on an anxious stomach."
  return

label school_cafeteria_counter_left_jacklyn_statement_ingredients:
  if mc.owned_item("garlic"):
    "A heist only works once. Everyone knows that."
  else:
    mc "Hello? Can I have some garlic, please?"
    "They're all busy stirring their pots."
    menu(side="middle"):
      extend ""
      "Commit larceny":
        if school_cafeteria["garlic_count"] >= 2:
          "I'm just going to pocket this garlic and no one will—"
          $mc.add_item("garlic")
          show jo angry at appear_from_right
          jo angry "Caught red-handed!"
          "Damn. Was she hiding inside behind the leek?"
          jo skeptical "Well, well, well... if it isn't the garlic thief himself!"
          mc "I just steal to survive and help others. I'm the Robin Hood of vegetables."
          jo laughing "Okay... since it's just one garlic this time, I'll let it slide."
          mc "Really?"
          jo angry "No! I'm cutting your TV cable."
          "Oh well. I've seen The Gimpsons a million times before."
          show jo angry at disappear_to_right
          pause(1)
        elif school_cafeteria["garlic_count"] >= 1:
          "What do you say after you ate the stolen garlic?"
          "You hold up your bowl and say, \"Please, ma'am, can I have some more garlic?\""
          "And then it just miraculously appears in your hand."
          $mc.add_item("garlic")
          show jo skeptical at appear_from_right
          jo skeptical "Ah-ha!"
          "Shit."
          jo neutral "What do you have to say for yourself?"
          mc "I, err... got hungry."
          jo neutral "I've told you not to enter the school kitchen."
          jo neutral "I'm revoking your video game privileges for a week."
          mc "That is just cruel and unusual!"
          mc "It's like taking water away from a fish..."
          jo displeased "I don't want to hear it."
          show jo displeased at disappear_to_right
          pause(1)
        else:
          "It takes a special kind of ninja to break into the kitchen unseen."
          "Luckily for me..."
          if mc.strength >= 5:
            "I've spent many nights honing the craft of stealth and trickery."
            "This is nothing compared to the creaky stairs at home."
            $mc.add_item("garlic")
          else:
            show jo neutral at appear_from_right
            jo neutral "What do you think you're doing?"
            mc "Err... I dropped my garlic."
            jo skeptical "We have a strict sanitary policy here, you know that."
            jo skeptical "Students aren't allowed in the kitchen."
            mc "But I dropped my garlic and it rolled into the kitchen!"
            jo annoyed "Sorry, [mc], but rules are rules. I'm going to have to give you detention."
            $mc["detention"]+=1
            "God damn it. Well, at least I got the garlic."
            show jo annoyed at disappear_to_right
            $mc.add_item("garlic")
        $school_cafeteria["garlic_count"] +=1
      "Wait for better times":
        pass
  return
