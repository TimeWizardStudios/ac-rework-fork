init python:
  class Interactable_school_cafeteria_kitchen(Interactable):

    def title(cls):
      return "Kitchen"

    def actions(cls,actions):
      actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_kitchen"])
