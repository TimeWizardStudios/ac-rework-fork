init python:
  class Interactable_school_cafeteria_pastries(Interactable):

    def title(cls):
      return "Pastries"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.started and quest.nurse_aid["name_reveal"]:
        # return "The two biggest consumers are the [guard] and [amelia]. Together, they make up for over seventy percent of all purchases."
        return "The two biggest consumers\nare the [guard] and [amelia].\n\nTogether, they make up for over seventy percent of all purchases."
      elif quest.isabelle_tour.started:
        # return "The two biggest consumers are the [guard] and the [nurse]. Together, they make up for over seventy percent of all purchases."
        return "The two biggest consumers\nare the [guard] and the [nurse].\n\nTogether, they make up for over seventy percent of all purchases."
      else:
        return "No one ever gets this excited about my cream frosting."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("doughnuts","mopforkate"):
            actions.append("lindsey_wrong_cafeteria_donut")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_stolen == "donuts":
          actions.append("isabelle_quest_stolen_cafeteria_donut")
        if quest.jacklyn_sweets == "doughnuts":
          actions.append("quest_jacklyn_sweets_doughnuts")
        elif (quest.jacklyn_sweets == "feed" and mc.owned_item_count("doughnut") < 5) or (quest.jacklyn_sweets == "paint" and mc.owned_item_count("doughnut") < 3):
          actions.append("lindsey_wrong_cafeteria_donut")
      if quest.isabelle_tour.started:
        actions.append("?school_cafeteria_pastries_interact")
      actions.append("?school_cafeteria_pastries_interact_two")


label school_cafeteria_pastries_interact_two:
  "As an unofficial rule, the doughnuts belong to the [guard]. Few dare be seen munching on one in his presence."
  return

label school_cafeteria_pastries_interact:
  "This goes straight to my thighs."
  "This is the only thing that goes straight to my thighs."
  return

label lindsey_wrong_cafeteria_donut:
  menu(side="middle"):
    "?mc.money>=2@[mc.money]/2|{image=ui hud icon_money}|Buy a doughnut":
      $mc.money-=2
      $mc.add_item("doughnut")
      "For this price, it better be good."
    "Outrageous price!":
      pass
  return
