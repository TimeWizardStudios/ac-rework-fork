init python:
  class Interactable_school_cafeteria_vending_back(Interactable):

    def title(cls):
      return "Vending Machine"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.started:
        return "Who thought it would be a good idea to drop a carbonated soda a few feet."
      elif quest.flora_cooking_chilli.started:
        return "Once saw a jock put fifty bucks into this machine before realizing it's broken. You clearly don't need brains to play football or be popular."
      else:
        return "Put a coin in, get nothing in return. Friend-zoned by the vending machine."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19 and "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
            actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_vending_machine"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if mc.owned_item("full_letter"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:act_one,vending_machine_letter|Use What?","vending_machine_letter_use"])
      if not school_cafeteria["vending_interacted"]:
        actions.append("school_cafeteria_vending_back_interact_first_time")
      if quest.flora_cooking_chilli.started:
        actions.append("?school_cafeteria_vending_back_interact_flora_cooking_chilli")
      if quest.isabelle_tour.started:
         actions.append("?school_cafeteria_vending_back_interact_two")
      actions.append("?school_cafeteria_vending_back_interact")


label school_cafeteria_vending_back_interact_first_time:
  "Right. This thing was out of order throughout my whole senior year."
  "Turned out that someone tried to pay using a shirt button."
  "The janitor eventually got it out using a paper clip."
  "Maybe this is my chance to rewrite history!"
  $school_cafeteria["vending_interacted"] = True
  return

label school_cafeteria_vending_back_interact_flora_cooking_chilli:
  "Love is like a vending machine."
  "All the good stuff is right there behind that glass. And if you don't have the means or the ability to push the right buttons, all you can do is look."
  return

label school_cafeteria_vending_back_interact_two:
  "Who thought it would be a good idea to drop a carbonated beverage a couple of feet after purchase?"
  "Probably the same people who think a paint job with red flames will make your car go faster."
  return

label school_cafeteria_vending_back_interact:
  "This is the machine with the good stuff. No wonder it's broken. We can't have nice things."
  return
