init python:
  class Interactable_school_cafeteria_vending_left(Interactable):

    def title(cls):
      return "Vending Machine"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.started:
        return "Mold sandwich or ant-infested brownie, take your pick."
      else:
        return  "[maxine] once wrote that there's a disease cult worshipping this beast. Their offerings keep the rotten sandwich business going."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19 and "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
            actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_vending_machine"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if mc.owned_item("full_letter"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:act_one,vending_machine_letter|Use What?","vending_machine_letter_use"])
      if quest.isabelle_tour.started:
        actions.append("?school_cafeteria_vending_left_interact_two")
      actions.append("?school_cafeteria_vending_left_interact")


label school_cafeteria_vending_left_interact_two:
  "Breaking the glass would be the easiest way to kill half the school and get the other half sick."
  "Luckily, the glass is reinforced and bulletproof."
  "However, you can put coins in if you feel like starting an epidemic."
  return

label school_cafeteria_vending_left_interact:
  "The neglected vending machine. No one's bought a sandwich in years. A bioweapon that breaks at least three Geneva conventions."
  return

label vending_machine_letter_use(item):
  if item.id == "full_letter":
    $mc.remove_item("full_letter")
    #SFX: Vending Machine Noises
    "The machine ate the letter..."
    $mc.add_item("ancient_ring")
  else:
    "The slot is too tight for my [item.title_lower]."
    "In a weird way, that inflates my ego."
    $quest.act_one.failed_item("vending_machine_letter",item)
return
