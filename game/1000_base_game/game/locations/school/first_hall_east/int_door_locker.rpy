init python:
  class Interactable_school_first_hall_east_door_locker(Interactable):

    def title(cls):
      return "Locker Rooms"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The locker rooms. The lions' den. The heart of darkness. Once you enter, you'll never be the same again."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_nurse":
          if quest.lindsey_nurse == "fetch_the_nurse":
            actions.append(["go","Locker Rooms","?school_first_hall_east_door_locker_lindsey_fetch_nurse"])
            return
        elif mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Locker Rooms","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong in ("florahelp","floralocker"):
            actions.append(["go","Locker Rooms","?school_first_hall_east_door_locker_lindseywrong"])
            return
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt":
            actions.append(["go","Locker Rooms","?quest_kate_fate_hunt_first_hall_east_door_locker"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Locker Rooms","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge","revenge_done","panik"):
            actions.append(["go","Locker Rooms","?quest_isabelle_dethroning_trap_locked"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Locker Rooms","?quest_lindsey_angel_keycard_school_first_hall_east_door_locker"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Locker Rooms","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.isabelle_stolen == "eavesdrop":
          actions.append(["interact","Eavesdrop","isabelle_quest_stolen_eavesdrop"])
          return
        if quest.mrsl_HOT == "eavesdrop":
          actions.append(["interact","Eavesdrop","mrsl_quest_HOT_eavesdrop"])
          return
      actions.append(["go","Locker Rooms","?school_first_hall_east_door_locker_interact"])


label school_first_hall_east_door_locker_lindseywrong:
  "Well, going in here is a death sentence to my afternoon-freedom. Really can't afford getting detention."
  "Maybe [flora] could help me do it..."
  return

label school_first_hall_east_door_locker_interact:
  "Imagine the girl's locker room..."
  "Fruity shampoo and sweet perfume. Laughter of bathing nymphs. The tiled garden of forbidden lust."
  return

label school_first_hall_east_door_locker_lindsey_fetch_nurse:
  "You're trying to go to the locker rooms?"
  "[lindsey] has a Defcon 5 concussion and you're trying to enter the locker rooms?!"
  "And you're laughing?!"
  return
