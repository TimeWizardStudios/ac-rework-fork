init python:
  class Interactable_school_first_hall_east_trophy(Interactable):

    def title(cls):
      return "Football Trophy"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "1982 Regional Championship. The pride of the school. Ridiculous how this is still relevant. Not even the coach is the same."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_first_hall_east_trophy_interact")


label school_first_hall_east_trophy_interact:
  "Despite not winning anything in ten years, the football team still think of themselves as winners, and of everyone else as losers."
  "Despite having their own locker room down at the football field, they still come here to bask in the glory."
  return
