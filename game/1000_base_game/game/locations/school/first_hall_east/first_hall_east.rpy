init python:
  class Location_school_first_hall_east(Location):

    default_title="Sports Wing"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      scene.append([(0,0),"school first_hall_east 1fecorridor"])

      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(0,38),"school first_hall_east night_window"])

      #Doors and Signs
      scene.append([(1762,0),"school first_hall_east locker_sign",("school_first_hall_east_door_locker",-64,255)])
      scene.append([(1722,105),"school first_hall_east locker_door",("school_first_hall_east_door_locker",-50,150)])
      scene.append([(1390,160),"school first_hall_east bathroom_sign",("school_first_hall_east_door_bathroom",2,65)])
      scene.append([(1390,225),"school first_hall_east bathroom","school_first_hall_east_door_bathroom"])
      scene.append([(1187,247),"school first_hall_east pool_sign",("school_first_hall_east_door_pool",3,33)])
      scene.append([(1172,280),"school first_hall_east pool","school_first_hall_east_door_pool"])
      scene.append([(929,280),"school first_hall_east gym_sign",("school_first_hall_east_door_gym",1,27)])
      scene.append([(873,307),"school first_hall_east gym","school_first_hall_east_door_gym"])

      if school_first_hall_east["vent_open"] and not quest.jo_washed.in_progress:
        scene.append([(1455,18),"school first_hall_east openvent",("school_first_hall_east_vent_ajar",28,370)]) #TBD replace image with opened vent
      else:
        scene.append([(1512,18),"school first_hall_east vent",("school_first_hall_east_vent",0,370)])

      if (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(663,0),"school first_hall_east lamp_off"])
      else:
        scene.append([(663,0),"school first_hall_east lamp"])
      scene.append([(861,0),"school first_hall_east banner"])#,"school_first_hall_east_banner"])

      scene.append([(1238,481),"school first_hall_east shoesbox"])#,"school_first_hall_east_shoesbox"])
      if sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 1:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 2:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 3:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 4:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 5:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
        scene.append([(1502,438),"school first_hall_east pileofshoes7"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 6:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
        scene.append([(1502,438),"school first_hall_east pileofshoes7"])
        scene.append([(1513,428),"school first_hall_east pileofshoes8"])
        scene.append([(1511,394),"school first_hall_east pileofshoes9"])
        scene.append([(1511,374),"school first_hall_east pileofshoes10"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 7:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
        scene.append([(1502,438),"school first_hall_east pileofshoes7"])
        scene.append([(1513,428),"school first_hall_east pileofshoes8"])
        scene.append([(1511,394),"school first_hall_east pileofshoes9"])
        scene.append([(1511,374),"school first_hall_east pileofshoes10"])
        scene.append([(1516,354),"school first_hall_east pileofshoes11"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 8:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
        scene.append([(1502,438),"school first_hall_east pileofshoes7"])
        scene.append([(1513,428),"school first_hall_east pileofshoes8"])
        scene.append([(1511,394),"school first_hall_east pileofshoes9"])
        scene.append([(1511,374),"school first_hall_east pileofshoes10"])
        scene.append([(1516,354),"school first_hall_east pileofshoes11"])
        scene.append([(1511,332),"school first_hall_east pileofshoes12"])
        scene.append([(1507,312),"school first_hall_east pileofshoes13"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 9:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
        scene.append([(1502,438),"school first_hall_east pileofshoes7"])
        scene.append([(1513,428),"school first_hall_east pileofshoes8"])
        scene.append([(1511,394),"school first_hall_east pileofshoes9"])
        scene.append([(1511,374),"school first_hall_east pileofshoes10"])
        scene.append([(1516,354),"school first_hall_east pileofshoes11"])
        scene.append([(1511,332),"school first_hall_east pileofshoes12"])
        scene.append([(1507,312),"school first_hall_east pileofshoes13"])
        scene.append([(1508,295),"school first_hall_east pileofshoes14"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 10:
        scene.append([(1512,573),"school first_hall_east pileofshoes1"])
        scene.append([(1511,543),"school first_hall_east pileofshoes2"])
        scene.append([(1518,528),"school first_hall_east pileofshoes3"])
        scene.append([(1511,504),"school first_hall_east pileofshoes4"])
        scene.append([(1506,484),"school first_hall_east pileofshoes5"])
        scene.append([(1504,466),"school first_hall_east pileofshoes6"])
        scene.append([(1502,438),"school first_hall_east pileofshoes7"])
        scene.append([(1513,428),"school first_hall_east pileofshoes8"])
        scene.append([(1511,394),"school first_hall_east pileofshoes9"])
        scene.append([(1511,374),"school first_hall_east pileofshoes10"])
        scene.append([(1516,354),"school first_hall_east pileofshoes11"])
        scene.append([(1511,332),"school first_hall_east pileofshoes12"])
        scene.append([(1507,312),"school first_hall_east pileofshoes13"])
        scene.append([(1508,295),"school first_hall_east pileofshoes14"])
        scene.append([(1502,269),"school first_hall_east pileofshoes15"])
        scene.append([(1513,253),"school first_hall_east pileofshoes16"])
      elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
        scene.append([(1502,222),"school first_hall_east pileofshoes",("school_first_hall_east_pileofshoes",10,570)])

      if not school_first_hall_east["shoe11"]:
        scene.append([(1541,643),"school first_hall_east shoes11","school_first_hall_east_shoes11"])
      if not school_first_hall_east["shoe10"]:
        scene.append([(1538,690),"school first_hall_east shoes10","school_first_hall_east_shoes10"])
      if not school_first_hall_east["shoe9"]:
        scene.append([(1508,721),"school first_hall_east shoes9","school_first_hall_east_shoes9"])
      if not school_first_hall_east["shoe8"]:
        scene.append([(1475,657),"school first_hall_east shoes8","school_first_hall_east_shoes8"])
      if not school_first_hall_east["shoe7"]:
        scene.append([(1443,723),"school first_hall_east shoes7","school_first_hall_east_shoes7"])
      if not school_first_hall_east["shoe6"]:
        scene.append([(1297,582),"school first_hall_east shoes6","school_first_hall_east_shoes6"])
      if not school_first_hall_east["shoe5"]:
        scene.append([(1279,570),"school first_hall_east shoes5","school_first_hall_east_shoes5"])
      if not school_first_hall_east["shoe4"]:
        scene.append([(1278,542),"school first_hall_east shoes4","school_first_hall_east_shoes4"])
      if not school_first_hall_east["shoe3"]:
        scene.append([(1263,585),"school first_hall_east shoes3","school_first_hall_east_shoes3"])
      if not school_first_hall_east["shoe2"]:
        scene.append([(1263,507),"school first_hall_east shoes2","school_first_hall_east_shoes2"])
      if not school_first_hall_east["shoe1"]:
        scene.append([(1245,522),"school first_hall_east shoes1","school_first_hall_east_shoes1"])
      scene.append([(1278,222),"school first_hall_east photos","school_first_hall_east_swim_club_photos"])
      if quest.maxine_wine == "locker" and mc.owned_item("flora_poster") and not quest.maxine_wine["sports_wing_poster"]:
        scene.append([(1417,303),"school first_hall_east board","school_first_hall_east_notice_board"])
      else:
        scene.append([(1417,303),"school first_hall_east board",("school_first_hall_east_door_bathroom",-14,-78)])
        if game.season == 1 and not quest.jo_washed.in_progress:
          if quest.maxine_wine["sports_wing_poster"]:
            scene.append([(1437,320),"school first_hall_east flora_poster",("school_first_hall_east_door_bathroom",-22,-95)])
      scene.append([(1779,297),"school first_hall_east poster",("school_first_hall_east_door_locker",-64,-42)])
      scene.append([(275,122),"school first_hall_east trophycase",("school_first_hall_east_trophy_case",99,199)])
      scene.append([(494,197),"school first_hall_east trophies",("school_first_hall_east_trophy_case",4,124)])
      if school_first_hall_east["newspaper"] and not quest.jo_washed.in_progress:
        scene.append([(514,337),"school first_hall_east newspaper",("school_first_hall_east_trophy_case",56,-16)])
      scene.append([(496,161),"school first_hall_east glass",("school_first_hall_east_trophy_case",-5,160)])
      scene.append([(528,75),"school first_hall_east trophy",("school_first_hall_east_trophy",0,150)])

      #Money & Books
      if not (quest.kate_blowjob_dream == "school" or quest.jo_washed.in_progress or quest.mrsl_bot == "dream"):
        if school_first_hall_east["dollar1_spawned_today"] == True and not school_first_hall_east["dollar1_taken_today"]:
          scene.append([(1533,168),"school first_hall_east dollar1",("school_first_hall_east_dollar1",0,200)])
        if school_first_hall_east["dollar2_spawned_today"] == True and not school_first_hall_east["dollar2_taken_today"]:
          scene.append([(492,737),"school first_hall_east dollar2","school_first_hall_east_dollar2"])
        if school_first_hall_east["dollar3_spawned_today"] == True and not school_first_hall_east["dollar3_taken_today"]:
          scene.append([(1547,676),"school first_hall_east dollar3","school_first_hall_east_dollar3"])
        if not school_first_hall_east["book_taken"]:
          scene.append([(238,780),"school first_hall_east book","school_first_hall_east_book"])

      if not kate.talking:
        if kate.at("school_first_hall_east", "sitting"):
          if not school_first_hall_east["exclusive"] or school_first_hall_east["exclusive"] == "kate":
            if game.season == 1:
              scene.append([(1359,414),"school first_hall_east kate","kate"])
            elif game.season == 2:
              scene.append([(1364,419),"school first_hall_east kate_autumn","kate"])

      if not mrsl.talking:
        if mrsl.at("school_first_hall_east","standing"):
          if not school_first_hall_east["exclusive"] or school_first_hall_east["exclusive"] == "mrsl":
            if game.season == 1:
              scene.append([(814,348),"school first_hall_east mrsl","mrsl"])
            elif game.season == 2:
              scene.append([(814,348),"school first_hall_east mrsl_autumn","mrsl"])

      if not lindsey.talking:
        if lindsey.at("school_first_hall_east", "stretching"):
          if not school_first_hall_east["exclusive"] or school_first_hall_east["exclusive"] == "lindsey":
            scene.append([(664,267),"school first_hall_east lindsey","lindsey"])

      if not flora.talking:
        if flora.at("school_first_hall_east", "standing"):
          if not school_first_hall_east["exclusive"] or school_first_hall_east["exclusive"] == "flora":
            if game.season == 1:
              if flora.equipped_item("flora_skirt"):
                scene.append([(1052,339),"school first_hall_east flora_skirt","flora"])
              else:
                scene.append([(1052,339),"school first_hall_east flora","flora"])
            elif game.season == 2:
              scene.append([(1056,342),"school first_hall_east flora_autumn","flora"])

      #Night
      if (game.hour >= 20
      or quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(0,0),"#school first_hall_east nightoverlay"])

      if quest.kate_wicked in ("costume","party"):
        scene.append([(1458,154),"school first_hall_east glowing_spiders",("school_first_hall_east_glowing_spiders",0,200)])

        if sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 7:
          scene.append([(1516,354),"school first_hall_east night_pileofshoes11"])
        elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 8:
          scene.append([(1516,354),"school first_hall_east night_pileofshoes11"])
          scene.append([(1511,332),"school first_hall_east night_pileofshoes12"])
        elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 9:
          scene.append([(1516,354),"school first_hall_east night_pileofshoes11"])
          scene.append([(1511,332),"school first_hall_east night_pileofshoes12"])
          scene.append([(1508,295),"school first_hall_east night_pileofshoes14"])
        elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 10:
          scene.append([(1516,354),"school first_hall_east night_pileofshoes11"])
          scene.append([(1511,332),"school first_hall_east night_pileofshoes12"])
          scene.append([(1508,295),"school first_hall_east night_pileofshoes14"])
          scene.append([(1502,269),"school first_hall_east night_pileofshoes15"])
          scene.append([(1513,253),"school first_hall_east night_pileofshoes16"])
        elif sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
          scene.append([(1502,222),"school first_hall_east night_pileofshoes",("school_first_hall_east_pileofshoes",10,570)])

      scene.append([(904,921),"school first_hall_east exit_arrow","school_first_hall_east_back"])

    def find_path(self,target_location):
      if target_location.id == "school_gym":
        return "school_first_hall_east_door_gym"
      elif target_location.id == "school_bathroom":
        return "school_first_hall_east_door_bathroom",dict(marker_offset=(25,250),marker_sector="right_top")
      elif target_location.id == "school_locker_room":
        return "school_first_hall_east_door_locker"
      else:
        return "school_first_hall_east_back",dict(marker_offset=(0,0),marker_sector="mid_mid")


label goto_school_first_hall_east:
  if school_first_hall_east.first_visit_today:
    $school_first_hall_east["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_first_hall_east["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_first_hall_east["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_first_hall_east)
  return
