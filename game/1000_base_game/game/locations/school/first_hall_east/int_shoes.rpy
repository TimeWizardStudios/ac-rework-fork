init python:
  class Interactable_school_first_hall_east_pileofshoes(Interactable):

    def title(cls):
      return "Pile of Shoes"

    def description(cls):
      if quest.kate_fate.started:
        return "My sole passion is starting to become a problem."
      else:
        return "Enough shoes to smell up the entire corridor.\n\nAlso, enough shoes to stand on and peer into the vent."

    def actions(cls,actions):
      if quest.kate_fate.in_progress:
        actions.append("?school_first_hall_east_pileofshoes_kate_fate_interact")
      else:
        actions.append("?school_first_hall_east_pileofshoes_interact")


label school_first_hall_east_pileofshoes_kate_fate_interact:
  "Life's a mountain to climb — if you fall, be prepared to get stepped on.{space=-55}"
  return

label school_first_hall_east_pileofshoes_interact:
  "There was a suspected shoe sniffer a couple of years back."
  "All the hot girls started putting their shoes in their bags or lockers after that."
  "Apparently, they'd been the serial sniffer's prime targets."
  return

label school_first_hall_east_pileofshoes_kate:
  show kate cringe with Dissolve(.5)
  kate cringe "..."
  mc "Wait, I wasn't going to—"
  kate embarrassed "Creep."
  $kate["at_none_now"] = True
  window hide
  show kate embarrassed at disappear_to_left
  pause 1.0
  window auto
  return


init python:
  class Interactable_school_first_hall_east_shoes11(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes11_interact")


label school_first_hall_east_shoes11_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe11"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes10(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes10_interact")


label school_first_hall_east_shoes10_interact:
  if school_first_hall_east["dollar3_spawned_today"] == True and not school_first_hall_east["dollar3_taken_today"]:
    $school_first_hall_east["dollar3_taken_today"] = True
    $mc.money+=20
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe10"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes9(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes9_interact")


label school_first_hall_east_shoes9_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe9"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes8(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes8_interact")


label school_first_hall_east_shoes8_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe8"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes7(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes7_interact")


label school_first_hall_east_shoes7_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe7"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes6(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes6_interact")


label school_first_hall_east_shoes6_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe6"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes5(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes5_interact")


label school_first_hall_east_shoes5_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe5"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes4(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes4_interact")


label school_first_hall_east_shoes4_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe4"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes3(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes3_interact")


label school_first_hall_east_shoes3_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe3"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes2(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes2_interact")


label school_first_hall_east_shoes2_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe2"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return


init python:
  class Interactable_school_first_hall_east_shoes1(Interactable):

    def title(cls):
      return "Shoes"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("school_first_hall_east_shoes1_interact")


label school_first_hall_east_shoes1_interact:
  if kate.at("school_first_hall_east"):
    call school_first_hall_east_pileofshoes_kate
  $school_first_hall_east["shoe1"] = True
  if quest.clubroom_access == "shoes" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return
