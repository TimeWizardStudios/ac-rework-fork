init python:
  class Interactable_school_first_hall_east_shoe_rack(Interactable):

    def title(cls):
      return "Shoe Rack"

    def description(cls):
      return "Shoes"

    def actions(cls,actions):
      actions.append("?school_first_hall_east_shoe_rack_interact")


label school_first_hall_east_shoe_rack_interact:
  "Shoes"
  return
