init python:
  class Interactable_school_first_hall_east_glowing_spiders(Interactable):

    def title(cls):
      return "Glowing Spiders"

    def description(cls):
      return "Looks like fireflies, but up close they have eight legs and no wings...{space=-15}"

    def actions(cls,actions):
      actions.append("?school_first_hall_east_glowing_spiders_interact")


label school_first_hall_east_glowing_spiders_interact:
  "They look real, but it's hard to tell..."
  "Maybe [maxine] is just playing a prank."
  "Surely, there aren't any real glowing spiders."
  return
