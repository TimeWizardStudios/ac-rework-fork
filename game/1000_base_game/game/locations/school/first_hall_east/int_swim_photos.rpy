
init python:
  class Interactable_school_first_hall_east_swim_club_photos(Interactable):

    def title(cls):
      return "Swim Team Photos"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return  "These are all the swim teams through the years. The oldest ones are in black and white."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_first_hall_east_swim_club_photos_interact")


label school_first_hall_east_swim_club_photos_interact:
  "This is where the school's real talent is. Even in the old pictures, the women all look stunning."
  "It's hard not to wonder what kind of bitches hide behind those fake smiles."
  return
