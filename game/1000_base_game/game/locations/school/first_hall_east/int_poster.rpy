init python:
  class Interactable_school_first_hall_east_front_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
      return "Poster"

    def actions(cls,actions):
      actions.append("?school_first_hall_east_front_poster_interact")


label school_first_hall_east_front_poster_interact:
  "Poster"
  return
