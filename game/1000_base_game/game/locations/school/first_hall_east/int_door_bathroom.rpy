init python:
  class Interactable_school_first_hall_east_door_bathroom(Interactable):

    def title(cls):
      return "Bathroom"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "My safe space."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_nurse":
          if quest.lindsey_nurse == "fetch_the_nurse":
            actions.append(["go","Women's Bathroom","?school_first_hall_east_door_bathroom_lindsey_fetch_nurse"])
            return
        elif mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Women's Bathroom","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt":
            actions.append(["go","Women's Bathroom","?quest_kate_fate_hunt_first_hall_east_door_bathroom"])
            return
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "stargold":
            actions.append(["go","Women's Bathroom","quest_kate_desire_stargold_bathroom_interact"])
          elif quest.kate_desire == "bathroomhelp":
            actions.append(["go","Women's Bathroom","?quest_kate_desire_bathroomhelp_bathroom_interact"])
            return
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "choice":
            actions.append(["go","Women's Bathroom","quest_flora_squid_choice"])
          elif quest.flora_squid == "wait":
            actions.append(["go","Women's Bathroom","?quest_flora_squid_wait_door_bathroom"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Women's Bathroom","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge","panik"):
            actions.append(["go","Women's Bathroom","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","Women's Bathroom","?quest_isabelle_dethroning_revenge_done_sports_wing_bathroom_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed == "insecure":
            actions.append(["go","Women's Bathroom","goto_school_bathroom"])
          elif quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Women's Bathroom","?quest_lindsey_angel_keycard_school_first_hall_east_door_bathroom"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Women's Bathroom","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.maxine_eggs in ("recon","bust"):
          actions.append(["go","Women's Bathroom","quest_maxine_eggs_bathroom_interact_recon"])
        elif quest.maxine_eggs == "statue":
          actions.append("quest_maxine_eggs_bathroom_interact_statue")
          return
        if "power_cut" >= quest.lindsey_motive >= "phone":
          actions.append(["go","Women's Bathroom","?quest_lindsey_motive_phone"])
          return
        elif quest.lindsey_motive == "bathroom":
          actions.append(["go","Women's Bathroom","quest_lindsey_motive_bathroom"])
      actions.append(["go","Women's Bathroom","?school_first_hall_east_door_bathroom_interact"])


label school_first_hall_east_door_bathroom_interact:
  "Have you ever had the honor of eating your turkey mayo sandwhich surrounded by poop particles?"
  "I have."
  return

label school_first_hall_east_door_bathroom_lindsey_fetch_nurse:
  "You're trying to go to the bathroom?"
  "[lindsey] has a Defcon 5 concussion and you're trying to enter the bathroom?!"
  "And you're laughing?!"
  return

label quest_maxine_eggs_bathroom_interact_recon:
  if quest.maxine_eggs == "recon":
    "This is where the glowing spider eggs were last seen, but entering now will land me a full year's detention."
    "Need to wait until after dark when nobody is around."
    "The problem is that the [guard] sweeps the school before locking up."
    "And if you don't have a special permit to stay late, your bacon is toast."
    "Getting a permit is impossible, though. Finding a place to hide is my best bet."
  elif quest.maxine_eggs == "bust":
    "The statue idea was a total bust... and not the antique bronze kind."
    "Still need to get those photos, though."
    "Hmm..."
  menu(side="middle"):
    extend ""
    "Hide in plain sight, masquerading as a statue" if not quest.maxine_eggs == "bust":
      if jacklyn.love >= 4:
        "If [jacklyn] has taught me anything, I'm going to need silver paint for this to work."
        "Pretty sure there's a bucket in the art classroom."
        $quest.maxine_eggs.advance("paint")
      else:
        "If street artists can do it, anyone can."
        "Just need to stand still and wait."
        while game.hour < 20:
          $game.advance()
        with Fade(.5,3,.5)
        "A few strange looks so far, but everyone has mostly ignored me."
        "That's how it usually goes, though."
        "Okay, now for—"
        show guard surprised_flashlight at appear_from_right
        guard surprised_flashlight "Huh?"
        guard angry_flashlight "Hey! What do you think you're doing?"
        "Shit."
        "Okay. Don't move a muscle..."
        guard suspicious_flashlight "Enough horsing around. You're coming with me."
        "Damn it, so close to working."
        $mc["detention"]+=1
        call home_bedroom_bed_interact_force_sleep
        call goto_home_bedroom
        "Well, last night was a complete failure, but the quest for the glowing spider eggs is far from over."
        "It sounds stupid even thinking it."
        $quest.maxine_eggs["busted"] = True
        $quest.maxine_eggs.advance("bust")
      return
    "Hide behind the trophy case":
      "This is the most obvious hiding spot. Hopefully, the [guard] is lazy tonight."
      show guard tcase_perspective_day with Dissolve(.5)
      "Patience and silence... two of my favorite virtues."
      while game.hour < 20:
        $game.advance()
        $quest.maxine_eggs.advance("bathroom")
      show guard tcase_perspective_night with Fade(.5,3,.5)
      "A dripping faucet in the distance."
      "The muted whirr of the AC from the gym."
      "The tranquil lapping of the pool."
      "When the noisy students disappear, the small sounds come out."
      "..."
      "Shit. Someone's coming..."
      show guard tcase_perspective_guard_night with Dissolve(.5)
      "..."
      "Why is he humming \"Don't Fear the Reaper?\""
      "Quick, hit me with another shot of adrenaline. Ugh."
      "As long as he doesn't search too closely..."
      "..."
      show guard tcase_perspective_night with Dissolve(.5)
      "Okay, he's gone. The coast is clear."
      hide guard with Dissolve(.5)
      "Time to go where no man has ever gone before."
    "?mc.strength>=6@[mc.strength]/6|{image=stats str}|Scale the wall and hide inside the air duct":
      "Strong and sneaky — the perfect combination."
      $school_first_hall_east["shoe1"] = school_first_hall_east["shoe2"] = school_first_hall_east["shoe3"] = True
      pause(.5)
      $school_first_hall_east["shoe4"] = school_first_hall_east["shoe5"] = True
      pause(.5)
      $school_first_hall_east["shoe6"] = school_first_hall_east["shoe7"] = school_first_hall_east["shoe8"] = True
      pause(.5)
      if school_first_hall_east["dollar3_spawned_today"] == True and not school_first_hall_east["dollar3_taken_today"]:
        $school_first_hall_east["dollar3_taken_today"] = True
        $mc.money+=20
      $school_first_hall_east["shoe9"] = school_first_hall_east["shoe10"] = school_first_hall_east["dollar3_taken"] = True
      pause(.5)
      $school_first_hall_east["shoe11"] = True
      pause(.5)
      show guard grill_perspective_day with Dissolve(.5)
      "This is some mission impossible shit. Tom Cruise would be proud."
      "Okay, now for the waiting game..."
      while game.hour < 20:
        $game.advance()
        $quest.maxine_eggs.advance("bathroom")
      show guard grill_perspective_night with Fade(.5,3,.5)
      "The soft creaking of the ceiling."
      "A mouse skittering through the ducts."
      "The muted banging in the pipes."
      "When the noisy students disappear, the small sounds come out."
      "..."
      "Shit. Someone's coming..."
      show guard grill_perspective_guard_night with Dissolve(.5)
      "..."
      "Why is he humming \"Don't Fear the Reaper?\""
      "Quick, hit me with another shot of adrenaline. Ugh."
      "As long as he doesn't point the flashlight up at my ugly face..."
      "..."
      show guard grill_perspective_night with Dissolve(.5)
      "Okay, he's gone. The coast is clear."
      hide guard with Dissolve(.5)
      "Time to go where no man has ever gone before."
      if mc['vent_hide']:#If the vent has already been hidden in once
        $achievement.call_me_beep_me.unlock()
      else:
        $mc['vent_hide'] = True
    "?mc.charisma>=6@[mc.charisma]/6|{image=stats cha}|Hide at the pool entrance, a smile and some bullshit at the ready":
      "This is the most risky spot, just how I like it."
      while game.hour < 20:
        $game.advance()
        $quest.maxine_eggs.advance("bathroom")
      with Fade(.5,3,.5)
      "Silence. The air vibrating in anticipation. The hammer hovering over the gavel."
      "Guilty? Innocent?"
      "Only time can be the judge."
      "Only the quiet thoughts of the—"
      "Shit. Someone's coming..."
      "Better undress fast."
      "..."
      show guard surprised_flashlight at appear_from_right
      guard surprised_flashlight "Huh?"
      mc "Good evening, sir! Quite the peaceful night, wouldn't you say?"
      guard suspicious_flashlight "I suppose..."
      mc "I've just finished up my last laps."
      mc "The water is the perfect temperature if you're in the mood for a swim!"
      guard smile_flashlight "That would be nice, but it's getting late."
      mc "All right. Well, I'm about to hit the showers."
      mc "It's been a long day, but I'm glad we have the chance to get some extra practice in after dark."
      guard smile_flashlight "Keep up the good work, kid."
      mc "See you around, sir!"
      show guard smile_flashlight at disappear_to_right
      pause(1)
      "Whoa! What a rush! That went a lot better than expected..."
      "Okay, time to get those photos."
      $achievement.silver_tongue.unlock()
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_bathroom"
  $mc["focus"] = "maxine_eggs"
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_maxine_eggs_bathroom_interact_statue:
  "Drenching myself in silver paint is the easy part..."
  "Standing still is going to be the challenge."
  show silver_paint:
    yoffset -1020 alpha 1
    easein 1 yoffset -20
    easeout 2 yoffset 0 alpha .75
  $mc.remove_item("silver_paint")
  "Oof, that's sticky."
  "That's also what she said."
  while game.hour < 20:
    $game.advance()
    $quest.maxine_eggs.advance("bathroom")
  hide silver_paint with Fade(.5,3,.5)
  "No senses but one."
  "Smell."
  "The asphyxiating smell of silver paint."
  "..."
  "Shit. Someone's coming..."
  "..."
  show guard surprised_flashlight at appear_from_right
  guard surprised_flashlight "Huh?"
  guard surprised_flashlight "What the hell is this?"
  guard suspicious_flashlight "..."
  guard neutral_flashlight "..."
  guard angry_flashlight "That is one ugly piece of art..."
  "Gee, thanks. Tell me how you really feel."
  show guard suspicious_flashlight with Dissolve(.5)
  "..."
  show guard suspicious_flashlight at disappear_to_right
  "Thank god, he finally left. I can't believe that worked!"
  "Finding those freaking eggs is going to be a nightmare, though."
  $achievement.the_right_kind_of_bust.unlock()
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_bathroom"
  $mc["focus"] = "maxine_eggs"
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return
