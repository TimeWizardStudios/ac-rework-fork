init python:
  class Interactable_school_first_hall_east_door_gym(Interactable):

    def title(cls):
      return "Gym"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Lungs burning. Sweat seeping into my eyes. The mocking laughter of my classmates. Every door in this corridor seems to lead to a place of horrible memories."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_nurse":
          if quest.lindsey_nurse == "fetch_the_nurse":
            actions.append(["go","Gym","?school_first_hall_east_door_gym_lindsey_fetch_nurse"])
            return
        elif mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Gym","goto_school_gym"])
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt":
            actions.append(["go","Gym","?first_hall_door_right_kate_fate_interact"])
            return
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "wait":
            actions.append(["go","Gym","?quest_flora_squid_wait_door_gym"])
            return
        elif mc["focus"] == "kate_trick":
          if quest.kate_trick == "gym":
            actions.append(["go","Gym","quest_kate_trick_gym"])
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Gym","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge","panik"):
            actions.append(["go","Gym","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","Gym","?quest_isabelle_dethroning_revenge_done_sports_wing_gym_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Gym","goto_school_gym"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Gym","?quest_lindsey_angel_keycard_school_first_hall_east_door_gym"])
            return
      else:
        if quest.kate_trick == "training":
          actions.append(["go","Gym","?quest_kate_trick_training"])
          return
        elif quest.kate_trick.in_progress:
          actions.append(["go","Gym","?quest_kate_trick_off_limits"])
          return
      if True in [school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]:
        actions.append(["go","Gym","first_hall_east_reset_gym"])
      else:
        actions.append(["go","Gym","goto_school_gym"])


label school_first_hall_east_door_gym_lindsey_fetch_nurse:
  "Not without the [nurse], smart one."
  return

label first_hall_east_reset_gym:
  $school_first_hall_east["shoe1"] = False
  $school_first_hall_east["shoe2"] = False
  $school_first_hall_east["shoe3"] = False
  $school_first_hall_east["shoe4"] = False
  $school_first_hall_east["shoe5"] = False
  $school_first_hall_east["shoe6"] = False
  $school_first_hall_east["shoe7"] = False
  $school_first_hall_east["shoe8"] = False
  $school_first_hall_east["shoe9"] = False
  $school_first_hall_east["shoe10"] = False
  $school_first_hall_east["shoe11"] = False
  if quest.clubroom_access == "vent":
    $quest.clubroom_access.advance("shoes",silent=True)
  jump goto_school_gym
