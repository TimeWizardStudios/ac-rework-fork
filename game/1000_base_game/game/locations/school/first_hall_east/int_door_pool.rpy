init python:
  class Interactable_school_first_hall_east_door_pool(Interactable):

    def title(cls):
      return "Pool"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The smell of chlorine always fills me with nostalgia, but the splashing water makes me want to drown."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_nurse":
          if quest.lindsey_nurse == "fetch_the_nurse":
            actions.append(["go","Pool","?school_first_hall_east_door_pool_lindsey_fetch_nurse"])
            return
        elif mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Pool","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt":
            actions.append(["go","Pool","?quest_kate_fate_hunt_first_hall_east_door_pool"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Pool","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("revenge","panik"):
            actions.append(["go","Pool","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","Pool","?quest_isabelle_dethroning_revenge_done_sports_wing_pool_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Pool","?quest_lindsey_angel_keycard_school_first_hall_east_door_pool"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Pool","kate_blowjob_dream_random_interact_locked_location"])
      else:
        if quest.poolside_story == "poolclosed":
          actions.append(["go","Pool","school_first_hall_east_door_pool_poolside_story_poolclosed"])
        elif quest.poolside_story == "meetmrsl":
          actions.append(["go","Pool","school_first_hall_east_door_pool_poolside_story_meetmrsl"])
      actions.append(["go","Pool","?school_first_hall_east_door_pool_interact"])


label school_first_hall_east_door_pool_interact:
  "It looks bad if students start drowning during the first week, so it's school policy to keep it closed."
  return

label school_first_hall_east_door_pool_lindsey_fetch_nurse:
  "You're trying to go to the pool?"
  "[lindsey] has a Defcon 5 concussion and you're trying to enter the pool?!"
  "And you're laughing?!"
  return

label school_first_hall_east_door_pool_poolside_story_poolclosed:
  "There's a notice taped to the door."
  "{i}\"Due to the amounts of soggy confetti in the filters last year, the pool will remain closed until after the Newfall Independence Day festivities.\"{/}"
  "Oh, right. That happened."
  "Luckily, no one found out it was me trying to ruin it."
  "Not sure what [mrsl]'s thoughts are regarding the swim team tryouts."
  "How am I supposed to feast my eyes upon the flesh of the innocent now?"
  $quest.poolside_story.advance("newplan")
  return
