init python:
  class Interactable_school_first_hall_east_banner(Interactable):

    def title(cls):
      return "Banner"

    def description(cls):
      return "Banner"

    def actions(cls,actions):
      actions.append("?school_first_hall_east_banner_interact")


label school_first_hall_east_banner_interact:
  "Banner"
  return
