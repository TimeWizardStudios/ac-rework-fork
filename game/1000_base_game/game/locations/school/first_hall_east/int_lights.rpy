init python:
  class Interactable_school_first_hall_east_lights(Interactable):

    def title(cls):
      return "Lights"

    def description(cls):
      return "Lights"

    def actions(cls,actions):
      actions.append("?school_first_hall_east_lights_interact")


label school_first_hall_east_lights_interact:
  "Lights"
  return
