init python:
  class Interactable_school_first_hall_east_vent_ajar(Interactable):

    def title(cls):
      return "Vent"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Hopefully, no one notices that the bolts are loose and the grill is hanging open."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt":
            if sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
              actions.append("school_first_hall_east_vent_kate_fate")
            else:
              actions.append("?school_first_hall_east_vent_kate_fate_shoes")
              return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
        actions.append("?school_first_hall_east_vent_ajar_interact_pile_of_shoes")
      else:
        actions.append("?school_first_hall_east_vent_ajar_interact")


label school_first_hall_east_vent_kate_fate:
  "If I get stuck now with my ass sticking out..."
  "Please don't be too fat..."
  window hide
  jump quest_kate_fate_air_duct_shortcut

label school_first_hall_east_vent_ajar_interact_pile_of_shoes:
  "Someone left a bag of dubious contents here."
  "If I ever need to rob a bank, I can get the outfit here."
  return

label school_first_hall_east_vent_ajar_interact:
  "I need to start wiping things clean of my fingerprints."
  "But on the other hand, maybe I want to be one of those criminals that take pride in their work and want the world to know."
  "Need to start thinking of my legacy as the Vent Buster."
  return
