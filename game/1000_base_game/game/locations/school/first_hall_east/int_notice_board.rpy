init python:
  class Interactable_school_first_hall_east_notice_board(Interactable):
    def title(cls):
      return "Bulletin Board"
    def actions(cls,actions):
      actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:maxine_wine,flora_poster|Use What?","school_first_hall_east_notice_board_maxine_wine_locker_use_item"])

label school_first_hall_east_notice_board_maxine_wine_locker_use_item(item):
  if item == "flora_poster":
    $quest.maxine_wine["sports_wing_poster"] = True
    $mc.remove_item("flora_poster")
    "Hard to reach this one without looking into the women's bathroom, and that would be a sin."
    "Well, it's up and no one's tried to kill me yet for trying to sneak a peek. That's a win."
    if quest.maxine_wine["entrance_hall_poster"] + quest.maxine_wine["homeroom_poster"] + quest.maxine_wine["admin_wing_poster"] + quest.maxine_wine["1f_hall_poster"] + quest.maxine_wine["sports_wing_poster"] == 5:
      "Okay, that's all of them. Praise the lord Jesus hallelujah. My ass is sweating more than that hipster up on the cross."
      $quest.maxine_wine.advance("florareward")
  else:
    "Maybe I could use my [item.title_lower] as a sort of mating-call-invitation-thing. Surely, that'd get me laid."
    $quest.maxine_wine.failed_item("flora_poster",item)
  return
