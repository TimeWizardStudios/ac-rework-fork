init python:
  class Interactable_school_first_hall_east_trophy_case(Interactable):

    def title(cls):
      return "Trophy Case"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "If there was a contest for the biggest loser, I'd have a trophy here."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "wait" and quest.flora_squid["smell_concern"]:
            actions.append("quest_flora_squid_wait_trophy_case")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.isabelle_dethroning == "trophy_case" and not jo["at_none_now"]:
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_dethroning,trophy_case|Use What?","quest_isabelle_dethroning_trophy_case"])
        elif quest.isabelle_dethroning == "newspaper" and mc.owned_item("newspaper"):
          actions.append("quest_isabelle_dethroning_newspaper")
      actions.append("?school_first_hall_east_trophy_case_interact")


label school_first_hall_east_trophy_case_interact:
  "All these trophies were won by the swim team. What a legacy."
  "Apart from the big football trophy over there, we haven't succeeded in any other sport."
  return
