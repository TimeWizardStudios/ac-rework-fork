init python:
  class Interactable_school_first_hall_east_back(Interactable):

    def title(cls):
      return "Back"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_bot == "dream"):
        return "Too late to go back now."
      else:
        return "The sign above this doorway should say, \"Run while you still can.\" It is the way out of the infamous Sports Wing, after all."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","First Floor Hall","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "wait":
            actions.append(["go","First Floor Hall","?quest_flora_squid_wait_back"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","First Floor Hall","goto_school_first_hall"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","First Floor Hall","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
      if True in [school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]:
        actions.append(["go","First Floor Hall","first_hall_east_reset"])
      else:
        actions.append(["go","First Floor Hall","goto_school_first_hall"])


label first_hall_east_reset:
  $school_first_hall_east["shoe1"] = False
  $school_first_hall_east["shoe2"] = False
  $school_first_hall_east["shoe3"] = False
  $school_first_hall_east["shoe4"] = False
  $school_first_hall_east["shoe5"] = False
  $school_first_hall_east["shoe6"] = False
  $school_first_hall_east["shoe7"] = False
  $school_first_hall_east["shoe8"] = False
  $school_first_hall_east["shoe9"] = False
  $school_first_hall_east["shoe10"] = False
  $school_first_hall_east["shoe11"] = False
  if quest.clubroom_access == "vent":
    $quest.clubroom_access.advance("shoes",silent=True)
  jump goto_school_first_hall
