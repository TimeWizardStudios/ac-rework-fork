init python:
  class Interactable_school_first_hall_east_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_east_dollar1_take"])


label school_first_hall_east_dollar1_take:
  $school_first_hall_east["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_east_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_east_dollar2_take"])


label school_first_hall_east_dollar2_take:
  $school_first_hall_east["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_east_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_first_hall_east_dollar3_take"])


label school_first_hall_east_dollar3_take:
  $school_first_hall_east["dollar3_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_first_hall_east_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_first_hall_east_book_interact"])


label school_first_hall_east_book_interact:
  $school_first_hall_east["book_taken"] = True
  "Finally, a book that's worth reading..."
  "{i}\"Silver Tongue: Cunnilingus and Linguistic Cunning A-Z.\"{/}"
  $mc.charisma+=1
  return
