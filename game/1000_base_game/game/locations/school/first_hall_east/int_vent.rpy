init python:
  class Interactable_school_first_hall_east_vent(Interactable):

    def title(cls):
      return "Vent"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_blowjob_dream == "school":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Beyond the unreachable heights lies a new frontier of dust bunnies and cloud rats."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "hunt":
            if sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
              actions.append("school_first_hall_east_vent_kate_fate_shoes_bolted")
            else:
              actions.append("?school_first_hall_east_vent_kate_fate_shoes")
              return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not quest.clubroom_access.started:
          actions.append(["quest","Quest","school_first_hall_east_vent_clubroom_access"])
        elif quest.clubroom_access == "vent" and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:act_one,east_vent|Use What?","school_first_hall_east_vent_use"])
      actions.append("?school_first_hall_east_vent_interact")


label school_first_hall_east_vent_kate_fate_shoes:
  "Shit! That would be a great hiding spot, but it's so high up..."
  "Shit, shit, shit. How do I close the distance?!"
  return

label school_first_hall_east_vent_kate_fate_shoes_bolted:
  "Oh fuck, it's bolted in place!"
  "I need to find another—"
  show kate smile at appear_from_left
  kate smile "There you are!"
  jump quest_kate_fate_fight_surrender

label school_first_hall_east_vent_clubroom_access:
  "This is the only ventilation duct in the entire school."
  "Where does it lead? No one knows."
  "Hm..."
  "There seems to be something dark in there, blocking the airflow."
  "Need to remove the grill to get a better look."
  $quest.clubroom_access.start()
  if mc.owned_item("monkey_wrench") and sum([school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"]]) == 11:
    $quest.clubroom_access.advance("vent")
  return

label school_first_hall_east_vent_use(item):
  if item == "monkey_wrench":
    "I've always been something of a handyman, if you know what I'm saying."
    "Just need to loosen these bolts..."
    "..."
    $school_first_hall_east["vent_open"] = True
    "There we go."
    "Truly an explorer of uncharted lands."
    "A modern-day Columbus, if you will. Haters and all."
    "Oh, what's this?"
    "A bag of some sort..."
    "Ski masks, a grappling hook, dark clothes..."
    "Is this Batman's secret stash? The only thing missing is the freaking Batmobile."
    "It's all dusty. Whoever brought this probably got lost in the vent maze and perished."
    "The grappling hook looks pretty cool."
    $mc.add_item("grappling_hook")
    $quest.clubroom_access.advance("grapplinghook")
  else:
    "Unscrewing the bolts with my [item.title_lower] would be break-in goals. Unfortunately, that's still something of a pipe dream."
    $quest.act_one.failed_item("east_vent", item)
  return

label school_first_hall_east_vent_interact:
  "The architects built these tunnels too greedily and too deep..."
  "They disturbed something in the howling depths."
  "At least, that's what [maxine] says."
  return
