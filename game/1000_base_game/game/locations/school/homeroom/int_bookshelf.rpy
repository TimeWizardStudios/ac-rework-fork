init python:
  class Interactable_school_homeroom_bookshelf(Interactable):

    def title(cls):
      return "Bookshelf"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Seems to be mostly smut novels here. Porn, but without the good parts of porn. Yeah, no thanks."

    def actions(cls,actions):
      if not school_homeroom["got_book"]:
        actions.append(["take","Take","school_homeroom_bookshelf_interact_first"])
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            if not school_homeroom["got_book"]:
              actions.remove(["take","Take","school_homeroom_bookshelf_interact_first"])
            actions.append(["interact","Hide","?quest_mrsl_table_school_homeroom_bookshelf_interact"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            if not school_homeroom["got_book"]:
              actions.remove(["take","Take","school_homeroom_bookshelf_interact_first"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_bookshelf")
      actions.append("?school_homeroom_bookshelf_interact")


label school_homeroom_bookshelf_interact_first:
  "{i}\"Bayonets, Etiquette, and Other Things the French Made Dull, Vol. 983\"{/}"
  "This title sounds interesting... and it's a lot heavier than expected."
  $mc.add_item("bayonets_etiquettes")
  $school_homeroom["got_book"] = True
  return

label school_homeroom_bookshelf_interact:
  "Let's see what else is here..."
  "{i}\"Kissing the Dead — the First Responder's Guide to CPR\"{/}"
  "That... might actually be the sign I've been looking for! Might be my only chance to put my mouth on a girl!"
  return

label isabelle_quest_stolen_maxine_paper_bookshelf:
  "Technically, these fuckers are filled with paper, but [maxine] would probably spend a year analyzing all the text on the pages for hidden meanings."
  return
