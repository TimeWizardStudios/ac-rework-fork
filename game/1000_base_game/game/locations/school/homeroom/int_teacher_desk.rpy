init python:
  class Interactable_school_homeroom_teacher_desk(Interactable):

    def title(cls):
      return "Teacher's Desk"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The butcher's desk, where papers and homework get sliced up and torn apart."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "puzzle" and not quest.isabelle_haggis["cups_found"]:
            actions.append(["take","Take","school_homeroom_teacher_desk_interact_isabelle_haggis"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            actions.append(["interact","Hide","quest_mrsl_table_school_homeroom_teacher_desk_interact"])
        elif mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "hide":
            actions.append("quest_lindsey_motive_hide_teacher_desk")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.kate_trick == "break_in":
         actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:kate_trick,break_in|Use What?","quest_kate_trick_break_in"])
        if quest.jo_day == "supplies" and not quest.jo_day["mug_taken"]:
          if mrsl.at("school_homeroom"):
            if quest.jo_day["steal_attempt"] > 2:
              actions.append(["take","Take","?quest_jo_day_supplies_teacher_desk_mrsl_repeat"])
            else:
              actions.append(["take","Take","quest_jo_day_supplies_teacher_desk_mrsl"])
          else:
            actions.append(["take","Take","quest_jo_day_supplies_teacher_desk_take"])
        if quest.maya_witch == "schedule" and not mc.owned_item("maya_schedule"):
          if mrsl.location == "school_homeroom":
            if quest.maya_witch["locked_of_course"]:
              actions.append(["use_item","Use item","?quest_maya_witch_schedule_school_homeroom_teacher_desk_use_item_mrsl"])
            actions.append("?quest_maya_witch_schedule_school_homeroom_teacher_desk_interact_mrsl")
          else:
            if quest.maya_witch["locked_of_course"]:
              actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:maya_witch,schedule|Use What?","quest_maya_witch_schedule_school_homeroom_teacher_desk_use_item"])
            actions.append("?quest_maya_witch_schedule_school_homeroom_teacher_desk_interact")
          return
        if quest.mrsl_bot == "message_box" and not school_homeroom["message_box"]:
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_bot,message_box|Use What?","quest_mrsl_bot_message_box_teacher_desk"])
      actions.append("?school_homeroom_teacher_desk_interact")


label school_homeroom_teacher_desk_interact_isabelle_haggis:
  "Let's see what [mrsl] is hiding..."
  "..."
  $mc.add_item("coffee_cup",3)
  "Just three porcelain coffee cups, apparently."
  "I wonder if she knows they invented disposables."
  $quest.isabelle_haggis["cups_found"] = True
  return

label school_homeroom_teacher_desk_interact:
  "I was hoping to find a stash of sex toys, but alas. Just a stash of writing utensils and what looks like a chewed-on angel toy."
  return
