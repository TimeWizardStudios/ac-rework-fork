init python:
  class Interactable_school_homeroom_ball_of_yarn(Interactable):

    def title(cls):
      return "Ball of Yarn"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.day1_take2.started:
        # return "There's only one person who knits in class. The girl who can't be charmed."
        return "There's only one person\nwho knits in class. The girl who can't be charmed."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      else:
        if quest.maxine_dive == "trade":
          actions.append(["take","Take","quest_maxine_dive_trade"])
      actions.append(["take","Take","?school_homeroom_ball_of_yarn_interact"])


label school_homeroom_ball_of_yarn_interact:
  "[maya] was always a bit of an oddball and a klutz. Often forgetting her stuff and being late to class."
  "And a wall would probably be more susceptible to romantic advances than her."
  "Anyway, returning her prized yarn might give me an opening... but, err, probably not."
  $school_homeroom["ball_of_yarn_taken"]=True
  $mc.add_item("ball_of_yarn",5)
  ## start maya quest here
  return
