init python:
  class Interactable_school_homeroom_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_haggis.started:
        return "Life's a set of prisons. Rooms, borders, outer space, but most of all your own head. Complete freedom is impossible to achieve."
      elif quest.day1_take2.started:
        return "Ah, the feeling of leaving a classroom. Nothing quite like it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "instructions" and quest.isabelle_haggis.in_progress:
            actions.append(["go","Entrance Hall","?school_homeroom_door_isabelle_haggis_puzzle"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            actions.append(["talk","Talk to [lacey]","?quest_mrsl_table_school_homeroom_door_talk"])
            return
        elif mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "hide":
            actions.append(["go","Entrance Hall","?quest_lindsey_motive_hide_door"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume" and not quest.kate_wicked["still_patrolling"]:
            actions.append(["go","Entrance Hall","quest_kate_wicked_costume_homeroom_door"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Entrance Hall","goto_school_ground_floor"])
      else:
        if quest.day1_take2 == "isabelle_pass":
          actions.append(["go","Entrance Hall","school_homeroom_door_isabelle_pass"])
        elif quest.day1_take2 == "isabelle_volunteer":
          actions.append(["go","Entrance Hall","school_homeroom_door_isabelle_volunteer"])
        if quest.fall_in_newfall in ("late","stick_around"):
          actions.append(["go","Entrance Hall","?quest_fall_in_newfall_late_school_homeroom_door"])
          return
      actions.append(["go","Entrance Hall","goto_school_ground_floor"])


label school_homeroom_door_isabelle_pass:
  show isabelle blush at appear_from_right
  isabelle blush "Hi! Sorry to bother you."
  mc "It's... fine. No need to be. Sorry, I mean. No need to be sorry!"
  "Crap. Speaking to women was never my strong suit."
  isabelle sad "I think my guide ditched me."
  "Typical [kate]. She always was the biggest bitch in the school. Not just to me."
  isabelle sad "Everyone else left. Would you mind showing me around?"
  show isabelle sad at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Sure thing! Follow me.\"":
      show isabelle sad at move_to(.5)
      mc "Sure thing! Follow me."
      $isabelle.love+=1
      isabelle smile "Thanks, this school is pretty big."
    "\"I could show you more than just the school...\"":
      show isabelle sad at move_to(.5)
      label isabelle_show_more_than_school:
      $unlock_stat_perk("lust7")
      $mc.lust+=1
      mc "I could show you more than just the school..."
      isabelle skeptical "And what would that be?"
      show isabelle skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "\"My dic...tionary. I heard English girls like big words.\"":
          show isabelle skeptical at move_to(.5)
          $mc.charisma+=1
          mc "My dic...tionary. I heard English girls like big words."
          isabelle laughing "I suppose you're right!"
          $isabelle.lust+=1
          isabelle confident "Luckily for you, we also like cheese."
          isabelle smile "Fine. Lead the way, then."
          "She did seem to like that, but maybe she's just polite."
          "Damn it! Girls are so hard to read."
          "Okay, just got to keep my head cool and the spaghetti in my pockets now."
        "\"Err, I just know the grounds really well. Follow me!\"":
          show isabelle skeptical at move_to(.5)
          mc "Err, I just know the grounds really well. Follow me!"
          $isabelle.love+=1
          isabelle excited "Okay, great! Lead the way."
        "\"The gym, of course. Nothing like starting the day off with a full-body workout!\"":
          show isabelle skeptical at move_to(.5)
          $mc.strength+=1
          $mc.charisma-=1
          mc "The gym, of course. Nothing like starting the day off with a full-body workout!"
          $isabelle.love-=1
          isabelle cringe "No offense, but you don't look like you work out."
          "Damn it. That was probably a bit over the top."
          mc "Sorry, bad joke. Want me to show you around?"
          isabelle smile "No worries. That would be great, thanks!"
    "\"Did you see me volunteering? No.\"":
      $unlock_stat_perk("love5")
      show isabelle sad at move_to(.5)
      $mc.charisma-=1
      $mc.love-=1
      mc "Did you see me volunteering? No."
      $isabelle.love-=1
      isabelle angry "No need to be rude about it, mate. I was just asking."
      "Ouch, she hit me with the \"mate.\" Oh, well... nothing lost, nothing gained."
      "Not that there was much chance of gaining anything, and English girls are too stuck-up for my taste, anyway."
      show mrsl thinking at appear_from_left(.25)
      show isabelle angry at move_to(.75)
      mrsl thinking "Everything okay here?"
      isabelle sad "My guide ditched me..."
      mrsl surprised "Huh! That's not very nice. Are you sure she's not outside?"
      isabelle concerned "Yes, I checked."
      mrsl thinking "Okay, then..."
      mrsl excited "[mc] is still here. He'll take you."
      isabelle annoyed "I already asked him, and he said no."
      mrsl confident "Nonsense! He'll take you, and that's final."
      show isabelle annoyed at move_to(.5)
      hide mrsl with Dissolve(.5)
      mc "Fine, I guess."
      isabelle skeptical "Great."
      "Ugh, already falling back into my old habits. That didn't take long."
  $quest.day1_take2.advance("lindsey_fall")
  $quest.isabelle_tour.start()
  $process_event("update_state")
  hide isabelle with Dissolve(.5)
  jump goto_school_ground_floor

label school_homeroom_door_isabelle_volunteer:
  show isabelle smile at appear_from_right
  isabelle smile "This school is pretty big. I almost got lost on the way here. Thanks for taking me!"
  show isabelle smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Hey, no problem! Follow me!\"":
      show isabelle smile at move_to(.5)
      mc "Hey, no problem! Follow me!"
      $isabelle.love+=1
      isabelle excited "All right!"
      "It's strange talking to someone who doesn't look like they want to spit me in the face."
      "Just got to keep my head cool and the spaghetti in my pockets now."
    "\"I could show you more than just the school...\"":
      show isabelle smile at move_to(.5)
      jump isabelle_show_more_than_school
    "\"I've changed my mind. You're on your own. Your accent hurts my ears.\"":
      $unlock_stat_perk("love6")
      show isabelle smile at move_to(.5)
      $mc.charisma-=1
      $mc.love-=1
      mc "I've changed my mind. You're on your own. Your accent hurts my ears."
      $isabelle.love-=1
      isabelle cringe "No need to be rude, mate."
      "Hmm... that did not go as planned. Oh, well... nothing lost, nothing gained."
      "Not that there was much chance of gaining anything, and English girls are too stuck-up for my taste, anyway."
      show mrsl thinking at appear_from_left(.25)
      show isabelle cringe at move_to(.75)
      mrsl thinking "Everything okay here?"
      isabelle annoyed "My guide ditched me..."
      mrsl surprised "Huh! That's not very nice. How come?"
      show mrsl surprised at move_to("left")
      show isabelle annoyed at move_to("right")
      menu(side="middle"):
        extend ""
        "\"I was just joking! I'll take her.\"":
          show mrsl surprised at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "I was just joking! I'll take her."
          isabelle annoyed "Not funny."
          "Shit. That was really stupid of me."
          mc "I'm sorry."
          mrsl smile "Glad we got that resolved."
          show isabelle annoyed at move_to(.5)
          hide mrsl with Dissolve(.5)
        "\"Err, this is all a misunderstanding.\"":
          show mrsl surprised at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Err, this is all a misunderstanding."
          isabelle angry "What is?"
          mc "I figured you'd like the bad boy type..."
          isabelle cringe "What's wrong with you?"
          mc "I'm so sorry. I'll show you around."
          mrsl smile "Glad we got that resolved."
          show isabelle annoyed at move_to(.5)
          hide mrsl with Dissolve(.5)
      isabelle annoyed "Fine. Let's just go."
      "Ugh, already falling back into my old habits. That didn't take long."
  $quest.day1_take2.advance("lindsey_fall")
  $quest.isabelle_tour.start()
  $process_event("update_state")
  hide isabelle with Dissolve(.5)
  jump goto_school_ground_floor

label school_homeroom_door_isabelle_haggis_puzzle:
  "[mrsl] locked the door. There's no getting out until we can decide who gets the reward."
  return
