init python:
  class Interactable_school_homeroom_no_globe(Interactable):

    def title(cls):
      return "Globe Holder"

    def description(cls):
      return "Nothing but the yawning void left where the great planet once was. The Death Star got nothing on my monkey wrench."

    def actions(cls,actions):
      if mc.owned_item("globe"):
        actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,homeroom_no_globe|Use What?","school_homeroom_no_globe_use"])
      actions.append("?school_homeroom_no_globe_interact")


label school_homeroom_no_globe_interact:
  "Map of the galaxy. Tiny dot. \"You are here.\""
  "Not anymore."
  return

label school_homeroom_no_globe_use(item):
  if item == "globe":
    $mc.remove_item("globe")
    "With my help, the cosmos has been restored."
    $quest.isabelle_haggis["got_globe"] = False
    $school_homeroom["globe_fixed"] = True
  else:
    "If only my [item.title_lower] could replace the world. It would be a better place."
    $quest.act_one.failed_item("homeroom_no_globe", item)
  return
