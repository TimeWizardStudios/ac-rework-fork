init python:
  class Interactable_school_homeroom_trash_can(Interactable):

    def title(cls):
      return "Trash Can"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Smells of racy perfume and bubblegum. An odd combination."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "puzzle" and not quest.isabelle_haggis["eraser_found"]:
            actions.append(["take","Take","school_homeroom_trash_can_interact_isabelle_haggis"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_homeroom_trash")
      actions.append("?school_homeroom_trash_can_interact")


label isabelle_quest_stolen_maxine_paper_homeroom_trash:
  "What's this? A paper?"
  "No, that's food."
  "..."
  "..."
  "Not bad. Not bad at all."
  return

label school_homeroom_trash_can_interact_isabelle_haggis:
  "Digging through the trash like a rat. Luckily, they're too busy with their own stuff to notice."
  $mc.add_item("eraser")
  $quest.isabelle_haggis["eraser_found"] = True
  return

label school_homeroom_trash_can_interact:
  "Someone left a bra with a snapped band. Few pairs of boobs are powerful enough to do that to a bra."
  return

label isabelle_quest_stolen_maxine_paper_homeroom_bin:
  "What's this? A paper?"
  "No, that's food."
  "..."
  "Not bad. Not bad at all."
  return
