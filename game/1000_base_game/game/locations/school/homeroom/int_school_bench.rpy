init python:
  class Interactable_school_homeroom_school_bench(Interactable):

    def title(cls):
      return "Table"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A thousand students have sat at this table before me. A thousand will sit here after me. Everyone's just a number."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "puzzle" and quest.isabelle_haggis.in_progress:
            if not quest.isabelle_haggis["table_cup"] == 3 or not quest.isabelle_haggis["table_eraser"]:
              actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:isabelle_haggis,homeroom_desk|Use What?","homeroom_use_bench"])
            if not school_homeroom["cupgame_played"] and quest.isabelle_haggis["table_cup"] == 3 and quest.isabelle_haggis["table_eraser"]:
              actions.append("isabelle_quest_haggis_cup_game")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?homeroom_school_bench_interact")


label homeroom_school_bench_interact:
  "Someone's carved a penis into this counter. What a dick move."
  return

label homeroom_use_bench(item):
  if item == "eraser":
    call isabelle_quest_haggis_use_eraser_school_bench(item)
  elif item == "coffee_cup":
    call isabelle_quest_haggis_use_coffee_cup_school_bench(item)
  else:
    "It's never a good idea to put everything on the table. Especially not my [item.title_lower]."
    $quest.isabelle_haggis.failed_item("homeroom_desk", item)
  return
