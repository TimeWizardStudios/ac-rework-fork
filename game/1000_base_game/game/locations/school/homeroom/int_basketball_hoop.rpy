init python:
  class Interactable_school_homeroom_basketball_hoop(Interactable):

    def title(cls):
      return "Basketball Hoop"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_haggis.in_progress:
        return "Three points, two points, or no points. ...  No points."
      else:
        return "The best thing to come out of basketball is Lola Bunny in a pair of gym shorts."

    def actions(cls,actions):
      if mc.owned_item("globe"):
        actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,homeroom_hoop|Use What?","school_homeroom_globe_basketball_hoop_use"])
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            if mc.owned_item("globe"):
              actions.remove(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,homeroom_hoop|Use What?","school_homeroom_globe_basketball_hoop_use"])
            actions.append("?quest_mrsl_table_school_homeroom_basketball_hoop_interact")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            if mc.owned_item("globe"):
              actions.remove(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,homeroom_hoop|Use What?","school_homeroom_globe_basketball_hoop_use"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.isabelle_haggis.started:
        actions.append("?school_homeroom_basketball_hoop_interact")
      actions.append("?school_homeroom_basket_interact")


label school_homeroom_basketball_hoop_interact:
  "Not sure how the jocks managed to get this thing installed. It's all a big conspiracy."
  return

label school_homeroom_basket_interact:
  "I wish I was a little bit taller."
  "I wish I was a baller."
  return

label  school_homeroom_globe_basketball_hoop_use(item):
  if item=="globe":
    "All right, going for a three-pointer..."
    $mc.remove_item(item)
    if (mc.strength>4):
      "And he shoots..."
      #SFX: Basketball Hoop Sound
      ## Shoot
      play sound "bball_hoop"
      $school_homeroom["floor_globe"] = True
      "He scores!"
      if quest.isabelle_haggis.in_progress and quest.isabelle_haggis>="puzzle":
        show isabelle afraid at appear_from_right(.75)
        isabelle afraid "Be careful!"
        isabelle cringe "I can't believe you just used the world as a basketball!"
        show flora excited at appear_from_left(.25)
        flora excited "Nice shot!"
        isabelle thinking "I don't think you should encourage this type of behavior."
        flora confident "It was a three-pointer, though."
        mc "Thank you, [flora]. I'm glad someone appreciates my athletic prowess."
        if not quest.isabelle_haggis["basketball_stats"]:
          $flora.lust+=1
          $isabelle.love-=1
          $quest.isabelle_haggis["basketball_stats"] = True
        hide isabelle with Dissolve(.5)
        hide flora with Dissolve(.5)
    else:
      "And he shoots..."
      #SFX: Bouncing Ball Sound
      ## Miss
      play sound "bball_bounce"
      $school_homeroom["floor_globe"] = True
      "Miss..."
      "Not sure why I expected another outcome."
      if mrsl.at("school_homeroom"):
        show mrsl angry with Dissolve(.5)
        mrsl angry "I was okay with it until you missed."
        hide mrsl with Dissolve(.5)
      if isabelle.at("school_homeroom"):
        show isabelle afraid at appear_from_right(.5)
        isabelle afraid "That almost hit me!"
        mc  "My deepest apologies. I was merely trying to give a tribute to Kobe."
        isabelle cringe "Well, I don't think you should be playing around with expensive school props."
        $isabelle.love-=1
        "She's probably right, and that shot impressed no one."
        hide isabelle with Dissolve(.5)
    if not quest.isabelle_haggis["greasy_bolt_acquired"]:
      "Huh, one of the bolts came loose."
      "This establishment is definitely on its last rope."
      $mc.add_item("greasy_bolt")
      $quest.isabelle_haggis["greasy_bolt_acquired"] = True
  else:
    "He gets into position."
    "He lines up his shot."
    "Takes aim."
    "Cocks his arms back."
    "And shoots!"
    "..."
    "..."
    "Actually, he doesn't do any of that because chucking his [item.title_lower] across the room would be stupid."
    $quest.act_one.failed_item("homeroom_hoop", item)
  return
