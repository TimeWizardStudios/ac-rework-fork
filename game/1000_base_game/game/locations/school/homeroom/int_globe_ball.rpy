init python:
  class Interactable_school_homeroom_globe_ball(Interactable):

    def title(cls):
      return "Globe"

    def description(cls):
      return "Maybe someone will trip over it if I leave it there. Just need to be ready with the camera this time."

    def actions(cls,actions):
      actions.append(["take","Take","school_homeroom_globe_ball_take"])
      actions.append("?school_homeroom_globe_ball_interact")


label school_homeroom_globe_ball_interact:
  "A ball of clay tumbling through the endless darkness of the universe... or across the floor of the homeroom."
  "Either way, you can't help but feel small."
  return

label school_homeroom_globe_ball_take:
  "The world in the palm of my hands."
  "It felt better in my daydreams."
  $school_homeroom["floor_globe"] = False
  $mc.add_item("globe")
  return
