init python:
  class Interactable_school_homeroom_notice_board(Interactable):
    def title(cls):
      return "Bulletin Board"
    def actions(cls,actions):
      actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:maxine_wine,flora_poster|Use What?","school_homeroom_notice_board_maxine_wine_locker_use_item"])

label school_homeroom_notice_board_maxine_wine_locker_use_item(item):
  if item == "flora_poster":
    $quest.maxine_wine["homeroom_poster"] = True
    $mc.remove_item("flora_poster")
    "Hmm... accidentally put this one upside down, but no one's going to read it anyway."
    if quest.maxine_wine["entrance_hall_poster"] + quest.maxine_wine["homeroom_poster"] + quest.maxine_wine["admin_wing_poster"] + quest.maxine_wine["1f_hall_poster"] + quest.maxine_wine["sports_wing_poster"] == 5:
      "Okay, that's all of them. Praise the lord Jesus hallelujah. My ass is sweating more than that hipster up on the cross."
      $quest.maxine_wine.advance("florareward")
  else:
    "This is probably the only board where putting my [item.title_lower] would be acceptable. [mrsl] would like the adventure of it."
    $quest.maxine_wine.failed_item("flora_poster",item)
  return
