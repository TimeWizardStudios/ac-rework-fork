init python:
  class Interactable_school_homeroom_rope(Interactable):

    def title(cls):
      return "Rope"

    def description(cls):
      return "A stairway to heaven of sorts. Except it's not a stairway, and it's only leading two floors up."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "hide":
            actions.append(["go","Clubroom","?quest_lindsey_motive_hide_rope"])
            return
        elif mc["focus"] == "maya_witch":
          if quest.maya_witch == "meet_up" and game.hour not in quest.maya_witch["absurd_timeframe"]:
            actions.append(["go","Clubroom","?quest_maya_witch_meet_up_school_homeroom_rope"])
            return
      else:
        if quest.kate_stepping == "meeting":
          actions.append(["go","Clubroom","quest_kate_stepping_meeting_rope"])
        if quest.fall_in_newfall in ("late","stick_around"):
          actions.append(["go","Clubroom","?quest_fall_in_newfall_late_school_homeroom_rope"])
          return
      if not school_homeroom["rope_first_climb"]:
        actions.append(["go","Climb","school_homeroom_rope_first_climb"])
      else:
        actions.append(["go","Clubroom","goto_school_clubroom"])


label school_homeroom_rope_first_climb:
  "The grappling hook caught something up there."
  "Who knows what treasures await for those unafraid of rope burns and full-body workouts!"
  $school_homeroom["rope_first_climb"] = True
  jump goto_school_clubroom
