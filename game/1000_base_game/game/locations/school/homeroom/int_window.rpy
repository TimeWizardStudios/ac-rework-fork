init python:
  class Interactable_school_homeroom_window(Interactable):

    def title(cls):
      return "Window"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_book.started:
        return "The green foliage of the forest. And here we are, stuck in a concrete box, watching the world instead of experiencing it."
      else:
        return "Ah, the great outdoors! Mankind should never have left the savannah."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "puzzle" and quest.isabelle_haggis.in_progress:
            if (quest.isabelle_haggis["actual_detention"] or mc.owned_item(("greasy_bolt","bolt"))) and not school_homeroom["open_window"]:
              actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,homeroom_window|Use What?","school_homeroom_window_interact_use_item"])
            if school_entrance["window_shortcut"]:
              actions.append(["go","Entrance","goto_school_entrance"])
            elif quest.isabelle_haggis["first_window"]:
              actions.append(["go","Entrance","school_homeroom_window_interact_open"])
            elif school_homeroom["open_window"]:
              actions.append(["go","Entrance","school_homeroom_window_interact_first"])
            else:
              actions.append("?school_homeroom_window_interact_haggis")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            actions.append("?quest_mrsl_table_school_homeroom_window_interact")
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Entrance","quest_kate_wicked_costume_entrance_hall_exit"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_homeroom["open_window"]:
          actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,homeroom_window|Use What?","school_homeroom_window_interact_use_item"])
        elif school_homeroom["open_window"] and mc.owned_item("grappling_hook"):
          actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:act_one,grapple_puzzle|Use What?","school_homeroom_window_grapple"])
        if not school_homeroom["stick_interacted"]:
          actions.append(["take","Take","school_homeroom_window_interact_lindsey_book"])
      actions.append("?school_homeroom_window_interact")
      #if mc.owned_item(("bolt","greasy_bolt")): breaks convention, but removed this


label school_homeroom_window_grapple(item):
  if item == "grappling_hook":
    "Na na na na na na na na na!"
    "Na na na na na na na na na — Robin!"
    "There's no way I'm the main character."
    $mc.remove_item("grappling_hook")
    $school_homeroom["rope"] = True
  else:
    "I'm definitely something of a nimble trickster, but climbing on my [item.title_lower] to the next floor is a feat beyond me."
  return

label school_homeroom_window_interact:
  "It's a beautiful day outside. A mediocre day inside. And a shitty one in... okay, time to think positive... "
  "Boobs."
  return

label school_homeroom_window_interact_lindsey_book:
  "The perfect stick is right there, just outside the window."
  "Screw it, I'll just go outside real quick and grab it."
  show black with Dissolve(.5)
  $game.advance()
  $set_dialog_mode("default_no_bg")
  "Ah, got it! And only a few people gave me weird looks."
  hide black
  $set_dialog_mode()
  $mc.add_item("stick")
  $school_homeroom["stick_interacted"] = True
  return

label school_homeroom_window_interact_use_item(item):
  if item == "monkey_wrench":
    "Okay, got the window open. Need something small to jam the lock, though. Else I'll never get back in."
  elif item=="greasy_bolt":
    "It does seem like this is the right tool to jam the lock mechanism, but it's so greasy and slippery, getting a good grip is impossible."
  elif item=="bolt":
    $mc.remove_item(item)
    #SFX: Lock clicking open
    ## Lock pick
    play sound "lock_click"
    "There we go! An escape route has been established."
    $school_homeroom["open_window"]=True
  else:
    "When the sun hits high noon at the overcast equinox meridian, the great window-opening shall commence!"
    "We, the disciples of my [item.title_lower], beg the sun to cast its rays at the exact angle of Euler's constant."
    "Any second now..."
    "Let the holy light smash against the subatomic gluons of my [item.title_lower] and sing out praise of the window gods!"
    "..."
    "Yes! It's all being revealed before my very eyes!"
    "I'm a giant idiot."
    $quest.act_one.failed_item("homeroom_window", item)
  return

label school_homeroom_window_interact_first:
  show flora confused with Dissolve(.5)
  flora confused "God, I should've eaten more of that haggis. I'm so damn hungry."
  mc "Did you forget to have breakfast?"
  flora annoyed "Someone had eaten my yogurt."
  mc "Wasn't me this time!"
  flora sad "I know. [jo] and I have the same favorite."
  flora sad "Man, I'd kill for some more haggis..."
  mc "Well, I'm out. Stop whining."
  flora angry "..."
  hide flora with dissolve2
  "Perhaps a bowl of haggis could convince her of endorsing someone other than herself..."
  "But you never know with [flora]. She's as unpredictable as they come."
  jump school_homeroom_window_interact_open

label school_homeroom_window_interact_haggis:
  "[mrsl] didn't say anything about escaping the classroom."
  "The window locking mechanism can probably be pushed open with the right tool."
  "Something with a tiny cylindrical shape would be ideal."
  "Although, doing so might land me in actual detention."
  $quest.isabelle_haggis["actual_detention"] = True
  return

label school_homeroom_window_interact_open:
  if quest.isabelle_haggis["first_window"]:
    "Climbing out is easy, but getting back in is much harder."
  $quest.isabelle_haggis["first_window"] = True
  menu(side="middle"):
    "Climb out":
      "The fresh air of freedom. Nothing quite like escaping your confined state and setting out on an adventure."
      "Nothing quite like—"
      show flora annoyed at appear_from_right
      flora annoyed "What are you doing?"
      mc "I'm making my escape!"
      flora annoyed "I'm going to tell [mrsl]."
      mc "Tell her what, exactly?"
      flora concerned "That you're ruining the exercise."
      mc "I'm not ruining anything! I'm just getting some fresh air."
      mc "In the meantime, you can convince [isabelle] to give me the reward."
      flora angry "You're not getting the reward!"
      mc "We'll see."
      flora skeptical "Well, if [mrsl] comes back you're in so much trouble."
      flora skeptical "And I refuse to spend another hour in detention with you."
      mc "I'll be back soon."
      flora angry "Ugh... do not jump out of that window. I'm warning you!"
      "Who knew picking up that haggis for Miss Cranky and Ungrateful would be such a hassle?"
      mc "I'll be back soon!"
      flora angry "If we get in trouble, I'm telling [jo]!"
      hide flora with Dissolve(.5)
      $quest.isabelle_haggis.advance("escape")
      jump goto_school_entrance
    "Not right now":
      return
