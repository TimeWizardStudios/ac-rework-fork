init python:
  class Interactable_school_homeroom_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_homeroom_dollar1_take"])


label school_homeroom_dollar1_take:
  $school_homeroom["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_homeroom_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_homeroom_dollar2_take"])


label school_homeroom_dollar2_take:
  $school_homeroom["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_homeroom_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_homeroom_dollar3_take"])


label school_homeroom_dollar3_take:
  $school_homeroom["dollar3_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_homeroom_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_homeroom_book_interact"])


label school_homeroom_book_interact:
  $school_homeroom["book_taken"] = True
  "Oh, look! It's \"The Complete Works of John Cenastus,\" the poet-wrestler of the 12th century."
  $mc.strength+=1
  return
