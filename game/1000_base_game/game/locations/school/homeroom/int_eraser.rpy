init python:
  class Interactable_school_homeroom_eraser(Interactable):

    def title(cls):
      return "Eraser"

    def description(cls):
      return "Completely erasing my past would've been preferred to another chance... but at least it's the devil I know."

    def actions(cls,actions):
      actions.append("?school_homeroom_eraser_interact")


label school_homeroom_eraser_interact:
  "Erasers are for people who make mistakes."
  "A lifetime supply should keep me covered."
  return
