
init python:
  class Interactable_school_homeroom_cup_left(Interactable):
    
    def title(cls):
      return "Coffee Cup"
    
    def description(cls):
      return "[mrsl] probably takes her coffee extra hot. And with sugar. Lots of sugar."
    
    def actions(cls,actions):
      actions.append("?school_homeroom_cup_left_interact")

init python:
  class Interactable_school_homeroom_cup_right(Interactable):
    
    def title(cls):
      return "Coffee Cup"
    
    def description(cls):
      return "Heretical bean juice. Perfect for the early-morning cultist."
    
    def actions(cls,actions):
      actions.append("?school_homeroom_cup_right_interact")

init python:
  class Interactable_school_homeroom_cup_middle(Interactable):
    
    def title(cls):
      return "Coffee Cup"
    
    def description(cls):
      return "[jo] probably takes her coffee extra black. And with zero sugar because she put it all in [flora]'s cup."
    
    def actions(cls,actions):
      if not school_homeroom["cupgame_played"] and quest.isabelle_haggis["table_cup"]==3 and quest.isabelle_haggis["table_eraser"]:
        actions.append("isabelle_quest_haggis_cup_game")
      else:
        actions.append("?school_homeroom_cup_middle_interact")


label school_homeroom_cup_left_interact:
  "Cupping it against my ear, I can hear the crashing waves of the coffee ocean."
  return

label school_homeroom_cup_middle_interact:
  "Spill a hot cup of coffee all over my groin. Sue the coffee-people. Get a fat settlement check."
  "Not like I'll be using those parts anyway."
  return

label school_homeroom_cup_right_interact:
  "Coffee is for doers and go-getters."
  "That's why I've never had a cup."
  "Besides, [jo] says it'll stunt my growth."
  "Can't take any chances with that."
  return    
