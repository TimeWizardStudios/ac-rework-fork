init python:
  class Interactable_school_homeroom_globe(Interactable):

    def title(cls):
      return "Globe"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif school_homeroom["globe_fixed"]:
        return "Who knew a loser like yours truly would make the world go round?"
      else:
        return "The world at my fingertips. It's a lot rounder than some might think. Why aren't there any flat earth models?"

    def actions(cls,actions):
      if mc.owned_item("monkey_wrench"):
        actions.append(["use_item","Use Item","select_inventory_item", "$quest_item_filter:act_one,homeroom_globe|Use What?","isabelle_quest_haggis_use_monkey_wrench_globe"])
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            if mc.owned_item("monkey_wrench"):
              actions.remove(["use_item","Use Item","select_inventory_item", "$quest_item_filter:act_one,homeroom_globe|Use What?","isabelle_quest_haggis_use_monkey_wrench_globe"])
            actions.append(["interact","Hide","?quest_mrsl_table_school_homeroom_globe_interact"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            if mc.owned_item("monkey_wrench"):
              actions.remove(["use_item","Use Item","select_inventory_item", "$quest_item_filter:act_one,homeroom_globe|Use What?","isabelle_quest_haggis_use_monkey_wrench_globe"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if school_homeroom["globe_fixed"]:
        actions.append("?school_homeroom_globe_interact_fixed")
      else:
        actions.append("?school_homeroom_globe_interact")


label school_homeroom_globe_interact:
  "The attachments at the poles are loose. It's probably global warming melting the ice caps."
  return

label school_homeroom_globe_interact_fixed:
  "Back in its proper orbit. Few cosmic entities show such benevolence with their monkey wrenches."
  return

label  isabelle_quest_haggis_use_monkey_wrench_globe(item):
  if item=="monkey_wrench":
    "With the right tools, anything's easy to take apart."
    "Even the planet."
    "Yes, nukes."
    if mrsl.at("school_homeroom"):
      show mrsl surprised with Dissolve(.5)
      mrsl surprised "I guess some days you just wanna tear the world apart."
      mrsl thinking "I've been there before. So who am I to stop you?"
      mrsl excited "Carry on."
      hide mrsl with Dissolve(.5)
    $quest.isabelle_haggis["got_globe"]=True
    $mc.add_item("globe")
  else:
    "That's how the dinosaurs went extinct. Death by [item.title_lower]."
    $quest.act_one.failed_item("homeroom_globe", item)
  return
