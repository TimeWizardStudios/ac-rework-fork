init python:
  class Interactable_school_homeroom_blackboard(Interactable):

    def title(cls):
      return "Blackboard"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_haggis.started:
        return "This blackboard has had a drastic increase in vulgar doodles since the start of the new year. Wonder why that is?"
      elif quest.day1_take2.started:
        return "Give a man a teacher and a blackboard, and he'll have knowledge for life.\n\nGive a man a piece of chalk, and he'll probably draw a dick."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis >= "instructions" and quest.isabelle_haggis.in_progress:
            actions.append(["interact","Read","?school_homeroom_blackboard_interact_isabelle_haggis"])
            if not quest.isabelle_haggis["wrote_on_blackboard"]:
              actions.append(["interact","Write","school_homeroom_blackboard_interact_write_isabelle_haggis"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "hide":
            actions.append(["interact","Hide","?quest_mrsl_table_school_homeroom_blackboard_interact"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if school_homeroom["wrote_on_blackboard"]:
        actions.append(["interact","Read","?school_homeroom_blackboard_interact"])
      else:
        actions.append(["interact","Write","school_homeroom_blackboard_interact_write"])


label school_homeroom_blackboard_interact:
  "{i}\"Seniors, sign up for your focus classes. Three in total. Sign-up lists available in each classroom.\"{/}"
  "{i}\"Focus classes require 100 points to pass. Regular classes require 20 points to pass.\"{/}"
  return

label school_homeroom_blackboard_interact_write:
  menu(side="middle"):
    "Write something nice":
      $school_homeroom["wrote_on_blackboard"]=True
      $mc.love+=1
      "{i}\"Buried or planted? It's always a matter of perspective.\"{/}"
      $unlock_stat_perk("love4")
    "Write something vulgar":
      $school_homeroom["wrote_on_blackboard"]=True
      $mc.lust+=1
      "{i}\"There's a reason 'bitch' rhymes with 'sandwich.' Get me one.\"{/}"
      $unlock_stat_perk("lust6")
    "Write something for [mrsl]":
      $school_homeroom["wrote_on_blackboard"]=True
      $mrsl.lust+=1
      "{i}\"Roses are red. Violets are blue. [mrsl], you're a 10, and I'm thinking of you.\"{/}"
    "Write nothing":
      pass
  return

label school_homeroom_blackboard_interact_isabelle_haggis:
  "{i}\"Cooperation & Teamwork 101\"{/}"
  "{i}\"One of you will be rewarded today. The reward is a secret, but it is something that will mark you as a problem solver.\"{/}"
  "{i}\"Your task as a group is to decide who gets the reward.\"{/}"
  "{i}\"The door will remain locked until you've found a solution.\"{/}"
  "{i}\"Good luck!\"{/}"
  $quest.isabelle_haggis.advance("puzzle")
  return

label school_homeroom_blackboard_interact_write_isabelle_haggis:
  menu(side="middle"):
    "Write something for [isabelle]":
      $quest.isabelle_haggis["wrote_on_blackboard"]=True
      $isabelle.love+=1
      "{i}\"[isabelle], if I lost my homework on the way to school, your homework is the first I'd try to steal.\"{/}"
    "Write something for [flora]":
      $quest.isabelle_haggis["wrote_on_blackboard"]=True
      $flora.love+=1
      "{i}\"[flora], you're the flickering fluorescent light in the basement of my life.\"{/}"
    "Write something for [mrsl]":
      $quest.isabelle_haggis["wrote_on_blackboard"]=True
      $mrsl.lust+=1
      "{i}\"[mrsl], I'll commit every sin cos of your curves.\"{/}"
    "Write nothing":
      pass
  return
