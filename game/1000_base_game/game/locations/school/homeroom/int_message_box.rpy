init python:
  class Interactable_school_homeroom_message_box(Interactable):

    def title(cls):
      return "Message Box"

    def description(cls):
      # return "Message in a box, no safer — or slower — form of communication."
      return "Message in a box, no safer\n— or slower —\nform of communication."

    def actions(cls,actions):
      if quest.mrsl_bot["note_from_maya_interacted_with"] and mc.owned_item("ball_of_yarn") and not quest.mrsl_bot["ball_of_yarn_delivered"]:
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_bot,further_notes_to_mrsl|Use What?","quest_mrsl_bot_back_and_forth_message_box"])
      elif quest.mrsl_bot["second_note_from_maya_received"] and not quest.mrsl_bot["second_note_from_maya_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_second_note_from_maya"])
      if quest.mrsl_bot["fifth_note_from_mrsl_received"] and not quest.mrsl_bot["fifth_note_from_mrsl_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_fifth_note_from_mrsl"])
      elif quest.mrsl_bot["fourth_note_from_mrsl_received"] and not quest.mrsl_bot["fourth_note_from_mrsl_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_fourth_note_from_mrsl"])
      elif mc.owned_item("fourth_note_to_mrsl"):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_bot,further_notes_to_mrsl|Use What?","quest_mrsl_bot_back_and_forth_message_box"])
      elif quest.mrsl_bot["third_note_from_mrsl_received"] and not quest.mrsl_bot["third_note_from_mrsl_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_third_note_from_mrsl"])
      elif quest.mrsl_bot["note_from_maya_received"] and not quest.mrsl_bot["note_from_maya_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_note_from_maya"])
      elif quest.mrsl_bot["note_from_maxine_received"] and not quest.mrsl_bot["note_from_maxine_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_note_from_maxine"])
      elif mc.owned_item("third_note_to_mrsl"):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_bot,further_notes_to_mrsl|Use What?","quest_mrsl_bot_back_and_forth_message_box"])
      elif quest.mrsl_bot["second_note_from_mrsl_received"] and not quest.mrsl_bot["second_note_from_mrsl_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_second_note_from_mrsl"])
      elif mc.owned_item("second_note_to_mrsl"):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_bot,further_notes_to_mrsl|Use What?","quest_mrsl_bot_back_and_forth_message_box"])
      elif quest.mrsl_bot["note_from_mrsl_received"] and not quest.mrsl_bot["note_from_mrsl_retrieved"]:
        actions.append(["take","Take","quest_mrsl_bot_back_and_forth_note_from_mrsl"])
      elif mc.owned_item("note_to_mrsl"):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_bot,note_to_mrsl|Use What?","quest_mrsl_bot_message_box_message_box"])
      actions.append("?school_homeroom_message_box_interact")


label school_homeroom_message_box_interact:
  "Just another inconspicuous box. [maxine] would be proud of my methods."
  return
