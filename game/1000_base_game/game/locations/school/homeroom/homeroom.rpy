init python:
  class Location_school_homeroom(Location):

    default_title="Homeroom"

    def __init__(self):
      super().__init__()
      self["wrote_on_blackboard"]=False

    def scene(self,scene):
      scene.append([(-12,-19),"school homeroom sky"])
      scene.append([(1135,669),"school homeroom trash","school_homeroom_trash_can"])
      if game.season == 1:
        scene.append([(0,49),"school homeroom window_1",("school_homeroom_window",0,350)])
        scene.append([(411,278),"school homeroom window_2"])
      elif game.season == 2:
        scene.append([(0,49),"school homeroom window_1_autumn",("school_homeroom_window",0,350)])
        scene.append([(411,278),"school homeroom window_2_autumn"])

      if (quest.kate_wicked in ("costume","party")
      or quest.maya_sauce == "dinner"):
        scene.append([(0,269),"school homeroom window_1_night",("school_homeroom_window",29,130)])
        scene.append([(414,395),"school homeroom window_2_night"])

      if ((school_homeroom["rope"]
      or quest.kate_wicked in ("costume","party"))
      and not (quest.mrsl_table in ("morning","hide")
      or quest.jo_washed.in_progress
      or (quest.maya_witch == "meet_up" and game.hour not in quest.maya_witch["absurd_timeframe"] and quest.maya_witch["pulled_up"]))):
        if quest.kate_wicked in ("costume","party"):
          scene.append([(221,337),"school homeroom rope",("school_homeroom_window",-52,172)])
        else:
          scene.append([(221,337),"school homeroom rope",("school_homeroom_rope",0,100)])

      if quest.maxine_wine == "locker" and mc.owned_item("flora_poster") and not quest.maxine_wine["homeroom_poster"]:
        scene.append([(1206,449),"school homeroom notice_board","school_homeroom_notice_board"])
      else:
        scene.append([(1206,449),"school homeroom notice_board"])
        if game.season == 1 and not quest.jo_washed.in_progress:
          if quest.maxine_wine["homeroom_poster"]:
            scene.append([(1252,501),"school homeroom flora_poster"])

      scene.append([(686,410),"school homeroom clock"])
      scene.append([(1525,398),"school homeroom flag"])
      scene.append([(1514,295),"school homeroom basket",("school_homeroom_basketball_hoop",0,0)])
      scene.append([(1657,363),"school homeroom closet",("school_homeroom_bookshelf",0,60)])
      scene.append([(1407,452),"school homeroom door","school_homeroom_door"])
      scene.append([(791,409),"school homeroom blackboard","school_homeroom_blackboard"])
      scene.append([(802,581),"school homeroom desk","school_homeroom_teacher_desk"])

      if quest.isabelle_haggis["got_globe"] and not quest.jo_washed.in_progress:
        scene.append([(607,529),"school homeroom no_globe","school_homeroom_no_globe"])
      else:
        scene.append([(607,529),"school homeroom globe","school_homeroom_globe"])

      if ((not school_homeroom["exclusive"] or "spinach" in school_homeroom["exclusive"])
      and spinach.at("school_homeroom","running")
      and not spinach.talking):
        scene.append([(423,790),"school homeroom spinach","spinach"])

      scene.append([(351,697),"school homeroom desks_front"])
      scene.append([(88,755),"school homeroom desks_back"])
      scene.append([(1073,755),"school homeroom cup_desk","school_homeroom_school_bench"])

      if school_homeroom["floor_globe"] and not quest.jo_washed.in_progress:
        scene.append([(1469,851),"school homeroom globe_ball","school_homeroom_globe_ball"])

      if not (school_homeroom["ball_of_yarn_taken"] or quest.mrsl_table in ("morning","hide") or (quest.fall_in_newfall.in_progress and quest.fall_in_newfall < "stick_around")) or quest.jo_washed.in_progress or quest.maxine_dive == "trade":
        scene.append([(179,723),"school homeroom yarn","school_homeroom_ball_of_yarn"])

      scene.append([(44,687),"school homeroom chairs"])

      if quest.isabelle_haggis.in_progress and quest.isabelle_haggis>="puzzle":
        if quest.isabelle_haggis["table_eraser"]:
          scene.append([(1179,768),"school homeroom eraser","school_homeroom_eraser"])
        if quest.isabelle_haggis["table_cup"] is not None:
          if quest.isabelle_haggis["table_cup"] >= 1:
            scene.append([(1112,724),"school homeroom cup_left","school_homeroom_cup_left"])
          if quest.isabelle_haggis["table_cup"] >= 2:
            scene.append([(1229,721),"school homeroom cup_right","school_homeroom_cup_right"])
          if quest.isabelle_haggis["table_cup"] == 3:
            scene.append([(1174,733),"school homeroom cup_middle","school_homeroom_cup_middle"])

      ###Characters####

      if ((not school_homeroom["exclusive"] or "mrsl" in school_homeroom["exclusive"])
      and mrsl.at("school_homeroom","sitting")
      and not mrsl.talking):
        if game.season == 1:
          scene.append([(889,523),"school homeroom mrsl","mrsl"])
        elif game.season == 2:
          scene.append([(889,523),"school homeroom mrsl_autumn","mrsl"])

      if school_homeroom["message_box"]:
        scene.append([(823,598),"school homeroom message_box","school_homeroom_message_box"])

      if ((not school_homeroom["exclusive"] or "maya" in school_homeroom["exclusive"])
      and maya.at("school_homeroom","sitting")
      and not maya.talking):
        scene.append([(104,623),"school homeroom maya_autumn",("maya",-50,0)])

      if ((not school_homeroom["exclusive"] or "isabelle" in school_homeroom["exclusive"])
      and isabelle.at("school_homeroom","sitting")
      and not isabelle.talking):
        if game.season == 1:
          scene.append([(609,611),"school homeroom isabelle",("isabelle",-25,0)])
        elif game.season == 2:
          scene.append([(609,612),"school homeroom isabelle_autumn",("isabelle",-25,0)])
          if isabelle.equipped_item("isabelle_collar"):
            scene.append([(640,698),"school homeroom isabelle_collar",("isabelle",28,-86)])

      if ((not school_homeroom["exclusive"] or "kate" in school_homeroom["exclusive"])
      and kate.at("school_homeroom", "sitting")
      and not kate.talking):
        if game.season == 1:
          scene.append([(1329,494),"school homeroom kate","kate"])
        elif game.season == 2:
          scene.append([(1329,494),"school homeroom kate_autumn","kate"])

      if not school_homeroom["book_taken"] and not (quest.mrsl_table in ("morning","hide") or quest.jo_washed.in_progress):
        scene.append([(1671,457),"school homeroom book","school_homeroom_book"])

      if ((not school_homeroom["exclusive"] or "flora" in school_homeroom["exclusive"])
      and flora.at("school_homeroom","sitting")
      and not flora.talking):
        if game.season == 1:
          if flora.equipped_item("flora_skirt"):
            scene.append([(1659,447),"school homeroom flora_skirt",("flora",-25,0)])
          else:
            scene.append([(1659,447),"school homeroom flora",("flora",-25,0)])
        elif game.season == 2:
          scene.append([(1659,447),"school homeroom flora_autumn",("flora",-25,0)])

      if not (quest.mrsl_table in ("morning","hide") or quest.jo_washed.in_progress):
        if school_homeroom["dollar1_spawned_today"] == True and not school_homeroom["dollar1_taken_today"]:
          scene.append([(498,748),"school homeroom dollar1","school_homeroom_dollar1"])
        if school_homeroom["dollar2_spawned_today"] == True and not school_homeroom["dollar2_taken_today"]:
          scene.append([(1221,556),"school homeroom dollar2","school_homeroom_dollar2"])
        if school_homeroom["dollar3_spawned_today"] == True and not school_homeroom["dollar3_taken_today"]:
          scene.append([(1573,314),"school homeroom dollar3","school_homeroom_dollar3"])

      scene.append([(0,0),"#school homeroom overlay"])
      if quest.kate_wicked in ("costume","party"):
        scene.append([(0,0),"#school homeroom overlay_night"])

    def find_path(self,target_location):
      if target_location == "school_clubroom":
        return "school_homeroom_rope",dict(marker_offset=(40,160),marker_sector="left_top")
      else:
        return "school_homeroom_door"


label goto_school_homeroom:
  if school_homeroom.first_visit_today:
    $school_homeroom["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_homeroom["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_homeroom["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_homeroom)
  play music "school_theme" if_changed
  return
