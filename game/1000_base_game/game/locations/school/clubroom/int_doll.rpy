init python:
  class Interactable_school_clubroom_doll(Interactable):

    def title(cls):
      return "The Saucer-Eyed Doll"

    def description(cls):
      return "This fucking thing keeps staring at me like I'm dinner.\n\nNever touching it again."

    def actions(cls,actions):
      if quest.maxine_lines == "prison":
        actions.append(["use_item", "Use", "select_inventory_item","$quest_item_filter:maxine_lines,clubroom_doll|Use What?","maxine_lines_doll_prison"])
      actions.append("?school_clubroom_doll_interact")


label school_clubroom_doll_interact:
  "Spinach keeps hissing at it. A cat is never wrong about these things."
  "I wonder what the symbols on its dress mean..."
  "It looks like they've been burnt into it."
  return

label maxine_lines_doll_prison(item):
  if item == "magnet":
    if mc.owned_item("magnet",3):
      $school_clubroom['magnets'] = True
      $mc.remove_item("magnet",3)
      show maxine confident with Dissolve(.5)
      maxine confident "We did it! And I only lost my train of thought once."
      mc "Great. Saved [lindsey] and the school, I'm sure."
      maxine sad "..."
      mc "What?"
      maxine thinking "There are bigger dangers than the saucer-eyed doll still lurking in the shadows."
      show maxine thinking at move_to(.75)
      menu(side="left"):
        extend ""
        "\"We'll pick them off together, one by one!\"":
          show maxine thinking at move_to(.5)
          mc "We'll pick them off together, one by one!"
          $maxine.love+=1
          maxine smile "It's been a lonely road so far, but after this success, I might entrust you with more important matters."
          mc "Thanks! I feel the same way."
        "\"What's next? The fork-toothed duck?\"":
          show maxine thinking at move_to(.5)
          $mc.charisma+=1
          mc "What's next? The fork-toothed duck?"
          maxine laughing "Don't be ridiculous!"
          maxine angry "We could never take it on."
          "There's no way there's a fork-toothed duck..."
          maxine smile "Anyway, good work on this case."
          mc "Thank you."
        "\"We're lucky that you're here watching over us.\"":
          show maxine thinking at move_to(.5)
          mc "We're lucky that you're here watching over us."
          $maxine.lust+=1
          maxine blush "I do my best..."
          mc "And it's appreciated."
          mc "I don't think enough people understand the importance of your work."
          mc "Your newspaper is an important part of staying... ahead of the curve."
          maxine excited "I'm glad someone finds it informative!"
          maxine concerned "It's an integral part of school safety."
      "It's sometimes hard to take her wild theories seriously."
      "But she was right about this doll, and somehow I don't think she planted it there."
      "This raises a lot of new questions... because who did put it there, and why?"
      "..."
      "[maxine] seems like the right person to ask for help in solving my time travelling mystery."
      "She might not have all the answers, but at least she's looking for them in her own way."
      "As it is, I doubt the doll had anything to do with [lindsey]'s poor balance, but I guess we'll see."
      "Let's hope [maxine] was right..."
      hide maxine with Dissolve(.5)
      $quest.maxine_lines.finish()
    else:
      "I need more magnets to form a circle around it..."
  else:
    "Imprisoning the doll with my [item.title_lower] would look kinda funny, but I doubt [maxine] would approve."
    $quest.maxine_lines.failed_item("clubroom_doll",item)
  return


init python:
  class Interactable_school_clubroom_eyelessdoll(Interactable):

    def title(cls):
      return "The Eyeless Doll"

    def description(cls):
      return "I'd burn this foul doll completely, but I'd probably get cursed doing so...\n\nBy [maxine]."

    def actions(cls,actions):
      actions.append("?school_clubroom_eyelessdoll_interact")


label school_clubroom_eyelessdoll_interact:
  "I'm not sure if the lack of eyes made it any less creepy."
  "There's something wrong with it, I can feel it..."
  "It's like a primal fear of some sort."
  return
