init python:
  class Location_school_clubroom(Location):

    default_title="Clubroom"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(-283,0),"school clubroom background"])
      if game.season == 1:
        if False:
          scene.append([(785,263),"school clubroom window_night",("school_clubroom_window",-2,3)])
        else:
          scene.append([(785,263),"school clubroom window",("school_clubroom_window",-2,3)])
      elif game.season == 2:
        if False:
          scene.append([(785,263),"school clubroom window_autumn_night",("school_clubroom_window",-2,3)])
        else:
          scene.append([(785,263),"school clubroom window_autumn",("school_clubroom_window",-2,3)])

      #money
      if school_clubroom["dollar1_spawned_today"] == True and not school_clubroom["dollar1_taken_today"]:
        scene.append([(645,497),"school clubroom dollar1","school_clubroom_dollar1"])
      if school_clubroom["dollar2_spawned_today"] == True and not school_clubroom["dollar2_taken_today"]:
        scene.append([(1593,1047),"school clubroom dollar2","school_clubroom_dollar2"])

      if quest.nurse_venting.started:
        scene.append([(118-8,865),"school clubroom vent","school_clubroom_vent"])
      elif maxine.at("school_clubroom","fishing"):
        scene.append([(118-8,865),"school clubroom vent",("maxine",440,-352)])
      else:
        scene.append([(118-8,865),"school clubroom vent"])

      scene.append([(1352,622),"school clubroom trash_can"])
      scene.append([(1391,522),"school clubroom worldmap"])
      scene.append([(246,76),"school clubroom closet",("school_clubroom_old_locker",10,220)])
      scene.append([(-19,160),"school clubroom blackboard"])
      if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access > "vent" and ((maxine.at("school_clubroom","fishing") and not quest.nurse_venting.started) or quest.nurse_venting.in_progress):
        scene.append([(-233,650),"school clubroom box"])
      else:
        scene.append([(0,624),"school clubroom box"])
      scene.append([(459,601),"school clubroom table"])

      if quest.isabelle_red.in_progress:
        scene.append([(621,776),"school clubroom note","school_clubroom_note"])

      if school_clubroom['books_moved']:
        scene.append([(802,518),"school clubroom books","school_clubroom_pile_of_books"])
      else:
        scene.append([(931,555),"school clubroom books","school_clubroom_pile_of_books"])

      scene.append([(594,542),"school clubroom computer",("school_clubroom_computer",-30,0)])
      scene.append([(681,592),"school clubroom typewriter",("school_clubroom_typewriter",10,0)])
      if not school_clubroom["camera_taken"]:
        scene.append([(900,663),"school clubroom camera","school_clubroom_old_camera"])
      scene.append([(1474,15),"school clubroom bookshelf"])
      scene.append([(1339,718),"school clubroom chest",("school_clubroom_chest",-30,0)])
      scene.append([(1582,897),"school clubroom newspapers",("school_clubroom_papers",-30,-10)])

      #Locator does not show up until ley of land starts####################
      if quest.maxine_lines.started and not quest.maxine_lines.failed and not school_clubroom["locator_taken"]:
        if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access > "vent" and ((maxine.at("school_clubroom","fishing") and not quest.nurse_venting.started) or quest.nurse_venting.in_progress):
          scene.append([(11,707),"school clubroom locator",("school_clubroom_locator",140,60)])
        else:
          scene.append([(243,682),"school clubroom locator","school_clubroom_locator"])

      #Doll does not show up until ley of land starts####################
      if game.season == 1:
        if school_clubroom["doll"]:
          if school_clubroom["doll"] == "eyeless":
            scene.append([(1576,101),"school clubroom eyelessdoll",("school_clubroom_eyelessdoll",-50,150)])
          else:
            scene.append([(1576,101),"school clubroom doll",("school_clubroom_doll",-50,150)])

        if school_clubroom['magnets']:
          scene.append([(1550,83),"school clubroom magnet_prison"])
        if school_clubroom['smoke']:
          scene.append([(1491,58),"school clubroom dollsmoke"])
        if school_clubroom['mirror']:
          scene.append([(835,485),"school clubroom mirror"])
        if school_clubroom['light']:
          scene.append([(845,125),"school clubroom light"])

      if not maxine.talking:
        if maxine.at("school_clubroom","sitting"):
          if game.season == 1:
            scene.append([(607,543),"school clubroom maxine","maxine"])
          elif game.season == 2:
            scene.append([(607,543),"school clubroom maxine_autumn","maxine"])
          scene.append([(571,743),"school clubroom chairback"])
        elif maxine.at("school_clubroom","fishing"):
          scene.append([(260,433),"school clubroom maxine_fishing",("maxine",130,80)])
          scene.append([(571,743),"school clubroom chairback"])

      if not spinach.talking:
        if quest.spinach_seek >= "play" or game.season > 1:
          if spinach.at("school_clubroom","playing"):
            scene.append([(1161,803),"school clubroom cat_yarn","spinach"])
          elif spinach.at("school_clubroom","licking"):
            scene.append([(1204,767),"school clubroom cat_lick","spinach"])
          elif spinach.at("school_clubroom","sleeping"):
            scene.append([(1428,749),"school clubroom cat_sleeping","spinach"])
          elif spinach.at("school_clubroom","hiding"):
            scene.append([(1362,610),"school clubroom cat_hidden","spinach"])
          elif spinach.at("school_clubroom","knitted"):
            scene.append([(1202,803),"school clubroom cat_knitted","spinach"])
        else:
          if spinach.at("school_clubroom","hiding"):
            scene.append([(1362,610),"school clubroom cat_hidden","spinach"])

      scene.append([(0,0),"#school clubroom overlay"])
      if False:
        scene.append([(0,0),"#school clubroom overlay_night"])

    def find_path(self,target_location):
      return "school_clubroom_window" #point to window


label goto_school_clubroom:
  if school_clubroom.first_visit_today:
    $school_clubroom["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_clubroom["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_clubroom)
  return
