init python:
  class Interactable_school_clubroom_old_locker(Interactable):

    def title(cls):
      return "Old Locker"

    def description(cls):
      return "There's a distinct smell coming from this locker. The smell of neglect."

    def actions(cls,actions):
      if quest.mrsl_HOT == "vhs":
        actions.append(["take","Take","school_clubroom_old_locker_take"])
      else:
        actions.append("?school_clubroom_old_locker_interact")


label school_clubroom_old_locker_interact:
  "This old locker is bursting at the seams with random items old and new."
  "Mostly old."
  return

label school_clubroom_old_locker_take:
  "There's all sorts of equipment in here."
  "Leftovers from [maxine]'s many investigations over the year."
  "There's so much stuff in here, but among the rubble there's..."
  "A sack of golf balls, some kind of radio equipment, a children's shopping cart, women's shoes, a VHS player, and an exquisitely handcrafted wooden car."
  "Hmm... perhaps the VHS player works and I can watch the tape at home."
  "That would certainly provide more privacy for... extracurricular activities."
  $mc.add_item("vhs_player")
  $quest.mrsl_HOT.advance("hookitup")
  return
