init python:
  class Interactable_school_clubroom_note(Interactable):

    def title(cls):
      return "Note"

    def description(cls):
      return "A mysterious note on a\npopcorn-greased paper."

    def actions(cls,actions):
      actions.append("?school_clubroom_note_interact")


label school_clubroom_note_interact:
  call quest_isabelle_red_note
  return
