init python:
  class Interactable_school_clubroom_papers(Interactable):

    def title(cls):
      return "Stack of Papers"

    def description(cls):
      if quest.maxine_eggs >= "recon":
        return "Death by a thousand papercuts? There are better ways to go."
      elif quest.maxine_wine.ended:
        return "Paperwork, the bane of joy and excitement."
      else:
        return "A stack of blank papers... all of them as colorless as my old life.\n\nMaybe it's finally time to put some ink on those pages."

    def actions(cls,actions):
      if not mc.owned_item("paper"):
        actions.append(["take","Take","school_clubroom_papers_take"])
      else:
        actions.append("?school_clubroom_papers_interact")


label school_clubroom_papers_take:
  "Paper plane, origami, or short story. The options are endless."
  $mc.add_item("paper")
  return

label school_clubroom_papers_interact:
  "[maxine] will probably mind if I stuff my pockets with her paper. One's enough for now."
  return
