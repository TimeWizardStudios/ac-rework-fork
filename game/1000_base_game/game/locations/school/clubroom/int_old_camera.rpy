init python:
  class Interactable_school_clubroom_old_camera(Interactable):

    def title(cls):
      return "[maxine]'s Camera"

    def description(cls):
      if quest.maxine_wine.finished:
        return "Secrets, embarrassments, triumphs, and downfalls. [maxine]'s camera has a lens for every occasion."
      else:
        return "[maxine] has a stark distrust for anything digital, that's why she keeps this old thing around."

    def actions(cls,actions):
      if quest.maxine_wine.finished and mc.owned_item("maxine_nude") and not mc.owned_item("maxine_other_nude"):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:maxine_wine,dark_chamber|Use What?","quest_maxine_wine_dark_chamber"])
      if quest.maxine_wine.finished:
        actions.append("?school_clubroom_old_camera_interact_two")
      actions.append("?school_clubroom_old_camera_interact_one")


label quest_maxine_wine_dark_chamber(item):
  if item == "maxine_nude":
    if quest.kate_desire == "camera":
      "How does this old thing even work again?"
      "Hmm... let's try taking a photo of the photo... because that would be typical [maxine]."
    else:
      "I had to google it, but it would be very much like [maxine] to leave cryptic messages on a nude."
      "\"Dark chamber\" of course refers to the latin word for camera."
      "The question is, what happens if you take a photo of the photo..."
    window hide
    $mc.remove_item("maxine_nude")
    show white with Dissolve(.15)
    play sound "camera_snap"
    hide white with Dissolve(.15)
    $mc.add_item("maxine_other_nude")
    window auto
    "Huh..."
    if quest.kate_desire == "camera":
      "I guess that's one way to send somebody a nude..."
      $quest.kate_desire.advance("delivery")
  else:
    "If I take enough photos of my [item.title_lower], I'll become known as the [item.title_lower]-man on Instagram."
    $quest.maxine_wine.failed_item("dark_chamber",item)
  return

label school_clubroom_old_camera_interact_one:
  "This heirloom of a bygone era is the reason all pictures in the school's newspaper are so grainy."
  return

label school_clubroom_old_camera_interact_two:
  "A flash in the pan or a flash in the cam? Regardless, life's too short to wait for the photos."
  return
