init python:
  class Interactable_school_clubroom_pile_of_books(Interactable):

    def title(cls):
      return "Pile of Books"

    def description(cls):
      if quest.maxine_eggs.ended:
        return "All these books must've come from somewhere, but it's impossible to say where.\n\nHalf of these authors don't even seem real..."
      elif quest.maxine_eggs >= "recon":
        return "These are some of the weirdest titles... [maxine] sure knows how to pick 'em."
      else:
        return "There's no telling where [maxine] found these books."

    def actions(cls,actions):
      if school_clubroom["books_moved"] and quest.maxine_lines.finished and not school_clubroom["doll"] == "eyeless":
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:maxine_lines,books|Use What?","school_clubroom_pile_of_books_use_mirror"])
      if not school_clubroom["microplastics_taken"]:
        actions.append(["take","Take","school_clubroom_pile_of_books_take"])
      else:
        actions.append("school_clubroom_pile_of_books_interact")


label school_clubroom_pile_of_books_use_mirror(item):
  if item == "pocket_mirror":
    $school_clubroom["light"] = True
    $school_clubroom["mirror"] = True
    $mc.remove_item(item)
    "The sunlight catches the mirror perfectly, focusing the beams."
    $school_clubroom["smoke"] = True
    "..."
    "It's burning the face of the doll."
    "Can't say I'm sad to—"
    window hide
    $school_clubroom["doll"] = "eyeless"
    play sound "UFO"
    show school clubroom doll_left_eye:
      xpos 1627 ypos 133
      parallel:
        easein 3.0 xpos 1022 ypos 416
      parallel:
        easein 0.5 xoffset 50 yoffset 50
        easein 1.0 xoffset -25 yoffset -25
        easein 1.0 xoffset 25 yoffset 50
        easein 0.5 xoffset 0 yoffset 0
      linear 0.25 alpha 0.0
    show school clubroom doll_right_eye as right_eye:
      xpos 1612 ypos 141
      parallel:
        easein 2.5 xpos 1024 ypos 416
      parallel:
        easein 0.5 xoffset -25 yoffset -50
        easein 0.5 xoffset 25 yoffset 25
        easein 1.0 xoffset -50 yoffset -50
        easein 0.5 xoffset 0 yoffset 0
      linear 0.25 alpha 0.0
    pause 3.0
    window auto
    "Huh? What was that?"
    hide school clubroom doll_left_eye ## This is just to avoid the animation if the player is skipping
    hide right_eye
    "Wait a minute — did the eyes of the doll just fly out the window?!"
    "Damn, I must be more tired than I feel..."
    "Oh, well."
    $school_entrance["crop"] = True
    $school_clubroom["light"] = False
    $school_clubroom["smoke"] = False
    $school_clubroom["mirror"] = False
    $mc.add_item(item)
  else:
    "..."
    $quest.maxine_lines.failed_item("books",item)
  return

label school_clubroom_pile_of_books_take:
  "Hold up! Isn't this the book that [flora] always talks about?"
  $school_clubroom["microplastics_taken"] = True
  $mc.add_item("microplastics")
  return

label school_clubroom_pile_of_books_interact:
  menu(side="middle"):
    "Move":
      $school_clubroom["books_moved"] = not school_clubroom["books_moved"]
    "Examine":
      if quest.maxine_eggs.ended:
        "Hmm... let's see what we got here..."
        "{i}\"Flying Saucers, Pans, and Cutlery — an Abducted Chef's Guide to Anger Management\" by Bim Mea & U. P. Scotty{/}"
        "{i}\"Hoggard's School of Lizardry\" by Zark Luckerberg{/}"
        "{i}\"Illuminati: the Theory of Changing Light Bulbs\" by Yureka Archimedeson{/}"
      elif quest.maxine_eggs >= "recon":
        "Hmm... these look interesting..."
        "{i}\"Pyramid Schemes and Other Types of Alien Architecture\" by Dorra T. X. P. Laura{/}"
        "{i}\"Rasputin: Losing the 'Ras' and Winning the Russian Presidency, an Autobiography\"{/}"
        "{i}\"Vatican's Secret Fashion Show — the Truth Beneath the Robe\" by Richard Pope{/}"
      elif quest.maxine_wine.ended:
        "Hmm... what are these titles?"
        "{i}\"Red Hot Chilipeppers, and Other Ways to Burn Your Eardrums\"{/}"
        "{i}\"Slapjacks and Paina Colada, a Masochist's Guide to an Enjoyable Diet\"{/}"
        "{i}\"Area 52 and Other Lesser Known Charter Destinations\"{/}"
      else:
        "Hmm... some interesting titles here..."
        "{i}\"Voodoo and Other Cures for Jealousy\" by T. S. Hex{/}"
        "{i}\"Moon Landings & Moon Dust, an Astronaut's Favorite Trips\" by Buzz Goodyear{/}"
        "{i}\"Pancakes, Pizzas, and Pies — the Flat Earther's Guide to a Well-Rounded Diet\" by Abso Lue Tefowl{/}"
  return
