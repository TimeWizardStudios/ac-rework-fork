init python:
  class Interactable_school_clubroom_window(Interactable):

    def title(cls):
      return "Window"

    def description(cls):
      return "Trapped between a dizzying vertigo and... well, not getting out, I guess."

    def actions(cls,actions):
      if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
        actions.append(["go","Homeroom","clubroom_window_paper"])
      elif mc["focus"]:
        if mc["focus"] == "nurse_venting":
          if quest.nurse_venting in ("stuck","not_stuck") and mc.owned_item("walkie_talkie") and mc.owned_item("maxine_camera"):
            actions.append(["go","Homeroom","quest_nurse_venting_clubroom_window_go"])
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "meeting" and not quest.kate_stepping["mildly_important"]:
            actions.append(["go","Homeroom","?quest_kate_stepping_meeting_window"])
            return
      actions.append(["go","Homeroom","goto_school_homeroom"])


label clubroom_window_paper:
  "Leaving with [maxine]'s priced paper will get me in so much trouble."
  "She doesn't mess around."
  $mc.remove_item("paper")
  if quest.nurse_venting in ("stuck","not_stuck") and mc.owned_item("walkie_talkie") and mc.owned_item("maxine_camera"):
    jump quest_nurse_venting_clubroom_window_go
  jump goto_school_homeroom
