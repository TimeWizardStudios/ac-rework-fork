init python:
  class Interactable_school_clubroom_locator(Interactable):

    def title(cls):
      return "Ley Line Locator"

    def description(cls):
      return "Some kind of happy meal toy. Cheap plastic trash."

    def actions(cls,actions):
      actions.append("?school_clubroom_locator_interact")


label school_clubroom_locator_interact:
  if maxine.at("school_clubroom"):
    show maxine afraid with Dissolve(.5)
  else:
    show maxine afraid at appear_from_left
  maxine afraid "That's a priceless artifact. Hands off."
  hide maxine with Dissolve(.5)
  return
