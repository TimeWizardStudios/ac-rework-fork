init python:
  class Location_school_hook_up_table(Location):

    default_title = "Clubroom"

    def scene(self,scene):
      scene.append([(0,0),"maxine avatar events hooking_up bg"])
      if self["router_connected"]:
        scene.append([(514,119),"maxine avatar events hooking_up router_connected"])
      scene.append([(461,417),"maxine avatar events hooking_up power_supply",("school_hook_up_table_power_supply",0,100)])
      if self["interactive"] and not self["ass_object"] == "power_supply_cable":
        scene.append([(675,705),"maxine avatar events hooking_up power_supply_cable",("school_hook_up_table_power_supply",-99,-188)])
      if not self["cable"]:
        scene.append([(839,129),"maxine avatar events hooking_up cable",("school_hook_up_table_cable",0,60)])
      if not self["vr_goggles"]:
        scene.append([(1094,131),"maxine avatar events hooking_up vr_goggles",("school_hook_up_table_vr_goggles",0,50)])
      if not self["ass_object"] == "watermelon":
        if self["watermelon_cut"]:
          scene.append([(1306,264),"maxine avatar events hooking_up watermelon1",("school_hook_up_table_watermelon",0,60)])
        else:
          scene.append([(1306,264),"maxine avatar events hooking_up watermelon2",("school_hook_up_table_watermelon",0,60)])

      if self["interactive"]:
        scene.append([(929,386),"maxine avatar events hooking_up maxine2_body"])
        scene.append([(935,966),"maxine avatar events hooking_up maxine2_panties"])
        scene.append([(948,429),"maxine avatar events hooking_up maxine2_bra"])
        scene.append([(948,425),"maxine avatar events hooking_up maxine2_top"])
        scene.append([(930,1014),"maxine avatar events hooking_up maxine2_shorts"])
        scene.append([(827,205),"maxine avatar events hooking_up maxine2_head" + str(self["expression"])])
        if self["vr_goggles"]:
          scene.append([(833,233),"maxine avatar events hooking_up maxine2_vr_goggles",("school_hook_up_table_vr_goggles",-60,120)])
        else:
          scene.append([(881,327),"maxine avatar events hooking_up maxine2_glasses"])
        if self["watermelon_slice"]:
          scene.append([(863,384),"maxine avatar events hooking_up maxine2_watermelon1"])
          scene.append([(863,384),"maxine avatar events hooking_up maxine2_watermelon1b",("school_hook_up_table_watermelon_slice",0,40)])
        scene.append([(820,152),"maxine avatar events hooking_up maxine2_hat"])
        scene.append([(1049,418),"maxine avatar events hooking_up maxine2_necklace"])
        scene.append([(944,424),"maxine avatar events hooking_up maxine2_blouse"])
        scene.append([(901,433),"maxine avatar events hooking_up maxine2_arms3_n"])
        scene.append([(900,425),"maxine avatar events hooking_up maxine2_arms3_c"])
        if self["cable"]:
          scene.append([(1084,535),"maxine avatar events hooking_up maxine2_cable1"])
          scene.append([(1097,535),"maxine avatar events hooking_up maxine2_cable2",("school_hook_up_table_cable",0,60)])
        if self["grab"]:
          scene.append([(1071,525),"maxine avatar events hooking_up mc_right_hand1"])
        if self["ass_object"] == "watermelon":
          if self["watermelon_cut"]:
            scene.append([(1147,492),"maxine avatar events hooking_up maxine2_watermelon2",("school_hook_up_table_watermelon",0,50)])
          else:
            scene.append([(1146,492),"maxine avatar events hooking_up maxine2_watermelon3",("school_hook_up_table_watermelon",0,50)])
          scene.append([(1347,568),"maxine avatar events hooking_up mc_right_hand2"])
        elif self["ass_object"] == "watermelon_slice":
          scene.append([(1097,549),"maxine avatar events hooking_up maxine2_watermelon4",("school_hook_up_table_watermelon_slice",25,40)])
          scene.append([(1153,559),"maxine avatar events hooking_up mc_right_hand3"])
        elif self["ass_object"] == "power_supply_cable":
          scene.append([(675,634),"maxine avatar events hooking_up maxine2_power_supply_cable",("school_hook_up_table_power_supply",-312,-117)])


init python:
  class Interactable_school_hook_up_table_power_supply(Interactable):

    def title(cls):
      return "Power Supply"

    def actions(cls,actions):
      if school_hook_up_table["ass_object"] == "power_supply_cable":
        actions.append("school_hook_up_table_power_supply_remove")
      else:
        actions.append("school_hook_up_table_power_supply_interact")


label school_hook_up_table_power_supply_interact:
  if school_hook_up_table["watermelon_slice_interacted"]:
    mc "Okay, I think we can plug this in now..."
    if school_hook_up_table["ass_object"] == "watermelon_slice":
      $school_hook_up_table["watermelon_slice"] = True
    $school_hook_up_table["ass_object"] = "power_supply_cable"
    $school_hook_up_table["expression"] = 5
    with vpunch
    mc "..."
  else:
    if school_hook_up_table["ass_object"] == "watermelon_slice":
      $school_hook_up_table["watermelon_slice"] = True
    $school_hook_up_table["ass_object"] = "power_supply_cable"
    $school_hook_up_table["expression"] = 5
    with vpunch
    mc "..."
    mc "How's that?"
    $school_hook_up_table["expression"] = 5
    if school_hook_up_table["watermelon_slice"]:
      maxine "Mmmph..."
    else:
      maxine "That's... q-quite deep..."
    mc "You didn't like that? I mean, the transmitter is in your colon somewhere.{space=-80}"
    $school_hook_up_table["expression"] = 5
    if school_hook_up_table["watermelon_slice"]:
      maxine "Mmmph..."
    else:
      maxine "Y-yes, but..."
    mc "It's still not connected, is it? Maybe it needs more conductivity?"
    label school_hook_up_table_power_supply_remove:
    $school_hook_up_table["ass_object"] = False
    $school_hook_up_table["expression"] = 2
  return


init python:
  class Interactable_school_hook_up_table_watermelon(Interactable):

    def title(cls):
      return "Watermelon"

    def actions(cls,actions):
      if school_hook_up_table["ass_object"] == "watermelon":
        actions.append("school_hook_up_table_watermelon_remove")
      else:
        actions.append("school_hook_up_table_watermelon_interact")
      if not school_hook_up_table["watermelon_cut"]:
        if school_hook_up_table["ass_object"] == "watermelon":
          actions.append(["consume","Cut Slice","school_hook_up_table_watermelon_cut_slice_ass"])
        else:
          actions.append(["consume","Cut Slice","school_hook_up_table_watermelon_cut_slice_table"])


label school_hook_up_table_watermelon_cut_slice_table:
  mc "How about a snack?"
  $school_hook_up_table["watermelon_cut"] = True
  mc "..."
  $school_hook_up_table["watermelon_slice"] = True
  with vpunch
  maxine "Mmmph!"
  mc "What's that? You want the watermelon in your butt?"
  return

label school_hook_up_table_watermelon_cut_slice_ass:
  mc "I think you've talked enough."
  $school_hook_up_table["watermelon_cut"] = True
  mc "..."
  $school_hook_up_table["watermelon_slice"] = True
  with vpunch
  maxine "Mmmph!"
  "It feels good to finally shut her up for a bit."
  return

label school_hook_up_table_watermelon_interact:
  if school_hook_up_table["ass_object"] == "watermelon_slice":
    $school_hook_up_table["watermelon_slice"] = True
  $school_hook_up_table["ass_object"] = "watermelon"
  $school_hook_up_table["expression"] = 4
  with vpunch
  if school_hook_up_table["watermelon_slice"]:
    maxine "Mmmph!"
  else:
    maxine "What?! No! That doesn't go there!"
  mc "Where there's a will... and enough vaseline... there's a way!"
  $school_hook_up_table["expression"] = 3
  if school_hook_up_table["watermelon_slice"]:
    maxine "Mmmph!"
  else:
    maxine "No, that is all wrong! You're ruining it!"
  return

label school_hook_up_table_watermelon_remove:
  $school_hook_up_table["ass_object"] = False
  $school_hook_up_table["expression"] = 2
  return


init python:
  class Interactable_school_hook_up_table_watermelon_slice(Interactable):

    def title(cls):
      return "Watermelon Slice"

    def actions(cls,actions):
      if school_hook_up_table["ass_object"] == "watermelon_slice":
        actions.append("school_hook_up_table_watermelon_slice_interact_ass")
      else:
        actions.append("school_hook_up_table_watermelon_slice_interact_mouth")


label school_hook_up_table_watermelon_slice_interact_mouth:
  $school_hook_up_table["watermelon_slice_interacted"]+=1
  if school_hook_up_table["watermelon_slice_interacted"] == 1:
    mc "Maybe some more conductivity wouldn't hurt?"
    $school_hook_up_table["watermelon_slice"] = False
    $school_hook_up_table["ass_object"] = "watermelon_slice"
    $school_hook_up_table["expression"] = 6
    with vpunch
    maxine "Hnngh!"
    mc "What? We need the electricity to reach the transmitter."
    mc "Electricity travels well through liquids like watermelon juice."
    $school_hook_up_table["expression"] = 7
    maxine "You're... ugh... right..."
  elif school_hook_up_table["watermelon_slice_interacted"] == 2:
    mc "Maybe some more juices?"
    $school_hook_up_table["watermelon_slice"] = False
    $school_hook_up_table["ass_object"] = "watermelon_slice"
    $school_hook_up_table["expression"] = 6
    with vpunch
    maxine "Ooooh!"
    $school_hook_up_table["expression"] = 7
    maxine "If you think so... I can't really tell..."
    mc "I definitely think so."
  else:
    mc "I don't think we need more conductivity, but..."
    $school_hook_up_table["watermelon_slice"] = False
    $school_hook_up_table["ass_object"] = "watermelon_slice"
    $school_hook_up_table["expression"] = 6
    with vpunch
    maxine "Owww!"
    mc "I have a question for you — do you like tasting your own ass?"
    $school_hook_up_table["expression"] = 3
    maxine "That is not—"
    $school_hook_up_table["ass_object"] = False
    $school_hook_up_table["watermelon_slice"] = True
    $school_hook_up_table["expression"] = 4
    if not school_hook_up_table["cable"]:
      $school_hook_up_table["grab"] = True
    with vpunch
    maxine "!!!"
    mc "That was a yes or no question."
    $school_hook_up_table["expression"] = 3
    maxine "Mmmph!"
    mc "Sounds like a yes to me."
    $school_hook_up_table["grab"] = False
  return

label school_hook_up_table_watermelon_slice_interact_ass:
  $school_hook_up_table["expression"] = 7
  maxine "How's... it looking?"
  mc "I think we've got plenty of conductivity now."
  $school_hook_up_table["expression"] = 3
  maxine "Well, then you—"
  $school_hook_up_table["ass_object"] = False
  $school_hook_up_table["watermelon_slice"] = True
  $school_hook_up_table["expression"] = 4
  if not school_hook_up_table["cable"]:
    $school_hook_up_table["grab"] = True
  with vpunch
  maxine "Mmmph!"
  mc "I agree. Too much talking again."
  $school_hook_up_table["grab"] = False
  return


init python:
  class Interactable_school_hook_up_table_vr_goggles(Interactable):

    def title(cls):
      return "VR Goggles"

    def actions(cls,actions):
      if school_hook_up_table["vr_goggles"]:
        actions.append("school_hook_up_table_vr_goggles_remove")
      else:
        actions.append("school_hook_up_table_vr_goggles_interact")


label school_hook_up_table_vr_goggles_interact:
  mc "Is this thing even on?"
  $school_hook_up_table["expression"] = 2
  if school_hook_up_table["watermelon_slice"]:
    maxine "Mmmph!"
  else:
    maxine "Yes! It's needed in order to orient myself in cyberspace."
  mc "Well, on it goes!"
  $school_hook_up_table["vr_goggles"] = True
  "..."
  "That looks almost like a blindfold..."
  return

label school_hook_up_table_vr_goggles_remove:
  $school_hook_up_table["vr_goggles"] = False
  $school_hook_up_table["expression"] = 2
  return


init python:
  class Interactable_school_hook_up_table_cable(Interactable):

    def title(cls):
      return "Cable"

    def actions(cls,actions):
      if school_hook_up_table["cable"]:
        actions.append("school_hook_up_table_cable_remove")
      else:
        actions.append("school_hook_up_table_cable_interact")


label school_hook_up_table_cable_interact:
  $school_hook_up_table["expression"] = 4
  if school_hook_up_table["watermelon_slice"]:
    maxine "Mmmph!"
  else:
    maxine "Hey, don't touch that cable!"
  mc "Don't worry, I'm something of a tech guy myself."
  $school_hook_up_table["cable"] = True
  $school_hook_up_table["expression"] = 3
  mc "..."
  $school_hook_up_table["expression"] = 3
  if school_hook_up_table["watermelon_slice"]:
    maxine "Mmmph..."
  else:
    maxine "I believe you've got me tangled up by mistake..."
  mc "It looks fine to me."
  return

label school_hook_up_table_cable_remove:
  $school_hook_up_table["cable"] = False
  $school_hook_up_table["expression"] = 2
  return
