init python:
  class Interactable_school_clubroom_chest(Interactable):
    def title(cls):
      return "[maxine]'s Chest"
    def description(cls):
      return "[maxine] always wears a key around her neck. My guess is that it fits this lock."
    def actions(cls,actions):
      actions.append("?school_clubroom_chest_interact")
      
label school_clubroom_chest_interact:
  if maxine.at("school_clubroom"):
    show maxine skeptical at appear_from_left
    maxine skeptical "What do you think you're doing?"
    mc "Err... just admiring your chest!"
    maxine flirty "Deja vu!"
    hide maxine with Dissolve(.5)
  else:
    "This chest fills me with a morbid curiosity."
    "Could be anything from a rotting corpse to a pirate's treasure inside."
    #"It looks sturdy, but maybe I could break it open..."
    #menu(side="middle"):
      #extend ""
      #"Break it" if False: #if STR >X (should be really high) #TBD
        #"Okay, let's see... man versus lid."
        #maxine_chest "Creak..."
        #maxine_chest "Groan..." #add in some visual effects
        #maxine_chest "Crack!"
        #"Hell yeah! See these guns? Solid steel."
        ##$achievement.gunmetal.unlock()
        #"The lock broke. Once the lid closes, it probably won't open again."
        ##Show MaxineChest01
      #"Live and let live":
        #"Better try to get her key instead."
  return
