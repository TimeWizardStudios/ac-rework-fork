init python:
  class Interactable_school_clubroom_typewriter(Interactable):

    def title(cls):
      return "Typewriter"

    def description(cls):
      if quest.maxine_eggs.ended:
        return "This typewriter has a backspace key — if you press it, a big inky smiley ruins the document.\n\nThat's [maxine]'s way of pranking people who don't know how typewriters work."
      elif quest.maxine_eggs >= "recon":
        return "The typewriter was invented in 1878. This model isn't quite as old, but the principle stays the same.\n\n\"Don't fuck up!\" Which ironically is my only real skill in life."
      else:
        return "An antique typewriter. Mistakes mean you'll have to start over. Feels familiar, somehow..."

    def actions(cls,actions):
      actions.append("?school_clubroom_typewriter_interact")


label school_clubroom_typewriter_interact:
  "It's out of paper. My masterpiece will have to wait."
  return
