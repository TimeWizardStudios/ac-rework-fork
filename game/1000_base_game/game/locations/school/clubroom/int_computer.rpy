### school_clubroom_computer interactable #####################################

init python:
  class Interactable_school_clubroom_computer(Interactable):
    def title(cls):
      return "Computer"
    def description(cls):
      return "Nothing soft or micro about this bad boy. Oh, and [maxine]'s computer is pretty cool too."
    def actions(cls,actions):
      actions.append("?school_clubroom_computer_interact")

label school_clubroom_computer_interact:
  "Ah, would you look at that... the mother-modem — the actual heart of the hard drive."
  return
