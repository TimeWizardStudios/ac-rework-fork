init python:
  class Interactable_school_clubroom_vent(Interactable):

    def title(cls):
      return "Vent"

    def description(cls):
      return "Looks dark down there.\nNot much to see."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "nurse_venting":
          if quest.nurse_venting == "bottle":
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:nurse_venting,clubroom_vent|Use What?","quest_nurse_venting_clubroom_vent_use_item"])
      actions.append("?school_clubroom_vent_interact")


label school_clubroom_vent_interact:
  call quest_nurse_venting_clubroom_vent_interact
  return
