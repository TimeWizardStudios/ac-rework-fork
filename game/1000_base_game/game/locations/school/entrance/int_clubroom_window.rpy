init python:
  class Interactable_school_entrance_clubroom_window(Interactable):

    def title(cls):
      return "Window"

    def description(cls):
      return "[maxine] is burning the midnight\noil up there...\n\n...and possibly other things with her portable incinerator."

    def actions(cls,actions):
      actions.append("?school_entrance_clubroom_window_interact")


label school_entrance_clubroom_window_interact:
  call quest_kate_wicked_costume_exterior_window
  return
