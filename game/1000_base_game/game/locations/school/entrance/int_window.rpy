init python:
  class Interactable_school_entrance_window(Interactable):

    def title(cls):
      return "Window"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.fall_in_newfall == "assembly":
        return
      else:
        return "A window into the belly of the beast. There's something special about watching souls being tormented from the outside."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
      else:
        if quest.fall_in_newfall == "assembly":
          return
      if school_gym["ladder_taken"]:
        actions.append(["go","Homeroom","goto_school_homeroom"])
      else:
        actions.append(["go","Homeroom","?school_entrance_window_interact"])


label school_entrance_window_interact:
  "The window is unlocked, but it's too high up for me to climb. Need a ladder or something."
  return
