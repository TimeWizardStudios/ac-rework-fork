init python:
  class Interactable_school_entrance_rope(Interactable):

    def title(cls):
      return "Rope"

    def description(cls):
      return "A stairway to heaven of sorts.\n\nExcept it's not a stairway, and it's only leading two floors up."

    def actions(cls,actions):
      actions.append(["go","Homeroom","school_entrance_rope_interact"])


label school_entrance_rope_interact:
  call quest_kate_wicked_costume_exterior_rope
  return
