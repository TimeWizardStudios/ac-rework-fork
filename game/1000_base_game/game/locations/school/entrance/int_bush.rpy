init python:
  class Interactable_school_entrance_bush(Interactable):

    def title(cls):
      return "Bush"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.berb_fight.started:
        return "Leaves, branches, thorns. Three things you don't want inside your underwear."
      elif quest.kate_search_for_nurse.started:
        return "This bush always has this sweet and musky scent. And yet, no flowers bloom here."
      elif quest.lindsey_book.started:
        return "Leaves, thorns, probably a whole bunch of nasty insects. Need to dive in just to be sure."
      else:
        return "Some people mind a little bush. I don't discriminate."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.isabelle_tour.started and not school_entrance["bush_interacted"]:
          actions.append(["take","Take","school_entrance_bush_interact_stick_first_time"])
        if quest.kate_search_for_nurse in ("follow_spinach","find_the_nurse"):
          actions.append("school_entrance_bush_quest_kate_search_for_nurse")
      if quest.berb_fight.started:
        actions.append("?school_entrance_bush_interact_berb_fight")
      if quest.lindsey_book.started:
        actions.append("?school_entrance_bush_interact_lindsey_book")
      if quest.kate_search_for_nurse.finished:
        actions.append("?school_entrance_bush_quest_kate_search_for_nurse_finished")
      actions.append("?school_entrance_bush_interact")


label school_entrance_bush_interact:
  "Always be sure to check your local bush for a certain Italian assassin."
  "His name is only whispered in hushed tones."
  "Luigi."
  return

label school_entrance_bush_interact_lindsey_book:
  "The perfect hiding spot. No one's going to look for me in a thorn bush. "
  "Shit. Ow!"
  return

label school_entrance_bush_quest_kate_search_for_nurse:
  $spinach.alternative_locations["school_entrance"] = ""
  "This is why [maxine] sometimes calls her cat \"Detective [spinach].\" The little critter has led me right to a perfect hiding spot."
  mc "[nurse], are you in there?"
  show nurse surprised at appear_from_left(.5)
  nurse surprised "Goodness me, I almost fell over in fright."
  nurse surprised "Please, don't sneak up on people like that!"
  show nurse surprised at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Sorry, didn't mean to scare you.\"":
      show nurse surprised at move_to(.5)
      mc "Sorry, didn't mean to scare you."
      $nurse.love+=1
      nurse concerned "I'm just jumpy these days. Not to worry."
      mc "You look a little stressed out. Are you okay?"
      nurse blush "I'm mostly fine, thank you."
      mc "So, did you find what you were looking for in there?"
      nurse concerned "Almost."
      mc "Almost?"
      nurse blush "I'll get it next time..."
      mc "Hmm... did I interrupt something?"
      nurse afraid "No, no! I was just collecting rare herbs!"
      mc "Okay, good. Everyone needs a little time to themselves."
      mc "Herb collecting is a fine way to relax."
      nurse smile "I'm glad you agree."
    "\"What are you doing in this bush?\"":
      show nurse surprised at move_to(.5)
      mc "What are you doing in this bush?"
      nurse annoyed "Just, err, collecting herbs and... other things. Nothing nefarious!"
      mc "What kind of herbs?"
      nurse thinking "Medical, um... Nymphoria Heartus is pretty common here... and err, Venus Vagenis."
      mc "Can't say I've heard of them."
      nurse annoyed "They're quite rare."
      mc "Thought you just said they're common here?"
      nurse thinking "I don't remember saying that. Those herbs are very rare!"
    "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"This is not the first time I've caught you.\"":
      show nurse surprised at move_to(.5)
      mc "This is not the first time I've caught you."
      $nurse.lust+=1
      nurse afraid "I promise I wasn't doing anything uncouth!"
      mc "So, if I crawl inside this bush, I won't find anything suspicious?"
      nurse annoyed "Probably not..."
      mc "Mhm... I'll be the judge of that."
      nurse afraid "There's no need! The thorns are sharp. I'll be patching you up all afternoon."
      mc "You know, I'm keeping my eye on you."
      nurse concerned "You don't have to do that."
      mc "I think I do. Remember that photo I have?"
      nurse annoyed "Yes..."
      mc "Well, then. You agree you can't really be trusted, right?"
      mc "Imagine if [jo] found out what a pervert the school nurse is?"
      nurse afraid "Please don't tell her! I can't afford to lose this job!"
      mc "I won't. At least, not for now."
      nurse concerned "Thank you, [mc]."
      mc "Of course... there are stipulations for every deal."
      nurse sad "What kind of...?"
      mc "Nothing too bad. I just need your phone number."
      nurse thinking "My phone number? Why?"
      mc "As I said, I need to keep an eye on you."
      nurse concerned "Oh. I suppose that's all right."
      nurse concerned "Here you go."
      $mc.add_phone_contact(nurse,silent=False)
  nurse smile "Anyway! I have to get going. Busy times, indeed."
  mc "Not so fast."
  show nurse annoyed with Dissolve(.5)
  mc "You still need to tell me what you're so stressed out about."
  mc "Everyone knows you're a hopeless stress eater, and I found a trail of candy wrappers."
  nurse afraid "Sorry, I'm so thoughtless when I get nervous. Please, don't tell the janitor! I'll pick them all up."
  mc "Don't worry, I already took care of it."
  nurse neutral "Thank goodness. I don't usually litter, I promise!"
  mc "What about the cat? Why did you lock [spinach] in the supply closet?"
  nurse sad "She was out on her own. I just needed a decoy..."
  nurse sad "I shouldn't have locked her in the closet. But it was only for a little while. Is she okay?"
  mc "The cat is fine. Now, can you tell me why you're so anxious?"
  nurse annoyed "There's this one student..."
  mc "It's [kate], isn't it?"
  nurse surprised "How did you know?"
  mc "She asked me to help her find you."
  nurse afraid "..."
  show nurse afraid at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'm taking you to her right now.\"":
      $unlock_stat_perk("lust20")
      show nurse afraid at move_to(.5)
      $mc.lust+=1
      mc "I'm taking you to her right now."
      $nurse.lust+=2
      nurse afraid "Oh my goodness."
      mc "Come on, let's get a move on, convict."
      nurse surprised "I, err, I didn't do anything bad. You have to tell her that."
      mc "We'll see when we get there."
      nurse neutral "Okay, fine. That's fine."
      mc "Why are you so nervous anyway?"
      nurse annoyed "You don't know what she's like..."
      mc "I think I have a pretty good idea, but I guess we're about to find out."
      window hide
      show nurse annoyed at disappear_to_right
      pause 0.5
      window auto
      $quest.kate_search_for_nurse.advance("meet_kate")
      show black with Dissolve(.25)
      pause 2.0
      call goto_school_nurse_room
      jump kate_quest_search_for_nurse_meet_kate
    "\"Don't worry, I'm not giving you up to her.\"":
      $unlock_stat_perk("love20")
      show nurse afraid at move_to(.5)
      $mc.love+=1
      mc "Don't worry, I'm not giving you up to her."
      $nurse.love+=2
      nurse blush "Oh... thank you."
      mc "I wouldn't let my worst enemy fall into [kate]'s hands."
      nurse blush "That's a relief. I'd like you to have one of these herbs as thanks."
      nurse blush "They're both perfect for brews."
      show nurse blush at move_to(.25)
      menu(side="right"):
        extend ""
        "{image=items nymphoria_heartus}|Nymphoria Heartus":
          show nurse blush at move_to(.5)
          $mc.add_item("nymphoria_heartus")
        "{image=items venus_vagenis}|Venus Vagenis":
          show nurse blush at move_to(.5)
          $mc.add_item("venus_vagenis")
      mc "Thanks! I don't usually drink tea, but I'm sure I can find some use for these."
      nurse smile "They're both very potent, so be careful!"
      show nurse smile at disappear_to_left
      "Feels good to do the right thing for once. Who knows what [kate]'s got on the poor woman."
      $quest.kate_search_for_nurse.finish()
      return

label school_entrance_bush_interact_stick_first_time:
  "People already call me a weirdo, and crawling through the bushes outside the school certainly doesn't help my case."
  "However, no one's going to bother me when this stick is in my possession. One end is slick and sticky, and also smells funny."
  $mc.add_item("stick")
  $school_entrance["bush_interacted"] = True
  return

label school_entrance_bush_quest_kate_search_for_nurse_finished:
  "A thorn in my side, a stone in my shoe — a venture into the bush and through."
  return

label school_entrance_bush_interact_berb_fight:
  "Nettles and an old rusty steering wheel. The story of how it ended up here is probably more interesting than the item itself."
  return
