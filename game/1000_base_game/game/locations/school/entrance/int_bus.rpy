init python:
  class Interactable_school_entrance_bus(Interactable):

    def title(cls):
      return "Bus"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif school_entrance["bus_jacklyn"] or school_entrance["bus_lust"] or school_entrance["bus_love"]:
        return "A statement on wheels."
      else:
        return "What was the bus driver's name again?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "starred":
            actions.append("quest_kate_desire_bus_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if school_entrance["bus_jacklyn"] or school_entrance["bus_lust"] or school_entrance["bus_love"]:
        actions.append("?school_entrance_bus_interact_jacklyn_statement")
      if quest.kate_search_for_nurse.started:
        actions.append("?school_entrance_bus_interact_kate_search_for_nurse")
      actions.append("?school_entrance_bus_interact")


label school_entrance_bus_interact:
  "[jo] used to tell me that I'd end up a bus driver unless my grades improved."
  "Joke's on her. I ended up unemployed."
  return

label school_entrance_bus_interact_kate_search_for_nurse:
  "Taking the bike every morning would probably be the wiser choice, but then the bus driver would have no one to judge."
  "Charity judgment is important for any small town."
  return

label school_entrance_bus_interact_jacklyn_statement:
  "Bye school bus. Hello art piece."
  return
