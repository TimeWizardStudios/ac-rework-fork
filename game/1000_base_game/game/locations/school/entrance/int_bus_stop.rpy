init python:
  class Interactable_school_entrance_bus_stop(Interactable):

    def title(cls):
      return "Bus Stop"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return "Too late to go back now."
      elif quest.berb_fight.started:
        return "Home, school, home again. The vicious cycle never ends."
      elif quest.lindsey_book.started:
        return "Honestly, let's just go home. This is all too much stress to handle."
      elif quest.flora_jacklyn_introduction.started:
        return "There's mostly kids from the nearby elementary school riding this bus. [maya] is one of the few seniors that still use public transportation."
      elif quest.day1_take2.started:
        return "In my freshman year, [jo] used to drive me to school. Then, as soon as she entered her office, I'd get on the bus and ride back home. I needed those extra hours to grind the new expansions."
      else:
        return "The wheels on the bus go...\n\nround and round...\n\nround and round."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Home","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "statement":
            actions.append(["go","Home","?school_entrance_bus_stop_jacklyn_statement_statement"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Home","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "nurse_venting":
          if quest.nurse_venting == "bottle" and home_kitchen["water_bottle_taken"]:
            actions.append(["go","Home","?quest_nurse_venting_entrance_bus_stop_go"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume":
            actions.append(["go","Home","quest_kate_wicked_costume_entrance_hall_exit"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "hospital":
            actions.append(["go","Home","?quest_lindsey_angel_hospital_school_entrance_bus_stop"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Home","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Home","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
          elif quest.mrsl_bot == "meeting":
            actions.append(["go","Home","?quest_mrsl_bot_meeting_bus_stop"])
            return
      else:
        if quest.day1_take2.in_progress:
          actions.append(["go","Home","?school_entrance_bus_stop_interact_day1_take2"])
          return
        if "endnurse" > quest.kate_desire > "gym":
          actions.append(["go","Home","?school_entrance_bus_stop_kate_desire"])
          return
        if (quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started:
          actions.append(["go","Home","quest_jo_washed_not_started_bedroom"])
        if quest.fall_in_newfall == "assembly":
          actions.append(["go","Home","?quest_fall_in_newfall_assembly_school_entrance_bus_stop"])
          return
#     if quest.lindsey_angel.started:
#       actions.append(["go","Take the bus","school_entrance_bus_stop_go"])
#     else:
      actions.append(["go","Home","goto_home_kitchen"])


label school_entrance_bus_stop_interact_day1_take2:
  "Skipping school on the first day would look bad. Besides, [jo] knows my old tricks."
  return

label school_entrance_bus_stop_interact_kate_blowjob_dream_school:
  "Too late to go back now."
  return

label school_entrance_bus_stop_jacklyn_statement_statement:
  "The last bus of the day left long ago. [jacklyn] has to drive me home when we're done."
  return

#label school_entrance_bus_stop_go:
# menu(side="middle"):
#   "Where should I go?"
#   "Go home":
#     jump goto_home_kitchen
#   "Go to the marina":
#     jump goto_marina
#   "Nevermind":
#     return
