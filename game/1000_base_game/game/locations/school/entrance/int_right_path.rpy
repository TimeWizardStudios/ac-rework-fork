init python:
  class Interactable_school_entrance_right_path(Interactable):

    def title(cls):
      return "Path"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Is this the path to success?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Park","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "statement":
            actions.append(["go","Park","?school_entrance_right_path_jacklyn_statement_statement"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Park","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "nurse_venting":
          if quest.nurse_venting == "bottle":
            actions.append(["go","Park","?quest_nurse_venting_entrance_right_path_go"])
            return
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume":
            actions.append(["go","Park","?quest_maxine_hook_night_school_entrance_right_path"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Park","goto_school_park"])
        elif mc["focus"] == "flora_walk":
          if quest.flora_walk == "park":
            actions.append(["go","Park","quest_flora_walk_park_upon_entering"])
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Park","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Park","kate_blowjob_dream_random_interact_locked_location"])
          elif quest.mrsl_bot == "meeting":
            actions.append(["go","Park","?quest_mrsl_bot_meeting_right_path"])
            return
      else:
        if quest.maxine_hook == "night":
          actions.append(["go","Park","?quest_maxine_hook_night_school_entrance_right_path"])
          return
        if (quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started:
          actions.append(["go","Park","?school_entrance_right_path_jacklyn_statement_statement"])
          return
        if quest.fall_in_newfall == "assembly":
          actions.append(["go","Park","?quest_fall_in_newfall_assembly_school_entrance_right_path"])
          return
      actions.append(["go","Park","goto_school_park"])


label school_entrance_right_path_jacklyn_statement_statement:
  "I'm something of a badass myself..."
  "...but going into the woods after dark? That's just stupid."
  return
