init python:
  class Interactable_school_entrance_beaver_light(Interactable):

    def title(cls):
      return "Beaver"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Ragged fur and frothing mouth, this miscreant is the definition of an animal criminal."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
      if quest.lindsey_book["beaver"] < 4:
        actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:lindsey_book,beaver_light|Use What?","school_entrance_beaver_interact_use"])
        actions.append("?school_entrance_beaver_interact_lindsey_book")


label school_entrance_beaver_interact_lindsey_book:
  if (mc.strength>=10):
    show black with fadehold
    hide black with Dissolve(.5)
    "Puh, that sucker is fast. It ran into the school."
    $quest.lindsey_book["beaver"] = 4
  else:
    "Damn it. Those legs are short but so fast."
    "It keeps slipping out of my grip. Need something hard to hit it with."
    $x = range(1,4)
    $x.remove(quest.lindsey_book["beaver"])
    $random.shuffle(x)
    $quest.lindsey_book["beaver"] = x.pop()
  return

label school_entrance_beaver_interact_use(item):
  if item.id == "monkey_wrench":
    show black with fadehold
    #SFX: Beaver noises
    #SFX: Thud/hitting noise
    #remove textbox
    $set_dialog_mode("default_no_bg")
    mc "Got you!"
    ## Hit plus beaver sound
    queue sound ["hit", "<to 0.7>squeaking_animal"]
    hide black with Dissolve(.5)
    $set_dialog_mode()
    "Clipped it with the wrench."
    "I know I hurt it."
    $quest.lindsey_book["beaver"] = 4
    "It's trying to escape into the school."
  else:
    "Those teeth will chew right through my [item.title_lower]. Need to find something else."
    $quest.lindsey_book.failed_item("beaver_light",item)
  return
