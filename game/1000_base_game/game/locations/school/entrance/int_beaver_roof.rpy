init python:
  class Interactable_school_entrance_beaver_roof(Interactable):
    def title(cls):
      return "Beaver"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_entrance_beaver_roof_interact")

label school_entrance_beaver_roof_interact:
  ""
  return
