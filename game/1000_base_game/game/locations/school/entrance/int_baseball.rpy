init python:
  class Interactable_school_entrance_baseball(Interactable):

    def title(cls):
      return "Baseball"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Not quite a boomerang, but a deadly weapon nonetheless."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take","Take","school_entrance_baseball_take"])
      actions.append("?school_entrance_baseball_interact")


label school_entrance_baseball_take:
  $school_entrance["baseball_picked_up"] = True
  $mc.add_item("baseball")
  return

label school_entrance_baseball_interact:
  "The best things in life aren't free."
  "Baseballs, hookers, and cocaine — all expensive."
  return
