init python:
  class Location_school_entrance(Location):

    default_title="Newfall High"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      #Background
      if (game.season == 1
      or quest.lindsey_voluntary == "dream"):
        scene.append([(0,0),"school entrance sky"])
        scene.append([(-108,151),"school entrance trees"])
        scene.append([(0,55),"school entrance school"])
      elif game.season == 2:
        scene.append([(0,0),"school entrance sky_autumn"])
        scene.append([(-108,151),"school entrance trees_autumn"])
        scene.append([(0,71),"school entrance school_autumn"])

      #Night Sky
      if (school_entrance["night_sky"]
      or (quest.maxine_hook in ("dig","night") and not 19 > game.hour > 6)
      or quest.kate_wicked == "costume"
      or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.jo_washed == "fundraiser_over"
      or quest.lindsey_angel in ("escape","hospital")
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "meeting"):
        scene.append([(0,0),"school entrance sky_night"])
        scene.append([(36,151),"school entrance trees_night"])

      #Window
      if (quest.isabelle_haggis >= "escape"
      and not (quest.jacklyn_statement == "statement"
      or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.jo_washed.in_progress
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream")):
        scene.append([(530,652),"school entrance window","school_entrance_window"])

      scene.append([(6,685),"school entrance path",("school_entrance_path",100,0)])
      scene.append([(1770,671),"school entrance right_path",("school_entrance_right_path",-75,0)])
      if (game.season == 1
      or quest.lindsey_voluntary == "dream"):
        scene.append([(73,647),"school entrance bush","school_entrance_bush"])
      elif game.season == 2:
        scene.append([(73,647),"school entrance bush_autumn","school_entrance_bush"])
      scene.append([(890,696),"school entrance door","school_entrance_door"])
      scene.append([(1510,627),"school entrance carport","school_entrance_bus_stop"])

      #Flora
      if not flora.talking:
        if flora.at("school_entrance", "standing"):
          scene.append([(131,732),"school entrance flora","flora"])
        elif flora.at("school_entrance", "car_wash"):
          scene.append([(1527,753),"school entrance flora_car_wash","flora"])

      #Bus
      if not quest.jo_washed.in_progress:
        scene.append([(1557,703),"school entrance bus","school_entrance_bus"])
        if school_entrance["bus_jacklyn"]:
          scene.append([(1559,703),"school entrance bus_jacklyn","school_entrance_bus"])
        elif school_entrance["bus_love"]:
          scene.append([(1559,703),"school entrance bus_love","school_entrance_bus"])
        elif school_entrance["bus_lust"]:
          scene.append([(1559,703),"school entrance bus_lust","school_entrance_bus"])

      #Jacklyn
      if not jacklyn.talking:
        if jacklyn.at("school_entrance", "standing"):
          if game.season == 1:
            scene.append([(1518,751),"school entrance jacklyn","jacklyn"])
          elif game.season == 2:
            scene.append([(1518,751),"school entrance jacklyn_autumn","jacklyn"])

      #Ladder
      if school_entrance["window_shortcut"] and not (quest.jacklyn_statement == "statement" or quest.maxine_hook == "night" or quest.kate_wicked == "costume" or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started) or quest.jo_washed.in_progress):
        scene.append([(511,673),"school entrance ladder","school_entrance_ladder"])

      #Rope
      if quest.kate_wicked == "costume" and school_entrance["rope"]:
        scene.append([(484,597),"school entrance rope",("school_entrance_rope",34,17)])

      #Spinach
      if not spinach.talking:
        if spinach.at("school_entrance","standing"):
          scene.append([(371,779),"school entrance spinach","spinach"])

      #Lindsey
      if not lindsey.talking:
        if lindsey.at("school_entrance", "sitting"):
          scene.append([(533,904),"school entrance lindsey","lindsey"])

      #Baseball
      if quest.berb_fight>= "chase" and not (school_entrance["baseball_picked_up"] or quest.jo_washed.in_progress):
        scene.append([(545,800),"school entrance baseball","school_entrance_baseball"])

      #Lindsey Book Beaver
      if quest.lindsey_book == "beaver":
        if quest.lindsey_book["beaver"]==1:
          scene.append([(117,888),"school entrance wbeaver1","school_entrance_beaver_light"])
        if quest.lindsey_book["beaver"]==2:
          scene.append([(685,1024),"school entrance wbeaver2","school_entrance_beaver_light"])
        if quest.lindsey_book["beaver"]==3:
          scene.append([(1358,884),"school entrance wbeaver_3","school_entrance_beaver_light"])

      #Car Wash
      if quest.jo_washed.in_progress:
        scene.append([(1588,810),"school entrance buckets_and_towels",("school_entrance_car_wash",70,-16)])
        if quest.jo_washed != "fundraiser_over":
          scene.append([(1652,794),"school entrance cars",("school_entrance_car_wash",-120,0)])

      #SprayCans
      else:
        if quest.jacklyn_statement == "statement" or school_entrance["bus_jacklyn"] or school_entrance["bus_lust"] or school_entrance["bus_love"]:
          if (game.season == 1
          or quest.lindsey_voluntary == "dream"):
            scene.append([(1592,873),"school entrance spraycans","school_entrance_spraycans"])
          elif game.season == 2:
            scene.append([(1592,873),"school entrance spraycans_autumn","school_entrance_spraycans"])

      #Beaver Operation B.O.B
      if not berb.talking:
        if berb.at("school_entrance","roof"):
          if quest.lindsey_book.ended:
            scene.append([(567,313),"school entrance beaver_roof","berb"])
        elif berb.at("school_entrance","ground"):
          scene.append([(567,786),"school entrance beaver_ground","berb"])

      #Misc
      if not (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.jo_washed.in_progress
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        if school_entrance["dollar1_spawned_today"] == True and not school_entrance["dollar1_taken_today"]:
          scene.append([(1109,891),"school entrance dollar1","school_entrance_dollar1"])
        if school_entrance["dollar2_spawned_today"] == True and not school_entrance["dollar2_taken_today"]:
          scene.append([(1759,907),"school entrance dollar2","school_entrance_dollar2"])
        if school_entrance["dollar3_spawned_today"] == True and not school_entrance["dollar3_taken_today"]:
          scene.append([(308,758),"school entrance dollar3","school_entrance_dollar3"])

        if school_entrance["crop"] or school_entrance["crop_today"]:
          scene.append([(1436,950),"school entrance cropcircle","school_entrance_cropcircle"])

      #Lamps
      scene.append([(468,604),"school entrance leftmost_lamp"])
      scene.append([(790,616),"school entrance left_lamp"])
      scene.append([(1038,615),"school entrance right_lamp"])
      scene.append([(1321,604),"school entrance rightmost_lamp"])

      #Night Overlay
      if (school_entrance["night_sky"]
      or (quest.maxine_hook in ("dig","night") and not 19 > game.hour > 6)
      or quest.kate_wicked == "costume"
      or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.jo_washed == "fundraiser_over"
      or quest.lindsey_angel in ("escape","hospital")
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "meeting"):
        scene.append([(-10,70),"#school entrance nightoverlay"])

        #Window Lights
        scene.append([(221,308),"school entrance attic_window_light"])
        if quest.kate_wicked == "costume":
          if school_entrance["rope"]:
            scene.append([(530,504),"school entrance clubroom_window_light_open"])
          else:
            scene.append([(530,513),"school entrance clubroom_window_light","school_entrance_clubroom_window"])
        elif school_entrance["window_shortcut"] and not (quest.jacklyn_statement == "statement" or quest.maxine_hook == "night" or ((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started) or quest.jo_washed.in_progress):
          scene.append([(538,663),"school entrance left_window_light"])
        scene.append([(1380,665),"school entrance right_window_light"])

        #Lamp Lights
        scene.append([(409,586),"#school entrance leftmost_lamp_light"])
        scene.append([(758,607),"#school entrance left_lamp_light"])
        scene.append([(1006,607),"#school entrance right_lamp_light"])
        scene.append([(1261,586),"#school entrance rightmost_lamp_light"])


    def find_path(self,target_location):
      if target_location == "school_forest_glade":
        return "school_entrance_path",dict(marker_offset=(-40,0),marker_sector="mid_bottom")
      elif target_location in ("school_park","marina","hospital"):
        return "school_entrance_right_path",dict(marker_offset=(75,-15))
      elif target_location.id.startswith("school_"):
        return "school_entrance_door"
      else: ## if target_location.id.startswith("home_"):
        return "school_entrance_bus_stop"


label goto_school_entrance:
  if school_entrance.first_visit_today:
    $school_entrance["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_entrance["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_entrance["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  if quest.lindsey_book["beaver"] < 4:
    $quest.lindsey_book["beaver"] = random.randint(1,3)
  call goto_with_black(school_entrance)
  play music "<from 35>outdoor_wind"
  #play music ["<silence 3>","school_theme"]
  return
