init python:
  class Interactable_school_entrance_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_entrance_dollar1_take"])


label school_entrance_dollar1_take:
  $school_entrance["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_entrance_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_entrance_dollar2_take"])


label school_entrance_dollar2_take:
  $school_entrance["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_entrance_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_entrance_dollar3_take"])


label school_entrance_dollar3_take:
  $school_entrance["dollar3_taken_today"] = True
  $mc.money+=20
  return
