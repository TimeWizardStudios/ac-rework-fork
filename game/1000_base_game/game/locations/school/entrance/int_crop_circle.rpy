init python:
  class Interactable_school_entrance_cropcircle(Interactable):

    def title(cls):
      return "Crops Circle"

    def description(cls):
      return "Some kind of weird circles burnt into the grass..."

    def actions(cls,actions):
      actions.append("?school_entrance_cropcircle_interact")


label school_entrance_cropcircle_interact:
  "Huh! There's lines of tiny text burnt into the grass."
  "{i}\"Our gratitude, earth-giant.\"{/}"
  "{i}\"Your solar-powered reflector glass has freed us from our textile prison.\"{/}"
  "{i}\"Several earth-cycles ago, an unusually short earth-giant captured two of our space vessels and bound them to the face of an earth-giant replica.\"{/}"
  "{i}\"Our scientists have yet to figure out why this unjust imprisonment occurred, and we'll make further investigations once we reach orbit.\"{/}"
  "{i}\"We are aware that the actions of short earth-giants are often excused in your culture, and as such, will not seek vengeance upon said earth-giant.\"{/}"
  "{i}\"Ever since the deprival of our liberties occurred, our efforts in amplifying our distress signals have caused a slight imbalance in your planet's magnetic field.\"{/}"
  "{i}\"We trust that you, [mc] — the great liberator of our space vessels —, are capable of restoring this imbalance.\"{/}"
  "{i}\"We leave in peace.\"{/}"
  $achievement.leave_in_peace.unlock()
  $school_entrance["crop"] = False
  $school_entrance["crop_today"] = True
  return
