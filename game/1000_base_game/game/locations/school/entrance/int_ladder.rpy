init python:
  class Interactable_school_entrance_ladder(Interactable):

    def title(cls):
      return "Ladder"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Imagine looking out your classroom window and seeing my mug staring back at you."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?school_entrance_ladder_interact_haggis")


label school_entrance_ladder_interact_haggis:
  "Surely, people will think that the janitor is doing some sort of maintenance work here."
  "Hopefully, the janitor will think that someone authorized put the ladder here."
  "Exploiting the trust and good will of people. I'm finally the villain I set out to be."
  return
