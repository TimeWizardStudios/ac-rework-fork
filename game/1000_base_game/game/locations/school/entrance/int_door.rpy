init python:
  class Interactable_school_entrance_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return "I have to go inside."
      elif quest.berb_fight.started:
        return "Bulky and carved from two big pieces of oak. Perfect to keep people from escaping."
      elif quest.kate_search_for_nurse.started:
        return "Step through and be forever changed. For the worse, of course, but changed nonetheless."
      elif quest.lindsey_book.started:
        return "After finally making my escape, that's surely the wrong way."
      elif quest.flora_jacklyn_introduction.started:
        return "A yawning mouth, ready to consume everything. The stench of anxiety and emotional suffering oozing out like a bad breath."
      elif quest.day1_take2.started:
        return "Abandon all hope, ye who enter here."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Enter School","goto_school_ground_floor"])
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "statement":
            actions.append(["go","Enter School","?school_entrance_door_jacklyn_statement_statement"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Enter School","goto_school_ground_floor"])
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume":
            actions.append(["go","Enter School","?quest_kate_wicked_costume_exterior_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Enter School","goto_school_ground_floor"])
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "hospital":
            actions.append(["go","Enter School","?quest_lindsey_angel_hospital_school_entrance_door"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "meeting":
            actions.append(["go","Enter School","quest_mrsl_bot_meeting"])
      else:
        if quest.day1_take2 == "day1_take2":
          actions.append(["go","Enter School","school_entrance_door_interact_day1_take2"])
        if quest.maxine_hook == "night":
          actions.append(["go","Enter School","?quest_maxine_hook_night_school_entrance_door"])
          return
        if (quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started:
          actions.append(["go","Enter School","?quest_isabelle_dethroning_trap_locked"])
          return
        if quest.fall_in_newfall == "assembly":
          actions.append(["go","Enter School","quest_fall_in_newfall_assembly_school_entrance_door"])
        if quest.maya_sauce == "school" and not quest.maya_sauce["dick_eating_contest"]:
          actions.append(["go","Enter School","quest_maya_sauce_school_upon_entering"])
      actions.append(["go","Enter School","goto_school_ground_floor"])


label school_entrance_door_interact_day1_take2:
  "Through the Gates of Hell... as I make my way to the homeroom... through the normie lines..."
  jump goto_school_ground_floor

label school_entrance_door_jacklyn_statement_statement:
  "Locked. At least the [guard] is doing something right."
  "Huh?"
  "..."
  "Could've sworn something just moved inside..."
  "..."
  "Probably just a shadow."
  return
