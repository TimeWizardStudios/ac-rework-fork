init python:
  class Interactable_school_entrance_car_wash(Interactable):

    def title(cls):
      return "Car Wash"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
      if quest.jo_washed.in_progress:
        actions.append(random.choice(quest_jo_washed_interact_lines))
