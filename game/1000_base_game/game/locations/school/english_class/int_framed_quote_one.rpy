init python:
  class Interactable_school_english_class_framed_quote_one(Interactable):

    def title(cls):
      return "Framed Quote"

    def description(cls):
      return "This is why they invented comic books."

    def actions(cls,actions):
      actions.append("?school_english_class_framed_quote_one_interact")


label school_english_class_framed_quote_one_interact:
  "{i}\"A picture is worth a thousand words, but a thousand words is merely two pages. Try reading the whole story, maybe you'll get the picture.\" — Unknown{/}"
  return
