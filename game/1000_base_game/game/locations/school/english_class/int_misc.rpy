init python:
  class Interactable_school_english_class_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_english_class_dollar1_take"])


label school_english_class_dollar1_take:
  $school_english_class["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_english_class_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_english_class_dollar2_take"])


label school_english_class_dollar2_take:
  $school_english_class["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_english_class_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_english_class_book_interact"])


label school_english_class_book_interact:
  $school_english_class["book_taken"] = True
  "Oh! I've had this book on my wishlist forever!"
  "{i}\"The Art of Winking.\"{/}"
  "Wait, I misread that title."
  $mc.charisma+=1
  return
