init python:
  class Interactable_school_english_class_cactus(Interactable):

    def title(cls):
      return "Cactus"

    def description(cls):
      if quest.lindsey_motive == "lure":
        return "A cactus like any other. Nothing remarkable about it."
      elif quest.jo_day == "supplies":
        return "I'm sure [jo] would appreciate the shape and size of this one."

    def actions(cls,actions):
      if quest.lindsey_motive == "lure":
        actions.append("?quest_lindsey_motive_lure_english_class_plant_interact")
      elif quest.jo_day == "supplies":
        actions.append(["take","Take","quest_jo_day_supplies_cactus_take"])
        actions.append("?quest_jo_day_supplies_cactus_interact")
