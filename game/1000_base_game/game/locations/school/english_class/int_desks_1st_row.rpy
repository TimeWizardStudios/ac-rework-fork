init python:
  class Interactable_school_english_class_desks_1st_row(Interactable):

    def title(cls):
      return "Student Desks First Row"

    def description(cls):
      return "PP"

    def actions(cls,actions):
      actions.append("?school_english_class_desks_1st_row_interact")


label school_english_class_desks_1st_row_interact:
  "P"
  return
