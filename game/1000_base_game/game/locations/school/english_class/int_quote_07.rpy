init python:
  class Interactable_school_english_class_quote_07(Interactable):

    def title(cls):
      return "Framed Quote"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "Easy to say. Pretty sure the ground there is covered with legos."
      else:
        return "Not sure if Dante would agree."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_quote_07_interact")


label school_english_class_quote_07_interact:
  "{i}\"If you are going through hell, keep going.\" — Winston Churchill{/}"
  return
