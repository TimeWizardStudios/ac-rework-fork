init python:
  class Interactable_school_english_class_clock(Interactable):

    def title(cls):
      return "Clock"

    def description(cls):
      return "PP"

    def actions(cls,actions):
      actions.append("?school_english_class_clock_interact")


label school_english_class_clock_interact:
  "P"
  return
