init python:
  class Interactable_school_english_class_note(Interactable):

    def title(cls):
      return "Note"

    def description(cls):
      return "A barely visible note on a black piece of paper."

    def actions(cls,actions):
      actions.append("?school_english_class_note_interact")


label school_english_class_note_interact:
  call quest_isabelle_red_black_note
  return
