init python:
  class Interactable_school_english_class_desk(Interactable):

    def title(cls):
      return "Teacher's Desk"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "This desk is impressive but ultimately useless. Much like Mr. Brown's English lessons."
      else:
        return "Polished mahogany. Hand-carved. Antique. It takes a Master of Fine Arts degree to fully appreciate its beauty."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "revenge" and not mc.owned_item(("high_tech_lockpick","safety_pin")):
            actions.append(["take","Take","school_english_class_desk_take"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.mrsl_bot == "message_box" and not quest.mrsl_bot["some_paper_and_pen"]:
          actions.append(["take","Take","quest_mrsl_bot_message_box_teachers_desk"])
        if quest.maxine_dive == "armor" and not quest.maxine_dive["armor_adhesive"]:
          actions.append(["take","Take","quest_maxine_dive_armor_teachers_desk"])
      actions.append("school_english_class_desk_interact")


label school_english_class_desk_interact:
  menu(side="middle"):
    "English Focus Sign-Up List":
      "Perhaps this is my time to finally get my act together?"
      "On one hand, [isabelle] would probably like me for focusing on her favorite subject."
      "On the other hand, lame."
      menu(side="middle"):
        extend ""
        #"English Focus Class"
        "To be":
          #If 3 other focus classes:
            #Dodged the proverbial bullet here. Nay to English.
          #If already signed up:
          if quest.isabelle_tour["english_focus"]:
            "The sunset of joy."
            "Broken under piles of books."
            "Desperation."
            return
          else:
            $quest.isabelle_tour["english_focus"] = True
            $isabelle.love +=1
            "[isabelle] better help me with my homework now. This is going to be rough."
            #$ Focus: English
            #$ +LoveIsabelle
        "Not to be":
          if quest.isabelle_tour["english_focus"]:
            "Too late. Already plastered my John Hancock all over the dotted line."
          else:
            "English must be the most boring subject. Why would anyone willingly subject themselves to Hamlet?"
    "Points List":
      "[mc] Total English Points: 0"
  return

label school_english_class_desk_take:
  "No papers, of course... but there are safety pins."
  "Perhaps one can be used to pick the lock to the art classroom?"
  $mc.add_item("safety_pin")
  return
