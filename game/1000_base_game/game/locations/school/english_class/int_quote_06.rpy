init python:
  class Interactable_school_english_class_quote_06(Interactable):

    def title(cls):
      return "Framed Quote"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "What about watching the movie interpretation? That has to count for something, right?"
      else:
        return "This is why they invented comic books."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_quote_06_interact")


label school_english_class_quote_06_interact:
  "{i}\"A picture is worth a thousand words, but a thousand words is merely two pages. Try reading the whole story, maybe you'll get the picture.\" — Unknown{/}"
  return
