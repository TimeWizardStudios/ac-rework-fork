init python:
  class Interactable_school_english_class_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A brave new world awaits... or, you know, the same old grueling shit."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_fate":
          if quest.kate_fate == "deadend":
            actions.append(["go","Fine Arts Wing","quest_kate_fate_dead_end_kate"])
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "encounter":
            actions.append(["go","Fine Arts Wing","quest_kate_desire_nude_encounter"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Fine Arts Wing","goto_school_first_hall_west"])
      else:
        if quest.isabelle_tour < "art_class":
          actions.append(["go","Fine Arts Wing","?school_english_class_door_interact_isabelle_tour"])
          return
      actions.append(["go","Fine Arts Wing","goto_school_first_hall_west"])


label school_english_class_door_interact_isabelle_tour:
  "Okay, time to check if [isabelle] finally signed up. Can't wait to get out of here."
  return
