init python:
  class Interactable_school_english_class_bookshelf(Interactable):

    def title(cls):
      return "Bookshelf"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.finished:
        return "All these titles, and none of them talk about the struggles of being a social outcast. Well, one does, but someone already borrowed it!"
      else:
        return "Juliet's Alfa Romeo, Frank's Stein of Lager, Muttering Heights, Prudes and Prejudice.\n\nClassics — the dust-gathering heart of any good English education."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "fetch" and quest.flora_bonsai["fetch"] == "book" and not school_english_class["catch"]:
            actions.append(["take","Take","quest_flora_bonsai_fetch_bookshelf_take"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_english_class["bookshelf_interact_first_time"]:
          actions.append(["take","Take","school_english_class_bookshelf_interact_first_time"])
        if quest.lindsey_book == "start":
          actions.append(["take","Take","school_english_class_bookshelf_interact_lindsey_book_start"])
        if quest.isabelle_stolen == "askmaxine":
          actions.append("?isabelle_quest_stolen_maxine_paper_english_bookshelf")
      if quest.isabelle_tour.finished and not quest.lindsey_book.ended:
        actions.append("?school_english_class_bookshelf_interact_isabelle_tour_finished")
      actions.append("?school_english_class_bookshelf_interact")


label quest_flora_bonsai_fetch_bookshelf_take:
  $mc.add_item("catch_thirty_four")
  $school_english_class['catch'] = True
  "She's not going to read it anyway. Might as well pick a title that'll make [jo] raise an eyebrow in her direction."
  return

label isabelle_quest_stolen_maxine_paper_english_bookshelf:
  "Technically, these fuckers are filled with paper, but [maxine] would probably spend a year analyzing all the text on the pages for hidden meanings."
  return

label school_english_class_bookshelf_interact_first_time:
  "Someone used a bookmark and forgot to take it out."
  "It's a rare drawing of Marilyn the Mason — the satanic brick-laying icon of the 1400s."
  $school_english_class["bookshelf_interact_first_time"] += 1
  $mc.add_item("marilyn_the_mason_drawing")
  return

label school_english_class_bookshelf_interact:
  "Catcher in the Rye... Catcher in the Rye..."
  "Crap. Someone already picked it out."
  return

label school_english_class_bookshelf_interact_isabelle_tour_finished:
  "{i}\"Catch-34, a Porno-Political Conflict.\"{/}"
  "{i}\"Atlas Plugged, a Tale of Rails, Fails, and Fluffy Tails.\"{/}"
  "{i}\"Snow White and the Seven Below-Average-Normal-Guys.\"{/}"
  "Classics — the dust-gathering heart of any good English education. "
  return

label school_english_class_bookshelf_interact_lindsey_book_start:
  "All right, let's see what's available here. Need to keep [lindsey] in mind while choosing."
  menu(side="middle"):
    extend ""
    "{image= items book catch_34}|Catch-34, a Porno-Political Conflict":
      $mc.add_item("catch_thirty_four")
      "This sounds too good to pass up on."
    "{image= items book atlas_plugged}|Atlas Plugged, a Tale of Rails, Fails, and Fluffy Tails":
      $mc.add_item("atlas_plugged")
      "Let's hope [lindsey] likes this. Some girls would."
    "{image= items book snow_white_and_7_bang}|Snow White and the Seven Below-Average-Normal-Guys":
      $mc.add_item("snow_white")
      "This one's a bit weird, but so are most fairy tales."
  $quest.lindsey_book.advance("suggest")
  return
