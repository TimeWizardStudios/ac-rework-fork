init python:
  class Interactable_school_english_class_quote_04(Interactable):

    def title(cls):
      return "Framed Quote"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      if not school_english_class["half_letter_taken"]:
        return "{#school_english_class_quote_04_sharp_object}Something's wedged behind the frame here, and it definitely doesn't glitter. A sharp object might help."
      else:
        return "That certainly holds true for people."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if "school_english_class_quote_04_sharp_object" in game.investigate_shown and not school_english_class["half_letter_taken"]:
          actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:act_one,english_quote|Use What?","school_english_class_quote_use"])
      actions.append("?school_english_class_quote_04_interact")


label school_english_class_quote_04_interact:
  "{i}\"All that glitters is not gold.\" — William Shakespeare{/}"
  return

label school_english_class_quote_use(item):
  if item == "glass_shard":
    "Let's see what's behind here. Must be a letter of timeless love or something otherwise important."
    $school_english_class["half_letter_taken"] = True
    $mc.add_item("half_letter_1")
  else:
    "All that glitters is not my [item.title_lower] either."
    "Nor would I compare it to gold."
    "Hmm... back to the drawing board."
    $quest.act_one.failed_item("english_quote",item)
  return
