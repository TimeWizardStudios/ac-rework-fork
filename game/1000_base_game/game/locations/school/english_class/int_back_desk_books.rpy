init python:
  class Interactable_school_english_class_back_desk_books(Interactable):

    def title(cls):
      return "Books"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Sarte was the first incel."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_back_desk_books_interact")


label school_english_class_back_desk_books_interact:
  "If you write a whole book about your love for lolis, you get heralded as a literary genius."
  "But when [jo] finds my forum posts expressing the same ideas, just in a more succinct manner, I get sent to the therapist?!"
  return
