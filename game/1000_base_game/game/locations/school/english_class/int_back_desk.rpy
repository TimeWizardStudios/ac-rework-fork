init python:
  class Interactable_school_english_class_back_desk(Interactable):

    def title(cls):
      return "Back Desk"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
         return "Two desks, huh? English teachers always overcompensate."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_back_desk_interact")


label school_english_class_back_desk_interact:
  "Reading out loud. Public humiliation."
  return
