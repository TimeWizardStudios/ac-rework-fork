init python:
  class Interactable_school_english_class_desk_one(Interactable):

    def title(cls):
      return "Student Desk"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Maybe this time..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_english_class["desk_one_used_today"] and school_english_class["desk_one_current_item"] is None:
          actions.append(["use_item", "Place an item","select_inventory_item","$quest_item_filter:act_one,english_desk_one|Use What?","school_english_class_desk_one_use_item"])
        if not school_english_class["desk_one_used_today"] and school_english_class["desk_one_current_item"] is not None:
          actions.append(["take","Take","school_english_class_desk_one_take"])
        elif school_english_class["desk_one_used_today"] and school_english_class["desk_one_current_item"] is not None:
          actions.append("?school_english_class_desk_one_interact")
        else:
          actions.append("?school_english_class_desk_one_interact_empty")

  q_desk_rewards = ["seven_hp",
  "strawberry_juice",
  "pepelepsi",
  "tide_pods",
  "milk",
  "salted_cola",
  "apple",
  "cake_slice",
  "empty_bottle",
  "water_bottle",
  "tissues",
  "lollipop",
  "onion_slice",
  "banana_milk"]


label school_english_class_desk_one_use_item(item):
  if getattr(item,"can_place_to_desk",False):
    if getattr(item,"bottled",False):
      $mc.remove_item(item)
      $mc.add_item("empty_bottle")
      "I open the desk and pour out the contents of my bottle inside."
      "What? Empty bottles are valuable! They should be grateful enough for the [item.title_lower]."
    else:
      $mc.remove_item(item)
      "Maybe someone will trade it for something else..."
    $school_english_class["desk_one_current_item"] = item
    $school_english_class["desk_one_trade_count"] +=1
    $school_english_class["desk_one_used_today"] = True
  else:
    "My [item.title_lower] could still be of use. Not giving it up for adoption."
    $quest.act_one.failed_item("english_desk_one",item)
  return

label school_english_class_desk_one_take:
  if school_english_class["desk_one_trade_count"] == 1:
    $mc.add_item("electrical_tape")
    "Perfect. Now I just need a girl to tie up."
    $school_english_class["desk_one_current_item"] = None
  else:
    "Whoa, would you look at that!"
    $school_english_class["desk_one_current_item"] = random.choice(q_desk_rewards)
    $x =find_item(school_english_class["desk_one_current_item"])
    if getattr(x,"bottled",False):
      if mc.owned_item("empty_bottle"):
        "The desk has been filled with some kind of fluid! No harm in bottling it up, I guess..."
        $mc.remove_item("empty_bottle")
    $mc.add_item(school_english_class["desk_one_current_item"])
    $school_english_class["desk_one_current_item"] = None
  return

label school_english_class_desk_one_interact:
  "All right. Time to wait. For better times and better things."
  return

label school_english_class_desk_one_interact_empty:
  "This one's always empty. No point in checking it in the future."
  return
