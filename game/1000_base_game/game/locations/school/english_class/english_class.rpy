init python:
  class Location_school_english_class(Location):

    default_title="English Class"

    def __init__(self):
      super().__init__()
      #Set default variables
      self["desk_one_trade_count"] = 0
      self["desk_one_current_item"] = None

    def scene(self,scene):

      scene.append([(-74,-287),"school english_class english_classroom"])

      #Quotes
      scene.append([(217,91),"school english_class quotes_shadow"])
      scene.append([(1848,170),"school english_class quote07",("school_english_class_quote_07",-120,70)])
      scene.append([(1428,83),"school english_class quote06",("school_english_class_quote_06",0,230)])
      scene.append([(1187,170),"school english_class quote05"])
      scene.append([(1015,260),"school english_class quote04","school_english_class_quote_04"])
      scene.append([(1031,178),"school english_class quote03"])
      scene.append([(381,142),"school english_class quote02"])
      scene.append([(217,133),"school english_class quote01",("school_english_class_quote_01",0,130)])

      #Misc
      scene.append([(558,144),"school english_class blackboard",("school_english_class_blackboard",0,80)])
      scene.append([(873,322),"school english_class chalk",("school_english_class_blackboard",-148,-98)])
      if quest.isabelle_red.in_progress and quest.isabelle_red >= "black_note":
        scene.append([(887,318),"school english_class note","school_english_class_note"])
      scene.append([(653,70),"school english_class plackard"])
      scene.append([(285,383),"school english_class furniture_back"])
      scene.append([(0,368),"school english_class beanbag","school_english_class_beanbag"])
      scene.append([(1595,169),"school english_class door",("school_english_class_door",0,60)])
      scene.append([(1712,365),"school english_class furniture_right"])
      scene.append([(1291,177),"school english_class bookshelf",("school_english_class_bookshelf",0,50)])
      scene.append([(1397,287),"school english_class safe","school_english_class_safe"])
      scene.append([(1057,401),"school english_class chair_back","school_english_class_chair_front_right"])

      if not lindsey.talking:
        if lindsey.at("school_english_class", "standing"):
          scene.append([(1244,232),"school english_class lindsey","lindsey"])

      #Student Desks
      scene.append([(285,608),"school english_class desks_shadow"])
      scene.append([(1314,565),"school english_class desk4","school_english_class_desk_four"])
      scene.append([(1078,594),"school english_class desk3","school_english_class_desk_three"])
      scene.append([(779,631),"school english_class desk2"])
      scene.append([(398,676),"school english_class desk1","school_english_class_desk_one"])

      #Misc
      scene.append([(1704,567),"school english_class chair_right"])
      scene.append([(1057,775),"school english_class table_back","school_english_class_back_desk"])
      scene.append([(1153,832),"school english_class books","school_english_class_back_desk_books"])
      scene.append([(863,391),"school english_class cabinet"])
      scene.append([(625,404),"school english_class teacher_chair"])
      scene.append([(496,407),"school english_class teacher_desk","school_english_class_desk"])
      if not school_english_class["cactus_taken"] or quest.jo_washed.in_progress:
        if quest.lindsey_motive == "lure" or quest.jo_day == "supplies":
          scene.append([(514,346),"school english_class cactus","school_english_class_cactus"])
        else:
          scene.append([(514,346),"school english_class cactus",("school_english_class_desk",163,61)])
      scene.append([(297,509),"school english_class desks_front"])
      scene.append([(30,764),"school english_class chair_left"])
      scene.append([(55,161),"school english_class clock"])

      if not isabelle.talking:
        if isabelle.at("school_english_class", "sitting"):
          if game.season == 1:
            scene.append([(121,398),"school english_class isabelle","isabelle"])
          elif game.season == 2:
            scene.append([(120,398),"school english_class isabelle_autumn","isabelle"])
            if isabelle.equipped_item("isabelle_collar"):
              scene.append([(143,458),"school english_class isabelle_collar",("isabelle",51,-60)])
          scene.append([(119,427),"school english_class couch_isabelle",("school_english_class_beanbag",-13,-59)])

      #Dollars
      if not quest.jo_washed.in_progress:
        if school_english_class["dollar1_spawned_today"] == True and not school_english_class["dollar1_taken_today"]:
          scene.append([(394,372),"school english_class dollar1","school_english_class_dollar1"])
        if school_english_class["dollar2_spawned_today"] == True and not school_english_class["dollar2_taken_today"]:
          scene.append([(972,459),"school english_class dollar2","school_english_class_dollar2"])

      #Book
      if not school_english_class["book_taken"] and not quest.jo_washed.in_progress:
        scene.append([(1853,369),"school english_class book","school_english_class_book"])

      if quest.isabelle_dethroning == "revenge":
        scene.append([(0,0),"#school art_class night_overlay"])

    def find_path(self,target_location):
      return ("school_english_class_door",dict(marker_sector="right_bottom"))

label goto_school_english_class:
  if school_english_class.first_visit_today:
    $school_english_class["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_english_class["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_english_class)
  return



