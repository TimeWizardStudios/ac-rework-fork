init python:
  class Interactable_school_english_class_blackboard(Interactable):

    def title(cls):
      return "Blackboard"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_gesture == "letter":
        return "Looks like as good a place as any to pen my apologum opus."
      elif quest.lindsey_nurse.started:
        return "In this day and age, why aren't all classes pre-recorded and available online? Going to school to learn is such a waste of time."
      else:
        return "Mr. Brown's blackboard is the cleanest in the school. Never even seen half a ballsack drawn on it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "kate_moment":
          if quest.isabelle_gesture == "letter":
            actions.append("quest_isabelle_gesture_letter")
      else:
        if quest.isabelle_dethroning == "poem":
          actions.append("quest_isabelle_dethroning_poem_blackboard")
        if quest.isabelle_gesture == "letter":
          actions.append("quest_isabelle_gesture_letter")
      if quest.isabelle_tour.started:
        actions.append(["interact","Read","?school_english_class_blackboard_interact"])
#       actions.append(["interact","Write","?school_english_class_blackboard_interact_write"])


label school_english_class_blackboard_interact:
  "{i}\"Welcome!\"{/}"
  "{i}\"The list for English Focus sign-ups is on my desk. Please, register your name there.\"{/}"
  "{i}\"I'm away on a teachers' conference this week, but will be back by Monday.\"{/}"
  "{i}\"Sincerely, Mr. Brown.\"{/}"
  return

label school_english_class_blackboard_interact_write:
  "There's no chalk. The music students must've stolen it again. Stupid chalk-munchers."
  return
