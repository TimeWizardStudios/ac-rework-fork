init python:
  class Interactable_school_english_class_chair_front_right(Interactable):

    def title(cls):
      return "Chair"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "I hate this chair particularly."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_chair_front_right_interact")


label school_english_class_chair_front_right_interact:
  "This is where you had to sit when you didn't do last night's reading homework."
  "Mr. Brown relished in having the whole classroom judge you."
  "It was also known as [mc]'s chair."
  return
