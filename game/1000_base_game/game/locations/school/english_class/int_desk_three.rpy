init python:
  class Interactable_school_english_class_desk_three(Interactable):

    def title(cls):
      return "Student Desk"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "Someone's carved a sentence into the wood here: {i}\"I'll cut you. I don't give threefolds of a fuck.\"{/}\n\nPoor desk, what a cruel fate."
      else:
        return "It's not theft if someone's stupid enough to forget their stuff."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      if school_english_class["desk_three_first"]:
        actions.append("?school_english_class_desk_three_interact")
      else:
        actions.append(["take","Take","school_english_class_desk_three_interact_first_time"])


label school_english_class_desk_three_interact_first_time:
  $mc.add_item("baseball")
  "Yoink. Mine now."
  $school_english_class["desk_three_first"] = True
  return

label school_english_class_desk_three_interact:
  "Empty, for now."
  return
