init python:
  class Interactable_school_english_class_quote_01(Interactable):

    def title(cls):
      return "Framed Quote"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "Driven by impulse, the story is irrelevant, it's like a background for my desires. It doesn't matter. Nothing matters."
      else:
        return "Hmm... not sure what to make of this one. Maybe both?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_quote_01_interact")


label school_english_class_quote_01_interact:
  "{i}\"Do you write the story, or does the story write you? Think about it.\" — Unknown{/}"
  return
