init python:
  class Interactable_school_english_class_safe(Interactable):

    def title(cls):
      return "Safe"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "Some say that Mr. Brown forgot the combination to this safe years ago. Maybe there's a severed head in there. Either way, I've never seen him open it."
      else:
        return "Mr. Brown used to claim that he keeps the examination answers in there, but who knows what else he's hiding.\n\nVintage porn, hopefully."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_english_class_safe_interact")


label school_english_class_safe_interact:
  "That thing is heavier than my heart on days when the strawberry juice runs out in the cafeteria."
  return
