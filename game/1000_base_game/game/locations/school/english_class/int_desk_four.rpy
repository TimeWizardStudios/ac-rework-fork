init python:
  class Interactable_school_english_class_desk_four(Interactable):

    def title(cls):
      return "Student Desk"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.lindsey_nurse.started:
        return "People usually leave offerings to the school god here in exchange for better grades. Never seems to be working."
      else:
        return "There are lockers for a reason."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_english_class["desk_four_interact_first"]:
          actions.append(["take","Take","school_english_class_desk_four_interact_first_time"])
      actions.append("?school_english_class_desk_four_interact")


label school_english_class_desk_four_interact_first_time:
  $mc.add_item("apple")
  $mc.money+=3
  "Finders, keepers."
  $school_english_class["desk_four_interact_first"] = True
  return

label school_english_class_desk_four_interact:
  "Empty, for now."
  return
