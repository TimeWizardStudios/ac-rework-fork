init python:
  class Interactable_school_roof_plants_left(Interactable):

    def title(cls):
      return "Plants"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "This one just says \"[flora]\"\non the label."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.lindsey_motive == "lure":
          actions.append("?quest_lindsey_motive_lure_roof_plants_interact")
      actions.append("?school_roof_plants_left_interact")


label school_roof_plants_left_interact:
  "Not sure if this is a science project or a non-scientific project..."
  return
