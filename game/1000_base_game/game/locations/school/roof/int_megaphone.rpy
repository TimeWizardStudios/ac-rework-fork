init python:
  class Interactable_school_roof_megaphone(Interactable):

    def title(cls):
      return "Megaphone"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "This old system is no longer in use. It was decommissioned due to hardware error."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_roof_megaphone_interact")


label school_roof_megaphone_interact:
  "Shout it from the top of the roof — we don't need no education!"
  return
