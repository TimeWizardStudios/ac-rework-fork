init python:
  class Interactable_school_roof_note(Interactable):

    def title(cls):
      return "Note"

    def description(cls):
      return "A note with yet another riddle."

    def actions(cls,actions):
      actions.append("?school_roof_note_interact")


label school_roof_note_interact:
  call quest_isabelle_red_greenhouse
  return
