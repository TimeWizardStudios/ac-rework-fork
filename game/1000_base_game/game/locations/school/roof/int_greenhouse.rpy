init python:
  class Interactable_school_roof_greenhouse(Interactable):

    def title(cls):
      return "Greenhouse"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Ah! The refreshing scent of greenery and plants. Humans must be drawn to it by instinct. Nature, the great unifier.\n\nProbably why Voldemort\nturned evil — no nose."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_roof_greenhouse_interact")


label school_roof_greenhouse_interact:
  "This is where [maxine] grows her illegal substances."
  "The school tried to bust her once; that didn't go well for the school."
  return
