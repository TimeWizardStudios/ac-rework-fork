init python:
  class Interactable_school_roof_telescope(Interactable):

    def title(cls):
      return "Telescope"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A telescope to watch the stars...\n\n...or, you know, amateurs."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.isabelle_stars == "telescope":
          actions.append(["take","Take","quest_isabelle_stars_telescope"])
      actions.append("school_roof_telescope_interact")


label school_roof_telescope_interact:
  menu(side="middle"):
    "Move":
      $school_roof["telescope_moved"] = not school_roof["telescope_moved"]
    "?False@|{space=29}Look through\n{color=#C00}(not yet available){/}":
      pass
  return
