init python:
  class Interactable_school_roof_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The portal to hell. Below lie the pits of despair."

    def actions(cls,actions):
      actions.append(["go","Roof Landing","goto_school_roof_landing"])
