init python:
  class Interactable_school_roof_ladder(Interactable):

    def title(cls):
      return "Ladder"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A perfect illustration of\nour society."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_roof_ladder_interact")


label school_roof_ladder_interact:
  "When you think you've reached the top, there's always another ladder{space=-35}\nto climb."
  return
