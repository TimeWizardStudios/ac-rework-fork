init python:
  class Interactable_school_roof_garden_hose(Interactable):

    def title(cls):
      return "Garden Hose"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Unlike other garden hoses, this one is very quirky and thinks it's one of the boys."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed == "supplies" and not mc.owned_item("garden_hose"):
            actions.append(["take","Take","quest_jo_washed_supplies_garden_hose"])
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_roof_garden_hose_interact")


label school_roof_garden_hose_interact:
  "A hose is like a pussy, you need to turn it on to make it squirt."
  return
