init python:
  class Location_school_roof(Location):

    default_title = "Roof"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      if (isabelle["roof"] and game.hour in (18,19) and game.season == 1) or school_roof["isabelle_kiss_today"]:
        if game.season == 1:
          scene.append([(-995,-323),"school roof sky_sunset"])
        elif game.season == 2:
          scene.append([(-995,-323),"school roof sky_autumn_sunset"])
      else:
        if game.season == 1:
          scene.append([(-995,-323),"school roof sky"])
        elif game.season == 2:
          scene.append([(-995,-323),"school roof sky_autumn"])
      scene.append([(0,0),"school roof background"])
      if game.season == 2:
        scene.append([(722,907),"school roof plants_middle_autumn"])

      ## Garden hose
      if not school_roof["garden_hose_taken"]:
        scene.append([(1201,730),"school roof garden_hose","school_roof_garden_hose"])

      if game.season == 1:
        scene.append([(1348,531),"school roof plants_left","school_roof_plants_left"])
      elif game.season == 2:
        scene.append([(1348,531),"school roof plants_left_autumn","school_roof_plants_left"])

      ## Roof landing
      scene.append([(137,431),"school roof door","school_roof_door"])
      scene.append([(324,90),"school roof megaphone",("school_roof_megaphone",0,300)])

      ## Greenhouse
      scene.append([(1415,244),"school roof greenhouse","school_roof_greenhouse"])
      if quest.isabelle_red.in_progress and quest.isabelle_red >= "greenhouse":
        scene.append([(1525,558),"school roof note","school_roof_note"])

      if game.season == 1:
        scene.append([(1778,875),"school roof plants_right",("school_roof_plants_right",-50,0)])
      elif game.season == 2:
        scene.append([(1778,875),"school roof plants_right_autumn",("school_roof_plants_right",-50,0)])

      ## Maxine
      if maxine.at("school_roof","standing"):
        if not maxine.talking:
          scene.append([(1505,482),"school roof maxine","maxine"])

      ## Telescope
      if school_roof["telescope_moved"] and not (school_roof["telescope_taken"] or quest.jo_washed.in_progress):
        scene.append([(448,486),"school roof telescope_left","school_roof_telescope"])
      elif not school_roof["telescope_taken"]:
        scene.append([(1291,430),"school roof telescope_right","school_roof_telescope"])

      ## Isabelle
      if isabelle.at("school_roof","standing"):
        if not isabelle.talking:
          scene.append([(799,387),"school roof isabelle",("isabelle",50,0)])

      ## Nurse
      if ((not school_roof["exclusive"] or "nurse" in school_roof["exclusive"])
      and nurse.at("school_roof","standing")
      and not nurse.talking):
        scene.append([(1128,335),"school roof nurse_autumn","nurse"])

      ## Benches
      scene.append([(469,747),"school roof benches"])

      ## Sunset
      if (isabelle["roof"] and game.hour in (18,19) and game.season == 1) or school_roof["isabelle_kiss_today"]:
        scene.append([(0,0),"#school roof sunset_overlay"])

    def find_path(self,target_location):
      return "school_roof_door",dict(marker_offset=(90,45),marker_sector="left_bottom")


label goto_school_roof:
  call goto_with_black(school_roof)
  return
