init python:
  class Interactable_school_nurse_room_bin(Interactable):

    def title(cls):
      return "Trash Bin"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "One man's trash is another man's reflection."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_bin_interact")


label school_nurse_room_bin_interact:
  "Imagine the absolute cesspool of pestilence and disease that must be festering in there."
  "Time to dive in."
  #$mc.add_item(something)
  #$mc.add_effect(hep_c)
  return
