init python:
  class Interactable_school_nurse_room_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.nurse_aid["name_reveal"]:
        # return "[amelia] really should put a lock on this door... but maybe she likes the risk?"
        return "[amelia] really should put\na lock on this door... but maybe\nshe likes the risk?"
      else:
        # return "The [nurse] really should put a lock on this door... but maybe she likes the risk?"
        return "The [nurse] really should put\na lock on this door... but maybe\nshe likes the risk?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "pads":
            if mc.owned_item("pad"):
              actions.append(["go","Admin Wing","quest_flora_bonsai_pads_door"])
            else:
              actions.append(["go","Admin Wing","?quest_flora_bonsai_pads_interact"])
              return
          elif quest.flora_bonsai == "fetch" and not quest.flora_bonsai["sweets_message"]:
            actions.append(["go","Admin Wing","quest_flora_bonsai_fetch_sweets"])
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "encounter":
            actions.append(["go","Admin Wing","quest_kate_desire_nude_encounter"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Admin Wing","goto_school_ground_floor_west"])
      else:
        if quest.lindsey_nurse.in_progress:
          actions.append(["go","Admin Wing","quest_lindsey_nurse_door"])
        if quest.kate_search_for_nurse.in_progress:
          actions.append(["go","Admin Wing","quest_kate_search_for_nurse_door"])
        if quest.nurse_strike_nude_office == "maxine_visitor":
          actions.append(["go","Admin Wing","quest_nurse_strike_nude_office_maxine_visitor"])
      actions.append(["go","Admin Wing","goto_school_ground_floor_west"])


label quest_flora_bonsai_pads_interact:
  "Better not keep [isabelle] waiting, else she might burst in."
  return

label quest_flora_bonsai_pads_door:
  call goto_school_ground_floor_west
  show isabelle smile with Dissolve(.5)
  mc "Don't lose these!"
  $mc.remove_item("pad")
  isabelle laughing "Hey, thanks! I appreciate it."
  mc "No problem. I know how rough it can be. [flora] gets crazy during her... time of the month. I'm glad you're so calm."
  isabelle smile "It hits people differently, but thanks!"
  $set_dialog_mode("default_no_bg")
  $isabelle.love+=1
  hide isabelle smile with Dissolve(.5)
  $quest.flora_bonsai["helped_isabelle"] = True
  $quest.flora_bonsai.advance('fetch')
  pause(.5)
  jump quest_flora_bonsai_fetch_sweets

label quest_lindsey_nurse_door:
  call goto_school_ground_floor_west
  if quest.lindsey_nurse=="nurse_room":
    $quest.lindsey_nurse.finish()
  if not school_nurse_room["curtain_off"]:
    if not quest.lindsey_nurse=="fetch_the_nurse":
      $school_nurse_room["curtain_off"] = True
  return

label quest_kate_search_for_nurse_door:
  call goto_school_ground_floor_west
  if quest.kate_search_for_nurse == "end":
    $quest.kate_search_for_nurse.finish()
  if not school_nurse_room["curtain_off"]:
    $school_nurse_room["curtain_off"] = True
  return

label quest_nurse_strike_nude_office_maxine_visitor:
  if nurse["strike_book_nude_office_today"]:
    $nurse.outfit = {}
    play sound "open_door"
    pause 0.25
    show nurse thinking with Dissolve(.5)
    pause 0.25
    show nurse thinking at move_to(.25,1.0)
    show maxine neutral at appear_from_right(.75)
    show nurse afraid with Dissolve(.5)
    maxine neutral "Did the mites eat your clothes too? That is fascinating."
    nurse afraid "Mites? Err, I..."
    mc "She just likes going naked."
    nurse surprised "That is not true!"
    maxine smile "Very well. I need seven gallons of malic acid, thank you."
    nurse annoyed "I don't think I can help you with that, [maxine]..."
    maxine neutral "That is unfortunate. Do you know where I can find crab apples?"
    nurse annoyed "..."
    "[maxine] doesn't seem fazed by the [nurse]'s nudity at all."
    "The [nurse], on the other hand, is sweating and squirming."
    "If her pussy wasn't glistening with her juices, one could think she didn't enjoy the situation."
    nurse annoyed "I don't know..."
    maxine neutral "What about you, [mc]?"
    mc "What about me?"
    maxine skeptical "Your crab apples. Hand them over."
    mc "I don't have any crab apples."
    maxine skeptical "You seem like someone who would hide crab apples."
    mc "Well, I don't."
    maxine annoyed "Would you submit yourself to a cavity search?"
    mc "...no?"
    maxine concerned "Very well."
    maxine concerned "If either of you see anyone in possession of crab apples, inform me immediately."
    window hide
    show nurse annoyed at move_to(.5,1.25)
    show maxine concerned at disappear_to_right
    pause 0.5
    show nurse concerned with Dissolve(.5)
    window auto
    nurse concerned "My goodness. I hope she doesn't report me."
    mc "I don't think [maxine] has much credibility with the principal. You could{space=-25}\njust deny it."
    nurse thinking "God, I hope so..."
    nurse sad "Would you please let me get dressed now?"
    mc "Absolutely not. You still have hours and hours left."
    mc "I'll be back to check on you."
    $unlock_replay("nurse_first_strike")
    $quest.nurse_strike_nude_office.advance("next_visitor")
  if school_nurse_room["curtain_off"]:
    $school_nurse_room["curtain_off"] = True
  call goto_school_ground_floor_west
  return
