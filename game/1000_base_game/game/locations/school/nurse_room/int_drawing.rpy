init python:
  class Interactable_school_nurse_room_drawing(Interactable):

    def title(cls):
      return "Obscene Poster"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Smut. There's no two ways about it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_drawing_interact")


label school_nurse_room_drawing_interact:
  "Why is this disgusting filth allowed in a school environment?"
  return
