init python:
  class Interactable_school_nurse_room_medical_supplies(Interactable):

    def title(cls):
      return "Medical Supplies"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Chloroform, heroin needles, nitroglycerin. Everything a truly sick person needs."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "pads" and not school_nurse_room["pads_taken"]:
            actions.append(["take","Take","school_nurse_room_medical_supplies_take_pads"])
          elif quest.flora_bonsai == "tinybigtree" and (not school_nurse_room["meds_taken"]
          or (not school_nurse_room["pads_taken"] and quest.flora_bonsai > "pads")):
            actions.append(["take","Take","school_nurse_room_medical_supplies_take"])
          else:
            actions.append("?school_nurse_room_medical_supplies_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if (not school_nurse_room["meds_taken"]
        or (not school_nurse_room["pads_taken"] and quest.flora_bonsai > "pads")):
          actions.append(["take","Take","school_nurse_room_medical_supplies_take"])
        else:
          actions.append("?school_nurse_room_medical_supplies_interact")
      actions.append("?school_nurse_room_medical_supplies_interact_pads")


label school_nurse_room_medical_supplies_take_pads:
  "Pads. Yep. Time to be someone's hero."
  $school_nurse_room["pads_taken"] = True
  $mc.add_item("pad",2)
  return

label school_nurse_room_medical_supplies_interact_pads:
  "Meds. All sorts of pills and stuff."
  "This unmarked bottle smells nice..."
  "Gulp. Gulp."
  "Yum!"
  return

label school_nurse_room_medical_supplies_take:
  if not school_nurse_room["meds_taken"]:
    $school_nurse_room["meds_taken"] = True
    "Unassorted meds. Might come in handy."
    $mc.add_item("meds")
  elif not school_nurse_room["pads_taken"] and quest.flora_bonsai > "pads":
    $school_nurse_room["pads_taken"] = True
    "Pads. Yep."
    $mc.add_item("pad")
  return

label school_nurse_room_medical_supplies_interact:
  "Already got myself a healthy stash. No need to overdose."
  return
