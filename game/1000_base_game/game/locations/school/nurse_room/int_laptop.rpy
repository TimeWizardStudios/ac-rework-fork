init python:
  class Interactable_school_nurse_room_laptop(Interactable):
    def title(cls):
      return "Laptop"
    def description(cls):
      return "desc"
    def actions(cls,actions):
      actions.append("?school_nurse_room_laptop_interact")

label school_nurse_room_laptop_interact:
  "Label"
  return
