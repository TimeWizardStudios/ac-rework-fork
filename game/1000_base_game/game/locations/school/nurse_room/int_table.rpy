
init python:
  class Interactable_school_nurse_room_table(Interactable):
    def title(cls):
      return "Table"
    def description(cls):
      return "Table"
    def actions(cls,actions):
      actions.append("?school_nurse_room_table_interact")

label school_nurse_room_table_interact:
  "Table"
  return
