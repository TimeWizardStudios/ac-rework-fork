init python:
  class Interactable_school_nurse_room_pen(Interactable):

    def title(cls):
      return "[nurse]'s Pen"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "With this pen, I can finally realize my dream of becoming a best-selling author.\n\nWriting is all in the equipment."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["take","Take","school_nurse_room_pen_take"])
      actions.append("?school_nurse_room_pen_interact")


label school_nurse_room_pen_take:
  $school_nurse_room["pen_taken"] = True
  $mc.add_item("nurse_pen")
  return

label school_nurse_room_pen_interact:
  "Looks like a butt thermometer..."
  "..."
  "Yup. 69°F."
  return
