
init python:
  class Interactable_school_nurse_room_skeleton(Interactable):

    def title(cls):
      return "Skeleton"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.finished:
        return "I am become Death, displayer of bones."
      else:
        return "This thing always gave me the creeps. It looks way too real."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_desire":
          if quest.kate_desire == "starblack":
            actions.append("quest_kate_desire_skeleton_interact")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.isabelle_tour.finished:
        actions.append("?school_nurse_room_skeleton_interact_tour_finished")
      actions.append("?school_nurse_room_skeleton_interact")


label school_nurse_room_skeleton_interact:
  "It says, \"Made in China.\""
  "But it doesn't look like an official stamp. Typical China."
  return

label school_nurse_room_skeleton_interact_tour_finished:
  "His handshake is a bit hard and loose. Typical for a bonehead."
  return

#Anatomical Skeleton
#    If Interact:
#      How many times did you get boned in life, friend? Hopefully, more than zero.
#    If Investigate:
#    The centerpiece of any good Halloween party.
#
