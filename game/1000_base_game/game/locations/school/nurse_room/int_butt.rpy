init python:
  class Interactable_school_nurse_room_butt(Interactable):

    def title(cls):
      return "Non Sexual Anatomical Poster"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Gluteus deliciousness."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_butt_interact")


label school_nurse_room_butt_interact:
  "{i}*BRAAAAAAAAAAAAAAAAAAAAAP*{/}"
  return
