init python:
  class Interactable_school_nurse_room_strikebook(Interactable):

    def title(cls):
      return "Strike Book"

    def description(cls):
      if quest.nurse_aid["name_reveal"]:
        # return "[amelia] is keeping good track of her strikes. She's a very diligent participant in her own debasement."
        return "[amelia] is keeping good\ntrack of her strikes.\n\nShe's a very diligent participant\nin her own debasement."
      else:
        # return "The [nurse] is keeping good track of her strikes. She's a very diligent participant in her own debasement."
        return "The [nurse] is keeping good\ntrack of her strikes.\n\nShe's a very diligent participant\nin her own debasement."

    def actions(cls,actions):
      if not mc["focus"]:
        actions.append("school_nurse_room_strikebook_interact")


label school_nurse_room_strikebook_interact:
  $nurse_strikes_text = nurse["strike_book"]
  "Let's see here..."
  if quest.nurse_aid["name_reveal"]:
    if nurse["strike_book"] == 1:
      "[amelia] currently has [nurse_strikes_text] strike."
    else:
      "[amelia] currently has [nurse_strikes_text] strikes."
  else:
    if nurse["strike_book"] == 1:
      "The [nurse] currently has [nurse_strikes_text] strike."
    else:
      "The [nurse] currently has [nurse_strikes_text] strikes."
  menu(side="middle"):
    extend ""
    "?nurse_strikes_text>=1@1 strike|{image=school nurse_room strikebook}|Forced nudity (Office)":
      $nurse["strike_book"]-=1
      jump nurse_strike_nude_office_first
#   "?nurse_strikes_text>=5@5 strikes|{image=school nurse_room strikebook}|Forced nudity (School)\n{space=33}{color=#C00}(not yet available){/}":
    "?False@5 strikes|{image=school nurse_room strikebook}|Forced nudity (School)\n{space=33}{color=#C00}(not yet available){/}":
      pass
#   "?nurse_strikes_text>=10@10 strikes|{image=school nurse_room strikebook}|{space=57}Spanking\n{color=#C00}(not yet available){/}":
    "?False@10 strikes|{image=school nurse_room strikebook}|{space=57}Spanking\n{color=#C00}(not yet available){/}":
      pass
    "Nothing for now":
      pass
  return
