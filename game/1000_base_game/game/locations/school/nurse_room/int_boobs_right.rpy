init python:
  class Interactable_school_nurse_room_boobs_right(Interactable):

    def title(cls):
      return "Non Sexual Anatomical Poster"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A non-sexual anatomical poster. Just as it says on the tin."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_boobs_right_interact")


label school_nurse_room_boobs_right_interact:
  "A sad case of heart nipple disease."
  "Poor girl."
  return
