init python:
  class Interactable_school_nurse_room_vent(Interactable):

    def title(cls):
      return "Vent"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A hole-the-wall type of deal."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_vent_interact")


label school_nurse_room_vent_interact:
  call quest_nurse_venting_nurse_room_vent_interact
  return
