init python:
  class Interactable_school_nurse_room_desk(Interactable):
    def title(cls):
      return "Desk"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_desk_interact")

label school_nurse_room_desk_interact:
  ""
  return
