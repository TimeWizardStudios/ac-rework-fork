init python:
  class Interactable_school_nurse_room_teddy(Interactable):
    def title(cls):
      return "Teddy"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_teddy_interact")

label school_nurse_room_teddy_interact:
  ""
  return
