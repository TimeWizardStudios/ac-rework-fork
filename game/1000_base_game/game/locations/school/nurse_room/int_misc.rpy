init python:
  class Interactable_school_nurse_room_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_nurse_room_dollar1_take"])


label school_nurse_room_dollar1_take:
  $school_nurse_room["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_nurse_room_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_nurse_room_dollar2_take"])


label school_nurse_room_dollar2_take:
  $school_nurse_room["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_nurse_room_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_nurse_room_dollar3_take"])


label school_nurse_room_dollar3_take:
  $school_nurse_room["dollar3_taken_today"] = True
  $mc.money+=20
  return
