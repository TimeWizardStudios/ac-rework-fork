init python:
  class Interactable_school_nurse_room_weight(Interactable):

    def title(cls):
      return "Weight"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "I need to go on keto again."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_weight_interact")


label school_nurse_room_weight_interact:
  "90%% of your weight is just the force of gravity."
  return
