init python:
  class Interactable_school_nurse_room_keycard(Interactable):

    def title(cls):
      return "Keycard"

    def description(cls):
      return "Gone are the days of key-shaped keys and star-shaped stars."

    def actions(cls,actions):
      actions.append(["take","Take","school_nurse_room_keycard_take"])
      actions.append("?school_nurse_room_keycard_interact")


label school_nurse_room_keycard_interact:
  "Ah, here it is!"
  if quest.nurse_aid["name_reveal"]:
    "Accidentally left behind, just like [amelia] said it would be."
  else:
    "Accidentally left behind, just like the [nurse] said it would be."
  return

label school_nurse_room_keycard_take:
  if ("school_nurse_room_keycard_interact",()) not in game.seen_actions:
    call school_nurse_room_keycard_interact
    window hide
  $school_nurse_room["keycard_taken"] = True
  $mc.add_item("keycard")
  pause 0.25
  window auto
  "Perfect. Now I have the means to get in."
  "Hmm... but I also need to fit in..."
  "..."
  "Maybe I can borrow a lab coat or something from the science classroom?"
  $quest.lindsey_angel.advance("off_limits")
  return
