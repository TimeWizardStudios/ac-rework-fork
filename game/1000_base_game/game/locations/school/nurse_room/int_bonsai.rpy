init python:
  class Interactable_school_nurse_room_bonsai(Interactable):

    def title(cls):
      return "Bonsai Tree"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Even the smallest tree needs water to live, and this one looks parched."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai < "water":
            actions.append("?school_nurse_room_bonsai_flora_bonsai")
          elif quest.flora_bonsai == "water":
            actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,bonsai|Use What?","quest_flora_bonsai_water2"])
          elif quest.flora_bonsai > "water":
            pass
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if game.season == 1:
          if quest.spinach_seek.finished and quest.flora_jacklyn_introduction.finished:
            actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,bonsai_start|Use What?","quest_flora_bonsai_bonsai_water"])
        if quest.jo_day == "supplies":
          actions.append("?quest_jo_day_supplies_plant_three")
        if quest.lindsey_motive == "lure":
          actions.append("?quest_lindsey_motive_lure_nurse_room_plant_interact")
      actions.append("?school_nurse_room_bonsai_interact")


label school_nurse_room_bonsai_interact:
  if mc.owned_item(((((("spray_banana_milk","spray_pepelepsi","spray_salted_cola","spray_seven_hp","spray_strawberry_juice","spray_urine","spray_water","spray_empty_bottle")))))):
    "This looks suspicious. I should consult with [flora] first."
  else:
    "The [nurse] always looks overworked. [jo] told me she fills in at the hospital during the summer."
    "Maybe her life would become a tiny bit easier if I watered her beloved bonsai?"
    "Just need to find a spray bottle... and something liquid."
  return

label school_nurse_room_bonsai_flora_bonsai:
  "This tree looks like it could drink just about anything at this point. Even..."
  "No, that would be gross..."
  return

#Bonsai Tree
#    If Interact:
#      Even the smallest tree in the forest grows toward the sky.
#    If Investigate:
#      Small but thirsty, just like my...
#...mom's plant in the kitchen.

#  Bonsai Tree
#    If Interact:
#      A pet tree that outlives its master. A metaphor for breaking your chains through perseverance.
#    If Investigate:
#      A potted tree. That's what it means. Bonsai — the art of tray planting.
