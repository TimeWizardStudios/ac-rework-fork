init python:
  class Interactable_school_nurse_room_curtain_closed(Interactable):

    def title(cls):
      return "Curtain"

    def description(cls):
      if quest.lindsey_nurse == "fetch_the_nurse" or quest.nurse_photogenic == "eavesdrop":
        return "Sounds like the [nurse] has another patient behind there.\n\nMaybe it would've been smart\nto knock?"
      else:
        return "Two kinds of people have curtains on their beds  — patients and princesses. Everyone else just has to live in complete exposure."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_nurse":
          if quest.lindsey_nurse == "fetch_the_nurse":
            actions.append("isabelle_quest_isabelle_tour_fetch_nurse")
        elif mc["focus"] == "nurse_photogenic":
          if quest.nurse_photogenic == "eavesdrop":
            actions.append("quest_nurse_photogenic_curtain")
      else:
        if quest.lindsey_nurse == "nurse_room":
          actions.append("isabelle_quest_isabelle_tour_peak_lindsey")
      actions.append("?school_nurse_room_curtain_closed_interact")


label school_nurse_room_curtain_closed_interact:
  "Is this my curtain call?"
  return
