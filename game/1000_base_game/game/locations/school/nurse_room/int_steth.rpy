init python:
  class Interactable_school_nurse_room_steth(Interactable):

    def title(cls):
      return "Stethoscope"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_tour.finished:
        return "Follow the beat of your own heart."
      else:
        return "Listening to someone's blood pump is a bit morbid. Only doctors and vampires enjoy it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["take","Take","school_nurse_room_steth_take"])
      actions.append("?school_nurse_room_steth_interact")


label school_nurse_room_steth_take:
  $school_nurse_room["stethoscope_taken"] = True
  $mc.add_item("stethoscope")
  return

label school_nurse_room_steth_interact:
  mc "..."
  menu(side="middle"):
    extend ""
    "Listen to your heart":
      "Blood pumping through my heart... a strong steady beat."
      "Whenever I think of [flora], it starts beating even faster!"
    "Listen to your... other organ of love":
      "Blood pumping through my err... hmm... it's more of a throbbing than a beat..."
      "Whenever I think of [mrsl]... mmm, yes!"
  return

##
#  If Investigate:
#    If you listen closely, you can hear the tiny tears and cracks from having your heart broken too many times.
