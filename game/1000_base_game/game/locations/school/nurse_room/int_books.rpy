init python:
  class Interactable_school_nurse_room_books(Interactable):

    def title(cls):
      return "Books"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Every medical maladie under the sun, but no mention of ugly-face syndrome and its laundry list of symptoms."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_books_interact")


label school_nurse_room_books_interact:
  "How many nuts were busted to medical books before the invention of pornography?"
  "A tit's a tit."
  return
