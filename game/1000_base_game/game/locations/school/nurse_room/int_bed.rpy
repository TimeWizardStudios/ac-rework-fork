init python:
  class Interactable_school_nurse_room_bed(Interactable):

    def title(cls):
      return "Bed"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_wicked in ("costume","party"):
        return "This bed has probably seen more action than a full-time hooker."
      else:
        return "It's not as soft as my bed, but the sheets are definitely cleaner. Good to keep in mind if [jo] starts her yearly laundry strike."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume":
            actions.append("quest_kate_wicked_costume_nurse_room_bed")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_nurse_room_bed_interact")


label school_nurse_room_bed_interact:
  "When I was a young boy, my mother took me into the city..."
  "And I ended up in a bed like this one. Turns out demons are pretty hard to defeat, and that marching bands don't stop for anybody."
  return

#Hospital Bed
#    If Interact:
#      The [nurse] probably sleeps here on her breaks. The sheets do smell a bit like her.
#    If Investigate:
#    This bed would probably reveal a lot under a blacklight. But unlike my bed, the stains are probably blood here.
