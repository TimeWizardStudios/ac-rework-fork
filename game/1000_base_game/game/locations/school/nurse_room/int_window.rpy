init python:
  class Interactable_school_nurse_room_window(Interactable):
    def title(cls):
      return "Window"
    def description(cls):
      return "Window"
    def actions(cls,actions):
      actions.append("?school_nurse_room_window_interact")

label school_nurse_room_window_interact:
  "Window"
  return
