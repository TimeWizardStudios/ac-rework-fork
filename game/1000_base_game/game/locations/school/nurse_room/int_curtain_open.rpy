init python:
  class Interactable_school_nurse_room_curtain_open(Interactable):
    def title(cls):
      return "Curtain"
    def description(cls):
      return "Curtain"
    def actions(cls,actions):
      actions.append("?school_nurse_room_curtain_open_interact")

label school_nurse_room_curtain_open_interact:
  "Curtain"
  return
