init python:
  class Interactable_school_nurse_room_lollipop(Interactable):

    def title(cls):
      return "Lollipop"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The only type of lolis I'm legally allowed to enjoy."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_nurse_room["lollipop_taken"]:
          actions.append(["take","Take","?home_bathroom_lollipop_take_jacklyn_broken_fuse"])


label home_bathroom_lollipop_take_jacklyn_broken_fuse:
  $school_nurse_room["lollipop_taken"]=True
  $mc.add_item("lollipop_2")
  "Diabetes on a stick. Yum."
  return
