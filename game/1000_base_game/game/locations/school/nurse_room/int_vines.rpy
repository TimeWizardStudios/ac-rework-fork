

### school_nurse_room_flora_torso_vine interactable ###########################

init python:
  class Interactable_school_nurse_room_vines_flora_torso_vine(Interactable):
    def title(cls):
      return "Thirsty Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      if quest.flora_bonsai['r_vine_1']+quest.flora_bonsai['r_vine_2']+quest.flora_bonsai['r_vine_3']+quest.flora_bonsai['r_vine_4']+quest.flora_bonsai['r_vine_5'] < 4:
        actions.append("?school_nurse_room_flora_torso_vine_interact_fail")
      else:
        actions.append("?school_nurse_room_flora_torso_vine_interact_success")
      if school_nurse_room["torso_vine_use_item"] and quest.flora_bonsai['r_vine_1']+quest.flora_bonsai['r_vine_2']+quest.flora_bonsai['r_vine_3']+quest.flora_bonsai['r_vine_4']+quest.flora_bonsai['r_vine_5'] == 4:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,squashed_vine|Use What?","school_nurse_room_flora_torso_vine_use_item"])

label school_nurse_room_flora_torso_vine_interact_fail: 
  "This little vine... is more like a hurdle of vines and roots that supports most of [flora]'s weight."
  "I should try to take on some of the smaller vines before I try to tackle one this thick."
  return

label school_nurse_room_flora_torso_vine_interact_success:
  "This little vine... is more like a hurdle of vines and roots that supports most of [flora]'s weight."
  "It looks thirsty as usual."
  $school_nurse_room["torso_vine_use_item"] = True
  return
        
label school_nurse_room_flora_torso_vine_use_item(item):
  if item == "water_bottle":
    "Let's try a healthier diet for a change."
    $mc.remove_item(item)
    $mc.add_item("empty_bottle")
    #SFX: hissing noise
    $quest.flora_bonsai["flora_down"] = True
    $quest.flora_bonsai['r_vine_5'] = True 
    $process_event("update_state")
    "Oh, it really didn't like that." with vpunch
    jump quest_flora_bonsai_ending_love
  elif item == "spray_water":
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: spray sounds
    #SFX: hissing noise
    $quest.flora_bonsai["flora_down"] = True
    $quest.flora_bonsai['r_vine_5'] = True
    $process_event("update_state")
    "Like a demon, it burns from a mere spray of holy water." with vpunch
    jump quest_flora_bonsai_ending_love
  else:
    "My [item.title_lower] won't do, I need something healthier. Holier, even."
    $quest.flora_bonsai.failed_item("squashed_vine",item) #TBD this line is improvised, check with lil
  return

### school_nurse_room_tictactoe interactable ##################################

init python:
  class Interactable_school_nurse_room_vines_tictactoe(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_tictactoe_interact")

label school_nurse_room_tictactoe_interact:
  ""
  return

### school_nurse_room_flora_rightarm_vine interactable ########################

init python:
  class Interactable_school_nurse_room_vines_flora_rightarm_vine(Interactable):
    def title(cls):
      return "Kinky Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_flora_rightarm_vine_interact")
      if school_nurse_room["rightarm_vine_use_item"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,kinkvine|Use What?","school_nurse_room_flora_rightarm_vine_use_item"])

label school_nurse_room_flora_rightarm_vine_interact:
  "This little vine is holding [flora]'s right arm captive, but it wouldn't mind being bound instead."
  $school_nurse_room["rightarm_vine_use_item"] = True
  return

label school_nurse_room_flora_rightarm_vine_use_item(item):
  if item == "stethoscope":
    $mc.remove_item(item)
    $quest.flora_bonsai['r_vine_2'] = True
    "There we go. Tied up neatly. And it's liking it!"
  elif item == "ball_of_yarn":
    "The yarn is too weak to hold this beast. Need to find something stronger."
    $quest.flora_bonsai.failed_item("kinkvine",item)
  else:
    "Tying this vine up with my [item.title_lower] would be way too kinky, even for me."
    $quest.flora_bonsai.failed_item("kinkvine",item)
  return

### school_nurse_room_flora_rightboob_vine interactable #######################

init python:
  class Interactable_school_nurse_room_vines_flora_rightboob_vine(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_flora_rightboob_vine_interact")

label school_nurse_room_flora_rightboob_vine_interact:
  ""
  return

### school_nurse_room_sleepvine interactable ##################################

init python:
  class Interactable_school_nurse_room_vines_sleepvine(Interactable):
    def title(cls):
      return "Sleepy Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_sleepvine_interact")
      if school_nurse_room["sleepvine_use_item"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,sleepvine|Use What?","school_nurse_room_sleepvine_use_item"])

label school_nurse_room_sleepvine_interact:
  "This little vine was up all night watching gardening videos."
  "Waking it harshly would be inadvisable... or advisable, depending on how you look at it."
  $school_nurse_room["sleepvine_use_item"] = True
  return

label school_nurse_room_sleepvine_use_item(item):
  if item == "water_bottle":
    mc "Hey! No sleeping at this hour!"
    $mc.remove_item(item)
    $mc.add_item("empty_bottle")
    $quest.flora_bonsai['a_vine_3'] = True
    $process_event("update_state")
    "Angered, wet, and bleary-eyed, this vine went to find another pillow!"
  elif item == "spray_water":
    #SFX: spray sounds:
    mc "Hey! No sleeping at this hour!"
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    $quest.flora_bonsai['a_vine_3'] = True
    $process_event("update_state")
    "Like a hissing cat, this vine went to find another pillow!"
  else:
    "Waking this vine up with my [item.title_lower] would count as pollution, and I'm all about preserving our local ecosystems."
    $quest.flora_bonsai.failed_item("sleepvine",item)
  if quest.flora_bonsai['a_vine_1']+quest.flora_bonsai['a_vine_2']+quest.flora_bonsai['a_vine_3']+quest.flora_bonsai['a_vine_4']+quest.flora_bonsai['a_vine_5'] == 5:
    jump quest_flora_bonsai_ending_lust
  return

### school_nurse_room_laptop_vine interactable ################################

init python:
  class Interactable_school_nurse_room_vines_laptop_vine(Interactable):
    def title(cls):
      return "Nerdy Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("school_nurse_room_laptop_vine_interact")

label school_nurse_room_laptop_vine_interact:
  "This little vine has no friends and a massive Steam library."
  menu(side="middle"):
    extend ""
    "Own this noob":
      $quest.flora_bonsai['a_vine_4'] = True
      $process_event("update_state")
      "After rage-quitting and breaking his keyboard, this vine went to look for a new source of entertainment."
      flora "Ayeeeeeee! Nooo! Not there!"
      # $start.tictactoe #or any other real simple/easy to make minigame
      # If win:
      # "After rage-quitting and breaking his keyboard, this vine went to look for a new source of entertainment."
      # flora "Ayeeeeeee! Nooo! Not there!"
      # hide nerdy_vine with dissolve(.5)
      # show flora_ass_vine #if the player quests flora
      # If lose:
      # 1st time:
      # "That was due to lag! He's so lucky!"
      # Return
      # 2nd time:
      # "Fuck! This kid is clearly cheating!"
      # Return
      # 3rd time:
      # "What the hell! Inhuman! That has to be an aimbot!"
      # Return
      # 4th time:
      # "ESP! Maphack! This guy has a surveillance camera up his own ass! No way he saw me there!"
      # Return
      # 5th time: 
      # "That's it. I'm uninstalling. This game is trash. The devs are incompetent!"
    "Don't pick on beginners":
      return
  if quest.flora_bonsai['a_vine_1']+quest.flora_bonsai['a_vine_2']+quest.flora_bonsai['a_vine_3']+quest.flora_bonsai['a_vine_4']+quest.flora_bonsai['a_vine_5'] == 5:
    jump quest_flora_bonsai_ending_lust
  return

### school_nurse_room_game interactable #######################################

init python:
  class Interactable_school_nurse_room_vines_game(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_game_interact")

label school_nurse_room_game_interact:
  ""
  return

### school_nurse_room_teddy_vine interactable #################################

init python:
  class Interactable_school_nurse_room_vines_teddy_vine(Interactable):
    def title(cls):
      return "Cuddly Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("school_nurse_room_teddy_vine_interact")

label school_nurse_room_teddy_vine_interact:
  "This little vine seems to need a cuddle. It's using the teddy bear as a substitute for real affection."
  menu(side="middle"):
    extend ""
    "Take away the teddy bear":
      $school_nurse_room["teddy_gone"] = True
      $quest.flora_bonsai['a_vine_1'] = True
      $process_event("update_state")
      "Filled with grief and anger, the vine went to look for other soft things to cuddle!"
      flora "Eeeek!"
      #show flora_leftboob_vine #if the player quests flora
    "Leave the poor fellow alone":
      return
  if quest.flora_bonsai['a_vine_1']+quest.flora_bonsai['a_vine_2']+quest.flora_bonsai['a_vine_3']+quest.flora_bonsai['a_vine_4']+quest.flora_bonsai['a_vine_5'] == 5:
    jump quest_flora_bonsai_ending_lust
  return

### school_nurse_room_flora_rightleg_vine interactable ########################

init python:
  class Interactable_school_nurse_room_vines_flora_rightleg_vine(Interactable):
    def title(cls):
      return "Hungry Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_flora_hungry_vine_interact")
      if school_nurse_room["hungry_vine_use_item"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,hungryvine|Use What?","school_nurse_room_flora_hungry_vine_use_item"])

label school_nurse_room_flora_hungry_vine_interact:
  "This little vine is so hungry, it's trying to pull [flora]'s right leg off and chew on it."
  "It'll probably eat just about anything."
  $school_nurse_room["hungry_vine_use_item"] = True
  return

label school_nurse_room_flora_hungry_vine_use_item(item):
  if item == "meds":
    "Here you go, little glutton. Swallow it all down."
    $mc.remove_item(item)
    $quest.flora_bonsai['r_vine_4'] = True
    $process_event("update_state")
    "It looks woozy and... welp, there we go. And it also let go of [flora]'s leg."
  else:
    "It'd probably eat my [item.title_lower] and then hunt for more. Need something that'll sedate it."
    $quest.flora_bonsai.failed_item("hungryvine",item)
  
  return
  
  
### school_nurse_room_poster_vine interactable ################################

init python:
  class Interactable_school_nurse_room_vines_poster_vine(Interactable):
    def title(cls):
      return "Naughty Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("school_nurse_room_poster_vine_interact")

label school_nurse_room_poster_vine_interact:
  "This little vine seems to be getting its rocks off to the medical poster."
  menu(side="middle"):
    extend ""
    "Rip down the poster":
      #SFX: ripping paper
      $school_nurse_room['poster_torn'] = True
      $quest.flora_bonsai['a_vine_2'] = True
      $process_event("update_state")
      "Blue-balled, this vine went to look for other forms of entertainment!"
      flora "Noooo!"
      #show flora_pussy_vine #if the player quests flora
    "Leave it be, vines have needs too":
      return
  if quest.flora_bonsai['a_vine_1']+quest.flora_bonsai['a_vine_2']+quest.flora_bonsai['a_vine_3']+quest.flora_bonsai['a_vine_4']+quest.flora_bonsai['a_vine_5'] == 5:
    jump quest_flora_bonsai_ending_lust
  return

### school_nurse_room_drugged_vine interactable ###############################

init python:
  class Interactable_school_nurse_room_vines_drugged_vine(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_drugged_vine_interact")

label school_nurse_room_drugged_vine_interact:
  ""
  return

### school_nurse_room_flora_leftleg_vine interactable #########################

init python:
  class Interactable_school_nurse_room_vines_flora_leftleg_vine(Interactable):
    def title(cls):
      return "Lazy Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_flora_leftleg_vine_interact")
      if school_nurse_room["leftleg_vine_use_item"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,lazyvine|Use What?","school_nurse_room_flora_leftleg_vine_use_item"])

label school_nurse_room_flora_leftleg_vine_interact:
  "This little vine is holding [flora]'s left leg captive. It's having an easy time and seems to be sleeping on the job."
  "Stabbing it would be a cakewalk."
  $school_nurse_room["leftleg_vine_use_item"] = True
  return

label school_nurse_room_flora_leftleg_vine_use_item(item):
  if item == "nurse_pen":
    #SFX: Stabbing sound
    $mc.remove_item(item) #return pen to desk
    $quest.flora_bonsai['r_vine_3'] = True
    $process_event("update_state")
    "Just a stab in the dark. One step away from you."
  elif item == "pen":
    "This is [isabelle]'s pen. She'll get pissed if I break it."
    $quest.flora_bonsai.failed_item("lazyvine",item)
  elif item == "glass_shard":
    "The shard might slice up my palm. And if this thing gets a taste for blood, we're all doomed."
    $quest.flora_bonsai.failed_item("lazyvine",item)
  else:
    "My [item.title_lower] can cut through armor, but this vine is operating on a philosophical plane of existence."
    $quest.flora_bonsai.failed_item("lazyvine",item)
  return

### school_nurse_room_flora_leftarm_vine interactable #########################

init python:
  class Interactable_school_nurse_room_vines_flora_leftarm_vine(Interactable):
    def title(cls):
      return "Adventurous Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("school_nurse_room_flora_leftarm_vine_interact")

label school_nurse_room_flora_leftarm_vine_interact:
  "This little vine is trying to drag [flora] out the window."
  menu(side="middle"):
    extend ""
    "Slam the window shut on it a few times":
      #SFX: slamming window
      $quest.flora_bonsai['r_vine_1'] = True
      $process_event("update_state")
      "Hurt and in pain, this vine let go of [flora]'s arm!"
      #show squashed_vine with dissolve(.5)
      #hide flora_leftarm_vine #if the player quests flora
    "It's doing no harm":
      return
  return


### school_nurse_room_book interactable #######################################

init python:
  class Interactable_school_nurse_room_vines_book(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_book_interact")

label school_nurse_room_book_interact:
  ""
  return

### school_nurse_room_bookclutter interactable ################################

init python:
  class Interactable_school_nurse_room_vines_bookclutter(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_bookclutter_interact")

label school_nurse_room_bookclutter_interact:
  ""
  return

### school_nurse_room_bookvine interactable ###################################

init python:
  class Interactable_school_nurse_room_vines_bookvine(Interactable):
    def title(cls):
      return "Literate Vine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_nurse_room_bookvine_interact")
      if school_nurse_room["bookvine_use_item"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,bookvine|Use What?","school_nurse_room_bookvine_use_item"])

label school_nurse_room_bookvine_interact:
  "This little vine is reading about the First Amendment."
  "A sword wouldn't be mighty enough."
  $school_nurse_room["bookvine_use_item"] = True
  return

label school_nurse_room_bookvine_use_item(item):
  if item == "nurse_pen":
    "Right. Free speech. Let me just go ahead and strike that out."
    $quest.flora_bonsai['a_vine_5_clutter'] = True
    $process_event("update_state")
    $mc.remove_item(item)
    $school_nurse_room["pen_taken"] = False
    $quest.flora_bonsai['a_vine_5'] = True
    $process_event("update_state")
    "Infuriated with the destruction of knowledge and liberty, this vine went to seek out vengeance."
  elif item == "pen":
    "Hmm... typical. It's out of ink."
    $quest.flora_bonsai.failed_item("bookvine",item)
  else:
    "One day I'll be known as the man who wrote an entire book with his [item.title_lower]... but not today."
    $quest.flora_bonsai.failed_item("bookvine",item)
  if quest.flora_bonsai['a_vine_1']+quest.flora_bonsai['a_vine_2']+quest.flora_bonsai['a_vine_3']+quest.flora_bonsai['a_vine_4']+quest.flora_bonsai['a_vine_5'] == 5:
    jump quest_flora_bonsai_ending_lust
  return


