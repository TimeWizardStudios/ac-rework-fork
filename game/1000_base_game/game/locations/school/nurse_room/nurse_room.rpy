init python:
  class Location_school_nurse_room(Location):

    default_title="Nurse's Office"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(-186,0),"school nurse_room nurse_wall"])

      if game.season == 1:
        scene.append([(77,161),"school nurse_room outside"])
      elif game.season == 2:
        scene.append([(77,162),"school nurse_room outside_autumn"])
      if (quest.kate_wicked in ("costume","party")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(79,165),"school nurse_room outside_night"])

      scene.append([(1412,287),"school nurse_room boobs_right","school_nurse_room_boobs_right"])
      scene.append([(1427,501),"school nurse_room side_table"])
      scene.append([(1259,248),"school nurse_room butt","school_nurse_room_butt"])
      scene.append([(1084,369),"school nurse_room rainbow"])
      scene.append([(1071,272),"school nurse_room beach"])
      scene.append([(958,540),"school nurse_room bin","school_nurse_room_bin"])
      scene.append([(907,213),"school nurse_room clock"])
      scene.append([(562,302),"school nurse_room drawing",("school_nurse_room_drawing",0,-40)])

      if not school_nurse_room['poster_torn']:
        scene.append([(416,263),"school nurse_room boobs_left","school_nurse_room_boobs_left"])

      if ("stuck" > quest.nurse_venting > "ready"
      or quest.nurse_venting == "not_stuck"):
        scene.append([(1195,548),"school nurse_room hole",("school_nurse_room_vent",2,-10)])
        scene.append([(1038,579),"school nurse_room open_vent",("school_nurse_room_vent",152,-41)])
      elif quest.nurse_venting == "stuck":
        scene.append([(1195,548),"school nurse_room hole",("nurse",-3,-9)])
        scene.append([(1038,579),"school nurse_room open_vent",("nurse",147,-40)])
      else:
        scene.append([(1195,548),"school nurse_room hole",("school_nurse_room_vent",2,-10)])
        scene.append([(1192,538),"school nurse_room vent","school_nurse_room_vent"])

      scene.append([(52,82),"school nurse_room curtain"])
      scene.append([(1813,180),"school nurse_room door",("school_nurse_room_door",-20,300)])
      if quest.nurse_venting > "supplies" and quest.nurse_venting.in_progress:
        scene.append([(1038,700),"school nurse_room bed_upside_down_shadow"])
        scene.append([(1218,165),"school nurse_room bed_upside_down",("school_nurse_room_bed",100,80)])
      else:
        scene.append([(1097,447),"school nurse_room bed","school_nurse_room_bed"])
      scene.append([(1020,185),"school nurse_room iron_bar"])

      if school_nurse_room["curtain_off"] or kate.at("school_nurse_room", "sitting") or quest.nurse_venting == "stuck" or quest.jo_washed.in_progress:
        scene.append([(1652,196),"school nurse_room curtain_open"])
      else:
        scene.append([(1030,195),"school nurse_room curtain_closed","school_nurse_room_curtain_closed"])

      scene.append([(831,276),"school nurse_room skeleton","school_nurse_room_skeleton"])
      scene.append([(377,484),"school nurse_room table"])
      scene.append([(637,248),"school nurse_room bookshelf_center"])
      scene.append([(0,48),"school nurse_room bookshelf_left"])
      scene.append([(410,414),"school nurse_room misc"])

      #Medical Supplies
      scene.append([(490,432),"school nurse_room medical_supplies","school_nurse_room_medical_supplies"])

      scene.append([(340,406),"school nurse_room weight","school_nurse_room_weight"])
      scene.append([(0,709),"school nurse_room desk"])
      scene.append([(889,899),"school nurse_room stool"])

      if quest.lindsey_angel == "keycard" and not school_nurse_room["keycard_taken"]:
        scene.append([(951,916),"school nurse_room keycard","school_nurse_room_keycard"])

      if not school_nurse_room["stethoscope_taken"] or quest.jo_washed.in_progress:
        scene.append([(283,717),"school nurse_room stethoscope","school_nurse_room_steth"])

      scene.append([(0,597),"school nurse_room books_desk","school_nurse_room_books"])
      scene.append([(516,615),"school nurse_room cup","school_nurse_room_lollipop_jar"])
      scene.append([(560,573),"school nurse_room sticks",("school_nurse_room_lollipop_jar",-23,43)])
      if not (kate.at("school_nurse_room","sitting") or quest.kate_search_for_nurse.in_progress) and (not school_nurse_room["lollipop_taken"] or quest.jo_washed.in_progress):
        scene.append([(475,556),"school nurse_room lollipop","school_nurse_room_lollipop"])

      scene.append([(677,656),"school nurse_room pot"])
      if game.season == 1:
        if quest.flora_bonsai < "water" or quest.jo_washed.in_progress:
          scene.append([(623,559),"school nurse_room bonsai","school_nurse_room_bonsai"])
        elif quest.flora_bonsai >= "water" and quest.flora_bonsai < "attack":
          scene.append([(377,2),"school nurse_room vines midtree",("school_nurse_room_bonsai",-385,385)])

      if school_nurse_room["puddle_today"]:
        scene.append([(622,714),"school nurse_room vines puddle"])

      if not school_nurse_room["teddy_gone"]:
        scene.append([(191,593),"school nurse_room teddy"])

      scene.append([(471,733),"school nurse_room paper"])

      if not school_nurse_room["pen_taken"] or quest.jo_washed.in_progress:
        scene.append([(614,753),"school nurse_room pen","school_nurse_room_pen"])

      scene.append([(0,621),"school nurse_room laptop"])
      scene.append([(245,812),"school nurse_room below_desk"])
      scene.append([(8,779),"school nurse_room chair_left"])

      if nurse["strike_book_activated"] and not quest.jo_washed.in_progress:
        scene.append([(672,735),"school nurse_room strikebook","school_nurse_room_strikebook"])

      if kate.at("school_nurse_room", "sitting"):
        if not kate.talking:
          if game.season == 1:
            scene.append([(1232,381),"school nurse_room kate","kate"])
          elif game.season == 2:
            scene.append([(1232,381),"school nurse_room kate_autumn","kate"])

      if not nurse.talking:
        if nurse.at("school_nurse_room","sitting"):
          if nurse["strike_book_nude_office_today"]:
            scene.append([(914,425),"school nurse_room nurse_strikebook","nurse"])
          else:
            if game.season == 1:
              scene.append([(914,425),"school nurse_room nurse","nurse"])
            elif game.season == 2:
              scene.append([(913,402),"school nurse_room nurse_autumn","nurse"])
        elif nurse.at("school_nurse_room","stuck"):
          scene.append([(1188,539),"school nurse_room nurse_stuck","nurse"])
          scene.append([(1218,668),"school nurse_room bed_upside_down_leg",("school_nurse_room_bed",285,-423)])

      if not quest.jo_washed.in_progress:
        if school_nurse_room["dollar1_spawned_today"] == True and not school_nurse_room["dollar1_taken_today"]:
          scene.append([(463,833),"school nurse_room dollar1","school_nurse_room_dollar1"])
        if school_nurse_room["dollar2_spawned_today"] == True and not school_nurse_room["dollar2_taken_today"]:
          scene.append([(912,275),"school nurse_room dollar2","school_nurse_room_dollar2"])
        if school_nurse_room["dollar3_spawned_today"] == True and not school_nurse_room["dollar3_taken_today"]:
          scene.append([(650,301),"school nurse_room dollar3","school_nurse_room_dollar3"])

      #lcolor/vcolor values:
      # ""  / "_7hp" / "_milk" / "_pepelepsi" / "_saltedcola" / "_strawberry"/ "_urine"
      #     Vine notes for anyone to understand easily
      #     attack vines: a_vine
      #     1 cuddly_vine (flora_leftboob_vine)
      #     2 naughty_vine (flora_pussy_vine)
      #     3 sleepy_vine (flora_rightboob_vine)
      #     4 nerdy_vine (flora_ass_vine)
      #     5 literate_vine (flora_mouth_vine)
      #     Restrict vines: r_vine
      #     1 adventurous_vine (flora_leftarm_vine)
      #     2 kinky_vine (flora_rightarm_vine)
      #     3 lazy_vine (flora_leftleg_vine)
      #     4 hungry_vine (flora_rightleg_vine)
      #     5 thirsty_vine (flora_torso_vine)
      if quest.flora_bonsai.in_progress:
        if quest.flora_bonsai in ("attack","tinybigtree") and not school_nurse_room["tree_done"]:
          if school_nurse_room["tree"]:
            lcolor = school_nurse_room["tree"]
          else:
            lcolor = "_strawberry"
          vcolor = school_nurse_room["vine"]

          if not quest.flora_bonsai["flora_down"]:
            scene.append([(844,331),"school nurse_room vines flora",("flora",110,10)])

          if quest.flora_bonsai['r_vine_1']: #adventurous_vine
            scene.append([(133,550),"school nurse_room vines squashed_vine"+vcolor])#,"school_nurse_room_vines_squashed_vine"])
          else:
            scene.append([(197,189),"school nurse_room vines flora_leftarm_vine"+vcolor,("school_nurse_room_vines_flora_leftarm_vine",-400,350)])
            scene.append([(136,514),"school nurse_room vines open_window"])

          if not school_nurse_room["teddy_gone"]:
            scene.append([(184,589),"school nurse_room teddy"])
          if quest.flora_bonsai['a_vine_1']: #cuddly vine
            if not quest.flora_bonsai["flora_down"]:
              scene.append([(394,416),"school nurse_room vines flora_leftboob_vine"+vcolor])#,"school_nurse_room_vines_flora_leftboob_vine"])
          else:
            scene.append([(223,639),"school nurse_room vines teddy_vine"+vcolor,("school_nurse_room_vines_teddy_vine",-265,10)])

          if quest.flora_bonsai['r_vine_5']: #thirsty vine
            scene.append([(816,658),"school nurse_room vines watered_vine"+vcolor])#,"school_nurse_room_vines_watered_vine"])
          else:
            scene.append([(717,440),"school nurse_room vines flora_torso_vine"+vcolor,("school_nurse_room_vines_flora_torso_vine",75,400)])

          if quest.flora_bonsai['r_vine_2']: #kinky vine
            scene.append([(1106,659),"school nurse_room vines tied_vine"+vcolor])#,"school_nurse_room_vines_tied_vine"])
          else:
            scene.append([(1106,414),"school nurse_room vines flora_rightarm_vine"+vcolor,("school_nurse_room_vines_flora_rightarm_vine",185,100)])

          if quest.flora_bonsai['r_vine_3']: #lazy vine
            scene.append([(816,736),"school nurse_room vines stabbed_vine"+vcolor])#,"school_nurse_room_vines_stabbed_vine"])
          else:
            scene.append([(816,433),"school nurse_room vines flora_leftleg_vine"+vcolor,("school_nurse_room_vines_flora_leftleg_vine",-25,495)])

          if quest.flora_bonsai['a_vine_3']: #sleepy vine
            if not quest.flora_bonsai["flora_down"]:
              scene.append([(1143,415),"school nurse_room vines flora_rightboob_vine"+vcolor])#,"school_nurse_room_vines_flora_rightboob_vine"])
          else:
            scene.append([(1353,456),"school nurse_room vines sleepvine"+vcolor,("school_nurse_room_vines_sleepvine",80,50)])

          if quest.flora_bonsai['r_vine_4']: #hungry vine
            scene.append([(1130,845),"school nurse_room vines drugged_vine"+vcolor])#,"school_nurse_room_vines_drugged_vine"])
          else:
            scene.append([(1160,491),"school nurse_room vines flora_rightleg_vine"+vcolor,("school_nurse_room_vines_flora_rightleg_vine",70,400)])

          if quest.flora_bonsai['a_vine_2']: #naughty vine
            if not quest.flora_bonsai["flora_down"]:
              scene.append([(731,531),"school nurse_room vines flora_pussy_vine"+vcolor])#,"school_nurse_room_flora_pussy_vine"])
          else:
            scene.append([(478,324),"school nurse_room vines poster_vine"+vcolor,("school_nurse_room_vines_poster_vine",-250,200)])

          scene.append([(1466,0),"school nurse_room vines doorvine"+vcolor])#,"school_nurse_room_vines_doorvine"])
          scene.append([(0,0),"school nurse_room vines bigtree"+lcolor])#,"school_nurse_room_vines_bigtree"])

          if quest.flora_bonsai['a_vine_5']: #literate vine
            if not quest.flora_bonsai["flora_down"]:
              scene.append([(849,134),"school nurse_room vines flora_mouth_vine"+vcolor])#,"school_nurse_room_vines_flora_mouth_vine"])
          else:
            scene.append([(39,111),"school nurse_room vines book",("school_nurse_room_vines_bookvine",383,178)])
            if quest.flora_bonsai['a_vine_5_clutter']:
              scene.append([(56,117),"school nurse_room vines bookclutter"])#,"school_nurse_room_vines_bookclutter"])
            scene.append([(92,119),"school nurse_room vines bookvine"+vcolor,("school_nurse_room_vines_bookvine",40,170)])

          if quest.flora_bonsai['a_vine_4']: #nerdy vine
            scene.append([(0,637),"school nurse_room vines game"])#,"school_nurse_room_vines_game"])
            if not quest.flora_bonsai["flora_down"]:
              scene.append([(0,565),"school nurse_room vines flora_ass_vine"+vcolor])#,"school_nurse_room_vines_flora_ass_vine"])
          else:
            scene.append([(0,637),"school nurse_room vines tictactoe",("school_nurse_room_vines_laptop_vine",116,93)])
            scene.append([(0,730),"school nurse_room vines laptop_vine"+vcolor,("school_nurse_room_vines_laptop_vine",110,0)])

        if quest.flora_bonsai["flora_down"] and not quest.flora_bonsai["flora_free"]:
          if quest.flora_bonsai['r_vine_5']:
            scene.append([(855,566),"school nurse_room vines flora_down"])
          else:
            scene.append([(855,566),"school nurse_room vines flora_down_cum"])

      scene.append([(0,0),"#school nurse_room overlay"])
      if (quest.kate_wicked in ("costume","party")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(0,0),"#school nurse_room overlay_night"])

    def find_path(self,target_location):
      return "school_nurse_room_door",dict(marker_offset=(60,90),marker_sector="right_top")


label goto_school_nurse_room:
  if school_nurse_room.first_visit_today:
    $school_nurse_room["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_nurse_room["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_nurse_room["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_nurse_room)
  return
