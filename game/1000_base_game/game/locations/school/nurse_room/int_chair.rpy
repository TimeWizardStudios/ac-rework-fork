init python:
  class Interactable_school_nurse_room_chair_left(Interactable):
    def title(cls):
      return "Chair"
    def description(cls):
      return "desc"
    def actions(cls,actions):
      actions.append("?school_nurse_room_chair_left_interact")

label school_nurse_room_chair_left_interact:
  "Label"
  return
