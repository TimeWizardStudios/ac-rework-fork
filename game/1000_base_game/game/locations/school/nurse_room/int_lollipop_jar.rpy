init python:
  class Interactable_school_nurse_room_lollipop_jar(Interactable):

    def title(cls):
      return "Lollipop Jar"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif ((not quest.kate_search_for_nurse.started and kate.at("school_nurse_room","sitting")) or quest.kate_search_for_nurse.in_progress) and quest.nurse_aid["name_reveal"]:
        # return "This jar is usually full of lollipops. It looks like someone got hungry. [amelia], probably... she likes lollipops more than the students do."
        return "This jar is usually full of lollipops. It looks like someone got hungry.\n\n[amelia], probably.\n\nShe likes lollipops more than\nthe students do."
      elif (not quest.kate_search_for_nurse.started and kate.at("school_nurse_room","sitting")) or quest.kate_search_for_nurse.in_progress:
        # return "This jar is usually full of lollipops. It looks like someone got hungry. The [nurse], probably... she likes lollipops more than the students do."
        return "This jar is usually full of lollipops. It looks like someone got hungry.\n\nThe [nurse], probably.\n\nShe likes lollipops more than\nthe students do."
      elif quest.nurse_aid["name_reveal"]:
        # return "The lollipops here aren't for patients. [amelia] has always had a sweet tooth."
        return "The lollipops here aren't\nfor patients. [amelia] has always\nhad a sweet tooth."
      else:
        # return "The lollipops here aren't for patients. The [nurse] has always had a sweet tooth."
        return "The lollipops here aren't\nfor patients. The [nurse] has always\nhad a sweet tooth."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if not school_nurse_room["got_pin"]:
          actions.append(["take","Take","?school_nurse_room_lollipop_jar_interact_get_pin"])
      actions.append("?school_nurse_room_lollipop_jar_interact")


label school_nurse_room_lollipop_jar_interact_get_pin:
  "What's this?"
  "Something prickly amongst the sweet."
  $school_nurse_room["got_pin"] = True
  $mc.add_item("safety_pin")
  return

label school_nurse_room_lollipop_jar_interact:
  "Dust and sweet-smelling candy wrappers. The perfect combination for mouth-watering sneezes."
  return
