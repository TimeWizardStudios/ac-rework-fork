init python:
  class Interactable_school_art_class_ball_of_yarn(Interactable):

    def title(cls):
      return "Yellow Ball of Yarn"

    def description(cls):
      return "You spin me right round,\nright round...\n\n...and now I'm all tangled."

    def actions(cls,actions):
      actions.append(["take","Take","school_art_class_ball_of_yarn_take"])
      actions.append("?school_art_class_ball_of_yarn_interact")


label school_art_class_ball_of_yarn_interact:
  "So versatile, so colorful..."
  "Just another tool in the artistic pioneer's art belt."
  return

label school_art_class_ball_of_yarn_take:
  if ("school_art_class_ball_of_yarn_interact",()) not in game.seen_actions:
    call school_art_class_ball_of_yarn_interact
    window hide
  $quest.jacklyn_romance["balls_of_yarn_found"]+=1
  $school_art_class["ball_of_yarn_taken"] = True
  $mc.add_item("ball_of_yarn_yellow",silent=True)
  $game.notify_modal(None,"Inventory","{image=items ball_of_yarn_yellow}{space=50}|Gained\n{color=#900}Yellow Ball of Yarn{/color}\n("+str(quest.jacklyn_romance["balls_of_yarn_found"])+"/5)",5.0)
  return
