init python:
  class Interactable_school_art_class_wooden_ship(Interactable):
    def title(cls):
      return "Wooden Ship"

    def description(cls):
      if "wooden_ship_investigated" not in game.investigate_shown:
        return "{#wooden_ship_investigated}The sight of this ship always fills me with rage. One of my biggest regrets after graduating was not smashing it like a pinata."
      else:
        return "The Flying Dutchman of my haunted past. Forever sailing through my dreams of disappointment."

    def actions(cls,actions):
      if school_art_class["wooden_ship_interacted"]:
        if not school_art_class["stick_interacted"]:
          actions.append(["take","Take","school_art_class_wooden_ship_interact_lindsey_book"])
        actions.append("?school_art_class_wooden_ship_interact_kate_search_for_nurse")
      else:
        actions.append("?school_art_class_wooden_ship_interact")


label school_art_class_wooden_ship_interact:
  "This ship was a group project in my sophomore year. "
  "The anticipation of building it was one of the few things that brought me genuine excitement that year."
  "Of course, [maxine] and [lindsey] decided to kick me out of the group. And, of course, they ended up winning the art contest."
  "That one still stings."
  $school_art_class["wooden_ship_interacted"] = True
  return

label school_art_class_wooden_ship_interact_kate_search_for_nurse:
  "The time will come for this cursed frigate. If only I could release the Kraken upon it."
  return

label school_art_class_wooden_ship_interact_lindsey_book:
  "Take this, cursed frigate!" with vpunch
  "..."
  $mc.add_item("stick")
  $school_art_class["stick_interacted"] = True
  "Ha! Not quite first place worthy anymore, are you?"
  "Let's be honest, it still is."
  return
