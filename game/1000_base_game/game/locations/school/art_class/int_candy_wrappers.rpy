init python:
  class Interactable_school_art_class_candy1(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_art_class_candy1_interact")

label school_art_class_candy1_interact:
  $mc.add_item("wrapper")
  $school_art_class["candy1_taken"] = True
  return

init python:
  class Interactable_school_art_class_candy3(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_art_class_candy3_interact")

label school_art_class_candy3_interact:
  $mc.add_item("wrapper")
  $school_art_class["candy3_taken"] = True
  return

init python:
  class Interactable_school_art_class_candy5(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_art_class_candy5_interact")

label school_art_class_candy5_interact:
  $mc.add_item("wrapper")
  $school_art_class["candy5_taken"] = True
  return

init python:
  class Interactable_school_art_class_candy6(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_art_class_candy6_interact")

label school_art_class_candy6_interact:
  $mc.add_item("wrapper")
  $school_art_class["candy6_taken"] = True
  return      

init python:
  class Interactable_school_art_class_candy7(Interactable):
    def title(cls):
      return "Wrapper"
    def description(cls):
      return "This is what pollutes our oceans. An important part of putting nature in its place."
    def actions(cls,actions):
      actions.append("?school_art_class_candy7_interact")

label school_art_class_candy7_interact:
  $mc.add_item("wrapper")
  $school_art_class["candy7_taken"] = True
  return  
