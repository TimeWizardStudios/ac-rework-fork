init python:
  class Interactable_school_art_class_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_search_for_nurse.started:
        return "Nothing more useful than a door. That's an open and shut case."
      else:
        return "In a place of exotic extravagance and beauty, how come the most alluring thing is the door out?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart":
            actions.append(["go","Fine Arts Wing","?school_art_class_door_lindseyart"])
            return
        elif mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "hidden":
            actions.append(["go","Fine Arts Wing","?quest_lindsey_motive_hidden_door"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "revenge":
            actions.append(["go","Fine Arts Wing","quest_isabelle_dethroning_revenge_art_class_door"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Fine Arts Wing","goto_school_first_hall_west"])
      else:
        if quest.isabelle_tour == "art_class":
          actions.append(["go","Fine Arts Wing","?school_art_class_door_isabelle_tour"])
          return
      actions.append(["go","Fine Arts Wing","goto_school_first_hall_west"])


label school_art_class_door_isabelle_tour:
  "Ditching [isabelle] now would just be dumb. There's only one classroom left to visit."
  return

label school_art_class_door_lindseyart:
  "[lindsey] wet and naked. There's no \"leaving\" in that sentence."
  return
