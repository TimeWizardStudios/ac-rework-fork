init python:
  class Interactable_school_art_class_brush(Interactable):

    def title(cls):
      return "Brush"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["take","Take","?school_art_class_brush_take"])

    #def circle(cls,actions,x,y):
    #  rv =super().circle(actions,x,y)
    #  rv.start_angle=220
    #  return rv


label school_art_class_brush_take:
  "The tool of the artist's trade. Much like the knight's sword and the writer's affinity for bullshit."
  $mc.add_item("brush")
  $school_art_class["brush_taken"] = True
  if quest.jacklyn_art_focus == "brush":
    $quest.jacklyn_art_focus.advance("artist")
  return
