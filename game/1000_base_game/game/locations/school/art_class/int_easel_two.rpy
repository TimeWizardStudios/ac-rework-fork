init python:
  class Interactable_school_art_class_easel_02(Interactable):

    def title(cls):
      return "Easel"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_search_for_nurse.started:
        return "It's an all right painting. Didn't like it very much myself, but there might be a few fans for it out there. Rating: 10/10."
      else:
        return "They say that Leonardo da Vinci worked his entire life on the Mona Lisa. Some art is perhaps never finished.\n\nThis piece definitely isn't."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.kate_search_for_nurse.started:
        actions.append("?school_art_class_easel_02_interact_kate_search_for_nurse")
      actions.append("?school_art_class_easel_02_interact")


label school_art_class_easel_02_interact:
  "Maybe I can finish it..."
  "..."
  "..."
  "Nope. Just made it worse."
  return

label school_art_class_easel_02_interact_kate_search_for_nurse:
  "If art is entirely subjective, how come there are professional artists out there?"
  return
