init python:
  class Interactable_school_art_class_art_in_progress(Interactable):

    def title(cls):
      return "Art In Progress"

    def description(cls):
      return "An artist takes his time. Patience is perfection."

    def actions(cls,actions):
      actions.append("goto_school_art_in_progress")


init python:
  class Location_school_art_in_progress(Location):

    default_title = "Art Class"

    def scene(self,scene):
      scene.append([(0,0),"school art_class art_in_progress canvas"])
      scene.append([(180,0),"school art_class art_in_progress sketch_libation",("school_art_in_progress_libation",0,300)])
      if self["libation"]:
        scene.append([(136,0),"school art_class art_in_progress libation",("school_art_in_progress_libation",-18,300)])
      scene.append([(207,40),"school art_class art_in_progress sketch_suction",("school_art_in_progress_suction",-100,700)])
      if self["suction"]:
        scene.append([(289,542),"school art_class art_in_progress suction",("school_art_in_progress_suction",23,198)])
      scene.append([(699,0),"school art_class art_in_progress sketch_caramelization",("school_art_in_progress_caramelization",100,650)])
      if self["caramelization"]:
        scene.append([(908,342),"school art_class art_in_progress caramelization",("school_art_in_progress_caramelization",2,308)])
      scene.append([(907,974),"school art_class art_in_progress exit_arrow","school_art_in_progress_exit_arrow"])

    def find_path(self,target_location):
      return "school_art_in_progress_exit_arrow"


label goto_school_art_in_progress:
  $game.location = school_art_in_progress
  return


init python:
  class Interactable_school_art_in_progress_libation(Interactable):

    def title(cls):
      return "Libation"

    def description(cls):
      if school_art_in_progress["libation"]:
        return "Uncharacteristic and jarring? Totally a style choice."
      else:
        return "Totally amateurish and downright repulsive? Style choice."

    def actions(cls,actions):
      if school_art_in_progress["libation"]:
        actions.append("?school_art_in_progress_libation_interact_pepelepsi")
      else:
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jacklyn_sweets,art_in_progress_libation|Use What?","school_art_in_progress_libation_use"])
        actions.append("?school_art_in_progress_libation_interact")


label school_art_in_progress_libation_interact_pepelepsi:
  "I painted the first thing that came into my head."
  "And it just so happened to be... the best art in the world. It was the best art in the world!"
  return

label school_art_in_progress_libation_interact:
  "Not exactly my best work."
  "I'm not really happy with how it turned out."
  "..."
  "Fuck it. Self-doubt won't make this piece shine."
  "I need need full on carbonated, dripping, dirty-ass motor oil!"
  return

label school_art_in_progress_libation_use(item):
  if item in ("pepelepsi","spray_pepelepsi"):
    $school_art_in_progress["libation"] = True
    $mc.remove_item(item)
    $mc.add_item(("spray_empty_bottle" if item == "spray_pepelepsi" else "empty_bottle"))
    "Toxic and highly corrosive."
    "The devil in a can? This is the devil on a canvas!"
  else:
    "My [item.title_lower]. My way."
    "The wrong way."
    $quest.jacklyn_sweets.failed_item("art_in_progress_libation",item)
  return


init python:
  class Interactable_school_art_in_progress_suction(Interactable):

    def title(cls):
      return "Suction"

    def description(cls):
      if school_art_in_progress["suction"]:
        return "Cliché meets kitsch? Style choice."
      else:
        return "Everyone who's ever seen it died after seven days? That, too, is a style choice."

    def actions(cls,actions):
      if school_art_in_progress["suction"]:
        actions.append("?school_art_in_progress_suction_interact_lollipops")
      else:
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jacklyn_sweets,art_in_progress_suction|Use What?","school_art_in_progress_suction_use"])
        actions.append("?school_art_in_progress_suction_interact")


label school_art_in_progress_suction_interact_lollipops:
  "Candy, anyone?"
  "Yes?"
  "Okay, please step into my art van."
  return

label school_art_in_progress_suction_interact:
  "Impeccable lines! Just impeccable!"
  "Well, except that line..."
  "And that line..."
  "Okay, there are a few really peccable lines here."
  "I guess I need to turn the pecks into suck-ulent sweetness."
  return

label school_art_in_progress_suction_use(item):
  if item == "lollipop":
    $lollipops = mc.owned_item_count("lollipop")
    if lollipops < 3:
      "It's not nearly sweet enough."
      "It needs to be two times as sweet! Or maybe three times as sweet!"
      "If the entire school was on fire and it rained sweets, maybe that would be sweet enough?"
      "But err... probably not."
    else:
      $school_art_in_progress["suction"] = True
      $mc.remove_item("lollipop",3)
      $mc.add_item("wrapper",3)
      "Sweet sweets o' mine, make this canvas oh so fine!"
  else:
    "Not going to waste my precious [item.title_lower] on some fake-ass art project."
    $quest.jacklyn_sweets.failed_item("art_in_progress_suction",item)
  return


init python:
  class Interactable_school_art_in_progress_caramelization(Interactable):

    def title(cls):
      return "Caramelization"

    def description(cls):
      if school_art_in_progress["caramelization"]:
        return "Tacky colors? It's a style choice."
      else:
        return "Sloppy lines? It's a style choice."

    def actions(cls,actions):
      if school_art_in_progress["caramelization"]:
        actions.append("?school_art_in_progress_caramelization_interact_doughnuts")
      else:
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jacklyn_sweets,art_in_progress_caramelization|Use What?","school_art_in_progress_caramelization_use"])
        actions.append("?school_art_in_progress_caramelization_interact")


label school_art_in_progress_caramelization_interact_doughnuts:
  "The details..."
  "They speak to you."
  "Whisper in your ear."
  "{i}\"Diabetes.\"{/}"
  return

label school_art_in_progress_caramelization_interact:
  "Fluffy and sweet. Something to make the critics puke sugar."
  return

label school_art_in_progress_caramelization_use(item):
  if item == "doughnut":
    $doughnuts = mc.owned_item_count("doughnut")
    if doughnuts < 3:
      "Art is all about exact amounts — three doughnuts, in this case."
    else:
      $school_art_in_progress["caramelization"] = True
      $mc.remove_item("doughnut",3)
      "Just like that, squash them on that canvas!"
      "If a taped banana is art, this is a masterpiece."
  else:
    "The artistic value of my [item.title_lower]: Nil."
    $quest.jacklyn_sweets.failed_item("art_in_progress_caramelization",item)
  return


init python:
  class Interactable_school_art_in_progress_exit_arrow(Interactable):

    def title(cls):
      return "Back"

    def actions(cls,actions):
      actions.append(["go","Art Classroom","exit_school_art_in_progress"])


label exit_school_art_in_progress:
  $game.location = school_art_class
  return
