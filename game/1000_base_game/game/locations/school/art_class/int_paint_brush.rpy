init python:
  class Interactable_school_art_class_paint_brush(Interactable):

    def title(cls):
      return "Paint Brush"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "If only I could paint over all my mistakes."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_art_class_paint_brush_interact")

    #def circle(cls,actions,x,y):
    #  rv =super().circle(actions,x,y)
    #  rv.start_angle=220
    #  return rv


label school_art_class_paint_brush_interact:
  "Sometimes during class, I would brush the bristles against my skin to simulate human contact."
  return
