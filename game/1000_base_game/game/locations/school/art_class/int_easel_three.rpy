init python:
  class Interactable_school_art_class_easel_03(Interactable):

    def title(cls):
      return "Easel"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_search_for_nurse.started:
        return "An accurate depiction of a dove carrying a letter through a cloud. Very artistic."
      else:
        return "Ah! A perfect depiction of a polar bear eating sugar on an iceberg in a snowstorm. True art."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.jacklyn_art_focus == "artist":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jacklyn_art_focus,easel|Use What?","jacklyn_art_focus_easel_use"])
        elif quest.jacklyn_art_focus == "canvas":
          actions.append("?school_art_class_easel_03_jacklyn_art_focus_start")
        if quest.jacklyn_sweets == "artroom":
          if quest.jacklyn_sweets["paper_removed"]:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jacklyn_sweets,easel|Use What?","quest_jacklyn_sweets_artroom_use"])
          actions.append("?quest_jacklyn_sweets_artroom_interact")
      if mc.owned_item("brush") and not quest.jacklyn_art_focus.started:
        actions.append("?school_art_class_easel_03_jacklyn_art_focus_permission")
        return
      if quest.kate_search_for_nurse.started:
        actions.append("?school_art_class_easel_03_interact_kate_search_for_nurse")
      actions.append("?school_art_class_easel_03_interact")


label school_art_class_easel_03_interact:
  "Going to need a brush. Finger painting is probably frowned on in these highly pretentious circles."
  return

label school_art_class_easel_03_interact_kate_search_for_nurse:
  "Different strokes for different folks. Is that about painting or masturbation?"
  return

label school_art_class_easel_03_jacklyn_art_focus_start:
  "Even though the minimalism of this canvas is highly attractive, [jacklyn] probably won't feel the same."
  "I need to find some paint to begin with."
  $quest.jacklyn_art_focus.advance("paint")
  return

label school_art_class_easel_03_jacklyn_art_focus_permission:
  "Got the brush, but first I need permission."
  "I'm a sucker for authority."
  return
