init python:
  class Interactable_school_art_class_supply_closet(Interactable):

    def title(cls):
      return "Supply Closet"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.kate_search_for_nurse.started:
        return "Supposedly, you'll find everything you need here. Where the hell is my muse?"
      elif quest.flora_jacklyn_introduction.started:
        return "Being an artist is less about equipment and more about imagination and execution — you can't have art without either."
      else:
        return "Whenever my artist career takes off, these things will come in handy. For now, I'll stick to drawing stick figures."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "revenge":
            actions.append("quest_isabelle_dethroning_revenge_art_class_supply_closet")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        if quest.kate_search_for_nurse == "open_closet":
          actions.append(["use_item","Use Item","select_inventory_item","$quest_item_filter:kate_search_for_nurse,closet|Use What?","school_art_class_supply_closet_kate_quest_search_for_nurse_pick_open"])
        if quest.poolside_story == "confetti" and not quest.poolside_story["old_artwork_acquired"]:
          actions.append(["take","Take","school_art_class_supply_closet_poolside_story_confetti"])
        elif quest.poolside_story == "makedecor" and not quest.poolside_story["string_acquired"]:
          actions.append(["take","Take","school_art_class_supply_closet_poolside_story_makedecor"])
        if quest.nurse_venting == "supplies" and quest.nurse_venting["naked"] and not quest.nurse_venting["body_oil_taken"]:
          actions.append(["take","Take","quest_nurse_venting_supplies_supply_closet"])
        if quest.kate_search_for_nurse == "start":
          actions.append("?school_art_class_supply_closet_kate_quest_search_for_nurse")
      if quest.flora_jacklyn_introduction.started:
        actions.append("?school_art_class_supply_closest_interact_flora_jacklyn_introduction")
      actions.append("?school_art_class_supply_closet_interact")


label school_art_class_supply_closet_interact:
  "This is where all the pranksters find their equipment."
  "It's so easy to place a bucket of paint over a doorway and just watch an unsuspecting victim get drenched."
  "That victim is usually me."
  return

label school_art_class_supply_closest_interact_flora_jacklyn_introduction:
  "Supplies and other stuff... basically, everything you need to be a Picasso. Well, you also need skills. "
  "Unless you're into modern art. Then, you just need a banana and some tape."
  return

label school_art_class_supply_closet_kate_quest_search_for_nurse:
  "The trail ends here. The supply closet."
  "..."
  "It's locked."
  #SFX: Bumping noise      tbd
  ## Locker bumping
  play sound "LockerBump"
  "Huh?"
  mc "Is someone in there?"
  "..."
  "I swear I heard something."
  "Weird. I didn't think the [nurse] would be able to fit inside."
  mc "Why are you hiding from [kate]?"
  if quest.kate_search_for_nurse["wrappers_picked"] >= 22:
    "Need to find a way to open it. The [nurse] is clearly too scared to talk."
    $quest.kate_search_for_nurse.advance("open_closet")
  else:
    #SFX: Door Creak           tbd
    ## Door creak
    play sound "bedroom_door"
    $quest.kate_search_for_nurse["kate_found_wrappers"] = True
    show kate neutral at appear_from_left(.5)
    "Shit. Speak of the devil."
    #kate in
    kate neutral "Did you just say my name?"
    kate eyeroll "I mean, I know you're really thirsty and probably have a bunch of disgusting fantasies about me, but come on."
    kate eyeroll "Anyway, have you managed to find the little slut yet?"
    mc "Slut?"
    kate excited "The [nurse], I mean. Sorry... slip up."
    "Hmm... [kate] probably followed the trail of candy wrappers here too."
    "It's risky getting in [kate]'s way, so the smart choice would be to tell the truth."
    "But then, I would be devastated if someone gave me up to [kate]."
    show kate excited at move_to(.25)
    menu(side="right"):
      extend ""
      "\"Yeah, she's in the supply closet.\"":
        $quest.kate_search_for_nurse["side_with_kate"] = True
        show kate excited at move_to(.5)
        mc "Yeah, she's in the supply closet."
        $kate.lust+=1
        kate confident "You're a good boy."
        kate confident "Here's a treat."
        $mc.add_item("wrapper",(22-mc.owned_item_count("wrapper")))
        kate laughing "Throw that away for me, will you?"
        mc "..."
        kate smile "Did you really think I was going to give you a reward?"
        kate smile "Anyway, open the supply closet."
        mc "It's locked."
        kate thinking "Hmm..."
        show kate thinking at move_to(.75)
        menu(side="left"):
          extend ""
          "\"We'll need to find a way to unlock it.\"":
            show kate thinking at move_to(.5)
            mc "We'll need to find a way to unlock it."
            kate eyeroll "No shit."
            kate annoyed_right "Why is she being so difficult? She knows it'll only be worse for her."
            mc "What will?"
            kate excited "Nothing of your concern."
            kate excited "Now, get to opening this door."
            show kate excited at disappear_to_left
            "Hmm... not the easiest lock to pick. Going to need a pretty high-tech tool."
            $quest.kate_search_for_nurse.advance("open_closet")
          "?mc.strength>=5@[mc.strength]/5|{image=stats str}|\"Want me to break it open for you?\"":
            show kate thinking at move_to(.5)
            mc "Want me to break it open for you?"
            kate laughing "You're not breaking anything open with those little T-Rex arms."
            mc "I totally can."
            kate smile "Okay, let's see it."
            "The power flows through me. A little wooden door can't stand in my way."
            show black onlayer overlay with Dissolve(.5)
            show black onlayer overlay
            play sound "<from 8.0 to 10.2>door_breaking_in"
            pause(1.5)
            show black onlayer overlay with vpunch
            show kate surprised
            hide black
            with Dissolve(.5)
            kate surprised "You did it!"
            $kate.love+=1
            kate surprised "I did not expect that!"
            if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_strength"]:
              $mc["moments_of_glory"]+=1
              $mc["moments_of_glory_strength"] = True
              $game.notify_modal(None,"Guts & Glory","Moments of Glory: Strength\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#             if mc["moments_of_glory"] == 3:
#               $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
            "First time I've seen [kate] genuinely surprised."
            "She really seemed to like this primal display of strength."
            "It's like a crack in her bitchy armor."
            "How typical is it that the only thing that impresses her is something as basic as muscles."
            "That's why the jocks get all the girls."
            mc "Okay, [nurse], you've been found. Come on out."
            mc "..."
            show kate surprised at move_to(.25)
            jump school_art_class_supply_closet_kate_quest_search_for_nurse_force_open
      "\"Sorry, I haven't seen her.\"":
        show kate excited at move_to(.5)
        mc "Sorry, I haven't seen her."
        kate neutral "Are you lying to me?"
        mc "...No."
        kate neutral "Look me in the eye."
        mc "I... I haven't seen her."
        kate laughing "Okay, relax!"
        kate neutral "But if you see her, you bring her to me. Understand?"
        mc "Yes, I understand."
        kate laughing "Come on, smile. No need to be scared!"
        kate neutral "Unless you cross me."
        kate confident "Bye!"
        show kate confident at disappear_to_left
        "Ugh, she's giving me gray hairs."
        "All right, time to find a way to unlock this thing."
        show kate thinking flip at appear_from_left
        kate thinking flip "Actually, [mc], there's something I wanted to talk to you about."
        mc "Oh?"
        kate blush flip "I kinda sorta wanted to ask you for a favor..."
        mc "Of course! Anything! What is it?"
        $mc.add_item("wrapper",(22-mc.owned_item_count("wrapper")))
        kate laughing "Throw that away for me, will you?"
        show kate laughing at disappear_to_left
        "I hate myself."
        $quest.kate_search_for_nurse["side_with_kate"] = False
        $quest.kate_search_for_nurse.advance("open_closet")
  return

label school_art_class_supply_closet_kate_quest_search_for_nurse_force_open:
  show spinach neutral at appear_from_right(.75)
  spinach "Meow?"
  kate gushing "It's a little kitty!"
  "Hmm... it was [maxine]'s cat hiding in the supply closet, not the [nurse]..."
  kate gushing "I love cats! They're so fluffy and cute!"
  kate gushing "Come here, kitty kitty."
  spinach "Hiss!" with hpunch
  kate sad "She hates me."
  "No wonder there. I bet animals can sense evil."
  spinach "Meow!"
  show kate sad at move_to(.5)
  hide spinach with moveoutright
  $quest.kate_search_for_nurse.advance("give_wrapper")
  $process_event("update_state")
  "The cat darted out of the closet. Probably couldn't stand the company."
  kate annoyed_right "Whatever. Stupid cat."
  kate excited "Anyway, let me know if you find the [nurse]."
  show kate excited at disappear_to_left
  pause(1)
  return

label school_art_class_supply_closet_kate_quest_search_for_nurse_pick_open(item):
  if item == "high_tech_lockpick":
    #SFX: Lock clicking open tbd
    ## Lock pick
    play sound "lock_click"
    if quest.kate_search_for_nurse["side_with_kate"]:
      "It... worked...?"
      "What are all these emotions I'm feeling?"
      "Happiness?"
      "Excitement?"
      "A sense of self worth?"
      "Only EA fans know this level of satisfaction."
      show kate laughing at appear_from_left
      kate "Look at that! You actually did it. The idiot fumbled himself into being useful for once."
      mc "Damn straight, Katie!"
      kate cringe "Gross."
      kate confident "Let's just extract my favorite subject."
      mc "Okay, [nurse], you've been found. Come on out."
      mc "..."
      show kate confident at move_to(.25)
      jump school_art_class_supply_closet_kate_quest_search_for_nurse_force_open
    else:
      mc "Aha! Found you!"
      show spinach neutral at appear_from_right(.5)
      spinach "Meow?"
      "Huh! It's just [maxine]'s silly cat."
      "Although someone definitely locked her inside the supply closet."
      "The trail of candy wrappers made it too easy. I should've known it was a decoy."
      "The [nurse] is still hiding somewhere. Better find her before [kate] does."
      $quest.kate_search_for_nurse.advance("give_wrapper")
      $process_event("update_state")
      hide spinach with Dissolve(.5)
  else:
    "Just like in Mission Impossible, I'll unlock this door only using my [item.title_lower]."
    "..."
    "..."
    "What the fuck! How did Tom Cruise do it?"
    $quest.kate_search_for_nurse.failed_item("closet",item)
  return

label school_art_class_supply_closet_poolside_story_confetti:
  "Anything in here I can use for confetti?"
  "..."
  "Hmm... no mylar or colored paper."
  "Maybe I'll just use my old paintings. It's not like they'll get a passing grade anyway."
  "[jacklyn] probably hates them as well."
  "Yeah. This is the way."
  $mc.add_item("old_artwork")
  $quest.poolside_story["old_artwork_acquired"] = True
  "Now I just need to find a way to shred this."
  "A pair of scissors? A wood chipper? Anything will do."
  return

label school_art_class_supply_closet_poolside_story_makedecor:
  "String. Check."
  "Not the type of string I'm usually looking for, but I've definitely got an eye for it."
  $mc.add_item("string")
  $quest.poolside_story["string_acquired"] = True
  return
  #revert interact lines back to previous
