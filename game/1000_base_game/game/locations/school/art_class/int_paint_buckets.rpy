init python:
  class Interactable_school_art_class_paint_buckets(Interactable):
    def title(cls):
      return "Paint Buckets"
    def description(cls):
      return "Non-toxic. What a letdown."
    def actions(cls,actions):

      actions.append("?school_art_class_paint_buckets_interact")

label school_art_class_paint_buckets_interact:
  "Should've went with tape and a banana."
  return
