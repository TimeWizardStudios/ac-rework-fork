init python:
  class Location_school_art_class(Location):

    default_title="Art Class"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      #Scene
      scene.append([(-74,-287),"school art_class art_class"])

      #Shelf
      scene.append([(54,214),"school art_class shelf","school_art_class_shelf"])

      #Brush
      if game.season == 1:
        if not school_art_class["brush_taken"] or quest.jo_washed.in_progress:
          scene.append([(1721,801),"school art_class brush","school_art_class_brush"])
      scene.append([(1729,734),"school art_class big_brush","school_art_class_paint_brush"])

      #Door
      scene.append([(1080,363),"school art_class door","school_art_class_door"])

      #Supply Closet
      scene.append([(1515,220),"school art_class supply_closet",("school_art_class_supply_closet",0,50)])
      scene.append([(672,270),"school art_class paintings"])
      scene.append([(1533,270),"school art_class paintings_supply_closet",("school_art_class_supply_closet",17,0)])

      #Discarded Art
      if game.season == 1:
        if not school_art_class["discarded_art_3_taken"] or quest.jo_washed.in_progress:
          scene.append([(1371,854),"school art_class discardedart_3","school_art_class_discardedart_3"])
        if not school_art_class["discarded_art_2_taken"] or quest.jo_washed.in_progress:
          scene.append([(1161,835),"school art_class discardedart_2","school_art_class_discardedart_2"])
        if not school_art_class["discarded_art_1_taken"] or quest.jo_washed.in_progress:
          scene.append([(87,871),"school art_class discardedart_1","school_art_class_discardedart_1"])

      scene.append([(368,624),"school art_class shadows"])
      scene.append([(920,409),"school art_class easel_1","school_art_class_easel_01"])
      if quest.jacklyn_sweets == "artroom" and quest.jacklyn_sweets["paper_removed"]:
        scene.append([(842,422),"school art_class easel_3_nopaper","school_art_class_easel_03"])
      elif quest.jacklyn_sweets in ("paint","finaltouches"):
        scene.append([(842,422),"school art_class easel_3_artinprogress","school_art_class_art_in_progress"])
        if school_art_in_progress["libation"]:
          scene.append([(879,438),"school art_class easel_3_artinprogress_pepelepsi",("school_art_class_art_in_progress",-4,-16)])
        if school_art_in_progress["suction"]:
          scene.append([(892,513),"school art_class easel_3_artinprogress_lollipops",("school_art_class_art_in_progress",6,-91)])
        if school_art_in_progress["caramelization"]:
          scene.append([(929,485),"school art_class easel_3_artinprogress_doughnuts",("school_art_class_art_in_progress",-32,-63)])
      else:
        scene.append([(841,422),"school art_class easel_3","school_art_class_easel_03"])

      if quest.jacklyn_romance == "hide_and_seek" and not school_art_class["ball_of_yarn_taken"]:
        scene.append([(1452,617),"school art_class ball_of_yarn","school_art_class_ball_of_yarn"])

      scene.append([(1404,333),"school art_class statue_right","school_art_class_statue_right"])
      scene.append([(610,309),"school art_class statue_left"])
      scene.append([(1537,563),"school art_class chair5","school_art_class_chairs"])
      scene.append([(1236,507),"school art_class chair4","school_art_class_chairs"])
      scene.append([(728,704),"school art_class chair3","school_art_class_chairs"])
      if school_art_class["chair2_moved"]:
        scene.append([(389,683),"school art_class chair2_moved_shadow"])
        scene.append([(401,561),"school art_class chair2_moved","school_art_class_chairs"])
      else:
        scene.append([(658,668),"school art_class chair2_shadow"])
        scene.append([(650,543),"school art_class chair2","school_art_class_chairs"])
      scene.append([(356,642),"school art_class chair1","school_art_class_chairs"])
      scene.append([(1318,414),"school art_class easel_2","school_art_class_easel_02"])
      if not quest.jo_washed.in_progress:
        scene.append([(905,4),"school art_class ship",("school_art_class_wooden_ship",0,200)])

      #Book
      if not school_art_class["book_taken"] and not quest.jo_washed.in_progress:
        scene.append([(230,472),"school art_class book","school_art_class_book"])

      #Smashed Ship
      if 0==1:
        scene.append([(897,232),"school art_class ship_smashed",("school_art_class_wooden_ship_smashed",0,200)])

      #Paint Buckets
      if game.season == 1 and not quest.jo_washed.in_progress:
        if school_art_class["easel_paint_buckets"]:
          scene.append([(829,627),"school art_class paint_buckets","school_art_class_paint_buckets"])

      #Candy Wrappers
      if not quest.kate_search_for_nurse["kate_found_wrappers"]:
        if quest.kate_search_for_nurse.in_progress and quest.kate_search_for_nurse=="start":
          if not school_art_class["candy7_taken"]:
            scene.append([(1710,735),"school art_class candy7","school_art_class_candy7"])
          if not school_art_class["candy6_taken"]:
            scene.append([(1570,809),"school art_class candy6","school_art_class_candy6"])
          if not school_art_class["candy5_taken"]:
            scene.append([(1360,854),"school art_class candy5","school_art_class_candy5"])
          if not school_art_class["candy3_taken"]:
            scene.append([(1252,806),"school art_class candy3","school_art_class_candy3"])
          if not school_art_class["candy1_taken"]:
            scene.append([(1193,638),"school art_class candy1","school_art_class_candy1"])

      if not quest.lindsey_wrong in("lindseyart","florahelp","floralocker","lindseyclothes"):

        #Naked Kate
        if not school_art_class["exclusive"] or "kate" in school_art_class["exclusive"]:
          if kate.at("school_art_class","pose"):
            if not kate.talking:
              scene.append([(849,408),"school art_class kate","kate"])

        #Naked Nurse
        if not school_art_class["exclusive"] or "nurse" in school_art_class["exclusive"]:
          if nurse.at("school_art_class", "pose"):
            if not nurse.talking:
              scene.append([(763,595),"school art_class nurse","nurse"])

        #Naked MrsL
        if not school_art_class["exclusive"] or "mrsl" in school_art_class["exclusive"]:
          if mrsl.at("school_art_class", "pose"):
            if not mrsl.talking:
              scene.append([(506,372),"school art_class mrsl","mrsl"])

        #Flora
        if not school_art_class["exclusive"] or "flora" in school_art_class["exclusive"]:
          if flora.at("school_art_class", "selfie"):
            if not flora.talking:
              if game.season == 1:
                if flora.equipped_item("flora_skirt"):
                  scene.append([(656,345),"school art_class flora_skirt","flora"])
                else:
                  scene.append([(656,345),"school art_class flora","flora"])
              elif game.season == 2:
                scene.append([(656,345),"school art_class flora_autumn","flora"])

        #Isabelle
        if not school_art_class["exclusive"] or "isabelle" in school_art_class["exclusive"]:
          if isabelle.at("school_art_class", "standing"):
            if not isabelle.talking:
              if game.season == 1:
                scene.append([(172,339),"school art_class isabelle",("isabelle",0,50)])
              elif game.season == 2:
                scene.append([(172,339),"school art_class isabelle_autumn",("isabelle",0,50)])

        #Jacklyn
        if not school_art_class["exclusive"] or "jacklyn" in school_art_class["exclusive"]:
          if not jacklyn.talking:
            if jacklyn.at("school_art_class","standing"):
              if game.season == 1:
                if quest.jo_washed.in_progress and not jacklyn.equipped_item("jacklyn_shirt"):
                  scene.append([(361,389),"school art_class jacklyn_dream","jacklyn"])
                else:
                  scene.append([(361,389),"school art_class jacklyn","jacklyn"])
              elif game.season == 2:
                scene.append([(365,393),"school art_class jacklyn_autumn","jacklyn"])
            elif jacklyn.at("school_art_class","pose"):
              scene.append([(333,390),"school art_class jacklyn_pose","jacklyn"])

        #Spinach
        if not school_art_class["exclusive"] or "spinach" in school_art_class["exclusive"]:
          if spinach.at("school_art_class", "standing"):
            if not spinach.talking:
              scene.append([(1229,671),"school art_class cat_look","spinach"])
          elif spinach.at("school_art_class", "running"):
            if not spinach.talking:
              scene.append([(1205,687),"school art_class cat_run","spinach"])

      #Lindsey
      if not school_art_class["exclusive"] or "lindsey" in school_art_class["exclusive"]:
        if not lindsey.talking:
          if lindsey.at("school_art_class", "naked"):
            scene.append([(1229,400),"school art_class lindseynaked","lindsey"])
          if lindsey.at("school_art_class", "mcshirt"):
            scene.append([(1229,400),"school art_class lindseyshirt","lindsey"])
          if lindsey.at("school_art_class", "standing"):
            scene.append([(1229,400),"school art_class lindsey","lindsey"])

      #Money
      if not quest.jo_washed.in_progress:
        if school_art_class["dollar1_spawned_today"] == True and not school_art_class["dollar1_taken_today"]:
          scene.append([(253,301),"school art_class dollar1","school_art_class_dollar1"])
        if school_art_class["dollar2_spawned_today"] == True and not school_art_class["dollar2_taken_today"]:
          scene.append([(814,465),"school art_class dollar2","school_art_class_dollar2"])
        if school_art_class["dollar3_spawned_today"] == True and not school_art_class["dollar3_taken_today"]:
          scene.append([(1613,256),"school art_class dollar3","school_art_class_dollar3"])

      if quest.isabelle_dethroning == "revenge" and not school_art_class["lights_on"]:
        scene.append([(0,0),"#school art_class night_overlay"])

    def find_path(self,target_location):
      return "school_art_class_door"


label goto_school_art_class:
  if school_art_class.first_visit_today:
    $school_art_class["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_art_class["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_art_class["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_art_class)
  return
