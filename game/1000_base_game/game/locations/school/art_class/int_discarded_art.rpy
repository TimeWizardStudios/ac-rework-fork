init python:
  class Interactable_school_art_class_discardedart_3(Interactable):

    def title(cls):
      return "Discarded Art"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "[maxine] made this. It's a... hmm... no idea."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["take","Take","?school_art_class_discarded_art3_take"])
      actions.append("?school_art_class_discarded_art3_interact")


label school_art_class_discarded_art3_interact:
  "Old artwork, left to roll aimlessly across the desert roads of the art classroom."
  return

label school_art_class_discarded_art3_take:
  "Pennant material!"
  $school_art_class["discarded_art_3_taken"]=True
  $mc.add_item("discarded_art3")
  return


init python:
  class Interactable_school_art_class_discardedart_2(Interactable):

    def title(cls):
      return "Discarded Art"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "[flora] made this... with actual spaghetti bolognese. No wonder she's an A-student."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["take","Take","?school_art_class_discarded_art2_take"])
      actions.append("?school_art_class_discarded_art2_interact")


label school_art_class_discarded_art2_interact:
  "This is what discarded avant-garde art looks like. Very similar to the undiscarded pieces."
  return

label school_art_class_discarded_art2_take:
  "Perfect to cut into pennants."
  $school_art_class["discarded_art_2_taken"]=True
  $mc.add_item("discarded_art2")
  return


init python:
  class Interactable_school_art_class_discardedart_1(Interactable):

    def title(cls):
      return "Discarded Art"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "Chad made this. It's a giant purple dick."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
      actions.append(["take","Take","?school_art_class_discarded_art1_take"])
      actions.append("?school_art_class_discarded_art1_interact")


label school_art_class_discarded_art1_interact:
  "Trying until you succeed. Or in Chad's case, until you break the easel."
  return

label school_art_class_discarded_art1_take:
  "I'll cut pennants from these."
  $school_art_class["discarded_art_1_taken"]=True
  $mc.add_item("discarded_art1")
  return
