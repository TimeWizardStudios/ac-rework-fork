init python:
  class Interactable_school_art_class_shelf(Interactable):

    def title(cls):
      return "Paint Shelf"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.mrsl_bot == "paint":
        return "Is it really a diversion if a bucket of paint isn't involved?"
      elif quest.kate_search_for_nurse.started:
        # return "All the colors of the rainbow. Also black — the color of my tarnished soul."
        return "All the colors of the rainbow.\n\nAlso black — the color of my tarnished soul."
      else:
        return "So, this is where all the color in my life ended up."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_motive":
          if quest.lindsey_motive == "hidden":
            actions.append("?quest_lindsey_motive_hidden_shelf")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "maya_spell":
          if quest.maya_spell == "paint":
            actions.append(["take","Take","quest_maya_spell_paint"])
      else:
        if quest.jo_day == "supplies" and mc.owned_item("cactus") and not quest.jo_day["paint_jar_taken"]:
          actions.append(["take","Take","quest_jo_day_supplies_paint_shelf_take"])
          actions.append("?quest_jo_day_supplies_paint_shelf_interact")
        if quest.lindsey_motive == "paint":
          if jacklyn.at("school_art_class"):
            actions.append(["take","Take","?quest_lindsey_motive_paint_jacklyn"])
          else:
            actions.append(["take","Take","quest_lindsey_motive_paint_shelf"])
        if quest.maxine_eggs == "paint" and not quest.maxine_eggs["silver_paint_taken"]:
          actions.append(["take","Take","quest_maxine_eggs_class_shelf_interact"])
        if quest.poolside_story == "makedecor" and not quest.poolside_story["paint_acquired"]:
          actions.append(["take","Take","school_art_class_shelf_poolside_story_makedecor"])
        if quest.mrsl_bot == "paint":
          actions.append(["take","Take","quest_mrsl_bot_paint"])
        if quest.jacklyn_art_focus == "paint":
          actions.append("?school_art_class_paint_shelf_jacklyn_art_focus_paint")
        if quest.kate_search_for_nurse.started:
          actions.append("?school_art_class_shelf_interact_kate_search_for_nurse")
        if quest.poolside_story == "confetti":
          actions.append("?school_art_class_shelf_poolside_story_confetti")
      actions.append("?school_art_class_shelf_interact")


label school_art_class_shelf_interact:
  "Blue plus yellow equals green..."
  "Red plus blue equals purple..."
  if jacklyn.location == school_art_class:
    show jacklyn annoyed at appear_from_left
    jacklyn annoyed "What are you doing over there?"
    mc "Um... just... art stuff."
    "Shit. Better get out of here quickly."
    hide jacklyn with Dissolve(.5)
  return

label school_art_class_shelf_interact_kate_search_for_nurse:
  "My grandmother always told me to taste the colors of the world. She probably wasn't talking about acrylics."
  return

label school_art_class_paint_shelf_jacklyn_art_focus_paint:
  "You can't really paint without paint, it's in the word itself."
  "Luckily, there's plenty to go around."
  "Let's just pick a few random buckets..."
  "A real artist doesn't choose his colors, he lets the colors choose him.{space=-30}"
  "..."
  $school_art_class["easel_paint_buckets"] = True
  "..."
  $process_event("update_state")
  "All right, the paint is ready. Now, I just need something to paint with.{space=-10}"
  if mc.owned_item(("brush","beaver_carcass","beaver_pelt","dish_brush","mrsl_brooch","nurse_panties","pad")):
    $quest.jacklyn_art_focus.advance("artist")
  else:
    $quest.jacklyn_art_focus.advance("brush")
  return

label school_art_class_shelf_poolside_story_confetti:
  "Just paint here. No hidden confetti."
  return

label school_art_class_shelf_poolside_story_makedecor:
  "All right. Orange, black, and white — the school colors..."
  "Hmm..."
  $mc.add_item("paint_buckets")
  $quest.poolside_story["paint_acquired"] = True
  "Well, that was easy."
  return

label quest_maxine_eggs_class_shelf_interact:
  "Ah, there it is! Time to set \"Codename: Statue of Liberty\" into motion."
  $quest.maxine_eggs["silver_paint_taken"] = True
  $mc.add_item("silver_paint")
  $quest.maxine_eggs.advance("statue")
  return
