init python:
  class Interactable_school_art_class_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_art_class_dollar1_take"])


label school_art_class_dollar1_take:
  $school_art_class["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_art_class_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_art_class_dollar2_take"])


label school_art_class_dollar2_take:
  $school_art_class["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_art_class_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_art_class_dollar3_take"])


label school_art_class_dollar3_take:
  $school_art_class["dollar3_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_art_class_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_art_class_book_interact"])


label school_art_class_book_interact:
  $school_art_class["book_taken"] = True
  "This might come in handy..."
  "{i}\"Twelve Things That Nobody Cares About.\"{/}"
  $mc.intellect+=1
  return
