init python:
  class Interactable_school_art_class_statue_right(Interactable):

    def title(cls):
      return "Statue"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The only girl who doesn't jump back at my touch."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_art_class_statue_right_interact")


label school_art_class_statue_right_interact:
  "Oh sweet Persephone."
  "You're the nicest girl in this whole damn school."
  return
