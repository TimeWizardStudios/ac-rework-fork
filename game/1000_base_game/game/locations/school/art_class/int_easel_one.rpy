init python:
  class Interactable_school_art_class_easel_01(Interactable):

    def title(cls):
      return "Easel"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "A picnic basket, but instead of food, it contains five candles, a salt shaker, a sword, and what looks like a pair of panties."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      actions.append("?school_art_class_easel_01_interact")


label school_art_class_easel_01_interact:
  "No human language can properly express the majestic beauty of this piece."
  "The man who painted it..."
  "Albert Einstein."
  return
