init python:
  class Interactable_school_forest_glade_dirt_patch(Interactable):

    def title(cls):
      return "Patch of Dirt"

    def description(cls):
      return "This patch of dirt is so dirty, it could have a career in amateur porn."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_buried":
          if quest.isabelle_buried == "dig":
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_buried,car_tire|Use What?","isabelle_buried_car_tire_use"])
      actions.append("?school_forest_glade_dirt_patch_interact")


label school_forest_glade_dirt_patch_interact:
  "Looks like dirt. Feels like dirt. Smells like dirt."
  "Tastes like..."
  "Yep, that's chocolate."
  return
