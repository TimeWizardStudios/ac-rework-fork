init python:
  class Interactable_school_forest_glade_birds_roof(Interactable):
    def title(cls):
      return "Birds"
    def description(cls):
      return "Cax-Cax Tittus-Shittus — also known as the Insufferable Shit-tit — is one of Newfall's most common birds."
    def actions(cls,actions):
      actions.append("?school_forest_glade_birds_roof_interact")

label school_forest_glade_birds_roof_interact:
  "These woodland devils — known to ruin any honest farmer's livelihood — must think I'm blind."
  "But I'm watching. I'm always watching." 
  return