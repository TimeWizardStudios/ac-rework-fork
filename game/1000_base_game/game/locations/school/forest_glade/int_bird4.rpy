init python:
  class Interactable_school_forest_glade_bird4_ground(Interactable):
    def title(cls):
      return "Bird"
    def actions(cls,actions):
      if quest.jo_potted == "small":
        actions.append("?school_forest_glade_bird4_ground_interact")
      else:
        actions.append("?school_forest_glade_bird4_ground_interact2")
        
label  school_forest_glade_bird4_ground_interact2:     
  bird "Hawk! Hawk! Nawk!!"
  $school_forest_glade["birds"][3] = "roof"
  return

label school_forest_glade_bird4_ground_interact:
  bird "Tweet! Beet! Meet!"
  $school_forest_glade["birds"][3] = "roof"  
  return

init python:
  class Interactable_school_forest_glade_bird4_drugged(Interactable):
    def title(cls):
      return "Bird"
    def actions(cls,actions):
      actions.append("?school_forest_glade_bird4_drugged_interact")

label school_forest_glade_bird4_drugged_interact:
  show bird bird4 with Dissolve(.5)
  bird "Quack?" 
  hide bird with Dissolve(.5)
  $school_forest_glade["birds"][3]= "roof"
  $quest.jo_potted["bird_swoop"] = random.randint(1, 4)  
  return