init python:
  class Interactable_school_forest_glade_wildflowers(Interactable):

    def title(cls):
      return "Wildflowers"

    def description(cls):
      return "I don't know what these are called, but they sure are pretty..."

    def actions(cls,actions):
      actions.append(["take","Take","school_forest_glade_wildflowers_take"])
      actions.append("?school_forest_glade_wildflowers_interact")


label school_forest_glade_wildflowers_interact:
  "I'll pick these flowers just like I pick my nose."
  "Thoroughly, and leaving nothing behind."
  return

label school_forest_glade_wildflowers_take:
  "It's a miracle that these have survived next to the contaminated stream..."
  "Girls do love decapitated vegetation, though."
  window hide
  $school_forest_glade["wildflowers_taken"] = True
  $mc.add_item("wildflowers")
  pause 0.25
  window auto
  "Here we go!"
  jump quest_isabelle_gesture_bouquet
