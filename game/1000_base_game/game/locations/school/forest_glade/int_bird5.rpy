init python:
  class Interactable_school_forest_glade_bird5_ground(Interactable):
    def title(cls):
      return "Bird"
    def actions(cls,actions):
      if quest.jo_potted == "small":
        actions.append("?school_forest_glade_bird5_ground_interact")
      else:
        actions.append("?school_forest_glade_bird5_ground_interact2")
        
label  school_forest_glade_bird5_ground_interact2:     
  bird "Nowkwek tow ruwk!"
  $school_forest_glade["birds"][4] = "roof"
  return
        
label school_forest_glade_bird5_ground_interact:
  bird "Squawk t'fawk awp!"
  $school_forest_glade["birds"][4] = "roof"  
  return

init python:
  class Interactable_school_forest_glade_bird5_drugged(Interactable):
    def title(cls):
      return "Bird"
    def actions(cls,actions):
      actions.append("?school_forest_glade_bird5_drugged_interact")

label school_forest_glade_bird5_drugged_interact:
  show bird bird5 with Dissolve(.5)
  bird "Cahf ah nafl mglw'nafh!"
  hide bird with Dissolve(.5)
  $school_forest_glade["birds"][4]= "roof"  
  $quest.jo_potted["bird_swoop"] = random.randint(1, 4)
  return