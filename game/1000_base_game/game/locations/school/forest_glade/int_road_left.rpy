init python:
  class Interactable_school_forest_glade_road_left(Interactable):

    def title(cls):
      return "School Entrance"

    def description(cls):
      if quest.jo_potted.started:
        return "The way back to civilization is long and overgrown. Perhaps it's best to stay away from the tragedy of my social life."
      else:
        return "Lost in the wilderness without a map or compass. Yet, returning to civilization doesn't seem that urgent."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_buried":
          if quest.isabelle_buried in ("bury","dig"):
            actions.append(["go","Return","?school_forest_glade_road_left_interact_isabelle_bury"])
            return
          elif quest.isabelle_buried == "funeral":
            actions.append(["go","Return","?school_forest_glade_road_left_interact_isabelle_bury_funeral"])
            return
        elif mc["focus"] == "maxine_hook":
          if quest.maxine_hook == "dig":
            if quest.maxine_hook["forest_glade_exit_count"] >= 4:
              actions.append(["go","Return","?quest_maxine_hook_dig_school_forest_glade_road_left_repeat"])
              return
            else:
              actions.append(["go","Return","quest_maxine_hook_dig_school_forest_glade_road_left"])
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "forest_glade":
            actions.append(["go","Return","?quest_flora_squid_forest_glade_road_left"])
            return
        elif mc["focus"] == "isabelle_hurricane":
          if quest.isabelle_hurricane == "call_isabelle":
            actions.append(["go","Return","?quest_isabelle_hurricane_call_isabelle_road"])
            return
          elif quest.isabelle_hurricane == "call_maxine":
            actions.append(["go","Return","?quest_isabelle_hurricane_call_maxine_road"])
            return
        elif mc["focus"] == "flora_walk":
          if quest.flora_walk == "forest_glade":
            actions.append(["go","Return","?quest_flora_walk_forest_glade_road"])
            return
      else:
        if quest.jo_potted == "plowcow":
          actions.append(["go","Return","?school_forest_glade_road_left_interact"])
          return
      actions.append(["go","Return","goto_school_entrance"])


label school_forest_glade_road_left_interact:
  "The [nurse] doesn't strike me as a person with a very good sense of direction. She'd probably get lost if I left her here."
  return

label school_forest_glade_road_left_interact_isabelle_bury:
  "Sneaking off during a funeral would be highly unorthodox and potentially lethal. [isabelle] seems like a person who holds grudges."
  return

label school_forest_glade_road_left_interact_isabelle_bury_funeral:
  "Sneaking off during a funeral would be highly unorthodox and potentially lethal."
  "[isabelle] seems like the type of person who would curse my bloodline for seven generations."
  return
