init python:
  class Interactable_school_forest_glade_trees(Interactable):

    def title(cls):
      return "Trees"

    def description(cls):
      # return "Time to cut down some trees to see the forest."
      return "Time to cut down some trees\nto see the forest."

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_stars,lumberjack|Use What?","quest_isabelle_stars_wood"])
      actions.append("?school_forest_glade_trees_interact")


label school_forest_glade_trees_interact:
  "Those trees look a bit pathetic for pines, but that's just Newfall."
  "..."
  "No, wait. Those are oaks... or maples, maybe?"
  "I could definitely make some syrup out of those if I wanted."
  return
