init python:
  class Interactable_school_forest_glade_tire(Interactable):

    def title(cls):
      return "Car Tire"

    def description(cls):
      return "Michelin, three stars. Worth the trip."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_buried":
          if quest.isabelle_buried == "dig":
            actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:isabelle_buried,car_tire|Use What?","isabelle_buried_car_tire_use"])
        elif mc["focus"] == "maxine_hook":
          if quest.maxine_hook == "dig":
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:maxine_hook,car_tire|Use What?","quest_maxine_hook_dig_use_item"])
      actions.append("?school_forest_glade_tire_interact")


label school_forest_glade_tire_interact:
  "Drove into the woods. Started making out with the girl. Heard a strange noise."
  "Only the tire remains."
  return
