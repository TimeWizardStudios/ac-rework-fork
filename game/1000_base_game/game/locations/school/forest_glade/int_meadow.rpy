init python:
  class Interactable_school_forest_glade_meadow(Interactable):
    def title(cls):
      return "Meadow"
    def description(cls):
      if quest.jo_potted == "plow":
        return "The sunlight in the grass, the wind whispering through the leaves. It's like this place doesn't belong in Newfall."
      else:  
        return "A forest meadow. This is where witches and fairies come to dance, and where cults perform their rituals."
    def actions(cls,actions):
      if quest.jo_potted in ("plow","getnurse","plowcow"):
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,meadow|Use What?","quest_jo_pot_meadow"])
      
      if quest.berb_fight.finished:  
        actions.append("?school_forest_glade_meadow_jo_potted")
      actions.append("?school_forest_glade_meadow_interact")
      
      
label school_forest_glade_meadow_jo_potted:
  "The perfect place to plant a bunch of dubious seeds and grow them without getting noticed."
  "Is this how meth-cookers scout out places to set up shop?"
  return
      
label school_forest_glade_meadow_interact:
  "There's a reason the trees don't want to grow here — cursed earth."
  return