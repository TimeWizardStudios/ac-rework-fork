
init python:
  class Interactable_school_forest_glade_grave(Interactable):

    def title(cls):
      return "Grave"

    def description(cls):
      return "Memories are all we are in the end. Maybe the afterlife is inside the minds of those that remember us?"

    def actions(cls,actions):
      actions.append("?school_forest_glade_grave_interact")


label school_forest_glade_grave_interact:
  "Would [flora] do the same for me as [isabelle] does for her sister?"
  "Probably not, and that's my fault."
  return
