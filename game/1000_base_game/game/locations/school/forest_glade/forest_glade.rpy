init python:
  class Location_school_forest_glade(Location):

    default_title="Forest"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      #Sky
      if ((quest.maxine_hook == "dig" and not 19 > game.hour > 6)
      or (quest.flora_walk == "forest_glade" and game.hour == 19)):
        scene.append([(0,0),"school forest_glade night_sky"])
      elif (quest.isabelle_buried in ("bury","dig","funeral")
      or (quest.jo_day == "picnic" and game.hour == 19)):
        scene.append([(-149,0),"school forest_glade sky_sunset"])
      elif game.season == 1:
        scene.append([(-149,0),"school forest_glade sky"])
      elif game.season == 2:
        scene.append([(-149,-114),"school forest_glade sky_autumn"])

      if game.season == 1:
        scene.append([(0,183),"school forest_glade trees"])
      elif game.season == 2:
        scene.append([(0,183),"school forest_glade trees_autumn"])

      #Trees
      if quest.isabelle_stars == "wood":
        scene.append([(0,183),"school forest_glade trees_only_autumn",("school_forest_glade_trees",0,100)])

      #Road
      scene.append([(0,591),"school forest_glade road_left","school_forest_glade_road_left"])
      scene.append([(1529,654),"school forest_glade road_right"])

      #Meadow
      if not school_forest_glade["farm"]:
        if game.season == 1:
          scene.append([(797,630),"school forest_glade meadow","school_forest_glade_meadow"])
        elif game.season == 2:
          scene.append([(797,630),"school forest_glade meadow_autumn","school_forest_glade_meadow"])

      #Hut
      scene.append([(419,390),"school forest_glade house","school_forest_glade_house"])
      if game.season == 1:
        scene.append([(0,518),"school forest_glade bushes"])
      elif game.season == 2:
        scene.append([(0,518),"school forest_glade bushes_autumn"])

      #River and pollution level
      if school_forest_glade['pollution'] == 3:
        scene.append([(0,734),"school forest_glade river_lv3","school_forest_glade_river_lv3"])
      elif school_forest_glade['pollution'] == 2:
        scene.append([(0,734),"school forest_glade river_lv2","school_forest_glade_river_lv2"])
      elif school_forest_glade['pollution'] == 1:
        scene.append([(0,734),"school forest_glade river_lv1","school_forest_glade_river_lv1"])
      else:
        scene.append([(0,734),"school forest_glade river_lv0","school_forest_glade_river_lv0"])

      if quest.isabelle_hurricane in ("call_isabelle","call_maxine") and quest.isabelle_hurricane["locker"]:
        scene.append([(134,644),"school forest_glade locker","school_forest_glade_locker"])

      if game.season == 2:
        if school_forest_glade["pollution"] == 3:
          scene.append([(-20,633),"#school forest_glade leafpile_lvl3"])
        else:
          scene.append([(-20,633),"#school forest_glade leafpile_lvl0"])

      # farm
      if school_forest_glade["farm"]:
        scene.append([(797,630),"school forest_glade earth","school_forest_glade_earth"])
        if school_forest_glade["farm"] == "dry":
          scene.append([(797,630),"school forest_glade dry","school_forest_glade_earth"])
        elif school_forest_glade["farm"] == "wet":
          scene.append([(797,630),"school forest_glade watered","school_forest_glade_earth"])
          if not school_forest_glade["canal"]:
            scene.append([(842,642),"school forest_glade watercanal_lvl0"])

      if school_forest_glade["canal"]:
        if school_forest_glade['pollution'] == 3:
          scene.append([(842,642),"school forest_glade watercanal_lvl3"])
        else:
          scene.append([(842,642),"school forest_glade watercanal_lvl0"])
      scene.append([(910,641),"#school forest_glade stone"])
      scene.append([(237,665),"school forest_glade stones"])

      #player dug canal
      if school_forest_glade['canal']:
        if school_forest_glade['pollution'] == 3:
          scene.append([(717,664),"school forest_glade rivercanal_lvl3",("school_forest_glade_river_lv3",177,70)])
        else:
          scene.append([(717,664),"school forest_glade rivercanal_lvl0",("school_forest_glade_river_lv0",177,70)])

      #farm plants
      if quest.jo_potted.in_progress:
        if quest.jo_potted == "small":
          scene.append([(835,639),"school forest_glade small_sprouts","school_forest_glade_earth"])
        if quest.jo_potted == "medium":
          scene.append([(820,642),"school forest_glade medium_sprouts","school_forest_glade_earth"])
        if quest.jo_potted == "big":
          scene.append([(820,621),"school forest_glade big_sprouts","school_forest_glade_earth"])

      scene.append([(873,743),"school forest_glade wood","school_forest_glade_wood"])
      scene.append([(1369,921),"school forest_glade mushrooms"])
      scene.append([(267,590),"school forest_glade stump"])
      if school_forest_glade["wrappers"]:
        scene.append([(284,760),"#school forest_glade candy1"])

      if not flora.talking:
        if flora.at("school_forest_glade","standing"):
          if not school_forest_glade["exclusive"] or school_forest_glade["exclusive"] == "flora":
            scene.append([(221,454),"school forest_glade flora_autumn","flora"])

      if quest.isabelle_buried["hole_state"] == 0:
        scene.append([(78,893),"school forest_glade tire","school_forest_glade_tire"])
      elif quest.isabelle_buried["hole_state"] == 1:
        scene.append([(58,870),"school forest_glade hole","school_forest_glade_hole"])
      elif quest.isabelle_buried["hole_state"] == 2:
        scene.append([(58,870),"school forest_glade hole"])
        scene.append([(260,969),"school forest_glade locked_box"])
      elif quest.isabelle_buried["hole_state"] == 3:
        scene.append([(195,937),"school forest_glade dirt_patch","school_forest_glade_dirt_patch"])
      elif quest.isabelle_buried["hole_state"] == 4:
        scene.append([(195,937),"school forest_glade dirt_patch"])
        scene.append([(263,958),"school forest_glade rocks"])
      elif quest.isabelle_buried["hole_state"] == 5:
        scene.append([(195,937),"school forest_glade dirt_patch","school_forest_glade_grave"])
        scene.append([(263,958),"school forest_glade rocks",("school_forest_glade_grave",-15,-21)])
        scene.append([(257,944),"school forest_glade ribbon",("school_forest_glade_grave",-13,-7)])
        scene.append([(257,959),"school forest_glade tiny_ribbon",("school_forest_glade_grave",0,-22)])

      if quest.isabelle_gesture == "bouquet" and not school_forest_glade["wildflowers_taken"]:
        scene.append([(422,824),"school forest_glade wildflowers","school_forest_glade_wildflowers"])

      scene.append([(665,786),"#school forest_glade duck"])
      if not school_forest_glade['pollution'] == 3:
        scene.append([(1180,807),"#school forest_glade dragonfly"])
      scene.append([(1381,635),"school forest_glade trap"])
      scene.append([(972,656),"#school forest_glade ant"])
      scene.append([(999,662),"#school forest_glade ants"])
      scene.append([(0,671),"school forest_glade egg"])
      if quest.jo_day >= "supplies" and quest.jo_day.in_progress:
        if school_forest_glade["picnic_blanket"]:
          scene.append([(367,1026),"school forest_glade picnic_blanket",("school_forest_glade_picnic",0,-32)])
        if school_forest_glade["picnic_blanket"] or school_forest_glade["jo_secret_wine"] or school_forest_glade["cactus_in_a_pot"]:
          scene.append([(540,994),"school forest_glade basket",("school_forest_glade_picnic",98,0)])
        else:
          scene.append([(540,994),"school forest_glade basket","school_forest_glade_picnic"])
        if school_forest_glade["cactus_in_a_pot"]:
          scene.append([(739,1012),"school forest_glade cactus_in_a_pot",("school_forest_glade_picnic",-43,-18)])
        if school_forest_glade["jo_secret_wine"]:
          if school_forest_glade["love_potion_droplet"]:
            scene.append([(490,929),"school forest_glade spiked_wine",("school_forest_glade_picnic",211,65)])
          else:
            scene.append([(490,929),"school forest_glade jo_secret_wine",("school_forest_glade_picnic",211,65)])
      else:
        scene.append([(486,1053),"school forest_glade basket_shadow"])
        scene.append([(540,994),"school forest_glade basket"])

      if not berb.talking:
        if berb.at("school_forest_glade","river"):
          if not school_forest_glade['pollution']:
            scene.append([(754,792),"school forest_glade beaver_river_lvl0","berb"])
          elif school_forest_glade['pollution'] == 1:
            scene.append([(754,792),"school forest_glade beaver_river_lvl1","berb"])
          elif school_forest_glade['pollution'] == 2:
            scene.append([(754,792),"school forest_glade beaver_river_lvl2","berb"])
          elif school_forest_glade['pollution'] == 3:
            scene.append([(760,792),"school forest_glade beaver_river_lvl3","berb"])
        elif berb.at("school_forest_glade","ground"):
          if berb["scalped"]:
            scene.append([(689,854),"school forest_glade tombstone","school_forest_glade_tombstone"])
          elif berb["dead"]:
            scene.append([(749,900),"school forest_glade dead_beaver","school_forest_glade_dead_beaver"])
          else:
            scene.append([(806,870),"school forest_glade beaver_ground","berb"])

      #candy
      if school_forest_glade["wrappers"]:
        scene.append([(1229,858),"#school forest_glade candy8"])
        scene.append([(1591,892),"#school forest_glade candy7"])
        scene.append([(1512,873),"#school forest_glade candy6"])
        scene.append([(1108,851),"#school forest_glade candy5"])
        scene.append([(1059,811),"#school forest_glade candy4"])
        scene.append([(876,867),"#school forest_glade candy3"])
        scene.append([(755,829),"#school forest_glade candy_hat"])
        scene.append([(593,768),"#school forest_glade candy2"])

      # BIRDSSSSS
      if school_forest_glade['birds']:
        if school_forest_glade['birds'][0]=="roof": #bird 1
          scene.append([(582,361),"school forest_glade bird1_roof","school_forest_glade_birds_roof"])
        elif school_forest_glade['birds'][0]=="ground":
          scene.append([(826,671),"school forest_glade bird1_ground","school_forest_glade_bird1_ground"])
        elif school_forest_glade['birds'][0]=="drugged":
          if school_forest_glade['birds'][3]=="drugged":
            scene.append([(1011,584),"school forest_glade bird1_drugged","school_forest_glade_bird1_drugged"])
          else:
            scene.append([(990,653),"school forest_glade bird1_drugged2","school_forest_glade_bird1_drugged"])

        if school_forest_glade['birds'][1]=="roof": #bird2
          scene.append([(548,395),"school forest_glade bird2_roof","school_forest_glade_birds_roof"])
        elif school_forest_glade['birds'][1]=="ground":
          scene.append([(1114,632),"school forest_glade bird2_ground","school_forest_glade_bird2_ground"])
        elif school_forest_glade['birds'][1]=="drugged":
          scene.append([(1182,689),"school forest_glade bird2_drugged","school_forest_glade_bird2_drugged"])

        if school_forest_glade['birds'][2]=="roof": #bird3
          scene.append([(549,418),"school forest_glade bird3_roof","school_forest_glade_birds_roof"])
        elif school_forest_glade['birds'][2]=="ground":
          scene.append([(799,626),"school forest_glade bird3_ground","school_forest_glade_bird3_ground"])
        elif school_forest_glade['birds'][2]=="drugged":
          scene.append([(842,548),"school forest_glade bird3_drugged","school_forest_glade_bird3_drugged"])

        if school_forest_glade['birds'][3]=="roof": #bird4
          scene.append([(624,356),"school forest_glade bird4_roof","school_forest_glade_birds_roof"])
        elif school_forest_glade['birds'][3]=="ground":
          scene.append([(1180,636),"school forest_glade bird4_ground","school_forest_glade_bird4_ground"])
        elif school_forest_glade['birds'][3]=="drugged":
          scene.append([(999,623),"school forest_glade bird4_drugged","school_forest_glade_bird4_drugged"])

        if school_forest_glade['birds'][4]=="roof": #bird5
          scene.append([(672,392),"school forest_glade bird5_roof","school_forest_glade_birds_roof"])
        elif school_forest_glade['birds'][4]=="ground":
          scene.append([(823,597),"school forest_glade bird5_ground","school_forest_glade_bird5_ground"])
        elif school_forest_glade['birds'][4]=="drugged":
          scene.append([(741,626),"school forest_glade bird5_drugged","school_forest_glade_bird5_drugged"])

        if school_forest_glade['birds'][5]=="roof": #bird 6
          scene.append([(690,417),"school forest_glade bird6_roof","school_forest_glade_birds_roof"])
        elif school_forest_glade['birds'][5]=="ground":
          scene.append([(562,678),"school forest_glade bird6_ground","school_forest_glade_bird6_ground"])
        elif school_forest_glade['birds'][5]=="drugged":
          scene.append([(801,746),"school forest_glade bird6_drugged","school_forest_glade_bird6_drugged"])

      if not nurse.talking:
        if nurse.at("school_forest_glade","sitting"):
          if not school_forest_glade["exclusive"] or school_forest_glade["exclusive"] == "nurse":
            if game.season == 1:
              scene.append([(1521,719),"school forest_glade nurse","nurse"])
            elif game.season == 2:
              scene.append([(1531,716),"school forest_glade nurse_autumn","nurse"])

      if not isabelle.talking:
        if isabelle.at("school_forest_glade","standing"):
          if not school_forest_glade["exclusive"] or school_forest_glade["exclusive"] == "isabelle":
            if game.season == 1:
              scene.append([(344,443),"school forest_glade isabelle","isabelle"])
            elif game.season == 2:
              scene.append([(357,450),"school forest_glade isabelle_autumn","isabelle"])
              if isabelle.equipped_item("isabelle_collar"):
                scene.append([(418,524),"school forest_glade isabelle_collar",("isabelle",-10,-74)])

      if not maxine.talking:
        if maxine.at("school_forest_glade","standing"):
          if not school_forest_glade["exclusive"] or school_forest_glade["exclusive"] == "maxine":
            if game.season == 1:
              scene.append([(519,441),"school forest_glade maxine","maxine"])
            elif game.season == 2:
              scene.append([(518,441),"school forest_glade maxine_autumn","maxine"])

      if not flora.talking:
        if quest.flora_squid == "forest_glade" and quest.flora_squid["polluted_stream"]:
          if not school_forest_glade["exclusive"] or school_forest_glade["exclusive"] == "flora":
            scene.append([(1353,739),"school forest_glade flora","flora"])

      #Tree monster
      if quest.flora_squid["tree_monster"]:
        scene.append([(1011,251),"school forest_glade big_tree"])
        scene.append([(1243,422),"school forest_glade forest_vines"])

      #Frogs
      if quest.maya_spell == "frogs":
        if "timmy" not in quest.maya_spell["frogs_caught"]:
          if quest.maya_spell["frog_hops"] == 1:
            scene.append([(342,718),"school forest_glade frog1a","school_forest_glade_frog1"])
          elif quest.maya_spell["frog_hops"] == 2:
            scene.append([(146,765),"school forest_glade frog1b","school_forest_glade_frog1"])
          elif quest.maya_spell["frog_hops"] == 3:
            scene.append([((214,903) if quest.isabelle_buried["hole_state"] else (111,852)),"school forest_glade frog1c","school_forest_glade_frog1"])
        if "jimmy" not in quest.maya_spell["frogs_caught"]:
          if quest.maya_spell["frog_hops"] == 1:
            scene.append([(473,643),"school forest_glade frog2a","school_forest_glade_frog2"])
          elif quest.maya_spell["frog_hops"] == 2:
            scene.append([(613,532),"school forest_glade frog2b","school_forest_glade_frog2"])
          elif quest.maya_spell["frog_hops"] == 3:
            scene.append([(740,658),"school forest_glade frog2c","school_forest_glade_frog2"])
        if "slimmy" not in quest.maya_spell["frogs_caught"]:
          if quest.maya_spell["frog_hops"] == 1:
            scene.append([(1250,785),"school forest_glade frog3a","school_forest_glade_frog3"])
          elif quest.maya_spell["frog_hops"] == 2:
            scene.append([(1348,619),"school forest_glade frog3b","school_forest_glade_frog3"])
          elif quest.maya_spell["frog_hops"] == 3:
            scene.append([(1014,707),"school forest_glade frog3c","school_forest_glade_frog3"])
        if "tammy" not in quest.maya_spell["frogs_caught"]:
          if quest.maya_spell["frog_hops"] == 1:
            scene.append([(1445,989),"school forest_glade frog4a","school_forest_glade_frog4"])
          elif quest.maya_spell["frog_hops"] == 2:
            scene.append([((865,936) if berb["scalped"] else (936,947)),"school forest_glade frog4b","school_forest_glade_frog4"])
          elif quest.maya_spell["frog_hops"] == 3:
            scene.append([(1359,887),"school forest_glade frog4c","school_forest_glade_frog4"])
        if "frank" not in quest.maya_spell["frogs_caught"]:
          if quest.maya_spell["frog_hops"] == 1:
            scene.append([(1707,790),"school forest_glade frog5a","school_forest_glade_frog5"])
          elif quest.maya_spell["frog_hops"] == 2:
            scene.append([(1834,845),"school forest_glade frog5b","school_forest_glade_frog5"])
          elif quest.maya_spell["frog_hops"] == 3:
            scene.append([(1641,905),"school forest_glade frog5c","school_forest_glade_frog5"])

      #Dollars
      if school_forest_glade["dollar1_spawned_today"] == True and not school_forest_glade["dollar1_taken_today"]:
        scene.append([(626,1046),"school forest_glade dollar1","school_forest_glade_dollar1"])
      if school_forest_glade["dollar2_spawned_today"] == True and not school_forest_glade["dollar2_taken_today"]:
        scene.append([(996,844),"school forest_glade dollar2","school_forest_glade_dollar2"])

      if ((quest.maxine_hook == "dig" and not 19 > game.hour > 6)
      or (quest.flora_walk == "forest_glade" and game.hour == 19)):
        scene.append([(0,0),"#school forest_glade night_overlay"])
      elif (quest.isabelle_buried in ("bury","dig","funeral")
      or (quest.jo_day == "picnic" and game.hour == 19)):
        scene.append([(0,0),"#school forest_glade sunsetoverlay"])

    def find_path(self,target_location):
      return ("school_forest_glade_road_left",dict(marker_sector="mid_bottom", marker_offset=(200,0)))


label goto_school_forest_glade:
  if school_forest_glade.first_visit_today:
    $school_forest_glade["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_forest_glade["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_forest_glade)
  return
