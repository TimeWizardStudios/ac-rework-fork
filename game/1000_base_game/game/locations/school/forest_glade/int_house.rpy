init python:
  class Interactable_school_forest_glade_house(Interactable):

    def title(cls):
      return "Creepy Hut"

    def description(cls):
      if quest.jo_potted.started:
        return "This hut looks ancient. Possibly centuries old. Or decades, who knows? Either way, old beyond comprehension."
      if quest.berb_fight.finished:
        return "Probably where the beaver keeps his human slaves."
      else:
        return "A relic of a past civilization. Aztec or Mayan in design."

    def actions(cls,actions):
      if quest.jo_potted=="plow" and not school_forest_glade["got_plow"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,house|Use What?","quest_jo_pot_house"])
        actions.append("?school_forest_glade_house_jo_potted")
      elif quest.isabelle_hurricane == "forest":
        actions.append("quest_isabelle_hurricane_forest_creepy_hut")
      else:
        actions.append("?school_forest_glade_house_interact")


label school_forest_glade_house_interact:
  "The door is locked, but from the cracks in the wall there seems to be all sorts of old farming equipment inside."
  return

label school_forest_glade_house_jo_potted:
  "Maybe I can use some of the farming equipment inside the hut to plow the meadow, but the door is locked."
  "Need to find something long and hook-shaped to pull stuff out through the cracks."
  return
