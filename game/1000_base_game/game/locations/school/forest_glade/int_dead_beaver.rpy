init python: 
  class Interactable_school_forest_glade_dead_beaver(Interactable):
    def title(cls):
      return "Beaver"
    def description(cls):
      return "The flies keep a safe distance. The maggots bring their appetite elsewhere. Even the rot seems afraid to touch this carcass."
    def actions(cls,actions):
      actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,dead|Use What?","school_forest_glade_dead_beaver_use"])      
      actions.append("?school_forest_glade_dead_beaver_interact")

label school_forest_glade_dead_beaver_use(item):
  if item == "book_of_the_dammed":
    "{i}\"The great philosopher and pacifist J. J. Timberlake once said, 'The only good beaver is a dead beaver.'\"{/}"
    "{i}\"This quote has been widely criticized as too charitable, and has led to experts accusing Timberlake of being a beaver sympathizer.\"{/}"
    "{i}\"Regardless of his true feelings, Timberlake's contributions to the dwindling numbers of beavers across the globe are undeniable.\"{/}"
    return
  elif item == "glass_shard":
    "What's better than taking a scalp from your defeated foe? Not much, that's what."
    show black with Dissolve(.5)
    show blood_splatter with Dissolve(.5)
    hide blood_splatter with Dissolve(.5)
    hide black with fadehold
    $berb["scalped"] = True    
    $process_event("update_state")
    $game.hour+=2
    "Took me a couple of hours to fully skin that beaver and bury the remains."
    "The upside is that I'm now the proud owner of a beaver pelt."
    $mc.add_item("beaver_pelt")
    return
  else:
    "He's already dead, Jim."
    $quest.berb_fight.failed_item("dead",item)
    return
      
label school_forest_glade_dead_beaver_interact:
  "With his demise, these lands are free of tyranny and boundless hatred."
  "Celebrate, for your days may yet be numbered."
  "Much like dwarves, the beavers keep records of past grudges."
  "Your bloodline might be the target of retribution for many generations to come."
  return