init python:
  class Interactable_school_forest_glade_bird6_ground(Interactable):
    def title(cls):
      return "Bird"
    def actions(cls,actions):
      if quest.jo_potted=="small":  
        actions.append("?school_forest_glade_bird6_ground_interact")
      else:
        actions.append("?school_forest_glade_bird6_ground_interact2")
        
label  school_forest_glade_bird6_ground_interact2:     
  bird "Twip-twip! Tweet-tweet!"
  $school_forest_glade["birds"][5] = "roof"
  return

label school_forest_glade_bird6_ground_interact:
  bird "Eep-eep! Squawk!"
  $school_forest_glade["birds"][5] = "roof"
  return

init python:
  class Interactable_school_forest_glade_bird6_drugged(Interactable):
    def title(cls):
      return "Bird"
    def actions(cls,actions):
      actions.append("?school_forest_glade_bird6_drugged_interact")

label school_forest_glade_bird6_drugged_interact:
  show bird bird6 with Dissolve(.5)
  bird "Tweet-tweet, fawkn skweet!"
  $school_forest_glade["birds"][5] = "roof"
  $quest.jo_potted["bird_swoop"] = random.randint(1, 4)
  return

