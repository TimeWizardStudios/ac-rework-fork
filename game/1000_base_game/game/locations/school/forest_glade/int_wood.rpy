init python:
  class Interactable_school_forest_glade_wood(Interactable):

    def title(cls):
      return "Beaver Dam"

    def description(cls):
      if quest.jo_potted.started:
        return "The gate to the infernal pits of damnation. The opening into the lifeless void of eternal evil. The devil's asshole."
      else:
        return "Constructed with the desecrated carcasses of trees and other innocent plants."

    def actions(cls,actions):
      if not school_forest_glade["dam_stick_retrieved"]:
        actions.append(["take","Take","school_forest_glade_wood_interact_dam_stick_retrieved"])
      actions.append("?school_forest_glade_wood_interact")


label school_forest_glade_wood_interact_dam_stick_retrieved:
  "Tearing down the monuments of our oppressors is always a good feeling."
  $mc.add_item("stick")
  "I'll keep this as a trophy."
  $school_forest_glade["dam_stick_retrieved"] = True
  return

label school_forest_glade_wood_interact:
  "If Voldemort and Sauron decided to become roommates, this is what their home would look like. This place reeks of evil."
  return
