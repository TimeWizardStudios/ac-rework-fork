init python:
  class Interactable_school_forest_glade_bird2_ground(Interactable):
    def title(cls):
      return "Bird"

    def actions(cls,actions):
      if quest.jo_potted == "small": 
        actions.append("?school_forest_glade_bird2_ground_interact")
      else:
        actions.append("?school_forest_glade_bird2_ground_interact2")
        
label  school_forest_glade_bird2_ground_interact2:     
  bird "Tawk-tawk! Squewk Newks!"
  $school_forest_glade["birds"][1] = "roof"
  return
  
label school_forest_glade_bird2_ground_interact:
  bird "Squawk!"
  $school_forest_glade["birds"][1] = "roof"
  return

init python:
  class Interactable_school_forest_glade_bird2_drugged(Interactable):
    def title(cls):
      return "Bird"

    def actions(cls,actions):
      actions.append("?school_forest_glade_bird2_drugged_interact")

label school_forest_glade_bird2_drugged_interact:
  show bird bird2 with Dissolve(.5)
  bird "Noot-noot!"
  hide bird with Dissolve(.5)
  $school_forest_glade["birds"][1] = "roof"
  $quest.jo_potted["bird_swoop"] = random.randint(1, 4)
  return