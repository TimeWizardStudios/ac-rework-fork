
init python:
  class Interactable_school_forest_glade_tombstone(Interactable):
    def title(cls):
      return "Grave"
    def description(cls):
      return "Rest in peace, fur-Satan."
    def actions(cls,actions):
      actions.append("?school_forest_glade_tombstone_interact")

label school_forest_glade_tombstone_interact:
  "Probably should've burned the body and staked the heart."
  return