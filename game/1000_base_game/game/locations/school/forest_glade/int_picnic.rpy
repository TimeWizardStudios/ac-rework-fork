
init python:
  class Interactable_school_forest_glade_picnic(Interactable):

    def title(cls):
      if school_forest_glade["picnic_blanket"] or school_forest_glade["jo_secret_wine"] or school_forest_glade["cactus_in_a_pot"]:
        return "Picnic"
      else:
        return "Basket"

    def description(cls):
      return "It's the thought that counts.\n\nNothing but the thought."

    def actions(cls,actions):
      if (quest.jo_day == "supplies" and not (school_forest_glade["picnic_blanket"] and school_forest_glade["jo_secret_wine"] and school_forest_glade["cactus_in_a_pot"])) or quest.jo_day == "droplet":
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:jo_day,picnic|Use What?","quest_jo_day_supplies_picnic"])
      actions.append("?school_forest_glade_picnic_interact")


label school_forest_glade_picnic_interact:
  "What's the opposite of peaking? Because this is that."
  return
