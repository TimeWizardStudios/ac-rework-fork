init python:
  class Interactable_school_forest_glade_frog1(Interactable):

    def title(cls):
      return "Frog"

    def description(cls):
#     return "Doth mine eyes deceive me? A frog!"
      return "Doth mine eyes deceive me?\nA frog!"

    def actions(cls,actions):
      actions.append(["take","Take","school_forest_glade_frog1_take"])


label school_forest_glade_frog1_take:
  show location with vpunch2
  $quest.maya_spell["frogs_caught"].add("timmy")
  $mc.add_item("frog")
  pause 0.25
  "Aha! I can't believe it! I actually caught one!"
  "Slippery little bugger, not unlike a booger..."
  return


init python:
  class Interactable_school_forest_glade_frog2(Interactable):

    def title(cls):
      return "Frog"

    def description(cls):
      return "Hello, frog! Where's toad?"

    def actions(cls,actions):
      actions.append(["take","Take","school_forest_glade_frog2_take"])


label school_forest_glade_frog2_take:
  mc "Excuse me, do you live in that creepy shed?"
  window hide
  show location with vpunch2
  $quest.maya_spell["frogs_caught"].add("jimmy")
  $mc.add_item("frog")
  pause 0.25
  window auto
  "Yoink! Not anymore!"
  return


init python:
  class Interactable_school_forest_glade_frog3(Interactable):

    def title(cls):
      return "Frog"

    def description(cls):
      return "Why so jumpy?"

    def actions(cls,actions):
      actions.append(["take","Take","school_forest_glade_frog3_take"])


label school_forest_glade_frog3_take:
  "I just have to..."
  extend " stay really still..."
  extend " before I..."
  $quest.maya_spell["frogs_caught"].add("slimmy")
  $mc.add_item("frog",silent=True)
  extend " pounce!" with vpunch
  window hide
  $game.notify_modal(None,"Inventory","{image=items frog}{space=50}|Gained\n{color=#900}Frog{/color}",5.0)
  pause 0.25
  window auto
  "Goddamn, look at my reflexes! Wax on, wax off!"
  return


init python:
  class Interactable_school_forest_glade_frog4(Interactable):

    def title(cls):
      return "Frog"

    def description(cls):
      return "What do you call a sad frog? Unhoppy."

    def actions(cls,actions):
      actions.append(["take","Take","school_forest_glade_frog4_take"])


label school_forest_glade_frog4_take:
  show location with vpunch2
  $quest.maya_spell["frogs_caught"].add("tammy")
  $mc.add_item("frog")
  pause 0.25
  "Gotcha!"
  "..."
  "If I kiss you, will you turn into a princess?"
  return


init python:
  class Interactable_school_forest_glade_frog5(Interactable):

    def title(cls):
      return "Frog"

    def description(cls):
      return "What's green and red all over? Frog Legs in ketchup."

    def actions(cls,actions):
      actions.append(["take","Take","school_forest_glade_frog5_take"])


label school_forest_glade_frog5_take:
  show location with vpunch2
  $quest.maya_spell["frogs_caught"].add("frank")
  $mc.add_item("frog")
  pause 0.25
  "I {i}really{/} hope you can't catch something from these guys..."
  return
