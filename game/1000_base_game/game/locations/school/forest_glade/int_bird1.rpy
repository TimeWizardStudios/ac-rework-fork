init python:
  class Interactable_school_forest_glade_bird1_ground(Interactable):
    def title(cls):
      return "Bird"

    def actions(cls,actions):
      if quest.jo_potted == "small":
        actions.append("?school_forest_glade_bird1_ground_interact")
      else:
        actions.append("?school_forest_glade_bird1_ground_interact2")
        
label  school_forest_glade_bird1_ground_interact2:     
  bird "Bawk-bawk! Mawkafawka!"
  $school_forest_glade["birds"][0] = "roof"
  return
  
label school_forest_glade_bird1_ground_interact:
  bird "Ca-caw!"
  $school_forest_glade["birds"][0] = "roof"
  return

init python:
  class Interactable_school_forest_glade_bird1_drugged(Interactable):
    def title(cls):
      return "Bird"

    def actions(cls,actions):
      actions.append("?school_forest_glade_bird1_drugged_interact")

label school_forest_glade_bird1_drugged_interact:
  if school_forest_glade['birds'][3]=="drugged":
    show bird bird4and1 with Dissolve(.5)
  else:
    show bird bird1 with Dissolve(.5)    
  bird "Squawk maaw deek!"
  $school_forest_glade["birds"][0] = "roof"
  $quest.jo_potted["bird_swoop"] = random.randint(1, 4)
  hide bird with Dissolve(.5)
  return