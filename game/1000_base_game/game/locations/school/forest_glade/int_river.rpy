init python:
  class Interactable_school_forest_glade_river_lv3(Interactable):

    def title(cls):
      return "Stream"

    def description(cls):
      if quest.jo_potted.started:
        return "The water itself is trying to escape the pollution. A sign that nature is finally on the retreat."
      else:
        return "Death stalks every corner of the riverbed. Life itself seems to have given up hope on resuscitating this forsaken body of water."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_squid":
          if quest.flora_squid == "forest_glade":
            actions.append("quest_flora_squid_forest_glade_stream_bottom")
      else:
        if school_forest_glade["pollution"] < 3 or not school_forest_glade["wrappers"]:
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,corrupt|Use What?","quest_berb_fight_river_items"])
          actions.append("?school_forest_glade_river_lv3_interact")
      actions.append("?school_forest_glade_river_lv3_interact_jo_potted")


label school_forest_glade_river_lv3_interact:
  "No life can live in this place anymore. The pollution and unhallowed soil make sure of that."
  return

label school_forest_glade_river_lv3_interact_jo_potted:
  "Bathing here would be like jumping into a vat of corrosive acid."
  "A refreshing change to the monotony of life."
  return


init python:
  class Interactable_school_forest_glade_river_lv2(Interactable):

    def title(cls):
      return "Stream"

    def description(cls):
      return "Carcasses litter the beaches. D-Day was a vegan garden party compared to this meat grinder."

    def actions(cls,actions):
      if school_forest_glade["pollution"]<3 or not school_forest_glade["wrappers"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,corrupt|Use What?","quest_berb_fight_river_items"])
      actions.append("?school_forest_glade_river_lv2_interact")


label school_forest_glade_river_lv2_interact:
  "In the future, this incident will be known as the Deadwater Horizon."
  return


init python:
  class Interactable_school_forest_glade_river_lv1(Interactable):

    def title(cls):
      return "Stream"

    def description(cls):
      return "Insects wobble up the beach, rowdy and intoxicated by the black gold."

    def actions(cls,actions):
      if school_forest_glade["pollution"]<3 or not school_forest_glade["wrappers"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,corrupt|Use What?","quest_berb_fight_river_items"])
      actions.append("?school_forest_glade_river_lv1_interact")


label school_forest_glade_river_lv1_interact:
  "Like an oil spill, the foul chemicals seep into the echo system, poisoning everything in their path."
  return


init python:
  class Interactable_school_forest_glade_river_lv0(Interactable):

    def title(cls):
      return "Stream"

    def description(cls):
      if quest.flora_walk.finished:
#       return "So clean, you could bottle and sell it for a few cents."
        return "So clean, you could bottle\nand sell it for a few cents."
      elif quest.jo_potted.started:
        return "The water itself is trying to escape the pollution. A sign that nature is finally on the retreat."
      else:
        return "The school typically bottles the water from this stream and sells it in the vending machines. No wonder it tastes so horrible."

    def actions(cls,actions):
      if quest.flora_walk.finished:
        actions.append("?school_forest_glade_river_lv0_interact_flora_walk")
      elif school_forest_glade["pollution"]<3 or not school_forest_glade["wrappers"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,corrupt|Use What?","quest_berb_fight_river_items"])
      else:
        actions.append("?school_forest_glade_river_lv0_interact")


label school_forest_glade_river_lv0_interact:
  "A once tranquil forest stream, now ruined by the sheer presence of the bark lord."
  return

label school_forest_glade_river_lv0_interact_flora_walk:
  "I wonder how many mutants the toxic spill caused?"
  "Probably enough for at least three X-Men movies..."
  return
