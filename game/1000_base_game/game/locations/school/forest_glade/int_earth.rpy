init python:
  class Interactable_school_forest_glade_earth(Interactable):

    def title(cls):
      return "Agricultured Meadow"

    def description(cls):
      rv = "Nothing like the smell of overturned dirt. Unless you count [flora]'s overturned panties."
      if not quest.jo_potted.in_progress:
        pass
      elif quest.jo_potted == "seeds":
        if school_forest_glade["farm"] == "dry":
          rv = "Lay thy eyes upon the field on which I grow my fucks and see that it is barren!"
        elif school_forest_glade["farm"] == "wet":
          rv = "A patch of well-plowed well-seeded dirt. Being a farmer and a porn star ain't so different."
          if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
            rv = "Smells like pepelepsi, earth, and concentrated evil. This is how Sauruman created the Uruk-hai."
      elif quest.jo_potted == "small":
        if school_forest_glade["farm"] == "dry":
          rv = "This is what it feels like to evolve from a hunter-gatherer.\n\nEach day my insights grow. Soon I'll write on clay tablets and sing praises to the sun god."
        elif school_forest_glade["farm"] == "wet":
          rv = "As a master of Plants vs. Zombies, I know for a fact that the undead are chanceless against this garden."
          if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
            rv = "Who knew a patch of earth could so perfectly represent my soul?"
      elif quest.jo_potted == "medium":
        if school_forest_glade["farm"] == "dry":
          rv = "Just like back on the flowing savannahs. Man vs. nature. No bullshit."
        elif school_forest_glade["farm"] == "wet":
          rv = "Water and thirsty saplings. The OTP of the ages. Batman and Robin got nothing on agriculture."
          if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
            rv = "Even the birds are scared. Do they want to take the chance with these crops? Do they want to take a chance with evil?"
      elif quest.jo_potted == "big":
        rv = "Ah! Look at them swaying in the gentle breeze. The time for harvesting has arrived!"
      return rv

    def actions(cls,actions):
      if quest.jo_potted == "plant":
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,plant|Use What?","quest_jo_pot_earth_plant"])
        actions.append("?school_forest_glade_earth_interact_plant")
      elif quest.jo_potted >= "seeds" and quest.jo_potted < "big":
        if not school_forest_glade["canal"] or school_forest_glade["farm"] == "dry":
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,earth|Use What?","quest_jo_pot_earth"])
        actions.append("school_forest_glade_earth_interact")
      elif quest.jo_potted == "big":
        actions.append("school_forest_glade_earth_interact_big")


label school_forest_glade_earth_interact_plant:
  "Ah, what a beauty! She's so ready to take my seeds."
  return

label school_forest_glade_earth_interact:
  if not quest.jo_potted.in_progress:
    pass
  elif quest.jo_potted == "seeds":
    if school_forest_glade["farm"] == "dry":
      "The seeds are planted, but thirsty. They scream for wet salvation from within their earthly hollows."
      "Digging a canal from the river would be an option if I had a shovel. Watering it manually is also possible."
    elif school_forest_glade["farm"] == "wet":
      if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
        "Watering fields with polluted water and deadly pesticides is mainstream practice nowadays."
        "Why shouldn't I profit from destructive capitalism when the big corps do?"
      else:
        "They're bathing in water, which is exactly how they like it. Now, the only thing left to do is wait."
  elif quest.jo_potted == "small":
    if school_forest_glade["farm"] == "dry":
      "Still thirsty. These little sprouts would drink a chronic alcoholic under the table with ease."
    elif school_forest_glade["farm"] == "wet":
      if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
        "Drink up the liquid corruption! Quench your thirst with pollution and death!"
        "Rise! Rise!"
      else:
        "They say that even the smallest lives are full of energy."
        "These little thirsters are also full of water."
  elif quest.jo_potted == "medium":
    if school_forest_glade["farm"] == "dry":
      "The scribes foretold this day!"
      "The day when the plants tickle the shins and the smell of shaved grass and freshly cut pussy fills their air."
      "Still, these grasslings need more water."
    elif school_forest_glade["farm"] == "wet":
      if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
        "They're growing stronger than ever! Reaching heights I never thought possible!"
        "It won't be long now. Not long at all."
        "Soon, my franklets! So very soon..."
      else:
        "Never before has a field looked so content with the water supply."
        "It's as if Aquarius himself stepped out of the sea and blessed this humble patch of land with his succulent dick juices."
  return

label school_forest_glade_earth_interact_big:
  "They grow up so fast! It felt like yesterday since they were little saplings!"
  "Now there's only one thing left to do. Harvest."
  show black with fadehold #fades in black, then fades it out back to the forest glade
  $school_forest_glade['farm'] = "farm"
  $process_event("update_state")
  hide black with Dissolve (1.5)
  if school_forest_glade["canal"] and school_forest_glade["pollution"] == 3:
    $mc.add_item("gigglypuff_leaves_corrupted")
    $quest.jo_potted["corrupt"] = True
  else:
    $mc.add_item("gigglypuff_leaves")
  $quest.jo_potted.advance("tea")
  $process_event("update_state")
  "The leaves are mine. Time to brew [jo] the ultimate throat-healing concoction."
  return
