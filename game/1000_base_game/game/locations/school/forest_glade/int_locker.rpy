init python:
  class Interactable_school_forest_glade_locker(Interactable):

    def title(cls):
      return isabelle.name + "'s Locker"

    def description(cls):
      return isabelle.name + "'s lost locker.\n\n...\n\nFound locker."

    def actions(cls,actions):
      actions.append("?school_forest_glade_locker_interact")


label school_forest_glade_locker_interact:
  call quest_isabelle_hurricane_call_maxine_locker
  return
