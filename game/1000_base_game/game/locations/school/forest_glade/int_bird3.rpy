init python:
  class Interactable_school_forest_glade_bird3_ground(Interactable):
    def title(cls):
      return "Bird"

    def actions(cls,actions):
      if quest.jo_potted == "small":   
        actions.append("?school_forest_glade_bird3_ground_interact")
      else:
        actions.append("?school_forest_glade_bird3_ground_interact2")
        
label  school_forest_glade_bird3_ground_interact2:     
  bird  "Kawk-kawk! Biwk!"
  $school_forest_glade["birds"][2] = "roof"
  return

label school_forest_glade_bird3_ground_interact:
  bird "Twip-twip! Beep!" 
  $school_forest_glade["birds"][2] = "roof"  
  return

init python:
  class Interactable_school_forest_glade_bird3_drugged(Interactable):
    def title(cls):
      return "Bird"

    def actions(cls,actions):
      actions.append("?school_forest_glade_bird3_drugged_interact")

label school_forest_glade_bird3_drugged_interact:
  show bird bird3 with Dissolve(.5)
  bird "Meep! Meep! Zoom!"
  $school_forest_glade["birds"][2] = "roof"  
  $quest.jo_potted["bird_swoop"] = random.randint(1, 4)
  return