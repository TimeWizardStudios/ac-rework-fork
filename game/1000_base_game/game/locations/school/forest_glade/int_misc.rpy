init python:
  class Interactable_school_forest_glade_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_forest_glade_dollar1_take"])


label school_forest_glade_dollar1_take:
  $school_forest_glade["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_forest_glade_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_forest_glade_dollar2_take"])


label school_forest_glade_dollar2_take:
  $school_forest_glade["dollar2_taken_today"] = True
  $mc.money+=20
  return
