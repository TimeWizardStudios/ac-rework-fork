init python:
  class Interactable_school_forest_glade_hole(Interactable):
    def title(cls):
      return "Hole"
    def description(cls):
      return "Not exactly six feet deep, but an adequate grave nonetheless."
    def actions(cls,actions):
      actions.append("?school_forest_glade_hole_interact")

label school_forest_glade_hole_interact:
  "Getting your hands and trusty shovel dirty. Nothing like a good old gravedigging."
  return