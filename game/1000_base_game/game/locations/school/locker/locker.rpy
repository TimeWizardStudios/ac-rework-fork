init python:
  class Location_school_locker(Location):

    default_title="Locker"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(-140,0),"school locker locker"])
      scene.append([(731,824),"school locker lunchfinal","school_locker_lunchfinal"])
      scene.append([(783,367),"school locker post"])

      if not school_locker["chocolate_box_got"] and quest.isabelle_buried == "getbox":
        scene.append([(1059,728),"school locker chocolate","school_locker_isabelle_box"])

      scene.append([(792,382),"school locker pen"])
      scene.append([(1130,931),"school locker todo"]) #,"school_locker_todo"]) ## Commented this out until we decide what to do with the to-do list

      #Polaroids
      if quest.kate_over_isabelle.finished:
        scene.append([(1075,141),"school locker isabelle"])
      if quest.isabelle_over_kate.finished:
        scene.append([(1075,141),"school locker kate"])
      if quest.lindsey_piano["lindsey_dancing_photo"]:
        scene.append([(1454,557),"school locker lindsey_dancing","school_locker_lindsey_dancing"])
      if quest.lindsey_wrong["kate_wet_photo"]:
        scene.append([(1523,434),"school locker katesoaked","school_locker_kate_soaked"])
      if False:#quest.lindsey_wrong["kate_wet_photo"]:
        scene.append([(1470,330),"school locker maxine"])
      if quest.maxine_wine.finished and not school_locker["maxine_nude_taken"]:
        scene.append([(1470,315),"school locker maxinewrist","school_locker_maxine_nude"])

      #Flora's posters
      if quest.maxine_wine == "locker" and not school_locker["flora_posters_taken"]:
        scene.append([(1021,655),"school locker posters","school_locker_flora_posters"])

      #Arrow Back
      scene.append([(400,921),"school first_hall_east exit_arrow","school_locker_back"])

    def find_path(self,target_location):
      return "school_locker_back"


label goto_school_locker:
  call goto_with_black(school_locker)
  return


init python:
  class Interactable_school_locker_lunchfinal(Interactable):

    def title(cls):
      return "Lunch Box"

    def description(cls):
      return "Nothing but the crumbled remains of meals past."

    def actions(cls,actions):
      if quest.isabelle_stolen == "maxineclue":
        actions.append(["take","Take","school_locker_lunch_maxine"])
      else:
        if school_locker["gotten_lunch_today"]:
          actions.append("?school_locker_lunchfinal_interact_empty")
        else:
          actions.append(["take","Take","school_locker_lunchfinal_interact"])


label school_locker_lunch_maxine:
  "Hmm... there's a doughnut in my lunch box."
  $mc.add_item("doughnut")
  "[jo] would never have put it there. It's too unhealthy."
  "[maxine] mentioned something about my personal food container... is this what she meant?"
  "Is this doughnut her clue?"
  "What does it mean?"
  "The first thing that comes to mind when I think of doughnuts is the [guard]."
  "Is this her way of saying I should talk to the [guard]?"
  "Why does she have to be so damn cryptic?"
  "Might as well start with the most obvious..."
  $quest.isabelle_stolen.advance("guard")
  return

label school_locker_lunchfinal_interact:
  if quest.jo_potted["got_soup"]:
    "Let's see what [jo] prepared for me today..."
    $mc.add_item("tide_pods",3)
    "Fucking [flora]...."
    "She thinks she's so funny."
    "I'll have to get her back for this."
  else:
    "On today's menu, soup in a can."
    "Not particularly tasty, but worth saving in case of the apocalypse."
    $quest.jo_potted["got_soup"] = True
    $mc.add_item("soup_can")
  $school_locker["gotten_lunch_today"] = True
  return

label school_locker_lunchfinal_interact_empty:
  "It's empty."
  "Just like my soul."
  "And my Contacts app."
  "Guess I'll have to wait till tomorrow."
  return


init python:
  class Interactable_school_locker_todo(Interactable):

    def title(cls):
      return "To Do list"

    def description(cls):
      return "You just made my list of things to do today."

    def actions(cls,actions):
      actions.append("?school_locker_todo_interact")


label school_locker_todo_interact:
  call screen todo_note
  return


init python:
  class Interactable_school_locker_back(Interactable):

    def title(cls):
      return "Ground Floor"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append(["go","Ground Floor","goto_school_ground_floor"])


screen todo_note():
  layer "master"
  add "misc todolist"
  if not quest.smash_or_pass.finished:
    add "items mysterious_coin" pos (700,300)
  else:
    add "items bottle water_bottle" pos (700,300)
  if not quest.kate_quest.finished:
    text "I need to do something with [kate]." style "ui_text" pos (650,400) at Transform(rotate=15)


init python:
  class Interactable_school_locker_isabelle_box(Interactable):

    def title(cls):
      return "Box of Chocolates"

    def description(cls):
      return "Life is like a box of chocolates, an overused cliche."

    def actions(cls,actions):
      actions.append(["take","Take","?school_locker_isabelle_box_take"])


label school_locker_isabelle_box_take:
  "Well, there it is. The legendary artifact at the heart of the conflict."
  "So, this means that the strange texter knew about the break in and [isabelle]'s box of hearts."
  "It also means that [maxine] either got extremely lucky with her riddle, or knows who the texter is."
  "Whether or not that's relevant is still up for debate. [maxine] doesn't share her secrets with anyone."
  "Well, I guess I should give the box back to [isabelle]."
  "Hell of a goose chase, and I haven't even gotten to taste one of those chocolate hearts..."
  $school_locker["chocolate_box_got"] = True
  $mc.add_item("isabelle_box")
  $quest.isabelle_buried.advance("isabelle")
  return


init python:
  class Interactable_school_locker_kate_soaked(Interactable):

    def title(cls):
      return "[kate]'s Soaked Photo"

    def description(cls):
      return "Maybe one day she'll look like that after I flush her head in the toilet."

    def actions(cls,actions):
      actions.append("?school_locker_kate_soaked_interact")


label school_locker_kate_soaked_interact:
  show misc kate_soaked with Dissolve(.5)
  "This one always makes me chuckle."
  "The day I made [kate] wet."
  "One of my biggest achievements."
  hide misc kate_soaked with Dissolve(.5)
  return


init python:
  class Interactable_school_locker_flora_posters(Interactable):

    def title(cls):
      return "[flora]'s Posters"

    def description(cls):
      return "[flora]'s posters. Very important."

    def actions(cls,actions):
      actions.append(["take","Take","school_locker_flora_posters_take"])
      actions.append("?school_locker_flora_posters_interact")


label school_locker_flora_posters_take:
  "All right. Time to be a poster boy."
  $school_locker["flora_posters_taken"] = True
  $mc.add_item("flora_poster",5)
  return

label school_locker_flora_posters_interact:
  "Freshly printed. Papercut warning."
  return


init python:
  class Interactable_school_locker_maxine_nude(Interactable):

    def title(cls):
      return "[maxine]'s Nude"

    def description(cls):
      return "This is more exciting than my porn mag back home."

    def actions(cls,actions):
      actions.append(["take","Take","school_locker_maxine_nude_take"])
      actions.append("?school_locker_maxine_nude_interact")


label school_locker_maxine_nude_take:
  $school_locker["maxine_nude_taken"] = True
  $mc.add_item("maxine_nude")
  return

label school_locker_maxine_nude_interact:
  show misc maxine_nude with Dissolve(.5)
  "Oh, fuck... that's hot.  So sleek and delicate!"
  "My nose is about to erupt!"
  hide misc maxine_nude with Dissolve(.5)
  return


init python:
  class Interactable_school_locker_lindsey_dancing(Interactable):

    def title(cls):
      return "[lindsey] Dancing Photo"

    def description(cls):
      return "I'll remember this moment forever, even without this picture."

    def actions(cls,actions):
      actions.append("?school_locker_lindsey_dancing_interact")


label school_locker_lindsey_dancing_interact:
  show misc lindsey_dancing with Dissolve(.5)
  "[lindsey] truly is a princess..."
  "Maybe one day I'll dance with her."
  hide misc lindsey_dancing with Dissolve(.5)
  return
