init python:
  class Location_school_secret_locker(Location):

    default_title="Secret Locker"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      scene.append([(-1,0),"school secret_locker locker"])
      #Panel
      if school_secret_locker['panel'] == "loose":
        scene.append([(784,416),"school secret_locker panel2","school_secret_locker_panel2"])
      elif school_secret_locker['panel'] == "off":
        if school_secret_locker['eyes']:
          scene.append([(850,496),"school secret_locker hole",("school_secret_locker_eye",-4,15)])
          scene.append([(829,511),"school secret_locker eye","school_secret_locker_eye"])
        else:
          scene.append([(850,496),"school secret_locker hole","school_secret_locker_hole"])
      else:
        scene.append([(830,418),"school secret_locker panel1"])

      if not school_secret_locker["secret_note_taken"]:
        scene.append([(799,614),"school secret_locker secret_note",("school_secret_locker_secret_note",15,0)])
      if not school_secret_locker["coin_taken"]:
        scene.append([(809,909),"school secret_locker coin","school_secret_locker_coin"])
      if not school_secret_locker["key_taken"]:
        scene.append([(992,906),"school secret_locker key","school_secret_locker_key"])

      #Jacklyn Hand
      if school_secret_locker['hand']:
        scene.append([(519,458),"school secret_locker jacklyn_hand_unscrew","school_secret_locker_jacklyn_hand_unscrew"])

      scene.append([(719,348),"school secret_locker spiderweb","school_secret_locker_spiderweb"])

      #Arrow Back
      scene.append([(400,921),"school first_hall_east exit_arrow","school_locker_back"])

    def find_path(self,target_location):
      return "school_locker_back"

label goto_school_secret_locker:
  $game.location=school_secret_locker
  return



### school_secret_locker_hole interactable ####################################

init python:
  class Interactable_school_secret_locker_hole(Interactable):
    def title(cls):
      return "Darkness"
    def description(cls):
      return "I never realized the gate to hell was this small...\n\nNot too worried about demonic invasions anymore!"
    def actions(cls,actions):
      actions.append("?school_secret_locker_hole_interact")

label school_secret_locker_hole_interact:
  "Who knows how deep this hole is? It might be connected to the air vents or something..."
  return


### school_secret_locker_locker interactable ##################################

init python:
  class Interactable_school_secret_locker_locker(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_secret_locker_locker_interact")

label school_secret_locker_locker_interact:
  ""
  return

### school_secret_locker_eye interactable #####################################

init python:
  class Interactable_school_secret_locker_eye(Interactable):
    def title(cls):
      return "???"
    def description(cls):
      return "It's staring right at me. What kind of sick demon hides in the walls?"
    def actions(cls,actions):
      actions.append("school_secret_locker_eye_interact")

label school_secret_locker_eye_interact:
  menu(side="left"):
    "Stick your hand in":
      "Okay, please don't bite my hand off..."
      mc "That's a good wall demon. Good wall demon."
      mc "..."
      $school_secret_locker["eyes"] = False
      $mc.add_item("saucer_eyed_doll")
      "Seriously? [maxine] wasn't joking..."
      $quest.maxine_lines.advance("seemaxine")
    "Stick your dick in" if not quest.maxine_lines["dicked"]:
      "I don't care if it's a demon in there. I'm getting my dick sucked."
      "..."
      "..."
      "It feels like... some kind of soft textile?"
      $mc.lust+=1
      mc "Start sucking, you demonic bitch!"
      "Shit, I think the guard heard me..."
      "Time to pull it out."
      $quest.maxine_lines["dicked"] = True
      $achievement.glorious_bastard.unlock()
    "Keep your limbs":
      "I'm not going near that shit..."
  return

### school_secret_locker_panel2 interactable ##################################

init python:
  class Interactable_school_secret_locker_panel2(Interactable):
    def title(cls):
      return "Loose Panel"
    def description(cls):
      return "A real life glory hole? This could be my big break!"
    def actions(cls,actions):
      if quest.maxine_lines == "finalnode" and quest.maxine_lines["right_tool"]:
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:maxine_lines,panel2|Use What?","school_secret_locker_panel2_use_item"])
      actions.append("?school_secret_locker_panel2_interact")

label school_secret_locker_panel2_interact:
  "I don't remember this panel being loose..."
  "Perhaps with the right tool, I could remove the screws and see what's behind it?"
  $quest.maxine_lines["right_tool"] = True
  return

label school_secret_locker_panel2_use_item(item):
  "Attacking these screws with my [item.title_lower] wouldn't exactly help... unless I wanted a quick ticket to the Newfall Asylum."
  "And ever since the Great Tool Rebellion, screwdrivers have been extinct."
  "I need to find something else. Or someone with long nails..."
  $quest.maxine_lines['lll_charged'] = False
  $quest.maxine_lines.advance("unscrew")
  return
### school_secret_locker_panel1 interactable ##################################

init python:
  class Interactable_school_secret_locker_panel1(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_secret_locker_panel1_interact")

label school_secret_locker_panel1_interact:
  ""
  return

### school_secret_locker_spiderweb interactable ###############################

init python:
  class Interactable_school_secret_locker_spiderweb(Interactable):
    def title(cls):
      return "Spider Web"
    def description(cls):
      return "Tugged by the gentle breeze; the sail of a sinking arachnid ship."
    def actions(cls,actions):
      actions.append("?school_secret_locker_spiderweb_interact")

label school_secret_locker_spiderweb_interact:
  "A web so old even the spider has abandoned it."
  return

### school_secret_locker_secret_note interactable #############################

init python:
  class Interactable_school_secret_locker_secret_note(Interactable):
    def title(cls):
      return "Secret Note"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","school_secret_locker_secret_note_take"])

label school_secret_locker_secret_note_take:
  $school_secret_locker["secret_note_taken"] = True
  $mc.add_item("secret_note")
  "You've been hit by, you've been struck by... well, it's not really a crime. More like collecting evidence for an investigation."
  return

### school_secret_locker_coin interactable ####################################

init python:
  class Interactable_school_secret_locker_coin(Interactable):
    def title(cls):
      return "Mysterious Coin"
    def description(cls):
      return "An ancient coin, covered in dust."
    def actions(cls,actions):
      actions.append(["take","Take","school_secret_locker_coin_take"])
      actions.append("?school_secret_locker_coin_interact")

label school_secret_locker_coin_interact:
  "Finally, some riches! This coin might be worth a lot!"
  "Or nothing. There's always a good chance it's worth nothing."
  return

label school_secret_locker_coin_take:
  $school_secret_locker["coin_taken"] = True
  $mc.add_item("mysterious_coin")
  return


### school_secret_locker_key interactable #####################################

init python:
  class Interactable_school_secret_locker_key(Interactable):
    def title(cls):
      return "Mysterious Key"
    def description(cls):
      return "Whoever left this behind didn't follow school protocol. Always {space=-10}leave your key at the [guard]'s booth!{space=-10}"
    def actions(cls,actions):
      actions.append(["take","Take","school_secret_locker_key_take"])
      actions.append("?school_secret_locker_key_interact")

label school_secret_locker_key_interact:
  "This strange key seems to fit the outdated lock of this locker..."
  return

label school_secret_locker_key_take:
  $school_secret_locker["key_taken"] = True
  $mc.add_item("secret_key")
  return

### school_secret_locker_jacklyn_hand_unscrew interactable ####################

init python:
  class Interactable_school_secret_locker_jacklyn_hand_unscrew(Interactable):
    def title(cls):
      return ""
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("?school_secret_locker_jacklyn_hand_unscrew_interact")

label school_secret_locker_jacklyn_hand_unscrew_interact:
  ""
  return
