init python:
  class Location_school_ground_floor_west(Location):

    default_title="Admin Wing"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,0),"school ground_floor_west gfwhall"])
      scene.append([(0,0),"school ground_floor_west nursedoor",("school_ground_floor_west_door_nurse",0,265)])
      scene.append([(498,163),"school ground_floor_west headmasterdoor",("school_ground_floor_west_principal_door",0,135)])
      scene.append([(611,0),"school ground_floor_west lamp"])
      scene.append([(695,351),"school ground_floor_west magazines","school_ground_floor_west_magazines"])
      scene.append([(930,279),"school ground_floor_west bookshelf","school_ground_floor_west_bookshelf"])
      scene.append([(1086,398),"school ground_floor_west sofa_right","school_ground_floor_west_sofa_right"])
      scene.append([(796,398),"school ground_floor_west sofa_left"])
      scene.append([(1306,556),"school ground_floor_west table"])

      if quest.jacklyn_romance == "hide_and_seek" and not school_ground_floor_west["ball_of_yarn_taken"]:
        scene.append([(837,441),"school ground_floor_west ball_of_yarn","school_ground_floor_west_ball_of_yarn"])

      #Maxine Plant
      if school_ground_floor_west["lifted_glass"] and quest.flora_bonsai.in_progress:
        scene.append([(1344,469),"school ground_floor_west pot"])
      else:
        scene.append([(1344,469),"school ground_floor_west pot","school_ground_floor_west_pot"])

      #Painting
      scene.append([(271,17),"school ground_floor_west paintings"])

      #Sun Painting Hidden Stash
      if quest.maxine_wine == "search":
        if quest.maxine_wine["hidden_stash_revealed"]:
          scene.append([(833,243),"school ground_floor_west sun_painting"])
          scene.append([(842,259),"school ground_floor_west sun_painting_hidden_stash"])
          scene.append([(889,280),"school ground_floor_west wine","school_ground_floor_west_wine"])
          scene.append([(771,220),"school ground_floor_west sun_painting_side","school_ground_floor_west_sun_painting_side"])
        else:
          scene.append([(833,243),"school ground_floor_west sun_painting","school_ground_floor_west_sun_painting"])
      else:
        scene.append([(833,243),"school ground_floor_west sun_painting"])

      #Bulletin Board
      if (quest.maxine_wine == "locker" and mc.owned_item("flora_poster") and not quest.maxine_wine["admin_wing_poster"]
      or quest.lindsey_angel == "phone_book"):
        scene.append([(1308,15),"school ground_floor_west info",("school_ground_floor_west_info",0,250)])
      else:
        scene.append([(1308,15),"school ground_floor_west info"])
        if game.season == 1 and not quest.jo_washed.in_progress:
          if quest.maxine_wine["admin_wing_poster"]:
            scene.append([(1595,166),"school ground_floor_west flora_poster"])

      #Aquarium
      scene.append([(241,251),"school ground_floor_west aquarium","school_ground_floor_west_aquarium"])

      #Glass Dome and Vines
      if quest.flora_bonsai.in_progress:
        if quest.flora_bonsai >= "vines" and not school_nurse_room["tree_done"]:
          if school_ground_floor_west["lifted_glass"]:
            scene.append([(1316,420),"school ground_floor_west glassdome_lifted"])
            scene.append([(1269,431),"school ground_floor_west glassvine_pot"])
          else:
            scene.append([(1269,431),"school ground_floor_west glassvine"])
            scene.append([(1297,449),"school ground_floor_west glassdome","school_ground_floor_west_glassdome"])

          scene.append([(12,40),"school ground_floor_west vines",("school_ground_floor_west_vines",180,830)])

          if school_ground_floor_west["lifted_glass"]:
            scene.append([(1382,438),"school ground_floor_west stickynote_lifted"])
          else:
            scene.append([(1358,483),"school ground_floor_west stickynote","school_ground_floor_west_glassdome"])

      #Flora
      if not school_ground_floor_west["exclusive"] or "flora" in school_ground_floor_west["exclusive"]:
        if not flora.talking:
          if flora.at("school_ground_floor_west","standing"):
            if game.season == 1:
              if flora.equipped_item("flora_skirt"):
                scene.append([(419,277),"school ground_floor_west flora_skirt","flora"])
              else:
                scene.append([(419,277),"school ground_floor_west flora","flora"])
            elif game.season == 2:
              scene.append([(421,277),"school ground_floor_west flora_autumn","flora"])
          if flora.at("school_ground_floor_west","vines"):
            if flora.equipped_item("flora_skirt"):
              scene.append([(419,277),"school ground_floor_west flora_bandage_skirt","flora"])
            else:
              scene.append([(419,277),"school ground_floor_west flora_bandage","flora"])
            scene.append([(159,739),"#school ground_floor_west reaching_vine"])

      #Maxine
      if not school_ground_floor_west["exclusive"] or "maxine" in school_ground_floor_west["exclusive"]:
        if not maxine.talking:
          if maxine.at("school_ground_floor_west","spying"):
            if game.season == 1:
              scene.append([(1018,0),"school ground_floor_west maxine",("maxine",0,200)])
            elif game.season == 2:
              scene.append([(1017,0),"school ground_floor_west maxine_autumn",("maxine",0,200)])
            scene.append([(1014,568),"school ground_floor_west maxine_shadow"])

      #Isabelle
      if not school_ground_floor_west["exclusive"] or "isabelle" in school_ground_floor_west["exclusive"]:
        if not isabelle.talking:
          if isabelle.at("school_ground_floor_west","standing"):
            if game.season == 1:
              scene.append([(1201,309),"school ground_floor_west isabelle","isabelle"])
            elif game.season == 2:
              scene.append([(1200,309),"school ground_floor_west isabelle_autumn","isabelle"])
              if isabelle.equipped_item("isabelle_collar"):
                scene.append([(1242,374),"school ground_floor_west isabelle_collar",("isabelle",25,-65)])

      #Nurse
      if not school_ground_floor_west["exclusive"] or "nurse" in school_ground_floor_west["exclusive"]:
        if not nurse.talking:
          if nurse.at("school_ground_floor_west","standing"):
            scene.append([(842,284),"school ground_floor_west nurse","nurse"])

      if quest.kate_wicked == "costume" and not quest.kate_wicked["own_costume"]:
        scene.append([(641,582),"school ground_floor_west mystery_box","school_ground_floor_west_mystery_box"])

      #Spinach
      if not school_ground_floor_west["exclusive"] or "spinach" in school_ground_floor_west["exclusive"]:
        if not spinach.talking:
          if spinach.at("school_ground_floor_west","running"):
            scene.append([(839,489),"school ground_floor_west spinach","spinach"])

      #Beaver
      if quest.lindsey_book["beaver"] == 5:
        scene.append([(1154,509),"school ground_floor_west beaver","school_ground_floor_west_beaver"])

      #Dollar/Books
      if not quest.jo_washed.in_progress:
        if school_ground_floor_west["dollar1_spawned_today"] == True and not school_ground_floor_west["dollar1_taken_today"]:
          scene.append([(381,353),"school ground_floor_west dollar1","school_ground_floor_west_dollar1"])
        if school_ground_floor_west["dollar2_spawned_today"] == True and not school_ground_floor_west["dollar2_taken_today"]:
          scene.append([(1337,202),"school ground_floor_west dollar2","school_ground_floor_west_dollar2"])
        if not school_ground_floor_west["book_taken"]:
          scene.append([(1148,442),"school ground_floor_west book","school_ground_floor_west_book"])

      if quest.kate_wicked == "costume" and not quest.kate_wicked["own_costume"]:
        scene.append([(0,0),"#school ground_floor_west night_overlay_mystery_box"])
      elif (quest.kate_wicked in ("costume","party")
      or quest.isabelle_dethroning in ("trap","revenge","revenge_done","panik")
      or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape")):
        scene.append([(0,0),"#school ground_floor_west night_overlay"])

      #Exit Arrow
      scene.append([(904,921),"school first_hall_east exit_arrow","school_ground_floor_west_back"])

    def find_path(self,target_location):
      if target_location == "school_nurse_room":
        return "school_ground_floor_west_door_nurse",dict(marker_offset=(30,300),marker_sector="left_bottom")
      elif target_location == "school_principal_office":
        return "school_ground_floor_west_principal_door",dict(marker_offset=(110,-50),marker_sector="left_bottom")
      else:
        return "school_ground_floor_west_back",dict(marker_offset=(0,0),marker_sector="right_bottom")


label goto_school_ground_floor_west:
  if school_ground_floor_west.first_visit_today:
    $school_ground_floor_west["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $school_ground_floor_west["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(school_ground_floor_west)
  return
