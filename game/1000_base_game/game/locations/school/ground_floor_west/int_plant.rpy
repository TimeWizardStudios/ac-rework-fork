init python:
  class Interactable_school_ground_floor_west_plant(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      return "Once during junior year, crime scene tape surrounded this plant. Not sure what that was about."

    def actions(cls,actions):
      if quest.isabelle_stolen == "askmaxine" and mc.owned_item("maxine_letter"):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_stolen|Use What?","ground_floor_west_plant_isabelle_stolen_maxine_letter"])
      actions.append("?ground_floor_west_plant")


label ground_floor_west_plant:
  "On the surface, a completely normal plant."
  "Under closer inspection, however..."
  "Still a completely normal plant."
  return

label ground_floor_west_plant_isabelle_stolen_maxine_letter(item):
  if item == "maxine_letter":
    $mc.remove_item("maxine_letter")
    "Now that the letter has been deposited into [maxine]'s mailbox, the only thing left to do is wait."
    "Knowing [maxine], her response is probably going to come in some random place other than this plant."
    $quest.isabelle_stolen.advance("maxineletter")
  else:
    "Not quite a letter..."
    $quest.isabelle_stolen.failed_item("give_liquid",item)
  return
