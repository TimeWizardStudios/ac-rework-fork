init python:
  class Interactable_school_ground_floor_west_vines(Interactable):

    def title(cls):
      return "Vines"

    def description(cls):
      return "Just some vines escaping through the cracks of the door. Surely, nothing to worry about."

    def actions(cls,actions):
      actions.append("?school_ground_floor_west_vines_interact")


label school_ground_floor_west_vines_interact:
  "Are you yanking my chain? Are you pulling my leg? Are you tugging my vine?"
  return
