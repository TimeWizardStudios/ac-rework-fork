init python:
  class Interactable_school_ground_floor_west_beaver(Interactable):
    
    def title(cls):
      return "Beaver"
   
    def description(cls):
      return "Oh, shit! It tried to bite me! Almost lost my arm there."
    
    def actions(cls,actions):
      actions.append("?school_ground_floor_west_beaver_interact")

label school_ground_floor_west_beaver_interact:
  #Beaver noises
  mc "Yeah, this is a dead end."
  mc "No more running."
  #Beaver noises
  "That beaver already chewed up the easel. Nothing but wood chips left."
  "There'll be no painting with [lindsey]..."
  "It's scared shitless."
  menu(side="left"):
    extend ""
    "Kick the beaver":
      $mc.love -= 1
      #SFX: thud
      #Beaver noises
      #Hide Beaver (light fur)
      #Show Beaver (dead) (light fur)
      ## Kick and Beaver
      queue sound ["hit", "<to 0.7>squeaking_animal"]
      "Hmm... it just collapsed. Didn't mean for that to happen."
      "It did ruin my painting date with [lindsey], but now I feel kinda bad."
      "..."
      "There's no pulse. Shit."
      "..."
      "Better hide the body before anyone notices that I killed a beaver."
    "Let the beaver go":
      $mc.love += 1
      $unlock_stat_perk("love21")
      "It's an innocent animal. It doesn't know any better."
      mc "Come on, you're free to go."
      "It doesn't trust me."
      mc "I won't hurt you. Get out of here."
      #Beaver noises
      "It's shaking in terror. Poor thing."
      "Hmm... I guess I'll just—"
      #Hide Beaver (light fur)
      #Show Beaver (dead) (light fur)
      "It just collapsed into a heap."
      "The chase and the fear must've been too much for it."
      "..."
      "There's no pulse. Shit."
      "..."
      "Better hide the body before anyone notices that I killed a beaver."
    "Drown the beaver in the aquarium":
      $mc.lust -= 1
      $mc.love -= 1
      $quest.lindsey_book["beaver"] = 6
      #SFX: Water splashing sounds
      ## Drowning
      play sound "<to 1.0>water_impact" fadeout 0.5
      queue sound "<to 2.3>water_splash" loop
      "Die, you little fuck. You ruined my chance with [lindsey]!"
      "I don't care if you bite me or struggle. I've suffered worse."
      #SFX: Water splashing sounds
      ## Drowning
      play sound "<to 1.0>water_impact" fadeout 0.5
      queue sound "<to 2.3>water_splash" loop
      "That's it."
      "Stop struggling. "
      "This is the end."
      "..."
      stop sound fadeout 1.0
      "It's finally dead. Justice served."
  $quest.lindsey_book["beaver"] = 6
  $mc.add_item("beaver_carcass")
  $quest.lindsey_book.finish()
  return
