init python:
  class Interactable_school_ground_floor_west_sun_painting(Interactable):
    def title(cls):
      return "Sun Painting"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("school_ground_floor_west_sun_painting_interact")

label school_ground_floor_west_sun_painting_interact:
  $quest.maxine_wine["hidden_stash_revealed"] = True
  return


init python:
  class Interactable_school_ground_floor_west_sun_painting_side(Interactable):
    def title(cls):
      return "Sun Painting"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append("school_ground_floor_west_sun_painting_side_interact")

label school_ground_floor_west_sun_painting_side_interact:
  $quest.maxine_wine["hidden_stash_revealed"] = False
  return


init python:
  class Interactable_school_ground_floor_west_wine(Interactable):
    def title(cls):
      return "Summer Wine"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","school_ground_floor_west_wine_take"])

label school_ground_floor_west_wine_take:
  "Ah, of course! [jacklyn]'s an artist, so naturally it'd be behind a painting!"
  "Hmm... this stash isn't all that dusty, which means it has seen recent use."
  "I wonder whose happy hour I'm ruining."
  $quest.maxine_wine["hidden_stash_revealed"] = False
  $mc.add_item("summer_wine")
  $quest.maxine_wine.advance("maxinewine")
  return
