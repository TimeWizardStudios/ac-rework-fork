init python:
  class Interactable_school_ground_floor_west_magazines(Interactable):

    def title(cls):
      return "Magazine Stand"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_stolen.started:
        return  "In this issue of Herpes Bazaar: Zandra Gullock's gullet — a phallic black hole."
      else:
        return "Nowadays, people are on their phones while they're waiting for appointments. It won't be long before the magazine market collapses."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.isabelle_stolen.started:
        actions.append("?school_ground_floor_west_magazines_interact_two")
      actions.append("?school_ground_floor_west_magazines_interact")


label school_ground_floor_west_magazines_interact:
  "Never been much for magazines, unless we're talking about pornographic ones. This stand has nothing of the sort."
  return

label school_ground_floor_west_magazines_interact_two:
  "A monthly magazine of popular culture, beauty, and fashion. Vanity Unfair."
  return
