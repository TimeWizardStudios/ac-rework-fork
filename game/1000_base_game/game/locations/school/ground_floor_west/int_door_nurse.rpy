init python:
  class Interactable_school_ground_floor_west_door_nurse(Interactable):

    def title(cls):
      if quest.nurse_aid["name_reveal"]:
        return "Nurse's Office"
      else:
        return "[nurse]'s Office"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The land of rubbing alcohol and lollipops."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "vines":
            actions.append(["go","School Nurse","?school_ground_floor_west_door_nurse_flora_bonsai_vines"])
            return
          elif quest.flora_bonsai == "attack":
            actions.append(["go","School Nurse","school_ground_floor_west_door_nurse_flora_bonsai_attack"])
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume" and not quest.kate_wicked["own_costume"]:
            actions.append(["go","School Nurse","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","panik"):
            actions.append(["go","School Nurse","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","School Nurse","?quest_isabelle_dethroning_revenge_done_admin_wing_nurse_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","School Nurse","goto_school_nurse_room"])
      else:
        if quest.nurse_aid.finished and quest.nurse_aid["vajay_wagon_now"]:
          actions.append(["go","School Nurse","?quest_nurse_aid_someone_else_vajay_wagon"])
          return
      if school_ground_floor_west["nurse_room_locked_today"]: ## or school_ground_floor_west["nurse_room_locked_now"]
        actions.append(["go","School Nurse","?school_ground_floor_west_door_nurse_locked"])
      else:
        actions.append(["go","School Nurse","goto_school_nurse_room"])


label school_ground_floor_west_door_nurse_locked:
  if school_ground_floor_west["nurse_room_locked_today"] == "flora":
    "[flora] needs rest. I should leave her alone for today."
  elif school_ground_floor_west["nurse_room_locked_today"] == "maxine":
    "[maxine] needs rest. I should leave her alone for today."
  return

label school_ground_floor_west_door_nurse_flora_bonsai_vines:
  "Those vines just slithered through the cracks."
  "I've watched enough hentai, err... {i}gardening videos{/} to know they're thirsty."
  return

label school_ground_floor_west_door_nurse_flora_bonsai_attack:
  "Whatever nightmares await on the other side of that door, it surely can't be worse than [kate]."
  call goto_school_nurse_room
  $quest.flora_bonsai.advance("tinybigtree")
  return
