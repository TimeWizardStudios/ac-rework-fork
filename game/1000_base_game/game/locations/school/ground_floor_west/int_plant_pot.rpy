init python:
  class Interactable_school_ground_floor_west_pot(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.jo_day == "supplies":
        return "Blink twice if you need help,\nlittle plant."
      else:
        return "Once during junior year, crime scene tape surrounded this plant. Not sure what that was about."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "phone_book":
            actions.append("?quest_lindsey_angel_phone_book_school_ground_floor_west_pot")
            return
      else:
        if mc.owned_item("maxine_letter"):
          actions.append(["use_item", "Use", "select_inventory_item","$quest_item_filter:isabelle_stolen,askmaxine|Use What?","school_ground_floor_west_pot_use_item"])
        if quest.isabelle_stolen == "maxineletter":
          actions.append("?school_ground_floor_west_pot_interact_letter_planted")
        if quest.lindsey_motive == "lure":
          actions.append("?quest_lindsey_motive_lure_ground_floor_west_plant_interact")
        if quest.jo_day == "supplies":
          actions.append("?quest_jo_day_supplies_plant_two")
      actions.append("?school_ground_floor_west_pot_interact")


label school_ground_floor_west_pot_use_item(item):
  if item.id == "maxine_letter":
    $mc.remove_item("maxine_letter")
    "Now that the letter has been deposited into [maxine]'s mailbox, the only thing left to do is wait."
    "Knowing [maxine], her response is probably going to come in some random place other than this plant."
    $quest.isabelle_stolen.advance("maxineletter")
  else:
    "Putting my [item.title_lower] into this potted plant might seem harmless at first glance..."
    "Next thing you know the school is overrun by aliens and [maxine] is marching down the street at the head of a spider army."
    "Not risking that."
    $quest.isabelle_stolen.failed_item("askmaxine",item)
  return

label school_ground_floor_west_pot_interact_letter_planted:
  "Where's that damn response from [maxine]? I've been looking everywhere. The sweat is dripping. Need to quench the thirst for information."
  return

label school_ground_floor_west_pot_interact:
  "There's something deeply unsettling about this plant. Too flourishing.{space=-35}\nToo happy in its lonesome state."
  return
