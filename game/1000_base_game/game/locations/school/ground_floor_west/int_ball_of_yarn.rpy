init python:
  class Interactable_school_ground_floor_west_ball_of_yarn(Interactable):

    def title(cls):
      return "Gray Ball of Yarn"

    def description(cls):
      return "If it looks like yarn and it spools like yarn... it could be any kind of thread, really."

    def actions(cls,actions):
      actions.append(["take","Take","school_ground_floor_west_ball_of_yarn_take"])
      actions.append("?school_ground_floor_west_ball_of_yarn_interact")


label school_ground_floor_west_ball_of_yarn_interact:
  "Just call me Theseus."
  "With enough yarn to give the minotaur the shibari treatment, and still find my way out of the maze."
  return

label school_ground_floor_west_ball_of_yarn_take:
  if ("school_ground_floor_west_ball_of_yarn_interact",()) not in game.seen_actions:
    call school_ground_floor_west_ball_of_yarn_interact
    window hide
  $quest.jacklyn_romance["balls_of_yarn_found"]+=1
  $school_ground_floor_west["ball_of_yarn_taken"] = True
  $mc.add_item("ball_of_yarn_gray",silent=True)
  $game.notify_modal(None,"Inventory","{image=items ball_of_yarn_gray}{space=50}|Gained\n{color=#900}Gray Ball of Yarn{/color}\n("+str(quest.jacklyn_romance["balls_of_yarn_found"])+"/5)",5.0)
  pause 0.25
  jump quest_jacklyn_romance_hide_and_seek_school_ground_floor_west_upon_leaving
