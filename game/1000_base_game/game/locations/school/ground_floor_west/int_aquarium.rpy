init python:
  class Interactable_school_ground_floor_west_aquarium(Interactable):

    def title(cls):
      return "Aquarium"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_stolen.started:
        return  "The blue fish used to be red, but she lost her lover. A tragedy of redefinned scales."
      else:
        return "The fish here probably think that this tank is the entire world. They'll never know the ocean. Must be nice to live in bliss, unaware of what they're missing."

    def actions(cls,actions):
      if mc.owned_item(("diary_page","secret_note","empty_bottle","spray_empty_bottle","kate_socks_dirty")):
        actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:flora_bonsai,aquarium|Use What?","school_ground_floor_west_aquarium_flora_bonsai_use"])
      if not mc["other_half_note_retrieved"]:
        actions.append(["take","Take","school_ground_floor_west_aquarium_take"])
      if mc["focus"]:
        if mc["focus"] == "kate_desire":
          if quest.kate_desire == "starblue":
            actions.append("quest_kate_desire_aquarium")
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            if mc.owned_item(("diary_page","secret_note","empty_bottle","spray_empty_bottle","kate_socks_dirty")):
              actions.remove(["use_item","Use item","select_inventory_item","$quest_item_filter:flora_bonsai,aquarium|Use What?","school_ground_floor_west_aquarium_flora_bonsai_use"])
            if not mc["other_half_note_retrieved"]:
              actions.remove(["take","Take","school_ground_floor_west_aquarium_take"])
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "phone_book":
            actions.append("?quest_lindsey_angel_phone_book_school_ground_floor_west_aquarium")
            return
      else:
        if quest.isabelle_stolen == "maxineletter" and not quest.isabelle_stolen["letter_from_maxine_recieved"]:
          actions.append("school_first_hall_water_fountain_interact_askmaxine")
      if quest.isabelle_stolen.started:
        actions.append("?school_ground_floor_west_aquarium_interact_two")
      actions.append("?school_ground_floor_west_aquarium_interact")


label school_ground_floor_west_aquarium_flora_bonsai_use(item):
  if item == "diary_page":
    show misc secret_diary with Dissolve(.5)
    show misc secret_diary_2 with Dissolve(2)
    pause
    hide secret_diary
    hide misc secret_diary_2
    with Dissolve(.5)
  elif item == "kate_socks_dirty":
    "I wonder what [kate] will think when I return her socks perfectly clean...{space=-45}"
    "It's weird, but I can't wait to feel that shame she'll undoubtedly pour on."
    "..."
    $mc.remove_item("kate_socks_dirty")
    $mc.add_item("kate_socks_clean")
  elif item == "secret_note":
    show misc secret_note_1 with Dissolve(.5)
    show misc tsn as secret_note_reveal with Dissolve(2)
    pause
    hide secret_note_reveal
    hide misc secret_note_1
    with Dissolve(.5)
  elif item == "empty_bottle":
    $mc.remove_item(item)
    $mc.add_item("water_bottle")
    "There we go. Refilled and as good as new."
    "Any fish caught inside will count as extra nutrients."
  elif item == "spray_empty_bottle":
    $mc.remove_item(item)
    $mc.add_item("spray_water")
    "Spray and Pray: Reloaded."
    "This is the new title for this episode."
  else:
    "My [item.title_lower] won't hold any water."
    $quest.flora_bonsai.failed_item("aquarium",item)
  return

label school_ground_floor_west_aquarium_take:
  "There's something peeking out of the sand. A hidden treasure, perhaps."
  $mc["other_half_note_retrieved"] = True
  $mc.add_item("half_letter_2")
  return

label school_ground_floor_west_aquarium_interact:
  "This tank is meant to calm you down before you receive a detention or medical exam."
  return

label school_ground_floor_west_aquarium_interact_two:
  "The green fish used to have a friend. Now he's shunned by the rest of the shoal."
  "Cannibalism never pays off."
  return
