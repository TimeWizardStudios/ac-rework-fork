init python:
  class Interactable_school_ground_floor_west_info(Interactable):

    def title(cls):
      return "Bulletin Board"

    def description(cls):
      if quest.lindsey_angel == "phone_book":
        return "All of the school's useless information consolidated into one unhelpful place."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "phone_book":
            actions.append("?quest_lindsey_angel_phone_book_school_ground_floor_west_info")
      else:
        actions.append(["use_item", "Use Item", "select_inventory_item","$quest_item_filter:maxine_wine,flora_poster|Use What?","school_ground_floor_west_info_maxine_wine_locker_use_item"])


label school_ground_floor_west_info_maxine_wine_locker_use_item(item):
  if item == "flora_poster":
    $quest.maxine_wine["admin_wing_poster"] = True
    $mc.remove_item("flora_poster")
    "Let's put this one right over the [nurse]'s schedule for yearly check-ups. Ain't nobody got time to be healthy."
    if quest.maxine_wine["entrance_hall_poster"] + quest.maxine_wine["homeroom_poster"] + quest.maxine_wine["admin_wing_poster"] + quest.maxine_wine["1f_hall_poster"] + quest.maxine_wine["sports_wing_poster"] == 5:
      "Okay, that's all of them. Praise the lord Jesus hallelujah. My ass is sweating more than that hipster up on the cross."
      $quest.maxine_wine.advance("florareward")
  else:
    "And on today's agenda... my [item.title_lower]!"
    $quest.maxine_wine.failed_item("flora_poster",item)
  return
