init python:
  class Interactable_school_ground_floor_west_glassdome(Interactable):

    def title(cls):
      return "Glass Dome"

    def description(cls):
      return "Looks like some form of impenetrable glass. Luckily, it can be lifted off the table if needed."

    def actions(cls,actions):
      actions.append("school_ground_floor_west_glassdome_interact")


label school_ground_floor_west_glassdome_interact:
  "Someone put a glass dome over the potted plant."
  "The note reads:"
  "{i}\"My fellow less-than-competent scientist.\"{/}"
  "{i}\"Please, put a leash on your pet plant. It appears to be rutting and ready to pollinate.\"{/}"
  "{i}\"The specimen in area 12 is to remain unscathed.\"{/}"
  "{i}\"Lifting the glass would jeopardize at least nine pseudo-civilizations as well as the janitor's stash of confiscated coital aids.\"{/}"
  "{i}\"Proceed with caution.\"{/}"
  "{i}\"M.\"{/}"
  menu(side="middle"):
    "Lift the glass":
      $school_ground_floor_west["lifted_glass"] = True
      "I know what it's like to be denied your love interest."
      "Enjoy her, big boy!"
      #$specimen_12.start() #a quest for much later
    "Leave it":
      return
  return
