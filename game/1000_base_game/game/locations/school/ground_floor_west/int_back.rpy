init python:
  class Interactable_school_ground_floor_west_back(Interactable):

    def title(cls):
      return "Entrance Hall"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "The upside of staying here is that nobody dares to mess with me outside the principal's office. The downside of staying here is that it's close to the principal's office."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai in ("vines","attack"):
            actions.append(["go","Entrance Hall","?ground_floor_west_back_flora_bonsai_attack_interact"])
            return
          elif quest.flora_bonsai == "tinybigtree":
            actions.append(["go","Entrance Hall","?ground_floor_west_back_flora_bonsai_interact"])
            return
        elif mc["focus"] == "nurse_photogenic":
          if quest.nurse_photogenic == "eavesdrop":
            if quest.nurse_photogenic["eavesdropped"]:
              actions.append(["go","Entrance Hall","?quest_nurse_photogenic_eavesdrop"])
            else:
              actions.append(["interact","Eavesdrop","quest_nurse_photogenic_eavesdrop"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(["go","Entrance Hall","goto_school_ground_floor"])
      actions.append(["go","Entrance Hall","goto_school_ground_floor"])


label ground_floor_west_back_flora_bonsai_attack_interact:
  "I can't just leave [flora] to her fate. That would be very... unethical."
  return

label ground_floor_west_back_flora_bonsai_interact:
  "Leave no [flora] behind. That's what they drilled into me at [jo]'s platoon."
  return
