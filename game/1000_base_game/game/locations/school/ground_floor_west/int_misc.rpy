init python:
  class Interactable_school_ground_floor_west_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take"," {i}Wet{/} Yoink","school_ground_floor_west_dollar1_take"])


label school_ground_floor_west_dollar1_take:
  $school_ground_floor_west["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_ground_floor_west_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","school_ground_floor_west_dollar2_take"])


label school_ground_floor_west_dollar2_take:
  $school_ground_floor_west["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_school_ground_floor_west_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?school_ground_floor_west_book_interact"])


label school_ground_floor_west_book_interact:
  $school_ground_floor_west["book_taken"] = True
  "I didn't even know I needed this book..."
  "{i}\"Just Smile and Wave — the successful politician's complete manual.\"{/}"
  $mc.charisma+=1
  return
