
init python:
  class Interactable_school_ground_floor_west_bookshelf(Interactable):

    def title(cls):
      return "Bookshelf"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_stolen.started:
        return  "Oh, look! They have [jo]'s favorite book: \"Lime and Lunichment — the Sour Cook's Cookbook.\""
      else:
        return "Poetry anthologies. Perfect if you're waiting for the [nurse] and aren't suffering enough."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "revenge_done":
            actions.append("?quest_isabelle_dethroning_revenge_done_admin_wing_bookshelf")
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "phone_book":
            actions.append("quest_lindsey_angel_phone_book_school_ground_floor_west_bookshelf")
      if quest.isabelle_stolen.started:
        actions.append("?school_ground_floor_west_bookshelf_interact_two")
      actions.append("?school_ground_floor_west_bookshelf_interact")


label school_ground_floor_west_bookshelf_interact:
  "There's a distinct lack of secret doors in this school. This would make the perfect revolving bookcase."
  return

label school_ground_floor_west_bookshelf_interact_two:
  "This bookcase is known to hold some of the worst titles in the school..."
  "{i}\"Headmistress A-Z — the ultimate guide to running a school or bordello.\"{/}"
  "{i}\"My Pussy Tastes Like Pepelepsi Cola — Lana's Tips on Female Hygiene.\"{/}"
  return
