init python:
  class Interactable_school_ground_floor_west_sofa_right(Interactable):

    def title(cls):
      return "Chair"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "Sitting or standing, just like a toilet. It probably has shit between the cushions too."
      else:
        return "This is one of those chairs that look comfortable from afar, but once you sit on it, you feel cheated."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      if quest.isabelle_stolen.started:
        actions.append("?school_ground_floor_west_chair_interact_two")
      actions.append("?school_ground_floor_west_chair_interact")


label school_ground_floor_west_chair_interact:
  "If adultery had a texture, this is exactly how it would feel."
  return

label school_ground_floor_west_chair_interact_two:
  "Ugly and uncomfortable, but still attracts assholes. Relatable."
  return
