init python:
  class Interactable_school_ground_floor_west_mystery_box(Interactable):

    def title(cls):
      return "Mystery Box"

    def description(cls):
      return "A big box with a hole\non each side."

    def actions(cls,actions):
      actions.append("?school_ground_floor_west_mystery_box_interact")


label school_ground_floor_west_mystery_box_interact:
  "What's this thing doing here?"
  "What even is it?"
  "Reminds me of a magician's box... the one they saw in two."
  return
