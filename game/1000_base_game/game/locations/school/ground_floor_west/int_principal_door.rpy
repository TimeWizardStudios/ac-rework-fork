init python:
  class Interactable_school_ground_floor_west_principal_door(Interactable):

    def title(cls):
      return "Principal's Office"

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)
      else:
        return "[jo]'s room, also known as the principal's office, is one of my least favorite places in the school.\n\nIt's the awkward conflict of interest that gets me."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_wicked":
          if quest.kate_wicked in ("costume","party"):
            actions.append(["go","Enter","?quest_kate_wicked_costume_locked"])
            return
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning in ("trap","revenge","panik"):
            actions.append(["go","Enter","?quest_isabelle_dethroning_trap_locked"])
            return
          elif quest.isabelle_dethroning == "revenge_done":
            actions.append(["go","Enter","?quest_isabelle_dethroning_revenge_done_admin_wing_principal_door"])
            return
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
            return
        elif mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape"):
            actions.append(["go","Enter","?quest_lindsey_angel_keycard_school_ground_floor_west_principal_door"])
            return
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "break_in":
            if quest.kate_moment["locked_and_unbreakable"]:
              actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:kate_moment,break_in|Use What?","quest_kate_moment_break_in"])
            actions.append(["go","Enter","?quest_kate_moment_break_in_principal_door"])
            return
          elif quest.kate_moment in ("text","fun","sleep"):
            actions.append(["go","Enter","?quest_kate_moment_principal_office_return"])
            return
      else:
        if quest.mrsl_table == "letter":
          actions.append(["interact","Knock","?quest_mrsl_table_principal_door_check"])
          return
        if quest.kate_moment == "coming_soon":
          actions.append(["go","Enter","?quest_kate_moment_principal_office_return"])
          return
      actions.append(["go","Enter","?school_ground_floor_west_headmasterdoor_interact"])


label school_ground_floor_west_headmasterdoor_interact:
  "I better not disturb her. Detention looms behind that door."
  return
