init python:
  class Interactable_school_principal_office_principal_desk(Interactable):

    def title(cls):
      return "Principal's Desk"

    def description(cls):
      return "The chair behind the desk looks really comfy, but using it will\nland you in detention."

    def actions(cls,actions):
      actions.append("?school_principal_office_principal_desk_interact")


label school_principal_office_principal_desk_interact:
  "The principal's desk, also known as the power desk."
  "Imposing and meant to intimidate students."
  return
