init python:
  class Interactable_school_principal_office_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      return "Even the door is\nradiating prestige..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_moment":
          if quest.kate_moment == "principal_office":
            actions.append(["go","Admin Wing","?quest_kate_moment_principal_office_door"])
            return
      actions.append(["go","Admin Wing","goto_school_ground_floor_west"])
