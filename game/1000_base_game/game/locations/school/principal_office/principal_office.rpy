init python:
  class Location_school_principal_office(Location):

    default_title = "Principal's Office"

    def scene(self,scene):
      scene.append([(0,0),"school principal_office background"])
      scene.append([(0,0),"school principal_office shelves",("school_principal_office_shelves",0,500)])
      scene.append([(525,315),"school principal_office framed_diploma_bottom"])
      scene.append([(525,162),"school principal_office framed_diploma_top",("school_principal_office_framed_diploma",0,350)])
      scene.append([(721,80),"school principal_office window"])
      scene.append([(1232,157),"school principal_office board",("school_principal_office_board",0,130)])
      scene.append([(1277,287),"school principal_office swimming_medal","school_principal_office_swimming_medal"])
      scene.append([(1388,430),"school principal_office brick","school_principal_office_brick"])
      scene.append([(1607,57),"school principal_office apocalyptic_road_painting",("school_principal_office_apocalyptic_road_painting",0,248)])
      scene.append([(1612,305),"school principal_office framed_quote","school_principal_office_framed_quote"])
      scene.append([(1693,290),"school principal_office flora_wall",("school_principal_office_flora_wall",0,15)])
      scene.append([(1571,478),"school principal_office plant","school_principal_office_plant"])
      scene.append([(1774,81),"school principal_office door",("school_principal_office_door",-20,400)])
      scene.append([(604,398),"school principal_office desk",("school_principal_office_principal_desk",-20,3)])
      scene.append([(696,493),"school principal_office papers","school_principal_office_papers"])
      if school_principal_office["flora_photo_smashed"]:
        scene.append([(828,566),"school principal_office flora_desk_smashed",("school_principal_office_flora_desk_smashed",-5,0)])
      else:
        scene.append([(819,522),"school principal_office flora_desk",("school_principal_office_flora_desk",5,0)])
      scene.append([(966,423),"school principal_office computer",("school_principal_office_computer",55,0)])
      scene.append([(1024,558),"school principal_office cup",("school_principal_office_principal_desk",-87,-157)])
      scene.append([(527,662),"school principal_office chairs"])
      scene.append([(0,0),"#school principal_office overlay"])

    def find_path(self,target_location):
      return "school_principal_office_door",dict(marker_offset=(50,-30))


label goto_school_principal_office:
  call goto_with_black(school_principal_office)
  return
