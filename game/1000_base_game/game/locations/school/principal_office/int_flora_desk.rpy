init python:
  class Interactable_school_principal_office_flora_desk(Interactable):

    def title(cls):
      return "Photo of [flora]"

    def description(cls):
      return "It's hard to find photos of [flora] smiling. That's probably my fault."

    def actions(cls,actions):
      if school_principal_office["flora_photo_interacted"]:
        actions.append("?school_principal_office_flora_desk_interact_two")
      else:
        actions.append("?school_principal_office_flora_desk_interact_one")


label school_principal_office_flora_desk_interact_one:
  menu(side="middle"):
    "Smash":
      play sound "<from 0.3>glass_break"
      $school_principal_office["flora_photo_smashed"] = True
      show location with vpunch
      pause 0.25
      "I should've done that a long time ago..."
      "This favoritism is disgusting. [jo] should love us both equally."
      $mc.lust+=2
      $jo.love-=2
    "Pass":
      "This is a good reminder that I still have a long way to go."
      "Perhaps, if I work hard, I'll earn a place next to her too?"
      $mc.love+=2
  $school_principal_office["flora_photo_interacted"] = True
  return

label school_principal_office_flora_desk_interact_two:
  "[flora] will be my inspiration for success from now on."
  return


init python:
  class Interactable_school_principal_office_flora_desk_smashed(Interactable):

    def title(cls):
      return "Photo of [flora]"

    def description(cls):
      return "Who's smiling now, huh?"

    def actions(cls,actions):
      actions.append("?school_principal_office_flora_desk_smashed_interact")


label school_principal_office_flora_desk_smashed_interact:
  "Shattered like my life. Finally, some vindication."
  return
