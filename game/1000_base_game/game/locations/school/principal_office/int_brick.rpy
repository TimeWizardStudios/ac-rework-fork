init python:
  class Interactable_school_principal_office_brick(Interactable):

    def title(cls):
      return "Glass Case"

    def description(cls):
      return "Supposedly, the last brick that Marilyn the Mason ever laid."

    def actions(cls,actions):
      actions.append("?school_principal_office_brick_interact")


label school_principal_office_brick_interact:
  "Sometimes, life hits you like a ton of bricks."
  "Sometimes, it's just the one."
  return
