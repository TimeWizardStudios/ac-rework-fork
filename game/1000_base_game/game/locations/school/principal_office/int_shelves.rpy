init python:
  class Interactable_school_principal_office_shelves(Interactable):

    def title(cls):
      return "Shelves"

    def description(cls):
      return "The files of every student — past, present, and maybe even future."

    def actions(cls,actions):
      actions.append("?school_principal_office_shelves_interact")


label school_principal_office_shelves_interact:
  "Hmm... I wonder if I could find my file here and change my grades..."
  "...it might be the only way to graduate."
  return
