init python:
  class Interactable_school_principal_office_papers(Interactable):

    def title(cls):
      return "Stack of Papers"

    def description(cls):
      return "This is about how many\nclean slates I need."

    def actions(cls,actions):
      actions.append("?school_principal_office_papers_interact")


label school_principal_office_papers_interact:
  "So, this is where all the paper in the school ends up..."
  "I should've known it was an administrative issue."
  return
