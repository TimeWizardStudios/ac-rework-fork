init python:
  class Interactable_school_principal_office_framed_diploma(Interactable):

    def title(cls):
      return "Framed Diploma"

    def description(cls):
      return "This is probably what earned the school the experimental program.\n\nOur five-star principal."

    def actions(cls,actions):
      actions.append("?school_principal_office_framed_diploma_interact")


label school_principal_office_framed_diploma_interact:
  "[jo]'s degree. She's more proud of it than anything I've ever done."
  "...not that I've done much."
  return
