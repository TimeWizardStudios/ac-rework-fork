init python:
  class Interactable_school_principal_office_board(Interactable):

    def title(cls):
      return "Board"

    def description(cls):
      return "Information of the most\nboring kind."

    def actions(cls,actions):
      actions.append("?school_principal_office_board_interact")


label school_principal_office_board_interact:
  "Oh, the pool will open around Christmas!"
  "I can't wait to see more bikini babes around here..."
  return
