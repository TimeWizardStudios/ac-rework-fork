init python:
  class Interactable_school_principal_office_framed_quote(Interactable):

    def title(cls):
      return "Framed Quote"

    def description(cls):
      return "Easy thing to say. What about all the people that have a gag reflex?"

    def actions(cls,actions):
      actions.append("?school_principal_office_framed_quote_interact")


label school_principal_office_framed_quote_interact:
  "{i}\"There are no secrets to success. It is the result of preparation, hard work, and sucking the right dick.\" — Polin Swovell{/}"
  return
