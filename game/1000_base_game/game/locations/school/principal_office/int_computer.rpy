init python:
  class Interactable_school_principal_office_computer(Interactable):

    def title(cls):
      return "Computer"

    def description(cls):
      return "It's a sinux — slower than the penguin glaciers of Antarctica."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_moment":
          if quest.kate_moment == "principal_office":
            actions.append("quest_kate_moment_principal_office")
      actions.append("?school_principal_office_computer_interact")


label school_principal_office_computer_interact:
  "Let's see here... [jo]'s search history..."
  "..."
  "..."
  "What the hell is this?"
  "Recipes and self-help manuals?"
  "Where's the porn?!"
  return
