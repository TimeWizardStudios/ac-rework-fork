init python:
  class Interactable_school_principal_office_apocalyptic_road_painting(Interactable):

    def title(cls):
      return "Painting"

    def description(cls):
      return "A painting by Tarmac MacArty — [jo]'s favorite road engineer."

    def actions(cls,actions):
      actions.append("?school_principal_office_apocalyptic_road_painting_interact")


label school_principal_office_apocalyptic_road_painting_interact:
  "Streetlamps like wilted steel flowers. Ashes falling like snow."
  return
