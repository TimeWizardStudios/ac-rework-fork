init python:
  class Interactable_school_principal_office_plant(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      return "[jo]'s throat's best friend."

    def actions(cls,actions):
      actions.append("?school_principal_office_plant_interact")


label school_principal_office_plant_interact:
  "Aloe vera."
  "Supposedly, one of the reasons she managed to climb the career ladder so quickly."
  return
