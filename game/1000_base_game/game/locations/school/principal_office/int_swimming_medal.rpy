init python:
  class Interactable_school_principal_office_swimming_medal(Interactable):

    def title(cls):
      return "Swimming Medal"

    def description(cls):
      return "Pure gold. [jo] has a special polishing kit just for this medal."

    def actions(cls,actions):
      actions.append("?school_principal_office_swimming_medal_interact")


label school_principal_office_swimming_medal_interact:
  "Another one of [jo]'s many achievements — she used to be the captain{space=-40}\nof the swim team."
  "As for me... I'm just the captain of failure."
  return
