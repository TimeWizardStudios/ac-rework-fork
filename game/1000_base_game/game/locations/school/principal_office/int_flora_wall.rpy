init python:
  class Interactable_school_principal_office_flora_wall(Interactable):

    def title(cls):
      return "Photo of [flora]"

    def description(cls):
      return "[flora] looks happy, but also smug.\n\nBasically, her resting face when I'm not around."

    def actions(cls,actions):
      actions.append("?school_principal_office_flora_wall_interact")


label school_principal_office_flora_wall_interact:
  "It's pretty obvious who [jo]'s favorite is..."
  "No surprise there. [flora]'s always been a suck-up."
  return
