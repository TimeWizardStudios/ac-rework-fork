init python:
  class Interactable_dress_shop_1800s_dress(Interactable):

    def title(cls):
      return "1800s Dress"

    def actions(cls,actions):
      if "sophisticated_dress" in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_1800s_dress_interact")


label dress_shop_1800s_dress_interact:
  mc "Oh! This could be good for roleplay."
  window hide
  show nurse concerned with Dissolve(.5)
  window auto
  mc "You're just an innocent maiden, ready to come undone..."
  nurse concerned "Um, maybe it's a bit much for a first date, then?"
  mc "Still, it sounds kind of fun."
  if "1800s_dress" not in quest.nurse_aid["dresses"]:
    $nurse.lust+=1
  nurse blush "Oh, my."
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("1800s_dress")
  return
