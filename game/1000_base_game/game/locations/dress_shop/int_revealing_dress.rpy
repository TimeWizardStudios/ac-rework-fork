init python:
  class Interactable_dress_shop_revealing_dress(Interactable):

    def title(cls):
      return "Revealing Dress"

    def actions(cls,actions):
      if "revealing_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_revealing_dress_interact")


label dress_shop_revealing_dress_interact:
  mc "Ah, here we go! I think you should try this one on."
  window hide
  show nurse smile with Dissolve(.5)
  window auto
  nurse smile "It does look rather nice."
  mc "Go ahead, then."
  nurse afraid "Do you mean, um, in front of you...?"
  mc "I do think that's the best way."
  nurse annoyed "O-okay."
  window hide
  hide screen interface_hider
  show dress_shop events dressing_room behind nurse
  show nurse blush
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.75
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  mc "Go ahead. Undress."
  $nurse["outfit_stamp"] = nurse.outfit
  $nurse.outfit = {}
  show black onlayer screens zorder 100
  pause 1.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Damn! She does look great naked."
  "That's just how she's meant to be."
  "With her full figure... big breasts and thick thighs you just want to bury your head between..."
  "Ironically, despite the blush in her cheeks and her shy modesty, she almost seems more confident naked."
  mc "Now, put on the dress, will you?"
  nurse blush "O-of course."
  window hide
  pause 0.25
  $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra"}
  pause 1.0
  $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_revealing_dress", "pendant":"nurse_pendant"}
  pause 0.75
  window auto
  mc "Oh, not bad!"
  mc "I think we should keep looking, though."
  nurse thinking "Good idea..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $nurse.outfit = nurse["outfit_stamp"]
  $quest.nurse_aid["dresses"].add("revealing_dress")
  hide nurse
  pause 1.0
  hide dress_shop events dressing_room
  # hide nurse
  hide black onlayer screens
  with Dissolve(.5)
  # $quest.nurse_aid["dresses"].add("revealing_dress")
  return
