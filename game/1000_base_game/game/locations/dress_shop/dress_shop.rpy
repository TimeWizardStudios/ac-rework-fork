init python:
  class Location_dress_shop(Location):

    default_title = "Dress Shop"

    def scene(self,scene):
      scene.append([(0,0),"dress_shop background"])
      scene.append([(54,0),"dress_shop misc_dresses"])
      scene.append([(514,306),"dress_shop hangers_back"])
      scene.append([(722,171),"dress_shop mirror"])
      scene.append([(842,669),"dress_shop ottoman"])
      scene.append([(1095,177),"dress_shop dressing_room_sign"])
      scene.append([(1211,169),"dress_shop plant"])
      scene.append([(1224,441),"dress_shop counter"])
      scene.append([(1379,3),"dress_shop lamp"])

      ## Summer Dress ##
      scene.append([(896,96),"dress_shop summer_dress",("dress_shop_summer_dress",0,340)])

      scene.append([(325,323),"dress_shop 1800s_dress",("dress_shop_1800s_dress",-45,0)])
      if "sophisticated_dress" not in quest.nurse_aid["dresses"]:
        scene.append([(229,316),"dress_shop sophisticated_dress","dress_shop_sophisticated_dress"])

      ## Ball Gown ##
      scene.append([(56,312),"dress_shop ball_gown",("dress_shop_ball_gown",-70,0)])

      ## Slutty Dress ##
      scene.append([(1188,394),"dress_shop slutty_dress",("dress_shop_slutty_dress",5,0)])

      ## Revealing Dress ##
      if "revealing_dress" not in quest.nurse_aid["dresses"]:
        scene.append([(821,44),"dress_shop revealing_dress",("dress_shop_revealing_dress",0,380)])

      scene.append([(839,337),"dress_shop shirtdress","dress_shop_shirtdress"])

      ## Amish Dress ##
      scene.append([(806,335),"dress_shop amish_dress",("dress_shop_amish_dress",-20,0)])

      ## Wedding Dress ##
      scene.append([(876,204),"dress_shop wedding_dress",("dress_shop_wedding_dress",0,30)])

      ## Sophisticated Dress ##
      # scene.append([(229,316),"dress_shop sophisticated_dress","dress_shop_sophisticated_dress"])

      ## Shirtdress ##
      # scene.append([(839,337),"dress_shop shirtdress","dress_shop_shirtdress"])

      ## 1800s Dress ##
      # scene.append([(325,323),"dress_shop 1800s_dress",("dress_shop_1800s_dress",-45,0)])

      ## Nice Dress ##
      if "nice_dress" not in quest.nurse_aid["dresses"]:
        scene.append([(1650,295),"dress_shop nice_dress",("dress_shop_nice_dress",20,0)])

      scene.append([(1754,270),"dress_shop hanger_front"])

      ## Nurse ##
      if ((not dress_shop["exclusive"] or "nurse" in dress_shop["exclusive"])
      and nurse.at("dress_shop","standing")
      and not nurse.talking):
        scene.append([(1293,330),"dress_shop nurse","nurse"])

      scene.append([(0,0),"#dress_shop overlay"])


label goto_dress_shop:
  call goto_with_black(dress_shop)
  return
