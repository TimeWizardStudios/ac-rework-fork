init python:
  class Interactable_dress_shop_ball_gown(Interactable):

    def title(cls):
      return "Ball Gown"

    def actions(cls,actions):
      if "revealing_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_ball_gown_interact")


label dress_shop_ball_gown_interact:
  "It might be a little too formal, but who knows?"
  mc "What about this one?"
  window hide
  show nurse concerned with Dissolve(.5)
  window auto
  nurse concerned "Um, isn't it a little much?"
  mc "I think it's nice and modest."
  if "ball_gown" not in quest.nurse_aid["dresses"]:
    $nurse.lust-=1
  nurse thinking "Maybe something else..."
  mc "Very well."
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("ball_gown")
  return
