init python:
  class Interactable_dress_shop_amish_dress(Interactable):

    def title(cls):
      return "Amish Dress"

    def actions(cls,actions):
      if "revealing_dress" in quest.nurse_aid["dresses"] and "sophisticated_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_amish_dress_interact")


label dress_shop_amish_dress_interact:
  # "Heh. The [nurse]'s personality would complement this dress in a way."
  "Heh. The [nurse]'s personality would complement this dress in a way.{space=-5}"
  "So shy. So quiet."
  "But I know inside her is an exhibitionist ready for a good time."
  window hide
  show nurse smile with Dissolve(.5)
  window auto
  nurse smile "What about this one?"
  mc "Absolutely not. You need to show off your figure."
  if "amish_dress" not in quest.nurse_aid["dresses"]:
    $nurse.lust+=1
  nurse afraid "Goodness gracious!"
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("amish_dress")
  return
