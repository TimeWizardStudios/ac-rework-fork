init python:
  class Interactable_dress_shop_shirtdress(Interactable):

    def title(cls):
      return "Shirtdress"

    def actions(cls,actions):
      if "sophisticated_dress" in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_shirtdress_interact")


label dress_shop_shirtdress_interact:
  mc "What about this one?"
  window hide
  show nurse annoyed with Dissolve(.5)
  window auto
  nurse annoyed "Err, maybe it's too casual?"
  mc "It could be... unless you just want a casual fuck?"
  if "shirtdress" not in quest.nurse_aid["dresses"]:
    $nurse.love-=1
  nurse afraid "Oh, dear! Not at all!"
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("shirtdress")
  return
