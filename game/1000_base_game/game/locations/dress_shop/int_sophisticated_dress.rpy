init python:
  class Interactable_dress_shop_sophisticated_dress(Interactable):

    def title(cls):
      return "Sophisticated Dress"

    def actions(cls,actions):
      if "revealing_dress" in quest.nurse_aid["dresses"] and "sophisticated_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_sophisticated_dress_interact")


label dress_shop_sophisticated_dress_interact:
  mc "Here we go! This one looks nice."
  window hide
  show nurse blush with Dissolve(.5)
  window auto
  nurse blush "Oh, yes! I love the color!"
  mc "You can try it on, then."
  nurse blush "Okay."
  window hide
  show dress_shop events dressing_room behind nurse
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $nurse["outfit_stamp"] = nurse.outfit
  $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_sophisticated_dress", "pendant":"nurse_pendant"}
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  nurse blush "What do you think?"
  mc "Hmm..."
  mc "Maybe give me a little spin so that I can see the back?"
  nurse thinking "Err, okay..."
  window hide
  pause 0.25
  show nurse thinking flip with Dissolve(.5)
  pause 0.5
  show nurse thinking with Dissolve(.5)
  pause 0.25
  window auto
  mc "It {i}is{/} nice, but let's try on one more, shall we?"
  nurse thinking "Good idea."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $nurse.outfit = nurse["outfit_stamp"]
  $quest.nurse_aid["dresses"].add("sophisticated_dress")
  hide nurse
  pause 1.0
  hide dress_shop events dressing_room
  # hide nurse
  hide black onlayer screens
  with Dissolve(.5)
  # $quest.nurse_aid["dresses"].add("sophisticated_dress")
  return
