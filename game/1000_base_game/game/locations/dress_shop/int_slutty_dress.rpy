init python:
  class Interactable_dress_shop_slutty_dress(Interactable):

    def title(cls):
      return "Slutty Dress"

    def actions(cls,actions):
      if "revealing_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_slutty_dress_interact")


label dress_shop_slutty_dress_interact:
  mc "Now, this could be really hot, don't you think?"
  window hide
  show nurse surprised with Dissolve(.5)
  window auto
  nurse surprised "F-for a first date?! Are you sure?"
  show nurse surprised at move_to(.75)
  menu(side="left"):
    extend ""
    # "\"Seriously? I'm trying to help you, and you question me?\"":
    "\"Seriously? I'm trying to help\nyou, and you question me?\"":
      show nurse surprised at move_to(.5)
      mc "Seriously? I'm trying to help you, and you question me?"
      nurse afraid "Sorry! I-I didn't mean to sound ungrateful!"
      # mc "Well, you did. So, please just let me make the decisions here, will you?"
      mc "Well, you did. So, please just let me make the decisions here, will you?{space=-50}"
      if "slutty_dress" not in quest.nurse_aid["dresses"]:
        $nurse.lust+=1
      nurse annoyed "O-of course. I do appreciate it."
    # "\"That's true. You don't want to look too easy, do you?\"":
    "\"That's true. You don't want\nto look too easy, do you?\"":
      show nurse surprised at move_to(.5)
      mc "That's true. You don't want to look too easy, do you?"
      nurse annoyed "Oh, dear..."
      # mc "Err, I just meant you deserve respect from anyone who takes you out!"
      mc "Err, I just meant you deserve respect from anyone who takes you out!{space=-40}"
      if "slutty_dress" not in quest.nurse_aid["dresses"]:
        $nurse.love+=1
      nurse smile "Aw, thank you!"
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("slutty_dress")
  return
