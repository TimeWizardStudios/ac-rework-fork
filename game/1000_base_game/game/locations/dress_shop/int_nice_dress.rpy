init python:
  class Interactable_dress_shop_nice_dress(Interactable):

    def title(cls):
      return "Nice Dress"

    def actions(cls,actions):
      if "sophisticated_dress" in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_nice_dress_interact")


label dress_shop_nice_dress_interact:
  show nurse smile with Dissolve(.5)
  nurse smile "This one is kind of nice..."
  mc "Indeed. Do you want to try it on?"
  nurse neutral "Oh, um. If you don't mind."
  mc "Go for it!"
  window hide
  show dress_shop events dressing_room behind nurse
  show nurse blush
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $nurse["outfit_stamp"] = nurse.outfit
  $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_nice_dress", "pendant":"nurse_pendant"}
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  "I have to admit, the [nurse] does look really pretty when she's not just wearing her scrubs all the time."
  mc "You clean up nicely."
  nurse blush "Aw, thank you!"
  mc "Now, we just have to decide which one to buy."
  show nurse blush at move_to(.25)
  menu(side="right"):
    extend ""
    "Choose for her":
      show nurse blush at move_to(.5)
      mc "And my favorite one is..."
      show nurse blush at move_to(.75)
      menu(side="left"):
        extend ""
        "\"...dress one.\"":
          show nurse blush at move_to(.5)
          extend " dress one."
          nurse thinking "Oh. Are you sure?"
          mc "I saw you try them all on, and I like that one the best."
          mc "So, go with it."
          $nurse.lust+=1
          nurse blush "Um, okay."
          mc "Or do you not like it?"
          $quest.nurse_aid["dress_chosen"] = "revealing_dress"
        "\"...dress two.\"":
          show nurse blush at move_to(.5)
          extend " dress two."
          mc "It makes you look like a princess, and you deserve a prince."
          $nurse.love+=1
          nurse blush "Aw! That's one of the nicest things I have heard!"
          nurse blush "I love it, [mc]!"
          mc "So, do you like it?"
          $quest.nurse_aid["dress_chosen"] = "sophisticated_dress"
        "\"...dress three.\"":
          show nurse blush at move_to(.5)
          extend " dress three."
          mc "It's a classic, and great for a first date."
          nurse blush "I agree wholeheartedly!"
          mc "So, do you like it?"
          $quest.nurse_aid["dress_chosen"] = "nice_dress"
      show nurse blush_workaround with dissolve2
      nurse blush_workaround "Oh, I most certainly do."
    "Let her choose":
      show nurse blush at move_to(.5)
      mc "Which one did you like the best?"
      nurse thinking "It's such a tough choice, but, um..."
      nurse blush "...I think I will go with this one."
      mc "A good choice! I like it, too."
      $quest.nurse_aid["dress_chosen"] = "nice_dress"
  if renpy.showing("nurse blush_workaround"):
    show nurse blush_workaround at move_to(.25)
  else:
    show nurse blush at move_to(.75)
  menu(side=("right" if renpy.showing("nurse blush_workaround") else "left")):
    extend ""
    "?mc.money>=249@[mc.money]/249|{image=ui hud icon_money}|\"Let me buy it for you, then.\"" if renpy.showing("nurse blush_workaround"):
      show nurse blush at move_to(.5)
      mc "Let me buy it for you, then."
      label dress_shop_nice_dress_interact_buy_it:
        nurse afraid "Oh, no! You couldn't possibly!"
        mc "Yes, I can, and I will."
        mc "This was my idea, and I want to treat you to something nice."
        nurse smile "[mc], you have been so much sweeter than I ever knew..."
        nurse smile "No one has ever been so kind and giving to me before."
        mc "Well, now you know how people feel to be around you."
        nurse smile "..."
        $nurse.love+=5
        nurse smile "Thank you..."
        mc "You're welcome."
        mc "Now, let's get out of here, shall we?"
        window hide
        show nurse smile at disappear_to_right
        pause 0.5
        show black onlayer screens zorder 100 with Dissolve(.5)
        $mc["focus"] = ""
        $game.location = "marina"
        if "quest_nurse_aid_shopping" in game.events_queue:
          $game.events_queue.remove("quest_nurse_aid_shopping")
        pause 1.0
        $nurse.outfit = nurse["outfit_stamp"]
        show nurse smile at appear_from_left
        pause 0.0
        hide dress_shop events dressing_room
        hide black onlayer screens
        with Dissolve(.5)
        $mc.money-=249
        window auto
    "?mc.money>=249@[mc.money]/249|{image=ui hud icon_money}|\"Let me buy it for you.\"" if not renpy.showing("nurse blush_workaround"):
      show nurse blush at move_to(.5)
      mc "Let me buy it for you."
      jump dress_shop_nice_dress_interact_buy_it
    # "\"I'll wait outside while you checkout, okay?\"":
    "\"I'll wait outside while\nyou checkout, okay?\"":
      show nurse blush at move_to(.5)
      mc "I'll wait outside while you checkout, okay?"
      nurse smile "Okay, I'll be there in a bit!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $nurse.outfit = nurse["outfit_stamp"]
      $mc["focus"] = ""
      $game.location = "marina"
      if "quest_nurse_aid_shopping" in game.events_queue:
        $game.events_queue.remove("quest_nurse_aid_shopping")
      pause 1.0
      hide dress_shop events dressing_room
      hide nurse
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "Man, what a day this has been."
      "It was kind of tiring waiting around while she looked at all those dresses... but at least the [nurse] seemed to enjoy herself."
      window hide
      show nurse smile at appear_from_left
      pause 0.5
      window auto
      nurse smile "All finished!"
      mc "Very nice!"
  $quest.nurse_aid["dresses"].add("nice_dress")
  jump quest_nurse_aid_shopping_upon_leaving
