init python:
  class Interactable_dress_shop_summer_dress(Interactable):

    def title(cls):
      return "Summer Dress"

    def actions(cls,actions):
      if "revealing_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_summer_dress_interact")


label dress_shop_summer_dress_interact:
  mc "What about this one?"
  window hide
  show nurse neutral with Dissolve(.5)
  window auto
  nurse neutral "Err, isn't it a bit cold for that?"
  show nurse neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You'll wear what I say you wear.\"":
      show nurse neutral at move_to(.5)
      mc "You'll wear what I say you wear."
      if "summer_dress" not in quest.nurse_aid["dresses"]:
        $nurse.lust+=1
      nurse afraid "Oh! Y-yes, of course!"
    "\"Right. Good point.\"":
      show nurse neutral at move_to(.5)
      mc "Right. Good point."
      mc "We can't have you freezing to death, can we?"
      if "summer_dress" not in quest.nurse_aid["dresses"]:
        $nurse.love+=1
      nurse smile "Thank you..."
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("summer_dress")
  return
