init python:
  class Interactable_dress_shop_wedding_dress(Interactable):

    def title(cls):
      return "Wedding Dress"

    def actions(cls,actions):
      if "revealing_dress" in quest.nurse_aid["dresses"] and "sophisticated_dress" not in quest.nurse_aid["dresses"]:
        actions.append("?dress_shop_wedding_dress_interact")


label dress_shop_wedding_dress_interact:
  show nurse blush with Dissolve(.5)
  nurse blush "I used to dream about my wedding when I was a girl..."
  "Somehow, I doubt it involved someone like [kate]."
  mc "Do you still want to get married?"
  nurse thinking "I think so? I would like to have someone to love and take care of."
  "She doesn't even think about being loved herself..."
  mc "Well, doesn't that sounds nice?"
  mc "You deserve the same, [nurse]."
  if "wedding_dress" not in quest.nurse_aid["dresses"]:
    $nurse.love+=1
  nurse blush "Thank you, [mc]."
  window hide
  hide nurse with Dissolve(.5)
  $quest.nurse_aid["dresses"].add("wedding_dress")
  return
