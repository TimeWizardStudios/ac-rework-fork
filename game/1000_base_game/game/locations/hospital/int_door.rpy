init python:
  class Interactable_hospital_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      return "This door seems a little unhinged. Maybe that's where the psychiatrist is."

    def actions(cls,actions):
      actions.append("?hospital_door_interact")


label hospital_door_interact:
  "I wonder what they keep back there?"
  "Boring files, or spare organs?"
  return
