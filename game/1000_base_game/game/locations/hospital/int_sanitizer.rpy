init python:
  class Interactable_hospital_sanitizer(Interactable):

    def title(cls):
      return "Sanitizer"

    def description(cls):
      return "One pump and done...\nbasically my motto."

    def actions(cls,actions):
      actions.append("?hospital_sanitizer_interact")


label hospital_sanitizer_interact:
  "When in disease ridden Rome, practice good hygiene."
  return
