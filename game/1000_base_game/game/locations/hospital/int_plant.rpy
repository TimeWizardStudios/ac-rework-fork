init python:
  class Interactable_hospital_plant(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      return "Paging Doctor Plant...\n\nIt's time to shrub in."

    def actions(cls,actions):
      actions.append("?hospital_plant_interact")


label hospital_plant_interact:
  "Hmm... looking a little too green, aren't you?"
  "Good thing you're in the right place."
  return
