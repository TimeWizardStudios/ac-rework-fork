init python:
  class Interactable_hospital_left_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
      return "Huh! So, that's what a patella is.\nI thought it was a creamy\nhazelnut spread."

    def actions(cls,actions):
      actions.append("?hospital_left_poster_interact")


label hospital_left_poster_interact:
  "Hospitals really know how to make anatomy pictures unsexy..."
  "Show me where the clitoris is!"
  return
