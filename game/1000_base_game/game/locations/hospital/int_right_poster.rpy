init python:
  class Interactable_hospital_right_poster(Interactable):

    def title(cls):
      return "Poster"

    def description(cls):
      return "{i}\"Employees must wash hands.\"{/}"

    def actions(cls,actions):
      actions.append("?hospital_right_poster_interact")


label hospital_right_poster_interact:
  "Is this showing me where to place my hands for maximum wetness?{space=-10}"
  return
