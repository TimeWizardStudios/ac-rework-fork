init python:
  class Interactable_hospital_exit_arrow(Interactable):

    def title(cls):
      return "Newfall Marina"

    def description(cls):
      return "That way lies fresh sea air and sunshine. It makes me sick."

    def actions(cls,actions):
      actions.append(["go","Newfall Marina","goto_marina"])
