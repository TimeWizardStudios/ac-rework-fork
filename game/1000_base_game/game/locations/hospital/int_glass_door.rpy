init python:
  class Interactable_hospital_glass_door(Interactable):

    def title(cls):
      return "Hospital Ward"

    def description(cls):
      return "Not nearly as thrilling back there as one of [jo]'s hospital soaps would have you believe..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_angel":
          if quest.lindsey_angel == "hospital":
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:lindsey_angel,hospital_ward|Use What?","quest_lindsey_angel_hospital_hospital_glass_door"])
      actions.append("?hospital_glass_door_interact")


label hospital_glass_door_interact:
  "This door is locked up tight."
  "No authorization without a key card or the right personnel."
  return
