init python:
  class Location_hospital(Location):

    default_title = "Newfall Hospital"

    def scene(self,scene):
      scene.append([(0,0),"hospital background"])
      scene.append([(911,265),"hospital left_poster","hospital_left_poster"])
      scene.append([(1074,381),"hospital door","hospital_door"])
      scene.append([(1248,387),"hospital right_poster","hospital_right_poster"])
      scene.append([(1417,353),"hospital cabinets"])
      scene.append([(283,41),"hospital glass_door",("hospital_glass_door",20,400)])
      if quest.fall_in_newfall.finished and (quest.lindsey_angel.finished and not quest.lindsey_angel["finished_this_week"]) and quest.maya_witch.finished:
        scene.append([(284,254),"hospital guard"])
      scene.append([(1265,552),"hospital sanitizer","hospital_sanitizer"])
      scene.append([(1332,550),"hospital desk_chairs"])
      scene.append([(1364,494),"hospital receptionists"])
      scene.append([(1452,485),"hospital pcs"])
      scene.append([(-14,642),"hospital left_plant"])
      scene.append([(865,511),"hospital middle_plant","hospital_plant"])
      scene.append([(0,0),"#hospital overlay"])
      scene.append([(907,924),"hospital exit_arrow","hospital_exit_arrow"])

    def find_path(self,target_location):
      return "hospital_exit_arrow", dict(marker_offset=(15,10))


label goto_hospital:
  call goto_with_black(hospital)
  return
