init python:
  class Interactable_home_bedroom_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif home_bedroom["clean"]:
        return "Scrubbed the handle clean.\nYou could eat off it now.\n\nI might use it as a spoon later if all the cutlery is dirty..."
      elif quest.lindsey_piano >= "listen_bedroom":
        return "The poster of the elf to the left? Garteradriel from Lord of the Strings — waifu #8."
      elif quest.kate_blowjob_dream >= "alarm":
        return "Those scratch marks. Made by my inner beast. My true persona. Danger McDragon."
      elif quest.kate_blowjob_dream >= "open_door":
        return "Nothing interesting about this door, except those scratch marks. The truth about them lies shrouded in mystery."
      elif quest.kate_blowjob_dream >= "courage_badge":
        return "Definitely not leaving my room anymore today. This has been a pretty great day. No need to push my luck."
      elif quest.smash_or_pass.started:
        return "My trusty door. The faithful barrier between the safety of my room and the perils of the outside."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "open_door":
            actions.append(["interact","Open Door","home_bedroom_door_interact_kate_blowjob_dream_open_door"])
            return
          elif quest.kate_blowjob_dream == "get_dressed":
            actions.append(["go","Hallway","?home_bedroom_door_interact_kate_blowjob_dream_get_dressed"])
            return
          elif quest.kate_blowjob_dream == "school":
            actions.append(["go","Hallway","goto_home_hall"])
          elif quest.kate_blowjob_dream == "alarm":
            actions.append(["go","Hallway","?home_bedroom_door_interact_kate_blowjob_dream_alarm"])
            return
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "water" and not quest.flora_bonsai['water_prompt']:
            actions.append(["go","Hallway","quest_flora_bonsai_water_prompt"])
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "clothes":
            actions.append(["go","Hallway","?home_bedroom_door_jacklyn_statement_clothes"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Hallway","goto_home_hall"])
        elif mc["focus"] == "isabelle_locker":
          if quest.isabelle_locker in ("bedroom","email","wait"):
            actions.append(["go","Hallway","?quest_isabelle_locker_home_bedroom_door"])
            return
        elif mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "listen_bedroom":
            if quest.lindsey_piano["lindsey_arrived"]:
              actions.append(["go","Hallway","?quest_lindsey_piano_listen_bedroom_door_leave"])
            else:
              actions.append(["interact","Open Door","quest_lindsey_piano_listen_bedroom_door"])
            return
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "fun" and game.hour == 19:
            actions.append(["go","Hallway","?quest_kate_moment_fun_door"])
            return
        elif mc["focus"] == "flora_walk":
          if quest.flora_walk == "xcube":
            actions.append(["go","Hallway","?quest_flora_walk_xcube_door"])
            return
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "suit":
            actions.append(["go","Hallway","?quest_jacklyn_town_suit_door"])
            return
          elif quest.jacklyn_town == "hairdo":
            actions.append(["go","Bathroom","goto_home_bathroom"])
      else:
        if quest.smash_or_pass.in_progress and home_bedroom["alarm"] == "beeping":
          actions.append(["go","Hallway","?home_bedroom_door_interact_smash_or_pass_beeping"])
          return
        if quest.dress_to_the_nine.in_progress:
          actions.append(["go","Hallway","?home_bedroom_door_interact_dress_to_the_nine"])
          return
        elif not quest.dress_to_the_nine.started and quest.smash_or_pass.finished:
          actions.append(["go","Bathroom","goto_home_bathroom"])
        if quest.back_to_school_special.in_progress:
          actions.append(["go","Kitchen","home_bedroom_door_interact_back_to_school_special"])
#       if quest.kate_blowjob_dream == "get_dressed":
#         actions.append(["go","Hallway","?home_bedroom_door_interact_kate_blowjob_dream_courage_badge"])
        if quest.kate_desire == "dress":
          actions.append(["go","Hallway","?home_bedroom_door_kate_desire_clothes"])
          return
        if quest.maxine_hook == "wake":
          actions.append(["go","Hallway","?quest_maxine_hook_wake_home_bedroom_door"])
        if quest.jacklyn_sweets in ("bedroom","text"):
          actions.append(["go","Hallway","?quest_jacklyn_sweets_bedroom_door"])
          return
        if quest.jo_washed.finished and not quest.fall_in_newfall.started:
          actions.append(["go","Hallway","?quest_fall_in_newfall_not_started_home_bedroom_door"])
          return
        if quest.fall_in_newfall == "break_over":
          actions.append(["go","Hallway","quest_fall_in_newfall_break_over_home_bedroom_door"])
        # elif game.hour in (0,1,2,3,4,5,6):
          # actions.append("?home_bedroom_door_interact_after_hours")
      actions.append(["go","Hallway","goto_home_hall"])


label home_bedroom_door_kate_desire_clothes:
  "If only [kate]'s plan was to wrestle me naked, then I might have a chance. Alas."
  return

label home_bedroom_door_interact_after_hours:
  ""#tbd, some witty comment about leaving the room being a bad idea
  return

label home_bedroom_door_interact_smash_or_pass_beeping:
  "The alarm will wake [jo] if I don't turn it off first."
  return

label home_bedroom_door_interact_dress_to_the_nine:
  "The only thing that would make this nightmare complete is going to school in my underwear. Better get dressed."
  return

label home_bedroom_door_interact_back_to_school_special:
  "This could be the start of something new. A total makeover. What do you call that?"
  $quest.back_to_school_special.advance("talk_to_jo")
  play sound "bedroom_door"
  jump goto_home_kitchen
  return

label home_bedroom_door_interact_kate_blowjob_dream_courage_badge:
  "Knock knock."
  "Who's there?"
  "No one's ever there."
  return

label home_bedroom_door_interact_kate_blowjob_dream_open_door:
  $flora.unequip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  $flora.unequip("flora_hat")
  show flora annoyed with Dissolve(.5)
  flora annoyed "Idiot."
  mc "What did I do?"
  flora annoyed "Are you serious? School started an hour ago!"
  "Oh, god... she's right. How the hell did this happen?"
  mc "Why aren't you at school, then?"
  flora angry "That's hardly any of your business, but if you must know..."
  flora angry "I get to sleep in a couple of days a week because I'm at my internship in the evenings."
  mc "Uh, right. Forgot about that."
  flora eyeroll "Seriously! This is the only time I get to play on your xCube!"
  flora eyeroll "If you're not out in five minutes, I'm telling [jo] you're already skipping classes."
  mc "Why didn't you wake me sooner?"
  flora annoyed "It's not my job to get you to school!"
  flora annoyed "I'm done with my morning routine and I want to play video games!"
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'm sorry for oversleeping, [flora]. I'll get dressed as soon as I can.\"":
      show flora annoyed at move_to(.5)
      mc "I'm sorry for oversleeping, [flora]. I'll get dressed as soon as I can."
#     $flora.love+=1
      $game.notify("flora contact_icon","{color=#090}+{/color}|{image=stats love}",5.0) ## Fake stat gain since this interaction is just a dream
      flora afraid "That's... unusually mature of you."
      flora confident "Can I play on your save, as well?"
      mc "Out of the question."
      flora annoyed  "Fine. Just making sure you weren't replaced by an imposter."
    "\"Stop being a crybaby. The new schedule allows me to sleep in, as well.\"":
      show flora annoyed at move_to(.5)
      mc "Stop being a crybaby. The new schedule allows me to sleep in, as well."
#     $flora.lust+=1
      $game.notify("flora contact_icon","{color=#090}+{/color}|{image=stats lust}",5.0) ## Fake stat gain since this interaction is just a dream
      flora concerned "No, it allows students who are ahead to sleep in."
      flora concerned "You're not ahead in any way, shape, or form. Four more minutes, then I'm calling [jo]."
      mc "Ugh, fine."
  "Why does she have to be so annoying?"
  "God fucking damn it. Why don't I ever get to sleep?"
  $quest.kate_blowjob_dream.advance("get_dressed")
  hide flora with Dissolve(.5)
  if quest.flora_handcuffs > "package":
    $flora.equip("flora_skirt")
  else:
    $flora.equip("flora_pants")
  $flora.equip("flora_shirt")
  return

label home_bedroom_door_interact_kate_blowjob_dream_get_dressed:
  "Clothes! I need to put on some clothes!"
  return

label home_bedroom_door_interact_kate_blowjob_dream_alarm:
  "That infernal noise needs to die first. Die."
  return

label home_bedroom_door_jacklyn_statement_clothes:
  "I can't go downstairs naked. [jacklyn] might get the wrong idea."
  "...or the right idea, depending on how you look at it."
  return
