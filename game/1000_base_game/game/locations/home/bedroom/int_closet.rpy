init python:
  class Interactable_home_bedroom_closet(Interactable):

    def title(cls):
      return "Closet"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.kate_blowjob_dream.in_progress:
        if quest.kate_blowjob_dream >= "alarm":
          return "Mismatched socks. Shirt on backward. Random stains on the pants. Loseria's Secret Fashion show."
        elif quest.kate_blowjob_dream >= "get_dressed":
          return "The road to hell is paved with bad fashion."
        elif quest.kate_blowjob_dream >= "courage_badge":
          return "Lots of skeletons in here. Too many to ever clean out. Coexisting with my dead past is the only way."
      else:
        if home_bedroom["clean"]:
          return "Sad to see the posters go, but the room looks more mature now."
        elif quest.lindsey_piano >= "listen_bedroom":
          return "Ah, this poster! Richigu from Starlight's Heaven — waifu #14."
        elif quest.isabelle_stolen.started:
          return "A limited space that induces claustrophobia and a will to escape. A walk-out closet."
        elif quest.back_to_school_special.started:
          return "Two shirts, one sock."
        elif quest.smash_or_pass.started:
          return "Shirts, pants, socks, and boxers. Dress to kill? Eh, it's probably easier for the funeral home if I arrive naked."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream in ("open_door","school"):
            actions.append("kate_blowjob_dream_random_interact")
          elif quest.kate_blowjob_dream == "get_dressed":
            actions.append("home_bedroom_closet_interact_kate_blowjob_dream_get_dressed")
          elif quest.kate_blowjob_dream == "alarm":
            actions.append("?home_bedroom_closet_interact_kate_blowjob_alarm")
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "clothes":
            actions.append("home_bedroom_closet_jacklyn_statement_clothes")
          elif quest.jacklyn_statement == "intruder":
            actions.append("home_bedroom_closet_jacklyn_statement_intruder")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "listen_bedroom":
            if not (quest.lindsey_piano["outfit_chosen"] or quest.lindsey_piano["lindsey_arrived"]):
              actions.append("quest_lindsey_piano_listen_bedroom_closet_first_time")
            elif home_bedroom["clean"]:
              actions.append("?quest_lindsey_piano_listen_bedroom_closet_clean")
              return
            else:
              actions.append("?quest_lindsey_piano_listen_bedroom_closet_dirty")
              return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.dress_to_the_nine.in_progress:
          actions.append("home_bedroom_closet_interact_dress_to_the_nine")
        if quest.back_to_school_special.in_progress:
          actions.append("?home_bedroom_closet_interact_back_to_school_special")
        if quest.isabelle_stolen == "askmaxine" and not mc.owned_item(("piece_of_paper","maxine_letter")):
          actions.append("isabelle_quest_stolen_maxine_paper_closet")
        if quest.kate_desire == "dress":
          actions.append("home_bedroom_closet_kate_fate_dress")
        if quest.jacklyn_sweets in ("bedroom","text"):
          actions.append("?quest_jacklyn_sweets_bedroom_closet")
      ## Flavour interactions
      if quest.isabelle_stolen.started:
        actions.append("?home_bedroom_closet_interact_isabelle_stolen")
      if quest.kate_blowjob_dream.finished:
        actions.append("?home_bedroom_closet_interact_kate_blowjob_dream_courage_badge")
      actions.append("?home_bedroom_closet_interact_smash_or_pass")

label home_bedroom_closet_kate_fate_dress:
  "Okay, time to pick my outfit for this endeavor..."
  menu(side="middle"):
    extend ""
    "Shorts and like eight sweatbands":
      $mc.strength+=1
      "The sweatband is like the Swiss army knife of sports."
      "You never know when you might need one."
    "Shades and swagger":
      $mc.charisma+=1
      "Dress to undress. Yeah, baby!"
    "Doesn't matter as long as the calculator and encyclopedia are in my pockets":
      $mc.intellect+=1
      "Brains always beat brawn."
      "It's as simple as rock paper scissors."
  "All right. Time to meet my fate."
  $quest.kate_desire.advance("gym")
  return

label home_bedroom_closet_interact_dress_to_the_nine:
  "Last time, my shirt was on backward and had a mustard stain."
  "Wouldn't hurt to put some more thought into it this time."
  menu(side="left"):
    extend ""
    "Wear the striped vest to display my fashion sense":
      $mc.charisma+=1
      $mc["first_choice"] = "striped vest"
      "Putting it on and pulling it off are two completely different skills when it comes to clothes."
      "The risk of getting laughed at always stopped me before, but it's not like playing it safe ever did me any favors."
    "Wear the tank top to show off my 'ceps":
      $mc.strength+=1
      $mc["first_choice"] = "tank top"
      "Not much to show, but maybe I'll get a desperately needed tan."
    "Wear the hoodie — if I want to show off, I'll just use my brain":
      $mc.intellect+=1
      $mc["first_choice"] = "hoodie"
      "Nothing special. Just a comfy outfit that'll help against first-day-stress."
  $quest.dress_to_the_nine.finish()
  $quest.back_to_school_special.start()
  return

label home_bedroom_closet_interact_back_to_school_special:
  "There's no time to second-guess my outfit now. The deed is done."
  return

label home_bedroom_closet_interact_kate_blowjob_dream_get_dressed:
  "Okay, quickly. What would be a good outfit today? There aren't many good options."
  menu(side="middle"):
    extend ""
    "Wear a shirt and the skirt\nyou stole from [flora]":
#     $isabelle.lust+=1
      $game.notify("isabelle contact_icon","{color=#090}+{/color}|{image=stats lust}",5.0) ## Fake stat gain since this interaction is just a dream
      "If the Scots can do it, so can I!"
      "Damn, it's breezy down there..."
      $quest.kate_blowjob_dream["dress_choice"] = "skirt"
    "Wear the kimono":
      "It's time to show the world what true sophistication looks like."
#     $mc.intellect+=1
      $game.notify(None,"{color=#090}+{/color}|{image=stats int}",5.0) ## Fake stat gain since this interaction is just a dream
      "My bedroom is a dojo of pleasure, and soon the school will be, too!"
      "Totally not a weeb, by the way."
      $quest.kate_blowjob_dream["dress_choice"] = "kimono"
    "Wear nothing":
      "All the decent options are dirty and I need to get going."
#     $mc.charisma+=1
      $game.notify(None,"{color=#090}+{/color}|{image=stats cha}",5.0) ## Fake stat gain since this interaction is just a dream
      "Besides, nothing says confidence like a straight up pair of balls. Let's just pray it doesn't get cold."
      $quest.kate_blowjob_dream["dress_choice"] = "nothing"
  $quest.kate_blowjob_dream.advance("school")
  $school_gym["exclusive"] = "kate"
  return

label home_bedroom_closet_interact_kate_blowjob_alarm:
  "It's probably fine to go naked today. It's not like anyone would care."
  return

label isabelle_quest_stolen_maxine_paper_closet:
  "Okay, let's see what's inside here..."
  "..."
  "A broken fidget spinner... a bottle with some unknown liquid... seven different types of unused body spray — thanks for the birthday gifts, [jo]..."
  "A crusty old sock... those dolls I stole from [flora] and never returned... I wonder if she wants them back now."
  "Oh, look! It's the love letter I got from that cute Chinese girl in fifth grade that says, \"kcus uoy.\""
  "Never figured out what it meant, but I'm still keeping it close to my heart."
  "..."
  "Hmm... this photo of me and [jo] from when I was little..."
  "That's actually perfect to write on. Hurts a bit on the sentimental side, but I'm used to pain at this point."
  $mc.add_item("piece_of_paper")
  $quest.isabelle_stolen["paper_got"] = True
  return

label home_bedroom_closet_interact_smash_or_pass:
  "Not the time to play dress-up. Besides, all my clothes are dirty."
  return

label home_bedroom_closet_interact_kate_blowjob_dream_courage_badge:
  "Most of the stuff in here would probably be considered a wardrobe malfunction, but the unsexy kind."
  return

label home_bedroom_closet_interact_isabelle_stolen:
  "I'm sorry, [jo]."
  "I never meant to hurt you."
  "I never meant to make you cry."
  "But I refuse to clean out my closet."
  return

label home_bedroom_closet_jacklyn_statement_clothes:
  "I should probably have prepared better for this date..."
  "Well, no other option than picking something."
  menu(side="left"):
    extend ""
    "Wear the pirate outfit from last Halloween":
      $mc.charisma+=2
      "[jacklyn] might find it humorous, and that's always a win on a first date."
      "At least, that's what the tips said on that dating tips forum..."
      $quest.jacklyn_statement["clothes"] = "pirate"
    "Wear [flora]'s spiked choker from her emo phase and [jo]'s stolen stockings":
      "Almost look like a punk bitch in this."
      "Just rip the stockings and..."
      "Voilà!"
      $quest.jacklyn_statement["clothes"] = "punk"
    "Wear yesterday's outfit":
      $mc.strength+=2
      "Nothing says manly more than yesterday's musk."
      "Sweat and testosterone, baby."
      $quest.jacklyn_statement["clothes"] = "stinky"
  $quest.jacklyn_statement.advance("datedoor")
  return

image flora_bonsai_baking = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-359,-182),Transform(LiveComposite((1920,1080),(0,0),"home kitchen kitchen",(497,227),"home kitchen stove",(805,93),"home kitchen lights",(757,386),"home kitchen florabandaidcook",(857,476),"home kitchen various_back",(691,521),"home kitchen kettle"),size=(960,540))),"ui circle_mask"))
image flora_bonsai_handholding = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(0,-46),Transform("flora bed smile hand",size=(288,162))),"ui circle_mask"))

label home_bedroom_closet_jacklyn_statement_intruder:
  "Huh?"
  show flora afraid with Dissolve(.5)
  flora afraid "Aaah!" with hpunch
  mc "[flora]?! What the hell?"
  flora embarrassed "Sorry, but..."
  mc "But what?"
  flora sad "Nothing."
  mc "Why aren't you at the internship?"
  flora sad "It's... stupid."
  mc "Why were you hiding in my closet?"
  flora confused "I heard you coming up the stairs and panicked!"
  mc "What's going on?"
  flora annoyed "It's nothing. I'm just gonna go."
  mc "Not until you explain why you were spying on us."
  flora eyeroll "I wasn't spying, okay?"
  mc "What were you doing then?"
  flora annoyed "Just... keeping an eye on you two."
  mc "That sounds like spying to me."
  show flora annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    #"?1==0@|{image= flora contact_icon}|Tiny Thirsty Tree is coming in the next update!\n\nPlease consider supporting us on Patreon.|{image= items monkey_wrench}":
      #pass
    "{image=flora_bonsai_baking}|\"[flora], what's\nreally going on?\"" if quest.flora_bonsai.finished and not quest.flora_bonsai["handholding"]:
      jump home_bedroom_closet_jacklyn_statement_really_going_on
    "?quest.flora_bonsai.finished@|{image=flora_bonsai_handholding}|\"[flora], what's\nreally going on?\"" if not quest.flora_bonsai.finished:
      jump home_bedroom_closet_jacklyn_statement_really_going_on
    "?quest.flora_bonsai.finished and quest.flora_bonsai['handholding']@|{image=flora_bonsai_handholding}|\"[flora], what's really going on?\"" if quest.flora_bonsai["handholding"]:
      label home_bedroom_closet_jacklyn_statement_really_going_on:
        show flora annoyed at move_to(.5)
        mc "[flora], what's really going on?"
        flora cringe "Well, I don't think she's good for you."
        mc "Why didn't you say something?"
        flora annoyed "I did!"
        flora annoyed "I did when you first told me about the date!"
        mc "You said it was fine."
        flora angry "Yeah, after you showed me those stupid texts!"
        "Hmm... she's on the verge of tears..."
        "What could be so bad about dating [jacklyn]?"
        mc "Can you tell me why you're so upset?"
        flora sad "I... just don't think you should date her."
        mc "That's my choice."
        if quest.flora_bonsai['handholding']:
          flora crying "Then why the hell did you hold my hand?!"
        else:
          flora crying "Then why the hell did you help me bake?!"
        window hide
        show flora crying at disappear_to_left(.75)
        pause 0.75
        play sound "<from 9.2 to 10.2>door_breaking_in"
        show location with hpunch
        window auto
        "Well, shit. That took an unexpected turn."
        "It seems like that moment meant a lot to her..."
        window hide
        play sound "bedroom_door"
        pause 1.25
        show jacklyn thinking with Dissolve(.5)
        window auto
        jacklyn thinking "Everything baller? I heard a door slam."
        mc "Hmm... yeah. I think [flora] forgot we had a date tonight and didn't want to intrude."
        jacklyn smile "Wicked. Wanna get out of here? I know a good way to make a statement."
        show jacklyn smile at move_to(.75)
        menu(side="left"):
          extend ""
          "Go with [jacklyn]":
            show jacklyn smile at move_to(.5)
            mc "Yeah, I'll talk to her later or something."
            "Knowing [flora], she'll probably hate me now."
            "It's a bit weird that she felt so strongly about the hand holding... I was just trying to be nice."
            "Besides, we can't really date or have a normal relationship. She knows that."
            jacklyn excited "Let's go. I've got paint in my car."
            $quest.jacklyn_statement.advance("statement")
            jump goto_school_entrance
          "Go look for [flora]":
            show jacklyn smile at move_to(.5)
            "This is a shitty situation. That moment with [flora] meant a lot to me as well..."
            "Not sure if we can ever have a real relationship, but maybe she has an idea?"
            "It was really stupid of me to go on a date with [jacklyn] when [flora] is the one I really care about."
            mc "Sorry, [jacklyn], but I have to go talk to her."
            mc "It's probably best you go. Sorry for wasting your time..."
            jacklyn laughing "The sweatshop is clean, brother."
            jacklyn laughing "I got a decent dinner out of it. Nothing fucked in my panties."
            mc "Thanks for being cool about it."
            jacklyn smile "No problemo. I'll see myself out."
            show jacklyn smile at disappear_to_left
            "All right, better go find [flora] before she does something stupid."
            $quest.jacklyn_statement.advance("florasad")
#   "\"You, me, and [jo] — you know all three of us are terrible liars.\"":
    "\"You, me, and [jo] — you know\nall three of us are terrible liars.\"":
      show flora annoyed at move_to(.5)
      mc "You, me, and [jo] — you know all three of us are terrible liars."
      flora eyeroll "Fine. I was spying on you."
      mc "Why?"
      flora annoyed "Because [jacklyn] is a liar!"
      mc "What do you mean?"
      flora angry "We went to have cake and she was being really flirty."
      flora angry "She said she wanted to paint me and stuff."
      flora angry "And now she's here on a date with you!"
      mc "Are you sure you didn't misinterpret her advances?"
      mc "She's pretty flirty with everyone."
      mc "She's also said many times that she doesn't dabble, which I think means she's not into girls."
      flora annoyed "That's bullshit."
      mc "Why didn't you speak up earlier?"
      flora annoyed "I did! I just didn't think it was true!"
      flora annoyed "Why do you always try to ruin things for me?"
      play sound "bedroom_door"
      pause(1.25)
      show jacklyn thinking at appear_from_left(.25)
      show flora annoyed at move_to(.75,1.0)
      jacklyn thinking "What's balling? I heard a scream."
      mc "..."
      flora laughing flip "Sorry, that was me."
      flora worried flip "Why are you dating [mc]?"
      jacklyn smile_right "I was hungry, mostly."
      "Ouch. That's rough."
      flora annoyed "Why did you tell me those things during our picnic?"
      jacklyn thinking "What things?"
      flora annoyed "That you liked my hair and my smile, and that you wanted to paint me without clothes!"
      jacklyn smile_right "Well, I think that, and I'd like to do that."
      flora concerned flip "So you're just trying to make me jealous?"
      jacklyn smile_right "Are you jealous?"
      flora angry flip "What do you think?!"
      show flora angry flip at disappear_to_right
      pause 0.5
      play sound "<from 9.2 to 10.0>door_breaking_in"
      with hpunch
      show jacklyn smile_right at move_to(.5)
      pause 0.0
      jacklyn cringe "Drama llama."
      mc "Did you really just go on a date with me to make her jealous?"
      jacklyn smile "Obviously not."
      jacklyn smile "I also did it for the free meal."
      "Ugh."
      mc "So, you care about her?"
      jacklyn thinking "I mean, it is a known fact that I don't dabble. Is that what you're asking?"
      mc "I don't really know..."
      show jacklyn thinking at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I think you should go after her. She looked heartbroken.\"":
          show jacklyn thinking at move_to(.5)
          mc "I think you should go after her. She looked heartbroken."
          jacklyn neutral "You're probably right. Can't have damsels weeping over me."
          jacklyn smile "Thanks for the dinner."
          show jacklyn smile at disappear_to_left
          $mc.love+=1
          "Not really sure what's gotten into me, but it felt like the right thing to do."
          "[flora] deserves an explanation from [jacklyn]."
          "Besides, we can make a statement later."
          "There's always another chance... right?"
          $quest.jacklyn_statement.finish()
        "\"Wanna get out of here and make a statement?\"":
          show jacklyn thinking at move_to(.5)
          mc "Wanna get out of here and make a statement?"
          jacklyn excited "You know it!"
          jacklyn annoyed "Can't stand people tripping over their feelings."
          jacklyn laughing "Let's go."
          show jacklyn laughing at disappear_to_left
          "Not sure where we're going, but this should be interesting if nothing else."
          $quest.jacklyn_statement.advance("statement")
          jump goto_school_entrance
  return
