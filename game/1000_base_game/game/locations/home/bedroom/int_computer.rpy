init python:
  class Interactable_home_bedroom_computer(Interactable):

    def title(cls):
      return "Computer"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.kate_blowjob_dream.in_progress:
        if quest.kate_blowjob_dream >= "alarm":
          return "Inter Pensium Magnum —\nhalf revolver, half processor —\nthe slowest gun in the west."
        elif quest.kate_blowjob_dream >= "get_dressed":
          return "This computer runs on diesel and prayers, and I'm almost out of both."
        elif quest.kate_blowjob_dream >= "courage_badge":
          return "At least it's not dial-up. Shit used to be grim back in the day."
      else:
        if quest.maya_witch.started:
          return "You haven't died on me yet,\nold friend, and I call upon you\nfor aid once more."
        elif quest.flora_handcuffs.started or home_bedroom["clean"]:
          return "The computer industry is on a sharp rise. For now, I'm stuck with this junk box."
        elif quest.isabelle_stolen.started:
          return "It's time to start saving up for a new computer. This shit can't even handle Crysis on highest resolution."
        elif quest.back_to_school_special.started:
          return "Imagine being one screen away from all knowledge and entertainment in the world. That's the definition of \"so close yet so far.\""
        elif quest.dress_to_the_nine.started:
          return "It's not an addiction. Stopping would be easy... but why, though? At least, it's not cigarettes or gambling."
        elif quest.smash_or_pass.started:
          return "It's not an addiction. Stopping would be easy... but why, though? At least, it's not drugs or alcohol."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream in ("open_door","get_dressed","school"):
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "isabelle_locker":
          if quest.isabelle_locker == "email":
            actions.append("quest_isabelle_locker_email")
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "computer":
            actions.append("quest_kate_desire_nude_computer")
        elif mc["focus"] == "kate_moment":
          if quest.isabelle_gesture == "chocolate":
            actions.append("quest_isabelle_gesture_chocolate")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        # quest interactions
        if quest.isabelle_stolen == "footage":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_stolen,computer|Use What?","home_bedroom_computer_interact_isabelle_stolen"])
        if quest.mrsl_HOT == "hookitup" and mc.owned_item("vhs_player"):
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_HOT,computer|Use What?","home_bedroom_computer_interact_mrsl_HOT_hookitup"])
        elif quest.mrsl_HOT == "attic2" and mc.owned_item("capture_card"):
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_HOT,computer_capture|Use What?","home_bedroom_computer_interact_mrsl_HOT_capture_card"])
        if quest.isabelle_hurricane == "surveillance_footage":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:isabelle_hurricane,surveillance_footage|Use What?","quest_isabelle_hurricane_surveillance_footage"])
        if home_computer["visited"]:
          actions.append(["black_market","Black Market","goto_home_computer"])
        if quest.kate_wicked >= "wait" and mc.owned_item("costume_shop_voucher"):
          actions.append(["costume_shop","Costume Shop","quest_kate_wicked_costume_shop"])
        if quest.kate_blowjob_dream == "sleep":
          actions.append("?home_bedroom_computer_interact_kate_blowjob_dream_sleep")
        elif quest.kate_blowjob_dream >= "courage_badge" and quest.kate_blowjob_dream.in_progress:
          actions.append("?home_bedroom_computer_interact_kate_blowjob_dream_courage_badge")
        if quest.jo_potted == "computer":
          actions.append("?jo_quest_potted_internet")
        if quest.maxine_lines == "magnetic" and not home_bedroom["lines_magnet"]:
          actions.append("?home_bedroom_computer_interact_maxine_lines_take_magnet")
        if quest.mrsl_HOT == "research":
          actions.append("?home_bedroom_computer_interact_mrsl_HOT_research")
        if quest.mrsl_HOT == "reply" and game.day > quest.mrsl_HOT["current_day"]:
          actions.append("?home_bedroom_computer_interact_mrsl_HOT_reply")
        if quest.mrsl_HOT == "watch":
          actions.append(["interact","Watch Tape","?home_bedroom_computer_interact_mrsl_HOT_watch"])
        if quest.jacklyn_sweets == "bedroom":
          actions.append("?quest_jacklyn_sweets_bedroom")
        if quest.isabelle_piano == "computer":
          actions.append("?quest_isabelle_piano_computer_wrong")
        if quest.flora_handcuffs == "order":
          actions.append("?quest_flora_handcuffs_order")
        if quest.lindsey_motive == "google":
          actions.append("quest_lindsey_motive_google")
        if quest.kate_stepping == "photoshop" and not quest.kate_stepping["print_it_out"]:
          actions.append("quest_kate_stepping_photoshop_computer")
        if quest.maya_witch == "black_market" and not home_computer["visited"]:
          actions.append("?quest_maya_witch_black_market_home_bedroom_computer")
        if quest.isabelle_gesture == "chocolate":
          actions.append("quest_isabelle_gesture_chocolate")
        if quest.maxine_dive == "milk" and quest.maxine_dive["black_market_milk"] and not home_computer["visited"]:
          actions.append("quest_maxine_dive_milk_black_market_milk")
      # flavour interactions
      if home_bedroom["clean"]:
        actions.append("?quest_lindsey_piano_listen_bedroom_computer")
      if quest.mrsl_HOT.started:
        actions.append("?home_bedroom_computer_interact_mrsl_HOT")
      if quest.isabelle_stolen.started:
        actions.append("?home_bedroom_computer_interact_isabelle_stolen_default")
      if quest.back_to_school_special.started:
        actions.append("?home_bedroom_computer_interact_back_to_school_special")
      if quest.dress_to_the_nine.started:
        actions.append("?home_bedroom_computer_interact_dress_to_the_nine")
      if quest.smash_or_pass.started:
        actions.append("?home_bedroom_computer_interact_smash_or_pass")
      if quest.kate_blowjob_dream >= "flora_knocking":
        actions.append("?home_bedroom_computer_interact_kate_blowjob_dream_get_dressed")
      if quest.kate_blowjob_dream >= "alarm":
        actions.append("?home_bedroom_computer_interact_kate_blowjob_alarm")


label home_bedroom_computer_interact_smash_or_pass:
  "This machine is one of the few things that brings me happiness. Fourteen hours a day has to be enough, though."
  return

label home_bedroom_computer_interact_dress_to_the_nine:
  "The secret folder, hidden within twenty other folders. Disguised among dummy folders and eye-catching images..."
  "Years of hard work and dedication. All my catalogues, indexes, and ratings. The perfect porn collection..."
  "My life's work... my magnum opus... all gone..."
  return

label home_bedroom_computer_interact_back_to_school_special:
  "When the day is over, a new quest begins here. The start of something great. The collection of collections! But that has to wait."
  return

label home_bedroom_computer_interact_kate_blowjob_dream_courage_badge:
  "Time to go through all the porn lost to time and start rebuilding my collection."
  "There's something special about clicking through these old sites."
  "They're a bit different from my memory and the stock is certainly more limited, but there should be a few golden nuggets..."
  "You just need to know where to look. Few people are as well-versed in early-internet pornography as yours truly."
  "..."
  "..."
  "Ah, here we go!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
# $game.advance()
  while game.hour < 20:
    $game.advance()
  pause 1.5
  hide black onlayer screens with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Damn, it's already late."
  "Somehow time always seems to pass so much faster when you're having a good wank or three."
  "Well, it's probably time to sleep, anyway."
  $ quest.kate_blowjob_dream.advance("sleep")
  return

label home_bedroom_computer_interact_kate_blowjob_dream_sleep:
  "Two times in a row?"
  "Do you want chaffing?"
  "Because that's how you get chaffing."
  return

label home_bedroom_computer_interact_kate_blowjob_dream_get_dressed:
  "Oh, shit. That video was still running. Good thing I caught it."
  return

label home_bedroom_computer_interact_kate_blowjob_alarm:
  "Let me just start a few downloads that'll run throughout the day. It's not illegal if you're not at the computer."
  return

label home_bedroom_computer_interact_mrsl_HOT:
  "Hello darkness my old friend."
  "I've come to waste my life on you again."
  return

label home_bedroom_computer_interact_mrsl_HOT_research:
  "Okay, let's see here..."
  "{i}High-intensity... Omni-flex... Training...{/}"
  "..."
  "Damn, I'd forgotten how slow our connection used to be..."
  "..."
  "There are only a few hits for this exact wording..."
  "They all link to a dead website."
  "..."
  "Luckily, there's always the deeper and darker places on the net."
  "Nothing ever really goes away if you just know where to look."
  $game.advance()
  with Fade(.5,3,.5)
  "Everything seems to have been scrubbed pretty thoroughly. Only managed to pull an image and an email address."
  "The image is slowly being decrypted."
  "The email is probably useless at this point, but it doesn't hurt to try."
  "..."
  "{i}\"Dear random person who once owned this site,\"{/}"
  "{i}\"I'm interested in your videotapes.\"{/}"
  "{i}\"Please get back to me.\"{/}"
  "..."
  "That should do it. Now, let's see that image..."
  "Hold up."
  $unlock_replay("mrsl_tape")
  show mrsl events LichYoga1_naked with Dissolve(.5)
  "No way..."
  "There's no way."
  "Fuck. That's really [mrsl]! Why the hell was she on that website?"
  "Man, she's a complete goddess."
  "That's simply incredible."
  "She's showing off every curve that her body has to offer. Tight but voluptuous at the same time."
  "An unholy grace."
  "This flawless balance. Every muscle and fiber, straining to keep the equilibrium."
  "The soft bend of her spine. The tightening of her already slim waist. The contrast of her wide hips and full breasts."
  "If feasting your eyes on carnal perfection is a sin, then send me straight to Hell. This is what my body and soul thirst for."
  "Her entire body seems to defy gravity apart from her boobs. They hang heavy, her pierced nipples begging to be touched. Elegance meets raw sexual desire."
  "It's like she's presenting her body to the world, begging someone to stick it up her ass... into her pussy... or down her throat."
  "I'm at a loss for words."
  "..."
  "Why was that picture there, though? Did that website once belong to her, or was she just posting on their forum?"
  "Also, why is she working out naked?"
  "Fuck. I really need to get my hands on those tapes now."
  $quest.mrsl_HOT["current_day"] = game.day
  $quest.mrsl_HOT.advance("reply")
  hide mrsl with Dissolve(.5)
  return

label home_bedroom_computer_interact_mrsl_HOT_reply:
  if quest.mrsl_HOT["broke_af_pt2"]:
    "Fifty bucks for a videotape feels like a scam, but..."
  else:
    "Huh. I got a reply to my email."
    "{i}\"Dear anonymous user with an anime girl picture,\"{/}"
    "{i}\"I have what you're looking for.\"{/}"
    "{i}\"Deposit $50 into the account below, and the first videotape will be delivered to an address of your choice.\"{/}"
    "Hmm, fifty bucks for a videotape... it feels like a scam, but..."
  menu(side="middle"):
    extend ""
    "?mc.money>=50@[mc.money]/50|{image=ui icon_money}|Pay the scammer":
      $mc.money-=50
      "Well, that's fifty bucks I'll probably never see again."
      "Hope is a strange thing sometimes..."
      $quest.mrsl_HOT.advance("delivery")
      $quest.mrsl_HOT["current_day"] = game.day
    "Nope":
      $quest.mrsl_HOT["broke_af_pt2"] = True
  return

label home_bedroom_computer_interact_mrsl_HOT_hookitup(item):
  if item == "vhs_player":
    $home_bedroom["vhs"] = True
    $mc.remove_item(item)
    "Hmm... despite this computer being older than Noah's Ark, this VHS player is older still."
    "I'll need a capture card for this to work."
    "[jo] might know if we have this stuff in the basement."
    $quest.mrsl_HOT.advance("items")
  else:
    "Hooking up my [item.title_lower] to my computer is a project for the future."
    $quest.mrsl_HOT.failed_item("computer",item)
  return

label home_bedroom_computer_interact_isabelle_stolen(item):
  if item=="flash_drive":
    "All right, let's see what's on here..."
    $clothvar = mc['first_choice']
    $unlock_replay("mrsl_dance")
    #$unlock_replay("nurse_first_strike") #Commenting this out as unsure why this scene is unlocked here.
    "Oh, look it's me. God, I look like shit in my [clothvar]..." #TBD make a variable for this
    "Fix your posture, man!"
    #SFX: rewind sound
    show mrsl surveillance_one with Dissolve(.5)
    "Always wondered why they put the surveillance camera in this spot, but it makes sense now. It's the perfect place to see the whole room."
    "Just people walking around. Seniors headed for the homeroom, everyone else going to the football field."
    "..."
    #SFX: rewind sound
    #SFX: rewind distortion effect
    "Here comes [lindsey] at full speed..."
    show mrsl surveillance_two with Dissolve(.5)
    "Look out!"
    show black with hpunch
    $set_dialog_mode("default_no_bg")
    "Ouch. That hurts. Damn slippery floors."
    "Her butt looked even better up close, but honestly, this is decent fapping material..."
    $set_dialog_mode()
    hide black with Dissolve(.5)
    show mrsl surveillance_three with Dissolve(.5)
    "Clipped."
    "..."
    #SFX: rewind sound
    #SFX: rewind distortion effect
    show mrsl surveillance_four with Dissolve(.5)
    "Hey, there's [isabelle]. She's looking lost."
    "Is that a dildo in her hand? Ah, no. Just her phone."
    "She stood there for a long time, didn't she?"
    "Daydreaming about the Scottish highland perhaps."
    "..."
    "Hmm... what else is on here..."
    #SFX: rewind sound
    show mrsl surveillance_five with Dissolve(.5)
    "Ah, there's [mrsl]!"
    "As usual, flirting with the poor [guard]. Teasing him with her body."
    "Looks like the glass to the [guard]'s booth is getting all steamy."
    "She drew something on the glass. A heart? A pair of luscious lips?"
    "Hmm... seems like a longer message..."
    $quest.mrsl_number.start() #hidden #TBD add quest?
    show mrsl surveillance_three with Dissolve(.5)
    "Okay, she left. Let's fast forward a bit..."
    "No point looking if her ass is no longer in the picture."
    #SFX: rewind sound
    #SFX: rewind distortion effect
    "..."
    show mrsl surveillance_five with Dissolve(.5)
    "Why, hello again there, [mrsl]!"
    "It's late in the day and you're still over there, bothering the [guard]."
    "He probably likes the attention, though. I know I would."
    "..."
    "The [guard] left to go upstairs for his usual rounds. Sweeping the school for potential stragglers and putting them in detention."
    "But, [mrsl]? What do you get up to after the school closes?"
    "..."
    show mrsl surveillance_three with Dissolve(.5)
    "She's walking over to the couch with a confident sway in her hips, grabbing a mop from the janitor's closet on the way."
    "..."
    "What... wait, what is she doing?"
    "Why is she stripping out of her dress?"
    show mrsl mrsl_pole_ready_frame_one with Dissolve(.5)
    show mrsl mrsl_pole_ready_frame_two with Dissolve(2)
    show mrsl mrsl_pole_ready_frame_three with Dissolve(2)
    show mrsl mrsl_poleready with Dissolve(.5)
    "..."
    "What the hell?"
    "..."
    "All right, time to pause and unzip."
    show mrsl mrsl_poletop with Dissolve(.5)
    "With the mop firmly planted on the floor, she grinds it, pole dancing around it."
    "A mischievous smile touches her lips as she slides mop up and down between her buttcheeks."
    show mrsl mrsl_poletop with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_polebot with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_poletop with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_polebot with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_poletop with Dissolve(.5)
    "Like a stripper she rides that mop, the muscles in her thighs and stomach flexing."
    "Rising to her tippy toes before plunging deep into a crouch."
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_polebot with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_poletop with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_polebot with Dissolve(.5)
    "Teasing her asshole and pussy lips, leaving a sheen of her juices along the pole."
    "Maybe this is why the homeroom smells like it does?"
    "It's almost like she's gripping the shaft, squeezing it, teasing it with her most intimate areas."
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_polebot with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_poletop with Dissolve(.5)
    show mrsl mrsl_polemid with Dissolve(.5)
    show mrsl mrsl_polebot with Dissolve(.5)
    "Her hair whips back and forth while her hips sway to an unheard tune."
    "Hypnotic."
    "Up and down the pole, dancing with her whole body. Pleasuring herself with the stick."
    "Licking her lips."
    "..."
    "Oh snap, the [guard] is returning."
    show mrsl mrsl_pole_bot_frame_three with Dissolve(.5)
    show mrsl mrsl_pole_bot_frame_two with Dissolve(.5)
    show mrsl mrsl_pole_bot_frame_one with Dissolve(.5)
    show mrsl surveillance_three with Dissolve(.5)
    "She saw it too, and hurried off just in time."
    "Close call..."
    #SFX: rewind sound
    #SFX: rewind distortion effect
    "All right. Next day."
    "Let's just skip forward a bit more."
    #SFX: rewind sound
    #SFX: rewind distortion effect
    show mrsl surveillance_four with Dissolve(.5)
    "There's [isabelle] again."
    "She's heading toward her locker."
    show mrsl surveillance_three with Dissolve(.5)
    "She just put her purse in and took out her sports bag."
    "All right, she left for the gym..."
    "Let's see who our thief is..."
    "..."
    "..."
    "Come on... show yourself..."
    "..."
    show mrsl lindsey_stealing1 with Dissolve(.5)
    "[lindsey]?!"
    "No way..."
    "She's the perfect princess. There's no way..."
    "How did she open [isabelle]'s locker?"
    show mrsl lindsey_stealing2 with Dissolve(.5)
    "Is she one of those rich kids who gets a kick out of stealing stuff?"
    "She has such a bright future ahead of her. Why would she risk it with something so stupid?"
    show mrsl lindsey_stealing1 with Dissolve(.5)
    "Maybe her dad has one of those harvey-specter-lawyers, so it doesn't matter if she is caught."
    "Huh. I didn't even see what she took."
    show mrsl surveillance_three with Dissolve(.5)
    "Well, I know who did it now."
    #If the player took the KateWarning route:
    if quest.isabelle_stolen["warned_kate"]:
      "So, [kate] didn't actually do it."
      "Still, she'll probably want the tape destroyed. She wouldn't want [lindsey] to get in trouble."
      "[kate]'s vendetta is against [isabelle], and she'd probably prefer her to keep wondering and growing frustrated."
      "Maybe it's still worth asking [lindsey] about it. If nothing else, to sate my own curiosity."
    else:
      "Somehow, I was expecting it to be [kate]. This makes it much tougher."
      "Hmm... is telling [isabelle] the right call even? She'll come down on [lindsey] with righteous fury."
      "Maybe I should ask [lindsey] why she did it?"
      "Hmm..."
    menu(side="middle"):
      "Continue watching":
        "I've always felt like an outsider looking in."
        "Peeping on people has become a hobby of mine."
        "They go about their days, unknowingly watched and judged."
        "It's fascinating what you notice if you're just people-watching."
        "Girls adjusting their thongs when they think no one's looking."
        "Brad checking his wallet to make sure he's still got dough."
        "Tanner picking his nose and eating the booger."
        "Chad... ugh, he still looks like a Greek god even when no one's watching..."
        #SFX: rewind sound
        #SFX: rewind distortion effect
        "Not much happening..."
        "Just people rushing to their classes. To the cafeteria."
        "The [guard] doing his final rounds."
        "[mrsl] doing her... wait a minute..."
        show mrsl mrsl_pole_ready_frame_one with Dissolve(.5)
        show mrsl mrsl_pole_ready_frame_two with Dissolve(2)
        show mrsl mrsl_pole_ready_frame_three with Dissolve(2)
        show mrsl mrsl_poleready with Dissolve(.5)
        "And she's got that mop again. If the janitor finds out... well, I'd like to see that conversation."
        show mrsl mrsl_poletop with Dissolve(.5)
        "She moves so casually yet sexy. Does she do this every night?"
        show mrsl mrsl_polemid with Dissolve(.5)
        show mrsl mrsl_polebot with Dissolve(.5)
        show mrsl mrsl_polemid with Dissolve(.5)
        show mrsl mrsl_poletop with Dissolve(.5)
        "Last time she was interrupted, but now... now she settles on the couch."
        show mrsl mrsl_couchmastpre with Dissolve(.5)
        "Slides into position. Spreading her legs like she wants it."
        "The contrast of missionary position and her sinful underwear is something out of a wet dream."
        "The way she teases the camera; it's almost like she knows someone's watching."
        "Playfully tugging at her underwear. Playfully winking at the camera?"
        show mrsl mrsl_couchmastready with Dissolve(.5)
        "Her fingers pull her panties to the side, exposing her flesh flower."
        "The clit piercing steals the show as she rubs it between her fingertips."
        "Biting her lip. Roughly flicking her little bean. Dark passion building inside her."
        "With neon embers burning in her eyes, she lines the tip of the mop up with her pussy."
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        "An inaudible gasp escapes her lips as the rough tip settles against her soft flesh."
        "A shiver of anticipation pricks the skin of her open thighs."
        "The muscles in her abdomen twitch, ready for the inevitable penetration."
        "Ready to take the shaft deep."
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        "Eyes closed, she pushes the tip inside."
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        "Her vagina provides the required lubrication... or perhaps it doesn't, but she likes the slight friction tearing at her walls."
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        "Slowly, with a steady rhythm she pushes the shaft deeper, only to let it back out again."
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        "Her hand finds her nipple, squeezing it between sharp nails, sending jolts of cruel pain and heightened pleasure into her brain."
        "Fingers tugging at her piercings, bullying the tortured nub."
        "Hey eyes light up, as the sensations of the masochism rolls through her, connecting with the sharp depravity of the sadistic act."
        "Of course, someone like her would enjoy both ends of the kink spectrum."
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        "Deeper. As deep as the shaft will go. Teasing the entrance of her cervical canal."
        "A gasp of pain undoubtedly shatters the silence of the entrance hall."
        "Perhaps that was a little too deep?"
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        "But perhaps... perhaps it was just a warmup?"
        "She wants another try. To feel full. To grip the shaft with the muscles in her pussy. Squeeze it. Milk it."
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        "Beads of sweat glitter on her skin."
        "The blush on her cheeks spreads like wildfire through her body."
        "Her labia shudders like jelly."
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        "Deep strokes. Fingers playing with her clit and nipple piercings."
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        "She's taking her time, despite rapidly approaching her climax."
        "Enjoying the janitor's mop to the fullest."
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasttip_eyes with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasthalf_nipple with Dissolve(.5)
        show mrsl mrsl_couchmastfull with Dissolve(.5)
        show mrsl mrsl_couchmasttip with Dissolve(.5)
        show mrsl mrsl_couchmasthalf with Dissolve(.5)
        "Suddenly, her entire body tenses up. Her eyes roll back, as the waves of pleasure roll through her."
        "Shudders. Muscles spasming to contain the raw sexual energy trying to rip out of her."
        show mrsl mrsl_couchmastsquirt with Dissolve(.5)
        "Her chest heaves as the juices squirt out of her, spraying the couch dark with her sin."
        "Wave after wave. Gasp after gasp. Each shuddering spasm tossing her body around."
        "Completely spent, she lies there, feeling the pleasure ebb out of her body."
        "Never thought I'd see something like this outside of Pornhub..."
        "It's like a dream come true. Only it's not a dream. At least I hope not."
        "Fuck. I wouldn't mind watching that again, but..."
        "[flora]'s always snooping, and I could get into some serious trouble if anyone finds out I stole the school's surveillance footage."
        hide mrsl mrsl_couchmastsquirt with Dissolve(.5)
        "I'll just clip the part with [lindsey] stealing and put it on my phone for now."
        "The rest of the tape has to go..."
        "What a truly painful loss of what can only be considered art."
        $quest.isabelle_stolen.advance("dilemma")
      "Move on":
        "There's probably something more interesting on TV."
        "..."
        "All right. I've clipped the part with [lindsey] stealing and put it on my phone for now."
        "The rest of the tape has to go."
        "[flora]'s always snooping, and I could get into some serious trouble if anyone finds out I stole the school's surveillance footage."
        $quest.isabelle_stolen.advance("dilemma")
        hide mrsl surveillance_three with Dissolve(.5)
  else:
    "Great! Now I just have [item.title_lower] crumbs in my USB port."
    $quest.isabelle_stolen.failed_item("computer",item)
  return

label jo_quest_potted_internet:
  if quest.jo_potted["internet_search"]:
    "Gigglypuff seeds sound like the stuff I need."
    jump jo_quest_potted_internet_buy
  "Surely, [flora] must've found her cure on the internet last time."
  "Okay, Google. Let's see what you got."
  "{i}Miracle... cures... that... fix... throat... pain...{/}"
  "Hmm... lots of results here..."
  "{i}\"Magic Jackson's easy tricks to fix your throat pain. The doctors hate him!\"{/}"
  "{i}\"Got something stuck in your throat? Try eating rocks.\"{/}"
  "{i}\"Avoid throat pain by becoming a lesbian today. Hot singles in your area!\"{/}"
  "Hmm... the results are all pretty useless. Well, the rock one was nice. I'll keep some in my pocket from now on."
  "..."
  "Should probably try the dark web instead..."
  "One of the drug sites should have something."
  "..."
  "Crystal meth lollipops? Hmm... no. [jo] hates lollipops."
  "Mexican cocaine doughnuts? The [guard] would probably be into that, but [jo] is very careful about her figure."
  "What's this? Gigglypuff seeds?"
  "{i}\"Swallowed too much dick? Like a lullaby for your pain and ailments!\"{/}"
  "{i}\"Grow it yourself. Stronger than pot. More relaxing than tranquilizers.\"{/}"
  "This sounds like the stuff I need."
  $quest.jo_potted["internet_search"] = True
  label jo_quest_potted_internet_buy:
  menu(side="middle"): #repeatable if player chooses "Not now", start with this menu then.
    extend ""
    "?mc.money>=69@[mc.money]/69|{image=ui icon_money}|Buy gigglypuff seeds": #$69
      $mc.money-=69
      "Can't really buy common drugs in case [flora] or [jo] opens the package."
      "These seeds seem inconspicuous enough."
      "Should arrive within a couple of days."
      $quest.jo_potted.advance('package')
      $quest.jo_potted["package_today"] = True
      $quest.jo_potted["days"] = 0
    "Not now":
      return
  return

label home_bedroom_computer_interact_isabelle_stolen_default:
  "What's new on the old buzz box?"
  "Right... nothing. Nothing new for several more years."
  return


label home_bedroom_computer_interact_maxine_lines_take_magnet:
  if mc.at("school_computer_room"):
    $school_computer_room["screen1"] = True
  if not quest.maxine_lines["magnet_order"]:
    "Where the hell do you order magnets from in this day and age?"
    "Hmm... online shopping is still pretty underdeveloped..."
    $quest.maxine_lines["magnet_order"] = True
  "There's only one place that sells magnets, and they only have one in stock."
  menu(side="middle"):
    extend ""
    "?mc.money>=10@[mc.money]/10|{image=ui hud icon_money}|Order the magnet":
      $mc.money-=10
      $home_bedroom["lines_magnet"] = True
      $home_kitchen["lines_magnet"] = True
      $mc["package_due_today"] = True
      "All right... order placed. It should arrive tomorrow."
    "Don't waste money on junk":
      pass
  if mc.at("school_computer_room"):
    $school_computer_room["screen1"] = False
  return
