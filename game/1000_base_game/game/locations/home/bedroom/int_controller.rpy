init python:
  class Interactable_home_bedroom_controller(Interactable):

    def title(cls):
      return "Controller"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The buttons are so worn down that the colors are fading... just how it should be."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream in ("open_door","get_dressed","school"):
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take","Take","home_bedroom_controller_take"])
      actions.append("?home_bedroom_controller_interact")


label home_bedroom_controller_take:
  if (quest.mrsl_HOT == "attic2"
  or (quest.maya_quixote == "attic" and quest.maya_quixote["old_tricks"])):
    "Okay, it's not really a lasso, but it's good enough."
    window hide
  $home_bedroom["controller_taken"] = True
  $mc.add_item("xcube_controller")
  return

label home_bedroom_controller_interact:
  "Controlling my fate. That's the goal here."
  return
