init python:
  class Interactable_home_bedroom_porn_mag(Interactable):

    def title(cls):
      return "Porn Mag"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.kate_blowjob_dream.in_progress:
        if quest.kate_blowjob_dream >= "alarm":
          return "An erotic blend between fiction and reality, where fantasy meets carnal desire.\n\nA review by [mc]."
        elif quest.kate_blowjob_dream >= "get_dressed":
          return "Printed porn just feels more exclusive, somehow."
        elif quest.kate_blowjob_dream >= "courage_badge":
          return "Harriet Hotter and the Philosopher's Boner. A modern day classic."
      elif quest.isabelle_stolen.started:
        return "Stilettos in my soul like this. Sharp and deadly. Only thing that gets my blood flowing."
      elif quest.back_to_school_special.started:
        return "A fine piece of classic literature. Moby-Dick got nothing on this."
      elif quest.dress_to_the_nine.started:
        return "The shunned pillar on which society rests. Why am I thinking of pillars all of a sudden?"
      else:
        return "Found it outside the neighbor's garage when I was fourteen. It's been a great source of inspiration over the years."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream in ("open_door","get_dressed","school"):
            actions.append("kate_blowjob_dream_random_interact")
          elif quest.kate_blowjob_dream == "alarm":
            actions.append("?home_bedroom_porn_mag_interact_kate_blowjob_alarm")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if quest.smash_or_pass.started:
        if home_bedroom["alarm"] == "beeping":
          actions.append("?home_bedroom_porn_mag_interact_smash_or_pass_beeping")
        else:
          actions.append("?home_bedroom_porn_mag_interact_smash_or_pass")
      if quest.dress_to_the_nine.started:
        actions.append("?home_bedroom_porn_mag_interact_dress_to_the_nine")
      if quest.back_to_school_special.started:
        actions.append("?home_bedroom_porn_mag_interact_back_to_school_special")
      if quest.kate_blowjob_dream > "alarm":
        actions.append("?home_bedroom_porn_mag_interact_kate_blowjob_finish")
      if quest.kate_blowjob_dream >= "get_dressed":
        actions.append("?home_bedroom_porn_mag_interact_kate_blowjob_dream_get_dressed")
      if quest.kate_blowjob_dream >= "courage_badge":
        actions.append("?home_bedroom_porn_mag_interact_kate_blowjob_dream_courage_badge")
      if quest.isabelle_stolen.started:
        actions.append("?home_bedroom_porn_mag_interact_isabelle_stolen")


label home_bedroom_porn_mag_interact_smash_or_pass_beeping:
  "Rubbing one out while the alarm is blaring seems risky. What if [jo] comes up the stairs and I don't hear it?"
  "It's not like I've never fantasized about her barging in, but I don't think I'm ready to make that a reality just yet."
  $achievement.devilish.unlock()
  return

label home_bedroom_porn_mag_interact_smash_or_pass:
  "Just a quick peek. Nothing more."
  return

label home_bedroom_porn_mag_interact_dress_to_the_nine:
  "Just a quick peek to get the blood flowing."
  return

label home_bedroom_porn_mag_interact_back_to_school_special:
  "No matter how bad things get, there's always time... for another peek."
  return

label home_bedroom_porn_mag_interact_kate_blowjob_dream_courage_badge:
  "Just an alactrious glance to stir the hemoglobin."
  return

label home_bedroom_porn_mag_interact_kate_blowjob_dream_get_dressed:
  "Just an expeditious scrying to turbulate the ichor."
  return

label home_bedroom_porn_mag_interact_kate_blowjob_alarm:
  "Tinnitus is my biggest fetish. There's just something so hot about shattered eardrums."
  return

label home_bedroom_porn_mag_interact_kate_blowjob_finish:
  "Just a quick peek to start the day. No big deal. Unless you count the growing bulge in my pants."
  return

label home_bedroom_porn_mag_interact_isabelle_stolen:
  "Don't look. Don't look. Don't look."
  "Fuck it. A quick skim never hurt nobody."
  return
