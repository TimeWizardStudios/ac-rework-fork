init python:
  class Interactable_home_bedroom_sports_magazine(Interactable):

    def title(cls):
      return "Sports Magazine"

    def description(cls):
      if (quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "A sports magazine... less sleazy than a porn mag, but only barely."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?home_bedroom_sports_magazine_interact")


label home_bedroom_sports_magazine_interact:
  "For the sake of keeping up appearances. Grown men playing with balls. Very manly."
  return
