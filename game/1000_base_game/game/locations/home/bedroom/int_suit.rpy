init python:
  class Interactable_home_bedroom_suit(Interactable):

    def title(cls):
      return "Suit"

    def description(cls):
      if quest.mrsl_bot == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "I could work for the CIA catching aliens wearing this thing."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "suit":
            actions.append("quest_jacklyn_town_suit")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?home_bedroom_suit_interact")


label home_bedroom_suit_interact:
  "Damn, the suit does look great!"
  "[maya] really is a goddess with those fingers of hers."
  return
