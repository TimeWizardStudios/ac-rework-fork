### home_bedroom_sugarcube5 interactable ######################################

init python:
  class Interactable_home_bedroom_sugarcube5(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_bedroom_sugarcube5_take"])

label home_bedroom_sugarcube5_take:
  $home_bedroom["sugarcube5_taken"] = True
  $mc.add_item("sugar_cube")
  return

### home_bedroom_sugarcube4 interactable ######################################

init python:
  class Interactable_home_bedroom_sugarcube4(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_bedroom_sugarcube4_take"])

label home_bedroom_sugarcube4_take:
  $home_bedroom["sugarcube4_taken"] = True
  $mc.add_item("sugar_cube")
  return

### home_bedroom_sugarcube3 interactable ######################################

init python:
  class Interactable_home_bedroom_sugarcube3(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_bedroom_sugarcube3_take"])

label home_bedroom_sugarcube3_take:
  $home_bedroom["sugarcube3_taken"] = True
  $mc.add_item("sugar_cube")
  return
