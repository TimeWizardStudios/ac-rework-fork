init python:
  class Interactable_home_bedroom_bed(Interactable):

    def title(cls):
      return "Bed"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif home_bedroom["clean"]:
        return "It actually looks inviting now. Girls! Welcome over!\n\n...\n\nNo one?"
      elif quest.kate_blowjob_dream >= "alarm":
        return "Forgotten treasures lie buried beneath this bed. Socks mostly, but also treasures."
#     elif quest.kate_blowjob_dream >= "get_dressed":
#       return "I've avoided making my bed for years for one reason. No one will ever tell me to lie in the bed I made."
      elif quest.kate_blowjob_dream >= "courage_badge":
        return "Undercover or under the covers. The difference is like pretending to have sex and actually having sex."
      elif quest.dress_to_the_nine.started:
        return "The covers need changing. Don't need a black light to know that."
      elif quest.smash_or_pass.started:
        return "[flora] once got one of those crime scene blacklights for her birthday.\n\nShe almost threw up when she pointed it at my bed.\n\nPoor [flora], she wasn't ready."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream in ("open_door","get_dressed","school"):
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "nightvisit":
            actions.append(["go","Sleep","quest_flora_bonsai_bed"])
          elif quest.flora_bonsai == "sleep":
            actions.append(["go","Sleep","quest_flora_bonsai_sleep"])
          elif quest.flora_bonsai in ("fetch","fetchover"):
            actions.append(["go","Sleep","quest_flora_bonsai_fetch_sleep"])
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement >= "intruder":
            actions.append("?home_bedroom_bed_interact_jacklyn_statement")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "maxine_hook":
          if quest.maxine_hook == "shovel":
            actions.append(["go","Sleep","quest_maxine_hook_shovel_home_bedroom_bed"])
        elif mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "listen_bedroom" and quest.lindsey_piano["lindsey_arrived"]:
            actions.append("?quest_lindsey_piano_listen_bedroom_bed")
            return
          elif quest.lindsey_piano in ("wait","cleaning","listen_bedroom"):
            actions.append("?quest_lindsey_piano_wait_bed")
            return
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "fun" and game.hour == 19:
            actions.append("?quest_kate_moment_fun_bed")
            return
        elif mc["focus"] == "flora_walk":
          if quest.flora_walk == "sleep":
            actions.append(["go","Sleep","quest_flora_walk_sleep"])
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.smash_or_pass.in_progress:
          if home_bedroom["alarm"] == "beeping":
            actions.append(["go","Sleep","?home_bedroom_bed_interact_smash_or_pass_beeping"])
          else:
            actions.append(["go","Sleep","?home_bedroom_bed_interact_smash_or_pass"])
        if quest.dress_to_the_nine.in_progress:
          actions.append(["go","Sleep","?home_bedroom_bed_interact_dress_to_the_nine"])
        if quest.back_to_school_special.in_progress:
          actions.append(["go","Sleep","?home_bedroom_bed_interact_back_to_school_special"])
        if quest.kate_blowjob_dream.in_progress:
          if quest.kate_blowjob_dream == "sleep":
            actions.append(["go","Sleep","home_bedroom_bed_interact_kate_blowjob_dream_sleep"])
#         elif quest.kate_blowjob_dream == "alarm":
#           actions.append(["go","Sleep","?home_bedroom_bed_interact_kate_blowjob_alarm"])
          elif quest.kate_blowjob_dream >= "courage_badge":
            actions.append(["go","Sleep","?home_bedroom_bed_interact_kate_blowjob_dream_courage_badge"])
            return
        if quest.jacklyn_sweets in ("bedroom","text"):
          actions.append("?quest_jacklyn_sweets_bedroom_bed")
          return
        if quest.flora_squid == "sleep":
          actions.append(["go","Sleep","quest_flora_squid_sleep"])
        if quest.jo_washed.finished and not quest.fall_in_newfall.started:
          actions.append(["go","Sleep","quest_fall_in_newfall_not_started_home_bedroom_bed"])
        if quest.fall_in_newfall in ("break_over","assembly"):
          actions.append(["go","Sleep","?quest_fall_in_newfall_break_over_home_bedroom_bed"])
        if quest.mrsl_HOT.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and not quest.mrsl_bot.started:
          actions.append(["go","Sleep","quest_mrsl_bot_start"])
      if not (quest.kate_blowjob_dream in ("courage_badge","sleep","open_door","get_dressed","school","alarm")
      or quest.jacklyn_statement in ("clothes", "datedoor", "jackdate","intruder","florasad")
      or quest.isabelle_locker in ("bedroom","email","wait")
      or quest.jacklyn_sweets in ("bedroom","text")
      or quest.flora_squid == "sleep"
      or quest.lindsey_piano in ("wait","cleaning","listen_bedroom")
      or quest.fall_in_newfall in ("break_over","assembly")
      or (quest.kate_moment.in_progress and not ((quest.kate_moment == "coming_soon" and quest.isabelle_gesture.in_progress) or quest.kate_moment == "sleep"))
      or (quest.flora_walk in ("xcube","park","confrontation","caught","forest_glade") and game.hour not in (19,20))
      or quest.maya_quixote in ("power_outage","attic")
      or quest.lindsey_voluntary == "dream"
      or quest.jacklyn_town in ("suit","hairdo","ready","flora")
      or quest.mrsl_bot == "dream"
      or not quest.day1_take2.started):
        actions.append(["go","Sleep","home_bedroom_bed_interact_sleep"])
      if quest.kate_blowjob_dream >= "open_door":
        actions.append("?home_bedroom_bed_interact_kate_blowjob_open_door")
      actions.append("?home_bedroom_bed_interact_isabelle_tour")


label home_bedroom_bed_interact_smash_or_pass_beeping:
  "With that blasted thing going off, there ain't no rest for the wicked."
  return

label home_bedroom_bed_interact_smash_or_pass:
  "Ah, the softness of my pillow! Going back to sleep is one of my last few pleasures in life."
  "..."
  "Those texts last night. That's so weird. No one has texted me in years. How did that person even get my number?"
  "No more sleep for me, it seems. Might as well get up. The bathroom calls."
  return

label home_bedroom_bed_interact_dress_to_the_nine:
  "Rest is for the dead... and those who don't have a bus to catch. The former would be my choice if I had one."
  return

label home_bedroom_bed_interact_back_to_school_special:
  "If only. But [jo]'s waiting downstairs."
  return

label home_bedroom_bed_interact_isabelle_tour:
  "Thread count: 400."
  "Girl count: 0."
  return

label home_bedroom_bed_interact_kate_blowjob_dream_courage_badge:
  "Sleeping would be nice, but the new porn collection isn't going to start itself. Now's a good time for it."
  "Always sleep better after getting off, anyway."
  return

label home_bedroom_bed_interact_kate_blowjob_dream_sleep:
  "Ah, finally! There's nothing better than lying down after a long day."
  "My mind needs rest for all the new challenges tomorrow."
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  $game.advance(hours=3)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "..."
  "Trying to fall asleep is one of my least favorite activities."
  "My mind keeps wandering to old embarrassments and failures."
  play sound "<from 0.25 to 1.0>phone_vibrate"
  "Huh?"
  "Was that my phone?"
  show black onlayer screens zorder 100
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  $set_dialog_mode("phone_message_centered","hidden_number")
  hidden_number "I really shouldn't be messaging you, but I just wanted to say that you've done well today."
  hidden_number "Some mistakes, granted. But you're still learning."
  "Who are you? Seriously? Why do you keep spying on me?"
  hidden_number "I'm no one special, but I am proud of you."
  window auto
  $set_dialog_mode("phone_message_plus_textbox")
  "Why is everyone suddenly proud of me! There's no way the old me would've had it this easy."
  "To be honest, it still feels like a practical joke."
  "They must have an agenda in all of this, else they wouldn't be so persistent."
  window hide
  $set_dialog_mode("phone_message_centered","hidden_number")
  "Can we meet? I'd like to talk to you about a few things."
  window auto
  $set_dialog_mode("phone_message_plus_textbox")
  "..."
  "..."
  "..."
  "No answer. How typical."
  hide screen phone
  "Oh, well..."
  "Sleep, take me!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  while game.hour in (19,20,21,22,23,0,1,2,3,4,5,6):
    $game.advance()
  $quest.kate_blowjob_dream.advance("flora_knocking")
  $quest.kate_blowjob_dream["dream_start"] = game.day
  pause 1.5
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  return

label home_bedroom_bed_interact_kate_blowjob_open_door:
  "So many crusty white stains on these sheets..."
  "I really need to stop drinking milk in bed."
  return

label home_bedroom_bed_interact_kate_blowjob_alarm:
  "Could definitely sleep for a while longer, but once prone it's hard to get up again."
  return

label home_bedroom_bed_interact_sleep:
  menu(side="middle"):
    "What time is it?"
    "Sleeping time":
      "My work here is done."
      window hide
      label home_bedroom_bed_interact_force_sleep:
      show black onlayer screens zorder 100 with Dissolve(3.0)
      $x = game.day-1 if game.hour < 7 else game.day
      while game.day != x+1:
          if quest.jo_potted >= "seeds" and quest.jo_potted < "big":
            if "ground" in school_forest_glade["birds"].values() or "drugged" in school_forest_glade["birds"].values():
              if game.hour not in (19,20,21,22,23,0,1,2,3,4,5,6):
                pause 0.25
                show black onlayer screens zorder 4
                $set_dialog_mode("default_no_bg")
                "An evil energy disturbs my sleep."
                "Darkness on the horizon."
                "Black eyes. Beaks and talons."
                "Wings of shadow."
                "Ravaging the holy land — the field of glory."
                "It seems to be coming from the forest glade."
                $set_dialog_mode()
                pause 0.25
                hide black onlayer screens with Dissolve(.5)
                window auto
                return
          if quest.jacklyn_statement == "dinner" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress or quest.mrsl_table == "sleep" or quest.kate_desire == "rest" or quest.maxine_hook in ("sleep","night","daybefore")):
            $quest.jacklyn_statement["doorbell_broken"] = True
          if quest.mrsl_table == "sleep" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress or quest.maxine_hook in ("sleep","night","daybefore")):
            $quest.mrsl_table["dream_sequence"] = True
          if quest.kate_desire == "rest" and not quest.kate_desire["rested"] and not (quest.kate_blowjob_dream.in_progress or quest.maxine_hook in ("sleep","night","daybefore")):
            $quest.kate_desire["rested"] = True
          if quest.maxine_hook == "sleep" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress):
            $quest.maxine_hook.advance("wake")
          if quest.maxine_hook in ("night","daybefore") and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress):
            $quest.maxine_hook.advance("day")
          if quest.lindsey_motive == "sleep" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress):
            $quest.lindsey_motive.advance("lindsey")
          $game.advance()
      while game.hour != 7:
          if quest.jo_potted >= "seeds" and quest.jo_potted < "big":
            if "ground" in school_forest_glade["birds"].values() or "drugged" in school_forest_glade["birds"].values():
              if game.hour not in (19,20,21,22,23,0,1,2,3,4,5,6):
                pause 0.25
                show black onlayer screens zorder 4
                $set_dialog_mode("default_no_bg")
                "An evil energy disturbs my sleep."
                "Darkness on the horizon."
                "Black eyes. Beaks and talons."
                "Wings of shadow."
                "Ravaging the holy land — the field of glory."
                "It seems to be coming from the forest glade."
                $set_dialog_mode()
                pause 0.25
                hide black onlayer screens with Dissolve(.5)
                window auto
                return
          if quest.jacklyn_statement == "dinner" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress or quest.mrsl_table == "sleep" or quest.kate_desire == "rest" or quest.maxine_hook in ("sleep","night","daybefore")):
            $quest.jacklyn_statement["doorbell_broken"] = True
          if quest.mrsl_table == "sleep" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress or quest.maxine_hook in ("sleep","night","daybefore")):
            $quest.mrsl_table["dream_sequence"] = True
          if quest.kate_desire == "rest" and not quest.kate_desire["rested"] and not (quest.kate_blowjob_dream.in_progress or quest.maxine_hook in ("sleep","night","daybefore")):
            $quest.kate_desire["rested"] = True
          if quest.maxine_hook == "sleep" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress):
            $quest.maxine_hook.advance("wake")
          if quest.maxine_hook in ("night","daybefore") and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress):
            $quest.maxine_hook.advance("day")
          if quest.lindsey_motive == "sleep" and not (quest.flora_bonsai.in_progress or quest.kate_blowjob_dream.in_progress):
            $quest.lindsey_motive.advance("lindsey")
          $game.advance()
      ## The call statement and visit function below are for randomizing the dollar bills in the bedroom after the player sleeps
      if quest.maya_sauce["bedroom_taken_over"]:
        call goto_home_hall
        $home_hall.visit()
      else:
        call goto_home_bedroom
        $home_bedroom.visit()
      hide black onlayer screens with Dissolve(.5)
      window auto
    "Brooding time":
      "Rest is for the dead."
      "And those who are afraid of energy drinks."
  return

label home_bedroom_bed_interact_jacklyn_statement:
  "Nothing under here apart from old dust and playboy bunnies."
  return
