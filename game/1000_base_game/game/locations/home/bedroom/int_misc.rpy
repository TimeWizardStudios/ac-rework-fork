init python:
  class Interactable_home_bedroom_dollar3(Interactable):

    def title(cls):
      return "Money"

    def description(cls):
      return ""

    def actions(cls,actions):
      if quest.kate_blowjob_dream == "school":
        actions.append("kate_blowjob_dream_random_interact")
      else:
        actions.append(["take","Yoink","home_bedroom_dollar3_take"])


label home_bedroom_dollar3_take:
  $home_bedroom["dollar3_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_bedroom_dollar2(Interactable):

    def title(cls):
      return "Money"

    def description(cls):
      return ""

    def actions(cls,actions):
      if quest.kate_blowjob_dream == "school":
        actions.append("kate_blowjob_dream_random_interact")
      else:
        actions.append(["take","Yoink","home_bedroom_dollar2_take"])


label home_bedroom_dollar2_take:
  $home_bedroom["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_bedroom_dollar1(Interactable):

    def title(cls):
      return "Money"

    def description(cls):
      return ""

    def actions(cls,actions):
      if quest.kate_blowjob_dream == "school":
        actions.append("kate_blowjob_dream_random_interact")
      else:
        actions.append(["take","Yoink","home_bedroom_dollar1_take"])


label home_bedroom_dollar1_take:
  $home_bedroom["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_bedroom_book(Interactable):

    def title(cls):
      return "Book"

    def description(cls):
      return ""

    def actions(cls,actions):
      if quest.kate_blowjob_dream == "school":
        actions.append("kate_blowjob_dream_random_interact")
      else:
        actions.append(["interact","Read","?home_bedroom_book_interact"])


label home_bedroom_book_interact:
  $home_bedroom["book_taken"] = True
  "Hmm... interesting book..."
  "{i}\"Big Words — the lazy intellectual's guide to sounding smart.\"{/}"
  $mc.intellect+=1
  return
