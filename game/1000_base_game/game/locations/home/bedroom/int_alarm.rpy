init python:
  class Interactable_home_bedroom_alarm(Interactable):

    def title(cls):
      if quest.kate_blowjob_dream in ("open_door","get_dressed","school") and not home_bedroom["alarm"] == "smashed_again":
        return "Alarm"
      elif home_bedroom["alarm"] == "smashed" or home_bedroom["alarm"] == "smashed_again":
        return "Broken Alarm"
      else:
        return "Alarm"

    def description(cls):
      if (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.kate_blowjob_dream == "alarm":
        return "Hell's Siren. Condemned to the eternal pits by the trumpets of heaven. A fate worse than death."
#     elif quest.kate_blowjob_dream == "get_dressed":
#       if home_bedroom["alarm"]=="smashed":
#         return "A new clock. I go through these like porn sites."
#       else:
#         return "The Grim Reaper. The Great Destroyer. The Eater of Worlds.\n\nTime."
      elif quest.kate_blowjob_dream >= "courage_badge":
        if home_bedroom["alarm"]=="off":
          return "It's a clock. A new one.\n\n...\n\nWait, a new one?"
        elif home_bedroom["alarm"]=="smashed":
          return "The past does not exist. Nor does the future. There is only the now."
        elif home_bedroom["alarm"]=="beeping":
          return "{i}*BEEP BEEP BEEP BEEP BEEP BEEP BEEP BEEP*{/}"
      elif quest.back_to_school_special.in_progress:
        return "{i}\"Although the most acute judges of the watches, and even the watches themselves, were convinced of the guilt of watchery; the guilt was nevertheless non-existent. It is thus with all guilt.\"{/}"
      elif quest.dress_to_the_nine.in_progress:
        if home_bedroom["alarm"]=="smashed":
          return "The vanquished foe lies shattered and broken on the field of glory... also known as, my nightstand."
        elif home_bedroom["alarm"]=="off":
          return "A vile device of torture. Tested on the innocent and unsuspecting, time and time again."
      elif quest.smash_or_pass.ended:
        if home_bedroom["alarm"]=="smashed":
          return "I should've sent this vile machine to the shadow realm ages ago. At least, it won't bother me again."
        elif home_bedroom["alarm"]=="off":
          return "Finally, some peace and quiet. More sleep!"
      elif home_bedroom["alarm"]=="beeping":
        return "{i}*BEEP BEEP BEEP BEEP BEEP BEEP BEEP BEEP!*{/}"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream in ("open_door","get_dressed","school"):
            actions.append("kate_blowjob_dream_random_interact")
#         elif quest.kate_blowjob_dream == "get_dressed":
#           if home_bedroom["alarm"] in ("off_again", "smashed_again"):
#             actions.append("?home_bedroom_alarm_interact_kate_blowjob_dream_get_dressed_off")
#           else: ###tbd test here
#             actions.append("?home_bedroom_alarm_interact_kate_blowjob_dream_get_dressed_smash")
          elif quest.kate_blowjob_dream == "alarm":
            if home_bedroom["alarm"] == "smashed":
              actions.append("home_bedroom_alarm_interact_kate_blowjob_dream_alarm_smashed_again")
            else:
              actions.append("home_bedroom_alarm_interact_kate_blowjob_dream_alarm_off_again")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.smash_or_pass.started and home_bedroom["alarm"] == "beeping":
          actions.append("home_bedroom_alarm_interact_smash_or_pass_beeping")
        if quest.kate_blowjob_dream >= "courage_badge":
          if home_bedroom["alarm"] == "beeping":
            actions.append("?home_bedroom_alarm_interact_kate_blowjob_dream_courage_badge_beeping")
          elif home_bedroom["alarm"] == "off":
            actions.append("?home_bedroom_alarm_interact_kate_blowjob_dream_courage_badge_off")
          elif home_bedroom["alarm"] == "smashed":
            actions.append("?home_bedroom_alarm_interact_kate_blowjob_dream_courage_badge_smashed")
      if quest.dress_to_the_nine.started:
        if home_bedroom["alarm"] == "smashed":
          actions.append("?home_bedroom_alarm_interact_dress_to_the_nine_smashed")
        elif home_bedroom["alarm"] == "off":
          actions.append("?home_bedroom_alarm_interact_dress_to_the_nine_off")
      if quest.smash_or_pass.ended:
        if home_bedroom["alarm"] == "smashed":
          actions.append("?home_bedroom_alarm_interact_smash_or_pass_smashed")
        elif home_bedroom["alarm"] == "off":
          actions.append("?home_bedroom_alarm_interact_smash_or_pass_off")


label home_bedroom_alarm_interact_smash_or_pass_beeping:
  "What should I do with this thing?"
  menu(side="middle"):
    extend ""
    "Smash it":
      $home_bedroom["alarm"]="smashed"
      $home_bedroom["alarm_smash_combo"]+=1
      play sound "alarm_smash"
      play music "home_theme"
      if quest.smash_or_pass.in_progress:
        $mc.strength+=1
        $home_bedroom["alarm_smash_combo"] = 1
        "Ding-dong, the watch is dead!" with vpunch
        "Who knew a proper smashing would feel so good? The testosterone! The sheer power!"
        "Ugh, I'm pathetic."
        "There's no need to be up for several more hours. Why the hell did my alarm go off so early?"
        $quest.smash_or_pass.finish("smashed")
    "Turn it off":
      $home_bedroom["alarm"]="off"
      play music "home_theme"
      if quest.smash_or_pass.in_progress:
        $mc.intellect+=1
        $home_bedroom["alarm_smash_combo"] = -1
        "Smashing it would've been nice, but at least I don't have to clean up the pieces now."
        "There's no need to be up for several more hours. Why the hell did my alarm go off so early?"
        $quest.smash_or_pass.finish("turned_off")
    "Leave it":
      "The melody of a better time. Might as well let it bump."
  return

label home_bedroom_alarm_interact_smash_or_pass_smashed:
  "You tell me, \"All I've done is create more despair and destruction in my environment.\""
  "I ask you, \"Why did it feel so good then?\""
  return

label home_bedroom_alarm_interact_smash_or_pass_off:
  "Those deprived of sympathy can still break the circle of abuse. Jesus turned the other cheek and so can I."
  "Except if there are clock-merchants involved, those get righteously whipped out of the temple."
  return


label home_bedroom_alarm_interact_dress_to_the_nine_off:
  "Now is not the time. Unless we're talking about time itself... then, it's always time."
  return

label home_bedroom_alarm_interact_dress_to_the_nine_smashed:
  "This thing won't be working anytime soon... or, with some luck, ever again."
  return

label home_bedroom_alarm_interact_kate_blowjob_dream_courage_badge_smashed:
  "Huh, [jo] already bought me a new alarm clock. She's always been relentless in her quest to keep me sleep-deprived."
  return

label home_bedroom_alarm_interact_kate_blowjob_dream_courage_badge_off:
 "Whenever this bastard wakes me up the urge to smash it grows. But patience is a virtue, or so they say."
 return

label home_bedroom_alarm_interact_kate_blowjob_dream_courage_badge_beeping:
  "To be honest, I'm more afraid of [flora] than I am of losing my hearing."
  "Better open the door first."
  "Or else soon I might not have a door to open..."
  return

#label home_bedroom_alarm_interact_kate_blowjob_dream_get_dressed_smash:
# "Must've slept through the damn thing. Who knew time travel would be so exhausting?"
# "[jo] replaced it yesterday and it's already malfunctioning."
# "This piece of garbage ruins my nights and days."
# menu(side="middle"):
#   extend ""
#   "Smash it":
#     #SFX: Smashing clock sound
#     ## Alarm smash
#     play sound "alarm_smash"
#     $home_bedroom["alarm"]="smashed_again"
#     $home_bedroom["alarm_smash_combo"]+=1
#     "Dead again!" with vpunch
#   "Show mercy":
#     $home_bedroom["alarm"]="off_again"
#     $home_bedroom["alarm_smash_combo"]-=1
#     "It's his first day on the job. He deserves another chance."
# return

#label home_bedroom_alarm_interact_kate_blowjob_dream_get_dressed_off:
# "Nine million, nine hundred eighty-six thousand minutes."
# "We actually sat down and did the math."
# return

label home_bedroom_alarm_interact_kate_blowjob_dream_alarm_smashed_again:
  menu(side="middle"):
    "Smash it again!":
      play sound "alarm_smash"
      play music "home_theme"
      $home_bedroom["alarm"] = "smashed_again"
      $quest.kate_blowjob_dream.advance("alarm_smashed")
      "Dead again!" with vpunch
      "[jo] really has to stop buying me new alarms..."
      $achievement.end_of_time.unlock()
    "Let the new clock live... for now":
      $home_bedroom["alarm"] = "off"
      play music "home_theme"
      "Good. This room could quickly turn into a clock cemetery."
  if quest.kate_blowjob_dream["bj_received"] == "yes":
    jump home_bedroom_alarm_interact_kate_blowjob_dream_alarm_bj_received
  elif quest.kate_blowjob_dream["bj_received"] == "no":
    jump home_bedroom_alarm_interact_kate_blowjob_dream_alarm_not_bj_received

label home_bedroom_alarm_interact_kate_blowjob_dream_alarm_off_again:
  menu(side="middle"):
    "Smash it! Enough is enough!":
      play sound "alarm_smash"
      play music "home_theme"
      $home_bedroom["alarm"] = "smashed"
      $quest.kate_blowjob_dream.advance("alarm_smashed")
      "Foul beast of the abyss! Return from whence you came!" with vpunch
    "Calm the storm, no one dies today":
      $home_bedroom["alarm"] = "off"
      play music "home_theme"
      "Good. This room could quickly turn into a clock cemetery."
      $achievement.eye_of_the_storm.unlock()
  if quest.kate_blowjob_dream["bj_received"] == "yes":
    jump home_bedroom_alarm_interact_kate_blowjob_dream_alarm_bj_received
  elif quest.kate_blowjob_dream["bj_received"] == "no":
    jump home_bedroom_alarm_interact_kate_blowjob_dream_alarm_not_bj_received
