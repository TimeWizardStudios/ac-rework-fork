init python:
  class Interactable_home_bedroom_ornamental_box(Interactable):

    def title(cls):
      return "Black Ornamental Box"

    def description(cls):
      if (quest.mrsl_table == "morning"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "An ornamental box, found in\nthe basement.\n\nIt probably belongs to [flora]."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?home_bedroom_ornamental_box_interact")


label home_bedroom_ornamental_box_interact:
  "Black as my soul. Empty as my heart."
  "It looks good on the surface, though. A facade!"
  return
