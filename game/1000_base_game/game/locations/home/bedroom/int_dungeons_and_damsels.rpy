init python:
  class Location_home_dungeons_and_damsels(Location):

    default_title = "  Dungeons and Damsels  "

    def scene(self,scene):
      scene.append([(0,0),"home bedroom dungeons_and_damsels sky"])
      scene.append([(0,132),"home bedroom dungeons_and_damsels paths"])
      scene.append([(0,0),"home bedroom dungeons_and_damsels town"])
      scene.append([(790,483),"home bedroom dungeons_and_damsels sign"])
      scene.append([(913,44),"home bedroom dungeons_and_damsels dragon"])
      scene.append([(1142,422),"home bedroom dungeons_and_damsels goblins"])
      scene.append([(1363,540),"home bedroom dungeons_and_damsels fairies"])
      scene.append([(1159,0),"home bedroom dungeons_and_damsels tree"])


label goto_home_dungeons_and_damsels:
  call goto_with_black(home_dungeons_and_damsels)
  return
