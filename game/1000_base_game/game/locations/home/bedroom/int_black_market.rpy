init python:
  class Character_black_market(BaseChar):

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("ball_of_yarn")
      self.add_item("stick")
      # self.add_item("mrsl_brooch") ## Taken care of automatically by the 'on_quest_finished' event in \quests\season_1\isabelle_haggis.rpy
      self.add_item("nymphoria_heartus")
      self.add_item("venus_vagenis")
      self.add_item("bag_of_weed")
      self.add_item("beaver_pelt")
      self.add_item("baseball_bat")
      self.add_item("sleeping_pills")
      # self.add_item("cutie_harlot") ## Taken care of automatically by the 'on_quest_started' event in \quests\season_1\flora_handcuffs.rpy
      # self.add_item("hex_vex_and_texmex") ## Taken care of automatically by the 'on_quest_advanced' event in \quests\season_2\maya_witch.rpy
      # self.add_item("goat") ## Taken care of automatically by the 'on_quest_advanced' event in \quests\season_2\maxine_dive.rpy
      # self.add_item("milk") ## Taken care of automatically by the 'on_quest_advanced' event in \quests\season_2\maxine_dive.rpy


init python:
  class Location_home_computer(Location):

    default_title = "Bedroom"

    def scene(self,scene):
      if 20 > game.hour > 6 and quest.maya_sauce["bedroom_taken_over"]:
        scene.append([(0,0),"home bedroom black_market computer_day_alt"])
      elif 20 > game.hour > 6:
        scene.append([(0,0),"home bedroom black_market computer_day"])
      elif quest.maya_sauce["bedroom_taken_over"]:
        scene.append([(0,0),"home bedroom black_market computer_night_alt"])
      else:
        scene.append([(0,0),"home bedroom black_market computer_night"])
      scene.append([(322,95),"home bedroom black_market background"])
      scene.append([(933,948),"home bedroom black_market exit_arrow","home_computer_exit_arrow"])

    def find_path(self,target_location):
      return "home_computer_exit_arrow"


label goto_home_computer:
  scene black with Dissolve(.07)
  $game.location = home_computer
  $renpy.pause(.07)
  $renpy.get_registered_image("location").reset_scene()
  show screen black_market
  scene location
  with Dissolve(.5)
  return


init python:
  class Interactable_home_computer_exit_arrow(Interactable):

    def title(cls):
      return "Back"

    def actions(cls,actions):
      if quest.maya_sauce["bedroom_taken_over"]:
        actions.append(["go","Hallway","exit_home_computer"])
      else:
        actions.append(["go","Bedroom","exit_home_computer"])


label exit_home_computer:
  hide screen black_market
  scene black
  with Dissolve(.07)
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $renpy.pause(.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location with Dissolve(.5)
  return


screen black_market(confirm_purchase=None, processing_payment=None, order_shipped=None):
  vpgrid id("black_market"):
    cols 4 draggable True mousewheel True pagekeys True xpos 322 xsize 1328-18 xspacing 18 ypos 95 ysize 676 yspacing 24
    left_margin 122 top_margin 178 bottom_margin 20
    for item in reversed(black_market.inv):
      python:
        item = items_by_id[item[0]]
        alt_title = {
          "ball_of_yarn": "Ball of Yarn x5",
          "mrsl_brooch": "Brooch",
          "hex_vex_and_texmex": "\"Hex, Vex, and TexMex\" by N.S.",
          "milk": "Goat Milk"}
        price = {
          "ball_of_yarn": 777,
          "stick": 20,
          "mrsl_brooch": 666,
          "nymphoria_heartus": 69,
          "venus_vagenis": 69,
          "bag_of_weed": 420,
          "beaver_pelt": 1111,
          "baseball_bat": 456,
          "sleeping_pills": 843,
          "cutie_harlot": 2499,
          "hex_vex_and_texmex": 666,
          "goat": 666,
          "milk": 66}
      frame:
        xsize 256 ysize 227
        background "home bedroom black_market item_slot"
        frame:
          xalign 0.5 xsize 95 yalign 0.14 ysize 95
          background item.icon
        text (alt_title[item.id] if item in [items for items in alt_title] else item.title()).upper() color "#FFF" font "fonts/GothamHTF-Bold.ttf" size 18 text_align 0.5 xalign 0.5 xmaximum 210 yalign 0.64
        button:
          padding (15,7,15,8) xalign 0.5 yalign 0.88
          background (AlphaMask("#452525", Frame("home bedroom black_market button_idle",10,10,10,10)) if quest.maxine_dive in ("line","milk","package","trade") and item == "ball_of_yarn" else Frame("home bedroom black_market button_idle",10,10,10,10) if mc.money >= price[item.id] else AlphaMask("#283634", Frame("home bedroom black_market button_idle",10,10,10,10))) hover_background Frame("home bedroom black_market button_hover",10,10,10,10)
          text ("SOLD OUT" if quest.maxine_dive in ("line","milk","package","trade") and item == "ball_of_yarn" else "BUY FOR $" + str(price[item.id])) color ("#452525" if quest.maxine_dive in ("line","milk","package","trade") and item == "ball_of_yarn" else "#52B2A4" if mc.money >= price[item.id] else "#283634") hover_color "#222" font "fonts/GothamHTF-Bold.ttf" size 18
          action (None if (quest.maxine_dive in ("line","milk","package","trade") and item == "ball_of_yarn") or (quest.maxine_dive == "milk" and quest.maxine_dive["black_market_milk"] and not quest.maxine_dive["shady_site_pulled_up"]) else (If(mc.money >= price[item.id], Show("black_market", confirm_purchase=item))))

  if len(black_market.inv) > 8:
    vbar value YScrollValue("black_market") base_bar AlphaMask("#F1F1F1", Crop((1310,0,1328-1310,676),"home bedroom black_market background")) thumb HBox(Solid("#F1F1F1",xsize=2),Solid("#C0C0C0",xsize=13),Solid("#F1F1F1",xsize=2)) hover_thumb HBox(Solid("#F1F1F1",xsize=2),Solid("#A8A8A8",xsize=13),Solid("#F1F1F1",xsize=2)) bottom_gutter 24 top_gutter 32 xpos 1632 xsize 18 ypos 95 ysize 676
    add Crop((0,0,1328-18,676-520),"home bedroom black_market background") xpos 322 ypos 95

  if confirm_purchase or processing_payment or order_shipped:
    button:
      xpos 322 xsize 1328-(18 if len(black_market.inv) > 8 else 0) ypos 95 ysize 676
      frame:
        background AlphaMask("#00000099", Crop((0,0,1328-(18 if len(black_market.inv) > 8 else 0),676),"home bedroom black_market background"))
      background AlphaMask("#00000099", Crop((0,0,1328-(18 if len(black_market.inv) > 8 else 0),676),"home bedroom black_market background"))
      action NullAction()

    if confirm_purchase:
      python:
        item = confirm_purchase
      hbox:
        xalign 0.5 xoffset 15 yalign 0.45 yoffset -10
        frame:
          xsize 256 yalign 0.5 ysize 256
          background item.icon.replace("items ","items black_market ")
        null width 50
        vbox:
          vbox:
            xalign 0.5
            text "CONFIRM" font "fonts/GothamHTF-Bold.ttf" size 36 text_align 0.5 xalign 0.5
            text "YOUR PURCHASE" font "fonts/GothamHTF-Bold.ttf" size 36 text_align 0.5 xalign 0.5
          null height 50
          vbox:
            xalign 0.5
            text (alt_title[item.id] if item in [items for items in alt_title] else item.title()).upper() color "#52B2A4" font "fonts/GothamHTF-Bold.ttf" size 18 text_align 0.5 xalign 0.5 xmaximum 210
            text "$" + str(price[item.id]) font "fonts/GothamHTF-Bold.ttf" size 18 text_align 0.5 xalign 0.5
          null height 50
          hbox:
            xalign 0.5
            button:
              padding (15,7,15,8) xalign 0.5
              background Frame("home bedroom black_market button_idle",10,10,10,10) hover_background Frame("home bedroom black_market button_hover",10,10,10,10)
              text "CONFIRM" color "#52B2A4" hover_color "#222" font "fonts/GothamHTF-Bold.ttf" size 18
              action Show("black_market", processing_payment=item)
            null width 25
            button:
              padding (15,7,15,8) xalign 0.5
              background Frame("home bedroom black_market button_idle",10,10,10,10) hover_background Frame("home bedroom black_market button_hover",10,10,10,10)
              text "CANCEL" color "#52B2A4" hover_color "#222" font "fonts/GothamHTF-Bold.ttf" size 18
              action Show("black_market")

    elif processing_payment:
      python:
        item = processing_payment
        old_value = 0.0
      vbox:
        xalign 0.5 xoffset 30 yalign 0.45 yoffset 20
        text "PROCESSING PAYMENT" font "fonts/GothamHTF-Bold.ttf" size 18 xalign 0.5
        null height 5
        bar:
          xalign 0.5 xsize 200 ysize 20
          left_bar Frame("home bedroom black_market button_hover",10,10,10,10) right_bar Frame("home bedroom black_market button_idle",10,10,10,10)
          value AnimatedValue(value=1.0, range=1.0, delay=2.0, old_value=old_value)
      timer 2.0 action [Function(black_market.remove_item, item), SetVariable("mc.money", mc.money-price[item.id]), If(item == "cutie_harlot", Jump("quest_flora_handcuffs_order_cutie_harlot")), If(item == "hex_vex_and_texmex", Jump("quest_maya_witch_black_market")), If(item == "goat", Jump("quest_maxine_dive_milk_black_market_milk_goat")), If(item == "milk", Jump("quest_maxine_dive_milk_black_market_milk_milk")), Show("black_market", order_shipped=item)]

    elif order_shipped:
      python:
        item = order_shipped
      vbox:
        xalign 0.5 xoffset 30 yalign 0.45 yoffset -10
        vbox:
          xalign 0.5
          text "THANK YOU FOR" font "fonts/GothamHTF-Bold.ttf" size 36 text_align 0.5 xalign 0.5
          text "YOUR ORDER" font "fonts/GothamHTF-Bold.ttf" size 36 text_align 0.5 xalign 0.5
        null height 50
        vbox:
          xalign 0.5
          text "ESTIMATED DELIVERY" color "#52B2A4" font "fonts/GothamHTF-Bold.ttf" size 18 text_align 0.5 xalign 0.5
          text game.dow_names[0 if game.dow == 6 else game.dow+1].upper() font "fonts/GothamHTF-Bold.ttf" size 18 text_align 0.5 xalign 0.5
        null height 50
        button:
          padding (15,7,15,8) xalign 0.5
          background Frame("home bedroom black_market button_idle",10,10,10,10) hover_background Frame("home bedroom black_market button_hover",10,10,10,10)
          text "CONTINUE SHOPPING" color "#52B2A4" hover_color "#222" font "fonts/GothamHTF-Bold.ttf" size 18
          action (None if (item == "cutie_harlot" and quest.flora_handcuffs == "order") or (item == "hex_vex_and_texmex" and quest.maya_witch == "black_market") or (item in ("goat","milk") and quest.maxine_dive == "milk" and quest.maxine_dive["black_market_milk"]) else Show("black_market"))
