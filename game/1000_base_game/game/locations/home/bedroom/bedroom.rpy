image alarm_noise:
  "sound_lines"
  zoom 0.15
  easein 0.5 yoffset -25 xoffset -25 alpha 1.0 zoom 0.25
  easeout 0.5 yoffset 0 xoffset 0 alpha 0.0 zoom 0.15
  repeat


init python:
  class Location_home_bedroom(Location):

    default_title="Bedroom"

    def __init__(self):
      super().__init__()
      self["alarm"]="beeping"
      self["flash_drive_taken"]=False

    def scene(self,scene):
      scene.append([(0,-50),"home bedroom bedroom"])

      if home_bedroom["clean"]:
        scene.append([(0,0),"home bedroom colored_walls"])
        scene.append([(197,150),"home bedroom painting"])

      #King of Sweets
      if not home_bedroom["clean"]:
        if home_bedroom["poster_removed"]:
          scene.append([(162,102),"home bedroom king_of_sweets_wall"])
        if home_bedroom["king_of_sweets"]:
          scene.append([(214,152),"home bedroom king_of_sweets",("home_bedroom_king_of_sweets",0,50)])

      if home_bedroom["clean"]:
        scene.append([(943,332),"home bedroom closet_clean","home_bedroom_closet"])
        scene.append([(1187,238),"home bedroom closet2_clean"])
        scene.append([(255,742),"home bedroom carpet_clean"])
      else:
        scene.append([(943,332),"home bedroom closet","home_bedroom_closet"])
        scene.append([(255,742),"home bedroom carpet"])
      scene.append([(736,406),"home bedroom door",("home_bedroom_door",0,-50)])
      if quest.maya_sauce["bedroom_taken_over"] and not quest.lindsey_voluntary == "dream":
        scene.append([(315,539),"home bedroom bed_maya"])
      elif home_bedroom["clean"]:
        scene.append([(315,539),"home bedroom bed_clean","home_bedroom_bed"])
      else:
        scene.append([(315,539),"home bedroom bed","home_bedroom_bed"])
      scene.append([(1419,658),"home bedroom drawers"])

      #Flash Drive
      if not home_bedroom["flash_drive_taken"]:
        scene.append([(1511,650),"home bedroom flash_drive","home_bedroom_flash_drive"])

      scene.append([(135,359),"home bedroom bookshelve_left"])
      if home_bedroom["clean"]:
        scene.append([(148,628),"home bedroom tv_clean"])
        if not (home_bedroom["controller_taken"] or (flora.at("home_bedroom","playing") and not flora.talking)):
          scene.append([(271,801),"home bedroom controller_clean","home_bedroom_controller"])
      else:
        scene.append([(148,628),"home bedroom tv"])
        if not (home_bedroom["controller_taken"] or (flora.at("home_bedroom","playing") and not flora.talking)):
          scene.append([(280,726),"home bedroom controller","home_bedroom_controller"])
      scene.append([(1330,32),"home bedroom bookshelves_right"])
      if home_bedroom["clean"] and quest.maya_sauce["bedroom_taken_over"] and not quest.lindsey_voluntary == "dream":
        scene.append([(1060,621),"home bedroom desk_clean_caseless"])
      elif home_bedroom["clean"]:
        scene.append([(1060,621),"home bedroom desk_clean"])
      elif quest.maya_sauce["bedroom_taken_over"] and not quest.lindsey_voluntary == "dream":
        scene.append([(1060,564),"home bedroom desk_caseless"])
      else:
        scene.append([(1060,564),"home bedroom desk"])

      #Alarm
      if quest.lindsey_voluntary == "dream":
        if quest.lindsey_voluntary["rooster_crowed"]:
          scene.append([(315,615),"home bedroom rooster","home_bedroom_rooster"])
        else:
          scene.append([(316,612),"home bedroom rooster_crowing"])
      elif home_bedroom["clean"] and not quest.maya_sauce["bedroom_taken_over"]:
        scene.append([(310,721),"home bedroom ornamental_box","home_bedroom_ornamental_box"])
      elif not quest.maya_sauce["bedroom_taken_over"]:
        if quest.kate_blowjob_dream in ("flora_knocking","open_door","get_dressed","school","awake","alarm"):
          scene.append([(305,704),"home bedroom alarm","home_bedroom_alarm"])
        elif home_bedroom["alarm"]=="smashed" or home_bedroom["alarm"]=="smashed_again":
          scene.append([(306,698),"home bedroom alarm_broken","home_bedroom_alarm"])
        else:
          scene.append([(305,704),"home bedroom alarm","home_bedroom_alarm"])

      #Flora
      if not flora.talking:
        if flora.at("home_bedroom","playing"):
          scene.append([(304,644),"home bedroom flora_playing_autumn",("flora",64,0)])
        elif flora.at("home_bedroom","sitting"):
          scene.append([(435,623),"home bedroom flora_sitting_autumn"])

      #Maya
      if maya.at("home_bedroom","sitting"):
        if not home_bedroom["clean"]:
          scene.append([(669,722),"home bedroom pillow"])
        if not maya.talking:
          scene.append([(588,671),"home bedroom maya_autumn"])

      #Computer
      if home_bedroom["clean"]:
        scene.append([(1554,498),"home bedroom table_lamp"])
      else:
        scene.append([(1534,469),"home bedroom statuettes"])
      if not quest.maya_sauce["bedroom_taken_over"] or quest.lindsey_voluntary == "dream":
        scene.append([(1143,485),"home bedroom small_pc","home_bedroom_computer"])
        # scene.append([(1112,484),"home bedroom big_pc","home_bedroom_big_pc"])
      if home_bedroom["capture_card"] and (not quest.maya_sauce["bedroom_taken_over"] or quest.lindsey_voluntary == "dream"):
        scene.append([(1158,502),"home bedroom capture_card_on",("home_bedroom_computer",0,-17)])
        scene.append([(1241,591),"home bedroom wire_vhs"])

      #Sugar Cubes
      if game.season == 1:
        if quest.flora_bonsai <= "sugar" and not (quest.kate_blowjob_dream in ("open_door","get_dressed","school") or quest.mrsl_table == "morning"):
          if not home_bedroom["sugarcube5_taken"]:
            scene.append([(1118,679),"home bedroom sugarcube5","home_bedroom_sugarcube5"])
          if not home_bedroom["sugarcube4_taken"]:
            scene.append([(1646,319),"home bedroom sugarcube4","home_bedroom_sugarcube4"])
          if not home_bedroom["sugarcube3_taken"]:
            scene.append([(138,840),"home bedroom sugarcube3","home_bedroom_sugarcube3"])

      #Various
      if not home_bedroom["clean"]:
        scene.append([(725,542),"home bedroom various"])
      if not quest.maya_sauce["bedroom_taken_over"] or quest.lindsey_voluntary == "dream":
        scene.append([(1159,600),"home bedroom chair"])
      if home_bedroom["clean"]:
        scene.append([(77,909),"home bedroom sports_magazine","home_bedroom_sports_magazine"])
      else:
        scene.append([(897,848),"home bedroom clothes"])
        scene.append([(77,909),"home bedroom porn_magazine",("home_bedroom_porn_mag",0,-0)])

      #Courage Badge
      if home_bedroom["courage_badge"] and (not quest.maya_sauce["bedroom_taken_over"] or quest.lindsey_voluntary == "dream"):
        scene.append([(307,435),"home bedroom badge","home_bedroom_courage_badge"])

      #Misc Quest
      if 0==1:
        scene.append([(581,510),"home bedroom tier1_painting"])#,"home_bedroom_tier1_painting"])
        scene.append([(581,510),"home bedroom tier2_painting"])#,"home_bedroom_tier2_painting"])

      #VHS
      if home_bedroom["vhs"]:
        scene.append([(1353,598),"home bedroom vhsplayer","home_bedroom_vhs_player"])

      #Lindsey best girl
      if lindsey.at("home_bedroom","sitting"):
        if not lindsey.talking:
          scene.append([(465,548),"home bedroom lindsey","lindsey"])

      #Isabelle
      if isabelle.at("home_bedroom","standing"):
        if not isabelle.talking:
          if game.season == 1:
            scene.append([(901,411),"home bedroom isabelle","isabelle"])
          elif game.season == 2:
            scene.append([(898,411),"home bedroom isabelle_autumn","isabelle"])

      #Maxine
      if maxine.at("home_bedroom","ouija_board"):
        if not maxine.talking:
          scene.append([(526,684),"home bedroom maxine",("maxine",-30,0)])
        scene.append([(471,817),"home bedroom ouijaboard"])

      if home_bedroom["clean"]:
        scene.append([(99,725),"home bedroom potted_plant"])
      else:
        if not maya.at("home_bedroom","sitting"):
          scene.append([(669,722),"home bedroom pillow"])
        scene.append([(475,832),"home bedroom pizza_box"])

      #Spinach
      if spinach.at("home_bedroom","licking"):
        if not spinach.talking:
          scene.append([(629,712),"home bedroom spinach",("spinach",-15,0)])

      #Money & Books
      if not (quest.kate_blowjob_dream in ("open_door","get_dressed","school")
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        if home_bedroom["dollar1_spawned_today"] == True and not home_bedroom["dollar1_taken_today"]:
          scene.append([(350,566),"home bedroom dollar1","home_bedroom_dollar1"])
        if home_bedroom["dollar2_spawned_today"] == True and not home_bedroom["dollar2_taken_today"]:
          scene.append([(1609,521),"home bedroom dollar2","home_bedroom_dollar2"])
        if home_bedroom["dollar3_spawned_today"] == True and not home_bedroom["dollar3_taken_today"]:
          scene.append([(1765,356),"home bedroom dollar3","home_bedroom_dollar3"])
        if not home_bedroom["book_taken"]:
          scene.append([(231,416),"home bedroom book","home_bedroom_book"])

      if quest.jacklyn_romance >= "suit_done" and (quest.jacklyn_town < "hairdo" or quest.jacklyn_town.failed) and not quest.lindsey_voluntary == "dream" and not quest.maya_sauce["bedroom_taken_over"]:
        scene.append([(1221,599),"home bedroom suit","home_bedroom_suit"])

      #Night Overlay
      scene.append([(0,0),"#home bedroom overlay"])
      if (home_bedroom["night"]
      or quest.maya_quixote in ("attic","board_game")):
        scene.append([(-16,-10),"#home bedroom overlay_night"])

      #Computer Screen
      if home_bedroom["small_pc"] == "google":
        scene.append([(1159,502),"home bedroom small_pc_google"])
      elif home_bedroom["small_pc"] == "wikipedia":
        scene.append([(1159,502),"home bedroom small_pc_wikipedia"])

    def find_path(self,target_location):
      ## there is only one way out, so we don't care about target location and other conditions
      return "home_bedroom_door"

label goto_home_bedroom:
  if home_bedroom.first_visit_today:
    $home_bedroom["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $home_bedroom["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $home_bedroom["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(home_bedroom)
  return
