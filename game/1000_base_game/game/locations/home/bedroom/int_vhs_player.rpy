init python:
  class Interactable_home_bedroom_vhs_player(Interactable):

    def title(cls):
      return "VHS Player"

    def description(cls):
      if (quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.mrsl_HOT >= "insert":
        return "One of those things that lost its value over night. Much like my semen after a good wank."
      else:
        return "Without a capture card this ancient box is useless."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.mrsl_HOT == "insert":
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:mrsl_HOT,vcr_tape|Use What?","home_bedroom_vcr_tape"])
      if quest.mrsl_HOT >= "insert":
        actions.append("?home_bedroom_vhs_player_interact_insert")
      actions.append("?home_bedroom_vhs_player_interact")


label home_bedroom_vcr_tape(item):
  if item == "vcr_tape":
    $mc.remove_item(item)
    "All right. In goes the tape..."
    "Let's see what's on this bad boy."
    $quest.mrsl_HOT.advance("watch")
  else:
    "\"Don't put your [item.title_lower] into the slot,\" they said. \"You'll ruin the machinery,\" they said."
    "Who is laughing now, VHS purists?"
    $quest.mrsl_HOT.failed_item("vcr_tape",item)
  return

label home_bedroom_vhs_player_interact_insert:
  "The only way to know if this  thing works is to press the power button."
  "..."
  "It works! And I didn't even have to oil the machinery or fill it up with diesel!"
  return

label home_bedroom_vhs_player_interact:
  "There's something special about putting a porn cassette into the slot."
  "If only I'd lived in the right day and age."
  return
