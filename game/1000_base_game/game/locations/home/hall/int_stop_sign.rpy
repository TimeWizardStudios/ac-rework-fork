init python:
  class Interactable_home_hall_stop(Interactable):
    def title(cls):
      return "Stop Sign"
    def description(cls):
      return "Desc"
    def actions(cls,actions):
      actions.append("?home_hall_stop_interact")

label home_hall_stop_interact:
  "label"
  return
