init python:
  class Interactable_home_hall_trail_down(Interactable):

    def title(cls):
      return "Wet Trail"

    def description(cls):
      return "A wet trail from [flora]'s room\nleads down the stairs."

    def actions(cls,actions):
      actions.append("?home_hall_trail_down_interact")


label home_hall_trail_down_interact:
  "Hmm... it's not that sticky..."
  "And it tastes a little salty."
  return
