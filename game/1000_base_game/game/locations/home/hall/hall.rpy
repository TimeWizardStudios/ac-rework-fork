init python:
  class Location_home_hall(Location):

    default_title="Hallway"

    def __init__(self):
      super().__init__()

    def scene(self,scene):
      scene.append([(0,-2),"home hall homehall"])
      scene.append([(82,706),"home hall stairs","home_hall_stairs"])
      scene.append([(620,85),"home hall hole",("home_hall_hole",0,330)])
      scene.append([(1572,63),"home hall door_right",("home_hall_door_bedroom",25,275)])
      scene.append([(1359,235),"home hall door_white",("home_hall_door_bathroom",15,90)])
      if quest.flora_squid >= "trail" and quest.flora_squid.in_progress:
        scene.append([(379,552),"home hall trail_down",("home_hall_trail_down",0,100)])
        scene.append([(1002,552),"home hall trail_toilet",("home_hall_trail_down",-545,100)])
      scene.append([(898,287),"home hall door_center","home_hall_door_flora"])
      scene.append([(440,251),"home hall door_left",("home_hall_door_jo",0,100)])
      scene.append([(937,345),"home hall back_poster",("home_hall_door_flora",-4,-58)])
      scene.append([(1650,320),"home hall stop",("home_hall_door_bedroom",31,18)])
      scene.append([(1265,352),"home hall cleaning_supply","home_hall_cleaning_supply"])
      scene.append([(1089,298),"home hall cabinet","home_hall_trphy_cabnet"])
      scene.append([(1439,274),"home hall shelves","home_hall_wall_cabinet"])
      scene.append([(1519,348),"home hall candle","home_hall_candle"])
      scene.append([(1475,340),"home hall freshener"])
      scene.append([(1517,260),"home hall fire"])
      scene.append([(1457,224),"home hall salt_lamp","home_hall_salt_lamp"])
      if maya.at("home_hall","trophy_lift") and not maya.talking:
        scene.append([(1105,312),"home hall trophies2",("home_hall_trphy_cabnet",9,-14)])
      else:
        scene.append([(1105,312),"home hall trophies",("home_hall_trphy_cabnet",9,-14)])
      scene.append([(1503,665),"home hall socket"])
      scene.append([(642,361),"home hall coat_hanger"])

      #Table & Lamp
      if not home_hall['table_taken']:
        #Table
        scene.append([(1345,550),"home hall table","home_hall_table"]) #table interactable
        if home_hall["note"]:
          scene.append([(1398,556),"home hall note","home_hall_note"])
        #Lamp
        if not home_hall['lamp_taken']:
          scene.append([(1374,448),"home hall lamp","home_hall_lamp"])
      else:
        scene.append([(684,484),"home hall table_back","home_hall_table"])
        if not home_hall['lamp_taken']:
          scene.append([(1405,638),"home hall lamp_ground","home_hall_lamp"])

      #Umbrellas
      scene.append([(592,457),"home hall umbrellas"])
      if not home_hall["umbrella_taken"]:
        scene.append([(624,451),"home hall blackumbrella","home_hall_umbrella"])
      scene.append([(598,448),"home hall redumbrella"])

      scene.append([(1441,736),"home hall shoes"])
      scene.append([(1859,568),"home hall bandaid"])

      #Shoebox
      if home_hall["shoebox"] and not home_hall["shoebox_taken"]:
        scene.append([(1523,824),"home hall shoebox","home_hall_shoebox"])

      #Machete
      if not home_hall["machete_taken"]:
        scene.append([(715,407),"home hall sword","home_hall_sword"])

      #Isabelle
      if ((not home_hall["exclusive"] or "isabelle" in home_hall["exclusive"])
      and isabelle.at("home_hall","standing")
      and not isabelle.talking):
        scene.append([(571,294),"home hall isabelle_autumn","isabelle"])

      #Photo
      scene.append([(739,289),"home hall family",("home_hall_family",0,240)])
      scene.append([(1864,123),"home hall sky"])
      scene.append([(320,288),"home hall flowers"])
      scene.append([(0,526),"home hall handle"])

      #Flora
      if ((not home_hall["exclusive"] or "flora" in home_hall["exclusive"])
      and flora.at("home_hall","texting")
      and not flora.talking):
        if game.season == 1:
          scene.append([(166,360),"home hall flora",("flora",-60,0)])
        elif game.season == 2:
          scene.append([(167,360),"home hall flora_autumn",("flora",-60,0)])

      #Maya
      if ((not home_hall["exclusive"] or "maya" in home_hall["exclusive"])
      and maya.at("home_hall","trophy_lift")
      and not maya.talking):
        scene.append([(1029,343),"home hall maya_autumn","maya"])

      #Sugar Cubes
      if game.season == 1:
        if quest.flora_bonsai <= "sugar" and not (quest.kate_blowjob_dream == "school" or quest.mrsl_table == "morning"):
          if not home_hall["sugarcube1_taken"]:
            scene.append([(1456,773),"home hall sugarcube1","home_hall_sugarcube1"])
          if not home_hall["sugarcube2_taken"]:
            scene.append([(735,113),"home hall sugarcube2",("home_hall_sugarcube2",0,200)])

      #Dollars & Books
      if not (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        if home_hall["dollar1_spawned_today"] == True and not home_hall["dollar1_taken_today"]:
          scene.append([(1513,727),"home hall dollar1","home_hall_dollar1"])
        if home_hall["dollar2_spawned_today"] == True and not home_hall["dollar2_taken_today"]:
          scene.append([(681,428),"home hall dollar2","home_hall_dollar2"])
        if not home_hall["book_taken"]:
          scene.append([(994,610),"home hall book","home_hall_book"])

      #Makeshift Bedroom
      if quest.maya_sauce["bedroom_taken_over"] and not quest.lindsey_voluntary == "dream":
        if home_bedroom["clean"]:
          scene.append([(571,546),"home hall clean_bed","home_bedroom_bed"])
        else:
          scene.append([(571,546),"home hall bed","home_bedroom_bed"])
        scene.append([(632,714),"home hall courage_badge","home_bedroom_courage_badge"])
        scene.append([(555,780),"home hall case"])
        scene.append([(550,849),"home hall monitor","home_bedroom_computer"])
        if home_bedroom["clean"]:
          scene.append([(720,787),"home hall ornamental_box","home_bedroom_ornamental_box"])
        elif home_bedroom["alarm"] in ("smashed","smashed_again"):
          scene.append([(711,767),"home hall broken_alarm","home_bedroom_alarm"])
        else:
          scene.append([(710,773),"home hall alarm","home_bedroom_alarm"])
        scene.append([(1121,660),"home hall chair"])
        if quest.jacklyn_romance >= "suit_done" and (quest.jacklyn_town < "hairdo" or quest.jacklyn_town.failed) and not quest.lindsey_voluntary == "dream":
          scene.append([(1183,659),"home hall suit","home_bedroom_suit"])
        # scene.append([(1200,653),"home hall hoodie","home_bedroom_hoodie"])
        scene.append([(1641,271),"home hall cat_poster",("home_hall_door_bedroom",27,67)])

      #Night
      if (home_hall["night"]
      or quest.maya_quixote in ("attic","board_game")):
        scene.append([(0,0),"#home hall nightoverlay"])

    def find_path(self,target_location):
      if target_location=="home_bedroom":
        return "home_hall_door_bedroom",dict(marker_offset=(100,0),marker_sector="right_bottom")
      elif target_location=="home_bathroom":
        return "home_hall_door_bathroom"
      ## todo add flora/jo room doors
      ## otherwise path is thru downstairs
      else:
        return "home_hall_stairs",dict(marker_offset=(300,100),marker_sector="left_bottom")


label goto_home_hall:
  if home_hall.first_visit_today:
    $home_hall["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $home_hall["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(home_hall)
  return
