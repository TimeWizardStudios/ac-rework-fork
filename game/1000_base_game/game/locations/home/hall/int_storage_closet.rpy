init python:
  class Interactable_home_hall_cleaning_supply(Interactable):

    def title(cls):
      return "Storage Closet"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "If I were gay, I'd need a bigger closet to come out of. This one can barely hold my current desires."
      elif quest.flora_cooking_chilli.started:
        return "[jo] bought this closet at an auction for like $3. Clearly, you get what you pay for."
      else:
        return "Maybe it's time to take the vacuum cleaner out for another spin, if you know what I'm saying?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and not quest.mrsl_table["mop_taken"]:
            actions.append(["take","Take","quest_mrsl_table_home_hall_storage_closet_take"])
          elif quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.maxine_lines == "magnetic" and not home_hall["closet_lines_magnet"]:
            actions.append(["take","Take","?home_hall_closet_interact_maxine_lines_take_magnet"])
        if quest.lindsey_piano == "cleaning" and not quest.lindsey_piano["vacuum_cleaner_taken"] and not home_bedroom["clean"]:
          actions.append(["take","Take","quest_lindsey_piano_cleaning_vacuum_cleaner"])
        if quest.jo_day == "cleaning" and not quest.jo_day["vacuum_cleaner_taken"] and not home_bedroom["clean"]:
          actions.append(["take","Take","quest_jo_day_cleaning_vacuum_cleaner"])
      if quest.isabelle_stolen.started:
        actions.append("?home_hall_cleaning_supply_interact_isabelle_stolen")
      if quest.flora_cooking_chilli.started:
        actions.append("?home_hall_cleaning_supply_interact_flora_cooking_chilli_return_phone")
      actions.append("?home_hall_cleaning_supply_interact")


label home_hall_closet_interact_maxine_lines_take_magnet:
  "Ha! I knew we had a magnet lying around somewhere!"
  $home_hall["closet_lines_magnet"] = True
  $mc.add_item("magnet")
  return

label home_hall_cleaning_supply_interact:
  "A closet with cleaning supplies. Also, the best hiding spot in the house."
  "It feels like a lifetime has passed since my glorious hide and seek victories."
  return

label home_hall_cleaning_supply_interact_flora_cooking_chilli_return_phone:
  "Cleaning supplies and other essentials... everything you need to operate a household."
  "It's not my turn to clean, though."
  "...Shit, it probably is."
  return

label home_hall_cleaning_supply_interact_isabelle_stolen:
  "Salt, a heap of garlic cloves, and an old wooden stake."
  "Glad to see my vampire hunting kit is still in good repair."
  "Just in case [flora] starts sucking blood in addition to my confidence."
  return
