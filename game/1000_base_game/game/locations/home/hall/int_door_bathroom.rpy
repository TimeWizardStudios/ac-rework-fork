init python:
  class Interactable_home_hall_door_bathroom(Interactable):

    def title(cls):
      return "Bathroom"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.jacklyn_town == "flora":
        # return "Dare I enter the domain of a woman scorned?"
        return "Dare I enter the domain\nof a woman scorned?"
      elif quest.flora_cooking_chilli >= "return_phone":
        return "For some reason, this door is the newest and most robust one in the house. Breaking it down would require at least three hundred Spartans.\n\nLuckily, the lock is from ancient Greece as well."
      else:
        return "Flushing the toilet turns the water in the shower cold. Best feature of the bathroom..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Throne Room","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "follow_bathroom":
            actions.append(["go","Throne Room","home_hall_door_bathroom_interact_flora_cooking_chilli_return_phone"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Throne Room","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "fun" and game.hour == 19:
            actions.append(["go","Throne Room","?quest_kate_moment_fun_bathroom"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Throne Room","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "suit":
            actions.append(["go","Throne Room","?quest_jacklyn_town_suit_bathroom"])
            return
          elif quest.jacklyn_town == "flora":
            actions.append(["interact","Knock","quest_jacklyn_town_flora"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Throne Room","kate_blowjob_dream_random_interact_locked_location"])
        elif mc["focus"] == "maya_sauce":
          if quest.maya_sauce == "siren_song":
            actions.append(["go","Throne Room","quest_maya_sauce_siren_song"])
          elif quest.maya_sauce == "breakfast":
            actions.append(["go","Throne Room","?quest_maya_sauce_breakfast_home_hall_door_bathroom"])
            return
      else:
        if quest.flora_jacklyn_introduction == "bathroom":
          actions.append(["go","Throne Room","home_hall_door_bathroom_interact_flora_jacklyn_introduction_milk"])
      if home_hall["bathroom_locked_now"] or home_hall["bathroom_locked_today"]:
        actions.append(["go","Throne Room","?home_hall_door_bathroom_locked"])
      elif quest.maya_quixote["maya_showering_now"]:
        actions.append(["go","Throne Room","?quest_maya_quixote_sleep_again_home_hall_door_bathroom"])
      else:
        actions.append(["go","Throne Room","goto_home_bathroom"])

    #def circle (cls,actions,x,y):
    #  rv=super().circle(actions,x,y)
    #  rv.start_angle+=120
    #  rv.angle_per_icon= 40
    #  return rv


label home_hall_door_bathroom_interact_flora_cooking_chilli_return_phone:
  mc "[flora], are you okay?"
  flora "Don't come in!"
  flora "Ah ah ah ah!"
  "Sounds like she's in a lot of pain."
  mc "I'm coming in, okay?"
  flora "Owie! Owie!"
  menu(side="middle"):
    extend ""
    "Go inside":
      flora "Ow! Ow! Ow!"
      mc "That's it. I'm coming in."
      $quest.flora_cooking_chilli.advance("enter_bathroom")
      jump goto_home_bathroom
    "Respect her privacy, in spite of her cries of agony":
      mc "Let me know if you need help or anything."
      "..."
      mc "I'm really worried!"
      flora "Please... don't... come in!"
      mc "Okay, I'll leave your phone here by the door in case you need to call for help or something."
      $mc.remove_item("flora_phone")
      $ mc.love+=2
      "She's more than capable of taking care of herself."
      $unlock_stat_perk("love19")
      $ quest.flora_cooking_chilli.finish()
  return

label home_hall_door_bathroom_interact_flora_jacklyn_introduction_milk:
  "Shit, I think I hear [jo] coming up the stairs. Better clear this up quickly."
  jump goto_home_bathroom

label home_hall_door_bathroom_locked:
  "Maybe I can still peek through the keyhole..."
  flora "I know you're still outside! Go away!"
  return
