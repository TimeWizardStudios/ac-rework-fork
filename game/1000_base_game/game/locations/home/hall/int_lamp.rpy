init python:
  class Interactable_home_hall_lamp(Interactable):

    def title(cls):
      return "Lamp"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        # return "So, this one didn't have a genie in it. Just my luck."
        return "So, this one didn't have a genie\nin it. Just my luck."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take","Take","home_hall_lamp_take"])
      actions.append("?home_hall_lamp_interact")


label home_hall_lamp_take:
  $home_hall["lamp_taken"] = True
  $mc.add_item("lamp")
  return

label home_hall_lamp_interact:
  "If this bad boy was running on oil, you know I'd rub it good."
  return
