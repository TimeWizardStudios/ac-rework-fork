init python:
  class Interactable_home_hall_shoebox(Interactable):

    def title(cls):
      return "Shoebox"

    def description(cls):
      return "Not as sexy as a message in a bottle, but it will do."

    def actions(cls,actions):
      actions.append(["take","Take","home_hall_shoebox_take"])


label home_hall_shoebox_take:
  "Sweet! [flora] came through for me."
  "I knew she had to have a spare box around..."
  "Unlike me, she has at least two pairs of shoes."
  window hide
  $home_hall["shoebox_taken"] = True
  $mc.add_item("shoebox")
  return
