init python:
  class Interactable_home_hall_note(Interactable):

    def title(cls):
      return "Note"

    def description(cls):
      if (quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "A note written in the furious handwriting of a house owner scorned."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("home_hall_table_note_mrsl_HOT_interact")
