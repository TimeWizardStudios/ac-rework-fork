init python:
  class Interactable_home_hall_coat_hanger(Interactable):
    def title(cls):
      return "Coat Hanger"
    def description(cls):
      return "Desc"
    def actions(cls,actions):
      actions.append("?home_hall_coat_hanger_interact")

label home_hall_coat_hanger_interact:
  "label"
  return
