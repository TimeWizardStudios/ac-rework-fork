init python:
  class Interactable_home_hall_trphy(Interactable):

    def title(cls):
      return "Trophies"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "When we're all dead and gone, would any of your heroic deeds be remebered?\n\nProbably."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?home_hall_trphy_interact")


label home_hall_trphy_interact:
  "What's the move?"
  "Can I tell the truth?"
  "If I was doing this for you then I'd have nothing left to prove."
  "Nah, this for me though."
  "I'm just tryna stay alive and take care of my people."
  "Trophies."
  "And they don't have no award for that."
  "Shit don't come with trophies, ain't no envelopes to open."
  "I just do it cause I'm 'sposed to..."
  "Neighbor."
  return
