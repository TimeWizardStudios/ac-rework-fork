init python:
  class Interactable_home_hall_shoes(Interactable):
    def title(cls):
      return "Shoes"
    def description(cls):
      return "Desc"
    def actions(cls,actions):
      actions.append("?home_hall_shoes_interact")

label home_hall_shoes_interact:
  "label"
  return
