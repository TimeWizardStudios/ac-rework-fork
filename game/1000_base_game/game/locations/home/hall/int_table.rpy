init python:
  class Interactable_home_hall_table(Interactable):

    def title(cls):
      return "Table"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif not home_hall["table_taken"]:
        return "This is one sturdy table. The sturdiest perhaps of all time."
      else:
        return "This is one wobbly table. Seems unsure of where it stands most of the time."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if home_hall["note"]:
        actions.append(["interact","Move","home_hall_table_note_mrsl_HOT_interact_note"])
      else:
        actions.append(["interact","Move","home_hall_table_mrsl_HOT_interact"])


label home_hall_table_mrsl_HOT_interact:
  $home_hall["table_taken"] = not home_hall["table_taken"]
  return

label home_hall_table_note_mrsl_HOT_interact_note:
  "Where did this note come from?"

label home_hall_table_note_mrsl_HOT_interact:
  $home_hall["note"] = False
  $jo.love-=1
  "{i}\"[mc], I almost tripped over the table. Please, do not move the furniture around. —[jo]\"{/}"
  return
