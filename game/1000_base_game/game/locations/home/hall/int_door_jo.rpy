init python:
  class Interactable_home_hall_door_mom(Interactable):
    pass

  class Interactable_home_hall_door_jo(Interactable):
    def title(cls):
      return "[jo]'s Room"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_cooking_chilli >= "return_phone":
        return "The creakiest door in the house. [jo] refuses to oil it for the sole reason of catching intruders."
      else:
        return "Not about to see your light... but if you want to find Hell with me..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and quest.mrsl_table["mess_seen"]:
            actions.append(["go","[jo]'s Room","?quest_mrsl_table_clean_up_soon"])
            return
          elif quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","[jo]'s Room","kate_blowjob_dream_random_interact_locked_location"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","[jo]'s Room","kate_blowjob_dream_random_interact_locked_location"])
            return
      else:
        if mc.owned_item("key_jo_room"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:act_one,jo_room_door|Use What?","jo_room_door"])
      if quest.flora_cooking_chilli >= "return_phone":
        actions.append("?home_hall_door_jo_interact_flora_cooking_chilli_return_phone")
      actions.append("?home_hall_door_jo_interact")


label home_hall_door_jo_interact:
  "When [jo] isn't working, she's sleeping. Those are her two states."
  return

label home_hall_door_jo_interact_flora_cooking_chilli_return_phone:
  "[jo]'s room is strictly off limits. She probably doesn't want me to find her vibrator."
  "But jokes on her, I spotted it on the nightstand while climbing the tree outside."
  return

label jo_room_door(item):
  if item == "key_jo_room":
    "Barging into her room will probably get me grounded. Need to be smarter about this."
  else:
    "Unlocking a door with something like my [item.title_lower] only happens in the movies. And in my dreams. Sometimes both at the same time."
    $quest.act_one.failed_item("jo_room_door",item)
  return
