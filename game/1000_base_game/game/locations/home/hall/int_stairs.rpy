init python:
  class Interactable_home_hall_stairs(Interactable):

    def title(cls):
      return "Stairs"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_cooking_chilli >= "return_phone":
        return "Down from my majestic height to the land of the pedestrians."
      else:
        return "Creaky steps. Sneak 100 required to reach the kitchen after bedtime."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Downstairs","goto_home_kitchen"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and quest.mrsl_table["mess_seen"]:
            actions.append(["go","Downstairs","?quest_mrsl_table_clean_up_soon"])
            return
          elif quest.mrsl_table == "morning":
            actions.append(["go","Downstairs","goto_home_kitchen"])
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "fun" and game.hour == 19:
            actions.append(["go","Downstairs","?quest_kate_moment_fun_stairs"])
            return
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "suit":
            actions.append(["go","Downstairs","?quest_jacklyn_town_suit_stairs"])
            return
          elif quest.jacklyn_town == "hairdo":
            actions.append(["go","Downstairs","?quest_jacklyn_town_hairdo_stairs"])
            return
#     actions.append(["go","Downstairs","?home_hall_stairs_interact"])
      actions.append(["go","Downstairs","goto_home_kitchen"])


label home_hall_stairs_interact:
  "Don't feel like climbing that staircase unless it's absolutely necessary."
  return
