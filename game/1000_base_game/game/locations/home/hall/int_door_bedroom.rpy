init python:
  class Interactable_home_hall_door_bedroom(Interactable):

    def title(cls):
      return "Bedroom"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return "Too late to go back now."
      elif quest.maya_sauce["bedroom_taken_over"]:
        return "It's like the cat's eyes follow me wherever I go..."
      elif quest.flora_cooking_chilli >= "return_phone":
        return "My very own oasis in life's endless desert."
      else:
        return "The pit of depravity. The tomb of the forsaken. The well of lost souls. My humble abode."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Safety","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli in ("chilli_done","return_phone","follow_bathroom","get_milk","distracted","got_liquid"):
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and quest.mrsl_table["mess_seen"]:
            actions.append(["go","Safety","?quest_mrsl_table_clean_up_soon"])
            return
          elif quest.mrsl_table == "morning":
            actions.append(["go","Safety","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Safety","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Safety","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
      if quest.maya_sauce["bedroom_taken_over"] and not quest.flora_walk == "xcube":
        actions.append(["go","Safety","?quest_maya_sauce_aftermath_home_hall_door_bedroom"])
      else:
        actions.append(["go","Safety","goto_home_bedroom"])

    #def circle (cls,actions,x,y):
    #  rv=super().circle(actions,x,y)
    #  rv.start_angle+=120
    #  rv.angle_per_icon= 40
    #  return rv
