init python:
  class Interactable_home_hall_door_flora(Interactable):

    def title(cls):
      return "[flora]'s Room"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.flora_cooking_chilli >= "return_phone":
        return "Walking past [flora]'s room is one of my favorite things. It always smells so nice."
      else:
        return "[flora]'s room is forbidden and sinful. Confessions are required afterward."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "florasad":
            actions.append("home_hall_door_flora_jacklyn_statement_florasad")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and quest.mrsl_table["mess_seen"]:
            actions.append(["go","[flora]'s Room","?quest_mrsl_table_clean_up_soon"])
            return
          elif quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","[flora]'s Room","kate_blowjob_dream_random_interact_locked_location"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","[flora]'s Room","kate_blowjob_dream_random_interact_locked_location"])
            return
      else:
        if game.season == 1:
          if quest.isabelle_tour.finished and quest.berb_fight.finished and quest.flora_bonsai.finished and quest.jacklyn_statement.finished and quest.maxine_eggs.finished and game.hour == 20 and not quest.flora_squid.started:
            actions.append(["quest","Quest","quest_flora_squid_start"])
        if quest.kate_stepping == "magazine":
          if flora.at("home_hall","texting"):
            actions.append("?quest_kate_stepping_magazine_flora")
            return
          else:
            actions.append("quest_kate_stepping_magazine")
      if quest.flora_cooking_chilli >= "return_phone":
        actions.append("?home_hall_door_flora_interact_flora_cooking_chilli_return_phone")
      actions.append("?home_hall_door_flora_interact")


label home_hall_door_flora_interact:
  "[flora]'s room is always clean and has a nice lavender scent. No wonder [jo] likes her more."
  return

label home_hall_door_flora_interact_flora_cooking_chilli_return_phone:
  "[flora] sure loves her posters. Unicorns, kittens, and everything adorable."
  "Then, there's Cthulhu. Not sure what that poster is doing there."
  return

label home_hall_door_flora_jacklyn_statement_florasad:
  #Zoom in on flora's door?
  #SFX: knocking
  mc "[flora], are you in there?"
  flora "Go away!"
  mc "Can we talk, please?"
  flora "I said go away!"
  mc "..."
  mc "Listen. I didn't realize how you felt."
  mc "I know I haven't always been the nicest person to you, but I'm really trying to change... and I think you've noticed that too."
  mc "I wouldn't go on a date with [jacklyn] if I knew how you felt. I wouldn't do that to you."
  "..."
  mc "[flora]?"
  show flora crying with Dissolve(.5)
  flora crying "Do you really mean that?"
  mc "Cross my heart."
  mc "That moment meant a lot to me too. I just...."
  flora crying "You just what?"
  mc "I wasn't sure you thought of me the same way."
  mc "I figured it'd be one of those moments that came and went and nothing else happened."
  flora sad "What do you want to happen?"
  mc "I don't know. I just don't know."
  mc "What can we really do?"
  flora sad "..."
  mc "How can we do anything with [jo] in the house?"
  mc "Do we even want to? You know it's wrong. We both do."
  flora crying "You're right. I don't know why I acted this way."
  flora crying "It would never work."
  flora crying "You've never had an easy time finding someone. I'm sorry for ruining it for you."
  mc "Hey..."
  mc "Why do you think I'm here now and not out vandalizing something with her?"
  mc "It's 'cause I care about you."
  flora sad "Really?"
  mc "Yes, really."
  mc "But we need to think this over carefully."
  mc "This isn't a game, [flora]."
  flora skeptical "I know that."
  mc "I know that you know, but sometimes your emotions take over. You can't cause drama if I ever look at another girl."
  mc "At least not in public."
  flora sad "I know. I'm sorry."
  mc "It's fine, I don't think [jacklyn] connected the dots."
  mc "And I think you in particular really need to think this over."
  mc "You're going to have a great career, a wonderful husband, children. Would you really throw that away?"
  flora skeptical "How do you know that?"
  "Because it happened in my old life!"
  mc "Because I know you're smart and driven and beautiful."
  flora blush "Beautiful?"
  mc "Oh, stop acting all coy. You know you are."
  mc "But seriously, you need to think this over good and hard."
  mc "I don't have anything to lose, but you do."
  flora thinking "You're right."
  flora worried "I'll make a pros and cons list."
  mc "Okay, sounds good."
  flora thinking "Don't tell [jo] about this."
  mc "You understand that we can't tell {i}anyone{/}, right?"
  flora worried "Yeah, I do."
  mc "Okay, I should go get some sleep."
  mc "Goodnight."
  $flora.love+=3
  flora blush "Goodnight, [mc]."
  $flora["madeamends"] = True
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  #while game.hour<23:
  #  $game.advance()
  jump goto_home_bedroom
