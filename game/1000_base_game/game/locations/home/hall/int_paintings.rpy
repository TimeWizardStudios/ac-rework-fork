init python:
  class Interactable_home_hall_painting_deco(Interactable):
    def title(cls):
      return "Paintings"
    def description(cls):
      return "Desc"
    def actions(cls,actions):
      actions.append("?home_hall_painting_deco_interact")

label home_hall_painting_deco_interact:
  "label"
  return
