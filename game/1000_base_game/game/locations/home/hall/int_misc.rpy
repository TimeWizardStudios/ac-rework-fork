init python:
  class Interactable_home_hall_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_hall_dollar2_take"])


label home_hall_dollar2_take:
  $home_hall["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_hall_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_hall_dollar1_take"])


label home_hall_dollar1_take:
  $home_hall["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_hall_book(Interactable):

    def title(cls):
      return "Book"

    def actions(cls,actions):
      actions.append(["interact","Read","?home_hall_book_interact"])


label home_hall_book_interact:
  $home_hall["book_taken"] = True
  "Damn, I've looked everywhere for this..."
  "{i}\"Brute-Forcing It — the tough-guy hacker's guide to buffer overflows and buff muscles, yo.\"{/}"
  $mc.strength+=1
  return
