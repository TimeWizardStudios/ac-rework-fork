init python:
  class Interactable_home_hall_umbrella(Interactable):

    def title(cls):
      return "Umbrella"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Never seen this before. No, I didn't bring it into the house. Maybe it's [flora]'s? No, [jo], I promise it's the first time I've seen it."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take", "Take","home_hall_umbrella_take"])
      actions.append("?home_hall_umbrella_interact")


label home_hall_umbrella_take:
  $home_hall["umbrella_taken"] = True
  $mc.add_item("umbrella")
  return

label home_hall_umbrella_interact:
  "Just a normal umbrella without any backstory or anything."
  return
