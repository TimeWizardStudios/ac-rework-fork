init python:
  class Interactable_home_hall_sword(Interactable):

    def title(cls):
      return "Machete"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.maxine_dive.start:
        return "You're steel the one I want."
      elif quest.isabelle_stolen.started:
        return "Hack 'n' slash — the go-to genre of any genocide."
      elif quest.flora_cooking_chilli.started:
        return "Perfect to hack through dense jungle, cut fruit, and fend off beavers."
      else:
        return "Elven steel. Glows while orcs nearby. Or was it trolls? Got to test on [flora] some time."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if mc.owned_item("colored_paper"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:poolside_story,machete|Use What?","home_hall_sword_poolside"])
        if quest.flora_squid == "weapon":
          actions.append(["take","Take","?quest_flora_squid_weapon_machete"])
        if quest.mrsl_bot == "shoebox":
          actions.append(["take","Take","quest_mrsl_bot_shoebox_machete"])
        if quest.maxine_dive == "weapons" and not home_hall["machete_taken"]:
          actions.append(["take","Take","quest_maxine_dive_weapons_machete"])
        if quest.jacklyn_statement.started:
          actions.append(["take","Take","?home_hall_sword_take"])
      if quest.isabelle_stolen.started:
        actions.append("?home_hall_sword_interact_isabelle_stolen")
      if quest.flora_cooking_chilli.started:
        actions.append("?home_hall_sword_interact_flora_cooking_chilli_return_phone")
      actions.append("?home_hall_sword_interact")


label home_hall_sword_interact:
  "I'm not sure where [jo] got this one. Probably while cutting her way to the top of the school board."
  return

label home_hall_sword_interact_flora_cooking_chilli_return_phone:
  "If only [jo] would allow me to take it to school, nobody would bother me."
  "Except the cops, of course."
  return

label home_hall_sword_poolside(item):
  if item == "colored_paper":
    "When someone asks for a slice of the action, this is what I show them."
    "Fifteen inches of carbon spring steel."
    "Chops paper decorations just as well as it tears through the heart of Africa."
    $mc.remove_item(item)
    $mc.add_item("pennants")
  else:
    "This machete could slice my [item.title_lower] like butter, but I think I'll put something else on my sandwich this time."
    $quest.poolside_story.failed_item("machete",item)
  return

label home_hall_sword_interact_isabelle_stolen:
  "Ouch!"
  "That'll leave a scar..."
  "...on my ego."
  "It's too early in the day to see a reflection that ugly."
  return

label home_hall_sword_take:
  "I better grab this just in case."
  $home_hall["machete_taken"] = True
  $mc.add_item("machete")
  $mc.intellect+=1
  return
