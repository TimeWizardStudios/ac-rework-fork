init python:
  class Interactable_home_hall_stair_handle(Interactable):
    def title(cls):
      return "Railing"
    def description(cls):
      return "Desc"
    def actions(cls,actions):
      actions.append("?home_hall_stair_handle_interact")

label home_hall_stair_handle_interact:
  "label"
  return
