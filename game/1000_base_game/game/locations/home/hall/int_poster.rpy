init python:
  class Interactable_home_hall_back_poster(Interactable):
    def title(cls):
      return "Poster"
    def description(cls):
      return "Desc"
    def actions(cls,actions):
      actions.append("?home_hall_back_poster_interact")

label home_hall_back_poster_interact:
  "label"
  return
