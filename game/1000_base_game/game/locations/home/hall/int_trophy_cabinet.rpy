init python:
  class Interactable_home_hall_trphy_cabnet(Interactable):

    def title(cls):
      return "Trophy Cabinet"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.isabelle_stolen.started:
        return "[jo] always says stuff like, \"[flora], darling, if you don't stop winning things we'll have to get a bigger shelf!\"\n\nInfuriating."
      elif quest.flora_cooking_chilli.started:
        return "It sure feels nice to be reminded what a failure you are every time you go to the bathroom."
      else:
        return "Don't get too excited, they're all [flora]'s."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if quest.isabelle_stolen.started:
        actions.append("?home_hall_trphy_cabnet_interact_isabelle_stolen")
      if quest.flora_cooking_chilli.started:
        actions.append("?home_hall_trphy_cabnet_interact_flora_cooking_chilli_return_phone")
      actions.append("?home_hall_trphy_cabnet_interact")


label home_hall_trphy_cabnet_interact:
  "It's all a sham."
  "Every trophy should be awarded for the same one thing."
  "A fortunate recombination of genetics."
  return

label home_hall_trphy_cabnet_interact_flora_cooking_chilli_return_phone:
  "[flora] has so many trophies. Science projects, spelling bees, various art competitions. She's good at everything she does."
  "Except video games. Or she's just pretending to be bad to make me feel better. Probably that."
  return

label home_hall_trphy_cabnet_interact_isabelle_stolen:
  "[flora] must've hidden all my trophies somewhere..."
  "Wait, no. There they are, my beautiful Newfall LAN participation stickers."
  return
