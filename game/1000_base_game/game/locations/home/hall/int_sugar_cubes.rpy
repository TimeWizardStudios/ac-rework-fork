### home_hall_sugarcube5 interactable ######################################

init python:
  class Interactable_home_hall_sugarcube1(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_hall_sugarcube1_take"])

label home_hall_sugarcube1_take:
  $home_hall["sugarcube1_taken"] = True
  $mc.add_item("sugar_cube")
  return

### home_hall_sugarcube4 interactable ######################################

init python:
  class Interactable_home_hall_sugarcube2(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_hall_sugarcube2_take"])

label home_hall_sugarcube2_take:
  $home_hall["sugarcube2_taken"] = True
  $mc.add_item("sugar_cube")
  return

