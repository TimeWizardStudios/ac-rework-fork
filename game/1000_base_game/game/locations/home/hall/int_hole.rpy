init python:
  class Interactable_home_hall_hole(Interactable):

    def title(cls):
      return "Hole"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.jo_day == "supplies":
        return "Back to the ol' glory hole again."
      elif quest.maya_quixote == "attic":
        return "I'm {i}atticted{/} to you..."
      else:
        return "We don't speak about that. It wasn't even my fault. Okay, fine, it was. But we don't speak about that."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "maya_quixote":
          if quest.maya_quixote in ("attic","board_game"):
            if quest.maya_quixote["old_tricks"] and home_hall["table_taken"]:
              actions.append(["use_item", "Use item", "select_inventory_item", "$quest_item_filter:maya_quixote,dungeons_and_damsels|Use What?", "quest_maya_quixote_attic"])
            actions.append("quest_maya_quixote_attic_home_hall_hole")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if home_hall["table_taken"]:
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:mrsl_HOT,hole|Use What?","home_hall_hole_use"])
        if quest.jo_day == "supplies" and not quest.jo_day["picnic_blanket_taken"]:
          actions.append(["take","Take","quest_jo_day_supplies_hole_take"])
          actions.append("?quest_jo_day_supplies_hole_interact")
        if quest.mrsl_HOT == "attic":
          actions.append("home_hall_mrsl_HOT_interact")
      actions.append("?home_hall_hole_interact")


label home_hall_hole_use(item):
  if item == "xcube_controller":
    if home_hall['hole_today']:#If the player has retrieved an item within the last 12 hours:
      "Ugh, my neck and back hurts from all the reaching and lassoing earlier. Need to rest for a while."
    else:
      "Yeehaw! Toss out the nets!"
      "..."
      "Come on..."
      "..."
      if not False in (home_hall['hole_diary'],home_hall['hole_card'],home_hall['hole_bag'],home_hall['hole_photo']):
        #If the player has gotten all four items:
        "Hmm... nothing really interesting up here anymore."
      elif quest.mrsl_HOT<"attic" and not False in (home_hall['hole_diary'],home_hall['hole_bag'],home_hall['hole_photo']):
        "Hmm... nothing really interesting up here."
      else:
        $items = [["a plastic bag","hole_bag"],["an old book","hole_photo"],["a cardboard box","hole_card"],["a shoebox","hole_diary"]]
        python:
          while True:
            catch = items[random.randint(0,3)]
            if quest.mrsl_HOT<"attic" and catch[0] == "a cardboard box":
              continue
            if not home_hall[catch[1]]:
              break
        "Got something!"
        "It's [catch[0]]!"
        menu(side="middle"):
          extend ""
          "Keep it":
            if catch[0] == "an old book":
              "Oh! It's actually a photo album."
              "There are lots of boring family photos here."
              "..."
              "Wait a minute..."
              "I forgot about the last photo, from our beach day this spring."
              window hide
              show misc beach_photo with Dissolve(.5)
              window auto
              "I had forgotten how cute they looked that day."
              "Well, [flora] is cute. [jo] is more... sexy? Is that weird to think?"
              "..."
              "The smell of sand and ocean water, bodies sweating in the sun..."
              # "[flora] doing that little shimmy towel dance to get out of her clothes..."
              "[flora] doing that little shimmy towel dance to get out of her clothes...{space=-15}"
              "[jo] wrapping her lips tight around a popsicle..."
              "Me trying to hide my raging erection by lying on my stomach..."
              "[jo] rubbing sunscreen on my back and legs..."
              "Man, what a time to be alive."
              "Right after this photo, I stole the sunscreen and [flora] chased me into the water."
              "She fell with a splash and lost her top."
              "The cute little thing floated right into my hand and pocket before she even realized."
              "She stayed in the water for an hour that day, trying to hide her perky nipples."
              "If I'd been more confident, I could probably have traded the bikini for a blowjob behind the pier..."
              "A good memory, nonetheless."
              window hide
              hide misc beach_photo with Dissolve(.5)
              pause 0.125
              $home_hall[catch[1]] = True
              $mc.add_item("beach_photo")
            elif catch[0] == "a cardboard box":
              "Okay! This might be exactly what I'm looking for!"
              "A cardboard box full of electronics."
              "Electronics. Electronics."
              "..."
              "Cables and mice and some kind of... rubber thing?"
              "Probably [jo]'s."
              "..."
              "Ah, there it is!"
              window hide
              $home_hall[catch[1]] = True
              $mc.add_item("capture_card")
            elif catch[0] == "a plastic bag":
              "Oh? What do we have here?"
              "A plastic bag with..."
              "..."
              "Is this what I think it is?"
              window hide
              $home_hall[catch[1]] = True
              $mc.add_item("bag_of_weed")
            elif catch[0] == "a shoebox":
              "Hmm... there seems to be something in it..."
              window hide
              $home_hall[catch[1]] = True
              $mc.add_item("diary_page")
            $home_hall["hole_today"] = True
          "Toss it back":
            "Not quite what I had in mind."
  elif item == "ball_of_yarn":
    "Thread's too weak and flimsy. Won't catch anything with a lasso like this."
    $quest.mrsl_HOT.failed_item("hole",item)
  else:
    "Not to shame my [item.title_lower], but... hmm... maybe that's actually the play."
    "Shame on you, [item.title_lower]! Shame!"
    "Nope. Didn't work."
    $quest.mrsl_HOT.failed_item("hole",item)
  return

label home_hall_hole_interact:
  "The uncharted abyss lies beyond this tear in the flimsy fabric of reality. The truth is up there."
  return

label home_hall_mrsl_HOT_interact:
  "Ever since the ladder to the attic's outdoor entrance broke and the floor around it collapsed, this hole is the only access point."
  "Retrieving stuff from up there would require something to stand on, as well as a lasso of sorts..."
  $quest.mrsl_HOT.advance("attic2")
  return
