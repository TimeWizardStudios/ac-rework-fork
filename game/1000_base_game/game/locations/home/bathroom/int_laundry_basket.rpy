init python:
  class Interactable_home_bathroom_laundry_basket(Interactable):

    def title(cls):
      return "Laundry Basket"

    def description(cls):
      if quest.mrsl_HOT.started and home_bathroom["dirty_clothes_taken"]:
        return "And they said spending hours on a Taiwanese basket weaving forum was a waste!"
      elif quest.flora_jacklyn_introduction.started:
        return "No one truly appreciates clean clothes until they run out."
      else:
        return "[jo]'s underthings. My stuff is in a separate basket because she's a raging sexist.\n\nAlso because her underwear sometimes ended up in my closet. Not sure how that happened."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "flora_called":
            actions.append("home_bathroom_clothes_flora_cooking_chilli_phone")
      if quest.mrsl_HOT.started and home_bathroom["dirty_clothes_taken"]:
        actions.append("?home_bathroom_laundry_basket_interact_dirty_clothes_taken")
      if quest.flora_jacklyn_introduction.started:
        actions.append("?home_bathroom_laundry_basket_interact_flora_jacklyn_introduction")
      actions.append("?home_bathroom_laundry_basket_interact")


label home_bathroom_laundry_basket_interact:
  "After a few of [flora]'s panties went missing and a skirt shrunk to kid-size, [jo] banned me from touching the laundry basket."
  "That was years ago, though."
  return

label home_bathroom_laundry_basket_interact_flora_jacklyn_introduction:
  "Cleanliness is a matter of choice."
  "Dirtiness is a matter of perspective."
  return

label home_bathroom_laundry_basket_interact_dirty_clothes_taken:
  "What do you call a basket that stores other baskets?"
  "A basket case."
  return
