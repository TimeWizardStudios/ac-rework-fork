init python:
  class Location_home_bathroom(Location):

    default_title="Bathroom"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      scene.append([(0,0),"home bathroom bathroom"])

      #Window
      scene.append([(996,166),"home bathroom window"])
      if (home_bathroom["night"]
      or quest.maya_quixote in ("attic","board_game")
      or quest.jacklyn_town == "flora"):
        scene.append([(1014,186),"home bathroom window_night"])

      #Shower
      scene.append([(482,29),"home bathroom shower"])

      #Sugar Cube #1
      if game.season == 1:
        if quest.flora_bonsai <= "sugar" and not home_bathroom["sugarcube7_taken"]:
          scene.append([(617,420),"home bathroom sugarcube7","home_bathroom_sugarcube7"])

      #Flora
      if flora.at("home_bathroom","shower"):
        if not flora.talking:
          scene.append([(657,501),"home bathroom flora","flora"])

      scene.append([(514,78),"#home bathroom shower_glass"])

      #Poster Instructions
      if quest.mrsl_HOT == "laundry":
        scene.append([(1364,210),"home bathroom poster","home_laundry_instructions"])
      else:
        scene.append([(1364,210),"home bathroom poster"])

      #Mirror
      scene.append([(1636,7),"home bathroom mirror",("home_bathroom_mirror",0,370)])
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["lipstick_writing_removed"]:
        scene.append([(1684,103),"#home bathroom lipstick_writing"])

      scene.append([(128,0),"home bathroom door",("home_bathroom_door",0,350)])
      scene.append([(428,797),"home bathroom carpet"])

      #Bathrobe
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["bathrobe_original_place"]:
        scene.append([(1088,802),"home bathroom bathrobe_down",("home_bathroom_bathrobe",0,-15)])
        scene.append([(1565,150),"home bathroom hook"])
      else:
        scene.append([(1503,151),"home bathroom bathrobe"])

      #Toilet
      scene.append([(795,423),"home bathroom toilet","home_bathroom_toilet"])
      scene.append([(936,399),"home bathroom bathtub"])
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["piss_puddles_removed"]:
        scene.append([(794,676),"home bathroom piss_puddles","home_bathroom_piss_puddles"])

      #Sugar Cube #2
      if game.season == 1:
        if quest.flora_bonsai <= "sugar" and not home_bathroom["sugarcube6_taken"]:
          scene.append([(1315,425),"home bathroom sugarcube6","home_bathroom_sugarcube6"])

      #Basket
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["laundry_original_place"] == 7:
        if quest.mrsl_table["basket_original_place"]:
          scene.append([(554,862),"home bathroom empty_basket","home_bathroom_laundry_basket"])
          if quest.mrsl_table["laundry_original_place"] >= 1:
            scene.append([(672,851),"home bathroom laundry1",("home_bathroom_basket_clothes",6,-14)])
          if quest.mrsl_table["laundry_original_place"] >= 2:
            scene.append([(745,870),"home bathroom laundry2",("home_bathroom_basket_clothes",-69,-33)])
          if quest.mrsl_table["laundry_original_place"] >= 3:
            scene.append([(597,841),"home bathroom laundry3",("home_bathroom_basket_clothes",69,-4)])
          if quest.mrsl_table["laundry_original_place"] >= 4:
            scene.append([(671,893),"home bathroom laundry4",("home_bathroom_basket_clothes",-11,-56)])
          if quest.mrsl_table["laundry_original_place"] >= 5:
            scene.append([(656,838),"home bathroom laundry5",("home_bathroom_basket_clothes",3,-1)])
          if quest.mrsl_table["laundry_original_place"] >= 6:
            scene.append([(562,852),"home bathroom laundry6",("home_bathroom_basket_clothes",117,-15)])
          if not quest.mrsl_table["laundry1_removed"]:
            scene.append([(1048,981),"home bathroom laundry1_down","home_bathroom_laundry1"])
          if not quest.mrsl_table["laundry2_removed"]:
            scene.append([(1021,753),"home bathroom laundry2_down","home_bathroom_laundry2"])
          if not quest.mrsl_table["laundry3_removed"]:
            scene.append([(345,935),"home bathroom laundry3_down","home_bathroom_laundry3"])
          if not quest.mrsl_table["laundry4_removed"]:
            scene.append([(498,767),"home bathroom laundry4_down","home_bathroom_laundry4"])
          if not quest.mrsl_table["laundry5_removed"]:
            scene.append([(856,98),"home bathroom laundry5_down",("home_bathroom_laundry5",0,200)])
          if not quest.mrsl_table["laundry6_removed"]:
            scene.append([(984,440),"home bathroom laundry6_down","home_bathroom_laundry6"])
          if not quest.mrsl_table["laundry7_removed"]:
            scene.append([(826,763),"home bathroom laundry7_down","home_bathroom_laundry7"])
        else:
          scene.append([(582,867),"home bathroom basket_down","home_bathroom_basket"])
          scene.append([(1048,981),"home bathroom laundry1_down"])
          scene.append([(1021,753),"home bathroom laundry2_down"])
          scene.append([(345,935),"home bathroom laundry3_down"])
          scene.append([(498,767),"home bathroom laundry4_down"])
          scene.append([(856,98),"home bathroom laundry5_down"])
          scene.append([(984,440),"home bathroom laundry6_down"])
          scene.append([(826,763),"home bathroom laundry7_down"])
      else:
        scene.append([(554,862),"home bathroom empty_basket","home_bathroom_laundry_basket"])
        if not home_bathroom["dirty_clothes_taken"]:
          scene.append([(672,851),"home bathroom laundry1",("home_bathroom_basket_clothes",6,-14)])
          scene.append([(745,870),"home bathroom laundry2",("home_bathroom_basket_clothes",-69,-33)])
          scene.append([(597,841),"home bathroom laundry3",("home_bathroom_basket_clothes",69,-4)])
          scene.append([(671,893),"home bathroom laundry4",("home_bathroom_basket_clothes",-11,-56)])
          scene.append([(656,838),"home bathroom laundry5",("home_bathroom_basket_clothes",3,-1)])
          scene.append([(562,852),"home bathroom laundry6",("home_bathroom_basket_clothes",117,-15)])
          scene.append([(782,851),"home bathroom laundry7",("home_bathroom_basket_clothes",-110,-14)])

      #Garbage
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["garbage_original_place"]:
        scene.append([(1288,702),"home bathroom garbage_down",("home_bathroom_garbage",-35,-10)])
        scene.append([(1213,728),"home bathroom trash",("home_bathroom_garbage",45,-35)])
      else:
        scene.append([(1387,679),"home bathroom garbage","home_bathroom_garbage"])

      #Sink
      if quest.mrsl_table == "laundry" and (not quest.mrsl_table["water_overflow_removed"] or not quest.mrsl_table["water_puddles_removed"]):
        scene.append([(1383,524),"home bathroom sink","home_bathroom_sink"])
        scene.append([(1497,463),"home bathroom sink_deco",("home_bathroom_sink",-49,61)])
        if not quest.mrsl_table["water_puddles_removed"]:
          scene.append([(1349,895),"home bathroom water_puddles","home_bathroom_water_puddles"])
        if not quest.mrsl_table["water_overflow_removed"]:
          scene.append([(1402,558),"home bathroom water_overflow","home_bathroom_water_overflow"])
      else:
        scene.append([(1383,524),"home bathroom sink","home_bathroom_sink"])
        scene.append([(1497,463),"home bathroom sink_deco",("home_bathroom_sink",-49,61)])
        if home_bathroom["sink_water"]:
          scene.append([(1454,572),"#home bathroom water"])
        if home_bathroom["sink_detergent"]:
          scene.append([(1460,548),"#home bathroom detergent"])
        if home_bathroom["sink_water"]:
          scene.append([(1481,437),"#home bathroom vapor"])
        if home_bathroom["washing"]:
          scene.append([(1495,560),"home bathroom clothes_sink","home_bathroom_clothes_sink"])
          scene.append([(1501,572),"home bathroom water_on_clothes"])

      #Tissues
      if not home_bathroom["tissues_taken_today"]:
        scene.append([(1580,606),"home bathroom tissues","home_bathroom_tissues"])

      #Clothesline
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["wet_laundry1_removed"] + quest.mrsl_table["wet_laundry2_removed"] + quest.mrsl_table["wet_laundry3_removed"] + quest.mrsl_table["wet_laundry4_removed"] == 4:
        if quest.mrsl_table["wet_laundry1_removed"] + quest.mrsl_table["wet_laundry2_removed"] + quest.mrsl_table["wet_laundry3_removed"] + quest.mrsl_table["wet_laundry4_removed"] < 1:
          scene.append([(1033,0),"home bathroom no_clothes",("home_bathroom_clothesline_empty",20,380)])
        else:
          scene.append([(1033,0),"home bathroom no_clothes"])
        if quest.mrsl_table["wet_laundry1_removed"]:
          scene.append([(1114,98),"home bathroom wet_laundry1",("home_bathroom_clothesline_full",167,282)])
        if quest.mrsl_table["wet_laundry2_removed"]:
          scene.append([(1196,119),"home bathroom wet_laundry2",("home_bathroom_clothesline_full",74,261)])
        if quest.mrsl_table["wet_laundry3_removed"]:
          scene.append([(1290,135),"home bathroom wet_laundry3",("home_bathroom_clothesline_full",-42,245)])
        if quest.mrsl_table["wet_laundry4_removed"]:
          scene.append([(1442,70),"home bathroom wet_laundry4",("home_bathroom_clothesline_full",-154,310)])
        if not quest.mrsl_table["wet_laundry1_removed"]:
          scene.append([(1434,922),"home bathroom wet_laundry1_down","home_bathroom_wet_laundry1"])
        if not quest.mrsl_table["wet_laundry2_removed"]:
          scene.append([(1230,643),"home bathroom wet_laundry2_down","home_bathroom_wet_laundry2"])
        if not quest.mrsl_table["wet_laundry3_removed"]:
          scene.append([(587,194),"home bathroom wet_laundry3_down","home_bathroom_wet_laundry3"])
        if not quest.mrsl_table["wet_laundry4_removed"]:
          scene.append([(173,469),"home bathroom wet_laundry4_down","home_bathroom_wet_laundry4"])
      elif quest.mrsl_HOT == "laundry":
        scene.append([(1033,0),"home bathroom no_clothes",("home_bathroom_clothesline_empty",20,380)])
      else:
        scene.append([(1033,0),"home bathroom no_clothes"])
        scene.append([(1114,98),"home bathroom wet_laundry1",("home_bathroom_clothesline_full",167,282)])
        scene.append([(1196,119),"home bathroom wet_laundry2",("home_bathroom_clothesline_full",74,261)])
        scene.append([(1290,135),"home bathroom wet_laundry3",("home_bathroom_clothesline_full",-42,245)])
        scene.append([(1442,70),"home bathroom wet_laundry4",("home_bathroom_clothesline_full",-154,310)])

      #Phone Ring
      if quest.flora_cooking_chilli == "flora_called":
        scene.append([(670,880),"alarm_noise"])

      if not home_bathroom['got_pocket_mirror'] and quest.clubroom_access.finished:
        scene.append([(1393,512),"home bathroom pocketmirror","home_bathroom_pocketmirror"])

      #Misc
      if home_bathroom["dollar1_spawned_today"] == True and not home_bathroom["dollar1_taken_today"]:
        scene.append([(852,566),"home bathroom dollar1","home_bathroom_dollar1"])
      if home_bathroom["dollar2_spawned_today"] == True and not home_bathroom["dollar2_taken_today"]:
        scene.append([(1494,66),"home bathroom dollar2",("home_bathroom_dollar2",0,200)])

      #Maya
      if maya.at("home_bathroom","drawing") or home_bathroom["lipstick_writing_this_scene"]:
        if maya.talking or home_bathroom["lipstick_writing_this_scene"]:
          scene.append([(1684,103),"#home bathroom lipstick_writing"])
        else:
          scene.append([(1285,267),"home bathroom maya_autumn"])
          scene.append([(1684,103),"#home bathroom lipstick_writing2"])

      if quest.maya_sauce == "siren_song":
        scene.append([(0,0),"#home bathroom steam_n_fog"])

      if (home_bathroom["night"]
      or quest.maya_quixote in ("attic","board_game")):
        scene.append([(0,0),"#home bathroom nightoverlay"])

    def find_path(self,target_location):
      ## there is only one way out, so we don't care about target location and other conditions
      return "home_bathroom_door"


label goto_home_bathroom:
  if home_bathroom.first_visit_today:
    $home_bathroom["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $home_bathroom["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(home_bathroom)
  return
