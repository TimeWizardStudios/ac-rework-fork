init python:
  class Interactable_home_bathroom_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_bathroom_dollar2_take"])


label home_bathroom_dollar2_take:
  $home_bathroom["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_bathroom_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_bathroom_dollar1_take"])


label home_bathroom_dollar1_take:
  $home_bathroom["dollar1_taken_today"] = True
  $mc.money+=20
  return
