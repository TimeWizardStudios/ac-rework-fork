init python:
  class Interactable_home_laundry_instructions(Interactable):

    def title(cls):
      return "Laundry Instructions"

    def description(cls):
      return "A neatly organized list of slave labor."

    def actions(cls,actions):
      actions.append(["interact","Read","?home_laundry_instructions_interact"])


label home_laundry_instructions_interact:
  "{i}\"Step 1: Fill the sink with hot water.\"{/}"
  "{i}\"Step 2: Add laundry detergent.\"{/}"
  "{i}\"Step 3: Wash the clothes in the laundry basket.\"{/}"
  "{i}\"Step 4: Hang the laundry to dry.\"{/}"
  $home_bathroom["laundry_instructions_read"] = True
  return
