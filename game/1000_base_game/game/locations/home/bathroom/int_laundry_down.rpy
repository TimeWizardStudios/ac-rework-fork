init python:
  class Interactable_home_bathroom_basket(Interactable):

    def title(cls):
      return "Laundry Basket"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_basket_interact")


init python:
  class Interactable_home_bathroom_laundry1(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry1_interact")


init python:
  class Interactable_home_bathroom_laundry2(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry2_interact")


init python:
  class Interactable_home_bathroom_laundry3(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry3_interact")


init python:
  class Interactable_home_bathroom_laundry4(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry4_interact")


init python:
  class Interactable_home_bathroom_laundry5(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry5_interact")


init python:
  class Interactable_home_bathroom_laundry6(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry6_interact")


init python:
  class Interactable_home_bathroom_laundry7(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry7_interact")


init python:
  class Interactable_home_bathroom_laundry8(Interactable):

    def title(cls):
      return "Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_laundry8_interact")


init python:
  class Interactable_home_bathroom_wet_laundry1(Interactable):

    def title(cls):
      return "Wet Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_wet_laundry1_interact")


init python:
  class Interactable_home_bathroom_wet_laundry2(Interactable):

    def title(cls):
      return "Wet Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_wet_laundry2_interact")


init python:
  class Interactable_home_bathroom_wet_laundry3(Interactable):

    def title(cls):
      return "Wet Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_wet_laundry3_interact")


init python:
  class Interactable_home_bathroom_wet_laundry4(Interactable):

    def title(cls):
      return "Wet Laundry"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_wet_laundry4_interact")
