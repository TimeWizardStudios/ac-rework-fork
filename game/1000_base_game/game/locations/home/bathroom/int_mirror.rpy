init python:
  class Interactable_home_bathroom_mirror(Interactable):

    def title(cls):
      return "Mirror"

    def description(cls):
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["lipstick_writing_removed"]:
        return "Pretty cute, but [jo] might not see it that way."
      elif quest.mrsl_HOT.started:
        return "Hello there, hotshot! Ugh, why is my reflection flipping me off?"
      elif quest.flora_cooking_chilli.started:
        return "Hmm... there's dirt on the glass... wait, that's my reflection."
      elif quest.flora_jacklyn_introduction.started:
        return "Becoming a vampire would solve so many problems for me. No reflection and fawning Twilight fangirls."
      elif quest.wash_hands.started:
        return "Through the looking-glass, nothing but disappointment."
      elif quest.natures_call.started:
        return "Mirror, mirror on the wall... oh, who am I kidding?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and not quest.mrsl_table["lipstick_writing_removed"]:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_table,lipstick_writing|Use What?","quest_mrsl_table_home_bathroom_mirror_use_item"])
            actions.append("?quest_mrsl_table_home_bathroom_mirror_interact")
            return
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "hairdo":
            actions.append("quest_jacklyn_town_hairdo")
      if quest.mrsl_HOT.started:
        actions.append("?home_bathroom_mirror_interact_mrsl_HOT")
      if quest.flora_cooking_chilli.started:
        actions.append("?home_bathroom_mirror_interact_flora_cooking_chilli")
      if quest.flora_jacklyn_introduction.started:
        actions.append("?home_bathroom_mirror_interact_flora_jacklyn_introduction")
      if quest.wash_hands.started:
        actions.append("?home_bathroom_mirror_interact_wash_hands")
      if quest.natures_call.started:
        actions.append("?home_bathroom_mirror_interact_natures_call")

    #def circle(cls,actions,x,y):
    #  rv =super().circle(actions,x,y)
    #  rv.start_angle=-128
    #  rv.angle_per_icon= 32
    #  #rv.center=(0,300)
    #  return rv


label home_bathroom_mirror_interact_mrsl_HOT:
  "It's just a piece of glass. It has no power over me."
  "Hmm... motivating yourself only works in fiction."
  return

label home_bathroom_mirror_interact_natures_call:
  "No reason to screw over the rotting remains of my self-esteem this early in the day."
  return

label home_bathroom_mirror_interact_wash_hands:
  "The single object that has ruined more days for me than any other person, place, or thing."
  return

label home_bathroom_mirror_interact_flora_jacklyn_introduction:
  "The good thing about having a bad reflection is that you'll never succumb to the same fate as Narcissus."
  return

label home_bathroom_mirror_interact_flora_cooking_chilli:
  "Bloody [kate]..."
  "Bloody [kate]..."
  "Nah, can't do it. Too scary."
  return
