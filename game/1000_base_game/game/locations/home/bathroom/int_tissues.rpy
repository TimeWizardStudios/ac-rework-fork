init python:
  class Interactable_home_bathroom_tissues(Interactable):
    def title(cls):
      return "Box of Tissues"
    def description(cls):
      return "For drying tears and other\nbodily fluids...\n\nSweat, for example."
    def actions(cls,actions):
      actions.append(["take","Take","home_bathroom_tissues_take"])

label home_bathroom_tissues_take:
  "These boxes always seem to run out. Better bring an extra."
  $home_bathroom["tissues_taken_today"] = True
  $mc.add_item("tissues",5)
  return
