init python:
  class Interactable_home_bathroom_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if quest.mrsl_HOT.in_progress:
        return "Don't let the door hit you on the way out. Or on the way in. Really, if it hits you, file assault charges."
      elif quest.flora_jacklyn_introduction.in_progress:
        return "Organized a petition to remove this door once. Lost the vote 1-2. The majority of voters in this household have very antiquated views on privacy."
      elif quest.flora_cooking_chilli.in_progress:
        return "The way out of this shit hole.\n\nThat was a bit harsh; it's a nice toilet..."
      else:
        return "A bathroom door with a\nworking lock.\n\nFor now."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "flora_called":
            actions.append(["go","Hallway","home_bathroom_door_interact_flora_cooking_chilli_flora_called"])
        elif mc["focus"] == "maya_quixote":
          if quest.maya_quixote == "text":
            actions.append(["go","Hallway","?quest_maya_quixote_text_home_bathroom_door"])
            return
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "hairdo":
            actions.append(["go","Hallway","?quest_jacklyn_town_hairdo_door"])
            return
      else:
        if quest.natures_call.in_progress:
          actions.append(["go","Bedroom","?home_bathroom_door_interact_natures_call"])
          return
        if quest.wash_hands.in_progress:
          actions.append(["go","Bedroom","?home_bathroom_door_interact_wash_hands"])
          return
        if quest.dress_to_the_nine.in_progress:
          actions.append(["go","Bedroom","goto_home_bedroom"])
        if quest.mrsl_HOT.in_progress:
          actions.append(["go","Hallway","home_bathroom_door_interact_mrsl_hot"])
        if home_bathroom["washing"]:
          actions.append(["go","Hallway","?home_bathroom_door_interact_mrsl_hot_washing"])
          return
      actions.append(["go","Hallway","goto_home_hall"])


label home_bathroom_door_interact_mrsl_hot_washing:
  "I'm so close to being done... might as well finish."
  return

label home_bathroom_door_interact_natures_call:
  "First, my business at the facilities."
  return

label home_bathroom_door_interact_wash_hands:
  "I've done a lot of disgusting things in my life, but I'm not leaving without washing my hands."
  return

label home_bathroom_door_interact_flora_cooking_chilli_flora_called:
  $ quest.flora_cooking_chilli.advance("chilli_done")
  jump goto_home_hall
  return

label home_bathroom_door_interact_mrsl_hot:
  call mrsl_quest_HOT_laundry_reset
  jump goto_home_hall
  return
