init python:
  class Interactable_home_bathroom_water_puddles(Interactable):

    def title(cls):
      return "Water Puddles"

    def description(cls):
      return "Wetter than [flora]'s pussy at the octopod aquarium."

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_table,water_puddles|Use What?","quest_mrsl_table_home_bathroom_water_puddles_use_item"])
      actions.append("?quest_mrsl_table_home_bathroom_water_puddles_interact")
