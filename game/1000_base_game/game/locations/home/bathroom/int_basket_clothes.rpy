init python:
  class Interactable_home_bathroom_basket_clothes(Interactable):

    def title(cls):
      return "Dirty Clothes"

    def description(cls):
      if quest.mrsl_HOT=="laundry":
        return "[jo]'s and [flora]'s undies in a pile of forbidden smells. Not that I've ever taken a whiff. That would be weird."
      elif quest.dress_to_the_nine.started:
        return "Dirty laundry that still smells kind of nice. Typical women."
      else:
        return "Who's on laundry duty in this house? That person needs to come clean."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "flora_called":
            actions.append("home_bathroom_clothes_flora_cooking_chilli_phone")
      else:
        if quest.mrsl_HOT == "laundry":
          actions.append(["take","Take","home_bathroom_clothes_mrsl_HOT"])
      if quest.dress_to_the_nine.started:
        actions.append("?home_bathroom_laundry_basket_interact_dress_to_the_nine")
      actions.append("?home_bathroom_clothes_interact")


label home_bathroom_clothes_interact:
  "Been caught with my hands in this basket more times than I'd like to admit."
  return

label home_bathroom_clothes_mrsl_HOT:
  "I have written permission to stick my grubby fingers in here this time."
  $home_bathroom["dirty_clothes_taken"] = True
  $mc.add_item("dirty_laundry")
  if quest.flora_cooking_chilli in ("flora_called", "chilli_done"):
    "There seems to be something hard and long in here."
    jump home_bathroom_clothes_flora_cooking_chilli_phone
  return

label home_bathroom_clothes_flora_cooking_chilli_phone:
  "There you are, you sneaky little bastard."
  $mc.add_item("flora_phone")
  $ quest.flora_cooking_chilli.advance("return_phone")
  return

label home_bathroom_laundry_basket_interact_dress_to_the_nine:
  "Why are women's underthings so much nicer than men's?"
  "What if I want lace, frills, and little ribbons?"
  return
