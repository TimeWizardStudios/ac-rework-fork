init python:
  class Interactable_home_bathroom_clothesline_empty(Interactable):

    def title(cls):
      return "Clothing Line"

    def description(cls):
      if quest.mrsl_table == "laundry" and not quest.mrsl_table["clothesline_original_place"]:
        return "It's weaker than it looks... can't even hold the weight of one man."
      else:
        return "Perfect to hang stuff — clothes, dreams, yourself."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and not quest.mrsl_table["clothesline_original_place"]:
            actions.append("?quest_mrsl_table_home_bathroom_clothesline_empty_interact")
            return
      else:
        if mc.owned_item("clean_laundry"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:mrsl_HOT,clean_laundry|Use What?","quest_mrsl_HOT_hang_laundry"])
      actions.append("?home_bathroom_clothesline_empty_interact")


label quest_mrsl_HOT_hang_laundry(item):
  if item=="clean_laundry":
    $quest.mrsl_HOT.advance("jo_talk_laundry_done")
    $mc.remove_item(item)
    "Aaaand done! [jo] better have those videotapes for me now."
  else:
    "I'm not leaving my [item.title_lower] hung out to dry!"
    $quest.mrsl_HOT.failed_item("clean_laundry",item)
  return

label home_bathroom_clothesline_empty_interact:
  "This seems like a good place to hang up my boots. To dry, of course. No one's retiring yet."
  return
