init python:
  class Interactable_home_bathroom_bathrobe(Interactable):

    def title(cls):
      return "Bathrobe"

    def description(cls):
      return ""

    def actions(cls,actions):
      actions.append("quest_mrsl_table_home_bathroom_bathrobe_interact")
