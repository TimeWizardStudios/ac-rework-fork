### home_bathroom_sugarcube5 interactable ######################################

init python:
  class Interactable_home_bathroom_sugarcube6(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_bathroom_sugarcube6_take"])

label home_bathroom_sugarcube6_take:
  $home_bathroom["sugarcube6_taken"] = True
  $mc.add_item("sugar_cube")
  return

### home_bathroom_sugarcube4 interactable ######################################

init python:
  class Interactable_home_bathroom_sugarcube7(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_bathroom_sugarcube7_take"])

label home_bathroom_sugarcube7_take:
  $home_bathroom["sugarcube7_taken"] = True
  $mc.add_item("sugar_cube")
  return

