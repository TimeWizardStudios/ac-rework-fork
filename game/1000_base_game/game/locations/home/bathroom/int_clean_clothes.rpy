init python:
  class Interactable_home_bathroom_clothesline_full(Interactable):

    def title(cls):
      return "Washed Clothes"

    def description(cls):
      if quest.mrsl_table >= "laundry" and quest.mrsl_table["clothesline_original_place"]:
        return "The lewd flag of a female-dominated household."
      else:
        return "Washed... what a shame."

    def actions(cls,actions):
      if quest.mrsl_table >= "laundry" and quest.mrsl_table["clothesline_original_place"]:
        actions.append("?quest_mrsl_table_home_bathroom_clothesline_full_interact")
      actions.append("?home_bathroom_clothesline_full_interact")


label home_bathroom_clothesline_full_interact:
  "Out of reach."
  "My height ruining my life, yet again."
  return
