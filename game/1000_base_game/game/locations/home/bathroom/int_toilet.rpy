init python:
  class Interactable_home_bathroom_toilet(Interactable):
    
    def title(cls):
      return "Toilet"
    
    def description(cls):


      if quest.mrsl_HOT.started:
        return "The best part of the bathroom, but also the worst part of the bathroom. A Tale of Two toilets."
        
      if quest.flora_cooking_chilli.started:
        return "Has anyone ever tried fishing in one of these? Who knows what treasures you may find!"
      
      elif quest.wash_hands.started:
        return "Don't think of Sonic the Toilet...\nDon't think of Sonic the Toilet...\nDon't think of Sonic the Toilet...\n\nFuck!"
      
      elif quest.natures_call.started:
        return "Might splash or flood. Use with extreme caution."
    
    def actions(cls,actions):
      
      if mc.owned_item("spray_empty_bottle"):
        actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:flora_bonsai,toilet|Use What?","quest_bathroom_toilet_interact_spray"])
      if quest.mrsl_HOT.started:
        actions.append("?home_bathroom_toilet_interact_mrsl_HOT_laundry")
        
      elif quest.flora_cooking_chilli.started:
        actions.append("?home_bathroom_toilet_interact_flora_cooking_chilli")

      elif quest.wash_hands.started:
        actions.append("?home_bathroom_toilet_interact_wash_hands")

      elif quest.natures_call.started:
        actions.append("?home_bathroom_toilet_interact_natures_call")

label quest_bathroom_toilet_interact_spray(item):
  if item == "spray_empty_bottle":
    show home bathroom events using_toilet with fadehold
    "Ahh! Just like in the old raiding days."
    hide home bathroom events using_toilet with Dissolve(.5)
    $mc.remove_item(item)
    $mc.add_item("spray_urine")
  else:
    "Some would probably argue that it's time to flush my [item.title_lower] down the toilet, but maybe there's still hope."
    $quest.flora_bonsai.failed_item("toilet",item)
  return

label home_bathroom_toilet_interact_mrsl_HOT_laundry:
  show home bathroom events using_toilet with fadehold
  "Ahh! Glorious release."
  "A weight lifted off my bladder."
  hide home bathroom events using_toilet with Dissolve(.5)
  return

label home_bathroom_toilet_interact_natures_call:
  scene home bathroom events using_toilet with fadehold
#  show home bathroom events using_toilet with fadehold
  "..."
  "Is there anything better than emptying a full bladder?"
  "Maybe sex, love, or joy. But those things don't come easily... or ever, really."
  "..."
  "Ahh, just like that."
  scene location with Dissolve(0.5)
#  hide home bathroom events using_toilet with Dissolve(.5)
  $quest.natures_call.finish()
  $quest.wash_hands.start()
  return
  
  
label home_bathroom_toilet_interact_wash_hands:
  "Never hurts to flush an extra time!"
  "Unless it's clogged..."
  "Or you live in Cali..."
  "Or you've injured your flushing-hand..."
  "Nevermind."
  return

label home_bathroom_toilet_interact_flora_cooking_chilli:
  "God damn... it feels like days since my last leak."
  "..."
  "Luckily, my bladder is immense."
  return
