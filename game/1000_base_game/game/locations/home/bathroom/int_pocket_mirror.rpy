init python:
  class Interactable_home_bathroom_pocketmirror(Interactable):

    def title(cls):
      return "Pocket Mirror"

    def description(cls):
      return "[flora]'s favorite pocket mirror. It would be a shame if something happened to it..."

    def actions(cls,actions):
      actions.append(["take","Take","home_bathroom_pocket_mirror_take"])
      actions.append("?home_bathroom_pocketmirror_interact")


label home_bathroom_pocketmirror_interact:
  "Everything apart from the glass is pretty cute, and that's not the designer's fault."
  return

label home_bathroom_pocket_mirror_take:
  "Finders keepers, [flora] — you know that's the rule here."
  $home_bathroom["got_pocket_mirror"] = True
  $mc.add_item("pocket_mirror")
  return
