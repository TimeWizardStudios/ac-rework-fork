init python:
  class Interactable_home_bathroom_water_overflow(Interactable):

    def title(cls):
      return "Water Overflowing"

    def description(cls):
      return "As usual, my charm and good looks aren't the cause of this lady's overflowing juices."

    def actions(cls,actions):
      actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_table,water_overflow|Use What?","quest_mrsl_table_home_bathroom_water_overflow_use_item"])
      actions.append("?quest_mrsl_table_home_bathroom_water_overflow_interact")
