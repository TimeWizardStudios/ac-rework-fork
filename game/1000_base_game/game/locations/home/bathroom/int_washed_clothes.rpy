init python:
  class Interactable_home_bathroom_clothes_sink(Interactable):

    def title(cls):
      return "Wet Clothes"

    def description(cls):
      return "I can feel the pheromones washing away."

    def actions(cls,actions):
      actions.append(["take","Take","?home_bathroom_clothes_sink_take"])
      actions.append("?home_bathroom_clothes_sink_interact")


label home_bathroom_clothes_sink_interact:
  "This better secure my spot in heaven."
  return

label home_bathroom_clothes_sink_take:
  "Now [jo] can't say I never did anything for her."
  call mrsl_quest_HOT_laundry_reset
  $mc.add_item("clean_laundry")
  return
