init python:
  class Interactable_home_bathroom_bathtub(Interactable):
    
    def title(cls):
      return "Bathtub"
    
    def description(cls):
      return "Why would I spend time showering if I'm just going to get dirty the next day anyway?"
    
    def actions(cls,actions):
      actions.append("?home_bathroom_bathtub_interact")

label home_bathroom_bathtub_interact:
  "I used to be 5'7 and balding but then I took that mythical shower and now I'm a 6'2 Aryan wet dream."
  return
