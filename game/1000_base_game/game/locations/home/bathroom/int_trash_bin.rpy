init python:
  class Interactable_home_bathroom_garbage(Interactable):

    def title(cls):
      return "Trash Bin"

    def description(cls):
      if not (quest.mrsl_table == "laundry" and not quest.mrsl_table["garbage_original_place"]):
        return "It's filled with used tampons and dirty tissues. Are they [jo]'s or [flora]'s?\n\nOnly one way to find out."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and not quest.mrsl_table["garbage_original_place"]:
            actions.append("quest_mrsl_table_home_bathroom_garbage_interact")
      actions.append("?home_bathroom_garbage_interact")


label home_bathroom_garbage_interact:
  "Nothing good can be gained from this."
  "Nothing bad either... apart from dirty hands."
  "Screw it! I'm going in!"
  return
