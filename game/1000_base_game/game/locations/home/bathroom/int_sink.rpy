init python:
  class Interactable_home_bathroom_sink(Interactable):

    def title(cls):
      return "Sink"

    def description(cls):
      if quest.mrsl_table == "laundry" and quest.mrsl_table["water_overflow_removed"]:
        return "It's never been so clean. I can almost see my—\n\nNot this time, ugly satan."
      elif quest.mrsl_HOT=="laundry":
        return "It doesn't matter how many times you wish for it, you can't clean off ugly."
      elif quest.flora_cooking_chilli.started:
        return "Soap might be too weak for this job. Where did [jo] put the bleach?"
      elif quest.flora_jacklyn_introduction.started:
        return "Sink or swim, my hands get washed now."
      elif quest.natures_call.started:
        return "Sink, water, and soap — the only thing missing is hope."
      else:
        return "Sink Fun Fact 317 — cold water makes the ejaculate drain easier. Hot water denatures the proteins."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "laundry" and quest.mrsl_table["water_overflow_removed"]:
            actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:mrsl_table,wet_tissue|Use What?","quest_mrsl_table_home_bathroom_sink_use_item"])
            actions.append("?quest_mrsl_table_home_bathroom_sink_interact")
            return
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "hairdo":
            actions.append("quest_jacklyn_town_hairdo")
      else:
        if quest.mrsl_HOT == "laundry":
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:mrsl_HOT,sink|Use What?","home_bathroom_sink_use_mrsl_HOT"])
        if mc.owned_item("kate_socks_dirty"):
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:act_one,sink|Use What?","home_bathroom_sink_use_socks"])
        if quest.wash_hands.in_progress:
          actions.append(["interact","Wash hands","home_bathroom_sink_interact_wash_hands"])
        if quest.mrsl_HOT == "laundry":
          actions.append("?home_bathroom_sink_interact_mrsl_HOT_laundry")
          return
      if quest.flora_cooking_chilli.started:
        actions.append("?home_bathroom_sink_interact_flora_cooking_chilli")
      if quest.flora_jacklyn_introduction.started:
        actions.append("?home_bathroom_sink_interact_flora_jacklyn_introduction")
      if quest.dress_to_the_nine.started:
        actions.append("?home_bathroom_sink_interact_dress_to_the_nine")
      actions.append("?home_bathroom_sink_interact")


label home_bathroom_sink_use_socks(item):
  if item == "kate_socks_dirty":
    "I wonder what [kate] will think when I return her socks perfectly clean...{space=-45}"
    "It's weird, but I can't wait to feel that shame she'll undoubtedly pour on."
    "..."
    $mc.remove_item("kate_socks_dirty")
    $mc.add_item("kate_socks_clean")
  else:
    "Whoa, there! I almost got my non-waterproof [item.title_lower] wet!"
    $quest.act_one.failed_item("sink",item)
  return

label home_bathroom_sink_use_mrsl_HOT(item):
  if item=="tide_pods":
    if home_bathroom["sink_detergent"]:
      $mc.remove_item("tide_pods")
      "One was enough... but I guess this makes up for all my other missed laundry days."
    elif home_bathroom["sink_water"]:
      $home_bathroom["sink_detergent"] = True
      $mc.remove_item("tide_pods")
      "There we go. Ready to wash some clothes."
    else:
      $mc.remove_item("tide_pods")
      "Great, I ruined it. The apocalypse draws nigh."
      "Better follow the instructions this time..."
      call mrsl_quest_HOT_laundry_reset
  elif item == "dirty_laundry":
    if home_bathroom["sink_detergent"]:
      $home_bathroom["washing"] = True
      $mc.remove_item("dirty_laundry")
      #SFX: Water splashing/washing sounds
      ## Splash of water
      play sound "<to 2.5>water_splash" loop
      "Washy."
      "Washy, washy."
      "Washy, washy, washy. There. Probably done."
      stop sound fadeout 0.5
    else:
      "Great, I ruined it. The apocalypse draws nigh."
      "Better follow the instructions this time."
      call mrsl_quest_HOT_laundry_reset
  else:
    "My [item.title_lower] is already pretty clean. I don't think it needs a scrubbing."
    "Errr... or at least this is as clean as it will ever get."
    $quest.mrsl_HOT.failed_item("sink",item)
  return

label home_bathroom_sink_interact_mrsl_HOT_laundry:
  if not home_bathroom["sink_water"]:
    #SFX: running tap/water
    #Show SinkWithWater01
    ## Running water
    play sound "running_tap_water" loop
    $home_bathroom["sink_water"] = True
    "Nice and hot. Need to add laundry detergent now."
    stop sound fadeout 0.5
  else:
    if not home_bathroom["sink_detergent"]:
      "Need to add laundry detergent now."
    else:
      "Nice and hot." #TBD actual dialogue for interact if sink is already full and has detergent in it
  return

label home_bathroom_sink_interact_wash_hands:
  "The monotony of staying hygienic."
  "Looking in the mirror will give me my daily dose of emotion — distilled misery."
  "Is it masochism to seek out this pain every morning, or just a need to feel something at all?"
  "Either way, it's better than looking at the dried boogers on the porcelain."
  "..."
  "..."
  "Hold on..."
  "The face of a true loser stares back from the mirror. Pale with a moonscape of zits and acne."
  "Vulture neck, dark eye-rings, weak jawline..."
  "But something doesn't seem quite right."
  "Did [jo] cut my hair in my sleep? That bowl-cut used to be my mark of shame in high school, but I've let it grow since."
  "Those few hours of sleep sucked, but somehow, my face looks younger... more rested. Fewer lines of despair and grief."
  "Also, less facial hair. Less weight. Less... of an urge to end it all..."
  "The scar above my eye from tumbling down the stage at graduation. It's gone."
  "What the hell...?"
  "The nerd in the mirror is definitely younger — high school age, perhaps."
  "It's like the late nights in front of the computer haven't caught up with him yet."
  "Like the fast food and soft drinks haven't left their mark on his body."
  "He's still me — the nervous eyes, the bad posture, the lack of confidence — but, somehow, a less broken version."
  "What the fuck is going on?"
  jo "[mc]! You and [flora] will have to catch the bus today. I have to leave early."
  "The bus? The bus to where?"
  jo "[mc], did you hear me? If you miss the bus, you'll regret it!"
  jo "Do you know how bad it looks if you're late on the first day? This is your last year! Just graduate for goodness' sakes!"
  "[jo]'s poker face is worse than Lady Gaga's. So, if this is a prank, she's really killing it."
  "There's also that hope in her voice, which I thought had died years ago."
  "First, I get a strange text. Then, the alarm goes off out of nowhere. My face changes. And now this?"
  "Did I just wake up from the nightmare that'll be my life? Or did I actually travel back in time?"
  "Oh, god. What if I have to live through all the suffering a second time?"
  jo "[mc]! Answer me!"
  menu(side="middle"):
    extend ""
    "\"Err... yes, [jo]! I'll be down in a minute!\"":
      mc "Err... yes, [jo]! I'll be down in a minute!"
      $jo.love+=1
      jo "Okay, but hurry up! First day of school! Make yourself presentable!"
    "\"Is this a joke?\"":
      mc "Is this a joke?"
      jo "Do I sound like I'm joking, [mc]? Get dressed and come down now!"
    "\"I'm going back to bed! See you tonight!\"":
      mc "I'm going back to bed! See you tonight!"
      $jo.love-=1
      jo "Don't test me, young man! You'll be down here in five minutes, or you can kiss your computer goodbye!"
  "She sounds serious. Better not antagonize her any further."
  $quest.wash_hands.finish()
  $quest.dress_to_the_nine.start()
  return

label home_bathroom_sink_interact_dress_to_the_nine:
  "Oh shit! Splashed water all over my boxers. "
  "Looks like pee. "
  "What a shame."
  return

label home_bathroom_sink_interact_flora_jacklyn_introduction:
  "What do you call it when you're sent back through the years and given another chance at life, but spend it washing your hands?"
  "A time sink."
  return

label home_bathroom_sink_interact_flora_cooking_chilli:
  "God, I wish I never stroke. Now I gotta wash my hand out with soap."
  return

label home_bathroom_sink_interact:
  "For years, my goal has been to wash off the filth, but I'm starting to think it's part of my personality."
  return
