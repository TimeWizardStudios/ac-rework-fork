init python:
  class Interactable_home_kitchen_flora_pants(Interactable):

    def title(cls):
      return "[flora]'s Pants"

    def description(cls):
      return "Typical [flora] — always leaves her stuff all over the house."

    def actions(cls,actions):
      if quest.flora_cooking_chilli.finished:
        actions.append("?home_kitchen_flora_pants_interact_two")
      else:
        actions.append("?home_kitchen_flora_pants_interact")


label home_kitchen_flora_pants_interact:
  "Distressed. In more ways than one."
  return

label home_kitchen_flora_pants_interact_two:
  "Here it is! My chance to get into [flora]'s pants!"
  return
