init python:
  class Interactable_home_kitchen_sauce_wench(Interactable):

    def title(cls):
      return "{i}\"The Saucy Wench\"{/}"

    def description(cls):
      # return "{i}\"Blowville Scale: 2 out of 5 peppers.\"{/}"
      return "{i}\"Blowville Scale:\n2 out of 5 peppers.\"{/}"

    def actions(cls,actions):
      actions.append("?home_kitchen_sauce_wench_interact")


label home_kitchen_sauce_wench_interact:
  show misc sauce_wench with Dissolve(.5)
  pause 0.125
  "{i}\"Do you like your hot sauce like your women?\"{/}"
  "{i}\"Hot, sexy, and accompanied with a dripping taco?\"{/}"
  window hide
  hide misc sauce_wench with Dissolve(.5)
  $quest.maya_sauce["hot_sauces"].add("sauce_wench")
  return
