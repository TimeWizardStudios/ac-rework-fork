init python:
  class Interactable_home_kitchen_pizza_boxes(Interactable):

    def title(cls):
      return " Pizza Boxes"

    def description(cls):
      return "There's that heavenly aroma of cheese, sauce, garlic, and crack."

    def actions(cls,actions):
      # actions.append("?home_kitchen_pizza_boxes_interact")
      actions.append("quest_maxine_dive_armor_pizza_boxes")
