init python:
  class Interactable_home_kitchen_package_gigglypuff_seeds(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Unmarked except for the address."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_gigglypuff_seeds_take"])
      actions.append("?home_kitchen_package_gigglypuff_seeds_interact")


label home_kitchen_package_gigglypuff_seeds_take:
  "Glad I found the package first. No awkward questions."
  $mc.add_item("package_gigglypuff_seeds")
  $home_kitchen["package"] = False
  return

label home_kitchen_package_gigglypuff_seeds_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_magnet(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Unmarked except for the address."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_magnet_take"])
      actions.append("?home_kitchen_package_magnet_interact")


label home_kitchen_package_magnet_take:
  $home_kitchen["lines_magnet"] = False
  $mc.add_item("package_magnet")
  return

label home_kitchen_package_magnet_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_ball_of_yarn(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_ball_of_yarn_take"])
      actions.append("?home_kitchen_package_ball_of_yarn_interact")


label home_kitchen_package_ball_of_yarn_take:
  "Oh, a package for me!"
  window hide
  $home_computer["ball_of_yarn_received"] = True
  $mc.add_item("package_ball_of_yarn")
  return

label home_kitchen_package_ball_of_yarn_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_stick(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_stick_take"])
      actions.append("?home_kitchen_package_stick_interact")


label home_kitchen_package_stick_take:
  "Oh, a package for me!"
  window hide
  $home_computer["stick_received"] = True
  $mc.add_item("package_stick")
  return

label home_kitchen_package_stick_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_mrsl_brooch(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_mrsl_brooch_take"])
      actions.append("?home_kitchen_package_mrsl_brooch_interact")


label home_kitchen_package_mrsl_brooch_take:
  "Oh, a package for me!"
  window hide
  $home_computer["mrsl_brooch_received"] = True
  $mc.add_item("package_mrsl_brooch")
  return

label home_kitchen_package_mrsl_brooch_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_nymphoria_heartus(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_nymphoria_heartus_take"])
      actions.append("?home_kitchen_package_nymphoria_heartus_interact")


label home_kitchen_package_nymphoria_heartus_take:
  "Oh, a package for me!"
  window hide
  $home_computer["nymphoria_heartus_received"] = True
  $mc.add_item("package_nymphoria_heartus")
  return

label home_kitchen_package_nymphoria_heartus_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_venus_vagenis(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_venus_vagenis_take"])
      actions.append("?home_kitchen_package_venus_vagenis_interact")


label home_kitchen_package_venus_vagenis_take:
  "Oh, a package for me!"
  window hide
  $home_computer["venus_vagenis_received"] = True
  $mc.add_item("package_venus_vagenis")
  return

label home_kitchen_package_venus_vagenis_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_bag_of_weed(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_bag_of_weed_take"])
      actions.append("?home_kitchen_package_bag_of_weed_interact")


label home_kitchen_package_bag_of_weed_take:
  "Oh, a package for me!"
  window hide
  $home_computer["bag_of_weed_received"] = True
  $mc.add_item("package_bag_of_weed")
  return

label home_kitchen_package_bag_of_weed_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_beaver_pelt(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_beaver_pelt_take"])
      actions.append("?home_kitchen_package_beaver_pelt_interact")


label home_kitchen_package_beaver_pelt_take:
  "Oh, a package for me!"
  window hide
  $home_computer["beaver_pelt_received"] = True
  $mc.add_item("package_beaver_pelt")
  return

label home_kitchen_package_beaver_pelt_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_baseball_bat(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_baseball_bat_take"])
      actions.append("?home_kitchen_package_baseball_bat_interact")


label home_kitchen_package_baseball_bat_take:
  "Oh, a package for me!"
  window hide
  $home_computer["baseball_bat_received"] = True
  $mc.add_item("package_baseball_bat")
  return

label home_kitchen_package_baseball_bat_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_sleeping_pills(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_sleeping_pills_take"])
      actions.append("?home_kitchen_package_sleeping_pills_interact")


label home_kitchen_package_sleeping_pills_take:
  "Oh, a package for me!"
  window hide
  $home_computer["sleeping_pills_received"] = True
  $mc.add_item("package_sleeping_pills")
  return

label home_kitchen_package_sleeping_pills_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_cutie_harlot(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_cutie_harlot_take"])
      actions.append("?home_kitchen_package_cutie_harlot_interact")


label home_kitchen_package_cutie_harlot_take:
  "Oh, a package for me!"
  window hide
  $home_computer["cutie_harlot_received"] = True
  $mc.add_item("package_cutie_harlot")
  return

label home_kitchen_package_cutie_harlot_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_pig_mask(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_pig_mask_take"])
      actions.append("?home_kitchen_package_pig_mask_interact")


label home_kitchen_package_pig_mask_take:
  "Oh, a package for me!"
  window hide
  $home_computer["pig_mask_received"] = True
  $mc.add_item("package_pig_mask")
  return

label home_kitchen_package_pig_mask_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_hex_vex_and_texmex(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_hex_vex_and_texmex_take"])
      actions.append("?home_kitchen_package_hex_vex_and_texmex_interact")


label home_kitchen_package_hex_vex_and_texmex_take:
  "Oh, a package for me!"
  window hide
  $home_computer["hex_vex_and_texmex_received"] = True
  $mc.add_item("package_hex_vex_and_texmex")
  return

label home_kitchen_package_hex_vex_and_texmex_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_hearshey_shesay(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_hearshey_shesay_take"])
      actions.append("?home_kitchen_package_hearshey_shesay_interact")


label home_kitchen_package_hearshey_shesay_take:
  "Oh, a package for me!"
  window hide
  $home_computer["hearshey_shesay_received"] = True
  $mc.add_item("package_hearshey_shesay")
  return

label home_kitchen_package_hearshey_shesay_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_tob_le_bone(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_tob_le_bone_take"])
      actions.append("?home_kitchen_package_tob_le_bone_interact")


label home_kitchen_package_tob_le_bone_take:
  "Oh, a package for me!"
  window hide
  $home_computer["tob_le_bone_received"] = True
  $mc.add_item("package_tob_le_bone")
  return

label home_kitchen_package_tob_le_bone_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_dick_wonky(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_dick_wonky_take"])
      actions.append("?home_kitchen_package_dick_wonky_interact")


label home_kitchen_package_dick_wonky_take:
  "Oh, a package for me!"
  window hide
  $home_computer["dick_wonky_received"] = True
  $mc.add_item("package_dick_wonky")
  return

label home_kitchen_package_dick_wonky_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return




init python:
  class Interactable_home_kitchen_package_milk(Interactable):

    def title(cls):
      return "Mysterious Package"

    def description(cls):
      return "Looks like Christmas came early!\n\nI should probably grab it before [flora] or [jo] sees it. There could be viagra or porn inside."

    def actions(cls,actions):
      actions.append(["take","Take","?home_kitchen_package_milk_take"])
      actions.append("?home_kitchen_package_milk_interact")


label home_kitchen_package_milk_take:
  "Oh, a package for me!"
  window hide
  $home_computer["milk_received"] = True
  $mc.add_item("package_milk")
  return

label home_kitchen_package_milk_interact:
  "Letting it sit too long on the counter might cause all sorts of uncomfortable questions."
  return
