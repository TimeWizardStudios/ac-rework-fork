init python:
  class Interactable_home_kitchen_sauce_taco_hell(Interactable):

    def title(cls):
      return "{i}\"Taco Hell Hot Sauce\"{/}"

    def description(cls):
      # return "{i}\"Blowville Scale: 1 out of 5 peppers.\"{/}"
      return "{i}\"Blowville Scale:\n1 out of 5 peppers.\"{/}"

    def actions(cls,actions):
      actions.append("?home_kitchen_sauce_taco_hell_interact")


label home_kitchen_sauce_taco_hell_interact:
  show misc sauce_taco_hell with Dissolve(.5)
  pause 0.125
  "{i}\"Don't put this on your weiner!\"{/}"
  "{i}\"Made by and for female hot sauce connoisseurs!\"{/}"
  window hide
  hide misc sauce_taco_hell with Dissolve(.5)
  $quest.maya_sauce["hot_sauces"].add("sauce_taco_hell")
  return
