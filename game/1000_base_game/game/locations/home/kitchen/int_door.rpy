init python:
  class Interactable_home_kitchen_door(Interactable):

    def title(cls):
      return "Door"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.lindsey_piano == "pick_up":
#       return "[lindsey] is one of the first girls to have ever crossed this threshold.\n\nMy sister doesn't count." ## Incest patch ##
        return "[lindsey] is one of the first girls to have ever crossed this threshold.\n\n[flora] doesn't count."
      elif quest.flora_cooking_chilli >= "return_phone":
        return "Adventure awaits!\n\nLet it."
      elif quest.flora_cooking_chilli >= "return_phone":
        return "The outside. The bane of my anxiety-ridden existence."
      elif quest.flora_cooking_chilli.started:
        return "The street outside is filled with cars. Some kind of meet-up. People with friends ruin traffic."
      elif quest.flora_jacklyn_introduction.started:
        return "A portal! But also a gate! But also a door! Yeah, it's a door."
      elif quest.back_to_school_special=="ready_to_leave":
        return "Why do we put weight into mundane things? It's just a door... and yet, the entire world awaits on the other side."
      elif quest.back_to_school_special.started:
        return "The door to the outside world. Let's just say we're not on good terms."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Outside","goto_school_entrance"])
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli >= "chilli_recipe_step_one" and quest.flora_cooking_chilli <= "try_again":
            actions.append(["go","Outside","?home_kitchen_door_interact_flora_cooking_chilli"])
            return
          elif quest.flora_cooking_chilli in ("chilli_done","flora_called","return_phone"):
            actions.append(["go","Outside","?home_kitchen_door_interact_flora_cooking_chilli_return_phone"])
            return
          elif quest.flora_cooking_chilli >= "follow_bathroom" and quest.flora_cooking_chilli.in_progress:
            actions.append(["go","Outside","?home_kitchen_door_interact_flora_cooking_chilli_get_milk"])
            return
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "sugar":
            actions.append(["go","Outside","?kitchen_door_interact_flora_bonsai_sugar"])
            return
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "datedoor":
            actions.append(["interact","Open Door","kitchen_door_jacklyn_statement_datedoor"])
            return
          elif quest.jacklyn_statement == "jackdate":
            actions.append(["go","Outside","?kitchen_door_jacklyn_statement_jackdate"])
            return
          elif quest.jacklyn_statement == "intruder":
            actions.append(["go","Outside","?kitchen_door_jacklyn_statement_intruder"])
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Outside","goto_school_entrance"])
        elif mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "pick_up":
            actions.append(["go","Outside","?quest_lindsey_piano_pick_up_door"])
            return
        elif mc["focus"] == "maya_quixote":
          if quest.maya_quixote in ("attic","board_game"):
            actions.append(["go","Outside","?quest_maya_quixote_attic_home_kitchen_door"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Outside","goto_school_entrance"])
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "ready":
            actions.append(["go","Outside","?quest_jacklyn_town_ready_door"])
            return
          elif quest.jacklyn_town == "marina":
            actions.append(["go","Outside","quest_jacklyn_town_marina"])
        elif mc["focus"] == "maya_sauce":
          if quest.maya_sauce == "sauces":
            actions.append(["go","Outside","?quest_maya_sauce_sauces_home_kitchen_door"])
            return
      else:
        if quest.flora_jacklyn_introduction == "bathroom":
          actions.append(["go","Outside","?home_kitchen_door_interact_flora_jacklyn_introduction_bathroom"])
          return
        elif quest.back_to_school_special=="ready_to_leave":
          actions.append(["go","Outside","home_kitchen_door_interact_back_to_school_special_leave"])
        elif quest.back_to_school_special=="talk_to_flora":
          actions.append(["go","Outside","?home_kitchen_door_interact_back_to_school_special_flora"])
          return
        elif quest.back_to_school_special.in_progress:
          actions.append(["go","Outside","?home_kitchen_door_interact_back_to_school_special"])
          return
      if game.hour in (19,20,21,22,23,0,1,2,3,4,5,6):
        actions.append(["go","Outside","?home_kitchen_door_after_hours"])
#     elif quest.lindsey_angel.started:
#       actions.append(["go","Outside","home_kitchen_door_go"])
      else:
        actions.append(["go","Outside","goto_school_entrance"])


label kitchen_door_interact_flora_bonsai_sugar:
  "As much as I wish we had a garden of sugar canes outside, the sugar is probably hiding inside."
  return

label home_kitchen_door_interact_back_to_school_special:
  "Deny, delay, desist. Also, [jo] wanted to talk first."
  return

label home_kitchen_door_interact_back_to_school_special_flora:
  "The bus isn't coming for another few minutes. No need to rush. Plus, [flora] is here."
  return

label home_kitchen_door_interact_back_to_school_special_leave:
  $quest.back_to_school_special.finish()
  show black with Dissolve(1)
  $set_dialog_mode("default_no_bg")
  "Whatever awaits me on the other side, I've already been through before."
  "With some luck, it won't be nearly as bad."
  jump goto_school_entrance

label home_kitchen_door_interact_flora_jacklyn_introduction_bathroom:
  "[flora] will tell [jo] that I put something in the milk. Need to clear it up with her before doing anything else."
  return

label home_kitchen_door_interact_flora_cooking_chilli:
  show flora cringe at appear_from_right
  flora cringe "You won't leave your room for weeks if you can get away with it, but the moment I need your help, you need to run outside?"
  mc "The vitamin D deficiency is starting to get to me..."
  flora annoyed "You'll have a life deficiency if you dare reach for that door handle."
  mc "Sunlight..."
  hide flora with Dissolve(.5)
  return

label home_kitchen_door_interact_flora_cooking_chilli_return_phone:
  "Going outside feels like a waste of a good house, you know?"
  return

label home_kitchen_door_interact_flora_cooking_chilli_get_milk:
  "I can't just leave [flora] hanging. Milk or bust."
  return

label home_kitchen_door_after_hours:
  "Everyone's asleep. Late-night expeditions are what landed me the curfew."
  "I need to work that off before further jeopardizing my freedom."
  return

label kitchen_door_jacklyn_statement_datedoor:
  if quest.jacklyn_statement["clothes"] == "pirate":
    show jacklyn neutral with Dissolve(.5)
    jacklyn neutral "Did I miss the costume party memo? I could've rocked my slutty Professor Oak outfit."
    mc "Arr! You did, but don't worry, you can dress up as the emperor from the Emperor's New Groove."
    $jacklyn.love+=1
    jacklyn laughing "Nice try, cherry pie."
    "Is she flirting with me? God, I hope so."
    $quest.jacklyn_statement.advance("jackdate")
  if quest.jacklyn_statement["clothes"] == "punk":
    show jacklyn smile with Dissolve(.5)
    jacklyn smile "Don't you look absolutely baby legs?"
    $jacklyn.lust+=1
    jacklyn smile "Did you dress up for me?"
    mc "I guess, yeah. Do you like it?"
    jacklyn excited_right "Yeah! I'd totally put my dick in your ass."
    mc "Huh?"
    jacklyn excited "I'm kidding!"
    jacklyn laughing "Unless you'd like that."
    mc "..."
    jacklyn neutral "Oh, quit your blushing. It's not even a real one."
    "Damn, she's quite the handful. How do you handle a girl like her?"
    "Maybe you don't. Maybe you just have to let her take the lead."
    "Always been shit at this, so maybe that's for the best."
    $quest.jacklyn_statement.advance("jackdate")
  if quest.jacklyn_statement["clothes"] == "stinky":
    show jacklyn cringe with Dissolve(.5)
    jacklyn cringe "Sweet limp-on, what is that smell?"
    mc "It's probably Whooshster™ Sauce."
    $jacklyn.lust-=1
    jacklyn cringe "Smells like sweat."
    "Crap. I should probably have worn something clean."
    $quest.jacklyn_statement.advance("jackdate")
  hide jacklyn with Dissolve(.5)
  return

label kitchen_door_jacklyn_statement_jackdate:
  "Pretty nervous about this, but bailing wouldn't be a great strategy, no matter how you look at it."
  return

label kitchen_door_jacklyn_statement_intruder:
  "I can't just leave [jacklyn] with a potential murderer in the house..."
  return

#label home_kitchen_door_go:
# menu(side="middle"):
#   "Where should I go?"
#   "Go to school":
#     jump goto_school_entrance
#   "Go to the marina":
#     jump goto_marina
#   "Nevermind":
#     return
