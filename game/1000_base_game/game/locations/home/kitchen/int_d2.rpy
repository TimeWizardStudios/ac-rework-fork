init python:
  class Interactable_home_kitchen_d2(Interactable):

    def title(cls):
      return "Drawer"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "check_with_flora":
            actions.append("flora_quest_flora_cooking_chilli_drawer_check_with_flora")
          elif quest.flora_cooking_chilli == "try_again":
            actions.append("home_kitchen_d2_interact_information")
          elif quest.flora_cooking_chilli == "get_milk":
            actions.append("flora_quest_flora_cooking_chilli_d2_distraction")
          elif quest.flora_cooking_chilli >= "chilli_done":
            actions.append("?flora_quest_flora_cooking_chilli_drawer_end")
            return
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_seven":
            actions.append("flora_quest_flora_cooking_chilli_d2")
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_six":
            actions.append("home_kitchen_d2_interact_information")
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_four":
            actions.append("flora_quest_flora_cooking_chilli_d2")
          elif quest.flora_cooking_chilli >= "chop_onion":
            actions.append("home_kitchen_d2_interact_information")
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_one":
            actions.append("flora_quest_flora_cooking_chilli_d2")
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "bake":
            actions.append("home_kitchen_d2_interact_flora_bonsai")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      actions.append("?home_kitchen_d2_interact_information")


label home_kitchen_d2_interact_flora_bonsai:
  if not quest.flora_bonsai['drawers']:
    jump home_kitchen_d2_interact_information
  else:
    if not quest.flora_bonsai['drawers-complete']:
      $quest.flora_bonsai['drawers-complete'] = 1
    if quest.flora_bonsai['drawers-complete'] == 7:
      "I already got enough items. I should give them to [flora]."
      return
    if quest.flora_bonsai['drawers'] in (2,3):
      $quest.flora_bonsai['drawers']+=1
    else:
      $quest.flora_bonsai['drawers'] = "Fail"
  "You picked up a cooking item. Is it the right one? No one knows."
  $quest.flora_bonsai['drawers-complete'] +=1
  return

label home_kitchen_d2_interact_information:
  "This drawer contains a cutting board, a grater, and a pan."
  "Just like my humor — grating, deadpan, and cutting edge."
  return

label flora_quest_flora_cooking_chilli_d2:
  $ quest.flora_cooking_chilli["drawer"] = "d2"
  jump flora_cooking_chilli_drawer_interact
  return

label flora_quest_flora_cooking_chilli_d2_distraction:
  "I can probably use something in here to make a distraction."
  menu(side="middle"):
    extend ""
    "Cutting Board":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a cutting board"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Grater":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a grater"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Pan":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a pan"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Nevermind":
        return
  return
