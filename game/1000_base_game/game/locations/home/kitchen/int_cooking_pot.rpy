init python:
  class Interactable_home_kitchen_cooking_pot(Interactable):

    def title(cls):
      return "Cooking Pot"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.maxine_dive.started:
        # return "Why are potholes called the way they are?"
        return "Why are potholes called\nthe way they are?"
      else:
        return "In the kitchen, wrist twistin' like a stir fry."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "chilli_recipe_step_three" and mc.owned_item("onion_slice"):#check inventory
            actions.append(["use_item","Add Ingredient","select_inventory_item","$quest_item_filter:flora_cooking_chilli,step_three|Add What?","home_kitchen_cooking_pot_interact_chilli_recipe_step_three"])
          elif quest.flora_cooking_chilli == "chilli_recipe_step_six":
            actions.append(["use_item","Add Ingredient","select_inventory_item","$quest_item_filter:flora_cooking_chilli,step_six|Add What?","home_kitchen_cooking_pot_interact_chilli_recipe_step_six"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.maya_spell == "container":
          actions.append(["take","Take","quest_maya_spell_container"])
        if quest.maxine_dive == "shield":
          actions.append(["take","Take","quest_maxine_dive_shield"])
      actions.append("?home_kitchen_cooking_pot_interact")


label home_kitchen_cooking_pot_interact:
  "Jesse! We have to cook..."
  return

label home_kitchen_cooking_pot_interact_chilli_recipe_step_three(item):
  if item.id == "onion_slice":
    $ mc.remove_item("onion_slice")
    show flora afraid with Dissolve(.5)
    flora afraid "What did you just do?"
    mc "I put the onion in the pot, like you said."
    flora sarcastic "Okay, just making sure."
    mc "Seriously?"
    flora annoyed "What? It's impossible to know what you're up to."
    flora annoyed "Anyway, I need a few more items."
    flora sarcastic "First, I need an apron. Then, I need a grater. And last, but not least, I need spices!"
    $ quest.flora_cooking_chilli["chilli_step"] = "four"
    $ quest.flora_cooking_chilli["drawers_interacted_with"] = 0
    $ quest.flora_cooking_chilli["current_items"] = "an apron, a grater, and spices"
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_four")
    hide flora with Dissolve(.5)
  else:
    "I'm not sure [flora] would appreciate me altering the recipe."
    $quest.flora_cooking_chilli.failed_item("step_three",item)
  return

label home_kitchen_cooking_pot_interact_chilli_recipe_step_six(item):
  if item.id == "spoonful_of_spice":
    $ mc.remove_item("spoonful_of_spice")
    show flora excited with Dissolve(.5)
    flora excited "Oh! That smells good!"
    flora excited "What spices did you put in?"
    mc "You didn't specify."
    flora neutral "Okay, it smells nice, so I'll let it slide this time."
    flora concerned "Anyway, the water wasn't enough to wash the spoon."
    flora concerned "Bring me a towel, then salt, and then a lighter."
    mc "You're going to wash it with salt? And then... burn it?"
    flora angry "Yes! Who knows what diseases live in your mouth."
    $ quest.flora_cooking_chilli["drawers_interacted_with"] = 0
    $ quest.flora_cooking_chilli["chilli_step"] = "seven"
    $ quest.flora_cooking_chilli["current_items"] = "a towel, salt, and a lighter"
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_seven")
    hide flora with Dissolve(.5)
  else:
    "I'm not sure [flora] would appreciate me altering the recipe."
    $quest.flora_cooking_chilli.failed_item("step_six",item)
  return
