init python:
  class Interactable_home_kitchen_kettle_stove(Interactable):

    def title(cls):
      return "Kettle"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.jo_potted == "boil":
        return "Now, where did I put my normie tears mug?"
      else:
        return "Boiling the water in a kettle is so bronze age.\n\nWhere's the pressure cooker when you need one?"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.jo_potted == "tea" and home_kitchen["kettle"] == "stove":
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,kettle_stove|Use What?","quest_jo_pot_kettle_stove"])
        elif quest.jo_potted == "boil" and mc.owned_item("lamp"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,kettle_stove_boil|Use What?","quest_jo_pot_kettle_stove_boil"])
      actions.append("?home_kitchen_kettle_stove_interact")


label home_kitchen_kettle_stove_interact:
  if quest.jo_potted == "boil":
    "Sleek unibody design. All-metal construction. Hydroponic technology. Rocket-inspired cylindrical shape. 0 to 100 in less than 20 minutes."
  else:
    "I once asked a girl out of a cup of tea. She said no."
    "Look, there she is again..."
    mc "Hi, [jo]."
    show jo smile with Dissolve (.5)
    jo smile "Hi there, sweetie."
    hide jo smile with Dissolve (.5)
  return
