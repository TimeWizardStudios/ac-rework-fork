init python:
  class Interactable_home_kitchen_d1(Interactable):

    def title(cls):
      return "Drawer"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "check_with_flora":
            actions.append("flora_quest_flora_cooking_chilli_drawer_check_with_flora")
          elif quest.flora_cooking_chilli == "try_again":
            actions.append("home_kitchen_d1_interact_information")
          elif quest.flora_cooking_chilli == "get_milk":
            actions.append("flora_quest_flora_cooking_chilli_d1_distraction")
          elif quest.flora_cooking_chilli >= "chilli_done":
            actions.append("?flora_quest_flora_cooking_chilli_drawer_end")
            return
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_seven":
            actions.append("flora_quest_flora_cooking_chilli_d1")
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_six":
            actions.append("home_kitchen_d1_interact_information")
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_four":
            actions.append("flora_quest_flora_cooking_chilli_d1")
          elif quest.flora_cooking_chilli >= "chop_onion":
            actions.append("home_kitchen_d1_interact_information")
          elif quest.flora_cooking_chilli >= "chilli_recipe_step_one":
            actions.append("flora_quest_flora_cooking_chilli_d1")
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "bake":
            actions.append("home_kitchen_d1_interact_flora_bonsai")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if mc.owned_item("letter_from_maxine"):
          actions.append(["use_item", "Use", "select_inventory_item","$quest_item_filter:isabelle_stolen,kitchen_d1|Use What?","isabelle_stolen_burn_notice"])
      actions.append("?home_kitchen_d1_interact_information")


label home_kitchen_d1_interact_flora_bonsai:
  if not quest.flora_bonsai['drawers']:
    jump home_kitchen_d1_interact_information
  else:
    if not quest.flora_bonsai['drawers-complete']:
      $quest.flora_bonsai['drawers-complete'] = 1
    if quest.flora_bonsai['drawers-complete'] == 7:
      "I already got enough items. I should give them to [flora]."
      return
    if quest.flora_bonsai['drawers'] == 1:
      $quest.flora_bonsai['drawers']+=1
    else:
      $quest.flora_bonsai['drawers'] = "Fail"
  "You picked up a cooking item. Is it the right one? No one knows."
  $quest.flora_bonsai['drawers-complete'] +=1
  return

label isabelle_stolen_burn_notice(item):
  if item.id == "letter_from_maxine":
    $mc.remove_item("letter_from_maxine")
    $home_kitchen["sink_fire"] = True
    "There we go, the letter has been disposed of."
    "Hopefully, [maxine] will take note of my meticulous ways."
    $achievement.burned_notice.unlock()
    $home_kitchen["sink_fire"] = False
  else:
    "If I leave my [item.title_lower] here, it might cause mass confusion."
    $quest.isabelle_stolen.failed_item("kitchen_d1", item)
  return

label home_kitchen_d1_interact_information:
  "This drawer contains knives, spoons, forks, and a lighter."
  "Murder, dinner, and arson... but perhaps not in that order."
  return

label flora_quest_flora_cooking_chilli_d1:
  $ quest.flora_cooking_chilli["drawer"] = "d1"
  jump flora_cooking_chilli_drawer_interact
  return

label flora_quest_flora_cooking_chilli_d1_distraction:
  "I can probably use something in here to make a distraction."
  menu(side="middle"):
    extend ""
    "Knife":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a knife"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Spoon":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a spoon"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Fork":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a fork"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Lighter":
      $ quest.flora_cooking_chilli["distraction_current_item"] = "a lighter"
      $ quest.flora_cooking_chilli["distraction_count"] +=1
      jump flora_quest_flora_cooking_chilli_distraction_add_item
    "Nevermind":
        return
  return
