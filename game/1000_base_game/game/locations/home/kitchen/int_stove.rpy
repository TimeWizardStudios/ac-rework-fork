init python:
  class Interactable_home_kitchen_stove(Interactable):

    def title(cls):
      return "Stove"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.jo_potted.finished:
        return "Boiling water in a microwave is pretty heretical, but after the other last incident that's no longer an option.\n\nRest in peace, micro-bae, you sexy provider of hot midnight snacks."
      else:
        return "Stovetop-regularis. Perfect for burning stuff. Also perfect for BURNING stuff."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if mc.owned_item("kettle"):
          actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:jo_potted,stove|Use What?","quest_jo_pot_stove"])
        if quest.jacklyn_statement == "ingredients":
          actions.append("home_kitchen_stove_jacklyn_statement_ingredients")
        if quest.jo_potted == "boil":
          actions.append("?home_kitchen_stove_interact_boil")
      actions.append("?home_kitchen_stove_interact")


label home_kitchen_stove_interact:
  "Right. After the last incident, [jo] put safety locks on the stove."
  return

label home_kitchen_stove_interact_boil:
  "After the last incident, I no longer have stove privileges."
  "Only [jo] and [flora] knows the twelve-digit code to the safety lock."
  "I need to find another way to boil these leaves."
  return

label quest_jo_pot_stove(item):
  #show kettle_on_stove with dissolve (.5)
  if item == "kettle":
      "Two minutes to midnight and the world is on fire. It's high time to add water and leaves, then sit back and enjoy a cup of tea."
      $home_kitchen["kettle"] = "stove"
      $mc.remove_item("kettle")
  else:
      "I'm all for experimenting with cooking, but boiling water on my [item.title_lower] is too Frankenstein even for me."
      $quest.jo_potted.failed_item("stove", item)
  return

label home_kitchen_stove_jacklyn_statement_ingredients:
  if mc.owned_item("garlic") and mc.owned_item("whoo_sauce") and mc.owned_item("croutons"):
    "Okay, time to toss that caesar salad..."
    show black with Dissolve(.5)
    $set_dialog_mode("default_no_bg")
    $mc.remove_item("garlic")
    $mc.remove_item("croutons")
    if mc.owned_item("whoo_sauce"):
      "Best to keep the Whooshster™ Sauce. Never know when it might come in handy."
    "..."
    hide black with Dissolve(.5)
    $set_dialog_mode("")
    "Okay, the salad is done."
    "Now to wait for [jacklyn]."
    while game.hour <20:
      $game.advance()
    $quest.jacklyn_statement["day"] = game.day
    $quest.jacklyn_statement["salad_made"] = True
    $quest.jacklyn_statement["all_ingredients"] = True
    $quest.jacklyn_statement.advance("dinner")
    jump goto_home_bedroom
  elif mc.owned_item(("garlic","whoo_sauce", "croutons")):
    jump home_kitchen_stove_jacklyn_statement_ingredients_missing
    #if mc.owned_item("whoo_sauce"):
    #  jump home_kitchen_stove_jacklyn_statement_ingredients_missing
    #else:
    #  "Where's that goddamn Whooshster™ Sauce?"
    #  jump home_kitchen_stove_jacklyn_statement_ingredients_missing
  else:
    "My first date ever and I refuse to do even the most basic of preparations?"
    "Suprising? No."
    "Disappointing? Yes."
    "I know I can do better."
    "How do you even make a salad without garlic?"
    "That's like a pizza without cheese."
    "The school kitchen should have a few cloves to spare."
  return

label home_kitchen_stove_jacklyn_statement_ingredients_missing:
  $missingstring = ""
  if not mc.owned_item("whoo_sauce"):
    $missingstring = missingstring + " Whooshster™ Sauce..."
  if not mc.owned_item("croutons"):
    $missingstring = missingstring + " croutons..."
  if not mc.owned_item("garlic"):
    $missingstring = missingstring + " garlic..."
  "Still missing the[missingstring]"
  "But let's be honest, you probably don't need every ingredient to make a good salad."
  menu(side="middle"):
    extend ""
    "Proceed without the missing ingredients":
      "Honestly, following recipes is for sheep and people who are afraid of trying new flavors..."
      "It'll be fine."
      $set_dialog_mode("default_no_bg")
      show black with Dissolve(.5)
      $mc.remove_item("garlic")
      $mc.remove_item("croutons")
      if mc.owned_item("whoo_sauce"):
        "Best to keep the Whooshster™ Sauce. Never know when it might come in handy."
      "..."
      hide black with Dissolve(.5)
      $set_dialog_mode("")
      "Okay, the salad is done."
      "Now to wait for [jacklyn]."
      while game.hour <20:
        $game.advance()
      $quest.jacklyn_statement["day"] = game.day
      $quest.jacklyn_statement["salad_made"] = True
      $quest.jacklyn_statement.advance("dinner")
      jump goto_home_bedroom
    "Do it right":
      "Mmm, yeah... probably best to follow the recipe."
  return
