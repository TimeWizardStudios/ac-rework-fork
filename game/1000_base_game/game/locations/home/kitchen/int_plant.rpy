init python:
  class Interactable_home_kitchen_plant_move(Interactable):

    def title(cls):
      return "Plant"

    def description(cls):
      if quest.jo_day == "supplies":
        return "[jo] takes better care of this plant than she does us..."

    def actions(cls,actions):
      if quest.jacklyn_statement == "ingredients":
        actions.append(["interact","Move","?home_kitchen_plant_interact"])
      elif quest.jo_day == "supplies":
        actions.append("?quest_jo_day_supplies_plant_one")


label home_kitchen_plant_interact:
  $home_kitchen["plant_right"] = True
  return
