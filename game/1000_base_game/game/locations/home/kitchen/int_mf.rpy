init python:
  class Interactable_home_kitchen_mf(Interactable):

    def title(cls):
      return "Mini Fridge"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif quest.jo_day == "supplies" and not quest.jo_day["jo_secret_wine_taken"]:
        return "If I pull the fridge out a little more, I can make out the glint of something hiding all the way in the back..."
      elif quest.maxine_dive == "milk" and quest.maxine_dive["kitchen_milk"]:
        # return "I'm suddenly very hungry for tacos..."
        return "I'm suddenly very hungry\nfor tacos..."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "get_milk":
            actions.append("?home_kitchen_mf_interact_flora_cooking_chilli_get_milk")
            return
          elif quest.flora_cooking_chilli == "distracted":
            actions.append("home_kitchen_mf_interact_flora_cooking_chilli_distracted")
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "bake":
            actions.append("home_kitchen_mf_interact_flora_bonsai")
          else:
            actions.append("?home_kitchen_mf_interact_flora_bonsai_check")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        if quest.jo_day == "supplies" and not quest.jo_day["jo_secret_wine_taken"]:
          actions.append(["take","Take","quest_jo_day_supplies_mini_fridge_take"])
          actions.append("?quest_jo_day_supplies_mini_fridge_interact")
        if quest.maxine_dive == "milk" and quest.maxine_dive["kitchen_milk"]:
          actions.append(["take","Take","quest_maxine_dive_milk_kitchen_milk"])
        if quest.jacklyn_statement == "fridge":
          actions.append("?home_kitchen_mf_jacklyn_statement_fridge")
        if quest.isabelle_stars == "homemade":
          actions.append("quest_isabelle_stars_homemade")
      actions.append("?home_kitchen_mf_interact")


label home_kitchen_mf_interact_flora_bonsai:
  if not quest.flora_bonsai['drawers']:
    jump home_kitchen_mf_interact_flora_bonsai_check
  else:
    if not quest.flora_bonsai['drawers-complete']:
      $quest.flora_bonsai['drawers-complete'] = 1
    if quest.flora_bonsai['drawers-complete'] == 7:
      "I already got enough items. I should give them to [flora]."
      return
    if quest.flora_bonsai['drawers'] == 5:
      $quest.flora_bonsai['drawers']+=1
    else:
      $quest.flora_bonsai['drawers'] = "Fail"
  "You picked up a cooking item. Is it the right one? No one knows."
  $quest.flora_bonsai['drawers-complete'] +=1
  return

label home_kitchen_mf_interact_flora_bonsai_check:
  "The mini fridge contains milk, eggs, and a bottle of pepelepsi."
  return

label home_kitchen_mf_interact_flora_cooking_chilli_distracted:
  "There it is. The perfect crime."
  "The mini fridge contains milk, eggs, and a bottle of Pepelepsi."
  "What should I grab?"
  menu(side="middle"):
    extend ""
    "{image=items bottle milk}|Milk":
      $mc.add_item("milk")
      "Not sure who put milk in a plastic bottle. That seems illegal, somehow."
      "Oh, well."
      if mc.owned_item("empty_bottle"):
        "Wouldn't want to carry around two plastic bottles, though. That's just burdensome."
        $mc.remove_item("empty_bottle")
    "{image=items bottle pepelepsi}|Pepelepsi":
      $mc.add_item("pepelepsi")
      "The Devil in a can. "
      "Yeah, that's their slogan."
      if mc.owned_item("empty_bottle"):
        "Wouldn't want to carry around two plastic bottles, though. That's just burdensome."
        $mc.remove_item("empty_bottle")
  "All right, hopefully this will do the trick."
  $ quest.flora_cooking_chilli.advance("got_liquid")
  return

label home_kitchen_mf_interact_flora_cooking_chilli_get_milk:
  "Although the urge to put my hand between [jo]'s legs is strong sometimes..."
  "I don't think this is the right time to strike."
  return

label home_kitchen_mf_interact:
  # "Four cupboards, six cabinets, nine drawers, and not a single full-sized fridge?"
  "Four cupboards, six cabinets, nine drawers, and not a single full-sized{space=-45}\nfridge?"
  return

label home_kitchen_mf_jacklyn_statement_fridge:
  "Okay, so we have eggs, anchovies, olive oil, lettuce, and peppers..."
  "With garlic, croutons, and Whooshster™ Sauce, I could make a mean caesar salad."
  "The school kitchen probably has garlic, so that's a good place to start."
  $quest.jacklyn_statement.advance("ingredients")
  return
