init python:
  class Interactable_home_kitchen_cheque(Interactable):

    def title(cls):
      return "Cheque"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "A seemingly authentic cheque."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take","Take","home_kitchen_cheque_take"])
      actions.append("?home_kitchen_cheque_interact")


label home_kitchen_cheque_interact:
  "I better cash this in soon before the deal is ended by TITs..."
  return

label home_kitchen_cheque_take:
  if ("home_kitchen_cheque_interact",()) not in game.seen_actions:
    call home_kitchen_cheque_interact
    $game.seen_actions.add(("home_kitchen_cheque_interact",()))
  if quest.kate_wicked["cheques"] == "bouncy":
    $mc.money+=renpy.random.randint(1,100)
  elif quest.kate_wicked["cheques"] == "flat":
    $mc.money+=40
  elif quest.kate_wicked["cheques"] == "medium":
    $mc.money+=game.day
  $home_kitchen["cheque_taken_today"] = True
  return
