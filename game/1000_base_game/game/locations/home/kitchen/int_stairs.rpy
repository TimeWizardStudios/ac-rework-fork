init python:
  class Interactable_home_kitchen_stairs(Interactable):

    def title(cls):
      return "Stairs"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return "Too late to go back now."
      elif quest.lindsey_piano == "pick_up":
        return "I hope [lindsey] didn't hear me running down the stairs to open the door. That would've been embarrassing."
      elif quest.flora_cooking_chilli >= "get_milk":
        return "Shoot for the stars! Err... stairs. Shoot for the stairs!"
      elif quest.flora_cooking_chilli >= "return_phone":
        return "[flora] always runs in the stairs but never gets in trouble for it. She's a lot lighter on her step, but it's still unfair."
      elif quest.flora_cooking_chilli.started:
        return "Life is like being stuck on the third step, unable to climb the entire staircase."
      elif quest.flora_jacklyn_introduction.started:
        return "One foot in front of the other. One step at a time. That's how you reach the second floor."
      elif quest.back_to_school_special.in_progress:
        return "These steps creak so badly. They make midnight-raids on the kitchen almost impossible."
      else:
        return "One small step for man. One giant leap to the safety of my bedroom."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append(["go","Upstairs","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli >= "chilli_recipe_step_one" and quest.flora_cooking_chilli <= "try_again":
            actions.append(["go","Upstairs","?home_kitchen_stairs_interact_flora_cooking_chilli"])
            return
        elif mc["focus"] == "jacklyn_statement":
          if quest.jacklyn_statement == "datedoor":
            actions.append(["go","Upstairs","?home_kitchen_stairs_jacklyn_statement_jackdate"])
            return
          elif quest.jacklyn_statement == "intruder":
            actions.append(["go","Upstairs","home_kitchen_stairs_jacklyn_statement_intruder"])
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append(["go","Upstairs","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "pick_up":
            actions.append(["go","Upstairs","?quest_lindsey_piano_pick_up_stairs"])
            return
        elif mc["focus"] == "nurse_venting":
          if quest.nurse_venting == "bottle":
            actions.append(["go","Upstairs","?quest_nurse_venting_kitchen_stairs_go"])
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append(["go","Upstairs","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town in ("ready","marina"):
            actions.append(["go","Upstairs","?quest_jacklyn_town_ready_stairs"])
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append(["go","Upstairs","school_entrance_bus_stop_interact_kate_blowjob_dream_school"])
        elif mc["focus"] == "maya_sauce":
          if quest.maya_sauce == "dinner":
            actions.append(["go","Upstairs","quest_maya_sauce_dinner"])
          elif quest.maya_sauce == "sauces":
            actions.append(["go","Upstairs","?quest_maya_sauce_sauces_home_kitchen_stairs"])
            return
      else:
        if quest.back_to_school_special.in_progress:
          actions.append(["go","Upstairs","?home_kitchen_stairs_interact_back_to_school_special"])
          return
      actions.append(["go","Upstairs","goto_home_hall"])


label home_kitchen_stairs_interact_back_to_school_special:
  "I probably forgot something upstairs, but no point living in the past."
  return

label home_kitchen_stairs_interact_flora_cooking_chilli:
  show flora angry at appear_from_right
  flora angry "That's not what I asked for. That's a staircase. Please, learn the difference!"
  hide flora with Dissolve(.5)
  return

label home_kitchen_stairs_jacklyn_statement_jackdate:
  "Okay, three calm breaths. No need to run."
  return

label home_kitchen_stairs_jacklyn_statement_intruder:
  if not quest.jacklyn_statement["intruder_line"]:
    "Okay, please don't be an axe murderer waiting for me up there..."
    $quest.jacklyn_statement["intruder_line"] = True
  jump goto_home_hall
