init python:
  class Character_shrub(BaseChar):

    default_name = "Goat"

style say_name_frame_shrub is say_name_frame_berb


init python:
  class Interactable_home_kitchen_goat(Interactable):

    def title(cls):
      return shrub.name

    def description(cls):
      if quest.mrsl_bot == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "The GOAT."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      else:
        if quest.maxine_dive == "package" and quest.maxine_dive["hard_part"]:
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:maxine_dive,shrub|Use What?","home_kitchen_goat_use_item"])
      actions.append("?home_kitchen_goat_interact")


label home_kitchen_goat_interact:
  mc "Hey, girl!"
  mc "You'll help me live deliciously, right?"
  shrub "Maaah!"
  if not quest.maxine_dive["hard_part"]:
    # "Hmm... maybe I should name her before touching her inappropriately?"
    "Hmm... maybe I should name her before touching her inappropriately?{space=-40}"
    if quest.flora_squid.actually_finished:
      "[flora] named her squid [cuthbert], which, I'm guessing, is like a cute nickname for Cthulhu..."
      $shrub.default_name = "Shrub"
      "So, how about... Shrub-Niggurath? [shrub], for short? Since goats eat shrubs?"
      "Yes, this is perfect!"
    $shrub.default_name = "Shrub"
    mc "Okay, girl, I'm naming you [shrub]."
    shrub "Maaah!"
    if not achievement.the_actual_goat.is_unlocked():
      window hide
      $achievement.the_actual_goat.unlock()
      pause 0.25
      window auto
    "Now for the hard part..."
    $quest.maxine_dive["hard_part"] = True
  return

label home_kitchen_goat_use_item(item):
  if item == "empty_bottle":
    $mc.remove_item("empty_bottle")
    pause 0.25
    "Heeeere, [shrub]! Let me just massage your nipples..."
    "..."
    "..."
    "..."
    "God, how awkward is this?"
    "I didn't even buy her dinner first."
    "But thanks for the milk, anyway!"
    window hide
    $mc.add_item("milk")
    $quest.maxine_dive.advance("trade")
  else:
    "Milking a goat with my [item.title_lower] would probably be considered animal abuse."
    $quest.maxine_dive.failed_item("shrub",item)
  return
