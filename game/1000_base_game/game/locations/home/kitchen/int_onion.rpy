init python:
  class Interactable_home_kitchen_onion(Interactable):

    def title(cls):
      return "Onion"

    def description(cls):
      return ""

    def actions(cls,actions):
      if quest.flora_cooking_chilli == 'chop_onion':
        actions.append("?kitchen_onion_interact_flora_cooking_chilli")
      else:
        actions.append("?kitchen_onion_interact")


label kitchen_onion_interact:
  "Crying because you're chopping an onion."
  "Or crying because you're all alone and no one will ever love you."
  "Your tear ducts don't know the difference."
  return

label kitchen_onion_interact_flora_cooking_chilli:
  #Start Onion Cutting Minigame (see below)
  show flora annoyed
  if 1==1:
    call start_kitchen_minigame
    flora annoyed "Seems like you managed to cut the onion and keep all your fingers intact..."
    mc "You don't have to look so upset about that."
    flora sarcastic "I'm just kidding!"
    flora sarcastic "Put the onion in the pot."
    $ mc.add_item("onion_slice")
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_three")
  if 0==1 :
    flora confused "Why is the onion cut in the shape of a crane?"
    mc "I was just making an onion origami. Pretty good, right?"
    flora eyeroll "Seriously? Start over!"
  hide flora with Dissolve(.5)
  return
