init python:
  class Interactable_home_kitchen_sauce_carolina(Interactable):

    def title(cls):
      return "{i}\"Carolina Jeepers Creeper\"{/}"

    def description(cls):
      # return "{i}\"Blowville Scale: 4 out of 5 peppers.\"{/}"
      return "{i}\"Blowville Scale:\n4 out of 5 peppers.\"{/}"

    def actions(cls,actions):
      actions.append("?home_kitchen_sauce_carolina_interact")


label home_kitchen_sauce_carolina_interact:
  show misc sauce_carolina with Dissolve(.5)
  pause 0.125
  "{i}\"Cookout with your annoying coworkers? Barbecue with the insufferable inlaws?\"{/}"
  # "{i}\"Put on your cowboy hat and let this serial killer do the job for you!\"{/}"
  "{i}\"Put on your cowboy hat and let this serial killer do the job\nfor you!\"{/}"
  window hide
  hide misc sauce_carolina with Dissolve(.5)
  $quest.maya_sauce["hot_sauces"].add("sauce_carolina")
  return
