init python:
  class Interactable_home_kitchen_dollar1(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_kitchen_dollar1_take"])


label home_kitchen_dollar1_take:
  $home_kitchen["dollar1_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_kitchen_dollar2(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_kitchen_dollar2_take"])


label home_kitchen_dollar2_take:
  $home_kitchen["dollar2_taken_today"] = True
  $mc.money+=20
  return


init python:
  class Interactable_home_kitchen_dollar3(Interactable):

    def title(cls):
      return "Money"

    def actions(cls,actions):
      actions.append(["take","Yoink","home_kitchen_dollar3_take"])


label home_kitchen_dollar3_take:
  $home_kitchen["dollar3_taken_today"] = True
  $mc.money+=20
  return
