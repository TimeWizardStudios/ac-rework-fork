### home_kitchen_sugarcube5 interactable ######################################

init python:
  class Interactable_home_kitchen_sugarcube8(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_kitchen_sugarcube8_take"])

label home_kitchen_sugarcube8_take:
  $home_kitchen["sugarcube8_taken"] = True
  $mc.add_item("sugar_cube")
  return

### home_kitchen_sugarcube4 interactable ######################################

init python:
  class Interactable_home_kitchen_sugarcube9(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_kitchen_sugarcube9_take"])

label home_kitchen_sugarcube9_take:
  $home_kitchen["sugarcube9_taken"] = True
  $mc.add_item("sugar_cube")
  return

### home_kitchen_sugarcube3 interactable ######################################

init python:
  class Interactable_home_kitchen_sugarcube10(Interactable):
    def title(cls):
      return "Sugar Cube"
    def description(cls):
      return ""
    def actions(cls,actions):
      actions.append(["take","Take","home_kitchen_sugarcube10_take"])

label home_kitchen_sugarcube10_take:
  $home_kitchen["sugarcube10_taken"] = True
  $mc.add_item("sugar_cube")
  return
