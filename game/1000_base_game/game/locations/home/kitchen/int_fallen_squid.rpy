init python:
  class Interactable_home_kitchen_fallen_squid(Interactable):
    def title(cls):
      return "Plushie"
    def description(cls):
      return  "Slithering over the glistening floor, sweeping it so evilly free of all litter."
    def actions(cls,actions):
      actions.append("?home_kitchen_fallen_squid_interact")

label home_kitchen_fallen_squid_interact:
  "What is that dreadful noise?"  
  "{i}\"Tekeli-li! Tekeli-li! Tekeli-li!\"{/}"
  "Dear lord, we're all doomed."
  return
