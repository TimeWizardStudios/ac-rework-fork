init python:
  class Interactable_home_kitchen_stone(Interactable):

    def title(cls):
      if (home_kitchen["stones"] == 1 and not home_kitchen["stone_interacted_today"]) or (home_kitchen["stones"] == 2 and home_kitchen["stone_interacted_today"]):
        return "Stone"
      else:
        return "Stones"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        if (home_kitchen["stones"] == 1 and not home_kitchen["stone_interacted_today"]) or (home_kitchen["stones"] == 2 and home_kitchen["stone_interacted_today"]):
          return "A very smooth stone."
        elif home_kitchen["stones"] == 2 or (home_kitchen["stones"] == 3 and home_kitchen["stone_interacted_today"]):
          return "A couple of very smooth stones."
        elif home_kitchen["stones"] == 3 or (home_kitchen["stones"] == 4 and home_kitchen["stone_interacted_today"]):
          return "A few very smooth stones."
        elif home_kitchen["stones"] == 4 or (home_kitchen["stones"] == 5 and home_kitchen["stone_interacted_today"]):
          return "Several very smooth stones."
        elif home_kitchen["stones"] == 5:
          return "Too many very smooth stones."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      if (home_kitchen["stones"] == 1 and not home_kitchen["stone_interacted_today"]) or (home_kitchen["stones"] == 2 and home_kitchen["stone_interacted_today"]):
        actions.append("?home_kitchen_stone_interact")
      elif home_kitchen["stones"] == 2 or (home_kitchen["stones"] == 3 and home_kitchen["stone_interacted_today"]):
        actions.append("?home_kitchen_stones_interact")
      elif home_kitchen["stones"] == 3 or (home_kitchen["stones"] == 4 and home_kitchen["stone_interacted_today"]):
        actions.append("?home_kitchen_stoness_interact")
      elif home_kitchen["stones"] == 4 or (home_kitchen["stones"] == 5 and home_kitchen["stone_interacted_today"]):
        actions.append("?home_kitchen_stonesss_interact")
      elif home_kitchen["stones"] == 5:
        actions.append("?home_kitchen_stonessss_interact")


label home_kitchen_stone_interact:
  "Where did this come from, and why is it so smooth?"
  if not home_kitchen["stone_interacted_today"]:
    $home_kitchen["stone_interacted_today"] = True
    $home_kitchen["stones"]+=1
  return

label home_kitchen_stones_interact:
  "Another stone? This one is even smoother than the last!"
  if not home_kitchen["stone_interacted_today"]:
    $home_kitchen["stone_interacted_today"] = True
    $home_kitchen["stones"]+=1
  return

label home_kitchen_stoness_interact:
  "What's happening?! They're multiplying!"
  if not home_kitchen["stone_interacted_today"]:
    $home_kitchen["stone_interacted_today"] = True
    $home_kitchen["stones"]+=1
  return

label home_kitchen_stonesss_interact:
  "What the hell? If [jo] notices these, I'm going to be in so much—"
  if jo.at("home_kitchen"):
    show jo smile with Dissolve(.5)
  else:
    show jo smile at appear_from_right
  jo smile "Honey, I didn't know you had an interest in geology!"
  mc "Uh... I don't, actually..."
  jo neutral "You don't? Why are you collecting these stones, then?"
  mc "I accidentally entered [maxine]'s, err... ASS."
  jo concerned "What did you just say?"
  mc "ASS is like... her new thing. She got me into it."
  jo skeptical "..."
  jo skeptical "Well, as long as you use protection."
  if jo.at("home_kitchen"):
    hide jo with dissolve2
  else:
    show jo skeptical at disappear_to_right
  mc "Wait, that's not—"
  if not home_kitchen["stone_interacted_today"]:
    $home_kitchen["stone_interacted_today"] = True
    $home_kitchen["stones"]+=1
  return

label home_kitchen_stonessss_interact:
  if flora.at("home_kitchen"):
    show flora worried with Dissolve(.5)
  else:
    show flora worried at appear_from_right
  flora worried "You have a problem."
  mc "You have to be more specific than that."
  flora thinking "Where did you even get all these stones?"
  mc "Does the acronym ASS tell you anything?"
  flora laughing "Yes, that you're one!"
  flora laughing "Did you sand paper the stones or something?"
  mc "No, they're just naturally this smooth."
  mc "At least, that's what [maxine] tells me..."
  flora confused "So, where did they come from?"
  mc "I asked [maxine] to throw down the rope from her office..."
  mc "...so I could get back into the school after dark..."
  mc "...but I ended up joining the Association of Smooth Stones..."
  mc "...and now they're getting delivered here."
  flora annoyed "You're messing with me, aren't you?"
  mc "This time I'm not! I swear!"
  flora eyeroll "Right. I totally believe you."
  window hide
  if flora.at("home_kitchen"):
    hide flora with Dissolve(.5)
  else:
    show flora eyeroll at disappear_to_right
    pause 0.5
  window auto
  "Hmm... I should probably get rid of these stones, but..."
  "They're just so smooth. So precious."
  "After all, why not? Why shouldn't I keep them?"
  $achievement.lord_of_the_stones.unlock()
  return
