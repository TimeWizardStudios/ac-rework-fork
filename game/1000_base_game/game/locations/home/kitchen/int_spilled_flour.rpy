init python:
  class Interactable_home_kitchen_spilled_flour(Interactable):
    def title(cls):
      return "Spilled Flour"
    def description(cls):
      return "It's on a knead to know basis."
    def actions(cls,actions):
      actions.append("?home_kitchen_spilled_flour_interact")

label home_kitchen_spilled_flour_interact:
  "Say hello to my little friend."
  "And by little friend I mean my self worth."
  return
