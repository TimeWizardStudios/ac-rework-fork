init python:
  class Interactable_home_kitchen_sauce_bust_a_ghost(Interactable):

    def title(cls):
      return "{i}\"Bust a Ghost!\"{/}"

    def description(cls):
      # return "{i}\"Blowville Scale: 3 out of 5 peppers.\"{/}"
      return "{i}\"Blowville Scale:\n3 out of 5 peppers.\"{/}"

    def actions(cls,actions):
      actions.append("?home_kitchen_sauce_bust_a_ghost_interact")


label home_kitchen_sauce_bust_a_ghost_interact:
  show misc sauce_bust_a_ghost with Dissolve(.5)
  pause 0.125
  "{i}\"Who you gonna call?\"{/}"
  "{i}\"The fire department, probably!\"{/}"
  "{i}\"Hot sauce made from non-locally (possibly cartel) sourced ghost peppers.\"{/}"
  # "{i}\"Bust a ghost pepper, then bust a nut with the hottest hot sauce on the market!\"{/}"
  "{i}\"Bust a ghost pepper, then bust a nut with the hottest hot sauce\non the market!\"{/}"
  window hide
  hide misc sauce_bust_a_ghost with Dissolve(.5)
  $quest.maya_sauce["hot_sauces"].add("sauce_bust_a_ghost")
  return
