init python:
  class Interactable_kitchen_water_bottle(Interactable):

    def title(cls):
      return "Water Bottle"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "This water was purified by way of distillation, deionization, demonization, reverse cosmosis, UV rays, x-rays, blu-rays, manta rays, and exorcism."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take","Take","?home_kitchen_water_bottle_take"])
      actions.append("?kitchen_water_bottle_interact")


label kitchen_water_bottle_interact:
  "Tastes a bit like water. Odd."
  return

label home_kitchen_water_bottle_take:
  $home_kitchen["water_bottle_taken"] = True
  $mc.add_item("water_bottle")
  return
