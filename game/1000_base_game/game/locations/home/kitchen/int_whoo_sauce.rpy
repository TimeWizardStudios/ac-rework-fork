init python:
  class Interactable_home_kitchen_sauce_left(Interactable):

    def title(cls):
      return "Whooshster™ Sauce"

    def description(cls):
      if (quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "A rare sight. A Whooshster™ Sauce in the wild. I'm a lucky person to witness such grandeur."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take", "Take","?home_kitchen_sauce_left_take"])
      actions.append("?home_kitchen_sauce_left_interact")


label home_kitchen_sauce_left_interact:
  "At long last. The sauce of all sauces. The brilliant star in the night sky of my stomach."
  return

label home_kitchen_sauce_left_take:
  "There you are!"
  $mc.add_item("whoo_sauce")
  $home_kitchen["whoo_sauce_taken"] = True
  return

