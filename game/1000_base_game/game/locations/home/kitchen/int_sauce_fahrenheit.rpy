init python:
  class Interactable_home_kitchen_sauce_fahrenheit(Interactable):

    def title(cls):
      return "{i}\"Fahrenheit-551\"{/}"

    def description(cls):
      # return "{i}\"Blowville Scale: 5 out of 5 peppers.\"{/}"
      return "{i}\"Blowville Scale:\n5 out of 5 peppers.\"{/}"

    def actions(cls,actions):
      actions.append("?home_kitchen_sauce_fahrenheit_interact")


label home_kitchen_sauce_fahrenheit_interact:
  show misc sauce_fahrenheit with Dissolve(.5)
  pause 0.125
  "{i}\"Do you want to burn your tongue or your books?\"{/}"
  "{i}\"Worry not! Trinidad Scorpion meets kerosene!\"{/}"
  "{i}\"This two-in-one hot sauce is guaranteed to set your world on fire, either way!\"{/}"
  window hide
  hide misc sauce_fahrenheit with Dissolve(.5)
  $quest.maya_sauce["hot_sauces"].add("sauce_fahrenheit")
  return
