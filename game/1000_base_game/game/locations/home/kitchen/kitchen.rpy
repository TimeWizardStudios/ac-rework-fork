init python:
  class Location_home_kitchen(Location):

    default_title = "Kitchen"

    def __init__(self):
      super().__init__()

    def scene(self,scene):

      scene.append([(0,0),"home kitchen kitchen"])

      #Night
      if (home_kitchen["night"]
      or quest.lindsey_voluntary == "dream"
      or (quest.isabelle_stars == "stargaze" and game.hour == 20)
      or quest.isabelle_stars["finished_this_scene"]
      or quest.maya_sauce == "dinner"):
        scene.append([(1160,289),"home kitchen nightwindow"])
      elif quest.maya_quixote in ("attic","board_game"):
        scene.append([(1160,289),"home kitchen window_storm"])

      scene.append([(241,832),"home kitchen carpets"])
      scene.append([(1028,524),"home kitchen furniture_03"])
      scene.append([(1432,183),"home kitchen closet"])

      #Drawers
      scene.append([(1457,728),"home kitchen d3","home_kitchen_d3"])
      scene.append([(1597,684),"home kitchen d1","home_kitchen_d1"])
      scene.append([(1507,261),"home kitchen s1","home_kitchen_s1"])
      scene.append([(966,325),"home kitchen cupboard"])
      scene.append([(1009,327),"home kitchen c1","home_kitchen_c1"])

      #Door
      scene.append([(175,197),"home kitchen door",("home_kitchen_door",0 ,200)])

      #Stove
      scene.append([(497,227),"home kitchen stove","home_kitchen_stove"])

      #Chilli Pot
      if home_kitchen["cooking_pot"] and not home_kitchen["cooking_pot_taken"]:
        scene.append([(707,446),"home kitchen cooking_pot","home_kitchen_cooking_pot"])
        scene.append([(715,533),"home kitchen stove_hotfix",("home_kitchen_stove",-30,-306)])

      #Furniture
      scene.append([(805,93),"home kitchen lights"])
      scene.append([(362,227),"home kitchen furniture_01"])
      scene.append([(444,667),"home kitchen d2","home_kitchen_d2"])
      scene.append([(831,669),"home kitchen mf","home_kitchen_mf"])
      scene.append([(689,272),"home kitchen clock"])

      #Cheque OR Stone(s)
      if quest.kate_wicked["cheques"] and not home_kitchen["cheque_taken_today"]:
        scene.append([(270,771),"home kitchen cheque","home_kitchen_cheque"])
      elif (home_kitchen["stones"] == 1 and not home_kitchen["stone_interacted_today"]) or (home_kitchen["stones"] == 2 and home_kitchen["stone_interacted_today"]):
        scene.append([(305,775),"home kitchen stone","home_kitchen_stone"])
      elif home_kitchen["stones"] == 2 or (home_kitchen["stones"] == 3 and home_kitchen["stone_interacted_today"]):
        scene.append([(276,770),"home kitchen stones","home_kitchen_stone"])
      elif home_kitchen["stones"] == 3 or (home_kitchen["stones"] == 4 and home_kitchen["stone_interacted_today"]):
        scene.append([(276,760),"home kitchen stoness","home_kitchen_stone"])
      elif home_kitchen["stones"] == 4 or (home_kitchen["stones"] == 5 and home_kitchen["stone_interacted_today"]):
        scene.append([(255,749),"home kitchen stonesss","home_kitchen_stone"])
      elif home_kitchen["stones"] == 5:
        scene.append([(236,736),"home kitchen stonessss","home_kitchen_stone"])

      #Flora Chilli Quest Misc
      if quest.flora_cooking_chilli < "return_phone":
        scene.append([(591,301),"home kitchen squid","home_kitchen_squid"])
      if quest.flora_cooking_chilli.in_progress:
        if quest.flora_cooking_chilli["distraction_method"] == "red":
          scene.append([(0,0),"home kitchen smoke",("home_kitchen_smoke",0,300)])
        if quest.flora_cooking_chilli >= "return_phone":
          scene.append([(1294,873),"home kitchen flora_pants","home_kitchen_flora_pants"])
        if quest.flora_cooking_chilli["distraction_method"] == "blue":
          scene.append([(1221,314),"home kitchen burning_towel","home_kitchen_burning_towel"])
        if quest.flora_cooking_chilli >= "return_phone":
          scene.append([(437,789),"home kitchen tipped_squid","home_kitchen_fallen_squid"])
      if home_kitchen["sink_fire"]:
        scene.append([(1221,314),"home kitchen burning_towel","home_kitchen_burning_towel"])


      #Stove & Kettle
      if home_kitchen["kettle"] == "stove":
        scene.append([(745,496),"home kitchen kettle_stove","home_kitchen_kettle_stove"])
      elif home_kitchen["kettle"] == "lamp":
        scene.append([(719,430),"home kitchen kettle_lamp"])
        scene.append([(595,163),"home kitchen smokekettle"])

      #Jo Newspaper
      if ((not home_kitchen["exclusive"] or "jo" in home_kitchen["exclusive"])
      and jo.at("home_kitchen","reading")
      and not jo.talking):
        if game.season == 1:
          scene.append([(678,396),"home kitchen jo_newspaper","jo"])
        elif game.season == 2:
          scene.append([(678,396),"home kitchen jo_newspaper_autumn","jo"])

      #Flora Pain
      if ((not home_kitchen["exclusive"] or "flora" in home_kitchen["exclusive"])
      and flora.at("home_kitchen","pain")
      and not flora.talking):
        scene.append([(757,386),"home kitchen florapain","flora"])

      #Flora Standing/Cooking
      if ((not home_kitchen["exclusive"] or "flora" in home_kitchen["exclusive"])
      and flora.at("home_kitchen","cooking_hat")
      and not flora.talking):
        if game.season == 1 and quest.flora_bonsai.in_progress:
          scene.append([(757,386),"home kitchen florabandaidcook","flora"])
        elif game.season == 1:
          scene.append([(757,371),"home kitchen flora","flora"])
        elif game.season == 2:
          scene.append([(757,371),"home kitchen flora_autumn","flora"])

      #Maya
      if ((not home_kitchen["exclusive"] or "maya" in home_kitchen["exclusive"])
      and maya.at("home_kitchen","staring")
      and not maya.talking):
        scene.append([(1195,405),"home kitchen maya_autumn","maya"])

      scene.append([(575,593),"home kitchen counter"])
      scene.append([(857,476),"home kitchen various_back"])

      #Water Bottle
      if not home_kitchen["water_bottle_taken"]:
        scene.append([(819,520),"home kitchen water_bottle","kitchen_water_bottle"])

      scene.append([(1223,626),"home kitchen barstool_2"])
      scene.append([(1087,639),"home kitchen barstool_1"])

      #Kettle
      if not home_kitchen["kettle"]:
        scene.append([(691,521),"home kitchen kettle","home_kitchen_kettle_counter"])

      scene.append([(424,482),"home kitchen various_front"])

      #Crouton
      if home_kitchen["croutons_activated"] and not home_kitchen["croutons_taken"]:
          scene.append([(1005,569),"home kitchen crouton","home_kitchen_crouton"])

      #Onion
      if quest.flora_cooking_chilli == "chop_onion":
        scene.append([(693,574),"home kitchen onion","home_kitchen_onion"])

      #Sauce
      if not home_kitchen["whoo_sauce_taken"]:
        scene.append([(1087,545),"home kitchen sauce_left","home_kitchen_sauce_left"])

      #More sauces
      if quest.maya_sauce == "sauces":
        scene.append([(908,544),"home kitchen sauce_fahrenheit","home_kitchen_sauce_fahrenheit"])
        scene.append([(956,544),"home kitchen sauce_bust_a_ghost","home_kitchen_sauce_bust_a_ghost"])
        scene.append([(1008,544),"home kitchen sauce_taco_hell","home_kitchen_sauce_taco_hell"])
        scene.append([(933,545),"home kitchen sauce_wench","home_kitchen_sauce_wench"])
        scene.append([(986,544),"home kitchen sauce_carolina","home_kitchen_sauce_carolina"])

      #Plant
      if home_kitchen["plant_right"]:
        scene.append([(1259,528),"home kitchen plant_right"])
      else:
        if quest.jacklyn_statement == "ingredients" or quest.jo_day == "supplies":
          scene.append([(1058,517),"home kitchen plant","home_kitchen_plant_move"])
        else:
          scene.append([(1058,517),"home kitchen plant"])

      #Pizza Boxes
      if home_kitchen["pizza_boxes"] >= 1:
        scene.append([(945,608),"home kitchen pizza_box1",("home_kitchen_pizza_boxes",-5,-39)])
      if home_kitchen["pizza_boxes"] >= 2:
        scene.append([(940,594),"home kitchen pizza_box2",("home_kitchen_pizza_boxes",-5,-25)])
      if home_kitchen["pizza_boxes"] >= 3:
        scene.append([(951,582),"home kitchen pizza_box3",("home_kitchen_pizza_boxes",-5,-13)])
      if home_kitchen["pizza_boxes"] == 4:
        scene.append([(950,569),"home kitchen pizza_box4","home_kitchen_pizza_boxes"])

      #Stairs
      scene.append([(0,0),"home kitchen stairs",("home_kitchen_stairs",-200,350)])
      if quest.flora_squid >= "trail" and quest.flora_squid.in_progress:
        scene.append([(0,326),"home kitchen water_trail",("home_kitchen_water_trail",55,555)])

      #Spilled Flour
      if quest.flora_cooking_chilli["distraction_method"] == "purple":
        scene.append([(863,573),"home kitchen spilled_flour","home_kitchen_spilled_flour"])

      #Flora Sitting
      if ((not home_kitchen["exclusive"] or "flora" in home_kitchen["exclusive"])
      and flora.at("home_kitchen","eating")
      and not flora.talking):
        if quest.flora_bonsai.in_progress:
          if quest.flora_bonsai in ("chef","bake","check","sugar"):
            scene.append([(1034,446),"home kitchen florabandaidnight","flora"])
          else:
            if flora.equipped_item("flora_skirt"):
              scene.append([(1034,446),"home kitchen florabandaid_skirt","flora"])
            else:
              scene.append([(1034,446),"home kitchen florabandaid","flora"])
        else:
          if game.season == 1:
            scene.append([(1034,446),"home kitchen flora_sitting","flora"])
          elif game.season == 2:
            scene.append([(1034,446),"home kitchen flora_sitting_autumn","flora"])

      #Flora Referee
      if ((not home_kitchen["exclusive"] or "flora" in home_kitchen["exclusive"])
      and flora.at("home_kitchen","referee")
      and not flora.talking):
        scene.append([(1022,439),"home kitchen flora_sitting_referee","flora"])

      #Jo Coffee
      if ((not home_kitchen["exclusive"] or "jo" in home_kitchen["exclusive"])
      and jo.at("home_kitchen","standing")
      and not jo.talking):
        if game.season == 1:
          scene.append([(787,359),"home kitchen jo_coffee","jo"])
        elif game.season == 2:
          scene.append([(787,359),"home kitchen jo_coffee_autumn","jo"])

      #Jacklyn
      if ((not home_kitchen["exclusive"] or "jacklyn" in home_kitchen["exclusive"])
      and jacklyn.at("home_kitchen","standing")
      and not jacklyn.talking):
        scene.append([(387,373),"home kitchen jacklyn","jacklyn"])

      #Lindsey best girl
      if ((not home_kitchen["exclusive"] or "lindsey" in home_kitchen["exclusive"])
      and lindsey.at("home_kitchen","standing")
      and not lindsey.talking):
        scene.append([(1509,375),"home kitchen lindsey","lindsey"])

      #Package
      if not (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        if home_kitchen["package"]:
          scene.append([(394,516),"home kitchen package","home_kitchen_package_gigglypuff_seeds"])
        if home_kitchen["lines_magnet"] and not mc['package_due_today']:
          scene.append([(394,516),"home kitchen package","home_kitchen_package_magnet"])

        for item in ("ball_of_yarn","stick","mrsl_brooch","nymphoria_heartus","venus_vagenis","bag_of_weed","beaver_pelt","baseball_bat","sleeping_pills","cutie_harlot","pig_mask","hex_vex_and_texmex","hearshey_shesay","tob_le_bone","dick_wonky","milk"):
          if home_computer[item + "_ordered"] and not home_computer[item + "_ordered_today"] and not home_computer[item + "_received"]:
            scene.append([(394,516),"home kitchen package","home_kitchen_package_" + item])

      #Goat
      if home_computer["goat_ordered"] and not home_computer["goat_ordered_today"] and not quest.lindsey_voluntary == "dream":
        scene.append([(423,544),"home kitchen goat","home_kitchen_goat"])

      #Dollars
      if not (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        if home_kitchen["dollar1_spawned_today"] == True and not home_kitchen["dollar1_taken_today"]:
          scene.append([(389,758),"home kitchen dollar1","home_kitchen_dollar1"])
        if home_kitchen["dollar2_spawned_today"] == True and not home_kitchen["dollar2_taken_today"]:
          scene.append([(129,326),"home kitchen dollar2","home_kitchen_dollar2"])
        if home_kitchen["dollar3_spawned_today"] == True and not home_kitchen["dollar3_taken_today"]:
          scene.append([(1663,355),"home kitchen dollar3","home_kitchen_dollar3"])

      #Sugar Cubes
      if game.season == 1:
        if quest.flora_bonsai <= "sugar" and not (quest.kate_blowjob_dream == "school" or quest.mrsl_table == "morning"):
          if not home_kitchen["sugarcube8_taken"]:
            scene.append([(1271,614),"home kitchen sugarcube8","home_kitchen_sugarcube8"])
          if not home_kitchen["sugarcube9_taken"]:
            scene.append([(126,670),"home kitchen sugarcube9","home_kitchen_sugarcube9"])
          if not home_kitchen["sugarcube10_taken"]:
            scene.append([(1807,907),"home kitchen sugarcube10","home_kitchen_sugarcube10"])

      #Night
      if (home_kitchen["night"]
      or quest.maya_quixote in ("attic","board_game")):
        scene.append([(0,0),"#home kitchen nightoverlay"])


    def find_path(self,target_location):
      ## todo add jo/flora rooms in this condition
      if target_location in ("home_bedroom","home_bathroom","home_hall"):
        return "home_kitchen_stairs",dict(marker_offset=(300,150),marker_sector="right_top")
      ## otherwise path is thru door
      else:
        return "home_kitchen_door",dict(marker_offset=(100,150),marker_sector="left_top")

label goto_home_kitchen:
  if home_kitchen.first_visit_today:
    $home_kitchen["dollar1_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $home_kitchen["dollar2_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
    $home_kitchen["dollar3_spawned_today"] = (renpy.random.randint(1,2) if (quest.flora_handcuffs == "order" or quest.maya_witch == "black_market") else renpy.random.randint(1,4))
  call goto_with_black(home_kitchen)
  play music "home_theme" if_changed
  return


