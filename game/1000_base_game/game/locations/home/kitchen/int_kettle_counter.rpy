init python:
  class Interactable_home_kitchen_kettle_counter(Interactable):

    def title(cls):
      return "Kettle"

    def description(cls):
      if (quest.kate_blowjob_dream == "school"
      or quest.mrsl_table == "morning"
      or quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "Tea, the poor man's coffee.\n\nCoffee, the poor man's diet coke."

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "kate_blowjob_dream":
          if quest.kate_blowjob_dream == "school":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_table":
          if quest.mrsl_table == "morning":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take","Take","home_kitchen_kettle_counter_take"])
      actions.append("?home_kitchen_kettle_counter_interact")


label home_kitchen_kettle_counter_take:
  $home_kitchen["kettle"] = "Taken"
  $mc.add_item("kettle")
  return

label home_kitchen_kettle_counter_interact:
  "The pot called the kettle black and got called out on the blatant racism. Never made it back to the stovetop after that."
  return
