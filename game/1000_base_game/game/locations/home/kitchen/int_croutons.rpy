init python:
  class Interactable_home_kitchen_crouton(Interactable):

    def title(cls):
      return "Croutons"

    def description(cls):
      if (quest.lindsey_voluntary == "dream"
      or quest.mrsl_bot == "dream"):
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      else:
        return "A sack of diced bread. Perfect to dip in milk!"

    def actions(cls,actions):
      if mc["focus"]:
        if mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "dream":
            actions.append("kate_blowjob_dream_random_interact")
            return
      actions.append(["take", "Take","?home_kitchen_crouton_take"])
      actions.append("?home_kitchen_crouton_interact")


label home_kitchen_crouton_interact:
  "Fifteen percent oil..."
  "Five percent bread..."
  "Fifty percent wood chips..."
  "And a hundred percent reason to remember the brand."
  #"Atleast the marketing is honest..."
  return

label home_kitchen_crouton_take:
  "Perfect. The best bread in the west."
  "Unless you count the baguette. No one messes with the baguette."
  $home_kitchen["croutons_taken"] = True
  $mc.add_item("croutons")
  return

