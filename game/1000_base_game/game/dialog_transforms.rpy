﻿define qdis=Dissolve(0.25)
define dis=Dissolve(0.5)
define dissolve=Dissolve(0.5)
define dissolve2={"master": Dissolve(0.5)}
define fadehold = Fade(0.5, 1.0, 0.5)
define open_eyes=ImageDissolve(im.Crop("images/blinking_eyes.webp",(0,270,1920,1080)), 0.75, 64)
define close_eyes=ImageDissolve(im.Crop("images/blinking_eyes.webp",(0,270,1920,1080)), 0.75, 64, reverse=True)
define vpunch2={"master": vpunch}
define hpunch2={"master": hpunch}


define config.say_attribute_transition=dis


define transform_positions={
  "left": 0.0,
  "midleft": 0.33,
  "mid_left": 0.33,
  "mid-left": 0.33,
  "mid left": 0.33,
  "center": 0.5,
  "mid": 0.5,
  "midright": 0.66,
  "mid_right": 0.66,
  "mid-right": 0.66,
  "mid right": 0.66,
  "right": 1.0,
  }

define transform_xzooms={
  True: (-1.0,-1.0),
  False: (1.0,1.0),
  "left": (1.0,1.0),
  "right": (-1.0,-1.0),
  "leftright": (1.0,-1.0),
  "left_right": (1.0,-1.0),
  "left-right": (1.0,-1.0),
  "left right": (1.0,-1.0),
  "rightleft": (-1.0,1.0),
  "right_left": (-1.0,1.0),
  "right-left": (-1.0,1.0),
  "right left": (-1.0,1.0),
  }

transform flipped():
    xzoom -1.0
    xalign .5
    yalign 1.0

transform normal():
    xzoom 1.0
    xalign .5
    yalign 1.0

transform appear_from_left(to=0.5,delay=1,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  xpos 0.0 xanchor 1.0
  yalign 1.0
  easein delay xalign transform_positions.get(to,to)
  xzoom transform_xzooms.get(flip,flip)[1]

transform appear_from_right(to=0.5,delay=1,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  xpos 1.0 xanchor 0.0
  yalign 1.0
  easein delay xalign transform_positions.get(to,to)
  xzoom transform_xzooms.get(flip,flip)[1]

transform appear_from_midright(to=0.5,delay=1,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  xpos .75 xanchor 0.75
  yalign 1.0
  easein delay xalign transform_positions.get(to,to)
  xzoom transform_xzooms.get(flip,flip)[1]

transform disappear_to_left(delay=1,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  easeout delay xpos 0.0 xanchor 1.0

transform disappear_to_right(delay=1,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  easeout delay xpos 1.0 xanchor 0.0

transform move_to(to=0.5,delay=0.5,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  easein delay xalign transform_positions.get(to,to)
  xzoom transform_xzooms.get(flip,flip)[1]


transform beaver(delay=1,flip=False):
  xzoom transform_xzooms.get(flip,flip)[0]
  yalign .75
  easeout delay xpos 1.0 xanchor 0.0

transform bounce:
  pause .15
  yoffset 0
  easein .175 yoffset -10
  easeout .175 yoffset 0
  easein .175 yoffset -4
  easeout .175 yoffset 0
  yoffset 0
  repeat 3

transform toss_items(to=0.8,delay=1,flip=False):
  xalign 0.66
  yalign 0.35
  xzoom 1.2
  yzoom 1.2
  parallel:
    easein 1 xzoom 0.8
  parallel:
    easein 1 yzoom 0.8
  parallel:
    easein 1 xalign transform_positions.get(to,to)
  parallel:
    ease 1 yalign transform_positions.get(0.95,0.95)
  parallel:
    linear 1 rotate 400

transform toss_items_at_wall(to=0.8):
  xalign 0.66
  yalign 0.35
  xzoom 1.2
  yzoom 1.2
  parallel:
    easein 1 xzoom 0.8
  parallel:
    easein 1 yzoom 0.8
  parallel:
    #xpos xfrom xanchor 0.0
    easein 1 xalign transform_positions.get(to,to)
    easeout 1 yalign transform_positions.get(0.95,0.95)
  parallel:
    linear 1 rotate 400

transform toss_items_at_floor_roll(to=0.8):
  xalign 0.66
  yalign 0.35
  xzoom 1.2
  yzoom 1.2
  parallel:
    easein 1 xzoom 0.8
  parallel:
    easein 1 yzoom 0.8
  parallel:
    easeout 1 yalign transform_positions.get(0.95,0.95)
    easeout 1 xalign transform_positions.get(to,to)
  parallel:
    linear 2 rotate 1200

transform toss_items_at_floor_bounce(to=0.8):
  xalign 0.66
  yalign 0.35
  xzoom 1.2
  yzoom 1.2
  parallel:
    easein 1 xzoom 0.8
  parallel:
    easein 1 yzoom 0.8
  parallel:
    easeout_bounce 1 yalign transform_positions.get(0.95,0.95)
  parallel:
    easeout_bounce 1 xalign transform_positions.get(to,to)
  parallel:
    linear 2 rotate 800

transform gym_basketball_hit:
  xalign 0.5
  yalign 0.8
  xzoom 1.2
  yzoom 1.2
  parallel:
    easein 1.5 xzoom 0.4
  parallel:
    easein 1.5 yzoom 0.4
  parallel:
    easein_back 1.5 yalign transform_positions.get(0.275,0.275)
  parallel:
    easein 1.5 xalign transform_positions.get(0.95,0.95)
  parallel:
    linear 1.5 rotate 400

transform gym_basketball_miss:
  xalign 0.5
  yalign 0.8
  xzoom 1.2
  yzoom 1.2
  parallel:
    easein 1.5 xzoom 0.2
  parallel:
    easein 1.5 yzoom 0.2
  parallel:
    easein_bounce 1.5 yalign transform_positions.get(0.595,0.595)
  parallel:
    easein_circ 1.5 xalign transform_positions.get(0.867,0.867)
  parallel:
    linear 1.5 rotate 400
