label phone_app(phone_app,*phone_app_args):
  $game.ui.current_phone_app=[phone_app]+list(phone_app_args)
  return

label phone(phone_app="selector",*phone_app_args):
  $game.ui.current_phone_app=[phone_app]+list(phone_app_args)
  while game.ui.current_phone_app:
    $game.ui.hud_active=False
    call screen phone(*game.ui.current_phone_app)
    if isinstance(_return,basestring):
      call expression _return from _call_expression_13
    elif isinstance(_return,(list,tuple)):
      call expression _return[0] pass (*_return[1]) from _call_expression_14
    else:
      return _return
  return

label no_phone_call_label:
  "No answer..."
  return
