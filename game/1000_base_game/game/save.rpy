init python:
  def add_save_json_info(json):
    try:
      save_info="Day {} - {}".format(game.day,game.location)
      json["save_info"]=save_info
      json["game_version"]=current_game_version
      json["game_save_id"]=game_save_id
    except:
      pass

  config.save_json_callbacks.append(add_save_json_info)

label after_load:
  ## do universal upgrade things
  $process_event("after_load")
  ## upgrade version by calling all upgrade handlers until game_version is current_game_version
  call upgrade_game_version
  ## change xray mode from "full" to "on" should the player load a save file from a patreon build to a public one
  if len(xray_fn_suffix) == 2 and game.xray_mode:
    $game.xray_mode = 1
  return

label upgrade_game_version:
  while game_version!=current_game_version:
    #$print("upgrade_game_version_"+"_".join([str(v) for v in game_version]))
    $updated_versions=process_event("upgrade_game_version_"+"_".join([str(v) for v in game_version]),all_rv=True)
    $updated_versions=[list(v) for v in set([tuple(v) for v in updated_versions])]
    #$print(updated_versions)
    if len(updated_versions)==0:
      $raise Exception("No version upgrade path")
    elif len(updated_versions)>1:
      $raise Exception("More than one version upgrade path")
    $game_version=updated_versions[0]
  return

init python:
  ## minimal version upgraders, used to define version upgrade path
  ## you should add these for every new version, building upgrade path from 1,1,0 to current_game_version
  class Event_upgrade_version_1_2_to_1_3_base_game(GameEvent):
    def on_upgrade_game_version_1_2(event):
      return [1,3]

  class Event_upgrade_version_1_3_to_1_4_base_game(GameEvent):
    def on_upgrade_game_version_1_3(event):
      ## we simply tell upgrade system this is next version
      ## if there is no other upgrade handlers for 1 then we assume upgrade was done by other means
      return [1,4]

  class Event_upgrade_version_1_4_to_1_5_base_game(GameEvent):
    def on_upgrade_game_version_1_4(event):
      return [1,5]

  class Event_upgrade_version_1_5_to_1_6_base_game(GameEvent):
    def on_upgrade_game_version_1_5(event):
      return [1,6]

  class Event_upgrade_version_1_6_to_1_7_base_game(GameEvent):
    def on_upgrade_game_version_1_6(event):
      if quest.isabelle_haggis["open_window"]:
        school_homeroom["open_window"] = True
      return [1,7]

  class Event_upgrade_version_1_7_to_1_8_base_game(GameEvent):
    def on_upgrade_game_version_1_7(event):
      if school_homeroom["rope"]:
        quest.clubroom_access.start()
        quest.clubroom_access.finish()
      return [1,8]

  class Event_upgrade_version_1_8_to_1_9_base_game(GameEvent):
    def on_upgrade_game_version_1_8(event):
      return [1,9]

  class Event_upgrade_version_1_9_to_1_10_base_game(GameEvent):
    def on_upgrade_game_version_1_9(event):
      if quest.mrsl_HOT.finished and not home_bedroom["vhs"]:
        quest.mrsl_HOT.started = False
        quest.mrsl_HOT.finished = False
        quest.mrsl_HOT.start("vhs")
      jacklyn.outfit_slots = ["hat","pin","shirt","bra","pants","panties"]
      if not jacklyn.owned_item("jacklyn_squidpin"):
        jacklyn.add_item("jacklyn_squidpin")
      if quest.maxine_wine >= "search":
        jacklyn.equip("jacklyn_squidpin")
      maxine.outfit_slots = ["hat","necklace","shirt","bra","pants","panties"]
      if not maxine.owned_item("maxine_necklace"):
        maxine.add_item("maxine_necklace")
      maxine.equip("maxine_hat")
      maxine.equip("maxine_necklace")
      return [1,10]

  class Event_upgrade_version_1_10_to_1_11_base_game(GameEvent):
    def on_upgrade_game_version_1_10(event):
      if not lindsey.owned_item("lindsey_towel"):
        lindsey.add_item("lindsey_towel")
      return [1,11]

  class Event_upgrade_version_1_11_to_1_12_base_game(GameEvent):
    def on_upgrade_game_version_1_11(event):
      global automatically_update_state
      automatically_update_state = True
      kate.outfit_slots = ["hat","necklace","shirt","bra","pants","panties"]
      if not kate.owned_item("kate_necklace"):
        kate.add_item("kate_necklace")
      kate.equip("kate_necklace")
      if not kate.owned_item("kate_cheerleader_top"):
        kate.add_item("kate_cheerleader_top")
      if not kate.owned_item("kate_cheerleader_bra"):
        kate.add_item("kate_cheerleader_bra")
      if not kate.owned_item("kate_cheerleader_skirt"):
        kate.add_item("kate_cheerleader_skirt")
      if not kate.owned_item("kate_cheerleader_panties"):
        kate.add_item("kate_cheerleader_panties")
      return [1,12]

  class Event_upgrade_version_1_12_to_1_13_base_game(GameEvent):
    def on_upgrade_game_version_1_12(event):
      return [1,13]

  class Event_upgrade_version_1_13_to_1_14_base_game(GameEvent):
    def on_upgrade_game_version_1_13(event):
      if mc["moments_of_glory"]:
        mc["moments_of_glory_strength"] = True
      return [1,14]

  class Event_upgrade_version_1_14_to_1_15_base_game(GameEvent):
    def on_upgrade_game_version_1_14(event):
      if not isabelle.owned_item("isabelle_glasses"):
        isabelle.add_item("isabelle_glasses")
      isabelle.equip("isabelle_glasses")
      return [1,15]

  class Event_upgrade_version_1_15_to_1_16_base_game(GameEvent):
    def on_upgrade_game_version_1_15(event):
      return [1,16]

  class Event_upgrade_version_1_16_to_1_17_base_game(GameEvent):
    def on_upgrade_game_version_1_16(event):
      return [1,17]

  class Event_upgrade_version_1_17_to_1_18_base_game(GameEvent):
    def on_upgrade_game_version_1_17(event):
      return [1,18]

  class Event_upgrade_version_1_18_to_1_19_base_game(GameEvent):
    def on_upgrade_game_version_1_18(event):
      SKIP_FIX = True #if the save was made prior to 1.19, skip the 1.19->1.20 fix
      return [1,19]

  class Event_upgrade_version_1_19_to_1_20_base_game(GameEvent):
    def on_upgrade_game_version_1_19(event):
      if persistent.unlocked_replays is not None and "flora_shower" in persistent.unlocked_replays:
        persistent.unlocked_replays.remove("flora_shower")
        persistent.unlocked_replays.add("flora_relief")
      return [1,20]

  class Event_upgrade_version_1_20_to_1_20_1_base_game(GameEvent):
    def on_upgrade_game_version_1_20(event):
      if 'SKIP_FIX' not in globals():#only implement this questionable fix if the save isn't from 1.18 or prior
        newstack = []
        for return_item in renpy.get_return_stack():
          if isinstance(return_item,(list,tuple)):
            if "core.rpy" in return_item[0]:
              if return_item[2] == 34:
                newstack.append("_call_expression_11")
              elif return_item[2] == 36:
                newstack.append("_call_expression_12")
              else:
                newstack.append("main_loop")
            elif "phone.rpy" in return_item[0]:
              if return_item[2] == 44811:
                newstack.append("_call_expression_13")#guessing here
              else:
                newstack.append("main_loop")
            elif "inventory.rpy" in return_item[0]:
              if return_item[2] == 34550:
                newstack.append("_call_expression_4")#an item was interacted with
              elif return_item[2] == 34554:
                newstack.append("_call_expression_5")
              elif return_item[2] == 34538:
                newstack.append("_call_expression_2")#an item was used
              elif return_item[2] == 34543:
                newstack.append("inventory")
              else:
                newstack.append("inventory")
            else:
              newstack.append(return_item)
          else:
            newstack.append(return_item)
#       print("save stack before fix:")
#       for value in renpy.get_return_stack():
#         print(value)
#       print("save stack after fix:")
#       for value in newstack:
#         print(value)
        renpy.set_return_stack(newstack)#replace the entire return stack with the newly made one
        renpy.block_rollback()#stops from rolling back further than the point of the save
      return [1,20,1]

  class Event_upgrade_version_1_20_1_to_1_21_base_game(GameEvent):
    def on_upgrade_game_version_1_20_1(event):
      if not lindsey.owned_item("lindsey_skirt"):
        lindsey.add_item("lindsey_skirt")
      if lindsey.equipped_item("lindsey_pants"):
        lindsey.equip("lindsey_skirt")
      if not lindsey.owned_item("lindsey_cropped_hoodie"):
        lindsey.add_item("lindsey_cropped_hoodie")
      if not lindsey.owned_item("lindsey_sweatpants"):
        lindsey.add_item("lindsey_sweatpants")
      return [1,21]

  class Event_upgrade_version_1_21_to_1_22_base_game(GameEvent):
    def on_upgrade_game_version_1_21(event):
      if school_first_hall_west["tuning_hammer"] and school_first_hall_west["tuning_fork"]:
        quest.piano_tuning.start()
        quest.piano_tuning.finish()
      return [1,22]

  class Event_upgrade_version_1_22_to_1_23_base_game(GameEvent):
    def on_upgrade_game_version_1_22(event):
      if not flora.owned_item("flora_business_shirt"):
        flora.add_item("flora_business_shirt")
      if not flora.owned_item("flora_skirt"):
        flora.add_item("flora_skirt")
      return [1,23]

  class Event_upgrade_version_1_23_to_1_24_base_game(GameEvent):
    def on_upgrade_game_version_1_23(event):
      return [1,24]

  class Event_upgrade_version_1_24_to_1_25_base_game(GameEvent):
    def on_upgrade_game_version_1_24(event):
      return [1,25]

  class Event_upgrade_version_1_25_to_1_25_2_base_game(GameEvent):
    def on_upgrade_game_version_1_25(event):
      return [1,25,2]

  class Event_upgrade_version_1_25_2_to_1_26_base_game(GameEvent):
    def on_upgrade_game_version_1_25_2(event):
      if quest.lindsey_wrong["first_hug"]:
        quest.lindsey_wrong["swooped_in"] = True
      return [1,26]

  class Event_upgrade_version_1_26_to_1_26_1_base_game(GameEvent):
    def on_upgrade_game_version_1_26(event):
      if quest.kate_over_isabelle.in_progress and quest.kate_over_isabelle >= "search_and_schedule":
        quest.kate_over_isabelle.finish(silent=True)
      if quest.isabelle_over_kate.in_progress and quest.isabelle_over_kate >= "search_and_schedule":
        quest.isabelle_over_kate.finish(silent=True)
      if school_music_class["masturbation_jacklyn"]:
        unlock_replay("jacklyn_interruption")
      if school_roof["isabelle_kiss"]:
        unlock_replay("isabelle_kissing_tips")
      return [1,26,1]

  class Event_upgrade_version_1_26_1_to_1_27_base_game(GameEvent):
    def on_upgrade_game_version_1_26_1(event):
      return [1,27]

  class Event_upgrade_version_1_27_to_1_28_base_game(GameEvent):
    def on_upgrade_game_version_1_27(event):
      if quest.nurse_venting > "bottle" and mc.owned_item("walkie_talkie") and mc.owned_item("maxine_camera") and not school_clubroom["camera_taken"]:
        mc.remove_item("walkie_talkie",mc.owned_item_count("walkie_talkie"),silent=True)
        mc.remove_item("maxine_camera",mc.owned_item_count("maxine_camera"),silent=True)
      return [1,28]

  class Event_upgrade_version_1_28_to_1_29_base_game(GameEvent):
    def on_upgrade_game_version_1_28(event):
      if school_forest_glade["exclusive"] == "maxine" and quest.maxine_hook not in ("glade","shovel","dig"):
        school_forest_glade["exclusive"] = False
      return [1,29]

  class Event_upgrade_version_1_29_to_1_30_base_game(GameEvent):
    def on_upgrade_game_version_1_29(event):
      if not kate.owned_item("kate_angel_costume"):
        kate.add_item("kate_angel_costume")
      if not lindsey.owned_item("lindsey_mummy_costume"):
        lindsey.add_item("lindsey_mummy_costume")
      lindsey.add_stat("hate",1,0)
      return [1,30]

  class Event_upgrade_version_1_30_to_1_31_base_game(GameEvent):
    def on_upgrade_game_version_1_30(event):
      if quest.nurse_venting.finished and mc.owned_item("fishing_hook") and not mc.owned_item("fishing_line"):
        mc.add_item("fishing_line", silent=True)
      if not isabelle.owned_item("isabelle_party_dress"):
        isabelle.add_item("isabelle_party_dress")
      if (quest.kate_stepping.finished and not quest.kate_stepping["second_time"]) and game.location == "school_gym":
        renpy.block_rollback()
        renpy.hide("black","screens")
        renpy.hide("to_be_continued","screens")
        quest.kate_stepping.finished = False
        quest.kate_stepping.advance("fly_to_the_moon")
        game.quest_guide = "kate_stepping"
        mc["focus"] = "kate_stepping"
      return [1,31]

  class Event_upgrade_version_1_31_to_1_31_2_base_game(GameEvent):
    def on_upgrade_game_version_1_31(event):
      return [1,31,2]

  class Event_upgrade_version_1_31_2_to_1_32_base_game(GameEvent):
    def on_upgrade_game_version_1_31_2(event):
      if not hasattr(game,"season"):
        game.season = 1
      if not hasattr(game,"timed_out_quests"):
        game.timed_out_quests = set()
      if not hasattr(game,"skipped_backstory_choices"):
        game.skipped_backstory_choices = False
      if not jo.owned_item("jo_bikini_top"):
        jo.add_item("jo_bikini_top")
      if not jo.owned_item("jo_bikini_bottom"):
        jo.add_item("jo_bikini_bottom")
      if (quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started:
        renpy.block_rollback()
        renpy.hide("misc ambulance","screens")
        renpy.hide("black","screens")
        renpy.hide("to_be_continued","screens")
        for quest_title,quest_id in get_available_quest_guides():
          if quest_id not in ("","act_one"):
            game.quests[quest_id].finish(silent=True)
            game.timed_out_quests.add(quest_id)
        game.quest_guide = "kate_stepping" if quest.kate_stepping.finished else "isabelle_dethroning"
      return [1,32]

  class Event_upgrade_version_1_32_to_1_32_1_base_game(GameEvent):
    def on_upgrade_game_version_1_32(event):
      if not hasattr(game,"hud_hint"):
        game.hud_hint = ""
      return [1,32,1]

  class Event_upgrade_version_1_32_1_to_1_33_base_game(GameEvent):
    def on_upgrade_game_version_1_32_1(event):
      if ((quest.lindsey_nurse.finished and mc["focus"] == "lindsey_nurse")
      or (quest.kate_wicked.finished and mc["focus"] == "kate_wicked")):
        mc["focus"] = ""
      if quest.jo_washed.finished and not quest.fall_in_newfall.started:
        renpy.block_rollback()
        renpy.hide("black","screens")
        renpy.hide("to_be_continued","screens")
        game.quest_guide = "fall_in_newfall"
      if not flora.owned_item("flora_dress"):
        flora.add_item("flora_dress")
      if not flora.owned_item("flora_blouse"):
        flora.add_item("flora_blouse")
      if not flora.owned_item("flora_purple_bra"):
        flora.add_item("flora_purple_bra")
      if not flora.owned_item("flora_striped_panties"):
        flora.add_item("flora_striped_panties")
      isabelle.outfit_slots = ["hat","collar","jacket","shirt","bra","pants","panties"]
      if ["isabelle_hat",1] in isabelle.inv:
        isabelle.inv.remove(["isabelle_hat",1])
      if not isabelle.owned_item("isabelle_tiara"):
        isabelle.add_item("isabelle_tiara")
      if not isabelle.owned_item("isabelle_collar"):
        isabelle.add_item("isabelle_collar")
      if not isabelle.owned_item("isabelle_jacket"):
        isabelle.add_item("isabelle_jacket")
      if not isabelle.owned_item("isabelle_top"):
        isabelle.add_item("isabelle_top")
      if not isabelle.owned_item("isabelle_green_bra"):
        isabelle.add_item("isabelle_green_bra")
      if not isabelle.owned_item("isabelle_skirt"):
        isabelle.add_item("isabelle_skirt")
      if not isabelle.owned_item("isabelle_green_panties"):
        isabelle.add_item("isabelle_green_panties")
      jacklyn.outfit_slots = ["choker","pin","shirt","bra","pants","fishnet","panties"]
      if not jacklyn.owned_item("jacklyn_chained_choker"):
        jacklyn.add_item("jacklyn_chained_choker")
      if not jacklyn.owned_item("jacklyn_jacket"):
        jacklyn.add_item("jacklyn_jacket")
      if not jacklyn.owned_item("jacklyn_orange_bra"):
        jacklyn.add_item("jacklyn_orange_bra")
      if not jacklyn.owned_item("jacklyn_shorts"):
        jacklyn.add_item("jacklyn_shorts")
      if not jacklyn.owned_item("jacklyn_high_waist_fishnet"):
        jacklyn.add_item("jacklyn_high_waist_fishnet")
      if not jacklyn.owned_item("jacklyn_orange_panties"):
        jacklyn.add_item("jacklyn_orange_panties")
      if not jo.owned_item("jo_blazer"):
        jo.add_item("jo_blazer")
      if jo.equipped_item("jo_shirt"):
        jo.equip("jo_blazer")
      if not jo.owned_item("jo_skirt"):
        jo.add_item("jo_skirt")
      if jo.equipped_item("jo_pants"):
        jo.equip("jo_skirt")
      jo.outfit_slots = ["glasses","coat","shirt","bra","pants","panties"]
      if ["jo_hat",1] in jo.inv:
        jo.inv.remove(["jo_hat",1])
      if not jo.owned_item("jo_glasses"):
        jo.add_item("jo_glasses")
      jo.equip("jo_glasses")
      if not jo.owned_item("jo_coat"):
        jo.add_item("jo_coat")
      if not jo.owned_item("jo_shirt"):
        jo.add_item("jo_shirt")
      if not jo.owned_item("jo_pants"):
        jo.add_item("jo_pants")
      if not jo.owned_item("jo_white_bra"):
        jo.add_item("jo_white_bra")
      if not jo.owned_item("jo_white_panties"):
        jo.add_item("jo_white_panties")
      kate.outfit_slots = ["necklace","shirt","bra","pants","panties","boots"]
      if ["kate_hat",1] in kate.inv:
        kate.inv.remove(["kate_hat",1])
      if not kate.owned_item("kate_choker"):
        kate.add_item("kate_choker")
      if not kate.owned_item("kate_bardot_top"):
        kate.add_item("kate_bardot_top")
      if not kate.owned_item("kate_blue_bra"):
        kate.add_item("kate_blue_bra")
      if not kate.owned_item("kate_skirt"):
        kate.add_item("kate_skirt")
      if not kate.owned_item("kate_blue_panties"):
        kate.add_item("kate_blue_panties")
      if not kate.owned_item("kate_knee_high_boots"):
        kate.add_item("kate_knee_high_boots")
      maxine.outfit_slots = ["hat","glasses","necklace","shirt","bra","pants","panties"]
      if not maxine.owned_item("maxine_glasses"):
        maxine.add_item("maxine_glasses")
      maxine.equip("maxine_glasses")
      if not maxine.owned_item("maxine_sweater"):
        maxine.add_item("maxine_sweater")
      if not maxine.owned_item("maxine_black_bra"):
        maxine.add_item("maxine_black_bra")
      if not maxine.owned_item("maxine_skirt"):
        maxine.add_item("maxine_skirt")
      if not maxine.owned_item("maxine_black_panties"):
        maxine.add_item("maxine_black_panties")
      if not mrsl.owned_item("mrsl_dress"):
        mrsl.add_item("mrsl_dress")
      mrsl.outfit_slots = ["coat","dress","bra","panties"]
      if "mrsl_shirt" in mrsl.outfit["shirt"]:
        mrsl.equip("mrsl_dress")
      if ["mrsl_hat",1] in mrsl.inv:
        mrsl.inv.remove(["mrsl_hat",1])
      if ["mrsl_shirt",1] in mrsl.inv:
        mrsl.inv.remove(["mrsl_shirt",1])
      if "shirt" in mrsl.outfit:
        del mrsl.outfit["shirt"]
      if ["mrsl_pants",1] in mrsl.inv:
        mrsl.inv.remove(["mrsl_pants",1])
      if "pants" in mrsl.outfit:
        del mrsl.outfit["pants"]
      if not mrsl.owned_item("mrsl_coat"):
        mrsl.add_item("mrsl_coat")
      if not mrsl.owned_item("mrsl_red_dress"):
        mrsl.add_item("mrsl_red_dress")
      if not mrsl.owned_item("mrsl_black_bra"):
        mrsl.add_item("mrsl_black_bra")
      if not mrsl.owned_item("mrsl_black_panties"):
        mrsl.add_item("mrsl_black_panties")
      nurse.outfit_slots = ["hat","outfit","shirt","bra","pants","panties"]
      if not nurse.owned_item("nurse_dress"):
        nurse.add_item("nurse_dress")
      if not nurse.owned_item("nurse_green_bra"):
        nurse.add_item("nurse_green_bra")
      if not nurse.owned_item("nurse_green_panties"):
        nurse.add_item("nurse_green_panties")
      maya.outfit_slots = ["jacket","dress","bra","panties"]
      if not maya.owned_item("maya_jacket"):
        maya.add_item("maya_jacket")
      maya.equip("maya_jacket")
      if not maya.owned_item("maya_dress"):
        maya.add_item("maya_dress")
      maya.equip("maya_dress")
      if not maya.owned_item("maya_bra"):
        maya.add_item("maya_bra")
      maya.equip("maya_bra")
      if not maya.owned_item("maya_panties"):
        maya.add_item("maya_panties")
      maya.equip("maya_panties")
      return [1,33]

  class Event_upgrade_version_1_33_to_1_34_base_game(GameEvent):
    def on_upgrade_game_version_1_33(event):
      return [1,34]

  class Event_upgrade_version_1_34_to_1_35_base_game(GameEvent):
    def on_upgrade_game_version_1_34(event):
      return [1,35]

  class Event_upgrade_version_1_35_to_1_35_2_base_game(GameEvent):
    def on_upgrade_game_version_1_35(event):
      return [1,35,2]

  class Event_upgrade_version_1_35_2_to_1_36_base_game(GameEvent):
    def on_upgrade_game_version_1_35_2(event):
      if hasattr(store,"most_lust"):
        quest.jacklyn_sweets["jacklyn_blowjob"] = True
      return [1,36]

  class Event_upgrade_version_1_36_to_1_36_1_base_game(GameEvent):
    def on_upgrade_game_version_1_36(event):
      if school_first_hall["trash_bin_interact"] and not mc.owned_item("rock"):
        del school_first_hall.flags["trash_bin_interact"]
      if quest.flora_cooking_chilli == "chilli_done" and home_hall["bathroom_locked_now"]:
        del home_hall.flags["bathroom_locked_now"]
      if quest.flora_cooking_chilli == "chilli_done" and home_hall["bathroom_locked_today"]:
        del home_hall.flags["bathroom_locked_today"]
      if quest.mrsl_table == "morning" and "quest_kate_wicked_school" in game.events_queue:
        game.events_queue.remove("quest_kate_wicked_school")
      return [1,36,1]

  class Event_upgrade_version_1_36_1_to_1_37_base_game(GameEvent):
    def on_upgrade_game_version_1_36_1(event):
      if "pee" in school_computer_room.flags:
        quest.maya_witch["piss_king"] = True
      return [1,37]

  class Event_upgrade_version_1_37_to_1_38_base_game(GameEvent):
    def on_upgrade_game_version_1_37(event):
      if quest.fall_in_newfall.finished and not quest.lindsey_book.finished:
        quest.lindsey_book.start(silent=True)
        if not school_english_class["bookshelf_interact_first_time"]:
          school_english_class["bookshelf_interact_first_time"] = True
          mc.add_item("marilyn_the_mason_drawing", silent=True)
        mc.add_item("snow_white", silent=True)
        mc.remove_item("snow_white", silent=True)
        if not school_entrance["bush_interacted"]:
          school_entrance["bush_interacted"] =  True
          mc.add_item("stick", silent=True)
        if not school_homeroom["stick_interacted"]:
          school_homeroom["stick_interacted"] = True
          mc.add_item("stick", silent=True)
        if not school_gym["stick_interacted"]:
          school_gym["stick_interacted"] = True
          mc.add_item("stick", silent=True)
        if not school_homeroom["ball_of_yarn_taken"]:
          school_homeroom["ball_of_yarn_taken"] = True
          mc.add_item("ball_of_yarn", 5, silent=True)
        mc.remove_item("stick", silent=True)
        mc.remove_item("stick", silent=True)
        mc.add_item("stick_two", silent=True)
        mc.remove_item("stick_two", silent=True)
        mc.remove_item("stick", silent=True)
        mc.add_item("stick_three", silent=True)
        mc.remove_item("stick_three", silent=True)
        mc.remove_item("ball_of_yarn", silent=True)
        mc.add_item("makeshift_easel", silent=True)
        mc.remove_item("makeshift_easel", silent=True)
        if not school_homeroom["got_book"]:
          school_homeroom["got_book"] = True
          mc.add_item("bayonets_etiquettes", silent=True)
        if not quest.isabelle_haggis["got_wrench"]:
          quest.isabelle_haggis["got_wrench"] = True
          mc.add_item("monkey_wrench", silent=True)
        mc.add_item("beaver_carcass", silent=True)
        quest.lindsey_book.finish(silent=True)
      if quest.fall_in_newfall.finished and not quest.berb_fight.actually_finished:
        quest.berb_fight.start(silent=True)
        mc.add_item("book_of_the_dammed", silent=True)
        if not school_english_class["desk_three_first"]:
          school_english_class["desk_three_first"] = True
          mc.add_item("baseball", silent=True)
        mc.remove_item("baseball", silent=True)
        school_entrance["baseball_picked_up"] = True
        mc.add_item("baseball", silent=True)
        school_forest_glade["unlocked"] = True
        if not home_kitchen["water_bottle_taken"]:
          home_kitchen["water_bottle_taken"] = True
          mc.add_item("water_bottle", silent=True)
          mc.remove_item("water_bottle", silent=True)
          mc.add_item("empty_bottle", silent=True)
        mc.remove_item("empty_bottle", silent=True)
        mc.add_item("pepelepsi", silent=True)
        school_forest_glade["pollution"]+=1
        mc.remove_item("pepelepsi", silent=True)
        mc.add_item("empty_bottle", silent=True)
        mc.remove_item("empty_bottle", silent=True)
        mc.add_item("pepelepsi", silent=True)
        school_forest_glade["pollution"]+=1
        mc.remove_item("pepelepsi", silent=True)
        mc.add_item("empty_bottle", silent=True)
        mc.remove_item("empty_bottle", silent=True)
        mc.add_item("pepelepsi", silent=True)
        school_forest_glade["pollution"]+=1
        mc.remove_item("pepelepsi", silent=True)
        mc.add_item("empty_bottle", silent=True)
        if mc.owned_item(("empty_bottle","spray_empty_bottle")):
          mc.remove_item("empty_bottle", silent=True)
        mc.add_item("lollipop", 5, silent=True)
        mc.remove_item("lollipop", 5, silent=True)
        mc.add_item("wrapper", 5, silent=True)
        school_forest_glade["wrappers"] = True
        mc.remove_item("wrapper", 5, silent=True)
        if not school_english_class["desk_four_interact_first"]:
          school_english_class["desk_four_interact_first"] = True
          mc.add_item("apple", silent=True)
        school_ground_floor["locker_unlocked"] = True
        if not quest.jo_potted["got_soup"]:
          quest.jo_potted["got_soup"] = True
          mc.add_item("soup_can", silent=True)
        if not school_locker["gotten_lunch_today"]:
          school_locker["gotten_lunch_today"] = True
          mc.add_item("tide_pods", 3, silent=True)
        mc.remove_item("apple", silent=True)
        mc.remove_item("tide_pods", silent=True)
        mc.add_item("poisoned_apple", silent=True)
        mc.remove_item("poisoned_apple", silent=True)
        if not school_gym["light_smashed"]:
          school_gym["light_smashed"] = True
          mc.remove_item("baseball", silent=True)
          mc.add_item("glass_shard", silent=True)
          mc.add_item("baseball", silent=True)
        if not berb["scalped"]:
          berb["scalped"] = True
          mc.add_item("beaver_pelt", silent=True)
        quest.berb_fight.finish(silent=True)
      if quest.fall_in_newfall.finished and not school_computer_room["welcome_back"]:
        school_computer_room["welcome_back"] = True
      if quest.flora_bonsai["love_route"]:
        del quest.flora_bonsai.flags["love_route"]
        quest.flora_bonsai["handholding"] = True
      return [1,38]

  class Event_upgrade_version_1_38_to_1_39_base_game(GameEvent):
    def on_upgrade_game_version_1_38(event):
      return [1,39]

  class Event_upgrade_version_1_39_to_1_40_base_game(GameEvent):
    def on_upgrade_game_version_1_39(event):
      if quest.kate_moment == "coming_soon" and not quest.isabelle_gesture.started:
        if mc["focus"]:
          quest.isabelle_gesture["released"] = True
        else:
          kate["at_none"] = True
          mc["focus"] = "kate_moment"
          quest.kate_moment.hidden = False
          game.quest_guide = game.quest_guide if game.quest_guide else "kate_moment"
      if quest.isabelle_dethroning["kate_love_points"] and not quest.kate_moment["kate_love_points_redeemed"]:
        kate.love+=3
        quest.kate_moment["kate_love_points_redeemed"] = True
      return [1,40]

  class Event_upgrade_version_1_40_to_1_41_base_game(GameEvent):
    def on_upgrade_game_version_1_40(event):
      if quest.jo_washed.started and school_first_hall["paint_splash"]:
        school_first_hall["paint_splash"] = False
      if quest.maxine_eggs.finished and quest.nurse_venting == "ready" and not mc.owned_item("maxine_camera"):
        school_clubroom["camera_taken"] = True
        mc.add_item("maxine_camera", silent=True)
      if not maya.owned_item("maya_towel"):
        maya.add_item("maya_towel")
      if not flora.owned_item("flora_armor"):
        flora.add_item("flora_armor")
      if not maya.owned_item("maya_armor"):
        maya.add_item("maya_armor")
      return [1,41]

  class Event_upgrade_version_1_41_to_1_42_base_game(GameEvent):
    def on_upgrade_game_version_1_41(event):
      if school_homeroom["exclusive"] == True:
        school_homeroom["exclusive"] = "True"
      if not maxine.owned_item("maxine_hazmat_suit"):
        maxine.add_item("maxine_hazmat_suit")
      return [1,42]

  class Event_upgrade_version_1_42_to_1_43_base_game(GameEvent):
    def on_upgrade_game_version_1_42(event):
      if quest.lindsey_angel.finished and not quest.lindsey_angel["finished_this_week_day_tracker"]:
        quest.lindsey_angel["finished_this_week"] = True
        quest.lindsey_angel["finished_this_week_day_tracker"] = game.day - 6
      if quest.jacklyn_romance.finished and not quest.jacklyn_romance["finished_day_tracker"]:
        quest.jacklyn_romance["finished_day_tracker"] = game.day - 4
      if quest.isabelle_gesture >= "wait" and not mc.check_phone_contact("isabelle"):
        mc.add_phone_contact("isabelle", silent=True)
      if not jacklyn.owned_item("jacklyn_dress"):
        jacklyn.add_item("jacklyn_dress")
      if not jacklyn.owned_item("jacklyn_thigh_high_fishnet"):
        jacklyn.add_item("jacklyn_thigh_high_fishnet")
      if not flora.owned_item("flora_sun_hat"):
        flora.add_item("flora_sun_hat")
      if not flora.owned_item("flora_trench_coat"):
        flora.add_item("flora_trench_coat")
      return [1,43]

  class Event_upgrade_version_1_43_to_1_44_base_game(GameEvent):
    def on_upgrade_game_version_1_43(event):
      mrsl.outfit_slots = ["hairstyle","panties","pants","bra","dress","coat"]
      if not mrsl.owned_item("mrsl_ponytail"):
        mrsl.add_item("mrsl_ponytail")
      if not mrsl.owned_item("mrsl_workout_panties"):
        mrsl.add_item("mrsl_workout_panties")
      if not mrsl.owned_item("mrsl_leggings"):
        mrsl.add_item("mrsl_leggings")
      if not mrsl.owned_item("mrsl_workout_bra"):
        mrsl.add_item("mrsl_workout_bra")
      if persistent.unlocked_replays is not None and "mrsl_workout" in persistent.unlocked_replays:
        persistent.unlocked_replays.remove("mrsl_workout")
        persistent.unlocked_replays.add("mrsl_tape")
      return [1,44]

  class Event_upgrade_version_1_44_to_1_45_base_game(GameEvent):
    def on_upgrade_game_version_1_44(event):
      if persistent.unlocked_replays is not None and "mom_kiss" in persistent.unlocked_replays:
        persistent.unlocked_replays.remove("mom_kiss")
        persistent.unlocked_replays.add("jo_kiss")
      if ("investigate",("home_hall_door_mom","Not about to see your light... but if you want to find Hell with me...")) in game.seen_actions:
        game.seen_actions.remove(("investigate",("home_hall_door_mom","Not about to see your light... but if you want to find Hell with me...")))
        game.seen_actions.add(("investigate",("home_hall_door_jo","Not about to see your light... but if you want to find Hell with me...")))
      if ("investigate",("home_hall_door_mom","The creakiest door in the house. [jo] refuses to oil it for the sole reason of catching intruders.")) in game.seen_actions:
        game.seen_actions.remove(("investigate",("home_hall_door_mom","The creakiest door in the house. [jo] refuses to oil it for the sole reason of catching intruders.")))
        game.seen_actions.add(("investigate",("home_hall_door_jo","The creakiest door in the house. [jo] refuses to oil it for the sole reason of catching intruders.")))
      if ("home_hall_door_mom_interact",()) in game.seen_actions:
        game.seen_actions.remove(("home_hall_door_mom_interact",()))
        game.seen_actions.add(("home_hall_door_jo_interact",()))
      if ("home_hall_door_mom_interact_flora_cooking_chilli_return_phone",()) in game.seen_actions:
        game.seen_actions.remove(("home_hall_door_mom_interact_flora_cooking_chilli_return_phone",()))
        game.seen_actions.add(("home_hall_door_jo_interact_flora_cooking_chilli_return_phone",()))
      if "home_hall_door_mom" in game.last_actions:
        del game.last_actions["home_hall_door_mom"]
      if quest.isabelle_stolen["not_what_it_looks_like"]:
        mc["detention"]+=1
      if quest.maxine_eggs["busted"]:
        mc["detention"]+=1
      if "pee" in school_cafeteria.flags:
        mc["detention"]+=1
      if quest.maya_spell["busted"]:
        mc["detention"]+=1
      if not maxine.owned_item("maxine_armor"):
        maxine.add_item("maxine_armor")
      # if ["buried_box", [count for item,count in mc.inv if item == "buried_box"][0]] in mc.inv:
      if ["buried_box",mc.owned_item_count("buried_box")] in mc.inv:
        mc.inv[mc.inv.index(["buried_box", [count for item,count in mc.inv if item == "buried_box"][0]])][0] = "locked_box_forest"
      if ("investigate",("buried_box","Wrapped in chains and with a rusty busty lock.","buried_box")) in game.seen_actions:
        game.seen_actions.remove(("investigate",("buried_box","Wrapped in chains and with a rusty busty lock.","buried_box")))
        game.seen_actions.add(("investigate",("locked_box_forest","Wrapped in chains and\nwith a rusty busty lock.","locked_box_forest")))
      if ("int_buried_box",("buried_box",)) in game.seen_actions:
        game.seen_actions.remove(("int_buried_box",("buried_box",)))
        game.seen_actions.add(("locked_box_forest_interact",("locked_box_forest",)))
      if "buried_box" in game.last_actions:
        del game.last_actions["buried_box"]
      return [1,45]

  class Event_upgrade_version_1_45_to_1_46_base_game(GameEvent):
    def on_upgrade_game_version_1_45(event):
      if isabelle["creampied"]:
        del isabelle.flags["creampied"]
      if flora["creampied"]:
        del flora.flags["creampied"]
      if not maxine.owned_item("maxine_boat_captain_uniform"):
        maxine.add_item("maxine_boat_captain_uniform")
      if not maxine.owned_item("maxine_boat_captain_hat"):
        maxine.add_item("maxine_boat_captain_hat")
      if not maxine.owned_item("maxine_diving_suit"):
        maxine.add_item("maxine_diving_suit")
      if not maxine.owned_item("maxine_snorkel"):
        maxine.add_item("maxine_snorkel")
      nurse.outfit_slots = ["hat","pendant","outfit","shirt","bra","pants","panties"]
      if not nurse.owned_item("nurse_hiking_gear"):
        nurse.add_item("nurse_hiking_gear")
      if not nurse.owned_item("nurse_sun_hat"):
        nurse.add_item("nurse_sun_hat")
      if not nurse.owned_item("nurse_black_panties"):
        nurse.add_item("nurse_black_panties")
      if not nurse.owned_item("nurse_black_bra"):
        nurse.add_item("nurse_black_bra")
      if not nurse.owned_item("nurse_revealing_dress"):
        nurse.add_item("nurse_revealing_dress")
      if not nurse.owned_item("nurse_sophisticated_dress"):
        nurse.add_item("nurse_sophisticated_dress")
      if not nurse.owned_item("nurse_nice_dress"):
        nurse.add_item("nurse_nice_dress")
      if not nurse.owned_item("nurse_pendant"):
        nurse.add_item("nurse_pendant")
      return [1,46]

  class Event_upgrade_version_1_46_to_1_47_base_game(GameEvent):
    def on_upgrade_game_version_1_46(event):
      if ((quest.maxine_dive == "armor" and mc.owned_item(("cardboard","armor"))) or quest.maxine_dive >= "dive") and home_kitchen["pizza_boxes"]:
        home_kitchen["pizza_boxes"] = 0
      if (mc.owned_item("armor") or quest.maxine_dive >= "dive") and mc.owned_item("cardboard"):
        mc.remove_item("cardboard", mc.owned_item_count("cardboard"), silent=True)
      if quest.nurse_aid["name_reveal"]:
        quest.nurse_aid["girlfriend"] = nurse["girlfriend"] = True
      return [1,47]

  class Event_upgrade_version_1_47_to_1_48_base_game(GameEvent):
    def on_upgrade_game_version_1_47(event):
      if not flora.owned_item("flora_referee_shirt"):
        flora.add_item("flora_referee_shirt")
      if not flora.owned_item("flora_referee_cap"):
        flora.add_item("flora_referee_cap")
      maya.outfit_slots = ["hat","jacket","dress","bra","panties"]
      if not maya.owned_item("maya_hair_towel"):
        maya.add_item("maya_hair_towel")
      return [1,48]




## Sample version upgrader
#
#init python:
#  ## some meaningful name to avoid clashes
#  class Event_upgrade_version_1_1_0_to_1_1_1_base_game(GameEvent):
#    ## this handler will be called when upgrading from version [1,1,0]
#    def on_upgrade_game_version_1_1_0(event):
#      ## set characters timestamp attribute if not found
#      for char_id,char in game.characters.items():
#        if "last_phone_message_timestamp" not in char.__dict__:
#          char.last_phone_message_timestamp=None
#      ## report back game is upgraded to version [1,1,1]
#      return [1,1,1]
#
## This handler is irrelevant now, as message timestamp was reworked, but it gives idea how to use upgraders
