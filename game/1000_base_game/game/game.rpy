init python:
  class Event_game_internal_events(GameEvent):
    def on_quest_started(event,quest,silent=False):
      if quest.id not in game.quest_started_order:
        game.quest_started_order.append(quest.id)

  class Game(BaseGame):
    max_side_notifications=5
    tod_names=[
      "night", # 0
      "night", # 1
      "night", # 2
      "night", # 3
      "night", # 4
      "night", # 5
      "night", # 6
      "day",   # 7
      "day",   # 8
      "day",   # 9
      "day",   # 10
      "day",   # 11
      "day",   # 12
      "day",   # 13
      "day",   # 14
      "day",   # 15
      "day",   # 16
      "day",   # 17
      "day",   # 18
      "day",   # 19
      "day",   # 20
      "day",   # 21
      "night", # 22
      "night", # 23
      ]
    dow_names=[
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
      ]
    dow_short_names=[
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "Sat",
      "Sun",
      ]
    def __init__(self):
      super().__init__()
      self.xray_mode=0
      self.notifications_counter=0
      self.notifications=[None]*(self.max_side_notifications+1)
      self.delayed_notifications=[]
      self.disable_notifications=False
      self.quest_started_order=[]
      self.quest_guide=None
      self.failed_recipes=set()
      self.fast_mode=False
    @property
    def time_str(self):
      return time_str(self.hour,0)
    @property
    def tod(self):
      return self.tod_names[self.hour]
    @property
    def dow(self):
      return (self.day-1)%7
    @property
    def dow_str(self):
      return self.dow_names[self.dow]
    @property
    def dow_short(self):
      return self.dow_short_names[self.dow]
    def day_dow(self,day):
      return self.dow_names[(day-1)%7]
    def day_dow_short(self,day):
      return self.dow_short_names[(day-1)%7]
    def notify(self,icon,message,wait=3.0):
      n=self.notifications.index(None)
      if n==self.max_side_notifications:
        self.delayed_notifications.append([icon,message,wait])
      else:
        self.notifications[n]=True
        self.notifications_counter+=1
        renpy.show_screen("notification_side",n,self.notifications_counter,icon,message,wait,_tag="notification_side_"+str(n))
    def process_delayed_notifications(self):
      while self.delayed_notifications and self.notifications.index(None)!=self.max_side_notifications:
        self.notify(*self.delayed_notifications.pop(0))
    def notify_modal(self,bg_type,title,message,wait=5.0,_transform=True):
      renpy.call_screen("notification_modal",bg_type,title,message,wait,_transform)

label advance_time:
  $game.advance()
  return
