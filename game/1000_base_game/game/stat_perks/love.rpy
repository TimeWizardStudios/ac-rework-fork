init python:
  class StatPerk_love1(StatPerk):
    parent_stat="love"
    title="Franz Hose"
    description="You chose to serve your hypothetical boss a CPU-cooked medium-rare goat sirloin. A man of wit and refined tastes always makes a delicious meal out of a bad situation."

  class StatPerk_love2(StatPerk):
    parent_stat="love"
    title="Cute and kind"
    description="You said that your ideal lover is someone cute and kind. A combination of looks and personality — the classic choice."

  class StatPerk_love3(StatPerk):
    parent_stat="love"
    title="Benevolent"
    description="Wholesomeness is your sword and decency is your shield. Even when dealing with potential scammers, you remain benevolent."
  
  class StatPerk_love4(StatPerk):
    parent_stat="love"
    title="Something nice"
    description="You wrote something nice instead of vulgar. You're like one of those graffiti artists who want to convey a positive message through their vandalism."
    
  class StatPerk_love5(StatPerk): #-1 love
    parent_stat="love"
    title="Not volunteering"
    description="[isabelle] asked you to show her around. Instead, you showed her who's boss."
    
  class StatPerk_love6(StatPerk): #-1 love
    parent_stat="love"
    title="Your accent hurts my ears"
    description="You tried your hand at negging. Backfired. Rudeness does not a bad boy make."
    
  class StatPerk_love7(StatPerk):
    parent_stat="love"
    title="Stay silent"
    description="It's better to keep your mouth closed and let people think you're a fool than to open it and remove all doubt. — Mark Twain"
      
  class StatPerk_love8(StatPerk):
    parent_stat="love"
    title="Help her up"
    description="A gentleman, are you? If only she knew where your hand has been."
      
  class StatPerk_love9(StatPerk): #+2 love
    parent_stat="love"
    title="No need for me to get involved"
    description="Phew! Certainly dodged a flaming hot bullet there. You could've gotten that chili in your eyes. And then how are you supposed to watch porn?"
      
  class StatPerk_love10(StatPerk):
    parent_stat="love"
    title="Wait outside"
    description="You decided to wait outside while [flora]'s pussy was consumed by flames. Long shall your heroic cowardice be remembered."
    
  class StatPerk_love11(StatPerk):
    parent_stat="love"
    title="Give her privacy"
    description="You saw [flora] naked and put your hand on her pussy, but watching her masturbate, that's taking it too far. Your moral compass seems to have lost its needle."
      
  class StatPerk_love12(StatPerk):
    parent_stat="love"
    title="Make your presence known"
    description="The [nurse] was clearly masturbating behind the curtain. Luckily for her, you've got a heart of gold. And now a limp dick to go along with it."
    
  class StatPerk_love13(StatPerk):#+2 love
    parent_stat="love"
    title="Not a creep"
    description="You saved a hurt girl from abject embarrassment. Saints and cherubs shall sing your name for generations to come."
    
  class StatPerk_love14(StatPerk): #+3 love
    parent_stat="love"
    title="No thanks"
    description="Even in a dream, you held strong in the face of temptation. Your sharp wits and virtuous backbone will likely keep you a virgin for life."
    
  class StatPerk_love15(StatPerk):
    parent_stat="love"
    title="Ask [kate] out"
    description="Asking [kate] out was perhaps naive, stupid, and all sorts of cringy. But it wasn't cowardly."
    
  class StatPerk_love16(StatPerk):
    parent_stat="love"
    title="Try to find love"
    description="You said you wanted to find love this time around. Obviously, whether it's true or not is irrelevant. You'll take just about any love (please!)."
    
  class StatPerk_love17(StatPerk):
    parent_stat="love"
    title="Give her a lollipop"
    description="You wanted to give the [nurse] a lollipop and call it a day. Was that to save her from further abuse at [kate]'s hands, or did you just want to see her suck on it?"
    
  class StatPerk_love18(StatPerk):
    parent_stat="love"
    title="No deal"
    description="Mrs. Lichtenstein's thoughts clearly weren't worth that amount of money. Besides, you're still saving up for that inflatable girlfriend."
    
  class StatPerk_love19(StatPerk): # +2 love 
    parent_stat="love"
    title="She can take care of herself"
    description="You follow the rulebook of respecting wamen to a T. And also an F. That's an F."
    
  class StatPerk_love20(StatPerk):
    parent_stat="love"
    title="Don't worry, I'm not giving you up to her."
    description="Why would you give up the [nurse] to [kate]? That doesn't even make sense. Better keep her all to yourself, like an object or possession."
    
  class StatPerk_love21(StatPerk):
    parent_stat="love"
    title="Let the beaver go"
    description="Forgiveness is the true test of character. You passed, but unfortunately, so did the beaver."
