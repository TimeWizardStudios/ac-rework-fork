init python:
  class Event_notifications_phone(GameEvent):
    def on_phone_app_installed(event,char,app,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_app_installed",True):
            if getattr(char,"notify_app_installed",False):
              if app.notify_app_installed:
                pass
    def on_phone_app_uninstalled(event,char,app,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_app_uninstalled",True):
            if getattr(char,"notify_app_uninstalled",False):
              if app.notify_app_uninstalled:
                pass
    def on_phone_contact_added(event,char,contact,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_phone_contact_added",True):
            if getattr(char,"notify_phone_contact_added",False):
              game.notify(contact.contact_icon,"Contact\nadded",5.0)
