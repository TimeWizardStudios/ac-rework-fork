init python:
  class Event_notifications_stats(GameEvent):
    def on_stat_xp_granted(event,char,stat,xp,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_xp_granted",True):
            if getattr(char,"notify_xp_granted",False):
              if stat.notify_xp_granted:
                pass
    def on_stat_level_changed(event,char,stat,old_level,level,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_level_changed",True):
            if getattr(char,"notify_level_changed",False):
              if stat.notify_level_changed:
                level_delta=level-old_level
                stat_icon=[stat.icon] if isinstance(stat.icon,basestring) else stat.icon
                if abs(level_delta)<=len(stat_icon):
                  stat_icon=stat_icon[abs(level_delta)-1]
                else:
                  stat_icon=stat_icon[-1]
                icon=None if char==game.pc else char.contact_icon
                msg=("{color=#090}+{/color}" if level_delta>0 else "{color=#900}-{/color}")+"|{image="+stat_icon+"}"
                game.notify(icon,msg,5.0)
