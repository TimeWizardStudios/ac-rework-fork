init python:
  class Event_notifications_inventory(GameEvent):
    def on_inventory_changed(event,char,item,old_count,new_count,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_inventory_changed",True):
            if getattr(char,"notify_inventory_changed",False):
              delta=new_count-old_count
              msg="Gained\n" if delta>0 else "Lost\n"
              msg+="{color=#900}"+str(item)+"{/color}"
              if abs(delta)!=1:
                msg+="x{color=#900}"+str(abs(delta))+"{/color}"
              icon=item.icon if renpy.loadable("images/"+item.icon.replace(" ","/")+".webp") else "items placeholder"
              game.notify_modal(None,"Inventory","{image="+icon+"}{space=50}|"+msg,5.0)
    def on_outfit_changed(event,char,slot,old_item,new_item,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_outfit_changed",True):
            if getattr(char,"notify_outfit_changed",False):
              pass
    def on_items_combined(event,item1,item2,combine_result,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_items_combined",True):
            icon=combine_result.icon if renpy.loadable("images/"+combine_result.icon.replace(" ","/")+".webp") else "items placeholder"
            msg="{image="+icon+"}{space=60}|You combine {color=#900}"+str(item1)+"{/color} with {color=#900}"+str(item2)+"{/color} and make {color=#900}"+str(combine_result)+"{/color}."
            game.notify_modal(None,"Combine",msg,10.0)
