init python:
  class Event_notifications_achievements(GameEvent):
    def on_achievement_unlocked(event,achievement,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_achievement_unlocked",True):
            msg=achievement.unlocked_message() or (achievement.title()+"\nunlocked!")
            game.notify_modal("achievement","Achievement",msg,5.0)
