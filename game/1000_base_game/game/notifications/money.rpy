init python:
  class Event_notifications_money(GameEvent):
    def on_money_changed(event,char,old_money,money,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_money_changed",True):
            if getattr(char,"notify_money_changed",False):
              delta=money-old_money
              if delta>0:
                msg="Gained\n${color=#090}"+str(delta)+"{/color}"
              else:
                msg="Lost\n${color=#900}"+str(-delta)+"{/color}"
              game.notify("ui icon_money",msg,5.0)
