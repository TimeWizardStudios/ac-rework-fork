init python:
  class Event_notifications_quests(GameEvent):
    def on_quest_started(event,quest,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_quest_started",True):
            if not quest.hidden:
              if quest.description:
                game.notify_modal("quest","Quest started",quest.title+"{hr}"+quest.description,5.0)
    def on_quest_advanced(event,quest,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_quest_advanced",True):
            if not quest.hidden:
              if quest.description:
                game.notify_modal("quest","Quest advanced",quest.title+"{hr}"+quest.description,5.0)
    def on_quest_finished(event,quest,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_quest_finished",True):
            if not quest.hidden:
              if quest.description:
                game.notify_modal("quest","Quest complete",quest.title+"{hr}"+quest.description,5.0)
    def on_quest_failed(event,quest,silent=False):
      if not silent:
        if not game.disable_notifications:
          if getattr(game,"notify_quest_failed",True):
            if not quest.hidden:
              if quest.description:
                game.notify_modal("quest","Quest failed",quest.title+"{hr}"+quest.description,5.0)
