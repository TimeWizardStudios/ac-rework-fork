init python:
  class Quest_maxine_eggs(Quest):
    title = "The Glowing Spider Eggs"
    class phase_1_cyberia:
      description = "Another day, another road! And by \"road\" I mean a thankless task for a thankless woman."
      hint = "Computer stuff — my only skill."
      quest_guide_hint = "Interact with the leftmost computer in the computer room."
    class phase_11_plugcy:
      hint = "She wanted it in the backdoor. Let's plug that port."
      quest_guide_hint = "Use your flash drive on [cyberia]."
    class phase_12_plugpc:
      hint = "Plug it in, take it out. In, out. In, out. A steady megabyte rhythm."
      quest_guide_hint = "Use your flash drive on the leftmost computer in the computer room."
    class phase_13_deliver:
      hint = "Delivering the electronic goods to the technological unbeliever... how ironic."
      quest_guide_hint = "Quest [maxine] in the clubroom."
    class phase_20_recon:
      description = "A modern-day Augean task."
      hint = "Never say you're going to stake out the women's bathroom — that'll get you on a list."
      quest_guide_hint = "Go to the women's bathroom."
    class phase_21_bust:
      hint = "Try. Fail. Try again. Getting into the women's bathroom is a life goal!"
      quest_guide_hint = "Go to the women's bathroom again."
    class phase_22_paint:
      hint = "Time to find the Long Dong Silver of paints."
      quest_guide_hint = "Pick up the silver paint in the art classroom."
    class phase_23_statue:
      hint = "A statue of liberty outside the women's bathroom! Okay, it's more of a gargoyle..."
      quest_guide_hint = "Interact with the door to the women's bathroom."
    class phase_30_bathroom:
      hint = "Time to find those spider eggs. Leave no stone unturned."
      quest_guide_hint = "Search through 6 interactables in the women's bathroom."
    class phase_40_maxine:
      hint = "Got the picture — time to get myself a meeting."
      def quest_guide_hint(_quest):
        if "stuck" > quest.nurse_venting > "supplies":
          return "Finish \"Venting Frustration.\""
        else:
          return "Quest [maxine] in the clubroom."
    class phase_1000_done:
      description = "And that's how you get a meeting with [maxine]! Piece of cake."

  class Event_quest_maxine_eggs(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_clubroom" and not quest.maxine_eggs["first_clubroom"]:
        game.events_queue.append("quest_maxine_eggs_clubroom_arrival")
      if new_location == "school_computer_room" and not school_computer_room["welcome_back"]:
        game.events_queue.append("quest_maxine_eggs_computer_room_arrival")
      if new_location == "school_bathroom" and not school_bathroom["utter_disappointment"]:
        game.events_queue.append("quest_maxine_eggs_bathroom_arrival")
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.maxine_eggs == "bathroom":
        return 0
    def on_can_advance_time(event):
      if quest.maxine_eggs == "bathroom":
        return False
    def on_quest_guide_maxine_eggs(event):
      rv = []
      if quest.maxine_eggs == "cyberia":
        rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room screen1@.35"]))
        rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_screen_left", marker = ["actions quest"], marker_sector = "right_top", marker_offset = (30,150)))
      elif quest.maxine_eggs == "plugcy":
        if mc.owned_item("flash_drive"):
          rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room tablet@.6"]))
          rv.append(get_quest_guide_marker("school_computer_room","cyberia", marker = ["items flash_drive@.6"], marker_sector = "left_top", marker_offset = (110,80)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom flash_drive"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_flash_drive", marker = ["actions take"], marker_offset = (10,-20)))
      elif quest.maxine_eggs == "plugpc":
        rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room screen1@.35"]))
        rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_screen_left", marker = ["items flash_drive@.6"], marker_sector = "right_top", marker_offset = (30,150)))
      elif quest.maxine_eggs == "deliver":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        if maxine.at("school_clubroom"):
          rv.append(get_quest_guide_marker("school_clubroom","maxine", marker = ["actions quest"], marker_offset = (75,25)))
        else:
          if mc.at("school_clubroom"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
      elif quest.maxine_eggs == "recon":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_door_bathroom", marker = ["actions go"]))
      elif quest.maxine_eggs == "bust":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_door_bathroom", marker = ["actions go"]))
      elif quest.maxine_eggs == "paint":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_shelf", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (580,50)))
      elif quest.maxine_eggs == "statue":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_door_bathroom", marker = ["actions interact"]))
      elif quest.maxine_eggs == "bathroom":
        x = 6-quest.maxine_eggs["bathroom_interactables"]
        rv.append(get_quest_guide_hud("map", marker=("$Search through\n{color=#48F}" + str(x) + "{/} interactable." if x == 1 else "$Search through\n{color=#48F}" + str(x) + "{/} interactables."), marker_sector = "mid_top", marker_offset = (60,0)))
      elif quest.maxine_eggs == "maxine":
        if "stuck" > quest.nurse_venting > "supplies":
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Venting Frustration{/}."))
        else:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          if maxine.at("school_clubroom"):
            rv.append(get_quest_guide_marker("school_clubroom","maxine", marker = ["actions quest"], marker_offset = (75,25)))
          else:
            if mc.at("school_clubroom"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
      return rv

label quest_maxine_eggs_clubroom_arrival:
  "Well, depending on how you look at it, there might be treasures here. One man's trash, and all that."
  "I knew this room existed, but I thought it was off-limits for students."
  "This used to be the classroom for extracurricular activities. Most recently used by the school's literature club."
  "Apparently, several of the members killed themselves. One girl cut herself open on this very floor. Another one hanged herself at home."
  "Over the years, many rumors have circulated about what really happened in that club."
  "Some say the tragedy was fueled by jealousy. Others claim that an ancient curse will warp the minds of those who stay too long."
  "Of course, this was long before my time..."
  if maxine.at("school_clubroom"):
    show maxine annoyed with Dissolve(.5)
  else:
    show maxine annoyed at appear_from_left
  "Crap."
  maxine annoyed "Did you just grapple-hook your way in through the window?"
  mc "Sort of."
  maxine thinking "Without triggering the intruder-alarm?"
  mc "I guess."
  maxine angry "I knew the popcorn dispenser could only be trusted with popcorn!"
  $maxine['at_clubroom_now'] = True
  $quest.maxine_eggs["first_clubroom"] = True
  $process_event("update_state")
  hide maxine angry with Dissolve(.5)
  $quest.clubroom_access.finish()
  return

label quest_maxine_eggs_computer_room_arrival:
  show cyberia
  cyberia "Welcome back, [mc]."
  cyberia "You are expected."
  "God. Not this again. How could I forget about the annoying AI professor?"
  cyberia "Welcome back, [mc]."
  cyberia "In order to proceed, kindly greet me back."
  mc "Hi, [cyberia]..."
  cyberia "{i}Professor{/} [cyberia]."
  "Whoever programmed this abomination had a really poor sense of humor."
  cyberia "Welcome back, [mc]."
  mc "Hi, Professor [cyberia]."
  cyberia "And a kind greeting to you, pupil. Are you ready to learn?"
  cyberia "Learning is fun and productive. Let's have fun."
  mc "Whatever you say..."
  $school_computer_room["welcome_back"] = True
  return

label quest_maxine_eggs_bathroom_arrival:
  "During all my years at Newfall High, the women's bathroom has been shrouded in mystery."
  "Romanticized by its forbidden nature. The setting of so many fantasies."
  "..."
  "Turns out, it's nothing special. Practically just a copy of the men's bathroom."
  "Where are the forgotten panties? The sinks laden with makeup products? The lipstick kisses on the mirrors?"
  "What a complete and utter disappointment."
  "Maybe the place looks different bustling with giggling girls..."
  "In any case, better find those glowing spider eggs before the [guard] returns."
  $school_bathroom["utter_disappointment"] = True
  return
