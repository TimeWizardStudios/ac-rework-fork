init python:
  class Quest_jo_washed(Quest):

    title = "Washed Up"

    class phase_1_supplies:
      description = "One of the best days of my life..."
      hint = "If I want them wet, I gotta get working."
      def quest_guide_hint(quest):
        if game.location == "school_ground_floor" and not mc.owned_item("bottle_of_soap"):
          return "Go to the janitor's closet."
        elif game.location == "school_roof" and not mc.owned_item("garden_hose"):
          return "Take the garden hose, or quest [maxine]."
        elif game.location == "school_art_class" and not mc.owned_item("sponges"):
          return "Quest [jacklyn]."
        elif game.location == "school_gym" and not mc.owned_item("car_wash_sign"):
          return "Quest [kate], or take the car wash{space=-10}\nsigns."
        else:
          return "Acquire a hose, signs, soap, and sponges. (Optional: quest [flora] outside the school for guidance)"

    class phase_2_car_wash:
      hint = "Bae-watch."
      quest_guide_hint = "Quest [flora] outside the school."

    class phase_3_insecure:
      hint = "Mamma Mia! Where's [jo]?"
      def quest_guide_hint(quest):
        if quest["emotional_turmoil"]:
          return "Go to the women's bathroom."
        else:
          return "Go to the sports wing."

    class phase_4_fundraiser:
      hint = "Itsy-Bitsy-Teeny-Weeny Bikini!"
      quest_guide_hint = "Leave the school."

    class phase_5_fundraiser_over:
      pass

    class phase_1000_done:
      description = "Love and lust, all in one."


init python:
  class Event_quest_jo_washed(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if (((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.jo_washed.in_progress):
        return 0

    def on_can_advance_time(event):
      if (((quest.kate_stepping.finished or quest.isabelle_dethroning.finished) and not quest.jo_washed.started)
      or quest.jo_washed.in_progress):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.jo_washed == "supplies" and new_location == "school_ground_floor" and not quest.jo_washed["muted_honking"]:
        game.events_queue.append("quest_jo_washed_supplies_entrance_hall")
        quest.jo_washed["muted_honking"] = True
      if quest.jo_washed == "supplies" and new_location == "school_homeroom" and not quest.jo_washed["out_of_her_cardigan"]:
        game.events_queue.append("quest_jo_washed_supplies_homeroom")
        quest.jo_washed["out_of_her_cardigan"] = True
      if quest.jo_washed == "supplies" and new_location == "school_first_hall" and not quest.jo_washed["out_the_window"]:
        game.events_queue.append("quest_jo_washed_supplies_first_hall")
        quest.jo_washed["out_the_window"] = True
      if quest.jo_washed == "supplies" and new_location == "school_roof" and not quest.jo_washed["maxine_location"]:
        game.events_queue.append("quest_jo_washed_supplies_roof")
      if quest.jo_washed == "supplies" and old_location == "school_roof" and renpy.showing("maxine_dream","screens"):
        renpy.hide("maxine_dream","screens")
      if quest.jo_washed == "supplies" and new_location == "school_art_class" and not quest.jo_washed["jacklyn_location"]:
        game.events_queue.append("quest_jo_washed_supplies_art_class")
      if quest.jo_washed == "supplies" and old_location == "school_art_class" and renpy.showing("jacklyn_dream","screens"):
        renpy.hide("jacklyn_dream","screens")
      if quest.jo_washed == "supplies" and new_location == "school_gym" and not quest.jo_washed["kate_location"]:
        game.events_queue.append("quest_jo_washed_supplies_gym")
      if quest.jo_washed == "supplies" and old_location == "school_gym" and renpy.showing("kate_dream","screens") and renpy.showing("signs_dream","screens"):
        renpy.hide("kate_dream","screens")
        renpy.hide("signs_dream","screens")
      if quest.jo_washed == "car_wash" and new_location != "school_entrance" and not quest.jo_washed["just_on_time"]:
        game.events_queue.append("quest_jo_washed_car_wash_other_locations")
      if quest.jo_washed == "car_wash" and new_location == "school_entrance" and not quest.jo_washed["just_on_time"]:
        game.events_queue.append("quest_jo_washed_car_wash_outside")
        quest.jo_washed["just_on_time"] = True
      if quest.jo_washed == "insecure" and new_location == "school_ground_floor" and not quest.jo_washed["heroic_sacrifice"]:
        game.events_queue.append("quest_jo_washed_insecure_entrance_hall")
        quest.jo_washed["heroic_sacrifice"] = True
      if quest.jo_washed == "insecure" and new_location not in ("school_entrance","school_first_hall_east") and quest.jo_washed["heroic_sacrifice"] and not quest.jo_washed["emotional_turmoil"] and "quest_jo_washed_insecure_entrance_hall" not in game.events_queue:
        game.events_queue.append("quest_jo_washed_insecure_other_locations")
      if quest.jo_washed == "insecure" and new_location == "school_first_hall_east" and not quest.jo_washed["emotional_turmoil"]:
        game.events_queue.append("quest_jo_washed_insecure_sports_wing")
        quest.jo_washed["emotional_turmoil"] = True
      if quest.jo_washed == "insecure" and new_location == "school_bathroom":
        game.events_queue.append("quest_jo_washed_insecure_bathroom")
      if quest.jo_washed == "fundraiser" and new_location == "school_entrance":
        game.events_queue.append("quest_jo_washed_fundraiser")

    def on_quest_started(event,quest,silent=False):
      if quest.id == "jo_washed":
        mc["focus"] = "jo_washed"
        quest["car_wash_volunteers"] = set()
      if quest.id == "jo_washed" and school_first_hall["paint_splash"]:
        school_first_hall["paint_splash"] = False

    def on_investigate_shown(event,obj_id,description):
      if description.startswith("{#daydream}") and ("investigate", (obj_id, description)) in game.seen_actions:
        game.seen_actions.remove(("investigate", (obj_id, description)))

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "jo_washed" and quest.jo_washed == "insecure":
        quest.jo_washed["flora_location"] = quest.jo_washed["maxine_location"] = quest.jo_washed["jacklyn_location"] = quest.jo_washed["kate_location"] = None

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "jo_washed":
        mc["focus"] = ""

    def on_quest_guide_jo_washed(event):
      rv = []

      if quest.jo_washed == "supplies":
        if not mc.owned_item("garden_hose"):
          rv.append(get_quest_guide_path("school_roof", marker = ["school roof garden_hose@.5"]))
          rv.append(get_quest_guide_marker("school_roof", "school_roof_garden_hose", marker = ["actions take"]))
#         rv.append(get_quest_guide_marker("school_roof", "maxine", marker = ["$or{space=-50}","actions quest","${space=50}"], marker_offset = (0,0)))
          rv.append(get_quest_guide_marker("school_roof", "school_roof_garden_hose", marker = ["$or{space=-50}","actions quest","${space=50}"], marker_offset = (304,-248)))
        if not mc.owned_item("car_wash_sign"):
          if quest.jo_washed["kate_location"]:
            rv.append(get_quest_guide_path("school_gym", marker = ["school gym car_wash_signs1@.325"]))
          else:
            rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))
#         rv.append(get_quest_guide_marker("school_gym", "kate", marker = ["actions quest"], marker_offset = (55,0)))
          rv.append(get_quest_guide_marker("school_gym", "school_gym_hula_hoop", marker = ["actions quest"], marker_offset = (574,-387)))
#         rv.append(get_quest_guide_marker("school_gym", "school_gym_car_wash_signs", marker = ["$or{space=-50}","actions take","${space=50}"], marker_sector = "left_bottom", marker_offset = (265,0)))
          rv.append(get_quest_guide_marker("school_gym", "school_gym_hula_hoop", marker = ["$or{space=-50}","actions take","${space=50}"], marker_sector = "left_bottom", marker_offset = (868,-61)))
        if not mc.owned_item("bottle_of_soap"):
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_janitor_door", marker = ["actions go"], marker_offset = (-15,-20)))
        if not mc.owned_item("sponges"):
          if quest.jo_washed["jacklyn_location"]:
            rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
          else:
            rv.append(get_quest_guide_path("school_art_class", marker = ["actions go"]))
#         rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_chairs", marker = ["actions quest"], marker_offset = (-1176,-174)))

      elif quest.jo_washed == "car_wash":
        rv.append(get_quest_guide_path("school_entrance", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_entrance", "flora", marker = ["actions quest"], marker_offset = (-5,-10)))

      elif quest.jo_washed == "insecure":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_door_bathroom", marker = ["actions go"]))

      elif quest.jo_washed == "fundraiser":
        rv.append(get_quest_guide_path("school_entrance", marker = ["actions go"]))

      return rv
