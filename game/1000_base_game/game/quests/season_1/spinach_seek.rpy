init python:
  class Quest_spinach_seek(Quest):

    title = "Hide and Seek"

    class phase_1_bait:
      description = "Hide and seek! I guess they never seek, huh? You got a cat-friend, I bet she doesn't lick ya! Meow?"
      hint = "If only I had a pussy-magnet."
      quest_guide_hint = "Give [spinach] a ball of yarn."
    class phase_2_play:
      hint = ""
    class phase_1000_done:
      description = "Balls and pussy — a classic combination."

  class Event_quest_spinach_seek(GameEvent):
    def on_quest_guide_spinach_seek(event):
      rv=[] 
      if mc.owned_item("ball_of_yarn"):
        rv.append(get_quest_guide_path("school_clubroom", marker = ["spinach contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom","spinach", marker = ["items ball_of_yarn"]))
      else:
        rv.append(get_quest_guide_path("school_homeroom", marker = ["items ball_of_yarn"]))
        rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker = ["actions take"], marker_offset = (70,-30)))
      return rv


label spinach_quest_hide_seek_start:
  spinach "Mewww..."
  "There's a look of guilt on her tiny feline face."
  mc "Did you not use your litter box?"
  spinach "..."
  "Maybe she stole something..."
  mc "Come here, kitty kitty!"
  spinach "Meep!"
  "She's not budging. Going to need something to lure her out."
  if not quest.spinach_seek.in_progress:
    $quest.spinach_seek.start()
  return

label spinach_quest_hide_seek_yarn(item):
  if item=="ball_of_yarn":
    $mc.remove_item(item)
    show spinach hearteyed with moveinright
    spinach hearteyed "Meow!"
    $spinach['playing_today'] = True
    $quest.spinach_seek.advance("play")
    $process_event("update_state")
    hide spinach with moveoutleft
    "That worked surprisingly well."
    "Now, let's see what's under here..."
    $mc.add_item("spray_cap")
    $quest.spinach_seek.finish()
  else:
    "I probably shouldn't let [spinach] dig her claws into my [item.title_lower]."
    $quest.spinach_seek.failed_item("spinach_yarn",item) 
  return
