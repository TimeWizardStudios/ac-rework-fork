init python:
  class Quest_jacklyn_art_focus(Quest):

    title = "Art Through Suffering"

    class phase_1_canvas:
      description = "The first stroke is always the hardest."
      hint = "Self-doubt is the artist's greatest foe. Don't think, just murder the canvas."
      quest_guide_hint = "Interact with the farthest easel in the art classroom."

    class phase_2_paint:
      hint = "What would life be in black and white? Colorless, that's what."
      quest_guide_hint = "Interact with the paint shelf in the art classroom."

    class phase_3_brush:
      hint = "Like a musketeer needs his musket. An artist needs his sword."
      quest_guide_hint = "Take the brush on the floor of the art classroom."

    class phase_4_artist:
      hint = "Death can be very pretty, and art is all about execution."
      def quest_guide_hint(quest):
        item = mc.owned_item(("beaver_carcass","beaver_pelt","dish_brush","mrsl_brooch","nurse_panties","pad"))
        if item:
          return ("Use the " + items_by_id[item].title().lower() + " on the farthest easel in the art classroom.").replace("the mrs. lichtenstein","Mrs. L").replace("nurse","Nurse")
        else:
          if quest["try_again"]:
            return "Play through \"Fully Booked\" to acquire a beaver carcass, then use it on the farthest easel in the art classroom."
          else:
            return "Use the paint brush on the farthest easel in the art classroom."

    class phase_5_showjacklyn:
      hint = "What is a masterpiece without an audience? Just pigments on a canvas."
      quest_guide_hint = "Quest [jacklyn] in the art classroom."

    class phase_1000_done:
      description = "I'm the new Van Gogh,\ngirl problems and all."

    class phase_1001_done_nofocus:
      description = "Press F to art."


init python:
  class Event_quest_jacklyn_art_focus(GameEvent):

    def on_inventory_changed(event,char,item,old_count,new_count,silent=False):
      if quest.jacklyn_art_focus == "brush":
        if item.id in ("beaver_carcass","beaver_pelt","dish_brush","mrsl_brooch","nurse_panties","pad") and new_count > old_count:
          quest.jacklyn_art_focus.advance("artist")

    def on_quest_guide_jacklyn_art_focus(event):
      rv = []

      if quest.jacklyn_art_focus == "canvas":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class easel_3@.4"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_easel_03", marker = "actions interact"))

      elif quest.jacklyn_art_focus == "paint":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_shelf", marker = ["actions interact"], marker_offset = (600,60), marker_sector = "left_bottom"))

      elif quest.jacklyn_art_focus == "brush":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class brush@.75"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_brush", marker = ["actions take"], marker_sector = "right_bottom", marker_offset = (-10,0)))

      elif quest.jacklyn_art_focus == "artist":
        item = mc.owned_item(("beaver_carcass","beaver_pelt","dish_brush","mrsl_brooch","nurse_panties","pad"))
        if not (quest.jacklyn_art_focus["try_again"] and not mc.owned_item("beaver_carcass")):
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class easel_3@.4"]))
        if item:
          rv.append(get_quest_guide_marker("school_art_class","school_art_class_easel_03", marker = [items_by_id[item].icon+("@.8" if item in ("dish_brush","mrsl_brooch","nurse_panties","pad") else "")], marker_offset = (15,0)))
        else:
          if quest.jacklyn_art_focus["try_again"]:
            if mc.owned_item("beaver_carcass"):
              rv.append(get_quest_guide_marker("school_art_class","school_art_class_easel_03", marker = ["items beaver_carcass"]))
            else:
              if quest.lindsey_book.in_progress:
                rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Fully Booked{/}."))
              else:
                rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
                rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
          else:
            rv.append(get_quest_guide_marker("school_art_class","school_art_class_easel_03", marker = ["items brush"]))

      elif quest.jacklyn_art_focus == "showjacklyn":
        rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
        if jacklyn.at("school_art_class","standing"):
          rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
        else:
          if mc.at("school_art_class"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))

      return rv
