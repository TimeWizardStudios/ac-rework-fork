init python:
  class Quest_clubroom_access(Quest):
    title = "Oxygen For Oxymoron"
    class phase_1_shoes:
      description = "Look! Up in the vent! Is it\na bird? Is it a plane?"
      def hint(quest):
        if mc.owned_item("monkey_wrench"):
          return "Construct. Climb. Ape around."
        else:
          return "To avoid suffocation, find the steel monkey."
      def quest_guide_hint(quest):
        if mc.owned_item("monkey_wrench"):
          return "Make a pile of shoes and climb to the vent."
        else:
          return "Extract the monkey wrench from \"Bayonets & Etiquettes\" in the homeroom."
    class phase_2_vent:
      hint = "The monkey's paw is a dangerous artifact. The monkey wrench, on the other hand..."
      quest_guide_hint = "Open the vent with the monkey wrench."
    class phase_3_grapplinghook:
      hint = "The place that smells like a seedy strip club. Bring the grappling hooker there."
      def quest_guide_hint(quest):
        if school_homeroom["open_window"]:
          if mc.owned_item("grappling_hook"):
            return "Attach the grappling hook to the homeroom window."
          else:
            return "Climb the rope."
        else:
          if not mc.owned_item("globe") and not mc.owned_item("greasy_bolt") and not mc.owned_item("bolt"):
            return "Dismantle the globe with the monkey wrench."
          if mc.owned_item("globe") and not mc.owned_item(("greasy_bolt","bolt")):
            return "Throw the globe in the basketball hoop. (Optional: score a three-pointer)"
          if mc.owned_item("greasy_bolt") and not mc.owned_item("tissues"):
            return "Grab the box of tissues from your bathroom."
          if mc.owned_item("greasy_bolt") and mc.owned_item("tissues"):
            return "Combine the greasy bolt with a tissue."
          if mc.owned_item("bolt"):
            return "Jam the homeroom window's lock mechanism with the bolt."
    class phase_1000_done:
      description = "Like breaking into the home and stealing candy from a child."

  class Event_quest_clubroom_access(GameEvent):
    def on_quest_guide_clubroom_access(event):
      rv = []
      if quest.clubroom_access == "shoes":
        if mc.owned_item("monkey_wrench"):
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east pileofshoes17"]))
          if not school_first_hall_east["shoe1"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes1", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe2"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes2", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe3"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes3", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe4"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes4", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe5"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes5", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe6"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes6", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe7"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes7", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe8"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes8", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe9"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes9", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe10"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes10", marker = ["actions interact"]))
          elif not school_first_hall_east["shoe11"]:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes11", marker = ["actions interact"]))
        else:
          if mc.owned_item("bayonets_etiquettes"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items book bayonets_etiquettes"]))
          else:
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom closet@.2"]))
            rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_bookshelf", marker = ["actions interact"]))
      elif quest.clubroom_access == "vent":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.5"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["items monkey_wrench"], marker_sector="right_top", marker_offset = (0,-220)))
      elif quest.clubroom_access == "grapplinghook":
        if school_homeroom["open_window"]:
          if mc.owned_item("grappling_hook"):
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom window_2@.3"]))
            rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_window", marker = ["items grappling_hook"], marker_sector = "left_top", marker_offset = (200,200)))
          else:
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom rope@.4"]))
            rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_window", marker = ["actions go"], marker_offset = (200,200)))
        else:
          if not mc.owned_item("globe") and not mc.owned_item("greasy_bolt") and not mc.owned_item("bolt"):
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom globe@.5"]))
            rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_globe", marker = ["items monkey_wrench@.9"]))
          if mc.owned_item("globe") and not mc.owned_item(("greasy_bolt","bolt")):
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom basket@.5"]))
            rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_basketball_hoop", marker = ["items globe"]))
          if mc.owned_item("greasy_bolt") and not mc.owned_item("tissues"):
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom tissues@.5"]))
            rv.append(get_quest_guide_marker("home_bathroom","home_bathroom_tissues", marker = ["actions take"]))
          if mc.owned_item("greasy_bolt") and mc.owned_item("tissues"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items greasy_bolt","actions combine","items tissues@.9"]))
          if mc.owned_item("bolt"):
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom window_2@.3"]))
            rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_window", marker = ["items clean_bolt"], marker_sector = "left_top", marker_offset = (200,200)))
      return rv
