init python:
  class Quest_lindsey_making_up(Quest):
    title = "[lindsey]'s Mercy"
    class phase_1_start:
      description = "Apologise to [lindsey]? But she started it!"
      hint = "I read somewhere that girls like gifts, and [lindsey]'s a typical girly girl. Might be a good place to start."
    class phase_2_second_item:
      hint="One's good, but two's the charm. At least when it comes to gifts."
    class phase_1000_done:
      description = "Wounds healed. Bridges repaired. Relationships mended."
    class phase_2000_failed:
      description = "Making enemies again. It's good to be back."
      
      
  class Event_quest_lindsey_making_up(GameEvent):
    def on_quest_guide_lindsey_making_up(event):
      rv=[] 
      if quest.lindsey_making_up in ("start","second_item"):
        rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "lindsey", marker=["items garlic","actions give","items lollipop_1"]))
      return rv
