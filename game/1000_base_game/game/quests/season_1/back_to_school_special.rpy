init python:
  class Quest_back_to_school_special(Quest):
    ## 1-1, Act 0, Scene 6, McQuestOne06
    ## 1-1, Act 0, Scene 6, McQuestOne07
    ## 1-1, Act 0, Scene 6, McQuestOne08
    ## 1-1, Act 0, Scene 6, McQuestOne09
    title="Back to School Special"
    class phase_1_back_to_school_special:
      description="Time to face those demons. And teachers. And everyone else at school. Again."
      hint="There's still a chance that this is all a bad dream."
    class phase_2_talk_to_jo:
      hint="There's only one front door. It's right next to those jars with stuffed animals. Yeah, the toad and the Chernobyl bunny."
    class phase_3_talk_to_flora:
      hint="There's only one front door. It's right next to those jars with stuffed animals. Yeah, the toad and the Chernobyl bunny."
    class phase_4_ready_to_leave:
      hint="There's only one front door. It's right next to those jars with stuffed animals. Yeah, the toad and the Chernobyl bunny."
      quest_guide_hint = "Click the kitchen door."
    class phase_1000_done:
      description="New year, new me."

  class Event_quest_back_to_school_special(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.back_to_school_special.in_progress:
        if quest.back_to_school_special=="talk_to_jo" and new_location=="home_kitchen":
          game.quest_guide="back_to_school_special"

    def on_quest_guide_back_to_school_special(event):
      rv=[]

      if quest.back_to_school_special=="back_to_school_special":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
      elif quest.back_to_school_special=="talk_to_jo":  
        rv.append(get_quest_guide_char("jo", marker = ["actions quest"], force_marker = True))
        rv.append(get_quest_guide_hud("map", marker_offset = (65,0), marker_sector = "mid_top", marker="$Press the {color=#48F}X{/} button for {color=#48F}X-Ray Mode{/}"))
      elif quest.back_to_school_special=="talk_to_flora":
        rv.append(get_quest_guide_char("flora", marker = ["actions quest"], force_marker = True))
        rv.append(get_quest_guide_hud("map", marker_offset = (65,0), marker_sector = "mid_top", marker="$Press the {color=#48F}X{/} button for {color=#48F}X-Ray Mode{/}"))
      elif quest.back_to_school_special=="ready_to_leave":
        rv.append(get_quest_guide_path("school_entrance", marker = ["mrsl contact_icon@.85"]))

      return rv
