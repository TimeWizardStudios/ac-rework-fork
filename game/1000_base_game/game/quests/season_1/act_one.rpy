init python:
  class Quest_act_one(Quest):

    title = "Unstarted Quests"

    class phase_1_start:
      pass

    class phase_1000_done:
      description = "I've done it."

    class phase_2000_failed:
      description = "I'm done for."


init python:
  class Event_quest_act_one(GameEvent):

    def on_location_changed(event,old_location,new_location,silent=False):
      if not quest.kate_blowjob_dream == "school":
        if new_location == home_bedroom and quest.jacklyn_art_focus.ended:
          if not "jacklyn" in game.pc.phone_contacts:
            game.events_queue.append("jacklyn_contact_second_chance")
        if game.season == 1:
          if new_location == "school_gym" and quest.isabelle_tour.finished and quest.kate_search_for_nurse.finished and quest.maxine_eggs.finished and not quest.kate_fate.started and not mc["focus"]:
            school_gym["exclusive"] = True ## This makes sure no unwanted free roam sprites are shown at the start
            game.events_queue.append("quest_kate_fate_start") ## Start trigger for Twisted Fate

    def on_can_advance_time(event):
      if not quest.day1_take2.finished:
        return False

    def on_quest_guide_act_one(event):
      rv=[]

      if mc["focus"]:
        rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}"+(quests_by_id[mc["focus"].split("@")[1]].title if "@" in mc["focus"] else quests_by_id[mc["focus"]].title)+"{/}."))

      else:
        if not quest.the_key.started:
          rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "mrsl", marker = ["actions quest"]))
          return rv

        ## Fully Booked
        if not quest.lindsey_book.started:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))

        ## Chili Con Carnal Sin
        if not quest.flora_cooking_chilli.started:
          if flora.at("school_*"):
            rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
          else:
            rv.append(get_quest_guide_path("school_homeroom", marker = ["flora contact_icon@.85"]))
            if mc.at("school_homeroom"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for [flora] {color=#48F} 1:00 PM{/}"))

        if quest.isabelle_tour > "art_class":

          ## Punked
          if not quest.flora_jacklyn_introduction.started:
            if flora.at("home_kitchen"):
              rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
              rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"]))
            else:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
              if mc.at("home_kitchen"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} at "+("{color=#48F}18:00{/}." if persistent.time_format == '24h' else "{color=#48F}6:00 PM{/}.")))

          ## A Short Fuse & Art Through Suffering
          if not quest.jacklyn_broken_fuse.started or not quest.jacklyn_art_focus.started:
            if jacklyn.at("school_art_class"):
              rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
              rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
            else:
              rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
              if mc.at("school_art_class"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for [jacklyn] {color=#48F} 1:00 PM{/}"))

        if quest.isabelle_tour > "gym":

          ## Search & Rescue
          if not quest.kate_search_for_nurse.started:
            if kate.at("school_nurse_room", "sitting"):
              rv.append(get_quest_guide_path("school_nurse_room", marker = ["kate contact_icon@.85"]))
              rv.append(get_quest_guide_marker("school_nurse_room", "kate", marker = ["actions quest"]))
            else:
              rv.append(get_quest_guide_path("school_nurse_room", marker = ["kate contact_icon@.85"]))
              if mc.at("school_nurse_room"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for [kate] {color=#48F} 1:00 PM{/}"))

          ## The Missing Tapes
          if not quest.mrsl_HOT.started:
            if mrsl.at("school_gym", "sitting"):
              rv.append(get_quest_guide_path("school_gym", marker = ["mrsl contact_icon@.85"]))
              rv.append(get_quest_guide_marker("school_gym", "mrsl", marker = ["actions quest"]))
            else:
              rv.append(get_quest_guide_path("school_gym", marker = ["mrsl contact_icon@.85"]))
              if mc.at("school_gym"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for [mrsl] {color=#48F} 1:00 PM{/}"))

          ## No True Scotswoman
          if not quest.isabelle_haggis.started:
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["items haggis_lunchbox@.8"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_counter_left", marker = ["actions quest"], marker_offset = (90,100), marker_sector = "left_top"))

        else:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

        ## Stolen Hearts
        if quest.isabelle_tour.finished and not quest.isabelle_stolen.started:
          if quest.jacklyn_sweets.in_progress:
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}King of Sweets{/}."))
          else:
            rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

        ## Operation B.O.B.
        if quest.lindsey_book.finished:
          if not quest.berb_fight.started:
            if berb.at("school_entrance"):
              rv.append(get_quest_guide_path("school_entrance", marker = ["berb contact_icon@.85"]))
              rv.append(get_quest_guide_marker("school_entrance", "berb", marker = ["actions quest"]))
            else:
              rv.append(get_quest_guide_path("school_entrance", marker = ["berb contact_icon@.85"]))
              if mc.at("school_entrance"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for [berb]"))

        if quest.berb_fight.finished:

          ## Potted Weeds
          if not quest.jo_potted.started:
            rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "jo", marker = ["actions quest"]))

          ## Buried Truths
          if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and not quest.isabelle_buried.started and not isabelle["romance_disabled"]:
            if quest.maxine_hook.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Hooking Up{/}."))
            else:
              rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

        ## The Statement
        if quest.flora_jacklyn_introduction.finished:
          if not quest.jacklyn_statement.started and "jacklyn" in game.pc.phone_contacts:
            rv.append(get_quest_guide_hud("phone",marker=["jacklyn contact","phone apps contact_info text@0.5","$Text Jacklyn"]))

        ## Nothing Wrong With Me
        if quest.isabelle_tour.ended and quest.lindsey_book.ended:
          if not quest.lindsey_wrong.started: # For free version do a modal.
            rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))

        ## Oxygen for Oxymoron
        if not quest.clubroom_access.started:
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.4"]))
          rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["actions quest"], marker_sector="right_top", marker_offset = (0,-220)))

        ## Hide and Seek
        if quest.clubroom_access.finished and not quest.kate_search_for_nurse.in_progress and not quest.spinach_seek.started:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["spinach contact_icon@.85"]))
          if spinach.at("school_clubroom"):
            rv.append(get_quest_guide_marker("school_clubroom","spinach", marker = ["actions quest"]))
          else:
            if mc.at("school_clubroom"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[spinach]{/} to come back."))

        ## Tiny Thirsty Tree
        if quest.flora_jacklyn_introduction.finished and not quest.flora_bonsai.started:
          if mc.owned_item(((((("spray_banana_milk","spray_pepelepsi","spray_salted_cola","spray_seven_hp","spray_strawberry_juice","spray_urine","spray_water","spray_empty_bottle")))))):
            if mc.owned_item("spray_empty_bottle"):
              rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"] ))
              rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["items bottle spray_empty_bottle@.75"], marker_sector="right_top", marker_offset = (50,125)))
            for item in ["spray_banana_milk","spray_pepelepsi","spray_salted_cola","spray_seven_hp","spray_strawberry_juice","spray_urine","spray_water"]:
              if mc.owned_item(item):
                rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room bonsai@.5"]))
                rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_bonsai", marker = items_by_id[item].icon))
          elif mc.owned_item("spray_cap"):#spraycap combine
            for item in ["banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle","empty_bottle"]:
              if mc.owned_item(item):
                rv.append(get_quest_guide_hud("inventory", marker = [items_by_id[item].icon, "actions combine", "items bottle spray_cap"]))
            else:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.8"]))
              rv.append(get_quest_guide_marker("home_kitchen","kitchen_water_bottle", marker = ["actions take"]))

        ## Poolside Story
        if not quest.poolside_story.started: # For free version do a modal.
          rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "mrsl", marker = ["actions quest"]))

        ## Strawberries, Cherries & The Glowing Spider Eggs
        if quest.clubroom_access.finished and (not quest.maxine_wine.started or not quest.maxine_eggs.started):
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          if maxine.at("school_clubroom","sitting"):
            rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
          else:
            if mc.at("school_clubroom") and not maxine.at("school_clubroom"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))

        ## Table Manners
        if quest.isabelle_tour.ended and quest.kate_blowjob_dream.ended and quest.poolside_story.ended and not quest.mrsl_table.started:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["mrsl contact_icon@.85"]))
          if mrsl.at("school_homeroom","sitting"):
            rv.append(get_quest_guide_marker("school_homeroom","mrsl", marker = ["actions quest"]))
          else:
            if mc.at("school_homeroom"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[mrsl]{/} between\n"+("{color=#48F}7:00{/} — {color=#48F}10:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 AM{/} — {color=#48F}10:00 AM{/}.")))

        ## The Ley of the Land
        if quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.clubroom_access.finished and not quest.maxine_lines.started:
          rv.append(get_quest_guide_path("school_gym", marker = ["maxine contact_icon@.85"]))
          if maxine.at("school_gym"):
            rv.append(get_quest_guide_marker("school_gym", "maxine", marker = ["actions quest"], marker_offset = (40,-10)))
          else:
            if mc.at("school_gym"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} at "+("{color=#48F}18:00{/}." if persistent.time_format == '24h' else "{color=#48F}6:00 PM{/}.")))

        ## Twisted Fate
        if quest.isabelle_tour.finished and quest.maxine_eggs.finished and quest.kate_search_for_nurse.finished and not quest.kate_fate.started:
          rv.append(get_quest_guide_path("school_gym", marker = ["kate contact_icon@.85"]))

        ## Gathering Storm
        if quest.kate_search_for_nurse.finished and quest.spinach_seek.finished: ## All other quests featuring Spinach must have been already finished, since Gathering Storm puts her in the MC's room indefinitely
          if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and quest.clubroom_access.finished and not quest.isabelle_locker.started and not isabelle["romance_disabled"]:
            if quest.isabelle_buried["done_today"]:
              rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}tomorrow{/}."))
            else:
              rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

        ## Twisted Desire
        if quest.isabelle_tour.finished and quest.kate_search_for_nurse.finished and quest.kate_fate.finished and not quest.kate_desire.started:
          rv.append(get_quest_guide_path("school_gym", marker = ["kate contact_icon@.85"]))
          if kate.at("school_gym"):
            rv.append(get_quest_guide_marker("school_gym", "kate", marker = ["actions quest"], marker_offset = (50,25)))
          else:
            if mc.at("school_gym"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[kate]{/} at "+("{color=#48F}17:00{/}." if persistent.time_format == '24h' else "{color=#48F}5:00 PM{/}.")))

        ## Hooking Up
        if quest.isabelle_tour.finished and quest.berb_fight.finished and quest.clubroom_access.finished and quest.maxine_eggs.finished and quest.maxine_lines.finished and not quest.maxine_hook.started:
          if quest.isabelle_buried.in_progress:
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Buried Truths{/}."))
          else:
            rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "maxine", marker = ["actions quest"]))

        ## King of Sweets
        if quest.isabelle_tour.finished and quest.jacklyn_art_focus.finished and quest.jacklyn_statement.finished and not quest.jacklyn_sweets.started:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["guard contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions quest"], marker_sector = "right_top", marker_offset = (70,170)))

        ## Chops and Nocs
        if ((quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.finished)) and not quest.isabelle_piano.started)
        or (quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and not quest.isabelle_piano.started)):
          if quest.piano_tuning.in_progress:
            if quest.piano_tuning == "cabinet":
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
            elif quest.piano_tuning == "baton":
              if school_music_class["got_conductor_baton"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
            elif quest.piano_tuning == "hammer":
              if school_first_hall_west["tuning_hammer"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                if mc.owned_item("tuning_hammer"):
                  rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                  rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
                else:
                  if school_music_class["middle_compartment"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            elif quest.piano_tuning == "fork":
              if school_music_class["bottom_compartment"]:
                if school_music_class["middle_compartment"]:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                else:
                  if school_music_class["top_compartment"] == "tuning_fork":
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  elif school_music_class["top_compartment"] == "conductor_baton":
                    if school_first_hall_west["tuning_fork"]:
                      rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                      rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                    else:
                      rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                      rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
              else:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
          else:
            rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
            if quest.piano_tuning.finished:
              rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\nsweet and romantic{space=-50}","actions quest","${space=275}"], marker_offset = (150,250)))
            else:
              rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["actions quest"], marker_offset = (150,250)))

        ## Squid Game
        if quest.isabelle_tour.finished and quest.berb_fight.finished and quest.flora_bonsai.finished and quest.jacklyn_statement.finished and quest.maxine_eggs.finished and not quest.flora_squid.started:
          rv.append(get_quest_guide_path("home_hall", marker = ["flora contact_icon@.85"]))
          if game.hour == 20:
            rv.append(get_quest_guide_marker("home_hall", "home_hall_door_flora", marker = ["actions quest"]))
          else:
            if mc.at("home_hall"):
              if game.hour in (21,22,23):
                rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}tomorrow{/}."))
              else:
                rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}[flora]{/} goes to \nher room at "+("{color=#48F}20:00{/}." if persistent.time_format == '24h' else "{color=#48F}8:00 PM{/}.")))

        ## Dead Girl's Score
        if quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.lindsey_wrong.finished and not lindsey["romance_disabled"] and not quest.lindsey_piano.started:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "lindsey", marker = ["actions quest"]))
          if quest.piano_tuning.finished or quest.lindsey_piano["failed"]:
            rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
            rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\nfast and exciting{space=-50}","actions quest","${space=250}"], marker_offset = (150,250)))

        ## Paint It Red
        if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access.finished and not quest.isabelle_red.started:
          rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
          if jacklyn.at("school_art_class","standing"):
            rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
          else:
            if mc.at("school_art_class"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))

        ## Photogenic
        if quest.isabelle_tour.finished and not quest.nurse_photogenic.started:
          if quest.nurse_photogenic["strawberry_juice_consumed_today"]:
            rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor main_stairs@.3"]))
            rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_stairs", marker = ["actions go"], marker_sector="mid_bottom", marker_offset = (220,0)))
          else:
            if mc.owned_item(("banana_milk","spray_banana_milk","salted_cola","spray_salted_cola","pepelepsi","spray_pepelepsi","seven_hp","spray_seven_hp","strawberry_juice","spray_strawberry_juice","spray_urine","water_bottle","spray_water")):
              x = mc.owned_item(("banana_milk","spray_banana_milk","salted_cola","spray_salted_cola","pepelepsi","spray_pepelepsi","seven_hp","spray_seven_hp","strawberry_juice","spray_strawberry_juice","spray_urine","water_bottle","spray_water"))
              rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x,"actions consume@.85"], marker_sector = "mid_top", marker_offset = (60,0)))
            elif mc.owned_item(("empty_bottle","spray_empty_bottle")):
              x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
              rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
              rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["fill_"+x+"bottle_with_strawberry_juice"], marker_sector="right_top", marker_offset = (50,125)))
            else:
              if home_kitchen["water_bottle_taken"]:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
              else:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
                rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))

        ## Stainless Steal
        if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and not quest.flora_handcuffs.started:
          rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))

        ## Venting Frustration
        if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access.finished and not quest.nurse_venting.started:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          if maxine.at("school_clubroom","fishing"):
            rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (65,15)))
          else:
            if mc.at("school_clubroom") and not maxine.at("school_clubroom"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))

        ## Hurricane Isabelle
        if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and quest.isabelle_locker["time_for_a_change"]) and not quest.isabelle_hurricane.started and not isabelle["romance_disabled"]:
          if quest.isabelle_locker["finished_today"]:
            rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}tomorrow{/}."))
          else:
            rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

        ## A Beautiful Motive
        if quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.lindsey_wrong.finished and quest.lindsey_piano.finished and quest.jacklyn_broken_fuse.finished and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and quest.maxine_eggs.finished and not quest.lindsey_motive.started:
          rv.append(get_quest_guide_path("school_gym", marker = ["lindsey contact_icon@.85"]))
          if lindsey.at("school_gym"):
            rv.append(get_quest_guide_marker("school_gym", "lindsey", marker = ["actions quest"], marker_offset = (60,0)))
          else:
            if mc.at("school_gym"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[lindsey]{/} "+((("at {color=#48F}9:00{/}." if persistent.time_format == "24h" else "at {color=#48F}9:00 AM{/}.") if game.hour == 8 else ("at {color=#48F}11:00{/}." if persistent.time_format == "24h" else "at {color=#48F}11:00 AM{/}.")) if game.hour in (8,10) else "tomorrow.")))

        ## Repeatable Sex Scene: Music Room Masturbation
        if quest.isabelle_tour.finished and quest.jacklyn_art_focus.finished and quest.jacklyn_broken_fuse.finished and not school_music_class["masturbation_jacklyn"]:
          rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class michael_jackson_statue@.2"]))
          rv.append(get_quest_guide_marker("school_music_class", "school_music_class_statue", marker = ["actions flirt"], marker_offset = (60,0)))

        ## Repeatable Sex Scene: Isabelle Roof Kiss & Sex
        if quest.isabelle_tour.finished and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.finished)) and not school_roof["isabelle_kiss"]:
          if isabelle["roof"]:
            rv.append(get_quest_guide_path("school_roof", marker = ["isabelle contact_icon@.85"]))
            if isabelle.at("school_roof"):
              rv.append(get_quest_guide_marker("school_roof", "isabelle", marker = ["actions flirt"], marker_offset = (75,-10)))
            else:
              if mc.at("school_roof"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[isabelle]{/} at "+("{color=#48F}18:00{/}." if persistent.time_format == '24h' else "{color=#48F}6:00 PM{/}.")))
          else:
            rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions flirt"]))

        ## Jo's Day
        if quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.berb_fight.finished and quest.jo_potted.finished and not quest.jo_day.started:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["jo contact_icon@.85"]))
          if jo.at("school_cafeteria"):
            rv.append(get_quest_guide_marker("school_cafeteria", "jo", marker = ["actions quest"]))
          else:
            if mc.at("school_cafeteria"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} between\n"+("{color=#48F}9:00{/} — {color=#48F}17:00{/}." if persistent.time_format == '24h' else "{color=#48F}9:00 AM{/} — {color=#48F}5:00 PM{/}.")))

        ## The Dog Trick
        if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and sum([mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills"),mc.owned_item("handcuffs")]) >= 2 and not quest.kate_trick.started:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["actions go"]))

        ## Stepping on the Rose
        if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and quest.maxine_eggs.finished and quest.isabelle_piano.finished and quest.nurse_photogenic.finished and quest.flora_handcuffs.finished and quest.kate_trick.finished and not quest.kate_stepping.started:
          rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "kate", marker = ["actions quest"]))

        ## Wicked Game
        if quest.isabelle_tour.finished and not quest.kate_wicked.started:
          if quest.piano_tuning.in_progress:
            if quest.piano_tuning == "cabinet":
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
            elif quest.piano_tuning == "baton":
              if school_music_class["got_conductor_baton"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
            elif quest.piano_tuning == "hammer":
              if school_first_hall_west["tuning_hammer"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                if mc.owned_item("tuning_hammer"):
                  rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                  rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
                else:
                  if school_music_class["middle_compartment"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            elif quest.piano_tuning == "fork":
              if school_music_class["bottom_compartment"]:
                if school_music_class["middle_compartment"]:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                else:
                  if school_music_class["top_compartment"] == "tuning_fork":
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  elif school_music_class["top_compartment"] == "conductor_baton":
                    if school_first_hall_west["tuning_fork"]:
                      rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                      rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                    else:
                      rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                      rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
              else:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
          else:
            rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
            if quest.piano_tuning.finished:
              rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\ndark and dramatic{space=-50}","actions quest","${space=275}"], marker_offset = (150,250)))
            else:
              rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["actions quest"], marker_offset = (150,250)))

        ## Dethroning the Queen
        if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.maxine_eggs.finished and quest.isabelle_red.finished and quest.nurse_venting.finished and quest.kate_wicked.finished and not quest.isabelle_dethroning.started:
          if mc.owned_item("pig_mask"):
            rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))
          elif home_computer["pig_mask_received"]:
            rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
          elif home_computer["pig_mask_ordered_today"]:
            rv.append(get_quest_guide_hud("time", marker="$Wait a day."))
          elif home_computer["pig_mask_ordered"]:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.7"]))
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_pig_mask", marker = ["actions take"]))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
            rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions costume_shop"]))

      if not rv:
        if len(get_available_quest_guides()) > 2:
          for quest_title,quest_id in get_available_quest_guides():
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}"+quest_title+"{/}."))
        else:
          rv.append(get_quest_guide_hud("quest_guide", marker="$\n\n\n\n\n\n\nThere are no more quests available at this time!\nThis likely means you've reached the end of the\ncurrent release.\n\nHowever, some quests have prerequisites which\nmight've not been met in this playthrough.\nCheck out our {color=#48F}Chapter Select{/} screen to make sure\nyou're not missing out on any of them!"))

      return rv
