init python:
  class Quest_mrsl_table(Quest):

    title = "Table Manners"

    class phase_1_letter:
      description = "Etiquette and other fun activities."
      hint = "Time to play mailman... the classic porno opening."
      quest_guide_hint = "Quest [jo] in the kitchen at home after the school day ends."
    class phase_2_laundry:
      hint = "A clean bathroom is a clean conscience."
      quest_guide_hint = "Clean up the mess in the bathroom at home."
    class phase_3_flora:
      hint = "On today's bucket list: annoy [flora]."
      quest_guide_hint = "Quest [flora] in the hallway at home."
    class phase_4_sleep:
      hint = "Bed-wit, the poor man's table manners."
      quest_guide_hint = "Go to sleep."
    class phase_5_morning:
      hint = "The bell already rang! Getting to class is paramount."
      quest_guide_hint = "Go to school."
    class phase_6_hide:
      hint = "Hide, for goodness sake! Save yourself!"
      quest_guide_hint = "Interact with the teacher's desk in the homeroom."
    class phase_1000_done:
      description = "A whole new world. A different point of view."


  class Event_quest_mrsl_table(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.mrsl_table == "letter" and new_location == "school_cafeteria" and not quest.mrsl_table["cafeteria_checked"]:
        game.events_queue.append("quest_mrsl_table_cafeteria_check")
      if quest.mrsl_table == "laundry" and new_location == "home_bathroom" and not quest.mrsl_table["mess_seen"] and not mc["focus"]:
        game.events_queue.append("quest_mrsl_table_mess")
      if quest.mrsl_table == "morning" and new_location == "home_kitchen":
        game.events_queue.append("quest_mrsl_table_totally_bawling")
      if quest.mrsl_table == "morning" and new_location == "school_ground_floor":
        game.events_queue.append("quest_mrsl_table_running_late")

    def on_energy_cost(event,energy_cost,silent=False):
      if quest.mrsl_table in ("laundry","morning","hide") and quest.mrsl_table["mess_seen"]:
        return 0

    def on_can_advance_time(event):
      if quest.mrsl_table in ("laundry","morning","hide") and quest.mrsl_table["mess_seen"]:
        return False

    def on_advance(event):
      if quest.mrsl_table == "sleep" and quest.mrsl_table["dream_sequence"] and not mc["focus"]:
        game.events_queue.append("quest_mrsl_table_sleep")

    def on_quest_guide_mrsl_table(event):
      rv = []

      if quest.mrsl_table == "letter":
        if mc.at("home_kitchen"):
          if jo.at("home_kitchen"):
            rv.append(get_quest_guide_marker("home_kitchen", "jo", marker = ["actions quest"]))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} to come\nback home at "+("{color=#48F}18:00{/}." if persistent.time_format == '24h' else "{color=#48F}6:00 PM{/}.")))
        else:
          if not quest.mrsl_table["cafeteria_checked"]:
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["jo contact_icon@.85"]))
          elif not quest.mrsl_table["principal_office_checked"]:
            rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["jo contact_icon@.85"]))
            rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_principal_door", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (100,150)))
          else:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
      elif quest.mrsl_table == "laundry":
        if not quest.mrsl_table["mess_seen"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall door_white@.35"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_door_bathroom", marker = ["actions go"]))
        elif not quest.mrsl_table["garbage_original_place"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom garbage_down@.6"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_garbage", marker = ["actions interact"], marker_offset = (0,20)))
        elif not quest.mrsl_table["bathrobe_original_place"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom bathrobe_down@.3"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_bathrobe", marker = ["actions interact"], marker_offset = (120,30)))
        elif not quest.mrsl_table["basket_original_place"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom basket_down@.3"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_basket", marker = ["actions interact"], marker_offset = (30,30)))
        elif not quest.mrsl_table["laundry3_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry3_down@.5"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry3", marker = ["actions interact"], marker_offset = (90,-10)))
        elif not quest.mrsl_table["laundry4_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry4_down@.8"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry4", marker = ["actions interact"], marker_offset = (40,-10)))
        elif not quest.mrsl_table["laundry5_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry5_down"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry5", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (50,-130)))
        elif not quest.mrsl_table["laundry6_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry6_down"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry6", marker = ["actions interact"], marker_offset = (0,-20)))
        elif not quest.mrsl_table["laundry7_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry7_down@.65"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry7", marker = ["actions interact"], marker_offset = (50,-25)))
        elif not quest.mrsl_table["laundry2_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry2_down"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry2", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (80,-20)))
        elif not quest.mrsl_table["laundry1_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom laundry1_down@.85"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_laundry1", marker = ["actions interact"], marker_offset = (50,-10)))
        elif not quest.mrsl_table["wet_laundry4_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom wet_laundry4_down@.85"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_wet_laundry4", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (50,0)))
        elif not quest.mrsl_table["wet_laundry3_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom wet_laundry3_down@.9"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_wet_laundry3", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (-20,60)))
        elif not quest.mrsl_table["wet_laundry2_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom wet_laundry2_down"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_wet_laundry2", marker = ["actions interact"], marker_offset = (0,-20)))
        elif not quest.mrsl_table["wet_laundry1_removed"]:
          rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom wet_laundry1_down"]))
          rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_wet_laundry1", marker = ["actions interact"], marker_offset = (60,30)))
        elif not quest.mrsl_table["piss_puddles_removed"]:
          if mc.owned_item("tissues"):
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom piss_puddles@.9"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_piss_puddles", marker = ["items tissues@.85"], marker_sector = "right_top", marker_offset = (10,70)))
          else:
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom tissues@.6"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_tissues", marker = ["actions take"], marker_offset = (50,-25)))
        elif not quest.mrsl_table["lipstick_writing_removed"]:
          if mc.owned_item("wet_tissue"):
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom mirror@.2"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_mirror", marker = ["items wet_tissue@.85"], marker_sector = "right_top", marker_offset = (60,-60)))
          else:
            if mc.owned_item(("water_bottle","spray_water")):
              for item in ("water_bottle","spray_water"):
                if mc.owned_item(item):
                  rv.append(get_quest_guide_hud("inventory", marker = ["items tissues","actions combine",items_by_id[item].icon]))
            elif not quest.mrsl_table["water_overflow_removed"]:
              rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
              rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_water_overflow", marker = ["items tissues@.85"], marker_offset = (130,20)))
            else:
              rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
              rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sink", marker = ["items tissues@.85"], marker_offset = (260,40)))
        elif not quest.mrsl_table["water_overflow_removed"]:
          if mc.owned_item("book_of_the_dammed"):
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_water_overflow", marker = ["items book book_of_the_dammed@.9"], marker_offset = (130,20)))
          elif mc.owned_item("umbrella"):
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_water_overflow", marker = ["items umbrella@.85,-30"], marker_offset = (130,20)))
          else:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall umbrellas@.85"]))
            rv.append(get_quest_guide_marker("home_hall", "home_hall_umbrella", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (40,0)))
        elif not quest.mrsl_table["water_puddles_removed"]:
          if mc.owned_item("mop2"):
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom water_puddles@.35"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_water_puddles", marker = ["items mop@.8"], marker_offset = (100,25)))
          else:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall cleaning_supply@.5"]))
            rv.append(get_quest_guide_marker("home_hall", "home_hall_cleaning_supply", marker = ["actions take"]))
      elif quest.mrsl_table == "flora":
        if 20 > game.hour > 6:
          rv.append(get_quest_guide_path("home_hall", marker = ["flora contact_icon@.85"]))
          rv.append(get_quest_guide_marker("home_hall", "flora", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (220,0)))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} to\nwake up at "+("{color=#48F}7:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 AM{/}.")))
      elif quest.mrsl_table == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (75,50)))
      elif quest.mrsl_table == "morning":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor homeroom"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_homeroom_door", marker = ["actions go"]))
      elif quest.mrsl_table == "hide":
        rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (200,20)))

      return rv
