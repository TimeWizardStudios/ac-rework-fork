image combine_old_artwork_beaver_carcass_pelt = LiveComposite((298,183),(0,44),"items old_artwork",(105,48),Transform("actions combine",zoom=0.85),(203,0-10),"items beaver_carcass",(198,88+10),"items beaver_pelt2",(224,66),Text("or",style="quest_guide_marker_text"))
image combine_discarded_art_paint_buckets = LiveComposite((369,140),(60,0),"items discarded_art3",(39,45),"items discarded_art2",(10,22),"items discarded_art1",(40,50),Text("(any)",style="quest_guide_marker_text"),(166,27),Transform("actions combine",zoom=0.85),(274,23),"items paint_buckets")
image combine_colored_paper_machete = LiveComposite((288,100),(0-5,0+20),"items colored_paper",(105-5,4+20),Transform("actions combine",zoom=0.85),(193-5,5+20),"items machete")
image combine_pennants_string = LiveComposite((288,96),(0-10,0+20),"items pennants",(105-5,4+20),Transform("actions combine",zoom=0.85),(193,0+20),"items string")


init python:
  class Quest_poolside_story(Quest):

    title =  "Poolside Story"

    class phase_1_poolclosed:
      description = "The invitation of a lifetime. Breaststrokes are definitely\non the table."
      hint = "Wet bodies in skimpy swimsuits.{space=-10}\nMy path is set."
      quest_guide_hint = "Go to the pool."

    class phase_2_newplan:
      hint = "Swim coach! Swim coach! The pool is out of order!"
      quest_guide_hint = "Quest [mrsl]."

    class phase_3_petition:
      hint = "To do list: seek out Lady Justice.{space=-5}"
      quest_guide_hint = "Quest [isabelle]."

    class phase_4_confetti:
      hint = "A painting-meets-woodchipper sort of deal."
      def quest_guide_hint(quest):
        if mc.owned_item("old_artwork"):
          return "Craft the confetti by combining the old artwork with the beaver carcass/pelt."
        else:
          return "Take the old artwork from the supply closet in the art classroom."

    class phase_5_decorations:
      hint = "No bullshit. Just talk to the resident artist."
      quest_guide_hint = "Quest [jacklyn]."

    class phase_6_makedecor:
      hint = "A multi-step decoration DIY project."
      def quest_guide_hint(quest):
        if mc.owned_item("pennants"):
          return "Craft the Independence Day decorations by combining the pennants with the string."
        elif mc.owned_item("colored_paper"):
          if home_hall["machete_taken"]:
            return "Craft the pennants by combining the colored papers with the machete."
          else:
            return "Use the colored papers on the machete in the home hall."
        elif mc.owned_item("discarded_art1") and mc.owned_item("discarded_art2") and mc.owned_item("discarded_art3") and mc.owned_item("string") and mc.owned_item("paint_buckets"):
          return "Craft the colored papers by combining the discarded art with the paint buckets."
        else:
          return "Take the paint buckets from the paint shelf, the string from the supply closet, and the discarded art on the floor of the art classroom."

    class phase_7_isadec:
      hint = "Trading favors with Lady Justice{space=-10}\n— how to ensure success and freedom."
      quest_guide_hint = "Quest [isabelle]."

    class phase_8_waitpet:
      hint = "It's always a waiting game. One, two, three, four — claim the petition score."
      quest_guide_hint = "Wait 3 hours for students to add their signatures to the pool petition, then interact with the bulletin board in the entrance hall."

    class phase_9_convince:
      hint = "School diplomacy — the art of taking names and kissing up to the teachers."
      quest_guide_hint = "Quest [mrsl]."

    class phase_10_meetmrsl:
      hint = "A secret meeting of high implication. Pool yourself together."
      quest_guide_hint = "Go to the pool."

    class phase_11_tryout:
      hint = "This is outrageous and humiliating! Make her explain!"
      quest_guide_hint = "Quest [mrsl]."

    class phase_12_text:
      hint = "This is outrageous and humiliating! Make her explain!"
      quest_guide_hint = "Leave the gym."

    class phase_13_text2:
      hint = "Dress smart. Smell nice. Respond to texts."
      quest_guide_hint = "Leave the sports wing."

    class phase_1000_done:
      description = "I don't know what's right\nand what's real anymore..."


init python:
  class Event_quest_poolside_story(GameEvent):

    def on_energy_cost(event,energy_cost,silent=False):
      if quest.poolside_story == "tryout" and mc.at("school_gym"):
        return 0

    def on_can_advance_time(event):
      if quest.poolside_story == "tryout" and mc.at("school_gym"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.poolside_story == "confetti" and new_location == "school_art_class" and not school_art_class["poolside_story_entered_art_class"]:
          game.events_queue.append("poolside_story_enter_art_class")
      if quest.poolside_story == "text":
          game.events_queue.append("poolside_story_enter_sports_text")
      if quest.poolside_story == "text2":
          game.events_queue.append("poolside_story_enter_sports_text2")

    def on_quest_guide_poolside_story(event):
      rv=[]

      if quest.poolside_story == "poolclosed":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_door_pool", marker = ["actions go"]))

      if quest.poolside_story == "newplan":
        rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "mrsl", marker = ["actions quest"]))

      if quest.poolside_story == "petition":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

      if quest.poolside_story == "confetti":
        if mc.owned_item("old_artwork"):
          if not mc.owned_item("beaver_carcass"):
            if quest.lindsey_book.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Fully Booked{/}."))
            else:
              rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
          else:
            rv.append(get_quest_guide_hud("inventory", marker = ["combine_old_artwork_beaver_carcass_pelt"], marker_sector = "mid_top", marker_offset = (55,0)))
        else:
          rv.append(get_quest_guide_path("school_art_class", marker = ["supply_closet_small"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_supply_closet", marker = ["actions take"]))

      if quest.poolside_story == "decorations":
        rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jacklyn", marker = ["actions quest"]))

      if quest.poolside_story == "makedecor":
        if not school_art_class["discarded_art_1_taken"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class discardedart_1@.45"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_discardedart_1", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (150,0)))
        elif not school_art_class["discarded_art_2_taken"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class discardedart_2@.5"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_discardedart_2", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (150,0)))
        elif not school_art_class["discarded_art_3_taken"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class discardedart_3@.45"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_discardedart_3", marker = ["actions take"], marker_offset = (65,0)))
        if not quest.poolside_story["string_acquired"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["supply_closet_small"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_supply_closet", marker = ["actions take"], marker_offset = (-15,-40)))
        if not quest.poolside_story["paint_acquired"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
          rv.append(get_quest_guide_marker("school_art_class","school_art_class_shelf", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (600,60)))
        if mc.owned_item("pennants"):
          rv.append(get_quest_guide_hud("inventory", marker = ["combine_pennants_string"], marker_sector = "mid_top", marker_offset = (55,0)))
        elif mc.owned_item("colored_paper"):
          if home_hall["machete_taken"]:
            rv.append(get_quest_guide_hud("inventory", marker = ["combine_colored_paper_machete"], marker_sector = "mid_top", marker_offset = (55,0)))
          else:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall sword@.7"]))
            rv.append(get_quest_guide_marker("home_hall", "home_hall_sword", marker = ["items colored_paper@.9"], marker_sector = "left_bottom", marker_offset = (160,-10)))
        elif mc.owned_item("discarded_art1") and mc.owned_item("discarded_art2") and mc.owned_item("discarded_art3") and mc.owned_item("string") and mc.owned_item("paint_buckets"):
          rv.append(get_quest_guide_hud("inventory", marker = ["combine_discarded_art_paint_buckets"]))

      if quest.poolside_story == "isadec":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

      if quest.poolside_story == "waitpet":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor board@.85"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_notice_board", marker = ["actions interact"]))
        if quest.poolside_story["more_signatures"] and (game.hour - quest.poolside_story["isadechour"]) < 3:
          hours = 3 - (game.hour - quest.poolside_story["isadechour"])
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}"+str(hours)+"{/} "+("hour." if hours == 1 else "hours."), marker_offset = (15,-15)))

      if quest.poolside_story == "convince":
        rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "mrsl", marker = ["actions quest"]))

      if quest.poolside_story == "meetmrsl":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_door_pool", marker = ["actions go"]))

      if quest.poolside_story == "tryout":
        rv.append(get_quest_guide_marker("school_gym", "mrsl", marker = ["actions quest"]))

      if quest.poolside_story == "text":
        rv.append(get_quest_guide_marker("school_gym", "school_gym_exit", marker = ["actions go"], marker_offset = (-15,15)))

      if quest.poolside_story == "text2":
        rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_back", marker = ["actions go"], marker_offset = (15,0)))

      return rv
