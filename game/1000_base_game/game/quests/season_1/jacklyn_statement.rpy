image text_jacklyn = LiveComposite((0,0),(63,-53),Transform("jacklyn contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info text",zoom=0.5))


init python:
  class Quest_jacklyn_statement(Quest):

    title = "The Statement"

    class phase_10_remove:
      description = "Love or lust? Sometimes you need to make a statement."
      hint = "Privacy and comfort. Two things you can't achieve with [jo] and [flora] in the house."
      quest_guide_hint = "Quest [flora] and [jo]."
    class phase_11_meetjacklyn:
      description = "Love or lust? Sometimes you need to make a statement."
      hint = "A one on one with [jacklyn]. The dream."
      quest_guide_hint = "Quest [jacklyn] outside the school."
    class phase_20_cook:
      hint = "Cooking is not an activity; it's a state of mind. Man the cutting boards and heat the stove!"
      quest_guide_hint = "Go to the kitchen at home."
    class phase_21_fridge:
      hint = "A man simply does not cook without checking the cold storage."
      quest_guide_hint = "Interact with the mini-fridge in the kitchen at home."
    class phase_22_ingredients:
      hint = "Anyone can throw stuff in a pot and call it a dinner. The skill is in adding the right ingredients."
      quest_guide_hint = "Interact with the stove in the kitchen at home.\n\nOPTIONAL:\nAdd 3 ingredients (Garlic, Bag of Croutons, Whooshster™ Sauce)."
    class phase_23_chef:
      hint = "Sometimes hiring a professional chef is the only solution."
      quest_guide_hint = "Quest [flora], then go to bed."
    class phase_30_dinner:
      hint = "Beep-beep, beep-beep! That's the sound of new possibilities and advertisements."
      quest_guide_hint = "Read [jacklyn]'s text on your phone."
    class phase_31_clothes:
      hint = "A successful date is all in the textiles. At least, that's how it seams."
      quest_guide_hint = "Interact with the closet in your bedroom."
    class phase_40_datedoor:
      hint = "Maybe if enough time is spent looking at the phone, the problems will leave through the front door?"
      quest_guide_hint = "Interact with the door in the kitchen at home."
    class phase_50_jackdate:
      hint = "Just talk to her, man. Stop being a wimp."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_60_intruder:
      hint = "Where would a serial killer hide? Think goddammit!"
      quest_guide_hint = "Interact with the closet in your bedroom."
    class phase_61_florasad:
      hint = "Upsetting [flora] is detrimental to my continued survival. She always plots her revenge behind closed doors."
      quest_guide_hint = "Interact with the door to [flora]'s room."
    class phase_70_statement:
      hint = "Making a statement is all in the presentation. Whatever that means is up to [jacklyn]."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_80_sex:
      hint = "Never thought I'd say this, but post-statement sex is the best kind of sex."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_1000_done:
      description = "[jacklyn] wanted to be impressed; solution: impressionism."


  class Event_quest_jacklyn_statement(GameEvent):
    def on_can_advance_time(event):
      if quest.jacklyn_statement in ("clothes","datedoor","jackdate","intruder","florasad","statement"):
        return False #disable time through the night

    def on_energy_cost(event,energy_cost,silent=False):
      if quest.jacklyn_statement in ("clothes","datedoor","jackdate","intruder","florasad","statement"):
        return 0

    def on_advance(event):
      if quest.jacklyn_statement == "dinner" and quest.jacklyn_statement["doorbell_broken"]:
        game.events_queue.append("jacklyn_statement_home_kitchen_dinner")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "jacklyn_statement" and quest.jacklyn_statement == "cook" and game.location == "home_kitchen":
        game.events_queue.append("jacklyn_statement_home_kitchen_cook")

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.jacklyn_statement=="cook" and new_location=="home_kitchen":
          game.events_queue.append("jacklyn_statement_home_kitchen_cook")
      if quest.jacklyn_statement=="ingredients" and not mc.owned_item("whoo_sauce"):
        game.events_queue.append("jacklyn_statement_ingredients_whoo_sauce_search")
      if quest.jacklyn_statement=="datedoor" and new_location=="home_kitchen":
          game.events_queue.append("jacklyn_statement_home_kitchen_datedoor")
      if quest.jacklyn_statement=="intruder" and new_location=="home_hall":
          game.events_queue.append("jacklyn_statement_home_hall_intruder")
      if quest.jacklyn_statement=="intruder" and new_location=="home_bathroom":
          game.events_queue.append("jacklyn_statement_home_bathroom_intruder")
      if quest.jacklyn_statement=="intruder" and new_location=="home_bedroom":
          game.events_queue.append("jacklyn_statement_home_bedroom_intruder")
      if quest.jacklyn_statement=="statement" and new_location=="school_entrance":
          game.events_queue.append("jacklyn_statement_school_entrance_statement")
      if new_location == "home_bedroom" and quest.jacklyn_statement == "florasad" and flora["madeamends"]:
        game.events_queue.append("quest_jacklyn_statement_florasad_end")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "jacklyn_statement":
        mc["focus"] = ""
        flora["hidden_now"] = jo["at_none_now"] = False

    def on_quest_guide_jacklyn_statement(event):
      rv = []

      if quest.jacklyn_statement == "meetjacklyn":
        rv.append(get_quest_guide_path("school_entrance", marker = ["jacklyn contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_entrance", "jacklyn", marker = ["actions quest"]))
      if quest.jacklyn_statement == "remove":
        if not quest.jacklyn_statement["flora_removed"]:
          rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
        if not quest.jacklyn_statement["jo_removed"]:
          rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "jo", marker = ["actions quest"]))
      if quest.jacklyn_statement == "cook":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
      if quest.jacklyn_statement == "fridge":
        rv.append(get_quest_guide_path("home_kitchen"))
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_mf", marker = ["actions interact"]))
      if quest.jacklyn_statement == "ingredients":
        if mc.owned_item("garlic") and mc.owned_item("croutons"):
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_stove", marker = ["actions interact"]))
        else:
          if not mc.owned_item("garlic"):
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["items garlic"]))
            rv.append(get_quest_guide_marker("school_cafeteria","school_cafeteria_counter_left", marker = ["items garlic"]))
          if not mc.owned_item("whoo_sauce"):
            rv.append(get_quest_guide_path("home_kitchen", marker = ["items whoo_sauce"]))
            rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_sauce_left", marker = ["items whoo_sauce"]))
          if not mc.owned_item("croutons"):
            if home_kitchen["croutons_activated"]:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["items croutons"]))
              rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_crouton", marker = ["items croutons"]))
            else:
              rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
      if quest.jacklyn_statement == "chef":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
      if quest.jacklyn_statement == "dinner":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      if quest.jacklyn_statement == "clothes":
        #rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_closet"))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_closet", marker = ["actions interact"]))
      if quest.jacklyn_statement == "datedoor":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["jacklyn contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_door", marker = ["jacklyn contact_icon@.85"]))
      if quest.jacklyn_statement == "jackdate":
        rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jacklyn", marker = ["actions quest"]))
      if quest.jacklyn_statement == "intruder":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["hidden_number contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_closet", marker = ["hidden_number contact_icon@.85"]))
      if quest.jacklyn_statement == "florasad":
        rv.append(get_quest_guide_path("home_hall", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_hall","home_hall_door_flora", marker = ["flora contact_icon@.85"]))
      #if quest.jacklyn_statement == "datedoor":
      #  rv.append(get_quest_guide_marker("home_kitchen", marker = ["jacklyn contact_icon@.85"]))
      #if quest.jacklyn_statement == "meetjacklyn":
      #  rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class easel_3@.5"]))
      #  rv.append(get_quest_guide_marker("school_art_class","school_art_class_easel_03"))
      #if quest.jacklyn_statement == "paint":
      #  rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class easel_3@.5"]))
      #  rv.append(get_quest_guide_marker("school_art_class","school_art_class_shelf", marker = ["items paint_buckets"], marker_offset = (500,150), marker_sector = "left_bottom"))
      #if quest.jacklyn_statement == "brush":
      #  rv.append(get_quest_guide_path("school_art_class", marker = ["items brush"]))
      #  rv.append(get_quest_guide_marker("school_art_class","school_art_class_brush", marker = ["items brush"], marker_sector = "right_bottom"))
      #if quest.jacklyn_statement == "artist":
      #  rv.append(get_quest_guide_path("school_art_class", marker = ["items beaver_pelt"]))
      #  rv.append(get_quest_guide_marker("school_art_class","school_art_class_easel_03", marker = ["items beaver_pelt"]))
      #if quest.jacklyn_statement == "showjacklyn":
      #  rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
      return rv


label quest_jacklyn_statement_florasad_end:
  $achievement.breaking_all_the_rules.unlock()
  $quest.jacklyn_statement.finish()
  return
