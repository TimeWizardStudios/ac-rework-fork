image kitchen_counter = LiveComposite((809,395),(0,0),"home kitchen counter",(256,76),"home kitchen mf")
image cactus_paint_jar_combine = LiveComposite((280,120),(0,25),"items cactus",(96,25),Transform("actions combine",zoom=0.85),(184,25),"items paint_jar")


init python:
  class Quest_jo_day(Quest):

    title = "Jo's Day"

    class phase_1_flora:
      description = "Selfless for selfish reasons."
      hint = "See the flower with the sharpest{space=-20}\nthorns."
      quest_guide_hint = "Quest [flora]."

    class phase_2_cleaning:
      hint = "Clean. No point beating around the supply closet."
      def quest_guide_hint(quest):
        if quest["vacuum_cleaner_taken"]:
          return "Go to your bedroom."
        else:
          return "Take the vacuum cleaner from the closet in the home hall."

    class phase_3_supplies:
      hint = "Blanket, flower, wine, mug, paint jar, mix and mash. Simple as that!"
      def quest_guide_hint(quest):
        if not quest["picnic_blanket_taken"]:
          return "Take the picnic blanket from the hole in the home hall."
        elif not quest["jo_secret_wine_taken"]:
          return "Take [jo]'s secret wine from the mini fridge in the home kitchen."
        elif not quest["mug_taken"]:
          if mrsl.at("school_homeroom") and quest["steal_attempt"] > 2:
            return "Wait for [mrsl] to leave the homeroom."
          else:
            return "Take the mug from the teacher's desk in the homeroom."
        elif not school_english_class["cactus_taken"]:
          return "Take the cactus from the teacher's desk in the English classroom."
        elif not quest["paint_jar_taken"]:
          return "Take the paint jar from the paint shelf in the art classroom."
        elif not quest["cactus_in_a_pot_crafted"]:
          return "Combine the cactus with the paint jar."
        elif not school_forest_glade["picnic_blanket"]:
          return "Use the picnic blanket on the basket in the forest glade."
        elif not school_forest_glade["jo_secret_wine"]:
          return "Use [jo]'s secret wine on the picnic setup."
        elif not school_forest_glade["cactus_in_a_pot"]:
          return "Use the cactus in a pot on the picnic setup."

    class phase_4_maxine:
      hint = "See the girl with four eyes. Oh, and she has glasses too."
      quest_guide_hint = "Quest [maxine]."

    class phase_5_nurse:
      hint = "Wait, talk, nature. The holy trinity... or something."
      def quest_guide_hint(quest):
        if quest["nurse_harvest"] and nurse["at_none_now"]:
          return "Wait for the [nurse] to come back.{space=-10}"
        else:
          return "Quest the [nurse] in the forest glade."

    class phase_6_love_potion:
      hint = "One word: concoction. One more{space=-5}\nword: [maxine]."
      def quest_guide_hint(_quest):
        if mc.owned_item("queen_rue"):
          return "Give the Queen's Rue to [maxine]."
        else:
          if mc.owned_item(("water_bottle","spray_water")):
            return "Give the water bottle to [maxine].{space=-5}"
          else:
            if mc.owned_item(("empty_bottle","spray_empty_bottle")):
              if quest.lindsey_wrong["fountain_state"] == 5:
                return "Use the empty bottle on the aquarium in the admin wing."
              else:
                return "Use the empty bottle on the water fountain in the first hall."
            else:
              if home_kitchen["water_bottle_taken"]:
                return "Buy a water bottle from the vending machine in the first hall for $100."
              else:
                return "Take the water bottle in the home kitchen."

    class phase_7_droplet:
      hint = "Love potion meets wholesome picnic."
      quest_guide_hint = "Use the love potion droplet on the picnic setup."

    class phase_8_amends:
      def hint(quest):
        if 18 > game.hour > 6:
          return "Today is the big day. Jo's day!"
        else:
          return "Tomorrow is the big day. Jo's day!"
      def quest_guide_hint(quest):
        if 18 > game.hour > 6:
          return "Quest [jo] in the cafeteria."
        else:
          return "Go to sleep."

    class phase_9_picnic:
      hint = "An evening picnic awaits in the forest glade."
      def quest_guide_hint(quest):
        if persistent.time_format == "24h":
          return "Wait until 19:00."
        else:
          return "Wait until 7:00 PM."

    class phase_1000_done:
      description = "Jo's day or my day? Our day!"


init python:
  class Event_quest_jo_day(GameEvent):

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.jo_day == "cleaning" and new_location == "home_bedroom":
        game.events_queue.append("quest_jo_day_cleaning")
      if quest.jo_day["flora_help"] and new_location == "home_bedroom" and not quest.jo_day["flora_helped"]:
        game.events_queue.append("quest_jo_day_cleaning_flora")

    def on_enter_roaming_mode(event):
      if quest.jo_day == "supplies" and school_forest_glade["picnic_blanket"] and school_forest_glade["jo_secret_wine"] and school_forest_glade["cactus_in_a_pot"]:
        game.events_queue.append("quest_jo_day_supplies_maxine")

    def on_advance(event):
      if quest.jo_day == "picnic" and game.hour == 19:
        game.events_queue.append("quest_jo_day_picnic")

    def on_quest_guide_jo_day(event):
      rv = []

      if quest.jo_day == "flora":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))

      elif quest.jo_day == "cleaning":
        if quest.jo_day["vacuum_cleaner_taken"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["items vacuum_cleaner@.7"]))
        else:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall cleaning_supply@.5"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_cleaning_supply", marker = ["actions take"]))

      elif quest.jo_day == "supplies":
        if not quest.jo_day["picnic_blanket_taken"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall hole@.75"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_hole", marker = ["actions take"], marker_sector = "right_top", marker_offset = (60,-260)))
        elif not quest.jo_day["jo_secret_wine_taken"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["kitchen_counter@.175"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_mf", marker = ["actions take"]))
        elif not quest.jo_day["mug_taken"]:
          if mrsl.at("school_homeroom") and quest.jo_day["steal_attempt"] > 2:
            rv.append(get_quest_guide_hud("time", marker = "$Wait for {color=#48F}[mrsl]{/} to leave the {color=#48F}homeroom{/}."))
          else:
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom desk@.35"]))
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (300,35)))
        elif not school_english_class["cactus_taken"]:
          rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class cactus@.75"]))
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_cactus", marker = ["actions take"]))
        elif not quest.jo_day["paint_jar_taken"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
          rv.append(get_quest_guide_marker("school_art_class","school_art_class_shelf", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (580,50)))
        elif not quest.jo_day["cactus_in_a_pot_crafted"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["cactus_paint_jar_combine"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif not school_forest_glade["picnic_blanket"]:
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade basket@.55"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_picnic", marker = ["items picnic_blanket@.9"]))
        elif not school_forest_glade["jo_secret_wine"]:
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade basket@.55"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_picnic", marker = ["items jo_secret_wine@.9"], marker_offset = (80,20)))
        elif not school_forest_glade["cactus_in_a_pot"]:
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade basket@.55"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_picnic", marker = ["items cactus_in_a_pot"], marker_offset = (80,20)))

      elif quest.jo_day == "maxine":
        if maxine.at("school_clubroom"):
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
        else:
          rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.jo_day == "nurse":
        if quest.jo_day["nurse_harvest"] and nurse["at_none_now"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait for the {color=#48F}[nurse]{/}\nto come back."))
        else:
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["nurse contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "nurse", marker = ["actions quest"], marker_offset = (40,10)))

      elif quest.jo_day == "love_potion":
        if mc.owned_item("queen_rue"):
          if maxine.at("school_clubroom"):
            rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
            rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["items queen_rue@.9"], marker_offset = (75,25)))
          else:
            rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["items queen_rue@.9"]))
        else:
          if mc.owned_item(("water_bottle","spray_water")):
            item = mc.owned_item(("water_bottle","spray_water"))
            if maxine.at("school_clubroom"):
              rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
              rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["items bottle " + item], marker_offset = (75,25)))
            else:
              rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["items bottle " + item]))
          else:
            if mc.owned_item(("empty_bottle","spray_empty_bottle")):
              item = mc.owned_item(("empty_bottle","spray_empty_bottle"))
              if quest.lindsey_wrong["fountain_state"] == 5:
                rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"]))
                rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_aquarium", marker = ["items bottle " + item], marker_sector = "left_top", marker_offset = (200,150)))
              else:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.6"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["items bottle " + item], marker_sector = "left_bottom", marker_offset = (0,35)))
            else:
              if home_kitchen["water_bottle_taken"]:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["$Buy a","items bottle water_bottle","$for {color=#48F}$100{/}"]))
              else:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
                rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))

      elif quest.jo_day == "droplet":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade basket@.55"]))
        rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_picnic", marker = ["items love_potion_droplet"], marker_offset = (125,-75)))

      elif quest.jo_day == "amends":
        if 18 > game.hour > 6:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["jo contact_icon@.85"]))
          if jo.at("school_cafeteria"):
            rv.append(get_quest_guide_marker("school_cafeteria", "jo", marker = ["actions quest"]))
          else:
            if mc.at("school_cafeteria"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} between\n"+("{color=#48F}9:00{/} — {color=#48F}17:00{/}." if persistent.time_format == '24h' else "{color=#48F}9:00 AM{/} — {color=#48F}5:00 PM{/}.")))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))

      elif quest.jo_day == "picnic":
        rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 PM{/}.")))

      return rv
