init python:
  class Quest_kate_blowjob_dream(Quest):
    title = "Make a Wish and Blow"
    class phase_1_start:
      description = "One day done. Happy second first day of the last school year!"
      hint = "A good wank and some rest. Doctor's orders."
    class phase_2_courage_badge:
      hint = "I feel a pull towards my comfort zone."
    class phase_3_sleep:
      hint = "Maybe I shouldn't have taken all that melatonin.."
    class phase_4_flora_knocking:
      hint = "Maybe I shouldn't have taken all that melatonin."
    class phase_5_open_door:
      hint = "[flora] will kick down the door if you don't open it. Her legs are stronger than they look."
    class phase_6_get_dressed:
      hint = "Dress up or dress down. As long as it happens."
    class phase_7_school:
      hint = "Late for school again, are you? No surprise there."
    class phase_8_awake:
      hint = "Late for school again, are you? No surprise there."
    class phase_9_alarm:
      hint = "Smash or pass. My favorite game."
    class phase_10_alarm_smashed:
      pass
    class phase_1000_done:
      description = "What a dream..."


  class Event_quest_kate_blowjob_dream(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_tour.finished:
        if not quest.kate_blowjob_dream.started and new_location=="school_ground_floor":
          game.events_queue.append("quest_kate_blowjob_dream_mrsl")
        if quest.kate_blowjob_dream == "start" and new_location=="home_bedroom":
          game.events_queue.append("quest_kate_blowjob_dream_home_bedroom")
        if quest.kate_blowjob_dream == "awake" and new_location=="home_bedroom":
          game.events_queue.append("quest_kate_blowjob_dream_home_bedroom_awake")
    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_blowjob_dream" and quest.kate_blowjob_dream == "flora_knocking":
        game.events_queue.append("quest_kate_blowjob_dream_home_bedroom_flora_knocking")
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.kate_blowjob_dream >= "open_door" and not quest.kate_blowjob_dream.ended:
        return 0
    def on_can_advance_time(event):
      if quest.kate_blowjob_dream >= "courage_badge" and not quest.kate_blowjob_dream.ended:
        return False
    def on_quest_finished(event,quest,silent=False):
      if quest.id == "kate_blowjob_dream":
        mc["focus"] = ""

    def on_quest_guide_kate_blowjob_dream(event):
      rv = []
      if quest.kate_blowjob_dream <= "courage_badge":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))
      if quest.kate_blowjob_dream == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      if quest.kate_blowjob_dream == "open_door":
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_door", marker = ["actions interact"]))
      if quest.kate_blowjob_dream == "get_dressed":
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_closet", marker = ["actions interact"]))
      if quest.kate_blowjob_dream == "school":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_gym", "kate", marker = ["actions quest"], marker_offset = (75,0)))
      if quest.kate_blowjob_dream == "alarm":
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_alarm", marker = ["actions interact"]))
      return rv
