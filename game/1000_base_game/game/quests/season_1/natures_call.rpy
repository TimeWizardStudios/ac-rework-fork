init python:
  class Quest_natures_call(Quest):
    ## 1-1, Act 0, Scene 3, McQuestOne03
    title="Nature's Call"
    class phase_1_natures_call:
      description="Standing up or sitting down? Either way, it's time for a leak."
      hint="The morning aim-practice begins. The toilet is that way."
    class phase_1000_done:
      description="Hardest part of the day completed. It's smooth sailing from here."

  class Event_quest_natures_call(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if not quest.natures_call.started:
        if new_location==home_bathroom:
          game.events_queue.append("quest_natures_call_start")
    def on_quest_guide_natures_call(event):
      if quest.natures_call=="natures_call":
        return get_quest_guide_marker("home_bathroom","home_bathroom_toilet", marker = "actions interact")

label quest_natures_call_start:
  "These tiles have reflected some of my dirtiest moments."
  "I'm surprised they still sparkle."
  $quest.natures_call.start()
  return
