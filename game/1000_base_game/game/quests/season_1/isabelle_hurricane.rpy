image call_isabelle = LiveComposite((0,0),(63,-53),Transform("isabelle contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))
image call_maxine = LiveComposite((0,0),(63,-53),Transform("maxine contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))


init python:
  class Quest_isabelle_hurricane(Quest):

    title = "Hurricane Isabelle"

    class phase_10_maxine_office:
      description = "Hurricane [isabelle] is raging toward Newfall. Flash floods and mudslides\nin the forecast."
      hint = "Interrogate the prime suspect."
      quest_guide_hint = "Go to [maxine]'s office."

    class phase_20_guard_booth:
      hint = "Toward the house of dough and coffee."
      quest_guide_hint = "Interact with the [guard]'s booth in the entrance hall."

    class phase_30_short_circuit:
      hint = "Time for a shocking experience."
      def quest_guide_hint(quest):
        if quest["scratching"]:
          return "Interact with the fuse box in the first hall."
        elif quest["backpack"]:
          return "Use [spinach] on the door in the first hall."
        elif quest["distraction"]:
          return "Take [spinach] in your bedroom."
        else:
          return "Interact with the fuse box in the first hall."

    class phase_31_blowjob:
      hint = "If you can't get your own dick sucked, lend a mouth."
      def quest_guide_hint(quest):
        if quest["cocksucker"]:
          return "Interact with the [guard]'s booth in the entrance hall."
        else:
          return "Quest [kate] in the sports wing."

    class phase_40_surveillance_footage:
      hint = "Flash plug."
      quest_guide_hint = "Use the flash drive on the computer in your bedroom."

    class phase_50_forest:
      hint = "Lost in the woods, are we?"
      def quest_guide_hint(quest):
        if quest["hiking"]:
          return "Interact with the creepy hut in the forest glade."
        else:
          return "Quest [isabelle]."

    class phase_60_call_isabelle:
      hint =  "Call her, maybe."
      quest_guide_hint = "Call [isabelle] on your phone."

    class phase_70_call_maxine:
      hint = "Flip or smart? Which one are you?{space=-20}"
      quest_guide_hint = "Call [maxine] on your phone."

    class phase_1000_done:
      description = "Nothing good ever happens\nin the forest glade."


init python:
  class Event_quest_isabelle_hurricane(GameEvent):

    def on_energy_cost(event,energy_cost,silent=False):
      if quest.isabelle_hurricane in ("call_isabelle","call_maxine"):
        return 0

    def on_can_advance_time(event):
      if ((quest.isabelle_hurricane == "short_circuit" and quest.isabelle_hurricane["darkness"])
      or quest.isabelle_hurricane in ("call_isabelle","call_maxine")):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_hurricane == "maxine_office" and new_location == "school_clubroom":
        game.events_queue.append("quest_isabelle_hurricane_maxine_office")
      elif quest.isabelle_hurricane == "surveillance_footage" and new_location == "home_bedroom" and quest.isabelle_hurricane["darkness"] and not quest.isabelle_hurricane["spinach_gone"]:
        game.events_queue.append("quest_isabelle_hurricane_short_circuit_spinach2")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "isabelle_hurricane":
        school_forest_glade["exclusive"] = False
        isabelle.equip("isabelle_pants")
        isabelle.equip("isabelle_shirt")
        isabelle.equip("isabelle_glasses")

    def on_quest_guide_isabelle_hurricane(event):
      rv = []

      if quest.isabelle_hurricane == "maxine_office":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))

      elif quest.isabelle_hurricane == "guard_booth":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (70,170)))

      elif quest.isabelle_hurricane == "short_circuit":
        if quest.isabelle_hurricane["darkness"]:
          rv.append(get_quest_guide_hud("phone", marker=["$Click on your {color=#48F}phone{/}."]))
        elif quest.isabelle_hurricane["scratching"]:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["actions interact"], marker_offset = (60,90)))
        elif quest.isabelle_hurricane["backpack"]:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall door@.5"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_door", marker = ["items spinach@.85"], marker_offset = (0,0)))
        elif quest.isabelle_hurricane["distraction"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["spinach contact_icon@.85"]))
          rv.append(get_quest_guide_marker("home_bedroom", "spinach", marker = ["actions take"]))
        else:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["actions interact"], marker_offset = (60,90)))

      elif quest.isabelle_hurricane == "blowjob":
        if quest.isabelle_hurricane["cocksucker"]:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (70,170)))
        else:
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["kate contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_first_hall_east", "kate", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (160,16)))

      elif quest.isabelle_hurricane == "surveillance_footage":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["items flash_drive@.8"]))

      elif quest.isabelle_hurricane == "forest":
        if quest.isabelle_hurricane["hiking"]:
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade house@.3"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_house", marker = ["actions interact"], marker_offset = (25,25)))
        else:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_hurricane == "call_isabelle":
        rv.append(get_quest_guide_hud("phone", marker=["call_isabelle","$\n\n\n{space=20}Call {color=#48F}[isabelle]{/}."]))

      elif quest.isabelle_hurricane == "call_maxine":
        rv.append(get_quest_guide_hud("phone", marker=["call_maxine","$\n\n\n{space=25}Call {color=#48F}[maxine]{/}."]))

      return rv
