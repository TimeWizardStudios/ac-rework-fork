image fire_axe_cropped = Crop((0,0,211,669),"school roof_landing fire_axe")


init python:
  class Quest_flora_squid(Quest):

    title = "Squid Game"

    class phase_1_sleep:
      description = "Some games are more serious\nthan others."
      hint = "Hear the siren song of the deep? Let it rock you to sleep."
      quest_guide_hint = "Go to sleep."
    class phase_2_trail:
      hint = "No need to be a tracker to know what to do."
      def quest_guide_hint(quest):
        if school_ground_floor_west["visited_today"]:
          return "Quest [flora] in the admin wing."
        elif quest["snail_flora"]:
          return "Follow the water trail."
        else:
          return "Leave your bedroom."
    class phase_3_search:
      hint = "Just touch the wet spot... no, not the one between her legs."
      quest_guide_hint = "Interact with the puddles in the first hall."
    class phase_4_choice:
      hint = "To the forbidden place!"
      quest_guide_hint = "Go to the women's bathroom."
    class phase_41_wait:
      def hint(quest):
        if quest["smell_concern"]:
          return "Some people can just admire the achievements of others. You're one of them."
        else:
          return "Some women are forced to talk to you. Exploit it!"
      def quest_guide_hint(quest):
        if quest["smell_concern"]:
          return "Interact with the trophy case."
        else:
          return "Quest [mrsl]."
    class phase_42_bathroom:
      def hint(quest):
        if quest["bathroom_interactables"] == 3:
          return "[flora], [flora], [flora], [flora]... It's always about her, isn't it?"
        else:
          return "Search high and search low — but more importantly, search wet!"
      def quest_guide_hint(quest):
        if quest["bathroom_interactables"] == 3:
          return "Quest [flora]."
        else:
          return "Interact with [lindsey]'s water bottle, the showers, and the urinal."
    class phase_5_weapon:
      hint = "The warrior is but a man without his blade."
      quest_guide_hint = "Take either the fire axe from the roof landing, or the machete from the home hall."
    class phase_6_ready:
      hint = "Let me spell it out for you:\nF-L-O-R-A."
      quest_guide_hint = "Quest [flora] outside the school."
    class phase_7_forest_glade:
      hint = "Time to get your hands dirty... but also wet. Wet and dirty."
      quest_guide_hint = "Interact with the stream."
    class phase_1000_done:
      description =  "Squids for the squid god.\nYou know the one."


  class Event_quest_flora_squid(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.flora_squid in ("search","choice","wait","bathroom","forest_glade"):
        return 0

    def on_can_advance_time(event):
      if quest.flora_squid in ("search","choice","wait","bathroom","forest_glade"):
        return False

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "flora_squid":
        if quest.flora_squid == "trail":
          school_ground_floor["sign_fallen"] = False
        elif quest.flora_squid == "weapon" and mc.owned_item("machete"):
          quest.flora_squid.advance("ready")
        elif quest.flora_squid == "forest_glade":
          school_forest_glade["exclusive"] = "flora"

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.flora_squid == "trail":
        if new_location == "home_hall" and not quest.flora_squid["snail_flora"]:
          game.events_queue.append("quest_flora_squid_trail_hall")
        elif new_location == "home_kitchen" and not quest.flora_squid["water_break"]:
          game.events_queue.append("quest_flora_squid_trail_kitchen")
        elif new_location == "school_ground_floor" and not quest.flora_squid["suspicious_puddle"]:
          game.events_queue.append("quest_flora_squid_trail_ground_floor")
      elif quest.flora_squid == "search" and new_location == "school_first_hall" and not quest.flora_squid["mess_of_puddles"]:
        game.events_queue.append("quest_flora_squid_search")
      elif quest.flora_squid == "choice" and new_location == "school_bathroom" and not quest.flora_squid["toilet_diving"]:
        game.events_queue.append("quest_flora_squid_bathroom")
      elif quest.flora_squid == "forest_glade":
        if new_location == "school_forest_glade" and not quest.flora_squid["polluted_stream"]:
          game.events_queue.append("quest_flora_squid_forest_glade")
        elif new_location == "home_bathroom":
          game.events_queue.append("quest_flora_squid_shower")

    def on_enter_roaming_mode(event):
      if quest.flora_squid == "forest_glade" and quest.flora_squid["tree_monster"]:
        game.events_queue.append("quest_flora_squid_forest_glade_stream_middle")

    def on_quest_finished(event,_quest,silent=False):
      if _quest.id == "flora_squid":
        school_forest_glade["exclusive"] = False
        flora.equip("flora_panties")
        flora.equip("flora_bra")
        if quest.flora_handcuffs > "package":
          flora.equip("flora_skirt")
        else:
          flora.equip("flora_pants")
        flora.equip("flora_shirt")

    def on_quest_guide_flora_squid(event):
      rv = []

      if quest.flora_squid == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))
      elif quest.flora_squid == "trail":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_ground_floor_west", "flora", marker = ["actions quest"], marker_offset = (100,0)))
      elif quest.flora_squid == "search":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water_puddle_6","school first_hall water_puddle_1","school first_hall water_puddle_5"]))
        rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddles_squid", marker = ["actions interact"], marker_sector = "left_top", marker_offset=(0,40)))
      elif quest.flora_squid == "choice":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_door_bathroom", marker = ["actions go"]))
      elif quest.flora_squid == "wait":
        if quest.flora_squid["smell_concern"]:
          rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_trophy_case", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset=(400,-40)))
        else:
          rv.append(get_quest_guide_marker("school_first_hall_east", "mrsl", marker = ["actions quest"]))
      elif quest.flora_squid == "bathroom":
        if quest.flora_squid["bathroom_interactables"] == 3:
          rv.append(get_quest_guide_marker("school_bathroom", "flora", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset=(100,-20)))
        else:
          if not quest.flora_squid["water_bottle_searched"]:
            rv.append(get_quest_guide_marker("school_bathroom", "school_bathroom_water_bottle", marker = ["actions interact"]))
          if not quest.flora_squid["shower_searched"]:
            rv.append(get_quest_guide_marker("school_bathroom", "school_bathroom_shower2", marker = ["actions interact"], marker_offset=(0,40)))
          if not quest.flora_squid["urinal_searched"]:
            rv.append(get_quest_guide_marker("school_bathroom", "school_bathroom_urinal", marker = ["actions interact"], marker_offset=(15,300)))
      elif quest.flora_squid == "weapon":
        rv.append(get_quest_guide_path("home_hall", marker = ["$or","home hall sword@.7","${space=50}"]))
        rv.append(get_quest_guide_marker("home_hall", "home_hall_sword", marker = ["$or{space=-50}","actions take"]))
        rv.append(get_quest_guide_path("school_roof_landing", marker = ["fire_axe_cropped@.2"]))
        rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_fire_axe", marker = ["actions take"], marker_sector = "right_top", marker_offset=(75,-200)))
      elif quest.flora_squid == "ready":
        rv.append(get_quest_guide_path("school_entrance", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_entrance", "flora", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset=(75,-25)))
      elif quest.flora_squid == "forest_glade":
        rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_river_lv3", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset=(1200,100)))

      return rv
