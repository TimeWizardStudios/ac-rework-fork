init python:
  class Quest_isabelle_tour(Quest):
    title="Tour de School"
    class phase_1_beginning:
      description="She wants it. The big tour. Show her {i}everything{/}."
      hint="Stop thinking about sex. It's a school tour, not a tinder date."
    class phase_2_go_upstairs:
      hint="An adventure awaits behind every corner... and maybe up every staircase? We'll see."
    class phase_3_first_hall_meetup:
      hint="A fleeting moment on a set of stairs. Enemies at first glance."
    class phase_4_confrontation_side_isabelle:
      hint="Confronting the dark queen? Don your shining armor and ride forth, oh whitest of knights."
    class phase_5_confrontation_side_kate:
      hint="The new girl versus the dark queen. Can I have an extra large bag of popcorn, please."
    class phase_6_english_class:
      hint="The English girl in the English classroom. Write me a posh romantic cliché."
    class phase_7_art_class:
      hint="As if English wasn't bad enough. Now she wants to be an artist! Why can't she just be uncultured?"
    class phase_8_gym:
      hint="Gym class? Seriously? Why can't she be more cultured?"
    class phase_9_crash:
      hint="Looks like I'm playing Mercy again."
    class phase_10_cafeteria:
      hint="The end of one journey is the beginning of another. And this one ends in the cafeteria."
    class phase_1000_done:
      description="Completed the tour almost scot-free. That's worth a trophy of some sort."
      
      
  class Event_quest_isabelle_tour(GameEvent):
    
    def on_location_changed(event,old_location,new_location,silent=False):
      
      if quest.isabelle_tour.in_progress:
        if quest.isabelle_tour=="go_upstairs" and new_location=="school_first_hall":
          game.events_queue.append("quest_isabelle_tour_go_upstairs")
      
        if quest.isabelle_tour=="english_class" and new_location=="school_english_class" and not quest.isabelle_tour["english_class_intro"] :
          game.events_queue.append("quest_isabelle_tour_english_arrive")
      
        if quest.isabelle_tour=="art_class" and new_location=="school_art_class" and not quest.isabelle_tour["art_class_intro"] :
          game.events_queue.append("quest_isabelle_tour_art_arrive")

    def on_quest_guide_isabelle_tour(event):
      rv=[] 

      if not quest.the_key.started:
        if mrsl.at("school_homeroom*"):
            rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "mrsl", marker = ["actions quest"]))
        else:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["mrsl contact_icon@.85"]))
          if mc.at("school_homeroom"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for [mrsl]"))
    
      if quest.isabelle_tour <= "first_hall_meetup":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"], force_marker = True))
        rv.append(get_quest_guide_path(isabelle.location, marker = ["isabelle contact_icon@.85"]))
      
      if quest.isabelle_tour in ("confrontation_side_isabelle", "confrontation_side_kate"):
        rv.append(get_quest_guide_char("kate", marker = ["actions quest"], force_marker = True))
        rv.append(get_quest_guide_path(kate.location, marker = ["kate contact_icon@.85"]))
      
      if quest.isabelle_tour == ("english_class"):
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))
      if quest.isabelle_tour == ("art_class"):
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_art_class", marker = ["isabelle contact_icon@.85"]))
      
      if quest.isabelle_tour == ("gym"):
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_gym", marker = ["isabelle contact_icon@.85"]))
      
      if quest.isabelle_tour == "cafeteria":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["isabelle contact_icon@.85"]))
      
      return rv


label quest_isabelle_tour_go_upstairs:
  $quest.isabelle_tour.advance("first_hall_meetup")
  "The 1st Floor — what a sight!"
  "Long have I waited to tread these halls, feel the wind in my hair, and behold the fair maidens of this elevated height!"
  "Or, you know... panic starts to set in, the AC is still broken, and the girls run at the mere sight of me..."
  "Not much has changed here."
  return

label quest_isabelle_tour_english_arrive:
  "English was never one of my good subjects."
  "Studying mossy poets and authors from a distant past always seemed like a waste of time."
  "This classroom is waking a lot of memories from hibernation..."
  "The only good one is getting third place at a mock spelling bee. How sad is that?"
  $quest.isabelle_tour["english_class_intro"] = True
  return

label quest_isabelle_tour_art_arrive:
  show isabelle neutral at Position(xalign=.75)
  show jacklyn excited at Position(xalign=.25)
  with Dissolve(.5)
  jacklyn excited "Hi and welcome! My name is [jacklyn] Hyde."
  jacklyn excited "I'm still a teacher's assistant, but due to Mrs. Bloomer's poor health, I've been asked to step in until she gets back."
  "During my last time around, [jacklyn] wasn't here... but she's definitely an upgrade to Mrs. Bloomer in terms of looks."
  "That lady was closing in on sixty, with cigarette-yellowed teeth and enough wrinkles to compete with a bowl of raisins..."
  show isabelle concerned_left
  jacklyn annoyed "Forgive the ass-stick intro."
  jacklyn laughing "Apparently, it's not enough to wear this top — sucking bureaucracy's cock is a job requirement."
  jacklyn excited "If you're feeling it, call me [jacklyn]... between artists, we're all on a first-name basis."
  isabelle excited "That's refreshing to hear! I'd like to sign up for your class, please!"
  jacklyn excited_right "Wicked! What's your name?"
  isabelle excited "Oh, great. I'm [isabelle]."
  jacklyn laughing "Badass. Okay, you're on the shortlist, babylegs!"
  hide isabelle
  hide jacklyn
  with Dissolve(.5)
  $quest.isabelle_tour["art_class_intro"] = True
  return
