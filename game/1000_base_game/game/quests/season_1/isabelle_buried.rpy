init python:
  class Quest_isabelle_buried(Quest):

    title = "Buried Truths"

    class phase_1_lindsey:
      description = "The dorks delved too greedily and too deep..."
      hint = "[isabelle]'s box — hot. [lindsey] might have it — also hot."
      quest_guide_hint = "Quest [lindsey]."
    class phase_2_buried_jacklyn:
      hint = "Who knew [jacklyn] dabbles with love boxes? Shocking."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_3_boxhunt:
      hint = "No love box littering! Unless it's in a specialized litter-container. The school has a few of those."
      quest_guide_hint = "Interact with the Trash Can next to the water fountain."
    class phase_4_buried_maxine:
      hint = "Sharp eyes. Like dagger-sharp. [maxine] might have a vat of them somewhere."
      quest_guide_hint = "Quest [maxine]."
    class phase_5_callforhelp:
      hint = "Help! Help! Yep, that's all."
      quest_guide_hint = "Call ??? on your phone."
    class phase_51_text:
      hint = "Let's play a wait-game, play a wait-game."
      quest_guide_hint = "Move to a new location, then read the text message on your phone."
    class phase_6_getbox:
      hint = "Special delivery! Yes, we deliver to school lockers."
      quest_guide_hint = "Interact with Personal Locker."
    class phase_7_isabelle:
      hint = "Get the box. Get the girl. Simple as that."
      quest_guide_hint = "Quest [isabelle]."
    class phase_71_shovel:
      hint = "The best shovels are the ones you make yourself. Or was that a meal? Sticks and meals may break my feels."
      quest_guide_hint = "Combine Stick and Empty Soup Can, then Quest [isabelle]."
    class phase_72_wait:
      hint = "Good comes to those who wait."
      quest_guide_hint = "Wait until the end of the day."
    class phase_8_bury:
      hint = "The perfect location under the trees. A lovely ceremony."
      quest_guide_hint = "Go to Forest Glade."
    class phase_9_dig:
      hint = "I'm used to burying myself in regrets. Maybe it's time to dig myself into another hole."
      def quest_guide_hint(_quest):
        if quest.maxine_hook.finished:
          return "Use the shovel on the patch of dirt."
        else:
          return "Use the shovel on the car tire."
    class phase_10_funeral:
      hint = "[isabelle]'s hole. Never thought I'd stick my shovel in it. What does she think?"
      quest_guide_hint = "Quest [isabelle]."
    class phase_110_missinglocker:
      hint = "A new day. New opportunities. Just got to drag myself to school."
      quest_guide_hint = "Go to Entrance Hall and Interact with Missing Locker."
    class phase_1000_done:
      description = "Life's a garden; dig it."


  class Event_quest_isabelle_buried(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.isabelle_buried in ("bury","dig","funeral"):
        return 0

    def on_can_advance_time(event):
      if quest.isabelle_buried in ("bury","dig","funeral"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_buried.in_progress:
        if quest.isabelle_buried=="text":
          game.events_queue.append("quest_isabelle_buried_callforhelp_text")
        if quest.isabelle_buried=="bury":
          game.events_queue.append("quest_isabelle_buried_entered_glade")

    def on_advance(event):
      if quest.isabelle_buried.in_progress:
        if quest.isabelle_buried=="text":
          game.events_queue.append("quest_isabelle_buried_callforhelp_text")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "isabelle_buried":
        quest["done_today"] = True

    def on_quest_guide_isabelle_buried(event):
      rv = []

      if quest.isabelle_buried == "lindsey":
        rv.append(get_quest_guide_char("lindsey",marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
      if quest.isabelle_buried == "buried_jacklyn":
        if jacklyn.at("school_art_class"):
          rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
        else:
          rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
          if mc.at("school_art_class"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for [jacklyn] {color=#48F} 1:00 PM{/}"))
      if quest.isabelle_buried == "boxhunt":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall bin"]))
        rv.append(get_quest_guide_marker("school_first_hall","school_first_hall_bin", marker = ["actions interact"], marker_sector = "right_bottom"))
      if quest.isabelle_buried == "buried_maxine":
        rv.append(get_quest_guide_char("maxine",marker = ["maxine contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "maxine", marker = ["actions quest"]))
      if quest.isabelle_buried == "callforhelp":
        rv.append(get_quest_guide_hud("phone",marker=["phone apps contact_info call@0.5","$Call for help."]))
      if quest.isabelle_buried == "text":
        rv.append(get_quest_guide_hud("time", marker="$Wait an hour."))
      if quest.isabelle_buried == "getbox":
        rv.append(get_quest_guide_path("school_locker", marker = ["items isabelle_box"]))
        rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_lockers_front_right", marker = ["items locker_key"]))
        rv.append(get_quest_guide_marker("school_locker","school_locker_isabelle_box", marker = ["actions interact"]))
      if quest.isabelle_buried == "isabelle":
        rv.append(get_quest_guide_char("isabelle",marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
      if quest.isabelle_buried == "shovel":
        if mc.owned_item("shovel"):
          rv.append(get_quest_guide_char("isabelle",marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
        else:
          if not mc.owned_item("stick"):
            rv.append(get_quest_guide_path("school_forest_glade",marker = ["items stick_1"]))
            rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_wood",marker = ["items stick_1"]))
          if not mc.owned_item(("soup_can","empty_can")):
            rv.append(get_quest_guide_path("school_locker",marker = ["items soupcan"]))
            rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_lockers_front_right", marker = ["items locker_key"]))
            rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["items soupcan"]))
          if mc.owned_item("empty_can") and mc.owned_item("stick"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items empty_can", "actions combine", "items stick_1"]))
          elif not mc.owned_item("empty_can") and mc.owned_item("soup_can"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items soupcan", "actions consume"]))
      if quest.isabelle_buried == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait until the end of the day."))
      if quest.isabelle_buried == "bury":
        rv.append(get_quest_guide_char("isabelle",marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
      if quest.isabelle_buried == "dig":
        if quest.maxine_hook.finished:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_dirt_patch", marker = ["items shovel@.8"], marker_offset = (75,10)))
        else:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_tire", marker = ["items shovel@.8"], marker_offset = (150,0)))
      if quest.isabelle_buried == "funeral":
        rv.append(get_quest_guide_char("isabelle",marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
      if quest.isabelle_buried == "missinglocker":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor missing_locker"]))
        rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_missing_locker"))

      return rv
