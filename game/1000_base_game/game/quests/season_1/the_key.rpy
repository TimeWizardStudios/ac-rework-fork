init python:
  class Quest_the_key(Quest):
    title="The Key"
    class phase_1_the_key:
      description="On the surface, it's a simple task. Actually getting it might be harder."
      hint="The place that smells of doughnuts, coffee, and sloth."
    class phase_1000_done:
      description="Help! I can't get the key from the [guard]! Oh, wait. It's right here. How silly of me."
  
  class Event_quest_the_key(GameEvent):

    def on_quest_guide_the_key(event):
      rv=[] 
      if quest.the_key.in_progress:
        rv.append(get_quest_guide_path("school_ground_floor"))
        rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_guard_booth", marker = "items permission_slip"))
      return rv
