init python:
  class Quest_kate_desire(Quest):

    title = "Twisted Desire"

    class phase_1_challenge:
      description = "It's time to rock the boat... ship... status quo. Yes, that's it."
      hint = "Time to throw the gauntlet at the queen."
      quest_guide_hint = "Quest [kate]."
    class phase_2_advice:
      hint = "Every champion needs an advisor — a wise old man, usually. But maybe a cute exchange student will do?"
      quest_guide_hint = "Quest [isabelle] in the admin wing."
    class phase_3_rest:
      hint = "Exit light. Enter night."
      quest_guide_hint = "Go to sleep."
    class phase_4_dress:
      hint = "Suit up! Cape, sword... everything you need to grab the devil by the horns."
      quest_guide_hint = "Interact with the bedroom closet."
    class phase_5_gym:
      hint = "Face off in the den of the devil. The odds could've been better."
      quest_guide_hint = "Quest [kate] in the gym."
    class phase_6_starblue:
      hint = "{i}\"When you're waiting for the principal, bored enough to moan.{space=-10}\nCome look at me! I host a school of my own.\"{/}"
      quest_guide_hint = "Interact with the aquarium in the admin wing."
    class phase_7_starred:
      def hint(quest):
        if quest["blue_star_interacted"]:
          return "{i}\"I'm big and I'm long.\nI let you take a ride on me.\nI take you along...\nTo the place you least want to be.\"{/}"
        else:
          return "I know you're semi-illiterate, but if you're in the gutter, look at the stars!"
      def quest_guide_hint(quest):
        if quest["blue_star_interacted"]:
          return "Interact with the bus outside the school."
        else:
          return "Interact with the blue star in your inventory."
    class phase_8_stargreen:
      def hint(quest):
        if quest["red_star_interacted"]:
          return "{i}\"Touch me once, I'll make a sound.\nPush my buttons. I don't mind.\nFinger me. Oh, do it right.\nI'll sing for you. I'll sing all night.\"{/}"
        else:
          return "I know you're semi-illiterate, but if you're in the gutter, look at the stars!"
      def quest_guide_hint(quest):
        if quest["red_star_interacted"]:
          return "Interact with the piano in the fine arts wing."
        else:
          return "Interact with the red star in your inventory."
    class phase_9_holdup:
      hint = "Run or fight — the choice is yours."
      quest_guide_hint = "Leave the fine arts wing, or quest [kate]."
    class phase_10_starblack:
      def hint(quest):
        if quest["green_star_interacted"]:
          return "{i}\"I'm hard.\nI'm strong.\nI'm inside you all life long.\"{/}"
        else:
          return "I know you're semi-illiterate, but if you're in the gutter, look at the stars!"
      def quest_guide_hint(quest):
        if quest["green_star_interacted"]:
          return "Interact with the skeleton in the [nurse]'s office."
        else:
          return "Interact with the green star in your inventory."
    class phase_11_stargold:
      def hint(quest):
        if quest["black_star_interacted"]:
          return "{i}\"I'm where no man dares to tread.\nA land of bowls and pipes of lead.\"{/}"
        else:
          return "I know you're semi-illiterate, but if you're in the gutter, look at the stars!"
      def quest_guide_hint(quest):
        if quest["black_star_interacted"]:
          return "Go to the women's bathroom."
        else:
          return "Interact with the black star in your inventory."
    class phase_12_bathroomhelp:
      hint = "All winners have friends. You don't have to go through this alone."
      quest_guide_hint = "Quest [isabelle]."
    class phase_13_endnurse:
      hint = "{i}\"To the victor goes the spoils.\"{/}\nThis time it's medic."
      quest_guide_hint = "Quest the [nurse]."
    class phase_14_encounter:
      def hint(quest):
        if quest["balls_busted"]:
          return "Leave the [nurse]'s office."
        else:
          return "Leave the English classroom."
    class phase_15_computer:
      hint = "Interact with your computer."
    class phase_16_camera:
      hint = "Use [maxine]'s nude on the old camera in her office."
    class phase_17_delivery:
      hint = "Quest [kate]."
    class phase_1000_done:
      description = "Sometimes rocking the boat can shake things up. Things might've changed forever."

  class Event_quest_kate_desire(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if "computer" > quest.kate_desire > "gym" and not quest.kate_desire == "endnurse":
        return 0

    def on_can_advance_time(event):
      if "computer" > quest.kate_desire > "gym" and not quest.kate_desire == "endnurse":
        return False

    def on_advance(event):
      if quest.kate_desire == "rest" and quest.kate_desire["rested"]:
        game.events_queue.append("quest_kate_desire_bed_sleep")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_desire":
        if quest.kate_desire == "gym":
          kate.unequip("kate_necklace")
          kate.equip("kate_cheerleader_top")
          kate.equip("kate_cheerleader_bra")
          kate.equip("kate_cheerleader_skirt")
          kate.equip("kate_cheerleader_panties")
        if quest.kate_desire == "starblack":
          school_ground_floor_west["nurse_room_locked_now"] = school_ground_floor_west["nurse_room_locked_today"] = False
        if quest.kate_desire == "stargold":
          school_art_class["exclusive"] = False
        if quest.kate_desire in ("endnurse","computer"):
          kate.equip("kate_necklace")
          kate.equip("kate_shirt")
          kate.equip("kate_bra")
          kate.equip("kate_pants")
          kate.equip("kate_panties")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "kate_desire":
        kate.equip("kate_necklace")
        kate.equip("kate_shirt")
        kate.equip("kate_bra")
        kate.equip("kate_pants")
        kate.equip("kate_panties")
        mc["focus"] = ""

    def on_quest_guide_kate_desire(event):
      rv = []

      if quest.kate_desire == "challenge":
        rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "kate", marker = ["actions quest"]))
      elif quest.kate_desire == "advice":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["isabelle contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_ground_floor_west", "isabelle", marker = ["actions quest"]))
      elif quest.kate_desire == "rest":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))
      elif quest.kate_desire == "dress":
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_closet", marker = ["actions interact"]))
      elif quest.kate_desire == "gym":
        rv.append(get_quest_guide_path("school_gym", marker = ["kate contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_gym", "kate", marker = ["actions quest"], marker_offset = (50,25)))
      elif quest.kate_desire == "starblue":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"]))
        rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_aquarium", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (200,200)))
      elif quest.kate_desire == "starred":
        if quest.kate_desire["blue_star_interacted"]:
          rv.append(get_quest_guide_path("school_entrance", marker = ["school entrance bus@.3"]))
          rv.append(get_quest_guide_marker("school_entrance", "school_entrance_bus", marker = ["actions interact"]))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["items paper_star_blue", "actions interact@.85"], marker_offset = (60,0), marker_sector = "mid_top"))
      elif quest.kate_desire == "stargreen":
        if quest.kate_desire["red_star_interacted"]:
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_piano", marker = ["actions interact"], marker_offset = (150,250)))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["items paper_star_red", "actions interact@.85"], marker_offset = (60,0), marker_sector = "mid_top"))
      elif quest.kate_desire == "holdup":
        rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_back", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_first_hall_west", "kate", marker = ["$or{space=-50}"]+["actions quest"]+["$"], marker_offset = (100,0)))
      elif quest.kate_desire == "starblack":
        if quest.kate_desire["green_star_interacted"]:
          rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room skeleton@.25"]))
          rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_skeleton", marker = ["actions interact"], marker_offset = (110,0), marker_sector = "left_bottom"))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["items paper_star_green", "actions interact@.85"], marker_offset = (60,0), marker_sector = "mid_top"))
      elif quest.kate_desire == "stargold":
        if quest.kate_desire["black_star_interacted"]:
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
          rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_door_bathroom", marker = ["actions go"]))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["items paper_star_black", "actions interact@.85"], marker_offset = (60,0), marker_sector = "mid_top"))
      elif quest.kate_desire == "bathroomhelp":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
      elif quest.kate_desire == "endnurse":
        rv.append(get_quest_guide_char("nurse", marker = ["nurse contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "nurse", marker = ["actions quest"]))
      elif quest.kate_desire == "encounter":
        if quest.kate_desire["balls_busted"]:
          rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_door", marker = ["actions go"], marker_offset = (60,40)))
        else:
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_door", marker = ["actions go"]))
      elif quest.kate_desire == "computer":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["actions interact"]))
      elif quest.kate_desire == "camera":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["school clubroom camera@.8"]))
        rv.append(get_quest_guide_marker("school_clubroom", "school_clubroom_old_camera", marker = ["items maxine_nude@.9"]))
      elif quest.kate_desire == "delivery":
        rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "kate", marker = ["actions quest"]))

      return rv


label quest_kate_desire_holdup_leave:
  show kate excited with Dissolve(.5)
  kate excited "Hey, where do you think you're going?"
  mc "To win this challenge. You?"
  kate annoyed_right "Where did you find the green star?"
  mc "Just follow the clues..."
  jump quest_kate_desire_holdup

label school_entrance_bus_stop_kate_desire:
  "Ride the bus off into the sunset? Where's the end of the day when you need it?"
  return

label quest_kate_desire_nude_model:
  $kate["outfit_stamp"] = kate.outfit
  $kate.outfit = {"bra": None, "necklace": None, "panties": None, "pants": None, "shirt": None}
  if kate["interacted_posed"]:
    show kate excited with Dissolve(.5)
    mc "Would you please show me that pose again?"
    jump quest_kate_desire_nude_model_replay
  if not quest.kate_desire["first_visit"]:
    show kate eyeroll with Dissolve(.5)
    kate eyeroll "I had a feeling you'd show up sooner rather than later."
    mc "I'm just here to get my grade, that's all."
    kate excited "Yeah? You're getting a D later, that's for sure."
    "[kate] threatening me while naked is slightly less frightening than usual...{space=-75}"
    $quest.kate_desire["first_visit"] = True
  if not renpy.showing("kate"):
    show kate excited with Dissolve(.5)
  mc "Can you pose for me, please?"
  kate excited "Absolutely."
  kate thinking "..."
  mc "That's..."
  kate blush "...not what you had in mind?"
  show kate blush at move_to(.75)
  menu(side="left"):
    extend ""
    "?jacklyn.love>=10@[jacklyn.love]/10|{image= jacklyn contact_icon}|{image= stats love_3}|\"Actually, this is better.\"":
      show kate blush at move_to(.5)
      mc "Actually, this is better."
      mc "I don't think anyone has previously captured you in such a vulnerable state."
      kate thinking "What?"
      mc "I'll paint you with blushing cheeks — nervous, embarrassed, shy."
      kate angry "No, you won't!"
      "Oh, wow... It seems like I've struck a nerve. Who would've thought?"
      mc "It's too late. The image of you covering yourself is forever seared into my mind."
      kate sad "There's no way you'll even succeed in painting a face, much less a face like mine..."
      if school_art_class["nude_model"] == "jacklyn":
        $jacklyn["outfit_stamp"] = jacklyn.outfit
        $jacklyn.outfit = {"bra": None, "panties": None, "pants": None, "pin": None, "shirt": None}
      show kate sad at move_to(.25,1.0)
      show jacklyn smile at appear_from_right(.75)
      jacklyn smile "I wouldn't put my bank on that piggy if I were you."
      jacklyn smile "Bowl-cut here has some real talent."
      mc "Thank you, [jacklyn]!"
      kate embarrassed "No freaking way!"
      mc "Oh, that's the exact pose I'm looking for! Please, stand still!"
      jacklyn laughing "I'll align my stars with yours on that."
      jacklyn laughing "That's raw, [kate]! Highly non-clich!"
      kate annoyed "And why should I care? It's his grade, not mine."
      jacklyn excited "I'll tell you what's up and down — credits for both of you."
      kate skeptical "And I'll have the rights to the painting when you've graded it."
      mc "Deal!"
      kate eyeroll "Fine... let's get this over with, then..."
      jacklyn laughing "Sparkling!"
      show kate eyeroll at move_to(.5)
      hide jacklyn with Dissolve(.5)
      mc "Come on then, [kate]! Pose for me!"
      kate skeptical "If you tell me what to do one more time, I'll have Chad break your legs.{space=-50}"
      mc "Sorry! Would you please show me that pose again?"
      label quest_kate_desire_nude_model_replay:
      kate afraid "Like this?"
      mc "Yeah, that's good!"
      mc "Now, can you think of Chad finding your secret diary?"
      kate blush "..."
      "Ugh, that's not exactly what I had in mind..."
      mc "How about me finding your secret diary?"
      kate embarrassed "..."
      mc "There it is! Perfection!"
      mc "Now could you please hold that pose for a moment?"
      kate embarrassed "Be quick about it. I don't like this."
      mc "I'll do my best..."
      if kate["interacted_posed"]:
        hide kate with Dissolve(.5)
        jump quest_kate_desire_nude_model_end
      window hide
      show black with Dissolve(.5)
      pause 1.0
      if game.hour != 18:
        $game.advance()
      pause 1.0
      hide black
      show kate skeptical
      with Dissolve(.5)
      window auto
      kate skeptical "Well, are you done?"
      mc "Yeah, it's done."
      show kate skeptical at move_to(.25,1.0)
      show jacklyn excited at appear_from_right(.75)
      if not quest.kate_desire["nude_model_jacklyn_stat_gained"]:
        $jacklyn.love+=5
        $quest.kate_desire["nude_model_jacklyn_stat_gained"] = True
      jacklyn excited "That is baller! Straight up Matisse!"
      kate eyeroll "Well, have that shit graded and sent to me. No one else can see it."
      kate eyeroll "Buh-bye."
      hide kate
      hide jacklyn
      with Dissolve(.5)
      if school_art_class["nude_model"] == "jacklyn":
        $jacklyn.outfit = jacklyn["outfit_stamp"]
      "Well, that was certainly interesting..."
      "Not sure I've ever seen [kate] so flustered."
      "It's a shame I can't keep the painting... You win some, you lose some, I guess."
      $kate["interacted_posed"] = True
    "\"I'm trying to learn the, err... curves of the... female anatomy.\"":
      show kate blush at move_to(.5)
      mc "I'm trying to learn the, err... curves of the... female anatomy."
      kate laughing "I bet you are!"
      kate smile "The eternal virgin. That's what it'll say on your tombstone."
      mc "That's perfect..."
      mc "The pose, I mean."
      mc "How do you feel?"
      kate laughing "That's adorable! You think you can rattle me?"
      kate excited "I could perform the whole cheerleader routine naked in front of the whole school."
      kate excited "It wouldn't bother me one bit."
      "I guess she's right. [kate] is perfect in every way and she's not afraid to show it."
      if not quest.kate_desire["nude_model_kate_stat_gained"]:
        $kate.lust+=2
        $quest.kate_desire["nude_model_kate_stat_gained"] = True
      kate excited "What's wrong? Did you think I'd be embarrassed?"
      mc "I guess not..."
      kate neutral "Go ahead, then. Get your C or whatever your goal in life is."
      kate neutral "I'll pose like this, and you'll be quiet and paint. Understood?"
      mc "Yeah... understood."
      "..."
      "I can't believe she just commanded me while naked."
      "This is a new low, and I'm the one embarrassed."
      "This is a weird boner..."
      hide kate with Dissolve(.5)
  label quest_kate_desire_nude_model_end:
  $kate.outfit = kate["outfit_stamp"]
  return
