init python:
  class Quest_jo_potted(Quest):
    title = "Potted Weeds"
    class phase_1_computer:
      description = "A deep-throat kind of issue."
      hint = "Every quest starts on the internet, at least that's what video games have taught me."
      quest_guide_hint = "Interact with Bedroom Computer."
    class phase_2_package:
      hint = "Man, I'm looking forward to getting my hands on that package. Is that a straight thing to say?"
      quest_guide_hint = "Wait 2 days, then interact with Mysterious Package in the Kitchen."
    class phase_3_plow:
      hint = "If you're wet, you can't plow the field. No, not the dirty kind of wet."
      quest_guide_hint = "Use Umbrella on Creepy Hut in the Forest Glade. Use Power-Plower 3000 on Meadow."
    class phase_4_getnurse:#3
      hint = "What do you call someone who takes good care of farmlands? A field nurse."
      quest_guide_hint = "Quest the [nurse]."
    class phase_5_plowcow:#3
      hint = "You can absolutely lead the cow to the field and make it plow. Horses just suck at drinking."
      quest_guide_hint = "Go to the Forest Glade."
    class phase_6_plant:
      hint = "Just jiggle it until the seeds pour out. This isn't a new concept."
      quest_guide_hint = "Use Gigglypuff Seeds on Agricultured Meadow."
    class phase_7_seeds:#7
      def hint(quest):
        if school_forest_glade["farm"] == "dry":
          return "Let's see who's more thirsty — me or the field."
        else:
          if school_forest_glade["pollution"]==3 and school_forest_glade['canal']:
            return "Wait for the dark minions of corruption to rise from the field of cropses!"
          else:
            return "Everything takes time, especially growing babies."

      def quest_guide_hint(quest):
        if school_forest_glade["farm"] == "dry":
          return "Use Bottle of Water 3 times on Agricultured Meadow."
        else:
          return "Wait 2 days for the seeds to grow."

    class phase_8_small:#7

      def hint(quest):
        if school_forest_glade["farm"] == "dry":
          return "Last time, the field was more thirsty. This time it comes down to the bottle. Also, birds."
        else:
          if school_forest_glade["pollution"]==3 and school_forest_glade['canal']:
            return "Last time, the field was more thirsty. This time it comes down to the bottle. Also, birds."
          else:
            return "If patience is a virtue, does that mean impatience is a sin? Also, screw birds."

      def quest_guide_hint(quest):
        if school_forest_glade["farm"] == "dry":
          return "Use Bottle of Water 3 times on Agricultured Meadow. Scare off the birds."
        else:
          return "Wait 2 days for the plants to grow. Scare off birds every day."

    class phase_9_medium:#7

      def hint(quest):
        if school_forest_glade["farm"] == "dry":
          return "This son of a bitch is drinking up all my hard earned water... but I'll give him a second chance. I mean third. Also, fuck birds."
        else:
          if school_forest_glade["pollution"]==3 and school_forest_glade['canal']:
            return "From the dark depths of the earth they claw out into the sunlight! There won't be much longer now! Also, fucking brids."
          else:
            return "Sucks to be stuck in a situation where all you can do is wait. Like life. Also, ****ing bitch birds."

      def quest_guide_hint(quest):
        if school_forest_glade["farm"] == "dry":
          return "Use Bottle of Water 3 times on Agricultured Meadow. Scare off the birds."
        else:
          return "Wait 2 days for the plants to grow. Scare off birds every day."

    class phase_10_big:#7
      hint = "Finally, the fruit of my labor. Harvest moon is upon us!"
      quest_guide_hint = "Interact with Agricultured Meadow."
    class phase_11_tea:
      hint = "What does a kettle, stove, and a bottle of water have in common? Gigglypuff tea."
      quest_guide_hint = "Use Kettle on Stove in the Kitchen. Use Bottle of Water and Gigglypuff Leaves on Kettle."
    class phase_12_boil:
      hint = "Sometimes lightning strikes more than once, especially if you're trying to boil water with a lamp."
      quest_guide_hint = "Use Lamp on Kettle."
    class phase_13_jodrink:
      hint = "Making [jo] swallow. That's the goal here."
      def quest_guide_hint(quest):
        if quest["school_talk"]:
          return "Quest [jo] at home."
        else:
          return "Quest [jo]."
    class phase_1000_done:
      description = "Life by the seeds. The best farmer in my field."

  class Event_quest_jo_potted(GameEvent):
    def on_quest_guide_jo_potted(event):
      rv = []

      if quest.jo_potted == "computer":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))

      if quest.jo_potted == "package":
        if home_kitchen["package"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.65"]))
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_package_gigglypuff_seeds", marker = ["actions take"]))
        else:
          if mc.owned_item("package_gigglypuff_seeds"):
            rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
          else:
            rv.append(get_quest_guide_hud("time", marker = ["items package", "$Wait two days."]))

      if quest.jo_potted == "plow":
        if mc.owned_item("power_plower"):
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["items plow"]))
          rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_meadow", marker = ["items plow"]))
        else:
          if mc.owned_item("umbrella"):
            rv.append(get_quest_guide_path("school_forest_glade", marker = ["items umbrella"]))
            rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_house", marker = ["items umbrella"]))
          else:
            rv.append(get_quest_guide_path("home_hall", marker = ["items umbrella"]))
            rv.append(get_quest_guide_marker("home_hall","home_hall_umbrella", marker = ["items umbrella"]))

      if quest.jo_potted == "getnurse":
        if quest.kate_search_for_nurse.in_progress:
          game.quest_guide = "kate_search_for_nurse"
        else:
          rv.append(get_quest_guide_char("nurse",marker = ["nurse contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "nurse", marker = ["actions quest"]))

      if quest.jo_potted == "plowcow":
        rv.append(get_quest_guide_path("school_forest_glade",marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_forest_glade","nurse",marker = ["actions quest"]))

      if quest.jo_potted == "plant":
        rv.append(get_quest_guide_path("school_forest_glade",marker = ["items gigglypuff_seeds@.85"]))
        rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_earth",marker = ["items gigglypuff_seeds@.85"], marker_offset = (60,30)))

      if quest.jo_potted in ("seeds", "small", "medium"):
        if school_forest_glade["farm"] == "dry":
          if mc.owned_item("shovel"):
            rv.append(get_quest_guide_path("school_forest_glade",marker = ["items shovel@.8"]))
            rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_earth",marker = ["items shovel@.8"], marker_offset = (60,30)))
          else:
            if not mc.owned_item("stick"):
              rv.append(get_quest_guide_path("school_forest_glade",marker = ["items stick_1"]))
              rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_wood",marker = ["items stick_1"]))
            if not mc.owned_item(("soup_can","empty_can")):
              rv.append(get_quest_guide_path("school_locker",marker = ["items soupcan"]))
              #rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["school locker lunchfinal@.25"], marker_offset = (100,0), marker_sector = "left_bottom"))
              rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["items soupcan"]))
            if mc.owned_item("empty_can") and mc.owned_item("stick"):
              rv.append(get_quest_guide_hud("inventory", marker = ["items empty_can", "actions combine", "items stick_1"]))
            elif not mc.owned_item("empty_can") and mc.owned_item("soup_can"):
              rv.append(get_quest_guide_hud("inventory", marker = ["items soupcan", "actions consume"]))
        elif not "drugged" in school_forest_glade["birds"].values() and  not "ground" in school_forest_glade["birds"].values():
          rv.append(get_quest_guide_hud("time", marker="$Wait a day"))

        x = 0
        while x<6:
          if school_forest_glade['birds'][x]=="ground":
            rv.append(get_quest_guide_path("school_forest_glade",marker = ["school forest_glade bird5_ground@2"]))
            rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_bird"+str(x+1)+"_ground", marker_offset = (15,-15), marker = ["actions interact@.5"], marker_sector = "mid_bottom"))
            rv.append(get_quest_guide_hud("time", marker=["$Clear the field", "school forest_glade bird5_ground@2","$before the day ends."]))
          if school_forest_glade['birds'][x]=="drugged":
            rv.append(get_quest_guide_path("school_forest_glade",marker = ["school forest_glade bird5_ground"]))
            rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_bird"+str(x+1)+"_drugged", marker_offset = (15,-15), marker = ["actions interact@.5"], marker_sector = "mid_bottom"))
            rv.append(get_quest_guide_hud("time", marker=["$Clear the field", "school forest_glade bird5_ground@2","$before the day ends."]))
          #if not school_forest_glade['birds'][x]=="drugged" and not school_forest_glade['birds'][x]=="drugged" and school_forest_glade["farm"] == "wet":
          #  rv.append(get_quest_guide_hud("time", marker="$Wait a day"))
          x+=1

      if quest.jo_potted == "big":
        if not mc.owned_item("gigglypuff_leaves") or  not mc.owned_item("corrupted_gigglypuff_leaves"):
          rv.append(get_quest_guide_path("school_forest_glade",marker = ["items gigglypuffleaves@.8"]))
          rv.append(get_quest_guide_marker("school_forest_glade","school_forest_glade_earth",marker = ["actions interact"], marker_offset = (60,30)))

      if quest.jo_potted == "tea":
        if home_kitchen["kettle"] == "stove":
          if not quest.jo_potted['water_added']:
            if mc.owned_item("water_bottle"):
              rv.append(get_quest_guide_path("home_kitchen", marker = ["items kettle"]))
              rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_kettle_stove", marker = ["items bottle water_bottle"], marker_sector = "left_bottom", marker_offset = (50,0)))
            elif mc.owned_item("empty_bottle"):
              if not quest.lindsey_wrong["fountain_state"]:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.7"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["items bottle empty_bottle"], marker_sector = "left_bottom"))
              else:
                rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"], marker_offset = (50,100)))
                rv.append(get_quest_guide_marker("school_ground_floor_west","school_ground_floor_west_aquarium", marker = ["items bottle empty_bottle"]))
            elif mc.owned_item(("water_bottle", "strawberry_juice", "banana_milk", "salted_cola","seven_hp","pepelepsi")):
              x = mc.owned_item(("water_bottle", "strawberry_juice", "banana_milk", "salted_cola","seven_hp","pepelepsi"))
              rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x,"actions consume"], marker_offset = (60,0), marker_sector = "mid_top"))
            elif not home_kitchen["water_bottle_taken"]:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["items bottle water_bottle"]))
              rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["items bottle water_bottle"]))
            else:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["items bottle water_bottle"] ))
              rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"], marker_sector = "mid_bottom", marker_offset = (100,0) ))
          if not quest.jo_potted['leaves_added']:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["items kettle"]))
            rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_kettle_stove", marker = ["items gigglypuffleaves"], marker_sector = "left_bottom", marker_offset = (50,0)))
        else:
          if not mc.owned_item("kettle"):
            rv.append(get_quest_guide_path("home_kitchen", marker = ["items kettle"]))
            rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_kettle_counter", marker = ["actions take"]))
          else:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["items kettle"]))
            rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_stove", marker = ["actions use_item", "items kettle"], marker_sector = "right_bottom", marker_offset = (150,50)))

      if quest.jo_potted == "boil":
        if not mc.owned_item("lamp"):
          rv.append(get_quest_guide_path("home_hall", marker = ["items lampshade"]))
          rv.append(get_quest_guide_marker("home_hall","home_hall_lamp", marker = ["actions take"]))
        else:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["items kettle"]))
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_kettle_stove", marker = ["items lamp"], marker_sector = "left_bottom", marker_offset = (50,0)))

      if quest.jo_potted == "jodrink":
        if quest.jo_potted["school_talk"]:
          if jo.location == "home_kitchen":
            rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
            rv.append(get_quest_guide_marker("home_kitchen", "jo", marker = ["actions quest"]))
          else:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
            if game.location == "home_kitchen":
              rv.append(get_quest_guide_hud("time", marker="$\nWait for {color=#48F}[jo]{/} to come\nback from school."))
        else:
          rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      return rv






    #location flags --------------------------------------------------------------
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.jo_potted >= "seeds" and quest.jo_potted < "big":
        if new_location == "school_forest_glade" and quest.jo_potted["eaten"]:
          game.events_queue.append("jo_quest_potted_crops_eaten")
          quest.jo_potted["eaten"] = False
    #Timers-----------------------------------------------------------------
    def on_advance(event):
      quest.jo_potted.flags = {k:v for k,v in quest.jo_potted.flags.items() if not k.endswith("_now")}
      if game.hour == 0:# now/today flags from default flagsmixin
        quest.jo_potted.flags = {k:v for k,v in quest.jo_potted.flags.items() if not k.endswith("_today")}
      #--- added functionality for potted quest
      #package counter
      if quest.jo_potted == "package":
        if not quest.jo_potted["package_today"] and quest.jo_potted["days"]<2:
          quest.jo_potted["package_today"] = True #package delivery timer
          quest.jo_potted["days"] += 1
          if quest.jo_potted["days"] == 2:
            home_kitchen["package"] = True

      if quest.jo_potted >= "seeds" and quest.jo_potted < "big":
        if game.hour == 0:#farm timer and bird timers
          if quest.jo_potted > "seeds":
            school_forest_glade["birds"] = {k:("drugged" if v == "ground" else v) for k,v in school_forest_glade["birds"].items()}
            if "drugged" in school_forest_glade["birds"].values():
              quest.jo_potted.advance(-1)
              quest.jo_potted["eaten"] = True
              return
            #if school_forest_glade["farm"] == "dry":
            #  quest.jo_potted.advance(-1)
            #  return
            quest.jo_potted["bird_swoop"] = random.randint(9, 12)

          quest.jo_potted["days"] += 1
          if quest.jo_potted["days"]>=1:
            if school_forest_glade["farm"] == "wet":
              quest.jo_potted.advance()

          #if quest.jo_potted["days"]<1:
          #  quest.jo_potted["days"] += 1
          #else:
          #  quest.jo_potted.advance()
        else:
          if quest.jo_potted > "seeds":
            if quest.jo_potted["bird_swoop"]:
              if isinstance(quest.jo_potted["bird_swoop"],int):
                quest.jo_potted["bird_swoop"]-=1
            else:
              quest.jo_potted["bird_swoop"] = "done"
              if game.hour<19:
                school_forest_glade["birds"] = {k:("ground" if v == "roof" else v) for k,v in school_forest_glade["birds"].items()}

    #On quest advance   ------------------------------------------------------------
    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "jo_potted":
        quest.jo_potted["days"] = 0
        if quest.jo_potted == "plant":
          school_forest_glade["birds"] = {k:"roof" for k in range(6)}
        if quest.jo_potted >="seeds" and quest.jo_potted < "big":
          if not school_forest_glade["canal"]:
            school_forest_glade["farm"] = "dry"
            quest.jo_potted["water"] = 0

    #Quest hints -------------------------------------------------------------
    def on_enter_roaming_mode(event):
      if quest.jo_potted.in_progress:# and  quest.jo_potted>"plant" and quest.jo_potted<"big":
        hint_holder = ""
        quest.jo_potted["hint_holder"] = ""
        quest_guide_hint_holder =""
        if quest.jo_potted == "seeds":
          if school_forest_glade["farm"] == "dry":
            hint_holder = "Let's see who's more thirsty — me or the field."
            quest_guide_hint_holder = "Use Bottle of Water 3 times on Agricultured Meadow."
          else:
            if school_forest_glade["pollution"]==3 and school_forest_glade['canal']:
              hint_holder = "Wait for the dark minions of corruption to rise from the field of cropses!"
              quest_guide_hint_holder = "Wait 2 days for the plants to grow."
            else:
              hint_holder = "Everything takes time, especially growing babies."
              quest_guide_hint_holder = "Wait 2 days for the seeds to grow."
        elif quest.jo_potted == "small":
          if school_forest_glade["farm"] == "dry":
            hint_holder = "Last time, the field was more thirsty. This time it comes down to the bottle. Also, birds."
            quest_guide_hint_holder = "Use Bottle of Water 3 times on Agricultured Meadow. Scare off the birds."
          elif school_forest_glade["farm"] == "wet":
            if school_forest_glade["pollution"]==3 and school_forest_glade['canal']:
              hint_holder = "The darkness grows within and around me. How longer must I wait? Also, birds."
              quest_guide_hint_holder = "Wait 2 days for the plants to grow. Scare off birds every day."
            else:
              hint_holder = "If patience is a virtue, does that mean impatience is a sin? Also, screw birds."
              quest_guide_hint_holder = "Wait 2 days for the plants to grow. Scare off birds every day."
        elif quest.jo_potted == "medium":
          if school_forest_glade["farm"] == "dry":
            hint_holder = "This son of a bitch is drinking up all my hard earned water... but I'll give him a second chance. I mean third. Also, fuck birds."
            quest_guide_hint_holder = "Use Bottle of Water 3 times on Agricultured Meadow. Scare off the birds."
          elif school_forest_glade["farm"] == "wet":
            if school_forest_glade["pollution"]==3 and school_forest_glade['canal']:
              hint_holder = "From the dark depths of the earth they claw out into the sunlight! There won't be much longer now! Also, fucking brids."
              quest_guide_hint_holder = "Wait 2 days for the plants to grow. Scare off birds every day."
            else:
              hint_holder = "Sucks to be stuck in a situation where all you can do is wait. Like life. Also, ****ing bitch birds."
              quest_guide_hint_holder = "Wait 2 days for the plants to grow. Scare off birds every day."

label jo_quest_potted_crops_eaten:
  "Son of squawking bitch! The birds ate the crops!"
  "Some of them are flapping around looking high as fuck."
  $achievement.flying_high_again.unlock()
  return
