init python:
  class Quest_kate_over_isabelle(Quest):

    title = "Stepping on the Rose"

    class phase_1_the_winning_team:
      hint = "It's always about playing it smart. Picking the winning team. Kissing up to [kate]."

    class phase_2_talking_to_kate:
      pass

    class phase_3_spy_focus:
      hint = "A spy. A covert operative. An undercover agent. Someone who sort of... learns [isabelle]'s schedule. Yeah, very badass."

    class phase_4_spy_drink:
      hint = "A beverage informant about to unveil the greatest mystery of our time — [isabelle]'s favorite drink."

    class phase_5_report:
      hint = "For services rendered, a reward awaits. The queen will be most gracious."

    class phase_6_search_and_schedule:
      def hint(quest):
        if sum([mc.owned_item("dog_collar"),mc.owned_item("handcuffs"),mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills")]) == 4:
          return "Schedule a meeting with the oracle on the upper floor."
        else:
          return "Acquire four magical artifacts: a dog collar, handcuffs, an ice tea, and sleeping pills."

    class phase_1000_done:
      description = "There's only picking one side in this war. Will you survive? Probably not."

    class phase_2000_failed:
      description = "There's only picking one side in this war. Will you survive? Probably not."


init python:
  class Event_quest_kate_over_isabelle(GameEvent):

    def on_quest_guide_kate_over_isabelle(event):
      rv=[]

      if quest.kate_over_isabelle == "the_winning_team":
        rv.append(get_quest_guide_char("kate", marker = ["actions quest"], force_marker = True))
        rv.append(get_quest_guide_path(mc.location, marker = ["kate contact_icon@.85"]))

      elif quest.isabelle_tour == "english_class":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))

      if quest.isabelle_tour == "art_class":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_art_class", marker = ["isabelle contact_icon@.85"]))

      if quest.isabelle_tour == "gym":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_gym", marker = ["isabelle contact_icon@.85"]))

      if quest.isabelle_tour == "cafeteria":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["isabelle contact_icon@.85"]))

      if quest.kate_over_isabelle == "report" and quest.isabelle_tour.ended:
        rv.append(get_quest_guide_char("kate", marker = ["actions quest"], force_marker = True))
        rv.append(get_quest_guide_path(mc.location, marker = ["kate contact_icon@.85"]))

      if quest.kate_over_isabelle == "search_and_schedule":
        if sum([mc.owned_item("dog_collar"),mc.owned_item("handcuffs"),mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills")]) == 4: ## "Schedule a meeting with the oracle on the upper floor."
          if quest.maxine_eggs.finished:
            rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "kate", marker = ["actions quest"]))
          elif quest.maxine_eggs.in_progress:
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}The Glowing Spider Eggs{/}."))
          elif quest.clubroom_access.finished:
            rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
            if maxine.at("school_clubroom","sitting"):
              rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75+75,25)))
            else:
              if mc.at("school_clubroom") and not maxine.at("school_clubroom"):
                rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
          elif quest.clubroom_access.in_progress:
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Oxygen for Oxymoron{/}."))
          else:
            rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.4"]))
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["actions quest"], marker_sector="right_top", marker_offset = (0,-220)))
        else:
          if sum([mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills"),mc.owned_item("handcuffs")]) >= 2 and not mc.owned_item("dog_collar"):
            if quest.kate_trick.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}The Dog Trick{/}."))
            else:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["actions go"]))
          if not mc.owned_item("handcuffs"):
            if quest.flora_handcuffs.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Stainless Steal{/}."))
            else:
              rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))
          if not mc.owned_item("ice_tea"):
            if quest.isabelle_piano.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Chops and Nocs{/}."))
            else:
              if quest.piano_tuning.in_progress:
                if quest.piano_tuning == "cabinet":
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
                elif quest.piano_tuning == "baton":
                  if school_music_class["got_conductor_baton"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
                elif quest.piano_tuning == "hammer":
                  if school_first_hall_west["tuning_hammer"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    if mc.owned_item("tuning_hammer"):
                      rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                      rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
                    else:
                      if school_music_class["middle_compartment"]:
                        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                        rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                      else:
                        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                        rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                elif quest.piano_tuning == "fork":
                  if school_music_class["bottom_compartment"]:
                    if school_music_class["middle_compartment"]:
                      rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                      rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                    else:
                      if school_music_class["top_compartment"] == "tuning_fork":
                        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                        rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                      elif school_music_class["top_compartment"] == "conductor_baton":
                        if school_first_hall_west["tuning_fork"]:
                          rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                          rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                        else:
                          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                          rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
                  else:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                if quest.piano_tuning.finished:
                  rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\nsweet and romantic{space=-50}","actions quest","${space=275}"], marker_offset = (150,250)))
                else:
                  rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["actions quest"], marker_offset = (150,250)))
          if not mc.owned_item("sleeping_pills"):
            if quest.nurse_photogenic.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Photogenic{/}."))
            else:
              if quest.nurse_photogenic["strawberry_juice_consumed_today"]:
                rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor main_stairs@.3"]))
                rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_stairs", marker = ["actions go"], marker_sector="mid_bottom", marker_offset = (220,0)))
              else:
                if mc.owned_item(("banana_milk","spray_banana_milk","salted_cola","spray_salted_cola","pepelepsi","spray_pepelepsi","seven_hp","spray_seven_hp","strawberry_juice","spray_strawberry_juice","spray_urine","water_bottle","spray_water")):
                  x = mc.owned_item(("banana_milk","spray_banana_milk","salted_cola","spray_salted_cola","pepelepsi","spray_pepelepsi","seven_hp","spray_seven_hp","strawberry_juice","spray_strawberry_juice","spray_urine","water_bottle","spray_water"))
                  rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x,"actions consume@.85"], marker_sector = "mid_top", marker_offset = (60,0)))
                elif mc.owned_item(("empty_bottle","spray_empty_bottle")):
                  x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
                  rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
                  rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["fill_"+x+"bottle_with_strawberry_juice"], marker_sector="right_top", marker_offset = (50,125)))
                else:
                  if home_kitchen["water_bottle_taken"]:
                    rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
                    rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
                  else:
                    rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
                    rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))

      return rv
