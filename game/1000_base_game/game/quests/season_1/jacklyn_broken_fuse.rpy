image lollipop_jar = LiveComposite((138,173),(42,60),"school nurse_room cup",(85,17),"school nurse_room sticks",(0,0),"school nurse_room lollipop")
image combine_plastic_fork_lollipop = LiveComposite((273,125),(25,45),Transform("items plastic_fork",zoom=0.65),(95,31),Transform("actions combine",zoom=0.85),(193,43),Transform("items lollipop_2",zoom=0.7))
image random_consumable = LiveComposite((249,95),(0,0),"items random_consumables_1",(107,0),"ui random",(154,0),"items random_consumables_2")
image combine_broken_cable_electrical_tape = LiveComposite((273,125),(-8,32),"items frayed_cable",(95,31),Transform("actions combine",zoom=0.85),(193,43),Transform("items electrical_tape",zoom=0.7))


init python:
  class Quest_jacklyn_broken_fuse(Quest):

    title = "A Short Fuse"

    class phase_1_start:
      description = "Light is the key to all art.\nExcept for music, but we\ndon't mention that here."
#     hint = "Quest [jacklyn]."

    class phase_2_investigate_fusebox:
      hint = "The search for power begins with a box."
      quest_guide_hint = "Interact with the fuse box in the first hall."

    class phase_3_find_fork:
#     description = "Insult or insulator — learning the difference could save your life."
      hint = "[flora] complained about her art grades for the entire semester. Sometimes I just want to fork her."
      quest_guide_hint = "Craft a specialized fuse cable replacement tool by combining the plastic fork with the big lollipop, then use it on the fuse box in the first hall."

    class phase_4_reach:
#     hint = "The fork is not long enough. Might need something sweet and sticky."
      pass

    class phase_5_fix:
#     description = "At least it's not a broken heart."
      hint = "I heard through the grapevine someone is offering tape through the desk exchange program."
      quest_guide_hint = "Use any consumable on the leftmost desk in the English classroom. The next day, take the electrical tape from the same desk and craft a fixed cable by combining it with the broken cable. Finally, use it on the fuse box in the first hall."

    class phase_6_reward:
      hint = "[jacklyn]'s precious lighting has been saved. Surely, there's a reward."
      quest_guide_hint = "Quest [jacklyn] in the art classroom."

    class phase_1000_done:
      description = "Saving art classes and grades\nleft and right.\n\nSome heroes don't wear a cape,\nthey wear electrical gloves."

#   class phase_2000_failed:
#     description = "Some things just can't be fixed. A short fuse is apparently one of them."


init python:
  class Event_quest_jacklyn_broken_fuse(GameEvent):

    def on_quest_guide_jacklyn_broken_fuse(event):
      rv=[]

      if quest.jacklyn_broken_fuse == "investigate_fusebox":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
        rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["actions interact"], marker_offset = (60,90)))

      elif quest.jacklyn_broken_fuse == "find_fork":
        if mc.owned_item("specialized_fuse_cable_replacement_tool"):
          rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power",marker = ["items specialized_fuse_cable_replacement_tool@.8"], marker_offset = (60,90)))
        else:
          if not mc.owned_item("plastic_fork"):
            if quest.flora_jacklyn_introduction.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Punked{/}."))
            else:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
              if flora.at("home_kitchen"):
                rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"], marker_offset = (40,0)))
              else:
                if mc.at("home_kitchen"):
                  rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} at "+("{color=#48F}18:00{/}." if persistent.time_format == "24h" else "{color=#48F}6:00 PM{/}.")))
          if not mc.owned_item("lollipop_2"):
            if school_nurse_room["lollipop_taken"]:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
              rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["$Buy a","items lollipop_2@.75","$for {color=#48F}$50{/}"]))
            else:
              rv.append(get_quest_guide_path("school_nurse_room", marker = ["lollipop_jar@.5"]))
              if kate.at("school_nurse_room"):
                if mc.at("school_nurse_room"):
                  rv.append(get_quest_guide_hud("time", marker = "$Wait for {color=#48F}[kate]{/} to leave\nthe {color=#48F}[nurse]'s office{/}."))
              elif quest.kate_search_for_nurse.in_progress:
                if mc.at("school_nurse_room"):
                  rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Search & Rescue{/}."))
              else:
                rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_lollipop", marker = ["actions take"]))
          if mc.owned_item("plastic_fork") and mc.owned_item("lollipop_2"):
            rv.append(get_quest_guide_hud("inventory", marker = ["combine_plastic_fork_lollipop"], marker_sector = "mid_top", marker_offset = (60,0)))

      elif quest.jacklyn_broken_fuse == "fix":
        if mc.owned_item("fixed_cable"):
          rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power",marker = ["items fixed_cable@.8"], marker_offset = (60,90)))
        elif mc.owned_item("electrical_tape"):
          rv.append(get_quest_guide_hud("inventory", marker = ["combine_broken_cable_electrical_tape"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif school_english_class["desk_one_used_today"]:
          rv.append(get_quest_guide_hud("time", marker="$Check the student desk\nagain {color=#48F}tomorrow{/}."))
        elif school_english_class["desk_one_current_item"] is not None:
          rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk1@.225"]))
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = ["actions take"]))
        elif mc.owned_item(("banana_milk","garlic","lollipop","pepelepsi","salted_cola","seven_hp","strawberry_juice","tide_pods","tissues","water_bottle")):
          rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk1@.225"]))
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = ["random_consumable"]))
        else:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria counterleft@.45"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_counter_left", marker = ["actions interact"], marker_offset = (90,100), marker_sector = "left_top"))
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["$Buy a","items lollipop_1@.75","$for {color=#48F}$1{/}"]))
          if school_locker["gotten_lunch_today"]:
            rv.append(get_quest_guide_hud("time", marker="$Check your lunch box\nagain {color=#48F}tomorrow{/}."))
          else:
            if not mc.at("school_locker"):
              rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
            if school_ground_floor["locker_unlocked"]:
              rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
            else:
              rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
            rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["actions take"]))
          if not home_bathroom["tissues_taken_today"]:
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom tissues@.5"]))
            rv.append(get_quest_guide_marker("home_bathroom","home_bathroom_tissues", marker = ["actions take"]))

      elif quest.jacklyn_broken_fuse == "reward":
        rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
        if jacklyn.at("school_art_class","standing"):
          rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
        else:
          if mc.at("school_art_class"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))

      return rv
