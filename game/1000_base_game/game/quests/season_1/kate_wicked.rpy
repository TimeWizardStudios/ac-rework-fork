image call_jo = LiveComposite((0,0),(63,-53),Transform("jo contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))


init python:
  class Quest_kate_wicked(Quest):

    title = "Wicked Game"

    class phase_1_piano:
      description = "It's all fun and games until\nsomeone gets boxed."
      def hint(_quest):
        if quest.piano_tuning.finished:
          return "What has 88 pearly white teeth?"
        else:
          if quest.piano_tuning == "cabinet":
            return "To slay a dragon, one needs a sword. To slay a piano, one needs a hammer and fork."
          elif quest.piano_tuning == "baton":
            return "Is your baton at the top end, at the very bottom, or at just in the middle?"
          elif quest.piano_tuning == "hammer":
            return "Hammer time! Slap that bottom, bitch!"
          elif quest.piano_tuning == "fork":
            return "Fork this shit."
      def quest_guide_hint(_quest):
        if quest.piano_tuning.finished:
          return "Quest the piano, then play something dark and dramatic on it."
        else:
          if quest.piano_tuning == "cabinet":
            return "Interact with the cabinet in the music classroom."
          elif quest.piano_tuning == "baton":
            if school_music_class["got_conductor_baton"]:
              return "Use the conductor's baton on the middle compartment of the cabinet."
            else:
              return "Take the conductor's baton from the teacher's podium in the music classroom."
          elif quest.piano_tuning == "hammer":
            if school_first_hall_west["tuning_hammer"]:
              return "Use the tuning hammer on the bottom compartment of the cabinet."
            else:
              if mc.owned_item("tuning_hammer"):
                return "Use the tuning hammer on the piano in the fine arts wing."
              else:
                if school_music_class["middle_compartment"]:
                  return "Take the tuning hammer from the bottom compartment of the cabinet."
                else:
                  return "Use the conductor's baton on the middle compartment of the cabinet."
          elif quest.piano_tuning == "fork":
            if school_music_class["bottom_compartment"]:
              if school_music_class["middle_compartment"]:
                return "Take the conductor's baton back from the middle compartment of the cabinet."
              else:
                if school_music_class["top_compartment"] == "tuning_fork":
                  return "Use the conductor's baton on the top compartment of the cabinet."
                elif school_music_class["top_compartment"] == "conductor_baton":
                  if school_first_hall_west["tuning_fork"]:
                    return "Use the tuning fork on the top compartment of the cabinet."
                  else:
                    return "Use the tuning fork on the piano in the fine arts wing."
            else:
              return "Use the tuning hammer on the bottom compartment of the cabinet."

    class phase_2_phone_call:
      description = "It's all fun and games until\nsomeone gets boxed."
      hint = "Principle or principal, make the call."
      quest_guide_hint = "Call [jo] on your phone."

    class phase_3_wait:
      hint = "The long awaited long wait."
      def quest_guide_hint(quest):
        if (quest["party_date"]+2) - game.day == 1:
          return "Wait a day."
        else:
          return "Wait 2 days."

    class phase_4_school:
      hint = "Insert tool in hole to start the party. This is not a metaphor."
      def quest_guide_hint(quest):
        if game.hour == 19:
         return "Use the key to the music classroom on the door to the music classroom."
        elif quest["party_tonight"]:
          if persistent.time_format == "24h":
            return "Wait until 19:00."
          else:
            return "Wait until 7:00 PM."
        else:
          return "Go to school."

    class phase_5_costume:
      hint = "A light in the black. It's not the fear."
      def quest_guide_hint(quest):
        if game.location == "school_nurse_room":
          return "Interact with the bed."
        elif quest["own_costume"]:
          return "Go to the [nurse]'s office."
        elif game.location == "school_ground_floor_west":
          return "Quest the [nurse]."
        elif game.location == "school_entrance":
          if school_entrance["rope"]:
            return "Interact with the rope."
          elif quest.kate_wicked["another_way"] or quest["maxine_number"]:
            if mc.check_phone_contact("maxine"):
              return "Call [maxine] on your phone."
            else:
              return "Call [flora] on your phone."
          else:
            return "Interact with [maxine]'s office window."
        else:
          return "Go to the admin wing."

    class phase_6_party:
      hint = "Go to the place of hoarse voices and tinnitus."
      quest_guide_hint = "Go to the music classroom."

    class phase_1000_done:
      description = "Perhaps parties aren't\nso bad after all..."

    class phase_1001_flaccid_done:
      description = "Missed something, but\nit's probably nothing."


init python:
  class Event_quest_kate_wicked(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if (quest.kate_wicked in ("phone_call","costume","party")
      or (quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"])):
        return 0

    def on_can_advance_time(event):
      if (quest.kate_wicked in ("phone_call","costume","party")
      or (quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"])):
        return False

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_wicked" and quest.kate_wicked == "wait":
        quest.kate_wicked["party_date"] = game.day

    def on_advance(event):
      if quest.kate_wicked == "wait" and (quest.kate_wicked["party_date"]+2) == game.day:
        quest.kate_wicked.advance("school")
      elif quest.kate_wicked == "school" and quest.kate_wicked["party_tonight"] and game.hour == 19:
        game.events_queue.append("quest_kate_wicked_school_guard")

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.kate_wicked == "school" and new_location == "school_ground_floor" and not (quest.mrsl_table == "morning" or quest.kate_wicked["party_tonight"]):
        game.events_queue.append("quest_kate_wicked_school")
      elif quest.kate_wicked == "costume" and new_location == "school_first_hall" and not quest.kate_wicked["final_rounds"]:
        game.events_queue.append("quest_kate_wicked_costume_first_hall")
      elif quest.kate_wicked in ("costume","party") and new_location == "school_first_hall_east" and not quest.kate_wicked["what_the_hell"]:
        game.events_queue.append("quest_kate_wicked_costume_sports_wing")
      elif quest.kate_wicked == "costume" and new_location == "school_ground_floor_west" and not quest.kate_wicked["going_on_here"]:
        game.events_queue.append("quest_kate_wicked_costume_admin_wing")
      elif quest.kate_wicked == "costume" and new_location == "school_entrance" and not quest.kate_wicked["reach_her"]:
        game.events_queue.append("quest_kate_wicked_costume_exterior")
      elif quest.kate_wicked == "costume" and new_location == "school_homeroom" and not quest.kate_wicked["safer_somehow"]:
        game.events_queue.append("quest_kate_wicked_costume_homeroom")
      elif quest.kate_wicked == "party" and new_location == "school_music_class":
        game.events_queue.append("quest_kate_wicked_party_music_class")
      elif quest.kate_wicked == "party" and (new_location == "school_first_hall_west" or old_location == "school_first_hall_west"):
        game.events_queue.append("quest_kate_wicked_party_music")

    def on_quest_finished(event,_quest,silent=False):
      if _quest.id == "kate_wicked" and quest.kate_wicked == "flaccid_done":
        mc["focus"] = ""
        if kate["outfit_stamp"]:
          kate.outfit = kate["outfit_stamp"]
      elif _quest.id == "kate_wicked" and quest.kate_wicked == "done":
        mc["focus"] = ""
        kate.outfit = kate["outfit_stamp"]
        lindsey.outfit = lindsey["outfit_stamp"]

    def on_quest_guide_kate_wicked(event):
      rv = []

      if quest.kate_wicked == "piano":
        if quest.piano_tuning.finished:
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\ndark and dramatic{space=-50}","actions quest","${space=275}"], marker_offset = (150,250)))
        else:
          if quest.piano_tuning == "cabinet":
            rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
            rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
          elif quest.piano_tuning == "baton":
            if school_music_class["got_conductor_baton"]:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            else:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
          elif quest.piano_tuning == "hammer":
            if school_first_hall_west["tuning_hammer"]:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            else:
              if mc.owned_item("tuning_hammer"):
                rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
              else:
                if school_music_class["middle_compartment"]:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                else:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
          elif quest.piano_tuning == "fork":
            if school_music_class["bottom_compartment"]:
              if school_music_class["middle_compartment"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                if school_music_class["top_compartment"] == "tuning_fork":
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                elif school_music_class["top_compartment"] == "conductor_baton":
                  if school_first_hall_west["tuning_fork"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                    rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
            else:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))

      elif quest.kate_wicked == "phone_call":
        rv.append(get_quest_guide_hud("phone", marker=["call_jo","$\n\n\n{space=50}Call {color=#48F}[jo]{/}."]))

      elif quest.kate_wicked == "wait":
        if (quest.kate_wicked["party_date"]+2) - game.day == 1:
          rv.append(get_quest_guide_hud("time", marker="$Wait a day."))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}2{/} days."))

      elif quest.kate_wicked == "school":
        if game.hour == 19:
          rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_door_music", marker = ["items key_music_class@.85"], marker_offset = (-8,-8)))
        elif quest.kate_wicked["party_tonight"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 PM{/}.")))
        else:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      elif quest.kate_wicked == "costume":
        if quest.kate_wicked["own_costume"]:
          rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room bed@.2"]))
          rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_bed", marker = ["actions interact"], marker_offset = (190,60)))
        elif game.location == "school_ground_floor_west":
          rv.append(get_quest_guide_marker("school_ground_floor_west", "nurse", marker = ["actions quest"], marker_offset = (40,0)))
        elif game.location == "school_entrance":
          if school_entrance["rope"]:
            rv.append(get_quest_guide_marker("school_entrance", "school_entrance_rope", marker = ["actions go"], marker_sector = "left_top", marker_offset = (60,220)))
          elif quest.kate_wicked["another_way"] or quest.kate_wicked["maxine_number"]:
            if mc.check_phone_contact("maxine"):
              rv.append(get_quest_guide_hud("phone", marker=["call_maxine","$\n\n\n{space=25}Call {color=#48F}[maxine]{/}."]))
            else:
              rv.append(get_quest_guide_hud("phone", marker=["call_flora","$\n\n\n{space=30}Call {color=#48F}[flora]{/}."]))
          else:
            rv.append(get_quest_guide_marker("school_entrance", "school_entrance_clubroom_window", marker = ["actions interact"], marker_offset = (-10,-10)))
        else:
          rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["actions go"]))

      elif quest.kate_wicked == "party":
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["kate contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_door_music", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (50,0)))

      return rv
