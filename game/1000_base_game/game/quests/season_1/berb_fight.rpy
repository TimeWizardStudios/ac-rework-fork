image fill_bottle_with_pepelepsi = LiveComposite((310,93),(0,27),Text("Use",style="quest_guide_marker_text"),(52,0),"items bottle empty_bottle",(135,27),Text(", then choose",style="quest_guide_marker_text"),(310,0),"items bottle pepelepsi")
image fill_spray_bottle_with_pepelepsi = LiveComposite((310,93),(0,27),Text("Use",style="quest_guide_marker_text"),(52,0),Transform("items bottle spray_empty_bottle",zoom=0.75),(135,27),Text(", then choose",style="quest_guide_marker_text"),(310,0),Transform("items bottle spray_pepelepsi",zoom=0.75))


init python:
  class Quest_berb_fight(Quest):
    title = "Operation B. O. B."
    class phase_1_communicate:
      description = "Alpha delta bravo, engage code name: Beat Off Beaver."
      hint = "As Sun Tzu once said: If you know yourself and know your enemy, you don't have to fear a hundred beavers."
      quest_guide_hint = "Interact with the bookshelf in the Arts Wing."
    class phase_2_conflict:
      hint = "Knowledge is power. Time to exert it over that beaver."
      quest_guide_hint = "Use the Beaver Encyclopedia on the beaver."
    class phase_3_curveball:
      hint = "Sometimes a curveball is needed to hit a homerun."
      quest_guide_hint = "Knock the beaver off the roof with a baseball."
    class phase_4_chase:
      hint = "Studying your foe's footprints is how you track him. Research!"
      quest_guide_hint = "Use the Beaver Encyclopedia on the beaver."
    class phase_5_corner:
      hint = "A beaver in water is untouchable. Only the Good Book knows how to get one out."
      quest_guide_hint = "Use the Beaver Encyclopedia on the beaver."
    class phase_6_corrupt:
      hint = "Pollution is how you put nature and freaks of nature in their place."
      quest_guide_hint = "Use 3 Pepelepsi and 5 Candy Wrappers on the stream."
    class phase_7_combat:
      hint = "Strength, wit, or finesse. Dwayne Johnsson, Pinocchio, or the Evil Queen."
      quest_guide_hint = "Use a Stick, Rock, or Poisoned Apple to defeat the beaver."

    class phase_1000_done:
      description = "Death by a thousand nuts... or something."
    class phase_2000_failed:
      pass


  class Event_quest_berb_fight(GameEvent):
    def on_items_combined(event,item1,item2,combine_result,silent=False):
      pass
    def on_location_changed(event,old_location,new_location,silent=False):
      pass
    def on_quest_guide_berb_fight(event):
      rv = []

      if quest.berb_fight == "communicate":
        if mc.owned_item("book_of_the_dammed"):
          rv.append(get_quest_guide_path("school_entrance", marker = ["items book book_of_the_dammed"]))
          if berb.at("school_entrance"):
            rv.append(get_quest_guide_marker("school_entrance", "berb", marker = ["items book book_of_the_dammed"]))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Wait for [berb]"))
        else:
          rv.append(get_quest_guide_path("school_first_hall_west" , marker = ["items book book_of_the_dammed"]))
          rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_bookshelf", marker = ["actions interact"]))

      #if quest.berb_fight == "conflict":
        #rv.append(get_quest_guide_path("school_entrance", marker = ["berb contact_icon@.85"]))
        #if berb.at("school_entrance"):
          #rv.append(get_quest_guide_marker("school_entrance", "berb", marker = ["actions quest"]))
        #else:
          #rv.append(get_quest_guide_hud("time", marker="$Wait for [berb]"))

      if quest.berb_fight == "curveball":
        if mc.owned_item("baseball"):
          rv.append(get_quest_guide_path("school_entrance", marker = ["berb contact_icon@.85"]))
          if berb.at("school_entrance"):
            rv.append(get_quest_guide_marker("school_entrance", "berb", marker = ["items baseball"]))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Wait for [berb]"))
        else:
          rv.append(get_quest_guide_path("school_english_class", marker = ["items baseball"]))
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_three", marker = ["items baseball"]))

      if quest.berb_fight == "chase":
        rv.append(get_quest_guide_path("school_entrance", marker = ["items book book_of_the_dammed"]))
        if berb.at("school_entrance"):
          rv.append(get_quest_guide_marker("school_entrance", "berb", marker = ["items book book_of_the_dammed"]))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait for [berb]"))

      if quest.berb_fight == "corner":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["berb contact_icon@.85"], marker_sector = "left_bottom"))
        rv.append(get_quest_guide_marker("school_forest_glade", "berb", marker = ["items book book_of_the_dammed"], marker_sector = "right_mid"))

      if quest.berb_fight == "corrupt":
        if school_forest_glade['pollution'] < 3:
          if mc.owned_item("pepelepsi"):
            rv.append(get_quest_guide_path("school_forest_glade", marker = ["items bottle pepelepsi"] ))
            rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_river_lv" + str(school_forest_glade['pollution']), marker = ["items bottle pepelepsi"], marker_sector = "mid_bottom", marker_offset = (1000,0) ))
          elif mc.owned_item("empty_bottle"):
            x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["items bottle pepelepsi"] ))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["fill_"+x+"bottle_with_pepelepsi"], marker_sector="right_top", marker_offset = (50,125)))
          elif mc.owned_item(("water_bottle", "strawberry_juice", "banana_milk", "salted_cola","seven_hp")):
            x = mc.owned_item(("water_bottle", "strawberry_juice", "banana_milk", "salted_cola","seven_hp"))
            rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x,"actions consume"], marker_offset = (60,0), marker_sector = "mid_top"))
          elif not home_kitchen["water_bottle_taken"]:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["items bottle water_bottle"]))
            rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["items bottle water_bottle"]))
          else:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["items bottle water_bottle"] ))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"], marker_sector = "mid_bottom", marker_offset = (100,0) ))
        if not school_forest_glade["wrappers"] and mc.owned_item_count("wrapper") < 5:
          if mc.owned_item("lollipop"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items lollipop_1", "actions consume"]))
          else:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["items lollipop_1"] ))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items lollipop_1"], marker_sector = "mid_bottom", marker_offset = (100,0) ))
        elif mc.owned_item("wrapper") and not school_forest_glade["wrappers"]:
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["items candy_wrapper"] ))
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_river_lv" + str(school_forest_glade['pollution']), marker = ["items candy_wrapper"], marker_sector = "mid_bottom", marker_offset = (1200,0) ))

          #if not quest.kate_search_for_nurse.started:
          #  rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85", "actions quest"], force_marker = True))
          #else:
          #  rv.append(get_quest_guide_hud("quest_guide", marker="$Finish [kate]'s quest"))

      if quest.berb_fight == "combat":
        if mc.owned_item("poisoned_apple"):
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["berb contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "berb", marker = ["items poisonedapple"], marker_sector = "right_bottom"))
        else:
          if mc.owned_item("apple"):
            pass
          else:
            rv.append(get_quest_guide_path("school_english_class", marker = ["items apple"]))
            rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_four", marker = ["actions interact"]))
          if mc.owned_item("tide_pods"):
            pass
          else:
            if school_locker["gotten_lunch_today"]:
              rv.append(get_quest_guide_hud("time", marker="$Wait until{color=#48F} tomorrow{/}."))
            else:
              rv.append(get_quest_guide_path("school_locker", marker = ["items tide_pods"]))
              rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_lockers_front_right", marker = ["items locker_key"]))
              rv.append(get_quest_guide_marker("school_locker","school_locker_lunchfinal", marker = ["items tide_pods"]))
          if mc.owned_item("apple") and mc.owned_item("tide_pods"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items apple", "actions combine", "items tide_pods"]))

      return rv


#I feel like this quest needs some extra lines.

#You need to have finished the Lindsey Easel quest and the Kate Nurse Quest (to get the wrappers) to start this quest.

#To start the quest, you click on a beaver on the roof. It gives you one line than the quest pop-up happens. That is jarring because usually there is more of an introduction before a quest is started.
#And can you write an interact line for the beaver on the roof? The current one is being used as "Quest".
#the line is
#"Not sure what the critter wants. Probably just attention. Those tiny black eyes are gleaming with malice."
#I think we should add some lines about beavers as an animal, some world-buidling, then maybe mention some author who wrote a bestseller about them. A subtle hint to check bookshelves.

#So we need a few quest lines to start the quest and an Interact line when you interact with him on the roof.

#maybe some more lines when you click the bookshelf. Right now it only says "This might help me understand the beaver better" and gives you the book.
#Maybe some lines about the author. More chance for world-building.

#After the talk with Flora/Lindsey in the beginning, you have to throw the baseball at him to get him down.
# we should add an interact or investigate to give a hint to that.

#Lines for the baseball on the ground.

#I think there should be a couple lines sprinkled over the place that hint at you needing to keep using the book on him.
#Cuz right now its a bit confusing. There is nothing in the game that tells you you need to keep using the book, and most people would try things like wrench, stick, and bunch of other stuff.
#So here and there, we need some lines about like how the MC needs to do more research on the beaver, needs more info to progress, etc.

#3 more investigate lines for when you add one bottle of pepelepsi, two bottles, and then three for full pollution.
#we have an interact line for when the stream is fully clean and one for when it's fully poluted. But we need two more for when there is one bottle , and one for when there is two bottles.

#When berb comes out, the mc says "        "Okay, time to club myself a rodent."  which can be confusing since we use a stick, a rock, or an apple, not really anything clubby.

#fighting the beaver is a little confusing. When he doesnt have enough strength the mc should comment that he wasn't strong enough and now needes to resort to items.
#right now you kinda just get bit and dont really understand what happens.
