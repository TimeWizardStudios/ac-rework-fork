init python:
  class Quest_lindsey_book(Quest):
    title = "Fully Booked"
    class phase_1_start:
      description = "Reading in the digital age? Unfathomable."
      hint = "[lindsey]'s thirst for knowledge and entertainment might not be lasting. Find those books!"
    class phase_2_suggest:
      hint = "Out of the way! Book delivery coming through!"
    class phase_3_feedback:
      hint = "Did [lindsey] like the book? Well, did she? Hello?"
    class phase_4_easel:
      #Change this hint to indicate the three locations.
      hint= "Sticks for the stick god!"
    class phase_5_easel_created:
      hint= "If [lindsey] doesn't come to the easel, the easel has to come to her. Outside too. Yeah, rough."
    class phase_6_beaver:
      hint= "The only way to deal with this animal problem is with an animal solution."
    class phase_1000_done:
      description = "Very unfortunate. For the beaver. That it ran into me. An apex predator of these lands."
    class phase_2000_failed:
      description = "Failure is me. Always has been."

  class Event_quest_lindsey_book(GameEvent):
    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "lindsey_book" and quest.lindsey_book == "easel" and mc.owned_item("makeshift_easel"):
        game.events_queue.append("quest_lindsey_book_premade_easel")
    def on_quest_guide_lindsey_book(event):
      rv=[]
      if quest.lindsey_book == "start":
        rv.append(get_quest_guide_path("school_english_class", marker = ["items book snow_white_and_7_bang"]))
        rv.append(get_quest_guide_marker("school_english_class","school_english_class_bookshelf",marker_sector = "right_bottom", marker = ["items book snow_white_and_7_bang"]))
      if quest.lindsey_book == "suggest":
        rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions give"]))
      if quest.lindsey_book == "feedback":
        if quest.lindsey_book["day_recieved"] == game.day:
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}"+game.day_dow(game.day+1)+"{/}"))
        else:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
      if quest.lindsey_book == "easel":
        if not school_entrance["bush_interacted"]:
          rv.append(get_quest_guide_path("school_entrance", marker = ["items stick_1"]))
          rv.append(get_quest_guide_marker("school_entrance", "school_entrance_bush", marker = ["items stick_1"], marker_offset = (100,0), marker_sector = "right_bottom"))
        if not school_gym["stick_interacted"]:
          rv.append(get_quest_guide_path("school_gym", marker = ["items stick_1"]))
          rv.append(get_quest_guide_marker("school_gym", "school_gym_trash_bin", marker = ["items stick_1"], marker_sector = "left_bottom"))
        if not school_homeroom["stick_interacted"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["items stick_1"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_window", marker = ["items stick_1"], marker_offset = (100,0), marker_sector = "right_bottom"))
        if not school_art_class["stick_interacted"] and mc.owned_item("shovel"):
          rv.append(get_quest_guide_path("school_art_class", marker = ["items stick_1"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_wooden_ship", marker = ["items stick_1"], marker_offset = (50,200), marker_sector = "right_top"))
        if mc.owned_item_count("stick") >= 3:
          rv.append(get_quest_guide_hud("inventory", marker = ["items stick_1", "actions combine", "items stick_1"]))
        if mc.owned_item("stick") and mc.owned_item("stick_two"):
          rv.append(get_quest_guide_hud("inventory", marker = ["items stick_2", "actions combine", "items stick_1"]))
        if mc.owned_item("ball_of_yarn") and mc.owned_item("stick_three"):
          rv.append(get_quest_guide_hud("inventory", marker = ["items stick_3", "actions combine", "items ball_of_yarn"]))
        if not mc.owned_item("ball_of_yarn"):
          rv.append(get_quest_guide_path("school_homeroom", marker = ["items ball_of_yarn"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker = ["items ball_of_yarn"], marker_offset = (0,0), marker_sector = "right_bottom"))

      if quest.lindsey_book == "easel_created":
        if lindsey.at("school_entrance"):
          rv.append(get_quest_guide_path("school_entrance", marker = ["lindsey contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_entrance", "lindsey", marker = ["items makeshift_easel"]))
        else:
          rv.append(get_quest_guide_path("school_entrance", marker = ["lindsey contact_icon@.85"]))
          if mc.at("school_entrance"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for [lindsey] outside{color=#48F} 5:00 PM{/}"))

      if quest.lindsey_book == "beaver":
        if quest.lindsey_book["beaver"] < 4:
          if mc.owned_item("monkey_wrench"):
            rv.append(get_quest_guide_path("school_entrance", marker = ["items monkey_wrench@,90"]))
            rv.append(get_quest_guide_marker("school_entrance", "school_entrance_beaver_light", marker = ["items monkey_wrench@,90"], marker_sector = "right_bottom"))
          else:
            if mc.owned_item("bayonets_etiquettes"):
              rv.append(get_quest_guide_hud("inventory",marker = ["items book bayonets_etiquettes"]))
            else:
              rv.append(get_quest_guide_path("school_homeroom", marker = ["items book bayonets_etiquettes"]))
              rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_bookshelf", marker = ["items book bayonets_etiquettes"]))
        if quest.lindsey_book["beaver"] == 4:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["berb contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_beaver", marker = ["actions interact"]))
        if quest.lindsey_book["beaver"] == 5:
          rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["berb contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_beaver", marker = ["actions interact"]))
      return rv

label quest_lindsey_book_premade_easel:
  "Good thing I already made one of those... for whatever reason." #TBD proper dialogue line
  $quest.lindsey_book.advance("easel_created")
  return
