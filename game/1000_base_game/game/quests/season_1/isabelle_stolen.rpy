init python:
  class Quest_isabelle_stolen(Quest):
    title = "Stolen Hearts"
    class phase_1_eavesdrop:
      description = "Lost highways, hearts, and direction."
      hint = "Spying on the cheerleaders? That's my specialty."
      quest_guide_hint = "Interact with the entrance to the Locker Rooms in the Sports Wing."
    class phase_2_tellisabelle:#1
      hint = "Tell [isabelle] everything... except stuff about your life or internet history or fantasies. Actually, just tell her about the investigation."
      quest_guide_hint = "Quest [isabelle]."
    class phase_3_noluck:
      description = "Lost highways, hearts, and direction."
      hint = "Harassing the law enforcement is always a thrill. Apart from [mrsl], one of the few things that really gets my blood flowing."
      quest_guide_hint = "Interact with the [guard]'s booth in the entrance hall."
    class phase_4_sorry:#3
      hint = "Unintuitively, dicking around sometimes leads to less pussy. Especially when it comes to [isabelle]. Time to apologize."
      quest_guide_hint = "Quest [isabelle]."
    class phase_5_askmaxine:
      description = "Lost highways, hearts, and direction."
      hint = "Composing a letter sometimes requires a clean slate and a tool more powerful than a sword. Plant it on a plant."
      quest_guide_hint = "Combine Pen and Piece of Paper (found in bedroom closet). Use Letter to [maxine] on the Plant in the Admin Wing."
    class phase_6_maxineletter:#5
      hint = "Where's that damn response from [maxine]? I've been looking everywhere. The sweat is dripping. Need to quench the thirst for information."
      quest_guide_hint = "Consume Water. Interact with Letter from [maxine]."
    class phase_7_maxineclue:#5
      hint = "Apparently, the proof is in the pudding... or whatever else is in my lunch box."
      quest_guide_hint = "Interact with the Lunch Box inside your Personal Locker." 
    class phase_8_warnkate:
      hint = "A snitch in time saves... well, the only one here who would look fire in nine inch dominatrix heels."
      quest_guide_hint = "Quest [kate]."
    class phase_9_guard:
      description = "Lost highways, hearts, and direction."
      hint = "Investigation of the biggest suspect of all. Yeah, the man in the uniform."
      quest_guide_hint = "Interact with the [guard]'s booth in the Entrance Hall."
    class phase_10_donuts:#9
      hint = "Killing the supply doughnut necessarily mean a lack of demand."
      quest_guide_hint = "Interact with the Pastries in the Cafeteria."
    class phase_11_nonuts:#9
      hint = "Surveilling the [guard] for weaknesses that doughnut appear on the surface. That inner hunger."
      quest_guide_hint = "Interact with the [guard]'s booth in the entrance hall."
    class phase_12_outnuts:#9
      hint = "Watching a victim step into a carefully laid trap is the icing on the doughnut — the powdered sugar in the pastry shop."
      quest_guide_hint = "Go to the Cafeteria."
    class phase_13_breakin:#9
      hint = "When the law takes a break to buy fresh doughnuts. Crimes can be committed in a flash."
      quest_guide_hint = "Use the Flash Drive on the [guard]'s booth."
    class phase_14_footage:#9
      hint = "Big brother time — where I enjoy some security footage and condemn criminals of the state."
      quest_guide_hint = "Use the Flash Drive on the Computer in your Bedroom."
    class phase_15_dilemma:#9
      hint = "It's never black and white and the truth is what you make for yourself. [isabelle] of all people should know that."
      quest_guide_hint = "Quest [isabelle]."
    class phase_16_slap:  #9
      hint = "[lindsey]'s the target for some swift hand to face action."
      quest_guide_hint = "Quest [lindsey]."
    
    class phase_1000_done:
      description = "Everything but the motive."
    class phase_2000_failed:
      pass
      
  class Event_quest_isabelle_stolen(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      
        if not quest.kate_blowjob_dream.in_progress or not quest.kate_blowjob_dream>="sleep":
          if quest.isabelle_stolen=="askmaxine":
            if new_location=="school_homeroom" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["homeroom_entered"]:
              quest.isabelle_stolen["homeroom_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_homeroom")
            
            elif new_location=="school_english_class" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["english_class_entered"]:
              quest.isabelle_stolen["english_class_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_english_class")
            
            elif new_location=="school_art_class" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["art_class_entered"]:
              quest.isabelle_stolen["art_class_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_art_class")
            
            elif new_location=="school_nurse_room" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["nurse_room_entered"]:
              quest.isabelle_stolen["nurse_room_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_nurse_room")
            
            elif new_location=="school_first_hall_west" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["first_hall_west_entered"]:
              quest.isabelle_stolen["first_hall_west_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_first_hall_west")
            
            elif new_location=="school_first_hall_east" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["first_hall_east_entered"]:
              quest.isabelle_stolen["first_hall_east_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_first_hall_east")
            
            elif new_location=="school_gym" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["gym_entered"]:
              quest.isabelle_stolen["gym_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_gym")
            
            elif new_location=="home_bedroom" and not quest.isabelle_stolen["paper_got"] and not quest.isabelle_stolen["bedroom_entered"]:
              quest.isabelle_stolen["bedroom_entered"] = True
              game.events_queue.append("isabelle_quest_isabelle_stolen_paper_bedroom")
        
          if new_location=="school_cafeteria" and quest.isabelle_stolen=="outnuts":
            game.events_queue.append("isabelle_quest_stolen_cafeteria_outnuts")
            
    def on_quest_guide_isabelle_stolen(event):
      rv = []

      if quest.isabelle_stolen == "eavesdrop":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["kate contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_door_locker", marker = ["actions interact"]))
      
      if quest.isabelle_stolen == "tellisabelle":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

      if quest.isabelle_stolen == "noluck":
        rv.append(get_quest_guide_path("school_ground_floor"))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions interact"]))

      if quest.isabelle_stolen == "sorry":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))

      if quest.isabelle_stolen == "warnkate":
        rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "kate", marker = ["actions quest"]))

      if quest.isabelle_stolen == "askmaxine":
        if mc.owned_item("maxine_letter"):
          rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["items polaroidback_text"]))
          rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_pot", marker = ["items polaroidback_text"]))
        elif mc.owned_item("piece_of_paper"):
          rv.append(get_quest_guide_hud("inventory", marker = ["items polaroidback", "actions combine", "items ballpen"]))      
        else:          
          rv.append(get_quest_guide_path("home_bedroom", marker = ["items polaroidback"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_closet", marker = ["actions interact"]))

      if quest.isabelle_stolen == "maxineletter":
        if mc.owned_item("letter_from_maxine"):
          rv.append(get_quest_guide_hud("inventory", marker = ["items letter_from_maxine"]))      
        else:
          if quest.lindsey_wrong["fountain_state"] == 5:
            rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"]))
            rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_aquarium", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (50,100)))
          else:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.6"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["actions interact"], marker_sector = "left_bottom"))

      if quest.isabelle_stolen == "maxineclue":
        if mc.owned_item("letter_from_maxine"):
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen d1@.5"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_d1", marker = ["items letter_from_maxine"]))
        else:
          if not mc.at("school_locker"):
            rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
          if school_ground_floor["locker_unlocked"]:
            rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
          else:
            rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
          rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["actions interact"]))

      if quest.isabelle_stolen == "guard":
        rv.append(get_quest_guide_path("school_ground_floor"))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth"))

      if quest.isabelle_stolen == "donuts":
        rv.append(get_quest_guide_path("school_cafeteria"))
        rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["items doughnut"]))

      if quest.isabelle_stolen == "nonuts":
        rv.append(get_quest_guide_path("school_ground_floor"))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth"))

      if quest.isabelle_stolen == "outnuts":
        rv.append(get_quest_guide_path("school_cafeteria"))
        rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries"))
 
      if quest.isabelle_stolen == "breakin":
        if mc.owned_item("flash_drive"):
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["items flash_drive"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["items flash_drive"]))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["items flash_drive"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_flash_drive", marker = ["actions take"]))

      if quest.isabelle_stolen == "footage":
          rv.append(get_quest_guide_path("home_bedroom", marker = ["items flash_drive"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["items flash_drive"]))

      if quest.isabelle_stolen == "dilemma":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
        if not quest.isabelle_stolen["lindsey_talk"]:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))

      if quest.isabelle_stolen == "slap":
        rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))

      return rv


label isabelle_quest_isabelle_stolen_paper_homeroom:
  "Papers? Has anyone seen 'em? No?"
  return

label isabelle_quest_isabelle_stolen_paper_english_class:
  "What do you write shitty renaissance poems on? Papers! Got to be some in here."
  return

label isabelle_quest_isabelle_stolen_paper_art_class:
  "This is the arts classroom, and what do you draw art on? Fucking papers!"
  return
  
label isabelle_quest_isabelle_stolen_paper_nurse_room:
  "The [nurse] is always writing on a notepad, maybe I could steal a page?"
  "What about those blank notes on the table?"
  "..."
  "Hmm... no. They're glued to the desk."
  return
  
label isabelle_quest_isabelle_stolen_paper_first_hall_west:
  "Surely, there must be papers somewhere around here."
  return
  
label isabelle_quest_isabelle_stolen_paper_first_hall_east:
  "None of the jocks can read, so the odds of finding a paper here is probably low."
  return

label isabelle_quest_isabelle_stolen_paper_gym:
  "Where the fuck are the papers?"
  "Maybe I'll send [maxine] a fucking hulahoop inscribed with glowing Mordorian letters like the One Ring."
  "Fucking {i}ash nazg gimbatul!{/}"
  return

label isabelle_quest_isabelle_stolen_paper_bedroom:
  "Okay, my patience is low, but those notes on the bookshelf are lists of all the porn I'll commission if I ever get rich."
  "I'm not sending those to [maxine]."
  "And no. Despite what [jo] says, those posters are sacred."
  "Maybe it's time to excavate my closet. There's a lot of stuff buried there."
  return
