image call_lindsey = LiveComposite((0,0),(63,-53),Transform("lindsey contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))
image music_class_door = LiveComposite((40,355), (0,79), "school first_hall_west music", (2,0), "school first_hall_west music_sign")


init python:
  class Quest_lindsey_piano(Quest):

    title = "Dead Girl's Score"

    class phase_10_nurse:
      description = "So much to live for, so much\nto die for."
      hint = "Where's the healer? Has anyone seen the healer?"
      def quest_guide_hint(quest):
        if quest["follow_me"]:
          return "Quest [lindsey]."
        else:
          return "Quest the [nurse]."
    class phase_20_piano:
      description = "So much to live for, so much\nto die for."
      def hint(_quest):
        if quest.piano_tuning.finished:
          return "What has 88 pearly white teeth?"
        else:
          if quest.piano_tuning == "not_started":
            return "What has 88 pearly white teeth?"
          elif quest.piano_tuning == "cabinet":
            return "To slay a dragon, one needs a sword. To slay a piano, one needs a hammer and fork."
          elif quest.piano_tuning == "baton":
            return "Is your baton at the top end, at the very bottom, or at just in the middle?"
          elif quest.piano_tuning == "hammer":
            return "Hammer time! Slap that bottom, bitch!"
          elif quest.piano_tuning == "fork":
            return "Fork this shit."
      def quest_guide_hint(_quest):
        if quest.piano_tuning.finished:
          return "Quest the piano, then play something fast and exciting on it.{space=-20}"
        else:
          if quest.piano_tuning == "not_started":
            return "Quest the piano in the fine arts wing."
          elif quest.piano_tuning == "cabinet":
            return "Interact with the cabinet in the music classroom."
          elif quest.piano_tuning == "baton":
            if school_music_class["got_conductor_baton"]:
              return "Use the conductor's baton on the middle compartment of the cabinet."
            else:
              return "Take the conductor's baton from the teacher's podium in the music classroom."
          elif quest.piano_tuning == "hammer":
            if school_first_hall_west["tuning_hammer"]:
              return "Use the tuning hammer on the bottom compartment of the cabinet."
            else:
              if mc.owned_item("tuning_hammer"):
                return "Use the tuning hammer on the piano in the fine arts wing."
              else:
                if school_music_class["middle_compartment"]:
                  return "Take the tuning hammer from the bottom compartment of the cabinet."
                else:
                  return "Use the conductor's baton on the middle compartment of the cabinet."
          elif quest.piano_tuning == "fork":
            if school_music_class["bottom_compartment"]:
              if school_music_class["middle_compartment"]:
                return "Take the conductor's baton back from the middle compartment of the cabinet."
              else:
                if school_music_class["top_compartment"] == "tuning_fork":
                  return "Use the conductor's baton on the top compartment of the cabinet."
                elif school_music_class["top_compartment"] == "conductor_baton":
                  if school_first_hall_west["tuning_fork"]:
                    return "Use the tuning fork on the top compartment of the cabinet."
                  else:
                    return "Use the tuning fork on the piano in the fine arts wing."
            else:
              return "Use the tuning hammer on the bottom compartment of the cabinet."
    class phase_30_jo_talk:
      hint = "Meet your maker."
      quest_guide_hint = "Quest [jo]."
    class phase_40_album_day:
      hint = "Talk to you or slip on it. Spell check that."
      def quest_guide_hint(quest):
        if game.day > quest["jo_talk"]:
          return "Quest [jo] in the kitchen."
        else:
          return "Go to sleep."
    class phase_41_album_night:
      hint = "Talk to you or slip on it. Spell check that."
      quest_guide_hint = "Quest [jo] in the kitchen."
    class phase_50_lindsey_talk:
      hint = "You seek the man of finest sand, or the girl with speed on land."
      def quest_guide_hint(quest):
        if 19 > game.hour > 6:
          return "Quest [lindsey]."
        else:
          return "Go to sleep."
    class phase_51_listen_music_class:
      hint = "Time to face the music."
      def quest_guide_hint(quest):
        if quest["lindsey_arrived"]:
          if quest["stereo_system"]:
            return "Quest [lindsey]."
          else:
            return "Use the King's Bard album on the stereo system."
        else:
          return "Go to the music classroom, or wait one hour."
    class phase_60_phone_call:
      hint = "As they say, a rested head makes for a good call, and a better conversation."
      def quest_guide_hint(quest):
        if 20 > game.hour > 6:
          if mc.location == lindsey.location:
            return "Leave " + (lindsey.location.title if lindsey.location == "school_entrance" else "the " + lindsey.location.title.lower()) + "."
          else:
            return "Call [lindsey] on your phone."
        else:
          return "Go to sleep."
    class phase_61_wait:
      hint = "Wait for the right time to strike... or, you know, talk. Don't be like [isabelle]."
      quest_guide_hint = "Wait for [lindsey] to come over to your house at "+("20:00." if persistent.time_format == "24h" else "8:00 PM.")
    class phase_62_pick_up:
      hint = "Wait for the right time to strike... or, you know, talk. Don't be like [isabelle]."
      quest_guide_hint = "Quest [lindsey]."
    class phase_63_cleaning:
      def hint(quest):
        if home_bedroom["clean"]:
          return "Good things come to those who wait."
        else:
          return "Time to use the blowjob machine to avoid embarrassment.{space=-20}"
      def quest_guide_hint(quest):
        if home_bedroom["clean"]:
          if persistent.time_format == "24h":
            return "Wait until 20:00."
          else:
            return "Wait until 8:00 PM."
        elif quest["vacuum_cleaner_taken"]:
          return "Go to your bedroom."
        else:
          return "Interact with the closet in the home hall."
    class phase_64_listen_bedroom:
      hint = "Oh, god! She's here! Don't panic! Aaah!"
      def quest_guide_hint(quest):
        if quest["lindsey_arrived"]:
          return "Quest [lindsey]."
        else:
         return "Interact with the bedroom door."
    class phase_1000_done:
      description = "Treasure every second\nlike it's the last."
    class phase_2000_failed:
      description =  "If chivalry is dead, we now know who killed it."


  class Event_quest_lindsey_piano(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.lindsey_piano in ("pick_up","listen_bedroom") or (quest.lindsey_piano == "listen_music_class" and quest.lindsey_piano["lindsey_arrived"]):
        return 0

    def on_can_advance_time(event):
      if quest.lindsey_piano in ("pick_up","listen_bedroom") or (quest.lindsey_piano == "listen_music_class" and quest.lindsey_piano["lindsey_arrived"]):
        return False

    def on_advance(event):
      if quest.lindsey_piano == "listen_music_class" and not quest.lindsey_piano["lindsey_arrived"] and not mc["focus"]:
        game.events_queue.append("quest_lindsey_piano_listen_music_class_time_is_up")
      elif quest.lindsey_piano == "cleaning" and game.hour == 20 and not mc["focus"]:
        game.events_queue.append("quest_lindsey_piano_cleaning_arrived")
      elif quest.lindsey_piano == "wait" and game.hour == 20 and not mc["focus"]:
        game.events_queue.append("quest_lindsey_piano_wait_arrived")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "lindsey_piano":
        if quest.lindsey_piano == "album_day":
          quest.lindsey_piano["jo_talk"] = game.day
        elif quest.lindsey_piano in ("pick_up","listen_bedroom"):
          lindsey.equip("lindsey_cropped_hoodie")
          lindsey.equip("lindsey_sweatpants")

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.lindsey_piano == "listen_music_class" and new_location == "school_music_class" and not quest.lindsey_piano["lindsey_arrived"] and not mc["focus"]:
        game.events_queue.append("quest_lindsey_piano_listen_music_class")
      elif quest.lindsey_piano == "cleaning" and new_location == "home_bedroom" and not home_bedroom["clean"] and not mc["focus"]:
        game.events_queue.append("quest_lindsey_piano_cleaning")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "lindsey_piano":
        lindsey.equip("lindsey_shirt")
        lindsey.equip("lindsey_skirt")
        mc["focus"] = ""

    def on_quest_guide_lindsey_piano(event):
      rv=[]

      if quest.lindsey_piano == "nurse":
        if quest.lindsey_piano["follow_me"]:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "lindsey", marker = ["actions quest"]))
        else:
          if game.location == "school_nurse_room":
            rv.append(get_quest_guide_char("nurse", marker = ["nurse contact_icon@.85"], marker_offset = (100,0), force_marker = True))
            rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (100,0)))
          else:
            rv.append(get_quest_guide_char("nurse", marker = ["nurse contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "nurse", marker = ["actions quest"]))
      elif quest.lindsey_piano == "piano":
        if quest.piano_tuning.finished:
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\nfast and exciting{space=-50}","actions quest","${space=250}"], marker_offset = (150,250)))
        else:
          if quest.piano_tuning == "not_started":
            rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
            rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["actions quest"], marker_offset = (150,250)))
          elif quest.piano_tuning == "cabinet":
            rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
            rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
          elif quest.piano_tuning == "baton":
            if school_music_class["got_conductor_baton"]:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            else:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
          elif quest.piano_tuning == "hammer":
            if school_first_hall_west["tuning_hammer"]:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            else:
              if mc.owned_item("tuning_hammer"):
                rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
              else:
                if school_music_class["middle_compartment"]:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                else:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
          elif quest.piano_tuning == "fork":
            if school_music_class["bottom_compartment"]:
              if school_music_class["middle_compartment"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                if school_music_class["top_compartment"] == "tuning_fork":
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                elif school_music_class["top_compartment"] == "conductor_baton":
                  if school_first_hall_west["tuning_fork"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                    rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
            else:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
      elif quest.lindsey_piano == "jo_talk":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))
      elif quest.lindsey_piano == "album_day":
        if game.day > quest.lindsey_piano["jo_talk"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
          if jo.at("home_kitchen"):
            rv.append(get_quest_guide_marker("home_kitchen", "jo", marker = ["actions quest"]))
          else:
            if mc.at("home_kitchen"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} between\n"+(("{color=#48F}18:00{/} — {color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}6:00 PM{/} — {color=#48F}7:00 PM{/}.") if 18 > game.hour > 8 else ("{color=#48F}7:00{/} — {color=#48F}8:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}8:00 AM{/}."))))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))
      elif quest.lindsey_piano == "album_night":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
        if jo.at("home_kitchen"):
          rv.append(get_quest_guide_marker("home_kitchen", "jo", marker = ["actions quest"]))
        else:
          if mc.at("home_kitchen"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} between\n"+(("{color=#48F}18:00{/} — {color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}6:00 PM{/} — {color=#48F}7:00 PM{/}.") if 18 > game.hour > 8 else ("{color=#48F}7:00{/} — {color=#48F}8:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}8:00 AM{/}."))))
      elif quest.lindsey_piano == "lindsey_talk":
        if 19 > game.hour > 6:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "lindsey", marker = ["actions quest"]))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))
      elif quest.lindsey_piano == "listen_music_class":
        if quest.lindsey_piano["lindsey_arrived"]:
          if quest.lindsey_piano["stereo_system"]:
            rv.append(get_quest_guide_marker("school_music_class","lindsey", marker = ["actions quest"]))
          else:
            rv.append(get_quest_guide_marker("school_music_class","school_music_class_stereo_system", marker = ["items king_bard_album@.9"], marker_sector = "left_bottom", marker_offset = (200,0)))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["$or{space=-150}","music_class_door@.5","${space=40}"]))
          rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_door_music", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (50,0)))
      elif quest.lindsey_piano == "phone_call":
        if 20 > game.hour > 6:
          if mc.location == lindsey.location:
            rv.append(get_quest_guide_hud("map", marker = "$Leave " + ("{color=#48F}" + lindsey.location.title if lindsey.location == "school_entrance" else "the {color=#48F}" + lindsey.location.title.lower()) + "{/}.", marker_sector = "mid_top"))
          else:
            rv.append(get_quest_guide_hud("phone", marker=["call_lindsey","$\n\n\nCall {color=#48F}[lindsey]{/}.{space=-40}"]))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))
      elif quest.lindsey_piano == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[lindsey]{/} to come over\nto your house at "+("{color=#48F}20:00{/}." if persistent.time_format == "24h" else "{color=#48F}8:00 PM{/}.")))
      elif quest.lindsey_piano == "pick_up":
        rv.append(get_quest_guide_marker("home_kitchen", "lindsey", marker = ["actions quest"]))
      elif quest.lindsey_piano == "cleaning":
        if home_bedroom["clean"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}20:00{/}." if persistent.time_format == '24h' else "{color=#48F}8:00 PM{/}.")))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Clean up your room before\n{color=#48F}[lindsey]{/} comes over at "+("{color=#48F}20:00{/}." if persistent.time_format == "24h" else "{color=#48F}8:00 PM{/}.")))
          if quest.lindsey_piano["vacuum_cleaner_taken"]:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["items vacuum_cleaner@.7"]))
          else:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall cleaning_supply@.5"]))
            rv.append(get_quest_guide_marker("home_hall", "home_hall_cleaning_supply", marker = ["actions take"]))
      elif quest.lindsey_piano == "listen_bedroom":
        if quest.lindsey_piano["lindsey_arrived"]:
          rv.append(get_quest_guide_marker("home_bedroom", "lindsey", marker = ["actions quest"]))
        else:
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_door", marker = ["actions interact"]))

      return rv
