image read_flora_text = LiveComposite((0,0),(63,-53),Transform("flora contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info text",zoom=0.5))
image ball_of_yarn_combine_safety_pin = LiveComposite((275,140),(0,45),"items ball_of_yarn",(95,47),Transform("actions combine",zoom=0.85),(180,45),"items safety_pin")
image art_in_progress = LiveComposite((145,279),(0,0),"school art_class easel_3_artinprogress",(37,16),ConditionSwitch("school_art_in_progress['libation']","school art_class easel_3_artinprogress_pepelepsi","not school_art_in_progress['libation']",Null()),(50,91),ConditionSwitch("school_art_in_progress['suction']","school art_class easel_3_artinprogress_lollipops","not school_art_in_progress['suction']",Null()),(87,63),ConditionSwitch("school_art_in_progress['caramelization']","school art_class easel_3_artinprogress_doughnuts","not school_art_in_progress['caramelization']",Null()))


init python:
  class Quest_jacklyn_sweets(Quest):

    title = "King of Sweets"

    class phase_1_doughnuts:
      description = "That one thing that everyone subconsciously craves."
      hint = "Don't pretend you don't know where the pastries are... It's basically your whole diet."
      quest_guide_hint = "Interact with the pastries in the cafeteria."
    class phase_2_feed:
      hint = "Feed the doughnut beast before you yourself become the feast."
      quest_guide_hint = "Use 5 doughnuts on the [guard]'s booth."
    class phase_3_oldpromises:
      hint = "Just chat her up — the girl with the cool hair."
      def quest_guide_hint(_quest):
        if quest.jacklyn_statement.finished:
          return "Quest [jacklyn] in the art classroom."
        elif quest.jacklyn_statement.in_progress:
          return "Finish \"The Statement.\""
        elif mc.check_phone_contact("jacklyn"):
          return "Text [jacklyn] on your phone."
        elif quest.jacklyn_art_focus.finished:
          return "Go to your bedroom."
        elif quest.jacklyn_art_focus.in_progress:
          return "Finish \"Art Through Suffering.\""
        elif quest.flora_jacklyn_introduction.finished:
          return "Quest [jacklyn] in the art classroom."
        elif quest.flora_jacklyn_introduction.in_progress:
          return "Finish \"Punked.\""
        else:
          return "Quest [flora] in the home kitchen.{space=-5}"
    class phase_4_inspiration:
      hint = "The source of inspiration. The well of creativity. Your personal oasis."
      quest_guide_hint = "Go to your bedroom."
    class phase_5_bedroom:
      hint = "Inspiration comes in many shapes and forms. Squares in particular."
      quest_guide_hint = "Interact with your computer."
    class phase_6_text:
      hint = "Do you really not know what that flashing thing on your phone indicates?"
      quest_guide_hint = "Read [flora]'s text in your phone."
    class phase_7_artroom:
      hint = "Art is a process with many steps. Try not to stumble."
      def quest_guide_hint(quest):
        if quest["paper_removed"]:
          if mc.owned_item(("specialized_fuse_cable_replacement_tool","high_tech_lockpick")):
            item = mc.owned_item(("specialized_fuse_cable_replacement_tool","high_tech_lockpick")).replace("h_t","h-t").replace("_"," ")
            return "Use the " + item + " on the blank canvas."
          else:
            if mc.owned_item("ball_of_yarn") and mc.owned_item("safety_pin"):
              return "Build a high-tech lockpick by combining a ball of yarn with the safety pin."
            else:
              if not mc.owned_item("ball_of_yarn"):
                return "Take the ball of yarn from the leftmost desk in the homeroom."
              if not mc.owned_item("safety_pin"):
                return "Take the safety pin from the lollipop jar in the [nurse]'s office."
        else:
          return "Interact with the blank canvas in the art classroom."
    class phase_8_paint:
      hint = "On the canvas, each thing has its place, and each place has its things."
      def quest_guide_hint(quest):
        if not school_art_in_progress["caramelization"]:
          doughnuts = mc.owned_item_count("doughnut")
          if doughnuts < 3:
            return "Buy " + str(3-doughnuts) + (" doughnuts" if 3-doughnuts > 1 else " doughnut") + " in the cafeteria."
          else:
            return "Interact with the sketched canvas, then use 3 doughnuts on the Caramelization area."
        if not school_art_in_progress["suction"]:
          lollipops = mc.owned_item_count("lollipop")
          if lollipops < 3:
            return "Buy " + str(3-lollipops) + (" lollipops" if 3-lollipops > 1 else " lollipop") + " from the vending machine in the first hall."
          else:
            return "Interact with the sketched canvas, then use 3 lollipops on the Suction area."
        if not school_art_in_progress["libation"]:
          if mc.owned_item(("pepelepsi","spray_pepelepsi")):
            return "Interact with the sketched canvas, then use a bottle of pepelepsi on the Libation area."
          else:
            if mc.owned_item(("empty_bottle","spray_empty_bottle")):
              return "Go to the cafeteria and fill up a bottle of pepelepsi."
            elif mc.owned_item(("banana_milk","spray_banana_milk","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")):
              item = mc.owned_item(("banana_milk","spray_banana_milk","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")).replace("spray_","").replace("_bottle","").replace("_hp","_HP").replace("_"," ")
              return "Consume the bottle of " + item + "."
            else:
              if home_kitchen["water_bottle_taken"]:
                return "Buy a water bottle from the vending machine in the first hall."
              else:
                return "Take the water bottle in the home kitchen."
    class phase_9_finaltouches:
      hint = "Time to show off! Woo that art teacher!"
      quest_guide_hint = "Quest [jacklyn] in the art classroom."
    class phase_1000_done:
      description = "A true artist combines the old\nand the new with dick.\n\nSuch is the king's decree."


  class Event_quest_jacklyn_sweets(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.jacklyn_sweets == "inspiration":
        if new_location == "school_ground_floor" and not quest.jacklyn_sweets["school_ground_floor_checked"]:
          game.events_queue.append("quest_jacklyn_sweets_inspiration_school_ground_floor")
        if new_location == "school_cafeteria" and not quest.jacklyn_sweets["school_cafeteria_checked"]:
          game.events_queue.append("quest_jacklyn_sweets_inspiration_school_cafeteria")
        if new_location == "school_gym" and not quest.jacklyn_sweets["school_gym_checked"]:
          game.events_queue.append("quest_jacklyn_sweets_inspiration_school_gym")
        if new_location == "school_english_class" and not quest.jacklyn_sweets["school_english_class_checked"]:
          game.events_queue.append("quest_jacklyn_sweets_inspiration_school_english_class")
        if new_location == "school_nurse_room" and not quest.jacklyn_sweets["school_nurse_room_checked"]:
          game.events_queue.append("quest_jacklyn_sweets_inspiration_school_nurse_room")
        if new_location == "home_bedroom":
          game.events_queue.append("quest_jacklyn_sweets_inspiration_home_bedroom")
      elif quest.jacklyn_sweets == "artroom" and new_location == "school_art_class" and not quest.jacklyn_sweets["arrived_at_artroom"]:
        game.events_queue.append("quest_jacklyn_sweets_artroom")
      elif quest.jacklyn_sweets.finished and new_location == "home_bedroom" and mc.owned_item("king_of_sweets") and not home_bedroom["clean"]:
        game.events_queue.append("quest_jacklyn_sweets_hang_up")

    def on_enter_roaming_mode(event):
      if quest.jacklyn_sweets == "paint" and school_art_in_progress["libation"] and school_art_in_progress["suction"] and school_art_in_progress["caramelization"]:
        game.events_queue.append("quest_jacklyn_sweets_paint")

    def on_quest_guide_jacklyn_sweets(event):
      rv = []

      if quest.jacklyn_sweets == "doughnuts":
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
        rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
      elif quest.jacklyn_sweets == "feed":
        doughnuts = mc.owned_item_count("doughnut")
        if doughnuts < 5:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
        else:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["items doughnut@.7"], marker_sector = "right_top", marker_offset = (70,170)))
      elif quest.jacklyn_sweets == "oldpromises":
        if quest.jacklyn_statement.finished:
          rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
          if jacklyn.at("school_art_class","standing"):
            rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
          else:
            if mc.at("school_art_class"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))
        elif quest.jacklyn_statement.in_progress:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}The Statement{/}."))
        elif mc.check_phone_contact("jacklyn"):
          rv.append(get_quest_guide_hud("phone", marker=["text_jacklyn","$\n\n\n{space=15}Text {color=#48F}[jacklyn]{/}."]))
        elif quest.jacklyn_art_focus.finished:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["actions go"]))
        elif quest.jacklyn_art_focus.in_progress:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Art Through Suffering{/}."))
        elif quest.flora_jacklyn_introduction.finished:
          rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
          if jacklyn.at("school_art_class","standing"):
            rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
          else:
            if mc.at("school_art_class"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))
        elif quest.flora_jacklyn_introduction.in_progress:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Punked{/}."))
        else:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
          if flora.at("home_kitchen"):
            rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"], marker_offset = (40,0)))
          else:
            if mc.at("home_kitchen"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} at "+("{color=#48F}18:00{/}." if persistent.time_format == '24h' else "{color=#48F}6:00 PM{/}.")))
      elif quest.jacklyn_sweets == "inspiration":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["actions go"]))
      elif quest.jacklyn_sweets == "bedroom":
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["actions interact"]))
      elif quest.jacklyn_sweets == "text":
        rv.append(get_quest_guide_hud("phone", marker=["read_flora_text","$\n\n\nRead {color=#48F}[flora]'s{/} text.{space=80}"]))
      elif quest.jacklyn_sweets == "artroom":
        if quest.jacklyn_sweets["paper_removed"]:
          if mc.owned_item(("specialized_fuse_cable_replacement_tool","high_tech_lockpick")):
            for item in ("specialized_fuse_cable_replacement_tool","high_tech_lockpick"):
              if mc.owned_item(item):
                rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class easel_3_nopaper@.4"]))
                rv.append(get_quest_guide_marker("school_art_class", "school_art_class_easel_03", marker = [items_by_id[item].icon+"@.8"]))
          else:
            if mc.owned_item("ball_of_yarn") and mc.owned_item("safety_pin"):
              rv.append(get_quest_guide_hud("inventory", marker = ["ball_of_yarn_combine_safety_pin"], marker_sector = "mid_top", marker_offset = (60,0)))
            else:
              if not mc.owned_item("safety_pin"):
                rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room cup@.5"]))
                rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_lollipop_jar", marker = ["actions take"], marker_sector = "right_top", marker_offset = (0,120)))
              if not mc.owned_item("ball_of_yarn"):
                rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom yarn@.5"]))
                rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker = ["actions take"], marker_offset = (60,-20)))
        else:
          rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class easel_3@.4"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_easel_03", marker = ["actions interact"]))
      elif quest.jacklyn_sweets == "paint":
        if not school_art_in_progress["caramelization"]:
          doughnuts = mc.owned_item_count("doughnut")
          if doughnuts < 3:
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
          else:
            if mc.at("school_art_in_progress"):
              rv.append(get_quest_guide_marker("school_art_in_progress", "school_art_in_progress_caramelization", marker = ["items doughnut@.7"], marker_sector = "right_top", marker_offset = (350,-50)))
            else:
              rv.append(get_quest_guide_path("school_art_class", marker = ["art_in_progress@.4"]))
              rv.append(get_quest_guide_marker("school_art_class", "school_art_class_art_in_progress", marker = ["actions interact"]))
        if not school_art_in_progress["suction"]:
          lollipops = mc.owned_item_count("lollipop")
          if lollipops < 3:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items lollipop_1@.7"]))
          else:
            if mc.at("school_art_in_progress"):
              rv.append(get_quest_guide_marker("school_art_in_progress", "school_art_in_progress_caramelization", marker = ["items lollipop_1@.7"], marker_sector = "right_top", marker_offset = (-330,40)))
            else:
              rv.append(get_quest_guide_path("school_art_class", marker = ["art_in_progress@.4"]))
              rv.append(get_quest_guide_marker("school_art_class", "school_art_class_art_in_progress", marker = ["actions interact"]))
        if not school_art_in_progress["libation"]:
          if mc.owned_item(("pepelepsi","spray_pepelepsi")):
            if mc.at("school_art_in_progress"):
              for item in ("pepelepsi","spray_pepelepsi"):
                if mc.owned_item(item):
                  rv.append(get_quest_guide_marker("school_art_in_progress", "school_art_in_progress_caramelization", marker = [items_by_id[item].icon+"@.75"], marker_sector = "right_top", marker_offset = (-80,-410)))
            else:
              rv.append(get_quest_guide_path("school_art_class", marker = ["art_in_progress@.4"]))
              rv.append(get_quest_guide_marker("school_art_class", "school_art_class_art_in_progress", marker = ["actions interact"]))
          else:
            if mc.owned_item(("empty_bottle","spray_empty_bottle")):
              x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
              rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
              rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["fill_"+x+"bottle_with_pepelepsi"], marker_sector="right_top", marker_offset = (50,125)))
            elif mc.owned_item(("banana_milk","spray_banana_milk","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")):
              x = mc.owned_item(("banana_milk","spray_banana_milk","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water"))
              rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x,"actions consume@.85"], marker_sector = "mid_top", marker_offset = (60,0)))
            else:
              if home_kitchen["water_bottle_taken"]:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
              else:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
                rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))
      elif quest.jacklyn_sweets == "finaltouches":
        rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
        if jacklyn.at("school_art_class","standing"):
          rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
        else:
          if mc.at("school_art_class"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))

      return rv
