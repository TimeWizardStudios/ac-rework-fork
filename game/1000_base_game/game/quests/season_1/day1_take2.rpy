init python:
  class Quest_day1_take2(Quest):
    title="Day 1, Take 2"
    class phase_1_day1_take2:
      description="Everything's possible if you really put your mind to it. Like getting to class."
      hint="The bell rang like five minutes ago, and you're still looking at your phone. Homeroom, now."
    class phase_2_entered:
      hint="The bell rang like five minutes ago, and you're still looking at your phone. Homeroom, now."
    class phase_3_homeroom:
      hint="we entered homeroom, this hint is never seen."
    class phase_4_isabelle_volunteer:
      hint="Raised your hand and the stakes. Tour time."
    class phase_5_isabelle_pass:
      hint="The new girl just got paired with the devil. Should be interesting."
    class phase_6_lindsey_fall:
      hint="Man, these floors are all sorts of wet today. Hopefully, nothing bad happens..."
    class phase_1000_done:
      description="A surprising first day to be sure, but a welcome one."
    
  class Event_quest_day1_take2(GameEvent):
 
    def on_location_changed(event,old_location,new_location,silent=False):
      if not quest.day1_take2.started:
        if new_location==school_entrance:
          game.events_queue.append("quest_day1_take2_arrived")
      else:
        if quest.day1_take2=="day1_take2" and new_location==school_ground_floor:
          game.events_queue.append("quest_day1_take2_entered")
        elif quest.day1_take2=="homeroom" and new_location==school_homeroom:
          game.events_queue.append("quest_day1_take2_homeroom")
  
    def on_quest_guide_day1_take2(event):
      rv=[] 
      if quest.day1_take2<="homeroom":
        rv.append(get_quest_guide_path("school_homeroom", marker = ["mrsl contact_icon@.85"]))
      if quest.day1_take2 in ("isabelle_volunteer","isabelle_pass"):
        rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_door"))
      if quest.day1_take2 == "lindsey_fall":
        rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_stairs",marker_offset=(125,125),marker_sector="right_top"))
      return rv

label quest_day1_take2_arrived:
  scene location with dissolve
  $set_dialog_mode()
  $quest.day1_take2.start()
  "Newfall High — the perfect place to have your confidence shattered and your hopes turned to dust, while on a semi-strict diet of eggos and strawberry juice."
  "Halls infested with gaggles of harpies, who snigger and quickly pass you by, but stop and fawn over idiot jocks."
  "She-devils in skimpy dresses, flaunting their bodies and brace-perfect smiles, reminding you that happiness and success are only for attractive people."
  "Classroom-dwelling tormentors, whose only pleasure in life is screwing you over with a big fat D."
  "And, of course, the principal... who keeps the rotting cogs turning and the social hierarchy intact."
  "To actually be back. Fuck."
  "The odds aren't going to be fair this time around either... but if I really did travel back in time, at least I know what's coming."
  "Perhaps there's a chance for some sort of redemption?"
  "Perhaps..."
  play sound "phone_vibrate"
  "This again?"
  $set_dialog_mode("phone_message","hidden_number")
  hidden_number "Blasted technology! Apologies for earlier."
  hidden_number "I have something important to tell you."
  menu(side="right"):
    "\"Seriously? Just out with it!\"":
      "Seriously? Just out with it!"
      label quest_day1_take2_arrived_choices:
      hidden_number "Your choices in life matter. Even the smallest ones."
      hidden_number "Sometimes, they have consequences. Not just for yourself, but also for others. Keep that in mind, [mc]."
      menu(side="right"):
        "\"That's it? Some wellness carpe diem bullshit?\"":
          $mc.lust+=1
          $hidden_number.love-=1
          $unlock_stat_perk("lust4")
          "That's it? Some wellness carpe diem bullshit?"
          hidden_number "..."
          hidden_number "Just follow your heart."
          "How utterly cliche."
        "\"Thanks. I guess that is important. Now more than ever. What else?\"":
          $mc.love+=1
          $hidden_number.love+=1
          $unlock_stat_perk("love3")
          "Thanks. I guess that is important. Now more than ever. What else?"
          hidden_number "Follow your heart, and you'll be okay!"
        "\"Not exactly the information I was hoping for.\"":
          "Not exactly the information I was hoping for."
          hidden_number "What were you hoping for?"
          menu(side="right"):
            "\"Something useful! The secrets to getting pussy, maybe?\"":
              $mc.lust+=1
              $hidden_number.love-=1
              "Something useful! The secrets to getting pussy, maybe?"
              hidden_number "..."
              hidden_number "Aren't there more important things in life? Like following your heart?"
              "How utterly cliche."
              $unlock_stat_perk("lust5")
            "\"Something helpful! How to get a girl to like me, maybe?\"":
              $mc.love+=1
              $hidden_number.love+1
              "Something helpful! How to get a girl to like me, maybe?"
              hidden_number "Follow your heart, and you'll be okay!"
    "\"Nice try. Go bother someone else.\"":
      $mc.intellect+=1
      $hidden_number.love-=1
      "Nice try. Go bother someone else."
      hidden_number "Fine. Just remember that all choices have consequences. Trust your heart!"
    "\"If you ghost me again, I'll block you.\"":
      "If you ghost me again, I'll block you."
      hidden_number "I promise that it was equipment malfunction! Besides, this is more important to me than you."
      "Okay, I guess I'm listening."
      jump quest_day1_take2_arrived_choices
  $set_dialog_mode()
  "How fucking useless."
  "Sent back in time, and the only person who seems to be aware of it is some demented grandma who found a phone and decided that her cookies and milk aren't enough to save the world anymore."
  "My heart? Seriously? This whole thing feels like a big joke."
  "My heart can't be trusted any more than my..."
  "Anyway, better get inside before the bell rings."
  return

label quest_day1_take2_entered:
  $quest.day1_take2.advance()
  "Ah, the smell of artificial lemons and misery. Nostalgia, stress, and terror brewing in an all-too-familiar concoction of emotions."
  "There's little in this place to cherish... apart from the girls running up and down the stairs."
  "Red lips pouting. Eyes crackling with determination. Struggling against gravity in a battle they can't win."
  "Some girls lose it harder than others. And those are the milkshakes that bring all the boys to the yard."
  "Or at least a charity show of bouncy delight for the wretched and deprived."
  "..."
  "Better get to the homeroom before they close the door."
  return

label quest_day1_take2_homeroom:
  "The thick smell of freshly sharpened pencils, cheap perfume, and cigarette smoke always gave the homeroom a shady character."
  "It's the only room in the school that could, by smell alone, be mistaken for a seedy strip club."
  "For whatever reason, the janitor never scrubbed this floor with her signature lemon soap, and that still seems to be the case."
  "Normally, keeping my head down would be my first instinct, but the girls are all busy hugging and chatting."
  "And the guys... they're all busy staring at..."
  show mrsl thinking
  with dissolve
  "That's... Whoa! What happened to the cardigan and knitted socks?"
  "It's like she just woke up from hibernation and decided that sex appeal is the new black."
  "Holy smokes. No wonder the guys aren't paying attention to their favorite victim."
  mrsl neutral "Welcome to your final year of high school! I hope you're as excited as I am."
  mrsl "It's okay, you don't have to sit down..."
  show mrsl flirty with Dissolve(1)
  pause(0.8)
  mrsl "...unless you need to!"
  mrsl excited "My name is Mrs. Lichtenstein and I'll be your homeroom teacher."
  mrsl excited "This year, things are going to be a bit different."
  mrsl  "Newfall High has been selected for an experimental program."
  mrsl "Some of you might've heard about the new curriculum already, but for those who haven't, this is how it'll work."
  show mrsl write1
  with fade
  "Oh, shit. Okay, definitely time to take a seat!"
  "Who knew she hid such a tight body underneath those layers of fabric. It's hard not to be vulgar because {i}damn{/}... that ass!"
  "And that strip of skin at the edge of her dress is like a glimpse into the second circle of Hell..."
  "The firm roundness of her cheeks..."
  "The belt that caresses her stripper waist..."
  "The outline of her freaking nipple piercings earlier!"
  "She's like sex on legs... long perfectly toned legs..."
  show mrsl write2
  with dissolve
  mrsl "Can everyone see well?"
  mrsl "Okay, perfect!"
  show mrsl write1
  with dissolve
  mrsl "Starting today, each of you will be in charge of your own schedule."
  mrsl "That means you have to sign up for three classes you want to focus on. There'll be a list in each classroom."
  mrsl "To graduate at the end of the year, you need to acquire one hundred points in your focus classes and twenty points in your regular classes."
  mrsl "This means that there won't be scheduled classes for you this year and that you're more responsible for your education."
  show mrsl write2
  with dissolve
  mrsl "Did everyone get that? Some of you seem a bit distracted."
  mrsl "In case you're confused, the details are on the blackboard."
  $unlock_replay("mrsl_lesson")
  mrsl neutral "Any questions?"
  show mrsl neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "Raise hand":
      $unlock_stat_perk("lust8")
      show mrsl surprised at move_to(.5)
      $mc.lust+=1
      mrsl surprised "Yes? [mc]."
      show mrsl surprised at move_to(.75)
      menu(side="left"):
        extend ""
        "\"So, we get to sleep in... all year?\"":
          show mrsl surprised at move_to(.5)
          $mc.charisma+=1
          mc "So, we get to sleep in... all year?"
          mrsl excited "Some of you will have to work just as hard as before."
          mrsl smile "Others might have more time for... other activities."
          mrsl smile "In the end, your grades are all up to you."
        "\"What classes do you teach this year, [mrsl]?\"":
          show mrsl surprised at move_to(.5)
          $mrsl.lust+=1
          mc "What classes do you teach this year, [mrsl]?"
          mrsl smile "Unfortunately, I won't be teaching any classes this year."
          mrsl smile "But I will be coaching the Newfall swim team. You're very welcome to try out, [mc]!"
          "That's... err. Is she being suggestive on purpose?"
          "She used to look at me with nothing but disgust, and now she's practically hitting on me."
          "Did she say swimsuit?"
    "Stay silent": 
      $unlock_stat_perk("love7")
      show mrsl neutral at move_to(.5)
      $mc.love+=1
      "No point risking embarrassment this early in the day."
      mrsl thinking "No? Okay, good."
  mrsl neutral "Most of you know each other from last year..."
  mrsl excited "But we have one new student. She has transferred all the way from Europe."
  mrsl excited "Everyone, please say hi to [isabelle]!"
  show mrsl excited at move_to(.25)
  show isabelle blush at appear_from_right(.75)
  isabelle "Hello."
  "There was no [isabelle] in my senior class..."
  "That's the third strange thing, along with [mrsl]'s new style and those texts."
  "But damn. She's pretty cute."
  "And, obviously, way out of my league."
  mrsl smile "Why don't you tell us a bit about yourself?"
  isabelle smile "I'm originally from London, but thanks to my dad's job, we move almost every year."
  isabelle sad "I get to see the world, but sometimes it's hard to stay in touch with people."
  mrsl excited "I'm sure you'll meet some new friends here!"
  mrsl thinking "Why don't we ask someone to show you around?" 
  isabelle excited  "That would be nice."
  mrsl excited "Okay! Any volunteers?"
  show mrsl excited at move_to(.15)
  show isabelle excited at move_to(.5)
  menu(side="right"):
    extend ""
    "Volunteer":
      show mrsl excited at move_to(.25)
      show isabelle excited at move_to(.75)
      $isabelle["volunteered"]=True
      $mc.charisma+=1
      $isabelle.love+=1
      mc "I could do it."
      show isabelle excited at move_to(.5)
      show kate eyeroll at appear_from_right("right")
      kate eyeroll "Suck-up."
      show isabelle excited at move_to(.75)
      hide kate with moveoutright
      mrsl excited "Perfect! [mc] will give you a tour of the school."
      show mrsl blush at move_to(.5)
      hide isabelle with moveoutright
      mrsl blush "This is your last year of school. So make sure to make the best of it!" 
      $quest.day1_take2.advance("isabelle_volunteer")
    "Pass":
      show mrsl excited at move_to(.25)
      show isabelle excited at move_to(.75)
      $isabelle.love-=1
      mrsl excited "Come on, now. Don't be shy!"
      show isabelle sad
      mrsl thinking "..."
      mrsl surprised "I'm just going to pick someone, then."
      mrsl excited "[kate], you're up!"
      show isabelle skeptical at move_to(.5)
      show kate eyeroll at appear_from_right("right")
      kate eyeroll "How exciting."
      show isabelle skeptical at move_to(.75)
      hide kate with moveoutright
      show mrsl blush at move_to(.5)     
      hide isabelle with moveoutright
      mrsl blush "This is your last year of school. So make sure to make the best of it!"
      $quest.day1_take2.advance("isabelle_pass")
  mrsl smile "I think that's all for now. Don't forget to sign up for your focus classes!"
  mrsl smile "If you have any questions throughout the year, feel free to stop by here or at the pool."
  hide mrsl with Dissolve(.5)
  $kate["at_none_today"] = True
  $quest.act_one.start()
  return

label quest_day1_take2_lindsey_fall:
    #sfx crashing sound
    ## The thud after Lindsey falling
    play sound "falling_thud"
    lindsey "Eeep!"
    show lindsey LindseyPose00 with fadehold: #this gives the image both the fadehold (transition) and vpunch (transform) effects
      block:
        linear 0.05 yoffset 15
        linear 0.05 yoffset -15
        repeat 18
      linear 0.05 yoffset 0
    hide lindsey
    show lindsey LindseyPose00 #this is just to avoid the screen shake if the player is skipping
    "She tripped over the wet-floor sign. Where's the sign with a warning about hazardous signs?"
    "For once in my life, I happened to be at the right place at the right time."
    "It's hard to imagine a top athlete like [lindsey] ever misstepping, but maybe this is that one blue-moon occurrence."
    "Hmm... this is more of a beige-pink moon, though... a pristine one, without a single blemish or crater..."
    "One might even call it a full one, if it weren't partially obscured by a sports thong."
    lindsey LindseyPose00b "Effing shit! I swear to god!"
    menu(side="far_left"):
      "Help her up":
        $mc.love+=1
        $lindsey.love+=1
        $unlock_stat_perk("love8")
        mc "Are you okay?"
        show lindsey LindseyPose00 with Dissolve(.5)
        lindsey LindseyPose00 "..."
        $school_ground_floor["sign_fallen"]=True
        $lindsey["at_none_now"] = True
        $process_event("update_state")        
        hide lindsey with Dissolve(.5) 
        "Hmm... no thanks? Well, she did look really embarrassed."
        "She probably just wanted to get the hell out of here and hide her face. Been there, can relate."
        $quest.day1_take2.finish()
      "Enjoy the view":
        $unlock_stat_perk("lust9")
        $mc.lust+=1
        $lindsey.love-=1
        "Clumsy girls are my favorite."
        "Especially, angry clumsy girls in skirts and thongs."
        "Girls who do everything they can to keep their modesty and good reputation."
        "Who'd hate to have their pert bottoms exposed to half the school. "
        "Their silky unblemished buttcheeks.... split by a pair of risqué panties that only hide their most intimate parts..."
        "Just a thin layer of fabric between me and that wet squishy pussy."
        "Between that tight tight rose of forbidden lust and the unworthy..."
        show lindsey LindseyPose00 with Dissolve(.5)
        lindsey "Piece of crap scum sign!"
        show lindsey LindseyPose00b with Dissolve(.5)
        lindsey "Ugh..."
        "Should be illegal to look that good!"
        "Hours at the gym, thousands of squats. Her falling over is a public service."
        "Should be a weekly thing with tickets and popcorn. There's profit to be made here."
        "If only girls weren't so protective of their bodies..."
        $quest.day1_take2.finish()
        if game.quest_guide:
          $game.quest_guide="isabelle_tour"
        $school_ground_floor["sign_fallen"]=True
        $lindsey["at_none_now"] = True
    $unlock_replay("lindsey_fall")
    $kate["at_none_today"] = False
    jump goto_school_first_hall
