init python:
  class Quest_mrsl_number(Quest):
    title = "Morning Mist"
    class phase_1_hidden_number:
      description = "Nothing but a breath on the window."
      hint="What was [mrsl] writing on the [guard]'s booth?"
      
    class phase_1000_done:
      pass
    class phase_2000_failed:
      pass
      
      
  class Event_quest_mrsl_number(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      pass
    def on_quest_guide_mrsl_number(event):
      rv = []
      if not mc.owned_item("cup_of_coffee"):
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["items coffee"]))
        rv.append(get_quest_guide_marker("school_cafeteria","school_cafeteria_coffee", marker = ["items coffee"]))
      else:
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["items coffee"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["items coffee"]))      
      return rv
            