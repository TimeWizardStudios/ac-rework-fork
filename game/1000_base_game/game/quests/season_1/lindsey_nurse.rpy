init python:
  class Quest_lindsey_nurse(Quest):

    title = "Loser to the Rescue"

    class phase_1_fetch_the_nurse:#go get nurse
      hint = "She hit the ground quite hard. Medic!"

    class phase_2_return_to_gym:#player had nurse interaction and returns to gym
      hint = "Help for the tired and wounded."

    class phase_3_carry_to_nurse:#carry to nurse
      hint = "Strong arms. Do you work out a lot?"

    class phase_4_nurse_room:#finished quest but lindsey is being checked out behind the curtain
      hint = "Beyond the veil of dreams and fantasies, the promised land awaits."

    class phase_1000_done:
      description = "Everyone was saved. Especially [lindsey]. Double saved."

    class phase_2000_failed:
      description = "There's always casualties in war.\nThis wasn't a war, but that\ndidn't matter, did it?"


init python:
  class Event_quest_lindsey_nurse(GameEvent):

    def on_energy_cost(event,energy_cost,silent=False):
      if quest.lindsey_nurse == "fetch_the_nurse":
        return 0

    def on_can_advance_time(event):
      if quest.lindsey_nurse == "fetch_the_nurse":
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.lindsey_nurse=="carry_to_nurse" and new_location=="school_nurse_room":
        game.events_queue.append("isabelle_quest_isabelle_tour_carry_to_nurse")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "lindsey_nurse":
        mc["focus"] = ""
        lindsey.equip("lindsey_shirt")
        if quest["revert_quest_guide"]:
          game.quest_guide = "isabelle_tour"

    def on_quest_guide_lindsey_nurse(event):
      rv=[]

      if quest.lindsey_nurse in ("fetch_the_nurse","nurse_room"):
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_curtain_closed", marker = ["actions interact"], marker_sector="right_bottom", marker_offset=(50,200)))

      return rv
