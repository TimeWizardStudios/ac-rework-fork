init python:
  class Quest_nurse_strike_nude_office(Quest):
    title =  "Nure Strike Nude Office"
    #hidden = True
    class phase_1_start:
      hint = ""
    class phase_2_maxine_visitor:
      hint = ""
    class phase_99_next_visitor:
      hint = ""
    class phase_1000_done:
      description = "I've done it"
    class phase_2000_failed:
      description = "I'm done for"

  class Event_quest_nurse_strike_nude_office(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      pass
    def on_quest_guide_nurse_strike_nude_office(event):
      rv=[]
      return rv

label nurse_strike_nude_office_first:
  $nurse["outfit_stamp"] = nurse.outfit
  window hide
  if nurse.location == "school_nurse_room":
    show nurse neutral with Dissolve(.5)
  else:
    show nurse neutral at appear_from_right
    pause 0.5
  window auto
  nurse neutral "Gosh, I'm not sure you should be looking through that book\nright now..."
  mc "Hmm? This little book? And why is that?"
  nurse annoyed "Because... it's inappropriate, and there's people just outside."
  mc "But they don't know what these strikes mean, right?"
  mc "They're our little secret."
  nurse annoyed "Oh, I don't know. I get really nervous when you flip through\nthose pages."
  mc "I can imagine."
  mc "At any time, I could redeem a strike..."
  nurse afraid "Oh, but I don't think that's necessary! Anyone could walk in!"
  mc "But that's part of the fun, isn't it?"
  mc "As a matter of fact, I'm redeeming one strike right now."
  nurse surprised "You're joking!"
  mc "Does it look like I'm joking? Look, I already erased one strike."
  nurse afraid "Please, don't! Not right now!"
  mc "It's done. I'm not really in the mood for an argument right now."
  mc "But you know I have those photos."
  nurse sad "But—"
  mc "No buts except your butt."
  mc "Strip."
  nurse concerned "..."
  mc "You're spending the rest of the day naked in your office. End of argument."
  nurse concerned "Oh, my goodness..."
  nurse concerned "Gosh, okay, but what if someone comes in?"
  window hide
  pause 0.25
  if game.season == 1:
    $nurse.unequip("nurse_shirt")
  elif game.season == 2:
    $nurse.unequip("nurse_hat")
    $nurse.unequip("nurse_dress")
  pause 0.75
  window auto show
  show nurse thinking with dissolve2
  nurse thinking "This is enough, right?"
  mc "Keep going."
  nurse afraid "But I'll be naked!"
  mc "That's the point!"
  nurse annoyed "Oh, um. Very well. This is making me really nervous."
  window hide
  pause 0.25
  if game.season == 1:
    $nurse.unequip("nurse_bra")
  elif game.season == 2:
    $nurse.unequip("nurse_green_bra")
  pause 0.75
  window auto show
  show nurse neutral with dissolve2
  nurse neutral "I don't know what I'll do if someone comes in..."
  nurse neutral "Sorry, but this is..."
  mc "It's pretty hot, right?"
  mc "Stripping on command."
  nurse thinking "I don't know about that..."
  mc "Well, give me your panties and I'll have a look. I'm sure they're soaked."
  nurse concerned "That's not true..."
  mc "Let's find out."
  nurse concerned "Okay, okay. Gosh."
  window hide
  pause 0.25
  if game.season == 1:
    $nurse.unequip("nurse_pantys")
  elif game.season == 2:
    $nurse.unequip("nurse_green_panties")
  pause 0.75
  window auto show
  show nurse sad with dissolve2
  nurse sad "This must be enough, right?"
  mc "Well, you're completely naked, so I'd say so."
  mc "Unless you want to take a lap around the school?"
  nurse afraid "No, no! I'm fine, thank you!"
  mc "Keep your clothes locked in one of those drawers for the rest of\nthe day."
  mc "And don't you dare to put them on. I could come in at any moment."
  nurse annoyed "Okay, all right. I promise."
  $nurse["strike_book_nude_office_today"] = True
  $quest.nurse_strike_nude_office.advance("maxine_visitor")
  window hide
  hide nurse with Dissolve(.5)
  $nurse.outfit = nurse["outfit_stamp"]
  return
