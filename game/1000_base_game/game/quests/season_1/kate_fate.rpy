image kate_icon_smallest = Transform("kate contact_icon",zoom=0.25)
image love_icon_smallest = Transform("stats love",zoom=0.6)
image lust_icon_smallest = Transform("stats lust",zoom=0.65)

init python:
  class Quest_kate_fate(Quest):

    title = "Twisted Fate"

    class phase_1_peek:
      description = "Big G, little O! Go, go!"
    class phase_20_hunt:
      hint = "We're always running from something. Sometimes, we're running to get caught."
      quest_guide_hint = "Either escape/hide from [kate]\n({image=kate_icon_smallest}{image=love_icon_smallest}) or get caught ({image=kate_icon_smallest}{image=lust_icon_smallest})."
    class phase_30_rescue:
      hint = "Catcher in the rye or another face left in the dust? You decide!"
      quest_guide_hint = "Either quest [isabelle]/escape the first hall ({image=kate_icon_smallest}{image=love_icon_smallest}) or get caught\n({image=kate_icon_smallest}{image=lust_icon_smallest})."
    class phase_31_run:
      hint = "Run, [mc], run!"
      quest_guide_hint = "Either escape the first hall\n({image=kate_icon_smallest}{image=love_icon_smallest}) or get caught ({image=kate_icon_smallest}{image=lust_icon_smallest})."
    class phase_40_hunt2:
      hint = "It's not about how fast you can run — it's about how fast you can get run over."
      quest_guide_hint = "Either go to the English classroom ({image=kate_icon_smallest}{image=love_icon_smallest}) or get caught ({image=kate_icon_smallest}{image=lust_icon_smallest})."
    class phase_50_deadend:
      hint = "When one door closes...\nToo bad there's only one door."
      quest_guide_hint = "Leave the English classroom."
    class phase_60_escaped:
      pass
    class phase_1000_done:
      description = "Is this the end or only the beginning...?"


  class Event_quest_kate_fate(GameEvent):
    def on_can_advance_time(event):
      if quest.kate_fate.in_progress and quest.kate_fate < "escaped":
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_first_hall" and quest.kate_fate == "hunt":
        game.events_queue.append("quest_kate_fate_rescue_enter_first_hall")
      if new_location == "school_first_hall_west" and quest.kate_fate == "rescue":
        game.events_queue.append("quest_kate_fate_hunt2")
      if new_location == "school_first_hall_west" and quest.kate_fate == "run":
        game.events_queue.append("quest_kate_fate_run_to_art")
      if new_location == "school_english_class" and quest.kate_fate == "hunt2":
        game.events_queue.append("quest_kate_fate_rescue_enter_english")

    def on_enter_roaming_mode(event):
      if quest.kate_fate in ("hunt","rescue","run","hunt2","deadend"):
        if game.pc and game.pc.energy <= game.pc.min_energy:
          game.pc.energy=1
          game.events_queue.append("quest_kate_fate_out_of_energy_fail")
      elif game.pc and game.pc.energy <= game.pc.min_energy:
        game.advance()

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_fate" and quest.kate_fate == "escaped":
        game.events_queue.append("quest_kate_fate_escaped")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "kate_fate":
        school_gym["exclusive"] = 0
        kate.equip("kate_panties")
        kate.equip("kate_pants")
        kate.equip("kate_bra")
        kate.equip("kate_shirt")
        kate.equip("kate_necklace")
        mc["focus"] = ""

    def on_quest_guide_kate_fate(event):
      rv = []

      if quest.kate_fate == "hunt":
        rv.append(get_quest_guide_hud("time", marker="$Don't let your energy hit {color=#48F}0{/}\nor you'll get caught.", marker_offset = (270,-20)))
        if school_first_hall_east["vent_open"]:
          if school_first_hall_east["shoe1"] == school_first_hall_east["shoe2"] == school_first_hall_east["shoe3"] == school_first_hall_east["shoe4"] == school_first_hall_east["shoe5"] == school_first_hall_east["shoe6"] == school_first_hall_east["shoe7"] == school_first_hall_east["shoe8"] == school_first_hall_east["shoe9"] == school_first_hall_east["shoe10"] == school_first_hall_east["shoe11"] == True:
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent_ajar", marker = ["actions interact"], marker_sector="right_top", marker_offset = (0,-220)))
          else:
            if not school_first_hall_east["shoe1"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes1", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe2"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes2", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe3"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes3", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe4"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes4", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe5"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes5", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe6"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes6", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe7"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes7", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe8"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes8", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe9"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes9", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe10"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes10", marker = ["actions interact"], marker_offset = (-10,-10)))
            elif not school_first_hall_east["shoe11"]:
              rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_shoes11", marker = ["actions interact"], marker_offset = (-10,-10)))
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["$or{space=-50}"]+["actions go"]+["$"]))
      elif quest.kate_fate == "rescue":
        rv.append(get_quest_guide_hud("time", marker="$Don't let your energy hit {color=#48F}0{/}\nor you'll get caught.", marker_offset = (270,-20)))
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_first_hall","isabelle", marker = ["$or{space=-50}"]+["actions quest"]+["$"]))
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["$or{space=-50}"]+["actions go"]+["$"]))
      elif quest.kate_fate == "run":
        rv.append(get_quest_guide_hud("time", marker="$Don't let your energy hit {color=#48F}0{/}\nor you'll get caught.", marker_offset = (270,-20)))
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["actions go"]))
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["$or{space=-50}"]+["actions go"]+["$"]))
      elif quest.kate_fate == "hunt2":
        rv.append(get_quest_guide_hud("time", marker="$Don't let your energy hit {color=#48F}0{/}\nor you'll get caught.", marker_offset = (270,-20)))
        rv.append(get_quest_guide_path("school_english_class", marker = ["actions go"]))
      elif quest.kate_fate == "deadend":
        rv.append(get_quest_guide_hud("time", marker="$Don't let your energy hit {color=#48F}0{/}\nor you'll get caught.", marker_offset = (270,-20)))
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["actions go"]))
      return rv


label quest_kate_fate_rescue_enter_english:
  $quest.kate_fate.advance("deadend")
  "Phew! I think they lost me."
  "What a rush!"
  "Never thought I'd get away with taunting [kate] and escaping..."
  "This timeline truly is different."
  return

label quest_kate_fate_run_to_art:
  $quest.kate_fate.advance("hunt2")
  return

label quest_kate_fate_hunt2:
  "[isabelle] sure looked surprised when I ran past her..."
  "But I've never relied on other people to help me. Not going to start now.{space=-75}"
  $quest.kate_fate.advance("hunt2")
  return

label quest_kate_fate_rescue_enter_first_hall:
  "Thank god, there's [isabelle]!"
  $quest.kate_fate.advance("rescue")
  return

label quest_kate_fate_out_of_energy_fail:
  "Fuck... I'm in worse shape than I thought..."
  "My lungs are burning!"
  "The enemy is approaching like phantoms through the haze of oxygen deprivation..."
  "Truly, this is the end."
  window hide
  show black with Dissolve(.5)
  pause 1.0
  $game.location = school_gym
  $game.advance()
  pause 1.0
  $set_dialog_mode("default_no_bg")
  "..."
  "Ugh, passed out from overexertion..."
  "Huh? Why can't I move?"
  jump quest_kate_fate_caught
