init python:
  class Quest_maxine_wine(Quest):
    title = "Strawberries, Cherries"
    class phase_1_wine:
      description = "And an angel's kiss in spring..."
      hint = "Rebels know booze. That's a saying."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_2_pins:
      hint = "Pins and needles, that's what the conversations with her are usually like."
      quest_guide_hint = "Quest [flora]."
    class phase_3_locker:
      hint = "Time to be the poster boy of Newfall High."
      def quest_guide_hint(quest):
        if school_locker["flora_posters_taken"]:
          return "Put one poster up on each bulletin board in the school (entrance hall, admin wing, homeroom, sports wing, 1F hall)."
        else:
          return "Pick up [flora]'s posters in your locker."
    class phase_4_florareward:
      hint = "Work and pay. That's how it goes. [flora] can't trick me out of this one."
      quest_guide_hint = "Quest [flora]."
    class phase_5_jacklynpin:
      hint = "The kingpin to the art queen. Deliverin' the goods."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_6_search:
      hint = "According to [jacklyn], there used to be a hidden stash where the sun does shine..."
      quest_guide_hint = "Interact with the sun painting in the admin wing."
    class phase_7_maxinewine:
      hint = "Round and round and round we go, on the booze-to-[maxine] show."
      quest_guide_hint = "Quest [maxine] in the clubroom."
    class phase_1000_done:
      description = "The only thing missing now is the sex and the rock 'n' roll."

  class Event_quest_maxine_wine(GameEvent):
    def on_quest_guide_maxine_wine(event):
      rv = []
      if quest.maxine_wine == "wine":
        rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jacklyn", marker = ["actions quest"]))
      elif quest.maxine_wine == "pins":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
      elif quest.maxine_wine == "locker":
        if school_locker["flora_posters_taken"]:
          if not quest.maxine_wine["entrance_hall_poster"]:
            rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor board"]))
            rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_notice_board", marker = ["items flora_poster@.7"]))
          if not quest.maxine_wine["homeroom_poster"]:
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom notice_board@.5"]))
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_notice_board", marker = ["items flora_poster@.7"]))
          if not quest.maxine_wine["admin_wing_poster"]:
            rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west info@.2"]))
            rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_info", marker = ["items flora_poster@.7"]))
          if not quest.maxine_wine["1f_hall_poster"]:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall board@.6"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_board", marker = ["items flora_poster@.7"], marker_sector = "left_bottom", marker_offset = (120,20)))
          if not quest.maxine_wine["sports_wing_poster"]:
            rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east board@.6"]))
            rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_notice_board", marker = ["items flora_poster@.7"]))
        else:
          if not mc.at("school_locker"):
            rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
          if school_ground_floor["locker_unlocked"]:
            rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
          else:
            rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
          rv.append(get_quest_guide_marker("school_locker", "school_locker_flora_posters", marker = ["actions take"]))
      elif quest.maxine_wine == "florareward":
        if flora.at("home_bathroom"):
          rv.append(get_quest_guide_hud("time", marker = "$Wait for {color=#48F}[flora]{/} to leave the {color=#48F}bathroom{/}."))
        else:
          rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
      elif quest.maxine_wine == "jacklynpin":
        rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jacklyn", marker = ["actions quest"]))
      elif quest.maxine_wine == "search":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west sun_painting@.75"]))
        if quest.maxine_wine["hidden_stash_revealed"]:
          rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_wine", marker = ["actions take"], marker_sector = "left_top", marker_offset = (25,50)))
        else:
          rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_sun_painting", marker = ["actions interact"]))
      elif quest.maxine_wine == "maxinewine":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        if maxine.at("school_clubroom"):
          rv.append(get_quest_guide_marker("school_clubroom","maxine", marker = ["actions quest"], marker_offset = (75,25)))
        else:
          if mc.at("school_clubroom"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
      return rv
