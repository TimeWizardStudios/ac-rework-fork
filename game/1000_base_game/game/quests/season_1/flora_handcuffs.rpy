image call_flora = LiveComposite((0,0),(63,-53),Transform("flora contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))


init python:
  class Quest_flora_handcuffs(Quest):

    title = "Stainless Steal"

    class phase_1_order:
      description = "Thievery is subjective."
      hint = "Dark web, deep web, black market. Time to surf."
      def quest_guide_hint(quest):
        if home_computer["visited"]:
          if mc.money >= 2499:
            return "Buy the Scarlet Starlet's Cutie-Harlot from the black market."
          else:
            return "Gather $2499."
        else:
          return "Interact with the computer in your bedroom."

    class phase_2_package:
      hint = "Good things come to those\nthat wait."
      def quest_guide_hint(quest):
        if home_computer["cutie_harlot_ordered_today"]:
          return "Wait until " + game.dow_names[0 if game.dow == 6 else game.dow+1] + "."
        else:
          if mc.owned_item("cutie_harlot"):
            return "Give [flora] the Scarlet Starlet's Cutie-Harlot at home."
          else:
            return "Take the mysterious package from the kitchen, and then consume it."

    class phase_3_phone_call:
      hint = "It's time to make a call. Now or never... or later."
      quest_guide_hint = "Call [flora] from the school's entrance hall."

    class phase_4_steal:
      hint = "Steal the steel."
      quest_guide_hint = "Interact with the [guard]'s booth."

    class phase_1000_done:
      description = "It could've gone better, but it could also have gone worse."


  class Event_quest_flora_handcuffs(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.flora_handcuffs == "steal":
        return 0

    def on_can_advance_time(event):
      if quest.flora_handcuffs == "steal":
        return False

    def on_quest_started(event,quest,silent=False):
      if quest.id == "flora_handcuffs":
        black_market.add_item("cutie_harlot")

    def on_inventory_changed(event,char,item,old_count,new_count,silent=False):
      if home_computer["visited"] and char == black_market and new_count < old_count:
        home_computer.flags[item.id + "_ordered"] = home_computer.flags[item.id + "_ordered_today"] = True

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "flora_handcuffs":
        flora.equip("flora_panties")
        flora.equip("flora_bra")
        flora.equip("flora_skirt")
        flora.equip("flora_shirt")
        mc["focus"] = ""

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == flora.location and jacklyn["prom_disabled"] and not quest.flora_handcuffs["streaking_aftermath"]:
        game.events_queue.append("quest_flora_handcuffs_streaking_aftermath")

    def on_quest_guide_flora_handcuffs(event):
      rv = []

      if quest.flora_handcuffs == "order":
        if (game.location != "home_computer"
        and not (home_computer["visited"] and mc.money < 2499)):
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        if home_computer["visited"]:
          if mc.money >= 2499:
            rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions black_market"]))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$2499{/}.", marker_offset = (500,0)))
        else:
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))

      elif quest.flora_handcuffs == "package":
        if home_computer["cutie_harlot_ordered_today"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))
        else:
          if mc.owned_item("cutie_harlot"):
            if 20 > game.hour > 6:
              if 18 > game.hour > 8:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
                if game.location == "home_kitchen":
                  rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} between\n"+("{color=#48F}18:00{/} — {color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}6:00 PM{/} — {color=#48F}7:00 PM{/}.")))
              else:
                rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
                rv.append(get_quest_guide_marker(game.location, "flora", marker = ["items cutie_harlot@.85"]))
            else:
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} to\nwake up at "+("{color=#48F}7:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 AM{/}.")))
          else:
            if home_computer["cutie_harlot_received"]:
              rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
            else:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.65"]))
              rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_cutie_harlot", marker = ["actions take"]))

      elif quest.flora_handcuffs == "phone_call":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))
        if game.location == "school_ground_floor":
          rv.append(get_quest_guide_hud("phone", marker=["call_flora","$\n\n\n{space=30}Call {color=#48F}[flora]{/}."]))

      elif quest.flora_handcuffs == "steal":
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (70,170)))

      return rv
