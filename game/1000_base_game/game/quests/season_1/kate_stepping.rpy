image flora_room_door = LiveComposite((160,270),(0,0),"home hall door_center",(39,58),"home hall back_poster")
image combine_ice_tea_sleeping_pills = LiveComposite((264,96),(0-8,0+20),"items ice_tea",(95-8,4+20),Transform("actions combine",zoom=0.85),(183-8,13+20),Transform("items sleeping_pills",zoom=0.85))
image call_kate = LiveComposite((0,0),(63,-53),Transform("kate contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))


init python:
  class Quest_kate_stepping(Quest):

    title = "Stepping on the Rose"

    class phase_10_magazine:
      description = "Makeshift fashion icon."
      hint = "The place you can't enter without a written permit."
      quest_guide_hint = "Interact with the door to [flora]'s room."

    class phase_11_photoshop:
      hint = "From mind to matter. A thought{space=-5}\nturned paper."
      def quest_guide_hint(quest):
        if quest["print_it_out"]:
          return "Interact with the printer in the computer room."
        else:
          return "Interact with the computer in your bedroom."

    class phase_12_poster:
      hint = "Time to get it straight. Bubble-water-leveler straight."
      quest_guide_hint = "Go to the first hall."

    class phase_20_meeting:
      description = "The big meeting.\nEccentric meets evil."
      hint = "Don't be late to the meeting with [kate]; that's tempting fate."
      def quest_guide_hint(quest):
        if quest["mildly_important"]:
          return "Leave [maxine]'s office."
        else:
          return "Quest [maxine] in her office."

    class phase_30_gym:
      hint = "Speak to the devil in her own domain."
      quest_guide_hint = "Quest [kate] in the gym."

    class phase_40_independence_day:
      hint = "Ah, freedom! Ironic that such a concept is celebrated in this place."
      def quest_guide_hint(quest):
        if game.day >= 21:
          return "Interact with the Independence Day banner in the entrance hall."
        else:
          independence_day = (21 - game.day)
          return "Wait until Independence Day\n("+str(independence_day)+(" days left)." if independence_day > 1 else " day left).")

    class phase_50_spiked_drink:
      hint = "Mixing drinks? Why, I'm something of a bartender myself!"
      quest_guide_hint = "Combine the ice tea with the sleeping pills."

    class phase_60_isabelle:
      hint = "Sleep the beauty."
      quest_guide_hint = "Give [isabelle] the spiked ice tea while in the English classroom."

    class phase_70_phone_call:
      hint = "Call the queen bee."
      def quest_guide_hint(quest):
        if quest["hung_up"]:
          return "Call [kate] on your phone again."
        else:
          return "Call [kate] on your phone."

    class phase_80_party:
      hint = "Time to wait."
      def quest_guide_hint(quest):
        if persistent.time_format == "24h":
          return "Wait until 19:00."
        else:
          return "Wait until 7:00 PM."

    class phase_90_fly_to_the_moon:
      def hint(quest):
        if game.location == "school_gym":
          return "Party's over."
        else:
          return "When all else fails, bring out your inner Doom Guy. Hack and slash, rip and tear."
      def quest_guide_hint(quest):
        if game.location == "school_gym":
          return "Leave the school."
        elif mc.owned_item("fire_axe"):
          return "Use the fire axe on the door to the roof."
        else:
          return "Take the fire axe in the roof landing."

    class phase_1000_done:
      description = "Making the world a little worse,\none day at the time..."


init python:
  class Event_quest_kate_stepping(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.kate_stepping == "meeting" and game.location == "school_clubroom" and not quest.kate_stepping["mildly_important"])
      or quest.kate_stepping in ("phone_call","fly_to_the_moon")):
        return 0

    def on_can_advance_time(event):
      if ((quest.kate_stepping == "meeting" and game.location == "school_clubroom" and not quest.kate_stepping["mildly_important"])
      or quest.kate_stepping in ("phone_call","fly_to_the_moon")):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.kate_stepping == "poster" and new_location == "school_first_hall":
        game.events_queue.append("quest_kate_stepping_poster")
      if (quest.kate_stepping == "meeting" and quest.kate_stepping["mildly_important"]) and new_location == "school_homeroom":
        school_homeroom["exclusive"] = "True"
        game.events_queue.append("quest_kate_stepping_meeting")
      if quest.kate_stepping > "meeting" and quest.kate_stepping.in_progress and school_homeroom["exclusive"] and old_location == "school_homeroom":
        school_homeroom["exclusive"] = False
      if quest.kate_stepping == "gym":
        if new_location == "school_gym":
          kate["outfit_stamp"] = kate.outfit
          kate.outfit = {"bra": "kate_cheerleader_bra", "necklace": None, "panties": "kate_cheerleader_panties", "pants": "kate_cheerleader_skirt", "shirt": "kate_cheerleader_top"}
        elif old_location == "school_gym":
          kate.outfit = kate["outfit_stamp"]
      if quest.kate_stepping == "phone_call" and new_location == "school_first_hall_west":
        game.events_queue.append("quest_kate_stepping_phone_call")

    def on_advance(event):
      if quest.kate_stepping > "meeting" and quest.kate_stepping.in_progress and school_homeroom["exclusive"]:
        school_homeroom["exclusive"] = False
      elif quest.kate_stepping == "party" and game.hour == 19:
        game.events_queue.append("quest_kate_stepping_party")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_stepping":
        if quest.kate_stepping == "independence_day":
          kate.outfit = kate["outfit_stamp"]
          school_ground_floor["independence_day_banner"] = True
        elif quest.kate_stepping == "spiked_drink" and mc.owned_item("spiked_ice_tea"):
          quest.kate_stepping.advance("isabelle")

    def on_quest_guide_kate_stepping(event):
      rv = []

      if quest.kate_stepping == "magazine":
        rv.append(get_quest_guide_path("home_hall", marker = ["flora_room_door@.4"]))
        rv.append(get_quest_guide_marker("home_hall","home_hall_door_flora", marker = ["actions interact"]))

      elif quest.kate_stepping == "photoshop":
        if quest.kate_stepping["print_it_out"]:
          rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room printer@.5"]))
          rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_printer", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (170,30)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))

      elif quest.kate_stepping == "poster":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["actions go"]))

      elif quest.kate_stepping == "meeting":
        if quest.kate_stepping["mildly_important"]:
          rv.append(get_quest_guide_marker("school_clubroom", "school_clubroom_window", marker = ["actions go"], marker_offset = (0,0)))
        else:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75+75,25)))

      elif quest.kate_stepping == "gym":
        rv.append(get_quest_guide_path("school_gym", marker = ["kate contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_gym", "kate", marker = ["actions quest"], marker_offset = (50,25)))

      elif quest.kate_stepping == "independence_day":
        if game.day >= 21:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor independence_day_banner@.4"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_independence_day_decorations", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (630,-100)))
        else:
          independence_day = (21 - game.day)
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}Independence Day{/}\n({color=#48F}"+str(independence_day)+("{/} days left)." if independence_day > 1 else "{/} day left).")))

      elif quest.kate_stepping == "spiked_drink":
        rv.append(get_quest_guide_hud("inventory", marker = ["combine_ice_tea_sleeping_pills"], marker_sector = "mid_top", marker_offset = (55,0)))

      elif quest.kate_stepping == "isabelle":
        rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_english_class", "isabelle", marker = ["items spiked_ice_tea@.85"], marker_sector = "left_bottom", marker_offset = (75,-25)))

      elif quest.kate_stepping == "phone_call":
        if quest.kate_stepping["hung_up"]:
          rv.append(get_quest_guide_hud("phone", marker=["call_kate","$\n\n\n{space=5}Call {color=#48F}[kate]{/} again."]))
        else:
          rv.append(get_quest_guide_hud("phone", marker=["call_kate","$\n\n\n{space=35}Call {color=#48F}[kate]{/}."]))

      elif quest.kate_stepping == "party":
        rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 PM{/}.")))

      elif quest.kate_stepping == "fly_to_the_moon":
        if game.location == "school_gym":
          rv.append(get_quest_guide_path("school_entrance", marker = ["actions go"]))
        elif mc.owned_item("fire_axe"):
          rv.append(get_quest_guide_path("school_roof_landing", marker = ["door_to_the_roof@.1"]))
          rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_door", marker = ["items fire_axe@.9"], marker_sector = "left_bottom", marker_offset=(550,0)))
        else:
          rv.append(get_quest_guide_path("school_roof_landing", marker = ["fire_axe_cropped@.2"]))
          rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_fire_axe", marker = ["actions take"], marker_sector = "right_top", marker_offset=(75,-200)))

      elif quest.kate_stepping == "done":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["actions go"]))

      return rv
