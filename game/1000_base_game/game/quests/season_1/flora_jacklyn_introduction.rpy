init python:
  class Quest_flora_jacklyn_introduction(Quest):
    title = "Punked"
    class phase_1_start:
      description = "The slug deal never stopped, yet [flora]'s tummy flopped."
      hint = "I should talk to [flora] in the kitchen"
    class phase_2_bathroom:
      hint = "No, it's not weird to follow [flora] into the bathroom! Not in the slightest."
    class phase_3_jacklyn:
      hint = "There's cake to be had! Teacher assistants to snare!"
    class phase_4_outside:
      hint = "Perfect weather outside for a trap."
    class phase_1000_done:
      description = "A cakewalk in the park."

  class Event_quest_flora_jacklyn_introduction(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.flora_jacklyn_introduction.in_progress: 
        if quest.kate_blowjob_dream.in_progress and quest.kate_blowjob_dream>="get_dressed":
          pass
        else:
          if quest.flora_jacklyn_introduction == "bathroom" and new_location=="home_bathroom":
            game.events_queue.append("quest_flora_jacklyn_introduction_bathroom")
          if quest.flora_jacklyn_introduction == "outside" and new_location=="school_entrance" and not quest.isabelle_haggis in ("instructions","puzzle", "escape"):
            game.events_queue.append("quest_flora_jacklyn_introduction_outside")
  
    def on_quest_guide_flora_jacklyn_introduction(event):
      rv=[] 
      if quest.flora_jacklyn_introduction == "bathroom":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["flora contact_icon@.85"]))
      if quest.flora_jacklyn_introduction == "jacklyn":
        rv.append(get_quest_guide_char("jacklyn", marker_sector = "right_bottom", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jacklyn", marker = ["actions quest"]))
      if quest.flora_jacklyn_introduction == "outside":
        rv.append(get_quest_guide_path("school_entrance", marker = ["flora contact_icon@.85"]))
      return rv
