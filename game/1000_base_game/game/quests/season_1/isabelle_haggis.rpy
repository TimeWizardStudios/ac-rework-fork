image bayonets_etiquettes_interact = LiveComposite((260,90),(0,5),Transform("items book bayonets_etiquettes",zoom=0.9),(90,0),Transform("actions interact",zoom=0.85))


init python:
  class Quest_isabelle_haggis(Quest):

    title = "No True Scotswoman"

    class phase_1_start:
      description = "The taste of freedom always comes at a price. In this case, a bad breath and an upset stomach."
      hint = "Time to put [isabelle]'s love for Scotland to the test — a taste test."
    class phase_2_trouble:
      hint = "Looks like your actions have left a sour taste in everyone's mouth again. Mrs. Lichtenstein's in charge of the punishment."
    class phase_3_instructions:
      hint = "Getting through life is all about following instructions. Not that I know anything about getting through life."
    class phase_4_puzzle:
      hint = "Most epic teamwork exercise in history! Who wins? Who's next? You decide!"
      quest_guide_hint = "Search the bookshelf. Read. Dismantle the globe. Shoot. Escape."
    class phase_5_escape:
      hint = "Haggis and ladders — business as usual."
    class phase_6_choice:
      hint = "There's no \"I\" in teamwork, but there's definitely an \"m\" and an \"e\"!"
    class phase_1000_done:
      description = "Most epic teamwork exercise in history! Who wins? Who's next? You decide!"
    class phase_2000_failed:
      description = "Sometimes you win. Sometimes you lose. For me, it's usually the latter."


  class Event_quest_isabelle_haggis(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.isabelle_haggis >= "instructions" and quest.isabelle_haggis <= "escape" and quest.isabelle_haggis.in_progress:
        return 0

    def on_can_advance_time(event):
      if quest.isabelle_haggis >= "instructions" and quest.isabelle_haggis <= "escape" and quest.isabelle_haggis.in_progress:
        return False

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "isabelle_haggis":
        mc["focus"] = ""
        school_homeroom["exclusive"] = False
        if flora["brooch"] or isabelle["brooch"]:
          black_market.add_item("mrsl_brooch")

    def on_quest_guide_isabelle_haggis(event):
      rv=[]

      if quest.isabelle_haggis == "start":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["items haggis_lunchbox"]))

      if quest.isabelle_haggis == "trouble":
        rv.append(get_quest_guide_path("school_homeroom", marker = ["mrsl contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_homeroom", "mrsl", marker = ["actions quest"]))

      if quest.isabelle_haggis == "instructions":
        rv.append(get_quest_guide_marker("school_homeroom","school_homeroom_blackboard", marker = ["actions interact"]))

      if quest.isabelle_haggis in ("puzzle", "escape"):
        if not quest.isabelle_haggis["flora_endorse"]:
          if not quest.isabelle_haggis["eraser_found"]:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_trash_can", marker = ["actions take"], marker_offset = (-10,-10)))
          elif not quest.isabelle_haggis["cups_found"]:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["actions take"], marker_offset = (30,30)))
          elif not quest.isabelle_haggis["table_eraser"]:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_school_bench", marker = ["items eraser"], marker_sector = "left_bottom", marker_offset = (275,15)))
          elif not quest.isabelle_haggis["table_cup"] == 3:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_school_bench", marker = ["items coffee_cup"], marker_sector = "left_bottom", marker_offset = (275,15)))
          else:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_school_bench", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (275,15)))
        elif not quest.isabelle_haggis["isabelle_endorse"]:
          if quest.isabelle_haggis["flora_endorse"] == "isabelle":
            rv.append(get_quest_guide_marker("school_homeroom", "isabelle", marker = ["$or{space=-50}","actions quest","${space=50}"], marker_offset = (10,0)))
          if not (mc.owned_item("bayonets_etiquettes") or quest.isabelle_haggis["isabelle_regret"]):
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_bookshelf", marker = ["actions take"], marker_offset = (-10,-25)))
          elif not mc.owned_item("monkey_wrench"):
            rv.append(get_quest_guide_hud("inventory", marker = ["bayonets_etiquettes_interact"]))
          elif not (quest.isabelle_haggis["got_globe"] or school_homeroom["globe_fixed"]):
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_globe", marker = ["items monkey_wrench@.8"], marker_sector = "left_bottom", marker_offset = (130,0)))
          elif not (mc.owned_item(("greasy_bolt","bolt")) or school_homeroom["open_window"]):
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_basketball_hoop", marker = ["items globe"], marker_offset = (10,10)))
          elif not (mc.owned_item("bolt") or school_homeroom["open_window"]):
            rv.append(get_quest_guide_marker("school_homeroom", "flora", marker = ["items greasy_bolt@.9"], marker_offset = (110,10)))
          elif not school_homeroom["open_window"]:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_window", marker = ["items clean_bolt@.9"], marker_sector = "left_top", marker_offset = (190,90)))
          elif not quest.isabelle_haggis["first_window"]:
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_window", marker = ["actions go"], marker_sector = "left_top", marker_offset = (190,90)))
          else:
            if quest.isabelle_haggis == "puzzle":
              rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_window", marker = ["actions go"], marker_sector = "left_top", marker_offset = (190,90)))

      if quest.isabelle_haggis == "escape":
        if not (mc.owned_item("haggis_lunchbox") or quest.isabelle_haggis["isabelle_regret"]):
          rv.append(get_quest_guide_path("school_cafeteria",  marker = ["school cafeteria counterleft@.45"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_counter_left", marker = ["actions take"], marker_offset = (90,100), marker_sector = "left_top"))
        elif not (mc.owned_item("water_bottle") or quest.isabelle_haggis["isabelle_regret"]):
          if mc.owned_item("empty_bottle"):
            if quest.lindsey_wrong["fountain_state"] == 5:
              rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"]))
              rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_aquarium", marker = ["items bottle empty_bottle"], marker_sector = "left_bottom", marker_offset = (325,20)))
            else:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.6"]))
              rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["items bottle empty_bottle"], marker_sector = "left_bottom", marker_offset = (0,35)))
          else:
            if home_kitchen["water_bottle_taken"]:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
              rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
            else:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
              rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))
        elif not school_gym["ladder_taken"]:
          rv.append(get_quest_guide_path("school_gym", marker = ["school gym ladder@.4"]))
          rv.append(get_quest_guide_marker("school_gym", "school_gym_ladder", marker = ["actions take"], marker_offset = (50,0)))
        else:
          if game.location == "school_homeroom":
            if not quest.isabelle_haggis["isabelle_regret"]:
              rv.append(get_quest_guide_marker("school_homeroom", "isabelle", marker = ["items book bayonets_etiquettes@.9"], marker_offset = (10,0)))
            else:
              if quest.isabelle_haggis["haggis_regretful"]:
                rv.append(get_quest_guide_marker("school_homeroom", "flora", marker = ["items haggis_lunchbox@.8"], marker_offset = (110,10)))
              else:
                if mc.owned_item("haggis_lunchbox"):
                  rv.append(get_quest_guide_marker("school_homeroom", "isabelle", marker = ["items haggis_lunchbox@.8"], marker_offset = (10,0)))
                else:
                  rv.append(get_quest_guide_marker("school_homeroom", "flora", marker = ["actions quest"], marker_offset = (110,10)))
          else:
            rv.append(get_quest_guide_path("school_entrance", marker = ["school entrance window@.9"]))
            rv.append(get_quest_guide_marker("school_entrance", "school_entrance_window", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (70,-10)))

      if quest.isabelle_haggis["flora_endorse"] == quest.isabelle_haggis["isabelle_endorse"] == "mc":
        rv.append(get_quest_guide_marker("school_homeroom", "isabelle", marker = ["actions quest"], marker_offset = (10,0)))
        rv.append(get_quest_guide_marker("school_homeroom", "flora", marker = ["actions quest"], marker_offset = (110,10)))

      return rv
