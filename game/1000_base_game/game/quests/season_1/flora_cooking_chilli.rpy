image combine_spoon_spice = LiveComposite((273,125),(25,45),Transform("items spoon",zoom=0.65),(95,31),Transform("actions combine",zoom=0.85),(193,43),Transform("items spice",zoom=0.7))


init python:
  class Quest_flora_cooking_chilli(Quest):

    title = "Chili Con Carnal Sin"

    class phase_1_start:
      description = "[flora]'s threats and cooking —\nequally lethal."
      hint = "I should find [flora] roaming in the school and talk to her."
    class phase_2_cooking_pot:
      hint = "[flora] is looking to poison everyone at her internship. For the mad cook, only the biggest cauldron will suffice."
    class phase_3_bring_pot:
      hint = "Deliver the gargantuan pot to the mad cook."
    class phase_4_chilli_recipe_step_one:
      hint = "Cook or die."
    class phase_5_chop_onion:
      hint = "Cook or die."
    class phase_6_chilli_recipe_step_three:
      hint = "Cook or die."
    class phase_7_chilli_recipe_step_four:
      hint = "Cook or die."
    class phase_8_chilli_recipe_step_five:
      hint = "Cook or die."
    class phase_9_chilli_recipe_step_six:
      hint = "Cook or die."
    class phase_10_chilli_recipe_step_seven:
      hint = "Cook or die."
    class phase_11_chilli_recipe_step_eight:
      hint = "Cook or die."
    class phase_12_check_with_flora:
      hint = "Cook or die."
    class phase_13_try_again:
      hint = "Cook or die."
    class phase_14_chilli_done:
      hint = "[flora] left her phone in the bathroom. It's definitely in there, might have to think outside the box to find it."
    class phase_15_flora_called:
      hint = "She tried to hide it in the laundry basket, but she didn't expect your sudden burst of ingenuity. Let's be honest, no one did."
    class phase_16_return_phone:
      hint = "Dark web recipes, a bunch of [jacklyn] photos, and a locked folder named \"squidly.\" Best give [flora] her phone back before she gets suspicious."
    class phase_17_follow_bathroom:
      hint = "Spotted a fiery comet on collision course with the bathroom. Flor-bopp."
    class phase_18_enter_bathroom:
      hint = "As [jo] always says, \"[flora]'s problems are your problems.\" Strange how that doesn't go both ways."
    class phase_19_get_milk:
      hint = "I have to distract [jo]. Maybe if I take a shot and perform some light arsony?"
      quest_guide_hint = "Red: (Pan, Plastic Bag, Apron)\nGreen: (Knife, Cutting Board, Ketchup)\nBlue: (Lighter, Kitchen Towel, Shot Glass)\nPurple: (Measuring Beaker, Spices, Flour)"
    class phase_20_distracted:
      hint = "While the cat is gone, the rats plunder the fridge."
    class phase_21_got_liquid:
      hint = "Firefighter coming through. Pussy in need of immediate spray-down."
    class phase_1000_done:
      description = "Just another day in the fire department."


  class Event_quest_flora_cooking_chilli(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.flora_cooking_chilli.in_progress:
        if quest.flora_cooking_chilli == "enter_bathroom" and new_location=="home_bathroom":
          game.events_queue.append("flora_quest_flora_cooking_enter_bathroom")
        if quest.flora_cooking_chilli == "get_milk" and new_location=="home_kitchen":
          game.events_queue.append("flora_quest_flora_cooking_get_milk")
        if new_location == "home_hall" and quest.flora_cooking_chilli["wait_outside"]:
          game.events_queue.append("flora_quest_flora_cooking_wait_outside")

    def on_energy_cost(event,energy_cost,silent=False):
      if quest.flora_cooking_chilli >= "chilli_recipe_step_one" and quest.flora_cooking_chilli.in_progress:
        return 0

    def on_can_advance_time(event):
      if quest.flora_cooking_chilli >= "chilli_recipe_step_one" and quest.flora_cooking_chilli.in_progress:
        return False

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "flora_cooking_chilli":
        if home_hall["bathroom_locked_now"] and quest.flora_cooking_chilli == "chilli_done":
          del home_hall.flags["bathroom_locked_now"]
        if home_hall["bathroom_locked_today"] and quest.flora_cooking_chilli == "chilli_done":
          del home_hall.flags["bathroom_locked_today"]

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "flora_cooking_chilli":
        mc["focus"] = ""

    def on_quest_guide_flora_cooking_chilli(event):
      rv=[]
      if quest.flora_cooking_chilli == "cooking_pot":
        rv.append(get_quest_guide_path("school_cafeteria"))
        rv.append(get_quest_guide_marker("school_cafeteria","school_cafeteria_cooking_pot", marker = ["actions take"]))
      if quest.flora_cooking_chilli == "bring_pot":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"]))
      if quest.flora_cooking_chilli == "chilli_recipe_step_one":
        if quest.flora_cooking_chilli["drawers_interacted_with"] == 0:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d1", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d2", marker = ["actions interact"]))
      if quest.flora_cooking_chilli == "chop_onion":
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_onion", marker = ["actions interact"]))
      if quest.flora_cooking_chilli in ("check_with_flora", "try_again"):
          rv.append(get_quest_guide_marker("home_kitchen","flora", marker = ["actions quest"]))
      if quest.flora_cooking_chilli == "chilli_recipe_step_three":
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_cooking_pot", marker = ["items onion_slice", "items cooking_pot"]))
      if quest.flora_cooking_chilli == "chilli_recipe_step_four":
        if quest.flora_cooking_chilli["drawers_interacted_with"] == 0:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_s1", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d2", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_c1", marker = ["actions interact"]))
      if quest.flora_cooking_chilli == "chilli_recipe_step_five":
        if quest.flora_cooking_chilli["drawers_interacted_with"] == 0:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d1", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d1", marker = ["actions interact"]))
      if quest.flora_cooking_chilli == "chilli_recipe_step_six":
        if mc.owned_item("spoonful_of_spice"):
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_cooking_pot", marker = ["items spoonful_of_spice@.6"]))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["combine_spoon_spice"], marker_sector = "mid_top", marker_offset = (60,0)))
      if quest.flora_cooking_chilli == "chilli_recipe_step_seven":
        if quest.flora_cooking_chilli["drawers_interacted_with"] == 0:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_s1", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_c1", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d1", marker = ["actions interact"]))
      if quest.flora_cooking_chilli == "chilli_recipe_step_eight":
        if quest.flora_cooking_chilli["drawers_interacted_with"] == 0:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d2", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d3", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d3", marker = ["actions interact"]))
        elif quest.flora_cooking_chilli["drawers_interacted_with"] == 3:
          rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_c1", marker = ["actions interact"]))
      if quest.flora_cooking_chilli == "chilli_done":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["items flora_phone"]))
        if game.location == "home_bathroom":
          rv.append(get_quest_guide_hud("phone",marker=["flora contact","phone apps contact_info call@0.5","$Call [flora]"]))
      if quest.flora_cooking_chilli == "flora_called":
        rv.append(get_quest_guide_marker("home_bathroom","home_bathroom_laundry_basket",marker = ["items flora_phone"], marker_offset = (150,0), marker_sector = "left_bottom"))
      if quest.flora_cooking_chilli == "return_phone":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"]))
      if quest.flora_cooking_chilli == "follow_bathroom":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["flora contact_icon@.85"]))
      if quest.flora_cooking_chilli == "get_milk":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_c1", marker = ["$Ketchup"]))
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d1", marker = ["$Knife"]))
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_d2", marker = ["$Board"]))
      if quest.flora_cooking_chilli == "distracted":
        rv.append(get_quest_guide_marker("home_kitchen","home_kitchen_mf", marker = ["items bottle milk"]))
      if quest.flora_cooking_chilli == "got_liquid":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_bathroom", "flora", marker = ["actions give"]))
      return rv
