image fill_bottle_with_strawberry_juice = LiveComposite((310,93),(0,27),Text("Use",style="quest_guide_marker_text"),(52,0),"items bottle empty_bottle",(135,27),Text(", then choose",style="quest_guide_marker_text"),(310,0),"items bottle strawberry_juice")
image fill_spray_bottle_with_strawberry_juice = LiveComposite((310,93),(0,27),Text("Use",style="quest_guide_marker_text"),(52,0),Transform("items bottle spray_empty_bottle",zoom=0.75),(135,27),Text(", then choose",style="quest_guide_marker_text"),(310,0),Transform("items bottle spray_strawberry_juice",zoom=0.75))


init python:
  class Quest_nurse_photogenic(Quest):

    title = "Photogenic"

    class phase_1_stairs:
      description = "If anyone asks, you\ndon't know me."

    class phase_2_eavesdrop:
      hint = "Listen carefully. No, that's it."
      def quest_guide_hint(quest):
        if game.location == "school_nurse_room":
          return "Interact with the curtain."
        else:
          return "Go to the [nurse]'s office."

    class phase_1000_done:
      description = "Sleeping pills — the woke man's worst enemy."


  class Event_quest_nurse_photogenic(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.nurse_photogenic == "eavesdrop":
        return 0

    def on_can_advance_time(event):
      if quest.nurse_photogenic == "eavesdrop":
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if game.season == 1:
        if new_location in ("school_first_hall","school_roof_landing") and quest.isabelle_tour.finished and quest.nurse_photogenic["strawberry_juice_consumed_today"] and not quest.nurse_photogenic.started and not mc["focus"]:
          game.events_queue.append("quest_nurse_photogenic_start")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "nurse_photogenic" and quest.nurse_photogenic == "eavesdrop":
        school_ground_floor_west["nurse_room_locked_now"] = school_ground_floor_west["nurse_room_locked_today"] = False
        school_nurse_room["curtain_off"] = False

    def on_quest_guide_nurse_photogenic(event):
      rv = []

      if quest.nurse_photogenic == "eavesdrop":
        if game.location == "school_nurse_room":
          rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_curtain_closed", marker = ["actions interact"], marker_sector="right_bottom", marker_offset=(50,200)))
        else:
          rv.append(get_quest_guide_path("school_nurse_room", marker = ["actions go"]))

      return rv
