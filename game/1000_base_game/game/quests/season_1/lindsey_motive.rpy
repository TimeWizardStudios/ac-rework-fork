image window_open = LiveComposite((929,474), (0,0), "school first_hall window", (33,34), "school first_hall window_open", (39,181), "school first_hall butterflies2")
image bathroom_door = LiveComposite((84,498), (0,0), "school first_hall_east bathroom", (27,78), "school first_hall_east board")


init python:
  class Quest_lindsey_motive(Quest):

    title = "A Beautiful Motive"

    class phase_1_paint:
      description = "Every stroke has an artist."
      hint = "You wouldn't download a tube of paint."
      def quest_guide_hint(quest):
        if quest["supplies_outside"]:
          return "Take the tubes of paint from the{space=-10}\npaint shelf in the art classroom when [jacklyn] isn't around."
        else:
          return "Take the tubes of paint from the{space=-10}\npaint shelf in the art classroom."

    class phase_2_hidden:
      hint = "You wouldn't download a tube of paint."
      quest_guide_hint = "Interact with the chair next to the paint shelf."

    class phase_3_jacklyn:
      hint = "[jacklyn] knows something, maybe?"
      quest_guide_hint = "Quest [jacklyn]."

    class phase_4_maxine:
      hint = "[maxine] knows something, definitely."
      quest_guide_hint = "Quest [maxine]."

    class phase_5_google:
      hint = "Google is your friend."
      quest_guide_hint = "Interact with either the leftmost computer in the computer room or the one in your bedroom."

    class phase_6_isabelle:
      hint = "Witness interrogation!"
      quest_guide_hint = "Quest [isabelle]."

    class phase_7_sleep:
      hint = "When all else fails — nap time."
      quest_guide_hint = "Go to sleep."

    class phase_8_lindsey:
      hint = "Don't be late for your painting date, mate."
      quest_guide_hint = "Quest [lindsey]."

    class phase_90_date_lindsey:
      hint = "Don't be late for your painting date, mate."
      quest_guide_hint = "Leave the school."

    class phase_91_date_mrsl:
      hint = "Don't be late for your painting date, mate."
      quest_guide_hint = "Leave the school."

    class phase_10_phone:
      hint = "Sleuthing in the forbidden land."
      quest_guide_hint = "Go to the women's bathroom."

    class phase_11_distraction:
      hint = "When the eye in the sky sees too much, poke it out."
      def quest_guide_hint(quest):
        if quest["movement"]:
          return "Interact with the window in the first hall."
        elif quest["security_camera"]:
          return "Interact with the camera in the first hall."
        else:
          return "Go to the first hall."

    class phase_12_keys:
      hint = "Time to see the authorities."
      def quest_guide_hint(quest):
        if quest["scram"]:
          return "Quest [jo] in the cafeteria."
        else:
          return "Either quest [jo] in the cafeteria, or interact with the [guard]'s booth."

    class phase_13_window_open:
      hint = "Don't touch the glass! Or do. You're not the one cleaning it."
      quest_guide_hint = "Interact with the window in the first hall."

    class phase_14_lure:
      hint = "Pollination time!"
      def quest_guide_hint(quest):
        if school_first_hall_west["plant_taken"]:
          return "Use the plant on the window in the first hall."
        else:
          return "Take the plant in the fine arts wing."

    class phase_15_power_cut:
      hint = "Cut the crap. And by crap, I mean{space=-10}\nelectricity."
      quest_guide_hint = "Interact with the fuse box in the first hall."

    class phase_16_bathroom:
      hint = "A journey into the garden of Eden."
      quest_guide_hint = "Go to the women's bathroom."

    class phase_17_steal:
      hint = "It's not wrong to ruin the surveillance system, break into the women's bathroom, and go through someone's phone if it's for a good cause!"
      quest_guide_hint = "Interact with the middle locker.{space=-5}"

    class phase_18_hide:
      hint = "Time to hide. No, the basketball hoop isn't a good spot."
      quest_guide_hint = "Interact with the teacher's desk in the homeroom."

    class phase_1000_done:
      description = "The tip of the iceberg."


init python:
  class Event_quest_lindsey_motive(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.lindsey_motive == "paint" and interactable == "school_art_class_shelf")
      or quest.lindsey_motive in ("hidden","steal")
      or (quest.lindsey_motive == "hide" and game.location == "school_homeroom")):
        return 0

    def on_can_advance_time(event):
      if (quest.lindsey_motive in ("hidden","steal")
      or (quest.lindsey_motive == "hide" and game.location == "school_homeroom")):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_gym" and quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.lindsey_wrong.finished and quest.lindsey_piano.finished and quest.jacklyn_broken_fuse.finished and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and quest.maxine_eggs.finished and not quest.lindsey_motive.started and game.quest_guide == "lindsey_motive" and "quest_kate_fate_start" in game.events_queue:
        school_gym["exclusive"] = 0
        game.events_queue.remove("quest_kate_fate_start")
      if quest.lindsey_motive == "paint" and new_location == "school_art_class" and not quest.lindsey_motive["supplies_outside"]:
        game.events_queue.append("quest_lindsey_motive_paint_supplies_outside")
      if quest.lindsey_motive == "date_lindsey" and new_location == "school_entrance":
        game.events_queue.append("quest_lindsey_motive_date_lindsey")
      if quest.lindsey_motive == "date_mrsl" and new_location == "school_entrance":
        game.events_queue.append("quest_lindsey_motive_date_mrsl")
      if quest.lindsey_motive == "distraction" and new_location == "school_first_hall" and not quest.lindsey_motive["security_camera"]:
        game.events_queue.append("quest_lindsey_motive_distraction")
      if quest.lindsey_motive == "steal" and new_location == "school_bathroom":
        game.events_queue.append("quest_lindsey_motive_steal")
      if quest.lindsey_motive == "hide" and old_location == "school_first_hall_east" and school_first_hall_east["exclusive"]:
        school_first_hall_east["exclusive"] = False
      if quest.lindsey_motive == "hide" and new_location == "school_homeroom" and not mc["focus"]:
        game.events_queue.append("quest_lindsey_motive_hide")
        if "event_player_force_go_home_at_night" in game.events_queue:
          game.events_queue.remove("event_player_force_go_home_at_night")

    def on_advance(event):
      if quest.lindsey_motive == "jacklyn":
        school_art_class["chair2_moved"] = False
      if quest.lindsey_motive == "hide" and game.hour in (18,19) and not game.location == "school_homeroom" and not mc["focus"]:
        game.events_queue.append("quest_lindsey_motive_hide_advance")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "lindsey_motive":
        if quest.lindsey_motive == "maxine":
          school_art_class["chair2_moved"] = False
        if quest.lindsey_motive == "google":
          school_computer_room["unlocked"] = True
        if quest.lindsey_motive == "phone":
          mrsl.equip("mrsl_panties")
          mrsl.equip("mrsl_dress")
        if quest.lindsey_motive == "bathroom":
          school_first_hall_east["exclusive"] = True
        if quest.lindsey_motive == "hide":
          school_homeroom["exclusive"] = "True"

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "lindsey_motive":
        school_homeroom["exclusive"] = False

    def on_quest_guide_lindsey_motive(event):
      rv = []

      if quest.lindsey_motive == "paint":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
        if jacklyn.at("school_art_class") and mc.at("school_art_class"):
          rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} to leave\nthe art classroom."))
        else:
          rv.append(get_quest_guide_marker("school_art_class","school_art_class_shelf", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (580,50)))

      elif quest.lindsey_motive == "hidden":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class chair2@.45"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_chairs", marker = ["actions interact"], marker_offset = (-900,-40)))

      elif quest.lindsey_motive == "jacklyn":
        rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jacklyn", marker = ["actions quest"]))

      elif quest.lindsey_motive == "maxine":
        rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.lindsey_motive == "google":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["$or{space=-25}","home bedroom small_pc@.4","${space=25}"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["$or{space=-50}","actions interact","${space=50}"]))
        rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room screen1@.35"]))
        rv.append(get_quest_guide_marker("school_computer_room", "school_computer_room_screen_left", marker = ["actions interact"]))

      elif quest.lindsey_motive == "isabelle":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.lindsey_motive == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))

      elif quest.lindsey_motive == "lindsey":
        rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "lindsey", marker = ["actions quest"]))

      elif quest.lindsey_motive in ("date_lindsey","date_mrsl"):
        rv.append(get_quest_guide_path("school_entrance", marker = ["actions go"]))

      elif quest.lindsey_motive == "phone":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_door_bathroom", marker = ["actions go"]))

      elif quest.lindsey_motive == "distraction":
        if quest.lindsey_motive["movement"]:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall window@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_window", marker = ["actions interact"], marker_offset=(-15,-25)))
        elif quest.lindsey_motive["security_camera"]:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall cctv@.8"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_camera", marker = ["actions interact"], marker_sector = "right_top", marker_offset=(0,-125)))
        else:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["actions go"]))

      elif quest.lindsey_motive == "keys":
        if not quest.lindsey_motive["scram"]:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["$or{space=-50}","school ground_floor guard_booth@.35","${space=50}"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["$or{space=-50}","actions interact","${space=50}"], marker_sector = "right_top", marker_offset = (70,170)))
        if jo.at("school_*"):
          rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))
        else:
          if mc.at("school_*"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} to come to school."))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} to go to school."))

      elif quest.lindsey_motive == "window_open":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["window_open@.125"]))
        rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_window", marker = ["actions interact"], marker_offset=(-15,-25)))

      elif quest.lindsey_motive == "lure":
        if school_first_hall_west["plant_taken"]:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["window_open@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_window", marker = ["items plant"], marker_offset=(-15,-25)))
        else:
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west plant@.7"]))
          rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_plant", marker = ["actions take"]))

      elif quest.lindsey_motive == "power_cut":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
        rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["actions interact"], marker_offset = (60,90)))

      elif quest.lindsey_motive == "bathroom":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["bathroom_door@.3"]))
        rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_door_bathroom", marker = ["actions go"]))

      elif quest.lindsey_motive == "steal":
        rv.append(get_quest_guide_marker("school_bathroom", "school_bathroom_locker2", marker = ["actions interact"]))

      elif quest.lindsey_motive == "hide":
        rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom desk@.35"]))
        rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (200,20)))

      return rv
