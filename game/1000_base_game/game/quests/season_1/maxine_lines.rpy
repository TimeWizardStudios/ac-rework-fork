image fill_bottle_with_seven_hp = LiveComposite((310,93),(0,27),Text("Use",style="quest_guide_marker_text"),(52,0),"items bottle empty_bottle",(135,27),Text(", then choose",style="quest_guide_marker_text"),(310,0),"items bottle seven_hp")
image fill_spray_bottle_with_seven_hp = LiveComposite((310,93),(0,27),Text("Use",style="quest_guide_marker_text"),(52,0),Transform("items bottle spray_empty_bottle",zoom=0.75),(135,27),Text(", then choose",style="quest_guide_marker_text"),(310,0),Transform("items bottle spray_seven_hp",zoom=0.75))
image fuse_box_cropped = Crop((1,205,52,55),"school first_hall electric")


init python:
  class Quest_maxine_lines(Quest):

    title = "The Ley of the Land"

    class phase_1_attention:
      description = "Down the rabbit hole.\nYou know which one."
      hint = "Not through the looking glass, but to the place with the big window."
      quest_guide_hint = "Leave the first hall."
    class phase_2_locator:
      description = "Down the rabbit hole.\nYou know which one."
      hint = "Next stop: crazy town, and talk to the locals. Well... local."
      quest_guide_hint = "Quest [maxine] in her office."
    class phase_3_shot:
      hint = "It's not always about jumping through hoops. Sometimes you have to hit them."
      def quest_guide_hint(quest):
        if maxine["hit_the_hoop"]:
          return "Gulp down an entire 7 HP bottle, then hit a shot in the gym."
        else:
          return "Quest [maxine] in the gym."
    class phase_4_reward:
      hint = "Crazy town. Wonderland. Same thing, really."
      def quest_guide_hint(quest):
        if maxine["hidden_passage"]:
          if mc.at("school_clubroom"):
            return "Quest [maxine]."
          else:
            return "Go to [maxine]'s office."
        else:
          return "Follow [maxine]."
    class phase_5_battery:
      hint = "Look, you're probably going to need the quest guide for this one, so I won't waste my breath."
      def quest_guide_hint(_quest):
        if not mc["lll_dead"]:
          return "Interact with the Ley Line Locator in your inventory."
        elif quest.jacklyn_broken_fuse.in_progress and quest.jacklyn_broken_fuse < "reward":
          return "Finish \"A Short Fuse.\""
        elif not school_computer_room["charger_cable_taken"]:
          return "Pick up the charger cable from the box of cables in the computer classroom."
        elif not school_first_hall["cable_surgery"]:
          return "Use the charger cable on the fuse box in the first hall."
        elif not mc["crafted_sliced_cable"]:
          return "Slice the charger cable by combining it with any of the glass shard, beaver pelt, or beaver carcass."
        elif not school_first_hall["personal_charging_station"]:
          return "Use the sliced cable on the fuse box in the first hall."
        else:
          return "Use the Ley Line Locator on the fuse box in the first hall."
    class phase_6_charge:
      hint = "Wow! Look at you, inspector gadget. Time to push the right buttons!"
      quest_guide_hint = "Press the blue button on the Ley Line Locator in each of the first hall, arts wing, and gym. Once all squares are yellow, press the red button."
    class phase_7_insight:
      hint = "Talk to the brain behind the operation."
      quest_guide_hint = "Quest [maxine] in her office."
    class phase_8_magnetic:
      hint = "Attract, repulse. Magnetize! It's all about position and alignment."
      def quest_guide_hint(quest):
        if not school_computer_room["box_lines_magnet"]:
          return "Take the first magnet from the box of cables in the computer classroom."
        elif not home_bedroom["lines_magnet"]:
          return "Order the second magnet from either the leftmost computer in the computer room or the one in your bedroom."
        elif not home_hall["closet_lines_magnet"]:
          return "Take the third magnet from the storage closet in the hallway at home."
        elif not home_kitchen["magnet_package_consumed"]:
          if mc["package_due_today"]:
            return "Wait a day for the last magnet to arrive."
          else:
            return "Take the mysterious package from the kitchen and then consume it."
        else:
          return "Use one magnet on each of the circles (first hall, arts wing, and gym), then press the red button on the Ley Line Locator."
    class phase_9_finalnode:
      hint = "There's this one locker that few know about... it's time to screw around."
      def quest_guide_hint(quest):
        if school_ground_floor["final_circle"]:
          return "Unscrew the loose panel inside the secret locker."
        else:
          return "Leave the first hall."
    class phase_10_unscrew:
      hint = "She has color, she has style, she will peg you with a smile."
      quest_guide_hint = "Quest [jacklyn]."
    class phase_11_helpinghand:
      hint = "The exit hall would be a better name..."
      quest_guide_hint = "Go to the entrance hall."
    class phase_12_doll:
      hint = "Secrets within secrets!"
      quest_guide_hint = "Interact with the removed panel inside the secret locker."
    class phase_13_seemaxine:
      hint = "Do you still doubt her? Just admit that she's a genius."
      quest_guide_hint = "Quest [maxine] in her office."
    class phase_14_prison:
      hint = "Magnetize triangularis! Dollus dollus!"
      quest_guide_hint = "Use all three magnets on the saucer-eyed doll."
    class phase_1000_done:
      description = "Well, the doll is taken care of... whether it had anything to do with [lindsey] remains to be seen."
    class phase_2000_failed:
      description = "Not this time. Not this rabbit. Not this hole. Damn it, why does everything come out dirty?"


  class Event_quest_maxine_lines(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.maxine_lines == "attention" and new_location == "school_ground_floor":
        game.events_queue.append("school_ground_floor_stairs_quest_lindsey_wrong_fall")
      if new_location == "school_clubroom" and quest.maxine_lines == "locator" and not quest.kate_trick.in_progress:
        game.events_queue.append("maxine_lines_enter_clubroom")
      if quest.maxine_lines == "reward" and old_location == "school_gym" and not maxine["hidden_passage"]:
        game.events_queue.append("maxine_lines_reward_leave_gym")
      if quest.maxine_lines in ("charge","magnetic","finalnode"):
        if new_location.id.startswith("home_"):
          quest.maxine_lines['lll_charged'] = 0
        if new_location == "school_leyline_locator":
          pass
      if quest.maxine_lines == "finalnode" and new_location == "school_first_hall" and not school_first_hall["new_line"]:
        game.events_queue.append("maxine_lines_finalnode_new_line")
      if quest.maxine_lines == "finalnode" and new_location == "school_ground_floor" and not school_ground_floor["final_circle"]:
        game.events_queue.append("maxine_lines_finalnode_final_circle")
      if quest.maxine_lines == "helpinghand":
        if new_location == "school_ground_floor" and not quest.maxine_lines['helpinghand_done']:
          quest.maxine_lines['helpinghand_done'] = True
          game.events_queue.append("quest_maxine_lines_helpinghand")
      if quest.maxine_lines == "doll":
        if new_location == "school_secret_locker" and not quest.maxine_lines["wtf_is_that"]:
          game.events_queue.append("maxine_lines_doll_locker_arrive")

    def on_quest_guide_maxine_lines(event):
      rv = []
      if quest.maxine_lines == "attention":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))
      elif quest.maxine_lines == "locator":
        if quest.kate_trick.in_progress:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}The Dog Trick{/}."))
        elif quest.clubroom_access.finished:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
        else:
          if quest.clubroom_access.in_progress:
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Oxygen For Oxymoron{/}."))
          else:
            rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.4"]))
            rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["actions quest"], marker_sector="right_top", marker_offset = (0,-220)))
      elif quest.maxine_lines == "shot":
        if quest.maxine_lines["doped_up_now"]:
          rv.append(get_quest_guide_path("school_gym", marker = ["school gym ball_throw@.5"]))
          if school_gym["ball_3_hidden"]:
            rv.append(get_quest_guide_marker("school_gym", "school_gym_balls", marker = ["actions interact"], marker_offset = (-10,-10)))
          else:
            rv.append(get_quest_guide_marker("school_gym", "school_gym_balls", marker = ["actions interact"], marker_offset = (-180,220)))
        else:
          if quest.maxine_lines["maybe_doping"]:
            if mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle")):
              x = mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle"))
              rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x, "actions consume@.85"], marker_offset = (60,0), marker_sector = "mid_top"))
            elif mc.owned_item("empty_bottle"):
              x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
              rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
              rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["fill_"+x+"bottle_with_seven_hp"], marker_offset = (25,-15)))
            else:
              if home_kitchen["water_bottle_taken"]:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
              else:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
                rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))
          else:
            if maxine["hit_the_hoop"]:
              rv.append(get_quest_guide_path("school_gym", marker = ["school gym ball_throw@.5"]))
              rv.append(get_quest_guide_marker("school_gym", "school_gym_balls", marker = ["actions interact"], marker_offset = (-180,220)))
            else:
              rv.append(get_quest_guide_path("school_gym", marker = ["maxine contact_icon@.85"]))
              rv.append(get_quest_guide_marker("school_gym", "maxine", marker = ["actions quest"], marker_offset = (40,-10)))
      elif quest.maxine_lines == "reward":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
      elif quest.maxine_lines == "battery":
        if mc["lll_dead"]:
          if quest.jacklyn_broken_fuse.in_progress and quest.jacklyn_broken_fuse < "reward":
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}A Short Fuse{/}."))
          else:
            if school_first_hall["cable_surgery"]:
              if school_first_hall["personal_charging_station"]:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["items ley_line_locator@.6"], marker_offset = (60,90)))
              elif mc.owned_item("sliced_cable"):
                rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["items sliced_cable@.7"], marker_offset = (60,90)))
              elif mc.owned_item(("beaver_carcass","glass_shard")) and mc.owned_item("charger_cable"):
                x = mc.owned_item(("beaver_carcass","glass_shard"))
                rv.append(get_quest_guide_hud("inventory", marker = ["items "+x,"actions combine@.85","items charger_cable@.7"]))
              else:
                if not mc.owned_item("glass_shard"):
                  if mc.owned_item(("rock","monkey_wrench","soup_can","baseball","baseball_bat","umbrella")):
                    x = mc.owned_item(("rock","monkey_wrench","soup_can","baseball","baseball_bat","umbrella"))
                    rv.append(get_quest_guide_path("school_gym", marker = ["school gym light@.7"]))
                    rv.append(get_quest_guide_marker("school_gym", "school_gym_light", marker = ["items "+x+"@.8"], marker_sector = "right_top", marker_offset = (-10,-20)))
                  else:
                    rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall bin@.8"]))
                    rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_bin", marker = ["actions interact"], marker_sector = "right_bottom"))
            else:
              if mc.owned_item("charger_cable"):
                rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["items charger_cable@.7"], marker_offset = (60,90)))
              else:
                rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room cable_box@.7"]))
                rv.append(get_quest_guide_marker("school_computer_room", "school_computer_room_cable_box", marker = ["actions take"], marker_sector = "right_bottom"))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["items ley_line_locator@.75","actions interact@.85"], marker_offset = (60,0), marker_sector = "mid_top"))
      elif quest.maxine_lines == "charge":
        if quest.maxine_lines["lll_charged"]:
          if quest.maxine_lines["leyline_light_3"] == quest.maxine_lines["leyline_light_2"] == quest.maxine_lines["leyline_light_1"] == "yellow":
            if game.location == "school_leyline_locator":
              rv.append(get_quest_guide_marker("school_leyline_locator", "locator_button_2", marker = ["actions interact@.85"], marker_offset=(350,-50)))
            else:
              rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
          else:
            if not quest.maxine_lines["leyline_light_1"] == "yellow":
              if game.location == "school_leyline_locator":
                if quest.maxine_lines["leyline_location"] == "school_first_hall":
                  rv.append(get_quest_guide_marker("school_leyline_locator", "locator_button_1", marker = ["actions interact"], marker_offset=(350,-50)))
                else:
                  rv.append(get_quest_guide_path("school_first_hall", marker = ["items ley_line_locator@.7"]))
              else:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["items ley_line_locator@.7"]))
                if game.location == "school_first_hall":
                  rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
            if not quest.maxine_lines["leyline_light_2"] == "yellow":
              if game.location == "school_leyline_locator":
                if quest.maxine_lines["leyline_location"] == "school_gym":
                  rv.append(get_quest_guide_marker("school_leyline_locator", "locator_button_1", marker = ["actions interact"], marker_offset=(350,-50)))
                else:
                  rv.append(get_quest_guide_path("school_gym", marker = ["items ley_line_locator@.7"]))
              else:
                rv.append(get_quest_guide_path("school_gym", marker = ["items ley_line_locator@.7"]))
                if game.location == "school_gym":
                  rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
            if not quest.maxine_lines["leyline_light_3"] == "yellow":
              if game.location == "school_leyline_locator":
                if quest.maxine_lines["leyline_location"] == "school_first_hall_west":
                  rv.append(get_quest_guide_marker("school_leyline_locator", "locator_button_1", marker = ["actions interact"], marker_offset=(350,-50)))
                else:
                  rv.append(get_quest_guide_path("school_first_hall_west", marker = ["items ley_line_locator@.7"]))
              else:
                rv.append(get_quest_guide_path("school_first_hall_west", marker = ["items ley_line_locator@.7"]))
                if game.location == "school_first_hall_west":
                  rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
        else:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["items ley_line_locator@.6"], marker_offset = (60,90)))
      elif quest.maxine_lines == "insight":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
      elif quest.maxine_lines == "magnetic":
        if school_computer_room["box_lines_magnet"] and home_kitchen["magnet_package_consumed"] and home_hall["closet_lines_magnet"]:
          if school_first_hall["magnet"] and school_first_hall_west["magnet"] and school_gym["magnet"]:
            if game.location == "school_leyline_locator":
              rv.append(get_quest_guide_marker("school_leyline_locator", "locator_button_2", marker = ["actions interact"], marker_offset=(350,-50)))
            else:
              rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
          else:
            if quest.maxine_lines["lll_charged"]:
              if not school_first_hall["magnet"]:
                if game.location == "school_leyline_locator":
                  if quest.maxine_lines["leyline_location"] == "school_first_hall":
                    rv.append(get_quest_guide_marker("school_leyline_locator", "locator_screen", marker = ["items magnet@.6"]))
                  else:
                    rv.append(get_quest_guide_path("school_first_hall", marker = ["items ley_line_locator@.7"]))
                else:
                  rv.append(get_quest_guide_path("school_first_hall", marker = ["items ley_line_locator@.7"]))
                  if game.location == "school_first_hall":
                    rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
              if not school_first_hall_west["magnet"]:
                if game.location == "school_leyline_locator":
                  if quest.maxine_lines["leyline_location"] == "school_first_hall_west":
                    rv.append(get_quest_guide_marker("school_leyline_locator", "locator_screen", marker = ["items magnet@.6"]))
                  else:
                    rv.append(get_quest_guide_path("school_first_hall_west", marker = ["items ley_line_locator@.7"]))
                else:
                  rv.append(get_quest_guide_path("school_first_hall_west", marker = ["items ley_line_locator@.7"]))
                  if game.location == "school_first_hall_west":
                    rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
              if not school_gym["magnet"]:
                if game.location == "school_leyline_locator":
                  if quest.maxine_lines["leyline_location"] == "school_gym":
                    rv.append(get_quest_guide_marker("school_leyline_locator", "locator_screen", marker = ["items magnet@.6"]))
                  else:
                    rv.append(get_quest_guide_path("school_gym", marker = ["items ley_line_locator@.7"]))
                else:
                  rv.append(get_quest_guide_path("school_gym", marker = ["items ley_line_locator@.7"]))
                  if game.location == "school_gym":
                    rv.append(get_quest_guide_hud("leyline_locator", marker = ["$Use the {color=#48F}Ley Line Locator{/} here."], marker_offset=(30,0)))
            else:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["fuse_box_cropped"]))
              rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_power", marker = ["items ley_line_locator@.6"], marker_offset = (60,90)))
        else:
          if school_computer_room["box_lines_magnet"] and home_bedroom["lines_magnet"] and home_hall["closet_lines_magnet"]:
            if mc["package_due_today"]:
              rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}a day{/} for the\nlast magnet to arrive."))
            else:
              if mc.owned_item("package_magnet"):
                rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
              else:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.65"]))
                rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_magnet", marker = ["actions take"]))
          else:
            if not school_computer_room["box_lines_magnet"]:
              rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room cable_box@.7"]))
              rv.append(get_quest_guide_marker("school_computer_room", "school_computer_room_cable_box", marker = ["actions take"]))
            elif not home_bedroom["lines_magnet"]:
              rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room screen1@.35"]))
              rv.append(get_quest_guide_marker("school_computer_room", "school_computer_room_screen_left", marker = ["actions interact"]))
              rv.append(get_quest_guide_path("home_bedroom", marker = ["$or{space=-25}","home bedroom small_pc@.4"]))
              rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["$or{space=-50}","actions interact"]))
            elif not home_hall["closet_lines_magnet"]:
              rv.append(get_quest_guide_path("home_hall", marker = ["home hall cleaning_supply@.5"]))
              rv.append(get_quest_guide_marker("home_hall", "home_hall_cleaning_supply", marker = ["actions take"]))
      elif quest.maxine_lines == "finalnode":
        if school_ground_floor["final_circle"]:
          if not mc.at("school_secret_locker"):
            rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.5"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_back", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (160,-10)))
          if quest.maxine_lines["right_tool"]:
            rv.append(get_quest_guide_marker("school_secret_locker","school_secret_locker_panel2", marker = ["actions use_item"], marker_sector = "left_bottom", marker_offset = (435,40)))
          else:
            rv.append(get_quest_guide_marker("school_secret_locker","school_secret_locker_panel2", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (435,40)))
        else:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))
      elif quest.maxine_lines == "unscrew":
        rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jacklyn", marker = ["actions quest"]))
      elif quest.maxine_lines == "helpinghand":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["jacklyn contact_icon@.85"]))
      elif quest.maxine_lines == "doll":
        if not mc.at("school_secret_locker"):
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.5"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_back", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (160,-10)))
        rv.append(get_quest_guide_marker("school_secret_locker", "school_secret_locker_eye", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (300,30)))
      elif quest.maxine_lines == "seemaxine":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
      elif quest.maxine_lines == "prison":
        if school_first_hall["magnet"] or school_first_hall_west["magnet"] or school_gym["magnet"]:
          if school_first_hall["magnet"]:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall magnet@.85"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_magnet", marker = ["actions take"]))
          if school_first_hall_west["magnet"]:
            rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall magnet@.85"]))
            rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_magnet", marker = ["actions take"]))
          if school_gym["magnet"]:
            rv.append(get_quest_guide_path("school_gym", marker = ["school first_hall magnet@.85"]))
            rv.append(get_quest_guide_marker("school_gym", "school_gym_magnet", marker = ["actions take"]))
        else:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["school clubroom doll@.65"]))
          rv.append(get_quest_guide_marker("school_clubroom", "school_clubroom_doll", marker = ["items magnet@.6"], marker_sector = "right_top", marker_offset = (60,-20)))
      return rv


label maxine_lines_enter_clubroom:
  "There it is — the prized artifact excavated from the school's imaginary basement."
  "Looks like a cheap 90s toy..."
  return

label maxine_lines_reward_leave_gym:
  "Crap! Lost her. She's a lot faster than she looks..."
  "Hmm... where could she even have disappeared to?"
  $maxine["hidden_passage"] = True
  return

label maxine_lines_finalnode_new_line:
  pause 0.1
  call goto_school_leyline_locator
  jump locator_screen_school_first_hall
  return

label maxine_lines_finalnode_final_circle:
  pause 0.1
  call goto_school_leyline_locator
  jump locator_screen_school_ground_floor
  return

label maxine_lines_doll_locker_arrive:
  pause 0.5
  "What the fuck is that...?"
  $quest.maxine_lines["wtf_is_that"] = True
  return
