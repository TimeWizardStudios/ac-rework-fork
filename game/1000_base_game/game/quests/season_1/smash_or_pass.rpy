init python:
  class Quest_smash_or_pass(Quest):
    title="Smash or Pass"
    class phase_1_smash_or_pass:
      description="Shut that infernal contraption up. No matter the cost!"
      hint="That vile beeping noise? That's nothing. Just ignore it... if you can."
      quest_guide_hint="Click on alarm next to bed and decide it's fate"
    class phase_1000_turned_off:
      description="Tried to smash it once. Was an uncomfortable experience. Sticking to girls now."
    class phase_1001_smashed:
      description="Ka-pow!"

  class Event_quest_smash_or_pass(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.smash_or_pass.in_progress:
        return 0
    def on_can_advance_time(event):
      if quest.smash_or_pass.in_progress:
        return False
    def on_quest_started(event,quest,silent=False):
      ## automatically turn quest guide on for first quest
      if quest.id=="smash_or_pass":
        game.quest_guide="smash_or_pass"
    def on_quest_finished(event,quest,silent=False):
      ## tutorial ends
      if quest.id=="smash_or_pass":
        game.events_queue.append("quest_smash_or_pass_tutorial_done")
    def on_quest_guide_smash_or_pass(event):
      rv=[]
      rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_alarm", marker = "actions interact"))
      rv.append(get_quest_guide_hud("quest_guide",marker="$\nYou can turn\nQuest Guides\non/off anytime",marker_sector="right_top"))
      rv.append(get_quest_guide_hud("game_mode",marker="$\nYou can switch to\nFast or Explore mode\nfor preferred gameplay",marker_sector="left_top"))
      return rv

label quest_smash_or_pass_tutorial_done:
#  $game.notify_modal(None,"Quest Guide","You can enable Quest Guide System(tm) if you stuck",wait=5.0)
  return
