init python:
  class Quest_isabelle_locker(Quest):

    title = "Gathering Storm"

    class phase_1_interrogate:
      description = "The calm before it really hits."
      hint = "Your social skills are atrocious. Do something about it."
      def quest_guide_hint(quest):
        if quest["might_have_an_idea"]:
          return "Quest [maxine] in her office."
        else:
          return "Quest [flora], [jo], [mrsl], [lindsey], [kate], and [maxine], in any order."
    class phase_2_talk:
      hint = "It's always about her, isn't it? The girl that goes better with tea than any biscuit."
      quest_guide_hint = "Quest [isabelle]."
    class phase_3_catnap:
      hint = "It's not stealing... it's just catnapping without permission."
      def quest_guide_hint(quest):
        if quest["get_rid_of_her"]:
          return "Quest [maxine]."
        else:
          return "Go to [maxine]'s office."
    class phase_4_scent:
      hint = "A pussycat following the scent of... well, [isabelle]."
      def quest_guide_hint(quest):
        if quest["spinach"]:
          return "Quest [spinach] until she leaves the school, making sure to avoid bumping into [maxine].\n\nHer current schedule is:\n{font=DejaVuSans.ttf}⚡{/}67-100: Admin Wing\n{font=DejaVuSans.ttf}⚡{/}34-66: Entrance Hall\n{font=DejaVuSans.ttf}⚡{/}0-33: First Hall"
        else:
          x = mc.owned_item(("isabelle_panties_inv","isabelle_glasses_inv")).replace("isabelle_","").replace("_inv","")
          return "Give [isabelle]'s " + x + " to [spinach]."
    class phase_41_repeat:
      hint = "Strap up — it's time to talk to crazy again."
      quest_guide_hint = "Quest [maxine] in her office."
    class phase_5_bedroom:
      hint = "No way you got [isabelle] into your room! Why are you looking at your phone, you weirdo?"
      quest_guide_hint = "Quest [isabelle]."
    class phase_6_email:
      hint = "You've got mail... to send.\nThe electronic kind."
      quest_guide_hint = "Interact with your computer."
    class phase_7_wait:
      hint = "The excruciating wait of sixty minutes. Will you last? Will you crack?"
      quest_guide_hint = "Wait 1 hour."
    class phase_1000_done:
      description = "Now you've done it. {i}Really{/} done it."
    class phase_2000_failed:
      description = "Not your cup of tea. Or kettle of tea. Or anything, really. Not your day."


  class Event_quest_isabelle_locker(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.isabelle_locker in ("bedroom","email"):
        return 0

    def on_can_advance_time(event):
      if quest.isabelle_locker in ("bedroom","email"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_locker == "catnap" and new_location == "school_clubroom" and not quest.isabelle_locker["get_rid_of_her"]:
        game.events_queue.append("quest_isabelle_locker_catnap")
      if quest.isabelle_locker == "scent" and new_location in ("school_ground_floor_west","school_ground_floor","school_first_hall") and mc.location == spinach.location == maxine.location:
        game.events_queue.append("quest_isabelle_locker_scent_busted")
      if quest.isabelle_locker == "scent" and new_location == spinach.location == "school_entrance" and not mc["focus"]:
        game.events_queue.append("quest_isabelle_locker_scent_isabelle")

    def on_advance(event):
      if quest.isabelle_locker == "wait" and not quest.isabelle_locker["got_a_response"]:
        game.events_queue.append("event_show_time_passed_screen")
        game.events_queue.append("quest_isabelle_locker_wait")

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "isabelle_locker":
        mc["focus"] = ""

    def on_quest_guide_isabelle_locker(event):
      rv = []

      if quest.isabelle_locker == "interrogate":
        if not quest.isabelle_locker["might_have_an_idea"]:
          if not quest.isabelle_locker["flora_interrogated"]:
            rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
          if not quest.isabelle_locker["jo_interrogated"] and jo.at("school_*"):
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["jo contact_icon@.85"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "jo", marker = ["actions quest"]))
          if not quest.isabelle_locker["mrsl_interrogated"]:
            rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "mrsl", marker = ["actions quest"]))
          if not quest.isabelle_locker["lindsey_interrogated"]:
            rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
          if not quest.isabelle_locker["kate_interrogated"]:
            rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(mc.location, "kate", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
      elif quest.isabelle_locker == "talk":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
      elif quest.isabelle_locker == "catnap":
        if quest.isabelle_locker["get_rid_of_her"]:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
        else:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["spinach contact_icon@.85"]))
      elif quest.isabelle_locker == "scent":
        if quest.isabelle_locker["spinach"]:
          rv.append(get_quest_guide_char("spinach", marker = ["spinach contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "spinach", marker = ["actions quest"]))
          if not quest.isabelle_locker["spinach"] == "school_entrance":
            if mc.energy >= 67:
              rv.append(get_quest_guide_hud("time", marker="$\n\n\n\nAvoid bumping into {color=#48F}[maxine]{/}!\n\nHer current schedule is:\n{color=#48F}{font=DejaVuSans.ttf}⚡{/}67-100: Admin Wing{/}\n{font=DejaVuSans.ttf}⚡{/}34-66: Entrance Hall\n{font=DejaVuSans.ttf}⚡{/}0-33: First Hall", marker_offset = (270,-20)))
            elif 66 >= mc.energy >= 34:
              rv.append(get_quest_guide_hud("time", marker="$\n\n\n\nAvoid bumping into {color=#48F}[maxine]{/}!\n\nHer current schedule is:\n{font=DejaVuSans.ttf}⚡{/}67-100: Admin Wing\n{color=#48F}{font=DejaVuSans.ttf}⚡{/}34-66: Entrance Hall{/}\n{font=DejaVuSans.ttf}⚡{/}0-33: First Hall", marker_offset = (270,-20)))
            else:
              rv.append(get_quest_guide_hud("time", marker="$\n\n\n\nAvoid bumping into {color=#48F}[maxine]{/}!\n\nHer current schedule is:\n{font=DejaVuSans.ttf}⚡{/}67-100: Admin Wing\n{font=DejaVuSans.ttf}⚡{/}34-66: Entrance Hall\n{color=#48F}{font=DejaVuSans.ttf}⚡{/}0-33: First Hall{/}", marker_offset = (270,-20)))
        else:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["spinach contact_icon@.85"]))
          x = mc.owned_item(("isabelle_panties_inv","isabelle_glasses_inv")).replace("_inv","")
          rv.append(get_quest_guide_marker("school_clubroom", "spinach", marker = ["items "+x+"@.9"]))
      elif quest.isabelle_locker == "repeat":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
      elif quest.isabelle_locker == "bedroom":
        rv.append(get_quest_guide_marker("home_bedroom", "isabelle", marker = ["actions quest"]))
      elif quest.isabelle_locker == "email":
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_computer", marker = ["actions interact"]))
      elif quest.isabelle_locker == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))

      return rv
