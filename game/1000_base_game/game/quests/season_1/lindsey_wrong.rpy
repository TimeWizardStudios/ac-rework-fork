init python:
  class Quest_lindsey_wrong(Quest):

    title = "Nothing Wrong With Me"

    class phase_1_follow:
      description = "Tripping, talking, thinking, falling. Nothing wrong with me but my head."
      hint = "Something's either very wrong with me, or very wrong with [lindsey]. Can't wait to find out."
      quest_guide_hint = "Quest [lindsey] at the school entrance."
    class phase_2_fall:
      hint = "The stairs are the tripper's worst enemy. Better stay vigilant around those locations."
      quest_guide_hint = "Walk up the stairs in the entrance hall."
    class phase_3_nurse:
      description = "Tripping, talking, thinking, falling. Nothing wrong with me but my head."
      hint = "Remedies, tonics, and advice. There's only one Mrs. Right."
      quest_guide_hint = "Quest the [nurse] in her office."
    class phase_4_drinks:
      hint = "Staying hydrated never used to be an issue. Maybe water got less hydrating? Let's ask the top consumer."
      quest_guide_hint = "Quest [lindsey] in the gym."
    class phase_5_fountain:
      hint = "Free refills! But only for water customers! Yeah, it's a privilege."
      quest_guide_hint = "Refill [lindsey]'s bottle on the water fountain."
    class phase_51_mop:
      hint = "Who has the fattest key ring and the fattest belly? Doughnut-man!"
      quest_guide_hint = "Quest the [guard] in his booth."
    class phase_52_doughnuts:
      hint = "Two doughnuts for the man in the booth! Yeah, the one with the bad haircut!"
      quest_guide_hint = "Buy two doughnuts in the cafeteria and give them to the [guard]."
    class phase_53_clean:
      hint = "Sometimes being a janitor is necessary to avoid becoming a janitor. One of life's many paradoxes."
      quest_guide_hint = "Clean all the puddles with the mop."
    class phase_54_lindseyart:
      hint = "The art of hiding. Who knew [lindsey]'s such an expert?"
      quest_guide_hint = "Quest [lindsey] in the art classroom."
    class phase_55_florahelp:
      hint = "There's only one person in the school who would agree to help me with something potentially illegal. Well, two, but it's not [jacklyn]."
      quest_guide_hint = "Quest [flora]."
    class phase_56_floralocker:
      hint = "Little miss impatient won't be happy if I leave her hanging. She has places to be and internships to do."
      quest_guide_hint = "Quest [flora] in the sports wing."
    class phase_57_lindseyclothes:
      hint = "Playing dress-up with the princess — lots of room for artistic freedom!"
      quest_guide_hint = "Quest [lindsey] in the art classroom."
    class phase_6_lindseybottle:
      hint = "One thirst must be quenched, and it's not mine."
      quest_guide_hint = "Go to the gym."
    class phase_7_splash:
      hint = "Hydrated the dehydrated. The only thing that remains now is escaping the sports wing."
      quest_guide_hint = "Go to the 1st floor hall."
    class phase_71_katephoto:
      hint = "If [kate] can light up the room, maybe her photo can make my locker less ugly?"
      quest_guide_hint = "Stash [kate]'s photo in your locker."
    class phase_8_verywet:
      hint = "Willingly wetting yourself. Hot and cold at the same time?"
      quest_guide_hint = "Remove your hoodie from the water fountain."
    class phase_9_guard:
      hint =  "Who has the fattest key ring and the fattest belly? Doughnut-man!"
      quest_guide_hint = "Quest the [guard] in his booth."
    class phase_10_mopforkate:
      hint = "Two doughnuts for the man in the booth! Yeah, the one with the bad haircut!"
      quest_guide_hint = "Buy two doughnuts in the cafeteria and give them to the [guard]."
    class phase_11_cleanforkate:
      hint = "Cleaning for [kate], a wet dream of sorts."
      quest_guide_hint = "Clean all the puddles with the mop, then quest [kate]."
    class phase_1000_done:
      description = "Spilling the source of all life. Been there, done that."
    class phase_2000_failed:
      description = "If something's wrong with [lindsey], it's probably nothing serious..."


  class Event_quest_lindsey_wrong(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.lindsey_wrong in ("mop","doughnuts","clean","lindseyart","florahelp","floralocker","lindseyclothes","verywet","guard","mopforkate","cleanforkate"):
        return 0

    def on_can_advance_time(event):
      if quest.lindsey_wrong in ("mop","doughnuts","clean","lindseyart","florahelp","floralocker","lindseyclothes","verywet","guard","mopforkate","cleanforkate"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.lindsey_wrong == "clean" and new_location == "school_first_hall":
        game.events_queue.append("lindsey_wrong_school_first_hall_mop")
      if quest.lindsey_wrong == "lindseyart" and new_location == "school_art_class":
        game.events_queue.append("lindsey_wrong_school_art_class_enter")
      if quest.lindsey_wrong == "lindseybottle" and new_location == "school_gym":
        game.events_queue.append("lindsey_wrong_school_gym_lindseybottle_enter")
      if quest.lindsey_wrong == "splash" and new_location == "school_first_hall" and not mc["focus"]:
        game.events_queue.append("lindsey_wrong_school_first_hall_splash_enter")
      if quest.lindsey_wrong == "splash" and new_location == "school_ground_floor" and mc.owned_item("kate_wet_photo"):
        game.events_queue.append("lindsey_wrong_school_ground_floor_kate_escape")
      if quest.lindsey_wrong == "katephoto" and new_location == "school_locker" and mc.owned_item("kate_wet_photo"):
        game.events_queue.append("lindsey_wrong_school_ground_floor_kate_escape_locker")
      if quest.lindsey_wrong == "cleanforkate" and new_location == "school_first_hall":
        game.events_queue.append("lindsey_wrong_school_ground_first_hall_kate_clean")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "lindsey_wrong" and quest.lindsey_wrong == "lindseyart" and quest.jacklyn_sweets == "finaltouches":
        school_art_class["exclusive"] = "lindsey"

    def on_quest_finished(event,_quest,silent=False):
      if _quest.id == "lindsey_wrong":
        if quest.jacklyn_sweets == "finaltouches":
          school_art_class["exclusive"] = "jacklyn"
        else:
          school_art_class["exclusive"] = False

    def on_quest_guide_lindsey_wrong(event):
      rv=[]

      if quest.lindsey_wrong == "follow":
        if lindsey["at_none_now"]:
          rv.append(get_quest_guide_hud("time", marker="$Give {color=#48F}[lindsey]{/} time."))
        else:
          rv.append(get_quest_guide_path("school_entrance", marker = ["lindsey contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_entrance", "lindsey", marker = ["actions quest"], marker_offset = (60,30)))
      if quest.lindsey_wrong == "fall":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor main_stairs@.3"]))
        rv.append(get_quest_guide_marker("school_ground_floor","school_ground_floor_stairs", marker = ["actions interact"], marker_sector="mid_bottom", marker_offset = (220,0)))
      if quest.lindsey_wrong == "nurse":
        if quest.kate_search_for_nurse.in_progress:
          game.quest_guide = "kate_search_for_nurse"
        else:
          rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))
      if quest.lindsey_wrong == "drinks":
        rv.append(get_quest_guide_path("school_gym", marker = ["lindsey contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_gym", "lindsey", marker = ["actions quest"], marker_offset = (40,50)))
      if quest.lindsey_wrong == "fountain":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.6"]))
        rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["items lindsey_bottle"], marker_sector = "left_bottom", marker_offset = (-40,30)))
      if quest.lindsey_wrong == "mop":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions quest"], marker_sector = "right_top", marker_offset = (70,170)))
      if quest.lindsey_wrong == "doughnuts":
        if mc.owned_item_count("doughnut") < 1:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
        else:
          if mc.owned_item_count("doughnut") < 2 and not quest.lindsey_wrong["first_doughnut"]:
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["items doughnut"], marker_sector = "right_top", marker_offset = (70,170)))
      if quest.lindsey_wrong == "clean":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water_puddle_6","school first_hall water_puddle_1","school first_hall water_puddle_5"]))
        if not quest.lindsey_wrong["puddle_6_clean"] == True:
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_6", marker = ["items mop@.7,10"], marker_offset=(30,-20), marker_sector = "mid_bottom"))
        elif not quest.lindsey_wrong["puddle_4_clean"] == True:
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_4", marker = ["items mop@.7,10"], marker_offset=(25,-30), marker_sector = "mid_bottom"))
        elif not quest.lindsey_wrong["puddle_5_clean"] == True:
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_5", marker = ["items mop@.7,10"], marker_offset=(40,-25), marker_sector = "mid_bottom"))
        elif not quest.lindsey_wrong["puddle_1_clean"] == True:
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_1", marker = ["items mop@.7,10"], marker_offset=(55,-25), marker_sector = "mid_bottom"))
        elif not quest.lindsey_wrong["puddle_2_clean"] == True:
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_2", marker = ["items mop@.7,10"], marker_offset=(30,-25), marker_sector = "mid_bottom"))
        elif not quest.lindsey_wrong["puddle_3_clean"] == True:
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_3", marker = ["items mop@.7,10"], marker_offset=(30,-25), marker_sector = "mid_bottom"))
      if quest.lindsey_wrong == "lindseyart":
        rv.append(get_quest_guide_path("school_art_class", marker = ["lindsey contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_art_class", "lindsey", marker = ["actions quest"], marker_offset = (60,0)))
      if quest.lindsey_wrong == "florahelp":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "flora", marker = ["actions quest"]))
      if quest.lindsey_wrong == "floralocker":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_first_hall_east", "flora", marker = ["actions quest"]))
      if quest.lindsey_wrong == "lindseyclothes":
        rv.append(get_quest_guide_path("school_art_class", marker = ["lindsey contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_art_class", "lindsey", marker = ["actions quest"], marker_offset = (60,0)))
      if quest.lindsey_wrong == "splash":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall cctv@.8"]))
      if quest.lindsey_wrong == "katephoto":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
        if school_ground_floor["locker_unlocked"]:
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
        else:
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
      if quest.lindsey_wrong == "verywet":
        rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain_hoodie", marker_offset=(-100,0),marker = ["actions interact"], marker_sector = "left_bottom"))
      if quest.lindsey_wrong == "guard":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions quest"], marker_sector = "right_top", marker_offset = (70,170)))
      if quest.lindsey_wrong == "mopforkate":
        if mc.owned_item_count("doughnut") < 1:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
        else:
          if mc.owned_item_count("doughnut") < 2 and not quest.lindsey_wrong["first_doughnut"]:
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pastries@.7"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_pastries", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (40,80)))
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["items doughnut"], marker_sector = "right_top", marker_offset = (70,170)))
      if quest.lindsey_wrong == "cleanforkate":
        if (quest.lindsey_wrong["puddle_1_clean"] and quest.lindsey_wrong["puddle_2_clean"] and quest.lindsey_wrong["puddle_3_clean"] and quest.lindsey_wrong["puddle_4_clean"] and quest.lindsey_wrong["puddle_5_clean"] and quest.lindsey_wrong["puddle_6_clean"]):
          rv.append(get_quest_guide_marker("school_first_hall", "kate", marker = ["actions quest"], marker_offset = (30,30)))
        else:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water_puddle_6","school first_hall water_puddle_1","school first_hall water_puddle_5"]))
          if not quest.lindsey_wrong["puddle_6_clean"] == True:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_6", marker = ["items mop@,-40"], marker_offset=(30,-20), marker_sector = "mid_bottom"))
          elif not quest.lindsey_wrong["puddle_4_clean"] == True:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_4", marker = ["items mop@,-40"], marker_offset=(25,-30), marker_sector = "mid_bottom"))
          elif not quest.lindsey_wrong["puddle_5_clean"] == True:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_5", marker = ["items mop@,-40"], marker_offset=(40,-25), marker_sector = "mid_bottom"))
          elif not quest.lindsey_wrong["puddle_1_clean"] == True:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_1", marker = ["items mop@,-40"], marker_offset=(55,-25), marker_sector = "mid_bottom"))
          elif not quest.lindsey_wrong["puddle_2_clean"] == True:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_2", marker = ["items mop@,-40"], marker_offset=(30,-25), marker_sector = "mid_bottom"))
          elif not quest.lindsey_wrong["puddle_3_clean"] == True:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_puddle_3", marker = ["items mop@,-40"], marker_offset=(30,-25), marker_sector = "mid_bottom"))

      return rv
