image interact_with_maxine_camera = LiveComposite((95+88,95),(0,0),"items maxine_camera",(95,5),Transform("actions interact",zoom=0.85))
image supply_closet_small = Transform(LiveComposite((280,501),(0,0),"school art_class supply_closet",(18,50),"school art_class paintings_supply_closet"),zoom=0.2)


init python:
  class Quest_nurse_venting(Quest):

    title = "Venting Frustration"

    class phase_1_help:
      description = "The light at the end of the tunnel."
      hint = "I don't know any hookers, but I've got the next best thing: a head nurse."
      quest_guide_hint = "Quest the [nurse] in her office."

    class phase_2_supplies:
      hint = "What do truckers, sailors, and conspiracy theorists have in common? Radios."
      def quest_guide_hint(quest):
        if quest["naked"] and mc.owned_item("walkie_talkie") and mc.owned_item("maxine_camera"):
          return "Take the body oil from the supply closet in the art classroom."
        else:
          return "Quest [maxine]."

    class phase_3_ready:
      hint = "Women and holes — classic combo."
      quest_guide_hint = "Quest the [nurse] in her office."

    class phase_4_vents:
      hint = "The easiest way to get detention is loitering outside the principal's office."
      quest_guide_hint = "Leave the admin wing."

    class phase_5_lost:
      hint = "If I got lost on the first floor while looking for [maxine]'s office, what would the furthest room be?"
      quest_guide_hint = "Go to the gym, then interact with [maxine]'s camera in your inventory."

    class phase_6_puzzle:
      hint = "She's heat-drunk. Help her stumble her way to [maxine]'s office."
      def quest_guide_hint(quest):
        return "Go to the " + quest["nurse_location"].replace("school_","").replace("_"," ").replace("class","classroom") + ", then interact with [maxine]'s camera in your inventory."

    class phase_7_bottle:
      hint = "It's like I'm rescuing trapped miners..."
      def quest_guide_hint(_quest):
        if mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")):
          item = mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")).replace("spray_","").replace("_bottle","").replace("_hp","_HP").replace("_"," ")
          return "Use a bottle of " + item + " on the vent in [maxine]'s office."
        elif mc.owned_item(("empty_bottle","spray_empty_bottle")):
          if quest.lindsey_wrong["fountain_state"] == 5:
            return "Go to the cafeteria and fill up a bottle with any liquid."
          else:
            return "Refill an empty bottle on the water fountain in the first hall."
        else:
          if home_kitchen["water_bottle_taken"]:
            return "Buy a water bottle from the vending machine in the first hall."
          else:
            return "Take the water bottle in the home kitchen."

    class phase_8_stuck:
      hint = "The rat escapes the maze."
      quest_guide_hint = "Quest the [nurse] in her office."

    class phase_9_not_stuck:
      hint = "The rat escapes the maze."
      quest_guide_hint = "Quest the [nurse] in her office."

    class phase_1000_done:
      description = "The start of something big. Something with cheeks."


  class Event_quest_nurse_venting(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.nurse_venting > "ready" and quest.nurse_venting.in_progress:
        return 0

    def on_can_advance_time(event):
      if quest.nurse_venting > "ready" and quest.nurse_venting.in_progress:
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if (quest.nurse_venting == "vents" and new_location == "school_ground_floor_west" and not quest.nurse_venting["keep_moving"]
      or quest.nurse_venting == "vents" and new_location == "school_ground_floor" and not quest.nurse_venting["come_find"]):
        game.events_queue.append("quest_nurse_venting_walkie_talkie")
      elif quest.nurse_venting == "lost" and new_location == "school_first_hall_east" and not quest.nurse_venting["play_pretend"]:
        game.events_queue.append("quest_nurse_venting_lost")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "nurse_venting" and quest.nurse_venting == "vents":
        mc["focus"] = "nurse_venting"

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "nurse_venting":
        nurse.equip("nurse_shirt")
        nurse.equip("nurse_bra")
        nurse.equip("nurse_pantys")
        mc["focus"] = ""

    def on_quest_guide_nurse_venting(event):
      rv = []

      if quest.nurse_venting == "help":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        if nurse.at("school_nurse_room"):
          rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))
        else:
          if mc.at("school_nurse_room"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for the {color=#48F}[nurse]{/}\nto come back."))

      elif quest.nurse_venting == "supplies":
        if quest.nurse_venting["naked"] and mc.owned_item("walkie_talkie") and mc.owned_item("maxine_camera"):
          rv.append(get_quest_guide_path("school_art_class", marker = ["supply_closet_small"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_supply_closet",marker = ["actions take"]))
        else:
          if game.location == "school_clubroom":
            rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], marker_offset = (75,25), force_marker = True))
            rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
          else:
            rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.nurse_venting == "ready":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))

      elif quest.nurse_venting == "vents":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      elif quest.nurse_venting == "lost":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))
        if game.location == "school_gym":
          rv.append(get_quest_guide_hud("inventory", marker = ["interact_with_maxine_camera"], marker_sector = "mid_top", marker_offset = (60,0)))

      elif quest.nurse_venting == "puzzle":
        rv.append(get_quest_guide_path(quest.nurse_venting["nurse_location"], marker = ["actions go"]))
        if game.location == quest.nurse_venting["nurse_location"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["interact_with_maxine_camera"], marker_sector = "mid_top", marker_offset = (60,0)))

      elif quest.nurse_venting == "bottle":
        if mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")):
          item = mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water"))
          rv.append(get_quest_guide_path("school_clubroom", marker = ["school clubroom vent@.55"]))
          rv.append(get_quest_guide_marker("school_clubroom", "school_clubroom_vent", marker = ["items bottle " + item], marker_sector = "left_bottom", marker_offset = (200,-20)))
        elif mc.owned_item(("empty_bottle","spray_empty_bottle")):
          if quest.lindsey_wrong["fountain_state"] == 5:
            x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["items bottle " + x + "empty_bottle"], marker_offset = (25,-15)))
          else:
            item = mc.owned_item(("empty_bottle","spray_empty_bottle"))
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.6"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["items bottle " + item], marker_sector = "left_bottom"))
        else:
          if home_kitchen["water_bottle_taken"]:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
          else:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
            rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))

      elif quest.nurse_venting == "stuck":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"]))

      elif quest.nurse_venting == "not_stuck":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))

      return rv
