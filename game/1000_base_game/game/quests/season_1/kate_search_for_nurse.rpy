init python:
  class Quest_kate_search_for_nurse(Quest):
    title = "Search & Rescue"
    class phase_1_start:
      description = "A wild hunt for the lost [nurse]. Muskets. Dogs. Falcons. Everyone's invited."
      hint = "Take your time. Look around."
    class phase_2_open_closet:
      hint = "Sometimes knowing is only half the battle. The remaining fifty or so percent of the battle is about having the right gear."
      quest_guide_hint = "Combine the pin from the [nurse]'s Office with the Yarn from the Homeroom and use on this closet."
    class phase_3_give_wrapper:
      hint = "Tried the dogs and the falcons to no avail. What animal remains?"
    class phase_4_follow_spinach:
      hint = "My grandfather always told me to follow the pussy. He didn't really, but it does seem like good advice."
    class phase_5_find_the_nurse:
      hint = "One that looks nice. And not too expensive."
    class phase_6_meet_kate:
      hint = "Return the lost [nurse] to her owner."
    class phase_7_anal:
      hint = "Return the lost [nurse] to her owner."
    class phase_8_end:
      hint = "Return the lost [nurse] to her owner."
    class phase_1000_done:
      description = "That's as far as it goes. And as deep."
  
  class Event_quest_kate_search_for_nurse(GameEvent):
    def on_inventory_changed(event,char,item,old_count,new_count,silent=False):
      if quest.kate_search_for_nurse.in_progress:
        if item.id == "wrapper": 
          quest.kate_search_for_nurse["wrappers_picked"] +=1
        if item.id == "wrapper" and quest.kate_search_for_nurse["wrappers_picked"] >= 22:
          if not quest.kate_search_for_nurse["kate_found_wrappers"]: 
            game.events_queue.append("wrapper_gotten_all")
    def on_quest_guide_kate_search_for_nurse(event):
      rv=[] 
      if quest.kate_search_for_nurse == "start":
        rv.append(get_quest_guide_path("school_art_class", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_supply_closet", marker = ["nurse contact_icon@.85"], marker_sector = "right_bottom"))
      if quest.kate_search_for_nurse == "open_closet":
        rv.append(get_quest_guide_path("school_art_class", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_art_class","school_art_class_supply_closet", marker_sector = "right_bottom", marker = ["items high_tech_lock"]))
        if not mc.owned_item("high_tech_lockpick"):
          if not mc.owned_item("safety_pin"):
            rv.append(get_quest_guide_path("school_nurse_room", marker = ["items safety_pin"]))
            rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_lollipop_jar", marker = ["actions take"]))
          if not mc.owned_item("ball_of_yarn"):
            rv.append(get_quest_guide_path("school_homeroom", marker = ["items ball_of_yarn"]))
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker_sector = "right_bottom", marker = ["actions take"]))
      if quest.kate_search_for_nurse == "give_wrapper":
        if spinach.at("school_art_class","standing"):
          rv.append(get_quest_guide_path("school_art_class", marker = ["spinach contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_art_class","spinach", marker = ["actions interact"]))
        elif spinach.at("school_first_hall_west","running"):
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["spinach contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_first_hall_west","spinach", marker = ["items candy_wrapper@.8"]))
        elif spinach.at("school_first_hall","running"):
          rv.append(get_quest_guide_path("school_first_hall", marker = ["spinach contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_first_hall","spinach", marker = ["items candy_wrapper@.8"]))
        elif spinach.at("school_ground_floor","running"):
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["spinach contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_ground_floor","spinach", marker = ["items candy_wrapper@.8"]))
        elif spinach.at("school_cafeteria","sitting"):
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["spinach contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_cafeteria","spinach", marker = ["items candy_wrapper@.8"]))
      if quest.kate_search_for_nurse in ("follow_spinach","find_the_nurse"):
        rv.append(get_quest_guide_path("school_entrance", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_entrance", "school_entrance_bush",marker = ["nurse contact_icon@.85"], marker_offset = (100,0), marker_sector = "right_bottom"))
      if quest.kate_search_for_nurse == "meet_kate":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["kate contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "kate", marker = ["actions quest"]))
      return rv
