image wooden_ship_cropped = Crop((1,202,233,195),"school art_class ship")
image combine_empty_can_stick = LiveComposite((273,125),(0,30),"items empty_can",(95,31),Transform("actions combine",zoom=0.85),(178,33),Transform("items stick_1",zoom=0.9))

init python:
  class Quest_maxine_hook(Quest):

    title = "Hooking Up"

    class phase_1_sleep:
      description = "Redoing or just doing?"
      hint = "It's not like you'll figure this out on four hours of sleep..."
      quest_guide_hint = "Go to sleep."
    class phase_2_wake:
      hint = "Not quite a monster under the bed."
      quest_guide_hint = "Quest [maxine] in your bedroom."
    class phase_3_computer:
      hint = "[maxine] has a point... well, she has a sock. Either way, she wants you in the computer classroom."
      quest_guide_hint = "Quest [maxine] in the computer classroom."
    class phase_4_lindsey:
      hint = "Something on your mind? Did she run through your thoughts?"
      quest_guide_hint = "Quest [lindsey]."
    class phase_5_juice:
      hint = "El Dorado, except it's red and sweet. Red Dorado...?"
      def quest_guide_hint(quest):
        if mc.owned_item(("empty_bottle","spray_empty_bottle")):
          return "Go to the cafeteria and fill up a bottle of strawberry juice."
        elif mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")):
          x = mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")).replace("spray_","").replace("_bottle","").replace("_hp","_HP").replace("_"," ")
          return "Consume the bottle of "+x+"."
        else:
          if home_kitchen["water_bottle_taken"]:
            return "Buy a water bottle from the vending machine in the first hall."
          else:
            return "Take the water bottle in the home kitchen."
    class phase_6_glade:
      hint = "Sometimes, even the best need a quiet drink! Not that you're the best, but you do look thirsty."
      quest_guide_hint = "Go to the forest glade and consume the bottle of strawberry juice."
    class phase_61_shovel:
      hint = "Can you dig it? We'll see."
      def quest_guide_hint(quest):
        if mc.owned_item("shovel"):
          return "Quest [maxine] in the forest glade."
        else:
          return "Build a shovel by combining an empty can with a stick."
    class phase_62_dig:
      hint = "You know you want to dig. It's in your blood... you dwarf."
      quest_guide_hint = "Use the shovel on the car tire."
    class phase_63_night:
      hint = "You feel tired. This is not a hypnosis attempt."
      quest_guide_hint = "Go to sleep."
    class phase_64_daybefore:
      hint = "You feel tired. This is not a hypnosis attempt."
      quest_guide_hint = "Go to sleep."
    class phase_7_day:
      hint = "Beep, beep! Yes, I'm your alarm clock now. Get to school!"
      quest_guide_hint = "Go to school."
    class phase_8_interrogation:
      hint = "Single and ready to mingle — at least half of that is true for you."
      quest_guide_hint = "Wait one hour."
    class phase_9_thief:
      hint = "Maybe [maxine] knows more than you? Surely, right?"
      quest_guide_hint = "Quest [maxine] in her office."
    class phase_10_table:
      hint = "Everything in its place, that's the road to cyberspace."
      def quest_guide_hint(quest):
        if not school_hook_up_table["cable"]:
          return "Interact with the cable."
        elif not school_hook_up_table["watermelon_cut"]:
          return "Cut a slice of the watermelon."
        elif not school_hook_up_table["watermelon_slice_interacted"]:
          return "Interact with the watermelon slice."
        elif not school_hook_up_table["watermelon_slice"]:
          return "Interact with the watermelon slice again."
        elif not school_hook_up_table["vr_goggles"]:
          return "Interact with the VR goggles."
        elif not school_hook_up_table["ass_object"] == "power_supply_cable":
          return "Interact with the power supply."
    class phase_1000_done:
      description = "One less wall to lean on."


  class Event_quest_maxine_hook(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.maxine_hook in ("shovel","table"):
        return 0

    def on_can_advance_time(event):
      if quest.maxine_hook in ("shovel","table"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_forest_glade" and quest.maxine_hook == "glade":
        school_forest_glade["exclusive"] = "maxine"
        game.events_queue.append("quest_maxine_hook_glade_upon_entering")
      if new_location == "school_ground_floor" and quest.maxine_hook == "day" and not quest.mrsl_table == "morning":
        school_ground_floor["exclusive"] = "mrsl"
        school_cafeteria["exclusive"] = ("isabelle","jacklyn","jo","kate","maxine")
        game.events_queue.append("quest_maxine_hook_day")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "maxine_hook":
        if quest.maxine_hook == "juice" and mc.owned_item("strawberry_juice"):
          quest.maxine_hook.advance("glade")
        if quest.maxine_hook in ("night","daybefore"):
          if not quest.isabelle_buried.finished:
            quest.isabelle_buried["hole_state"] = 3
          school_forest_glade["exclusive"] = False
        if quest.maxine_hook == "thief":
          school_ground_floor["exclusive"] = False
          school_cafeteria["exclusive"] = False

    def on_advance(event):
      if quest.maxine_hook == "interrogation":
        school_cafeteria["exclusive"] = ("isabelle","jacklyn","kate","maxine")
        game.events_queue.append("event_show_time_passed_screen")
        game.events_queue.append("quest_maxine_hook_interrogation")

    def on_enter_roaming_mode(event):
      if quest.maxine_hook == "table" and school_hook_up_table["cable"] and school_hook_up_table["watermelon_slice"] and school_hook_up_table["watermelon_slice_interacted"] and school_hook_up_table["vr_goggles"] and school_hook_up_table["ass_object"] == "power_supply_cable":
        game.events_queue.append("quest_maxine_hook_table")

    def on_quest_guide_maxine_hook(event):
      rv = []

      if quest.maxine_hook == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      elif quest.maxine_hook == "wake":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_bedroom", "maxine", marker = ["actions quest"]))
      elif quest.maxine_hook == "computer":
        rv.append(get_quest_guide_path("school_computer_room", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_computer_room", "maxine", marker = ["actions quest"]))
      elif quest.maxine_hook == "lindsey":
        rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
      elif quest.maxine_hook == "juice":
        if mc.owned_item(("empty_bottle","spray_empty_bottle")):
          x = mc.owned_item(("empty_bottle","spray_empty_bottle")).replace("empty_bottle","")
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["fill_"+x+"bottle_with_strawberry_juice"], marker_offset = (25,-15)))
        elif mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water")):
          x = mc.owned_item(("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water"))
          rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x,"actions consume@.85"], marker_sector = "mid_top", marker_offset = (60,0)))
        else:
          if home_kitchen["water_bottle_taken"]:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
          else:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.75"]))
            rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))
      elif quest.maxine_hook == "glade":
        if mc.owned_item(("strawberry_juice","spray_strawberry_juice")):
          x = mc.owned_item(("strawberry_juice","spray_strawberry_juice"))
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["actions go"]))
          if mc.at("school_forest_glade"):
            rv.append(get_quest_guide_hud("inventory", marker = ["items bottle " + x, "actions consume@.85"], marker_sector = "mid_top", marker_offset = (60,0)))
      elif quest.maxine_hook == "shovel":
        if mc.owned_item("shovel"):
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "maxine", marker = ["actions quest"]))
        else:
          if mc.owned_item("empty_can") and mc.owned_item("stick"):
            rv.append(get_quest_guide_hud("inventory", marker = ["combine_empty_can_stick"], marker_sector = "mid_top", marker_offset = (60,0)))
          else:
            if not mc.owned_item("stick"):
              if not school_forest_glade["dam_stick_retrieved"]:
               rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade wood@.35"]))
               rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_wood", marker = ["actions take"]))
              elif not school_entrance["bush_interacted"]:
                rv.append(get_quest_guide_path("school_entrance", marker = ["school entrance bush@.3"]))
                rv.append(get_quest_guide_marker("school_entrance", "school_entrance_bush", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (270,0)))
              elif not school_homeroom["stick_interacted"]:
                rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom window_2@.3"]))
                rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_window", marker = ["actions take"], marker_sector = "left_top", marker_offset = (200,200)))
              elif not school_art_class["stick_interacted"]:
                rv.append(get_quest_guide_path("school_art_class", marker = ["wooden_ship_cropped@.4"]))
                if school_art_class["wooden_ship_interacted"]:
                  rv.append(get_quest_guide_marker("school_art_class", "school_art_class_wooden_ship", marker = ["actions take"]))
                else:
                  rv.append(get_quest_guide_marker("school_art_class", "school_art_class_wooden_ship", marker = ["actions interact"]))
              elif not school_gym["stick_interacted"]:
                rv.append(get_quest_guide_path("school_gym", marker = ["school gym bin@1.1"]))
                rv.append(get_quest_guide_marker("school_gym", "school_gym_trash_bin", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (20,-10)))
            if not mc.owned_item("empty_can"):
              if mc.owned_item("soup_can"):
                rv.append(get_quest_guide_hud("inventory", marker = ["items soupcan@.75","actions consume@.85"], marker_sector = "mid_top", marker_offset = (60,0)))
              else:
                if not mc.at("school_locker"):
                  rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
                if school_ground_floor["locker_unlocked"]:
                  rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
                else:
                  rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
                rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["actions take"]))
      elif quest.maxine_hook == "dig":
        rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_tire", marker = ["items shovel@.8"], marker_sector = "left_bottom", marker_offset = (200,0)))
      elif quest.maxine_hook == "night":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      elif quest.maxine_hook == "daybefore":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      elif quest.maxine_hook == "day":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))
      elif quest.maxine_hook == "interrogation":
        rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))
      elif quest.maxine_hook == "thief":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom","maxine", marker = ["actions quest"], marker_offset = (75,25)))
        if mc.at("school_clubroom") and not maxine.at("school_clubroom"):
          rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
      elif quest.maxine_hook == "table":
        if not school_hook_up_table["cable"]:
          rv.append(get_quest_guide_marker("school_hook_up_table","school_hook_up_table_cable", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,-40)))
        elif not school_hook_up_table["watermelon_cut"]:
          rv.append(get_quest_guide_marker("school_hook_up_table","school_hook_up_table_watermelon", marker = ["actions consume"]))
        elif not school_hook_up_table["watermelon_slice_interacted"]:
          rv.append(get_quest_guide_marker("school_hook_up_table","school_hook_up_table_watermelon_slice", marker = ["actions interact"], marker_offset = (-10,-30)))
        elif not school_hook_up_table["watermelon_slice"]:
          rv.append(get_quest_guide_marker("school_hook_up_table","school_hook_up_table_watermelon_slice", marker = ["actions interact"], marker_offset = (30,0)))
        elif not school_hook_up_table["vr_goggles"]:
          rv.append(get_quest_guide_marker("school_hook_up_table","school_hook_up_table_vr_goggles", marker = ["actions interact"], marker_offset = (20,-25)))
        elif not school_hook_up_table["ass_object"] == "power_supply_cable":
          rv.append(get_quest_guide_marker("school_hook_up_table","school_hook_up_table_power_supply", marker = ["actions interact"], marker_offset = (50,-70)))

      return rv
