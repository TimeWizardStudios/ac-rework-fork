init python:
  class Quest_isabelle_piano(Quest):

    title = "Chops and Nocs"

    class phase_10_piano:
      description = "In and out of tune."
      def hint(_quest):
        if quest.piano_tuning.finished:
          return "What has 88 pearly white teeth?"
        else:
          if quest.piano_tuning == "cabinet":
            return "To slay a dragon, one needs a sword. To slay a piano, one needs a hammer and fork."
          elif quest.piano_tuning == "baton":
            return "Is your baton at the top end, at the very bottom, or at just in the middle?"
          elif quest.piano_tuning == "hammer":
            return "Hammer time! Slap that bottom, bitch!"
          elif quest.piano_tuning == "fork":
            return "Fork this shit."
      def quest_guide_hint(_quest):
        if quest.piano_tuning.finished:
          return "Quest the piano, then play something sweet and romantic on it."
        else:
          if quest.piano_tuning == "cabinet":
            return "Interact with the cabinet in the music classroom."
          elif quest.piano_tuning == "baton":
            if school_music_class["got_conductor_baton"]:
              return "Use the conductor's baton on the middle compartment of the cabinet."
            else:
              return "Take the conductor's baton from the teacher's podium in the music classroom."
          elif quest.piano_tuning == "hammer":
            if school_first_hall_west["tuning_hammer"]:
              return "Use the tuning hammer on the bottom compartment of the cabinet."
            else:
              if mc.owned_item("tuning_hammer"):
                return "Use the tuning hammer on the piano in the fine arts wing."
              else:
                if school_music_class["middle_compartment"]:
                  return "Take the tuning hammer from the bottom compartment of the cabinet."
                else:
                  return "Use the conductor's baton on the middle compartment of the cabinet."
          elif quest.piano_tuning == "fork":
            if school_music_class["bottom_compartment"]:
              if school_music_class["middle_compartment"]:
                return "Take the conductor's baton back from the middle compartment of the cabinet."
              else:
                if school_music_class["top_compartment"] == "tuning_fork":
                  return "Use the conductor's baton on the top compartment of the cabinet."
                elif school_music_class["top_compartment"] == "conductor_baton":
                  if school_first_hall_west["tuning_fork"]:
                    return "Use the tuning fork on the top compartment of the cabinet."
                  else:
                    return "Use the tuning fork on the piano in the fine arts wing."
            else:
              return "Use the tuning hammer on the bottom compartment of the cabinet."
    class phase_20_computer:
      description = "An electronic symphony of sound and partially dried up ink."
      hint = "Play the keyboard to play the keyboard, get it?"
      quest_guide_hint = "Interact with the leftmost computer in the computer room."
    class phase_30_printer:
      hint = "All good music comes from the blues. That doesn't get plugged enough."
      def quest_guide_hint(quest):
        if quest["missing_cable"]:
          if mc.owned_item("power_cable_blue"):
            return "Use the blue cable on the printer in the computer room."
          else:
            return "Take the blue cable from the box of cables in the computer room."
        else:
          return "Interact with the printer in the computer room."
    class phase_40_isabelle:
      hint = "Speak to your audience."
      quest_guide_hint = "Quest [isabelle]."
    class phase_50_concert:
      hint = "Meet with the lady of fine arts."
      quest_guide_hint = "Quest [isabelle] in the fine arts wing."
    class phase_60_score:
      hint = "Let's play the weakest clink."
      quest_guide_hint = "Use the Chopin music score on the piano, or simply interact with the piano."
    class phase_1000_done:
      description = "Chops, Nocs, and girls without\nsocks — everything needed to orchestrate a good time."


  class Event_quest_isabelle_piano(GameEvent):
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.isabelle_piano in ("concert","score") and game.location == "school_first_hall_west":
        return 0

    def on_can_advance_time(event):
      if quest.isabelle_piano in ("concert","score") and game.location == "school_first_hall_west":
        return False

    def on_quest_started(event,quest,silent=False):
      if quest.id == "isabelle_piano" and quest == "computer":
        school_computer_room["unlocked"] = True

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "isabelle_piano" and quest.isabelle_piano == "computer":
        school_computer_room["unlocked"] = True

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_piano == "concert" and new_location == "school_first_hall_west" and not mc["focus"]:
        mc["focus"] = "isabelle_piano"

    def on_quest_finished(event,quest,silent=False):
      if quest.id == "isabelle_piano":
        mc["focus"] = ""

    def on_quest_guide_isabelle_piano(event):
      rv = []

      if quest.isabelle_piano == "piano":
        if quest.piano_tuning.finished:
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\nsweet and romantic{space=-50}","actions quest","${space=275}"], marker_offset = (150,250)))
        else:
          if quest.piano_tuning == "cabinet":
            rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
            rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
          elif quest.piano_tuning == "baton":
            if school_music_class["got_conductor_baton"]:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            else:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
          elif quest.piano_tuning == "hammer":
            if school_first_hall_west["tuning_hammer"]:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
            else:
              if mc.owned_item("tuning_hammer"):
                rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
              else:
                if school_music_class["middle_compartment"]:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                else:
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
          elif quest.piano_tuning == "fork":
            if school_music_class["bottom_compartment"]:
              if school_music_class["middle_compartment"]:
                rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                if school_music_class["top_compartment"] == "tuning_fork":
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                elif school_music_class["top_compartment"] == "conductor_baton":
                  if school_first_hall_west["tuning_fork"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                    rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
            else:
              rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
              rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
      elif quest.isabelle_piano == "computer":
        rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room screen1@.35"]))
        rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_screen_left", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (30,150)))
      elif quest.isabelle_piano == "printer":
        if quest.isabelle_piano["missing_cable"]:
          if mc.owned_item("power_cable_blue") or mc.owned_item("power_cable_red") and not quest.isabelle_piano["red_cable_refused"] or mc.owned_item("power_cable_yellow") and not quest.isabelle_piano["yellow_cable_refused"]:
            rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room printer@.5"]))
            if mc.owned_item("power_cable_blue"):
              rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_printer", marker = ["items power_cable_blue@.8"], marker_sector = "left_bottom", marker_offset = (170,30)))
            elif mc.owned_item("power_cable_red") and not quest.isabelle_piano["red_cable_refused"]:
              rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_printer", marker = ["items power_cable_red@.8"], marker_sector = "left_bottom", marker_offset = (170,30)))
            elif mc.owned_item("power_cable_yellow") and not quest.isabelle_piano["yellow_cable_refused"]:
              rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_printer", marker = ["items power_cable_yellow@.8"], marker_sector = "left_bottom", marker_offset = (170,30)))
          else:
            rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room cable_box@.7"]))
            rv.append(get_quest_guide_marker("school_computer_room", "school_computer_room_cable_box", marker = ["actions take"], marker_sector = "right_bottom"))
        else:
          rv.append(get_quest_guide_path("school_computer_room", marker = ["school computer_room printer@.5"]))
          rv.append(get_quest_guide_marker("school_computer_room","school_computer_room_printer", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (170,30)))
      elif quest.isabelle_piano == "isabelle":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "isabelle", marker = ["actions quest"]))
      elif quest.isabelle_piano == "concert":
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["isabelle contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_first_hall_west", "isabelle", marker = ["actions quest"], marker_offset = (40,0)))
      elif quest.isabelle_piano == "score":
        rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items chopin_music_score@.9"], marker_offset = (150,250)))

      return rv
