init python:
  class Quest_mrsl_HOT(Quest):
    title = "The Missing Tapes"
    class phase_1_start:
      description = "The tip of the iceberg is just as cold. Yes, the trail as well."
      hint = "A few dollars for her thoughts."
      quest_guide_hint = "Quest [mrsl]."
    class phase_2_eavesdrop:
      hint = "A forbidden place. Whispers in the dark."
      def quest_guide_hint(quest):
        if school_first_hall_east["mrsl_quest_HOT_flora_entered_school_first_hall_east"]:
          return "Eavesdrop the locker room."
        else:
          return "Leave the gym."
    class phase_3_lindsey_talk:
      hint = "[lindsey] might have a clue... after all, she is the Newfall gym bunny."
      quest_guide_hint =  "Quest [lindsey]."
    class phase_4_jo_talk:
      hint = "[jo]. Yeah. Good idea."
      quest_guide_hint =  "Quest [jo]."
    class phase_5_text_flora:
      hint = "Pushing chores on [flora] through texts — my favorite activity."
      quest_guide_hint =  "Text [flora]."
    class phase_6_laundry:
      hint = "Manual labor should be outlawed, but then no one would have clean clothes. Laundry, ohoy!"
      def quest_guide_hint(quest):
        if not mc.owned_item("tide_pods"):
          return "Pick up laundry detergents in your lunch box."
        else:
          if not home_bathroom["laundry_instructions_read"]:
            return "Read the laundry instructions."
          else:
            if not mc.owned_item("clean_laundry"):
              if not home_bathroom["sink_water"]:
                return "Fill the sink with hot water."
              elif not home_bathroom["sink_detergent"]:
                return "Add laundry detergent."
              elif not home_bathroom["dirty_clothes_taken"]:
                return "Pick up the dirty clothes."
              elif not home_bathroom["washing"]:
                return "Wash the dirty clothes."
              else:
                return "Pick up the wet clothes."
            else:
              return "Hang the laundry to dry."
    class phase_7_jo_talk_laundry_done:
      hint = "Laundry done! What happens next, you won't believe."
      quest_guide_hint =  "Quest [jo]."
    class phase_8_research:
      hint = "Some surf and turf. Especially some surf."
      quest_guide_hint = "Interact with the computer in your bedroom."
    class phase_9_reply:
      hint = "Wait for better times. And for a reply. Those are good too."
      quest_guide_hint = "Go to sleep, then interact with the computer in your bedroom."
    class phase_10_delivery:
      hint = "Scammed? New mail? One can only hope."
      quest_guide_hint = "Go to sleep, then quest [jo]."
    #class phase_11_delivery_arrive:
    #  hint = "(Coming Soon) Strange videotape. Missing VHS player. The mystery deepens."
    #class phase_14_end_day_gym:
    #  hint = "(Coming Soon) Strange videotape. Missing VHS player. The mystery deepens."
#################
    class phase_12_vhs:
      hint = "There's only one collector of old electronics! Okay, maybe there's many, but..."
      quest_guide_hint = "Interact with the old locker in [maxine]'s office."
    class phase_13_hookitup:
      hint = "Carrying around a VHS player on my shoulder makes me look like a boombox guy without the boom. Best find a place to put it."
      quest_guide_hint = "Use the VHS player on the computer in your bedroom."
    class phase_14_items:
      hint = "No one knows the basement like the resident rats, but apart from them it's [jo]."
      quest_guide_hint = "Quest [jo] in the kitchen at home."
    class phase_15_attic:
      hint = "Some people call it the howling darkness above. Others call it the attic. Not sure which is more creepy."
      quest_guide_hint = "Interact with the hole in the home hall."
    class phase_16_attic2:
      hint = "As they in the old wild west: catching cards requires a strong lasso."
      def quest_guide_hint(quest):
        if mc.owned_item("capture_card"):
          return "Use the capture card on the computer in your bedroom."
        elif mc.owned_item("xcube_controller"):
          if not home_hall['table_taken']:
            return "Move the table in the home hall."
          else:
            return "Use the xCube Controller on the hole and fish until you find the capture card."
        else:
          return "Pick up the xCube controller in your bedroom."
    class phase_18_insert:
      hint = "Insertions. Very underrated when it comes to... movies."
      quest_guide_hint = "Use the HOT 6 tape on the VHS player in your bedroom."
    class phase_19_watch:
      hint = "My favorite activity. Enough said."
      quest_guide_hint = "Interact with the computer in your bedroom."
    class phase_1000_done:
      description = "The first lesson is always free. And always means forever."
      #description = "(Coming Soon) Strange videotape. Missing VHS player. The mystery deepens."
    class phase_2000_failed:
      description = "Like Atlantis into the sea. Oh, tragic day."


  class Event_quest_mrsl_HOT(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.mrsl_HOT=="laundry" and new_location=="home_bathroom" and not home_bathroom["mrsl_quest_HOT_flora_entered_bathroom"]:
        game.events_queue.append("mrsl_quest_HOT_flora_entered_bathroom")
      if quest.mrsl_HOT=="eavesdrop" and new_location=="school_first_hall_east" and not school_first_hall_east["mrsl_quest_HOT_flora_entered_school_first_hall_east"]:
        game.events_queue.append("mrsl_quest_HOT_flora_entered_school_first_hall_east")
      if old_location == "home_hall" and home_hall['table_taken'] and not quest.maya_quixote == "attic":
        game.events_queue.append("mrsl_quest_HOT_mc_left_table")
    def on_quest_guide_mrsl_HOT(event):
      rv=[]
      if quest.mrsl_HOT == "start":
        rv.append(get_quest_guide_path("school_gym", marker = ["mrsl contact_icon@.85"]))
        if mrsl.at("school_gym", "sitting"):
          rv.append(get_quest_guide_marker("school_gym","mrsl", marker = ["actions quest"]))
        else:
          if mc.at("school_gym"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for [mrsl] between\n{color=#48F}1:00 PM — 3:00 PM{/}."))
      if quest.mrsl_HOT == "eavesdrop":
        rv.append(get_quest_guide_path("school_first_hall_east", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_door_locker", marker = ["actions interact"], marker_offset = (50,100)))
      if quest.mrsl_HOT == "lindsey_talk":
        rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
      if quest.mrsl_HOT == "jo_talk":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jo", marker = ["actions quest"]))
      if quest.mrsl_HOT == "text_flora":
        rv.append(get_quest_guide_hud("phone",marker=["flora contact","phone apps contact_info text@0.5","$Text {color=#48F}[flora]{/}."]))
      if quest.mrsl_HOT == "laundry":
        if not mc.owned_item("tide_pods"):
          if school_locker["gotten_lunch_today"]:
            rv.append(get_quest_guide_hud("time", marker="$Check your lunch box again {color=#48F}tomorrow{/}."))
          else:
            if not mc.at("school_locker"):
              rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
            if school_ground_floor["locker_unlocked"]:
              rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
            else:
              rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
            rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["actions interact"]))
        else:
          if not home_bathroom["laundry_instructions_read"]:
            rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom clothes_sink@.6"]))
            rv.append(get_quest_guide_marker("home_bathroom", "home_laundry_instructions", marker = ["actions interact"], marker_offset = (-30,30)))
          else:
            if not mc.owned_item("clean_laundry"):
              if not home_bathroom["sink_water"]:
                rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
                rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sink", marker = ["actions interact"], marker_offset = (180,90)))
              elif not home_bathroom["sink_detergent"]:
                rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
                rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sink", marker = ["items tide_pods"], marker_offset = (180,90)))
              elif not home_bathroom["dirty_clothes_taken"]:
                rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
                rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_basket_clothes", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (220,0)))
              elif not home_bathroom["washing"]:
                rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
                rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sink", marker = ["items dirty_laundry"], marker_offset = (180,90)))
              else:
                rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom sink@.2"]))
                rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_clothes_sink", marker = ["actions take"], marker_offset = (50,-25)))
            else:
              rv.append(get_quest_guide_path("home_bathroom", marker = ["home bathroom no_clothes@.35"]))
              rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_clothesline_empty", marker = ["items nurse_panties"], marker_sector = "right_top", marker_offset = (200,-20)))
      if quest.mrsl_HOT == "jo_talk_laundry_done":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(mc.location, "jo", marker = ["actions quest"]))
      if quest.mrsl_HOT == "research":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))
      if quest.mrsl_HOT == "reply":
        if quest.mrsl_HOT["current_day"] == game.day:
          s = game.dow_names[game.dow]
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}tomorrow{/}."))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))
      if quest.mrsl_HOT == "delivery":
        if quest.mrsl_HOT["current_day"] == game.day:
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}tomorrow{/}."))
        else:
          rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(mc.location, "jo", marker = ["actions quest"]))
      if quest.mrsl_HOT == "vhs":
        if not quest.clubroom_access.started:
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.5"]))
          rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["actions quest"], marker_sector = "right_top", marker_offset = (0,-220)))
        elif quest.clubroom_access.in_progress:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Oxygen For Oxymoron{/}."))
        else:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["school clubroom closet_small@.4"]))
          rv.append(get_quest_guide_marker("school_clubroom","school_clubroom_old_locker", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (220,180)))
      if quest.mrsl_HOT == "hookitup":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["items vhs_player@.8"]))
      if quest.mrsl_HOT == "items":
        if jo.at("home_kitchen"):
          rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
          rv.append(get_quest_guide_marker("home_kitchen", "jo", marker = ["actions quest"]))
        else:
          if game.hour<18 and game.hour>8:
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} to come\nback home at {color=#48F}6:00 PM{/}."))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}tomorrow{/}."))
      if quest.mrsl_HOT == "attic":
        rv.append(get_quest_guide_path("home_hall", marker = ["home hall hole"]))
        rv.append(get_quest_guide_marker("home_hall", "home_hall_hole", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (60,-260)))
      if quest.mrsl_HOT == "attic2":
        if mc.owned_item("capture_card"):
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["items capture_card@.75"]))
        elif mc.owned_item("xcube_controller"):
          if home_hall["hole_today"]:
            rv.append(get_quest_guide_hud("time", marker="$Try again {color=#48F}tomorrow{/}."))
          else:
            if not home_hall['table_taken']:
              rv.append(get_quest_guide_path("home_hall", marker = ["home hall table@.4"]))
              rv.append(get_quest_guide_marker("home_hall", "home_hall_table", marker = ["actions interact"], marker_offset = (60,100)))
            else:
              rv.append(get_quest_guide_path("home_hall", marker = ["home hall hole"]))
              rv.append(get_quest_guide_marker("home_hall", "home_hall_hole", marker = ["items xcube_controller@.8"], marker_sector = "right_top", marker_offset = (60,-260)))
        else:
          if home_bedroom["clean"]:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom controller_clean"], marker_sector = "right_top", marker_offset = (80,220)))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_controller", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (80,-20)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom controller@.8"], marker_sector = "right_top", marker_offset = (80,220)))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_controller", marker = ["actions take"], marker_offset = (100,110)))
      if quest.mrsl_HOT == "insert":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["items vcr_tape@.8"], marker_offset = (260,90)))
      if quest.mrsl_HOT == "watch":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))
      return rv


label mrsl_quest_HOT_mc_left_table:
  $home_hall['table_taken'] = False
  $home_hall["note"] = True
  return
