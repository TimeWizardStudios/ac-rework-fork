image trophy_case = LiveComposite((464,782),(0,0),"school first_hall_east trophycase",(219,75),"school first_hall_east trophies",(221,39),"school first_hall_east glass")
image teacher_desk = LiveComposite((416,212),(129,0),"school english_class teacher_chair",(0,3),"school english_class teacher_desk")
image door_to_the_roof = LiveComposite((998,1050),(0,0),"school roof_landing door",(394,286),"school roof_landing calendar")


init python:
  class Quest_isabelle_dethroning(Quest):

    title = "Dethroning the Queen"

    class phase_1_meeting:
      description = "Throw dice to the wind.\nThe course is up."
      hint = "Meeting with [maxine] at now o'clock."
      quest_guide_hint = "Quest [maxine] in her office."

    class phase_2_riddle:
      hint = "As high as we can possibly go."
      quest_guide_hint = "Go to the roof."

    class phase_3_sleep:
      hint = "Tomorrow is a big day — that means one thing."
      quest_guide_hint = "Go to sleep."

    class phase_4_newspaper:
      hint = "Newspaper. I rest my case."
      def quest_guide_hint(quest):
        if mc.owned_item("newspaper"):
          return "Interact with the trophy case in the sports wing."
        else:
          return "Interact with the newspaper stand in the first hall."

    class phase_5_key:
      hint = "Not show. Not low. Not snow. But her name does sound similar.{space=-10}"
      quest_guide_hint = "Quest [jo] in the school."

    class phase_6_trophy_case:
      hint = "Patience is a virtue, and you're about to be very virtuous. And\nwhen you're done being a saintly{space=-10}\ndick, put the newspaper in the trophy case."
      def quest_guide_hint(quest):
        if jo["at_none_now"]:
          return "Wait 1 hour."
        else:
          return "Use the newspaper on the trophy case in the sports wing."

    class phase_7_independence_day:
      hint = "Ah, freedom! Ironic that such a concept is celebrated in this place."
      def quest_guide_hint(quest):
        if game.day >= 21:
          return "Interact with the Independence Day banner in the entrance hall."
        else:
          independence_day = (21 - game.day)
          return "Wait until Independence Day\n("+str(independence_day)+(" days left)." if independence_day > 1 else " day left).")

    class phase_8_announcement:
      hint = "The place of bean soup and circular confectionery."
      quest_guide_hint = "Go to the cafeteria."

    class phase_9_principal:
      hint = "Talk to everyone but [flora], [maxine], [lindsey], [kate], [isabelle], [mrsl], [jacklyn], and the [nurse]."
      quest_guide_hint = "Quest [jo]."

    class phase_10_poem:
      hint = "The place of penitence, where criminals atone through repeated{space=-20}\nscribing."
      quest_guide_hint = "Interact with the blackboard in the English classroom."

    class phase_11_poem_done:
      hint = "Jesus, there's a lot of talking to [jo], isn't there? Do it again."
      quest_guide_hint = "Quest [jo]."

    class phase_12_good_news:
      hint = "Pass on the good news to the girl with the glasses! Can you guess which one?"
      quest_guide_hint = "Quest [isabelle]."

    class phase_13_invitations:
      hint = "Inviting people — your specialty{space=-10}\nbesides jerking off."
      def quest_guide_hint(quest):
        rv = "Quest, " + ("[flora], " if "flora" not in quest["invitations"] else "") + ("[kate], " if "kate" not in quest["invitations"] else "") + ("[lindsey], " if "lindsey" not in quest["invitations"] else "") + ("[maxine], " if "maxine" not in quest["invitations"] else "")
        rv = rv.split(", ")
        if len(quest["invitations"]) < 3:
          rv[-2] = "and " + rv[-2]
        rv = ", ".join(rv[:-1])
        if len(quest["invitations"]) == 2:
          rv = rv.replace(",","")
        else:
          rv = rv.replace(",","",1)
        rv = rv + "."
        return rv

    class phase_14_invitations_done:
      hint = "Time to jerk... I mean, talk to [isabelle]."
      quest_guide_hint = "Quest [isabelle]."

    class phase_15_dinner:
      hint = "Single and ready to mingle? Alone and ready to bone?\nSinner and ready for dinner? Stuck and ready to fuck?\nJust picture it!"
      def quest_guide_hint(quest):
        if game.hour < 19:
          return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."
        elif quest["dinner_picture"]:
          return "Quest [maxine]."
        elif "maxine" in quest["dinner_conversations"]:
          return "Take a picture for [maxine]."
        else:
          rv = "Quest, " + ("[flora], " if "flora" not in quest["dinner_conversations"] else "") + ("[isabelle], " if "isabelle" not in quest["dinner_conversations"] else "") + ("[kate], " if "kate" not in quest["dinner_conversations"] else "") + ("[lindsey], " if "lindsey" not in quest["dinner_conversations"] else "") + ("the [nurse], " if "nurse" not in quest["dinner_conversations"] else "")
          rv = rv.split(", ")
          if len(quest["dinner_conversations"]) < 4:
             rv[-2] = "and " + rv[-2]
          rv = ", ".join(rv[:-1])
          if len(quest["dinner_conversations"]) == 3:
            rv = rv.replace(",","")
          else:
            rv = rv.replace(",","",1)
          rv = rv + ")"
          return "Quest [maxine]." if len(quest["dinner_conversations"]) == 5 else "Quest [maxine]. (Optional: " + rv

    class phase_16_trap:
      hint = "Use all the secret techniques you've learned, the items you've{space=-5}\ngathered, and the hatred of ten thousand years."
      def quest_guide_hint(quest):
        if game.location == "school_cafeteria":
          return "Leave the cafeteria."
        elif game.location == "school_first_hall":
          if school_first_hall["fishing_hook"]:
            return "Use the fishing line on the red paint."
          elif school_first_hall["red_paint"]:
            return "Use the fishing hook on the red paint."
          else:
            return "Use the red paint on the vending{space=-10}\nmachine."
        else:
          return "Go to the first hall."

    class phase_17_revenge:
      hint = "I mean, it's pretty obvious, isn't it? Follow the red trail through every barrier and obstacle along your way. That is how you find what you're looking for."
      def quest_guide_hint(quest):
        if game.location == "school_art_class":
          return "Interact with the supply closet."
        elif quest["pea_brains"]:
          if mc.owned_item(("high_tech_lockpick","safety_pin")):
            item = mc.owned_item(("high_tech_lockpick","safety_pin"))
            return "Use the " + items_by_id[item].title().lower() + " on the door to the art classroom."
          else:
            return "Take the safety pin from the teacher's desk in the English classroom."
        elif quest["be_right_back"] and not quest["chance_for_revenge"]:
          return "Go to the arts wing."
        else:
          return "Go to the art classroom."

    class phase_18_revenge_done:
      hint = "Run to the hills! And if you don't have hills, take the next best option of higher altitude."
      quest_guide_hint = "Go to the roof."

    class phase_19_panik:
      def hint(quest):
        return "[lindsey] ran to the roof, are you going to save her or not? Are you going to be the hero or the zero? " + ("The low or the person who{space=-15}\ncalls [jo]?" if quest["call_jo"] else "The absentee or the man with the key?")
      def quest_guide_hint(quest):
        if quest["gravity_of_the_situation"]:
          return "Leave the school."
        elif quest["call_jo"]:
          return "Call [jo] on your phone."
        else:
          return "Interact with the [guard]'s booth in the entrance hall."

    class phase_20_fly_to_the_moon:
      hint = "When all else fails, bring out your inner Doom Guy. Hack and slash, rip and tear."
      def quest_guide_hint(quest):
        if mc.owned_item("fire_axe"):
          return "Use the fire axe on the door to the roof."
        else:
          return "Take the fire axe in the roof landing."

    class phase_1000_done:
      description = "Seasons don't fear the reaper,\nthey just change."


init python:
  class Event_quest_isabelle_dethroning(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if (quest.isabelle_dethroning in ("riddle","trap","revenge","revenge_done","panik","fly_to_the_moon")
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)):
        return 0

    def on_can_advance_time(event):
      if (quest.isabelle_dethroning in ("riddle","trap","revenge","revenge_done","panik","fly_to_the_moon")
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)):
        return False

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "isabelle_dethroning":
        if quest.isabelle_dethroning == "riddle":
          school_homeroom["exclusive"] = "True"
        if quest.isabelle_dethroning == "independence_day":
          school_ground_floor["independence_day_banner"] = True
        if quest.isabelle_dethroning == "invitations":
          quest.isabelle_dethroning["invitations"] = set()
        if quest.isabelle_dethroning == "dinner":
          quest.isabelle_dethroning["dinner_conversations"] = set()
        if quest.isabelle_dethroning == "panik":
          school_first_hall_west["scorch_marks"] = True

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_dethroning == "riddle" and new_location == "school_homeroom" and not quest.isabelle_dethroning["school_regulations"]:
        game.events_queue.append("quest_isabelle_dethroning_climb_down")
      if quest.isabelle_dethroning == "riddle" and old_location == "school_homeroom":
        school_homeroom["exclusive"] = False
      if quest.isabelle_dethroning == "riddle" and new_location == "school_roof":
        game.events_queue.append("quest_isabelle_dethroning_riddle")
      if quest.isabelle_dethroning == "announcement" and new_location == "school_cafeteria":
        game.events_queue.append("quest_isabelle_dethroning_announcement")
      if quest.isabelle_dethroning == "poem" and new_location == "school_english_class" and not quest.isabelle_dethroning["weekly_assignment"]:
        game.events_queue.append("quest_isabelle_dethroning_poem")
      if quest.isabelle_dethroning == "trap" and new_location == "school_ground_floor" and not quest.isabelle_dethroning["better_hurry"]:
        game.events_queue.append("quest_isabelle_dethroning_trap")
      if quest.isabelle_dethroning == "trap" and new_location == "school_first_hall" and not quest.isabelle_dethroning["lets_do_this"]:
        game.events_queue.append("quest_isabelle_dethroning_trap_first_hall")
      if quest.isabelle_dethroning == "revenge" and new_location == "school_first_hall_west" and not quest.isabelle_dethroning["chance_for_revenge"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge")
      if quest.isabelle_dethroning == "revenge" and new_location == "school_art_class":
        game.events_queue.append("quest_isabelle_dethroning_revenge_art_class")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_cafeteria":
        game.events_queue.append("quest_isabelle_dethroning_revenge_done")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_ground_floor" and not quest.isabelle_dethroning["where_do_i_start"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge_done_entrance_hall")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_ground_floor_west" and not quest.isabelle_dethroning["only_maya"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge_done_admin_wing")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_first_hall" and not quest.isabelle_dethroning["no_lindsey_here"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge_done_first_hall")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_first_hall_west" and not quest.isabelle_dethroning["where_are_you"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge_done_arts_wing")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_first_hall_east" and not quest.isabelle_dethroning["lindsey_domain"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge_done_sports_wing")
      if quest.isabelle_dethroning == "revenge_done" and new_location == "school_roof_landing" and not quest.isabelle_dethroning["what_the_hell"]:
        game.events_queue.append("quest_isabelle_dethroning_revenge_done_roof_landing")

    def on_advance(event):
      if quest.isabelle_dethroning == "sleep" and game.hour == 7:
        quest.isabelle_dethroning.advance("newspaper")
      elif quest.isabelle_dethroning == "dinner" and game.hour == 19 and not mc["focus"]:
        game.events_queue.append("quest_isabelle_dethroning_dinner")

    def on_enter_roaming_mode(event):
      if quest.isabelle_dethroning == "invitations" and "flora" in quest.isabelle_dethroning["invitations"] and "kate" in quest.isabelle_dethroning["invitations"] and "lindsey" in quest.isabelle_dethroning["invitations"] and "maxine" in quest.isabelle_dethroning["invitations"]:
        game.events_queue.append("quest_isabelle_dethroning_invitations")
      if quest.isabelle_dethroning == "dinner" and "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not mc.owned_item("maxine_camera"):
        game.events_queue.append("quest_isabelle_dethroning_dinner_kate_departure")

    def on_quest_finished(event,_quest,silent=False):
      if _quest.id == "isabelle_dethroning" and quest.isabelle_dethroning == "done":
        isabelle.outfit = isabelle["outfit_stamp"]

    def on_quest_guide_isabelle_dethroning(event):
      rv = []

      if quest.isabelle_dethroning == "meeting":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))

      elif quest.isabelle_dethroning == "riddle":
        rv.append(get_quest_guide_path("school_roof", marker = ["actions go"]))

      elif quest.isabelle_dethroning == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (75,25)))

      elif quest.isabelle_dethroning == "newspaper":
        if mc.owned_item("newspaper"):
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["trophy_case@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_trophy_case", marker_sector = "left_bottom", marker = ["actions interact"], marker_offset = (380,-85)))
        else:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall newspaper@.25"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_newspaper", marker = ["actions interact"], marker_offset = (65,15)))

      elif quest.isabelle_dethroning == "key":
        if jo.at("school_*"):
          rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))
        else:
          rv.append(get_quest_guide_hud("time", marker="$\nWait for {color=#48F}[jo]{/} to\n" + ("come" if mc.at("school_*") else "go") + " to school."))

      elif quest.isabelle_dethroning == "trophy_case":
        if jo["at_none_now"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))
        else:
          rv.append(get_quest_guide_path("school_first_hall_east", marker = ["trophy_case@.125"]))
          rv.append(get_quest_guide_marker("school_first_hall_east", "school_first_hall_east_trophy_case", marker_sector = "left_bottom", marker = ["items newspaper@.9"], marker_offset = (380,-85)))

      elif quest.isabelle_dethroning == "independence_day":
        if game.day >= 21:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor independence_day_banner@.4"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_independence_day_decorations", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (630,-100)))
        else:
          independence_day = (21 - game.day)
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}Independence Day{/}\n({color=#48F}"+str(independence_day)+("{/} days left)." if independence_day > 1 else "{/} day left).")))

      elif quest.isabelle_dethroning == "announcement":
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["actions go"]))

      elif quest.isabelle_dethroning == "principal":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      elif quest.isabelle_dethroning == "poem":
        rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class blackboard@.25"]))
        rv.append(get_quest_guide_marker("school_english_class", "school_english_class_blackboard", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (420,-80)))

      elif quest.isabelle_dethroning == "poem_done":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      elif quest.isabelle_dethroning == "good_news":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_dethroning == "invitations":
        if "flora" not in quest.isabelle_dethroning["invitations"]:
          rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))
        if "kate" not in quest.isabelle_dethroning["invitations"]:
          rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "kate", marker = ["actions quest"]))
        if "lindsey" not in quest.isabelle_dethroning["invitations"]:
          rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "lindsey", marker = ["actions quest"]))
        if "maxine" not in quest.isabelle_dethroning["invitations"]:
          rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.isabelle_dethroning == "invitations_done":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_dethroning == "dinner":
        if game.hour < 19:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 PM{/}.")))
        elif quest.isabelle_dethroning["dinner_picture"]:
          rv.append(get_quest_guide_marker("school_cafeteria", "maxine", marker = ["actions quest"], marker_offset = (25,-10)))
        elif "maxine" in quest.isabelle_dethroning["dinner_conversations"]:
          rv.append(get_quest_guide_marker("school_cafeteria", "flora", marker = ["actions camera"], marker_offset = (75,5)))
          rv.append(get_quest_guide_marker("school_cafeteria", "isabelle", marker = ["actions camera"], marker_offset = (5,0)))
          rv.append(get_quest_guide_marker("school_cafeteria", "kate", marker = ["actions camera"], marker_offset = (370,0)))
          if quest.isabelle_dethroning["kitchen_shenanigans"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_kitchen", marker = ["actions camera"], marker_sector = "left_mid", marker_offset = (85,40)))
          rv.append(get_quest_guide_marker("school_cafeteria", "lindsey", marker = ["actions camera"], marker_sector = "left_bottom", marker_offset = (130,5)))
          rv.append(get_quest_guide_marker("school_cafeteria", "maxine", marker = ["actions camera"], marker_offset = (25,-10)))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["actions camera"], marker_offset = (30,-15)))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_vending_left", marker = ["actions camera"], marker_sector = "left_bottom", marker_offset = (250,5)))
        else:
          if "flora" not in quest.isabelle_dethroning["dinner_conversations"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "flora", marker = ["actions quest"], marker_offset = (75,5)))
          if "isabelle" not in quest.isabelle_dethroning["dinner_conversations"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "isabelle", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (80,0)))
          if "kate" not in quest.isabelle_dethroning["dinner_conversations"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "kate", marker = ["actions quest"], marker_offset = (370,0)))
          if "lindsey" not in quest.isabelle_dethroning["dinner_conversations"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "lindsey", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (130,5)))
          if "maxine" not in quest.isabelle_dethroning["dinner_conversations"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "maxine", marker = ["actions quest"], marker_offset = (25,-10)))
          if "nurse" not in quest.isabelle_dethroning["dinner_conversations"]:
            rv.append(get_quest_guide_marker("school_cafeteria", "nurse", marker = ["actions quest"], marker_offset = (30,0)))

      elif quest.isabelle_dethroning == "trap":
        rv.append(get_quest_guide_path("school_first_hall", marker = ["actions go"]))
        if game.location == "school_first_hall":
          if school_first_hall["fishing_hook"]:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_red_paint", marker = ["items fishing_line@.9"]))
          elif school_first_hall["red_paint"]:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_red_paint", marker = ["items fishing_hook"]))
          else:
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items red_paint@.9"]))

      elif quest.isabelle_dethroning == "revenge":
        if game.location == "school_art_class":
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_supply_closet", marker = ["actions interact"]))
        elif quest.isabelle_dethroning["pea_brains"]:
          if mc.owned_item(("high_tech_lockpick","safety_pin")):
            item = mc.owned_item(("high_tech_lockpick","safety_pin"))
            rv.append(get_quest_guide_path("school_first_hall_west", marker = ["actions go"]))
            rv.append(get_quest_guide_marker("school_first_hall_west", "school_first_hall_west_door_art", marker = [items_by_id[item].icon+"@.9"], marker_sector = "left_bottom", marker_offset = (80,80)))
          else:
            rv.append(get_quest_guide_path("school_english_class", marker = ["teacher_desk@.3"]))
            rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (410,50)))
        else:
          rv.append(get_quest_guide_path("school_art_class", marker = ["actions go"]))

      elif quest.isabelle_dethroning == "revenge_done":
        rv.append(get_quest_guide_path("school_roof", marker = ["actions go"]))

      elif quest.isabelle_dethroning == "panik":
        if quest.isabelle_dethroning["gravity_of_the_situation"]:
          rv.append(get_quest_guide_path("school_entrance", marker = ["actions go"]))
        elif quest.isabelle_dethroning["call_jo"]:
          rv.append(get_quest_guide_hud("phone", marker=["call_jo","$\n\n\n{space=50}Call {color=#48F}[jo]{/}."]))
        else:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor guard_booth@.35"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_guard_booth", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (70,170)))

      elif quest.isabelle_dethroning == "fly_to_the_moon":
        if mc.owned_item("fire_axe"):
          rv.append(get_quest_guide_path("school_roof_landing", marker = ["door_to_the_roof@.1"]))
          rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_door", marker = ["items fire_axe@.9"], marker_sector = "left_bottom", marker_offset=(550,0)))
        else:
          rv.append(get_quest_guide_path("school_roof_landing", marker = ["fire_axe_cropped@.2"]))
          rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_fire_axe", marker = ["actions take"], marker_sector = "right_top", marker_offset=(75,-200)))

      elif quest.isabelle_dethroning == "done":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["actions go"]))

      return rv
