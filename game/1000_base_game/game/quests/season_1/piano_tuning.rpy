init python:
  class Quest_piano_tuning(Quest):

    hidden = True

    class phase_1_cabinet:
      pass
    class phase_2_baton:
      pass
    class phase_3_hammer:
      pass
    class phase_4_fork:
      pass
    class phase_1000_done:
      pass


  class Event_piano_tuning(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if game.season == 1:
        if new_location == "school_music_class" and not quest.piano_tuning.started and not mc["focus"]:
          quest.piano_tuning.start()
          if not quest.piano_tuning["out_of_tune"]:
            game.events_queue.append("quest_piano_tuning_start")

    def on_quest_started(event,quest,silent=False):
      if quest.id == "piano_tuning":
        school_music_class["top_compartment"] = "tuning_fork"
        school_music_class["middle_compartment"] = None
        school_music_class["bottom_compartment"] = "tuning_hammer"


label quest_piano_tuning_start:
  "Music class... the teacher always made it horrendous, but now that I've discovered the piano, maybe my grade will get bumped up?"
  "But tuning the piano outside requires a tuning hammer and a tuning fork..."
  return
