init python:
  class Quest_kate_trick(Quest):

    title = "The Dog Trick"

    class phase_10_break_in:
      description = "Not exactly the fittest, but\nit's a matter of survival."
      hint = "Lick the pock."
      def quest_guide_hint(quest):
        if mrsl.location == mc.location == "school_homeroom" and quest["break_in_attempt"]:
          return "Wait for [mrsl] to leave the homeroom."
        else:
          if mc.owned_item("high_tech_lockpick"):
            return "Use the high-tech lockpick on the teacher's desk in the homeroom."
          elif mc.owned_item("ball_of_yarn") and mc.owned_item("safety_pin"):
            return "Craft a high-tech lockpick by combining the ball of yarn with the safety pin."
          elif not mc.owned_item("ball_of_yarn"):
            return "Take the ball of yarn from the leftmost desk in the homeroom."
          elif not mc.owned_item("safety_pin"):
            return "Take the safety pin from the lollipop jar in the [nurse]'s office."

    class phase_20_jo:
      description = "A matter of unflinching ethics."
      hint = "Talk to the lady of the land."
      quest_guide_hint = "Quest [jo]."

    class phase_30_training:
      hint = "Time to take the highway to Hell.{space=-20}"
      quest_guide_hint = "Go to the gym."

    class phase_40_maxine:
      hint = "Talk to the girl who knows it all.{space=-10}"
      quest_guide_hint = "Quest [maxine]."

    class phase_50_flora:
      hint = "Talk to the girl who is a\nknow-it-all."
      quest_guide_hint = "Quest [flora]."

    class phase_51_park:
      hint = "The place of picnics and sunstrokes."
      quest_guide_hint = "Quest [flora] in the park."

    class phase_52_school:
      hint = "All the roads lead back to this place of deep-seated regret."
      quest_guide_hint = "Go to school."

    class phase_60_nurse:
      hint = "Who's a good doggy?"
      quest_guide_hint = "Quest the [nurse] in her office."

    class phase_70_gym:
      hint = "Return to the lake of fire."
      quest_guide_hint = "Go to the gym."

    class phase_80_workout:
      hint = "Time to get the blood pumping."
      quest_guide_hint = "Quest the [nurse]."

    class phase_90_report:
      hint = "Collect the, possibly, worst reward ever."
      def quest_guide_hint(quest):
        if 18 > game.hour > 6:
          return "Quest [jo] in the cafeteria."
        else:
          return "Go to sleep."

    class phase_1000_done:
      description = "They call you the Dog Whisperer now."


init python:
  class Event_quest_kate_trick(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.kate_trick == "park" and interactable == "flora" and mc.energy <= 13)
      or (quest.kate_trick == "nurse" and interactable == "nurse" and mc.energy <= 13)
      or quest.kate_trick in ("school","gym","workout")):
        return 0

    def on_can_advance_time(event):
      if quest.kate_trick in ("school","gym","workout"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if game.season == 1:
        if new_location == "school_first_hall" and quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and sum([mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills"),mc.owned_item("handcuffs")]) >= 2 and not quest.kate_trick.started:
          game.events_queue.append("quest_kate_trick_start")
        elif new_location == "school_ground_floor" and quest.kate_trick == "school":
          game.events_queue.append("quest_kate_trick_school")
        elif old_location == "school_gym" and quest.kate_trick == "report":
          school_gym["exclusive"] = 0

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_trick" and quest.kate_trick == "workout":
        nurse["at_none_now"] = False
        school_gym["exclusive"] = "nurse"

    def on_quest_guide_kate_trick(event):
      rv = []

      if quest.kate_trick == "break_in":
        if mrsl.location == mc.location == "school_homeroom" and quest.kate_trick["break_in_attempt"]:
          rv.append(get_quest_guide_hud("time", marker = "$Wait for {color=#48F}[mrsl]{/} to leave the {color=#48F}homeroom{/}."))
        else:
          if mc.owned_item("high_tech_lockpick"):
            rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom desk@.35"]))
            rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["items high_tech_lock@.8"], marker_sector = "left_bottom", marker_offset = (300,35)))
          elif mc.owned_item("ball_of_yarn") and mc.owned_item("safety_pin"):
              rv.append(get_quest_guide_hud("inventory", marker = ["ball_of_yarn_combine_safety_pin"], marker_sector = "mid_top", marker_offset = (60,0)))
          else:
            if not mc.owned_item("safety_pin"):
              rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room cup@.5"]))
              rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_lollipop_jar", marker = ["actions take"], marker_sector = "right_top", marker_offset = (0,120)))
            if not mc.owned_item("ball_of_yarn"):
              rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom yarn@.5"]))
              rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker = ["actions take"], marker_offset = (60,-20)))

      elif quest.kate_trick == "jo":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      elif quest.kate_trick == "training":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))

      elif quest.kate_trick == "maxine":
        if maxine.at("school_clubroom"):
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (70,20)))
        else:
          rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.kate_trick == "flora":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))
        if quest.kate_trick["no_deal"] and mc.money < 50:
          rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$50{/}.", marker_offset = (440,0)))

      elif quest.kate_trick == "park":
        rv.append(get_quest_guide_path("school_park", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_park", "flora", marker = ["actions quest"], marker_offset = (25,0)))

      elif quest.kate_trick == "school":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      elif quest.kate_trick == "nurse":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))

      elif quest.kate_trick == "gym":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))

      elif quest.kate_trick == "workout":
        rv.append(get_quest_guide_marker("school_gym", "nurse", marker = ["actions quest"], marker_offset = (25,0)))

      elif quest.kate_trick == "report":
        if 18 > game.hour > 6:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["jo contact_icon@.85"]))
          if jo.at("school_cafeteria"):
            rv.append(get_quest_guide_marker("school_cafeteria", "jo", marker = ["actions quest"]))
          else:
            if mc.at("school_cafeteria"):
              rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jo]{/} between\n"+("{color=#48F}9:00{/} — {color=#48F}17:00{/}." if persistent.time_format == '24h' else "{color=#48F}9:00 AM{/} — {color=#48F}5:00 PM{/}.")))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"]))

      return rv
