init python:
  class Quest_dress_to_the_nine(Quest):
    ## 1-1, Act 0, Scene 5, McQuestOne05
    title="Dress to the Nine"
    class phase_1_dress_to_the_nine:
      description="Or, like... to the four, maybe. Nothing fancy in that wardrobe."
      hint="In case you're wondering what to do: Put your shirt on, Sparky!"
    class phase_1000_done:
      description="At least you're not naked. You've spared many from years of therapy."

  class Event_quest_dress_to_the_nine(GameEvent):
    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.dress_to_the_nine.in_progress:
        if new_location==home_bedroom:
          game.events_queue.append("quest_dress_to_the_nine_paying_attention")
    def on_quest_guide_dress_to_the_nine(event):
      if quest.dress_to_the_nine=="dress_to_the_nine":
        rv=[]
        rv.append(get_quest_guide_path("home_bedroom"))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_closet", marker = "actions interact"))
        return rv

label quest_dress_to_the_nine_paying_attention:
  "I see it now."
  "The bedroom looks different."
  "The old posters have all returned."
  "These busty babes deserved a better fate than the bin."
  "My old computer hums in the corner again. It cost me a fortune to replace this piece of crap... and what about my secret folder?"
  "[jo] would never dare touch or make threats about my new computer, even as a prank."
  "Fuck."
  "I guess I did travel back in time..."
  "If this is all real, then maybe it's time to make an effort. Last time, everyone laughed at me for stumbling in late on the first day."
  "Shit."
  "Fully processing this fuckery would require more lifeforce than I currently have."
  "Might as well get dressed."
  return
