image triangle_cropped = Crop((0,184,80,150),"school music_class triangle")


init python:
  class Quest_isabelle_red(Quest):

    title = "Paint It Red"

    class phase_1_note:
      description = "The new color of my life."
      hint = "Note the note in the notoriously secret room."
      def quest_guide_hint(quest):
        if game.location == "school_clubroom":
          return "Interact with the note in the clubroom."
        else:
          return "Go to the clubroom."

    class phase_2_greenhouse:
      hint = "{i}\"From my office, feet so light, I've made my way this day most bright. Hidden in a world of greens — petals, pistils, color-schemes.\"{/}"
      def quest_guide_hint(quest):
        if quest["greenhouse_riddle_help"]:
          if game.location == "school_roof":
            return "Interact with the note on the greenhouse on the roof."
          else:
            return "Go to the roof."
        else:
          return "Try solving the riddle on your own, or quest [isabelle] for help."

    class phase_3_black_note:
      hint = "{i}\"Thought you had me, did you not? But I've already left this spot. Down the stairway, through the hall. In a classroom, on a wall.\"{/}"
      def quest_guide_hint(quest):
        if quest["black_note_riddle_help"]:
          if game.location == "school_english_class":
            return "Interact with the black note on the blackboard in the English classroom."
          else:
            return "Go to the English classroom."
        else:
          return "Try solving the riddle on your own, or quest [isabelle] for help."

    class phase_4_triangle:
      hint = "{i}\"Clever is your middle name, your eye for detail is no shame. My name is a tricky matter, hit me hard and ears may shatter.\"{/}"
      def quest_guide_hint(quest):
        if quest["triangle_riddle_help"]:
          if game.location == "school_music_class":
            return "Interact with the triangle in the music classroom."
          else:
            return "Go to the music classroom."
        else:
          return "Try solving the riddle on your own, or quest [isabelle] for help."

    class phase_5_cabinet:
      hint = "Clarinet or cabinet, it's music time."
      quest_guide_hint = "Interact with the cabinet in the music classroom."

    class phase_1000_done:
      description = "One step closer to revenge."


  class Event_quest_isabelle_red(GameEvent):
    def on_quest_guide_isabelle_red(event):
      rv = []

      if quest.isabelle_red == "note":
        rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_clubroom","school_clubroom_note", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (70,-10)))

      elif quest.isabelle_red == "greenhouse":
        if quest.isabelle_red["greenhouse_riddle_help"]:
          rv.append(get_quest_guide_path("school_roof", marker = ["school roof greenhouse@.15"]))
          rv.append(get_quest_guide_marker("school_roof","school_roof_note", marker = ["actions interact"], marker_offset = (-10,-10)))
        else:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_red == "black_note":
        if quest.isabelle_red["black_note_riddle_help"]:
          rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class blackboard@.25"]))
          rv.append(get_quest_guide_marker("school_english_class","school_english_class_note", marker = ["actions interact"], marker_offset = (-10,-20)))
        else:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_red == "triangle":
        if quest.isabelle_red["triangle_riddle_help"]:
          rv.append(get_quest_guide_path("school_music_class", marker = ["triangle_cropped@.6"]))
          rv.append(get_quest_guide_marker("school_music_class","school_music_class_triangle", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (-20,50)))
        else:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_red == "cabinet":
        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
        rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))

      return rv
