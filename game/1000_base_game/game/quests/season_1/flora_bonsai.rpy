init python:
  class Quest_flora_bonsai(Quest):
    title = "Tiny Thirsty Tree"
    class phase_1_flora:
      description = "All that I can see... is just another tiny tree!"
      hint = "\"Helping\" is a strong word. \"[flora]\" is another strong word. Too many strong words."
      quest_guide_hint = "Quest [flora] at home."
    class phase_20_refreshments:
      hint = "This shouldn't be too hard. A walk in the park. A piece of..."
      quest_guide_hint = "Give [flora] a slice of cake."
    class phase_30_nightvisit:
      hint = "My duty ends. Rest is eternal. Wishful thinking."
      quest_guide_hint = "Go to sleep."
    class phase_31_chef:
      hint = "It's a piece of cake to bake a pretty cake!"
      quest_guide_hint = "Quest [flora] in the kitchen."
    class phase_32_bake:
      hint = "It's a piece of cake to bake a pretty cake!"
      quest_guide_hint = "Bring [flora] a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
    class phase_33_check:
      hint = "It's a piece of cake to bake a pretty cake!"
      quest_guide_hint = "Bring [flora] a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
    class phase_34_sugar:
      hint = "A cubic ton of pure sugar. That's how the professor baked his three morbidly obese daughters."
      quest_guide_hint = "Find and give [flora] at least 6 sugar cubes."
    class phase_35_sleep:
      hint = "A date with the genderbend sandman. Hot like Sahara itself."
      quest_guide_hint = "Go back to sleep."
    class phase_40_water:
      hint = "A tree that hates water. Let's not drink-shame anyone here."
      quest_guide_hint = "Water the bonsai tree in the [nurse]'s office."
    class phase_41_pads:
      hint = "[isabelle] urgently needs something between her legs. Unfortunately, it's not my dick."
      quest_guide_hint = "Give [isabelle] the pads in the medical supplies."
    class phase_50_fetch:
      hint = "Your sweets, madam. Ah, of course. Coming right up. The one-day butler job."
      quest_guide_hint = "Fulfill all of [flora]'s requests... or just go to sleep."
    class phase_51_fetchover:
      hint = "Enough is enough. I quit!"
      quest_guide_hint = "Go to sleep."
    class phase_52_florahelp:
      hint = "[flora] also needs something between her legs. Still not my dick."
      quest_guide_hint = "Go to school."
    class phase_60_vines:
      hint = "Surely, those vines mean no harm. They seem perfectly harmless. [flora] is in no trouble."
      quest_guide_hint = "Quest [flora]."
    class phase_70_attack:
      hint = "Oh, no! The unthinkable! What are we going to do? Step one: open the door."
      quest_guide_hint = "Go to the [nurse]'s office."
    class phase_80_tinybigtree:
      hint = "What a tangled mess! A can of worms! Stop the vines at all costs!"
      quest_guide_hint = "Help or stop the vines from assaulting [flora]."
    class phase_1000_done:
      description = "It's hard to see the tree for all the forest. That monster is still out there somewhere."

  class Event_quest_flora_bonsai(GameEvent):
    #location flags --------------------------------------------------------------
    def on_location_changed(event,old_location,new_location,silent=False):
      #if new_location == "school_ground_floor" and quest.flora_bonsai == "water" and not quest.flora_bonsai['water_prompt']:
        #game.events_queue.append("quest_flora_bonsai_water_prompt")
      if new_location == "home_kitchen" and quest.flora_bonsai == "flora"  and not quest.flora_bonsai["blindfold_modal"]:
        game.events_queue.append("quest_flora_bonsai_blindfold_modal")
      if new_location == "school_nurse_room" and quest.flora_bonsai == "water"  and not quest.flora_bonsai['water_prompt_nurse']:
        game.events_queue.append("quest_flora_bonsai_water_nurse_room")
      #if new_location == "school_ground_floor_west" and quest.flora_bonsai == "fetch" and not quest.flora_bonsai["fetch"]:
        #game.events_queue.append("quest_flora_bonsai_fetch_sweets")
      #if new_location == "school_ground_floor" and quest.flora_bonsai == "fetch":
        #if not quest.flora_bonsai['fetch']:
          #game.events_queue.append("quest_flora_bonsai_fetch_sweets")
        #if quest.flora_bonsai['fetch'] == "bathroom" and quest.flora_bonsai["days"] == 1:
          #game.events_queue.append("quest_flora_bonsai_fetch_book")
      if new_location == "school_ground_floor" and quest.flora_bonsai == "florahelp":
        game.events_queue.append("quest_flora_bonsai_florahelp")
      if new_location == "school_ground_floor_west" and quest.flora_bonsai == "vines":
        game.events_queue.append("quest_flora_bonsai_vines_arrive")
      if new_location == "school_nurse_room" and quest.flora_bonsai == "attack":
        game.events_queue.append("quest_flora_bonsai_attack_arrive")
      if new_location == "school_ground_floor_west" and quest.flora_bonsai == "tinybigtree" and school_nurse_room["tree_done"]:
        game.events_queue.append("quest_flora_bonsai_tinybigtree_end")

      if quest.flora_bonsai["sweets_delivered"] and not quest.flora_bonsai["drink_message"]:
        game.events_queue.append("quest_flora_bonsai_fetch_drink")
      if quest.flora_bonsai["drink_delivered"] and not quest.flora_bonsai["bathroom_message"]:
        game.events_queue.append("quest_flora_bonsai_fetch_bathroom")
      if quest.flora_bonsai["bathroom_delivered"] and not quest.flora_bonsai["book_message"]:
        game.events_queue.append("quest_flora_bonsai_fetch_book")
      if quest.flora_bonsai["book_delivered"] and not quest.flora_bonsai["onions_message"]:
        game.events_queue.append("quest_flora_bonsai_fetch_onions")
    def on_advance(event): #Do not use any today/now flags attached to the flora_bonsai quest because it is overriden
      if quest.flora_bonsai == "fetch":
        if not flora["task_now"] and quest.flora_bonsai["hours"]<5:
          flora["task_now"] = True #instead use a character or location flag where appropriate
          quest.flora_bonsai["hours"] += 1
          #if quest.flora_bonsai["hours"] == 2 and quest.flora_bonsai["days"] == 0:
            #game.events_queue.append("quest_flora_bonsai_fetch_drink")
          #if quest.flora_bonsai["hours"] == 2 and quest.flora_bonsai["days"] == 1:
            #game.events_queue.append("quest_flora_bonsai_fetch_onions")
          #elif quest.flora_bonsai["hours"] == 3 and quest.flora_bonsai["days"] == 0:
            #game.events_queue.append("quest_flora_bonsai_fetch_bathroom")
        if not flora["task_today"]:
          flora["task_today"] = True
          quest.flora_bonsai["days"] += 1
          if quest.flora_bonsai["days"] == 1:
            #book but this takes place in change location because it happens in entrance hall
            pass
          #elif quest.flora_bonsai["days"] == 2:
            #if not quest.flora_bonsai['current_complete']:
              #flora.love -= 1
            #quest.flora_bonsai.advance("florahelp")
    def on_energy_cost(event,energy_cost,silent=False):
      if quest.flora_bonsai in ("chef","bake","check","sugar","sleep"):
        return 0
      if quest.flora_bonsai in ("florahelp","vines","attack","tinybigtree"):
        return 0
    def on_can_advance_time(event):
      if quest.flora_bonsai in ("chef","bake","check","sugar","sleep"):
        return False #disable time for night visit and cooking
      if quest.flora_bonsai in ("florahelp","vines","attack","tinybigtree"):
        return False

    def on_quest_guide_flora_bonsai(event):
      rv = []
      if quest.flora_bonsai == "flora":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
        rv.append(get_quest_guide_char("flora", marker = ["actions quest"]))
      elif quest.flora_bonsai == "refreshments":
        if mc.owned_item("cake_slice"):
          rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
          rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["items cake_slice"], marker_sector = "right_bottom"))
        else:
          if mc.owned_item("tide_pods") or mc.owned_item("lollipop"):
            for item in ["tide_pods","lollipop"]:
              if mc.owned_item(item):
                if school_english_class["desk_one_current_item"]:
                  if school_english_class["desk_one_used_today"]:
                    rv.append(get_quest_guide_hud("time", marker="$Check the student desk again {color=#48F}tomorrow{/}."))
                  else:
                    if school_english_class["desk_one_trade_count"] == 1:
                      rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk2@.25"]))
                      rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = ["actions interact"]))
                    else:
                      rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk2@.25"]))
                      rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = ["${space=175}"]+["actions interact"]+["${space=-50}Rollback until\n{space=-50}you get a\n{space=-50}{color=#48F}slice of cake."]))
                else:
                  rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk2@.25"]))
                  rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = items_by_id[item].icon))
          else:
            if school_english_class["desk_one_current_item"]:
              if school_english_class["desk_one_used_today"]:
                rv.append(get_quest_guide_hud("time", marker="$Check the student desk again {color=#48F}tomorrow{/}."))
              else:
                if school_english_class["desk_one_trade_count"] == 1:
                  rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk2@.25"]))
                  rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = ["actions interact"]))
                else:
                  rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class desk2@.25"]))
                  rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk_one", marker = ["${space=175}"]+["actions interact"]+["${space=-50}Rollback until\n{space=-50}you get a\n{space=-50}{color=#48F}slice of cake."]))
            else:
              if school_locker["gotten_lunch_today"]:
                rv.append(get_quest_guide_hud("time", marker="$Check your lunch box again {color=#48F}tomorrow{/}."))
                rv.append(get_quest_guide_path("school_first_hall", marker = ["$or{space=-75}"]+["school first_hall vending@.2"]+["$"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["$or{space=-75}"]+["items lollipop_1"]+["$"]))
              else:
                if not mc.at("school_locker"):
                  rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.6"]))
                if school_ground_floor["locker_unlocked"]:
                  rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["actions go"], marker_sector = "left_top", marker_offset = (100,100)))
                else:
                  rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_front_right", marker = ["items locker_key"], marker_sector = "left_top", marker_offset = (100,100)))
                rv.append(get_quest_guide_marker("school_locker", "school_locker_lunchfinal", marker = ["actions interact"]))
      elif quest.flora_bonsai == "nightvisit":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      elif quest.flora_bonsai == "chef":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
        rv.append(get_quest_guide_char("flora", marker = ["actions quest"]))
      elif quest.flora_bonsai == "bake":
        if quest.flora_bonsai['drawers'] == 1: #or quest.flora_bonsai['drawers'] == 0
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_d1", marker = ["actions interact"]))
        elif quest.flora_bonsai['drawers'] == 2:
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_d2", marker = ["actions interact"]))
        elif quest.flora_bonsai['drawers'] == 3:
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_d2", marker = ["actions interact"]))
        elif quest.flora_bonsai['drawers'] == 4:
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_d3", marker = ["actions interact"]))
        elif quest.flora_bonsai['drawers'] == 5:
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_mf", marker = ["actions interact"]))
        elif quest.flora_bonsai['drawers'] == 6:
          rv.append(get_quest_guide_path("home_kitchen"))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_c1", marker = ["actions interact"]))
        elif quest.flora_bonsai['drawers'] == 7 or quest.flora_bonsai['drawers-complete'] == 7:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
          rv.append(get_quest_guide_char("flora", marker = ["actions quest"]))
      elif quest.flora_bonsai == "check":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
        rv.append(get_quest_guide_char("flora", marker = ["actions quest"]))
      elif quest.flora_bonsai == "sugar":
        if mc.owned_item("sugar_cube",6):
          rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
          rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["items sugar_cube"]))
        else: #find cubes
          if not home_hall['sugarcube1_taken']:
            rv.append(get_quest_guide_path("home_hall"))
            rv.append(get_quest_guide_marker("home_hall", "home_hall_sugarcube1", marker = ["items sugar_cube"]))
          if not home_hall['sugarcube2_taken']:
            rv.append(get_quest_guide_path("home_hall"))
            rv.append(get_quest_guide_marker("home_hall", "home_hall_sugarcube2", marker = ["items sugar_cube"], marker_sector = "right_top", marker_offset = (0,-180)))
          if not home_bedroom['sugarcube3_taken']:
            rv.append(get_quest_guide_path("home_bedroom"))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_sugarcube3", marker = ["items sugar_cube"]))
          if not home_bedroom['sugarcube4_taken']:
            rv.append(get_quest_guide_path("home_bedroom"))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_sugarcube4", marker = ["items sugar_cube"]))
          if not home_bedroom['sugarcube5_taken']:
            rv.append(get_quest_guide_path("home_bedroom"))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_sugarcube5", marker = ["items sugar_cube"]))
          if not home_bathroom['sugarcube6_taken']:
            rv.append(get_quest_guide_path("home_bathroom"))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sugarcube6", marker = ["items sugar_cube"]))
          if not home_bathroom['sugarcube7_taken']:
            rv.append(get_quest_guide_path("home_bathroom"))
            rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sugarcube7", marker = ["items sugar_cube"]))
          if not home_kitchen['sugarcube8_taken']:
            rv.append(get_quest_guide_path("home_kitchen"))
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sugarcube8", marker = ["items sugar_cube"]))
          if not home_kitchen['sugarcube9_taken']:
            rv.append(get_quest_guide_path("home_kitchen"))
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sugarcube9", marker = ["items sugar_cube"]))
          if not home_kitchen['sugarcube10_taken']:
            rv.append(get_quest_guide_path("home_kitchen"))
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sugarcube10", marker = ["items sugar_cube"]))
      elif quest.flora_bonsai == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      elif quest.flora_bonsai == "water":
        if quest.flora_bonsai['water_prompt']:
          if mc.owned_item(((((("spray_banana_milk","spray_pepelepsi","spray_salted_cola","spray_seven_hp","spray_strawberry_juice","spray_urine","spray_water")))))):
            for item in ["spray_banana_milk","spray_pepelepsi","spray_salted_cola","spray_seven_hp","spray_strawberry_juice","spray_urine","spray_water"]:
              if mc.owned_item(item):
                rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room bonsai@.5"]))
                rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_bonsai", marker = items_by_id[item].icon, marker_offset = (400,-100)))        #marker_sector = "right_bottom",
          else:
            rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria drink@.4"]))
            rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_drink_machine", marker = ["items bottle spray_empty_bottle"], marker_offset = (25,-15)))
        else:
            rv.append(get_quest_guide_hud("map", marker = "$Leave your {color=#48F}bedroom{/}.", marker_sector = "mid_top"))
      elif quest.flora_bonsai == "pads":
        if not mc.owned_item('pad'):
          rv.append(get_quest_guide_path("school_nurse_room"))
          rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_medical_supplies", marker = ["actions take"]))
        else:
          rv.append(get_quest_guide_path("school_nurse_room"))
          rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_door", marker = ["isabelle contact_icon@.85"], marker_offset = (60,40)))
      elif quest.flora_bonsai == "fetch":
        if quest.flora_bonsai['fetch'] == "sweets":
          if quest.flora_bonsai["sweets_delivered"]:
            rv.append(get_quest_guide_hud("map", marker = "$Leave the {color=#48F}kitchen{/}.", marker_sector = "mid_top"))
          else:
            if mc.owned_item("doughnut") or mc.owned_item('lollipop_2') or mc.owned_item('lollipop'):
              for item in ["doughnut","lollipop_2","lollipop"]:
                if mc.owned_item(item):
                  rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
                  rv.append(get_quest_guide_char("flora", marker = items_by_id[item].icon))
            else:
              rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
              rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items lollipop_1"]))
        elif quest.flora_bonsai['fetch'] == "drink":
          if quest.flora_bonsai["drink_delivered"]:
            rv.append(get_quest_guide_hud("map", marker = "$Leave the {color=#48F}kitchen{/}.", marker_sector = "mid_top"))
          else:
            if mc.owned_item("seven_hp") or mc.owned_item("cup_of_coffee") or mc.owned_item("banana_milk") or mc.owned_item("pepelepsi") or mc.owned_item("strawberry_juice") or mc.owned_item("water_bottle") or mc.owned_item("salted_cola") or mc.owned_item("milk"):
              for item in ["seven_hp","cup_of_coffee","banana_milk","pepelepsi","strawberry_juice","water_bottle","salted_cola","milk"]:
                if mc.owned_item(item):
                  rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
                  rv.append(get_quest_guide_char("flora", marker = items_by_id[item].icon))
            elif mc.owned_item("empty_bottle"):
              if quest.lindsey_wrong["fountain_state"] == 5:
                rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"]))
                rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_aquarium", marker = ["items bottle empty_bottle"], marker_sector = "left_top", marker_offset = (200,150)))
              else:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall water@.6"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_water_fountain", marker = ["items bottle empty_bottle"], marker_sector = "left_bottom", marker_offset = (0,35)))
            else:
              if not home_kitchen["water_bottle_taken"]:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["items bottle water_bottle"]))
                rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"]))
              elif not school_cafeteria["got_coffee"]:
                rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria coffee"]))
                rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_coffee", marker = ["actions interact"]))
              else:
                rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
                rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["items bottle water_bottle"]))
        elif quest.flora_bonsai['fetch'] == "bathroom":
          if quest.flora_bonsai["bathroom_delivered"]:
            rv.append(get_quest_guide_hud("map", marker = "$Leave the {color=#48F}hallway{/}.", marker_sector = "mid_top"))
          else:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
            rv.append(get_quest_guide_char("flora", marker = ["actions quest"]))
        elif quest.flora_bonsai['fetch'] == "book":
          if quest.flora_bonsai["book_delivered"]:
            rv.append(get_quest_guide_hud("map", marker = "$Leave the {color=#48F}kitchen{/}.", marker_sector = "mid_top"))
          else:
            if mc.owned_item("catch_thirty_four"):
              if flora['hidden_now']:
                rv.append(get_quest_guide_hud("time", marker = "$Wait for {color=#48F}[flora]{/} to leave the {color=#48F}bathroom{/}."))
              else:
                rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
                rv.append(get_quest_guide_char(("flora"), marker = ["items book catch_34"]))
            else:
              rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class bookshelf@.3"]))
              rv.append(get_quest_guide_marker("school_english_class", "school_english_class_bookshelf", marker = ["actions take"]))
        elif quest.flora_bonsai['fetch'] == "onions":
          rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon_bandaid@.85"]))
          rv.append(get_quest_guide_char("flora", marker = ["actions quest"]))

        #also, just sleep twice
        #ask TWS for a nice line here to hint the player to skip time forward or sleep
      elif quest.flora_bonsai == 'fetchover':
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
        rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_bed", marker = ["actions go"]))
      elif quest.flora_bonsai == "florahelp":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["flora contact_icon_bandaid@.85"]))
        rv.append(get_quest_guide_char("flora"))
      elif quest.flora_bonsai == "vines":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["flora contact_icon_bandaid@.85"]))
        rv.append(get_quest_guide_char("flora", marker = ["actions quest"], marker_offset = (100,0)))
      elif quest.flora_bonsai == "attack":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["flora contact_icon_bandaid@.85"]))
        rv.append(get_quest_guide_char("flora"))
      elif quest.flora_bonsai == "tinybigtree":
        #making this an elif so player does not start with
        #r_vine_3, which is the only vine that locks a player
        #into a particular ending before reaching all 5
        #(because the pen is stuck in the vine and can't be used to scribble in the book)
        #also reduces clutter
        if not quest.flora_bonsai['r_vine_1']:
          rv.append(get_quest_guide_path("school_nurse_room"))
          rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_leftarm_vine", marker = ["actions interact"], marker_offset = (420,-50)))
        elif not quest.flora_bonsai['r_vine_2']:
          rv.append(get_quest_guide_path("school_nurse_room"))
          if not school_nurse_room["rightarm_vine_use_item"]:
            rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_rightarm_vine", marker = ["actions interact"], marker_offset = (-70,-80)))
          else:
            if mc.owned_item('stethoscope'):
              rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_rightarm_vine", marker = ["items stethoscope"], marker_offset = (-70,-80)))
            else:
              rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_steth", marker = ["actions take"]))
        elif not quest.flora_bonsai['r_vine_3']:
          rv.append(get_quest_guide_path("school_nurse_room"))
          if not school_nurse_room["leftleg_vine_use_item"]:
            rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_leftleg_vine", marker = ["actions interact"], marker_offset = (150,-40)))
          else:
            if mc.owned_item('nurse_pen'):
              rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_leftleg_vine", marker = ["items nurse_pen"], marker_offset = (150,-40)))
            else:
              rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_pen", marker = ["actions take"]))
        elif not quest.flora_bonsai['r_vine_4']:
          rv.append(get_quest_guide_path("school_nurse_room"))
          if not school_nurse_room["hungry_vine_use_item"]:
            rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_rightleg_vine", marker = ["actions interact"], marker_offset = (130,-10)))
          else:
            if mc.owned_item("meds"):
              rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_rightleg_vine", marker = ["items meds"], marker_offset = (130,-10)))
            else:
              rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_medical_supplies", marker = ["actions take"]))
        elif not quest.flora_bonsai['r_vine_5']:
          if not school_nurse_room["torso_vine_use_item"]:
            rv.append(get_quest_guide_path("school_nurse_room"))
            rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_torso_vine", marker = ["actions interact"], marker_offset = (290,-390)))
          else:
            if mc.owned_item("spray_water") or mc.owned_item("water_bottle"):
              for item in ["spray_water","water_bottle"]:
                if mc.owned_item(item):
                  rv.append(get_quest_guide_path("school_nurse_room", marker = ["flora contact_icon_bandaid@.85"]))
                  rv.append(get_quest_guide_marker("school_nurse_room","school_nurse_room_vines_flora_torso_vine", marker = items_by_id[item].icon, marker_offset = (290,-390)))
            else:
              rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west aquarium@.2"], marker_offset = (50,100)))
              rv.append(get_quest_guide_marker("school_ground_floor_west","school_ground_floor_west_aquarium", marker = ["items bottle spray_empty_bottle"]))
      return rv


label quest_flora_bonsai_blindfold_modal:
  $mc["focus"] = "flora_bonsai"
  $game.notify_modal(None,"Warning","{image=flora contact_icon_bandaid}{space=25}|"+"[flora] can't see anything!\n\nTo avoid inconsistencies,\nall other quests have been\nlocked for the time being.|{space=25}{image=items padlock}",5.0)
  if game.quest_guide != "flora_bonsai":
    $game.quest_guide = ""
  $quest.flora_bonsai["blindfold_modal"] = True
  return

label quest_flora_bonsai_water_prompt:
  $quest.flora_bonsai['water_prompt'] = True
  "Crap. I was supposed to water the [nurse]'s bonsai. They drink more than an alcoholic during happy hour..."
  "Better make sure it doesn't wilt. It is the [nurse]'s baby."
  jump goto_home_hall

label quest_flora_bonsai_water_nurse_room:
  $quest.flora_bonsai['water_prompt_nurse'] = True
  "Well, that's odd..."
  "I didn't know a bonsai could grow like this..."
  "The [nurse] does cut and trim it sometimes, so maybe this is what happens if you don't."
  "I should probably try to find a pair of scissors or something..."
  "But for now, let's keep it from dying."
  return

label quest_flora_bonsai_fetch_sweets:
  play sound "phone_vibrate"
  $set_dialog_mode("phone_message","flora")
  flora "Bring me sweets!"
  $set_dialog_mode()
  "Welp, that was quick."
  "Sweets? Well, as long as it's only that..."
  $quest.flora_bonsai["sweets_message"] = True
  $quest.flora_bonsai['fetch'] = "sweets"
  $quest.flora_bonsai['hours'] = 0
  $quest.flora_bonsai['days'] = 0
  $quest.flora_bonsai['current_complete'] = False
  $flora['task_now'] = True
  $flora['task_today'] = True
  if quest.flora_bonsai["helped_isabelle"]:
    return
  else:
    jump goto_school_ground_floor_west

label quest_flora_bonsai_fetch_drink:
  play sound "phone_vibrate"
  $set_dialog_mode("phone_message","flora")
  flora "I'm thirsty now! Bring me a drink!"
  $set_dialog_mode()
  "...Already?"
  $quest.flora_bonsai["drink_message"] = True
  $quest.flora_bonsai['fetch'] = "drink"
  if not quest.flora_bonsai['current_complete']:
    $flora.love -= 1
  $quest.flora_bonsai['current_complete'] = False
  return

label quest_flora_bonsai_fetch_bathroom:
  play sound "phone_vibrate"
  $set_dialog_mode("phone_message","flora")
  flora "Help me to the bathroom!"
  $set_dialog_mode()
  "Is a little bit of gratitude too much to ask for?"
  "On my way, I guess."
  $quest.flora_bonsai["bathroom_message"] = True
  $quest.flora_bonsai['fetch'] = "bathroom"
  if not quest.flora_bonsai['current_complete']:
    $flora.love-=1
  $quest.flora_bonsai['current_complete'] = False
  return

label quest_flora_bonsai_fetch_book:
  play sound "phone_vibrate"
  $set_dialog_mode("phone_message","flora")
  flora "Bring me a book from the English classroom!"
  mc "Why? How are you supposed to read it?"
  $set_dialog_mode()
  "Ugh, why am I even replying? It's not like she can see it."
  $quest.flora_bonsai["book_message"] = True
  $quest.flora_bonsai['fetch'] = "book"
  if not quest.flora_bonsai['current_complete']:
    $flora.lust -= 1
  $quest.flora_bonsai['current_complete'] = False
  $quest.flora_bonsai['hours'] = 0
  return

label quest_flora_bonsai_fetch_onions:
  play sound "phone_vibrate"
  $set_dialog_mode("phone_message","flora")
  flora "I need you to cut onions for me."
  $set_dialog_mode()
  "..."
  "Whatever. Fine. Better she not lose her fingers."
  $quest.flora_bonsai["onions_message"] = True
  $quest.flora_bonsai['fetch'] = "onions"
  if not quest.flora_bonsai['current_complete']:
    $flora.lust -= 1
  $quest.flora_bonsai['current_complete'] = False
  return

label quest_flora_bonsai_florahelp:
  show flora sad with Dissolve(.5)
  mc "[flora]? What's wrong?"
  flora sad "Oh, thank god. I got lost trying to get to the [nurse]'s office..."
  mc "Have you hurt yourself?"
  flora annoyed "No! But someone stole my pads when I was on the toilet!"
  mc "Huh? That's odd."
  flora annoyed "More like rude!"
  mc "Odd because the same exact thing happened to [isabelle] earlier."
  flora confused "That is odd... but also, we can find the pad thief later."
  flora sarcastic "Onward to the [nurse]'s office!"
  $quest.flora_bonsai.advance("vines")
  hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_vines_arrive:
  "Holy crap..."
  "Those vines? Surely, it can't be..."
  return

label quest_flora_bonsai_attack_arrive:
  mc "Oh, fuck."
  flora "Aaaaaaah!"
  "Need to do something quick before that thing eats her... or worse!"
  "Well, depends on how you look at it. Might be better."
  "Anyway..."
  flora "Noooo!"
  mc "Shit! Shit! Shit! Hold on, I'm coming!"
  return

label quest_flora_bonsai_tinybigtree_end:
  $school_nurse_room["teddy_gone"] = False
  $school_nurse_room['poster_torn'] = False
  if school_nurse_room["stethoscope_taken"] and not mc.owned_item("stethoscope"):
    $school_nurse_room["stethoscope_taken"] = False
  if school_nurse_room["pen_taken"] and not mc.owned_item("nurse_pen"):
    $school_nurse_room["pen_taken"] = False
  $school_nurse_room["curtain_off"] = True

  $nurse["none"] = False
  $quest.flora_bonsai.finish()
  return
