init python:
  class Quest_isabelle_over_kate(Quest):

    title = "Dethroning the Queen"

    class phase_1_choosing_isabelle:
      hint = "[kate]'s reign of terror must be stopped. The new girl said so. I don't even work here."

    class phase_2_meeting:
      hint = "For schemes that will likely end in failure — Cafeteria."

    class phase_3_search_and_schedule:
      def hint(quest):
        if sum([mc.owned_item("red_paint"),mc.owned_item("fishing_hook"),mc.owned_item("fishing_line"),mc.owned_item("pig_mask")]) == 4:
          return "Schedule a meeting with the oracle on the upper floor."
        else:
          return "Acquire four magical artifacts: a bucket of red paint, a fishing hook, a fishing line, and a pig mask."

    class phase_1000_done:
      description = "At this point, saving your own skin is what matters. Consequences be damned."

    class phase_2000_failed:
      description = "At this point, saving your own skin is what matters. Consequences be damned."


init python:
  class Event_quest_isabelle_over_kate(GameEvent):

    def on_quest_guide_isabelle_over_kate(event):
      rv=[]

      if quest.isabelle_tour == "english_class":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))

      elif quest.isabelle_tour == "art_class":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_art_class", marker = ["isabelle contact_icon@.85"]))

      elif quest.isabelle_tour == "gym":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_gym", marker = ["isabelle contact_icon@.85"]))

      elif quest.isabelle_tour == "cafeteria":
        rv.append(get_quest_guide_char("isabelle", marker = ["actions quest"]))
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["isabelle contact_icon@.85"]))

      elif quest.isabelle_over_kate == "search_and_schedule":
        if sum([mc.owned_item("red_paint"),mc.owned_item("fishing_hook"),mc.owned_item("fishing_line"),mc.owned_item("pig_mask")]) == 4:
          if quest.kate_wicked.finished:
            if quest.maxine_eggs.finished:
              rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
              rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))
            elif quest.maxine_eggs.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}The Glowing Spider Eggs{/}."))
            else:
              rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
              if maxine.at("school_clubroom","sitting"):
                rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
              else:
                if mc.at("school_clubroom") and not maxine.at("school_clubroom"):
                  rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
          else:
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Wicked Game{/}."))
        elif quest.kate_blowjob_dream.in_progress:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Make a Wish and Blow{/}."))
        else:
          if not mc.owned_item("red_paint"):
            if quest.isabelle_red.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Paint It Red{/}."))
            else:
              if quest.jacklyn_art_focus.finished and quest.jacklyn_broken_fuse.finished and quest.clubroom_access.finished:
                rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
                if jacklyn.at("school_art_class"):
                  rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
                else:
                  if mc.at("school_art_class"):
                    rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))
              if quest.jacklyn_art_focus.finished:
                pass
              elif quest.jacklyn_art_focus.in_progress:
                rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Art Through Suffering{/}."))
              else:
                if quest.lindsey_book.finished:
                  rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
                  if jacklyn.at("school_art_class"):
                    rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
                  else:
                    if mc.at("school_art_class"):
                      rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))
                elif quest.lindsey_book.in_progress:
                  rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Fully Booked{/}."))
                else:
                  rv.append(get_quest_guide_char("lindsey", marker = ["lindsey contact_icon@.85"], force_marker = True))
                  rv.append(get_quest_guide_marker(mc.location, "lindsey", marker = ["actions quest"]))
              if quest.jacklyn_broken_fuse.finished:
                pass
              elif quest.jacklyn_broken_fuse.in_progress:
                rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}A Short Fuse{/}."))
              else:
                if quest.flora_jacklyn_introduction.finished:
                  rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
                  if jacklyn.at("school_art_class"):
                    rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
                  else:
                    if mc.at("school_art_class"):
                      rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))
                elif quest.flora_jacklyn_introduction.in_progress:
                  rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Punked{/}."))
                else:
                  rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
                  if flora.at("home_kitchen"):
                    rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"]))
                  else:
                    if mc.at("home_kitchen"):
                      rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[flora]{/} at "+("{color=#48F}18:00{/}." if persistent.time_format == '24h' else "{color=#48F}6:00 PM{/}.")))
              if quest.clubroom_access.finished:
                pass
              elif quest.clubroom_access.in_progress:
                rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Oxygen for Oxymoron{/}."))
              else:
                rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.4"]))
                rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["actions quest"], marker_sector="right_top", marker_offset = (0,-220)))
          if not mc.owned_item(("fishing_hook","fishing_line")):
            if quest.nurse_venting.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Venting Frustration{/}."))
            else:
              if quest.clubroom_access.finished:
                rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
                if maxine.at("school_clubroom","fishing"):
                  rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (65,15)))
                else:
                  if mc.at("school_clubroom") and not maxine.at("school_clubroom"):
                    rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[maxine]{/} to come back."))
              elif quest.clubroom_access.in_progress:
                rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Oxygen for Oxymoron{/}."))
              else:
                rv.append(get_quest_guide_path("school_first_hall_east", marker = ["school first_hall_east vent@.4"]))
                rv.append(get_quest_guide_marker("school_first_hall_east","school_first_hall_east_vent", marker = ["actions quest"], marker_sector="right_top", marker_offset = (0,-220)))
          if not mc.owned_item("pig_mask"):
            if home_computer["pig_mask_received"]:
              rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
            elif home_computer["pig_mask_ordered_today"]:
              rv.append(get_quest_guide_hud("time", marker="$Wait a day."))
            elif home_computer["pig_mask_ordered"]:
              rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.7"]))
              rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_pig_mask", marker = ["actions take"]))
            elif mc.owned_item("costume_shop_voucher"):
              rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
              rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions costume_shop"]))
            elif quest.kate_wicked.in_progress:
              rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}Wicked Game{/}."))
            else:
              if quest.piano_tuning.in_progress:
                if quest.piano_tuning == "cabinet":
                  rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                  rv.append(get_quest_guide_marker("school_music_class","school_music_class_cabinet", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (220,110)))
                elif quest.piano_tuning == "baton":
                  if school_music_class["got_conductor_baton"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class podium@.4"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_teacher_podium", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-10)))
                elif quest.piano_tuning == "hammer":
                  if school_first_hall_west["tuning_hammer"]:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                  else:
                    if mc.owned_item("tuning_hammer"):
                      rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                      rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_hammer@.7"], marker_offset = (150,250)))
                    else:
                      if school_music_class["middle_compartment"]:
                        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                        rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                      else:
                        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                        rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                elif quest.piano_tuning == "fork":
                  if school_music_class["bottom_compartment"]:
                    if school_music_class["middle_compartment"]:
                      rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                      rv.append(get_quest_guide_marker("school_music_class","school_music_class_middle_compartment", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                    else:
                      if school_music_class["top_compartment"] == "tuning_fork":
                        rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                        rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items conductor_baton@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                      elif school_music_class["top_compartment"] == "conductor_baton":
                        if school_first_hall_west["tuning_fork"]:
                          rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                          rv.append(get_quest_guide_marker("school_music_class","school_music_class_top_compartment", marker = ["items tuning_fork@.8"], marker_sector = "left_bottom", marker_offset = (110,-40)))
                        else:
                          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                          rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["items tuning_fork@.8"], marker_offset = (150,250)))
                  else:
                    rv.append(get_quest_guide_path("school_music_class", marker = ["school music_class cabinet@.2"]))
                    rv.append(get_quest_guide_marker("school_music_class","school_music_class_bottom_compartment", marker = ["items tuning_hammer@.7"], marker_sector = "left_bottom", marker_offset = (110,-40)))
              else:
                rv.append(get_quest_guide_path("school_first_hall_west", marker = ["school first_hall_west piano@.125"]))
                if quest.piano_tuning.finished:
                  rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["$Play something{space=-50}\ndark and dramatic{space=-50}","actions quest","${space=275}"], marker_offset = (150,250)))
                else:
                  rv.append(get_quest_guide_marker("school_first_hall_west","school_first_hall_west_piano", marker = ["actions quest"], marker_offset = (150,250)))

      return rv
