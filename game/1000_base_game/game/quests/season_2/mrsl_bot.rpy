image shoebox_beaver_carcass_combine = LiveComposite((280,130),(0,35),"items shoebox",(96,35),Transform("actions combine",zoom=0.85),(184+5,35),"items beaver_carcass")
image shoebox_beaver_pelt_combine = LiveComposite((280,130),(0,35),"items shoebox",(96,35),Transform("actions combine",zoom=0.85),(184+5,35),"items beaver_pelt")
image shoebox_fire_axe_combine = LiveComposite((280,130),(0,35),"items shoebox",(96,35),Transform("actions combine",zoom=0.85),(184-10,35),"items fire_axe")
image shoebox_glass_shard_combine = LiveComposite((280,130),(0,35),"items shoebox",(96,35),Transform("actions combine",zoom=0.85),(184-10,35),"items glass_shard")
image shoebox_machete_combine = LiveComposite((280,130),(0,35),"items shoebox",(96,35),Transform("actions combine",zoom=0.85),(184-10,35),"items machete")
image shoebox_nurse_pen_combine = LiveComposite((280,130),(0,35),"items shoebox",(96,35),Transform("actions combine",zoom=0.85),(184-15,35),"items nurse_pen")
image paper_pen_combine = LiveComposite((280,130),(0,35),"items paper",(96,35),Transform("actions combine",zoom=0.85),(184,35),"items ballpen")
image note_from_mrsl_interact = LiveComposite((180,130),(0,35),"items note_from_mrsl",(95,37),Transform("actions interact",zoom=0.85))
image note_from_mrsl_pen_combine = LiveComposite((280,130),(0,35),"items note_from_mrsl",(96,35),Transform("actions combine",zoom=0.85),(184,35),"items ballpen")
image note_from_maxine_interact = LiveComposite((180,130),(0,35),"items note_from_maxine",(95,37),Transform("actions interact",zoom=0.85))
image note_from_maya_interact = LiveComposite((180,130),(0,35),"items note_from_maya",(95,37),Transform("actions interact",zoom=0.85))


init python:
  class Quest_mrsl_bot(Quest):

    title = "Hot My Bot"

    class phase_1_dream:
      description = "Just like the commercial."
      # hint = "What is this sudden urge to run and be healthy? You better hurry before the motivation leaves you!"
      hint = "What is this sudden urge to run{space=-5}\nand be healthy? You better hurry{space=-20}\nbefore the motivation leaves you!{space=-20}"
      quest_guide_hint = "Go to the gym."

    class phase_2_phrase:
      hint = "For the hot bot, go straight to the source."
      quest_guide_hint = "Quest [mrsl]."

    class phase_3_wait:
      hint = "Life is just an endless series of waiting."
      def quest_guide_hint(quest):
        return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."

    class phase_4_meeting:
      # hint = "Time to rendezvous en la eschool."
      hint = "Time to rendezvous en la eschool.{space=-20}"
      quest_guide_hint = "Go to school."

    class phase_5_sleep:
      # hint = "The hour is here for your favorite activity that isn't video games or masturbation."
      hint = "The hour is here for your favorite{space=-15}\nactivity that isn't video games or{space=-15}\nmasturbation."
      quest_guide_hint = "Go to sleep."

    class phase_6_confrontation:
      # hint = "Time to get hard... by asking the tough questions to the right teacher."
      hint = "Time to get hard... by asking the{space=-5}\ntough questions to the right teacher."
      quest_guide_hint = "Quest [mrsl] again."

    class phase_7_help:
      # hint = "Go annoy the girl that gets most annoyed by you."
      hint = "Go annoy the girl that gets most{space=-10}\nannoyed by you."
      quest_guide_hint = "Quest [flora]."

    class phase_8_shoebox:
      hint = "Think outside the box. Inside it. Think of the box. Make the box."
      def quest_guide_hint(_quest):
        item = mc.owned_item(("beaver_carcass","beaver_pelt","fire_axe","glass_shard","machete","nurse_pen"))
        if home_hall["shoebox_taken"] and item and quest.nurse_aid["name_reveal"]:
          item = "the " + item
          return "Combine the shoebox with {}.".format(item.replace("_"," ").replace("the nurse","[amelia]'s"))
        elif home_hall["shoebox_taken"] and item:
          return "Combine the shoebox with the {}.".format(item.replace("_"," ").replace("nurse","[nurse]'s"))
        elif home_hall["shoebox_taken"]:
          # return "Take the machete in the hallway."
          return "Take the machete in the hallway.{space=-20}"
        elif home_hall["shoebox"]:
          # return "Take the shoebox in the hallway."
          return "Take the shoebox in the hallway.{space=-10}"
        else:
          return "Wait 1 hour."

    class phase_9_message_box:
      # hint = "What goes in the box? A message. Where should the box be? The homeroom."
      hint = "What goes in the box? A message.{space=-20}\nWhere should the box be? The homeroom."
      def quest_guide_hint(quest):
        if school_homeroom["message_box"]:
          return "Use the note to [mrsl] on the message box."
        elif mc.owned_item("note_to_mrsl"):
          # return "Use the message box on the teacher's desk in the homeroom."
          return "Use the message box on the{space=-5}\nteacher's desk in the homeroom.{space=-10}"
        elif quest["some_paper_and_pen"]:
          # return "Combine the paper with the pen."
          return "Combine the paper with the pen.{space=-10}"
        else:
          # return "Take the paper and pen from the teacher's desk in the English classroom."
          return "Take the paper and pen from the{space=-5}\nteacher's desk in the English classroom."

    class phase_10_back_and_forth:
      hint = "Wait, sleep, read, write, repeat."
      def quest_guide_hint(quest):
        if mc.owned_item("fourth_note_from_mrsl"):
          return "Interact with the fourth note from [mrsl] in your inventory."
        elif quest["fourth_note_from_mrsl_received"]:
          # return "Take the fourth note from [mrsl] from the message box in the homeroom."
          return "Take the fourth note from [mrsl]{space=-10}\nfrom the message box in the homeroom."
        elif quest["fourth_note_to_mrsl_delivered"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        elif mc.owned_item("fourth_note_to_mrsl"):
          return "Use the fourth note to [mrsl] on the message box."
        elif quest["third_note_from_mrsl_interacted_with"]:
          return "Combine the third note from [mrsl] with the pen."
        elif mc.owned_item("third_note_from_mrsl"):
          return "Interact with the third note from [mrsl] in your inventory."
        elif quest["third_note_from_mrsl_received"]:
          # return "Take the third note from [mrsl] from the message box in the homeroom."
          return "Take the third note from [mrsl]{space=-5}\nfrom the message box in the homeroom."
        elif quest["note_from_maya_interacted_with"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        elif mc.owned_item("note_from_maya"):
          return "Interact with the (hopefully) third note from [mrsl] in your inventory."
        elif quest["note_from_maya_received"]:
          # return "Take the third note from [mrsl] from the message box in the homeroom."
          return "Take the third note from [mrsl]{space=-5}\nfrom the message box in the homeroom."
        elif quest["note_from_maxine_interacted_with"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        elif mc.owned_item("note_from_maxine"):
          return "Interact with the third note from [mrsl] in your inventory."
        elif quest["note_from_maxine_received"]:
          # return "Take the third note from [mrsl] from the message box in the homeroom."
          return "Take the third note from [mrsl]{space=-5}\nfrom the message box in the homeroom."
        elif quest["third_note_to_mrsl_delivered"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        elif mc.owned_item("third_note_to_mrsl"):
          return "Use the third note to [mrsl] on the message box."
        elif quest["second_note_from_mrsl_interacted_with"]:
          return "Combine the second note from [mrsl] with the pen."
        elif mc.owned_item("second_note_from_mrsl"):
          return "Interact with the second note from [mrsl] in your inventory."
        elif quest["second_note_from_mrsl_received"]:
          # return "Take the second note from [mrsl] from the message box in the homeroom."
          return "Take the second note from [mrsl]{space=-15}\nfrom the message box in the homeroom."
        elif quest["second_note_to_mrsl_delivered"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        elif mc.owned_item("second_note_to_mrsl"):
          # return "Use the second note to [mrsl] on the message box."
          return "Use the second note to [mrsl] on{space=-5}\nthe message box."
        elif quest["note_from_mrsl_interacted_with"]:
          return "Combine the note from [mrsl] with the pen."
        elif mc.owned_item("note_from_mrsl"):
          return "Interact with the note from [mrsl] in your inventory."
        elif quest["note_from_mrsl_received"]:
          # return "Take the note from [mrsl] from the message box in the homeroom."
          return "Take the note from [mrsl] from{space=-5}\nthe message box in the homeroom."
        else:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."

    class phase_11_help_2_electric_boogaloo:
      hint = "Find someone that doesn't care about the rules. Someone feral."
      quest_guide_hint = "Quest [maya]."

    class phase_12_paint:
      # hint = "You can accomplish quite a lot with a bucket of paint and a plan."
      hint = "You can accomplish quite a lot with a bucket of paint and a plan.{space=-15}"
      quest_guide_hint = "Take the red paint from the paint shelf in the art classroom."

    class phase_13_diversion:
      hint = "Time for round two with the redhead."
      quest_guide_hint = "Quest [maya] again."

    class phase_14_wait_2_electric_boogaloo:
      # hint = "Leave the premise, leave the day."
      hint = "Leave the premise, leave the day.{space=-15}"
      def quest_guide_hint(quest):
        if quest["wait_some_more"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        else:
          return "Leave the entrance hall."

    class phase_15_aftermath:
      # hint = "You've got mail! If you don't know where, pay more attention!"
      hint = "You've got mail! If you don't know where, pay more attention!{space=-20}"
      def quest_guide_hint(quest):
        if mc.owned_item("fifth_note_from_mrsl"):
          return "Interact with the fifth note from [mrsl] in your inventory."
        else:
          return "Take the fifth note from [mrsl] from the message box in the homeroom."

    class phase_16_meeting_2_electric_boogaloo:
      # hint = "The worst part of life is waiting."
      hint = "The worst part of life is waiting.{space=-5}"
      def quest_guide_hint(quest):
        return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."

    class phase_1000_done:
      description = "Between a boob and a soft place."


init python:
  class Event_quest_mrsl_bot(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.mrsl_bot in ("dream","meeting","diversion"):
        return 0

    def on_can_advance_time(event):
      if quest.mrsl_bot in ("dream","meeting","diversion"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_gym" and quest.mrsl_bot == "dream":
        game.events_queue.append("quest_mrsl_bot_dream")
      elif new_location == "school_homeroom" and quest.mrsl_bot == "back_and_forth" and not quest.mrsl_bot["message_trap"]:
        game.events_queue.append("quest_mrsl_bot_back_and_forth_upon_entering")
      elif new_location == "school_art_class" and quest.mrsl_bot == "paint" and not quest.mrsl_bot["just_one_bucket"]:
        game.events_queue.append("quest_mrsl_bot_paint_upon_entering")
      elif old_location == "school_ground_floor" and quest.mrsl_bot == "wait_2_electric_boogaloo" and not quest.mrsl_bot["wait_some_more"]:
        game.events_queue.append("quest_mrsl_bot_wait_2_electric_boogaloo")

    def on_advance(event):
      if quest.mrsl_bot == "wait" and game.hour == 19:
        game.events_queue.append("quest_mrsl_bot_wait")
      elif quest.mrsl_bot == "sleep" and game.hour == 7:
        game.events_queue.append("quest_mrsl_bot_sleep")
      elif quest.mrsl_bot == "shoebox" and not home_hall["shoebox"]:
        home_hall["shoebox"] = True
      elif quest.mrsl_bot == "back_and_forth" and game.hour == 7 and not quest.mrsl_bot["note_from_mrsl_received"]:
        quest.mrsl_bot["note_from_mrsl_received"] = True
      elif quest.mrsl_bot == "back_and_forth" and quest.mrsl_bot["second_note_to_mrsl_delivered"] and game.hour == 7 and not quest.mrsl_bot["second_note_from_mrsl_received"]:
        quest.mrsl_bot["second_note_from_mrsl_received"] = True
      elif quest.mrsl_bot == "back_and_forth" and quest.mrsl_bot["third_note_to_mrsl_delivered"] and game.hour == 7 and not quest.mrsl_bot["note_from_maxine_received"]:
        quest.mrsl_bot["note_from_maxine_received"] = True
      elif quest.mrsl_bot == "back_and_forth" and quest.mrsl_bot["note_from_maxine_interacted_with"] and game.hour == 7 and not quest.mrsl_bot["note_from_maya_received"]:
        quest.mrsl_bot["note_from_maya_received"] = True
      elif quest.mrsl_bot == "back_and_forth" and quest.mrsl_bot["note_from_maya_interacted_with"] and game.hour == 7 and not quest.mrsl_bot["third_note_from_mrsl_received"]:
        quest.mrsl_bot["third_note_from_mrsl_received"] = True
      elif quest.mrsl_bot == "back_and_forth" and quest.mrsl_bot["fourth_note_to_mrsl_delivered"] and game.hour == 7 and not quest.mrsl_bot["fourth_note_from_mrsl_received"]:
        quest.mrsl_bot["fourth_note_from_mrsl_received"] = True
      elif quest.mrsl_bot == "wait_2_electric_boogaloo" and game.hour == 7:
        quest.mrsl_bot["fifth_note_from_mrsl_received"] = True
        quest.mrsl_bot.advance("aftermath")
      elif quest.mrsl_bot == "meeting_2_electric_boogaloo" and game.hour == 19:
        game.events_queue.append("quest_mrsl_bot_meeting_2_electric_boogaloo")
      if quest.mrsl_bot["ball_of_yarn_delivered"] and game.hour == 7 and not quest.mrsl_bot["second_note_from_maya_received"]:
        quest.mrsl_bot["second_note_from_maya_received"] = True

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "mrsl_bot" and quest.mrsl_bot == "diversion":
        school_ground_floor["exclusive"] = "maya"

    def on_quest_guide_mrsl_bot(event):
      rv = []

      if quest.mrsl_bot == "dream":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))

      elif quest.mrsl_bot == "phrase":
        rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "mrsl", marker = ["actions quest"]))

      elif quest.mrsl_bot == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 PM{/}.")))

      elif quest.mrsl_bot == "meeting":
        rv.append(get_quest_guide_marker("school_entrance", "school_entrance_door", marker = ["actions go"], marker_offset = (-20,-20)))

      elif quest.mrsl_bot == "sleep":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.mrsl_bot == "confrontation":
        rv.append(get_quest_guide_char("mrsl", marker = ["mrsl contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "mrsl", marker = ["actions quest"]))

      elif quest.mrsl_bot == "help":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))

      elif quest.mrsl_bot == "shoebox":
        item = mc.owned_item(("beaver_carcass","beaver_pelt","fire_axe","glass_shard","machete","nurse_pen"))
        if home_hall["shoebox_taken"] and item:
          rv.append(get_quest_guide_hud("inventory", marker = ["shoebox_{}_combine".format(item)], marker_sector = "mid_top", marker_offset = (60,0)))
        elif home_hall["shoebox_taken"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall sword@.7"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_sword", marker = ["actions take"], marker_offset = (-10,-10)))
        elif home_hall["shoebox"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall shoebox@.4"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_shoebox", marker = ["actions take"]))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))

      elif quest.mrsl_bot == "message_box":
        if school_homeroom["message_box"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["items note_to_mrsl"], marker_offset = (-5,-15)))
        elif mc.owned_item("note_to_mrsl"):
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom desk@.35"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["items message_box"], marker_sector = "left_bottom", marker_offset = (300,35)))
        elif quest.mrsl_bot["some_paper_and_pen"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["paper_pen_combine"], marker_sector = "mid_top", marker_offset = (60,0)))
        else:
          rv.append(get_quest_guide_path("school_english_class", marker = ["teacher_desk@.3"]))
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (410,50)))

      elif quest.mrsl_bot == "back_and_forth":
        if mc.owned_item("fourth_note_from_mrsl"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.mrsl_bot["fourth_note_from_mrsl_received"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["fourth_note_to_mrsl_delivered"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif quest.mrsl_bot["fourth_note_to_mrsl_delivered"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif mc.owned_item("fourth_note_to_mrsl"):
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["items second_note_to_mrsl"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["third_note_from_mrsl_interacted_with"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_pen_combine"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif mc.owned_item("third_note_from_mrsl"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.mrsl_bot["third_note_from_mrsl_received"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["note_from_maya_interacted_with"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif quest.mrsl_bot["note_from_maya_interacted_with"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif mc.owned_item("note_from_maya"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_maya_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.mrsl_bot["note_from_maya_received"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["note_from_maxine_interacted_with"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif quest.mrsl_bot["note_from_maxine_interacted_with"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif mc.owned_item("note_from_maxine"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_maxine_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.mrsl_bot["note_from_maxine_received"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["third_note_to_mrsl_delivered"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif quest.mrsl_bot["third_note_to_mrsl_delivered"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif mc.owned_item("third_note_to_mrsl"):
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["items second_note_to_mrsl"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["second_note_from_mrsl_interacted_with"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_pen_combine"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif mc.owned_item("second_note_from_mrsl"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.mrsl_bot["second_note_from_mrsl_received"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["second_note_to_mrsl_delivered"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif quest.mrsl_bot["second_note_to_mrsl_delivered"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif mc.owned_item("second_note_to_mrsl"):
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["items second_note_to_mrsl"], marker_offset = (-5,-15)))
        elif quest.mrsl_bot["note_from_mrsl_interacted_with"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_pen_combine"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif mc.owned_item("note_from_mrsl"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.mrsl_bot["note_from_mrsl_received"]:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))
        elif quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))

      elif quest.mrsl_bot == "help_2_electric_boogaloo":
        rv.append(get_quest_guide_char("maya", marker = ["maya contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maya", marker = ["actions quest"]))

      elif quest.mrsl_bot == "paint":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
        rv.append(get_quest_guide_marker("school_art_class", "school_art_class_shelf", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (580,50)))

      elif quest.mrsl_bot == "diversion":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["maya contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "maya", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (100,0)))

      elif quest.mrsl_bot == "wait_2_electric_boogaloo":
        if quest.mrsl_bot["wait_some_more"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif quest.mrsl_bot["wait_some_more"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        else:
          rv.append(get_quest_guide_hud("map", marker = "$\nLeave the\n{color=#48F}entrance hall{/}.", marker_sector = "mid_top"))

      elif quest.mrsl_bot == "aftermath":
        if mc.owned_item("fifth_note_from_mrsl"):
          rv.append(get_quest_guide_hud("inventory", marker = ["note_from_mrsl_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        else:
          rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom message_box@.9"]))
          rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_message_box", marker = ["actions take"], marker_offset = (-5,-15)))

      elif quest.mrsl_bot == "meeting_2_electric_boogaloo":
        rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 PM{/}.")))

      return rv
