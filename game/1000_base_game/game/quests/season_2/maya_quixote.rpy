image bathroom_shower = LiveComposite((347,719), (0,0), "home bathroom shower", (32,49), "home bathroom shower_glass")
image text_jo = LiveComposite((0,0),(63,-53),Transform("jo contact_icon",zoom=0.69),(149,-38),Transform("phone apps contact_info text",zoom=0.5))


init python:
  class Quest_maya_quixote(Quest):

    title = "Don Juan Quixote"

    class phase_1_sleep:
      # description = "Fantasy. Delusion. Seduction. What's not to love?"
      description = "Fantasy. Delusion. Seduction.\nWhat's not to love?"
      hint = "Close your eyes and dream."
      quest_guide_hint = "Go to sleep."

    class phase_2_arrival:
      hint = "It's said that the fastest way to a man's heart is through his stomach."
      quest_guide_hint = "Go to the kitchen."

    class phase_3_xCube:
      hint = "Time for this troll to retreat to his cave."
      quest_guide_hint = "Go to your bedroom."

    class phase_4_shower:
      hint = "Something smells ripe... and I'm pretty sure it's you. Time to bathe."
      quest_guide_hint = "Go to the bathroom."

    class phase_5_text:
      hint = "Call her, beep her, when you need her."
      quest_guide_hint = "Text [jo] on your phone."

    class phase_6_sleep_again:
      hint = "When the girls exhaust you, simply take a rest."
      quest_guide_hint = "Go to sleep again."

    class phase_7_power_outage:
      hint = "What kind of double-trouble-tinfoil-bubble are those b-witches getting into? You'll never know if you stay here."
      quest_guide_hint = "Leave your bedroom."

    class phase_8_attic:
      hint = "Time to dig around in an old hole... no, not [jo]'s!"
      def quest_guide_hint(quest):
        if mc.owned_item("xcube_controller") and home_hall["table_taken"] and quest["old_tricks"]:
          return "Use the xCube controller on the hole in the hallway until you acquire the Dungeons and Damsels board game."
        elif home_hall["table_taken"] and quest["old_tricks"]:
          return "Take the xCube controller in your bedroom."
        elif quest["old_tricks"]:
          return "Move the table in the hallway."
        else:
          return "Interact with the hole in the hallway."

    class phase_9_board_game:
      hint = "With your spoils, you shall return once again to your cave."
      quest_guide_hint = "Go to your bedroom."

    class phase_10_power_back:
      pass

    class phase_1000_done:
      # description = "And so they rode into the sunset and lived happily ever after. At least, until the next adventure."
      description = "And so they rode into the sunset\nand lived happily ever after.\n\nAt least, until the next adventure."


init python:
  class Event_quest_maya_quixote(GameEvent):

    def on_advance(event):
      if quest.maya_quixote == "sleep" and game.hour == 7:
        quest.maya_quixote.advance("arrival")
      if quest.maya_quixote == "sleep_again" and game.hour == 7 and not mc["focus"]:
        quest.maya_quixote.advance("power_outage")
        mc["focus"] = "maya_quixote"

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "home_kitchen" and quest.maya_quixote == "arrival":
        game.events_queue.append("quest_maya_quixote_arrival")
      elif new_location == "home_bedroom" and quest.maya_quixote == "xCube":
        game.events_queue.append("quest_maya_quixote_xCube")
      elif new_location == "home_bathroom" and quest.maya_quixote == "shower" and not mc["focus"]:
        game.events_queue.append("quest_maya_quixote_shower")
      elif new_location == "home_hall" and quest.maya_quixote == "power_outage":
        game.events_queue.append("quest_maya_quixote_power_outage")
      elif new_location == "home_bedroom" and quest.maya_quixote == "board_game":
        game.events_queue.append("quest_maya_quixote_board_game")

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if (quest.maya_quixote in ("text","power_outage","attic","board_game")
      or (quest.maya_quixote == "shower" and interactable == "home_hall_door_bathroom" and mc.energy <= 11)):
        return 0

    def on_can_advance_time(event):
      if quest.maya_quixote in ("text","power_outage","attic","board_game"):
        return False

    def on_quest_guide_maya_quixote(event):
      rv = []

      if quest.maya_quixote == "sleep":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.maya_quixote == "arrival":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))

      elif quest.maya_quixote == "xCube":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom controller_clean"]))

      elif quest.maya_quixote == "shower":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["bathroom_shower@.175"]))

      elif quest.maya_quixote == "text":
        rv.append(get_quest_guide_hud("phone", marker=["text_jo","$\n\n\n{space=50}Text {color=#48F}[jo]{/}."]))

      elif quest.maya_quixote == "sleep_again":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.maya_quixote == "power_outage":
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_door", marker = ["actions go"]))

      elif quest.maya_quixote == "attic":
        if mc.owned_item("xcube_controller") and home_hall["table_taken"] and quest.maya_quixote["old_tricks"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall hole@.8"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_hole", marker = ["items xcube_controller@.7"], marker_sector = "right_top", marker_offset = (60,-260)))
        elif home_hall["table_taken"] and quest.maya_quixote["old_tricks"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom controller_clean"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_controller", marker = ["actions take"], marker_sector = ("left_bottom" if home_bedroom["clean"] else "right_bottom"), marker_offset = ((80,-20) if home_bedroom["clean"] else (100,110))))
        elif quest.maya_quixote["old_tricks"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall table@.4"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_table", marker = ["actions interact"], marker_offset = (50,100)))
        else:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall hole@.8"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_hole", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (60,-260)))

      elif quest.maya_quixote == "board_game":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["actions go"]))

      return rv
