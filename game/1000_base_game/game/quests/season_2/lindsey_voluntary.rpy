init python:
  class Quest_lindsey_voluntary(Quest):

    title = "Voluntary Good"

    class phase_1_wait:
      description = "Caring hands, healing hearts."
      hint = "The place where the heart is."
      quest_guide_hint = "Go home."

    class phase_2_investigation:
      # hint = "Scientifically speaking... that's all."
      hint = "Scientifically speaking... that's all.{space=-10}"
      quest_guide_hint = "Quest [maxine] in the science classroom."

    class phase_3_discovery:
      hint = "Lock 'n' roll secrets."
      quest_guide_hint = "Open the secret locker in the entrance hall."

    class phase_4_interrogation:
      # hint = "If it walks like a cat and talks like a cat, it's probably..."
      hint = "If it walks like a cat and talks like{space=-15}\na cat, it's probably..."
      quest_guide_hint = "Quest [maya]."

    class phase_5_locker_file:
      hint = "Seek out administration if you know what's good."
      quest_guide_hint = "Quest [jo]."

    class phase_6_help:
      hint = "It's in a room with tables and at least one computer."
      def quest_guide_hint(_quest):
        if not quest.lindsey_voluntary["old_prison"] and not quest.maya_spell.finished:
          return "Go to the computer room."
        elif not quest.lindsey_voluntary["few_people"] and not quest.maya_spell.finished:
          return "Go to the clubroom."
        else:
          return "Go to the science classroom."

    class phase_7_new_computer:
      hint = "When you need a shock collar, but you like it up the butt."
      def quest_guide_hint(quest):
        if quest["rogue_AI"] and mc.money < 200:
          return "Gather $200."
        else:
          return "Interact with Sparks & Plugs in the Newfall Marina."

    class phase_8_delivery:
      hint = "Technology and science — the perfect match."
      # quest_guide_hint = "Go back to the science classroom."
      quest_guide_hint = "Go back to the science classroom.{space=-20}"

    class phase_9_volunteer:
      hint = "Out of the goodness of your heart... time to work."
      quest_guide_hint = "Go to the Newfall Hospital."

    class phase_10_dream:
      pass

    class phase_11_bedroom:
      pass

    class phase_1000_done:
      description = "Slowly healing, body and soul."


init python:
  class Event_quest_lindsey_voluntary(GameEvent):

    def on_quest_finished(event,quest_finished,silent=False):
      if quest_finished.id == "lindsey_angel":
        quest.lindsey_angel["finished_today"] = quest.lindsey_angel["finished_this_week"] = True

    def on_advance(event):
      if quest.lindsey_voluntary == "wait" and game.hour == 19:
        game.events_queue.append("quest_lindsey_voluntary_wait_on_advance")
      elif quest.lindsey_voluntary == "volunteer" and game.hour == 19:
        game.events_queue.append("quest_lindsey_voluntary_volunteer_on_advance")

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "home_kitchen" and quest.lindsey_voluntary == "wait":
        game.events_queue.append("quest_lindsey_voluntary_wait")
      elif new_location == "school_computer_room" and quest.lindsey_voluntary == "help" and not quest.lindsey_voluntary["old_prison"] and not quest.maya_spell.finished:
        game.events_queue.append("quest_lindsey_voluntary_help_computer_room")
      elif new_location == "school_clubroom" and quest.lindsey_voluntary == "help" and quest.lindsey_voluntary["old_prison"] and not quest.lindsey_voluntary["few_people"] and not quest.maya_spell.finished:
        game.events_queue.append("quest_lindsey_voluntary_help_clubroom")
      elif new_location == "school_science_class" and quest.lindsey_voluntary == "help" and ((quest.lindsey_voluntary["old_prison"] and quest.lindsey_voluntary["few_people"]) or quest.maya_spell.finished):
        game.events_queue.append("quest_lindsey_voluntary_help")
      elif new_location == "school_science_class" and quest.lindsey_voluntary == "delivery":
        game.events_queue.append("quest_lindsey_voluntary_delivery")
      elif new_location == "hospital" and quest.lindsey_voluntary == "volunteer" and not mc["focus"]:
        game.events_queue.append("quest_lindsey_voluntary_volunteer")
      elif new_location == "school_ground_floor" and quest.lindsey_voluntary == "dream":
        game.events_queue.append("quest_lindsey_voluntary_dream_entrance_hall")
      elif new_location == "school_roof_landing" and quest.lindsey_voluntary == "dream":
        game.events_queue.append("quest_lindsey_voluntary_dream_roof_landing")

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.lindsey_voluntary == "dream":
        return 0

    def on_can_advance_time(event):
      if quest.lindsey_voluntary == "dream":
        return False

    def on_quest_guide_lindsey_voluntary(event):
      rv = []

      if quest.lindsey_voluntary == "wait":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))

      elif quest.lindsey_voluntary == "investigation":
        rv.append(get_quest_guide_path("school_science_class", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_science_class", "maxine", marker = ["actions quest"], marker_offset = (35,0)))

      elif quest.lindsey_voluntary == "discovery":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor lockersback@.5"]))
        rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_lockers_back", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (160,-20)))

      elif quest.lindsey_voluntary == "interrogation":
        rv.append(get_quest_guide_char("maya", marker = ["maya contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maya", marker = ["actions quest"]))

      elif quest.lindsey_voluntary == "locker_file":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      elif quest.lindsey_voluntary == "help":
        if not quest.lindsey_voluntary["old_prison"] and not quest.maya_spell.finished:
          rv.append(get_quest_guide_path("school_computer_room", marker = ["actions go"]))
        elif not quest.lindsey_voluntary["few_people"] and not quest.maya_spell.finished:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["actions go"]))
        else:
          rv.append(get_quest_guide_path("school_science_class", marker = ["actions go"]))

      elif quest.lindsey_voluntary == "new_computer":
        if quest.lindsey_voluntary["rogue_AI"] and mc.money < 200:
          rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$200{/}.", marker_offset = (500,0)))
        else:
          rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
          rv.append(get_quest_guide_marker("marina", "marina_sparks_plugs", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (400,40)))

      elif quest.lindsey_voluntary == "delivery":
        rv.append(get_quest_guide_path("school_science_class", marker = ["actions go"]))

      elif quest.lindsey_voluntary == "volunteer":
        rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("marina", "marina_hospital", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (300,-70)))

      elif quest.lindsey_voluntary == "dream":
        if quest.lindsey_voluntary["lights_off"]:
          rv.append(get_quest_guide_path("school_roof_landing", marker = ["lindsey contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_fire_axe", marker = ["actions interact"], marker_sector = "right_top", marker_offset=(75,-200)))
        else:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      return rv
