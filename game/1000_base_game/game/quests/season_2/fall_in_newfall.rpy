init python:
  class Quest_fall_in_newfall(Quest):

    title = "Fall in Newfall"

    class phase_1_break_over:
      hint = "Time to leave the den of misery and face the world."
      quest_guide_hint = "Leave your bedroom."

    class phase_2_assembly:
      description = "The fall of man... or just\nanother autumn."
      hint = "Being late for the assembly would be very on brand, but your{space=-20}\nbrand isn't exactly high end."
      quest_guide_hint = "Go to school."

    class phase_3_late:
      hint = "Don't be such an introvert."
      def quest_guide_hint(_quest):
        if quest.kate_stepping.finished:
          return "Quest [flora], [isabelle], and [maya]."
        elif quest.isabelle_dethroning.finished:
          return "Quest [flora], [isabelle], [kate], and [maya]."

    class phase_4_stick_around:
      hint = "What has long legs and smells of cigarettes?"
      quest_guide_hint = "Quest [mrsl]."

    class phase_1000_done:
      description = "Seasons change, and the world still turns on its tilted axis."


init python:
  class Event_quest_fall_in_newfall(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.jo_washed.finished and not quest.fall_in_newfall.started)
      or quest.fall_in_newfall.in_progress):
        return 0

    def on_can_advance_time(event):
      if ((quest.jo_washed.finished and not quest.fall_in_newfall.started)
      or quest.fall_in_newfall.in_progress):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.fall_in_newfall == "assembly" and new_location == "school_entrance":
        game.events_queue.append("quest_fall_in_newfall_assembly_school_entrance")
      elif quest.fall_in_newfall == "late" and new_location == "school_homeroom":
        game.events_queue.append("quest_fall_in_newfall_late_school_homeroom")

    def on_enter_roaming_mode(event):
      if ((quest.kate_stepping.finished and quest.fall_in_newfall == "late" and "flora" in quest.fall_in_newfall["assembly_conversations"] and "isabelle" in quest.fall_in_newfall["assembly_conversations"] and "maya" in quest.fall_in_newfall["assembly_conversations"])
      or (quest.isabelle_dethroning.finished and quest.fall_in_newfall == "late" and "flora" in quest.fall_in_newfall["assembly_conversations"] and "kate" in quest.fall_in_newfall["assembly_conversations"] and "isabelle" in quest.fall_in_newfall["assembly_conversations"] and "maya" in quest.fall_in_newfall["assembly_conversations"])):
        game.events_queue.append("quest_fall_in_newfall_late")

    def on_quest_guide_fall_in_newfall(event):
      rv = []

      if quest.fall_in_newfall == "not_started":
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (75,25)))

      elif quest.fall_in_newfall == "break_over":
        rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_door", marker = ["actions go"]))

      elif quest.fall_in_newfall == "assembly":
        rv.append(get_quest_guide_path("school_entrance", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_entrance", "school_entrance_door", marker = ["actions go"]))

      elif quest.fall_in_newfall == "late":
        if "flora" not in quest.fall_in_newfall["assembly_conversations"]:
          rv.append(get_quest_guide_marker("school_homeroom", "flora", marker = ["actions quest"], marker_offset = (50,0)))
        if "isabelle" not in quest.fall_in_newfall["assembly_conversations"]:
          rv.append(get_quest_guide_marker("school_homeroom", "isabelle", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (140,0)))
        if quest.isabelle_dethroning.finished:
          if "kate" not in quest.fall_in_newfall["assembly_conversations"]:
            rv.append(get_quest_guide_marker("school_homeroom", "kate", marker = ["actions quest"], marker_offset = (20,0)))
        if "maya" not in quest.fall_in_newfall["assembly_conversations"]:
          rv.append(get_quest_guide_marker("school_homeroom", "maya", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (200,0)))

      elif quest.fall_in_newfall == "stick_around":
        rv.append(get_quest_guide_marker("school_homeroom", "mrsl", marker = ["actions quest"], marker_offset = (10,0)))

      return rv


label quest_fall_in_newfall_not_started_home_bedroom_bed:
  menu(side="middle"):
    "What time is it?"
    "Sleeping time":
      "My work here is done."
      window hide
      hide location
      hide screen hud
      hide screen interface_hider
      show black
      with Dissolve(3.0)
      if not quest.lindsey_motive.actually_finished:
        show expression Text("Two Weeks Later", font="fonts/Fresca-Regular.ttf", color="#fefefe", size=69) onlayer screens zorder 100 as two_weeks_later:
          xalign 0.5 yalign 0.5 alpha 0.0
          pause 1.0
          easein 3.0 alpha 1.0
      pause 5.0
      jump quest_fall_in_newfall_start
    "Brooding time":
      "Rest is for the dead."
      "And those who are afraid of energy drinks."
  return

label quest_fall_in_newfall_not_started_home_bedroom_door:
  "I'm definitely not leaving my room anymore today."
  return

label quest_fall_in_newfall_start:
  python:
    game.season = 2
    game.day+=14
    flora.outfit = {"bra": "flora_purple_bra", "panties": "flora_striped_panties", "pants": "flora_dress", "shirt": "flora_blouse"}
    isabelle.outfit = {"hat": "isabelle_tiara", "collar": "isabelle_collar" if quest.kate_stepping.finished else None, "jacket": "isabelle_jacket", "shirt": "isabelle_top", "bra": "isabelle_green_bra", "pants": "isabelle_skirt", "panties": "isabelle_green_panties"}
    jacklyn.outfit = {"choker": "jacklyn_chained_choker", "shirt": "jacklyn_jacket", "bra": "jacklyn_orange_bra", "pants": "jacklyn_shorts", "fishnet": "jacklyn_high_waist_fishnet", "panties": "jacklyn_orange_panties"}
    jo.outfit = {"glasses": "jo_glasses", "coat": "jo_coat", "shirt": "jo_shirt", "bra": "jo_white_bra", "pants": "jo_pants", "panties": "jo_white_panties"}
    kate.outfit = {"necklace": "kate_choker", "shirt": "kate_bardot_top", "bra": "kate_blue_bra", "pants": "kate_skirt", "panties": "kate_blue_panties", "boots": "kate_knee_high_boots"}
    maxine.outfit = {"hat":"maxine_hat", "glasses":"maxine_glasses", "necklace":"maxine_necklace", "shirt":"maxine_sweater", "bra":"maxine_black_bra", "pants":"maxine_skirt", "panties":"maxine_black_panties"}
    mrsl.outfit = {"coat":"mrsl_coat", "dress":"mrsl_red_dress", "bra":"mrsl_black_bra", "panties":"mrsl_black_panties"}
    nurse.outfit = {"hat":"nurse_hat", "shirt":"nurse_dress", "bra":"nurse_green_bra", "panties":"nurse_green_panties"}

    home_bedroom["night"] = True
    school_forest_glade["unlocked"] = school_computer_room["unlocked"] = True
    school_first_hall_west["scorch_marks"] = True

    for quest_title,quest_id in get_available_quest_guides():
      if quest_id:
        game.quests[quest_id].finish(silent=True)
        game.timed_out_quests.add(quest_id)
    if "act_one" in game.timed_out_quests:
      game.timed_out_quests.remove("act_one")
    quest.act_two.start()
    quest.fall_in_newfall.start()

  if quest.lindsey_motive.actually_finished:
    show expression "images/misc/pulse_monitor_firstframe.webp" as pulse_monitor with Dissolve(.5)
    pause 0.25
#   SFX: Pulse monitor
    show expression Movie(fps=24, channel=u'sprite', play="images/misc/pulse_monitor.webm", side_mask=False, start_image="images/misc/pulse_monitor_firstframe.webp") as pulse_monitor
    $renpy.music.stop(channel=u'sprite', fadeout=0)
    pause 1.0
    window auto
    "Life, it seems, is all about loss."
    "It doesn't matter how many chances you get."
    window hide
    pause 0.5
    show misc ambulance_away as ambulance
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 3.0
    hide pulse_monitor
    hide black onlayer screens zorder 100
    with Dissolve(.5)
    window auto
#   SFX: Ambulance
    "You can do your best, give everything you've got, and still end up with your heart shattered against the concrete."
    "Some things are just outside of your control."
    "Why [lindsey] of all people?"
    "Why her?"
    "Why...?"
    window hide
    show misc ambulance_gone as ambulance with Dissolve(1.0)
    window auto
    "The sirens fade into nothing."
    "All that's left is an empty silence."
    "An empty silence and doubts."
    "Maybe it's my fault for traveling back in time?"
    "Maybe something broke when she ran into me on that first day?"
    "..."
    "Maybe it's all my fault."
    window hide
    show misc panorama_window_on as panorama:
      xalign 0.03
    show black onlayer screens zorder 100
    with Dissolve(3.0)
    show expression Text("Two Weeks Later", font="fonts/Fresca-Regular.ttf", color="#fefefe", size=69) onlayer screens zorder 100 as two_weeks_later:
      xalign 0.5 yalign 0.5 alpha 0.0
      pause 1.0
      easein 3.0 alpha 1.0
    pause 5.0
    hide ambulance
    hide two_weeks_later onlayer screens
    hide black onlayer screens
    with Dissolve(.5)
  if not renpy.showing("panorama"):
    hide two_weeks_later onlayer screens
    show misc panorama_window_on as panorama:
      xalign 0.03
    with Dissolve(.5)
  pause 0.25
  window auto
  "Autumn in Newfall."
  "Ringlets of smoke rise from the chimneys."
  window hide
  show misc panorama_window_on as panorama:
    ease 3.0 xalign 0.52
  pause 3.0
  hide panorama
  show misc panorama_window_on as panorama:
    xalign 0.52
  window auto
  "The forest changes into an orange dress, and the girls follow suit."
  "Relaxed picnics turn into brisk walks through the parks."
  "The sweet scents of hot chocolate and pumpkin pies fill the streets."
  window hide
  show misc panorama_window_on as panorama:
    ease 3.0 xalign 0.97
  pause 3.0
  hide panorama
  show misc panorama_window_on as panorama:
    xalign 0.97
  window auto
  "When the darkness creeps in, the warm lights of the cafés promise an escape from the damp and cold."
  "People spill in and out, laughing, hugging, and having a carefree time."
  "Their only worry is if their cone of candied almonds will run out."
  window hide
  show misc panorama_window_off as panorama:
    xalign 0.97
    ease 3.0 xalign 0.03
  pause 3.0
  hide panorama
  show misc panorama_window_off as panorama:
    xalign 0.03
  pause 0.0
  scene location
  show screen hud
  show screen interface_hider
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide panorama
  hide black onlayer screens zorder 100
  with Dissolve(.5)
  window auto
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   "They don't have a stiff neck from looking over their shoulders."
#   "Their heart rates don't spike to two hundred whenever the phone rings."
#   "Movies make it seem like you're safe once you leave the interrogation{space=-30}\nroom, but that's not the case."
#   "Being the prime suspect in an investigation for attempted murder eats away at you."
#   "Any one of these days, [lindsey] might wake up and..."
#   "Well, who knows what that little bitch will remember?"
#   "And who knows what lies she'll tell?"
#   window hide
#   play sound "<from 0.25 to 1.0>phone_vibrate"
#   pause 0.5
#   $set_dialog_mode("phone_message_centered","hidden_number")
#   hidden_number "The path you are on is one of regret.{nw}"
#   pause 1.0
#   hidden_number "Change course now, or end up worse than before.{nw}"
#   pause 1.0
#   mc "I forgot the part where I asked for your opinion.{nw}"
#   pause 1.0
#   window auto
#   $set_dialog_mode("phone_message_plus_textbox")
#   "Does this person know something?"
#   "..."
#   "I should probably be careful with them from here on out."
#   "I'm not out of the woods, but [lindsey] has decided to give me the gift of time."
#   "I better make the most of it."
    pass
  elif quest.lindsey_motive.actually_finished:
    "They don't understand the pain."
    "They didn't see the look on her face as she tumbled off the roof."
    "They didn't hear her bones snap."
    "And of course the cops didn't believe that a doll caused her fall..."
    "Her parents blamed me. Some students did as well."
    "The world is so unfair..."
    "[lindsey] would tell them if she could, I'm sure."
    "She would tell them I did everything in my power to save her."
    "..."
    "Every night, I've been praying."
    "What else can you do?"
    "I always thought that religion was a big scam, but now I understand.{space=-5}"
    "Religion is for the powerless. It's for those refusing to let go of hope.{space=-20}"
    "For those balancing on the edge between ruin and bliss."
    "..."
    "If only I could take her hand in mine..."
    "Wake her with a kiss..."
    "See those big blue eyes open again..."
    "That's what I pray for."
    "Please, give me a sign!"
    window hide
    play sound "<from 0.25 to 1.0>phone_vibrate"
    pause 0.5
    $set_dialog_mode("phone_message_centered","hidden_number")
    hidden_number "Do not give up.{nw}"
    pause 1.0
    window auto
    $set_dialog_mode("phone_message_plus_textbox")
    "Thanks for the pep talk. Exactly what I needed."
    "It's hard to find anyone so out of touch..."
  else:
    "And then there's me..."
    "Two weeks of sleepless nights and what ifs."
    "Perhaps it's karma hitting me."
    if quest.kate_stepping.finished:
      "Payback for the betrayal against [isabelle]."
    elif quest.isabelle_dethroning.finished:
      "Payback for breaking the status quo with [isabelle]."
    "Why would [lindsey] jump off the roof?"
    "She had so much to live for..."
    "Not that she's dead, of course, but surely that was her intention."
    "It can't be a fun life for her now... even if she does wake up from her coma."
    window hide
    play sound "<from 0.25 to 1.0>phone_vibrate"
    pause 0.5
    $set_dialog_mode("phone_message_centered","hidden_number")
    hidden_number "She needs you.{nw}"
    pause 1.0
    window auto
    $set_dialog_mode("phone_message_plus_textbox")
    "Not sure what I can do that a team of the best doctors in the state couldn't do..."
    "Maybe I could pay her a visit, anyway."
  window hide
  $set_dialog_mode("")
  pause 0.5
  window auto
  "Tomorrow, fall break ends, and everything is supposed to return\nto normal."
  "But how can it?"
  "Normalcy has been shattered, maybe forever."
  "Newfall High is the center of tragedy."
  "[flora] even said someone had left flowers..."
  "They all think [lindsey] is never going to wake up again."
  window hide
  call home_bedroom_bed_interact_force_sleep
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if quest.kate_stepping.finished:
    play sound "<from 0.25 to 1.0>phone_vibrate"
    pause 0.5
    $mc.add_message_to_history("kate", kate, "{image=misc isabelle_picture}", day=game.day-15, hour=20)
    $mc.add_message_to_history("kate", kate, "A little thanks for the help to make this bitch heel.", day=game.day-15, hour=20)
    $set_dialog_mode("phone_message_centered","kate")
    pause 1.0
    window auto
    $set_dialog_mode("phone_message_plus_textbox")
    "Whoa! Perhaps I should have checked my phone sooner..."
    "..."
    "[isabelle] really looks good as a sub. There's no denying that."
    "And [kate] messaging me out of the blue..."
    "It feels like I've reached a new height of social status."
  elif quest.isabelle_dethroning.finished:
    if quest.isabelle_dethroning["isabelle_caught"]:
      return
    else:
      play sound "<from 0.25 to 1.0>phone_vibrate"
      pause 0.5
      $mc.add_message_to_history("isabelle", isabelle, "Hey, how are you doing?", day=game.day-14, hour=21)
      $mc.add_message_to_history("isabelle", isabelle, "Just thought I'd check in.", day=game.day-14, hour=21)
      $mc.add_message_to_history("isabelle", isabelle, "Hey, I'm worried about you.", day=game.day-10, hour=12)
      $mc.add_message_to_history("isabelle", isabelle, "Can we talk?", day=game.day-10, hour=12)
      $mc.add_message_to_history("isabelle", isabelle, "I know what you're going through. Shutting out the world isn't the answer.", day=game.day-7, hour=19)
      $mc.add_message_to_history("isabelle", isabelle, "You need to talk to someone, even if it's not me.", day=game.day-7, hour=19)
      $mc.add_message_to_history("isabelle", isabelle, "Don't be too hard on yourself, okay?", day=game.day-5, hour=22)
      $mc.add_message_to_history("isabelle", isabelle, "Message me back, please!", day=game.day-3, hour=8)
      $set_dialog_mode("phone_message_centered","isabelle")
      pause 0.5
      if not mc.check_phone_contact("isabelle"):
        $mc.add_phone_contact("isabelle")
      pause 0.5
      window auto
      $set_dialog_mode("phone_message_plus_textbox")
      "[isabelle] really does seem to care..."
      "I just haven't had it in me to text her back."
  window hide
  $set_dialog_mode("")
  pause 0.5
  return

label quest_fall_in_newfall_break_over_home_bedroom_bed:
  "I could definitely sleep for a while longer, but once prone it's hard to get up again..."
  return

label quest_fall_in_newfall_break_over_home_bedroom_door:
  scene black with Dissolve(.07)
  $game.location = "home_hall"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  show flora smile
  with Dissolve(.5)
  flora smile "Hey!"
  mc "Shit, you scared me."
  flora smile "Sorry! I thought you were still asleep."
  mc "Were you spying on me?"
  flora afraid "No! Why would I do that?"
  mc "Why do you have to dress like that?"
  flora confused "What's wrong with it?"
  mc "Nothing... I just don't like it..."
  "It means she's ready to move on."
  "Ready to start a new life."
  flora annoyed "Well, tough shit. I'm going to my internship right after the assembly."
  flora eyeroll "Which, by the way, you'll be late for if you don't hurry up."
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   mc "I'm not going."
#   flora angry "It's mandatory!"
#   mc "What are they going to do? Lock me up?"
#   flora concerned "Maybe they should."
#   mc "Are you being serious right now?"
#   flora concerned "We both know you have a darker side."
#   mc "What's that supposed to mean?"
#   flora thinking "Nothing..."
#   "I can't believe it. [flora] of all people?"
#   mc "So, you're going to start accusing me too?"
#   mc "I was cleared of the assault charges."
#   mc "And I had nothing to do with [lindsey] jumping off the roof."
#   flora worried "I'm sorry. Forget I said anything, okay?"
#   flora worried "But you really should go to the assembly. You haven't left the house in weeks."
#   "She's right. There's no point in hiding."
#   "If anything, it'll only make me look more suspicious..."
#   mc "Fine, I guess."
#   flora blush "Good. See you there."
#   window hide
#   show flora blush at disappear_to_left
#   pause 0.5
#   window auto
#   "I better hurry. The only thing worse than being suspected of crimes, is being suspected of crimes and late."
#   window hide
    pass
  else:
    show flora eyeroll at move_to(.75)
    menu(side="left"):
      extend ""
      "?flora.love>=10@[flora.love]/10|{image=flora contact_icon}|{image=stats love_3}|\"Can we go together?\"":
        show flora eyeroll at move_to(.5)
        mc "Can we go together?"
        flora blush "Do you want to?"
        mc "Sure. We rarely do."
        flora thinking "That's true. But you better hurry up, then."
        mc "I'm ready to go."
        flora worried "You haven't even had breakfast..."
        "She's pretty adorable when she's trying to take care of me."
        mc "It's fine. I'll just have an early lunch."
        flora blush "Okay! Let's go!"
        mc "Are you nervous?"
        flora laughing "No! Why would I be?"
        mc "I don't know..."
        flora laughing "Just going to school together. Nothing weird about it, right?"
        mc "Nothing weird at all."
        "It would be weird if we held hands, though."
        "That's one thing we'll never be able to do in public."
        $flora.love+=2
        flora flirty "Come on, then!"
        mc "Right behind you!"
        $quest.fall_in_newfall["right_behind_you"] = True
        window hide
        show flora flirty at disappear_to_left
        pause 0.5
      "\"I'm sorry. You look great.\"":
        show flora eyeroll at move_to(.5)
        mc "I'm sorry. You look great."
        flora angry "I don't have time for your bullshit..."
        mc "It wasn't bullshit! You do look good."
        mc "This style suits you."
        flora concerned "Really, now?"
        flora concerned "I can tell when you're lying."
        mc "It's really a me-problem. Just... ignore me."
        flora confident "That, I don't have a problem with."
        flora confident "Did I mention you'll be late?"
        window hide
        show flora confident at disappear_to_left
        pause 0.5
        window auto
        "Damn it. I really need to work on my poker face."
        window hide
  $quest.fall_in_newfall.started = False
  $quest.fall_in_newfall.start("assembly")
  return

label quest_fall_in_newfall_assembly_school_entrance:
  "Wet, cold, and the world dying around you..."
  "Autumn really is the worst."
  return

label quest_fall_in_newfall_assembly_school_entrance_path:
  "As much as I would love getting my shoes and pants wet, now is not the time for such activities."
  return

label quest_fall_in_newfall_assembly_school_entrance_door:
  $quest.fall_in_newfall.advance("late")
  $quest.fall_in_newfall["assembly_conversations"] = set()
  jump goto_school_homeroom

label quest_fall_in_newfall_assembly_school_entrance_bus_stop:
  "I'm already late. Better hurry up."
  return

label quest_fall_in_newfall_assembly_school_entrance_right_path:
  "As much as I would love getting my shoes and pants wet, now is not the time for such activities."
  return

label quest_fall_in_newfall_late_school_homeroom:
  "Everyone's already here, but where is [mrsl]?"
  "I thought for sure I'd be late..."
  return

label quest_fall_in_newfall_late_school_homeroom_rope:
  "I'm not going to swing out of the window like fucking Tarzan with everyone watching..."
  return

label quest_fall_in_newfall_late_school_homeroom_maya:
  show maya bored with Dissolve(.5)
  "[maya]... damn... I forgot how cute she is."
  "Of course, she's always been like a wildfire sweeping through\nthe halls."
  "Even the jocks steer clear of her, and you can't really blame them."
  "It is said that being in her mere presence can result in a third degree burn."
  "Naturally, I was always too much of a pussy to ever talk to her..."
  "...but maybe this time I'll try my luck?"
  "New semester, new me."
  mc "Hey, [maya]?"
  maya bored "If you're looking for a blowjob, get in line."
  mc "I just wanted to talk..."
  maya bored "That'll cost you extra."
  "Hmm... if I remember correctly, she's into computers and stuff."
  "Maybe that's something we have in common?"
  mc "I have a computer question."
  maya bored "That will still cost you extra."
  mc "Well, what do you want, then?"
  maya dramatic "I want what every girl wants! Love, understanding, and nine inches of throbbing, bulging... kindness."
  maya sarcastic "But I'll settle for a ball of yarn."
  show maya sarcastic at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.owned_item('ball_of_yarn')@|{image=items ball_of_yarn}|\"In that case, it's your lucky day.\"":
      show maya sarcastic at move_to(.5)
      mc "In that case, it's your lucky day."
      window hide
      $mc.remove_item("ball_of_yarn")
      pause 0.25
      window auto show
      show maya thinking with dissolve2
      maya thinking "..."
      maya thinking "Did you steal this?"
      mc "I don't know what you're talking about."
      maya thinking "Oh, yeah? Don't you think I recognize my own yarn?"
      mc "Well, let's just say I found it, and now am returning it."
      mc "You're up one ball of yarn regardless."
      maya afraid "What did you do with it?"
      mc "Nothing!"
      maya afraid "It's smaller than I remember it..."
      show maya afraid at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Is that a euphemism?\"":
          show maya afraid at move_to(.5)
          mc "Is that a euphemism?"
          maya cringe "You can't use big words around me like that, my last brain cell is already working overtime."
          mc "Very funny. I know you're smart."
          maya dramatic "Oh, thank god! The validation I always needed!"
          maya dramatic "No longer just an object of sexual desire! I've reached peak womanhood!"
          maya sarcastic "So, anyway, what was the question?"
        "\"It's just as big, if not bigger.\"":
          show maya afraid at move_to(.5)
          mc "It's just as big, if not bigger."
          maya skeptical "Are you saying you found some string and added it to my ball?"
          mc "Yes, that's exactly what I'm saying."
          maya confident "That's all sorts of twisted, and exactly what I'm looking for in a man.{space=-15}"
          mc "Really?"
          maya blush "Absolutely! You've just been moved to the front of the blowjob line."
          maya blush "Please drop your pants and let's get started."
          "Hmm... I once saw Tanner drop his pants in front of her like that, and everyone just laughed."
          mc "Not falling for that."
          maya confident "Good move."
          maya confident "So, anyway, what was the question?"
      mc "Err, forget about it..."
      maya eyeroll "Already did."
      maya neutral "But there is one thing that I find interesting."
      mc "What's that?"
      maya neutral "I didn't know you could speak."
      "Huh... I guess I used to be the quiet kind..."
      show maya neutral at move_to(.75)
      menu(side="left"):
        extend ""
        "?mc.charisma>=10@[mc.charisma]/10|{image=stats cha}|\"I do all kinds of tricks.\"":
          show maya neutral at move_to(.5)
          mc "I do all kinds of tricks."
          maya thinking "Are cheesy lines one of them?"
          mc "Come on, you can't set me up like that and not expect a little comeback."
          maya flirty "We can do cum-on-the-back, but only if you clean me up after."
          "Fuck me..."
          mc "I'll be a gentleman and just let you swallow."
          maya kiss "Well, gee golly! You're such a giver!"
          mc "I'll give it to you, if that's what you want."
          maya sarcastic "Maybe in your dreams tonight."
          mc "Or yours."
          maya cringe "I don't dream ever since the lobotomy."
          "Not sure if she's serious or not, but I'm still kind of into it..."
          mc "Well, your loss."
          maya dramatic "You have no idea!"
          maya dramatic "Woe be to my sleeping brain not to have sex dreams about you..."
          maya dramatic "I guess you'll just have to dream enough for us both."
          mc "Shouldn't be a problem."
          $maya.lust+=1
          maya sarcastic "Didn't think so."
          window hide
          hide maya with Dissolve(.5)
        "\"It's hard when you have me\nall tongue tied.\"":
          show maya neutral at move_to(.5)
          mc "It's hard when you have me all tongue tied."
          maya cringe "Am I the cat that got your tongue?"
          "Crap, that was not as smooth as I was hoping it would be..."
          mc "Err, I just meant because you're so beautiful!"
          maya sarcastic "Oh, thank you for explaining it to me!"
          maya sarcastic "The basics of the English language escape my tiny, sex-addled brain, you know?"
          maya sarcastic "All the brain power I have goes straight to my tits and ass, so men like you can ogle me to your dick's content."
          mc "I didn't mean it like that!"
          maya dramatic "You and your dick can't fool me!"
          maya dramatic "I know what you menfolk think about!"
          mc "..."
          $maya.lust-=1
          maya bored "Anyway..."
          window hide
          hide maya with Dissolve(.5)
          window auto
          "Welp. Safe to say this relationship is off to a rocky start."
        "\"We've been in the same grade\nsince elementary school...\"":
          show maya neutral at move_to(.5)
          mc "We've been in the same grade since elementary school..."
          maya skeptical "Are you sure?"
          mc "...yes."
          maya skeptical "I think I would remember that."
          mc "Well, it's true."
          maya confident "So, what, you've learned how to speak since then?"
          mc "I've always known how! I just... was never good about talking to you.{space=-10}"
          maya thinking "Why would you want to?"
          maya thinking "Don't you know I'm the hellspawn of Satan and the Real Housewives?{space=-30}"
          mc "Fuck the rumors."
          maya afraid "Gasp!"
          maya flirty "Beware, ye mute man! Get too close, and I might bite your dick off."
          "Yikes..."
          mc "I'll, err... keep that in mind."
          window hide
          hide maya with Dissolve(.5)
          window auto
          "Goddamn, close encounters of the craziest kind."
    "\"I don't have any on me...\"":
      show maya sarcastic at move_to(.5)
      mc "I don't have any on me..."
      maya bored "And here I got all excited to be your own personal computer question{space=-25}\nbimbo for nothing..."
      maya bored "Back to the end of the line for you."
      window hide
      hide maya with Dissolve(.5)
  $quest.fall_in_newfall["assembly_conversations"].add("maya")
  return

label quest_fall_in_newfall_late_school_homeroom_isabelle:
  if quest.kate_stepping.finished:
    show isabelle concerned with Dissolve(.5)
    isabelle concerned "..."
    "She used to be so confident in herself, but now she looks nervous."
    "Did [kate] really break her spirit?"
    mc "Hey, [isabelle]."
    isabelle concerned "What do you want?"
    "Ouch. Cold."
    mc "Nothing, I was just saying hello..."
    isabelle concerned_left "..."
    window hide
    show isabelle concerned_left at move_to(.75)
    pause 0.25
    show kate neutral_right at Transform(xalign=.25) with Dissolve(.5)
    window auto
    kate neutral_right "Is he bothering you?"
    isabelle concerned_left "Yes."
    "Wait, what?"
    mc "..."
    show isabelle concerned
    kate confident "You look confused, [mc]."
    mc "What kind of twisted game is this?"
    kate laughing "What do you mean? We're friends now."
    kate laughing "We patched things up during the break."
    kate smile_right "Right, [isabelle]?"
    isabelle neutral "Right."
    kate smile_right "We went over some things, and sorted out our differences."
    kate smile_right "It took some effort, but I made her see things from my perspective."
    kate gushing "Oh! And, by the way, we're dating now."
    kate gushing "So, keep your creepy eyes off my girlfriend. Got it?"
    isabelle blush "Thanks, babe."
    "What the hell...?"
    mc "What about Chad?"
    kate gushing "[isabelle] doesn't mind me dating him too."
    kate gushing "Right, cutie?"
    isabelle blush "Right."
    "[isabelle] is completely docile. What the hell happened to her?"
    "[kate] did say she would break her, but... I thought it would take longer than a couple weeks."
    window hide
    hide kate
    hide isabelle
    with Dissolve(.5)
  elif quest.isabelle_dethroning.finished:
    if quest.isabelle_dethroning["isabelle_caught"]:
      show isabelle annoyed with Dissolve(.5)
      isabelle annoyed "Don't talk to me."
      "Ouch. She's still pissed."
      mc "I didn't even say anything..."
      isabelle annoyed "Good. Keep it that way."
      window hide
      hide isabelle with Dissolve(.5)
      $isabelle["romance_disabled"] = True
      $game.notify_modal(None,"Love or Lust","Romance with [isabelle]:\nDisabled.",wait=5.0)
      pause 0.25
      window auto
      "Probably shouldn't have gotten caught with my dick in [kate], huh?"
      "Damn it. I'll have to fix this later."
      window hide
    else:
      show isabelle laughing with Dissolve(.5)
      isabelle laughing "Heya!"
      "Her smile always makes my heart a little bit lighter..."
      isabelle confident "I've been trying to call you."
      show isabelle confident at move_to(.25)
      menu(side="right"):
        extend ""
        "?quest.lindsey_motive.actually_finished@|{image=lindsey contact_icon}|{image=stats love}|\"Have you tried\ncalling [lindsey]?\"":
          show isabelle confident at move_to(.5)
          mc "Have you tried calling [lindsey]?"
          isabelle concerned "I haven't."
          mc "Why not?"
          isabelle concerned "Because... we weren't that close, I guess? I didn't want to intrude."
          isabelle skeptical "Besides, what's with the hostility?"
          mc "Ugh, I'm sorry..."
          mc "I haven't slept much since that day..."
          isabelle sad "I can imagine."
          isabelle sad "I was just worried about you."
          mc "Thanks... I just can't get her out of my head..."
          mc "She had such a big impact on my life and self-esteem..."
          mc "I think I may have feelings for her..."
          $lindsey.love+=3
          if quest.isabelle_locker.actually_finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.actually_finished):
            isabelle cringe "Feelings?"
            isabelle cringe "What about us?"
            show isabelle cringe at move_to(.75)
            menu(side="left"):
              extend ""
              "\"I still like you...\"":
                show isabelle cringe at move_to(.5)
                mc "I still like you..."
                mc "...but I guess almost losing her made me realize how much she means to me..."
                isabelle sad "And here I thought we had something special..."
                mc "We do!"
                $isabelle.love-=5
                $isabelle.lust-=2
                isabelle angry "Doesn't seem like it."
                isabelle angry "Thanks for letting me know, I suppose."
                window hide
                hide isabelle with Dissolve(.5)
                window auto
                "Fuck me! Why do I always have to run my mouth?"
                $quest.fall_in_newfall["run_my_mouth"] = True
                $quest.fall_in_newfall["assembly_conversations"].add("isabelle")
                return
              "\"I didn't mean it like that!\"":
                show isabelle cringe at move_to(.5)
                mc "I didn't mean it like that!"
                isabelle skeptical "What did you mean, then?"
                mc "I meant that I feel like I need to protect her, if that makes sense."
                isabelle skeptical "So, nothing romantic?"
                "Not that she needs to know about..."
                mc "I want to care for her, is what I meant. Like you would care for a wounded animal."
                $isabelle.love-=1
                isabelle skeptical "Mhm..."
                mc "Sorry, I should have been more clear..."
          else:
            isabelle concerned "Oh?"
            mc "...sorry, I'm sleep deprived. Ignore me."
          isabelle concerned "No, I get it."
          isabelle concerned "She's sweet, and..."
          mc "What?"
          isabelle sad "I mean..."
        "\"Sorry, I needed some time.\"":
          show isabelle confident at move_to(.5)
          mc "Sorry, I needed some time."
          isabelle sad "I guess you guys were closer than I realized."
          mc "She didn't deserve this."
          isabelle sad "Yeah, I can't imagine what she went through."
      isabelle sad "She seemed so happy and future-focused. I still can't believe she would do that."
      mc "She didn't jump."
      isabelle thinking "What do you mean?"
      mc "Well, she did jump, but... she wasn't herself that night."
      isabelle thinking "Yeah, I imagine with a depression that runs that deep—"
      mc "No! I mean she wasn't herself, as in, she was in a trance of some sort!{space=-45}"
      isabelle thinking "What are you suggesting?"
      mc "She wasn't depressed or suicidal. She was sort of... hypnotized?"
      isabelle sad "I know it's hard when people you're close to..."
      isabelle sad "When my sister—"
      mc "This wasn't a suicide attempt!" with vpunch
      isabelle concerned "..."
      if quest.lindsey_motive.actually_finished:
        mc "Remember that doll?"
        mc "It was all a scheme. Someone did this to her."
        isabelle skeptical "Are you saying it was an attempted murder?"
        mc "Yes!"
        isabelle skeptical "Did you tell the police?"
        mc "I did, but they didn't believe me..."
        isabelle skeptical "I don't know what to say..."
        "[isabelle] doesn't believe me either. It's written all over her face."
        isabelle concerned "Look, I believe you, but this was clearly a traumatic event for you."
      else:
        isabelle concerned "How do you know?"
        mc "I just have a feeling something isn't right."
        isabelle concerned "I don't know what to say..."
      isabelle concerned "The mind can play tricks on you. I know that from experience."
      isabelle concerned "Maybe it would help to talk to someone?"
      mc "Yeah, maybe it would."
      isabelle sad "I can tell you're angry at me."
      isabelle sad "But I care about you, and I would hate for you to go off the deep end.{space=-30}"
      "Maybe it's too big of an ask for her to believe me..."
      "She's had her fair share of grief, and is probably just trying to help."
      mc "Thank you for having my back."
      isabelle blush "Of course."
      window hide
      hide isabelle with Dissolve(.5)
      window auto
      "I guess I'll just have to figure this out on my own."
  $quest.fall_in_newfall["assembly_conversations"].add("isabelle")
  return

label quest_fall_in_newfall_late_school_homeroom_kate:
  show kate sad with Dissolve(.5)
  kate sad "..."
  "A lot has changed in just a few weeks."
  "[kate] doesn't look so scary anymore."
  "Perhaps, that's what happens when actual grief hits you?"
  "Or when you lose your spot on the cheerleader squad?"
  "..."
  "Or perhaps it's just the fact that her air of invulnerability broke\nfor me..."
  "She doesn't show it, but I'm sure the ghost pig still haunts her memories."
  window hide
  hide kate with Dissolve(.5)
  $quest.fall_in_newfall["assembly_conversations"].add("kate")
  return

label quest_fall_in_newfall_late_school_homeroom_flora:
  show flora annoyed with Dissolve(.5)
  if quest.fall_in_newfall["right_behind_you"]:
    flora annoyed "What?"
    mc "I like the way you... smile."
    flora annoyed "I'm not smiling."
    mc "You could be."
    flora eyeroll "I could also smack you."
    mc "And I would like it."
    flora cringe "Why do you always have to make things weird?"
  else:
    flora annoyed "I see you made it."
    mc "You don't have to look so annoyed..."
    flora eyeroll "Well, I woke up an hour earlier than you to get ready."
    flora eyeroll "And somehow you still make it on time."
  mc "It's a gift I have."
  flora annoyed "The only gift you have is being effortlessly obnoxious."
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.charisma>=6@[mc.charisma]/6|{image=stats cha}|\"And your gift is being\neffortlessly adorable.\"":
      show flora annoyed at move_to(.5)
      mc "And your gift is being effortlessly adorable."
      flora afraid "Not so loud!"
      mc "Why not? It's not like anyone's paying attention."
      mc "Hey, everyone! Doesn't [flora] look nice today?"
      flora embarrassed "Don't listen to him! He's just being annoying!"
      mc "See? No one even noticed."
      $flora.lust+=2
      flora skeptical "I don't want to hear another word out of your mouth."
    "\"You're not wrong, you're just an—\"":
      show flora annoyed at move_to(.5)
      mc "You're not wrong, you're just an—"
      flora angry "I dare you to finish that sentence."
      "She looks serious. I better not push my luck."
  window hide
  hide flora with Dissolve(.5)
  $quest.fall_in_newfall["assembly_conversations"].add("flora")
  return

label quest_fall_in_newfall_late_school_homeroom_door:
  "Stay and pray, was it? Hmm... I need to play more FPS games."
  return

label quest_fall_in_newfall_late:
  show mrsl excited at appear_from_right
  pause 0.5
  mrsl excited "Good morning, everyone, and welcome to the new semester!"
  "How can she be so chipper after what happened to [lindsey]?"
  mrsl concerned "I'm sure you all know by now that one of the students here got hurt right before the break."
  mrsl concerned "I spoke to [lindsey]'s mother this morning, and [lindsey] is still in a coma."
  mrsl concerned "She has our thoughts and prayers."
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   "Prayers that she'll not wake up again..."
    pass
  mrsl confident "Now, the classes will continue as usual, but this semester, everyone{space=-5}\nhas to pick an extracurricular activity!"
  mrsl confident "There are several options, but I'll provide you with more information on that later."
  mrsl flirty "If anyone has a question, stick around after the assembly."
  mrsl flirty "If not, you're free to get on with your day!"
  window hide
  $quest.fall_in_newfall.advance("stick_around")
  hide mrsl with Dissolve(.5)
  window auto
  "She spoke to [lindsey]'s mom?"
  "I thought her mother blocked all calls, but apparently just mine..."
  "Maybe [mrsl] knows more."
  return

label quest_fall_in_newfall_stick_around:
  show mrsl excited with Dissolve(.5)
  mrsl excited "How can I help you today, [mc]?"
  mc "It's about [lindsey]."
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   mrsl annoyed "Oh, well. What is it?"
#   "Not sure what to make of her expression..."
    pass
  elif quest.lindsey_motive.actually_finished:
    mrsl afraid "I'm very sorry. I know you were close."
    mc "Thanks..."
  mc "I was wondering if you knew anything else about her condition?"
  mc "I've been trying to call, but..."
  mrsl concerned "[lindsey]'s parents are still very upset. I think you should give them some space."
  mrsl concerned "In fact, her mother asked me to let everyone know that they don't want visitors."
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   "That works for me. As long as I don't have to see that bitch, I'm good."
#   mc "Will you let me know if she wakes up?"
#   mrsl thinking "I suppose there will be an announcement. You'll know when everyone{space=-20}\nelse knows."
#   mc "Fair enough, I guess."
#   window hide
#   hide mrsl with Dissolve(.5)
#   window auto
#   "Hopefully, I can just get on with my life and forget everything about [lindsey]..."
    pass
  elif quest.lindsey_motive.actually_finished:
    mc "I need to see her..."
    mrsl concerned "I'm sorry I can't help you there."
    window hide
    hide mrsl with Dissolve(.5)
    window auto
    "If only I could see [lindsey]..."
    "I guess I'll have to be patient or something."
  else:
    mc "Will you at least let me know if she wakes up?"
    mrsl thinking "I suppose there will be an announcement. You'll know when everyone{space=-20}\nelse knows."
    mc "Fair enough, I guess."
    window hide
    hide mrsl with Dissolve(.5)
    window auto
    "Hopefully, [lindsey] will wake up soon and make a full recovery."
  window hide
  $quest.fall_in_newfall.finish()
  $quest.fall_in_newfall["finished_today"] = quest.fall_in_newfall["finished_this_week"] = True
  return
