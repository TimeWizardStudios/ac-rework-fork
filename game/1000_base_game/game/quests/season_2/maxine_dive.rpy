image call_master_splinter_pizza = LiveComposite((0,0),(63,-53),Transform(LiveComposite((128,128),(6,6),Transform("hidden_number contact_icon_alt",size=(116,116))),zoom=0.69),(149,-38),Transform("phone apps contact_info call",zoom=0.5))
image pizza_boxes = LiveComposite((164,80),(5,39),"home kitchen pizza_box1",(0,25),"home kitchen pizza_box2",(11,13),"home kitchen pizza_box3",(10,0),"home kitchen pizza_box4")
image cardboard_duct_tape_combine = LiveComposite((280,130),(0-5,35),"items cardboard",(96,35),Transform("actions combine",zoom=0.85),(184,35),"items duct_tape")
image armor_interact = LiveComposite((180,130),(0,35),"items armor",(95,37),Transform("actions interact",zoom=0.85))
image banana_milk_consume = LiveComposite((180,130),(0,35),"items bottle banana_milk",(95,37),Transform("actions consume",zoom=0.85))
image pepelepsi_consume = LiveComposite((180,130),(0,35),"items bottle pepelepsi",(95,37),Transform("actions consume",zoom=0.85))
image salted_cola_consume = LiveComposite((180,130),(0,35),"items bottle salted_cola",(95,37),Transform("actions consume",zoom=0.85))
image seven_hp_consume = LiveComposite((180,130),(0,35),"items bottle seven_hp",(95,37),Transform("actions consume",zoom=0.85))
image strawberry_juice_consume = LiveComposite((180,130),(0,35),"items bottle strawberry_juice",(95,37),Transform("actions consume",zoom=0.85))
image water_bottle_consume = LiveComposite((180,130),(0,35),"items bottle water_bottle",(95,37),Transform("actions consume",zoom=0.85))
image interact_right_mid = LiveComposite((103+50,108),(0,0),"actions interact")
image interact_mid_top = LiveComposite((103,108-60),(0,0),"actions interact")


init python:
  class Quest_maxine_dive(Quest):

    title = "The Dive"

    class phase_1_weapons:
      description = "A deep dive into the unknown."
      hint = "Considering that you're helpless without a weapon, now's the time to find one."
      def quest_guide_hint(quest):
        if mc.owned_item("fire_axe") and not mc.owned_item("machete"):
          return "Take the machete in the home hallway."
        elif mc.owned_item("machete") and not mc.owned_item("fire_axe"):
          return "Take the fire axe in the roof landing."
        else:
          return "Take both the fire axe in the roof landing, and the machete in the home hallway."

    class phase_2_shield:
      hint = "As the saying goes... if you can't eat them, it's just because you haven't found the right pan."
      def quest_guide_hint(_quest):
        if quest.flora_cooking_chilli.actually_finished and not quest.maya_spell.finished:
          return "Take the cooking pot in the home kitchen."
        else:
          return "Take the cooking pot in the cafeteria."

    class phase_3_armor:
      hint = "This is a really simple 6-step guide to success. Order pizza. Wait for pizza. Eat pizza. Faint from pizza. Use duct tape on pizza remains. Become one with pizza. It's not that hard, is it?"
      def quest_guide_hint(quest):
        if mc.owned_item("armor"):
          return "Interact with the armor in your inventory."
        elif mc.owned_item("duct_tape"):
          # return "Combine the cardboard with the duct tape."
          return "Combine the cardboard with the{space=-10}\nduct tape."
        elif mc.owned_item("cardboard") and (7 > game.hour >= 0 or 23 >= game.hour > 18):
          return "Go to sleep."
        elif mc.owned_item("cardboard"):
          return "Take the duct tape from the teacher's desk in the English classroom."
        elif home_kitchen["pizza_boxes"]:
          return "Interact with the pizza boxes in the home kitchen."
        elif quest["pizza_ordered"]:
          return "Wait 1 hour."
        elif quest["not_a_hot_girl"] and mc.money < 60:
          return "Gather $60."
        else:
          return "Call Master Splinter's Pizza on your phone."

    class phase_4_dive:
      hint = "Armor donned and weapon at the ready. A real knight would ride into wilderness, but you'll just have to walk."
      quest_guide_hint = "Go to the park."

    class phase_5_line:
      hint = "You can't handle [maya] in your current state. Get some rest first."
      def quest_guide_hint(quest):
        if 7 > game.hour >= 0 or 23 >= game.hour > 18:
          return "Go to sleep."
        else:
          return "Quest [maya]."

    class phase_6_milk:
      def hint(quest):
        if quest["kitchen_milk"] or quest["cafeteria_milk"]:
          # return "Who would even order milk online? And on the black market? And for $66? When you can just get it from the cafeteria or, you know, the fridge?"
          return "Who would even order milk online? And on the black market?{space=-10}\nAnd for $66? When you can just get it from the cafeteria or,\nyou know, the fridge?"
        elif quest["black_market_milk"]:
          # return "Who even orders milk online? And on the black market? And for $66? When you could just get it from the cafeteria or, you know, the fridge?"
          return "Who even orders milk online? And on the black market? And for $66? When you could just get it from the cafeteria or,\nyou know, the fridge?"
      def quest_guide_hint(_quest):
        if quest.maxine_dive["kitchen_milk"]:
          return "Take the milk from the mini fridge in the home kitchen."
        elif quest.maxine_dive["cafeteria_milk"]:
          return "Go to the cafeteria."
        elif quest.maxine_dive["black_market_milk"]:
          if quest.maxine_dive["shady_site_pulled_up"] and mc.money >= 66:
            return "Buy a bottle of goat milk from the black market."
          elif quest.maxine_dive["shady_site_pulled_up"]:
            return "Gather $66."
          elif home_computer["visited"] and quest.maya_sauce["bedroom_taken_over"]:
            return "Access the black market from the computer in the hallway."
          elif home_computer["visited"]:
            return "Access the black market from the computer in your bedroom."
          elif quest.maya_sauce["bedroom_taken_over"]:
            return "Interact with the computer in the home hallway."
          else:
            return "Interact with the computer in your bedroom."

    class phase_7_package:
      hint = "Goat things come to those that wait. And to those that know how to milk it."
      def quest_guide_hint(quest):
        if home_computer["goat_ordered_today"] or home_computer["milk_ordered_today"]:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."
        elif home_computer["milk_ordered"]:
          return "Take the mysterious package in the home kitchen, and then consume it."
        elif quest["hard_part"] and mc.owned_item("empty_bottle"):
          return "Use an empty bottle on [shrub]."
        elif quest["hard_part"] and mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle")):
          # return "Consume the bottle of " + mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle")).replace("_"," ").replace("hp","HP").replace(" bottle","") + "."
          return "Consume the bottle of " + mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle")).replace("_"," ").replace("hp","HP").replace(" bottle","") + ".{space=-10}"
        elif quest["hard_part"] and not home_kitchen["water_bottle_taken"]:
          return "Take the water bottle in the home kitchen."
        elif quest["hard_part"] and mc.money >= 100:
          # return "Buy a water bottle from the vending machine in the first hall."
          return "Buy a water bottle from the vending machine in the first hall.{space=-10}"
        elif quest["hard_part"]:
          return "Gather $100."
        else:
          return "Interact with the goat in the home kitchen."

    class phase_8_trade:
      hint = "This is not a trick of any kind. Just take the yarn."
      # quest_guide_hint = "Take the ball of yarn on the leftmost desk in the homeroom."
      quest_guide_hint = "Take the ball of yarn on the\nleftmost desk in the homeroom.{space=-5}"

    class phase_9_dive_2_electric_boogaloo:
      hint = "A conversation with [maxine] is never a walk in the park. Well, almost never."
      def quest_guide_hint(quest):
        if 7 > game.hour >= 0 or 23 >= game.hour > 18:
          return "Go to sleep."
        else:
          return "Quest [maxine] in the park."

    class phase_10_cave:
      hint = "Discussing, inspecting, and taking things. The three pillars of exploring ancient caves and other underground places that are totally safe and stuff."
      def quest_guide_hint(quest):
        if all(interactable in quest["cave_interactables"] for interactable in ("ancient_book","ancient_chastity_belt","ancient_chests","crown","glowing_rune_door","maxine","portrait","ship_wheel")):
          return "Take the locked box."
        elif quest["cave_interactables"]:
          # return "Interact with all interactables in the cave."
          return "Interact with all interactables in{space=-5}\nthe cave."
        else:
          return "Quest [maxine] again."

    class phase_1000_done:
      # description = "So, that just happened. Too bad no one will ever believe you."
      description = "So, that just happened.\nToo bad no one will ever\nbelieve you."


init python:
  class Event_quest_maxine_dive(GameEvent):

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_park" and quest.maxine_hook.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and quest.maya_spell.finished and not quest.maxine_dive.started:
        game.events_queue.append("quest_maxine_dive_start")
      elif new_location == "school_roof_landing" and quest.maxine_dive == "weapons" and not mc.owned_item("fire_axe") and not quest.maxine_dive["how_deep"]:
        game.events_queue.append("quest_maxine_dive_weapons_fire_axe_upon_entering")
      elif new_location == "home_hall" and quest.maxine_dive == "weapons" and not mc.owned_item("machete") and not quest.maxine_dive["suitable_weapon"]:
        game.events_queue.append("quest_maxine_dive_weapons_machete_upon_entering")
      elif new_location == ("home_kitchen" if quest.flora_cooking_chilli.actually_finished and not quest.maya_spell.finished else "school_cafeteria") and quest.maxine_dive == "shield" and not quest.maxine_dive["crazier_than_usual"]:
        game.events_queue.append("quest_maxine_dive_shield_upon_entering")
      elif new_location == "school_park" and quest.maxine_dive == "dive":
        game.events_queue.append("quest_maxine_dive_dive")
      elif new_location == "home_kitchen" and quest.maxine_dive == "milk" and quest.maxine_dive["kitchen_milk"] and not quest.maxine_dive["following_in_his_footsteps"]:
        game.events_queue.append("quest_maxine_dive_milk_kitchen_milk_upon_entering")
      elif new_location == "school_cafeteria" and quest.maxine_dive == "milk" and quest.maxine_dive["cafeteria_milk"]:
        game.events_queue.append("quest_maxine_dive_milk_cafeteria_milk")
      elif new_location == "home_computer" and quest.maxine_dive == "milk" and quest.maxine_dive["black_market_milk"] and not quest.maxine_dive["shady_site_pulled_up"]:
        game.events_queue.append("quest_maxine_dive_milk_black_market_milk")
      elif new_location == "home_kitchen" and quest.maxine_dive == "package" and home_computer["goat_ordered"] and not home_computer["goat_ordered_today"] and not quest.maxine_dive["taken_care_of"]:
        game.events_queue.append("quest_maxine_dive_package")

    def on_quest_started(event,quest,silent=False):
      if quest.id == "maxine_dive" and mc.owned_item("fire_axe") and mc.owned_item("machete"):
        game.events_queue.append("quest_maxine_dive_weapons")

    def on_inventory_changed(event,char,item,old_count,new_count,silent=False):
      if "maxine_dive" not in game.quests:
        game.quests["maxine_dive"] = Quest_maxine_dive()
      if (quest.maxine_dive == "weapons" and mc.owned_item("fire_axe") and item.id == "machete"
      or quest.maxine_dive == "weapons" and mc.owned_item("machete") and item.id == "fire_axe"):
        game.events_queue.append("quest_maxine_dive_weapons_taken")
      if home_computer["visited"] and char == black_market and item == "goat" and new_count < old_count:
        black_market.inv.remove(["milk",1])
      elif home_computer["visited"] and char == black_market and item == "milk" and new_count < old_count:
        black_market.inv.remove(["goat",1])

    def on_advance(event):
      if quest.maxine_dive == "armor" and quest.maxine_dive["pizza_ordered"] and game.hour == 19 and not game.location.id.startswith("home_"):
        game.events_queue.append("quest_maxine_dive_armor_advance")
      if quest.maxine_dive == "armor" and quest.maxine_dive["pizza_ordered"] and not home_kitchen["pizza_boxes"] and not mc.owned_item(("cardboard","armor")):
        home_kitchen["pizza_boxes"] = 4

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "maxine_dive" and quest.maxine_dive == "dive" and game.location == "school_park":
        game.events_queue.append("quest_maxine_dive_dive_on_quest_advanced")
      elif quest_advanced.id == "maxine_dive" and quest.maxine_dive == "milk" and quest.maxine_dive["black_market_milk"]:
        black_market.add_item("goat")
        black_market.add_item("milk")

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.maxine_dive == "dive_2_electric_boogaloo" and interactable == "maxine" and mc.energy <= 13)
      or quest.maxine_dive == "cave"):
        return 0

    def on_can_advance_time(event):
      if quest.maxine_dive == "cave":
        return False

    def on_quest_guide_maxine_dive(event):
      rv = []

      if quest.maxine_dive == "weapons":
        if not mc.owned_item("fire_axe"):
          rv.append(get_quest_guide_path("school_roof_landing", marker = ["fire_axe_cropped@.2"]))
          rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_fire_axe", marker = ["actions take"], marker_sector = "right_top", marker_offset=(75,-200)))
        if not mc.owned_item("machete"):
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall sword@.7"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_sword", marker = ["actions take"], marker_offset=(-10,-10)))

      elif quest.maxine_dive == "shield":
        if quest.flora_cooking_chilli.actually_finished and not quest.maya_spell.finished:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen cooking_pot@.65"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_cooking_pot", marker = ["actions take"]))
        else:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pot"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_cooking_pot", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (80,-15)))

      elif quest.maxine_dive == "armor":
        if mc.owned_item("armor"):
          rv.append(get_quest_guide_hud("inventory", marker = ["armor_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif mc.owned_item("duct_tape"):
          rv.append(get_quest_guide_hud("inventory", marker = ["cardboard_duct_tape_combine"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif mc.owned_item("cardboard") and (7 > game.hour >= 0 or 23 >= game.hour > 18) and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        elif mc.owned_item("cardboard") and (7 > game.hour >= 0 or 23 >= game.hour > 18):
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        elif mc.owned_item("cardboard"):
          rv.append(get_quest_guide_path("school_english_class", marker = ["teacher_desk@.3"]))
          rv.append(get_quest_guide_marker("school_english_class", "school_english_class_desk", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (410,50)))
        elif home_kitchen["pizza_boxes"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["pizza_boxes@.6"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_pizza_boxes", marker = ["actions interact"], marker_offset = (0,-10)))
        elif quest.maxine_dive["pizza_ordered"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))
        elif quest.maxine_dive["not_a_hot_girl"] and mc.money < 60:
          rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$60{/}.", marker_offset = (500,0)))
        else:
          rv.append(get_quest_guide_hud("phone", marker=["call_master_splinter_pizza","$\n\n\n\n{space=-30}Call {color=#48F}Master\n{space=-30}Splinter's Pizza{/}."]))

      elif quest.maxine_dive == "dive":
        rv.append(get_quest_guide_path("school_park", marker = ["actions go"]))

      elif quest.maxine_dive == "line":
        if (7 > game.hour >= 0 or 23 >= game.hour > 18) and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        elif 7 > game.hour >= 0 or 23 >= game.hour > 18:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        else:
          rv.append(get_quest_guide_char("maya", marker = ["maya contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "maya", marker = ["actions quest"]))

      elif quest.maxine_dive == "milk":
        if quest.maxine_dive["kitchen_milk"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["kitchen_counter@.175"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_mf", marker = ["actions take"]))
        elif quest.maxine_dive["cafeteria_milk"]:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["actions go"]))
        elif quest.maxine_dive["black_market_milk"]:
          if quest.maxine_dive["shady_site_pulled_up"] and mc.money >= 66 and game.location == "home_computer":
            rv.append(get_quest_guide_hud("time", marker="$\nBuy a bottle\nof {color=#48F}goat milk{/}.", marker_offset = (471+(len(str(mc.money))*10),0)))
          elif quest.maxine_dive["shady_site_pulled_up"] and mc.money >= 66 and quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall monitor@.5"]))
            rv.append(get_quest_guide_marker("home_hall","home_bedroom_computer", marker = ["actions black_market"], marker_offset = (-10,-10)))
          elif quest.maxine_dive["shady_site_pulled_up"] and mc.money >= 66:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
            rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions black_market"]))
          elif quest.maxine_dive["shady_site_pulled_up"]:
            rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$66{/}.", marker_offset = (471+(len(str(mc.money))*10),0)))
          elif home_computer["visited"] and quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall monitor@.5"]))
            rv.append(get_quest_guide_marker("home_hall","home_bedroom_computer", marker = ["actions black_market"], marker_offset = (-10,-10)))
          elif home_computer["visited"]:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
            rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions black_market"]))
          elif quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall monitor@.5"]))
            rv.append(get_quest_guide_marker("home_hall","home_bedroom_computer", marker = ["actions interact"], marker_offset = (-10,-10)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
            rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))

      elif quest.maxine_dive == "package":
        if (home_computer["goat_ordered_today"] or home_computer["milk_ordered_today"]) and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif home_computer["goat_ordered_today"] or home_computer["milk_ordered_today"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        elif home_computer["milk_received"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
        elif home_computer["milk_ordered"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.65"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_milk", marker = ["actions take"], marker_offset = (-10,-15)))
        elif quest.maxine_dive["hard_part"] and mc.owned_item("empty_bottle"):
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen goat@.25"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_goat", marker = ["items bottle empty_bottle"], marker_offset = (50,110)))
        elif quest.maxine_dive["hard_part"] and mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle")):
          rv.append(get_quest_guide_hud("inventory", marker = [mc.owned_item(("banana_milk","pepelepsi","salted_cola","seven_hp","strawberry_juice","water_bottle")) + "_consume"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif quest.maxine_dive["hard_part"] and not home_kitchen["water_bottle_taken"]:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen water_bottle@.8"]))
          rv.append(get_quest_guide_marker("home_kitchen", "kitchen_water_bottle", marker = ["actions take"], marker_offset = (-5,-5)))
        elif quest.maxine_dive["hard_part"] and mc.money >= 100:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall vending@.2"]))
          rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_vending", marker = ["$Buy a{space=-50}","items bottle water_bottle","${space=50}"]))
        elif quest.maxine_dive["hard_part"]:
          rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$100{/}.", marker_offset = (471+(len(str(mc.money))*10),0)))
        else:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen goat@.25"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_goat", marker = ["actions interact"], marker_offset = (50,110)))

      elif quest.maxine_dive == "trade":
        rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom yarn@.85"]))
        rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker = ["actions take"], marker_offset = (40,-15)))

      elif quest.maxine_dive == "dive_2_electric_boogaloo":
        if (7 > game.hour >= 0 or 23 >= game.hour > 18) and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        elif 7 > game.hour >= 0 or 23 >= game.hour > 18:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        else:
          rv.append(get_quest_guide_path("school_park", marker = ["maxine contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_park", "maxine", marker = ["actions quest"], marker_offset = (55,0)))

      elif quest.maxine_dive == "cave":
        if all(interactable in quest.maxine_dive["cave_interactables"] for interactable in ("ancient_book","ancient_chastity_belt","ancient_chests","crown","glowing_rune_door","maxine","portrait","ship_wheel")):
          rv.append(get_quest_guide_marker("school_cave", "school_cave_locked_box", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (140,0)))
        elif quest.maxine_dive["cave_interactables"]:
          if "ancient_book" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_ancient_book", marker = ["interact_right_mid"], marker_sector = "right_mid", marker_offset = (10,60)))
          if "ancient_chastity_belt" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_ancient_chastity_belt", marker = ["actions interact"], marker_offset = (40,0)))
          if "ancient_chests" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_ancient_chests", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (330,0)))
          if "crown" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_crown", marker = ["actions interact"], marker_offset = (-5,-10)))
          if "glowing_rune_door" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_glowing_rune_door", marker = ["actions interact"], marker_offset = (15,10)))
          if "portrait" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_portrait", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (135,-15)))
          if "ship_wheel" not in quest.maxine_dive["cave_interactables"]:
            rv.append(get_quest_guide_marker("school_cave", "school_cave_ship_wheel", marker = ["interact_mid_top"], marker_sector = "mid_top", marker_offset = (70,90)))
        else:
          rv.append(get_quest_guide_path("school_cave", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_cave", "maxine", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (140,-10)))

      return rv
