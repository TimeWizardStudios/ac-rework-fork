init python:
  class Quest_act_two(Quest):

    title = "Unstarted Quests"

    class phase_1_start:
      pass

    class phase_1000_done:
      description = "I've done it."

    class phase_2000_failed:
      description = "I'm done for."


init python:
  class Event_quest_act_two(GameEvent):

    def on_quest_guide_act_two(event):
      rv = []

      if mc["focus"]:
        rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}"+(quests_by_id[mc["focus"].split("@")[1]].title if "@" in mc["focus"] else quests_by_id[mc["focus"]].title)+"{/}."))

      else:
        ## Touched by an Angel ##
        if quest.fall_in_newfall.finished and not (quest.lindsey_angel.started or quest.lindsey_angel.failed):
          if quest.fall_in_newfall["finished_this_week"]:
            days = quest.fall_in_newfall["finished_this_week_day_tracker"] + 7 - game.day
            if quest.fall_in_newfall["finished_today"]:
              rv.append(get_quest_guide_hud("time", marker="$Wait a {color=#48F}week{/}."))
            elif days > 1:
              rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}" + str(days) + "{/} days."))
            else:
              rv.append(get_quest_guide_hud("time", marker="$Wait a {color=#48F}day{/}."))
          else:
            rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

        ## Big Witch Energy ##
        if quest.fall_in_newfall.finished and not quest.maya_witch.started:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["maya contact_icon@.85"]))

        ## A Single Moment ##
        if quest.isabelle_dethroning.finished and quest.fall_in_newfall.finished and not (quest.kate_moment.started or quest.kate_moment.failed):
          rv.append(get_quest_guide_path("school_first_hall_west", marker = ["kate contact_icon@.85"]))

        ## Suitable Romance ##
        if quest.jacklyn_sweets.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and not quest.jacklyn_romance.started:
          rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "jacklyn", marker = ["actions quest"]))

        ## Spell You Later ##
        if quest.jacklyn_art_focus.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and not quest.maya_spell.started:
          rv.append(get_quest_guide_char("maya", marker = ["maya contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "maya", marker = ["actions quest"]))

        ## A Walk in the Park ##
        if quest.flora_squid.actually_finished and quest.flora_squid["shower"] in ("hug","ass") and quest.fall_in_newfall.finished and not quest.flora_walk.started:
          if game.hour in (7,8,18):
            rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
            rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"], marker_offset = (40,0)))
          elif 8 < game.hour < 17:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
            if game.location == "home_kitchen":
              rv.append(get_quest_guide_hud("time", marker="$\nWait for {color=#48F}[flora]{/} to come\nback from {color=#48F}school{/}."))
          elif game.hour == 17:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
            if game.location == "home_kitchen":
              rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[flora]{/} to leave\nthe {color=#48F}bathroom{/}."))
          elif game.hour == 19:
            rv.append(get_quest_guide_path("home_hall", marker = ["flora contact_icon@.85"]))
            rv.append(get_quest_guide_marker("home_hall", "flora", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (190,-10)))
          elif quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
            rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

        ## Vicious Intrigue ##
        if quest.kate_stepping.finished and quest.fall_in_newfall.finished and not quest.kate_intrigue.started:
          if isabelle.location == "school_english_class":
            rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[isabelle]{/} to leave\nthe {color=#48F}English classroom{/}."))
          else:
            rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

        ## The Grand Gesture ##
        if (quest.isabelle_locker.actually_finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.actually_finished)) and (quest.isabelle_dethroning.finished and quest.isabelle_dethroning["isabelle_caught"]) and quest.fall_in_newfall.finished and not quest.isabelle_gesture.started:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))

        ## Don Juan Quixote ##
        if quest.fall_in_newfall.finished and quest.maya_spell.finished and not quest.maya_quixote.started:
          if game.hour in (7,8,18):
            rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
            rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"], marker_offset = (40,0)))
          elif 8 < game.hour < 17:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
            if game.location == "home_kitchen":
              rv.append(get_quest_guide_hud("time", marker="$\nWait for {color=#48F}[flora]{/} to come\nback from {color=#48F}school{/}."))
          elif game.hour == 17:
            rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
            if game.location == "home_kitchen":
              rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[flora]{/} to leave\nthe {color=#48F}bathroom{/}."))
          elif game.hour == 19:
            rv.append(get_quest_guide_path("home_hall", marker = ["flora contact_icon@.85"]))
            rv.append(get_quest_guide_marker("home_hall", "flora", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (190,-10)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

        ## Voluntary Good ##
        if quest.fall_in_newfall.finished and quest.lindsey_angel.finished and quest.maya_witch.finished and not quest.lindsey_voluntary.started:
          if quest.lindsey_angel["finished_this_week"]:
            days = quest.lindsey_angel["finished_this_week_day_tracker"] + 7 - game.day
            if quest.lindsey_angel["finished_today"]:
              rv.append(get_quest_guide_hud("time", marker="$Wait a {color=#48F}week{/}."))
            elif days > 1:
              rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}" + str(days) + "{/} days."))
            else:
              rv.append(get_quest_guide_hud("time", marker="$Wait a {color=#48F}day{/}."))
          else:
            rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
            rv.append(get_quest_guide_marker("marina", "marina_hospital", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (300,-70)))

        ## Paint the Town ##
        if quest.fall_in_newfall.finished and quest.jacklyn_romance.finished and not (quest.jacklyn_town.started or quest.jacklyn_town.failed):
          if quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
            rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          days = quest.jacklyn_romance["finished_day_tracker"] + 5 - game.day
          if days > 1:
            rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}" + str(days) + "{/} days."))
          else:
            rv.append(get_quest_guide_hud("time", marker="$Wait a {color=#48F}day{/}."))

        ## Hot My Bot ##
        if quest.mrsl_HOT.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and not quest.mrsl_bot.started:
          if quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
            rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

        ## The Dive ##
        if quest.maxine_hook.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and quest.maya_spell.finished and not quest.maxine_dive.started:
          rv.append(get_quest_guide_path("school_park", marker = ["actions go"]))

        ## Service Aid ##
        if (((quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished)
        or (quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished))
        and quest.fall_in_newfall.finished and not quest.nurse_aid.started):
          rv.append(get_quest_guide_path("school_roof", marker = ["actions go"]))

        ## Under the Stars ##
        # if (quest.isabelle_locker.actually_finished and not quest.isabelle_locker["time_for_a_change"]) and (quest.isabelle_dethroning.finished and not quest.isabelle_dethroning["kate_sex"]) and quest.fall_in_newfall.finished and not quest.isabelle_stars.started:
        if (quest.isabelle_locker.actually_finished and not quest.isabelle_locker["time_for_a_change"]) and (quest.isabelle_dethroning.finished and not quest.isabelle_dethroning["isabelle_caught"]) and quest.fall_in_newfall.finished and not quest.isabelle_stars.started:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

        ## Sauce/Off ##
        if quest.fall_in_newfall.finished and quest.maya_quixote.finished and not quest.maya_sauce.started:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      if not rv:
        if len(get_available_quest_guides()) > 2:
          for quest_title,quest_id in get_available_quest_guides():
            rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}"+quest_title+"{/}."))
        else:
          rv.append(get_quest_guide_hud("quest_guide", marker="$\n\n\n\n\n\n\nThere are no more quests available at this time!\nThis likely means you've reached the end of the\ncurrent release.\n\nHowever, some quests have prerequisites which\nmight've not been met in this playthrough.\nCheck out our {color=#48F}Chapter Select{/} screen to make sure\nyou're not missing out on any of them!"))

      return rv
