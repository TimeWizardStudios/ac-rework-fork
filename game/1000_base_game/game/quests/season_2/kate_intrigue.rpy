init python:
  class Quest_kate_intrigue(Quest):

    title = "Vicious Intrigue"

    class phase_1_private_chat:
      description = "The answer — it's time to find it."
#     hint = "What's going on with the not-so-new-girl-anymore? Privacy to find out."
      hint = "What's going on with the not-so-new-girl-anymore? Privacy to{space=-5}\nfind out."
      def quest_guide_hint(quest):
        if isabelle.location == "school_english_class":
          return "Quest [isabelle] in the English classroom."
        elif quest["us_together"]:
          if game.hour < 11:
            return "Wait for [isabelle] to " + ("come to the\n" if game.location == "school_english_class" else "go to the ") + "English classroom between " + ("10:00 — 11:00." if persistent.time_format == "24h" else "10:00 AM — 11:00 AM.")
          else:
            return "Wait for [isabelle] to go to the English classroom tomorrow."
        else:
          if game.location == "school_english_class":
#           return "Wait for [isabelle] to come to the English classroom between " + ("10:00 — 11:00." if persistent.time_format == "24h" else "10:00 AM — 11:00 AM.")
            return "Wait for [isabelle] to come to the{space=-5}\nEnglish classroom between " + ("10:00 — 11:00." if persistent.time_format == "24h" else "10:00 AM — 11:00 AM.")
          else:
            return "Quest [isabelle]."

    class phase_2_snitch:
      hint = "The girl that is scary and hot. Scary hot."
      quest_guide_hint = "Quest [kate]."

    class phase_3_wait:
      def hint(quest):
        if quest["after_school"]:
          return "Sometimes, waiting is the hardest part... but not always."
        else:
          return "Sometimes, waiting is the hardest part."
      def quest_guide_hint(quest):
        if quest["after_school"]:
          return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."
        else:
          return "Wait 1 hour."

    class phase_4_eavesdrop:
      hint = "Spying, watching, and other types of maverick behavior."
      def quest_guide_hint(quest):
        if "school_cafeteria" not in quest["eavesdropped_locations"]:
          return "Quest [isabelle] in the cafeteria."
        if "school_first_hall" not in quest["eavesdropped_locations"]:
          return "Quest [isabelle] in the first hall."
        if "school_gym" not in quest["eavesdropped_locations"]:
          return "Quest [isabelle] in the gym."
        if "school_ground_floor" not in quest["eavesdropped_locations"]:
          return "Quest [isabelle] in the entrance hall."
        else:
          return "Quest [isabelle] in the English classroom."

    class phase_5_spy:
#     hint = "Sometimes, it's best to just ask her. You know, the girl with the accent?"
      hint = "Sometimes, it's best to just ask her. You know, the girl with the{space=-10}\naccent?"
      quest_guide_hint = "Quest [isabelle]."

    class phase_6_report:
      hint = "Betrayal has never been so in. The queen will love it."
      quest_guide_hint = "Quest [kate] again."

    class phase_7_lion_den:
#     hint = "Never thought you'd end up here, did you? Well, time to see how the other side lives."
      hint = "Never thought you'd end up here,{space=-20}\ndid you? Well, time to see how the other side lives."
      def quest_guide_hint(quest):
        interactables = 8-kate_house_bedroom["interactables"]
#       return "Interact with any " + str(interactables) + (" interactables" if interactables > 1 else " interactable") + " in [kate]'s room."
        return "Interact with any " + str(interactables) + (" interactables{space=-15}" if interactables > 1 else " interactable") + "\nin [kate]'s room."

    class phase_1000_done_got_pegged:
#     description = "Passed the trials of pride and pain, and yet the future remains obscured."
      description = "Passed the trials of pride\nand pain, and yet the future\nremains obscured."

    class phase_1001_done_left_alone:
#     description = "Always play the winning side. That's how you never lose."
      description = "Always play the winning side.\nThat's how you never lose."

    class phase_1002_done_cold_feet:
#     description = "Sometimes, you just have to admit that this lifestyle isn't for you."
      description = "Sometimes, you just have to\nadmit that this lifestyle isn't\nfor you."

    class phase_2000_failed_help_isa:
#     description = "To admit that you're lost is never shameful. It's the first step onto the right path."
      description = "To admit that you're lost is\nnever shameful. It's the first\nstep onto the right path."

    class phase_2001_failed_kate_bet:
#     description = "Lonely, difficult, and rarely rewarding. The path of the sigma is not for everyone."
      description = "Lonely, difficult, and rarely rewarding. The path of the\nsigma is not for everyone."


init python:
  class Event_quest_kate_intrigue(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.kate_intrigue == "lion_den":
        return 0

    def on_can_advance_time(event):
      if quest.kate_intrigue == "lion_den":
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_english_class" == isabelle.location and quest.kate_intrigue == "private_chat" and not quest.kate_intrigue["least_likely"]:
        game.events_queue.append("quest_kate_intrigue_private_chat_upon_entering")

    def on_advance(event):
      if quest.kate_intrigue == "private_chat" and game.location == "school_english_class" and game.hour == 10:
        game.events_queue.append("quest_kate_intrigue_private_chat_advance_time")
      elif quest.kate_intrigue == "wait" and quest.kate_intrigue["sub_suits_you"] and not quest.kate_intrigue["after_school"]:
        quest.kate_intrigue["eavesdropped_locations"] = set()
        quest.kate_intrigue.advance("eavesdrop")
      elif quest.kate_intrigue == "wait" and quest.kate_intrigue["after_school"] and game.hour == 19:
        quest.kate_intrigue.advance("lion_den")
      elif quest.kate_intrigue == "wait" and not quest.kate_intrigue["after_school"]:
        quest.kate_intrigue.advance("spy")

    def on_enter_roaming_mode(event):
      if quest.kate_intrigue == "lion_den" and not quest.kate_intrigue["cages_whips_and_screams"]:
        game.events_queue.append("quest_kate_intrigue_lion_den_advance_time")
      elif quest.kate_intrigue == "lion_den" and kate_house_bedroom["interactables"] == 8:
        game.events_queue.append("quest_kate_intrigue_lion_den")

    def on_quest_guide_kate_intrigue(event):
      rv = []

      if quest.kate_intrigue == "private_chat":
        if isabelle.location == "school_english_class":
          rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_english_class", "isabelle", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (90,0)))
        elif quest.kate_intrigue["us_together"]:
          if game.hour < 11:
            rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[isabelle]{/} to " + ("come" if game.location == "school_english_class" else "go") + " to the {color=#48F}English\nclassroom{/} between "+("{color=#48F}10:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}10:00 AM{/} — {color=#48F}11:00 AM{/}.")))
          elif quest.maya_sauce["bedroom_taken_over"]:
            rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[isabelle]{/} to go to the\n{color=#48F}English classroom{/} tomorrow."))
            rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
            rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          else:
            rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[isabelle]{/} to go to the\n{color=#48F}English classroom{/} tomorrow."))
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
            rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        else:
          if game.location == "school_english_class":
            rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[isabelle]{/} to come to the {color=#48F}English\nclassroom{/} between "+("{color=#48F}10:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}10:00 AM{/} — {color=#48F}11:00 AM{/}.")))
          else:
            rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
            rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.kate_intrigue == "snitch":
        rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "kate", marker = ["actions quest"]))

      elif quest.kate_intrigue == "wait":
        if quest.kate_intrigue["after_school"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 PM{/}.")))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))

      elif quest.kate_intrigue == "eavesdrop":
        if "school_cafeteria" not in quest.kate_intrigue["eavesdropped_locations"]:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["isabelle contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "isabelle", marker = ["actions quest"]))
        elif "school_first_hall" not in quest.kate_intrigue["eavesdropped_locations"]:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["isabelle contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_first_hall", "isabelle", marker = ["actions quest"], marker_offset = (25,0)))
        elif "school_gym" not in quest.kate_intrigue["eavesdropped_locations"]:
          rv.append(get_quest_guide_path("school_gym", marker = ["isabelle contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_gym", "isabelle", marker = ["actions quest"], marker_offset = (-5,-5)))
        elif "school_ground_floor" not in quest.kate_intrigue["eavesdropped_locations"]:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["isabelle contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "isabelle", marker = ["actions quest"]))
        else:
          rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_english_class", "isabelle", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (90,0)))

      elif quest.kate_intrigue == "spy":
        rv.append(get_quest_guide_path("school_english_class", marker = ["isabelle contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_english_class", "isabelle", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (90,0)))

      elif quest.kate_intrigue == "report":
        rv.append(get_quest_guide_char("kate", marker = ["kate contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "kate", marker = ["actions quest"]))

      elif quest.kate_intrigue == "lion_den":
        interactables = 8-kate_house_bedroom["interactables"]
        rv.append(get_quest_guide_hud("map", marker=("$\nInteract with any\n{color=#48F}" + str(interactables) + ("{/} interactables." if interactables > 1 else "{/} interactable.")), marker_sector = "mid_top"))

      return rv
