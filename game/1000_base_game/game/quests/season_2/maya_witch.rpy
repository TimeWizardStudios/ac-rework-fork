image maya_schedule_interact = LiveComposite((180,120),(0,25),"items maya_schedule",(95,27),Transform("actions interact",zoom=0.85))
image package_consume = LiveComposite((180,120),(0,25),"items package",(95,27),Transform("actions consume",zoom=0.85))


init python:
  class Quest_maya_witch(Quest):

    title = "Big Witch Energy"

    class phase_1_schedule:
      description = "I used to love flowers until\nI discovered cats."
      hint = "Do cats even have schedules? Let's find out."
      def quest_guide_hint(_quest):
        if mc.owned_item("maya_schedule"):
          return "Interact with [maya]'s schedule in{space=-5}\nyour inventory."
        else:
          if mrsl.location == "school_homeroom" == mc.location:
            return "Wait for [mrsl] to leave the homeroom."
          else:
            if quest.maya_witch["locked_of_course"]:
              if mc.owned_item("high_tech_lockpick"):
                return "Use the high-tech lockpick on the{space=-20}\nteacher's desk in the homeroom.{space=-10}"
              elif mc.owned_item("ball_of_yarn") and mc.owned_item("safety_pin"):
                return "Craft a high-tech lockpick by combining the safety pin with the ball of yarn."
              else:
                if not mc.owned_item("safety_pin") and quest.nurse_aid["name_reveal"]:
                  return "Take the safety pin from the lollipop jar in the nurse's office."
                elif not mc.owned_item("safety_pin"):
                  return "Take the safety pin from the lollipop jar in the [nurse]'s office."
                if not mc.owned_item("ball_of_yarn"):
                  return "Take the ball of yarn from the{space=-5}\nleftmost desk in the homeroom.{space=-5}"
            else:
              return "Interact with the teacher's desk{space=-5}\nin the homeroom."

    class phase_2_english_class:
      hint = "The place where poets and academic elitism are born."
      quest_guide_hint = "Go to the English classroom."

    class phase_3_gym_class:
      hint = "Time to visit the church of speed, strength, and endurance."
      quest_guide_hint = "Go to the gym."

    class phase_4_computer_class:
      hint = "My favorite place in the school, besides the soda fountain."
      quest_guide_hint = "Go to the computer room."

    class phase_5_locked:
      hint = "My work here is done."
      quest_guide_hint = "Leave the computer room."

    class phase_6_phone_call:
      hint = "Calling for help isn't cowardly! At least, that's what [jo] always says."
      quest_guide_hint = "Call [jo] on your phone."

    class phase_7_wait:
      hint = "The girl who can't be charmed."
      quest_guide_hint = "Quest [maya]."

    class phase_8_help:
      hint = "The girl with glasses. You know which one."
      quest_guide_hint = "Quest [maxine]."

    class phase_9_meet_up:
      hint = "Time to rendezvous with the girl who has a pussy."
      def quest_guide_hint(quest):
        if game.hour < quest["absurd_timeframe"][0]:
          return "Wait between " + time_str(quest["absurd_timeframe"][0],0) + " and " + time_str(quest["absurd_timeframe"][1],0) + "."
        elif game.hour in quest["absurd_timeframe"]:
          return "Quest [maxine] in her office."
        elif game.hour > quest["absurd_timeframe"][1]:
          return "Go to sleep."

    class phase_10_black_market:
      hint = "There's only one place to buy illegal books and contraband."
      def quest_guide_hint(quest):
        if quest["black_market_accessed"]:
          if mc.money < 666:
            return "Gather $666."
          else:
            return "Buy the \"Hex, Vex, and TexMex\" book from the black market."
        elif home_computer["visited"]:
          return "Access the black market from the computer in your bedroom."
        else:
          return "Interact with the computer in your bedroom."

    class phase_11_package:
      hint = "One anti-hex, magical spell recipe book coming right up!"
      def quest_guide_hint(quest):
        if home_computer["hex_vex_and_texmex_ordered_today"]:
          return "Wait until " + game.dow_names[0 if game.dow == 6 else game.dow+1] + "."
        else:
          return "Take the mysterious package from the kitchen, and then consume it."

    class phase_1000_done:
      description = "Cat girls, arsonism, and voodoo — what else do you need on a Tuesday?"


init python:
  class Event_quest_maya_witch(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.maya_witch in ("locked","phone_call","wait"):
        return 0

    def on_can_advance_time(event):
      if quest.maya_witch in ("locked","phone_call","wait"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_first_hall" and quest.fall_in_newfall.finished and not (quest.maya_witch.started or mc["focus"]):
        game.events_queue.append("quest_maya_witch_start")
      elif new_location == "school_homeroom" and quest.maya_witch == "schedule" and not quest.maya_witch["overthink_it"]:
        game.events_queue.append("quest_maya_witch_schedule")
      elif new_location == "school_first_hall_west" and quest.maya_witch == "english_class":
        game.events_queue.append("quest_maya_witch_english_class")
      elif new_location == "school_first_hall_east" and quest.maya_witch == "gym_class" and not quest.maya_witch["act_casual"]:
        game.events_queue.append("quest_maya_witch_gym_class_school_first_hall_east")
      elif new_location == "school_gym" and quest.maya_witch == "gym_class":
        game.events_queue.append("quest_maya_witch_gym_class")
      elif new_location == "home_computer" and quest.maya_witch == "black_market":
        quest.maya_witch["black_market_accessed"] = True

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "maya_witch" and quest.maya_witch == "black_market":
        black_market.add_item("hex_vex_and_texmex")

    def on_quest_guide_maya_witch(event):
      rv = []

      if quest.maya_witch == "schedule":
        if mc.owned_item("maya_schedule"):
          rv.append(get_quest_guide_hud("inventory", marker = ["maya_schedule_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        else:
          if mrsl.location == "school_homeroom" == mc.location:
            rv.append(get_quest_guide_hud("time", marker = "$\nWait for {color=#48F}[mrsl]{/} to leave\nthe {color=#48F}homeroom{/}."))
          else:
            if quest.maya_witch["locked_of_course"]:
              if mc.owned_item("high_tech_lockpick"):
                rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom desk@.35"]))
                rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["items high_tech_lock@.8"], marker_sector = "left_bottom", marker_offset = (300,35)))
              elif mc.owned_item("ball_of_yarn") and mc.owned_item("safety_pin"):
                rv.append(get_quest_guide_hud("inventory", marker = ["ball_of_yarn_combine_safety_pin"], marker_sector = "mid_top", marker_offset = (60,0)))
              else:
                if not mc.owned_item("safety_pin"):
                  rv.append(get_quest_guide_path("school_nurse_room", marker = ["school nurse_room cup@.5"]))
                  rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_lollipop_jar", marker = ["actions take"], marker_sector = "right_top", marker_offset = (0,120)))
                if not mc.owned_item("ball_of_yarn"):
                  rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom yarn@.5"]))
                  rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_ball_of_yarn", marker = ["actions take"], marker_offset = (60,-20)))
            else:
              rv.append(get_quest_guide_path("school_homeroom", marker = ["school homeroom desk@.35"]))
              rv.append(get_quest_guide_marker("school_homeroom", "school_homeroom_teacher_desk", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (300,35)))

      elif quest.maya_witch == "english_class":
        rv.append(get_quest_guide_path("school_english_class", marker = ["actions go"]))

      elif quest.maya_witch == "gym_class":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))

      elif quest.maya_witch == "computer_class":
        rv.append(get_quest_guide_path("school_computer_room", marker = ["actions go"]))

      elif quest.maya_witch == "locked":
        rv.append(get_quest_guide_marker("school_computer_room", "school_computer_room_door", marker = ["actions go"], marker_sector = "right_top", marker_offset = (110,70)))

      elif quest.maya_witch == "phone_call":
        rv.append(get_quest_guide_hud("phone", marker=["call_jo","$\n\n\n{space=50}Call {color=#48F}[jo]{/}."]))

      elif quest.maya_witch == "wait":
        rv.append(get_quest_guide_marker("school_computer_room", "maya", marker = ["actions quest"], marker_offset = (40,-10)))

      elif quest.maya_witch == "help":
        rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.maya_witch == "meet_up":
        if game.hour < quest.maya_witch["absurd_timeframe"][0]:
          rv.append(get_quest_guide_hud("time", marker="$\nWait between\n{color=#48F}"+time_str(quest.maya_witch["absurd_timeframe"][0],0)+"{/} and {color=#48F}"+time_str(quest.maya_witch["absurd_timeframe"][1],0)+"{/}."))
        elif game.hour in quest.maya_witch["absurd_timeframe"]:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["maxine contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_clubroom", "maxine", marker = ["actions quest"], marker_offset = (75,25)))
        elif game.hour > quest.maya_witch["absurd_timeframe"][1]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.maya_witch == "black_market":
        if quest.maya_witch["black_market_accessed"]:
          if mc.money < 666:
            rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$666{/}.", marker_offset = (471+(len(str(mc.money))*10),0)))
          elif game.location == "home_computer":
            rv.append(get_quest_guide_hud("time", marker="$\nBuy the {color=#48F}\"Hex, Vex,\nand TexMex\"{/} book.", marker_offset = (471+(len(str(mc.money))*10),0)))
          else:
            rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
            rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions black_market"]))
        elif home_computer["visited"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions black_market"]))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"]))

      elif quest.maya_witch == "package":
        if home_computer["hex_vex_and_texmex_ordered_today"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.3"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        elif home_computer["hex_vex_and_texmex_received"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
        else:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.65"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_hex_vex_and_texmex", marker = ["actions take"], marker_offset = (-10,-15)))

      return rv
