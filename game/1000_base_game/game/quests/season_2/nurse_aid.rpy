image interact_left_mid = LiveComposite((103-50,108),(0,0),"actions interact")
image fishing_pole_worms_combine = LiveComposite((280,130),(0+10,35),"items fishing_pole",(96,35),Transform("actions combine",zoom=0.85),(184-5,35),"items worms")


init python:
  class Quest_nurse_aid(Quest):

    title = "Service Aid"

    class phase_1_fishing_pole:
      # description = "Sometimes, all you need is a little help."
      description = "Sometimes, all you need\nis a little help."
      def hint(_quest):
        if quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished:
          return "Go to where the air smells like the sea and where the oars are filthy."
        elif quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished:
          # return "She has glasses. How many people do you know with glasses?"
          return "She has glasses. How many people{space=-25}\ndo you know with glasses?"
      def quest_guide_hint(_quest):
        if (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished) and game.location == "marina":
          # return "Interact with the Filthy Oar boat."
          return "Interact with the Filthy Oar boat.{space=-15}"
        elif quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished:
          return "Go to the Newfall Marina."
        elif quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished:
          return "Quest [maxine]."

    class phase_2_fishing:
      hint = "When you're outnumbered by trees, find a clearing."
      quest_guide_hint = "Quest the [nurse] in the forest glade."

    class phase_3_bait:
      # hint = "Construct a worm-pole, and then speak to the healer of bodies."
      hint = "Construct a worm-pole, and then{space=-15}\nspeak to the healer of bodies."
      def quest_guide_hint(quest):
        if mc.owned_item("baited_pole"):
          return "Quest the [nurse] again."
        else:
          return "Combine the fishing pole with the worms."

    class phase_4_sleep:
      hint = "Another day, another... day."
      def quest_guide_hint(quest):
        return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."

    class phase_5_hiking:
      hint = "Another word for easy. Another word for candy-striper."
      quest_guide_hint = "Quest the [nurse] in the park."

    class phase_6_sleep_again:
      hint = "When god gives you days, you find a new day!"
      def quest_guide_hint(quest):
        return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."

    class phase_7_shopping:
      def hint(quest):
        if game.location == "dress_shop":
          return "Shopping spree! Take your time, look around."
        else:
          return "Shopping spree!"
      def quest_guide_hint(quest):
        if game.location == "dress_shop" and "revealing_dress" not in quest["dresses"]:
          return "Interact with the revealing dress."
        elif game.location == "dress_shop" and  "sophisticated_dress" not in quest["dresses"]:
          return "Interact with the sophisticated dress."
        elif game.location == "dress_shop" and  "nice_dress" not in quest["dresses"]:
          return "Interact with the nice dress."
        else:
          return "Go to the Newfall Marina."

    class phase_8_date:
      hint = "Just wait a minute, or a few hours, or several hours."
      def quest_guide_hint(quest):
        if game.day >= quest["date"]:
          return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."
        else:
          return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "."

    class phase_9_aftermath:
      hint = "Wait for a new dawn, a new chance, and a new moment to converse with the lady that used to be in scrubs."
      def quest_guide_hint(quest):
        if game.day > quest["date"] and game.hour >= 7:
          return "Quest the [nurse]."
        elif game.day > quest["date"]:
          return "Wait until 7:00." if persistent.time_format == "24h" else "Wait until 7:00 AM."
        elif game.day < quest["date"]:
          return "Wait until " + game.dow_names[0 if game.dow == 5 else 1 if game.dow == 6 else game.dow+2] + "."
        else:
          return "Wait until " + game.dow_names[0 if game.dow == 6 else game.dow+1] + "."

    class phase_10_someone_else:
      # hint = "Time to play matchmaker between the punk and the bitch."
      hint = "Time to play matchmaker between the punk and the bitch.{space=-15}"
      def quest_guide_hint(quest):
        if quest["jacklyn_on_board"]:
          return "Quest the [nurse] in her office."
        else:
          return "Quest [jacklyn]."

    class phase_11_relationship:
      hint = "It's time to self-insert yourself into the [nurse]... and her life."
      quest_guide_hint = "Quest the [nurse] in her office."

    class phase_1000_done:
      # description = "A new chapter in the book of love."
      description = "A new chapter in the\nbook of love."

    class phase_1001_done_jacklyn:
      # description = "A new chapter in the book of lust."
      description = "A new chapter in the\nbook of lust."


init python:
  class Event_quest_nurse_aid(GameEvent):

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_roof" and (((quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished)
      or (quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished))
      and quest.fall_in_newfall.finished and not quest.nurse_aid.started):
        game.events_queue.append("quest_nurse_aid_start")
      elif quest.nurse_aid == "fishing_pole" and (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished) and new_location == "marina" and not quest.nurse_aid["look_around"]:
        game.events_queue.append("quest_nurse_aid_fishing_pole_marina_upon_entering")
        quest.nurse_aid["look_around"] = True
      elif quest.nurse_aid == "shopping" and new_location == "marina":
        game.events_queue.append("quest_nurse_aid_shopping")

    def on_advance(event):
      if quest.nurse_aid == "sleep" and game.hour == 7:
        quest.nurse_aid.advance("hiking")
      elif quest.nurse_aid == "sleep_again" and game.hour == 7:
        quest.nurse_aid.advance("shopping")
      elif quest.nurse_aid == "date" and game.day >= quest.nurse_aid["date"] and game.hour == 19:
        game.events_queue.append("quest_nurse_aid_date")

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.nurse_aid == "shopping" and game.location == "dress_shop":
        return 0

    def on_can_advance_time(event):
      if quest.nurse_aid == "shopping" and game.location == "dress_shop":
        return False

    def on_quest_guide_nurse_aid(event):
      rv = []

      if quest.nurse_aid == "fishing_pole":
        if quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished:
          rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
          if ("quest_nurse_aid_fishing_pole_marina_prawn_star",()) not in game.seen_actions:
            rv.append(get_quest_guide_marker("marina", "marina_prawn_star", marker = ["interact_left_mid"], marker_sector = "left_mid", marker_offset = (495,-235)))
          if ("quest_nurse_aid_fishing_pole_marina_pancake_brothel",()) not in game.seen_actions:
            rv.append(get_quest_guide_marker("marina", "marina_pancake_brothel", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (315,50)))
          rv.append(get_quest_guide_marker("marina", "marina_filthy_oar", marker = ["actions interact"], marker_offset = (130,10)))
        elif quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished:
          rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.nurse_aid == "fishing":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_forest_glade", "nurse", marker = ["actions quest"], marker_offset = (20,10)))

      elif quest.nurse_aid == "bait":
        if mc.owned_item("baited_pole"):
          rv.append(get_quest_guide_path("school_forest_glade", marker = ["nurse contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_forest_glade", "nurse", marker = ["actions quest"], marker_offset = (20,10)))
        else:
          rv.append(get_quest_guide_hud("inventory", marker = ["fishing_pole_worms_combine"], marker_sector = "mid_top", marker_offset = (60,0)))

      elif quest.nurse_aid == "sleep":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))

      elif quest.nurse_aid == "hiking":
        rv.append(get_quest_guide_path("school_park", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_park", "nurse", marker = ["actions quest"], marker_offset = (0,0)))

      elif quest.nurse_aid == "sleep_again":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))

      elif quest.nurse_aid == "shopping":
        if game.location == "dress_shop" and "revealing_dress" not in quest.nurse_aid["dresses"]:
          if "summer_dress" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_summer_dress", marker = ["interact_left_mid"], marker_sector = "left_mid", marker_offset = (110,-280)))
          if "ball_gown" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_ball_gown", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (210,0)))
          if "slutty_dress" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_slutty_dress", marker = ["interact_right_mid"], marker_sector = "right_mid", marker_offset = (-35,100)))
          rv.append(get_quest_guide_marker("dress_shop", "dress_shop_revealing_dress", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (15,-185)))
        elif game.location == "dress_shop" and "sophisticated_dress" not in quest.nurse_aid["dresses"]:
          if "amish_dress" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_amish_dress", marker = ["interact_right_mid"], marker_sector = "right_mid", marker_offset = (-10,100)))
          if "wedding_dress" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_wedding_dress", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (30,575)))
          rv.append(get_quest_guide_marker("dress_shop", "dress_shop_sophisticated_dress", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (115,5)))
        elif game.location == "dress_shop" and "nice_dress" not in quest.nurse_aid["dresses"]:
          if "shirtdress" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_shirtdress", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (75,-15)))
          if "1800s_dress" not in quest.nurse_aid["dresses"]:
            rv.append(get_quest_guide_marker("dress_shop", "dress_shop_1800s_dress", marker = ["interact_left_mid"], marker_sector = "left_mid", marker_offset = (240,100)))
          rv.append(get_quest_guide_marker("dress_shop", "dress_shop_nice_dress", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (-20,365)))
        else:
          rv.append(get_quest_guide_path("marina", marker = ["actions go"]))

      elif quest.nurse_aid == "date":
        if game.day >= quest.nurse_aid["date"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 PM{/}.")))
        elif quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 6 else "7:00" if persistent.time_format == "24h" else "7:00 AM") + "{/}."))

      elif quest.nurse_aid == "aftermath":
        if game.day > quest.nurse_aid["date"] and game.hour >= 7:
          rv.append(get_quest_guide_char("nurse", marker = ["nurse contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "nurse", marker = ["actions quest"]))
        elif game.day > quest.nurse_aid["date"]:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}7:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/}.")))
        elif game.day < quest.nurse_aid["date"] and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 5 else 1 if game.dow == 6 else game.dow+2] + "{/}."))
        elif game.day < quest.nurse_aid["date"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 5 else 1 if game.dow == 6 else game.dow+2] + "{/}."))
        elif quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 6 else game.dow+1] + "{/}."))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 6 else game.dow+1] + "{/}."))

      elif quest.nurse_aid == "someone_else":
        if quest.nurse_aid["jacklyn_on_board"]:
          rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))
        else:
          rv.append(get_quest_guide_char("jacklyn", marker = ["jacklyn contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "jacklyn", marker = ["actions quest"]))

      elif quest.nurse_aid == "relationship":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["nurse contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))

      return rv
