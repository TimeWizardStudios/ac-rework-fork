image interact_mid_bottom = LiveComposite((103,108+60),(0,0),"actions interact")


init python:
  class Quest_maya_sauce(Quest):

    title = "Sauce/Off"

    class phase_1_siren_song:
      # description = "The siren song is a dangerous guide."
      description = "The siren song is a\ndangerous guide."
      # hint = "Follow. Go closer. Don't be afraid."
      hint = "Follow. Go closer. Don't be afraid.{space=-5}"
      def quest_guide_hint(quest):
        if quest["drowning_duck"]:
          return "Go to the bathroom."
        else:
          return "Leave your bedroom."

    class phase_2_breakfast:
      hint = "Even a fat person must eat. Take that as you will."
      quest_guide_hint = "Quest [flora] in the kitchen."

    class phase_3_school:
      hint = "Time to get schooled, kid."
      def quest_guide_hint(quest):
        if quest["dick_eating_contest"]:
          return "Interact with the bulletin board in the entrance hall."
        else:
          return "Go to school."

    class phase_4_classes:
      hint = "The room has windows. And chairs! Chairs and windows."
      quest_guide_hint = "Go to the homeroom."

    class phase_5_dinner:
      # hint = "Why cook dinner yourself when you can be lazy and ask someone else to cook for you?"
      hint = "Why cook dinner yourself when{space=-5}\nyou can be lazy and ask someone{space=-10}\nelse to cook for you?"
      quest_guide_hint = "Quest [flora]."

    class phase_6_preparation:
      # hint = "Sleep. Wake. And so on. Life in a nutshell."
      hint = "Sleep. Wake. And so on. Life in\na nutshell."
      def quest_guide_hint(quest):
        if 7 > game.hour >= 0 or 23 >= game.hour > 19:
          return "Go to sleep."
        else:
          return "Leave your bedroom."

    class phase_7_wait:
      hint = "Wait for better times. Specifically, noon."
      def quest_guide_hint(quest):
        if 7 > game.hour >= 0 or 23 >= game.hour > 12:
          return "Go to sleep."
        else:
          return "Wait until 12:00." if persistent.time_format == "24h" else "Wait until 12:00 PM."

    class phase_8_sauces:
      hint = "It all boils down to this — peppers and spices in a variety of sauces."
      quest_guide_hint = "Interact with all five hot sauces in the kitchen."

    class phase_9_aftermath:
      # hint = "Time to hide your face and lick your wounds. Shame is upon you and your bloodline."
      hint = "Time to hide your face and lick your wounds. Shame is upon you{space=-5}\nand your bloodline."
      quest_guide_hint = "Go to your bedroom."

    class phase_1000_done:
      # description = "Everything will taste like cardboard for the rest of your life, but at least you beat a girl."
      description = "Everything will taste like\ncardboard for the rest of your life,\nbut at least you beat a girl."

    class phase_1001_done_but_bedroom_taken_over:
      # description = "Life is too short to dwell on losses, especially if they're as common as yours."
      description = "Life is too short to dwell\non losses, especially if they're\nas common as yours."


init python:
  class Event_quest_maya_sauce(GameEvent):

    def on_advance(event):
      if quest.fall_in_newfall.finished and quest.maya_quixote.finished and not quest.maya_sauce.started and game.location == "home_bedroom" and game.hour == 7 and not mc["focus"]:
        game.events_queue.append("quest_maya_sauce_start")
      if quest.maya_sauce == "wait" and game.hour == 12 and not mc["focus"]:
        game.events_queue.append("quest_maya_sauce_wait")

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.maya_sauce == "siren_song" and new_location == "home_hall" and not quest.maya_sauce["drowning_duck"]:
        game.events_queue.append("quest_maya_sauce_siren_song_upon_entering")
        quest.maya_sauce["drowning_duck"] = True
      elif quest.maya_sauce == "school" and new_location == "school_ground_floor" and not quest.maya_sauce["dick_eating_contest"]:
        # game.events_queue.append("quest_maya_sauce_school_upon_entering")
        quest.maya_sauce["dick_eating_contest"] = True
      elif quest.maya_sauce == "classes" and new_location == "school_homeroom" and not mc["focus"] and not quest.maya_sauce["surprise_test"]:
        game.events_queue.append("quest_maya_sauce_classes")
        quest.maya_sauce["surprise_test"] = True
      elif quest.maya_sauce == "preparation" and new_location == "home_hall" and 6 < game.hour < 20:
        game.events_queue.append("quest_maya_sauce_preparation")
      elif quest.maya_sauce == "aftermath" and new_location == "home_hall":
        game.events_queue.append("quest_maya_sauce_aftermath")

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.maya_sauce in ("siren_song","breakfast","dinner","sauces"):
        return 0

    def on_can_advance_time(event):
      if quest.maya_sauce in ("siren_song","breakfast","dinner","sauces"):
        return False

    def on_enter_roaming_mode(event):
      if quest.maya_sauce == "sauces" and all(hot_sauce in quest.maya_sauce["hot_sauces"] for hot_sauce in ("sauce_bust_a_ghost","sauce_carolina","sauce_fahrenheit","sauce_taco_hell","sauce_wench")):
        game.events_queue.append("quest_maya_sauce_sauces")

    def on_quest_guide_maya_sauce(event):
      rv = []

      if quest.maya_sauce == "siren_song":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["actions go"]))

      elif quest.maya_sauce == "breakfast":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"], marker_offset = (40,0)))

      elif quest.maya_sauce == "school":
        if quest.maya_sauce["dick_eating_contest"]:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["school ground_floor board@.85"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_notice_board", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (60,-20)))
        else:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      elif quest.maya_sauce == "classes":
        rv.append(get_quest_guide_path("school_homeroom", marker = ["actions go"]))

      elif quest.maya_sauce == "dinner":
        rv.append(get_quest_guide_marker("home_kitchen", "flora", marker = ["actions quest"], marker_offset = (25,0)))

      elif quest.maya_sauce == "preparation":
        if 7 > game.hour >= 0 or 23 >= game.hour > 19:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        else:
          rv.append(get_quest_guide_path("home_hall", marker = ["actions go"]))

      elif quest.maya_sauce == "wait":
        if 7 > game.hour >= 0 or 23 >= game.hour > 12:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 6 else game.dow+1] + "{/}."))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}12:00{/}." if persistent.time_format == "24h" else "{color=#48F}12:00 PM{/}.")))

      elif quest.maya_sauce == "sauces":
        if "sauce_fahrenheit" not in quest.maya_sauce["hot_sauces"]:
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sauce_fahrenheit", marker = ["interact_right_mid"], marker_sector = "right_mid", marker_offset = (-25,50)))
        if "sauce_bust_a_ghost" not in quest.maya_sauce["hot_sauces"]:
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sauce_bust_a_ghost", marker = ["actions interact"], marker_offset = (0,-20)))
        if "sauce_taco_hell" not in quest.maya_sauce["hot_sauces"]:
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sauce_taco_hell", marker = ["interact_mid_bottom"], marker_sector = "mid_bottom", marker_offset = (17,-25)))
        if "sauce_wench" not in quest.maya_sauce["hot_sauces"]:
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sauce_wench", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (0,120)))
        if "sauce_carolina" not in quest.maya_sauce["hot_sauces"]:
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_sauce_carolina", marker = ["interact_mid_top"], marker_sector = "mid_top", marker_offset = (15,130)))

      elif quest.maya_sauce == "aftermath":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["actions go"]))

      return rv
