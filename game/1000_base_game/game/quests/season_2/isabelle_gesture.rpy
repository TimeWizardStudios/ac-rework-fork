init python:
  class Quest_isabelle_gesture(Quest):

    title = "The Grand Gesture"

    class phase_1_courage:
      description = "No one can do it for you."
      hint = "Get over your fears and face the consequences of your actions."
      quest_guide_hint = "Quest [isabelle]."

    class phase_2_advice:
      hint = "You need some input from an older, mature woman who understands relationships."
      quest_guide_hint = "Quest [jo]."

    class phase_3_letter:
      hint = "Time to sharpen your pencil and your wit."
      quest_guide_hint = "Interact with the blackboard in the English classroom."

    class phase_4_bouquet:
      hint = "A pretty and pretty convenient place for your floral needs."
      quest_guide_hint = "Take the wildflowers in the forest glade."

    class phase_5_chocolate:
      # hint = "There's only one place where you can buy anything and everything you need in one click. Okay, maybe two or three clicks."
      hint = "There's only one place where you{space=-15}\ncan buy anything and everything{space=-10}\nyou need in one click. Okay, maybe two or three clicks."
      def quest_guide_hint(_quest):
        if quest.maya_sauce["bedroom_taken_over"]:
          return "Interact with the computer in the hallway."
        else:
          return "Interact with the computer in your bedroom."

    class phase_6_package:
      def hint(quest):
        if home_computer["hearshey_shesay_ordered_today"] or home_computer["tob_le_bone_ordered_today"] or home_computer["dick_wonky_ordered_today"]:
          return "He who waits for something good, usually gets it delivered by mail."
        else:
          return "It's a decent size, but not as big as yours."
      def quest_guide_hint(quest):
        if home_computer["hearshey_shesay_ordered_today"] or home_computer["tob_le_bone_ordered_today"] or home_computer["dick_wonky_ordered_today"]:
          return "Wait until " + game.dow_names[0 if game.dow == 6 else game.dow+1] + "."
        else:
          return "Take the mysterious package from the kitchen, and then consume it."

    class phase_7_apology:
      hint = "You have everything you need. Now is the moment of truth."
      quest_guide_hint = "Quest [isabelle] again."

    class phase_8_wait:
      hint = "Nothing to do but wait for fate."
      quest_guide_hint = "Wait 1 hour."

    class phase_9_revenge:
      hint = "The place where old poetry makes up the wallpaper."
      quest_guide_hint = "Go to the English classroom."

    class phase_1000_done:
      description = "And so begins a new chapter."


init python:
  class Event_quest_isabelle_gesture(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.isabelle_gesture == "revenge":
        return 0

    def on_can_advance_time(event):
      if quest.isabelle_gesture == "revenge":
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if ((new_location == isabelle.location and (quest.isabelle_locker.actually_finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.actually_finished)) and (quest.isabelle_dethroning.finished and quest.isabelle_dethroning["isabelle_caught"]) and quest.fall_in_newfall.finished and not (quest.isabelle_gesture.started or mc["focus"]))
      or (new_location == isabelle.location and quest.kate_moment == "coming_soon" and not quest.isabelle_gesture.started)):
        game.events_queue.append("quest_isabelle_gesture_start")
      elif new_location == "school_english_class" and quest.isabelle_gesture == "letter" and not quest.isabelle_gesture["mossy_authors"]:
        game.events_queue.append("quest_isabelle_gesture_letter_upon_entering")
      elif new_location == "school_forest_glade" and quest.isabelle_gesture == "bouquet" and not quest.isabelle_gesture["strong_flower"]:
        game.events_queue.append("quest_isabelle_gesture_bouquet_upon_entering")
      elif new_location == isabelle.location and quest.isabelle_gesture == "apology" and not quest.isabelle_gesture["time_to_apologize"]:
        game.events_queue.append("quest_isabelle_gesture_apology_upon_entering")

    def on_advance(event):
      if quest.isabelle_gesture == "wait":
        quest.isabelle_gesture.advance("revenge")

    def on_enter_roaming_mode(event):
      if quest.isabelle_gesture == "revenge" and not quest.isabelle_gesture["only_one_way"]:
        game.events_queue.append("quest_isabelle_gesture_wait")

    def on_quest_guide_isabelle_gesture(event):
      rv = []

      if quest.isabelle_gesture == "courage":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_gesture == "advice":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      elif quest.isabelle_gesture == "letter":
        rv.append(get_quest_guide_path("school_english_class", marker = ["school english_class blackboard@.25"]))
        rv.append(get_quest_guide_marker("school_english_class", "school_english_class_blackboard", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (420,-80)))

      elif quest.isabelle_gesture == "bouquet":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["school forest_glade wildflowers@.6"]))
        rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_wildflowers", marker = ["actions take"], marker_offset = (10,0)))

      elif quest.isabelle_gesture == "chocolate":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall monitor@.5"]))
          rv.append(get_quest_guide_marker("home_hall","home_bedroom_computer", marker = ["actions interact"], marker_offset = (-10,-10)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom small_pc@.5"]))
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_computer", marker = ["actions interact"], marker_offset = (-15,-15)))

      elif quest.isabelle_gesture == "package":
        if (home_computer["hearshey_shesay_ordered_today"] or home_computer["tob_le_bone_ordered_today"] or home_computer["dick_wonky_ordered_today"]) and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 6 else game.dow+1] + "{/}."))
        elif home_computer["hearshey_shesay_ordered_today"] or home_computer["tob_le_bone_ordered_today"] or home_computer["dick_wonky_ordered_today"]:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
          rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + game.dow_names[0 if game.dow == 6 else game.dow+1] + "{/}."))
        elif home_computer["hearshey_shesay_received"] or home_computer["tob_le_bone_received"] or home_computer["dick_wonky_received"]:
          rv.append(get_quest_guide_hud("inventory", marker = ["package_consume"], marker_offset = (60,0), marker_sector = "mid_top"))
        else:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen package@.65"]))
          if home_computer["hearshey_shesay_ordered"]:
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_hearshey_shesay", marker = ["actions take"], marker_offset = (-10,-15)))
          elif home_computer["tob_le_bone_ordered"]:
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_tob_le_bone", marker = ["actions take"], marker_offset = (-10,-15)))
          elif home_computer["dick_wonky_ordered"]:
            rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_package_dick_wonky", marker = ["actions take"], marker_offset = (-10,-15)))

      elif quest.isabelle_gesture == "apology":
        rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))

      elif quest.isabelle_gesture == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}1{/} hour.", marker_offset = (15,-15)))

      elif quest.isabelle_gesture == "revenge":
        rv.append(get_quest_guide_path("school_english_class", marker = ["actions go"]))

      return rv
