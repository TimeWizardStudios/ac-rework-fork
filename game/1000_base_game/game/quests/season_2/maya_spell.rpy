image bippity_boppity_bitch_interact = LiveComposite((180,120),(0,25),"items book bippity_boppity_bitch",(95,27),Transform("actions interact",zoom=0.85))
image hex_vex_and_texmex_interact = LiveComposite((180,120),(0,25),"items book hex_vex_and_texmex",(95,27),Transform("actions interact",zoom=0.85))


init python:
  class Quest_maya_spell(Quest):

    title = "Spell You Later"

    class phase_10_book:
      description = "Times of hex and magic."
#     hint = "That old book of odd writing and strange texts. Yeah, read it."
      hint = "That old book of odd writing and{space=-5}\nstrange texts. Yeah, read it."
      def quest_guide_hint(quest):
        if mc.owned_item("bippity_boppity_bitch"):
          return "Interact with the \"Bippity Boppity, Bitch\" book in your inventory."
        elif mc.owned_item("hex_vex_and_texmex"):
#         return "Interact with the \"Hex, Vex, and TexMex\" book in your inventory."
          return "Interact with the \"Hex, Vex, and{space=-5}\nTexMex\" book in your inventory.{space=-5}"

    class phase_20_hair:
      hint = "Hair of the witch. You know which one."
      def quest_guide_hint(_quest):
        if quest.kate_stepping.finished:
          return "Quest [isabelle]."
        elif quest.isabelle_dethroning.finished:
          return "Quest [kate]."

    class phase_30_sweat:
      hint = "The pungent sweat of a lusty, free spirited babe..."
      quest_guide_hint = "Quest [jacklyn] in the art classroom."

    class phase_40_container:
#     hint = "You just need a suitable domicile for your impending frog collection..."
      hint = "You just need a suitable domicile{space=-10}\nfor your impending frog collection..."
      def quest_guide_hint(_quest):
        if quest.flora_cooking_chilli.actually_finished:
          return "Take the cooking pot from the kitchen at home."
        else:
          return "Take the cooking pot from the cafeteria."

    class phase_50_frogs:
      hint = "Frog's breath. No, not yours."
      quest_guide_hint = "Take five frogs from the forest glade."

    class phase_60_maya:
      hint = "Red hair and can turn into a cat at will. Witchcraft!"
      quest_guide_hint = "Quest [maya]."

    class phase_70_midnight:
      hint = "Hiding isn't your forte. You're neither creative nor limber. But at least you're not completely stupid. That's all scientifically proven."
      quest_guide_hint = "Go to the science classroom before school ends."

    class phase_71_busted:
#     hint = "If at first you don't succeed, hide your face and try to suck less."
      hint = "If at first you don't succeed, hide{space=-10}\nyour face and try to suck less."
      quest_guide_hint = "Go to the science classroom before school ends."

    class phase_72_paint:
#     hint = "Time to paint it... silver. Much less edgy."
      hint = "Time to paint it... silver.\nMuch less edgy."
      quest_guide_hint = "Take the silver paint from the paint shelf in the art classroom."

    class phase_73_statue:
      hint = "A statue of many limitations..."
      quest_guide_hint = "Go to the science classroom."

    class phase_80_spell:
      pass

    class phase_1000_done:
      description = "As the clock strikes twelve."


init python:
  class Event_quest_maya_spell(GameEvent):

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_art_class" and quest.maya_spell == "sweat" and not quest.maya_spell["more_than_jacklyn"]:
        game.events_queue.append("quest_maya_spell_sweat_upon_entering")
      elif new_location == (isabelle.location if quest.kate_stepping.finished else "school_first_hall") and quest.maya_spell == "hair" and not quest.maya_spell["must_be_done"]:
        game.events_queue.append("quest_maya_spell_hair_upon_entering")
      elif new_location == ("home_kitchen" if quest.flora_cooking_chilli.actually_finished else "school_cafeteria") and quest.maya_spell == "container" and not quest.maya_spell["house_some_frogs"]:
        game.events_queue.append("quest_maya_spell_container_upon_entering")
      elif new_location == "school_forest_glade" and quest.maya_spell == "frogs" and not quest.maya_spell["down_and_dirty"]:
        game.events_queue.append("quest_maya_spell_frogs_upon_entering")
      elif new_location == "school_science_class" and quest.maya_spell in ("midnight","busted"):
        game.events_queue.append("quest_maya_spell_midnight_upon_entering")
      elif new_location == "school_ground_floor" and quest.maya_spell == "busted" and not quest.maya_spell["amateur_hour"]:
        game.events_queue.append("quest_maya_spell_busted_upon_entering")
      elif new_location == "school_science_class" and quest.maya_spell == "statue":
        game.events_queue.append("quest_maya_spell_statue_upon_entering")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "maya_spell":
        if quest.maya_spell == "container" and mc.owned_item("cooking_pot"):
          mc.remove_item("cooking_pot",silent=True)
        if quest.maya_spell == "frogs":
          quest.maya_spell["frogs_caught"] = set()
          quest.maya_spell["frog_hops"] = 1
        if quest.maya_spell == "midnight" and game.hour in (11,12,13,14,15):
          guard["at_the_cafeteria_this_scene"] = True

    def on_enter_roaming_mode(event):
      if quest.maya_spell == "hair" and game.location == (isabelle.location if quest.kate_stepping.finished else "school_first_hall") and not quest.maya_spell["must_be_done"]:
        game.events_queue.append("quest_maya_spell_hair_pause")
      if quest.maya_spell == "frogs" and mc.owned_item_count("frog") == 5:
        game.events_queue.append("quest_maya_spell_frogs")
      if quest.maya_spell == "spell":
        game.events_queue.append("quest_maya_spell_spell")

    def on_inventory_changed(event,char,item,old_count,new_count,silent=False):
      if "maya_spell" not in game.quests:
        game.quests["maya_spell"] = Quest_maya_spell()
      if quest.maya_spell == "frogs" and item.id == "frog" and new_count > old_count:
        quest.maya_spell["frog_hops"] = 1 if quest.maya_spell["frog_hops"] == 3 else quest.maya_spell["frog_hops"]+1

    def on_advance(event):
      if quest.maya_spell in ("midnight","busted") and game.hour == 19 and game.location != "school_science_class":
        game.events_queue.append("quest_maya_spell_midnight_getting_late")

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.maya_spell in ("paint","statue"):
        return 0

    def on_can_advance_time(event):
      if quest.maya_spell in ("paint","statue"):
        return False

    def on_quest_guide_maya_spell(event):
      rv = []

      if quest.maya_spell == "book":
        if mc.owned_item("bippity_boppity_bitch"):
          rv.append(get_quest_guide_hud("inventory", marker = ["bippity_boppity_bitch_interact"], marker_sector = "mid_top", marker_offset = (60,0)))
        elif mc.owned_item("hex_vex_and_texmex"):
          rv.append(get_quest_guide_hud("inventory", marker = ["hex_vex_and_texmex_interact"], marker_sector = "mid_top", marker_offset = (60,0)))

      elif quest.maya_spell == "hair":
        if quest.kate_stepping.finished:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))
          rv.append(get_quest_guide_marker(game.location, "isabelle", marker = ["actions quest"]))
        elif quest.isabelle_dethroning.finished:
          rv.append(get_quest_guide_path("school_first_hall", marker = ["kate contact_icon@.85"]))
          rv.append(get_quest_guide_marker("school_first_hall", "kate", marker = ["actions quest"], marker_offset = (40,0)))

      elif quest.maya_spell == "sweat":
        rv.append(get_quest_guide_path("school_art_class", marker = ["jacklyn contact_icon@.85"]))
        if jacklyn.at("school_art_class","standing"):
          rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))
        else:
          if mc.at("school_art_class"):
            rv.append(get_quest_guide_hud("time", marker="$Wait for {color=#48F}[jacklyn]{/} between\n"+(("{color=#48F}13:00{/} — {color=#48F}15:00{/}." if persistent.time_format == "24h" else "{color=#48F}1:00 PM{/} — {color=#48F}3:00 PM{/}.") if game.hour == 12 else ("{color=#48F}7:00{/} — {color=#48F}11:00{/}." if persistent.time_format == "24h" else "{color=#48F}7:00 AM{/} — {color=#48F}11:00 AM{/}."))))

      elif quest.maya_spell == "container":
        if quest.flora_cooking_chilli.actually_finished:
          rv.append(get_quest_guide_path("home_kitchen", marker = ["home kitchen cooking_pot@.65"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_cooking_pot", marker = ["actions take"], marker_offset = (0,0)))
        else:
          rv.append(get_quest_guide_path("school_cafeteria", marker = ["school cafeteria pot"]))
          rv.append(get_quest_guide_marker("school_cafeteria", "school_cafeteria_cooking_pot", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (80,-15)))

      elif quest.maya_spell == "frogs":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["items frog"]))
        if "timmy" not in quest.maya_spell["frogs_caught"]:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_frog1", marker = ["actions take"], marker_sector = ("left_bottom" if quest.maya_spell["frog_hops"] in (2,3) else "right_bottom"), marker_offset = ((70,-20) if quest.maya_spell["frog_hops"] in (2,3) else (-10,-10))))
        if "jimmy" not in quest.maya_spell["frogs_caught"]:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_frog2", marker = ["actions take"], marker_sector = ("left_bottom" if quest.maya_spell["frog_hops"] == 1 else "right_bottom"), marker_offset = ((60,-15) if quest.maya_spell["frog_hops"] == 1 else (-10,-10))))
        if "slimmy" not in quest.maya_spell["frogs_caught"]:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_frog3", marker = ["actions take"], marker_sector = ("left_bottom" if quest.maya_spell["frog_hops"] == 3 else "right_bottom"), marker_offset = ((60,-15) if quest.maya_spell["frog_hops"] == 3 else (-10,-20))))
        if "tammy" not in quest.maya_spell["frogs_caught"]:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_frog4", marker = ["actions take"], marker_sector = ("left_bottom" if quest.maya_spell["frog_hops"] in (1,2) else "right_bottom"), marker_offset = ((60,-15) if quest.maya_spell["frog_hops"] in (1,2) else (0,-20))))
        if "frank" not in quest.maya_spell["frogs_caught"]:
          rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_frog5", marker = ["actions take"], marker_offset = ((-10,-20) if quest.maya_spell["frog_hops"] == 3 else (-10,-10))))

      elif quest.maya_spell == "maya":
        rv.append(get_quest_guide_path("school_cafeteria", marker = ["maya contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_cafeteria", "maya", marker = ["actions quest"], marker_offset = (40,0)))

      elif quest.maya_spell == "midnight":
        rv.append(get_quest_guide_path("school_science_class", marker = ["actions go"]))

      elif quest.maya_spell == "busted":
        rv.append(get_quest_guide_path("school_science_class", marker = ["actions go"]))

      elif quest.maya_spell == "paint":
        rv.append(get_quest_guide_path("school_art_class", marker = ["school art_class shelf@.15"]))
        rv.append(get_quest_guide_marker("school_art_class", "school_art_class_shelf", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (580,50)))

      elif quest.maya_spell == "statue":
        rv.append(get_quest_guide_path("school_science_class", marker = ["actions go"]))

      return rv
