init python:
  class Quest_jacklyn_town(Quest):

    title = "Paint the Town"

    class phase_1_wait:
      # description = "Beauty is programmed. It's part of the human psychosis."
      description = "Beauty is programmed. It's part\nof the human psychosis."
      # hint = "Oh, shit! That text from [jacklyn] wasn't a dream! What time is it?!"
      hint = "Oh, shit! That text from [jacklyn] wasn't a dream! What time is it?!{space=-10}"
      def quest_guide_hint(quest):
        return "Wait until " + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 19 else "19:00" if persistent.time_format == "24h" else "7:00 PM") + "."

    class phase_2_suit:
      # hint = "You can't let that suit from [maya] go to waste. A favor from her is rarer than her favorite cut of steak!"
      hint = "You can't let that suit from [maya]{space=-10}\ngo to waste. A favor from her is{space=-5}\nrarer than her favorite cut of steak!"
      def quest_guide_hint(_quest):
        if quest.maya_sauce["bedroom_taken_over"]:
          return "Interact with the suit in the hallway."
        else:
          return "Interact with the suit in your bedroom."

    class phase_3_hairdo:
      hint = "There's an ugly mug that needs polishing up... and it's not [flora]'s \"World's Best Intern\" cup."
      quest_guide_hint = "Interact with either the sink or the mirror in the bathroom."

    class phase_4_ready:
      hint = "It won't get better than that, pal. Now say goodbye to your landlady!"
      quest_guide_hint = "Quest [jo] in the kitchen."

    class phase_5_marina:
      # hint = "I don't know much about fishing, but I know there's a hot girl in fishnets waiting for you."
      hint = "I don't know much about fishing,{space=-10}\nbut I know there's a hot girl in fishnets waiting for you."
      quest_guide_hint = "Leave the house."

    class phase_6_gallery:
      hint = "Time to check if there are any naked depicted French girls around here..."
      # quest_guide_hint = "Interact with all five paintings in Tam's Pancake Brothel."
      quest_guide_hint = "Interact with all five paintings in{space=-10}\nTam's Pancake Brothel."

    class phase_7_pancake_brothel:
      # hint = "You're out of your cave, for once. At least have a look around."
      hint = "You're out of your cave, for once.{space=-10}\nAt least have a look around."
      def quest_guide_hint(quest):
        if all(interactable in quest["interactables"] for interactable in ("door","syrup_fountain","bar_counter","handcuffs","employee_sign","pink_feather_boa")):
          return "Quest the mystery woman."
        else:
          return "Interact with all remaining interactables in Tam's Pancake Brothel."

    class phase_8_flora:
      # hint = "Just follow the trail of tears and sobs to the home of Moaning Myrtle."
      hint = "Just follow the trail of tears and{space=-10}\nsobs to the home of Moaning Myrtle."
      def quest_guide_hint(quest):
        if quest["quiet_sobs"]:
          return "Interact with the door to the bathroom."
        else:
          return "Find [flora]."

    class phase_1000_done_jacklyn:
      # description = "Sometimes, you just have to put your own needs first."
      description = "Sometimes, you just have to\nput your own needs first."

    class phase_1001_done_flora:
      # description = "They say that love conquers all... but the war has just started."
      description = "They say that love conquers\nall... but the war has just\nstarted."

    class phase_1002_done_challenge:
      # description = "Jealousy and intrigue is a sticky web, and once you're stuck, it's hard to get out."
      description = "Jealousy and intrigue is a sticky\nweb, and once you're stuck,\nit's hard to get out."

    class phase_2000_failed:
      # description = "Staying clear of the tangled web of jealousy and intrigue is sometimes the best option."
      description = "Staying clear of the tangled\nweb of jealousy and intrigue is sometimes the best option."


init python:
  class Event_quest_jacklyn_town(GameEvent):

    def on_quest_finished(event,quest_finished,silent=False):
      if quest_finished.id == "jacklyn_romance":
        if not quest.jacklyn_romance["finished_day_tracker"]:
          quest.jacklyn_romance["finished_day_tracker"] = game.day
      elif quest_finished.id == "jacklyn_town":
        jacklyn.outfit = jacklyn["outfit_stamp"]
        if flora["outfit_stamp"]:
          flora.outfit = flora["outfit_stamp"]

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.jacklyn_town == "not_started" and (quest.jacklyn_romance["finished_day_tracker"]+4) == game.day and game.hour == 23)
      or quest.jacklyn_town in ("suit","hairdo","ready","marina","gallery","pancake_brothel","flora")):
        return 0

    def on_can_advance_time(event):
      if ((quest.jacklyn_town == "not_started" and (quest.jacklyn_romance["finished_day_tracker"]+4) == game.day and game.hour == 23)
      or quest.jacklyn_town in ("suit","hairdo","ready","marina","gallery","pancake_brothel","flora")):
        return False

    def on_advance(event):
      if quest.fall_in_newfall.finished and quest.jacklyn_romance.finished and not (quest.jacklyn_town.started or quest.jacklyn_town.failed) and not mc["focus"] and (quest.jacklyn_romance["finished_day_tracker"] + 5) == game.day:
        game.events_queue.append("quest_jacklyn_town_start")
      elif quest.jacklyn_town == "wait" and game.hour == 19:
        game.events_queue.append("quest_jacklyn_town_wait")

    def on_enter_roaming_mode(event):
      if quest.jacklyn_town == "gallery" and all(painting in quest.jacklyn_town["paintings"] for painting in ("the_cream","the_moan_aaah_lisa","the_birth_of_penis","the_furry_night","the_fast_supper")):
        game.events_queue.append("quest_jacklyn_town_gallery")
      elif quest.jacklyn_town == "pancake_brothel" and all(interactable in quest.jacklyn_town["interactables"] for interactable in ("door","syrup_fountain","bar_counter","handcuffs","employee_sign","pink_feather_boa","mystery_woman")):
        game.events_queue.append("quest_jacklyn_town_pancake_brothel")

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.jacklyn_town == "flora" and new_location == "home_hall" and not quest.jacklyn_town["quiet_sobs"]:
        game.events_queue.append("quest_jacklyn_town_flora_upon_entering")

    def on_quest_guide_jacklyn_town(event):
      rv = []

      if quest.jacklyn_town == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait until {color=#48F}" + (game.dow_names[0 if game.dow == 6 else game.dow+1] if game.hour > 19 else "19:00" if persistent.time_format == "24h" else "7:00 PM") + "{/}."))
        if game.hour > 19 and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        elif game.hour > 19:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.jacklyn_town == "suit":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_marker("home_hall","home_bedroom_suit", marker = ["actions interact"], marker_offset = (60,-20)))
        else:
          rv.append(get_quest_guide_marker("home_bedroom","home_bedroom_suit", marker = ["actions interact"], marker_offset = (60,-20)))

      elif quest.jacklyn_town == "hairdo":
        rv.append(get_quest_guide_path("home_bathroom", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_mirror", marker = ["$or{space=-50}","actions interact","${space=50}"], marker_sector = "right_top", marker_offset = (100,-60)))
        rv.append(get_quest_guide_marker("home_bathroom", "home_bathroom_sink", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (60,130)))

      elif quest.jacklyn_town == "ready":
        rv.append(get_quest_guide_path("home_kitchen", marker = ["jo contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_kitchen", "jo", marker = ["actions quest"], marker_offset = (30,0)))

      elif quest.jacklyn_town == "marina":
        rv.append(get_quest_guide_path("marina", marker = ["actions go"]))

      elif quest.jacklyn_town == "gallery":
        if "the_cream" not in quest.jacklyn_town["paintings"]:
          rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_the_cream", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (130,-185)))
        if "the_moan_aaah_lisa" not in quest.jacklyn_town["paintings"]:
          rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_the_moan_aaah_lisa", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (135,210)))
        if "the_birth_of_penis" not in quest.jacklyn_town["paintings"]:
          rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_the_birth_of_penis", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (140,-10)))
        if "the_furry_night" not in quest.jacklyn_town["paintings"]:
          rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_the_furry_night", marker = ["actions interact"], marker_sector = "left_bottom", marker_offset = (60,-15)))
        if "the_fast_supper" not in quest.jacklyn_town["paintings"]:
          rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_the_fast_supper", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (20,195)))

      elif quest.jacklyn_town == "pancake_brothel":
        if all(interactable in quest.jacklyn_town["interactables"] for interactable in ("door","syrup_fountain","bar_counter","handcuffs","employee_sign","pink_feather_boa")):
          rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_mystery_woman", marker = ["actions quest"], marker_offset = (0,-5)))
        else:
          if "door" not in quest.jacklyn_town["interactables"]:
            rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_door", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (70,170)))
          if "syrup_fountain" not in quest.jacklyn_town["interactables"]:
            rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_syrup_fountain", marker = ["actions interact"], marker_offset = (-10,30)))
          if "bar_counter" not in quest.jacklyn_town["interactables"]:
            rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_bar_counter", marker = ["actions interact"], marker_sector = "right_top", marker_offset = (70,125)))
          if "handcuffs" not in quest.jacklyn_town["interactables"]:
            rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_handcuffs", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (75,115)))
          if "employee_sign" not in quest.jacklyn_town["interactables"]:
            rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_employee_sign", marker = ["actions interact"], marker_offset = (15,-20)))
          if "pink_feather_boa" not in quest.jacklyn_town["interactables"]:
            rv.append(get_quest_guide_marker("pancake_brothel", "pancake_brothel_pink_feather_boa", marker = ["actions interact"], marker_sector = "left_top", marker_offset = (80,-300)))

      elif quest.jacklyn_town == "flora":
        if quest.jacklyn_town["quiet_sobs"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall door_white@.35"]))
          rv.append(get_quest_guide_marker("home_hall", "home_hall_door_bathroom", marker = ["actions interact"]))
        else:
          rv.append(get_quest_guide_path("home_hall", marker = ["flora contact_icon@.85"]))

      return rv
