image text_isabelle = LiveComposite((0,0),(63,-53),Transform("isabelle contact_icon",zoom=0.69),(149,-29),Transform("phone apps contact_info text",zoom=0.5))


init python:
  class Quest_kate_moment(Quest):

    title = "A Single Moment"

    class phase_1_gym:
      description = "A single moment during an\nautumn day, at some point in time."
      hint = "The former domain of the fallen queen."
      quest_guide_hint = "Go to the gym."

    class phase_2_nurse_room:
      hint = "In sickness and in unhealth, this is the place."
      def quest_guide_hint(_quest):
        if game.location == "school_nurse_room" and quest.nurse_aid["name_reveal"]:
          return "Quest [amelia]."
        elif game.location == "school_nurse_room":
          return "Quest the [nurse]."
        elif quest.nurse_aid["name_reveal"]:
          return "Go to the nurse's office."
        else:
          return "Go to the [nurse]'s office."

    class phase_3_jacklyn:
      hint = "The girl most likely to have a dragon tattoo."
      def quest_guide_hint(quest):
        if game.location == "school_art_class":
          return "Quest [jacklyn]."
        else:
          return "Go to the art classroom."

    class phase_4_maxine:
      hint = "The girl most likely to have a dragon."
      quest_guide_hint = "Quest [maxine]."

    class phase_5_nurse:
      hint = "The girl most likely to have a dragon's hoard of candy."
      def quest_guide_hint(_quest):
        if quest.nurse_aid["name_reveal"]:
          return "Quest [amelia] again."
        else:
          return "Quest the [nurse] again."

    class phase_6_jo:
      hint = "The girl most likely to have a dragon's temper if you're late for school."
      quest_guide_hint = "Quest [jo]."

    class phase_7_break_in:
      hint = "Trashcan. Rock. Door. Shoot!"
      def quest_guide_hint(quest):
        if quest["locked_and_unbreakable"]:
          if mc.owned_item("rock"):
            return "Use the rock on the door to the principal's office."
          else:
            return "Take the rock from the trash bin in the first hall."
        else:
          return "Go to the principal's office."

    class phase_8_principal_office:
      hint = "Some surf and strap in the principal's office, minus the strap."
      quest_guide_hint = "Interact with the computer."

    class phase_9_coming_soon:
      hint = "Fixing things with [isabelle] will take no small gesture."
      def quest_guide_hint(_quest):
        if quest.isabelle_gesture.started:
          return "Finish \"The Grand Gesture.\""
        else:
          return "Go to the " + isabelle.location.title.lower().replace("art class","art classroom").replace("english class","English classroom").replace("forest","forest glade") + "."

    class phase_10_text:
      hint = "Text the girl with a green sweater and black skirt that you like the most."
      quest_guide_hint = "Text [isabelle] on your phone."

    class phase_11_fun:
      hint = "Wait until the clock strikes five hours to midnight."
      def quest_guide_hint(quest):
        if game.hour < 19:
          return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."
        else:
          return "Quest [isabelle]."

    class phase_12_sleep:
      hint = "No more point being awake, is there?"
      quest_guide_hint = "Go to sleep."

    class phase_13_next_day:
      hint = "The girl that is most likely to be a dragon... but who knows at this point?"
      quest_guide_hint = "Quest [kate]."

    class phase_1000_done:
      description = "Sometimes, all it takes is\na single moment."

    class phase_2000_failed:
      description = "A man without glory is a man without a single moment."


init python:
  class Event_quest_kate_moment(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if ((quest.kate_moment.in_progress and not quest.kate_moment in ("coming_soon","fun","sleep"))
      or quest.kate_moment == "fun" and game.hour == 19):
        return 0

    def on_can_advance_time(event):
      if ((quest.kate_moment.in_progress and not quest.kate_moment in ("coming_soon","fun","sleep"))
      or quest.kate_moment == "fun" and game.hour == 19):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_first_hall_west" == kate.location and quest.isabelle_dethroning.finished and quest.fall_in_newfall.finished and not (quest.kate_moment.started or quest.kate_moment.failed) and not mc["focus"]:
        game.events_queue.append("quest_kate_moment_start")
      elif new_location == "school_gym" and quest.kate_moment == "gym":
        game.events_queue.append("quest_kate_moment_gym")
      elif new_location == "school_art_class" and quest.kate_moment == "jacklyn" and not quest.kate_moment["actual_art"]:
        game.events_queue.append("quest_kate_moment_jacklyn_art_class")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "kate_moment":
        if quest.kate_moment == "break_in":
          school_ground_floor_west["exclusive"] = "True"
        if quest.kate_moment == "principal_office":
          school_ground_floor_west["exclusive"] = False

    def on_advance(event):
      if quest.kate_moment == "fun" and game.hour == 19:
        game.events_queue.append("quest_kate_moment_fun")
      if quest.kate_moment == "sleep" and game.hour == 7:
        kate["at_none"] = False
        quest.kate_moment.advance("next_day")

    def on_enter_roaming_mode(event):
      if quest.isabelle_gesture["released"] and not mc["focus"]:
        renpy.pause(.5)
        kate["at_none"] = True
        mc["focus"] = "kate_moment"
        quest.kate_moment.hidden = False
        game.quest_guide = game.quest_guide if game.quest_guide else "kate_moment"
        renpy.transition(Dissolve(.25),layer="screens")

    def on_quest_guide_kate_moment(event):
      rv = []

      if quest.kate_moment == "gym":
        rv.append(get_quest_guide_path("school_gym", marker = ["actions go"]))

      elif quest.kate_moment == "nurse_room":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "nurse", marker = ["actions quest"], marker_offset = (120,0)))

      elif quest.kate_moment == "jacklyn":
        rv.append(get_quest_guide_path("school_art_class", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_art_class", "jacklyn", marker = ["actions quest"]))

      elif quest.kate_moment == "maxine":
        rv.append(get_quest_guide_char("maxine", marker = ["maxine contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maxine", marker = ["actions quest"]))

      elif quest.kate_moment == "nurse":
        rv.append(get_quest_guide_char("nurse", marker = ["nurse contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "nurse", marker = ["actions quest"]))

      elif quest.kate_moment == "jo":
        rv.append(get_quest_guide_char("jo", marker = ["jo contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "jo", marker = ["actions quest"]))

      elif quest.kate_moment == "break_in":
        if quest.kate_moment["locked_and_unbreakable"]:
          if mc.owned_item("rock"):
            rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west headmasterdoor@.275"]))
            rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_principal_door", marker = ["items rock@.85"], marker_sector = "left_bottom", marker_offset = (110,-50)))
          else:
            rv.append(get_quest_guide_path("school_first_hall", marker = ["school first_hall bin@.85"]))
            rv.append(get_quest_guide_marker("school_first_hall", "school_first_hall_bin", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (50,0)))
        else:
          rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west headmasterdoor@.275"]))
          rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_principal_door", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (110,-50)))

      elif quest.kate_moment == "principal_office":
        rv.append(get_quest_guide_marker("school_principal_office", "school_principal_office_computer", marker = ["actions interact"], marker_offset = (90,-10)))

      elif quest.kate_moment == "coming_soon":
        if quest.isabelle_gesture.started:
          rv.append(get_quest_guide_hud("quest_guide", marker="$Finish {color=#48F}The Grand Gesture{/}."))
        else:
          rv.append(get_quest_guide_char("isabelle", marker = ["isabelle contact_icon@.85"], force_marker = True))

      elif quest.kate_moment == "text":
        rv.append(get_quest_guide_hud("phone", marker=["text_isabelle","$\n\n\n{space=20}Text {color=#48F}[isabelle]{/}."]))

      elif quest.kate_moment == "fun":
        if game.hour < 19:
          rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 PM{/}.")))
        elif quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_marker("home_hall", "isabelle", marker = ["actions quest"], marker_offset = (15,0)))
        else:
          rv.append(get_quest_guide_marker("home_bedroom", "isabelle", marker = ["actions quest"], marker_offset = (15,0)))

      elif quest.kate_moment == "sleep":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.kate_moment == "next_day":
        rv.append(get_quest_guide_path("school_first_hall_west", marker = ["kate contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_first_hall_west", "kate", marker = ["actions quest"], marker_offset = (75,0)))

      return rv
