#image go_from_school_to_marina = LiveComposite((103,108),(-130,13),Text("Go to the\nmarina",style="quest_guide_marker_text"),(0,0),"actions go")
#image go_from_home_to_marina = LiveComposite((103,108),(0,0),"actions go",(115,15),Text("Go to the\nmarina",style="quest_guide_marker_text"))


init python:
  class Quest_lindsey_angel(Quest):

    title = "Touched by an Angel"

    class phase_1_good_news:
      description = "The girl that lived."
      hint = "You couldn't catch her, but maybe you can comfort her?"
      def quest_guide_hint(quest):
        if game.location == "marina":
          return "Go to the Newfall Hospital."
        else:
          return "Go to the Newfall Marina."

    class phase_2_nurse:
      hint = "Only one person you know works at the hospital. Which one is it?"
      def quest_guide_hint(_quest):
        if quest.nurse_aid["name_reveal"]:
          return "Quest [amelia]."
        else:
          return "Quest the [nurse]."

    class phase_3_wait:
      hint = "A nightly key infiltration."
      def quest_guide_hint(quest):
        return "Wait until 19:00." if persistent.time_format == "24h" else "Wait until 7:00 PM."

    class phase_4_keycard:
      hint = "A nightly key infiltration."
      def quest_guide_hint(_quest):
        if quest.nurse_aid["name_reveal"]:
          return "Take the keycard from the nurse's office."
        else:
          return "Take the keycard from the [nurse]'s office."

    class phase_5_off_limits:
      hint = "There's only one place with lab coats. It doesn't take a rocket scientist to know where."
      quest_guide_hint = "Go to the science classroom."

    class phase_6_phone_book:
      hint = "Numbers in a book. An archaic concept."
      quest_guide_hint = "Interact with the bookshelf in the admin wing."

    class phase_7_lab_coat:
      hint = "Not all heroes wear a cape. Some wear a lab coat."
      quest_guide_hint = "Take the lab coat from the science classroom."

    class phase_8_escape:
      hint = "Not all heroes wear a cape. Some wear a lab coat."
      quest_guide_hint = "Leave the school."

    class phase_9_hospital:
      hint = "Let's get phys-ician."
      quest_guide_hint = "Go to the Newfall Hospital."

    class phase_1000_done:
      description = "Things break and fall apart, but\nlove always finds a way."

    class phase_2000_failed:
      description = "She'll be fine, and so will you."


init python:
  class Event_quest_lindsey_angel(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape","hospital"):
        return 0

    def on_can_advance_time(event):
      if quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape","hospital"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "school_ground_floor" and quest.fall_in_newfall.finished and not (quest.lindsey_angel.started or quest.lindsey_angel.failed) and not quest.fall_in_newfall["finished_this_week"] and not mc["focus"]:
        game.events_queue.append("quest_lindsey_angel_start")
      elif new_location == "marina" and quest.lindsey_angel == "good_news" and not quest.jacklyn_town == "marina" and not quest.lindsey_angel["constant_support"]:
        game.events_queue.append("quest_lindsey_angel_good_news")
      elif new_location == "hospital" and quest.lindsey_angel == "good_news":
        game.events_queue.append("quest_lindsey_angel_good_news_hospital")
      elif new_location == "school_nurse_room" and quest.lindsey_angel == "keycard" and not quest.lindsey_angel["V-card"]:
        game.events_queue.append("quest_lindsey_angel_keycard_school_nurse_room")
      elif new_location == "school_ground_floor" and quest.lindsey_angel == "off_limits":
        game.events_queue.append("quest_lindsey_angel_off_limits_school_ground_floor")
      elif new_location == "school_ground_floor_west" and quest.lindsey_angel == "phone_book" and not quest.lindsey_angel["around_here_somewhere"]:
        game.events_queue.append("quest_lindsey_angel_phone_book_school_ground_floor_west")
      elif new_location == "school_ground_floor" and quest.lindsey_angel == "lab_coat" and quest.lindsey_angel["troll_outside_its_cave"]:
        game.events_queue.append("quest_lindsey_angel_lab_coat_school_ground_floor")
      elif new_location == "school_science_class" and quest.lindsey_angel == "lab_coat":
        game.events_queue.append("quest_lindsey_angel_lab_coat_school_science_class")
      elif new_location == "school_entrance" and quest.lindsey_angel == "escape":
        game.events_queue.append("quest_lindsey_angel_escape")

    def on_advance(event):
      if quest.lindsey_angel == "wait" and game.hour == 19 and not mc["focus"]:
        game.events_queue.append("quest_lindsey_angel_wait")
        quest.lindsey_angel.advance("keycard")

    def on_quest_guide_lindsey_angel(event):
      rv = []

      if quest.lindsey_angel == "good_news":
#       if game.location == "school_entrance":
#         rv.append(get_quest_guide_marker("school_entrance", "school_entrance_bus_stop", marker = ["go_from_school_to_marina"]))
#       elif game.location == "home_kitchen":
#         rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_door", marker = ["go_from_home_to_marina"], marker_sector = "left_top", marker_offset = (100,150)))
#       else:
        rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("marina", "marina_hospital", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (300,-70)))

      elif quest.lindsey_angel == "nurse":
        rv.append(get_quest_guide_char("nurse", marker = ["nurse contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "nurse", marker = ["actions quest"]))

      elif quest.lindsey_angel == "wait":
        rv.append(get_quest_guide_hud("time", marker="$Wait until "+("{color=#48F}19:00{/}." if persistent.time_format == '24h' else "{color=#48F}7:00 PM{/}.")))

      elif quest.lindsey_angel == "keycard":
        rv.append(get_quest_guide_path("school_nurse_room", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_nurse_room", "school_nurse_room_keycard", marker = ["actions take"], marker_offset = (15,-10)))

      elif quest.lindsey_angel == "off_limits":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      elif quest.lindsey_angel == "phone_book":
        rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["school ground_floor_west bookshelf@.4"]))
        rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_bookshelf", marker = ["actions interact"]))

      elif quest.lindsey_angel == "lab_coat":
        rv.append(get_quest_guide_path("school_science_class", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("school_science_class", "school_science_class_lab_coat", marker = ["actions take"]))

      elif quest.lindsey_angel == "escape":
        rv.append(get_quest_guide_path("school_entrance", marker = ["actions go"]))

      elif quest.lindsey_angel == "hospital":
        rv.append(get_quest_guide_path("hospital", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("hospital", "hospital_glass_door", marker = ["items keycard@.85"], marker_offset = (275,0)))

      return rv
