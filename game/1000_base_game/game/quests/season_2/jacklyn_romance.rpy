init python:
  class Quest_jacklyn_romance(Quest):

    title = "Suitable Romance"

    class phase_1_suit:
      description = "You and me, baby, we could weave\na suitable romance."
      hint = "The place where deep sea meets{space=-10}\ndeep seam."
      def quest_guide_hint(quest):
        if quest["what_better_place"]:
          return "Go to the Newfall Tailor & Aquarium."
        else:
          return "Go to the Newfall Marina."

    class phase_2_flora:
      hint = "Time to pull out the big guns and{space=-15}\nget some help from the brat\nfashionista herself."
      quest_guide_hint = "Quest [flora]."

    class phase_3_maya:
      hint = "There's only one girl around here who really knows her way around some thread... among other things."
      quest_guide_hint = "Quest [maya]."

    class phase_4_hide_and_seek:
      hint =  "Time to search every nook, cranny, and fanny for some yarn.{space=-5}"
      def quest_guide_hint(quest):
        return "Take [maya]'s balls of yarn in the art classroom (yellow), the gym (blue), the entrance hall (pink), the admin wing (gray), and the clubroom (red). \n\nTotal balls of yarn found: "+str(quest["balls_of_yarn_found"])+"/5"

    class phase_5_found_them:
      hint = "Yarn for the yarn god."
      quest_guide_hint = "Quest [maya] again."

    class phase_6_wait:
      hint = "Waiting to get fashionable? Suits me fine."
      def quest_guide_hint(quest):
        if (quest["suit_done"]+2) - game.day == 1:
          return "Wait a day."
        else:
          return "Wait 2 days."

    class phase_7_suit_done:
      pass

    class phase_1000_done:
      description = "Every day, a little bit better, a little bit smarter, a little bit prettier."

    class phase_1001_done_but_sexless:
      description = "Incredibly unfashionable.\nStraight démodé."


init python:
  class Event_quest_jacklyn_romance(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.jacklyn_romance == "wait" and (quest.jacklyn_romance["suit_done"]+1) == game.day and game.hour == 23:
        return 0

    def on_can_advance_time(event):
      if quest.jacklyn_romance == "wait" and (quest.jacklyn_romance["suit_done"]+1) == game.day and game.hour == 23:
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "marina" and quest.jacklyn_romance == "suit" and not quest.jacklyn_romance["what_better_place"]:
        game.events_queue.append("quest_jacklyn_romance_suit_upon_entering")
      elif new_location == "school_ground_floor" and quest.jacklyn_romance == "hide_and_seek" and not quest.jacklyn_romance["so_many_options"]:
        game.events_queue.append("quest_jacklyn_romance_hide_and_seek_school_ground_floor_upon_entering")
      elif new_location == "school_clubroom" and quest.jacklyn_romance == "hide_and_seek" and not quest.jacklyn_romance["got_to_be_spinach"]:
        game.events_queue.append("quest_jacklyn_romance_hide_and_seek_school_clubroom_upon_entering")
      elif new_location == "school_art_class" and quest.jacklyn_romance == "hide_and_seek" and not quest.jacklyn_romance["revoke_the_invitation"]:
        game.events_queue.append("quest_jacklyn_romance_hide_and_seek_school_art_class_upon_entering")
      elif new_location == "school_gym" and quest.jacklyn_romance == "hide_and_seek" and not quest.jacklyn_romance["work_some_magic"]:
        game.events_queue.append("quest_jacklyn_romance_hide_and_seek_school_gym_upon_entering")

    def on_enter_roaming_mode(event):
      if quest.jacklyn_romance == "hide_and_seek" and quest.jacklyn_romance["balls_of_yarn_found"] == 5:
        game.events_queue.append("quest_jacklyn_romance_hide_and_seek_found_them")

    def on_quest_advanced(event,quest_advanced,silent=False):
      if quest_advanced.id == "jacklyn_romance" and quest.jacklyn_romance == "wait":
        quest.jacklyn_romance["suit_done"] = game.day

    def on_advance(event):
      if quest.jacklyn_romance == "wait" and (quest.jacklyn_romance["suit_done"]+2) == game.day:
        quest.jacklyn_romance.advance("suit_done")
      elif quest.jacklyn_romance == "suit_done":
        game.events_queue.append("quest_jacklyn_romance_suit_done")

    def on_quest_guide_jacklyn_romance(event):
      rv = []

      if quest.jacklyn_romance == "suit":
        rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
        rv.append(get_quest_guide_marker("marina", "marina_tailor_aquarium", marker = ["actions go"], marker_sector = "left_bottom", marker_offset = (175,25)))

      elif quest.jacklyn_romance == "flora":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))

      elif quest.jacklyn_romance == "maya":
        rv.append(get_quest_guide_char("maya", marker = ["maya contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maya", marker = ["actions quest"]))

      elif quest.jacklyn_romance == "hide_and_seek":
        if not school_ground_floor["ball_of_yarn_taken"]:
          rv.append(get_quest_guide_path("school_ground_floor", marker = ["items ball_of_yarn_pink"]))
          rv.append(get_quest_guide_marker("school_ground_floor", "school_ground_floor_ball_of_yarn", marker = ["actions take"], marker_offset = (-10,-10)))
        if not school_clubroom["ball_of_yarn_taken"]:
          rv.append(get_quest_guide_path("school_clubroom", marker = ["items ball_of_yarn_red"]))
          rv.append(get_quest_guide_marker("school_clubroom", "spinach", marker = ["actions interact"], marker_offset = (0,0)))
        if not school_ground_floor_west["ball_of_yarn_taken"]:
          rv.append(get_quest_guide_path("school_ground_floor_west", marker = ["items ball_of_yarn_gray"]))
          rv.append(get_quest_guide_marker("school_ground_floor_west", "school_ground_floor_west_ball_of_yarn", marker = ["actions take"], marker_offset = (-10,-10)))
        if not school_art_class["ball_of_yarn_taken"]:
          rv.append(get_quest_guide_path("school_art_class", marker = ["items ball_of_yarn_yellow"]))
          rv.append(get_quest_guide_marker("school_art_class", "school_art_class_ball_of_yarn", marker = ["actions take"], marker_sector = "left_bottom", marker_offset = (50,-5)))
        if not school_gym["ball_of_yarn_taken"]:
          rv.append(get_quest_guide_path("school_gym", marker = ["items ball_of_yarn_blue"]))
          rv.append(get_quest_guide_marker("school_gym", "school_gym_ball_of_yarn", marker = ["actions take"], marker_offset = (-10,-10)))

      elif quest.jacklyn_romance == "found_them":
        rv.append(get_quest_guide_char("maya", marker = ["maya contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "maya", marker = ["actions quest"]))

      elif quest.jacklyn_romance == "wait":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        if (quest.jacklyn_romance["suit_done"]+2) - game.day == 1:
          rv.append(get_quest_guide_hud("time", marker="$Wait a {color=#48F}day{/}."))
        else:
          rv.append(get_quest_guide_hud("time", marker="$Wait {color=#48F}2{/} days."))

      return rv
