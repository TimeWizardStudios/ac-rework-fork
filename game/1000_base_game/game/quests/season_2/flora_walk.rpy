init python:
  class Quest_flora_walk(Quest):

    title = "A Walk in the Park"

    class phase_1_xcube:
#     description = "Sometimes, the best things in life are free."
      description = "Sometimes, the best things\nin life are free."
      hint = "Some quality time with your... flower."
      def quest_guide_hint(_quest):
        if quest.maya_sauce["bedroom_taken_over"]:
          return "Quest [flora] in your old bedroom."
        else:
          return "Quest [flora] in your bedroom."

    class phase_2_park:
      hint = "An autumn day in the park? What has become of your life?"
      def quest_guide_hint(quest):
        if game.hour in (19,20):
          return "Go to sleep."
        else:
          return "Go to the park."

    class phase_3_sleep:
      hint = "One day at a time, and this one's over."
      quest_guide_hint = "Go to sleep."

    class phase_4_confrontation:
      hint = "School. That's why you're here."
      quest_guide_hint = "Go to school."

    class phase_5_caught:
      hint = "Bad news. Flower-wiltingly bad."
      quest_guide_hint = "Quest [flora]."

    class phase_6_forest_glade:
      hint = "To the forest to meet your maker."
#     quest_guide_hint = "Go to the forest glade and quest either [flora] or [isabelle]."
      quest_guide_hint = "Go to the forest glade and quest{space=-5}\neither [flora] or [isabelle]."

    class phase_1000_done:
#     description = "All's well that ends well. For now."
      description = "All's well that ends well.\nFor now."


init python:
  class Event_quest_flora_walk(GameEvent):

    def on_energy_cost(event,interactable,energy_cost,silent=False):
      if quest.flora_walk in ("xcube","park","confrontation","caught","forest_glade"):
        return 0

    def on_can_advance_time(event):
      if quest.flora_walk in ("xcube","park","confrontation","caught","forest_glade"):
        return False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location == "home_bedroom" and quest.flora_walk == "xcube" and not quest.flora_walk["hanging_out"]:
        game.events_queue.append("quest_flora_walk_xcube_upon_entering")
      elif new_location == "school_ground_floor" and quest.flora_walk == "confrontation":
        game.events_queue.append("quest_flora_walk_confrontation")
      elif new_location == "school_forest_glade" and quest.flora_walk == "forest_glade":
        game.events_queue.append("quest_flora_walk_forest_glade_upon_entering")

    def on_quest_guide_flora_walk(event):
      rv = []

      if quest.flora_walk == "xcube":
        rv.append(get_quest_guide_path("home_bedroom", marker = ["flora contact_icon@.85"]))
        rv.append(get_quest_guide_marker("home_bedroom", "flora", marker = ["actions quest"], marker_sector = "left_bottom", marker_offset = (175,0)))

      elif quest.flora_walk == "park":
        if game.hour in (19,20) and quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        elif game.hour in (19,20):
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))
        else:
          rv.append(get_quest_guide_path("school_park", marker = ["actions go"]))

      elif quest.flora_walk == "sleep":
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(get_quest_guide_path("home_hall", marker = ["home hall bed@.175"]))
          rv.append(get_quest_guide_marker("home_hall", "home_bedroom_bed", marker = ["actions go"], marker_offset = (100,20)))
        else:
          rv.append(get_quest_guide_path("home_bedroom", marker = ["home bedroom bed@.25"]))
          rv.append(get_quest_guide_marker("home_bedroom", "home_bedroom_bed", marker = ["actions go"], marker_offset = (0,20)))

      elif quest.flora_walk == "confrontation":
        rv.append(get_quest_guide_path("school_ground_floor", marker = ["actions go"]))

      elif quest.flora_walk == "caught":
        rv.append(get_quest_guide_char("flora", marker = ["flora contact_icon@.85"], force_marker = True))
        rv.append(get_quest_guide_marker(game.location, "flora", marker = ["actions quest"]))

      elif quest.flora_walk == "forest_glade":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["isabelle contact_icon@.85"]))
        rv.append(get_quest_guide_marker("school_forest_glade", "flora", marker = ["actions quest"], marker_offset = (10,0)))
        rv.append(get_quest_guide_marker("school_forest_glade", "isabelle", marker = ["$or{space=-50}","actions quest","${space=50}"], marker_sector = "left_bottom", marker_offset = (115,0)))

      return rv
