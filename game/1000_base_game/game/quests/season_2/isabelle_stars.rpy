image trees_only_autumn = Crop((0,0,1920,475),"school forest_glade trees_autumn")


init python:
  class Quest_isabelle_stars(Quest):

    title = "Under the Stars"

    class phase_1_telescope:
      # description = "Like the stars, love lights up the night."
      description = "Like the stars, love\nlights up the night."
      hint = "In the highest of places, seeing furthest of all."
      quest_guide_hint = "Take the telescope on the roof."

    class phase_2_fire_axe:
      hint = "A famous Gimli quote comes to mind."
      quest_guide_hint = "Take the fire axe in the roof landing."

    class phase_3_wood:
      hint = "...and he's okay. He works all night and he sleeps all day."
      quest_guide_hint = "Use the fire axe on the trees in the forest glade."

    class phase_4_takeout:
      hint = "Money solves everything, especially if you're hungry or preparing a meal."
      def quest_guide_hint(quest):
        if quest["broke_bitch"] and mc.money < 10:
          return "Gather $10."
        elif game.location == "marina" or quest["no_better_place"]:
          return "Interact with The Prawn Star" + (" in the Newfall Marina." if quest["no_better_place"] else ".")
        else:
          return "Go to the Newfall Marina."

    class phase_5_homemade:
      # hint = "Cold, filled, and edible. No, I'm not talking about vampire pussy."
      hint = "Cold, filled, and edible. No, I'm not talking about vampire pussy.{space=-5}"
      def quest_guide_hint(quest):
        if quest["meal"]:
          return "Interact with the mini fridge."
        else:
          return "Go to the home kitchen."

    class phase_6_text:
      hint = "Time to sext, or whatever you kids call it nowadays."
      quest_guide_hint = "Text [isabelle] on your phone."

    class phase_7_stargaze:
      pass

    class phase_1000_done:
      # description = "When the stars align, even the beast can find his beauty."
      description = "When the stars align, even\nthe beast can find his beauty."

    class phase_1001_done_feelings_rejected:
      # description = "Times change and so does the mood. Settling down is for old people."
      description = "Times change and so does\nthe mood. Settling down is\nfor old people."


init python:
  class Event_quest_isabelle_stars(GameEvent):

    def on_location_changed(event,old_location,new_location,silent=False):
      if quest.isabelle_stars == "wood" and new_location == "school_forest_glade":
        game.events_queue.append("quest_isabelle_stars_wood_upon_entering")
      elif quest.isabelle_stars == "takeout" and new_location == "marina" and not quest.isabelle_stars["no_better_place"]:
        game.events_queue.append("quest_isabelle_stars_takeout_upon_entering")
        quest.isabelle_stars["no_better_place"] = True
      elif quest.isabelle_stars == "homemade" and new_location == "home_kitchen" and not quest.isabelle_stars["culinary_genius"]:
        game.events_queue.append("quest_isabelle_stars_homemade_upon_entering")
        quest.isabelle_stars["culinary_genius"] = True

    def on_quest_finished(event,quest_finished,silent=False):
      if quest_finished.id == "isabelle_stars":
        del school_roof.flags["telescope_taken"]

    def on_quest_guide_isabelle_stars(event):
      rv = []

      if quest.isabelle_stars == "telescope":
        rv.append(get_quest_guide_path("school_roof", marker = [("school roof telescope_left@.375" if school_roof["telescope_moved"] else "school roof telescope_right@.55")]))
        rv.append(get_quest_guide_marker("school_roof", "school_roof_telescope", marker = ["actions take"], marker_offset=(-12,-12) if school_roof["telescope_moved"] else (0,0)))

      elif quest.isabelle_stars == "fire_axe":
        rv.append(get_quest_guide_path("school_roof_landing", marker = ["fire_axe_cropped@.2"]))
        rv.append(get_quest_guide_marker("school_roof_landing", "school_roof_landing_fire_axe", marker = ["actions take"], marker_sector = "right_top", marker_offset=(75,-200)))

      elif quest.isabelle_stars == "wood":
        rv.append(get_quest_guide_path("school_forest_glade", marker = ["trees_only_autumn@.1"]))
        rv.append(get_quest_guide_marker("school_forest_glade", "school_forest_glade_trees", marker = ["items fire_axe@.85"], marker_offset=(840,10)))

      elif quest.isabelle_stars == "takeout":
        if quest.isabelle_stars["broke_bitch"] and mc.money < 10:
          rv.append(get_quest_guide_hud("time", marker="$Gather {color=#48F}$10{/}.", marker_offset = (471+(len(str(mc.money))*10),0)))
        else:
          rv.append(get_quest_guide_path("marina", marker = ["actions go"]))
          rv.append(get_quest_guide_marker("marina", "marina_prawn_star", marker = ["interact_left_mid"], marker_sector = "left_mid", marker_offset = (495,-235)))

      elif quest.isabelle_stars == "homemade":
          rv.append(get_quest_guide_path("home_kitchen", marker = ["actions go"]))
          rv.append(get_quest_guide_marker("home_kitchen", "home_kitchen_mf", marker = ["actions interact"]))

      elif quest.isabelle_stars == "text":
        rv.append(get_quest_guide_hud("phone", marker=["text_isabelle","$\n\n\n{space=20}Text {color=#48F}[isabelle]{/}."]))

      return rv
