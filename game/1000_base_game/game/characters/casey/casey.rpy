init python:
  class Character_retinue(BaseChar): ## For when they all say the same thing at the same time

    default_name = "{size=24}Lacey, Casey \n  and Stacy{/}"

style say_name_frame_retinue is say_name_frame_berb:
  padding (30,10,30,16)


init python:
  class Character_casey(BaseChar):

    notify_level_changed=True

    default_name="Casey"

    default_stats=[
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon="casey contact_icon"

    default_outfit_slots=["top","bra","skirt","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("casey_cheerleader_top")
      self.add_item("casey_cheerleader_bra")
      self.add_item("casey_cheerleader_skirt")
      self.add_item("casey_cheerleader_panties")
      self.equip("casey_cheerleader_top")
      self.equip("casey_cheerleader_bra")
      self.equip("casey_cheerleader_skirt")
      self.equip("casey_cheerleader_panties")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("casey avatar "+state,True):
          return "casey avatar "+state
      rv=[]

      if state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((680,1080))
        rv.append(("casey avatar body1",123,90))
        if "_cheerleader_panties" in state:
          rv.append(("casey avatar b1panties",137,740))
        if "_cheerleader_skirt" in state:
          rv.append(("casey avatar b1skirt",101,729))
        if "_cheerleader_bra" in state:
          rv.append(("casey avatar b1bra",164,391))
        if "_cheerleader_top" in state:
          rv.append(("casey avatar b1top",164,385))
        if "_shifty" in state:
          rv.append(("casey avatar face_shifty",267,235))
        else:
          rv.append(("casey avatar face_neutral",267,230))
        rv.append(("casey avatar b1arm1_n",62,390))
        if "_cheerleader_top" in state:
          rv.append(("casey avatar b1arm1_c",61,389))
        if "_paddle" in state:
          rv.append(("casey avatar b1paddle",279,377))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((680,1080))
        rv.append(("casey avatar body1",123,90))
        if "_cheerleader_panties" in state:
          rv.append(("casey avatar b1panties",137,740))
        if "_cheerleader_skirt" in state:
          rv.append(("casey avatar b1skirt",101,729))
        if "_cheerleader_bra" in state:
          rv.append(("casey avatar b1bra",164,391))
        if "_cheerleader_top" in state:
          rv.append(("casey avatar b1top",164,385))
        rv.append(("casey avatar face_confident",267,233))
        if "_mocking" in state:
          rv.append(("casey avatar b1arm2_n",115,313))
          if "_cheerleader_top" in state:
            rv.append(("casey avatar b1arm2_c",116,384))
        else:
          rv.append(("casey avatar b1arm1_n",62,390))
          if "_cheerleader_top" in state:
            rv.append(("casey avatar b1arm1_c",61,389))
        if "_paddle" in state:
          rv.append(("casey avatar b1paddle",279,377))

      return rv


  class Interactable_casey(Interactable):

    def title(cls):
      return casey.name

    def actions(cls,actions):
      pass
