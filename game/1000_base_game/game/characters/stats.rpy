init python:
  class LoveStat(BaseStat):
    id="love"
    title="Love"
    color="#F88"
    icon=["stats love","stats love_2","stats love_3"]
    notify_xp_granted=False
    notify_level_changed=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]

  class LustStat(BaseStat):
    id="lust"
    title="Lust"
    color="#F00"
    icon=["stats lust","stats lust_2","stats lust_3"]
    notify_xp_granted=False
    notify_level_changed=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]

  class HateStat(BaseStat):
    id="hate"
    title="Hate"
    color="#303446"
    icon=["stats hate","stats hate_2","stats hate_3"]
    notify_xp_granted=False
    notify_level_changed=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]
