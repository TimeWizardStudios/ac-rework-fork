init python:
  class Character_cuthbert(BaseChar):
    notify_level_changed = True

    default_name = "Cuthbert"

    default_stats = [
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon = "cuthbert contact_icon"

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("cuthbert avatar "+state, True):
          return "cuthbert avatar "+state
      rv=[]

      if state.startswith("neutral"):
        rv.append((569,1080))
        rv.append(("cuthbert avatar body",151,657))

      return rv
