image isabelle_gesture_not_girlfriend = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-6,0), Transform("isabelle kitchen_kiss embrace", size=(206,116))), "ui circle_mask"), (6,6), "ui crossed_out_circle2")


label quest_nurse_aid_start:
  "Finally, a chance for some air and to just get away from it all for a moment."
  "It's nice not feeling so lonely anymore, but every now and then you just need a breather."
  "A moment to remember why doing this all over again is worth it."
  "Why I—"
  "..."
  "That's weird. What is the [nurse] doing up here?"
  "She looks kind of out of it and sad, and she clearly didn't even hear me enter."
  # "Maybe she prefers to be left alone... but that feels wrong if something is up."
  "Maybe she prefers to be left alone... but that feels wrong if something{space=-35}\nis up."
  mc "[nurse]?"
  window hide
  show nurse concerned with Dissolve(.5)
  window auto
  nurse concerned "Mmm?"
  nurse concerned "Oh, hi, [mc]. I didn't see you there."
  mc "What are you doing up here?"
  nurse thinking "Well, I just, um, needed to come water my... cactus?"
  "Weakest excuse ever. There isn't even a cactus in the greenhouse."
  "She looks kind of distracted on top of looking sad."
  mc "Is everything okay?"
  nurse sad "..."
  nurse sad "Yes, I just needed some air."
  nurse sad "What are {i}you{/} doing out here?"
  mc "Same thing, I guess."
  nurse blush "That's good."
  mc "Are you sure you're okay, [nurse]?"
  "With her standing so close to the edge and looking so upset, it's probably best to be extra certain."
  nurse thinking "Well, not really. It's just..."
  mc "What?"
  nurse concerned "I just feel so lost lately."
  mc "What do you mean?"
  if (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished):
    nurse concerned "You see, I am grateful to you for freeing me from [kate]. Really."
    # nurse concerned "But... I don't know, ever since then I suppose I feel I have lost direction in life?"
    nurse concerned "But... I don't know, ever since then I suppose I feel I have lost direction{space=-55}\nin life?"
  elif (quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished):
    nurse concerned "It's just, um, it feels like [kate] has lost interest in me lately."
    nurse concerned "I am worried about her, but I guess I also feel kind of relieved about it on some level?"
    nurse concerned "Even though I feel like I have lost all direction in my life..."
  mc "Oh. I see."
  mc "What about hobbies? Do you have any of those?"
  mc "Maybe something you enjoy would help you take your mind off that."
  nurse thinking "Goodness, it's so embarrassing to admit it, but not really."
  nurse thinking "I'm usually so busy. When I wasn't serving [kate] I would either be working here, or at the hospital, or resting."
  show nurse thinking at move_to(.25)
  menu(side="right"):
    extend ""
    # "?mc.love>=6@[mc.love]/6|{image=stats love}|\"It sounds like you're constantly trying to do things for other people.\"":
    "?mc.love>=6@[mc.love]/6|{image=stats love}|\"It sounds like you're constantly trying to do things for other people.\"{space=-60}":
      show nurse thinking at move_to(.5)
      mc "It sounds like you're constantly trying to do things for other people."
      mc "You should slow down and do something for yourself for once."
      nurse neutral "I suppose that's true."
      nurse neutral "I just... don't know where to even begin doing something for myself."
      mc "Well, I could help you."
      nurse neutral "Help me?"
      mc "You know, figure out what you enjoy doing to wind down and relax?"
      nurse surprised "You would do that?"
      mc "Of course!"
      nurse surprised "But, um, why?"
      if (quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished):
        mc "I suppose I feel partially responsible for the way you feel now..."
        mc "...but you don't need anyone else to make you feel whole. Got it?"
      mc "I want to show you how nice it can be to treat yourself."
      mc "The more you do for yourself, the more you can then do for others."
      nurse smile "How incredibly insightful of you, [mc]..."
      $nurse.love+=1
      nurse smile "...and sweet, too."
    # "\"Working that much is a sure way to burn out, you know?\"":
    "\"Working that much is a sure\nway to burn out, you know?\"":
      show nurse thinking at move_to(.5)
      mc "Working that much is a sure way to burn out, you know?"
      # nurse annoyed "I suppose you're right. I just don't know what else to do with myself."
      nurse annoyed "I suppose you're right. I just don't know what else to do with myself.{space=-10}"
      nurse annoyed "It's the only thing that keeps me distracted..."
      mc "Sure, but you can't nurse as well if you're tired or down, right?"
      mc "Sunlight and hobbies are good for you."
      # mc "And once you're feeling better, maybe then you could look into seeing someone else?"
      mc "And once you're feeling better, maybe then you could look into seeing{space=-40}\nsomeone else?"
      nurse neutral "Oh, but of course. You are right."
      # nurse neutral "I certainly don't want to put anyone at risk by overextending myself."
      nurse neutral "I certainly don't want to put anyone at risk by overextending myself.{space=-5}"
      $mc.intellect+=1
      mc "Exactly!"
    # "\"Fuck [kate]. You don't need her to have a purpose.\"":
    "\"Fuck [kate]. You don't need\nher to have a purpose.\"":
      show nurse thinking at move_to(.5)
      mc "Fuck [kate]. You don't need her to have a purpose."
      nurse annoyed "It's just, um, I get so overwhelmed sometimes..."
      nurse annoyed "I suppose it is nice having someone tell me what to do."
      mc "If that's the case, then I can tell you what to do."
      nurse afraid "Oh, goodness!"
      nurse afraid "I-I don't want to be an imposition."
      mc "It's no imposition at all."
      mc "In fact, I'll tell you what we're going to do right now."
      $nurse.lust+=1
      nurse blush "Oh, my..."
      $quest.nurse_aid["fuck_kate"] = True
  mc "We are going to discover what sort of hobbies you enjoy."
  nurse blush "Oh, my! We are?"
  mc "Indeed. So, what would you like to do first?"
  nurse thinking "Err..."
  extend " I, um..."
  mc "..."
  "She really is indecisive outside of a hospital room, isn't she?"
  mc "It's fine. I'll choose for you."
  mc "How about, err... fishing?"
  "People always talk about how relaxing that can be. Maybe it would be good for her?"
  nurse concerned "Fishing?"
  mc "Sure, why not?"
  nurse blush "I have never tried it before, but I do enjoy eating fish."
  # mc "Great! Meet me in the forest glade in a bit and we'll give it a try, then."
  mc "Great! Meet me in the forest glade in a bit and we'll give it a try, then.{space=-25}"
  nurse blush "Very well."
  nurse blush "Thank you for being so willing to help me, [mc]."
  mc "No problem!"
  window hide
  $nurse["on_roof_this_scene"] = True
  hide nurse with Dissolve(.5)
  window auto
  # "Okay. If we're going to be fishing, I should probably see about getting a fishing pole."
  "Okay. If we're going to be fishing, I should probably see about getting{space=-30}\na fishing pole."
  # "Preferably, one I can just borrow or rent, since this whole thing could be a boring bust."
  "Preferably, one I can just borrow or rent, since this whole thing could{space=-25}\nbe a boring bust."
  window hide
  $quest.nurse_aid.start()
  return

label quest_nurse_aid_fishing_pole_maxine:
  show maxine neutral with Dissolve(.5)
  mc "[maxine], can I borrow your—"
  maxine smile "Indeed you can."
  maxine smile "My fishing pole is already in your backpack."
  mc "Wait, what? Seriously?"
  maxine smile "See it for yourself."
  mc "..."
  window hide
  pause 0.125
  $mc.add_item("fishing_pole", silent=True)
  $game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
  show screen inventory with Dissolve(.25)
  pause 0.25
  window auto
  mc "What the fuck?"
  window hide
  pause 0.125
  hide screen inventory with Dissolve(.25)
  pause 0.25
  window auto
  mc "How did you even know I would need it?"
  maxine laughing "You already know the answer to that."
  show maxine laughing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Maybe I do...\"":
      show maxine laughing at move_to(.5)
      mc "Maybe I do..."
      mc "You keep a pretty close eye on things around here, don't you?"
      maxine blush "Of course I do."
      mc "That's good. So do I."
      mc "There are lots of strange things going on. We have to be prepared."
      $maxine.lust+=1
      $maxine.love+=1
      maxine flirty "Prepared for anything, at all times!"
      mc "Exactly."
      maxine excited "It will be good to have the [nurse] on our side."
      mc "Our side?"
      maxine concerned "You never know when a medical professional may be needed."
      mc "Ah. True."
      mc "Well, thank you for the fishing pole, [maxine]."
      maxine excited "I hope you catch something interesting!"
      window hide
      hide maxine with Dissolve(.5)
      window auto
      "I hope I catch anything at all..."
      "Time to meet the [nurse]."
    # "\"I don't, and I would appreciate you staying out of my backpack.\"":
    "\"I don't, and I would appreciate\nyou staying out of my backpack.\"":
      show maxine laughing at move_to(.5)
      mc "I don't, and I would appreciate you staying out of my backpack."
      maxine skeptical "I was merely providing you with what you require."
      $maxine.love-=1
      maxine skeptical "According to the rules of etiquette, a \"thank you\" would suffice."
      mc "The rules of etiquette tell you not to fiddle with other people's personal belongings."
      mc "Not to mention, it's pretty weird that you knew I needed it in the first place."
      mc "I don't appreciate being stalked, okay?"
      maxine eyeroll "It's called surveillance."
      mc "Well, don't do it again."
      maxine annoyed "Someone needs to."
      mc "We have a security guard."
      maxine annoyed "A false sense of security guard."
      mc "Whatever. Thank you for the pole, I guess."
      maxine skeptical "We'll see if I ever provide you with another one."
      window hide
      hide maxine with Dissolve(.5)
      window auto
      "Somehow, I think I'm okay with that..."
      "At least I can go fishing with the [nurse] now."
  $quest.nurse_aid.advance("fishing")
  return

label quest_nurse_aid_fishing_pole_marina_upon_entering:
  "If there was ever a place to find a fishing pole, it would surely be around here, right?"
  "I just need to take a little look around."
  return

label quest_nurse_aid_fishing_pole_marina_prawn_star:
  "Hmm... they buy and cook fresh seafood, but I don't think they fish it themselves..."
  return

label quest_nurse_aid_fishing_pole_marina_pancake_brothel:
  "Hmm... they do have some poles, but not the kind I'm looking for..."
  return

label quest_nurse_aid_fishing_pole_marina_filthy_oar:
  "Hmm... I could always ask to—"
  window hide
  $maxine["outfit_stamp"] = maxine.outfit
  $maxine.outfit = {"glasses":"maxine_glasses", "panties":"maxine_black_panties", "bra":"maxine_black_bra", "shirt":"maxine_boat_captain_uniform", "hat":"maxine_boat_captain_hat"}
  show maxine confident at appear_from_right
  pause 0.5
  window auto
  maxine confident "Ahoy, matey!"
  mc "[maxine]? What were you doing on the boat?"
  maxine skeptical "The better question is, what are you {i}not{/} doing on the boat?"
  mc "Not fishing, I guess?"
  maxine skeptical "Sounds fishy."
  mc "It certainly is."
  mc "Speaking of, do you know where I could get a fishing pole?"
  maxine confident_hands_down "That depends on what you're hoping to catch."
  mc "Err, fish?"
  maxine skeptical "Is that all?"
  mc "Yes?"
  maxine skeptical "It seems the art of fishing is lost on you."
  show maxine skeptical at move_to(.75)
  menu(side="left"):
    extend ""
    "?maxine.love>=8@[maxine.love]/8|{image=maxine contact_icon}|{image=stats love_3}|\"Maybe you can teach me more about it sometime?\"":
      show maxine skeptical at move_to(.5)
      mc "Maybe you can teach me more about it sometime?"
      maxine excited "I would sustain thorough pleasure from such a thing."
      "Oh? That sounds promising."
      mc "Great! I look forward to learning more from you."
      mc "You might even say I'm {i}hooked{/} on the idea."
      maxine concerned "..."
      maxine concerned "We can work on the art of wordplay, as well."
      "Ouch. I didn't think it was that bad..."
      "Besides, [maxine] is the last person I'd ask for help on being witty."
      mc "Maybe we can also just... {i}play around?{/}"
      $maxine.love+=1
      maxine flirty "A youthful spirit! I admire that in you, [mc]."
      $mc.love+=1
      mc "Heh. Thanks."
    # "\"And the art of being normal is lost on you.\"":
    "\"And the art of being\nnormal is lost on you.\"":
      show maxine skeptical at move_to(.5)
      mc "And the art of being normal is lost on you."
      maxine eyeroll "I find it to be more of a social science, which is not a real science."
      mc "How shocking."
      maxine confident "Shock therapy is much more interesting to me."
      mc "You just might need shock therapy."
      mc "Possibly even a lobotomy."
      $maxine.love-=1
      maxine annoyed "An outmoded pastime. Please don't waste my time with such talk."
      maxine annoyed "Now, if you'll excuse me, I have deep sea excavation to return to."
      mc "Wait! Before you go!"
      mc "Can I borrow your fishing pole?"
    # "\"If I fish for some answers, will you give them to me?\"":
    "\"If I fish for some answers,\nwill you give them to me?\"":
      show maxine skeptical at move_to(.5)
      mc "If I fish for some answers, will you give them to me?"
      maxine skeptical "Do you have the proper bait?"
      show maxine skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        # "\"Do you know what has been going on around here?\"":
        "\"Do you know what has been\ngoing on around here?\"":
          show maxine skeptical at move_to(.5)
          mc "Do you know what has been going on around here?"
          maxine eyeroll "You will have to be more specific."
          mc "I know you know."
          maxine annoyed "Then why inquire?"
          mc "Because it never hurts to ask more questions, right?"
          maxine confident "It's only a matter of asking the right questions."
          $maxine.lust+=1
          # maxine confident "There is a trail to be followed. I know you walk it, but you're not alone."
          maxine confident "There is a trail to be followed. I know you walk it, but you're not alone.{space=-40}"
          mc "That's good to know. Thanks, [maxine]."
        # "\"Do you know that I know what has been going on around here?\"":
        "\"Do you know that I know what\nhas been going on around here?\"":
          show maxine skeptical at move_to(.5)
          mc "Do you know that I know what has been going on around here?"
          maxine annoyed "I think you know far less than you think you do."
          mc "Maybe I know far more than you know I do."
          maxine eyeroll "That's unlikely."
          mc "Well, rest assured, I am keeping an eye on things, too."
          $maxine.love+=1
          maxine confident "Five eyes are better than two."
          mc "Err, right. Something like that..."
        # "\"If an orgasm is had in the woods and no one is around to hear it, did she still fake it?\"":
        "\"If an orgasm is had in the woods and no one{space=-35}\nis around to hear it, did she still fake it?\"":
          show maxine skeptical at move_to(.5)
          # mc "If an orgasm is had in the woods and no one is around to hear it, did she still fake it?"
          mc "If an orgasm is had in the woods and no one is around to hear it, did{space=-10}\nshe still fake it?"
          maxine concerned "Who is she?"
          mc "Every woman, probably."
          maxine excited "Ah! A philosophical question!"
          maxine excited "That is indeed good bait."
          mc "Err, thank you."
          $maxine.lust+=1
          maxine flirty "The answer, therefore, is unknowable."
          mc "I knew it!"
        # "\"Do you want to go below deck and get naked?\"":
        "\"Do you want to go below\ndeck and get naked?\"":
          show maxine skeptical at move_to(.5)
          $mc.lust+=1
          mc "Do you want to go below deck and get naked?"
          maxine skeptical "Why? Because the dangler fish is best approached without loose garments?"
          mc "I do know of a certain dangler that hates garments."
          maxine skeptical "Tempting... but I have no need for such fish right now."
          # "I can't tell if she's serious, or if she's just trying to let me down easy."
          "I can't tell if she's serious, or if she's just trying to let me down easy.{space=-15}"
          "Either way, it's not working."
        "\"Did you catch anything good?\"":
          show maxine skeptical at move_to(.5)
          mc "Did you catch anything good?"
          maxine eyeroll "Only some red herring."
          mc "Gross."
          maxine annoyed "Indeed."
          show maxine confident with dissolve2
          extend " Unless it's pickled."
          mc "..."
  if not renpy.showing("maxine annoyed"):
    mc "So, about that fishing pole. Can I borrow it?"
  maxine flirty "Only if you share half of your catch with me."
  "That's if we even catch anything..."
  "Oh, well. I'm not a huge fish person, anyway."
  mc "Deal."
  maxine excited "It's all yours, then!"
  window hide
  $mc.add_item("fishing_pole")
  pause 0.25
  window auto show
  show maxine concerned with dissolve2
  maxine concerned "...for a limited amount of time."
  mc "Do you mean you want it back?"
  maxine concerned "Inded. I will retrieve it after the allotted time is up."
  mc "Err, okay. Thanks, anyway!"
  window hide
  show maxine concerned at disappear_to_right
  pause 0.5
  window auto
  "Okay, now that that's taken care of, time to meet the [nurse]."
  $quest.nurse_aid.advance("fishing")
  $maxine.outfit = maxine["outfit_stamp"]
  return

label quest_nurse_aid_fishing:
  "I wasn't sure if she would show, but I guess being explicitly told made her feel like she had to..."
  "Who knows? Maybe she will get some real enjoyment out of this."
  window hide
  show nurse neutral with Dissolve(.5)
  window auto
  mc "Hello, [nurse]."
  if game.hour in (7,8,9,10):
    nurse neutral "Good morning, [mc]."
  elif game.hour in (11,12,13,14):
    nurse neutral "Good afternoon, [mc]."
  elif game.hour in (15,16,17,18):
    nurse neutral "Good evening, [mc]."
  mc "I borrowed a fishing pole. Are you ready?"
  if school_forest_glade["pollution"]:
    nurse neutral "Well, yes, I suppose I am."
    # nurse annoyed "But I think it would be a health risk to eat anything that still lives here..."
    nurse annoyed "But I think it would be a health risk to eat anything that still lives here...{space=-65}"
    mc "Err, you're probably right."
    nurse annoyed "I can't believe someone would ruin nature like this."
    "She has clearly never dealt with invasive beavers."
    mc "Maybe they had a reason?"
    nurse neutral "I don't think there's a good reason to ruin a place like this."
    mc "Fair enough."
    mc "Let's just focus on the activity rather than the outcome, shall we?"
    nurse smile "Right. Okay."
  else:
    nurse smile "Well, yes, I suppose I am."
  nurse smile "Thank you again for doing this."
  mc "No problem!"
  nurse neutral "So, um, how does this work, exactly?"
  mc "I don't do a lot of fishing, to be honest, but I'll help you set up."
  mc "First, we need a bit of bait on the line."
  nurse smile "Oh! That makes sense."
  show nurse smile at move_to(.25)
  menu(side="right"):
    extend ""
    "Dig some worms out of the dirt yourself":
      show nurse smile at move_to(.5)
      mc "I'll just dig up some worms for us real quick."
      "Real men don't mind getting down in the dirt, right?"
      "Maybe the [nurse] likes that kind of thing and doesn't even know it?"
      window hide
      hide screen interface_hider
      show nurse blush
      show black
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.75
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "The soil is nice and cool the deeper I dig."
      "It reminds me of my childhood and digging up bugs to scare [flora]."
      "The nostalgia at the memory lights up my brain and brings a smile to my lips."
      "Who knew that being out here with the [nurse] could make me feel young again and remember the innocent fun to be had?"
      "..."
      "Although, I suppose [flora] hated it..."
      mc "Ah, here we go! Nice juicy fat ones!"
      nurse blush "Oh, my!"
      show black onlayer screens zorder 100
      pause 0.5
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      pause 0.125
      $mc.add_item("worms")
      pause 0.25
      window auto
      $set_dialog_mode("")
      mc "Well, that wasn't so bad."
      mc "Are you ready?"
      nurse concerned "I think so... but I'd rather not touch it, if that's okay?"
      mc "It's fine! I'll do it for you."
      $nurse.love+=1
      nurse blush "Thank you, [mc]. You're very kind."
    "Make the [nurse] dig for the worms":
      show nurse smile at move_to(.5)
      mc "Remember how this is all about finding hobbies you like?"
      # mc "I think you should get down in the dirt and dig up some worms yourself."
      mc "I think you should get down in the dirt and dig up some worms yourself.{space=-80}"
      nurse surprised "Wh-what?"
      mc "You heard me, [nurse]."
      # mc "If you want to do this, then get down there and dig up your own bait."
      mc "If you want to do this, then get down there and dig up your own bait.{space=-15}"
      nurse afraid "..."
      "The words seem to come as a shock to her... but they do something else to her, as well."
      # "A flush of embarrassment crimsons her cheeks, and her chest hitches as she catches her breath."
      "A flush of embarrassment crimsons her cheeks, and her chest hitches{space=-45}\nas she catches her breath."
      nurse annoyed "I, um, I suppose you have a point."
      mc "Yes, I do. Go ahead."
      nurse annoyed "Err, okay..."
      window hide
      show nurse worm_digging
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      # "Unsurprisingly, she does as I say, even if she pretends to be indignant about it."
      "Unsurprisingly, she does as I say, even if she pretends to be indignant{space=-40}\nabout it."
      "She looks right at home down there on her hands and knees."
      "Down in the dirt with the literal worms."
      mc "That's it. Make sure you get plenty."
      nurse worm_digging "Oh, gracious..."
      "Her face turns a darker shade of red, but even down on the ground, she seems to relax a little."
      "Just being told what to do and how to do it must have this effect on her."
      # "I can definitely see how [kate] got carried away with this kind of power."
      "I can definitely see how [kate] got carried away with this kind of power.{space=-35}"
      "The [nurse] gives in so easily. She really needs someone who won't abuse it."
      mc "Okay, I think that's plenty. Nice job."
      $nurse.lust+=1
      nurse worm_digging "Th-thank you..."
      window hide
      show nurse blush
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      pause 0.125
      $mc.add_item("worms")
      pause 0.25
      window auto
      mc "Let's fish now, shall we?"
      $unlock_replay("nurse_dig")
  window hide
  hide nurse with Dissolve(.5)
  window auto
  "Okay, I just have to get a damn worm on the hook, and then we can begin."
  $quest.nurse_aid.advance("bait")
  return

label quest_nurse_aid_bait(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  $process_event("items_combined", item,item2, items_by_id["baited_pole"])
  $mc.add_item("baited_pole", silent=True)
  pause 0.25
  "There! Easier than hooking up with a girl."
  if ("quest_nurse_aid_bait_nurse",()) in game.seen_actions:
    $game.seen_actions.remove(("quest_nurse_aid_bait_nurse",()))
  return

label quest_nurse_aid_bait_nurse:
  if mc.owned_item("baited_pole"):
    show nurse smile with Dissolve(.5)
    nurse smile "I am ready when you are."
    mc "Let's do this, then!"
    window hide
    hide screen interface_hider
    show nurse fishing wait
    show black
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    show black onlayer screens zorder 4
    $set_dialog_mode("default_no_bg")
    "It takes the [nurse] a few tries and a couple of snags in the bush..."
    "...and some light coaching, too..."
    "...but at last, we get it."
    show black onlayer screens zorder 100
    pause 0.5
    hide black
    hide black onlayer screens
    show screen interface_hider
    with Dissolve(.5)
    pause 0.0625
    $mc.remove_item("baited_pole")
    pause 0.25
    window auto
    $set_dialog_mode("")
    mc "There you go! Just like that."
    nurse fishing wait "Gracious... it's not as easy as it looks..."
    mc "Just make sure you hold it firmly, okay?"
    nurse fishing wait "Oh, um. O-okay."
    nurse fishing wait "Like this?"
    mc "Yes, just like that!"
    nurse fishing wait "..."
    "We stand there together in silence for a moment."
    "Enjoying the soft fall breeze and the lapping of the water against the shore."
    "It's actually kind of peaceful. I get now why people do this."
    "Nothing but nature and the slowing down of thoughts."
    "It's weird how standing still and just waiting can make everything else feel insignificant."
    "..."
    mc "So, are you having fun?"
    nurse fishing wait "It's okay, I suppose."
    nurse fishing wait "However, once the pole is in the water, it's kind of, well, boring?"
    nurse fishing wait "You have lots of time to think..."
    mc "I guess you do."
    mc "Are you thinking about [kate]?"
    nurse fishing wait "Err, a bit... and work, too."
    mc "Just enjoy the fresh air, will you? Relax a bit."
    mc "The point of fishing is to forget about those things."
    nurse fishing wait "Right. You're right."
    nurse fishing wait "It's just... easier said than done."
    mc "Hey, I get it."
    window hide
    pause 0.125
    show nurse fishing wait with vpunch
    pause 0.25
    window auto
    nurse fishing wait "Oh! Did you feel that?!"
    mc "It feels like you got a bite!"
    mc "Try reeling it in!"
    nurse fishing wait "Will you, um, help me?"
    mc "Gladly."
    window hide
    pause 0.125
    play sound "<from 0.5>fishing_reel"
    show nurse fishing reeling_in with Dissolve(.5)
    pause 0.25
    window auto
    nurse fishing reeling_in "Goodness! Should it be this hard?!"
    "I don't want to admit it to her, but when the pole bends downwards, the muscles in my arms definitely strain with effort to hold on."
    mc "Err, it must be a r-really big one..."
    mc "L-let me try doing it on my own, okay?"
    nurse fishing reeling_in "Yes, please!"
    window hide
    hide screen interface_hider
    show nurse fishing maxine_reveal
    show black
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    play sound "<from 0.5>fishing_reel"
    show black onlayer screens zorder 4
    $set_dialog_mode("default_no_bg")
    "What the hell kind of fish do they keep stocked in this stream?!"
    "The harder I try to reel it in, the heavier it seems to get, fighting against me, against the current of the water."
    "It's fully bending the pole, now."
    "Maybe I should just give up and let it win?"
    "Maybe it's—"
    show black onlayer screens zorder 100
    pause 0.5
    play sound "<from 0.2>water_splash2"
    hide black
    hide black onlayer screens
    show screen interface_hider
    with Dissolve(.5)
    window auto
    $set_dialog_mode("")
    mc "[maxine]?! What the hell are you doing?"
    if quest.isabelle_hurricane.actually_finished:
      maxine "I'm searching for my homunculus."
      maxine "...and also the portal to the underworld."
      mc "Whoops. Sorry."
    else:
      maxine "I'm preparing."
    maxine "So, if you could throw me back, that would be great."
    mc "You do realize this is probably not a good place to swim, don't you?"
    if school_forest_glade["pollution"]:
      mc "The pollution can't be good for your health."
    else:
      mc "You might be ruining the ecosystem."
    maxine "There are worse things than me below the surface."
    "I somehow doubt that..."
    "At least, after I got rid of that damned beaver."
    mc "That's not even the point I was trying to make."
    nurse fishing maxine_reveal "Goodness gracious, [maxine]! Are you all right?!"
    maxine "I would be better if you would allow me to continue my work."
    menu(side="middle"):
      extend ""
      "Throw her back in":
        mc "I suppose what you're doing down there is not really my business..."
        mc "Very well. I'll release you."
        nurse fishing maxine_reveal "Oh, um. Are you certain, [mc]?"
        mc "I'm sure she'll be fine."
        nurse fishing maxine_reveal "Well, let me at least unhook her and make sure she's all right first, will you?"
        window hide
        show nurse sad at Transform(xalign=.25)
        $maxine["outfit_stamp"] = maxine.outfit
        $maxine.outfit = {"panties":"maxine_black_panties", "bra":"maxine_black_bra", "shirt":"maxine_diving_suit", "glasses":"maxine_snorkel"}
        show maxine neutral at Transform(xalign=.75)
        show black onlayer screens zorder 100
        with Dissolve(.5)
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        window auto
        "The [nurse] really is one of the kindest people I know."
        "She just wants to help others, in any way she can."
        "It's no wonder she got into the line of work she did."
        "She takes her time looking [maxine] over for any injury, her hands so calm and sure — a contrast from her usual nature."
        "Taking care of people is something she understands and loves."
        "It really is special... and it makes me want to be kinder, too."
        nurse blush "Okay, [maxine]. Everything checks out."
        $maxine.love+=1
        maxine smile "Excellent! Thank you."
        maxine smile "Now, if you don't mind, I'll be on my way."
        window hide
        show nurse blush:
          ease 1.0 xalign 0.5
        show maxine smile behind nurse at disappear_to_left
        show nurse concerned with Dissolve(.5)
        pause 0.5
        play sound "<from 0.2>water_splash2" volume 0.5
        show location with vpunch
        window auto
        "...and just like that, [maxine] slips right back into the water."
        "That's not exactly how I pictured this little outing going."
      "Keep her":
        mc "I'm sorry, [maxine], but I'm not letting you go back down there."
        # mc "Congratulations on being our biggest — and only — catch of the day."
        mc "Congratulations on being our biggest — and only — catch of the day.{space=-20}"
        maxine "You disturbed the process."
        mc "What process?"
        # nurse fishing maxine_reveal "Please, just come on out of there and let me make sure you're all right, will you, [maxine]?"
        nurse fishing maxine_reveal "Please, just come on out of there and let me make sure you're all right,{space=-60}\nwill you, [maxine]?"
        "Man, the [nurse] really is too nice..."
        # "Instead of being annoyed by [maxine]'s antics, she just wants to help her."
        "Instead of being annoyed by [maxine]'s antics, she just wants to help her.{space=-75}"
        "I don't think I have ever met anyone kinder."
        nurse fishing maxine_reveal "Would you please help her up, [mc]?"
        mc "You got it."
        window hide
        show nurse sad at Transform(xalign=.25)
        $maxine["outfit_stamp"] = maxine.outfit
        $maxine.outfit = {"panties":"maxine_black_panties", "bra":"maxine_black_bra", "shirt":"maxine_diving_suit", "glasses":"maxine_snorkel"}
        show maxine angry at Transform(xalign=.75)
        show black onlayer screens zorder 100
        with Dissolve(.5)
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        window auto
        $maxine.love-=1
        maxine angry "I am perfectly unscathed, thank you."
        nurse sad "Well, I just had to be sure, okay?"
        nurse sad "You know you're not supposed to be in there."
        maxine angry "That's what they said about the moon landing."
        mc "Who even are {i}they?{/}"
        maxine neutral "Nevermind. Just don't obstruct progress again."
        maxine neutral "It won't end well for anyone."
        mc "I wasn't—"
        window hide
        show nurse sad:
          ease 1.0 xalign 0.5
        show maxine neutral behind nurse at disappear_to_left
        show nurse concerned with Dissolve(.5)
        pause 0.5
        play sound "<from 0.2>water_splash2" volume 0.5
        show location with vpunch
        window auto
        "Welp. There she goes, back into the water anyway."
    nurse concerned "I do hope she'll be safe in there."
    mc "I'm sure she'll be fine."
    # mc "So, how did you like fishing? Even though we technically didn't catch a fish..."
    mc "So, how did you like fishing? Even though we technically didn't catch{space=-25}\na fish..."
    nurse blush "It was kind of exciting, I have to admit."
    # nurse thinking "Except, well, I don't think it's something I would want to do on my own."
    nurse thinking "Except, well, I don't think it's something I would want to do on my own.{space=-65}"
    mc "That's fair."
    mc "Maybe something else outdoors, then? How about hiking?"
    mc "Exercise is supposed to be good for the brain as well as the body."
    "..."
    "I can't help but feel a like a hypocrite giving her advice like this."
    # "But perhaps it could also be a chance to get a good habit of my own?"
    "But perhaps it could also be a chance to get a good habit of my own?{space=-30}"
    nurse blush "That's very true."
    nurse blush "I would be willing to try that if you think it's a good idea."
    if quest.nurse_aid["fuck_kate"]:
      # mc "I do think it's a good idea. And I'm here to tell you what to do, remember?"
      mc "I do think it's a good idea. And I'm here to tell you what to do, remember?{space=-110}"
    else:
      mc "It doesn't matter what I think, remember?"
      mc "We're trying to find things {i}you{/} like, [nurse]."
    nurse blush "Oh, um. You're right."
    nurse blush "In that case, I might indeed enjoy a hike."
    mc "It's decided, then. Let's try hiking!"
    mc "Meet me in the park tomorrow?"
    nurse blush "I can hardly wait!"
    window hide
    if nurse.location == "school_forest_glade":
      hide nurse with Dissolve(.5)
    else:
      show nurse blush at disappear_to_left
      pause 0.5
    window auto
    "That was a weird little adventure, but at least it seemed to take her mind off things."
    "I guess we'll see how our walk through nature goes tomorrow..."
    $unlock_replay("nurse_catch")
    $quest.nurse_aid.advance("sleep")
    $maxine.outfit = maxine["outfit_stamp"]
  else:
    show nurse afraid with Dissolve(.5)
    nurse afraid "Please, bait the hook, will you?"
    nurse afraid "I don't think I can do it myself."
    mc "Okay, okay."
    window hide
    hide nurse with Dissolve(.5)
  return

label quest_nurse_aid_hiking:
  $nurse["outfit_stamp"] = nurse.outfit
  $nurse.outfit = {"panties":"nurse_green_panties", "bra":"nurse_green_bra", "outfit":"nurse_hiking_gear", "hat":"nurse_sun_hat"}
  show nurse blush with Dissolve(.5)
  nurse blush "It's a surprisingly lovely day for a walk, isn't it?"
  mc "It's not bad. And you look great, by the way!"
  nurse blush "Oh, thank you! I just figured it's best to be prepared."
  mc "That's smart! I bet you're always like that."
  nurse blush "I do try, anyway. You just never know."
  mc "I suppose that's true."
  mc "So, are you ready?"
  nurse blush "Yes! Lead the way!"
  window hide
  show nurse blush at disappear_to_right
  pause 0.5
  hide screen interface_hider
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Normally, I don't go walking around in nature just for fun, but if it helps the [nurse], then I suppose it's a good deed I can bear."
  # "She's always doing so much for other people. It's about time someone reciprocated."
  "She's always doing so much for other people. It's about time someone{space=-45}\nreciprocated."
  "..."
  "God, even if it does make my calves burn..."
  "I don't really know why I suggested this. I generally hate walking and exercise."
  "Though I suppose the sunlight and fresh air is kind of nice."
  "I guess what they say about that is true, at least."
  $game.location = "school_woods"
  show black onlayer screens zorder 100
  pause 0.5
  show nurse blush at appear_from_left
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  nurse blush "It really is so lovely out here. And so peaceful, too."
  mc "..."
  nurse concerned "Are you okay, [mc]?"
  show nurse concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.strength>=12@[mc.strength]/12|{image=stats str}|\"Yes! Let's keep going!\"":
      show nurse concerned at move_to(.5)
      mc "Yes! Let's keep going!"
      mc "A little bit of muscle strain never hurt anybody, right?"
      mc "I bet you can attest to that."
      nurse blush "Well, yes. It can even be good for you."
      mc "I do love to feel the burn."
      $nurse.lust+=1
      nurse blush "Me too..."
    "\"I, err... I could use a bit of a break...\"":
      show nurse concerned at move_to(.5)
      mc "I, err... I could use a bit of a break..."
      nurse concerned "Right. Of course."
      nurse concerned "How about some water, too?"
      mc "Yes, please!"
      mc "It's a good thing you came prepared..."
      nurse blush "Well, staying hydrated {i}is{/} very important."
      mc "That's true. Thank you."
      "I really should drink more water, shouldn't I?"
      "It's just so boring compared to strawberry juice..."
      nurse blush "My pleasure!"
      $quest.nurse_aid["break"] = True
  mc "So, how do you like hiking so far?"
  nurse blush "It is nice to get out of the clinic, I must say."
  nurse blush "Away from all the sterile smells and sickness."
  mc "Heh. I guess that would be a nice change."
  mc "And nobody needs your help out here."
  nurse blush "Oh, but I do like that part!"
  mc "Have you always been this helpful and kind?"
  nurse blush "Goodness... I, um, I suppose so."
  nurse blush "I just like to help people feel their best, and I hope that in return it makes the world a little better."
  "God, she is way too sweet for someone like [kate]..."
  nurse thinking "My mom was a single mother and always so busy, but she always found time to help others."
  nurse thinking "I suppose I get some of that from her."
  mc "That's really nice."
  nurse blush "Thank you. She was a good woman."
  mc "..."
  nurse blush "..."
  "We stand there in silence for a moment, listening to the birds chirp and breathing in the autumn air."
  mc "Should we keep going?"
  nurse blush "Oh, yes. Right after you."
  mc "Follow me!"
  window hide
  show nurse blush at disappear_to_right
  pause 0.5
  hide screen interface_hider
  # show nurse hiking_help mc_down
  # or
  # show nurse hiking_hurt
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Puh! She doesn't look like it, but she's more fit than I am."
  "It must be all that running up and down the hospital corridors..."
  "..."
  # "As we trek deeper into the forest, the air grows colder and the breeze begins to pick up."
  "As we trek deeper into the forest, the air grows colder and the breeze{space=-35}\nbegins to pick up."
  "But overall, it's far from the worst experience."
  "It's even kind of nice hanging out with the [nurse] like this—"
  show black onlayer screens zorder 100
  pause 0.5
  if quest.nurse_aid["break"]:
    play sound "falling_thud"
    show nurse hiking_help mc_down at center: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
      block:
        linear 0.05 yoffset 15
        linear 0.05 yoffset -15
        repeat 3
      linear 0.05 yoffset 0
    pause 0.0
    hide black
    hide black onlayer screens
    show screen interface_hider
    with Dissolve(.5)
    hide nurse
    show nurse hiking_help mc_down ## This is just to avoid the screen shake if the player is skipping
    window auto
    $set_dialog_mode("")
    mc "Ow! Fuck!"
    nurse hiking_help mc_down "Oh, dear! Are you okay?"
    mc "I-I think I twisted my ankle..."
    nurse hiking_help mc_down "Goodness, I'm so sorry."
    nurse hiking_help mc_down "Let me take a look, will you?"
    window hide
    pause 0.125
    show nurse hiking_help ankle_hold with Dissolve(.5)
    pause 0.25
    window auto
    mc "O-ouch..."
    "Somehow, it hurts a little less when she's looking over me."
    "Cradling my injured foot in her soft, caring hands."
    nurse hiking_help ankle_hold "There's just a bit of swelling, okay?"
    nurse hiking_help ankle_hold "Let me wrap it for you, and you should be able to get back up."
    mc "Okay... thank you..."
    window hide
    pause 0.125
    show nurse hiking_help ankle_wrap with Dissolve(.5)
    pause 0.25
    window auto
    # "It's like a fire ignites in her eyes when she gets to work on my ankle."
    "It's like a fire ignites in her eyes when she gets to work on my ankle.{space=-5}"
    "A determination and purpose, which I've only seen in [lindsey]'s eyes while blazing across the gym."
    "In a way, I envy such passion."
    "How do you find it?"
    "She wraps my ankle until it's tight and stiff, and most of the pain is already fading."
    mc "Thank you, [nurse]. That's much better."
    show nurse hiking_help ankle_wrap smile with dissolve2
    nurse hiking_help ankle_wrap smile "I'm glad. Do you think you can walk?"
    mc "Well, it's not like I have much choice, do I?"
    nurse hiking_help ankle_wrap smile "We can stop as often as you need, okay?"
    mc "Thanks..."
    window hide
    show nurse sad
    show black onlayer screens zorder 100
    with Dissolve(.5)
    $game.location = "school_park"
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    "It takes a whole lot of effort, and literally leaning on the [nurse], but at a turtle's pace, we're able to manage."
    mc "Thank goodness I have you out here with me."
    nurse sad "I'm just glad you're okay..."
    mc "You really did come prepared, didn't you?"
    nurse sad "You can never be too careful outdoors."
    mc "Err, right."
    $unlock_replay("nurse_aid")
  else:
    play sound "falling_thud"
    show nurse hiking_hurt at center: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
      block:
        linear 0.05 yoffset 15
        linear 0.05 yoffset -15
        repeat 3
      linear 0.05 yoffset 0
    pause 0.0
    hide black
    hide black onlayer screens
    show screen interface_hider
    with Dissolve(.5)
    hide nurse
    show nurse hiking_hurt ## This is just to avoid the screen shake if the player is skipping
    window auto
    $set_dialog_mode("")
    nurse hiking_hurt "Ahhh! Owww!"
    mc "Oh, shit! Are you okay?"
    nurse hiking_hurt "My... my ankle... I twisted it..."
    mc "I see. Don't move it."
    mc "I'll just carry you back, all right?"
    nurse hiking_hurt "I, um... I don't know..."
    mc "It's okay. I got you."
    window hide
    show nurse hiking_carried
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    "When I pick her up and hold her in my arms, I can feel her heart beating wildly against my own."
    "She looks up at me with her round, soft eyes and her breath tickles my chin when she speaks."
    nurse hiking_carried "You're, um, much stronger than you look."
    mc "I have put a little more effort into maintaining my physique."
    nurse hiking_carried "You make me feel like I actually weigh less than you..."
    mc "I think you feel great."
    nurse hiking_carried "I sometimes have a hard time laying off the sweets, and get a little discouraged by the scale..."
    mc "I think you're perfect the way you are. Really."
    $nurse.love+=1
    nurse hiking_carried "Thank you... and thank you for carrying me..."
    mc "I got you, [nurse]."
    # "She smiles sweetly and rests her head against my shoulder, relaxing into me."
    "She smiles sweetly and rests her head against my shoulder, relaxing{space=-10}\ninto me."
    "It's clear that she's starting to feel more and more at ease around me, and that feels great."
    "In the past, many people have looked at me like I'm some kind of creep, but there's none of that in her eyes."
    window hide
    show nurse sad
    show black onlayer screens zorder 100
    with Dissolve(.5)
    $game.location = "school_park"
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    mc "We made it back!"
    nurse sad "It hurts, but I think some rest and elevation tonight will help."
    mc "That's good to hear."
    $unlock_replay("nurse_carry")
  mc "Maybe we should try something less outdoorsy next?"
  nurse blush "I think that's a good idea."
  nurse blush "What did you have in mind?"
  mc "How about shopping?"
  "If I know anything about women, it's that they like to spend money."
  # nurse thinking "Shopping? Goodness, I can't remember the last time I bought something that wasn't a necessity..."
  nurse thinking "Shopping? Goodness, I can't remember the last time I bought something{space=-95}\nthat wasn't a necessity..."
  mc "That's all the more reason to do it!"
  mc "You have to remember to spoil yourself sometimes."
  nurse blush "Oh, um. I do suppose you're right."
  nurse blush "Shopping it is, then."
  mc "Great! Meet me on the marina tomorrow?"
  nurse blush "It's a date!"
  window hide
  show nurse blush at disappear_to_left
  pause 0.5
  window auto
  "A date, eh?"
  "It is kind of nice to get to know the [nurse] better."
  "We're the same in a way. Often overlooked and forgotten."
  "Rarely do people spend enough time to get to know me, and I think that's true for her, as well."
  "I wonder what sort of stuff she's into buying for fun?"
  "..."
  "I guess I'll find out tomorrow."
  $quest.nurse_aid.advance("sleep_again")
  $nurse.outfit = nurse["outfit_stamp"]
  return

label quest_nurse_aid_shopping:
  "Ah! This is more like it already."
  "Back in society with all the amenities one can ever dream of."
  # "The smell of freshly fried prawn filling the air... the hustle and bustle of families going about their day..."
  "The smell of freshly fried prawn filling the air... the hustle and bustle{space=-10}\nof families going about their day..."
  "The rolling fall air off the salty sea..."
  "People spending the day with friends, or shopping for their loved ones..."
  # "I used to hate going out in public, but it's good to be reminded of how nice it is to get out and just spoil yourself sometimes."
  "I used to hate going out in public, but it's good to be reminded of how{space=-40}\nnice it is to get out and just spoil yourself sometimes."
  "Life is too short not to have a bit of fun, after all."
  window hide
  show nurse smile at appear_from_left
  pause 0.5
  window auto
  nurse smile "Hello, [mc]!"
  mc "Hey, [nurse]!"
  if quest.nurse_aid["break"]:
    nurse neutral "How is your ankle?"
    mc "It's much better. Thank you again."
    nurse smile "Thank goodness..."
  else:
    mc "How is your ankle?"
    nurse smile "It's much better. Thank you again."
    nurse annoyed "I still can't believe you carried me all the way back."
    nurse annoyed "No one has ever carried me before!"
    mc "Really? That's shocking."
    mc "I'd carry you every day if you let me."
    nurse surprised "I-I don't think that will be necessary!"
    nurse surprised "I do need to walk if I want to burn more calories."
    mc "Oh, stop it! You don't need to burn anything, you hear me?"
    nurse smile "That's really nice of you..."
  mc "So, what will it be first?"
  mc "Do you have your eyes on anything in particular?"
  nurse neutral "Oh, um. Not really."
  mc "Maybe something nice to wear? A new top, or some pretty jewelry?"
  nurse annoyed "I, err... I don't know..."
  # mc "Come on! It's good to let loose and spend a little every now and then."
  mc "Come on! It's good to let loose and spend a little every now and then.{space=-35}"
  # mc "And what could be better than something that makes you feel pretty?"
  mc "And what could be better than something that makes you feel pretty?{space=-35}"
  nurse smile "Well, yes, I suppose you're right."
  nurse smile "Thank you for the reminder."
  # mc "I was actually thinking, maybe you would enjoy going out on a date?"
  mc "I was actually thinking, maybe you would enjoy going out on a date?{space=-5}"
  nurse surprised "A d-date?!"
  mc "Indeed! Like, with Mr. Brown, for instance?"
  nurse afraid "Goodness gracious!"
  nurse afraid "I, um, I hadn't considered that."
  nurse afraid "I haven't been on a date in so long..."
  mc "There's no time like the present."
  mc "So, how about a nice dress?"
  nurse neutral "Err, okay. If you think it's a good idea."
  mc "Yes, I do. Come with me."
  window hide
  show nurse neutral at disappear_to_left
  pause 0.5
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "dress_shop"
  $quest.nurse_aid["dresses"] = set()
  pause 1.0
  show nurse neutral at appear_from_right
  pause 0.0
  hide black onlayer screens with Dissolve(.5)
  window auto show
  show nurse surprised with dissolve2
  nurse surprised "My goodness! Are you sure about this?"
  mc "I have never been so sure."
  show nurse surprised at move_to(.75)
  menu(side="left"):
    extend ""
    "\"How about you pick something out?\"":
      show nurse surprised at move_to(.5)
      mc "How about you pick something out?"
      nurse neutral "Um, okay..."
      mc "After you."
      window hide
      pause 0.125
      show nurse neutral at move_to("right",1.25)
      pause 1.75
      hide nurse
      show nurse neutral at Transform(xalign=1.0)
      show nurse neutral flip at move_to("left",1.5)
      pause 2.0
      hide nurse
      show nurse neutral flip at Transform(xalign=0.0)
      show nurse neutral at move_to(.5,1.0)
      pause 1.25
      hide nurse
      show nurse neutral at Transform(xalign=.5)
      window auto
      "This is the first time I have really seen the [nurse] loosen up."
      "She takes her time looking at each of the dresses, her eyes lit up like a child at Christmas."
      mc "See? This isn't so bad, right?"
      nurse smile "It's not. I am rather enjoying it, to be honest."
      nurse smile "This is a nice reminder to spend on myself sometimes."
      mc "I'm glad! You deserve it."
      mc "Hopefully, you can be nicer to yourself from now on, as well."
      nurse smile "Oh, I think so!"
      window hide
      pause 0.125
      show nurse smile at move_to("right",1.25)
      pause 1.75
      hide nurse
      show nurse smile at Transform(xalign=1.0)
      show nurse smile flip at move_to("left",1.5)
      pause 2.0
      hide nurse
      show nurse smile flip at Transform(xalign=0.0)
      show nurse smile at move_to(.5,1.0)
      pause 1.25
      hide nurse
      show nurse smile at Transform(xalign=.5)
      window auto
      # "She continues her search for a little longer, and while I'm happy for her..."
      "She continues her search for a little longer, and while I'm happy\nfor her..."
      "...I can't deny that dress shopping isn't exactly high on my fun ranking."
      nurse smile "Oh! I think this is the one!"
      nurse smile "Let me just go try it on, okay?"
      mc "Phew! Okay!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $nurse["outfit_stamp"] = nurse.outfit
      $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_nice_dress", "pendant":"nurse_pendant"}
      $quest.nurse_aid["dresses"].add("nice_dress")
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      nurse smile "So? What do you think?"
      mc "Oh, wow! It looks great on you, [nurse]!"
      mc "Do you like it?"
      nurse smile "Yes! I love it!"
      $quest.nurse_aid["dress_chosen"] = "nice_dress"
      show nurse smile at move_to(.25)
      menu(side="right"):
        extend ""
        "?mc.money>=249@[mc.money]/249|{image=ui hud icon_money}|\"Let me buy it for you, then.\"":
          show nurse smile at move_to(.5)
          mc "Let me buy it for you, then."
          nurse afraid "Oh, no! You couldn't possibly!"
          mc "Yes, I can, and I will."
          mc "This was my idea, and I want to treat you to something nice."
          nurse smile "[mc], you have been so much sweeter than I ever knew..."
          nurse smile "No one has ever been so kind and giving to me before."
          mc "Well, now you know how people feel to be around you."
          nurse smile "..."
          $nurse.love+=5
          nurse smile "Thank you..."
          mc "You're welcome."
          mc "Now, let's get out of here, shall we?"
          window hide
          show nurse smile at disappear_to_right
          pause 0.5
          show black onlayer screens zorder 100 with Dissolve(.5)
          $game.location = "marina"
          if "quest_nurse_aid_shopping" in game.events_queue:
            $game.events_queue.remove("quest_nurse_aid_shopping")
          pause 1.0
          $nurse.outfit = nurse["outfit_stamp"]
          show nurse smile at appear_from_left
          pause 0.0
          hide black onlayer screens with Dissolve(.5)
          $mc.money-=249
          window auto
        # "\"I'll wait outside while you checkout, okay?\"":
        "\"I'll wait outside while\nyou checkout, okay?\"":
          show nurse smile at move_to(.5)
          mc "I'll wait outside while you checkout, okay?"
          nurse smile "Okay, I'll be there in a bit!"
          window hide
          show black onlayer screens zorder 100 with Dissolve(.5)
          $nurse.outfit = nurse["outfit_stamp"]
          $game.location = "marina"
          if "quest_nurse_aid_shopping" in game.events_queue:
            $game.events_queue.remove("quest_nurse_aid_shopping")
          pause 1.0
          hide nurse
          hide black onlayer screens
          with Dissolve(.5)
          window auto
          "Man, what a day this has been."
          "It was kind of tiring waiting around while she looked at all those dresses... but at least the [nurse] seemed to enjoy herself."
          window hide
          show nurse smile at appear_from_left
          pause 0.5
          window auto
          nurse smile "All finished!"
          mc "Very nice!"
    "\"I will pick something out for you.\"":
      show nurse surprised at move_to(.5)
      mc "I will pick something out for you."
      nurse afraid "Err, you will?"
      # mc "Indeed. I'll take a look around and pick out a few dresses for you to try on."
      mc "Indeed. I'll take a look around and pick out a few dresses for you\nto try on."
      $nurse.lust+=1
      nurse annoyed "That's, um... okay..."
      mc "Let's see here..."
      window hide
      $mc["focus"] = "nurse_aid"
      hide nurse with Dissolve(.5)
      return

label quest_nurse_aid_shopping_upon_leaving:
  mc "So, you have the dress."
  mc "Now, all you have to do is go get yourself a date."
  if nurse.love >= 10:
    $girl_love = [flora.love, isabelle.love, jacklyn.love, jo.love, kate.love, lindsey.love, maxine.love, maya.love, mrsl.love]
    $girl_name = [flora.name, isabelle.name, jacklyn.name, jo.name, kate.name, lindsey.name, maxine.name, maya.name, mrsl.name]
    $most_love = girl_name[girl_love.index(max(girl_love))]
    if isabelle["girlfriend"]:
      # "I would love to take her out myself, but I would never cheat on [isabelle] like that."
      "I would love to take her out myself, but I would never cheat on [isabelle]{space=-55}\nlike that."
    elif most_love == flora.name:
      "I would love to take her out myself, but [flora] would kill me if I did."
    elif most_love == lindsey.name:
      # "I would love to take her out myself, but I couldn't do that to [lindsey]."
      "I would love to take her out myself, but I couldn't do that to [lindsey].{space=-10}"
      "I must stay strong for her."
    else:
      # "I would love to take her out myself, but she probably wants someone her own age..."
      "I would love to take her out myself, but she probably wants someone{space=-20}\nher own age..."
  mc "Ask Mr. Brown out, or whoever it is you like."
  mc "Maybe you can use one of those fancy dating apps?"
  nurse annoyed "I, um, I don't know about this..."
  nurse annoyed "I don't have the time, and—"
  mc "We've talked about this, [nurse]."
  mc "It's good to make time for things besides work and pining after someone like [kate]."
  nurse neutral "Err, I suppose you are right."
  mc "I am. Believe me."
  # mc "I know more about not throwing away your life than you might think."
  mc "I know more about not throwing away your life than you might think.{space=-5}"
  nurse neutral "..."
  nurse smile "Oh, all right! I will try it."
  nurse annoyed "But, um..."
  mc "What is it?"
  nurse annoyed "Maybe you could be there? You know, as moral support?"
  show nurse annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I absolutely can.\"":
      show nurse annoyed at move_to(.5)
      mc "I absolutely can."
      nurse blush "R-really?"
      mc "Sure! I know it's a lot all at once."
      mc "I don't mind being there to support you."
      nurse blush "Thank you, [mc]. I really appreciate it."
      mc "No problem. I bet you'll be just fine, though."
      nurse sad "It's just been so long since I have been on a proper date..."
      mc "You've got this, [nurse]."
      mc "Whoever ends up getting to take you out is a lucky man."
      $nurse.love+=1
      nurse blush "Oh, gracious! I don't know about that!"
      mc "Well, I do."
      $quest.nurse_aid["moral_support"] = True
    # "\"I'm sorry, but you have to do this on your own.\"":
    "\"I'm sorry, but you have\nto do this on your own.\"":
      show nurse annoyed at move_to(.5)
      mc "I'm sorry, but you have to do this on your own."
      mc "You'll be just fine."
      # "Call it tough love or whatever you want... but the [nurse] should really try to grow a backbone."
      "Call it tough love or whatever you want... but the [nurse] should really{space=-10}\ntry to grow a backbone."
      nurse sad "You're right. I'm sorry."
      mc "There's no need to apologize."
      # mc "Just put yourself out there, okay? Most of the time, you won't regret it."
      mc "Just put yourself out there, okay? Most of the time, you won't regret it.{space=-60}"
      nurse blush "Err, yes. Thank you for reminding me."
  mc "How about you ask him to meet you at the pancake brothel?"
  if game.hour in (7,8,9,10,11,12):
    mc "Tonight at seven."
  elif game.hour in (13,14,15,16,17,18):
    mc "Tomorrow night at seven."
  nurse concerned "O-okay."
  if quest.nurse_aid["moral_support"]:
    mc "I'll be there at my own table, as moral support."
    nurse blush "Oh, I like that."
  mc "It will be easy, and you'll probably end up having a great night out."
  mc "And who knows where else it could lead?"
  nurse blush "You're right. I'll be on my way."
  nurse blush "Thanks for today, [mc]."
  window hide
  show nurse blush at disappear_to_left
  pause 0.5
  window auto
  "She really did look great in that dress, didn't she?"
  if game.hour in (7,8,9,10,11,12):
    "Her soon-to-be date for tonight is a lucky man."
    $quest.nurse_aid["date"] = game.day
  elif game.hour in (13,14,15,16,17,18):
    "Her soon-to-be date for tomorrow is a lucky man."
    $quest.nurse_aid["date"] = game.day + 1
  if quest.nurse_aid["moral_support"]:
    $quest.nurse_aid.advance("date")
  else:
    $quest.nurse_aid.advance("aftermath")
  return

label quest_nurse_aid_date:
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location != "pancake_brothel":
    "It's getting late. I promised the [nurse] I'd be there for her date as moral support."
    "I need to hurry up and get myself a table before she and Mr. Brown arrive."
    if mc.owned_item("paper"):
      "I just have to put [maxine]'s priced paper back in the stack first."
      "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    if mc.owned_item("paper"):
      $mc.remove_item("paper")
      pause 0.25
    show black onlayer screens zorder 100 with Dissolve(.5)
    if game.location in ("marina","hospital"):
      $game.location = "pancake_brothel"
      pause 1.0
    else:
      $game.location = "pancake_brothel"
      pause 2.0
    hide black onlayer screens with Dissolve(.5)
    stop music
    window auto
  else:
    # "Okay, I need to hurry up and get myself a table before the [nurse] and Mr. Brown arrive."
    "Okay, I need to hurry up and get myself a table before the [nurse] and{space=-10}\nMr. Brown arrive."
    "..."
    "..."
  "Ah, here we go! A nice, darkened corner."
  "..."
  "And just in time, too."
  window hide
  show nurse date_spying date
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Damn, the [nurse] pulled out all the stops for tonight!"
  "She looks beautiful."
  "And that dress accentuates her best features and contrasts nicely with her skin tone."
  "Mr. Brown really is a lucky son of a bitch tonight..."
  nurse date_spying date "Such a lovely evening, don't you think?"
  mrbrown "It's fine, I guess."
  nurse date_spying date "Err, yeah..."
  nurse date_spying date "So, um, did you have a nice day?"
  mrbrown "It was fine."
  "What a great conversationalist for an English teacher..."
  nurse date_spying date "Oh. That's good."
  mrbrown "Anyway, I'm going to have the wine."
  "Is he not even going to ask her about her day? Rude."
  "Even I had better manners than that in my old life."
  "The [nurse] seems a little put out, but persistent."
  "Honestly, good for her."
  # nurse date_spying date "I, err... I think I'm going to have the shrimp pancakes with ice cream..."
  nurse date_spying date "I, err... I think I'm going to have the shrimp pancakes with ice cream...{space=-25}"
  mrbrown "Huh. Are you sure?"
  mrbrown "There are some healthier options, you know."
  "Seriously, dude?"
  nurse date_spying date sad "Oh, um. Right. You're right."
  nurse date_spying date sad "Maybe I'll try the seaweed crepe instead..."
  mrbrown "So, you got all dolled up for me tonight."
  mrbrown "Does this mean you will be putting out?"
  show nurse date_spying date annoyed with dissolve2
  nurse date_spying date annoyed "Oh, my goodness!" with vpunch
  menu(side="middle"):
    extend ""
    "Intervene":
      "All right, that's enough of that."
      window hide
      pause 0.125
      show nurse date_spying confrontation with Dissolve(.5)
      pause 0.25
      window auto
      mc "Sir, you can't just talk to her like that."
      mrbrown "Excuse me? This is none of your business, kid."
      mc "I just couldn't help but overhear."
      # mc "And I really think you should treat women with more respect than that."
      mc "And I really think you should treat women with more respect than that.{space=-55}"
      mrbrown "Now, look here, you little—" with vpunch
      nurse date_spying confrontation sad "Oh, my... please don't make a scene, you two..."
      # nurse date_spying confrontation sad "I'm sorry, Mr. Brown, but I really don't think this is going to work out..."
      nurse date_spying confrontation sad "I'm sorry, Mr. Brown, but I really don't think this is going to work out...{space=-35}"
      mrbrown "Seriously? The audacity to ask me out and then treat me like this!"
      # mrbrown "And I bet you expected me to pay for dinner without anything in return, didn't you?"
      mrbrown "And I bet you expected me to pay for dinner without anything in return,{space=-65}\ndidn't you?"
      mc "Dates aren't a transactional affair..."
      mrbrown "Tsk! I knew I shouldn't have wasted my time with this nonsense."
      mrbrown "Don't expect me to ever give you another chance."
      window hide
      pause 0.125
      show nurse date_spying aftermath with Dissolve(.5)
      pause 0.25
      window auto
      nurse date_spying aftermath "Oh, goodness... that was awful..."
      mc "Good grief. What an asshole."
      mc "I'm sorry you had to put up with that."
      nurse date_spying aftermath "It's all right..."
      mc "No, it's not. You deserve more respect than that."
      nurse date_spying aftermath blush "Well, I do appreciate you standing up for me."
      nurse date_spying aftermath blush "I'm not so sure I could have done it on my own..."
      mc "I bet you could, but it was nothing, really."
      $nurse.love+=3
      nurse date_spying aftermath blush "No, I mean it. Thank you, [mc]."
      mc "Don't mention it!"
    "Let the [nurse] handle it":
      "Mr. Brown really is something else outside the classroom, isn't he?"
      "That's not exactly gentlemanly of him... but she's an independent woman capable of dealing with this on her own."
      nurse date_spying date annoyed "What a terribly rude thing to say."
      mrbrown "Come on. We both know you'll do it."
      nurse date_spying date annoyed "I just wanted a nice evening out with some good company..."
      nurse date_spying date annoyed "...but I guess that was too much to ask for."
      "Go on, girl! You tell him!"
      mrbrown "Seriously? No need to be a bitch about it."
      nurse date_spying date annoyed "..."
      nurse date_spying date annoyed "I'm sorry, Mr. Brown, but I think we're done here."
      mrbrown "Tsk. Whatever. What a waste of time."
      window hide
      pause 0.125
      show nurse date_spying aftermath with Dissolve(.5)
      pause 0.25
      window auto
      mc "Good grief. What an asshole."
      mc "I'm sorry you had to put up with that."
      nurse date_spying aftermath "That wasn't how I envisioned tonight going..."
      mc "You did handle it really well, though. I'm impressed."
      mc "You didn't even need me!"
      nurse date_spying aftermath blush "Err, your presence might have just given me the courage I needed..."
      mc "I don't know about that. You have been getting better all week."
      nurse date_spying aftermath blush "Oh, um. Thank you."
  mc "Anyway, since Mr. Brown left..."
  menu(side="middle"):
    extend ""
    # "\"...let me buy you dinner instead, will you?\"":
    "\"...let me buy you dinner instead, will you?\"{space=-10}":
      # extend " let me buy you dinner instead, will you?"
      extend " let me buy you dinner instead, will you?{space=-55}"
      nurse date_spying aftermath "Oh, no! I couldn't possibly let you do that!"
      # mc "Why not? It's the least I can do after your date turned out to be an asshole."
      mc "Why not? It's the least I can do after your date turned out to be\nan asshole."
      nurse date_spying aftermath "That's not your fault..."
      mc "Still, I want to apologize on behalf of all men."
      # mc "Now, go ahead and order those shrimp pancakes with ice cream to your heart's content. My treat!"
      mc "Now, go ahead and order those shrimp pancakes with ice cream\nto your heart's content. My treat!"
      nurse date_spying aftermath blush "Oh, all right! Thank you, [mc]."
      window hide
      show nurse date_pancakes
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      nurse date_pancakes "This is lovely. Thank you again."
      mc "It really is! I'm glad we stayed for dinner."
      nurse date_pancakes "Well, I did get all dressed up..."
      mc "Heh. Not only you did, but you look great, too."
      nurse date_pancakes "Thanks to {i}your{/} help, anyway."
      mc "Was it? I mean, you could make even a potato sack look hot."
      nurse date_pancakes "Oh, stop it, you!"
      mc "I'm serious!"
      nurse date_pancakes "It {i}is{/} nice to get out of those scrubs and into something pretty every now and then..."
      nurse date_pancakes "And these pancakes are delicious, by the way!"
      mc "They are, aren't they?"
      mc "I guess the night wasn't a total waste, after all."
      nurse date_pancakes "Not at all!"
      nurse date_pancakes "I'm glad you're the one I'm sharing this moment with, to be honest."
      nurse date_pancakes "Did you know I was one of the popular ones in high school?"
      mc "Oh, really? I had no idea."
      nurse date_pancakes "It's true! Well, I was friends with the popular girls, at least."
      nurse date_pancakes "Although, I got along with everyone, really."
      mc "Now, that part doesn't surprise me one bit."
      # nurse date_pancakes "I just wish I had been better about being kinder to everyone back then, and not going along with what everyone else did."
      nurse date_pancakes "I just wish I had been better about being kinder to everyone back then,{space=-50}\nand not going along with what everyone else did."
      nurse date_pancakes "I suppose that's why I try harder these days."
      "I wonder if [kate] will ever reflect back on her high school years like this..."
      mc "Well, it's never too late to start trying to be a better person, right?"
      nurse date_pancakes "Exactly!"
      nurse date_pancakes "And, um, I think you're certainly one of the good ones, [mc]."
      mc "Likewise, [nurse]..."
      nurse date_pancakes "..."
      nurse date_pancakes "Goodness, those pancakes go down easy with that ice cream, don't they?"
      mc "They really do. I'm glad I agreed to be your moral support!"
      nurse date_pancakes "Heh! So am I."
      window hide
      show nurse blush
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      $nurse["outfit_stamp"] = nurse.outfit
      if quest.nurse_aid["dress_chosen"] == "revealing_dress":
        $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_revealing_dress", "pendant":"nurse_pendant"}
      elif quest.nurse_aid["dress_chosen"] == "sophisticated_dress":
        $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_sophisticated_dress", "pendant":"nurse_pendant"}
      elif quest.nurse_aid["dress_chosen"] == "nice_dress":
        $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_nice_dress", "pendant":"nurse_pendant"}
      $game.location = "marina"
      $game.advance()
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      if "event_player_force_go_home_at_night" in game.events_queue:
        $game.events_queue.remove("event_player_force_go_home_at_night")
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
      mc "It's getting late. I hope you can still enjoy some of your night, and not work too much?"
      nurse blush "How about only a little?"
      mc "I'll allow it."
      nurse blush "Goodnight, [mc]."
      mc "Goodnight, [nurse]."
      window hide
      show nurse blush at disappear_to_right
      pause 0.5
      window auto
      "What an unexpectedly great night this turned out to be..."
      "Fuck Mr. Brown. He definitely missed out on a good one."
      $unlock_replay("nurse_treat")
    "\"...let's take our leave, too, shall we?\"":
      extend " let's take our leave, too, shall we?"
      nurse date_spying aftermath "Yes, please."
      nurse date_spying aftermath "I am getting rather tired after the week we just had."
      mc "But it was fun, wasn't it?"
      nurse date_spying aftermath blush "It really was!"
      nurse date_spying aftermath blush "Thank you for getting me out of my own head. Sincerely."
      mc "Anytime! I know what it's like to feel stuck and hopeless."
      nurse date_spying aftermath blush "Exactly..."
      window hide
      show nurse blush
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      $nurse["outfit_stamp"] = nurse.outfit
      if quest.nurse_aid["dress_chosen"] == "revealing_dress":
        $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_revealing_dress", "pendant":"nurse_pendant"}
      elif quest.nurse_aid["dress_chosen"] == "sophisticated_dress":
        $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_sophisticated_dress", "pendant":"nurse_pendant"}
      elif quest.nurse_aid["dress_chosen"] == "nice_dress":
        $nurse.outfit = {"panties":"nurse_black_panties", "bra":"nurse_black_bra", "shirt":"nurse_nice_dress", "pendant":"nurse_pendant"}
      $game.location = "marina"
      $game.advance()
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      if "event_player_force_go_home_at_night" in game.events_queue:
        $game.events_queue.remove("event_player_force_go_home_at_night")
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
      nurse blush "See you tomorrow, [mc]?"
      mc "See you, [nurse]."
      window hide
      show nurse blush at disappear_to_right
      pause 0.5
      window auto
      "Welp. Tonight was a bit of a waste."
      "But at least she got out and tried, right?"
      # "And instead of doing nothing about it, I helped my fellow person and that feels pretty good."
      "And instead of doing nothing about it, I helped my fellow person and{space=-15}\nthat feels pretty good."
  "Anyway, I better get home before [jo] turns into an angry witch."
  $unlock_replay("nurse_moral_support")
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $nurse.outfit = nurse["outfit_stamp"]
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $quest.nurse_aid.advance("aftermath")
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  return

label quest_nurse_aid_aftermath:
  show nurse concerned with Dissolve(.5)
  if quest.nurse_aid["moral_support"]:
    mc "Hey, [nurse]..."
    mc "So, the date was kind of a total bust, wasn't it?"
    nurse concerned "Oh, um..."
    mc "Yeah, I thought so, too."
  else:
    mc "Hey, [nurse]!"
    mc "So, how did the date go?"
  nurse concerned "Oh, gosh. It was such a disaster..."
  nurse concerned "But you have been so kind in setting it up and helping me, I didn't want to let you down."
  mc "It's not your fault, okay? I'm sorry it sucked."
  mc "Maybe you could try again some other time?"
  show nurse sad with dissolve2
  nurse sad "..."
  "Damn. She's looking kind of sad, isn't she?"
  mc "What is it?"
  nurse sad "It's just, um, maybe I should ask [kate] to consider taking me back?"
  mc "Seriously, now?"
  nurse sad "I'm feeling so lonely and down. It would be better than nothing."
  nurse sad "And I, well... I kind of miss her."
  "I'd be lying if I said I don't understand what loneliness feels like..."
  "...but that doesn't mean anyone should settle for less than they deserve..."
  "...and the [nurse] definitely deserves better than [kate]."
  mc "Look, I just think there has to be a better way."
  mc "Relationships like that don't have to be so toxic, you know?"
  mc "And I really do think you deserve better than her."
  nurse blush "...you do?"
  mc "Of course I do! And you should realize your worth, too."
  nurse blush "You're always so kind to me, [mc]. Thank you."
  nurse concerned "I suppose part of it is that I just feel so... hollow, still."
  nurse concerned "Back when I was with [kate], I felt somewhat fulfilled, at least."
  mc "Hmm..."
  mc "If service really is the only way for you to find purpose, then maybe we could find someone else for you?"
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  show nurse concerned at move_to(.75)
  menu(side="left"):
    extend ""
    # "\"And I think that person could be [jacklyn].\"":
    "\"And I think that person\ncould be [jacklyn].\"":
      show nurse concerned at move_to(.5)
      mc "And I think that person could be [jacklyn]."
      nurse surprised "J-[jacklyn]?"
      # mc "Why not? She's confident, smart, sexually open, and most importantly, still kind."
      mc "Why not? She's confident, smart, sexually open, and most importantly,{space=-60}\nstill kind."
      nurse neutral "I, um, I suppose you're right. I just never thought of her like that."
      mc "I can talk to her for you, if you'd like me to?"
      nurse smile "Would you do that for me?"
      mc "Absolutely!"
      nurse smile "In that case, yes, please! Thank you!"
      "That's probably the first real bit of true happiness I have seen from her these last few days..."
      "I guess some people really do live just to serve others."
      "All the better if she can do it in a safe, caring environment."
      mc "We'll meet up in your office, okay?"
      $nurse.lust+=1
      nurse smile "I can't wait!"
      window hide
      if game.location == "school_forest_glade":
        show nurse smile at disappear_to_left
      elif game.location == "school_cafeteria":
        show nurse smile at disappear_to_right
      pause 0.5
      window auto
      "Hopefully, [jacklyn] doesn't shoot the idea down."
      "I would hate to get the [nurse]'s hopes up for nothing..."
      $nurse["at_none_this_scene"] = True
      $quest.nurse_aid.advance("someone_else")
    # "?nurse.love>=20@[nurse.love]/20|{image=nurse contact_icon}|{image=stats love_3}|\"And, honestly, I think that person could be me.\"" if not isabelle["girlfriend"]:
    "?nurse.love>=20@[nurse.love]/20|{image=nurse contact_icon}|{image=stats love_3}|\"And, honestly, I think that person could be me.\"{space=-55}" if not isabelle["girlfriend"]:
      show nurse concerned at move_to(.5)
      mc "And, honestly, I think that person could be me."
      nurse surprised "Y-you?"
      nurse surprised "But I'm so much older than you!"
      mc "That doesn't matter to me."
      mc "I want to show you that this sort of relationship can be healthy."
      mc "I'll treat you exactly how you deserve."
      nurse afraid "Goodness gracious! I-I never thought..."
      nurse annoyed "..."
      nurse annoyed "Are you sure about this, [mc]?"
      mc "Yes, I am."
      mc "I think you're one of the best, kindest people I've ever met, and you deserve a safe space to be your true self."
      nurse smile "Oh, my! No one has ever said that to me before..."
      nurse smile "I, um, I don't know what to say."
      mc "Well, would you like to give it try?"
      nurse smile "I would love to."
      mc "It's decided, then!"
      mc "Let's meet up in your office and have some fun?"
      $nurse.love+=5
      nurse smile "O-okay!"
      window hide
      if game.location == "school_forest_glade":
        show nurse smile at disappear_to_left
      elif game.location == "school_cafeteria":
        show nurse smile at disappear_to_right
      pause 1.0
      $nurse["at_none_this_scene"] = True
      $quest.nurse_aid.advance("relationship")
    # "?not isabelle['girlfriend']@[nurse.love]/20|{image=nurse contact_icon}|{image=stats love_3}||+||{image=isabelle_gesture_not_girlfriend}|\"And, honestly, I think that person could be me.\"" if isabelle["girlfriend"]:
    "?not isabelle['girlfriend']@{space=20}|[nurse.love]/20|{image=nurse contact_icon}|{image=stats love_3}||+||{image=isabelle_gesture_not_girlfriend}|\"And, honestly,{space=-140}\nI think that person{space=-180}\ncould be me.\"{space=-120}" if isabelle["girlfriend"]:
      pass
  return

label quest_nurse_aid_someone_else:
  show jacklyn neutral with Dissolve(.5)
  mc "Hey, [jacklyn]?"
  mc "I have kind of a weird question for you."
  jacklyn smile_hands_down "The weirder, the starrier."
  # jacklyn smile_hands_down "What kind of twisted tango are you cha-chaing in your melon cavern?"
  jacklyn smile_hands_down "What kind of twisted tango are you cha-chaing in your melon cavern?{space=-30}"
  mc "So, the [nurse] has been feeling kind of down lately, and as you may know, she lives to help and serve other people."
  mc "And, well, she and I were wondering if you would have any interest in, err... helping her out with that?"
  show jacklyn neutral with dissolve2
  jacklyn neutral "She can nurse me back to health next time I'm plague ridden. How's that?"
  mc "That's not quite what I had in mind..."
  # jacklyn thinking "Look, I know what kind of spin you set your wash to, but I don't dabble."
  jacklyn thinking "Look, I know what kind of spin you set your wash to, but I don't dabble.{space=-70}"
  show jacklyn thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Don't think of it as dabbling, then.\"":
      show jacklyn thinking at move_to(.5)
      mc "Don't think of it as dabbling, then."
      mc "Instead, think of it as putting the [nurse] in the right hands."
      mc "Good ownership, if you will."
      jacklyn annoyed "What about [kate]?"
      mc "She's too crazy for her."
      jacklyn excited "The [nurse] is a little meeksauce, isn't she?"
      mc "Heh. Just a little bit."
      mc "Maybe you could give her the confidence she needs?"
      jacklyn laughing "And what if she doesn't want her spine stretched?"
      mc "Oh, I think she'll stretch whatever you want her to."
      jacklyn laughing "That could be a pants dance!"
    "\"Sure you don't.\"":
      show jacklyn thinking at move_to(.5)
      mc "Sure you don't."
      jacklyn annoyed "Contrary to popular belief, I really don't."
      jacklyn laughing "It gets messy."
      mc "Heh. I bet it does."
      mc "Except this is less dabbling and more... delegating?"
      mc "Think of it as putting your teaching skills to work."
      jacklyn annoyed "That's green bean and all, but it's really about going full bristle with the brush."
      mc "Hey, full bristle can be hot."
      # mc "I'm sure the [nurse] wouldn't mind, if you tell her that's what you want."
      mc "I'm sure the [nurse] wouldn't mind, if you tell her that's what you want.{space=-30}"
      $jacklyn.lust+=1
      jacklyn laughing "You're breaking the funny bone, now!"
      jacklyn laughing "Still, you have a way of cash-moneying the non-dabble, don't you?"
      mc "Err, yeah! That's exactly what I was going for..."
    # "\"It could be an interesting project for you, don't you think?\"":
    "\"It could be an interesting project\nfor you, don't you think?\"":
      show jacklyn thinking at move_to(.5)
      mc "It could be an interesting project for you, don't you think?"
      jacklyn annoyed "How so?"
      mc "Well, you know how the female form itself is a work of art, right?"
      jacklyn excited "Go on..."
      mc "Just think of all the difficult positions you could paint her in!"
      mc "And the best part is, she would love it! A bit of squid pro go, really."
      $jacklyn.love+=1
      jacklyn laughing "You're inking my agreement forms right now!"
  mc "So, you'll do it?"
  jacklyn excited "Sure, why not? I always love a good bit of experimentation and adventure."
  mc "Sweet! Let's meet up with her, then?"
  jacklyn excited "Lead the way!"
  window hide
  if game.location == "school_entrance":
    show jacklyn excited at disappear_to_left
  elif game.location in ("school_art_class","school_cafeteria"):
    show jacklyn excited at disappear_to_right
  pause 0.5
  window auto
  "Phew! That went better than expected..."
  # "Time to see how their little meetup goes. Hopefully, [jacklyn] can help make the [nurse] happy and keep her away from [kate]."
  "Time to see how their little meetup goes. Hopefully, [jacklyn] can help{space=-10}\nmake the [nurse] happy and keep her away from [kate]."
  $jacklyn["at_none_this_scene"] = True
  $quest.nurse_aid["jacklyn_on_board"] = True
  return

label quest_nurse_aid_someone_else_meetup:
  show nurse blush with Dissolve(.5)
  nurse blush "Are we really doing this, [mc]?"
  mc "Definitely."
  window hide
  show nurse blush:
    ease 1.0 xalign 0.25
  show jacklyn laughing at appear_from_right(.75)
  pause 0.5
  window auto
  jacklyn laughing "Hey, hey! How's the labia flapping?"
  nurse blush "H-hi, [jacklyn]."
  nurse thinking "It's, err... well..."
  mc "It's free in the wind."
  show nurse blush
  jacklyn excited "That's what I like to hear!"
  jacklyn excited "I also hear we're going to dibble in the dabble paint?"
  mc "That's right. The [nurse] here asked me to speak to you."
  nurse concerned "Err, [mc] has been very helpful and kind these last few days..."
  mc "...and now, we just want to make sure this is going to work out."
  mc "So, what do you say?"
  jacklyn laughing "Well, I have to taste the cheese before I slice it, don't I?"
  nurse blush "I, um, I would like that very much."
  jacklyn excited "Bang-ganger! Thanks for the help, [mc]."
  jacklyn excited "Speaking of, will you be hitching your cock to this vajay wagon?"
  show nurse blush at move_to(.1)
  show jacklyn excited at move_to(.9)
  menu(side="middle"):
    extend ""
    "Stay":
      show nurse blush at move_to(.25)
      show jacklyn excited at move_to(.75)
      mc "I wasn't planning on it, but maybe I should stay the first time just to make sure this goes well for you both?"
      jacklyn laughing "A virtuous voyeur, eh? I'm down."
      nurse blush "Oh, um. That would be nice."
      mc "Don't mind me, okay? Just do your thing, ladies."
      jacklyn excited "Aces!"
      # jacklyn excited "So, [nurse], do you need someone to juice your melon and cinch the leash?"
      jacklyn excited "So, [nurse], do you need someone to juice your melon and cinch the leash?{space=-120}"
      nurse blush "Mmm... yes..."
      window hide
      show nurse jacklyn_service anticipation
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide jacklyn
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      jacklyn "Are you excited?"
      nurse jacklyn_service anticipation "Very..."
      jacklyn "And do you want to play?"
      nurse jacklyn_service anticipation "Yes! I do!"
      # jacklyn "I can turbo the jets, but I need you to show me how well behaved you can be."
      jacklyn "I can turbo the jets, but I need you to show me how well behaved you{space=-30}\ncan be."
      jacklyn "Do you think you can do that?"
      nurse jacklyn_service anticipation "Oh, goodnes... I-I think so..."
      jacklyn "You {i}think so?{/}"
      jacklyn "I {i}know{/} you have it in you to be a good girl."
      jacklyn "You're so attentive and obedient, aren't you?"
      nurse jacklyn_service anticipation "I, um..."
      jacklyn "I bet your faucet is already dripping at the prospect of ownership blowing in your ear."
      nurse jacklyn_service anticipation "Mmm... it's just, well..."
      nurse jacklyn_service anticipation "It feels like it's been so long since any of... this..."
      window hide
      pause 0.125
      show nurse jacklyn_service rubbing1 with Dissolve(.5)
      pause 0.25
      window auto
      jacklyn "Swish! You have full on sprung a leak already!"
      nurse jacklyn_service rubbing1 "Err, I told you..."
      jacklyn "You like to be ordered around, don't you?"
      window hide
      pause 0.125
      show nurse jacklyn_service rubbing2 with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service rubbing2 with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 with Dissolve(.1875)
      pause 0.25
      window auto
      nurse jacklyn_service rubbing1 "Mmmm!"
      "As she says this, the [nurse] shudders and presses her ass back into [jacklyn]."
      "Probably because [jacklyn] is teasing her dripping pussy as she speaks."
      jacklyn "Answer the question."
      nurse jacklyn_service rubbing1 "Y-yes! I do!"
      window hide
      pause 0.125
      show nurse jacklyn_service rubbing1 ear_bite with vpunch
      pause 0.25
      window auto
      nurse jacklyn_service rubbing1 ear_bite "O-oh!"
      "Damn, I did not see that coming."
      "It clearly caught the [nurse] by surprise, too."
      jacklyn "I've been told you have more respect than that."
      jacklyn "Show me what a good, respectful girl you are."
      window hide
      pause 0.125
      show nurse jacklyn_service rubbing2 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 ear_bite with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service rubbing2 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 ear_bite with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service rubbing2 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 ear_bite with Dissolve(.1875)
      pause 0.25
      window auto
      nurse jacklyn_service rubbing1 ear_bite "Goodness... I..."
      jacklyn "You do want to be my good girl, don't you?"
      nurse jacklyn_service rubbing1 ear_bite "Yes, Miss [jacklyn]! I do!"
      jacklyn "There's a good girl!"
      jacklyn "So soft and demure. A gentle little bunny."
      window hide
      pause 0.125
      show nurse jacklyn_service rubbing2 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 ear_bite with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service rubbing2 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing3 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing4 ear_bite with Dissolve(.1875)
      show nurse jacklyn_service rubbing1 ear_bite with Dissolve(.1875)
      pause 0.25
      window auto
      nurse jacklyn_service rubbing1 ear_bite "Mmm... yes, Miss... please..."
      jacklyn "Please, what?"
      nurse jacklyn_service rubbing1 ear_bite "P-please don't stop!"
      jacklyn "Your pussy has been aching for this, hasn't it?"
      window hide
      pause 0.125
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.5)
      pause 0.25
      window auto
      jacklyn "For this kind of talking to."
      window hide
      pause 0.2
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.25)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.25)
      pause 0.25
      window auto
      nurse jacklyn_service fingering1 neck_lick "Mmmmm... god, yes..."
      # jacklyn "Do you think you have what it takes to be my submissive little nurse?"
      jacklyn "Do you think you have what it takes to be my submissive little nurse?{space=-30}"
      nurse jacklyn_service fingering1 neck_lick "I-I do, Miss!"
      jacklyn "Do you think you deserve to have your pink canoe paddled?"
      nurse jacklyn_service fingering1 neck_lick "P-paddled?"
      jacklyn "Oh, so you {i}are{/} into that corporeal pussy punch!"
      nurse jacklyn_service fingering1 neck_lick "I, um, I don't know..."
      jacklyn "Yes, you do."
      window hide
      pause 0.125
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.25)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering3 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering4 neck_lick with Dissolve(.1875)
      show nurse jacklyn_service fingering1 neck_lick with Dissolve(.25)
      pause 0.25
      window auto show
      show nurse jacklyn_service fingering1 imminent_orgasm with dissolve2
      # "Her head falls back against [jacklyn] now as [jacklyn]'s pace increases."
      "Her head falls back against [jacklyn] now as [jacklyn]'s pace increases.{space=-15}"
      "Her body is practically going limp as her knees literally grow weak."
      "I bet [jacklyn] really knows how to tease a clit."
      window hide
      pause 0.125
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.25)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.25)
      pause 0.25
      window auto
      jacklyn "We're just getting started here, you know?"
      jacklyn "But it does salt my caramel that you're this juicy for me already."
      jacklyn "You're such a good, soaking girl..."
      nurse jacklyn_service fingering1 imminent_orgasm "Th-thank you, Miss!"
      window hide
      pause 0.125
      show nurse jacklyn_service rubbing1 imminent_orgasm with Dissolve(.5)
      pause 0.25
      window auto
      jacklyn "I think you still have a bit more to prove, though."
      jacklyn "What kind of strong hand would I have if I let you cum so soon?"
      nurse jacklyn_service rubbing1 imminent_orgasm "N-no! God, no!"
      jacklyn "Oh, god, yes."
      nurse jacklyn_service rubbing1 imminent_orgasm "Please... [jacklyn]..."
      jacklyn "Please, what?"
      nurse jacklyn_service rubbing1 imminent_orgasm "P-please let me cum, Miss!"
      jacklyn "Hmm..."
      "Goddamn! Will she let her?"
      "I don't know if I can stand it. Just watching this has me wanting to cum right along with the [nurse]."
      window hide
      pause 0.125
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.375)
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.25)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.25)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.25)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.25)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.1875)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.1875)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.125)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.125)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.125)
      pause 0.0
      show nurse jacklyn_service fingering2 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering3 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering4 imminent_orgasm with Dissolve(.125)
      show nurse jacklyn_service fingering1 imminent_orgasm with Dissolve(.125)
      # pause 0.25
      window auto
      nurse jacklyn_service fingering1 imminent_orgasm "Oh, god... I'm going to—"
      window hide
      pause 0.125
      show nurse jacklyn_service orgasm_denial with Dissolve(.5)
      pause 0.25
      window auto
      nurse jacklyn_service orgasm_denial "..."
      jacklyn "Not yet."
      "Fuck me, that's hot!"
      "Just as she's about to cum, [jacklyn] pulls away at the last second."
      jacklyn "Yum! It's not bad."
      jacklyn "But it tastes a little too much like desperation, don't you think?"
      # jacklyn "You're my good girl, but you need to show me a little more of just how good you are."
      jacklyn "You're my good girl, but you need to show me a little more of just how{space=-50}\ngood you are."
      jacklyn "Some self-control."
      nurse jacklyn_service orgasm_denial "O-oh..."
      jacklyn "This is what you wanted, remember?"
      jacklyn "Someone with a stronger personality than you can muster."
      jacklyn "And I don't judge, you subby little lifesaver."
      # jacklyn "However, it does mean you have to tune your channel to [jacklyn] FM."
      jacklyn "However, it does mean you have to tune your channel to [jacklyn] FM.{space=-10}"
      nurse jacklyn_service orgasm_denial "..."
      nurse jacklyn_service orgasm_denial "I, err... I understand..."
      nurse jacklyn_service orgasm_denial "I'm sorry for questioning you, Miss."
      jacklyn "It's okay. We're just getting started here, babycakes."
      nurse jacklyn_service orgasm_denial "R-right. Thank you again for taking me on."
      jacklyn "It's going to be a dreamsicle!"
      window hide
      show nurse blush at Transform(xalign=.25)
      show jacklyn laughing at Transform(xalign=.75)
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      # "It's a bit of a slow start, but I knew the [nurse] would be in good hands with [jacklyn]."
      "It's a bit of a slow start, but I knew the [nurse] would be in good hands{space=-35}\nwith [jacklyn]."
      "I bet she'll push boundaries more and more as time goes on."
      "I'm glad it all checks out."
      "..."
      "But fuck me, do I need to get home and rub this out now..."
      # mc "Yep! I think the two of you will be great together! See you later, ladies! My work here is done!"
      mc "Yep! I think the two of you will be great together! See you later, ladies!{space=-45}\nMy work here is done!"
      $unlock_replay("nurse_dabble")
    "Leave":
      show nurse blush at move_to(.25)
      show jacklyn excited at move_to(.75)
      mc "No, actually. I think my work here is done."
      mc "I have full confidence that the [nurse] is in good hands now."
      jacklyn laughing "Token!"
      nurse blush "I think this will be good..."
      mc "Me too! Have fun, you two."
      $quest.nurse_aid["vajay_wagon_now"] = True
  window hide
  pause 0.0625
  scene black
  show nurse
  with Dissolve(.07)
  $game.location = "school_ground_floor_west"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location with Dissolve(.5)
  pause 0.125
  jump quest_nurse_aid_end

label quest_nurse_aid_someone_else_vajay_wagon:
  "It's hard not to wonder what's going on behind that door..."
  "But I'm sure [jacklyn] is experienced enough to take good care of the [nurse]."
  "I better give them some privacy."
  return

label quest_nurse_aid_relationship:
  show nurse blush with Dissolve(.5)
  nurse blush "Are we really doing this, [mc]?"
  mc "Definitely."
  # mc "If this is what you need, I'm more than happy to be the one to give it to you."
  mc "If this is what you need, I'm more than happy to be the one to give it{space=-5}\nto you."
  mc "No more [kate]. No more toxic shit."
  nurse concerned "That almost sounds too good to be true..."
  mc "Well, I promise you it's not."
  mc "We've tried distracting you and finding you new hobbies, but you can't seem to fight your nature."
  nurse thinking "My nature?"
  mc "Indeed. Your submissive nature."
  # mc "You just want to serve and please people, and there's nothing wrong with that."
  mc "You just want to serve and please people, and there's nothing wrong{space=-15}\nwith that."
  nurse blush "Err, I guess not..."
  mc "So, from now on, you can start serving and pleasing me."
  nurse blush "Oh, goodness!"
  mc "If you're okay with it, of course?"
  nurse blush "..."
  nurse blush "Yes, I am."
  mc "And you do know we can stop at any time, if you're not enjoying it?"
  nurse thinking "Oh, um. Okay."
  mc "I'm serious, [nurse]."
  mc "If you don't like it, you just have to say it. I'm not like [kate]."
  nurse blush "I understand. Thank you, [mc]."
  mc "Very well!"
  extend " Now, undress."
  nurse blush "Oh, my!"
  "Her eyes light up at the sound of my stricter tone."
  "Each word enunciated carefully, leaving no room for questioning."
  window hide
  pause 0.25
  $nurse.unequip("nurse_hat")
  pause 1.0
  $nurse.unequip("nurse_dress")
  pause 1.0
  $nurse.unequip("nurse_green_bra")
  pause 1.0
  $nurse.unequip("nurse_green_panties")
  pause 0.75
  window auto
  mc "Good girl."
  mc "Now, turn around and lean over your desk."
  nurse blush "O-okay..."
  mc "Wrong. Try, \"yes, sir.\""
  nurse blush "Oh, um. Y-yes, sir!"
  mc "That's better."
  window hide
  show nurse mc_service anticipation
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  nurse mc_service anticipation "Like this?"
  mc "Exactly like that."
  mc "Now, stand still and keep your legs spread."
  nurse mc_service anticipation "Yes, sir!"
  window hide
  pause 0.125
  show nurse mc_service bound with Dissolve(.5)
  pause 0.25
  window auto
  mc "Good girl."
  mc "Such a fast learner, aren't you?"
  nurse mc_service bound "Thank you, sir..."
  nurse mc_service bound "What, um, what is that for?"
  mc "Just a little bit of restraint. Plus, I like how it looks."
  nurse mc_service bound "Err, I understand."
  mc "Oh, you will."
  window hide
  pause 0.125
  show nurse mc_service dick_reveal with Dissolve(.5)
  pause 0.25
  window auto
  nurse mc_service dick_reveal "Goodness gracious!"
  mc "Do you like that?"
  nurse mc_service dick_reveal "I do, sir! Very much so!"
  window hide
  pause 0.125
  show nurse mc_service rubbing1 with Dissolve(.5)
  pause 0.0625
  show nurse mc_service rubbing2 with Dissolve(.375)
  pause 0.125
  show nurse mc_service rubbing3 with Dissolve(.5)
  pause 0.125
  show nurse mc_service rubbing4 with Dissolve(.5)
  pause 0.0625
  show nurse mc_service rubbing1 with Dissolve(.375)
  pause 0.25
  window auto
  mc "Is this what you want?"
  mc "My dick in your soaking wet pussy?"
  nurse mc_service rubbing1 "Mmm! Yes, please!"
  # nurse mc_service rubbing1 "I have been so needy for days... and the way you have been treating me..."
  nurse mc_service rubbing1 "I have been so needy for days... and the way you have been treating me...{space=-100}"
  nurse mc_service rubbing1 "I-I need it!"
  mc "I had a feeling you would say that."
  mc "From now on, this pussy is mine and only mine. Am I making myself clear?"
  nurse mc_service rubbing1 "Y-yes, sir! Crystal clear!"
  mc "Not even your own fingers are allowed in without my say so."
  nurse mc_service rubbing1 "Hnnngh..."
  window hide
  pause 0.125
  show nurse mc_service rubbing2 with Dissolve(.375)
  pause 0.125
  show nurse mc_service rubbing3 with Dissolve(.375)
  pause 0.125
  show nurse mc_service rubbing4 with Dissolve(.375)
  pause 0.0625
  show nurse mc_service rubbing1 with Dissolve(.375)
  pause 0.0625
  show nurse mc_service rubbing2 with Dissolve(.375)
  pause 0.125
  show nurse mc_service rubbing3 with Dissolve(.375)
  pause 0.125
  show nurse mc_service rubbing4 with Dissolve(.375)
  pause 0.0625
  show nurse mc_service rubbing1 with Dissolve(.375)
  pause 0.25
  window auto
  # "As if her loss of speech wasn't enough, I can tell how badly she needs it by the way she presses back into me."
  "As if her loss of speech wasn't enough, I can tell how badly she needs it{space=-80}\nby the way she presses back into me."
  "Trying to swallow my dick with her pulsating pussy."
  mc "Nuh-uh! Be patient."
  nurse mc_service rubbing1 "I aaaam!"
  mc "No, you're acting like a slut right now."
  nurse mc_service rubbing1 "Goodness gracious!"
  nurse mc_service rubbing1 "I, um... I'm sorry..."
  mc "I didn't say I don't like it."
  mc "Do you want this cock?"
  nurse mc_service rubbing1 "Yes, I do! P-please!"
  mc "Then, take it!"
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.5)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  nurse mc_service penetration1 "Ohhhhh!"
  "Fuck, she feels good!"
  "Her juices instantly coat my shaft as her warm hole welcomes me eagerly."
  mc "Do you... like... that...?"
  nurse mc_service penetration1 "Oh, goodness! More, sir!"
  mc "You got it!"
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  # "My dick slams into her hard and fast, and her pussy swallows it whole."
  "My dick slams into her hard and fast, and her pussy swallows it whole.{space=-55}"
  # "She releases her breath in a sharp exhale as she's pressed into the wood of the desk."
  "She releases her breath in a sharp exhale as she's pressed into the wood{space=-100}\nof the desk."
  "But I don't even care. I just want to fuck her brains out now."
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  # "My balls slap into her big, soft ass and I have to hold onto her wide hips for leverage."
  "My balls slap into her big, soft ass and I have to hold onto her wide hips{space=-80}\nfor leverage."
  "I love the feel of her skin beneath my fingertips."
  # "I can really dig in and hold onto her. It fills my head with the euphoric rush of fucking an older, mature woman."
  "I can really dig in and hold onto her. It fills my head with the euphoric{space=-25}\nrush of fucking an older, mature woman."
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  nurse mc_service penetration1 "Y-yes! Oh, god, yes!"
  # "Every time I slam into her, her breath is forced from her lungs and the desk scrapes against the floor."
  "Every time I slam into her, her breath is forced from her lungs and the{space=-40}\ndesk scrapes against the floor."
  # "But I have released all inhibitions. All that's left is this carnal need to fuck her as deeply as I can."
  "But I have released all inhibitions. All that's left is this carnal need to{space=-15}\nfuck her as deeply as I can."
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  nurse mc_service penetration1 "Goodness... you're so..."
  mc "Big? Amazing? What?"
  nurse mc_service penetration1 "...sooo forceful!"
  mc "Exactly what you need, isn't it?"
  # mc "Someone to take control... and fuck you hard... and tell you what to do... and how to do it..."
  mc "Someone to take control... and fuck you hard... and tell you what to do...{space=-80}\nand how to do it..."
  nurse mc_service penetration1 "Mmmm! Yes, sir! P-please!"
  mc "You're mine now, and whatever I say goes."
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  pause 0.25
  $renpy.transition(vpunch2)
  show nurse mc_service shoulder_bite with Dissolve(.5)
  pause 0.125
  window auto
  nurse mc_service shoulder_bite "O-oh!"
  "She squeals in surprise when my teeth make contact with her skin."
  # "Nothing too hard, but enough for her to feel it, and for me to leave a mark."
  "Nothing too hard, but enough for her to feel it, and for me to leave a{space=-5}\nmark."
  "She tastes like soap and body lotion, and the salt of her skin lingers on my tongue."
  "With long hard strokes, my cock conquers her trembling body."
  window hide
  pause 0.125
  show nurse mc_service penetration4 with Dissolve(.5)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  mc "Fuck, fuck, fuuuuck!"
  nurse mc_service penetration1 "Please, [mc]... please..."
  mc "Does my... good girl... want this cum...?"
  nurse mc_service penetration1 "I-I do!"
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.0
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with hpunch
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration1 with Dissolve(.2)
  pause 0.125
  window auto
  # "When she cries out, the urge to pump her full increases. The urge to give her every last bit."
  "When she cries out, the urge to pump her full increases. The urge to{space=-10}\ngive her every last bit."
  mc "I want you... to cum... with me..."
  mc "Oh, f-fuck!"
  window hide
  pause 0.125
  show nurse mc_service penetration2 with Dissolve(.1)
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration4 with Dissolve(.1)
  show white with {"master": Dissolve(.25)}
  show nurse mc_service penetration4 with hpunch
  hide white with {"master": Dissolve(.25)}
  pause 0.7
  show white with {"master": Dissolve(.25)}
  show nurse mc_service penetration4 with hpunch
  hide white with {"master": Dissolve(.25)}
  pause 1.0
  show white with {"master": Dissolve(.25)}
  show nurse mc_service penetration4 with hpunch
  hide white with {"master": Dissolve(.25)}
  pause 0.3
  show nurse mc_service penetration3 with Dissolve(.1)
  show nurse mc_service penetration2 with Dissolve(.2)
  # pause 0.25
  window auto show
  show nurse mc_service cum with dissolve2
  nurse mc_service cum "Ohhhhh!"
  "With a final thrust, my cock explodes inside her convulsing pussy."
  "She cums hard around my dick and presses her ass back into me."
  "Pulse after pulse, my balls unload the semen."
  window hide
  pause 0.125
  show nurse mc_service aftermath with Dissolve(.5)
  pause 0.25
  window auto
  # "Meanwhile, she keeps spasming so hard that it forces my dick out of her."
  "Meanwhile, she keeps spasming so hard that it forces my dick out of{space=-15}\nher."
  # "In a fountain, our mixed juices squirt out of her pink pussy and onto the floor."
  "In a fountain, our mixed juices squirt out of her pink pussy and onto{space=-5}\nthe floor."
  mc "Goddamn..."
  nurse mc_service aftermath "Goodness... gracious..."
  # "As she comes down from the intense pleasure, her voice regains some composure."
  "As she comes down from the intense pleasure, her voice regains some{space=-55}\ncomposure."
  "Back to her usual, soft spoken self."
  mc "Get dressed."
  # mc "I want you to walk around in your cum stained panties the rest of the day."
  mc "I want you to walk around in your cum stained panties the rest of the day.{space=-110}"
  nurse mc_service aftermath "Oh, my! How embarrassing!"
  mc "Do I need to repeat myself?"
  nurse mc_service aftermath "N-no, sir!"
  window hide
  hide screen interface_hider
  show nurse aftercare
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 1.0
  show black onlayer screens zorder 4
  $nurse.equip("nurse_green_panties")
  $nurse.equip("nurse_green_bra")
  $nurse.equip("nurse_dress")
  $nurse.equip("nurse_hat")
  $set_dialog_mode("default_no_bg")
  "After we both dress and I regain a figment of composure, we sit back on the bed to discuss what just happened."
  "And somehow, she ends up in my arms."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "So, what do you think?"
  nurse aftercare "About what?"
  mc "Is this something you'd like to continue to pursue with me?"
  nurse aftercare "Oh, um. Yes. It really is."
  mc "Good. Me too."
  mc "I'm excited to explore what a healthy kinky relationship looks like together."
  mc "This is going to sound a bit corny, but you're a wonderful woman, and you deserve to be happy, both in and out of the bedroom."
  nurse aftercare "Aw! That's very sweet of you."
  mc "You're very brave to make yourself so vulnerable to me like this."
  nurse aftercare "Th-thank you..."
  mc "I just want you to understand that you never have to do anything you don't want to, even if I push you."
  mc "Don't be afraid to ask me to stop for any reason, okay?"
  nurse aftercare "I, um, I understand."
  nurse aftercare "I look forward to playing with you again."
  mc "Heh. So do I."
  # mc "And if you want me to go harder next time, then we should talk about a safeword."
  mc "And if you want me to go harder next time, then we should talk about{space=-30}\na safeword."
  nurse aftercare "Oh, um. Okay."
  nurse aftercare "..."
  mc "..."
  "We sit there in silence for a moment, just enjoying the ease of each other's company."
  "The [nurse] really does have a comforting aura about her."
  # "How anyone could ever mistreat her in a malicious way is beyond me."
  "How anyone could ever mistreat her in a malicious way is beyond me.{space=-30}"
  "She is a person, after all, and one of the best I have met."
  "..."
  "And yet, I don't even know her name. How crazy is that?"
  # mc "Err, this is going to sound a bit strange, and hopefully you won't think any less of me, but have you ever told me your name?"
  mc "Err, this is going to sound a bit strange, and hopefully you won't think{space=-40}\nany less of me, but have you ever told me your name?"
  mc "I'm sorry. I know I should have asked you a long time ago."
  nurse aftercare "Oh, um. That's fine. No one ever asks."
  "Ouch. That's so sad."
  mc "So, what is your name?"
  nurse aftercare "It's Amelia."
  mc "Amelia. That's a beautiful name."
  $nurse.default_name = "Amelia"
  $amelia = nurse
  amelia "Thank you."
  # mc "Remember, [amelia], that no matter how I might treat you in the bedroom, we can always talk about it and you can give me your input."
  mc "Remember, [amelia], that no matter how I might treat you in the bedroom,{space=-90}\nwe can always talk about it and you can give me your input."
  mc "I want this to be good for both of us."
  $amelia.love+=3
  amelia "Thank you, [mc]..."
  mc "I look forward to calling you by your name from now on."
  amelia "I like the way it sounds coming from you."
  "She sounds so at ease and happy again. It makes my heart swell."
  # mc "Anyway, as much as I enjoy your company, I should probably let you get back to work."
  mc "Anyway, as much as I enjoy your company, I should probably let you{space=-5}\nget back to work."
  amelia "Ah. Right."
  mc "Goodbye for now, [amelia]."
  amelia "..."
  amelia "Lollipop."
  mc "Err, what?"
  amelia "That's my safeword. If you want to play again and go a bit harder?"
  mc "Oh! Very well. I will keep it in mind."
  amelia "Bye, [mc]..."
  $unlock_replay("nurse_relationship")
  $quest.nurse_aid["girlfriend"] = nurse["girlfriend"] = True
  $quest.nurse_aid["name_reveal"] = True
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  $game.location = "school_ground_floor_west"
  pause 1.0
  hide nurse
  hide black onlayer screens
  with Dissolve(.5)

label quest_nurse_aid_end:
  window auto
  if quest.nurse_aid["name_reveal"]:
    "Well, that turned into quite the journey for [amelia], didn't it?"
  else:
    "Well, that turned into quite the journey for the [nurse], didn't it?"
  "And in the process, a journey for me, too."
  "It feels good to try and do the selfless thing by helping someone out."
  "Especially someone like her who almost never does for herself, but is always doing for others."
  if quest.nurse_aid["name_reveal"]:
    "Not that I didn't get something out of it myself in the end..."
    "...but I really do think we can make this work together."
    # "I look forward to seeing how it goes, and how open she can really be with me."
    "I look forward to seeing how it goes, and how open she can really be{space=-15}\nwith me."
  "It's nice that she seems so much happier already than she did when I found her on the roof a few days ago."
  if quest.nurse_aid["name_reveal"]:
    "So, mission accomplished. And now I need to make sure I don't fuck this up."
    "Maybe she's the happiness I've been looking for?"
  else:
    "Hopefully, her happiness blossoms from now on. She definitely deserves it."
  window hide
  if quest.nurse_aid["vajay_wagon_now"]:
    $renpy.transition(Dissolve(.125))
    $nurse["at_none_now"] = jacklyn["at_none_now"] = True
  if quest.nurse_aid["name_reveal"]:
    $quest.nurse_aid.finish()
  else:
    $quest.nurse_aid.finish("done_jacklyn")
  return
