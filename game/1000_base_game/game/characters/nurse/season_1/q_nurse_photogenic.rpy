image nurse_photogenic_not_romance_disabled = LiveComposite((128,128), (6,6), LiveComposite((116,116), (4,5), Transform("lindsey contact_icon_evil", size=(106,106))), (6,6), "ui crossed_out_circle1")


label quest_nurse_photogenic_strawberry_juice(item):
  $mc.remove_item(item)
  if item == "strawberry_juice":
    $mc.add_item("empty_bottle")
  elif item == "spray_strawberry_juice":
    $mc.add_item("spray_empty_bottle")
  mc "Ah, the lifeblood courses through my veins once more!"
  $quest.nurse_photogenic["strawberry_juice_consumed_today"] = True
  return True

label quest_nurse_photogenic_start:
  $quest.nurse_photogenic.start()
  "Damn, that last staircase took the piss out of me..."
  "Too many pizzas, too little exercise..."
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  "Hmm... this has never happened before..."
  "My heart is trying to break out of my chest..."
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  "I never knew what the galaxy looked like up close. They say that there{space=-35}\nare more stars in the night sky than grains of sand in the Sahara..."
  "With so many stars, there's bound to be planets full of girls interested in me..."
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  "Then again, why would they be interested in someone who can't even make it up a flight of stairs without gasping for air?"
  "Man, I really hope there's a civilization out there who worships bad diets, shitty sleeping schedules, and a lack of social skills..."
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  "So... dizzy..."
  "What's happening...?"
  menu(side="middle"):
    extend ""
    "Call for help":
      $mc.charisma-=1
      $mc.intellect+=1
      mc "Can someone get the [nurse], please?"
      "..."
      "That's pretty embarrassing. Now everyone's looking at me."
      show flora laughing at appear_from_left
      flora laughing "What are you up to now?"
      "Of course, [flora] has to show up for my moment of weakness..."
      flora worried "You look pale. Are you okay?"
      mc "I feel a little lightheaded..."
      flora thinking "Let's go to the [nurse]'s office."
      flora thinking  "Right now."
      mc "Yeah, okay..."
      window hide
      show black onlayer screens zorder 100 with close_eyes
      call goto_school_ground_floor_west
      $mc["focus"] = "nurse_photogenic"
      pause 1.0
      hide black onlayer screens
      show flora annoyed
      with open_eyes
      window auto
      flora annoyed "Gross, you drooled on my shirt."
      mc "That's just your sweat..."
      flora annoyed "If it is, it's because you weigh a ton."
      flora sarcastic "I can't believe I dragged you all the way here! Call me Wonder Woman!{space=-45}"
      mc "I leaned on you a little bit, that's all..."
      flora eyeroll "Whatever. Next time don't ask me for help."
      show flora eyeroll at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Sorry, [flora]... I'm grateful you\nwere around.\"":
          show flora eyeroll at move_to(.5)
          mc "Sorry, [flora]... I'm grateful you were around."
          flora skeptical "You know what? I prefer it when you're annoying."
          flora skeptical "This is weirding me out..."
          "Great. Got to love lose-lose situations."
          mc "Don't, uh... don't help me next time I ask?"
          flora afraid "That's the best you've got? You really must be dizzy."
          mc "Yeah, it's pretty bad..."
          $flora.love+=1
          flora smile "For what it's worth, I'm glad you didn't fall down the stairs."
          flora smile "But I need to get back to my classes now, okay?"
          mc "Thanks, I appreciate the help..."
          flora smile "No worries! Talk to you later!"
          window hide
          show flora smile at disappear_to_right
          pause 0.5
          window auto
          "Okay, time to talk to the [nurse] before I pass out."
        "\"Noted. Next time, I'll just die on\nthe floor.\"":
          show flora eyeroll at move_to(.5)
          mc "Noted. Next time, I'll just die on the floor."
          flora smile "Awesome! Can I have your Wiintendo then?"
          mc "For the hundredth time — that's the company name, not the console! The console is called xCube!"
          flora skeptical "As long as it plays games, I don't care what it's called."
          mc "Well, in that case, I'm donating it to charity if I die."
          flora afraid "You would not!"
          show flora afraid at move_to(.25)
          menu(side="right"):
            extend ""
            "\"It's already in my will.\"":
              show flora afraid at move_to(.5)
              $mc.charisma+=1
              mc "It's already in my will."
              flora annoyed "..."
              flora annoyed "It honestly wouldn't surprise me."
              mc "What's in your will, [flora]?"
              flora sarcastic "For you? Only my middle finger."
              mc "I can do a lot with just one finger..."
              flora cringe "Eww!"
              flora cringe "That's it! I'm out of here!"
              window hide
              show flora cringe at disappear_to_right
              pause 0.5
              window auto
              "Probably not the best thing to say to her... but not the worst either."
              "Okay, I definitely need to see the [nurse] now before I pass out."
            "\"No, I would have it cremated with my remains. The xCube and I would watch you judgingly from our urn on your nightstand.\"":
              show flora afraid at move_to(.5)
              mc "No, I would have it cremated with my remains. The xCube and I would watch you judgingly from our urn on your nightstand."
              flora eyeroll "Your urn would never be on my nightstand."
              mc "Why not?"
              flora sarcastic "You're banned from my room, remember? You think you can circumvent that by dying?"
              "Damn it! She has thought of everything."
              mc "It'll be in the bathroom, then. Right next to the shower."
              $flora.lust+=1
              flora annoyed "Dream on, perv."
              mc "A dead person can't be a perv, [flora]."
              flora confused "If anyone could pull it off, it'd be you."
              flora confused "Anyway, I have to go. Don't faint out here."
              window hide
              show flora confused at disappear_to_right
              pause 0.5
              window auto
              "Okay, time to talk to the [nurse] before I pass out."
        "\"Listen to me carefully, [flora] — if you ever{space=-5}\nget pulled into the sewers by a tentacle abomination... I will stand idly by.\"":
          show flora eyeroll at move_to(.5)
          $mc.lust+=1
          mc "Listen to me carefully, [flora] — if you ever get pulled into the sewers by a tentacle abomination... I will stand idly by."
          $flora.lust+=1
          flora afraid "W-what?"
          mc "The old gods aren't kind. Remember that."
          flora skeptical_hands_up "..."
          show flora skeptical_hands_up with vpunch:
            linear .25 zoom 1.5 yoffset 750
          show flora skeptical_hands_up:
            linear .25 zoom 1 yoffset 0
          mc "Ouch!"
          flora skeptical_hands_up "That's what you get!"
          mc "Hitting a dying man?! Now I've seen everything!"
          flora smile "Shut up!"
          flora smile "I... I have to go!"
          window hide
          show flora smile at disappear_to_right
          pause 0.5
          window auto
          "Years of practice and meditation have left me with the darkest of insights, the most secret of techniques..."
          "Getting under [flora]'s battle-hardened skin is a mere walk in the park.{space=-30}"
          "Anyway, time to talk to the [nurse] before I pass out."
      "Huh? Is someone there?"
      "..."
      "Man, what the hell is wrong with me?"
      "Where are those voices coming from?"
    "?not lindsey['romance_disabled']@|{image=nurse_photogenic_not_romance_disabled}|It's probably nothing serious" if lindsey["romance_disabled"]:
      pass
    "It's probably nothing serious" if not lindsey["romance_disabled"]:
      $mc.intellect-=1
      $mc.strength+=1
      "The hours at the gym have surely prepared my body for a little wave of dizziness..."
      "Besides, what kind of pussy would call for help?"
      "I just need to stay focused. Deep breaths."
      "..."
      "Ugh, I need to sit down before—"
      play sound "falling_thud"
      show black onlayer screens zorder 100 with vpunch
      call goto_school_ground_floor_west
      $mc["focus"] = "nurse_photogenic"
      pause 2.0
      hide black onlayer screens
      show lindsey blush
      with open_eyes
      window auto
      lindsey blush "Oh, good! You're awake again!"
      lindsey thinking "I was worried I wouldn't be able to drag you all the way..."
      mc "Ugh, what happened?"
      lindsey cringe "You passed out."
      mc "You dragged me here all by yourself?"
      lindsey laughing "I'm quite strong, but no. [maxine] helped."
      show lindsey laughing at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Where is she now? And what was she doing outside her office?!\"":
          show lindsey laughing at move_to(.5)
          mc "Where is she now? And what was she doing outside her office?!"
          $mc.charisma+=1
          mc "The last bit is more upsetting than me fainting."
          lindsey smile "It is a rare sight!"
          lindsey smile "She's right outside. Got a super important phone call or something."
          mc "More important than helping a dying man?"
          lindsey flirty "You know how she is."
          show lindsey flirty at move_to(.25)
          menu(side="right"):
            extend ""
            "\"Yeah, she's a complete weirdo.\"":
              show lindsey flirty at move_to(.5)
              $mc.lust+=1
              mc "Yeah, she's a complete weirdo."
              lindsey concerned "She used to be more normal..."
              mc "I don't remember that."
              lindsey concerned "It was a long time ago, but maybe I'm holding on to the good memories.{space=-75}"
              mc "Probably. I only ever knew her as an eccentric."
              lindsey sad "Yeah, it's been going on for a while..."
              mc "I guess it's easy to get sucked into conspiracy holes."
              lindsey annoyed "I wish there was something I could do."
              show lindsey annoyed at move_to(.75)
              menu(side="left"):
                extend ""
                "\"She's a lost cause.\"":
                  show lindsey annoyed at move_to(.5)
                  mc "She's a lost cause."
                  lindsey cringe "I don't usually give up on people, but you might have a point."
                  mc "I've always found her too difficult to deal with."
                  lindsey thinking "She used to have her moments, but I barely see her anymore."
                  mc "It's probably best to move on."
                  lindsey thinking "I suppose that's true..."
                "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"Have you tried talking to her?\"":
                  show lindsey annoyed at move_to(.5)
                  mc "Have you tried talking to her?"
                  lindsey eyeroll "Many times. She's always busy."
                  mc "Maybe schedule a meeting with her? Make sure you have her full attention.{space=-130}"
                  lindsey laughing "That's a good idea!"
                  lindsey flirty "I'll definitely do that. Thank you!"
                  if mc["moments_of_cupidity"] < 3:
                    $mc["moments_of_cupidity"]+=1
                    $game.notify_modal(None,"Guts & Glory","Moments of Cupidity:\n"+str(mc["moments_of_cupidity"])+"/3 Unlocked!",wait=5.0)
                  mc "Happy to help!"
                "\"Are you sure you have to?\"":
                  show lindsey annoyed at move_to(.5)
                  mc "Are you sure you have to?"
                  lindsey thinking "What do you mean?"
                  mc "Maybe she'll come around if you just wait."
                  lindsey thinking "Maybe..."
                  mc "All I'm saying is that giving her another chance might be worth it."
                  $lindsey.love+=1
                  lindsey blush "Maybe you're right!"
                  lindsey blush "I'll try to talk to her, at least!"
            "\"I think she's quite charming!\"":
              show lindsey flirty at move_to(.5)
              $mc.love+=1
              mc "I think she's quite charming!"
              lindsey laughing "She does have her moments!"
              mc "I've always found her type fascinating."
              lindsey smile "I've always thought she was unique."
              mc "I guess that's true."
              lindsey flirty "So, you find her fascinating?"
              $maxine.love+=1
              lindsey flirty "I'll let her know!"
              mc "It'll go over her head..."
              lindsey laughing "Maybe! Who knows?"
        "\"You two are unstoppable together!\"":
          show lindsey laughing at move_to(.5)
          mc "You two are unstoppable together!"
          $lindsey.love+=1
          lindsey concerned "I agree, but I doubt she thinks so..."
          mc "How come?"
          lindsey sad "It's been a while since we sat down and had a genuine talk. I miss that.{space=-55}"
          mc "Why don't you ask her for one?"
          lindsey sad "I don't want to waste her time. You know how she is."
          show lindsey sad at move_to(.25)
          menu(side="right"):
            extend ""
            "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"If it's important to you, maybe just book an appointment with her?\"{space=-35}":
              show lindsey sad at move_to(.5)
              mc "If it's important to you, maybe just book an appointment with her?"
              lindsey laughing "Oh, good idea! That would be funny!"
              lindsey laughing "I just might!"
              if mc["moments_of_cupidity"] < 3:
                $mc["moments_of_cupidity"]+=1
                $game.notify_modal(None,"Guts & Glory","Moments of Cupidity:\n"+str(mc["moments_of_cupidity"])+"/3 Unlocked!",wait=5.0)
            "\"Yeah, she can be difficult...\"":
              show lindsey sad at move_to(.5)
              mc "Yeah, she can be difficult..."
              lindsey laughing "Very."
      lindsey smile "Do you think you can make it the rest of the way on your own without fainting?"
      mc "Yeah, no problem! I think it's just low blood sugar or something."
      mc "Thanks for the help!"
      if quest.lindsey_nurse["carried"]:
        lindsey flirty "Now we're even!"
        show lindsey flirty at move_to(.75)
        menu(side="left"):
          extend ""
          "?mc.charisma>=3@[mc.charisma]/3|{image=stats cha}|\"I mean... dragging and carrying someone is a bit different, don't you think?\"":
            show lindsey flirty at move_to(.5)
            mc "I mean... dragging and carrying someone is a bit different, don't you think?{space=-130}"
            lindsey laughing "I suppose that's true."
            mc "So, that means you still owe me... big time!"
            lindsey flirty "Yeah?"
            lindsey flirty "How will I ever repay you?"
            mc "If this doesn't kill me, there'll be plenty of time for you to hit the gym.{space=-30}"
            mc "I'll be expecting a proper carrying."
            $lindsey.lust+=3
            lindsey laughing "Maybe I can repay you in some other way!"
            mc "I'm sure we can come to an agreement!"
          "\"Again, I really appreciate it. You're a lot stronger than you look.\"":
            show lindsey flirty at move_to(.5)
            mc "Again, I really appreciate it. You're a lot stronger than you look."
            $lindsey.love+=1
            lindsey laughing "Thank you! I've been spending more time at the gym lately!"
            $mc.strength+=1
            mc "If this doesn't kill me, we should totally meet at the gym some time."
            lindsey flirty "Sure thing!"
      else:
        lindsey flirty "Don't mention it!"
      lindsey flirty "Anyway, I'll see you around, [mc]!"
      window hide
      show lindsey flirty at disappear_to_right
      pause 0.5
      window auto
      "Okay, time to talk to the [nurse] before I pass out."
      "Huh? Is someone there?"
      "..."
      "Man, what the hell is wrong with me?"
      "Where are those voices coming from?"
  $quest.nurse_photogenic.advance("eavesdrop")
  return

label quest_nurse_photogenic_eavesdrop:
  if not quest.nurse_photogenic["eavesdropped"]:
    lindsey "...You did what?"
    maxine "There's no need to concern yourself with this."
    lindsey "You know that's illegal, right?"
    maxine "The quest for truth is above all laws."
    lindsey "You could get in trouble!"
    maxine "Then, so be it. Someone has to be prepared."
    lindsey "Prepared for what?"
    maxine "Why, the Juice Apocalypse, of course."
    lindsey "The what?"
    maxine "Trying times lie ahead..."
    lindsey "..."
    lindsey "You know he could've gotten hurt, right?"
    maxine "A small sacrifice in the grand scheme of things."
    lindsey "Look around, these are real people! You can't experiment on them!"
    maxine "On the contrary, it seems to be working quite well."
    lindsey "I can't believe it, Max!"
    maxine "It's nothing for you to worry about."
    maxine "Besides, it was a great success."
    lindsey "You don't know that! What if it has lasting effects?"
    maxine "It doesn't. I've tested it on myself already."
    lindsey "What about your heart? You're not supposed to put drugs into your body.{space=-110}"
    maxine "It's not exactly drugs... it's more of a chemical compound that—"
    lindsey "Seriously, Max!"
    lindsey "Please, you need to stop this. Why can't we go back to how it used to be?{space=-110}"
    maxine "I'm a truth-seeker. This is my job."
    maxine "Besides, you were the one that ended that era."
    maxine "In any case, my duties are calling."
    lindsey "..."
    lindsey "Oh my god..."
    $achievement.the_all_seeing_one.unlock()
    "That's weird. It almost sounded like [maxine] did this to me."
    "Maybe it's the dizziness talking, but if she's conducting experiments{space=-5}\non the students, someone needs to stop her..."
    "...although, most people here deserve it."
    "But the strawberry juice is sacred! She can't be allowed to spike it!"
    $quest.nurse_photogenic["eavesdropped"] = True
    $game.notify_modal(None,"Coming soon","{image=maxine contact_icon}{space=25}|"+"The Juice Apocalypse\nis currently being worked on!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image=items monkey_wrench}",5.0)
#   $quest.maxine_experiments.start() ## Placeholder ## The Juice Apocalypse ##
    "Ugh, anyway..."
  "I need to get to the [nurse]... who knows what [maxine] poisoned me with?{space=-75}"
  return

label quest_nurse_photogenic_curtain:
  if mc.owned_item("compromising_photo"):
    nurse "...I can't."
    nurse "..."
    nurse "...yes, but—"
    nurse "..."
    nurse "Fine! I know that! I'll be there!"
    nurse "..."
    nurse "Sorry, I didn't mean to shout..."
    nurse "..."
    nurse "No, please! I'll do better, I promise!"
    "I'm not sure what's going on behind there, but it sounds serious..."
    menu(side="middle"):
      extend ""
      "Pull back the curtain":
        $school_nurse_room["curtain_off"] = True
        show nurse neutral_phone with Dissolve(.5)
        show nurse surprised_phone with dissolve2
        nurse surprised_phone "Aah!" with vpunch
        nurse afraid_phone "I have to go! Sorry!"
        $nurse.lust+=1
        nurse afraid "Oh my goodness! You scared the everloving bejesus out of me!"
        if quest.kate_desire["stakes"] == "release":
          mc "Give me the phone."
          nurse annoyed "I-I don't think—"
          mc "Do as I say."
          nurse concerned "O-okay..."
          window hide
          $set_dialog_mode("phone_call","kate")
          pause 0.5
          mc "[kate]?"
          kate "[mc]? What the fuck\ndo you want?"
          mc "We had a deal. The [nurse] is\nno longer any of your concern."
          kate "God, you're lame."
          mc "Don't ever call her again."
          kate "No one talks to me like—"
          play sound "end_call"
          $set_dialog_mode("")
          pause 0.5
          window auto
          mc "Here's your phone back."
          nurse blush "Thanks..."
          mc "I've blocked her number. If she tries to call you again, you let me know.{space=-55}"
          nurse blush "O-okay."
        else:
          mc "Sorry, I should've knocked..."
      "Keep listening":
        nurse "..."
        nurse "No, it's not like that."
        nurse "..."
        nurse "It's too risky..."
        nurse "..."
        nurse "Please, [kate]..."
        nurse"..."
        nurse "Yes... I'll be there..."
        nurse"..."
        nurse "...thank you."
        nurse "I have to go now."
        $school_nurse_room["curtain_off"] = True
        show nurse neutral_phone with Dissolve(.5)
        show nurse surprised_phone with dissolve2
        nurse surprised_phone "My goodness! How long have you been here?" with vpunch
        if quest.kate_desire["stakes"] == "release":
          mc "Give me the phone."
          nurse afraid_phone "I-I don't think—"
          mc "Do as I say."
          nurse concerned "O-okay..."
          window hide
          $set_dialog_mode("phone_call","kate")
          pause 0.5
          mc "[kate]?"
          kate "[mc]? What the fuck\ndo you want?"
          mc "We had a deal. The [nurse] is\nno longer any of your concern."
          kate "God, you're lame."
          mc "Don't ever call her again."
          kate "No one talks to me like—"
          play sound "end_call"
          $set_dialog_mode("")
          pause 0.5
          window auto
          mc "Here's your phone back."
          nurse blush "Thanks..."
          mc "I've blocked her number. If she tries to call you again, you let me know.{space=-55}"
          nurse blush "O-okay."
        else:
          mc "Not that long..."
          "So, she was talking to [kate]. It sounded like she's being blackmailed.{space=-5}"
          "I wouldn't put it past [kate] to do something like that..."
          "Somehow, the [nurse] seems to end up in all sorts of sticky situations{space=-20}\nlately. It's odd... She used to be so prim and proper."
  else:
    nurse "Mr. Brown!"
    nurse "That's not how you use a medical massager!"
    nurse "Oh! Oh, my... {i}Mr. Brown!{/}"
    nurse "You can't let anyone know! My reputation! My job!"
    "Despite being on the verge of fainting, this seems like one of those opportunities you just can't pass up on..."
    "Maybe it's wrong to intrude on something like this, but..."
    call quest_nurse_photogenic_second_chance
  mc "Anyway, I'm feeling really dizzy. That's why I came here."
  if nurse.equipped_item("nurse_masturbating"):
    nurse concerned "Oh, um... okay, let me just..."
    window hide
    $school_nurse_room["curtain_off"] = False
    hide nurse with Dissolve(.5)
    window auto
    nurse "..."
    nurse "..."
    window hide
    $school_nurse_room["curtain_off"] = True
    $nurse.equip("nurse_outfit")
    show nurse concerned with Dissolve(.5)
    window auto
  else:
    nurse concerned "Oh, um... okay, let me have a look at you."
    nurse concerned "..."
  nurse concerned "Have you eaten enough today?"
  mc "I think so."
  nurse concerned "Okay, what about water? Are you staying hydrated?"
  mc "Yeah, I just had strawberry juice."
  nurse neutral "Your heart rate is fine."
  nurse neutral "Have you been sleeping well?"
  mc "Err..."
  nurse neutral "Getting enough hours is very important. You do look tired."
  "She might be right. This whole going back in time ordeal is taking its toll."
  nurse neutral "Stress can be a huge problem; especially if you combine it with poor sleep."
  nurse smile "Here, move to the bed and rest a bit."
  mc "Okay."
  nurse smile "There you go. I think lying down will help as well."
  nurse smile "Just relax and let yourself drift."
  window hide
  hide nurse
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 1.0
  $mc["focus"] = ""
  $current_game_hour = game.hour
  while game.hour != current_game_hour+2:
    $game.advance()
  $nurse.location,nurse.activity = "school_nurse_room","sitting"
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  show nurse blush with Dissolve(.5)
  window auto
  nurse blush "Oh, good! You're awake. How do you feel?"
  mc "Yeah, all good now..."
  mc "How long was I out for?"
  nurse thinking "You slept for a couple of hours."
  nurse concerned "I think it might be poor sleeping habits. You need to start going to bed on time."
  nurse concerned "Hopefully, these will help. Just take one before bed and you'll sleep tight.{space=-95}"
  $mc.add_item("sleeping_pills")
  nurse concerned "And if you get dizzy again, you need to go to the hospital and have a doctor look at you."
  mc "I feel fine now. Thanks!"
  nurse blush "Don't hesitate to stop by if you need to!"
  hide nurse with Dissolve(.5)
  $quest.nurse_photogenic.finish()
  return
