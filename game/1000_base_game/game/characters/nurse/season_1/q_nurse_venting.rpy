screen walkie_talkie(xpos=None):
  add "#0008" at phone_backdrop_fade(phone_is_showing)

  fixed:
    add "misc walkie_talkie" xpos xpos or 734 ypos 29
    at phone_slide_in_and_out(phone_is_showing)


label quest_nurse_venting_start:
  show maxine sad_fishing with Dissolve(.5)
  mc "I don't believe what I'm seeing."
  maxine sad_fishing "On the road to truth, the eyes are false signposts."
  mc "I don't know anything about eye signposts, but what I'm seeing is you fishing in the school vents."
  maxine sad_fishing "This time your signposts are correct."
  mc "What are you fishing for? Dust bunnies?"
  maxine confident_fishing "Soap goblins."
  "Talking to [maxine] is like talking to a brick wall with its head in the clouds."
  mc "What are soap goblins?"
  maxine eyeroll_fishing "They're goblins. Made of soap."
  maxine skeptical_fishing"Very dangerous, but their hide can be used to make bombs."
  mc "Do I even want to know what you're planning on blowing up?"
  maxine concerned_fishing "I'd never blow something up!"
  maxine excited_fishing "Or, if I was going to blow something up, I'd never tell someone beforehand."
  maxine excited_fishing "I'll use the soap goblins as soap. That's all."
  "That's a relief. I was about to call in a bomb squad."
  mc "All right... tell me, are you using a fishing hook to snag the soap goblins?"
  maxine smile_fishing "Can't go fishing without a fishing hook, can I?"
  mc "Can I borrow the hook when you're done?"
  maxine smile_fishing "Maybe. If I get a good haul."
  mc "But what about if you get no haul, because soap goblins aren't real?{space=-5}"
  maxine angry_fishing "Soap goblins are very much real! In fact, just the other day—"
  window hide
  play sound "<to 0.6>metal_scrape" volume 0.5
  show layer master
  with vpunch
  pause 0.5
  play sound "<from 0.5>fishing_reel"
  show maxine smile_fishing with Dissolve(.5):
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  window auto
  maxine smile_fishing "I've got one!"
  play sound "<from 0.5>fishing_reel"
  show maxine smile_fishing:
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  maxine smile_fishing "Soap goblins always bite when there's doubt in the air."
  play sound "<from 0.5>fishing_reel"
  show maxine laughing_fishing with Dissolve(.5):
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  maxine laughing_fishing "Wow, it's a big one! It's really fighting me!"
  play sound "<from 0.5>fishing_reel"
  show maxine laughing_fishing:
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  maxine laughing_fishing "I can hardly get it to budge at all!"
  "Of course it won't budge... she's caught on a piece of metal."
  mc "Let it out a little. See if that dislodges it."
  play sound "<from 0.5>fishing_reel"
  show maxine angry_fishing with Dissolve(.5):
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  maxine angry_fishing "It's not lodged anywhere! It's a big soap goblin!"
  play sound "<from 0.5>fishing_reel"
  show maxine angry_fishing:
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  maxine angry_fishing "Help me pull!"
  show maxine angry_fishing at move_to(.75)
  menu(side="left"):
    extend ""
    "Help her pull":
      show maxine angry_fishing at move_to(.5)
      mc "Fine! Let me grab the rod."
      $maxine.love+=1
      maxine flirty_fishing "Thank you! Let's get this soap goblin out of there!"
      maxine flirty_fishing "I'll let you keep some of the soap once I've butchered it!"
      "If a soap goblin shoots up through this vent, I'll never doubt [maxine] again..."
      mc "We'll give it a big pull on three."
      window hide
      show black onlayer screens zorder 100
      show maxine afraid
      with Dissolve(.5)
      pause 0.25
      play sound "<from 0.5>fishing_reel"
      pause 0.5
      play sound "line_snapping"
      show black onlayer screens with vpunch
      play sound "<to 0.6>metal_scrape" volume 0.5
      pause 0.75
      hide black onlayer screens with Dissolve(.5)
      window auto
      maxine afraid "The line snapped! It got away!"
      mc "And that means the fishing hook is now stuck in the vent..."
      maxine afraid "Who cares about the fishing hook?!"
      maxine afraid "That was the biggest soap goblin I've never seen!"
      "The biggest she's {i}never{/} seen? What does that even mean?"
      maxine thinking "A stronger line, that's what I need."
      mc "Can you think of a way to get the hook?"
      maxine sad "Steel, maybe? No, too weak..."
      maxine thinking "Carbon nano-filament. That's the ticket."
      maxine thinking "I'll do some research."
    "Let her figure it out":
      show maxine angry_fishing at move_to(.5)
      mc "There's no such thing as soap goblins. You're on your own for\nthis one."
      $maxine.love-=1
      maxine afraid_fishing "We'll see about that!"
      maxine thinking_fishing "You'll be so jealous when you see how clean the soap gets me."
      maxine thinking_fishing "And don't even get me started on the bombs I'll make."
      play sound "<from 0.5>fishing_reel"
      show maxine afraid_fishing with Dissolve(.5):
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      maxine afraid_fishing "If I could just... get this line... to move!"
      "[maxine] is officially losing her mind."
      "Maybe she'll become a touch more sane if she sees that soap goblins don't exist?"
      mc "Pull with your back."
      play sound "<from 0.5>fishing_reel"
      show maxine afraid_fishing:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      maxine afraid_fishing "You had your chance to be helpful!"
      mc "Stop dragging the line on the edge of the vent. It's fraying."
      play sound "<from 0.5>fishing_reel"
      show maxine afraid_fishing:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      maxine afraid_fishing "I know what I'm doing!"
      mc "It's gonna break..."
      play sound "<from 0.5>fishing_reel"
      show maxine afraid_fishing:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      maxine afraid_fishing "No, it won't! I've got it loose!"
      window hide
      show black onlayer screens zorder 100
      show maxine concerned
      with Dissolve(.5)
      pause 0.25
      play sound "<from 0.5>fishing_reel"
      pause 0.5
      play sound "line_snapping"
      show black onlayer screens with vpunch
      play sound "<to 0.6>metal_scrape" volume 0.5
      pause 0.75
      hide black onlayer screens with Dissolve(.5)
      window auto
      mc "It frayed right through! I told you that would happen!"
      maxine concerned "This wouldn't have happened if you'd helped me."
      mc "And now the hook is lost in the vent..."
      maxine concerned "Maybe the trick is getting the soap gobling to come to me?"
      maxine concerned "They subsist on a diet of centipedes and mildew."
      maxine excited "Maybe if I set up a centipede terrarium, with a fan to blow the smell into the vents..."
  $quest.nurse_venting.start(silent=True)
  hide maxine with dissolve2
  "Alright, she's not gonna be any more help."
  "I know the vents get pretty big in there... maybe I could get inside and crawl around?"
  "Hmm... and then get stuck in there with all the soap goblins."
  "Even though there's no way soap goblins are real... definitely\nno way..."
  "But... strange things have been happening around the school."
  "I'd better find someone to go into the vents for me."
  "Somebody who likes to be helpful..."
  $quest.nurse_venting.started = False
  $quest.nurse_venting.start()
  return

label quest_nurse_venting_help:
  show nurse smile with Dissolve(.5)
  nurse smile "How can I help you today, [mc]?"
  mc "..."
  nurse concerned "Are you all right, dear? You look a bit flushed."
  mc "Yes, just..."
  "Flexing."
  nurse blush "Oh, my."
  nurse blush "Now that I think about it, you have been looking healthy lately. I'm happy to see that."
  mc "I've been hitting the gym. And I've been getting more of that special cardio."
  nurse thinking "Special cardio?"
  mc "Let's just say it's less about the feet and more about the hips..."
  nurse concerned "That... motion... is quite improper, [mc]."
  if mc.owned_item("compromising_photo"):
    mc "Well, you're the expert on improper behavior."
    nurse afraid "Not so loud, please!"
    mc "Or what?"
    nurse annoyed "Or nothing..."
    mc "That's what I thought."
    nurse annoyed "I'm sorry to cut this short, but I have to get going..."
  else:
    mc "Sorry..."
    "Not sure what I was thinking there..."
    nurse smile "That's all right."
    nurse smile "I'm sorry to cut this short, but I have to get going..."
  mc "Wait, I actually need your help with something!"
  nurse neutral "What is it?"
  show nurse neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"You're going into the school's vents for me.\"":
      $quest.nurse_venting["naked"] = True
      show nurse neutral at move_to(.5)
      mc "You're going into the school's vents for me."
      mc "You're gonna crawl."
      nurse afraid "Into the vents?! What if I get stuck?"
      mc "This isn't when you ask questions. This is when you say yes."
      nurse annoyed "...yes."
      mc "Good. Now, the first thing we need is to find a vent you can get into.{space=-10}"
      nurse annoyed "There's one in my office... right behind the bed..."
      mc "Perfect! We'll use that."
      nurse annoyed "That does sound... perfect."
      mc "I'll be back when I've got everything I need to guide you."
      mc "And I wouldn't be too worried about getting stuck... I'll make sure you're nice and slippery."
    "\"I lost my inhaler...\"":
      show nurse neutral at move_to(.5)
      mc "I lost my inhaler..."
      mc "I need help getting it back."
      nurse thinking "You have an inhaler?"
      nurse thinking "My records don't have you as asthmatic."
      mc "It's... a recent development."
      nurse concerned "Should you be doing that special cardio if you're asthmatic?"
      nurse concerned "I'll make a note about this. No more cardio for you."
      "The [nurse] is always so concerned about my health."
      "I wish there was an easier way to make her do what I want...{space=-25}"
      mc "Don't worry about my asthma. It's fine."
      mc "Will you help me find my inhaler?"
      nurse smile "I'm not sure how helpful I can be, but I'll certainly try!"
      mc "So, I actually dropped it into a vent..."
      nurse neutral "A vent?"
      mc "Yeah. Really clumsy of me."
      nurse smile "That's okay, I'm oftentimes clumsy myself."
      nurse smile "Have you tried fishing it out?"
      mc "Err... yes. I've actually tried everything except climbing into the vent.{space=-10}"
      mc "Because that would be bad for my... asthma."
      nurse thinking "Yes, the vents can be a bit dusty."
      mc "So, can you help me?"
      nurse sad "Um, I don't know..."
      nurse sad "Crawling into a vent seems dangerous. I don't know if I can do that."
      mc "You know what? You're right. I'll go in myself."
      mc "Just me and my poor asthmatic lungs crawling around in the dark... All that dry air... No big deal...."
      nurse surprised "Oh, no, don't do that!"
      mc "Don't have much other choice, do I?"
      nurse neutral "Can't you just get a new one?"
      mc "Technically, yes. But it'll be a few days before the renewal of the prescription..."
      mc "I kind of need it now or I might fail gym class."
      nurse afraid "Oh, no! We can't have that..."
      mc "Yeah, that would be bad. All that hard work and special cardio for nothing."
      nurse annoyed "All right. I'll go in for you."
      mc "Thank you! You really are the best nurse in this school."
      nurse blush "Aw, that's so kind."
      nurse blush "Okay, do you know where to enter the vent?"
      mc "I know there's one in the sports wing, but that's hard to reach."
      nurse thinking "Hmm... now that I think of it, there's actually one in my office. Right behind the bed."
      mc "Perfect! We'll use that."
      mc "I'll be back when I've got everything I need to guide you."
      nurse concerned "Okay, then. Just be careful with your lungs in the meantime."
  hide nurse with dissolve2
  "Shit, come to think of it, what do I need?"
  "She's going into a dark hole. I need to be able to talk to her. And see her, maybe? Is that a thing?"
  "I've seen spelunking videos online before. Those people have radios and cameras to keep track of each other."
  "Something like that could be a good start..."
  "Who do I know who likes to mess around with gadgets?"
  $quest.nurse_venting.advance("supplies")
  return

label quest_nurse_venting_supplies_maxine:
  show maxine confident_hands_down with Dissolve(.5)
  maxine confident_hands_down "Did the soap goblins scare you off?"
  mc "A little. I found someone else to go into the vents for me."
  maxine confident_hands_down "And you want some tips for fighting the goblins?"
  maxine skeptical "You might think water is a good move, but all that does is make them frothy."
  "Frothy goblins... there's a mental image."
  mc "I need a way to talk to the person while they're in the vent."
  maxine skeptical "A decoy is a good way of luring your prey. You'd be making a smart move if you were hunting soap goblins."
  mc "I could do both. I'll look for the fish hook, and I'll let you know if I come across any soap goblins."
  mc "But you'll have to help me talk to the person. Do you have any radios?{space=-45}"
  "Deal of the century, right here — give me radios and I'll pretend your crazy theories aren't crazy."
  maxine smile "I'd be willing to consider that deal."
  maxine smile "I tried using these walkie-talkies to talk to the gaseous aliens who live{space=-35}\nin the clouds, but they must be operating on a paranormal frequency.{space=-25}"
  maxine smile "Here, you can keep them."
  $mc.add_item("walkie_talkie",2)
  mc "Walkie-talkies? You thought the cloud aliens were chatting like kids in 80s movies?"
  maxine angry "It was only a theory! Besides, if I had the right paranormal antennae,{space=-10}\nit would have worked."
  maxine neutral "Now, if you're done being critical, I'd like you to test something\nfor me."
  maxine neutral "I've outfitted my camera with an x-ray function. You can use it to see the person you're guiding."
  $school_clubroom["camera_taken"] = True
  if mc.owned_item("maxine_camera"):
    $mc.remove_item("maxine_camera",silent=True)
  $mc.add_item("maxine_camera")
  "Oh, wow! X-ray vision!"
  mc "Tell me, can it see through... anything?"
  maxine thinking "I've got it configured for walls and metal. Not fabric."
  mc "Okay, okay. Just checking..."
  mc "I'll let you know if we run into any soap goblins in there."
  maxine thinking "Their venom will clean a person's blood until it's like water. Watch out for that."
  hide maxine with Dissolve(.5)
  if quest.nurse_venting["naked"] and not mc.owned_item("body_oil"):
    return
  $quest.nurse_venting.advance("ready")
  return

label quest_nurse_venting_supplies_supply_closet:
  "Easy on the body, easy on the eyes."
  "No way does [jacklyn] use this for art."
  $quest.nurse_venting["body_oil_taken"] = True
  $mc.add_item("body_oil")
  if not mc.owned_item("walkie_talkie") and not mc.owned_item("maxine_camera"):
    return
  $quest.nurse_venting.advance("ready")
  return

label quest_nurse_venting_nurse_room_vent_interact:
  "Nah. Who knows what lurks inside the school's walls?"
  "Spiders... soap goblins... [maxine]..."
  "Yeah, no thanks."
  return

label quest_nurse_venting_ready:
  show nurse concerned with Dissolve(.5)
  nurse concerned "Is it time? Into the vent?"
  show nurse concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.owned_item('walkie_talkie') and mc.owned_item('maxine_camera') and mc.owned_item('body_oil')@|{image=items walkie_talkie}|{color=#fefefe}+{/}|{image=items maxine_camera}|{color=#fefefe}+{/}|{image=items body_oil}|\"First we have to{space=-40}\nget you ready. Strip.\"{space=-95}" if quest.nurse_venting["naked"]:
      show nurse concerned at move_to(.5)
      mc "First we have to get you ready. Strip."
      nurse thinking "Strip?"
      mc "Of course. The vents are tight. We have to remove every unnecessary{space=-30}\nlayer of clothing."
      nurse sad "If you say so..."
      nurse sad "Some days I think I spend more time undressed than I do dressed."
      window hide
      $nurse.unequip("nurse_shirt")
      pause 0.5
      show nurse concerned with Dissolve(.5)
      window auto
      nurse concerned "I don't know how I feel about that."
      mc "Don't be coy. It excites you."
      window hide
      $nurse.unequip("nurse_bra")
      pause 0.5
      show nurse blush with Dissolve(.5)
      window auto
      nurse blush "I don't know about that..."
      mc "But I do."
      window hide
      $nurse.unequip("nurse_pantys")
      pause 0.5
      show nurse thinking with Dissolve(.5)
      window auto
      nurse thinking "I'm so exposed like this..."
      nurse thinking "What if I get caught?"
      mc "Don't be ridiculous. The [guard] has eaten way too many doughnuts to fit in the vent."
      nurse annoyed "But what if I get stuck?"
      mc "See, that's why I brought this. Let's oil you up."
      nurse neutral "But won't I be all icky then?"
      mc "Do you want friction burns?"
      nurse annoyed "No..."
      mc "Then shut up and let me spread this on you."
      nurse annoyed "Yes, sir..."
      mc "..."
      show nurse afraid with Dissolve(.5):
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      nurse afraid "Ah! It's cold!"
      show nurse afraid_oiled_up with dissolve2
      $mc.remove_item("body_oil")
      "The oil spreads over her skin like liquid light."
      "My hands explore every inch of her, the curve of her hips, the fold under her tits, the warmth between her thighs..."
      "The heat of me goes into the oil, so that her skin heats up, getting slick and warm."
      "Little shivers of ecstasy radiate out from my touch. She's trembling.{space=-5}"
      "Look at her. The way her lips part, her eyes close, and her head\ntilts back."
      mc "You're getting off on this, aren't you?"
      nurse annoyed_oiled_up "I'm not sure I should say..."
      mc "I'm telling you to answer me. Are you enjoying this?"
      nurse blush_oiled_up "Y-yes."
      mc "Are you getting excited?"
      $nurse.lust+=3
      nurse blush_oiled_up "Yes..."
      mc "Haha! You really are! You're soaked."
      mc "If only I'd known, we wouldn't have needed so much oil."
      nurse sad_oiled_up "Please, you're embarrassing me..."
      mc "Whatever."
    "?mc.owned_item('walkie_talkie') and mc.owned_item('maxine_camera')@|{image=items walkie_talkie}|{color=#fefefe}+{/}|{image=items maxine_camera}|\"I've got everything\nwe need.\"" if not quest.nurse_venting["naked"]:
      show nurse concerned at move_to(.5)
      mc "I've got everything we need."
      nurse thinking "And what is it we're looking for again?"
      mc "My inhaler. But if you happen to find a fish hook in there, take it."
      nurse sad "Are you sure you have an inhaler?"
      mc "Of course I do!"
      nurse sad "Because I'd help you even if you were looking for something else."
      nurse blush "I like to be helpful."
      mc "...fine. There's no inhaler. It's the fish hook I'm really after."
      $nurse.love+=2
      nurse smile "Thank you for being honest. Honesty is very important to me."
      "Ugh, now I kind of feel bad. She's so sweet."
      "But... it's for the greater good..."
      mc "Are you sure you're okay with this?"
      nurse smile "Will it make you happy?"
      mc "It would."
      nurse smile "Then I'm okay with it."
    "\"Not yet. I need more things.\"":
      show nurse concerned at move_to(.5)
      mc "Not yet. I need more things."
      nurse concerned "Okay, I'll be waiting..."
      hide nurse with Dissolve(.5)
      return
  mc "Let's get this started. Here's your walkie-talkie."
  $mc.remove_item("walkie_talkie")
  mc "The place you're trying to get to is two floors above us, so try to go up as much as possible."
  mc "If you get stuck or lost, wait for me to come find you. I'll help you figure out where to go."
  if quest.nurse_venting["naked"]:
    nurse annoyed_oiled_up "I'm not sure my hips can fit into the vent... It's going to be tight."
  else:
    nurse annoyed "I'm not sure my hips can fit into the vent... It's going to be tight."
  mc "Tight fits are the best fits! Nothing a little push can't fix."
  if quest.nurse_venting["naked"]:
    nurse neutral_oiled_up "Okay... Please just... don't abandon me in there. It's dark."
  else:
    nurse neutral "Okay... Please just... don't abandon me in there. It's dark."
  mc "We've got our walkie-talkies. Don't worry. I won't let you down."
  if quest.nurse_venting["naked"]:
    nurse smile_oiled_up "Thank you..."
    nurse smile_oiled_up "Here I go."
  else:
    nurse smile "Thank you..."
    nurse smile "Here I go."
  window hide
  $quest.nurse_venting.advance("vents")
  pause 0.5
  if quest.nurse_venting["naked"]:
    show nurse smile_oiled_up at disappear_to_right
  else:
    show nurse smile at disappear_to_right
  pause 1.0
  window auto
  return

label quest_nurse_venting_walkie_talkie(item=None):
  if (quest.nurse_venting == "lost"
  or quest.nurse_venting == "puzzle" and game.location == quest.nurse_venting["nurse_location"] == "school_clubroom"):
    window hide None
    $set_dialog_mode("walkie_talkie","nurse")
    play ambient "audio/sound/walkie_talkie1.ogg" noloop
    queue ambient "audio/sound/walkie_talkie2.ogg"
    pause 0.5
    nurse "I have no idea where I am..."
    nurse "I might be dehydrated.\nThe air is so hot and dry."
    mc "Do you need me to\ncome find you?"
    nurse "P-please..."
    mc "Hold tight. Let me\ncheck some rooms."
  elif quest.nurse_venting == "bottle":
    "The [nurse] has finally made it to [maxine]'s office! Now I just need to drop her a water bottle before she passes out..."
    return True
  else:
    if game.location == quest.nurse_venting["nurse_location"]:
      window hide None
      $set_dialog_mode("walkie_talkie_left_aligned","nurse")
      play ambient "audio/sound/walkie_talkie1.ogg" noloop
      queue ambient "audio/sound/walkie_talkie2.ogg"
      pause 0.5
      if game.location == "school_gym":
        nurse "I forgot which way I was\ngoing... I might be a bit\ndisoriented..."
        nurse "I'm so dehydrated...\nI'm seeing double...\nNo, triple... No, single..."
        mc "You do sound dehydrated.\nLet's get you out of here."
        mc "You're in the gym. You\nhave to turn around."
        nurse "There's an intersection.\nWhich way should I go?"
        menu(side="right"):
          "\"Left.\"":
            mc "Left."
            nurse "To the left, to the left!"
            nurse "I used to sing that\nin nursing school..."
            nurse "Hey, it smells like paint!"
            $quest.nurse_venting["nurse_location"] = "school_art_class"
          "\"Straight.\"":
            mc "Straight."
            nurse "I can do straight! And I can\ndo ladies, too! Haha!"
            nurse "..."
            nurse "Sounds like someone is\nwriting on a blackboard...\nand I think I heard [mrsl]'s\nvoice."
            $quest.nurse_venting["nurse_location"] = "school_homeroom"
          "\"Right.\"":
            mc "Right."
            nurse "Right!"
            mc "Exactly."
            nurse "Haha! I think I heard a\nvending machine puke!"
            $quest.nurse_venting["nurse_location"] = "school_first_hall"
      elif game.location == "school_first_hall":
        nurse "I could drink a whole vending\nmachine right now..."
        mc "That's all sugar stuff.\nShouldn't nurses be\nmore into water?"
        nurse "I have a hopeless\nsweet tooth!"
        mc "Maybe that's something\nthat needs working on..."
        mc "Anyway, keep going."
        nurse "Which way should\nI go?"
        menu(side="right"):
          "\"Left.\"":
            mc "Left."
            nurse "Getting left behind...\nthat's what I was always\nworried about in school."
            nurse "..."
            nurse "I can hear balls up ahead!\nBouncing, bouncing! And\nrunning feet!"
            $quest.nurse_venting["nurse_location"] = "school_gym"
          "\"Straight.\"":
            mc "Straight."
            nurse "As an arrow!"
            nurse "Ow!" with vpunch
            nurse "I hit my head..."
            nurse "I think I can hear [mrsl]?\nSmells like cigarettes here."
            $quest.nurse_venting["nurse_location"] = "school_homeroom"
          "\"Right.\"":
            mc "Right."
            nurse "Right, okay."
            nurse "..."
            nurse "I'm hearing a chisel.\nNo, wait! Two chisels!"
            $quest.nurse_venting["nurse_location"] = "school_art_class"
      elif game.location == "school_art_class":
        mc "Let's hurry this up. I\ndon't want you to get\nstuck in there."
        nurse "If I get stuck they'll have\nto perform a c-section on\nthe vent! Haha!"
        window auto
        $set_dialog_mode("walkie_talkie_left_aligned_plus_textbox","nurse")
        "That's probably easier said than done."
        window hide
        $set_dialog_mode("walkie_talkie_left_aligned","nurse")
        mc "Please don't get\nstuck in there..."
        nurse "I'll do my best!"
        mc "Okay, let's pick a\ndirection for you."
        menu(side="right"):
          "\"Left.\"":
            mc "Left."
            nurse "Left, left, left,\nright, left!"
            nurse "If it wasn't so serious, I'd\nlove to be an army medic."
            nurse "They're always getting\ntold what to do."
            nurse "..."
            nurse "Squeaky shoes and\ncheers in this direction!"
            $quest.nurse_venting["nurse_location"] = "school_gym"
          "\"Straight.\"":
            mc "Straight."
            nurse "Straight? You or me?"
            window auto
            $set_dialog_mode("walkie_talkie_left_aligned_plus_textbox","nurse")
            "That dehydration is really getting to her..."
            window hide
            $set_dialog_mode("walkie_talkie_left_aligned","nurse")
            mc "Just keep going, [nurse]."
            nurse "Oh, deary me...\nI'm so thirsty."
            nurse "There's a {i}tap tap tap tap,\nclink{/} up ahead!"
            nurse "Sounds like an\nold typewriter!"
            $quest.nurse_venting["nurse_location"] = "school_clubroom"
          "\"Right.\"":
            mc "Right."
            nurse "You said it!"
            mc "I did say it."
            nurse "..."
            nurse "It's quiet up ahead... just\nthe sound of chalk scratching\na blackboard."
            $quest.nurse_venting["nurse_location"] = "school_homeroom"
      elif game.location == "school_homeroom":
        nurse "Are we close? We\nhave to be close..."
        mc "Almost there... I think."
        nurse "Okay. I trust you."
        nurse "Where to next?"
        menu(side="right"):
          "\"Left.\"":
            mc "Left."
            nurse "It feels like I've been\nin here forever..."
            nurse "Maybe I'll be old enough\nto retire when I get out."
            nurse "..."
            nurse "I hear a tap and\nsplashing water."
            $quest.nurse_venting["nurse_location"] = "school_first_hall"
          "\"Straight.\"":
            mc "Straight."
            nurse "I've got a good feeling\nabout this one!"
            mc "Me too!"
            nurse "[maxine] plays basketball\nin her office, right?"
            nurse "I'm hearing basketballs..."
            $quest.nurse_venting["nurse_location"] = "school_gym"
          "\"Right.\"":
            mc "Right."
            nurse "That's the question!\nWhere to?"
            mc "No, I'm answering\nyou. Go right."
            nurse "Oh, I thought you were\nsaying... sorry, I'm getting\na bit sleepy..."
            nurse "Okay, right."
            mc "What are you hearing now?"
            nurse "You!"
            mc "Yes, but put the walkie-talkie\ndown and then listen."
            nurse "I did! I heard you\nthrough the wall!"
            mc "..."
            nurse "..."
            mc "Did you just go\nin a circle?"
            nurse "Possibly!"
    else:
      window hide None
      $set_dialog_mode("walkie_talkie","nurse")
      play ambient "audio/sound/walkie_talkie1.ogg" noloop
      queue ambient "audio/sound/walkie_talkie2.ogg"
      pause 0.5
      if not quest.nurse_venting["keep_moving"]:
        nurse "It's dark in here. I keep catching\non the vent's edges."
        mc " But are you making progress?\nCan you see where you're going?"
        nurse "Sort of... there's openings\nthat let in some light..."
        nurse "And it's so hot. I'm sweating.\nThe dust is sticking to my sweat."
        mc "We'll get you showered\noff once you're done."
        mc "Just keep moving for now.\nHave you gone up a floor yet?"
        nurse "Yes, but I can't find another\nway up. I've passed a few\njunctions and I may have\ngotten turned around..."
        window auto
        $set_dialog_mode("walkie_talkie_plus_textbox","nurse")
        "I shouldn't be surprised. She could get lost on a one-way street."
        window hide
        $set_dialog_mode("walkie_talkie","nurse")
        mc "Just keep moving. I'm sure\nyou'll find the way eventually."
        $quest.nurse_venting["keep_moving"] = True
      elif not quest.nurse_venting["come_find"]:
        nurse "I have no idea where I am..."
        nurse "I might be dehydrated.\nThe air is so hot and dry."
        mc "Do you need me to\ncome find you?"
        nurse "P-please..."
        mc "Hold tight. Let me\ncheck some rooms."
        $quest.nurse_venting["come_find"] = True
        $quest.nurse_venting["nurse_location"] = "school_gym"
        $quest.nurse_venting.advance("lost")
      elif quest.nurse_venting["keep_moving"] and quest.nurse_venting["come_find"]:
        nurse "I have no idea where I am..."
        nurse "I might be dehydrated.\nThe air is so hot and dry."
        mc "Do you need me to\ncome find you?"
        nurse "P-please..."
        mc "Hold tight. Let me\ncheck some rooms."
  queue ambient "audio/sound/walkie_talkie3.ogg" noloop
  pause 0.5
  $set_dialog_mode("")
  hide screen walkie_talkie
  pause 0.5
  window auto
  return True

label quest_nurse_venting_walkie_talkie_home(item):
  "Hmm... no signal. Maybe I got too far from the [nurse]?"
  return True

label quest_nurse_venting_ground_floor_exit_arrow_go1:
  "I promised the [nurse] I wouldn't abandon her in the vents. Can't let her down now."
  return

label quest_nurse_venting_lost:
  window hide None
  $set_dialog_mode("walkie_talkie_left_aligned","nurse")
  play ambient "audio/sound/walkie_talkie1.ogg" noloop
  queue ambient "audio/sound/walkie_talkie2.ogg"
  pause 0.5
  nurse "[mc]? I'm getting nervous...\nand possibly delirious."
  nurse "There's strange shapes\nmoving inside the vents..."
  window auto
  $set_dialog_mode("walkie_talkie_left_aligned_plus_textbox","nurse")
  "Could those be the soap goblins...?"
  "No way. Those definitely aren't real."
  window hide
  $set_dialog_mode("walkie_talkie_left_aligned","nurse")
  mc "Hold on. I'm still checking\nrooms. I'll find you soon."
  nurse "Please hurry!"
  if mc.owned_item("compromising_photo"):
    mc "You don't tell me\nwhat to do."
    nurse "Of course. I'm sorry."
    nurse "It's the heat... I'm\nfeeling so open."
    mc "You've been in there for\nwhat? Five minutes?"
    nurse "I believe it's been a\nbit longer than that..."
    mc "Well, either way. You're\ngoing to quit whining."
    mc "Let's talk about\nsomething else."
    nurse "Okay..."
  else:
    mc "Just try to relax. I'm\nsorry it's taking so long."
    mc "Try to breathe slower and\nthink of something else."
    nurse "..."
    nurse "I like that."
    mc "Like what?"
    nurse "That you're taking care\nof me. It's nice..."
  nurse "Do you want to know why I\nlike helping people so much?"
  mc "Sure, I've been wondering\nabout that."
  $nurse.love+=1
  nurse "I have a hard time knowing\nwhat I'm supposed to do..."
  nurse "This same feeling is why\npeople get into astrology\nor self-help groups."
  nurse "So, I almost appreciate it\nwhen somebody is sick."
  nurse "When they're sick, I know\nexactly what I'm supposed\nto do."
  nurse "I care for them. Give them\nmedicine. And make sure\nthey're healthy again."
  nurse "I don't have to think."
  nurse "But there's more to my life\nthan my job, and I guess I\nfeel lost most of the time..."
  nurse "When somebody tells me\nI have to do something —\neven if I don't like it — it's\ncomfortable in a way."
  nurse "I have to do it, so I do."
  if mc.owned_item("compromising_photo"):
    mc "That's good, because I like\ntelling you what to do."
    mc "You're so kind and gentle,\nI just want to..."
    nurse "What?"
    mc "I guess I want to see how\nfar I can push you."
    nurse "Oh..."
    mc "Do you know what\nI'm talking about?"
    nurse "I think so..."
    mc "And how do you\nfeel about that?"
    nurse "Um... I trust you."
    mc "But how do you\nfeel about it?"
    nurse "It makes me nervous...\nand..."
    mc "And?"
    nurse "...excited."
  else:
    mc "I know exactly how\nyou feel."
    mc "I've also felt lost and aimless\nfor most of my life."
    nurse "But you're only a senior!\nYou still have structure\nin your life."
    mc "Right..."
    nurse "When you finish school,\na lot of it will disappear."
    window auto
    $set_dialog_mode("walkie_talkie_left_aligned_plus_textbox","nurse")
    "I'm all too aware of that..."
    window hide
    $set_dialog_mode("walkie_talkie_left_aligned","nurse")
    nurse "At the end of the day,\nyou'll only have yourself\nto turn to."
    mc "And then you fall into a pit\nof self-loathing, because\nyou can no longer blame\nanyone else..."
    nurse "That's... exactly it."
    nurse "You're wise beyond\nyour years, [mc]."
    nurse "I think that's why\nI trust you."
    nurse "..."
    nurse "Sorry, I keep\nblabbering on..."
    mc "That's okay."
    mc "Letting someone else\ntake control for a bit...\nit's very freeing in a way."
    nurse "Oh, it is!"
  nurse "I just wish... some people...\nwere a little nicer when\nthey make me do things..."
  mc "Is this about me?"
  nurse "No, no! It's...\nsomeone else."
  nurse "I like being told what to\ndo, but I like to know\nthat the person cares."
  nurse "Do you understand\nwhat I'm saying?"
  menu(side="right"):
    "\"I think so.\"":
      mc "I think so."
      mc "You want to be put in\nyour place, but only for\nthe right reasons."
      $nurse.love+=1
      nurse "You get it. Thanks\nfor listening."
      nurse "Sorry for talking\nso much..."
      mc "That's all right. We all want\nto know our place in the world."
      mc "That comes more easily to\nsome people than others."
      mc "I've known my place for\na long time, but it's only\nrecently that I'm figuring\nout how to get there."
      nurse "I'm happy to hear that!"
      mc "Me, too. But anyway, I should\nget back to looking for you."
    "\"You're talking about\n[kate], aren't you?\"":
      mc "You're talking about\n[kate], aren't you?"
      nurse "I... I shouldn't say."
      if quest.kate_desire["stakes"]:
        if quest.kate_desire["stakes"] == "release":
          mc "You don't have to worry\nabout her anymore."
          nurse "How come?"
          mc "Well, let's just say she won't\nbe bothering you anymore."
          nurse "Oh..."
          mc "You don't sound too\nhappy about that?"
          nurse "N-no... I am."
          mc "She gave you a certain\nfeeling, didn't she?"
          nurse "..."
          nurse "I suppose so."
          mc "There are others like her,\nyou know. Others who would\nactually care about you."
          nurse "I know. It's for the best."
          $nurse.love+=5
          nurse "Thank you, [mc]."
        else:
          mc "She's irrelevant\nto you now."
          nurse "She is?"
          mc "Yes. I own you now.\nCompletely."
          nurse "Oh..."
          mc "When I tell you to do something,\nalways remember that I have all\nthe dirt on you that [kate] did."
          nurse "..."
          mc "..."
          mc "[nurse]?"
          nurse "Sorry. I'm just a bit\ndizzy, that's all..."
          mc "Okay, let's get\na move on."
      else:
        mc "I know that you are."
        mc "She can be quite scary."
        nurse "She definitely can..."
        mc "Let's get a move on and\nwe'll figure out [kate] later."
        nurse "O-okay."
    "\"Maybe. But I don't care.\"" if mc.owned_item("compromising_photo"):
      mc "Maybe. But I don't care."
      nurse "You don't?"
      mc "Not even a little bit."
      mc "We've all got our reasons\nfor being the way we are,\nand most of them aren't\nthat interesting."
      mc "You're no exception."
      mc "All I want to hear from\nyou is, \"Yes, sir.\""
      $nurse.lust+=2
      nurse "Yes, sir..."
      nurse "Thank you for being\nhonest. I'll do better."
      mc "Good girl. We'll talk\nlater after I find you."
  queue ambient "audio/sound/walkie_talkie3.ogg" noloop
  pause 0.5
  $set_dialog_mode("")
  hide screen walkie_talkie
  pause 1.0
  show jo smile at appear_from_right
  pause 0.5
  window auto
  jo smile "Hey, honey! What are you up to?"
  "Crap. How does [jo] manage to show up at all the worst times?"
  "I can't let her find out I've got the [nurse] up in the vents."
  "That's got to be breaking every school health code..."
  mc "Oh, um... just killing time between classes. Going for a stroll."
  jo concerned "Killing time in the sports wing? Is that right? I thought you hated\nit here."
  mc "Yeah, but you know, lately I've been trying to be more open to things,{space=-25}\nso here I am..."
  jo skeptical "More open to things..."
  jo skeptical "Does that have to do with the walkie-talkie? Who are you being open{space=-10}\nto things with, hm?"
  mc "Oh, this? No one, no one. I'm just playing pretend, you know?"
  jo confident "Aren't you a little old to be playing pretend?"
  "I really am. But I'm committed now."
  mc "No way! I'm being a pilot. It's a lot of fun."
  play ambient "audio/sound/walkie_talkie1.ogg" noloop
  queue ambient "audio/sound/walkie_talkie2.ogg"
  mc "Kch! This is Red-Niner, calling Blue-Seven! Come in, Blue-Seven!"
  queue ambient "audio/sound/walkie_talkie3.ogg" noloop
  "I hate myself right now..."
  mc "See? Pretty cool."
  jo laughing "If you say so!"
  jo excited "I'll admit, I miss seeing you have fun like this..."
  jo excited "Who cares if other people make fun of you? You be the best you that you can be, okay?"
  jo laughing "Just try to keep it down. This is still a school, you know? It's not an aircraft carrier!"
  mc "Absolutely, [jo]. Quiet as a mouse."
  jo laughing "A mouse in an airplane! Oh, you're adorable!"
  window hide
  show jo laughing at disappear_to_right
  pause 0.5
  window auto
  "Alright, I hated that conversation. Let's get this vent adventure over and done with..."
  "Where the hell is the [nurse] at?"
  $quest.nurse_venting["play_pretend"] = True
  return

label quest_nurse_venting_maxine_camera(item):
  $background = renpy.random.randint(0,1)
  if game.location == quest.nurse_venting["nurse_location"]:
    if (quest.nurse_venting == "lost"
    or quest.nurse_venting in ("puzzle","bottle") and game.location == quest.nurse_venting["nurse_location"] == "school_clubroom"):
      show nurse vent_waiting video
    else:
      show nurse vent_crawling video
    show expression "lindsey avatar events spidereggs cameraoverlay" as camera
    with Dissolve(.5)
    if game.location == "school_clubroom":
      "The [nurse] has finally made it to [maxine]'s office! Now I just need to drop her a water bottle before she passes out..."
      $quest.nurse_venting.advance("bottle")
      window hide
      hide nurse
      hide camera
      with Dissolve(.5)
      window auto
      return True
    pause 0.5
    play ambient "audio/sound/walkie_talkie1.ogg" noloop
    queue ambient "audio/sound/walkie_talkie2.ogg"
    if game.location == "school_gym":
      if quest.nurse_venting == "lost":
        mc "Are you okay?"
        nurse vent_waiting video "I don't know..."
        nurse vent_waiting video "I forgot which way I was going... I might be a bit disoriented..."
        nurse vent_waiting video "I'm so dehydrated... I'm seeing double... No, triple... No, single..."
        mc "You do look dehydrated. Let's get you out of here."
        mc "You're in the gym. You have to turn around."
        nurse vent_waiting video "In nursing school, they taught us that one of the symptoms of dehydration... is thirst!"
        nurse vent_waiting video "Did you know that?"
        mc "I did not."
        nurse vent_waiting video "Well, now you do!"
        mc "Great. Listen, you need to turn around. You need to find a way to go up in the vents."
        nurse vent_waiting video "At nursing school they also called me the head nurse! Do you\nknow why?"
        "She's really getting delirious. We have to hurry this up."
        mc "I'm guessing because you give good head."
        nurse vent_waiting video "No, not that! It's because I give good head!"
        nurse vent_waiting video "Wait, is that what you said?"
        "It's never easy. Why is it never easy?"
        mc "Have you turned around?"
        window hide
        show black onlayer screens zorder 100 with Dissolve(.5)
        pause 0.5
        hide nurse
        $renpy.music.stop(channel=u'sprite', fadeout=0)
        show nurse vent_crawling video behind camera
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        window auto
        nurse vent_crawling video "Y-yes."
        mc "Start crawling."
        nurse vent_crawling video "Hehe! Yes, sir!"
        "Okay, now I just need to—"
        nurse vent_crawling video "[mc], I need help..."
        "Ugh, what now?"
        mc "What's wrong?"
      else:
        nurse vent_crawling video "I forgot which way I was going... I might be a bit disoriented..."
        nurse vent_crawling video "I'm so dehydrated... I'm seeing double... No, triple... No, single..."
        mc "You do look dehydrated. Let's get you out of here."
        mc "You're in the gym. You have to turn around."
      nurse vent_crawling video "There's an intersection. Which way should I go?"
      menu(side="middle"):
        extend ""
        "\"Left.\"":
          mc "Left."
          nurse vent_crawling video "To the left, to the left!"
          nurse vent_crawling video "I used to sing that in nursing school..."
          nurse vent_crawling video "Hey, it smells like paint!"
          $quest.nurse_venting["nurse_location"] = "school_art_class"
        "\"Straight.\"":
          mc "Straight."
          nurse vent_crawling video "I can do straight! And I can do ladies, too! Haha!"
          nurse vent_crawling video "..."
          nurse vent_crawling video "Sounds like someone is writing on a blackboard... and I think I heard [mrsl]'s voice."
          $quest.nurse_venting["nurse_location"] = "school_homeroom"
        "\"Right.\"":
          mc "Right."
          nurse vent_crawling video "Right!"
          mc "Exactly."
          nurse vent_crawling video "Haha! I think I heard a vending machine puke!"
          $quest.nurse_venting["nurse_location"] = "school_first_hall"
      if quest.nurse_venting == "lost":
        nurse vent_crawling video "What am I looking for again?"
        mc "A way to go up a floor, and then a fishing hook."
        nurse vent_crawling video "Haha! Am I looking for a hook or a hooker? Haha!"
        mc "A hook!"
        mc "For now, just keep crawling, go up a floor, and find the hook, okay?"
        mc "I'm gonna drop a water bottle where the hook is. Drink that when you get there."
        $quest.nurse_venting.advance("puzzle")
    elif game.location == "school_first_hall":
      nurse vent_crawling video "I could drink a whole vending machine right now..."
      mc "That's all sugar stuff. Shouldn't nurses be more into water?"
      nurse vent_crawling video "I have a hopeless sweet tooth!"
      mc "Maybe that's something that needs working on..."
      mc "Anyway, keep going."
      nurse vent_crawling video "Which way should I go?"
      menu(side="middle"):
        extend ""
        "\"Left.\"":
          mc "Left."
          nurse vent_crawling video "Getting left behind... that's what I was always worried about in school."
          nurse vent_crawling video "..."
          nurse vent_crawling video "I can hear balls up ahead! Bouncing, bouncing! And running feet!"
          $quest.nurse_venting["nurse_location"] = "school_gym"
        "\"Straight.\"":
          mc "Straight."
          nurse vent_crawling video "As an arrow!"
          nurse vent_crawling video "Ow!" with vpunch
          nurse vent_crawling video "I hit my head..."
          nurse vent_crawling video "I think I can hear [mrsl]? Smells like cigarettes here."
          $quest.nurse_venting["nurse_location"] = "school_homeroom"
        "\"Right.\"":
          mc "Right."
          nurse vent_crawling video "Right, okay."
          nurse vent_crawling video "..."
          nurse vent_crawling video "I'm hearing a chisel. No, wait! Two chisels!"
          $quest.nurse_venting["nurse_location"] = "school_art_class"
    elif game.location == "school_art_class":
      mc "Let's hurry this up. I don't want you to get stuck in there."
      nurse vent_crawling video "If I get stuck they'll have to perform a c-section on the vent! Haha!"
      "That's probably easier said than done."
      mc "Please don't get stuck in there..."
      nurse vent_crawling video "I'll do my best!"
      mc "Okay, let's pick a direction for you."
      menu(side="middle"):
        extend ""
        "\"Left.\"":
          mc "Left."
          nurse vent_crawling video "Left, left, left, right, left!"
          nurse vent_crawling video "If it wasn't so serious, I'd love to be an army medic."
          nurse vent_crawling video "They're always getting told what to do."
          nurse vent_crawling video "..."
          nurse vent_crawling video "Squeaky shoes and cheers in this direction!"
          $quest.nurse_venting["nurse_location"] = "school_gym"
        "\"Straight.\"":
          mc "Straight."
          nurse vent_crawling video "Straight? You or me?"
          "That dehydration is really getting to her..."
          mc "Just keep going, [nurse]."
          nurse vent_crawling video "Oh, deary me... I'm so thirsty."
          nurse vent_crawling video "There's a {i}tap tap tap tap, clink{/} up ahead!"
          nurse vent_crawling video "Sounds like an old typewriter!"
          $quest.nurse_venting["nurse_location"] = "school_clubroom"
        "\"Right.\"":
          mc "Right."
          nurse vent_crawling video "You said it!"
          mc "I did say it."
          nurse vent_crawling video "..."
          nurse vent_crawling video "It's quiet up ahead... just the sound of chalk scratching a blackboard."
          $quest.nurse_venting["nurse_location"] = "school_homeroom"
    elif game.location == "school_homeroom":
      nurse vent_crawling video "Are we close? We have to be close..."
      mc "Almost there... I think."
      nurse vent_crawling video "Okay. I trust you."
      nurse vent_crawling video "Where to next?"
      menu(side="middle"):
        extend ""
        "\"Left.\"":
          mc "Left."
          nurse vent_crawling video "It feels like I've been in here forever..."
          nurse vent_crawling video "Maybe I'll be old enough to retire when I get out."
          nurse vent_crawling video "..."
          nurse vent_crawling video "I hear a tap and splashing water."
          $quest.nurse_venting["nurse_location"] = "school_first_hall"
        "\"Straight.\"":
          mc "Straight."
          nurse vent_crawling video "I've got a good feeling about this one!"
          mc "Me too!"
          nurse vent_crawling video "[maxine] plays basketball in her office, right?"
          nurse vent_crawling video "I'm hearing basketballs..."
          $quest.nurse_venting["nurse_location"] = "school_gym"
        "\"Right.\"":
          mc "Right."
          nurse vent_crawling video "That's the question! Where to?"
          mc "No, I'm answering you. Go right."
          nurse vent_crawling video "Oh, I thought you were saying... sorry, I'm getting a bit sleepy..."
          nurse vent_crawling video "Okay, right."
          mc "What are you hearing now?"
          nurse vent_crawling video "You!"
          mc "Yes, but put the walkie-talkie down and then listen."
          nurse vent_crawling video "I did! I heard you through the wall!"
          mc "..."
          nurse vent_crawling video "..."
          mc "Did you just go in a circle?"
          nurse vent_crawling video "Possibly!"
    window hide
    queue ambient "audio/sound/walkie_talkie3.ogg" noloop
    pause 0.5
    hide nurse
    hide camera
    with Dissolve(.5)
    window auto
  else:
    if background:
      show expression "nurse avatar events vent background1" as background
    else:
      show expression "nurse avatar events vent background2" as background
    show expression "lindsey avatar events spidereggs cameraoverlay" as camera
    with Dissolve(.5)
    pause
    hide background
    hide camera
    with Dissolve(.5)
  return True

label quest_nurse_venting_maxine_camera_home(item):
  "Huh? The x-ray function on this thing only seems to work on the school vents..."
  "How did [maxine] even do this?"
  return True

label quest_nurse_venting_entrance_path_go:
  "I promised the [nurse] I wouldn't abandon her in the vents. Can't let her down now."
  return

label quest_nurse_venting_entrance_right_path_go:
  "I promised the [nurse] I wouldn't abandon her in the vents. Can't let her down now."
  return

label quest_nurse_venting_entrance_bus_stop_go:
  "I promised the [nurse] I wouldn't abandon her in the vents. Can't let her down now."
  return

label quest_nurse_venting_kitchen_stairs_go:
  "I promised the [nurse] I wouldn't abandon her in the vents. Can't let her down now."
  return

label quest_nurse_venting_clubroom_vent_interact:
  "As if this room wasn't weird enough..."
  "What kind of architect puts a vent in the floor?"
  return

label quest_nurse_venting_clubroom_vent_use_item(item):
  if item in ("banana_milk","spray_banana_milk","pepelepsi","spray_pepelepsi","strawberry_juice","spray_strawberry_juice","salted_cola","spray_salted_cola","seven_hp","spray_seven_hp","spray_urine","water_bottle","spray_water"):
    play sound "hit2"
    show layer master:
      block:
        linear 0.05 yoffset 15
        linear 0.05 yoffset -15
        repeat 4
      linear 0.05 yoffset 0
    $quest.nurse_venting["drink"] = item.id
    $mc.remove_item(item)
    pause 0.25
    window hide None
    $set_dialog_mode("walkie_talkie","nurse")
    play ambient "audio/sound/walkie_talkie1.ogg" noloop
    queue ambient "audio/sound/walkie_talkie2.ogg"
    pause 0.5
    nurse "Oww! You dropped a\nbottle on my head!"
    nurse "That's a funny coincidence!\nI'm so thirsty!"
    mc "Drink the bottle,\nyou'll feel better."
    nurse "Oh, my! You could be a\nnurse, you know that?"
    nurse "..."
    nurse "{i}*Gurgle*{/}"
    nurse "Ah, I chugged it... and I\nstabbed my hand on\nsomething sharp..."
    mc "Is it a fishing hook?"
    nurse "It might be? It's\npretty dark..."
    nurse "I'm either feeling better\nnow... or much worse."
    mc "You feel better."
    nurse "You know what?\nI think I do!"
    mc "Can you figure out how to\nget back to your office?"
    nurse "Now that my head is clear...\nI think I can. It's down, left,\nright, right again, then left—"
    mc "You got this! I'll\nmeet you there."
    queue ambient "audio/sound/walkie_talkie3.ogg" noloop
    pause 0.5
    $set_dialog_mode("")
    hide screen walkie_talkie
    pause 0.5
    window auto
    $mc.add_item("walkie_talkie", silent=True)
    if quest.nurse_venting["naked"]:
      $quest.nurse_venting.advance("stuck")
    else:
      $quest.nurse_venting.advance("not_stuck")
  else:
    "The [nurse] is so out of her mind, she might actually try to drink my [item.title_lower]..."
    $quest.nurse_venting.failed_item("clubroom_vent",item)
  return

label quest_nurse_venting_clubroom_window_go:
  "Assuming the [nurse] makes it back safe and sound, I'm not gonna need these gadgets anymore."
  "Might as well leave them here before [maxine] pesters me about the soap goblins again..."
  window hide
  $mc.remove_item("walkie_talkie",2)
  if not quest.maxine_eggs == "maxine":
    $school_clubroom["camera_taken"] = False
    $mc.remove_item("maxine_camera")
  pause 0.25
  jump goto_school_homeroom

label quest_nurse_venting_ground_floor_exit_arrow_go2:
  "As enticing as escaping this hellhole is, I promised to meet the [nurse] in her office."
  return

label quest_nurse_venting_stuck:
  $nurse.unequip("nurse_shirt")
  $nurse.unequip("nurse_bra")
  $nurse.unequip("nurse_pantys")
  nurse "[mc]? Is that you?"
  "Woah, her ass looks extra fine coming out of there!"
  "Those thick juicy thighs wobbling like jelly..."
  nurse "Oh, my..."
  mc "What's wrong?"
  nurse "I can't move..."
  nurse "Oh, no..."
  mc "You're stuck?"
  nurse "Y-yes..."
  nurse "This is entirely unprecedented!"
  mc "Let me see if I can help get you loose."
  nurse "Very well, but I have felt bloated lately and there's a couple of extra pounds I've meant to get rid off..."
  mc "Nonsense! You look great."
  "My fingers press into her thickness. She's so soft."
  mc "And you feel great too."
  nurse "Oh, but... [mc]..."
  mc "What?"
  nurse "That's a bit inappropriate..."
  mc "So is crawling naked through the vents."
  nurse "I suppose..."
  "If I grab her by the hips, I could perhaps work her free."
  "But seeing her like this... stuck... presenting... helpless... I can't help but feel her up."
  nurse "I... I think if you help me wiggle, it might help..."
  mc "You bet. Let's get you wiggling."
  menu(side="middle"):
    extend ""
    "Pull her out":
      mc "All right, here we go."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      nurse "Oh!" with vpunch
      $set_dialog_mode("")
      show nurse blush
      show black onlayer screens zorder 100
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      nurse blush "You did it!"
      nurse blush "Thank you. That was quite the ordeal."
      mc "Don't mention it!"
      mc "Do you have the hook?"
      nurse blush "Here you go."
      $mc.add_item("fishing_hook")
      $mc.add_item("fishing_line")
      if quest.nurse_venting["drink"].startswith("spray"):
        $mc.add_item("spray_empty_bottle")
      else:
        $mc.add_item("empty_bottle")
      mc "Thank you. You should probably get some rest now."
      nurse concerned "I suppose I should."
      nurse concerned "This was enough excitement to last the rest of the year..."
    "?nurse.lust>=8@[nurse.lust]/8|{image=nurse contact_icon}|{image=stats lust}|Find an alternative plan":
      "My fingers dig deeper into her hips."
      "Her ass shakes and wiggles, and something stirs inside me."
      "Perhaps helping her can wait a bit..."
      nurse "Oh... that's... that..."
      mc "I have a different plan."
      nurse "What do you mean?"
      "She's built like a fertility goddess. Those big thighs leading up to that full ass. That winking sphincter."
      mc "You're incredibly stuck, but I have an idea."
      window hide
      show black onlayer screens zorder 100
      show expression "images/characters/nurse/avatar/events/vent/hotdogging_firstframe.webp" as movie_workaround
      with Dissolve(.5)
      pause 0.25
      play sound "<from 0.2>bedsheet"
      pause 0.75
      hide black onlayer screens with Dissolve(.5)
      window auto
      nurse "Oh!"
      "She knows where this is going. Her knees are pulling apart. She's opening herself to me."
      nurse "But what if someone comes in?"
      mc "They'll see me fucking you."
      nurse "I could get in trouble!"
      mc "Oh, you're already in trouble."
      nurse "I suppose I am..."
      mc "That's right."
      "My fingers run up and down her inner thighs. This brings my face close to her pussy and the heat coming off it."
      "A sheen of wetness is already spreading along the line of her lips."
      "I give her a lick, top to bottom, and her knees give out. She tastes a little sour, a little sweet."
      "The flat of my tongue works her lips. With the tip, I give her clit the occasional flick."
      "She's trembling, arching her back upwards. She can't help herself."
      nurse "P-please!"
      mc "Please what?"
      nurse "Please, give it to me... sir."
      mc "Oh, I do like that!"
      window hide
      show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/vent/hotdogging.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/vent/hotdogging_firstframe.webp") as movie_workaround
      pause 1.0
      window auto
      "I've always wanted to try a pornstar position..."
      "I play the head of my dick over her pussy."
      "I have to make her mine. The power over her is exhilarating."
      "Before I press in, I savor how good it will feel."
      "That pressure, that warmth, that long slide..."
      window hide
      show black onlayer screens zorder 100
      show expression "images/characters/nurse/avatar/events/vent/insertion_firstframe.webp" as movie_workaround
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/vent/insertion.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/vent/insertion_firstframe.webp", loop=False, image="images/characters/nurse/avatar/events/vent/insertion_lastframe.webp") as movie_workaround
      window auto
      "Carefully, I ease the tip in. Savoring her soft heat."
      nurse "Oh!"
      "The touch sends a wave of electricity through me."
      window hide
      show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/vent/penetration.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/vent/insertion_lastframe.webp") as movie_workaround
      pause 1.0
      window auto
      "Slow, steady, heavy thrusts. I've got her ass moving like the surface of the ocean."
      "It's mesmerizing."
      "Time to put some marks on her."
      window hide
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      pause 0.5
      window auto
      "The first spank is a doozy, right where the fat of her ass folds over the fat of her thigh."
      nurse "Ow!"
      mc "Shut up. You like it."
      nurse "I do!"
      "I increase the pace."
      window hide
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      pause 0.5
      window auto
      nurse "Harder!"
      mc "Beg for it!"
      nurse "Please, harder!"
      window hide
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      pause 0.5
      window auto
      "I put my back into it, rising off the floor to give her my attention."
      "As I get closer, I increase the pace still."
      nurse "Yes! Yes! Please!"
      window hide
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      pause 0.5
      window auto
      "After years and years of masturbation and self-edging, I've mastered my own pleasure..."
      "Holding it in. Holding it in longer and longer."
      nurse "Oh, my lord!"
      window hide
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      pause 0.5
      window auto
      "Fucking her deep and hard."
      "Well... as hard as this position allows me..."
      "My stomach muscles flex with every thrust."
      "Her ass jiggles. She moans. An orgasm rips through her."
      "And that's not something my masturbation practice has prepared me for."
      "Tight, pulsing, pulling me in..."
      "I have to pull out!"
      show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/vent/cum.webm", side_mask=False, loop=False, image="images/characters/nurse/avatar/events/vent/cum_lastframe.webp") as movie_workaround
      mc "F-fuck!"
      "Ropes of cum stream out across her ass cheeks."
      nurse "Oooh! That's burning hot!"
      "I've never felt so good finishing myself off. It's like my soul is leaving my body."
      "I brace myself against the floor and pour out the rest of my jizz onto her."
      "Some of it runs down the slope of her ass cheeks and pools in the small of her back."
      nurse "Oh, dear..."
      mc "Oh, dear, indeed."
      $unlock_replay("nurse_wiggle")
      $quest.nurse_venting["sex"] = True
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 1.0
      hide movie_workaround
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "And now what, I help her get free?"
      "Maybe I won't."
      "Maybe I'll walk away and lock the door behind me..."
      "I wouldn't mind having my own personal fuck toy stuck in this vent."
      "Just need to figure out how to bring her food and I'm—"
      nurse "[mc]? It worked!"
      mc "What did?"
      nurse "Your plan! I'm free!"
      show nurse blush with dissolve2
      "No freaking way..."
      mc "Err... of course it did! Told you so!"
      nurse blush "So you did."
      nurse blush "I should trust you from now on."
      mc "You definitely should."
      mc "Do you have the hook?"
      nurse blush "Here you go."
      $mc.add_item("fishing_hook")
      $mc.add_item("fishing_line")
      if quest.nurse_venting["drink"].startswith("spray"):
        $mc.add_item("spray_empty_bottle")
      else:
        $mc.add_item("empty_bottle")
      nurse concerned "I don't think I'll be able to sit for a week..."
      nurse concerned "Time to break out the polysporin."
  window hide
  show nurse concerned at disappear_to_right
  $quest.nurse_venting.finish()
  window auto
  return

label quest_nurse_venting_not_stuck:
  show nurse smile with Dissolve(.5)
  mc "You've made it!"
  mc "Do you have the hook?"
  nurse smile "Here you go."
  $mc.add_item("fishing_hook")
  $mc.add_item("fishing_line")
  if quest.nurse_venting["drink"].startswith("spray"):
    $mc.add_item("spray_empty_bottle")
  else:
    $mc.add_item("empty_bottle")
  mc "You should probably get some rest now."
  mc "Thanks for the help!"
  nurse smile "Anytime!"
  hide nurse with dissolve2
  "The [nurse] is so sweet. I wish we could hang out more."
  "I don't care that she's a bit older, she's so... soft and agreeable."
  "So squeezable!"
  "Maybe I could ask her out some time?"
  $quest.nurse_venting.finish()
  return
