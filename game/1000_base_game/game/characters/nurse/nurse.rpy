init python:

  class Character_nurse(BaseChar):
    notify_level_changed=True

    default_name="Nurse"

    name_color="#00751a"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="nurse contact_icon"

    default_outfit_slots=["hat","pendant","outfit","shirt","bra","pants","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("nurse_sun_hat")
      self.add_item("nurse_hat")
      self.add_item("nurse_pendant")
      self.add_item("nurse_hiking_gear")
      self.add_item("nurse_outfit")
      self.add_item("nurse_revealing_dress")
      self.add_item("nurse_sophisticated_dress")
      self.add_item("nurse_nice_dress")
      self.add_item("nurse_dress")
      self.add_item("nurse_shirt")
      self.add_item("nurse_black_bra")
      self.add_item("nurse_green_bra")
      self.add_item("nurse_bra")
      self.add_item("nurse_black_panties")
      self.add_item("nurse_green_panties")
      self.add_item("nurse_pantys")
      self.add_item("nurse_masturbating")
      self.equip("nurse_outfit")
      self.equip("nurse_bra")
      self.equip("nurse_shirt")
      self.equip("nurse_pantys")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

      #######alt locations
      if quest.nurse_strike_nude_office in ("maxine_visitor","next_visitor") and nurse["strike_book_nude_office_today"]:
        self.alternative_locations["school_nurse_room"] = "sitting"

      if quest.jo_potted == "plowcow":
        self.alternative_locations["school_forest_glade"] = "sitting"

      if quest.jo_day == "nurse":
        self.alternative_locations["school_forest_glade"] = "sitting"

      if quest.kate_trick == "nurse":
        self.alternative_locations["school_nurse_room"] = "sitting"
      elif quest.kate_trick == "workout":
        self.alternative_locations["school_gym"] = "standing"

      if quest.kate_wicked == "costume" and not quest.kate_wicked["own_costume"]:
        self.alternative_locations["school_ground_floor_west"] = "standing"
        if quest.kate_search_for_nurse.in_progress:
          return

      if (quest.isabelle_dethroning == "dinner" and game.hour == 19 and not quest.isabelle_dethroning["kitchen_shenanigans"]) or quest.isabelle_dethroning in ("trap","revenge_done"):
        self.alternative_locations["school_cafeteria"] = "standing"

      if quest.kate_moment == "nurse_room":
        self.alternative_locations["school_nurse_room"] = "sitting"

      if ((((quest.kate_desire.actually_finished and quest.kate_desire["stakes"] == "release" and quest.kate_stepping.finished)
      or (quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished))
      and quest.fall_in_newfall.finished and not quest.nurse_aid.started)
      or nurse["on_roof_this_scene"]):
        self.alternative_locations["school_roof"] = "standing"
      elif quest.nurse_aid in ("fishing","bait"):
        self.alternative_locations["school_forest_glade"] = "sitting"
      elif quest.nurse_aid == "hiking":
        self.alternative_locations["school_park"] = "standing"
      elif quest.nurse_aid == "shopping":
        self.alternative_locations["dress_shop"] = "standing"
      elif (quest.nurse_aid == "someone_else" and quest.nurse_aid["jacklyn_on_board"]) or quest.nurse_aid == "relationship":
        self.alternative_locations["school_nurse_room"] = "sitting"
      #######alt locations

      #######forced locations
      if (nurse["at_none_this_scene"] or nurse["at_none_now"] or nurse["at_none_today"]
      or quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm")
      or quest.mrsl_table in ("morning","hide")
      or quest.kate_fate in ("peek","hunt","run","hunt2","deadend","rescue")
      or quest.kate_search_for_nurse.in_progress
      or quest.flora_bonsai.in_progress
      or quest.lindsey_nurse.in_progress
      or (quest.lindsey_piano == "nurse" and quest.lindsey_piano["follow_me"])
      or quest.nurse_photogenic.in_progress
      or ("stuck" > quest.nurse_venting  > "ready")
      or quest.jo_washed.in_progress
      or quest.fall_in_newfall.in_progress
      or quest.mrsl_bot == "dream"):
        self.location = None
        self.activity = None
        self.alternative_locations = {}
        return

      if quest.lindsey_wrong == "nurse":
        self.location = "school_nurse_room"
        self.activity = "sitting"
        return

      if quest.kate_desire == "bathroomhelp" and quest.kate_desire["balls_busted"]:
        self.location = "school_nurse_room"
        self.activity = "sitting"
        return

      if quest.nurse_venting in ("supplies","ready","not_stuck"):
        self.location = "school_nurse_room"
        self.activity = "sitting"
        return
      elif quest.nurse_venting == "stuck":
        self.location = "school_nurse_room"
        self.activity = "stuck"
        return

      if quest.isabelle_hurricane in ("forest","call_isabelle","call_maxine") and game.hour in (10,11,13,14,15):
        self.location = "school_nurse_room"
        self.activity = "sitting"
        return

      if quest.jo_day == "nurse":
        self.location = "school_forest_glade"
        self.activity = "sitting"
        return

      if quest.maya_spell == "frogs" and game.hour in (10,11,13,14,15):
        self.location = "school_cafeteria"
        self.activity = "standing"
        return
      elif quest.maya_spell == "maya" and game.hour == 12:
        self.location = "school_forest_glade"
        self.activity = "sitting"
        return

      if quest.nurse_aid == "aftermath" and game.day > quest.nurse_aid["date"] and game.hour in (7,8,9,16,17,18):
        self.location = "school_cafeteria"
        self.activity = "standing"
        self.alternative_locations = {}
        return
      #######forced locations

      #######schedule
      if game.dow in (0,1,2,3,4,5,6,7):

        if nurse["plowcow_now"]:
          self.location = "school_forest_glade"
          self.activity = "sitting"
          return

        if game.hour in (7,8,9):
          self.location="school_nurse_room"
          self.activity="sitting"
          return

        elif game.hour == 12:
          self.location="school_cafeteria"
          self.activity="standing"
          return

        elif game.hour in (10,11,13,14,15):
          self.location="school_forest_glade"
          self.activity="sitting"
          return

        elif game.hour in (16,17,18):
          if game.season == 1 and school_art_class["nude_model"] == self:
            self.location="school_art_class"
            self.activity="pose"
            return
          else:
            self.location="school_nurse_room"
            self.activity="sitting"
            return

        #else:
        #  self.location=None
        #  self.activity=None
        #  self.alternative_locations={}

    def call_label(self):
      if quest.isabelle_buried == "callforhelp":
        return "quest_isabelle_buried_callforhelp_nurse"
      else:
        return None

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("nurse avatar "+state,True):
          return "nurse avatar "+state
      rv=[]

      if state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body1",61,153))
        if "_oiled_up" in state:
          rv.append(("nurse avatar b1oil",116,231))
        rv.append(("nurse avatar face_afraid",169,265))
        if "_green_panties" in state:
          rv.append(("nurse avatar b1green_panties",66,757))
        elif "_black_panties" in state:
          rv.append(("nurse avatar b1black_panties",67,771))
        elif "_pantys" in state:
          rv.append(("nurse avatar b1panty",84,688))
        if "_green_bra" in state:
          rv.append(("nurse avatar b1green_bra",141,399))
        elif "_black_bra" in state:
          rv.append(("nurse avatar b1black_bra",154,534))
        elif "_bra" in state:
          rv.append(("nurse avatar b1bra",144,411))
        if "_masturbating" in state:
          rv.append(("nurse avatar b1arm1_n_vibrator",102,400))
        elif "_phone" in state:
          rv.append(("nurse avatar b1arm2_n_phone",101,301))
        else:
          rv.append(("nurse avatar b1arm1_n",101,400))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b1outfit",57,399))
          if "_phone" in state:
            rv.append(("nurse avatar b1arm2_c_phone",100,405))
          else:
            rv.append(("nurse avatar b1arm1_c",102,400))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b1openshirt",46,397))
          rv.append(("nurse avatar b1arm1_c_vibrator",102,400))
        elif "_nice_dress" in state:
          rv.append(("nurse avatar b1nice_dress",61,399))
          rv.append(("nurse avatar b1arm1_nice_dress",100,403))
        elif "_dress" in state:
          rv.append(("nurse avatar b1dress",41,399))
          rv.append(("nurse avatar b1id",201,549))
          rv.append(("nurse avatar b1arm1_dress",98,399))
        if "_pendant" in state:
          rv.append(("nurse avatar b1pendant",214,421))
        if "_hat" in state:
          rv.append(("nurse avatar b1hat",158,125))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body1",61,153))
        if "_oiled_up" in state:
          rv.append(("nurse avatar b1oil",116,231))
        rv.append(("nurse avatar face_annoyed",174,272))
        if "_green_panties" in state:
          rv.append(("nurse avatar b1green_panties",66,757))
        elif "_pantys" in state:
          rv.append(("nurse avatar b1panty",84,688))
        if "_green_bra" in state:
          rv.append(("nurse avatar b1green_bra",141,399))
        elif "_bra" in state:
          rv.append(("nurse avatar b1bra",144,411))
        if "_masturbating" in state:
          rv.append(("nurse avatar b1arm1_n_vibrator",102,400))
        else:
          rv.append(("nurse avatar b1arm1_n",101,400))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b1outfit",57,399))
          rv.append(("nurse avatar b1arm1_c",102,400))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b1openshirt",46,397))
          rv.append(("nurse avatar b1arm1_c_vibrator",102,400))
        elif "_dress" in state:
          rv.append(("nurse avatar b1dress",41,399))
          rv.append(("nurse avatar b1id",201,549))
          rv.append(("nurse avatar b1arm1_dress",98,399))
        if "_hat" in state:
          rv.append(("nurse avatar b1hat",158,125))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body1",61,153))
        if "_oiled_up" in state:
          rv.append(("nurse avatar b1oil",116,231))
        rv.append(("nurse avatar face_neutral",173,266))
        if "_green_panties" in state:
          rv.append(("nurse avatar b1green_panties",66,757))
        elif "_pantys" in state:
          rv.append(("nurse avatar b1panty",84,688))
        if "_green_bra" in state:
          rv.append(("nurse avatar b1green_bra",141,399))
        elif "_bra" in state:
          rv.append(("nurse avatar b1bra",144,411))
        if "_masturbating" in state:
          rv.append(("nurse avatar b1arm2_n_vibrator",101,440))
        elif "_phone" in state:
          rv.append(("nurse avatar b1arm2_n_phone",101,301))
        else:
          rv.append(("nurse avatar b1arm2_n",101,440))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b1outfit",57,399))
          if "_phone" in state:
            rv.append(("nurse avatar b1arm2_c_phone",100,405))
          else:
            rv.append(("nurse avatar b1arm2_c",100,405))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b1openshirt",46,397))
          rv.append(("nurse avatar b1arm2_c_vibrator",90,413))
        elif "_dress" in state:
          rv.append(("nurse avatar b1dress",41,399))
          rv.append(("nurse avatar b1id",201,549))
          rv.append(("nurse avatar b1arm2_dress",96,398))
        if "_hat" in state:
          rv.append(("nurse avatar b1hat",158,125))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body1",61,153))
        if "_oiled_up" in state:
          rv.append(("nurse avatar b1oil",116,231))
        if "_right" in state:
          rv.append(("nurse avatar face_smile_right",172,270))
        else:
          rv.append(("nurse avatar face_smile",172,270))
        if "_green_panties" in state:
          rv.append(("nurse avatar b1green_panties",66,757))
        elif "_black_panties" in state:
          rv.append(("nurse avatar b1black_panties",67,771))
        elif "_pantys" in state:
          rv.append(("nurse avatar b1panty",84,688))
        if "_green_bra" in state:
          rv.append(("nurse avatar b1green_bra",141,399))
        elif "_black_bra" in state:
          rv.append(("nurse avatar b1black_bra",154,534))
        elif "_bra" in state:
          rv.append(("nurse avatar b1bra",144,411))
        if "_masturbating" in state:
          rv.append(("nurse avatar b1arm2_n_vibrator",101,440))
        else:
          rv.append(("nurse avatar b1arm2_n",101,440))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b1outfit",57,399))
          rv.append(("nurse avatar b1arm2_c",100,405))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b1openshirt",46,397))
          rv.append(("nurse avatar b1arm2_c_vibrator",90,413))
        elif "_nice_dress" in state:
          rv.append(("nurse avatar b1nice_dress",61,399))
          rv.append(("nurse avatar b1arm2_nice_dress",99,431))
        elif "_dress" in state:
          rv.append(("nurse avatar b1dress",41,399))
          rv.append(("nurse avatar b1id",201,549))
          rv.append(("nurse avatar b1arm2_dress",96,398))
        if "_pendant" in state:
          rv.append(("nurse avatar b1pendant",214,421))
        if "_hat" in state:
          rv.append(("nurse avatar b1hat",158,125))

      elif state.startswith("surprised"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body1",61,153))
        rv.append(("nurse avatar face_surprised",169,261))
        if "_green_panties" in state:
          rv.append(("nurse avatar b1green_panties",66,757))
        elif "_pantys" in state:
          rv.append(("nurse avatar b1panty",84,688))
        if "_green_bra" in state:
          rv.append(("nurse avatar b1green_bra",141,399))
        elif "_bra" in state:
          rv.append(("nurse avatar b1bra",144,411))
        if "_masturbating" in state:
          rv.append(("nurse avatar b1arm2_n_vibrator",101,440))
        elif "_phone" in state:
          rv.append(("nurse avatar b1arm2_n_phone",101,301))
        elif "_crossed_arms" in state:
          rv.append(("nurse avatar b1arm1_n",101,400))
        else:
          rv.append(("nurse avatar b1arm2_n",101,440))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b1outfit",57,399))
          if "_phone" in state:
            rv.append(("nurse avatar b1arm2_c_phone",100,405))
          else:
            rv.append(("nurse avatar b1arm2_c",100,405))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b1openshirt",46,397))
          rv.append(("nurse avatar b1arm2_c_vibrator",90,413))
        elif "_dress" in state:
          rv.append(("nurse avatar b1dress",41,399))
          rv.append(("nurse avatar b1id",201,549))
          if "_crossed_arms" in state:
            rv.append(("nurse avatar b1arm1_dress",98,399))
          else:
            rv.append(("nurse avatar b1arm2_dress",96,398))
        if "_hat" in state:
          rv.append(("nurse avatar b1hat",158,125))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body2",82,159))
        if "_oiled_up" in state:
          rv.append(("nurse avatar b2oil",172,273))
        rv.append(("nurse avatar face_blush",243,259))
        if "_green_panties" in state:
          rv.append(("nurse avatar b2green_panties",90,755))
        elif "_black_panties" in state:
          rv.append(("nurse avatar b2black_panties",88,762))
        elif "_pantys" in state:
          rv.append(("nurse avatar b2panty",107,682))
        if "_green_bra" in state:
          rv.append(("nurse avatar b2green_bra",196,420))
        elif "_black_bra" in state:
          rv.append(("nurse avatar b2black_bra",199,555))
        elif "_bra" in state:
          rv.append(("nurse avatar b2bra",169,421))
        if "_masturbating" in state:
          rv.append(("nurse avatar b2arm2_n_vibrator",138,373))
        else:
          rv.append(("nurse avatar b2arm2_n",138,373))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b2outfit",76,408))
          rv.append(("nurse avatar b2arm2_c",133,373))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b2openshirt",78,405))
          rv.append(("nurse avatar b2arm2_c_vibrator",125,373))
        elif "_revealing_dress" in state:
          rv.append(("nurse avatar b2revealing_dress",82,416))
        elif "_sophisticated_dress" in state:
          rv.append(("nurse avatar b2sophisticated_dress",67,419))
        elif "_nice_dress" in state:
          rv.append(("nurse avatar b2nice_dress",81,419))
        elif "_dress" in state:
          rv.append(("nurse avatar b2dress",68,403))
          rv.append(("nurse avatar b2id",221,550))
        elif "_hiking_gear" in state:
          rv.append(("nurse avatar b2hiking_gear",71,416))
          rv.append(("nurse avatar b2arm2_hiking_gear",335,373))
        if any(dress in state for dress in ("_dress","_revealing_dress","_sophisticated_dress","_nice_dress")):
          rv.append(("nurse avatar b2arm2_dress",335,373))
        if "_pendant" in state:
          rv.append(("nurse avatar b2pendant",228,412))
        if "_sun_hat" in state:
          rv.append(("nurse avatar b2sun_hat",113,136))
        elif "_hat" in state:
          rv.append(("nurse avatar b2hat",241,124))

      elif state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body2",82,159))
        rv.append(("nurse avatar face_concerned",240,257))
        if "_green_panties" in state:
          rv.append(("nurse avatar b2green_panties",90,755))
        elif "_pantys" in state:
          rv.append(("nurse avatar b2panty",107,682))
        if "_green_bra" in state:
          rv.append(("nurse avatar b2green_bra",196,420))
        elif "_bra" in state:
          rv.append(("nurse avatar b2bra",169,421))
        if "_masturbating" in state:
          rv.append(("nurse avatar b2arm1_n_vibrator",138,430))
        else:
          rv.append(("nurse avatar b2arm1_n",138,430))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b2outfit",76,408))
          rv.append(("nurse avatar b2arm1_c",133,430))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b2openshirt",78,405))
          rv.append(("nurse avatar b2arm1_c_vibrator",125,430))
        elif "_dress" in state:
          rv.append(("nurse avatar b2dress",68,403))
          rv.append(("nurse avatar b2id",221,550))
          rv.append(("nurse avatar b2arm1_dress",277,430))
        elif "_hiking_gear" in state:
          rv.append(("nurse avatar b2hiking_gear",71,416))
          rv.append(("nurse avatar b2arm1_hiking_gear",277,430))
        if "_sun_hat" in state:
          rv.append(("nurse avatar b2sun_hat",113,136))
        elif "_hat" in state:
          rv.append(("nurse avatar b2hat",241,124))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body2",82,159))
        if "_oiled_up" in state:
          rv.append(("nurse avatar b2oil",172,273))
        rv.append(("nurse avatar face_sad",240,259))
        if "_green_panties" in state:
          rv.append(("nurse avatar b2green_panties",90,755))
        elif "_pantys" in state:
          rv.append(("nurse avatar b2panty",107,682))
        if "_green_bra" in state:
          rv.append(("nurse avatar b2green_bra",196,420))
        elif "_bra" in state:
          rv.append(("nurse avatar b2bra",169,421))
        if "_masturbating" in state:
          rv.append(("nurse avatar b2arm1_n_vibrator",138,430))
        else:
          rv.append(("nurse avatar b2arm1_n",138,430))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b2outfit",76,408))
          rv.append(("nurse avatar b2arm1_c",133,430))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b2openshirt",78,405))
          rv.append(("nurse avatar b2arm1_c_vibrator",125,430))
        elif "_dress" in state:
          rv.append(("nurse avatar b2dress",68,403))
          rv.append(("nurse avatar b2id",221,550))
          rv.append(("nurse avatar b2arm1_dress",277,430))
        elif "_hiking_gear" in state:
          rv.append(("nurse avatar b2hiking_gear",71,416))
          rv.append(("nurse avatar b2arm1_hiking_gear",277,430))
        if "_sun_hat" in state:
          rv.append(("nurse avatar b2sun_hat",113,136))
        elif "_hat" in state:
          rv.append(("nurse avatar b2hat",241,124))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("nurse avatar body2",82,159))
        rv.append(("nurse avatar face_thinking",240,253))
        if "_green_panties" in state:
          rv.append(("nurse avatar b2green_panties",90,755))
        elif "_black_panties" in state:
          rv.append(("nurse avatar b2black_panties",88,762))
        elif "_pantys" in state:
          rv.append(("nurse avatar b2panty",107,682))
        if "_green_bra" in state:
          rv.append(("nurse avatar b2green_bra",196,420))
        elif "_black_bra" in state:
          rv.append(("nurse avatar b2black_bra",199,555))
        elif "_bra" in state:
          rv.append(("nurse avatar b2bra",169,421))
        if "_masturbating" in state:
          rv.append(("nurse avatar b2arm2_n_vibrator",138,373))
        else:
          rv.append(("nurse avatar b2arm2_n",138,373))
        if "_outfit" in state and "_shirt" in state:
          rv.append(("nurse avatar b2outfit",76,408))
          rv.append(("nurse avatar b2arm2_c",133,373))
        elif "_masturbating" in state:
          rv.append(("nurse avatar b2openshirt",78,405))
          rv.append(("nurse avatar b2arm2_c_vibrator",125,373))
        elif "_revealing_dress" in state:
          rv.append(("nurse avatar b2revealing_dress",82,416))
        elif "_sophisticated_dress" in state:
          rv.append(("nurse avatar b2sophisticated_dress",67,419))
        elif "_nice_dress" in state:
          rv.append(("nurse avatar b2nice_dress",81,419))
        elif "_dress" in state:
          rv.append(("nurse avatar b2dress",68,403))
          rv.append(("nurse avatar b2id",221,550))
        elif "_hiking_gear" in state:
          rv.append(("nurse avatar b2hiking_gear",71,416))
          rv.append(("nurse avatar b2arm2_hiking_gear",335,373))
        if any(dress in state for dress in ("_dress","_revealing_dress","_sophisticated_dress")):
          rv.append(("nurse avatar b2arm2_dress",335,373))
        if "_pendant" in state:
          rv.append(("nurse avatar b2pendant",228,412))
        if "_sun_hat" in state:
          rv.append(("nurse avatar b2sun_hat",113,136))
        elif "_hat" in state:
          rv.append(("nurse avatar b2hat",241,124))

      elif state.startswith("clinic_nurse"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events clinic LindseyClinicBG",0,0))
        rv.append(("nurse avatar events clinic LindseyClinic_Nurse_n",110,19))
        rv.append(("nurse avatar events clinic LindseyClinic_Nurse_panty",119,736))
        rv.append(("nurse avatar events clinic LindseyClinic_Nurse_bra",227,322))
        rv.append(("nurse avatar events clinic LindseyClinic_Nurse",109,295))
        if "_angry" in state:
          rv.append(("nurse avatar events clinic LindseyClinic_Nurse_angry",285,134))
        if "_confused" in state:
          rv.append(("nurse avatar events clinic LindseyClinic_Nurse_confused",269,136))
        if "_surprised" in state:
          rv.append(("nurse avatar events clinic LindseyClinic_Nurse_surprised",277,136))

      elif state.startswith("plowcow"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events plowcow plowcow_n",0,0))

      elif state.startswith("blowjob"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events blowjob bg",0,0))
        if state.endswith(("reveal","encouragement","dick_out")):
          rv.append(("nurse avatar events blowjob nurse_body1",449,272))
        elif state.endswith(("shove","kate_touched_mcs_dick_lmao","deepthroat1","deepthroat6","cum_in_mouth")):
          rv.append(("nurse avatar events blowjob nurse_body2a",571,513))
          rv.append(("nurse avatar events blowjob nurse_body2b",452,401))
          rv.append(("nurse avatar events blowjob nurse_body2c",466,618))
        elif state.endswith(("suck2","suck4","deepthroat4")):
          rv.append(("nurse avatar events blowjob nurse_body3a",594,474))
          rv.append(("nurse avatar events blowjob nurse_body3b",492,364))
          rv.append(("nurse avatar events blowjob nurse_body3c",503,652))
        elif state.endswith(("suck5","deepthroat3a","deepthroat3b")):
          rv.append(("nurse avatar events blowjob nurse_body4a",586,488))
          rv.append(("nurse avatar events blowjob nurse_body4b",477,377))
          rv.append(("nurse avatar events blowjob nurse_body4c",478,473))
        elif state.endswith(("deepthroat2a","deepthroat2b")):
          rv.append(("nurse avatar events blowjob nurse_body5a",569,501))
          rv.append(("nurse avatar events blowjob nurse_body5b",466,391))
          rv.append(("nurse avatar events blowjob nurse_body5c",480,681))
        else:
          rv.append(("nurse avatar events blowjob nurse_body6a",596,457))
          rv.append(("nurse avatar events blowjob nurse_body6b",504,346))
          rv.append(("nurse avatar events blowjob nurse_body6c",502,621))
        if state.endswith(("reveal","encouragement")):
          rv.append(("nurse avatar events blowjob nurse_right_arm1",610,271))
        elif state.endswith("dick_out"):
          rv.append(("nurse avatar events blowjob nurse_right_arm2",453,272))
        elif state.endswith(("shove","kate_touched_mcs_dick_lmao","deepthroat1")):
          rv.append(("nurse avatar events blowjob nurse_right_arm3",411,498))
        elif state.endswith(("suck2","suck4")):
          rv.append(("nurse avatar events blowjob nurse_right_arm4",439,458))
        elif state.endswith("suck5"):
          rv.append(("nurse avatar events blowjob nurse_right_arm5",425,472))
        elif state.endswith(("deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth","ahegao")):
          pass
        else:
          rv.append(("nurse avatar events blowjob nurse_right_arm11",448,441))
        if state.endswith(("reveal","encouragement","dick_out")):
          rv.append(("nurse avatar events blowjob nurse_head1",565,32))
        elif state.endswith("shove"):
          rv.append(("nurse avatar events blowjob nurse_head2",255,249))
        elif state.endswith("headpat"):
          rv.append(("nurse avatar events blowjob nurse_head3",436,186))
        elif state.endswith(("kiss","lick2","lick4")):
          rv.append(("nurse avatar events blowjob nurse_head4",439,207))
        elif state.endswith(("lick1","lick3")):
          rv.append(("nurse avatar events blowjob nurse_head5",449,208))
        elif state.endswith("kate_touched_mcs_dick_lmao"):
          rv.append(("nurse avatar events blowjob nurse_head6",335,218))
        elif state.endswith(("suck1","suck2","suck3","suck4","suck5","handsy","no_touching","hair_grab","deepthroat1","deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth","ahegao")):
          pass
        else:
          rv.append(("nurse avatar events blowjob nurse_head12",392,195))
        if state.endswith("reveal"):
          if "_concerned" in state:
            rv.append(("nurse avatar events blowjob nurse_face2",618,269))
          else:
            rv.append(("nurse avatar events blowjob nurse_face1",618,254))
        elif state.endswith("encouragement"):
          rv.append(("nurse avatar events blowjob nurse_face2",618,269))
        elif state.endswith("dick_out"):
          rv.append(("nurse avatar events blowjob nurse_face3",621,254))
        elif state.endswith("kiss"):
          rv.append(("nurse avatar events blowjob nurse_face4",500,413))
        elif state.endswith(("lick2","lick4")):
          rv.append(("nurse avatar events blowjob nurse_face5",498,413))
        if state.endswith(("encouragement","dick_out")):
          rv.append(("nurse avatar events blowjob kate_right_arm1_n",991,306))
          rv.append(("nurse avatar events blowjob kate_right_arm1_c",988,301))
        elif state.endswith("shove"):
          rv.append(("nurse avatar events blowjob kate_right_arm2_n",613,337))
          rv.append(("nurse avatar events blowjob kate_right_arm2_c",613,337))
        elif state.endswith("headpat"):
          rv.append(("nurse avatar events blowjob kate_right_arm3_n",746,171))
          rv.append(("nurse avatar events blowjob kate_right_arm3_c",746,171))
        elif state.endswith("kate_touched_mcs_dick_lmao"):
          rv.append(("nurse avatar events blowjob kate_right_arm4_n",601,228))
          rv.append(("nurse avatar events blowjob kate_right_arm4_c",824,298))
        elif state.endswith(("hair_grab","deepthroat1","deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth","ahegao")):
          pass
        else:
          rv.append(("nurse avatar events blowjob kate_right_arm11_n",1167,357))
          rv.append(("nurse avatar events blowjob kate_right_arm11_c",1170,357))
        rv.append(("nurse avatar events blowjob kate_legs_n",1144,744))
        rv.append(("nurse avatar events blowjob kate_legs_u",1289,796))
        rv.append(("nurse avatar events blowjob kate_legs_c",1137,790))
        if state.endswith(("kate_touched_mcs_dick_lmao","hair_grab","deepthroat1","deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth","ahegao")):
          rv.append(("nurse avatar events blowjob kate_chest1_n",1269,301))
          rv.append(("nurse avatar events blowjob kate_chest1_u",1286,357))
          rv.append(("nurse avatar events blowjob kate_chest1_c",1269,301))
        else:
          rv.append(("nurse avatar events blowjob kate_chest2_n",1340,357))
          rv.append(("nurse avatar events blowjob kate_chest2_u",1332,371))
          rv.append(("nurse avatar events blowjob kate_chest2_c",1340,357))
        if state.endswith(("kate_touched_mcs_dick_lmao","no_touching")):
          pass
        elif state.endswith(("hair_grab","deepthroat1","deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth","ahegao")):
          rv.append(("nurse avatar events blowjob kate_left_arm3_n",1161,577))
          rv.append(("nurse avatar events blowjob kate_left_arm3_c",1139,573))
        else:
          rv.append(("nurse avatar events blowjob kate_left_arm4_n",1013,617))
          rv.append(("nurse avatar events blowjob kate_left_arm4_c",1083,613))
        if state.endswith(("reveal","headpat","no_touching")):
          if "_annoyed" in state:
            rv.append(("nurse avatar events blowjob kate_head5",1357,41))
          else:
            rv.append(("nurse avatar events blowjob kate_head1",1363,36))
        elif state.endswith(("kate_touched_mcs_dick_lmao","hair_grab","deepthroat1","deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth","ahegao")):
          pass
        else:
          rv.append(("nurse avatar events blowjob kate_head5",1357,41))
        if state.endswith(("reveal","headpat")):
          if "_annoyed" in state:
            rv.append(("nurse avatar events blowjob kate_face5",1407,241))
          else:
            rv.append(("nurse avatar events blowjob kate_face1",1440,210))
        elif state.endswith(("encouragement","kiss","suck1","suck2")):
          rv.append(("nurse avatar events blowjob kate_face2",1411,240))
        elif state.endswith(("dick_out","shove")):
          rv.append(("nurse avatar events blowjob kate_face3",1363,240))
        elif state.endswith(("lick2","lick1","suck3","suck4","suck5")):
          rv.append(("nurse avatar events blowjob kate_face4",1405,240))
        elif state.endswith(("lick3","lick4")):
          rv.append(("nurse avatar events blowjob kate_face5",1407,241))
        elif state.endswith("handsy"):
          rv.append(("nurse avatar events blowjob kate_face7",1409,240))
        elif state.endswith("no_touching"):
          rv.append(("nurse avatar events blowjob kate_face8",1438,210))
        if state.endswith(("reveal","encouragement")):
          rv.append(("nurse avatar events blowjob mc_pants1",0,452))
        else:
          rv.append(("nurse avatar events blowjob mc_pants2",0,452))
        if state.endswith(("reveal","encouragement")):
          pass
        elif state.endswith(("dick_out","headpat","kiss","lick2","lick4","hair_grab","deepthroat1")):
          rv.append(("nurse avatar events blowjob mc_dick1",330,617))
        elif state.endswith("shove"):
          rv.append(("nurse avatar events blowjob mc_dick2",301,633))
        elif state.endswith("kate_touched_mcs_dick_lmao"):
          rv.append(("nurse avatar events blowjob mc_dick3",331,613))
        elif state.endswith(("deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth")):
          pass
        elif state.endswith("ahegao"):
          rv.append(("nurse avatar events blowjob mc_dick5",330,697))
        else:
          rv.append(("nurse avatar events blowjob mc_dick6",331,602))
        if state.endswith(("deepthroat2a","deepthroat2b")):
          rv.append(("nurse avatar events blowjob nurse_right_arm6",0,408))
        elif state.endswith(("deepthroat3a","deepthroat3b")):
          rv.append(("nurse avatar events blowjob nurse_right_arm7",16,380))
        elif state.endswith("deepthroat4"):
          rv.append(("nurse avatar events blowjob nurse_right_arm8",21,347))
        elif state.endswith(("deepthroat5","ahegao")):
          rv.append(("nurse avatar events blowjob nurse_right_arm9",21,331))
        elif state.endswith(("deepthroat6","cum_in_mouth")):
          rv.append(("nurse avatar events blowjob nurse_right_arm10",0,430))
        if state.endswith(("deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b","deepthroat4","deepthroat5","deepthroat6","cum_in_mouth")):
          rv.append(("nurse avatar events blowjob mc_dick4",331,602))
        rv.append(("nurse avatar events blowjob mc_shirt",0,445))
        if state.endswith(("shove","handsy","no_touching")):
          pass
        else:
          rv.append(("nurse avatar events blowjob mc_right_arm3",0,684))
        if state.endswith(("suck2","suck4","deepthroat4")):
          rv.append(("nurse avatar events blowjob nurse_head7",355,242))
        elif state.endswith(("suck5","deepthroat3a","deepthroat3b")):
          rv.append(("nurse avatar events blowjob nurse_head8",321,279))
        elif state.endswith(("hair_grab","ahegao")):
          rv.append(("nurse avatar events blowjob nurse_head9",364,151))
        elif state.endswith(("deepthroat1","deepthroat6","cum_in_mouth")):
          rv.append(("nurse avatar events blowjob nurse_head10",240,352))
        elif state.endswith(("deepthroat2a","deepthroat2b")):
          rv.append(("nurse avatar events blowjob nurse_head11",282,316))
        elif state.endswith(("suck1","suck3","handsy","no_touching","deepthroat5")):
          rv.append(("nurse avatar events blowjob nurse_head12",392,195))
        if state.endswith("suck1"):
          rv.append(("nurse avatar events blowjob nurse_face6",417,430))
        elif state.endswith("suck2"):
          rv.append(("nurse avatar events blowjob nurse_face7",395,502))
        elif state.endswith(("suck3","handsy")):
          rv.append(("nurse avatar events blowjob nurse_face8",417,434))
        elif state.endswith("suck4"):
          rv.append(("nurse avatar events blowjob nurse_face9",395,504))
        elif state.endswith("suck5"):
          rv.append(("nurse avatar events blowjob nurse_face10",386,563))
        elif state.endswith("no_touching"):
          rv.append(("nurse avatar events blowjob nurse_face11",417,434))
        elif state.endswith("hair_grab"):
          rv.append(("nurse avatar events blowjob nurse_face12",462,304))
        elif state.endswith("deepthroat1"):
          rv.append(("nurse avatar events blowjob nurse_face13",296,664))
        elif state.endswith("deepthroat2a"):
          rv.append(("nurse avatar events blowjob nurse_face14",334,617))
        elif state.endswith("deepthroat2b"):
          rv.append(("nurse avatar events blowjob nurse_face14b",334,617))
        elif state.endswith("deepthroat3a"):
          rv.append(("nurse avatar events blowjob nurse_face15",384,556))
        elif state.endswith("deepthroat3b"):
          rv.append(("nurse avatar events blowjob nurse_face15b",384,556))
        elif state.endswith("deepthroat4"):
          rv.append(("nurse avatar events blowjob nurse_face16",395,501))
        elif state.endswith("deepthroat5"):
          rv.append(("nurse avatar events blowjob nurse_face17",417,442))
        elif state.endswith("deepthroat6"):
          if "_eyes_closed" in state:
            rv.append(("nurse avatar events blowjob nurse_face13",296,664))
          else:
            rv.append(("nurse avatar events blowjob nurse_face18",296,664))
        elif state.endswith("cum_in_mouth"):
          rv.append(("nurse avatar events blowjob nurse_face18b",296,664))
        elif state.endswith("ahegao"):
          rv.append(("nurse avatar events blowjob nurse_face19",462,307))
        if state.endswith(("shove","handsy")):
          rv.append(("nurse avatar events blowjob mc_right_arm1",0,653))
        elif state.endswith("no_touching"):
          rv.append(("nurse avatar events blowjob mc_right_arm2",0,530))
        if state.endswith(("hair_grab","ahegao")):
          rv.append(("nurse avatar events blowjob kate_right_arm5_n",697,82))
          rv.append(("nurse avatar events blowjob kate_right_arm5_c",878,187))
        elif state.endswith(("deepthroat1","deepthroat6","cum_in_mouth")):
          rv.append(("nurse avatar events blowjob kate_right_arm6_n",349,299))
          rv.append(("nurse avatar events blowjob kate_right_arm6_c",612,287))
        elif state.endswith(("deepthroat2a","deepthroat2b")):
          rv.append(("nurse avatar events blowjob kate_right_arm7_n",383,266))
          rv.append(("nurse avatar events blowjob kate_right_arm7_c",654,259))
        elif state.endswith(("deepthroat3a","deepthroat3b")):
          rv.append(("nurse avatar events blowjob kate_right_arm8_n",457,222))
          rv.append(("nurse avatar events blowjob kate_right_arm8_c",706,239))
        elif state.endswith("deepthroat4"):
          rv.append(("nurse avatar events blowjob kate_right_arm9_n",513,188))
          rv.append(("nurse avatar events blowjob kate_right_arm9_c",752,217))
        elif state.endswith("deepthroat5"):
          rv.append(("nurse avatar events blowjob kate_right_arm10_n",648,157))
          rv.append(("nurse avatar events blowjob kate_right_arm10_c",836,211))
        if state.endswith("kate_touched_mcs_dick_lmao"):
          rv.append(("nurse avatar events blowjob kate_left_arm1_n",375,569))
          rv.append(("nurse avatar events blowjob kate_left_arm1_c",654,555))
        elif state.endswith("no_touching"):
          rv.append(("nurse avatar events blowjob kate_left_arm2_n",572,617))
          rv.append(("nurse avatar events blowjob kate_left_arm2_c",838,614))
        if state.endswith(("kate_touched_mcs_dick_lmao","deepthroat4","deepthroat5")):
          rv.append(("nurse avatar events blowjob kate_head2",1077,0))
        elif state.endswith(("hair_grab","deepthroat1","deepthroat6","cum_in_mouth","ahegao")):
          rv.append(("nurse avatar events blowjob kate_head3",1088,10))
        elif state.endswith(("deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b")):
          rv.append(("nurse avatar events blowjob kate_head4",1083,4))
        if state.endswith("kate_touched_mcs_dick_lmao"):
          rv.append(("nurse avatar events blowjob kate_face6",1093,293))
        elif state.endswith("hair_grab"):
          rv.append(("nurse avatar events blowjob kate_face9",1101,309))
        elif state.endswith(("deepthroat1","deepthroat6")):
          rv.append(("nurse avatar events blowjob kate_face10",1101,309))
        elif state.endswith(("deepthroat2a","deepthroat2b","deepthroat3a","deepthroat3b")):
          rv.append(("nurse avatar events blowjob kate_face11",1101,306))
        elif state.endswith(("deepthroat4","deepthroat5")):
          if "_scolding" in state:
            rv.append(("nurse avatar events blowjob kate_face6",1093,293))
          else:
            rv.append(("nurse avatar events blowjob kate_face12",1088,293))
        elif state.endswith("cum_in_mouth"):
          rv.append(("nurse avatar events blowjob kate_face13",961,296))
        elif state.endswith("ahegao"):
          rv.append(("nurse avatar events blowjob kate_face14",1101,309))

      elif state.startswith("vent"):
#       rv.append((DO,NOT))                                  ## Unlike regular poses, dimensions for animated poses are ignored — they'll be automatically set to size of video once they're played.
#       rv.append(("nurse avatar events vent example",0,0))  ## However, x and y positions can still be provided to position the video should it not be fullscreen (i.e. animated sprites).
#       rv.append("nurse avatar events vent example")        ## Should it be fullscreen, though, a simple string will suffice.
        if state.endswith("waiting"):
          if nurse.equipped_item("nurse_shirt"):
            rv.append("nurse avatar events vent waiting_c")
          else:
            rv.append("nurse avatar events vent waiting_n")
        elif state.endswith("_crawling"):
          if nurse.equipped_item("nurse_shirt"):
            rv.append("nurse avatar events vent crawling_c")
          else:
            rv.append("nurse avatar events vent crawling_n")

      elif state.startswith("dog"):
        rv.append((1920,1080))
        if "_kate" in state:
          if "_backgroundless" not in state:
            rv.append(("nurse avatar events dog kate_background",0,0))
          rv.append(("nurse avatar events dog kate_body",613,89))
          if "_annoyed" in state:
            rv.append(("nurse avatar events dog kate_face1",983,177))
          elif "_excited" in state:
            rv.append(("nurse avatar events dog kate_face2",982,176))
          elif "_neutral" in state:
            rv.append(("nurse avatar events dog kate_face3",981,176))
          elif "_skeptical" in state:
            rv.append(("nurse avatar events dog kate_face4",982,177))
          elif "_surprised" in state:
            rv.append(("nurse avatar events dog kate_face5",980,171))
        else:
          if "_mouth_closed" in state:
            if "_right" in state:
              rv.append(("nurse avatar events dog nurse_body1",0,0))
            else:
              rv.append(("nurse avatar events dog nurse_body2",0,0))
          elif "_mouth_open" in state:
            rv.append(("nurse avatar events dog nurse_body3",0,0))

      elif state.startswith("gym_sex"):
        rv.append((1920,1080))
        if "_stare" in state:
          rv.append(("nurse avatar events gym_sex stare",0,0))
        elif "_explanation" in state:
          rv.append(("nurse avatar events gym_sex explanation",0,0))
        elif "_into_position" in state:
          rv.append(("nurse avatar events gym_sex into_position",0,0))
        elif "_leg_lift" in state:
          rv.append(("nurse avatar events gym_sex leg_lift",0,0))

      elif state.startswith("mystery_box"):
        rv.append((1920,1080))
        if "_close_up" in state:
          rv.append(("nurse avatar events mystery_box close_up",0,0))
        elif "_reveal" in state:
          rv.append(("nurse avatar events mystery_box reveal",0,0))

      elif state.startswith("kitchen_"):
        rv.append((1920,1080))
        if "_crawling" in state:
          rv.append(("nurse avatar events kitchen crawling",0,0))
        elif "_posing" in state:
          rv.append(("nurse avatar events kitchen posing",0,0))

      elif state.startswith("worm_digging"):
        rv.append((1920,1080))
        if school_forest_glade["pollution"]:
          rv.append(("nurse avatar events worm_digging background_dirty",0,0))
        else:
          rv.append(("nurse avatar events worm_digging background_clean",0,0))
        rv.append(("nurse avatar events worm_digging nurse_body",0,93))
        rv.append(("nurse avatar events worm_digging nurse_underwear",392,181))
        rv.append(("nurse avatar events worm_digging nurse_clothes",339,73))
        rv.append(("nurse avatar events worm_digging mc_body",0,309))

      elif state.startswith("fishing"):
        rv.append((1920,1080))
        if school_forest_glade["pollution"]:
          rv.append(("nurse avatar events fishing background_dirty",0,0))
        else:
          rv.append(("nurse avatar events fishing background_clean",0,0))
        if state.endswith("maxine_reveal"):
          rv.append(("nurse avatar events fishing maxine_body",36,352))
          rv.append(("nurse avatar events fishing maxine_clothes",221,326))
        if state.endswith("wait"):
          rv.append(("nurse avatar events fishing nurse_body1",62,0))
        elif state.endswith("reeling_in"):
          rv.append(("nurse avatar events fishing nurse_body2",38,0))
        elif state.endswith("maxine_reveal"):
          rv.append(("nurse avatar events fishing nurse_body3",288,0))
        if state.endswith(("wait","maxine_reveal")):
          rv.append(("nurse avatar events fishing nurse_underwear1",989,505))
          rv.append(("nurse avatar events fishing nurse_clothes1",987,113))
        elif state.endswith("reeling_in"):
          rv.append(("nurse avatar events fishing nurse_underwear2",1086,541))
          rv.append(("nurse avatar events fishing nurse_clothes2",1074,133))

      elif state.startswith("hiking_help"):
        expression = None if state.endswith(("mc_down","ankle_hold","ankle_wrap")) else state.rsplit(" ", 1)[1]
        state = state if state.endswith(("mc_down","ankle_hold","ankle_wrap")) else state.rsplit(" ", 1)[0]
        rv.append((1920,1080))
        rv.append(("nurse avatar events hiking_help background",0,0))
        if state.endswith("mc_down"):
          rv.append(("nurse avatar events hiking_help mc_body1",0,453))
          rv.append(("nurse avatar events hiking_help nurse_body1",1247,118))
          rv.append(("nurse avatar events hiking_help nurse_underwear1",1324,346))
          rv.append(("nurse avatar events hiking_help nurse_clothes1",1235,80))
        elif state.endswith(("ankle_hold","ankle_wrap")):
          rv.append(("nurse avatar events hiking_help mc_body2",0,452))
          if state.endswith("ankle_hold"):
            rv.append(("nurse avatar events hiking_help nurse_body2",646,23))
          elif state.endswith("ankle_wrap") and not expression:
            rv.append(("nurse avatar events hiking_help nurse_body3",646,23))
          elif state.endswith("ankle_wrap") and expression.endswith("smile"):
            rv.append(("nurse avatar events hiking_help nurse_body4",646,23))
          rv.append(("nurse avatar events hiking_help nurse_underwear2",968,469))
          rv.append(("nurse avatar events hiking_help nurse_clothes2",499,0))
          if state.endswith("ankle_hold"):
            rv.append(("nurse avatar events hiking_help nurse_left_arm1",1432,640))
          elif state.endswith("ankle_wrap"):
            rv.append(("nurse avatar events hiking_help nurse_left_arm2",547,529))

      elif state.startswith("hiking_hurt"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events hiking_hurt background",0,0))
        rv.append(("nurse avatar events hiking_hurt nurse_body",393,108))
        rv.append(("nurse avatar events hiking_hurt nurse_underwear",747,318))
        rv.append(("nurse avatar events hiking_hurt nurse_clothes",387,0))

      elif state.startswith("hiking_carried"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events hiking_carried background",0,0))
        rv.append(("nurse avatar events hiking_carried mc_body",337,0))
        rv.append(("nurse avatar events hiking_carried nurse_body",72,0))
        rv.append(("nurse avatar events hiking_carried nurse_underwear",548,389))
        rv.append(("nurse avatar events hiking_carried nurse_clothes",0,0))

      elif state.startswith("date_spying"):
        expression = None if state.endswith(("date","confrontation","aftermath")) else state.rsplit(" ", 1)[1]
        state = state if state.endswith(("date","confrontation","aftermath")) else state.rsplit(" ", 1)[0]
        rv.append((1920,1080))
        rv.append(("nurse avatar events date_spying background",0,0))
        if state.endswith("date"):
          rv.append(("nurse avatar events date_spying mr_brown_body1",109,0))
          rv.append(("nurse avatar events date_spying mc_body1",0,0))
        elif state.endswith(("confrontation","aftermath")):
          if state.endswith("confrontation"):
            rv.append(("nurse avatar events date_spying mr_brown_body2",109,0))
          rv.append(("nurse avatar events date_spying mc_body2",981,0))
        rv.append(("nurse avatar events date_spying nurse_body",1036,242))
        rv.append(("nurse avatar events date_spying nurse_bra",1111,624))
        if quest.nurse_aid["dress_chosen"] == "revealing_dress":
          rv.append(("nurse avatar events date_spying nurse_revealing_dress",1112,510))
        elif quest.nurse_aid["dress_chosen"] == "sophisticated_dress":
          rv.append(("nurse avatar events date_spying nurse_sophisticated_dress",1057,502))
        else: # elif quest.nurse_aid["dress_chosen"] == "nice_dress":
          rv.append(("nurse avatar events date_spying nurse_nice_dress",1036,497))
        if state.endswith("date") and not expression:
          rv.append(("nurse avatar events date_spying nurse_face1",1196,381))
        elif state.endswith("confrontation") and not expression:
          rv.append(("nurse avatar events date_spying nurse_face2",1196,375))
        elif (state.endswith("aftermath") and not expression) or expression.endswith("sad"):
          rv.append(("nurse avatar events date_spying nurse_face3",1196,375))
        elif expression.endswith("annoyed"):
          rv.append(("nurse avatar events date_spying nurse_face4",1196,387))
        elif expression.endswith("blush"):
          rv.append(("nurse avatar events date_spying nurse_face5",1196,376))

      elif state.startswith("date_pancakes"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events date_pancakes background",0,0))
        rv.append(("nurse avatar events date_pancakes nurse_body",676,61))
        rv.append(("nurse avatar events date_pancakes nurse_bra",860,507))
        if quest.nurse_aid["dress_chosen"] == "revealing_dress":
          rv.append(("nurse avatar events date_pancakes nurse_revealing_dress",839,374))
        elif quest.nurse_aid["dress_chosen"] == "sophisticated_dress":
          rv.append(("nurse avatar events date_pancakes nurse_sophisticated_dress",766,372))
        else: # elif quest.nurse_aid["dress_chosen"] == "nice_dress":
          rv.append(("nurse avatar events date_pancakes nurse_nice_dress",735,333))
        rv.append(("nurse avatar events date_pancakes foreground",1231,375))

      elif state.startswith("jacklyn_service"):
        suffix = None if state.endswith(("anticipation","rubbing1","rubbing2","rubbing3","rubbing4","fingering1","fingering2","fingering3","fingering4","orgasm_denial")) else state.rsplit(" ", 1)[1]
        state = state if state.endswith(("anticipation","rubbing1","rubbing2","rubbing3","rubbing4","fingering1","fingering2","fingering3","fingering4","orgasm_denial")) else state.rsplit(" ", 1)[0]
        rv.append((1920,1080))
        rv.append(("nurse avatar events jacklyn_service background",0,0))
        rv.append(("nurse avatar events jacklyn_service jacklyn_body",546,596))
        rv.append(("nurse avatar events jacklyn_service jacklyn_underwear",858,794))
        rv.append(("nurse avatar events jacklyn_service jacklyn_clothes",550,670))
        rv.append(("nurse avatar events jacklyn_service nurse_body",0,65))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events jacklyn_service nurse_underwear1",71,120))
        else:
          rv.append(("nurse avatar events jacklyn_service nurse_underwear2",195,120))
        rv.append(("nurse avatar events jacklyn_service nurse_clothes",1,121))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events jacklyn_service nurse_head1",1116,0))
        elif state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4")) and not suffix:
          rv.append(("nurse avatar events jacklyn_service nurse_head2",1116,0))
        elif state.endswith(("fingering1","fingering2","fingering3","fingering4")) and not suffix:
          rv.append(("nurse avatar events jacklyn_service nurse_head3",1116,0))
        elif state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4")) and suffix.endswith("ear_bite"):
          rv.append(("nurse avatar events jacklyn_service nurse_head4",1154,22))
        elif state.endswith(("fingering1","fingering2","fingering3","fingering4")) and suffix.endswith("neck_lick"):
          rv.append(("nurse avatar events jacklyn_service nurse_head5",1116,0))
        elif state.endswith(("rubbing1","fingering1","fingering2","fingering3","fingering4")) and suffix.endswith("imminent_orgasm"):
          rv.append(("nurse avatar events jacklyn_service nurse_head6",1154,22))
        elif state.endswith("orgasm_denial"):
          rv.append(("nurse avatar events jacklyn_service nurse_head7",1116,0))
        if state.endswith(("anticipation","orgasm_denial")):
          rv.append(("nurse avatar events jacklyn_service nurse_hat1",1510,19))
        elif (state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4","fingering1","fingering2","fingering3","fingering4")) and not suffix) or suffix.endswith("neck_lick"):
          rv.append(("nurse avatar events jacklyn_service nurse_hat2",1511,0))
        elif state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4","fingering1","fingering2","fingering3","fingering4")) and suffix.endswith(("ear_bite","imminent_orgasm")):
          rv.append(("nurse avatar events jacklyn_service nurse_hat3",1570,11))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_head1",1346,345))
        elif state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4")) and not suffix:
          rv.append(("nurse avatar events jacklyn_service jacklyn_head2",1302,290))
        elif state.endswith(("fingering1","fingering2","fingering3","fingering4")) and not suffix:
          rv.append(("nurse avatar events jacklyn_service jacklyn_head3",1302,290))
        elif state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4")) and suffix.endswith("ear_bite"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_head4",1323,321))
        elif state.endswith(("fingering1","fingering2","fingering3","fingering4")) and suffix.endswith("neck_lick"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_head5",1280,248))
        elif state.endswith(("rubbing1","fingering1","fingering2","fingering3","fingering4")) and suffix.endswith("imminent_orgasm"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_head6",1302,290))
        elif state.endswith("orgasm_denial"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_head7",1346,345))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm1",99,452))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve1",858,753))
        elif state.endswith("rubbing1"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm2",8,560))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve2",518,782))
        elif state.endswith("rubbing2"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm3",3,549))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve3",515,771))
        elif state.endswith("rubbing3"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm4",5,572))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve4",513,789))
        elif state.endswith("rubbing4"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm5",6,573))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve5",526,793))
        elif state.endswith("fingering1"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm6",0,515))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve6",403,680))
        elif state.endswith("fingering2"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm7",3,483))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve7",421,651))
        elif state.endswith("fingering3"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm8",5,514))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve8",417,668))
        elif state.endswith("fingering4"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm9",6,518))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve9",412,677))
        elif state.endswith("orgasm_denial"):
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_arm10",0,459))
          rv.append(("nurse avatar events jacklyn_service jacklyn_left_sleeve10",861,664))

      elif state.startswith("mc_service"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events mc_service background",0,0))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events mc_service nurse_head1",49,86))
        elif state.endswith("bound"):
          rv.append(("nurse avatar events mc_service nurse_head2",9,119))
        elif state.endswith("dick_reveal"):
          rv.append(("nurse avatar events mc_service nurse_head3",9,119))
        elif state.endswith(("rubbing1","rubbing2","rubbing3","rubbing4")):
          rv.append(("nurse avatar events mc_service nurse_head4",49,86))
        elif state.endswith("penetration1"):
          rv.append(("nurse avatar events mc_service nurse_head5",85,94))
        elif state.endswith("penetration2"):
          rv.append(("nurse avatar events mc_service nurse_head6",49,86))
        elif state.endswith("penetration3"):
          rv.append(("nurse avatar events mc_service nurse_head7",32,81))
        elif state.endswith("penetration4"):
          rv.append(("nurse avatar events mc_service nurse_head8",23,74))
        elif state.endswith("shoulder_bite"):
          rv.append(("nurse avatar events mc_service nurse_head9",23,74))
        elif state.endswith("cum"):
          rv.append(("nurse avatar events mc_service nurse_head10",49,86))
        elif state.endswith("aftermath"):
          rv.append(("nurse avatar events mc_service nurse_head11",9,119))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events mc_service nurse_body1",95,259))
        elif state.endswith(("bound","dick_reveal","rubbing1","rubbing2","rubbing3","rubbing4","penetration2","cum","aftermath")):
          rv.append(("nurse avatar events mc_service nurse_body2",395,128))
        elif state.endswith("penetration1"):
          rv.append(("nurse avatar events mc_service nurse_body3",429,128))
        elif state.endswith("penetration3"):
          rv.append(("nurse avatar events mc_service nurse_body4",384,128))
        elif state.endswith(("penetration4","shoulder_bite")):
          rv.append(("nurse avatar events mc_service nurse_body5",369,128))
        if state.endswith("anticipation"):
          rv.append(("nurse avatar events mc_service nurse_legs1",912,280))
        elif state.endswith(("bound","dick_reveal")):
          rv.append(("nurse avatar events mc_service nurse_legs2",801,280))
        elif state.endswith("rubbing1"):
          rv.append(("nurse avatar events mc_service nurse_legs3",801,280))
        elif state.endswith("rubbing2"):
          rv.append(("nurse avatar events mc_service nurse_legs4",801,280))
        elif state.endswith("rubbing3"):
          rv.append(("nurse avatar events mc_service nurse_legs5",801,280))
        elif state.endswith("rubbing4"):
          rv.append(("nurse avatar events mc_service nurse_legs6",801,280))
        elif state.endswith("penetration1"):
          rv.append(("nurse avatar events mc_service nurse_legs7",802,294))
        elif state.endswith(("penetration2","cum","aftermath")):
          rv.append(("nurse avatar events mc_service nurse_legs8",801,280))
        elif state.endswith("penetration3"):
          rv.append(("nurse avatar events mc_service nurse_legs9",799,274))
        elif state.endswith(("penetration4","shoulder_bite")):
          rv.append(("nurse avatar events mc_service nurse_legs10",802,264))
        if state.endswith("dick_reveal"):
          rv.append(("nurse avatar events mc_service mc_dick1",1528,643))
        elif state.endswith("rubbing1"):
          rv.append(("nurse avatar events mc_service mc_dick2",1532,598))
        elif state.endswith("rubbing2"):
          rv.append(("nurse avatar events mc_service mc_dick3",1533,569))
        elif state.endswith("rubbing3"):
          rv.append(("nurse avatar events mc_service mc_dick4",1514,594))
        elif state.endswith("rubbing4"):
          rv.append(("nurse avatar events mc_service mc_dick5",1538,567))
        elif state.endswith("penetration1"):
          rv.append(("nurse avatar events mc_service mc_dick6",1483,202))
        elif state.endswith("penetration2"):
          rv.append(("nurse avatar events mc_service mc_dick7",1454,165))
        elif state.endswith("penetration3"):
          rv.append(("nurse avatar events mc_service mc_dick8",1447,158))
        elif state.endswith("penetration4"):
          rv.append(("nurse avatar events mc_service mc_dick9",1442,0))
        elif state.endswith("shoulder_bite"):
          rv.append(("nurse avatar events mc_service mc_dick10",99,0))
        elif state.endswith("cum"):
          rv.append(("nurse avatar events mc_service mc_dick11",1454,165))
        elif state.endswith("aftermath"):
          rv.append(("nurse avatar events mc_service mc_dick12",1454,165))

      elif state.startswith("aftercare"):
        rv.append((1920,1080))
        rv.append(("nurse avatar events aftercare background",0,0))
        rv.append(("nurse avatar events aftercare mc_body",250,406))
        rv.append(("nurse avatar events aftercare nurse_body",64,0))
        rv.append(("nurse avatar events aftercare nurse_underwear",800,77))
        rv.append(("nurse avatar events aftercare nurse_clothes",20,0))

      return rv


  class Interactable_nurse(Interactable):
    def title(cls):
      return nurse.name

    def actions(cls,actions):
      ########talks###########
      if not (quest.nurse_venting == "stuck"
      or quest.kate_wicked == "costume"
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or (quest.nurse_aid == "hiking" and game.location == "school_park")
      or quest.nurse_aid == "shopping"):
        if not nurse.at("school_art_class","pose"):#hide if nurse is posing naked
          if nurse["strike_book_nude_office_today"] and nurse.at("school_nurse_room"):
            if game.season == 1:
              nurse.unequip("nurse_outfit")
              nurse.unequip("nurse_shirt")
              nurse.unequip("nurse_bra")
              nurse.unequip("nurse_pantys")
            elif game.season == 2:
              nurse.unequip("nurse_hat")
              nurse.unequip("nurse_dress")
              nurse.unequip("nurse_green_bra")
              nurse.unequip("nurse_green_panties")
          else:
            if game.season == 1:
              nurse.equip("nurse_outfit")
              nurse.equip("nurse_shirt")
              nurse.equip("nurse_bra")
              nurse.equip("nurse_pantys")
            elif (game.season == 2
            and not quest.nurse_aid == "hiking"):
              nurse.equip("nurse_hat")
              nurse.equip("nurse_dress")
              nurse.equip("nurse_green_bra")
              nurse.equip("nurse_green_panties")
          if nurse["talk_limit_today"]<3:
            actions.append(["talk","Talk","?nurse_talk_one"])
            actions.append(["talk","Talk","?nurse_talk_two"])
            actions.append(["talk","Talk","?nurse_talk_three"])
            actions.append(["talk","Talk","?nurse_talk_four"])
            actions.append(["talk","Talk","?nurse_talk_five"])
          else:
            actions.append(["talk","Talk","?nurse_talk_over"])
      #######################

      ##########Quests#########
      if mc["focus"]:
        if mc["focus"] == "nurse_venting":
          if quest.nurse_venting == "stuck":
            actions.append(["quest","Quest","?quest_nurse_venting_stuck"])
          elif quest.nurse_venting == "not_stuck":
            actions.append(["quest","Quest","?quest_nurse_venting_not_stuck"])
        elif mc["focus"] == "kate_trick":
          if quest.kate_trick == "workout" and game.location == "school_gym":
            actions.append(["quest","Quest","quest_kate_trick_workout"])
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume":
            actions.append(["quest","Quest","quest_kate_wicked_costume_admin_wing_nurse"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19:
            if "nurse" not in quest.isabelle_dethroning["dinner_conversations"]:
              actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_nurse"])
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "nurse_room":
            actions.append(["quest","Quest","quest_kate_moment_nurse_room"])
          elif quest.kate_moment == "nurse":
            actions.append(["quest","Quest","quest_kate_moment_nurse"])
      else:
        if nurse.at("school_art_class","pose") and game.location=="school_art_class":
          actions.append(["consume","\''Paint\''","jacklyn_quest_jacklyn_broken_fuse_model_nurse"])

        elif quest.isabelle_stolen == "askmaxine":
          actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_nurse"])

        elif not quest.kate_search_for_nurse == "meet_kate":
          if quest.jo_potted == "getnurse":
            actions.append(["quest","Quest","?quest_jo_pot_getnurse"])
          elif quest.jo_potted == "plowcow" and game.location == "school_forest_glade":
            actions.append(["quest","Quest","?quest_jo_pot_nurse_plow"])

          if quest.lindsey_wrong == "nurse":
            actions.append(["quest","Quest","?quest_lindsey_wrong_nurse"])

          if quest.kate_desire == "endnurse":
            actions.append(["quest","Quest","?quest_kate_desire_nurse_interact"])

          if quest.lindsey_piano == "nurse" and not quest.lindsey_piano["follow_me"]:
            actions.append(["quest","Quest","?quest_lindsey_piano_nurse"])

          if quest.jo_day == "nurse":
            if quest.jo_day["nurse_harvest"]:
              actions.append(["quest","Quest","quest_jo_day_nurse_harvest"])
            else:
              actions.append(["quest","Quest","quest_jo_day_nurse"])

          if quest.nurse_venting == "help" and game.location == "school_nurse_room":
            actions.append(["quest","Quest","?quest_nurse_venting_help"])
          elif quest.nurse_venting in ("supplies","ready"):
            actions.append(["quest","Quest","quest_nurse_venting_ready"])

          if quest.kate_trick == "nurse" and game.location == "school_nurse_room":
            actions.append(["quest","Quest","quest_kate_trick_nurse"])

          if quest.lindsey_angel == "nurse":
            actions.append(["quest","Quest","quest_lindsey_angel_nurse"])

          if quest.nurse_aid == "fishing" and game.location == "school_forest_glade":
            actions.append(["quest","Quest","quest_nurse_aid_fishing"])
          elif quest.nurse_aid == "bait" and game.location == "school_forest_glade":
            actions.append(["quest","Quest","?quest_nurse_aid_bait_nurse"])
          elif quest.nurse_aid == "hiking" and game.location == "school_park":
            actions.append(["quest","Quest","?quest_nurse_aid_hiking"])
          elif quest.nurse_aid == "aftermath" and game.day > quest.nurse_aid["date"]:
            actions.append(["quest","Quest","?quest_nurse_aid_aftermath"])
          elif quest.nurse_aid == "someone_else" and quest.nurse_aid["jacklyn_on_board"]:
            actions.append(["quest","Quest","?quest_nurse_aid_someone_else_meetup"])
          elif quest.nurse_aid == "relationship":
            actions.append(["quest","Quest","?quest_nurse_aid_relationship"])
          ######################

      #############flirts#########
      if not (quest.nurse_venting == "stuck"
      or quest.kate_wicked == "costume"
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or (quest.nurse_aid == "hiking" and game.location == "school_park")
      or quest.nurse_aid == "shopping"):
        if not nurse.at("school_art_class","pose"):#hide if nurse is posing naked
          if nurse["strike_book_nude_office_today"] and nurse.at("school_nurse_room"):
            if game.season == 1:
              nurse.unequip("nurse_outfit")
              nurse.unequip("nurse_shirt")
              nurse.unequip("nurse_bra")
              nurse.unequip("nurse_pantys")
            elif game.season == 2:
              nurse.unequip("nurse_hat")
              nurse.unequip("nurse_dress")
              nurse.unequip("nurse_green_bra")
              nurse.unequip("nurse_green_panties")
          else:
            if game.season == 1:
              nurse.equip("nurse_outfit")
              nurse.equip("nurse_shirt")
              nurse.equip("nurse_bra")
              nurse.equip("nurse_pantys")
            elif (game.season == 2
            and not quest.nurse_aid == "hiking"):
              nurse.equip("nurse_hat")
              nurse.equip("nurse_dress")
              nurse.equip("nurse_green_bra")
              nurse.equip("nurse_green_panties")
          if nurse["flirt_limit_today"]<3:
            if nurse["strike_book_nude_office_today"] and nurse.at("school_nurse_room"):
              actions.append(["flirt","Flirt","?nurse_office_nude_flirt_one"])
              actions.append(["flirt","Flirt","?nurse_office_nude_flirt_two"])
              actions.append(["flirt","Flirt","?nurse_office_nude_flirt_three"])
              actions.append(["flirt","Flirt","?nurse_office_nude_flirt_four"])
              actions.append(["flirt","Flirt","?nurse_office_nude_flirt_five"])
            else:
              actions.append(["flirt","Flirt","?nurse_flirt_one"])
              actions.append(["flirt","Flirt","?nurse_flirt_two"])
              actions.append(["flirt","Flirt","?nurse_flirt_three"])
              actions.append(["flirt","Flirt","?nurse_flirt_four"])
              actions.append(["flirt","Flirt","?nurse_flirt_five"])
          else:
            actions.append(["flirt","Flirt","?nurse_flirt_over"])
      ############


label nurse_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label nurse_talk_one:
  show nurse sad with Dissolve(.5)
  $nurse["talk_limit_today"]+=1
  nurse sad "I studied to become a doctor in med school, but had to change my focus."
  nurse sad "I'm not great with that kind of responsibility."
  hide nurse with Dissolve(.5)
  return

label nurse_talk_two:
  show nurse thinking with Dissolve(.5)
  $nurse["talk_limit_today"]+=1
  nurse thinking "I hope you're feeling okay, [mc]."
  nurse thinking "The flu season seems to be especially bad this year, and I only have one hospital bed."
  hide nurse with Dissolve(.5)
  return

label nurse_talk_three:
  show nurse smile with Dissolve(.5)
  $nurse["talk_limit_today"]+=1
  nurse smile "Working so closely with students is one of my great joys."
  nurse smile "It's a much less stressful atmosphere here than at the hospital."
  hide nurse with Dissolve(.5)
  return

label nurse_talk_four:
  show nurse neutral with Dissolve(.5)
  $nurse["talk_limit_today"]+=1
  nurse neutral "If you ever need someone to talk to, don't hesitate to stop by."
  nurse smile "Nurses also operate under the patient confidentiality laws."
  hide nurse with Dissolve(.5)
  return

label nurse_talk_five:
  show nurse neutral with Dissolve(.5)
  $nurse["talk_limit_today"]+=1
  nurse neutral "I always do my best to fill each student's needs."
  nurse annoyed "But sometimes it feels like I can't do enough."
  hide nurse with Dissolve(.5)
  return

label nurse_flirt_over:
  "She must be sick of my poor attempts at flirting by now..."
  return

label nurse_flirt_one:
  show nurse smile with Dissolve(.5)
  $nurse["flirt_limit_today"]+=1
  nurse smile "I am a bit of a stress-eater, not going to lie."
  nurse smile "Sucking on something always calms my nerves."
  nurse annoyed "Sorry, that was poor phrasing."
  hide nurse with Dissolve(.5)
  return

label nurse_flirt_two:
  show nurse thinking with Dissolve(.5)
  $nurse["flirt_limit_today"]+=1
  nurse thinking "I'm usually really tired when I get home at night."
  nurse blush "But if I can keep my eyes open, I'm partial to cooking shows."
  hide nurse with Dissolve(.5)
  return

label nurse_flirt_three:
  show nurse concerned with Dissolve(.5)
  $nurse["flirt_limit_today"]+=1
  nurse concerned "Rest is really important. I can see that you're not getting enough."
  nurse concerned "Try to get to bed in time tonight."
  hide nurse with Dissolve(.5)
  return

label nurse_flirt_four:
  show nurse sad with Dissolve(.5)
  $nurse["flirt_limit_today"]+=1
  nurse sad "Sorry, I've never been much of a talker."
  nurse sad "I like to listen, though. I think good listeners are underrated."
  hide nurse with Dissolve(.5)
  return

label nurse_flirt_five:
  show nurse neutral with Dissolve(.5)
  $nurse["flirt_limit_today"]+=1
  nurse neutral "Helping others gives me great pleasure."
  nurse annoyed "I'm probably too trusting sometimes..."
  hide nurse with Dissolve(.5)
  return

label nurse_office_nude_flirt_one:
  show nurse concerned with Dissolve(.5)
  $nurse["flirt_limit_today"]+=1
  nurse concerned "Gosh, I would prefer it if you looked away."
  nurse concerned "This is all too embarrassing."
  hide nurse with Dissolve(.5)
  return

label nurse_office_nude_flirt_two:
  show nurse annoyed with Dissolve(.5)
  $nurse["office_nude_flirt_limit_today"]+=1
  nurse annoyed "Wrapping myself in a bed sheet doesn't count as being naked, does it?"
  nurse annoyed "At least this humiliation is only for a day..."
  hide nurse with Dissolve(.5)
  return

label nurse_office_nude_flirt_three:
  show nurse blush with Dissolve(.5)
  $nurse["office_nude_flirt_limit_today"]+=1
  nurse blush "Goodness me. Where did I put the stethoscope?"
  mc "Maybe it's in your—"
  nurse afraid "No, no! It wouldn't fit!"
  nurse annoyed "Not that I've tried..."
  hide nurse with Dissolve(.5)
  return

label nurse_office_nude_flirt_four:
  show nurse thinking with Dissolve(.5)
  $nurse["office_nude_flirt_limit_today"]+=1
  nurse thinking "Don't you have classes to attend?"
  mc "Maybe, but this is more entertaining."
  nurse afraid "I... don't see how potentially exposing me to students and staff is entertaining!"
  mc "Are you sure?"
  nurse annoyed "..."
  hide nurse with Dissolve(.5)
  return

label nurse_office_nude_flirt_five:
  show nurse sad with Dissolve(.5)
  $nurse["office_nude_flirt_limit_today"]+=1
  nurse sad "How am I supposed to do my job now?"
  mc "Think of it as a brief vacation."
  nurse thinking "I... don't find this very relaxing."
  mc "Who said anything about relaxing?"
  nurse concerned "I thought the two were related somehow..."
  mc "Well, your vacations from now on will be anything but relaxing."
  nurse afraid "Goodness gracious."
  hide nurse with Dissolve(.5)
  return
