
image kate_intrigue_mistress = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-273,-74),Transform(LiveComposite((1920,1080),(-275,0),"school gym gym",(0,349),"school gym bleachers",(745,289),"school gym window_right",(625+117,0+99),"kate avatar body1",(625+229,0+208),"kate avatar face_confident",(625+224,0+377),"kate avatar b1cheerleader_bra",(625+28,0+202),"kate avatar b1mask_n",(625+198,0+372),"kate avatar b1cheerleader_top",(625+100,0+391),"kate avatar b1mask_cheerleader"),size=(768,432))),"ui circle_mask"))


label quest_kate_intrigue_start:
  show isabelle subby with Dissolve(.5)
  if kate.location == game.location:
    show isabelle subby at move_to(.25)
    with None
    show isabelle blush
    show kate flirty at Transform(xalign=.75)
    with Dissolve(.5)
  else:
    show isabelle subby:
      ease 1.0 xalign 0.25
    show kate flirty at appear_from_right(.75)
    show isabelle blush with Dissolve(.5)
  kate flirty "Hey, you!"
  isabelle blush "H-hi..."
  kate flirty "I've been looking for you."
  isabelle blush "Oh? How come?"
  kate flirty "I wanted to give you something."
  isabelle blush "What's—"
  window hide
  show kate flirty at move_to(.25,1.0)
  with None
  show kate isabelle_kiss
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Whoa... I didn't expect that..."
  "In fact, I didn't expect any of this."
  "I thought that they were somehow messing with me."
# "I seriously can't believe that [isabelle] would date [kate] after what she put her through."
  "I seriously can't believe that [isabelle] would date [kate] after what she{space=-5}\nput her through."
  "I thought there would be a huge lawsuit."
  "And even as the scene unfolds before my very eyes..."
  "...as the lipsticks of opposite worlds meet..."
  "...as vanilla frosting meets icy mint..."
  "...as cinnamon meets rose thorn..."
  "...I struggle to believe that this is real."
  "Surely, [isabelle]'s dad would've crushed [kate] in court?"
  "Unless there's something preventing legal action..."
  "..."
  "[kate] did say she'd have an airtight alibi, so maybe that's it?"
  "But that still doesn't explain why they're kissing and being friendly..."
  if quest.nurse_aid["name_reveal"]:
    # "Perhaps there's also a blackmail angle? [kate] certainly didn't have any issues doing that to [amelia]."
    "Perhaps there's also a blackmail angle? [kate] certainly didn't have any{space=-35}\nissues doing that to [amelia]."
  else:
    # "Perhaps there's also a blackmail angle? [kate] certainly didn't have any issues doing that to the [nurse]."
    "Perhaps there's also a blackmail angle? [kate] certainly didn't have any{space=-35}\nissues doing that to the [nurse]."
  "And honestly, I didn't think [kate] would take it this far with [isabelle]..."
  $unlock_replay("kate_girlfriend")
  window hide
  show kate flirty at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  if kate.location == game.location:
    hide isabelle
    hide kate
    with Dissolve(.5)
  else:
    show isabelle blush:
      ease 1.0 xalign 0.5
    show kate flirty at disappear_to_right
    pause 1.0
    hide isabelle with Dissolve(.5)
  window auto
  menu(side="middle"):
    "\"Guilt has been eating away at me...\"":
      "Guilt has been eating away at me..."
      "...and it's partially my fault that [isabelle] is in this position now."
      "I should probably try to help her, even though she hates my guts."
      $quest.kate_intrigue["guilty"] = True
#   "\"I need to make sure that [kate]'s control over [isabelle] is solid.\"":
    "\"I need to make sure that [kate]'s\ncontrol over [isabelle] is solid.\"":
      "I need to make sure that [kate]'s control over [isabelle] is solid."
#     "If she gets into legal trouble, she would likely throw me under the bus."
      "If she gets into legal trouble, she would likely throw me under the bus.{space=-50}"
      "Maybe if I help her keep [isabelle] in check, she'll let me watch and participate again?"
      "I should probably try to learn what [isabelle] is thinking..."
  window hide
  $quest.kate_intrigue.start()
  return

label quest_kate_intrigue_private_chat_wrong_location:
  "Hmm... I better talk to her in private."
  "I don't want [kate] or any of her cronies to see us together."
  $quest.kate_intrigue["us_together"] = True
  return

label quest_kate_intrigue_private_chat_advance_time:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5

label quest_kate_intrigue_private_chat_upon_entering:
  "Okay, this is probably one of the classrooms that [kate] is least likely to visit."
  "Perfect for a private chat with [isabelle]."
  $quest.kate_intrigue["least_likely"] = True
  return

label quest_kate_intrigue_private_chat:
  show isabelle annoyed with Dissolve(.5)
  isabelle annoyed "What the hell do you want?"
  if quest.kate_intrigue["guilty"]:
    show isabelle annoyed at move_to(.75)
    menu(side="left"):
      extend ""
      "\"Hear me out, okay?\"":
        show isabelle annoyed at move_to(.5)
        mc "Hear me out, okay?"
        isabelle annoyed "You have five seconds, then I'm leaving."
        mc "You've changed... and I guess I'm worried about you."
        isabelle angry "Worried? You're bloody worried?!"
        isabelle angry "That's brilliant, mate!"
        "Some of that fire clearly still burns inside her."
        "Maybe [kate] didn't break her as thoroughly as she thought?"
  #   "?isabelle.love>=6@[isabelle.love]/6|{image=isabelle contact_icon}|{image=stats love}|\"I came to apologize.\"":
      "?isabelle.love>=6@[isabelle.love]/6|{image=isabelle contact_icon}|{image=stats love}|\"I came to\napologize.\"":
        show isabelle annoyed at move_to(.5)
        mc "I came to apologize."
        isabelle cringe "Apologize?"
        mc "Yeah, for betraying you."
        isabelle cringe "Don't you mean, for drugging me?"
        isabelle cringe "And watching me get degraded and abused?"
        if quest.kate_stepping["fuck_isabelle_too"]:
          isabelle cringe "And for fucking my arse?"
        isabelle cringe "And then going missing for two weeks straight?"
        isabelle cringe "Is that what you came to apologize for?"
        mc "It does sound horrible when you put it like that..."
        isabelle sad "Yeah, it does. I trusted you."
        mc "I know. If I could take it back, I would."
        mc "No one deserves to be treated like that, and it's eating me up."
        mc "I truly am sorry for my part in this."
        isabelle sad "Nothing you can say will change anything."
        isabelle sad "But... I suppose I appreciate the gesture."
        $quest.kate_intrigue["apologized"] = True
  else:
    mc "Hear me out, okay?"
    isabelle annoyed "You have five seconds, then I'm leaving."
    mc "You've changed... and I guess I'm worried about you."
    isabelle angry "Worried? You're bloody worried?!"
    isabelle angry "That's brilliant, mate!"
    "Some of that fire clearly still burns inside her."
    "Maybe [kate] didn't break her as thoroughly as she thought?"
  mc "Can I ask you why you're not pressing charges?"
  isabelle sad "As it turns out, [kate]'s friends are extremely loyal."
  isabelle sad "And they all vouch for her being at some party when it happened."
  mc "I mean... surely, someone—"
  isabelle sad "No one. Everyone's sticking to their stories."
  isabelle sad "It's like she was actually there, and I just imagined all of it."
# mc "Well, there must be surveillance footage, right? I could ask [maxine] to—"
  mc "Well, there must be surveillance footage, right? I could ask [maxine] to—{space=-65}"
  isabelle sad "I already did. There's nothing."
  isabelle sad "I've tried everything."
  if quest.kate_intrigue["apologized"]:
    mc "And so, what, you decided to become her girlfriend instead?"
    mc "How does that make any sense?"
    isabelle concerned "A lot happened during the fall break."
    mc "What do you mean?"
    mc "Is she blackmailing you?"
    isabelle concerned_left "Not exactly..."
    "[isabelle] looks shifty, nervous, glancing over her shoulder."
    isabelle concerned_left "I asked her."
    mc "You did what?"
#   isabelle concerned "I asked her to be my girlfriend."
    isabelle concerned "{i}I{/} asked {i}her{/} to be my girlfriend."
    mc "But why?"
    isabelle concerned "It's... complicated."
    mc "You can tell me."
    isabelle skeptical "I already trusted you once, remember?"
    mc "I know, and you have every right to doubt me."
    mc "But I swear I just want to make things right."
    isabelle skeptical "..."
    isabelle concerned "I guess you do look sincere."
    mc "I am. I made a huge mistake, and I regret it deeply."
    mc "Please, let me make it up to you."
    isabelle concerned_left "I don't know..."
    mc "Please, [isabelle]. It's eating me up."
    isabelle concerned "Fine, but you can't tell anyone, okay?"
    mc "You have my word."
    isabelle concerned "Okay..."
    isabelle concerned "Like I said, a lot of things happened during the fall break."
#   mc "How about you start with what happened right after the, err... incident in the gym?"
    mc "How about you start with what happened right after the, err... incident{space=-40}\nin the gym?"
    isabelle sad "[kate] walked me around the school on my elbows and knees."
    isabelle sad "She took me back to all the places where I'd argued with her..."
    isabelle sad "...and made me kiss her shoes and apologize for everything I'd said."
    mc "Yikes. I'm really sorry."
    isabelle sad "We then went to her place, and she dominated me for hours."
    mc "It takes a lot of willpower to stand up to her in general."
    mc "Being at her complete mercy must've been... soul-crushing."
    isabelle annoyed_left "She really did her best to break me."
    isabelle annoyed_left "She put me through all sorts of humiliations."
    isabelle annoyed_left "She tried to make me like it. Tried to turn me into a willing pet."
    isabelle annoyed_left "And it almost worked."
    mc "...almost?"
    isabelle neutral "I put on a show for her."
    isabelle neutral "She bought this collar. I let her put it on me."
    mc "But why?"
#   isabelle concerned "At first, I just wanted her to let me go. But then I realized something."
    isabelle concerned "At first, I just wanted her to let me go. But then I realized something.{space=-15}"
    isabelle concerned "[kate] is a very troubled individual."
    mc "Don't you mean, \"evil?\""
#   isabelle concerned "I don't think she's evil. She's driven by her needs and urges, just like everyone else."
    isabelle concerned "I don't think she's evil. She's driven by her needs and urges, just like{space=-15}\neveryone else."
    isabelle skeptical "Only hers are a bit... different."
    isabelle skeptical "I figured if I can be what she's looking for, she'll leave everyone else alone."
    "Becoming her sub so that she stops bullying others?"
    "That's certainly an outside-the-box solution."
    mc "There's only one problem with your theory..."
    if quest.nurse_aid["name_reveal"]:
      # mc "Did you know that she's had the nurse all along? That didn't change anything."
      mc "Did you know that she's had the nurse all along? That didn't change{space=-10}\nanything."
    else:
      # mc "Did you know that she's had the [nurse] all along? That didn't change anything."
      mc "Did you know that she's had the [nurse] all along? That didn't change{space=-10}\nanything."
    isabelle concerned "I didn't know that, no. But it's not the same."
    mc "What do you mean?"
    if quest.nurse_aid["name_reveal"]:
      isabelle concerned "The nurse probably wasn't what she was looking for."
    else:
      isabelle concerned "The [nurse] probably wasn't what she was looking for."
    mc "Okay, now I'm really confused."
    if quest.nurse_aid["name_reveal"]:
      isabelle concerned_left "The nurse seems a bit, err... how do I put this?"
    else:
      isabelle concerned_left "The [nurse] seems a bit, err... how do I put this?"
    mc "Easy?"
    isabelle concerned "I was going to say, \"agreeable.\""
    isabelle concerned "I think [kate] needs someone that challenges her."
    isabelle concerned "Dominating me is a lot more fun to her."
    mc "I guess I can see that."
  else:
    mc "Damn. She's got you good, doesn't she?"
    isabelle subby "She does."
    mc "Making you wear a collar in public and everything..."
    isabelle subby "Did you just come here to gloat?"
    mc "I'm just curious as to why you agreed to let it go on."
    isabelle subby "I didn't have much of a choice, did I?"
    show isabelle subby at move_to(.25)
    menu(side="right"):
      extend ""
      "?mc.intellect>=8@[mc.intellect]/8|{image=stats int}|\"Are you saying she's blackmailing you?\"":
        show isabelle subby at move_to(.5)
        mc "Are you saying she's blackmailing you?"
        isabelle concerned "Yes. She has all sorts of compromising photos."
        mc "I don't believe you."
        isabelle skeptical "What do you mean? I know she even sent you one."
        mc "No, I'm sure she has plenty of indecent photos of you..."
        mc "...I just don't believe that's what's keeping you under her heel."
        mc "You don't strike me as a person that would let something like a few nudes get in the way of justice."
        isabelle neutral "I guess you're a lot smarter than I thought."
        mc "What can I say? I'm not just a pretty face."
        mc "So, what's really going on?"
        isabelle neutral "I'm good at acting."
      "\"Well, being a sub suits you.\"":
        show isabelle subby at move_to(.5)
        mc "Well, being a sub suits you."
        mc "Maybe she'll let me have you when she gets bored?"
        isabelle cringe "That's not how it works..."
        mc "Are you sure?"
        isabelle afraid "Yes, I'm bloody sure!"
        mc "I think [kate] decides how it works."
        mc "And I don't think you have much say."
        isabelle annoyed "..."
        mc "If [kate] tells you to get on your knees and suck my cock, you don't have much of a choice, do you?"
        isabelle angry "You bloody pig! You have no idea what you're talking about!"
        "Oh? It seems like I hit a nerve."
        mc "Why don't you enlighten me, then?"
        isabelle angrier "I'm not doing this for your entertainment or hers!"
        isabelle angrier "I'm my own goddamn person, with freewill and choices!"
#       isabelle angrier "And if I can't fix her, I'll make sure she never hurts anyone ever again!"
        isabelle angrier "And if I can't fix her, I'll make sure she never hurts anyone ever again!{space=-40}"
        window hide
        show isabelle angrier at disappear_to_right(.75)
        $isabelle["at_none_this_scene"] = True
        pause 0.75
        play sound "<from 9.2 to 10.2>door_breaking_in"
        show location with hpunch
        window auto
        "Well, that explains a lot of things."
        "I knew something was going on beneath the surface."
        "Someone like [isabelle] doesn't just roll over..."
#       "...and it's very much like her to try to fix [kate] in spite of what happened."
        "...and it's very much like her to try to fix [kate] in spite of what happened.{space=-80}"
        $quest.kate_intrigue["sub_suits_you"] = True
  if not quest.kate_intrigue["sub_suits_you"]:
    isabelle neutral "Also, I think I can fix her."
    mc "Are you serious? Fix her how?"
    isabelle neutral "I think I can minimize her bad traits, and maximize her good ones."
    mc "You can't fix people."
    isabelle skeptical "Are you sure? I believe in second chances."
    "Welp. It's hard for me to argue with that."
    mc "Maybe you're right. And maybe you can fix her."
    mc "But what if you can't?"
    isabelle neutral "I did think of that."
    isabelle neutral "Being this close to her, I know everything she's doing and more."
    isabelle neutral "So, if she truly can't be fixed... I'm going to take her down for good."
    mc "Damn. You've really thought this through."
#   isabelle skeptical "I have, and by Christmas, she will either be a changed person, or gone for good."
    isabelle skeptical "I have, and by Christmas, she will either be a changed person, or gone{space=-45}\nfor good."
    mc "That is one hell of a statement."
    isabelle neutral "I suppose so, but I believe I can pull it off."
    mc "If anyone can, it's you."
    isabelle blush "Thank you, [mc]."
    isabelle blush "I need to get going now. I'll see you around."
    window hide
    show isabelle blush at disappear_to_right
    $isabelle["at_none_this_scene"] = True
    pause 0.5
    window auto
    "Well, [isabelle] certainly still has the confidence..."
    "...and this leaves me with a choice."
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  menu(side="middle"):
    "{image= kate contact_icon}|Tell [kate]":
      "Unfortunately for [isabelle], I'm still playing for team [kate]."
      if quest.kate_intrigue["sub_suits_you"]:
        "And as always, her emotions got the better of her."
        "She spilled the beans, and now she'll be in an even tougher spot."
      else:
        "But it's really her own fault for trusting me with her plan."
        "She should know better by now."
      "Time to tell the queen about this little ploy."
      $quest.kate_intrigue.advance("snitch")
    "{image= isabelle contact_icon}|Help [isabelle]":
      "If [isabelle] is going to pull this off, she'll need help."
      if quest.kate_intrigue["sub_suits_you"]:
#       "She's in a tough spot, and even though she exploded on me, I probably deserved it."
        "She's in a tough spot, and even though she exploded on me, I probably{space=-60}\ndeserved it."
      else:
        "[kate] really shouldn't be underestimated."
      "I better keep an eye on them for a while."
      window hide
      $quest.kate_intrigue.fail()
  return

label quest_kate_intrigue_snitch:
  show kate confident with Dissolve(.5)
  kate confident "Well, well, well... if it isn't my partner in crime."
  mc "No belittlement? Are you okay?"
  kate laughing "What do you mean? Why would I do that?"
  show kate laughing at move_to(.75)
  menu(side="left"):
    extend ""
#   "\"It's good that you finally give me the respect I deserve.\"":
    "\"It's good that you finally give me\nthe respect I deserve.\"":
      show kate laughing at move_to(.5)
      mc "It's good that you finally give me the respect I deserve."
      $kate.lust-=2
      kate neutral "Now you're just playing with fire."
#     mc "I've been burned so many times that fire doesn't bother me anymore."
      mc "I've been burned so many times that fire doesn't bother me anymore.{space=-20}"
      kate neutral "So, what do you want?"
    "\"To make you feel good about yourself?\"" if quest.kate_intrigue["apologized"]:
      show kate laughing at move_to(.5)
      mc "To make you feel good about yourself?"
      kate smile "I only need to look in your direction for that."
      mc "Interesting..."
      kate smile "What is?"
#     mc "I'm just curious what would happen if I told [isabelle] you're giving me crap again."
      mc "I'm just curious what would happen if I told [isabelle] you're giving me{space=-10}\ncrap again."
      kate eyeroll "Why would anything happen?"
#     mc "Are you telling me that she didn't ask you to stop bullying other people?"
      mc "Are you telling me that she didn't ask you to stop bullying other people?{space=-85}"
      $kate.lust-=1
      kate skeptical "..."
      mc "So, it's true."
      kate skeptical "What do you want?"
#   "\"It doesn't feel right when you don't, ma'am.\"":
    "\"It doesn't feel right when you don't, ma'am.\"{space=-35}":
      show kate laughing at move_to(.5)
      mc "It doesn't feel right when you don't, ma'am."
      $kate.lust+=1
      kate flirty "I knew you always liked it, you little pervert."
      mc "It's not—"
      kate neutral "Oh, cut the crap, [mc]."
      kate neutral "You've always enjoyed when I walk all over you."
      mc "Maybe... but that's not why I came here."
      kate neutral "What do you want, then?"
  mc "I have some information you might be interested in."
  kate skeptical "What sort of information?"
  mc "The highly sensitive and secret kind."
  kate excited "Out with it, then."
  mc "Not so fast!"
  mc "I want something in return for it."
  kate skeptical "Well, it better be some good information, in that case."
  mc "Oh, trust me. You want to know this."
  kate eyeroll "Fine, I'll give you what you want. Within reason."
  mc "Sounds good. Okay, listen closely."
  mc "You're currently getting played big time."
  kate thinking "Played? By whom?"
  mc "Turns out, your new girlfriend isn't quite as docile as she seems."
  kate thinking "What are you talking about?"
  mc "You didn't break her, and she's trying to take you down."
  kate thinking "How do you know this?"
  mc "Because she told me."
  kate thinking "Why would she do that?"
  if quest.kate_intrigue["sub_suits_you"]:
    mc "Let's just say I pushed all her right buttons."
    mc "She then exploded on me and admitted to everything in anger."
  else:
    mc "Maybe she thinks I'm on her side, in spite of everything?"
    mc "Maybe she likes me?"
    mc "Either way, she sounded serious about it."
  kate thinking "Hmm... I did find it a little suspicious when she asked me out..."
# kate blush "I know I'm amazing, but I didn't expect that from her."
  kate blush "I know I'm amazing, but I didn't expect {i}that{/} from her."
  mc "Well, now you know."
  kate annoyed "That little bitch!"
  kate annoyed "It's good that you told me."
  if kate.lust >= 3:
    mc "Anything for the queen!"
    kate eyeroll "Oh, please. You just wanted a reward."
  else:
    mc "For the right price, anything."
  kate eyeroll "Why can't you ever do anything out of the goodness of your heart?"
  kate eyeroll "So, what did you have in mind?"
  show kate eyeroll at move_to(.25)
  menu quest_kate_intrigue_snitch_repeat(side="right"):
    extend ""
    "\"I want to fuck you.\"" if not quest.kate_intrigue["taken_seriously"]:
      show kate eyeroll at move_to(.5)
      mc "I want to fuck you."
      kate laughing "Duh. You and everyone else."
      mc "I'm being serious."
      kate laughing "You're being funny."
      if kate.lust >= 8:
        mc "..."
        kate neutral "Sorry, your dick isn't going anywhere near me."
#       kate neutral "But... I guess I could fuck you."
        kate neutral "But... I guess {i}I{/} could fuck {i}you.{/}"
        mc "What? How would that even work?"
        kate confident "I have my ways."
        "She's probably talking about pegging me, isn't she?"
        show kate confident at move_to(.75)
        menu(side="left"):
          extend ""
          "\"I'm not interested.\"":
            show kate confident at move_to(.5)
            mc "I'm not interested."
            kate laughing "Are you blushing? That's adorable."
            mc "Not as much as you'll be when the time comes."
            kate smile "Oh, really? When what time comes?"
            mc "You know what I'm talking about."
            mc "I'm going to fuck you before Christmas. That's a promise."
            kate smile "Do you want to make a bet?"
            mc "Let's do it."
            "No hesitation. If I show fear now, it's over."
            mc "What do I get if I win?"
            kate laughing "You're not going to win."
            mc "We'll see about that."
            kate skeptical "So, what do you want?"
            mc "I want you to suck my dick in front of the entire school."
            kate eyeroll "Of course it's something perverted..."
            kate excited "If I win, you're getting a tattoo of my choice."
            "Ugh, that's going to say something humiliating, isn't it?"
            mc "Err, deal..."
            kate excited "You don't sound so sure."
            mc "I'm sure, okay?"
            kate excited "Well, then! It's official."
            window hide
            $kate["christmas_challenge_active"] = True
            $game.notify_modal(None,"Against All Odds","Challenge: Fuck [kate]\nbefore Christmas.",wait=5.0)
            pause 0.25
            window auto show
            show kate annoyed_right with dissolve2
          "\"Fine. Let's do it.\"":
            show kate confident at move_to(.5)
            mc "Fine. Let's do it."
            kate laughing "Are you sure?"
            mc "Yes, I'm sure. Name the time and place."
            kate smile "All right, tough guy. My house after school."
            "Err, she's being serious, isn't she?"
            "Welp. It's too late to back out now."
            mc "Very well. I'll see you then."
            kate neutral "Don't you dare pussy out."
            $quest.kate_intrigue["tough_guy"] = True
            $quest.kate_intrigue["after_school"] = True
      else:
        "Ugh, I hate not being taken seriously..."
        kate smile "So, what did you actually have in mind?"
        show kate smile at move_to(.25)
        $quest.kate_intrigue["taken_seriously"] = True
        jump quest_kate_intrigue_snitch_repeat
#   "?quest.kate_stepping['mistress']@|{image=kate_intrigue_mistress}|\"I want you to make good on your promise.\"":
    "?quest.kate_stepping['mistress']@|{image=kate_intrigue_mistress}|\"I want you to make good\non your promise.\"":
      if renpy.showing("kate eyeroll"):
        show kate eyeroll at move_to(.5)
      elif renpy.showing("kate smile"):
        show kate smile at move_to(.5)
      mc "I want you to make good on your promise."
      kate thinking "And what promise is that?"
      mc "I asked you to be my mistress, remember?"
#     mc "You said it wouldn't happen overnight, but now I've proven myself to you again."
      mc "You said it wouldn't happen overnight, but now I've proven myself to{space=-10}\nyou again."
      kate thinking "I guess you have..."
      mc "..."
      kate blush "Fine. I'll give you an audition."
      mc "Thank you, ma'am!"
#     "Oh, man. This is both going to suck and probably be the most amazing thing ever."
      "Oh, man. This is both going to suck and probably be the most amazing{space=-55}\nthing ever."
      "I've always wanted to worship someone like [kate]."
      kate blush "Stop by my house after school."
      mc "Err, your house?"
      kate embarrassed "Is that a problem?"
      mc "No, ma'am! I'll be there!"
      kate blush "Good boy."
      $quest.kate_intrigue["after_school"] = True
    "\"I want to help you with [isabelle].\"":
      if renpy.showing("kate eyeroll"):
        show kate eyeroll at move_to(.5)
      elif renpy.showing("kate smile"):
        show kate smile at move_to(.5)
      mc "I want to help you with [isabelle]."
      kate blush "You're worse than Judas, you know that?"
      mc "I don't care. I enjoy watching you step on her."
      mc "She was born to be a sub."
      kate blush "Ain't that the truth!"
      kate blush "Fine. It's a deal."
      mc "Thanks! Okay, tell me what you need."
      kate thinking "I guess I need to find out what exactly she's planning."
      kate thinking "Maybe you can try to get it out of her?"
      show kate thinking at move_to(.75)
      $moments_of_glory = mc["moments_of_glory"]
      menu(side="left"):
        extend ""
        "?mc['moments_of_glory']>=1@[moments_of_glory]/1 Moments\n{space=31}of Glory|\"I'll bury her.\"":
          show kate thinking at move_to(.5)
          mc "I'll bury her."
          kate neutral "I'll be the only one doing anything to her. She's mine."
          mc "..."
          kate neutral "What's your problem with her, anyway?"
#         mc "She's a self-righteous stuck-up bitch. What's your problem with her?"
          mc "She's a self-righteous stuck-up bitch. What's {i}your{/} problem with her?{space=-15}"
          kate laughing "I guess we feel the same about her."
          mc "I guess we do."
          kate smile "Very well. Let's do it together, then."
          mc "Great! I'll let you know what I find out."
          if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_unity"]:
            window hide
            $mc["moments_of_glory"]+=1
            $mc["moments_of_glory_unity"] = True
            $game.notify_modal(None,"Guts & Glory","Moments of Glory: Unity\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#           if mc["moments_of_glory"] == 3:
#             $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
            pause 0.25
            window auto show
            show kate annoyed_right with dissolve2
        "\"I'm at your service, ma'am.\"":
          show kate thinking at move_to(.5)
          mc "I'm at your service, ma'am."
          kate confident "That's the attitude I like."
          mc "Where would you like me to start?"
          kate confident "I want you to find out what her plan is."
          mc "I can do that, ma'am."
          $kate.lust+=2
          kate confident "Good boy."
  kate annoyed_right "Now, I have a treacherous little snake to take care of."
  "Oh, boy. [isabelle] sure is in big trouble."
  mc "What's your plan?"
  kate skeptical "I don't know yet what I'm going to do about her little ploy..."
  kate skeptical "...but I do know I'm going to start by punishing her."
  mc "Won't that give away that you know about it?"
# kate excited "No, she's used to getting punished. She'll take it like a good little bitch."
  kate excited "No, she's used to getting punished. She'll take it like a good little bitch.{space=-60}"
# kate excited "And the best part is that she'll pretend to like it, but now I know that she doesn't."
  kate excited "And the best part is that she'll pretend to like it, but now I know that{space=-10}\nshe doesn't."
  kate excited "Oh, I'm really going to put her acting skills to the test!"
  window hide
  show kate excited at disappear_to_right
  $kate["at_none_this_scene"] = True
  pause 0.5
  window auto
  if kate["christmas_challenge_active"]:
    "And I'm done with [kate]'s and [isabelle]'s drama."
    "I should instead keep my eyes on the prize, which is [kate]'s pussy."
    window hide
    $quest.kate_intrigue.fail("failed_kate_bet")
    return
  elif quest.kate_intrigue["after_school"]:
    "Well, my work here is done."
    "Now I just need to wait until school ends to collect my reward at her house."
    "..."
    "Shit. What have I gotten myself into?"
  else:
    "I guess I'll have to wait for [kate] to be done with her before I start my little investigation..."
    if quest.kate_intrigue["sub_suits_you"]:
      "...and I'll have to be sneaky, too. [isabelle] probably won't talk to me after I provoked her."
  $quest.kate_intrigue.advance("wait")
  return

label quest_kate_intrigue_eavesdrop_school_cafeteria:
# "Hmm... eating with the fork in her left hand seems pretty suspicious..."
  "Hmm... eating with the fork in her left hand seems pretty suspicious...{space=-35}"
  "...but it's ultimately nothing that will bring down [kate]."
  $quest.kate_intrigue["eavesdropped_locations"].add("school_cafeteria")
  window hide
  return

label quest_kate_intrigue_eavesdrop_school_first_hall:
  "She's looking annoyed... but that's not really out of the ordinary."
  "That collar looks good on her, though."
  $quest.kate_intrigue["eavesdropped_locations"].add("school_first_hall")
  window hide
  return

label quest_kate_intrigue_eavesdrop_school_gym:
  "It seems like she has come to support her new girlfriend... while secretly plotting her downfall."
  "Twisted, but nothing that tells me what she has planned."
  $quest.kate_intrigue["eavesdropped_locations"].add("school_gym")
  window hide
  return

label quest_kate_intrigue_eavesdrop_school_ground_floor:
  show isabelle skeptical with Dissolve(.5)
  isabelle skeptical "..."
  "Crap. She almost caught me following her."
  "I need to be more careful."
  isabelle concerned_left_phone "Hey, I just wanted to let you know that I miss you."
  isabelle concerned_left_phone "I still think about you all the time."
  isabelle concerned_left_phone "And... if you were here... I know you would understand."
  isabelle sad_phone "That's what I miss the most. Not having you with me."
  isabelle sad_phone "But I know you watch over me, and that's why I keep fighting."
  isabelle sad_phone "I love you, sis."
  window hide
  show isabelle sad with Dissolve(.5)
  show isabelle sad at disappear_to_left
  pause 0.5
  window auto
  "That was... not what I expected."
  "Leaving voicemails for her dead sister?"
  "Jesus..."
  $quest.kate_intrigue["eavesdropped_locations"].add("school_ground_floor")
  return

label quest_kate_intrigue_eavesdrop_school_english_class:
  show isabelle neutral with Dissolve(.5)
  "Bingo! I knew I'd find her here."
  "This is like her safe space."
  window hide
  play sound "notification" volume 0.5
  pause 0.5
  show isabelle concerned_left_phone with Dissolve(.5)
  window auto
  isabelle concerned_left_phone "Hey, dad."
  isabelle concerned_left_phone "..."
  isabelle concerned_left_phone "Did you find anything on... you know what?"
  isabelle concerned_left_phone "..."
  isabelle subby_phone "No, it's just that... [kate] has escalated things..."
  isabelle subby_phone "So, I suppose I'm more in a hurry."
  isabelle subby_phone "..."
  isabelle sad_phone "No, no, I'm okay! Don't worry about that."
  isabelle sad_phone "Just... please tell me you've found something in those files."
  isabelle sad_phone "..."
  isabelle concerned_left_phone "What? Nothing at all?"
  isabelle concerned_left_phone "..."
  isabelle annoyed_left_phone "Bollocks."
  isabelle annoyed_left_phone "..."
  isabelle annoyed_left_phone "I'll send you more pictures the next time I'm over there."
  isabelle annoyed_left_phone "There has to be something shady in those files."
  isabelle annoyed_left_phone "..."
  isabelle sad_phone "I will. Love you too, dad."
  window hide
  show isabelle sad with Dissolve(.5)
  hide isabelle with Dissolve(.5)
  $isabelle["reading_this_scene"] = True
  window auto
  "Okay, that's suspicious, all right."
  "It's probably worth reporting to [kate]."
  $quest.kate_intrigue.advance("report")
  return

label quest_kate_intrigue_spy:
  show isabelle subby with Dissolve(.5)
  "Oh, man. She looks all sorts of bashful and embarrassed."
  "[kate] must've punished her already."
  mc "Hey, how's it going?"
  isabelle subby "Err... it's... ehm..."
  "It's not often you see [isabelle] this flustered."
  mc "Are you okay?"
  isabelle subby "Y-yes. I'm... brilliant. Thank you."
  show isabelle subby at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You seem a bit flustered.\"":
      show isabelle subby at move_to(.5)
      mc "You seem a bit flustered."
      isabelle afraid "What? No, I'm not!"
      $mc.lust+=1
      mc "Your cheeks are so red, though."
      mc "Maybe you have a fever?"
      isabelle thinking "No... I, err... I'm just a bit winded, that's all."
      mc "Did you run laps in the gym?"
      isabelle cringe "Something like that..."
    "\"Do you need aloe vera?\"":
      show isabelle subby at move_to(.5)
      mc "Do you need aloe vera?"
      isabelle cringe "W-what?"
      mc "It's good for your skin."
      isabelle thinking "Oh, um... yes, that would be nice."
      mc "It's especially nice on welts."
      isabelle afraid "I, err... I need to go!"
      mc "Oh, come on, [isabelle]."
      mc "You're a sub. You get punished. It's nothing to be ashamed of."
      if quest.nurse_aid["name_reveal"]:
        mc "I know there's aloe vera gel in the nurse's office."
      else:
        mc "I know there's aloe vera gel in the [nurse]'s office."
      $isabelle.love+=1
      isabelle flirty "Err, thank you."
  mc "So, anyway, I was wondering what your plan to take [kate] down is?"
  isabelle afraid "Shh! Keep your voice down!"
  mc "Sorry! I'm just excited to know you're still fighting the good fight."
  isabelle thinking "I have a few things I'm working on..."
  mc "That's not vague at all."
  isabelle subby "..."
  "She looks over her shoulder and then lowers her voice to a whisper."
  isabelle subby "[kate] leaves me alone at her house from time to time."
  isabelle subby "So, I've been going through her dad's files..."
  isabelle subby "And if he's anything like her, I'm sure I'll find some shady dealings sooner or later."
  mc "That is ingenious."
  isabelle concerned "I'll try to change her first, though."
  isabelle skeptical "But if that doesn't work, I'm taking her entire family down."
  mc "You're serious about this, aren't you?"
  isabelle skeptical "Of course I'm bloody serious."
  mc "Okay, well, if you need help somehow, just let me know."
  isabelle neutral "Thank you. I have some other things planned, as well."
  isabelle neutral "I just need to find out more about her father first."
  isabelle concerned "I'll also need you as a witness, because I'm not ready to let my humiliation at her hands go."
  mc "You're right. You can rely on me."
  isabelle excited "Thank you, [mc]!"
  window hide
  hide isabelle with Dissolve(.5)
  $isabelle["reading_this_scene"] = True
  window auto
# "Unfortunately for her, I'm going to have to twist the knife in her back a bit more..."
  "Unfortunately for her, I'm going to have to twist the knife in her back{space=-15}\na bit more..."
  $quest.kate_intrigue.advance("report")
  return

label quest_kate_intrigue_report:
  show kate confident_phone with Dissolve(.5)
  kate confident_phone "No, you don't have permission to touch yourself. Naughty girl."
  kate confident_phone "..."
  kate laughing_phone "It seems like you enjoyed the extra harsh punishment, then?"
  kate laughing_phone "..."
  kate flirty_phone "That's what I thought. You little pain slut."
  kate flirty_phone "..."
  kate neutral_phone "Don't be greedy, [isabelle]."
  kate neutral_phone "..."
  kate smile_phone "Oh? If you're that desperate, we can certainly arrange something."
  kate smile_phone "How about tomorrow night?"
  kate smile_phone "..."
  kate flirty_phone "Okay, it's a date!"
  kate flirty_phone "Goodbye, cutie."
  mc "..."
  kate eyeroll "What now?"
  mc "I, err... I hope I'm not interrupting."
  kate skeptical "You always are."
  mc "Well, I learned something that you might be interested in."
  kate excited "I'm listening."
  if "eavesdropped_locations" in quest.kate_intrigue.flags:
#   mc "So, I followed [isabelle] around for a while, and she seems to be giving her dad pictures of some files."
    mc "So, I followed [isabelle] around for a while, and she seems to be giving{space=-25}\nher dad pictures of some files."
    mc "Do you have any files lying around?"
#   kate skeptical "Hmm... my father has a business cabinet where he puts all his files..."
    kate skeptical "Hmm... my father has a business cabinet where he puts all his files...{space=-10}"
    mc "Does [isabelle] have access to it?"
  else:
#   mc "I spoke to [isabelle], and she confided that she's going after your dad."
    mc "I spoke to [isabelle], and she confided that she's going after your dad.{space=-15}"
    mc "She specifically mentioned some business files."
    mc "How does she have access to them?"
  kate skeptical "I mean, I don't watch her all the time..."
# mc "It seems like she's trying to find something shady on your dad, then."
  mc "It seems like she's trying to find something shady on your dad, then.{space=-15}"
  kate annoyed "That little whore!"
  mc "What are you going to do?"
  kate annoyed "I guess I'll have to lock her up whenever I'm not watching her."
  kate annoyed "Thanks for telling me about this."
  show kate annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'm just trying to keep us both out of jail.\"":
      show kate annoyed at move_to(.5)
      mc "I'm just trying to keep us both out of jail."
      kate thinking "That's good."
      kate thinking "I guess I need to come up with even harsher punishments for my treacherous little pet."
#     kate thinking "Keep watching her just in case she's trying to cook up more trouble, will you?"
      kate thinking "Keep watching her just in case she's trying to cook up more trouble,{space=-10}\nwill you?"
      mc "Will do."
      window hide
      hide kate with Dissolve(.5)
      window auto
      "I probably screwed over [isabelle] completely with this..."
      "...but it had to be done."
      "And [kate] genuinely seemed to appreciate my efforts, which is rare and exciting."
      "She'll probably leave me alone as long as I'm on her side, and that might be as good as it gets."
      "It allows me to enjoy my second chance without constantly looking over my shoulder."
      window hide
      $quest.kate_intrigue.finish("done_left_alone")
#   "?kate.lust>=10@[kate.lust]/10|{image=kate contact_icon}|{image=stats lust_3}|\"It doesn't come for free.\"":
    "?kate.lust>=10@[kate.lust]/10|{image=kate contact_icon}|{image=stats lust_3}|\"It doesn't come\nfor free.\"":
      show kate annoyed at move_to(.5)
      mc "It doesn't come for free."
      kate thinking "I guess you've done an adequate job."
      kate thinking "What do you want?"
      mc "I want to take [isabelle]'s place."
      kate blush "You little freak!"
      mc "It should've been me, not her!"
      kate blush "Oh, my god! Jealous much?"
      mc "Yes!"
      kate laughing "This is too funny!"
      mc "So, you're okay with it?"
      kate smile "I didn't say that."
      mc "Please, give me a chance!"
      kate neutral "You're pathetic."
      mc "Please? I've done so well..."
      kate neutral "I guess you have."
      mc "Pretty please?"
      kate eyeroll "I can give you a tryout, but no promises."
      mc "Yes! Thank you!"
      kate skeptical "Stop by my house tonight. And don't make me regret this."
      mc "I won't, ma'am!"
      kate skeptical "Now, get out of my face."
      window hide
      hide kate with Dissolve(.5)
      window auto
      "Finally! Okay, I better prepare myself for this."
      "I can't afford to mess it up."
      $quest.kate_intrigue["after_school"] = True
      $quest.kate_intrigue.advance("wait")
  return

label quest_kate_intrigue_lion_den_advance_time:
  pause 0.3
  "It's getting late. I promised to meet up with [kate] at her house."
  "If I want any chance of pulling this off, I can't be late."
  window hide
  hide screen interface_hider
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 1.0
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "It's hard not to be nervous when it feels like you're heading straight for the lion's den."
  "But at the same time, I've always fantasized about [kate]."
  "And hopefully, this could be me realizing those daydreams."
  $game.location = "kate_house_bedroom"
  show black onlayer screens zorder 100
  pause 1.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "I'm not sure what is more concerning... that [kate] didn't let me in herself, or the fact that she has a butler."
  "I can't believe she has a butler."
  "We live in completely different worlds."
  "But damn, look at this place."
  "So many years dreaming of coming here, and now... I finally set foot in [kate]'s bedroom."
  "..."
  "It's not really what I had envisioned."
  "It's luxurious, sure, but..."
  "Where are the cages, whips, and screams of the damned?"
  $quest.kate_intrigue["cages_whips_and_screams"] = True
  return

label quest_kate_intrigue_lion_den:
  show kate neutral at appear_from_right
  pause 0.5
  kate neutral "Are you done going through my stuff?"
  mc "Err, I wasn't—"
  if kate_house_bedroom["lamp_moved"] or kate_house_bedroom["plushie_moved"] or kate_house_bedroom["slippers_moved"]:
    kate confident "Oh, please. You're a little snoop, and you know it."
    kate confident "Some things aren't as I left them."
    mc "Fine. I couldn't help it."
    kate laughing "You can't help a lot of things, can you?"
    kate laughing "That's why you came here in the first place."
    mc "I can't say I wasn't curious to see your bedroom..."
    kate smile "You haven't been in a girl's bedroom before, I take it?"
    kate smile "I bet [flora] doesn't even let you into hers."
    "How does she know these things?"
    mc "So what if she doesn't?"
    $kate.lust-=2
    kate sad_hands_up "That's so sad!"
    mc "..."
    kate sad_hands_up "You can't be trusted or loved?"
    kate sad_hands_up "Poor little loser."
  else:
    kate neutral "Hmm..."
    "She looks around the room, carefully inspecting every little thing."
    kate confident "Perhaps you did keep your hands to yourself."
    mc "I sure did, ma'am."
    kate confident "It's awfully rude to snoop around, you know?"
    mc "I, um... I swear I didn't."
    kate laughing "Haha! You sound so pathetic! I almost believe you."
    kate smile "..."
    "She takes another long look around."
    $kate.lust+=2
    kate gushing "Very well."
    kate gushing "You're still a little loser that can't be trusted, but... perhaps there's hope yet."
    kate gushing "Perhaps I could turn you into an obedient plaything, who would never dare to step out of line."
  mc "Are you done?"
  kate gushing "Oh, I'm just getting started."
  show kate gushing at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You're already going to peg me...\"":
      show kate gushing at move_to(.5)
      mc "You're already going to peg me..."
      mc "...do you need to berate me, too?"
      kate eyeroll "Berate? I'm just telling you the truth."
      if quest.kate_intrigue["tough_guy"]:
        mc "..."
      else:
        kate skeptical "Besides, I don't remember saying anything about that."
        kate excited "Is that what you want?"
        mc "I... um..."
      kate excited "You seem nervous."
      mc "I guess I am..."
      kate excited "Oh, that's just funny!"
    "\"I don't like it when you taunt me, okay?\"":
      show kate gushing at move_to(.5)
      mc "I don't like it when you taunt me, okay?"
      kate sad "Oh, no! Is the sissy going to cry?"
      mc "No, I'm not."
      kate sad "Are you sure? Really sure?"
      mc "I'm not going to cry."
      $kate.lust+=1
      kate gushing "We'll see about that."
    "\"Do you like me? Be honest.\"":
      show kate gushing at move_to(.5)
      mc "Do you like me? Be honest."
      kate eyeroll "I haven't even fucked you yet and you're already getting attached?"
      mc "I just want to know..."
      kate skeptical "Don't get attached. Do you understand me?"
      kate skeptical "This is an audition. Nothing is decided."
      mc "I understand, but—"
      kate annoyed "That's enough."
      kate annoyed "You came here knowing full well that I'm never going to date you."
      $kate.lust-=1
      kate annoyed "Stop being clingy. It's repulsive."
      mc "I'm sorry..."
  mc "Can we just get started?"
  kate confident "Oh, my. Someone's eager."
  mc "No, it's just that—"
  kate neutral "Shut your mouth."
  mc "..."
  kate neutral "If you want to get started, we can get started."
  kate neutral "Strip."
  show kate neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yes, ma'am.\"":
      show kate neutral at move_to(.5)
      mc "Yes, ma'am."
    "\"Right here?\"":
      show kate neutral at move_to(.5)
      mc "Right here?"
      kate eyeroll "Is that a problem?"
      mc "Well, I just thought you'd have a dungeon or something..."
      kate excited "Maybe I do, maybe I don't."
      kate skeptical "But if you're not naked in five seconds, we're done here."
      kate skeptical "So, if you want this to happen, I suggest you stop wasting my time and do as you're told."
      show kate skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "\"I've... changed my mind.\"":
          show kate skeptical at move_to(.5)
          mc "I've... changed my mind."
          kate annoyed "Are you serious?"
          mc "Sorry, but this isn't what I want."
          kate annoyed "Quit wasting my time, then."
          mc "..."
          kate angry "I said, get the fuck out of my house!"
          kate angry "You're so pathetic, you can't even submit right!"
          mc "I'm sorry!"
          window hide
          $kate["romance_disabled"] = True
          $game.notify_modal(None,"Love or Lust","Romance with [kate]:\nDisabled.",wait=5.0)
          pause 0.25
          show black onlayer screens zorder 100 with Dissolve(3.0)
          stop music
          $game.advance()
          $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
          pause 1.0
          hide kate
          hide black onlayer screens
          with Dissolve(.5)
          if "event_show_time_passed_screen" in game.events_queue:
            $game.events_queue.remove("event_show_time_passed_screen")
          call screen time_passed
          pause 0.5
          play music "home_theme" fadein 0.5
          window auto
          "Well, tonight didn't go exactly as planned."
#         "I'm not sure why I got cold feet all of a sudden, but at least my asshole is intact..."
          "I'm not sure why I got cold feet all of a sudden, but at least my asshole{space=-60}\nis intact..."
          "...which is more than I can say about my future chances with [kate]."
          window hide
          $quest.kate_intrigue.finish("done_cold_feet")
          return
        "\"Yes, ma'am.\"":
          show kate skeptical at move_to(.5)
          mc "Yes, ma'am."
  "Under [kate]'s watchful gaze, my awkward stripping begins."
  "Nervous, but also more than a little bit excited."
  "She's always been in my fantasies, from the day I first saw her all those years ago."
  "Say what you will about her personality, but she's without a doubt the most beautiful girl in the school."
  "And a chance like this?"
  "Well, it doesn't even come once in a lifetime. I know that better than anyone."
  "As the last piece of my clothing falls to the floor, she looks me up and down."
  kate "Hmm..."
  "She has a serious look in her eyes, which tells me to be quiet."
  "At least, she's not making fun of my body. That's probably a good start."
  kate "On your knees."
  "Oh, fuck. This is really happening."
  window hide
  show kate looking_down
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  kate looking_down "Listen carefully, because I'll only say this once."
  kate looking_down "You do not speak without permission."
  kate looking_down "You do not touch me without permission."
  kate looking_down "You don't even look at me without permission."
  kate looking_down "Am I making myself clear?"
  mc "Y-yes..."
  kate looking_down "Yes, what?"
  menu(side="middle"):
    extend ""
    "\"Yes, ma'am.\"":
      mc "Yes, ma'am."
      $kate.lust+=1
      kate looking_down "Good boy."
      kate looking_down "However, I think today we'll go for something different."
      kate looking_down "This is a step up, don't you think?"
    "\"Yes, Miss [kate].\"":
      mc "Yes, Miss [kate]."
      $kate.lust+=2
      kate looking_down "Good bitch."
      kate looking_down "That's how you will address me today."
    "\"Yes, mistress.\"":
      mc "Yes, mistress."
      $kate.lust-=1
      kate looking_down "Wrong. I am not your mistress."
      kate looking_down "This is simply a tryout. I've not yet decided if you're worthy."
      mc "I'm sorry..."
    "\"Yes, goddess.\"":
      mc "Yes, goddess."
      kate looking_down "Ha! Compared to you, that's exactly what I am."
      kate looking_down "However, it's a bit pompous for my taste."
  kate looking_down "Call me Miss [kate]."
  mc "Yes, Miss [kate]."
  kate looking_down "That does have a certain ring to it..."
  kate looking_down "...but basic obedience is just the start."
  kate looking_down "We'll see if you can hack the rest, though I doubt it."
  "As she speaks, my eyes remain downcast."
  "Careful not to look at her, as instructed."
# kate looking_down "Being a good bitch is all about showing the proper respect and reverence."
  kate looking_down "Being a good bitch is all about showing the proper respect and reverence.{space=-120}"
  kate looking_down "Kiss my shoes."
  "Her cool, calm voice snaps like a whip and stutters my heart."
  window hide
  show kate feet_worship right_shoe_kiss
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
# "As my lips meet the rough texture of her toe, a shiver runs through me."
  "As my lips meet the rough texture of her toe, a shiver runs through me.{space=-70}"
  "A hint of sweat and leather. A somewhat salty taste."
  kate feet_worship right_shoe_kiss "And the other one."
  window hide
  pause 0.125
  show kate feet_worship left_shoe_kiss with Dissolve(.5)
  pause 0.25
  window auto
  "As if on submissive autopilot, my lips brush over her other shoe."
  kate feet_worship left_shoe_kiss "Dirty little freak! You have no idea where those have even been."
  kate feet_worship left_shoe_kiss "I bet you would like to clean them for me, though, wouldn't you?"
  mc "Yes, Miss [kate]."
  kate feet_worship left_shoe_kiss "Take them off, then."
  window hide
  pause 0.125
  show kate feet_worship left_shoe_removal with Dissolve(.5)
  pause 0.25
  window auto
  "It feels like a privilege to even touch her."
  "For so long I've been building her up in my mind, putting her on a throne-like pedestal."
  show kate feet_worship left_shoe_removed with dissolve2
  "She wiggles her toes in my grasp."
  "Touching her... smelling her... being this close to any part of her makes me flush with excitement."
  kate feet_worship left_shoe_removed "Now the other one."
  window hide
  pause 0.125
  show kate feet_worship right_shoe_removal with Dissolve(.5)
  pause 0.25
  window auto
  "She doesn't have to tell me twice."
  "Being bossed around like this has been on my mind for decades."
  "Always a guilty pleasure thought late at night."
  show kate feet_worship right_shoe_removed with dissolve2
  "In one swift movement, she now stands before me barefoot."
  kate feet_worship right_shoe_removed "Kiss my feet, bitch."
  "Telling people what to do comes so naturally to her."
  "It's like she was born to rule."
  window hide
  pause 0.125
  show kate feet_worship right_foot_kiss with Dissolve(.5)
  pause 0.25
  window auto
  "Reverently, my lips meet her feet in a humiliating kiss."
  "First one, then the other."
  window hide
  pause 0.125
  show kate feet_worship left_foot_kiss with Dissolve(.5)
  pause 0.25
  window auto
# "My mouth lingers against her skin, tasting the sweat of her, breathing in the faint smell of soap and salt."
  "My mouth lingers against her skin, tasting the sweat of her, breathing{space=-30}\nin the faint smell of soap and salt."
  kate feet_worship left_foot_kiss "God, my feet are aching after such a long day..."
  kate feet_worship left_foot_kiss "Massage them like a good boy, will you?"
  window hide
  pause 0.125
  show kate feet_worship right_foot_massage with Dissolve(.5)
  pause 0.25
  window auto
  "And that's all I want to be."
  "I want her approval more than anything."
  "Even if it'll cost me my dignity."
  kate feet_worship right_foot_massage "Now with the tongue."
  menu(side="middle"):
    extend ""
    "?kate.lust>=12@[kate.lust]/12|{image=kate contact_icon}|{image=stats lust_3}|\"Yes, Miss [kate].\"":
      mc "Yes, Miss [kate]."
    "\"No way!\"":
      mc "No way!"
      show kate feet_worship skeptical right_foot_massage with dissolve2
      kate feet_worship skeptical right_foot_massage "Excuse me?"
      mc "Sorry, but... that's disgusting!"
      window hide
      pause 0.125
      show kate feet_worship failed with Dissolve(.5)
      pause 0.25
      window auto
      kate feet_worship failed "I knew you didn't have what it takes."
      kate feet_worship failed "You wasting my time is what's disgusting."
      kate feet_worship failed "Get the hell out, you pathetic waste of space."
      mc "I'm sorry..."
      kate feet_worship failed "Out. Now."
      window hide
      $kate["romance_disabled"] = True
      $game.notify_modal(None,"Love or Lust","Romance with [kate]:\nDisabled.",wait=5.0)
      pause 0.25
      show black onlayer screens zorder 100 with Dissolve(3.0)
      stop music
      $game.advance()
      $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
      pause 1.0
      hide kate
      hide black onlayer screens
      with Dissolve(.5)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      play music "home_theme" fadein 0.5
      window auto
      "Well, tonight didn't go exactly as planned."
#     "I really thought I could do this... but I guess I don't have what it takes, after all."
      "I really thought I could do this... but I guess I don't have what it takes,{space=-35}\nafter all."
      "But maybe that's for the best? [kate] would probably eat me alive..."
      window hide
      $quest.kate_intrigue.finish("done_cold_feet")
      return
  window hide
  pause 0.125
  show kate feet_worship right_foot_lick with Dissolve(.5)
  pause 0.25
  window auto
  "It's a demeaning order, but I'm completely under her spell."
  "I'm a slave to her whims, to her desires, and to my own."
  "Slowly, up to her toes."
  "Her foot trembles in my hand from the sensation and fills me with a sense of delight and pride."
  "Despite the degrading act, I just want to please her."
  "To make her feel good."
  window hide
  pause 0.125
  show kate feet_worship right_toe_suck with Dissolve(.5)
  pause 0.25
  window auto
  "Suddenly, she slips her big toe into my open mouth."
  "Making me get a full taste."
  kate feet_worship right_toe_suck "Suck it."
  "And I do. God, I do."
  "[kate] laughs, her voice full of derision."
  kate feet_worship right_toe_suck "Oh, my god! You really are disgusting!"
  kate feet_worship right_toe_suck "Make sure you get them all, you pathetic foot bitch."
  "My face burns, the shame makes me sweat."
  window hide
  pause 0.125
  show kate feet_worship right_toe_choke with Dissolve(.5)
  pause 0.25
  window auto
  "She applies more force, pushing her foot inside my mouth."
  "She wants more. Deeper."
  "She keeps on pushing until the toes hit the back of my throat."
  "Tears well up as I do my best not to choke."
  "And yet... the treatment makes my dick twitch and rise higher."
  kate feet_worship right_toe_choke "You're totally getting off on this, aren't you?"
# kate feet_worship right_toe_choke "You're probably going to cream all over the floor because I let you choke on my foot."
  kate feet_worship right_toe_choke "You're probably going to cream all over the floor because I let you choke{space=-85}\non my foot."
  window hide
  pause 0.125
  show kate feet_worship aftermath_confident with Dissolve(.5)
  pause 0.25
  window auto
  "Abruptly, she slides her foot out of my mouth."
  kate feet_worship aftermath_confident "Isn't that right? Aren't you loving this?"
  "Like a rising wave, the emotions threaten to swallow me whole."
  "The shame... the degradation... the arousal..."
  show kate feet_worship aftermath_skeptical with dissolve2
  kate feet_worship aftermath_skeptical "Answer me."
  mc "Y-yes, Miss [kate]..."
  kate feet_worship aftermath_skeptical "Be more specific, bitch."
  mc "Yes, Miss [kate], I love choking on your foot."
  show kate feet_worship aftermath_laughing with dissolve2
  kate feet_worship aftermath_laughing "Interesting, but not surprising."
  kate feet_worship aftermath_laughing "I always knew you were a little worm desperate to crawl at my feet."
  mc "Yes, Miss [kate]. Thank you..."
  show kate feet_worship aftermath_skeptical with dissolve2
  kate feet_worship aftermath_skeptical "That's enough talk from you."
  kate feet_worship aftermath_skeptical "It's time for the next trial. Get down on all fours and turn around."
  window hide
  show kate spanking_trial kateless
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
# "The dryness of my throat... the sweat pouring down my back and legs..."
  "The dryness of my throat... the sweat pouring down my back and legs...{space=-70}"
  "Never before have I felt so stepped on. So completely owned."
  "My body moves on its own. Like an obedient zombie driven by pure lust and need."
  "..."
  "Is this it already?"
  "Am I about to be fucked in the ass by [kate]?"
  "She moves behind me, her wet feet padding against the carpet..."
  window hide
  pause 0.125
  show kate spanking_trial anticipation with Dissolve(.5)
  pause 0.25
  window auto
  "...but moments later, she returns."
  "She comes up behind me, a smile in her voice when she talks."
  kate spanking_trial anticipation "Let's see how well you handle this."
  "Something swishes through the air, and suddenly all the hair on my neck stands up."
  kate spanking_trial anticipation "Do you know this sound?"
  "Another loud swish right next to my ear makes me freeze."
  kate spanking_trial anticipation "Oh, I see you do."
  kate spanking_trial anticipation "But I wonder if you know the feeling..."
  "She takes a step back, and for a moment, her words hang in the air as the anticipation builds."
  window hide
  pause 0.125
  show kate spanking_trial workaround spanking1 with Dissolve(.5)
  play sound "slap"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_trial spanking2 with Dissolve(.125)
  show kate spanking_trial spanking3 with Dissolve(.075)
  show kate spanking_trial spanking4 with Dissolve(.25)
  pause 0.25
  window auto
  mc "Owww!"
# "And when the cane comes whipping down and cracks across my bare ass, pain engulfs every nerve in my body."
  "And when the cane comes whipping down and cracks across my bare{space=-30}\nass, pain engulfs every nerve in my body."
  "Reflexively, my ass clenches and I flinch away from the pain that sears through me."
  kate spanking_trial spanking4 "Oh, that little tap? That's just one of many."
  kate spanking_trial spanking4 "Are you ready for some more?"
  menu(side="middle"):
    extend ""
    "?mc.strength>=4@[mc.strength]/4|{image=stats str}|\"Yes, Miss [kate]!\"":
      mc "Yes, Miss [kate]!"
      kate spanking_trial spanking4 "What was that?"
      kate spanking_trial spanking4 "I don't have time for an ill-mannered bitch, you know."
      mc "Y-yes, please, Miss [kate]!"
      window hide
      pause 0.125
      show kate spanking_trial spanking1 with Dissolve(.125)
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      show kate spanking_trial spanking2 with Dissolve(.125)
      show kate spanking_trial spanking3 with Dissolve(.075)
      show kate spanking_trial spanking4 with Dissolve(.25)
      pause 0.25
      window auto
      mc "Hnnnng!"
      "In response, she brings the cane down again."
      "Snapping it with practiced speed and grace on my asscheek."
      "She's an artist of pain with a simple twist of her wrist."
      "It's terrifying and intoxicating all at once."
      "And it makes my dick quiver, hardening even more."
      window hide
      pause 0.125
      show kate spanking_trial spanking1 with Dissolve(.125)
      play sound "slap"
      $renpy.transition(vpunch, layer="master")
      show kate spanking_trial spanking2 with Dissolve(.125)
      show kate spanking_trial spanking3 with Dissolve(.075)
      show kate spanking_trial spanking4 with Dissolve(.25)
      pause 0.25
      window auto show
      show kate spanking_trial spanking4_red with dissolve2
      mc "Puuuh!"
      "The cane whistles down again, this time on my other cheek."
      kate spanking_trial spanking4_red "What's that? Are you going to beg for it?"
      menu(side="middle"):
        extend ""
        "?mc.strength>=8@[mc.strength]/8|{image=stats str}|\"Yes, please, Miss [kate]!\"":
          mc "Yes, please, Miss [kate]!"
          kate spanking_trial spanking4_red "Then beg, bitch."
          mc "P-please... I would really like some more, Miss [kate]..."
          kate spanking_trial spanking4_red "Do you want me to hit you?"
          mc "Please!"
          kate spanking_trial spanking4_red "Do you like being my pain slut?"
          mc "Yes, Miss [kate]!"
          kate spanking_trial spanking4_red "Keep groveling, then."
          window hide
          pause 0.125
          show kate spanking_trial spanking1_red with Dissolve(.125)
          play sound "slap"
          $renpy.transition(vpunch, layer="master")
          show kate spanking_trial spanking2_red with Dissolve(.125)
          show kate spanking_trial spanking3_red with Dissolve(.075)
          show kate spanking_trial spanking4_red with Dissolve(.25)
          pause 0.0
          show kate spanking_trial spanking1_red with Dissolve(.125)
          play sound "slap"
          $renpy.transition(vpunch, layer="master")
          show kate spanking_trial spanking2_red with Dissolve(.125)
          show kate spanking_trial spanking3_red with Dissolve(.075)
          show kate spanking_trial spanking4_red with Dissolve(.25)
          pause 0.25
          window auto
          mc "Ayyyeeee!"
          "Her strokes get harder. Lashing across my skin."
          "The harsh, stinging pain makes me feel like I am being flayed."
          "Ropes of fire burning across my flesh."
          window hide
          pause 0.125
          show kate spanking_trial spanking1_red with Dissolve(.125)
          play sound "slap"
          $renpy.transition(vpunch, layer="master")
          show kate spanking_trial spanking2_red with Dissolve(.125)
          show kate spanking_trial spanking3_red with Dissolve(.075)
          show kate spanking_trial spanking4_red with Dissolve(.25)
          pause 0.0
          show kate spanking_trial spanking1_red with Dissolve(.125)
          play sound "slap"
          $renpy.transition(vpunch, layer="master")
          show kate spanking_trial spanking2_red with Dissolve(.125)
          show kate spanking_trial spanking3_red with Dissolve(.075)
          show kate spanking_trial spanking4_red with Dissolve(.25)
          pause 0.25
          window auto show
          show kate spanking_trial spanking4_redder with dissolve2
#         "Every fiber in my body screams at me to get away, and yet somehow I manage to remain still."
          "Every fiber in my body screams at me to get away, and yet somehow{space=-15}\nI manage to remain still."
          "Shaking... but still."
#         "[kate]'s breathing increases. Perhaps in excitement, or perhaps because she's putting more force into each stroke."
          "[kate]'s breathing increases. Perhaps in excitement, or perhaps because{space=-55}\nshe's putting more force into each stroke."
          kate spanking_trial spanking4_redder "Look at you moaning and screaming like a pussy..."
          kate spanking_trial spanking4_redder "...and your pathetic little dick getting harder."
          kate spanking_trial spanking4_redder "Is that all you've got? Had enough yet?"
          menu(side="middle"):
            extend ""
            "?mc.strength>=12@[mc.strength]/12|{image=stats str}|\"No, Miss [kate]!\"":
              mc "No, Miss [kate]!"
              "She pauses for a moment."
              kate spanking_trial spanking4_redder "Do you want some more?"
              kate spanking_trial spanking4_redder "Do you really think you have what it takes to be my bitch?"
              mc "Yes, Miss [kate]! I do!"
              kate spanking_trial spanking4_redder "Very well. You better buckle up, then."
              kate spanking_trial spanking4_redder "Here comes the actual pain."
              "Oh, god..."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.0
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.0
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.25
              window auto
              "Her voice rings through the room with unrestrained delight."
              "And then she makes good on her promise."
              "She rains down blow after blow."
#             "The cane singing through the air and reaching its crescendo on my skin."
              "The cane singing through the air and reaching its crescendo on\nmy skin."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.0
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.0
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.25
              window auto
              "The pain ravages my nerves and screams in my head, and yet somehow it feels like I'm approaching my biggest orgasm ever."
              "My body shakes uncontrollably."
              "Desperate to crawl away from her."
              "To escape the torment that is burning through the very core of my being."
#             "Yet each strike sends shockwaves through my stomach, humming into my rock hard dick."
              "Yet each strike sends shockwaves through my stomach, humming into{space=-55}\nmy rock hard dick."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.0
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.0
              show kate spanking_trial spanking1_redder with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_redder with Dissolve(.125)
              show kate spanking_trial spanking3_redder with Dissolve(.075)
              show kate spanking_trial spanking4_redder with Dissolve(.25)
              pause 0.25
              window auto show
              show kate spanking_trial spanking4_reddest with dissolve2
              mc "Ahhhh!"
              "It escapes me in screams and ragged pants."
              "An inhale of air, an exhaled scream."
              "Again and again and again."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_reddest with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_reddest with Dissolve(.125)
              show kate spanking_trial spanking3_reddest with Dissolve(.075)
              show kate spanking_trial spanking4_reddest with Dissolve(.25)
              pause 0.25
              window auto
              kate spanking_trial spanking4_reddest "That's."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_reddest with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_reddest with Dissolve(.125)
              show kate spanking_trial spanking3_reddest with Dissolve(.075)
              show kate spanking_trial spanking4_reddest with Dissolve(.25)
              pause 0.25
              window auto
              extend " A."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_reddest with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_reddest with Dissolve(.125)
              show kate spanking_trial spanking3_reddest with Dissolve(.075)
              show kate spanking_trial spanking4_reddest with Dissolve(.25)
              pause 0.25
              window auto
              extend " Good."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_reddest with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_reddest with Dissolve(.125)
              show kate spanking_trial spanking3_reddest with Dissolve(.075)
              show kate spanking_trial spanking4_reddest with Dissolve(.25)
              pause 0.25
              window auto
              extend " Bitch."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_reddest with Dissolve(.125)
              play sound "slap"
              $renpy.transition(vpunch, layer="master")
              show kate spanking_trial spanking2_reddest with Dissolve(.125)
              show kate spanking_trial spanking3_reddest with Dissolve(.075)
              show kate spanking_trial spanking4_reddest with Dissolve(.25)
              pause 0.25
              window auto
              "She punctuates each word with a smack."
              "Tears sting my eyes and run down my cheeks."
              "I am incapable of actual speech."
              "Nothing but pain and sweat and tears."
              "My cock aching for release as the pressure builds in my balls."
              window hide
              pause 0.125
              show kate spanking_trial spanking1_reddest with Dissolve(.125)
              show kate spanking_trial spanking2_reddest with Dissolve(.125)
              play sound "slap"
              play ambient "audio/sound/line_snapping.ogg" noloop
              $renpy.transition(Move((0,20), (0,-20), 0.10, bounce=True, repeat=True, delay=0.275))
              hide screen interface_hider
              show kate spanking_trial aftermath
              show black
              show black onlayer screens zorder 100
              pause 0.5
              show black onlayer screens zorder 4
              $set_dialog_mode("default_no_bg")
              mc "Aaaaaaaah!"
              "Until [kate] brings the cane down one last time and a new sound bounces off my eardrums..."
              "...a loud snap as the cane breaks against my reddened buttcheeks."
              "And then silence."
              "Only the sound of our pants clogging the sweat drenched room."
              "My whole body trembles in pain and fatigue."
              "Ready to collapse on the floor."
              show black onlayer screens zorder 100
              pause 0.5
              hide black
              hide black onlayer screens
              show screen interface_hider
              with Dissolve(.5)
              window auto
              $set_dialog_mode("")
              kate spanking_trial aftermath "Would you look at that? Not only did you take it, but you broke my cane too."
              kate spanking_trial aftermath "Which, of course, you'll have to replace."
              kate spanking_trial aftermath "But I have to admit, I didn't think you would last."
              mc "T-thank you... Miss... [kate]..."
              kate spanking_trial aftermath "There's a good bitch."
              $unlock_replay("kate_trials")
            "\"Y-yes!\"":
              mc "Y-yes!"
              window hide
              pause 0.125
              show kate spanking_trial failed_redder with Dissolve(.5)
              pause 0.25
              window auto
              kate spanking_trial failed_redder "Yes, you've had enough?"
              "Shaky labored breaths tremble over my lips."
              "Sweat dripping off my forehead and onto the floor."
              mc "P-please..."
              kate spanking_trial failed_redder "Please, what, worm?"
              mc "Please... I have had enough..."
              "The words tumble from me in a pathetic whisper."
              "But I just can't take anymore."
              kate spanking_trial failed_redder "I knew you couldn't do it."
#             kate spanking_trial failed_redder "Look at you, sniveling on the floor. You don't have the balls to be my sub."
              kate spanking_trial failed_redder "Look at you, sniveling on the floor. You don't have the balls to be my sub.{space=-110}"
              kate spanking_trial failed_redder "Get out of here, you little sissy bitch."
              "Her words sting almost as much as the strokes from the cane."
              "The feeling is one of utter disappointment."
              "It feels like I won a million bucks... and then lost it the same day."
              window hide
              $kate["romance_disabled"] = True
              $game.notify_modal(None,"Love or Lust","Romance with [kate]:\nDisabled.",wait=5.0)
              pause 0.25
              show black onlayer screens zorder 100 with Dissolve(3.0)
              stop music
              $game.advance()
              $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
              pause 1.0
              hide kate
              hide black onlayer screens
              with Dissolve(.5)
              if "event_show_time_passed_screen" in game.events_queue:
                $game.events_queue.remove("event_show_time_passed_screen")
              call screen time_passed
              pause 0.5
              play music "home_theme" fadein 0.5
              window auto
              "Well, tonight didn't go exactly as planned."
              "Walking back home hurt so bad... and yet, the biggest pain is that I couldn't complete [kate]'s trial."
              "I guess I don't have what it takes, after all."
              "The shame pumps through my still pounding heart."
              window hide
              $quest.kate_intrigue.finish("done_cold_feet")
              return
        "\"Please, no more!\"":
          mc "Please, no more!"
          window hide
          pause 0.125
          show kate spanking_trial failed_red with Dissolve(.5)
          pause 0.25
          window auto
          kate spanking_trial failed_red "What was that?"
          mc "Please... have mercy..."
          mc "I-I beg you."
          kate spanking_trial failed_red "Mercy? You have got to be kidding me."
          kate spanking_trial failed_red "You are without a doubt the worst wannabe sub I have ever seen."
          mc "I'm sorry..."
          kate spanking_trial failed_red "I don't want to hear your apologies."
          kate spanking_trial failed_red "I'm not going to sit around and waste any more time on you."
          mc "I just... need a moment..."
          kate spanking_trial failed_red "Too late, you pathetic cry baby bitch."
          kate spanking_trial failed_red "Just get out of here."
          mc "I—"
          kate spanking_trial failed_red "Leave. Now."
          window hide
          $kate["romance_disabled"] = True
          $game.notify_modal(None,"Love or Lust","Romance with [kate]:\nDisabled.",wait=5.0)
          pause 0.25
          show black onlayer screens zorder 100 with Dissolve(3.0)
          stop music
          $game.advance()
          $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
          pause 1.0
          hide kate
          hide black onlayer screens
          with Dissolve(.5)
          if "event_show_time_passed_screen" in game.events_queue:
            $game.events_queue.remove("event_show_time_passed_screen")
          call screen time_passed
          pause 0.5
          play music "home_theme" fadein 0.5
          window auto
          "Well, tonight didn't go exactly as planned."
#         "I really thought I could do this... but I guess I don't have what it takes, after all."
          "I really thought I could do this... but I guess I don't have what it takes,{space=-35}\nafter all."
          "But maybe that's for the best? The pain was just too much..."
          window hide
          $quest.kate_intrigue.finish("done_cold_feet")
          return
    "\"N-no!\"":
      mc "N-no!"
      window hide
      pause 0.125
      show kate spanking_trial failed with Dissolve(.5)
      pause 0.25
      window auto
      kate spanking_trial failed "Ugh, seriously?"
      mc "It... it hurts..."
      kate spanking_trial failed "Duh! That's the point."
      kate spanking_trial failed "And that wasn't even hard. A frail woman could take it."
      "My face burns in embarrassment and disappointment, but the pain is just too much for me to handle."
#     kate spanking_trial failed "How absolutely pathetic. Just get out of here, you sad little bitch boy."
      kate spanking_trial failed "How absolutely pathetic. Just get out of here, you sad little bitch boy.{space=-30}"
      mc "Err, yes, Miss—"
      kate spanking_trial failed "You failed. Get out."
      window hide
      $kate["romance_disabled"] = True
      $game.notify_modal(None,"Love or Lust","Romance with [kate]:\nDisabled.",wait=5.0)
      pause 0.25
      show black onlayer screens zorder 100 with Dissolve(3.0)
      stop music
      $game.advance()
      $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
      pause 1.0
      hide kate
      hide black onlayer screens
      with Dissolve(.5)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      play music "home_theme" fadein 0.5
      window auto
      "Well, tonight didn't go exactly as planned."
#     "I really thought I could do this... but I guess I don't have what it takes, after all."
      "I really thought I could do this... but I guess I don't have what it takes,{space=-35}\nafter all."
      "But maybe that's for the best? The pain was just the beginning..."
      window hide
      $quest.kate_intrigue.finish("done_cold_feet")
      return
  if quest.kate_intrigue["tough_guy"] or kate.lust >= 15:
    "Her words fill me with a strange pride."
    "It's fulfilling somehow to hear someone like her praise me."
    "Endurance is something that I never thought I'd get praised for."
    "My head spins in a mix of euphoria and stinging pain."
    "I made it. I goddamn made it!"
    kate spanking_trial aftermath "I guess you deserve a little reward."
    kate spanking_trial aftermath "Or, well, perhaps not so little..."
    window hide
    hide screen interface_hider
#   show kate pegging insertion
    show black
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    show black onlayer screens zorder 4
    $set_dialog_mode("default_no_bg")
    "She leaves me there, hunched over, ass smarting from the beating, and walks away again."
    "My eyes sting with a mix of tears and sweat as I wait patiently for her."
    "The minutes tick by, leaving me there with my throbbing agony and lust."
    "I won't be able to sit for weeks..."
    "It will hurt just to wear pants..."
    "I—"
    show black onlayer screens zorder 100
    pause 0.5
    hide black
    hide black onlayer screens
    show kate pegging insertion: ## This gives the image both the dissolve (transition) and hpunch (transform) effects
      block:
        linear 0.05 xoffset 15
        linear 0.05 xoffset -15
        repeat 3
      linear 0.05 xoffset 0
    show screen interface_hider
    with Dissolve(.5)
    hide kate
    show kate pegging insertion ## This is just to avoid the screen shake if the player is skipping
    window auto
    $set_dialog_mode("")
    mc "Ohhh, fuck!"
    "I didn't hear [kate] come back..."
    "...but as soon as she does, she comes up behind me and grabs my hips..."
    "...before thrusting a huge strap-on into my caned ass."
    kate pegging insertion "Oh, relax. That's barely just the tip."
    mc "A-are you sure?"
    kate pegging insertion "You're not seriously questioning me now, are you?"
    mc "..."
    mc "No, Miss [kate]."
    kate pegging insertion "I didn't think so."
    kate pegging insertion "Now hold still while I peg your sissy ass."
    "She holds the tip of the strap-on inside of me while my sphincter contracts around it."
    "Reacting to the sudden object being forced into me."
    "Then, she starts to push it deeper."
    window hide
    pause 0.125
    show kate pegging penetration2_grin with Dissolve(.5)
    pause 0.25
    window auto
    "Oh, god. Inch by inch it slides in, as [kate] works me open."
    "My muscles clench, trying to push her back out."
    "But she seems to welcome the resistance as she forces the giant thing even deeper."
    window hide
    pause 0.125
    show kate pegging penetration3_grin with Dissolve(.5)
    pause 0.25
    window auto
    mc "Hnngh..."
    "Fuck, she's going to bottom out against my colon at this rate."
    "My stomach contracts and my dick goes even harder than during the caning."
    "It feels so wrong, and yet so right at the same time."
    "Not because of the fake dick... but because it's attached to [kate]."
    kate pegging penetration3_grin "It's not even all the way in yet."
    show kate pegging penetration3_grinner with dissolve2
    kate pegging penetration3_grinner "Keep taking it, bitch."
    window hide
    pause 0.125
    show kate pegging penetration4_grinner with Dissolve(.5)
    pause 0.25
    window auto
    mc "Ahhh!"
    "Every inch she gives radiates through me."
    "Pulsing through my very skin."
    "Reverberating through the welts on my ass."
    kate pegging penetration4_grinner "You've always been so easy to walk all over."
    kate pegging penetration4_grinner "Just a pathetic, creepy little nobody drooling all over my feet."
    kate pegging penetration4_grinner "And now, here you are, with my strap-on buried in your ass."
    kate pegging penetration4_grinner "I've never seen such a pathetic bitch."
    kate pegging penetration4_grinner "And I have dealt with a lot of bitches."
    kate pegging penetration4_grinner "So, what do you say for being allowed to take part in this tryout?"
    "The prosthetic dick in my ass is making it hard to concentrate..."
#   "...especially as the orgasm starts creeping through my balls, flooding my stomach with ecstasy."
    "...especially as the orgasm starts creeping through my balls, flooding{space=-25}\nmy stomach with ecstasy."
    mc "I'm, um... thank you, Miss [kate]!"
    window hide
    pause 0.125
    show kate pegging penetration3_grinner with Dissolve(.1)
    show kate pegging penetration2_grinner with Dissolve(.1)
    show kate pegging penetration1_grinner with hpunch
    show kate pegging penetration2_grinner with Dissolve(.1)
    show kate pegging penetration3_grinner with Dissolve(.1)
    show kate pegging penetration4_grinner with Dissolve(.1)
    pause 0.25
    window auto
    "As her name leaves my lips, she gives me a hard thrust."
    "Bottoming out inside of my aching rectum."
    "Adding to the pain from earlier."
    kate pegging penetration4_grinner "Does it hurt, bitch?"
    "God, she has no idea..."
    "The sweat from the pain and exertion slicks my entire body."
    "And yet, a calm settles into my mind. Removing all thoughts and inhibitions."
    "All the pain and humiliation transforms into a pleasure I have never felt before."
    mc "Y-yes..."
    window hide
    pause 0.125
    show kate pegging penetration3_grinner with Dissolve(.1)
    show kate pegging penetration2_grinner with Dissolve(.1)
    show kate pegging penetration1_grinner with hpunch
    show kate pegging penetration2_grinner with Dissolve(.1)
    show kate pegging penetration3_grinner with Dissolve(.1)
    show kate pegging penetration4_grinner with Dissolve(.1)
    pause 0.25
    window auto
    "She withdraws halfway, then slams it home again, hard and fast."
    kate pegging penetration4_grinner "I didn't quite catch that."
    mc "Yes, Miss [kate]!"
    show kate pegging penetration4_grinnest with dissolve2
    kate pegging penetration4_grinnest "Good. That's good."
    window hide
    pause 0.125
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.1
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.1
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.1
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.1
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.25
    window auto
    "Her voice drips with glee as she grips me."
    "As she starts to fuck me harder and harder."
    "Going impossibly deep."
    "Burying herself to the hilt as she impales me."
    window hide
    pause 0.125
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.075
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.075
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.075
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.075
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.075
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.075
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.075)
    show kate pegging penetration3_grinnest with Dissolve(.075)
    show kate pegging penetration4_grinnest with Dissolve(.075)
    pause 0.25
    window auto
    mc "Owwww!"
    "My breath turns into little gasps as my body rocks under her merciless thrusts."
    kate pegging penetration4_grinnest "Take it, bitch!"
    window hide
    pause 0.125
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.05
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration1_grinnest with hpunch
    show kate pegging penetration2_grinnest with Dissolve(.05)
    show kate pegging penetration3_grinnest with Dissolve(.05)
    show kate pegging penetration4_grinnest with Dissolve(.05)
    pause 0.25
    window auto
    "I can't hold on any longer as she fucks me good."
    "Working my prostate, building me up to an unbelievable orgasm."
    mc "It's... oh, god... I'm going to—"
    kate pegging penetration4_grinnest "Do it! Cum with my dick buried in your fucking ass!"
    window hide
    pause 0.125
    show kate pegging cum with vpunch
    pause 0.25
    window auto
    mc "Ohhhh, f-fuck!"
    "Her words undo me."
#   "My dick has a mind of its own, cumming hard, even as my ass tightens and squeezes around her."
    "My dick has a mind of its own, cumming hard, even as my ass tightens{space=-55}\nand squeezes around her."
#   "Tears slide down my cheeks, drool creeping from my open mouth as the orgasm shudders through me."
    "Tears slide down my cheeks, drool creeping from my open mouth as{space=-10}\nthe orgasm shudders through me."
    "It feels like two or maybe three orgasms roll through me."
    "It's the longest my dick has ever kept pumping out semen."
#   "[kate] rests against my ass for a moment, clearly enjoying her handiwork."
    "[kate] rests against my ass for a moment, clearly enjoying her handiwork.{space=-85}"
    "And she did work me over like no one else has ever done."
    "Only our ragged breathing fills the silence in her room."
    window hide
    pause 0.125
    show kate pegging aftermath with Dissolve(.5)
    pause 0.25
    window auto
    "Until she finally pulls the strap-on out."
    mc "Oh, my god..."
    kate pegging aftermath "Now, that's what I call an ass claiming."
    kate pegging aftermath "You went full sub mode there, [mc]."
    mc "T-thank... you..."
    "For a good while, I just lie there on the floor, sweat dripping, ass in agony."
    "She watches me silently in amusement, but allows me this moment to process."
    $unlock_replay("kate_reward")
    window hide
    show kate blush
    show black onlayer screens zorder 100
    with Dissolve(3.0)
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    kate blush "Well, I'll be damned. You did it."
    kate blush "You survived each trial and even got to cum."
    kate blush "I'm shocked... but mildly impressed."
    mc "Thank—"
    kate embarrassed "Don't let it go to your head, though."
    kate embarrassed "This is just the beginning."
    "Those words fill me with both elation and fear..."
    kate thinking "Anyway, you may go now."
    kate thinking "I'll be in touch."
    mc "Yes, Miss [kate]."
    window hide
    show black onlayer screens zorder 100 with Dissolve(3.0)
    stop music
    $game.advance()
    $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
    pause 1.0
    hide kate
    hide black onlayer screens
    with Dissolve(.5)
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5
    play music "home_theme" fadein 0.5
    window auto
    "Holy fuck... I can't believe that just happened."
    "My ass burns and I feel like I have been torn open."
    "I won't be able to sit for weeks..."
    "...but I wouldn't change any second of it."
    "And this probably means [kate] is considering taking me as her sub."
    "Oh, man! This is going to be a great year!"
  else:
    window hide
    show kate blush
    show black onlayer screens zorder 100
    with Dissolve(3.0)
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    kate blush "That's enough for now."
    mc "But—"
    kate embarrassed "I said enough. Don't ruin all the hard work you just put in."
    mc "..."
    mc "Yes, Miss [kate]..."
    kate thinking "Good. I will decide when I deign to treat you this way again."
    kate thinking "Now, you may go."
    "After all of that, I expected a little more..."
    "...but now is not the time to talk back."
    "I don't want to ruin future chances with her."
    window hide
    show black onlayer screens zorder 100 with Dissolve(3.0)
    stop music
    $game.advance()
    $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
    pause 1.0
    hide kate
    hide black onlayer screens
    with Dissolve(.5)
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5
    play music "home_theme" fadein 0.5
    window auto
    "Fuck, it really hurts..."
    "My ass burns more than my face."
    "I need some pain meds and maybe a good cry."
    "But a small part of me is beaming with pride."
    "I actually passed [kate]'s tests!"
    "And good things come to those that are patient."
  window hide
  $quest.kate_intrigue.finish()
  return
