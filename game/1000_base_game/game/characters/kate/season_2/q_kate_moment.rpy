label quest_kate_moment_start:
  "Huh? This is an odd sight..."
  "I never thought I'd see [kate] sleeping during school hours."
  "She's always had more energy than anyone but [lindsey]."
  "That's how she never gets tired of bullying me."
  "..."
  window hide
# show kate passed_out
  show black onlayer screens zorder 100
  with Dissolve(.5)
  show kate passed_out
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "She really is pretty, though. A real sleeping beauty."
  "...that is, if the evil queen accidentally stung herself on the spindle."
  "Although, she looks pretty peaceful now."
  "Definitely less threatening."
  $moments_of_glory = mc["moments_of_glory"]
  menu(side="middle"):
    "?mc['moments_of_glory']>=3@[moments_of_glory]/3 Moments\n{space=31}of Glory|Wake her up":
      "Considering how many sleepless nights she has caused me, it's only fair..."
      mc "Wake up!" with vpunch
      window hide
      show kate sad_tired
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      kate sad_tired "Mmm...? What...?"
      "Her eyes are bloodshot, and she looks confused."
      "Maybe she grew a conscience, and it's keeping her up at night?"
      show kate sad_tired at move_to(.25)
      menu(side="right"):
        extend ""
        "?mc.strength>=10@[mc.strength]/10|{image=stats str}|\"That chair doesn't look\nvery comfortable.\"":
          show kate sad_tired at move_to(.5)
          mc "That chair doesn't look very comfortable."
          kate eyeroll_tired "Why don't you bow down so I can use your face instead, then?"
          mc "All right. Get naked."
          kate excited_tired "You wish."
          mc "Don't talk the talk if you can't walk the walk."
          mc "Undress or quit smiling."
          kate skeptical_tired "Don't tell me what to do."
          mc "Why not? That's your favorite thing to do."
          mc "You can't walk the walk, and you don't like your own medicine?"
          mc "Are you telling me you're a hypocrite, Katie?"
          kate annoyed_right_tired "F-fuck off, loser."
          "Somehow, there's an unusual lack of conviction in her words..."
          "Cracks in her otherwise impenetrable walls."
          mc "Call me that again and I'll smack you."
          kate surprised_tired "Y-you wouldn't hit a girl..."
          mc "But you're not a girl, you're a bitch."
          mc "And it's called a bitch-slap for a reason."
          kate sad_tired2 "..."
          "She knows I'm being serious, and that's a win in and of itself."
          "Those extra hours at the gym are finally paying dividends."
          kate sad_tired2 "Err, whatever..."
          mc "No, not whatever. Don't call me that again."
          kate surprised_tired "..."
          "She looks dumbfounded. She's never been spoken to like this before."
          "Something shifts behind her eyes — maybe it's exhaustion, maybe it's something else."
          "Our eyes meet for a moment, and it takes everything in me to remain unflinching."
          "It's a staring contest that makes my eyes burn and my heart\npound."
          "This moment is everything. A test of wills. All-defining."
          show kate sad_right_hands_up_tired2 with dissolve2
          "And [kate] blinks first, and looks away. A soft pink shade touching her cheeks."
          if quest.isabelle_dethroning["kate_love_points"]:
            $kate.love+=5
            $quest.kate_moment["kate_love_points_redeemed"] = True
          else:
            $kate.love+=2
          kate sad_right_hands_up_tired2 "...fine, I won't."
          "A stunned silence follows her words."
          "Her first ever concession to me."
          "It's hard to say what it means in the grand scale of things, but it's something."
          "My lungs fill with air once more, and I breathe a sigh of relief."
        "\"Are you okay?\"":
          show kate sad_tired at move_to(.5)
          $mc.love+=1
          mc "Are you okay?"
          kate skeptical_tired "I don't see how that's any of your—"
          mc "I just asked how you're doing. No need to be rude."
          mc "You look like hell... and not the fiery kind that we're all used to."
          kate eyeroll_tired "Sigh..."
          "Her shoulders slouch visibly."
          "She's always been a posture queen — head held high, back straighter{space=-30}\nthan Chad's sexuality."
          "What happened to her over the break?"
          if quest.isabelle_dethroning["kate_love_points"]:
            $kate.love+=4
            $quest.kate_moment["kate_love_points_redeemed"] = True
          else:
            $kate.love+=1
          kate eyeroll_tired "I'm fine. Thanks for asking."
          "She doesn't look fine, but pushing her further seems like a bad idea.{space=-10}"
      mc "Anyway, shouldn't you be at cheer practice right now?"
      kate thinking_tired "Err, no."
      mc "But I saw [stacy] and the others heading to the gym..."
      kate embarrassed_tired "..."
      mc "What?"
      kate embarrassed_tired "Nothing. I have to go."
      window hide
      show kate embarrassed_tired at disappear_to_left
      pause 0.5
      $kate["at_none"] = True
      window auto
      "Well, that's odd..."
      "She's never missed practice before."
      "..."
      "Huh? What's this?"
      "This bottle must've fallen out of her pocket when she was sleeping."
      window hide
      $mc.add_item("kate_pills")
      pause 0.25
      window auto
      "That's weird. [kate] doesn't seem like someone who needs drugs."
      "Hmm... Xynoparazyl-something..."
      "It sounds expensive. I guess I should return them."
      window hide
      $quest.kate_moment.start()
      $mc["focus"] = "kate_moment"
      $renpy.transition(Dissolve(.5),layer="screens")
    "?mc['moments_of_glory']<3|Don't poke a sleeping bitch":
      "Yeah, I'm not touching that. Not even with a ten-foot dick."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      hide kate passed_out
      pause 0.5
#     hide kate passed_out
      hide black onlayer screens
      with Dissolve(.5)
      pause 0.25
      $kate["sleeping_this_scene"] = mrsl["flirting_this_scene"] = True
      $quest.kate_moment.fail()
  return

label quest_kate_moment_gym:
  "I guess the easiest place to start looking for [kate] is in here..."
  "Although, the gym always makes me nervous."
  "..."
  "Maybe this wasn't such a—"
  window hide
  show casey confident flip at appear_from_left(.15)
  show stacy confident at appear_from_right(.525)
  show lacey confident at appear_from_right(.85) behind stacy
  pause 0.5
  window auto
  "Shit. Fuck. Cock."
  mc "H-hello, ladies..."
  show casey confident_mocking flip
  show lacey confident_mocking
  stacy confident_mocking "Ew!"
  lacey confident_mocking "I know, right?!"
  casey confident_mocking flip "Makes my skin crawl!"
  stacy confident_mocking "Total ick factor!"
  casey neutral flip "It's \"it factor.\""
  stacy neutral "Babe, it's because he's giving me the ick. Don't ruin it."
  lacey confident "Slay!"
  "God, even without [kate], they're like a trio of Stepford Cheerleaders...{space=-25}"
  mc "I, err... I don't suppose you ladies have seen [kate] around?"
  show casey neutral_shifty flip
  show stacy neutral_shifty
  show lacey neutral_shifty
  with Dissolve(.5)
  retinue "..."
  "That seems to shut them up for a moment."
  "They get shifty eyed and [lacey] nervously bites her nails."
  stacy neutral_shifty "Not that it concerns the likes of you..."
  lacey neutral_shifty "Err, yeah! It doesn't!"
  stacy neutral_shifty "...but I can't say that we have... or that we care."
  casey confident flip "Miss Cheer, no one wants you here!"
  show stacy confident
  lacey confident "Queen!"
  lacey confident "[stacy] is the new head cheerleader, didn't you know?"
  "Damn. So, the plan actually worked?"
  "[kate] is no longer on the cheer team, and they seem happy about it."
  "I thought that she'd somehow manage to wrangle herself back on, but I guess not..."
  "The sudden loss of social status must've turned her world upside down."
  "But knowing [kate], I doubt that this is how her story ends. She always{space=-30}\nseems to have an ace up her skirt."
  "Somehow, I doubt that [stacy] can fill [kate]'s shoes..."
  "...but why encourage her to rise to the challenge?"
  mc "Right. Thanks, anyway."
  mc "And happy, err... ruling?"
  show casey neutral flip
  show lacey neutral
  stacy neutral "Whatever, dork."
  window hide
  hide casey
  hide stacy
  hide lacey
  with Dissolve(.5)
  window auto
  "I wonder how long that'll last."
  "Dethrone a queen, and you inevitably open up a power vacuum."
  "..."
  "Where the hell is [kate], though? Where does she usually hang out besides the gym?"
  $quest.kate_moment.advance("nurse_room")
  return

label quest_kate_moment_nurse_room:
  show nurse blush with Dissolve(.5)
  nurse blush "Hello, [mc]!"
  mc "Hello, [nurse]."
  mc "Have you seen [kate] around, by any chance?"
  nurse concerned "Oh, dear. I can't say that I have. Not today."
  mc "That's odd..."
  nurse concerned "Is everything okay? You look concerned."
  mc "I don't know if concerned is the right word. I'm more... intrigued."
  nurse thinking "Have you looked in the gym?"
  mc "Yes, I just did. She's not there."
  mc "I actually found her sleeping earlier, which was weird. And now she's gone."
  nurse afraid "Oh, dear! That's not like her, is it?"
  mc "Right. It's completely out of character."
  nurse neutral "Goodness."
  mc "Anyway, thanks for the info."
  nurse smile "No worries at all!"
  window hide
  hide nurse with Dissolve(.5)
  window auto
  "Well, that didn't help much..."
  "Maybe someone else knows something?"
  $quest.kate_moment.advance("jacklyn")
  return

label quest_kate_moment_jacklyn_art_class:
  "[kate] certainly has a way of getting creative with her punishments..."
  "Maybe she's channeling whatever's going on now into actual art?"
  "..."
  "Doubtful."
  "Either way, [jacklyn] might know something as a teacher's aide."
  $quest.kate_moment["actual_art"] = True
  return

label quest_kate_moment_jacklyn:
  show jacklyn excited with Dissolve(.5)
  jacklyn excited "'Sup, brochacho?"
  jacklyn excited "How's the cheese dripping?"
  mc "Err, hot?"
  jacklyn laughing "Wepa!"
  mc "Totally."
  "I think..."
  mc "Anyway, I was just looking for [kate]."
  mc "Have you seen her around?"
  jacklyn thinking "Can't say I have."
  jacklyn thinking "Girlfriend suffers from a case of artistic repression, if you ask me."
  "Hmm... maybe repression has been her issue all along..."
  mc "Yeah, it could be."
  jacklyn smile_hands_down "Trust me, I have a way of knowing these things."
  jacklyn neutral "...not that I've dabbled my paint in the turpentine."
  "Mmm... [jacklyn] and [kate] dabbling would be something to see..."
  "If only..."
  mc "Do you have any idea where she might be?"
  mc "I have something for her."
  jacklyn excited "Shimmy your hand sausages this way and I'll do you the hottest doggy!"
  mc "Err, no, that's okay! I'll just wait until she's around."
  mc "But thanks, anyway!"
  jacklyn laughing "No grass off my lawn."
  window hide
  hide jacklyn with Dissolve(.5)
  window auto
  "Well, while [jacklyn] might have a sense for the dabblers and\nnon-dabblers..."
  "...I think it's time I talk to the eyes and ears of this place."
  $quest.kate_moment.advance("maxine")
  return

label quest_kate_moment_maxine:
  show maxine thinking with Dissolve(.5)
  maxine thinking "I know what you did."
  mc "Last summer?"
  maxine thinking "What does the season have to do with it?"
  mc "Err, nothing. Nevermind."
  maxine sad "It's all about reading in between the lines, remember?"
  "I wonder if all this time [maxine] has meant lines of coke..."
  "It would almost explain her weird behavior."
  mc "I remember."
  mc "Anyway, have you seen [kate] around?"
  maxine thinking "I have seen either [kate] or her doppelganger or both."
  mc "..."
  mc "When?"
  maxine skeptical "I think the more appropriate question is, \"How?\""
  mc "Err, okay. How, then?"
  "I swear to god, if she says with her eyes..."
  maxine confident "I'm glad you asked!"
  maxine confident "It was a dark and stormy night..."
  maxine confident "Harsh winds were whipping through the treetops..."
  maxine confident "The gargoyles spat rain and threw long shadows across the courtyard..."
  maxine confident "A lonely raven cawed out in existential dread..."
  mc "Sorry, is this relevant?"
  maxine annoyed "Don't interrupt me."
  show maxine annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I don't have time for this, [maxine].\"":
      show maxine annoyed at move_to(.5)
      mc "I don't have time for this, [maxine]."
      $maxine.love-=1
      maxine angry "If you don't have time to listen, then why did you come?"
      mc "Well, I don't need to know about the raven and what not!"
      mc "Can't you just give me the short version?"
      maxine neutral "This is the short version."
      mc "Is there an, err... extra-short version?"
      maxine smile "Do you want it told in a haiku?"
      mc "If that's the shortest you've got..."
      maxine laughing "Very well."
      maxine smile "Stormy night, two met."
      maxine smile "Green figure and goldilocks."
      maxine smile "Exchange of parcel."
      mc "..."
      mc "That's it?"
      maxine neutral "Naturally. A haiku is no longer."
      maxine neutral "Now, if you'll excuse me."
      window hide
      hide maxine with Dissolve(.5)
      window auto
      "Ugh, maybe I should have let her tell the full story..."
      "I guess goldilocks must be [kate]."
      "Green figure, though? Who could that be?"
    "\"My apologies. Please, go ahead.\"":
      show maxine annoyed at move_to(.5)
      mc "My apologies. Please, go ahead."
      $maxine.love+=1
      maxine excited "Thank you."
      maxine concerned "So, as I was saying..."
      maxine concerned "I was on the roof with my binoculars..."
      maxine concerned "When suddenly, out of the woods, came a figure in green..."
      maxine concerned "She was short and stout, and her bosom was warm like a teacup..."
      mc "If you were on the roof, how would you know that?"
      maxine excited "Biothermal vision, of course."
      mc "Right. Of course."
      maxine concerned "Anyway, under the cover of an old oak..."
      maxine concerned "The figure in green, now dripping wet from the storm, hugged herself against the chilly autumn air..."
      maxine concerned "But she wasn't alone..."
      maxine concerned "No, there was someone else lurking in the dusky forestline..."
      maxine concerned "A lady, whose umbrella was tossed this way and that by the boisterous wind, suddenly stepped forth..."
      maxine concerned "Her hair, like strands of glimmering gold, lit up the night..."
      maxine concerned "In hushed voices, they traded words, the lady and the figure\nin green..."
      maxine concerned "Indeed, even a parcel changed hands... before they went their separate ways."
      mc "..."
      maxine excited "That's all I'll say on the matter."
      maxine excited "Now, if you'll excuse me."
      window hide
      hide maxine with Dissolve(.5)
      window auto
      "So, [kate] met with someone wearing green? Is that it?"
      "Knowing [maxine], she might've meant something entirely different..."
      "Still, who could that figure in green be?"
  $quest.kate_moment.advance("nurse")
  return

label quest_kate_moment_nurse:
  show nurse neutral with Dissolve(.5)
  window auto show
  show nurse surprised with dissolve2
  nurse surprised "Oh, goodness! You nearly gave me a fright." with vpunch
  "Hmm... she certainly does seem jumpy... and she's always wearing her green scrubs..."
  mc "Hello again, [nurse]."
  mc "Remember when I asked you about [kate] earlier?"
  nurse annoyed "..."
  mc "You were acting like you didn't know what's going on with her."
  mc "But you do, don't you?"
  nurse annoyed "I, um... I don't know..."
  show nurse annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "?nurse['strike_book_activated']@{image=school nurse_room strikebook}|\"Yes, you do.\"":
      show nurse annoyed at move_to(.5)
      mc "Yes, you do."
      nurse concerned "No! I-I mean, I really don't know..."
      "She's definitely hiding something."
      nurse thinking "It's just, err... very unlike her to be absent without saying anything."
      nurse thinking "T-that's all I know."
      mc "Oh, really? And what about your secret meeting with her?"
      nurse concerned "...right, that."
      mc "So, you lied to me?"
      nurse sad "I..."
      mc "That's two strikes. Add it to the book."
      $nurse["strike_book"]+=2
      $nurse.lust+=2
      nurse afraid "Oh, my goodness..."
      mc "Now, tell me what happened, or we're adding another two."
      nurse annoyed "I'm sorry... I really shouldn't tell..."
      mc "Fine. Add two more strikes."
      $nurse["strike_book"]+=2
      nurse surprised "Goodness gracious!"
      mc "We'll keep going until you start talking."
      nurse afraid "Oh, no! Please!"
      mc "Will you tell me, then?"
      nurse sad "O-okay."
      nurse sad "[kate] is having trouble sleeping, the poor dear."
      nurse sad "So, I gave her some pills for it. I swear that's all."
      $quest.kate_moment["four_strikes"] = True
    "\"Is everything okay?\"":
      show nurse annoyed at move_to(.5)
      mc "Is everything okay?"
      nurse concerned "Oh, um... yes, I think so..."
      mc "I'm glad to hear that."
      mc "I know your job is to take care of others, but don't forget to take care of yourself too, okay?"
      $nurse.love+=1
      nurse blush "Gracious, how thoughtful!"
      mc "Look, the reason I'm asking you is because something's going on with [kate]."
      mc "And I believe you might have the answer."
      nurse thinking "..."
      mc "Because you met with her, didn't you?"
      nurse concerned "I, err..."
      mc "And you also gave her something."
      nurse sad "Fine, fine!"
      nurse sad "But you can't tell anyone, okay? I might get in trouble."
      mc "I promise I won't. I just need to know what you gave her."
      nurse sad "[kate] is having trouble sleeping, and the pills they gave her weren't working."
      nurse sad "So, I gave her something stronger."
  "Hmm... that explains the bottle..."
  mc "And you don't know where she is now?"
  nurse concerned "I promise I don't."
  if quest.nurse_aid["name_reveal"]:
    "Well, [amelia] does seem genuine now."
  else:
    "Well, the [nurse] does seem genuine now."
  "Considering no one has seen [kate], she's probably not in school."
  "So, either she's ditching class... or she has permission."
  "And if she does have permission, [jo] would know."
  if quest.kate_moment["four_strikes"]:
    mc "I'll deal with you later."
    nurse sad "I'm sorry..."
  else:
    mc "Thanks for telling me."
  window hide
  hide nurse with Dissolve(.5)
  $quest.kate_moment.advance("jo")
  return

label quest_kate_moment_jo:
  show jo thinking with Dissolve(.5)
  mc "Hey, [jo]!"
  jo worried "Shouldn't you be in class, [mc]?"
  mc "Err, yes... I was just on my way there."
  mc "It's just..."
  jo annoyed "What?"
  mc "I couldn't help but notice [kate] has been absent."
  mc "And I guess you could say I've grown concerned."
  jo thinking "Why are you concerned?"
  show jo thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I like [kate].\"":
      show jo thinking at move_to(.5)
      mc "I like [kate]."
      jo afraid "You do?"
      jo afraid "I didn't know you two were close."
      jo flirty_hands_to_the_side "Are you... close, close?"
      "Pfft! [jo] wishes."
      "Imagine me, with the most popular girl in school..."
      "...or at least, she used to be."
      "Maybe now I would have a chance?"
      mc "A bit, yeah."
      mc "I've been trying to branch out. Expand my social circle and all that."
      $jo.love+=1
      jo sarcastic "That's wonderful, [mc]! You can learn so much from others."
      mc "Err, right."
    "\"What if there's something\ngoing around?\"":
      show jo thinking at move_to(.5)
      mc "What if there's something going around?"
      jo eyeroll "Like what? Mono?"
      mc "Sure... or worse."
      $mc.intellect+=1
      mc "I recently read about this new deadly disease caused by fungus—"
      jo sarcastic "I see."
      jo sarcastic "Well, I can assure you it's nothing like that."
      mc "Nothing contagious, then?"
      jo sarcastic "Not at all."
    "\"She's like a viper.\"":
      show jo thinking at move_to(.5)
      mc "She's like a viper."
      jo cringe "Excuse me? What are you talking about, young man?"
      mc "You never know when she'll strike!"
      mc "She could be lying in wait for me right now..."
      mc "Don't you care about my wellbeing, [jo]?"
      jo displeased "That's enough nonsense, [mc]."
      jo displeased "You have class to get to."
      "Ugh, having [kate]'s back over my own personal safety. Figures."
      mc "...fine."
  mc "So, can you tell me where [kate] is?"
  jo annoyed "I absolutely cannot."
  jo annoyed "This is a confidential matter, and I won't have rumors flying around the school."
  mc "Sorry, sorry!"
  window hide
  hide jo with Dissolve(.5)
  window auto
  "At least she does know [kate]'s whereabouts. That's one point for Detective [mc]."
  "But if she won't tell me, I guess I have no choice..."
  "..."
  "I mean, I do have a choice."
  "But how often is there an opportunity for me to get some potential dirt on [kate]?"
  "I'm going to have to do some digging, and perhaps some breaking and entering."
  "Because if there's one thing I know about [jo]... she'll keep a record of everything."
  $quest.kate_moment.advance("break_in")
  return

label quest_kate_moment_break_in_principal_door:
  "Okay, this is where [jo] keeps all school records."
  "Every little morsel of data sits pretty behind this door."
  "This very locked and unbreakable door."
  "Ugh, how do I get in?"
  $quest.kate_moment["locked_and_unbreakable"] = True
  return

label quest_kate_moment_break_in(item):
  if item == "rock":
    "Hmm... this door is very old..."
    "Maybe some good old bashing will do the trick?"
    window hide
    $mc.remove_item("rock")
    pause 0.25
    play sound "fire_axe_impact"
    show location with hpunch
    pause 0.5
    play sound "fire_axe_impact"
    show location with hpunch
    pause 0.5
    play sound "fire_axe_impact"
    show location with hpunch
    pause 0.5
    play sound "open_door"
    pause 0.75
    show maxine skeptical at appear_from_left
    pause 0.5
    window auto
    maxine skeptical "What do you think you're doing?"
    mc "[maxine]? What were you doing in there?"
    maxine annoyed "Updating the security."
    mc "Err, why?"
    maxine annoyed "It had to be done. Too many snoops around here."
    maxine skeptical "You're not one of them, are you?"
    show maxine skeptical at move_to(.25)
    menu(side="right"):
      extend ""
      "\"Would a snoop bang on the door\nwith a rock?\"":
        show maxine skeptical at move_to(.5)
        mc "Would a snoop bang on the door with a rock?"
        maxine sad "Hmm... that's a very good point..."
        mc "So, what kind of security are we talking about here?"
        maxine thinking "You ask a lot of questions for a regular rock-wielder."
        mc "I'm just curious, that's all! Not trying to break in or anything!"
        maxine thinking "The new security system isn't to stop break ins."
        mc "Err, what's it for, then?"
        maxine laughing "Why, to protect the specimen, of course!"
        maxine laughing "The principal's office was a weak point."
        mc "..."
        maxine smile "Anyway, I have important things to do. Farewell."
        window hide
        show maxine smile at disappear_to_right
        pause 0.5
        window auto
        "Well, I guess that's one way to get in..."
      "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"I'm just a man searching\nfor the truth.\"":
        show maxine skeptical at move_to(.5)
        mc "I'm just a man searching for the truth."
        $maxine.love+=1
        maxine flirty "Which truth?"
        show maxine flirty at move_to(.75)
        menu(side="left"):
          extend ""
          "\"The meaning of love.\"":
            show maxine flirty at move_to(.5)
            $mc.love+=1
            mc "The meaning of love."
            $maxine.love-=1
            maxine cringe "That is not something I'm willing to discuss."
            mc "Why not?"
            maxine cringe "It has no foundation in this place."
            mc "This place?"
            mc "You mean you don't want to date anyone until you graduate?"
            maxine sad "I've taken an oath."
            mc "Damn, I didn't know you were religious."
            maxine thinking "Not religious. Principled."
            mc "Oh, well. I guess I'll just have to save the flowers and chocolate for someone else."
            maxine thinking "Don't wait too long, or they'll wilt."
            window hide
            show maxine thinking at disappear_to_right
            pause 0.5
            window auto
            "I can't tell if [maxine] is being literal or prophetic..."
            "Either way, she left the door unlocked."
          "\"I'm just trying to make\nsense of the world.\"":
            show maxine flirty at move_to(.5)
            mc "I'm just trying to make sense of the world."
            maxine afraid "If you look too deep for meaning—"
            mc "Yeah, I know. You'll go insane."
            maxine thinking "No, you'll find out that you're the only sane one."
            "That's ironic coming from [maxine]..."
            mc "Are you implying you're the only sane one? Prove it, then."
            maxine thinking "That's currently a waste of time. You're not ready."
            maxine thinking "Hopefully, one day you will be..."
            maxine sad "...or not. Most people live out their lives in ignorance."
            mc "Well, I'm doing my best to break the mold here."
            $maxine.lust+=1
            maxine smile "That's the first step."
            mc "And what's the next step?"
            maxine laughing "Probably this way."
            window hide
            show maxine laughing at disappear_to_right
            pause 0.5
            window auto
            "It's definitely not that way... especially now that she left the door unlocked."
    window hide
    $mc.add_item("rock")
    pause 0.25
    $quest.kate_moment.advance("principal_office")
    jump goto_school_principal_office
  elif item == "high_tech_lockpick":
    "This door is too old for such a cutting edge tool."
    "If only I had a battering ram..."
  else:
    "Breaking down this door with my [item.title_lower] seems like a longshot at best... and a crap-shot at worst."
  $quest.kate_moment.failed_item("break_in",item)
  return

label quest_kate_moment_principal_office_door:
  "This might be my only chance to snoop here."
  "I can't leave until I learn [kate]'s whereabouts."
  return

label quest_kate_moment_principal_office:
  "Luckily, [jo] has never been technologically minded."
  "So, the login name is probably \"admin,\" and the password is probably..."
  "...yep, \"password.\""
  "All right, let's see here..."
  "[kate], [kate], [kate]..."
  "..."
  "..."
  "Ah, here we go!"
  "Damn, that's a very small folder. Just a couple of entries."
  "I guess she's never been sick in her life."
  "Hmm... there's an email from some doctor... addressed to [jo]."
  "{i}\"As we discussed earlier over the phone, I've scheduled one appointment per week for [kate].\"{/}"
  "{i}\"This appointment will be during school hours for privacy reasons, as per her request.\"{/}"
  "{i}\"It's my understanding that her schedule is flexible enough to account for this until I see improvement.\"{/}"
  "{i}\"With regards, Dr. Clamp.\"{/}"
  "Huh..."
  "So, she's at a doctor's appointment right now."
  "That's very interesting. I wonder what's wrong with her."
  "Maybe she hit her head during cheer practice or something?"
  "Or could it really be that we somehow rattled her?"
  "..."
  "In any case, that's one day less of [kate] every week. This requires some celebration!"
  if quest.isabelle_dethroning["isabelle_caught"]:
    "[isabelle] will be thrilled. I better send her a te—"
    "..."
    "Right, [isabelle] still isn't talking to me after I fucked [kate]."
    "I should probably try to patch that up first..."
#   python:
#     kate["at_none"] = False
#     mc["focus"] = ""
#   window hide
#   python:
#     quest.kate_moment.hidden = True
#     game.quest_guide = ""
#     game.notify_modal(None,"Coming soon","{image=isabelle contact_icon}{space=25}|"+"The Grand Gesture is\ncurrently being worked on!\n\nPlease consider\nsupporting us on Patreon.|{space=25}{image=items monkey_wrench}",5.0)
#   pause 0.25
    $quest.kate_moment.advance("coming_soon")
  else:
    "[isabelle] will be thrilled. I better send her a text."
    $quest.kate_moment.advance("text")
  return

label quest_kate_moment_text:
  if quest.isabelle_gesture.finished:
    $set_dialog_mode("phone_message_centered","isabelle")
    mc "Hey! I forgot to tell you."
    mc "I have some good news."
    isabelle "And what's that?"
    if quest.isabelle_gesture["revenge"]:
      window hide None
      window auto
      $set_dialog_mode("phone_message_plus_textbox")
      "It feels a bit awkward texting [isabelle] after watching her kiss [kate]..."
      "But she's doing a good job of keeping the mask up."
      "..."
      "I guess that's what's needed until we can both move past it."
      "Maybe it'll take a while for our hearts to mend, but she's worth it."
      "And honestly, I'm happy pretending that everything is fine for as long as it takes to make it a reality."
      window hide
    jump quest_kate_moment_text_missing_school
  else:
    $set_dialog_mode("phone_message_centered","isabelle")
    mc "Hey! I have some good news."
    isabelle "And what's that?"
    $set_dialog_mode("phone_message","isabelle")
    menu(side="middle"):
      "\"[kate] is going to be missing school one day per week.\"":
        label quest_kate_moment_text_missing_school:
          $set_dialog_mode("phone_message_centered","isabelle")
          mc "[kate] is going to be missing school one day per week."
          mc "That's twenty percent\nless [kate]!"
          isabelle "That is good news!"
          isabelle "The less I see of that miserable slag, the better."
          mc "I thought you would approve."
          isabelle "I most certainly do."
          isabelle "Thanks for letting me know!"
          mc "Any idea why she'd be seeing a doctor once a week?"
          isabelle "A weekly consultation does sound weird..."
          mc "Cancer, maybe?"
          isabelle "I don't think she'd come to school at all, then."
          isabelle "Have you noticed anything unusual about her?"
          isabelle "It might give us a clue."
          mc "Well, I did find her sleeping in a chair earlier..."
          mc "...and a bottle of pills."
          isabelle "What kind of pills?"
          mc "Sleeping pills, I think?"
          isabelle "Sounds like it could be\ntherapy, then."
          isabelle "I was having trouble sleeping right after my sister died."
          isabelle "This sounds a lot like that."
          mc "Hmm... you might be right..."
          mc "I wonder who died."
          isabelle "Honestly, she probably just broke a nail."
          mc "Damn! Savage!"
          mc "Real talk, though..."
          mc "Could it be that we gave her lasting nightmares?"
          isabelle "Now that you mention it..."
          isabelle "I had almost forgotten\nabout that, what with\n[lindsey] jumping and\nthat whole thing."
          isabelle "This is very interesting."
          isabelle "Thanks for letting me know!"
          mc "No problemo."
          window hide None
          $set_dialog_mode("")
          pause 0.5
          window auto
          "So, therapy, huh?"
          "I feel like digging deeper, but knowing [kate], that's probably not a good idea."
          "I guess I'll just return her pills tomorrow, and leave it at that."
          $quest.kate_moment.advance("sleep")
      "?quest.isabelle_locker.actually_finished and (not quest.isabelle_locker['time_for_a_change'] or quest.isabelle_hurricane.actually_finished) and isabelle.lust>=10@|{image=isabelle_hurricane_sex}|{color=#000}+{/}|[isabelle.lust]/10|{image=isabelle contact_icon}|{image=stats lust_3}|\"I'll tell you in person if{space=-20}\nyou come over tonight.\"{space=-25}" if quest.isabelle_locker.actually_finished and quest.isabelle_hurricane.actually_finished:
        jump quest_kate_moment_text_come_over
      "?quest.isabelle_locker.actually_finished and (not quest.isabelle_locker['time_for_a_change'] or quest.isabelle_hurricane.actually_finished) and isabelle.lust>=10@|{image=isabelle_locker_sex}|{color=#000}+{/}|[isabelle.lust]/10|{image=isabelle contact_icon}|{image=stats lust_3}|\"I'll tell you in person if{space=-20}\nyou come over tonight.\"{space=-25}" if (quest.isabelle_locker.actually_finished and not quest.isabelle_locker["time_for_a_change"]) or not quest.isabelle_locker.actually_finished:
        label quest_kate_moment_text_come_over:
          $set_dialog_mode("phone_message_centered","isabelle")
          mc "I'll tell you in person if you come over tonight."
          isabelle "{image=phone emojis smirk}"
          mc "[jo] will be out as well, I think."
          mc "We could have some fun..."
          isabelle "I do like fun."
          mc "Come on over, and we will!"
          isabelle "Great! See you then!"
          isabelle "{image=phone emojis kissing_heart}"
          window hide None
          $set_dialog_mode("")
          pause 0.5
          $quest.kate_moment.advance("fun")
  return

label quest_kate_moment_principal_office_return:
  "I feel like digging deeper, but knowing [kate], that's probably not a good idea."
  if quest.kate_moment == "text":
    "[isabelle] will be thrilled nonetheless. I better send her a text!"
  elif quest.kate_moment == "coming_soon":
    "I guess I'll just return her pills once I patch things up with [isabelle], and leave it at that."
  else:
    "I guess I'll just return her pills tomorrow, and leave it at that."
  return

label quest_kate_moment_fun:
  if game.location == "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom":
    show isabelle
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  $home_hall["exclusive"] = home_bedroom["exclusive"] = "isabelle"
  call screen time_passed
  pause 0.5
  if game.location == "home_hall" and quest.maya_sauce["bedroom_taken_over"]:
    show isabelle laughing at appear_from_left
    pause 0.5
    isabelle laughing "Hey! Did I make you wait long?"
    mc "No, not really..."
    jump quest_kate_moment_fun_isabelle
  elif game.location == "home_bedroom":
    play sound "open_door"
    pause 0.75
    show isabelle laughing at appear_from_left
    pause 0.5
    isabelle laughing "Hey! Did I make you wait long?"
    mc "No, not really..."
    jump quest_kate_moment_fun_isabelle
  else:
    "It's getting late. Time to have some fun with [isabelle]."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    if quest.maya_sauce["bedroom_taken_over"]:
      call goto_home_hall
    else:
      call goto_home_bedroom
    $quest.kate_moment["stopped_by_early"] = True
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
    return

label quest_kate_moment_fun_bed:
  "Yes, that's the goal. We'll see if I can score."
  return

label quest_kate_moment_fun_door:
  "[isabelle] is here already. It would be rude to leave her unattended."
  return

label quest_kate_moment_fun_stairs:
  "[isabelle] is here already. It would be rude to leave her unattended."
  return

label quest_kate_moment_fun_bathroom:
  "[isabelle] is here already. It would be rude to leave her unattended."
  return

label quest_kate_moment_fun_isabelle:
  if quest.kate_moment["stopped_by_early"]:
    show isabelle smile with Dissolve(.5)
    isabelle smile "There you are!"
    isabelle smile "I decided to stop by early. [flora] let me in."
    mc "Sorry! Did you have to wait long?"
    isabelle laughing "Ages!"
    isabelle confident "No, not really. I just got here."
  if quest.maya_sauce["bedroom_taken_over"]:
    if quest.kate_moment["stopped_by_early"]:
      isabelle confident "What's going on, by the way? Why is your bed out in the hallway?"
    else:
      isabelle confident "What's going on here? Why is your bed out in the hallway?"
    mc "Oh, uh. [maya] will be staying with us for a while."
    mc "We're helping her due to some problems at home."
    isabelle laughing "Oh! And you gave her your room? You're such a gentleman!"
    mc "Err, yep. Sort of."
    "That does sound better than saying I lost a stupid bet..."
  isabelle confident "So, you said you had some good news?"
  mc "I did say that."
  isabelle confident "Well, you got me over here."
  mc "I sure did."
  isabelle laughing "Haha! Is that where your plan ends?"
  mc "...what plan?"
  mc "Oh! Unless you mean the ruse to get you here simply to bask in your presence?"
  isabelle laughing "Why, of course!"
  mc "Okay, fine. That's not actually my plan."
  isabelle confident "Oh? What is your plan, then?"
  mc "Something like... this."
  window hide
# show isabelle bed_kiss startled
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens
  play sound "<from 0.2>bedsheet"
  show isabelle bed_kiss startled: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  with Dissolve(.5)
  hide isabelle
  show isabelle bed_kiss startled ## This is just to avoid the screen shake if the player is skipping
  window auto
  "With a single step forward, I lean in, taking [isabelle] in my arms."
  "She gasps in surprise as I lay her down on the bed."
  "In a moment of astonishment at my boldness, our lips meet."
  "So soft. A hint of sweet vanilla."
  window hide
  pause 0.125
  show isabelle bed_kiss worried with Dissolve(.5)
  pause 0.125
  window auto
  isabelle bed_kiss worried "[mc]..."
  mc "[isabelle]."
  isabelle bed_kiss excited "I liked that."
  mc "Yeah?"
  window hide
  pause 0.125
  show isabelle bed_kiss eyes_closed with Dissolve(.5)
  pause 0.125
  window auto
  "Our lips meet again, and this time she's ready."
  "This time she kisses me right back."
  "Her chest presses against mine."
  "The warmth of her skin seeps into me, and it feels like I could hold her forever."
  window hide
  pause 0.125
  show isabelle bed_kiss excited with Dissolve(.5)
  pause 0.125
  window auto
  "Slowly, she withdraws and looks up at me, her breathing uneven, her pupils dilated."
  isabelle bed_kiss excited "I really liked that..."
  mc "I really like you, [isabelle]."
  "I say her name with unwavering confidence. In her, in us, in this moment."
  "I like the way it forms on my tongue, each syllable carefully enunciated."
  "She blushes and smiles."
  "An invitation to keep going."
  window hide
  pause 0.125
  show isabelle bed_kiss eyes_closed with Dissolve(.5)
  pause 0.125
  window auto
  "It's a moment that will be preserved in my mind forever, I just know it.{space=-40}"
  "We kiss each other to our hearts' content."
  "A lustful dance of our lips and tongues."
  if quest.maya_sauce["bedroom_taken_over"]:
    window hide
    pause 0.125
    show isabelle bed_kiss worried with Dissolve(.5)
    pause 0.125
    window auto
    # "Then suddenly, she draws back, dropping me headfirst into a moment of confused vertigo."
    "Then suddenly, she draws back, dropping me headfirst into a moment{space=-40}\nof confused vertigo."
    mc "What's wrong?"
    isabelle bed_kiss worried "We're going to get caught. It's a certainty."
    # "She's probably right. Anyone could walk up the stairs at any moment."
    "She's probably right. Anyone could walk up the stairs at any moment.{space=-30}"
    "We have been risky in the past, but the odds of getting caught here are much higher."
    "And I want to take my time with her without interruption."
    mc "You're right. I have an idea."
  window hide
  hide screen interface_hider
  show isabelle bed_sex exposed
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  if quest.maya_sauce["bedroom_taken_over"]:
    $mc.add_message_to_history("flora", mc, "I need a huge favor. It's life or death, basically.")
    $mc.add_message_to_history("flora", mc, "I need my room for the night without [maya] in it.")
    $mc.add_message_to_history("flora", flora, "Gross.")
    $mc.add_message_to_history("flora", mc, "Please? I really need this.")
    $mc.add_message_to_history("flora", flora, "Ugh, fine. But it will cost you.")
    $mc.add_message_to_history("flora", flora, "Not only in VIP movie tickets, but also perhaps in future favors and concessions.")
    $mc.add_message_to_history("flora", mc, "Anything! Just keep her out of my room until tomorrow, okay?")
    $mc.add_message_to_history("flora", flora, "Deal, but expect to pay up later.")
    $mc.add_message_to_history("flora", flora, "Also, gross.")
    $mc.add_message_to_history("flora", mc, "Thank you, [flora]!")
    play sound "<from 0.25 to 1.0>phone_vibrate"
    $set_dialog_mode("default_no_bg")
    "A message and a bribe to [flora] to keep [maya] away. An all night marathon at the movies."
    play sound "lock_click"
    "In the meantime, [isabelle] unlocks the door to my room with a hairpin, British spy agent style."
    "It all goes so quick."
    $game.location = "home_bedroom"
  $set_dialog_mode("default_no_bg")
  "Suddenly, we're undressing. It just happens naturally."
  "An eager moment here and there between the kisses..."
  "...the clothes come off without me even registering it..."
  "...and then suddenly we're naked."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  isabelle bed_sex exposed "Goodness, [mc]..."
  "She gives a tiny gasp, surprised by the way I left her exposed\nall of a sudden."
  "I love it when she says my name. I can never get enough of it."
  "Even better when she's screaming it, pleading for my dick in her pussy.{space=-65}"
  window hide
  pause 0.125
  show isabelle bed_sex pulled_closer with Dissolve(.5)
  pause 0.125
  window auto
  if quest.maya_sauce["bedroom_taken_over"]:
    "I drag her forcefully to the end of [maya]'s bed and spread her legs wide."
  else:
    "I drag her forcefully to the end of my bed and spread her legs wide."
  "Her glistening flower is dripping with her dewy arousal, open and pink and pulsing for me."
  mc "God, that's beautiful."
  window hide
  pause 0.125
  show isabelle bed_sex licking with Dissolve(.5)
  pause 0.125
  window auto
  "Unable to resist the need to have a taste, I drop to my knees."
  "My tongue pokes out and caresses her folds with just the tip."
  "I start from the outside and work my way in, lapping at her wetness.{space=-5}"
  "[isabelle] shudders at my touch."
  isabelle bed_sex licking "Jesus, [mc]..."
  "The panting and writhing increases as I swirl my tongue around her clit, never actually touching."
  show isabelle bed_sex sucking with dissolve2
  "Then, just as she releases a shuddering moan, I dive in fully."
  "My tongue penetrates her folds, and her juices rush into my mouth."
  "It's a mix of sweet and salt, and pure feminine goodness."
  window hide
  pause 0.125
  show isabelle bed_sex anticipation with Dissolve(.5)
  pause 0.125
  window auto
  "She moans in frustration as my mouth leaves her pussy."
  "My dick rests against the opening of her leaking slit."
  "Eyes wide, she looks up at me, perfectly open and ready."
  "She gasps and squirms beneath me, growing impatient."
  isabelle bed_sex anticipation "Please... I need you inside me..."
  "The force of what she says slams into me like an avalanche and steals my breath away."
  "[isabelle], needing me, wanting me."
  "I can hardly deny either of us any longer."
  window hide
  pause 0.125
  show isabelle bed_sex penetration1 with Dissolve(.5)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with hpunch
  pause 0.125
  window auto
  "Happily, desperately, I slam my dick full force into her eager hole."
  isabelle bed_sex penetration4 "Ooooh!"
  "She moans and tilts her head back against the mattress."
  "Her pussy clenches down around me, making her even tighter."
  "Slowly, inch by desperate inch, I push into her."
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration2 with Dissolve(.2)
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration4 with Dissolve(.2)
  pause 0.175
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration2 with Dissolve(.2)
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration4 with Dissolve(.2)
  pause 0.175
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration2 with Dissolve(.2)
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration4 with Dissolve(.2)
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration2 with Dissolve(.2)
  show isabelle bed_sex penetration3 with Dissolve(.2)
  show isabelle bed_sex penetration4 with Dissolve(.2)
  pause 0.125
  window auto
  isabelle bed_sex penetration4 "Yes, god, yes! I need it!"
  mc "Here you go, baby!"
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.125
  window auto
  "I start to thrust harder, faster."
  "I grip her ankles as I pound into her."
  "Turn my head to the side and kiss the top of her delicate foot\nas I do."
  "I've found my rhythm now, the momentum of my thrusts rocking her whole body against the bed."
  "I look down at her, her eyes rolling halfway into the back of her head, her mouth open in a silent scream."
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with hpunch
  pause 0.125
  window auto
  mc "You..."
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with hpunch
  pause 0.125
  window auto
  extend " like..."
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with hpunch
  pause 0.125
  window auto
  extend" that...?"
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with hpunch
  pause 0.125
  window auto
  "I punctuate each word with another thrust."
  "The bed shakes and squeaks beneath us and I briefly take the time to hope [jo] isn't home."
  if quest.maya_sauce["bedroom_taken_over"]:
    "And pray that [flora] will keep [maya] busy... while I rail [isabelle] in her bed."
    "It's a little twisted, but that extra thrill seems to get us both going."
  isabelle bed_sex penetration4 "Mmm, yes! P-please, don't stop!"
  "Her words are coming in ragged pants now, each one drawn out as she whines for more."
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration1 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration4 with Dissolve(.15)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration1 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration4 with Dissolve(.15)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration1 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration4 with Dissolve(.15)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration1 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration4 with Dissolve(.15)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration1 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration4 with Dissolve(.15)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration1 with Dissolve(.15)
  show isabelle bed_sex penetration2 with Dissolve(.15)
  show isabelle bed_sex penetration3 with Dissolve(.15)
  show isabelle bed_sex penetration4 with Dissolve(.15)
  pause 0.125
  window auto
  "I slam my hips into hers, thrusting and grinding into her pussy as it swallows me whole."
  "I'm desperate to be as close to her as two people can get."
  "[isabelle] moans beneath me and her pussy begins to contract and pulsate with her building orgasm."
  "She grips the sheets in her hands, practically arching off the bed as her head rolls back."
  isabelle bed_sex penetration4 "Oh, god! Oh, god, it's coming!"
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.075
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration1 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.125
  window auto
  "As my own orgasm builds, I look down into her face."
  "Her eyes are rolling back, her tongue hanging out as she whimpers and whines for more."
  "Our juices mingling and soaking us both."
  window hide
  pause 0.125
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration4 with Dissolve(.1)
  pause 0.025
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex penetration2 with Dissolve(.1)
  show isabelle bed_sex penetration3 with Dissolve(.1)
  show isabelle bed_sex orgasm with hpunch
  pause 0.125
  window auto
  isabelle bed_sex orgasm "Ohhhh!"
  "She cums hard around my dick, eyes rolling back, coherent thoughts evaporating in an instant."
  "Unable to hold myself back any longer, I explode inside her."
  "My dick spurting and tingling within her tightness."
  isabelle bed_sex orgasm "D-did you just...?"
  mc "F-fuck..."
  "For a moment, I worry she'll be mad I came inside her without asking..."
  window hide
  pause 0.125
  show isabelle bed_sex aftermath with Dissolve(.5)
  pause 0.125
  window auto
  "...but then an exhausted smile spreads across her flushed face as she continues to tremble with the aftershocks of her orgasm."
  isabelle bed_sex aftermath "That was... amazing."
  mc "{i}You're{/} amazing..."
  "She giggles, takes my hand, and just holds it for a moment."
  "Her fingers wrapped in mine, the silence not awkward, but warm and welcome."
  "It's special to feel so close and comfortable to a girl like [isabelle]."
  "Just basking in the calm serenity of each other's company."
  "Especially after mind blowing sex."
  window hide
  show isabelle bed_after blush
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $game.advance()
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "We lie down for what feels like hours, her head on my chest, just chatting about anything and everything."
  "Our hands still locked together."
  "And as night creeps in and I grow sleepy, I can't help but think of [kate].{space=-50}"
  mc "So, I actually did have some good news. I just wanted to fuck you first.{space=-55}"
  isabelle bed_after blush "I did like that order of things..."
  mc "It's about [kate]."
  isabelle bed_after worried "Really? You want to talk about [kate]?"
  mc "Well, apparently, she's seeing a doctor once a week and will miss school regularly."
  mc "Less [kate] for everyone!"
  isabelle bed_after smile "That is indeed quite good."
  isabelle bed_after smile "Do you know why?"
  mc "I have a few ideas, but I was kind of hoping to hear what you think."
  isabelle bed_after thinking "Hmm... once a week..."
  isabelle bed_after worried "That's pretty unusual. It might be something serious."
  isabelle bed_after worried "Do you have any other clues?"
  mc "Sort of? I found her sleeping in one of the chairs upstairs."
  isabelle bed_after thinking "That is odd."
  mc "I also found a bottle of strong sleeping pills that belongs to her."
  isabelle bed_after worried "Interesting... I guess it could be mental health related, then."
  isabelle bed_after worried "Maybe something's eating her up."
  mc "Oh, you mean like one of the million bad things she has done?"
  mc "Sounds like a stretch."
  isabelle bed_after smile "You're probably right."
  isabelle bed_after smile "What exactly are you thinking, then?"
  menu(side="right"):
    extend ""
    "\"I bet it's because she's developed\na split personality.\"":
      mc "I bet it's because she's developed a split personality."
      isabelle bed_after worried "Bollocks. Her one personality is plenty."
      mc "It could be a good thing, then."
      mc "Imagine a nice [kate]..."
      "This might be the actual power vacuum."
      "Power over [kate]..."
      isabelle bed_after worried "Sounds pretty far-fetched."
      mc "I guess."
    "\"I wonder if it's because she's\nsecretly gay...\"":
      mc "I wonder if it's because she's secretly gay..."
      $mc.charisma+=1
      mc "We could test it by getting you and her together and—"
      isabelle bed_after excited "Oh, piss off, mate!"
      mc "Damn! Be careful not to get yourself canceled!"
      isabelle bed_after worried "It's not the fact that she's a girl."
      isabelle bed_after worried "It's the fact that she's [kate]."
      mc "Heh, fair enough."
      mc "Can't blame a guy for fantasizing..."
      $isabelle.love-=1
      isabelle bed_after worried "I bloody well can."
    "\"Maybe the pills are just placebos?\"":
      mc "Maybe the pills are just placebos?"
      isabelle bed_after worried "Why would they be?"
      mc "It could be safer than the other potential options."
      $mc.intellect+=1
      mc "I read that they can be good for stress management."
      isabelle bed_after thinking "Hmmm..."
  mc "Well, what do you think is going on?"
  isabelle bed_after excited "We did scare the bloody daylights out of her at the dinner party."
  isabelle bed_after excited "I bet she's been so inside out, she can't sleep."
  mc "And now she has to take sleeping pills..."
  mc "In all seriousness, I had that thought too. And it does sort of add up.{space=-20}"
  isabelle bed_after smile "Mm-hmm."
  isabelle bed_after smile "Did you hear she's also out of the cheer squad?"
  mc "I did, yeah."
  isabelle bed_after smile "We really screwed her over, didn't we?"
  mc "We sure did... I just didn't think it would affect her this much."
  isabelle bed_after excited "Well, I'm glad she got some proper lasting comeuppance."
  mc "I guess she deserved as much..."
  isabelle bed_after smile "She definitely did."
  isabelle bed_after smile "But enough about [kate]."
  mc "Err, right."
  window hide
  pause 0.125
  show isabelle bed_after eyes_closed with Dissolve(.5)
  pause 0.125
  window auto
  "It's just that... I can't stop thinking about her."
  "How ironic is that?"
  if quest.maya_sauce["bedroom_taken_over"]:
    "You finally have the perfect girl in a bed... and then this happens."
  else:
    # "You finally have the perfect girl in your bed... and then this happens."
    "You finally have the perfect girl in your bed... and then this happens.{space=-5}"
  "Damn it, these thoughts could be costly! A million pennies or more."
  $unlock_replay("isabelle_fun")
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  pause 1.0
  while game.hour != 7:
    $game.advance()
  $home_hall["exclusive"] = home_bedroom["exclusive"] = ""
  $kate["at_none"] = False
  hide isabelle
  hide black onlayer screens
  with Dissolve(.5)
  if quest.maya_sauce["bedroom_taken_over"]:
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5
    window auto
    "Mmm..."
    "It feels good waking up in your own room for once."
    "Without nightmares or anything. Simply fulfilled by a night of incredible sex."
    "Man, I bet this is how Chad feels every day."
    "But all good things must come to an end, and it's probably time to go before [maya] gets back."
    window hide
    scene black with Dissolve(.07)
    $game.location = "home_hall"
    $renpy.pause(0.07)
    $renpy.get_registered_image("location").reset_scene()
    scene location with Dissolve(.5)
  $quest.kate_moment.advance("next_day")
  return

label quest_kate_moment_next_day:
  "There she is again..."
  "Sleeping during the day, in the most crowded corridor."
  mc "Hey, [kate]."
  kate "Mmm...?"
  window hide
  show kate sad_tired with Dissolve(.5)
  window auto
  kate sad_tired "What do you want?"
  mc "You look great."
  mc "No, wait, what's the opposite of great?"
  kate gushing_tired "You."
  "Crap, she's good even when sleep deprived..."
  mc "I've got to give it to you—"
  window hide None
  play sound "light_switch"
  hide screen interface_hider
  show kate really_scared
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Huh? That's weird."
  "It must be a blown fuse again."
  kate "N-noooo...."
  "As the darkness settles around us, a trembling whine pierces the silence."
  "The sheer despair and terror in her voice is heartbreaking."
  "I never thought I'd hear [kate] whimper, but there it is..."
  mc "Hey, it's just a fuse, okay?"
  "It feels weird to be comforting my worst enemy, but she probably isn't even listening."
  "She's hyperventilating and gripping my arm like it's the last parachute{space=-35}\non a crashing plane."
  show black onlayer screens zorder 100
  play sound "<from 20.2>light_flicker"
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Perhaps, a lot of things led up to this..."
  "But it's when the lights turn back on, and everything freezes, that\nit hits me in full."
  "It's in this fraction of a second, in this single moment, that everything changes."
  "And we both know it."
  "The world is split into before and after this moment, and we're balancing on the edge."
  "But then a bird chirps outside the window, and—"
  window hide
  show kate angry_tired
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  kate angry_tired "What the hell? Why did you put your arm there?!"
  kate angry_tired "I've told you to not come close!"
  kate angry_tired "Fuck you!"
  window hide
  show kate angry_tired at disappear_to_left(.75)
  pause 0.5
  window auto
  "...and time resumes its regular flow."
  "But something is different now. Very different."
  "..."
  "And I didn't even get the chance to return her pills..."
  $mc["focus"] = ""
  window hide
  $quest.kate_moment.finish()
  return
