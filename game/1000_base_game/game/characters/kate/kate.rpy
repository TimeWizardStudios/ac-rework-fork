init python:

  class Character_kate(BaseChar):
    notify_level_changed=True

    default_name="Kate"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="kate contact_icon"

    default_outfit_slots=["necklace","shirt","bra","pants","panties","boots"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("kate_necklace")
      self.add_item("kate_choker")
      self.add_item("kate_angel_costume")
      self.add_item("kate_shirt")
      self.add_item("kate_cheerleader_top")
      self.add_item("kate_bardot_top")
      self.add_item("kate_bra")
      self.add_item("kate_cheerleader_bra")
      self.add_item("kate_blue_bra")
      self.add_item("kate_pants")
      self.add_item("kate_cheerleader_skirt")
      self.add_item("kate_skirt")
      self.add_item("kate_panties")
      self.add_item("kate_cheerleader_panties")
      self.add_item("kate_blue_panties")
      self.add_item("kate_knee_high_boots")
      self.equip("kate_necklace")
      self.equip("kate_shirt")
      self.equip("kate_bra")
      self.equip("kate_pants")
      self.equip("kate_panties")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

      ###Forced Locations
      if quest.jo_washed.in_progress:
        if quest.jo_washed["kate_location"] == "school_gym":
          self.location = "school_gym"
          self.activity = "car_wash_signs"
        else:
          self.location = None
          self.activity = None
        return

      if kate["at_none_this_scene"] or kate["at_none_now"] or kate["at_none_today"] or  kate["at_none"]:
        self.location = None
        self.alternative_locations={}
        return

      if quest.mrsl_table in ("morning","hide"):
        self.location=None
        self.activity=None
        return

      if quest.kate_blowjob_dream == "school":
        self.location="school_gym"
        self.activity="standing"
        return

      if quest.kate_fate in ("peek","hunt","hunt2","deadend","rescue","escaped"):
        self.location = None
        self.alternative_locations={}
        return

      if quest.kate_desire in ("advice","rest","dress","starblue","starred","stargreen","starblack","stargold","bathroomhelp","encounter"):
        self.location = None
        self.alternative_locations={}
        return
      elif quest.kate_desire in ("bathroomhelp","gym"):
        self.location="school_gym"
        self.activity="standing"
        self.alternative_locations={}
        return
      elif quest.kate_desire == "holdup":
        self.location="school_first_hall_west"
        self.activity="standing"
        self.alternative_locations={}
        return

      if (quest.kate_over_isabelle == "the_winning_team" or
         (quest.isabelle_tour >= "go_upstairs" and quest.isabelle_tour < "english_class")):
        self.location="school_first_hall"
        self.activity="standing"
        return

      if quest.day1_take2 < "isabelle_volunteer":
        self.location="school_homeroom"
        self.activity="sitting"
        return

      if quest.kate_search_for_nurse>"meet_kate" and quest.kate_search_for_nurse.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.isabelle_stolen == "eavesdrop":
        self.location = None
        self.activity = None
        return

      if quest.lindsey_wrong in ("verywet", "guard", "mopforkate", "cleanforkate"):
        self.location = "school_first_hall"
        self.activity = "standing"
        return

      if quest.maxine_hook in ("day","interrogation"):
        self.location = "school_cafeteria"
        self.activity = "sitting"
        return

      if quest.isabelle_hurricane == "blowjob" and not quest.isabelle_hurricane["cocksucker"]:
        self.location = "school_first_hall_east"
        self.activity = "sitting"
        return

      if quest.kate_trick.in_progress:
        self.location = "school_gym"
        self.activity = "standing"
        return

      if quest.isabelle_dethroning == "trophy_case":
        self.location = "school_homeroom"
        self.activity = "sitting"
        return

      if quest.fall_in_newfall.in_progress and quest.fall_in_newfall < "stick_around":
        self.location = "school_homeroom"
        self.activity = "sitting"
        return

      if (quest.fall_in_newfall.finished and not quest.maya_witch.started) and game.hour in (17,18):
        self.location = "school_homeroom"
        self.activity = "sitting"
        return

      if quest.maya_spell == "hair" and quest.isabelle_dethroning.finished:
        self.location = "school_first_hall"
        self.activity = "standing"
        return
      elif quest.maya_spell == "maya" and game.hour == 12:
        self.location = "school_nurse_room"
        self.activity = "sitting"
        return

      if (((quest.isabelle_dethroning.finished and quest.fall_in_newfall >= "stick_around" and not (quest.kate_moment.started or quest.kate_moment.failed))
      or kate["sleeping_this_scene"]
      or quest.kate_moment == "next_day")
      and not quest.mrsl_bot == "dream"):
        self.location = "school_first_hall_west"
        self.activity = "sleeping"
        if not quest.maya_sauce in ("classes","dinner"):
          return

      if quest.isabelle_gesture == "revenge":
        self.location = "school_english_class"
        return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return

      if ((quest.nurse_aid == "someone_else" and quest.nurse_aid["jacklyn_on_board"]) or quest.nurse_aid == "relationship") and game.hour in (10,11,13,14,15):
        self.location = "school_cafeteria"
        self.activity = "sitting"
        return
      ###Forced Locations

      ###ALT Locations
      if quest.kate_over_isabelle == "report" or (quest.isabelle_tour == "cafeteria" and quest.kate_over_isabelle.in_progress):
        self.alternative_locations["school_cafeteria"] = "sitting"

      if quest.kate_search_for_nurse == "meet_kate" and not quest.flora_bonsai.in_progress:
        self.alternative_locations["school_nurse_room"] = "sitting"

      if game.season == 1:
        if quest.kate_desire["nude_model"] and game.hour in (16,17,18):
          self.alternative_locations["school_art_class"] = "pose"

      if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and sum([mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills"),mc.owned_item("handcuffs")]) >= 2 and not quest.kate_trick.started:
        self.alternative_locations["school_first_hall"] = "standing"

      if quest.kate_stepping == "gym":
        self.alternative_locations["school_gym"] = "standing"

      if quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"]:
        self.alternative_locations["school_first_hall_west"] = "waiting"

      if quest.isabelle_dethroning == "announcement" or (quest.isabelle_dethroning == "dinner" and game.hour == 19 and not quest.isabelle_dethroning["kitchen_shenanigans"]):
        self.alternative_locations["school_cafeteria"] = "sitting"

      if quest.maya_witch == "gym_class" and quest.kate_stepping.finished:
        self.alternative_locations["school_gym"] = "standing"

      if quest.maya_sauce in ("classes","dinner"):
        self.alternative_locations["school_homeroom"] = "sitting"
        if (((quest.isabelle_dethroning.finished and quest.fall_in_newfall >= "stick_around" and not (quest.kate_moment.started or quest.kate_moment.failed))
        or kate["sleeping_this_scene"]
        or quest.kate_moment == "next_day")
        and not quest.mrsl_bot == "dream"):
          return
      ###ALT Locations

      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):

        if game.hour in (7,8):
          self.location = "school_homeroom"
          self.activity = "sitting"
          return

        elif game.hour in (9,16):
          if (not True in (school_first_hall_east["shoe1"],school_first_hall_east["shoe2"],school_first_hall_east["shoe3"],school_first_hall_east["shoe4"],school_first_hall_east["shoe5"],school_first_hall_east["shoe6"],school_first_hall_east["shoe7"],school_first_hall_east["shoe8"],school_first_hall_east["shoe9"],school_first_hall_east["shoe10"],school_first_hall_east["shoe11"])
          or quest.isabelle_dethroning.finished):
            self.location = "school_homeroom"
            self.activity = "sitting"
          else:
            self.location = "school_first_hall_east"
            self.activity = "sitting"
          return

        elif game.hour == 12:
          self.location = "school_cafeteria"
          self.activity = "sitting"
          return

        elif game.hour in (10,11,13,14,15) and not (quest.lindsey_nurse.in_progress or quest.flora_bonsai.in_progress or quest.nurse_photogenic.in_progress):
          if quest.nurse_venting > "supplies" and quest.nurse_venting.in_progress:
            self.location = "school_gym"
            self.activity = "standing"
          else:
            self.location = "school_nurse_room"
            self.activity = "sitting"
          return

        elif game.hour == 17:
          if quest.isabelle_dethroning.finished:
            self.location = "school_first_hall"
            self.activity = "standing"
          else:
            self.location = "school_gym"
            self.activity = "standing"
          return

        elif game.hour == 18:
          self.location = "school_first_hall"
          self.activity = "standing"
          return

        else:
          self.location = None
          self.activity = None
      ### Schedule

      ## if no ai/schedule condition was met, use next defaults
      #self.location=None
      #self.activity=None
      #self.activity_duration=0
      #self.alternative_locations={}
    def call_label(self):
      if mc["focus"]:
        if mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "phone_call":
            return "quest_kate_stepping_phone_call_kate"
      else:
        if quest.isabelle_buried == "callforhelp":
          return "quest_isabelle_buried_callforhelp_kate"
      return None

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("kate avatar "+state,True):
          return "kate avatar "+state
      rv=[]

      if state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((670,1080))
        rv.append(("kate avatar body1",117,99))
        if state.startswith("confident_right"):
          rv.append(("kate avatar face_confident_right",230,208))
        else:
          rv.append(("kate avatar face_confident",229,208))
        if "_cheerleader_top" in state:
          if "_rope" not in state:
            if "_paddle" in state:
              rv.append(("kate avatar b1arm1_paddle",17,387))
            elif "_mask" in state:
              rv.append(("kate avatar b1mask_n",28,202))
            else:
              rv.append(("kate avatar b1arm1_n",17,387))
        if "_cheerleader_bra" in state:
          if "_cheerleader_top" not in state:
            rv.append(("kate avatar b1arm1_n",17,387))
          rv.append(("kate avatar b1cheerleader_bra",224,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b1blue_bra",224,522))
        elif "_bra" in state:
          rv.append(("kate avatar b1bra",216,371))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b1cheerleader_panties",147,738))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b1blue_panties",139,736))
        elif "_panties" in state:
          rv.append(("kate avatar b1panty",143,747))
        if "_phone" in state:
          rv.append(("kate avatar b1phone_n",121,233))
        elif "_shirt" not in state and "_cheerleader_top" not in state and "_angel_costume" not in state:
          rv.append(("kate avatar b1arm1_n",17,387))
        if "_pants" in state:
          rv.append(("kate avatar b1pants",41,526))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b1cheerleader_skirt",90,735))
        elif "_skirt" in state:
          rv.append(("kate avatar b1skirt",114,645))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b1knee_high_boots",118,983))
        if "_shirt" in state:
          rv.append(("kate avatar b1top",195,348))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1cheerleader_top",198,372))
          if "_rope" in state:
            rv.append(("kate avatar b1arm1_rope",17,387))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b1angel_costume",29,334))
          rv.append(("kate avatar b1arm1_n",17,387))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1bardot_top",208,443))
        if "_necklace" in state:
          rv.append(("kate avatar b1necklace",295,376))
        elif "_choker" in state:
          rv.append(("kate avatar b1choker",310,359))
        if "_shirt" in state:
          rv.append(("kate avatar b1arm1_n",17,387))
          rv.append(("kate avatar b1arm1_c",120,388))
        elif "_cheerleader_top" in state:
          if "_mask" in state:
            rv.append(("kate avatar b1mask_cheerleader",100,391))
          else:
            rv.append(("kate avatar b1arm1_cheerleader",17,388))
        elif "_phone" in state:
          rv.append(("kate avatar b1phone_bardot_top",118,233))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1arm1_bardot_top",14,387))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((670,1080))
        rv.append(("kate avatar body1",117,99))
        rv.append(("kate avatar face_flirty",193,153))
        if "_cheerleader_top" in state and "_hands_on_hips" not in state:
          rv.append(("kate avatar b1arm2_n",118,392))
        if "_cheerleader_bra" in state:
          if "_cheerleader_top" not in state:
            rv.append(("kate avatar b1arm2_n",118,392))
          rv.append(("kate avatar b1cheerleader_bra",224,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b1blue_bra",224,522))
        elif "_bra" in state:
          rv.append(("kate avatar b1bra",216,371))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b1cheerleader_panties",147,738))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b1blue_panties",139,736))
        elif "_panties" in state:
          rv.append(("kate avatar b1panty",143,747))
        if "_hands_on_hips" in state:
          rv.append(("kate avatar b1arm1_n",17,387))
        elif "_phone" in state:
          rv.append(("kate avatar b1phone_n",121,233))
        elif "_shirt" not in state and "_cheerleader_top" not in state and "_angel_costume" not in state:
          rv.append(("kate avatar b1arm2_n",118,392))
        if "_pants" in state:
          rv.append(("kate avatar b1pants",41,526))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b1cheerleader_skirt",90,735))
        elif "_skirt" in state:
          rv.append(("kate avatar b1skirt",114,645))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b1knee_high_boots",118,983))
        if "_shirt" in state:
          rv.append(("kate avatar b1top",195,348))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1cheerleader_top",198,372))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b1angel_costume",29,334))
          rv.append(("kate avatar b1arm2_n",118,392))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1bardot_top",208,443))
        if "_necklace" in state:
          rv.append(("kate avatar b1necklace",295,376))
        elif "_choker" in state:
          rv.append(("kate avatar b1choker",310,359))
        if "_shirt" in state:
          rv.append(("kate avatar b1arm2_n",118,392))
          rv.append(("kate avatar b1arm2_c",178,392))
        elif "_cheerleader_top" in state:
          if "_hands_on_hips" in state:
            rv.append(("kate avatar b1arm1_cheerleader",17,388))
          else:
            rv.append(("kate avatar b1arm2_cheerleader",117,392))
        elif "_phone" in state:
          rv.append(("kate avatar b1phone_bardot_top",118,233))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1arm2_bardot_top",116,392))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((670,1080))
        rv.append(("kate avatar body1",117,99))
        rv.append(("kate avatar face_laughing",222,200))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b1arm2_n",118,392))
        if "_cheerleader_bra" in state:
          if "_cheerleader_top" not in state:
            rv.append(("kate avatar b1arm2_n",118,392))
          rv.append(("kate avatar b1cheerleader_bra",224,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b1blue_bra",224,522))
        elif "_bra" in state:
          rv.append(("kate avatar b1bra",216,371))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b1cheerleader_panties",147,738))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b1blue_panties",139,736))
        elif "_panties" in state:
          rv.append(("kate avatar b1panty",143,747))
        if "_phone" in state:
          rv.append(("kate avatar b1phone_n",121,233))
        elif "_shirt" not in state and "_cheerleader_top" not in state and "_angel_costume" not in state:
          rv.append(("kate avatar b1arm2_n",118,392))
        if "_pants" in state:
          rv.append(("kate avatar b1pants",41,526))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b1cheerleader_skirt",90,735))
        elif "_skirt" in state:
          rv.append(("kate avatar b1skirt",114,645))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b1knee_high_boots",118,983))
        if "_shirt" in state:
          rv.append(("kate avatar b1top",195,348))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1cheerleader_top",198,372))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b1angel_costume",29,334))
          rv.append(("kate avatar b1arm2_n",118,392))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1bardot_top",208,443))
        if "_necklace" in state:
          rv.append(("kate avatar b1necklace",295,376))
        elif "_choker" in state:
          rv.append(("kate avatar b1choker",310,359))
        if "_shirt" in state:
          if "_hands_up" in state:
            rv.append(("kate avatar b1arm1_n",17,387))
            rv.append(("kate avatar b1arm1_c",120,388))
          else:
            rv.append(("kate avatar b1arm2_n",118,392))
            rv.append(("kate avatar b1arm2_c",178,392))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1arm2_cheerleader",117,392))
        elif "_phone" in state:
          rv.append(("kate avatar b1phone_bardot_top",118,233))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1arm2_bardot_top",116,392))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((670,1080))
        rv.append(("kate avatar body1",117,99))
        if state.startswith("neutral_right"):
          rv.append(("kate avatar face_neutral_right",236,201))
        else:
          rv.append(("kate avatar face_neutral",228,201))
        if "_cheerleader_top" in state:
          if "_rope" not in state:
            if "_paddle" in state:
              rv.append(("kate avatar b1arm1_paddle",17,387))
            elif "_hands_down" in state:
              rv.append(("kate avatar b1arm2_n",118,392))
            elif "_mask" in state:
              rv.append(("kate avatar b1mask_n",28,202))
            else:
              rv.append(("kate avatar b1arm1_n",17,387))
        if "_cheerleader_bra" in state:
          if "_cheerleader_top" not in state:
            rv.append(("kate avatar b1arm1_n",17,387))
          rv.append(("kate avatar b1cheerleader_bra",224,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b1blue_bra",224,522))
        elif "_bra" in state:
          rv.append(("kate avatar b1bra",216,371))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b1cheerleader_panties",147,738))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b1blue_panties",139,736))
        elif "_panties" in state:
          rv.append(("kate avatar b1panty",143,747))
        if "_phone" in state:
          rv.append(("kate avatar b1phone_n",121,233))
        elif "_shirt" not in state and "_cheerleader_top" not in state and "_angel_costume" not in state:
          rv.append(("kate avatar b1arm1_n",17,387))
        if "_pants" in state:
          rv.append(("kate avatar b1pants",41,526))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b1cheerleader_skirt",90,735))
        elif "_skirt" in state:
          rv.append(("kate avatar b1skirt",114,645))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b1knee_high_boots",118,983))
        if "_shirt" in state:
          rv.append(("kate avatar b1top",195,348))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1cheerleader_top",198,372))
          if "_rope" in state:
            rv.append(("kate avatar b1arm1_rope",17,387))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b1angel_costume",29,334))
          rv.append(("kate avatar b1arm1_n",17,387))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1bardot_top",208,443))
        if "_necklace" in state:
          rv.append(("kate avatar b1necklace",295,376))
        elif "_choker" in state:
          rv.append(("kate avatar b1choker",310,359))
        if "_shirt" in state:
          if "_hands_down" in state:
            rv.append(("kate avatar b1arm2_n",118,392))
            rv.append(("kate avatar b1arm2_c",178,392))
          elif "_slap" in state:
            rv.append(("kate avatar b1slap_n",131,314))
            rv.append(("kate avatar b1slap_c",149,388))
          else:
            rv.append(("kate avatar b1arm1_n",17,387))
            rv.append(("kate avatar b1arm1_c",120,388))
        elif "_cheerleader_top" in state:
          if "_hands_down" in state:
            rv.append(("kate avatar b1arm2_cheerleader",117,392))
          elif "_mask" in state:
            rv.append(("kate avatar b1mask_cheerleader",100,391))
          else:
            rv.append(("kate avatar b1arm1_cheerleader",17,388))
        elif "_phone" in state:
          rv.append(("kate avatar b1phone_bardot_top",118,233))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1arm1_bardot_top",14,387))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((670,1080))
        rv.append(("kate avatar body1",117,99))
        if "_phone" in state:
          rv.append(("kate avatar b1phone_n",121,233))
        else:
          rv.append(("kate avatar b1arm2_n",118,392))
        if state.startswith("smile_right"):
          rv.append(("kate avatar face_smile_right",228,200))
        else:
          rv.append(("kate avatar face_smile",228,200))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b1arm2_n",118,392))
        if "_cheerleader_bra" in state:
          if "_cheerleader_top" not in state:
            rv.append(("kate avatar b1arm2_n",118,392))
          rv.append(("kate avatar b1cheerleader_bra",224,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b1blue_bra",224,522))
        elif "_bra" in state:
          rv.append(("kate avatar b1bra",216,371))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b1cheerleader_panties",147,738))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b1blue_panties",139,736))
        elif "_panties" in state:
          rv.append(("kate avatar b1panty",143,747))
        if "_pants" in state:
          rv.append(("kate avatar b1pants",41,526))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b1cheerleader_skirt",90,735))
        elif "_skirt" in state:
          rv.append(("kate avatar b1skirt",114,645))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b1knee_high_boots",118,983))
        if "_shirt" in state:
          rv.append(("kate avatar b1top",195,348))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1cheerleader_top",198,372))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b1angel_costume",29,334))
          rv.append(("kate avatar b1arm2_n",118,392))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1bardot_top",208,443))
        if "_necklace" in state:
          rv.append(("kate avatar b1necklace",295,376))
        elif "_choker" in state:
          rv.append(("kate avatar b1choker",310,359))
        if "_shirt" in state:
          rv.append(("kate avatar b1arm2_n",118,392))
          rv.append(("kate avatar b1arm2_c",178,392))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b1arm2_cheerleader",117,392))
        elif "_phone" in state:
          rv.append(("kate avatar b1phone_bardot_top",118,233))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b1arm2_bardot_top",116,392))

      elif state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        if "_soaked" in state:
          rv.append(("kate avatar body2_soaked",93,102))
        else:
          rv.append(("kate avatar body2",93,102))
        if "_tired" in state:
          rv.append(("kate avatar b2messy_hair",125,87))
#       rv.append(("kate avatar face_angry",151,170))
        rv.append(("kate avatar face_angry",207,223))
        if "_soaked" in state:
          rv.append(("kate avatar b2arm2soaked",6,628))
        elif "_rope" in state:
          rv.append(("kate avatar b2arm2_rope",0,0))
        else:
          rv.append(("kate avatar b2arm2_n",6,628))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b2cheerleader_bra",166,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b2blue_bra",166,532))
        elif "_bra" in state:
          rv.append(("kate avatar b2bra",166,379))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b2cheerleader_panties",142,745))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b2blue_panties",138,739))
        elif "_panties" in state:
          rv.append(("kate avatar b2panty",131,772))
        if "_pants" in state:
          rv.append(("kate avatar b2pants",107,699))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b2cheerleader_skirt",99,746))
        elif "_skirt" in state:
          rv.append(("kate avatar b2skirt",99,687))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b2knee_high_boots",119,1029))
        if "_shirt" in state:
          if "_soaked" in state:
            rv.append(("kate avatar b2topsoaked",113,355))
          else:
            rv.append(("kate avatar b2top",113,355))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b2cheerleader_top",115,374))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2bardot_top",112,462))
        if "_necklace" in state:
          rv.append(("kate avatar b2necklace",233,375))
        elif "_choker" in state:
          rv.append(("kate avatar b2choker",244,366))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b2arm2_cheerleader",85,620))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2arm2_bardot_top",6,626))

      elif state.startswith("gushing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body2",93,102))
        if "_tired" in state:
          rv.append(("kate avatar b2messy_hair",125,87))
#       rv.append(("kate avatar face_gushing",165,212))
        rv.append(("kate avatar face_gushing",207,211))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b2cheerleader_bra",166,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b2blue_bra",166,532))
        elif "_bra" in state:
          rv.append(("kate avatar b2bra",166,379))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b2cheerleader_panties",142,745))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b2blue_panties",138,739))
        elif "_panties" in state:
          rv.append(("kate avatar b2panty",131,772))
        if "_pants" in state:
          rv.append(("kate avatar b2pants",107,699))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b2cheerleader_skirt",99,746))
        elif "_skirt" in state:
          rv.append(("kate avatar b2skirt",99,687))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b2knee_high_boots",119,1029))
        if "_shirt" in state:
          rv.append(("kate avatar b2top",113,355))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b2cheerleader_top",115,374))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b2angel_costume",7,332))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2bardot_top",112,462))
        if "_necklace" in state:
          rv.append(("kate avatar b2necklace",233,375))
        elif "_choker" in state:
          rv.append(("kate avatar b2choker",244,366))
        if "_rope" in state:
          rv.append(("kate avatar b2arm1_rope",0,0))
        elif "_star" in state:
          rv.append(("kate avatar b2arm1_star",120,347))
        else:
          rv.append(("kate avatar b2arm1_n",120,365))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b2arm1_cheerleader",116,491))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2arm1_bardot_top",115,365))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body2",93,102))
        if "_tired" in state:
          rv.append(("kate avatar b2messy_hair",125,87))
        if "_right" in state:
          rv.append(("kate avatar face_sad_right",207,210))
        elif "_tired" in state and "2" not in state:
          rv.append(("kate avatar face_sad_tired",208,210))
        else:
#         rv.append(("kate avatar face_sad",165,211))
          rv.append(("kate avatar face_sad",207,210))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b2cheerleader_bra",166,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b2blue_bra",166,532))
        elif "_bra" in state:
          rv.append(("kate avatar b2bra",166,379))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b2cheerleader_panties",142,745))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b2blue_panties",138,739))
        elif "_panties" in state:
          rv.append(("kate avatar b2panty",131,772))
        if "_pants" in state:
          rv.append(("kate avatar b2pants",107,699))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b2cheerleader_skirt",99,746))
        elif "_skirt" in state:
          rv.append(("kate avatar b2skirt",99,687))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b2knee_high_boots",119,1029))
        if "_shirt" in state:
          rv.append(("kate avatar b2top",113,355))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b2cheerleader_top",115,374))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2bardot_top",112,462))
        if "_necklace" in state:
          rv.append(("kate avatar b2necklace",233,375))
        elif "_choker" in state:
          rv.append(("kate avatar b2choker",244,366))
        if "_rope" in state:
          rv.append(("kate avatar b2arm2_rope",0,0))
        elif "_hands_up" in state:
          rv.append(("kate avatar b2arm1_n",120,365))
        else:
          rv.append(("kate avatar b2arm2_n",6,628))
        if "_cheerleader_top" in state:
          if "_hands_up" in state:
            rv.append(("kate avatar b2arm1_cheerleader",116,491))
          else:
            rv.append(("kate avatar b2arm2_cheerleader",85,620))
        elif "_bardot_top" in state:
          if "_hands_up" in state:
            rv.append(("kate avatar b2arm1_bardot_top",115,365))
          else:
            rv.append(("kate avatar b2arm2_bardot_top",6,626))

      elif state.startswith("surprised"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body2",93,102))
        if "_tired" in state:
          rv.append(("kate avatar b2messy_hair",125,87))
#       rv.append(("kate avatar face_surprised",173,212))
        rv.append(("kate avatar face_surprised",207,211))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b2cheerleader_bra",166,377))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b2blue_bra",166,532))
        elif "_bra" in state:
          rv.append(("kate avatar b2bra",166,379))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b2cheerleader_panties",142,745))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b2blue_panties",138,739))
        elif "_panties" in state:
          rv.append(("kate avatar b2panty",131,772))
        if "_pants" in state:
          rv.append(("kate avatar b2pants",107,699))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b2cheerleader_skirt",99,746))
        elif "_skirt" in state:
          rv.append(("kate avatar b2skirt",99,687))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b2knee_high_boots",119,1029))
        if "_shirt" in state:
          rv.append(("kate avatar b2top",113,355))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b2cheerleader_top",115,374))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b2angel_costume",7,332))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2bardot_top",112,462))
        if "_necklace" in state:
          rv.append(("kate avatar b2necklace",233,375))
        elif "_choker" in state:
          rv.append(("kate avatar b2choker",244,366))
        if "_rope" in state:
          rv.append(("kate avatar b2arm1_rope",0,0))
        else:
          rv.append(("kate avatar b2arm1_n",120,365))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b2arm1_cheerleader",116,491))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b2arm1_bardot_top",115,365))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((636,1080))
        rv.append(("kate avatar body3",20,106))
        if "_tired" in state:
          rv.append(("kate avatar b3messy_hair",169,89))
        rv.append(("kate avatar b3arm2",365,291))
        if "_right" in state:
          rv.append(("kate avatar face_annoyed_right",289,199))
        else:
          rv.append(("kate avatar face_annoyed",289,199))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b3cheerleader_bra",219,365))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b3blue_panties",136,716))
        elif "_bra" in state:
          rv.append(("kate avatar b3bra",220,359))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b3cheerleader_panties",137,728))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b3blue_bra",219,499))
        elif "_panties" in state:
          rv.append(("kate avatar b3panty",131,733))
        if "_pants" in state:
          rv.append(("kate avatar b3pants",103,679))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b3cheerleader_skirt",85,722))
        elif "_skirt" in state:
          rv.append(("kate avatar b3skirt",91,655))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b3knee_high_boots",106,981))
        if "_shirt" in state:
          rv.append(("kate avatar b3top",105,345))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b3cheerleader_top",19,361))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b3bardot_top",17,368))
        if "_necklace" in state:
          rv.append(("kate avatar b3necklace",267,363))
        elif "_choker" in state:
          rv.append(("kate avatar b3choker",274,355))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((636,1080))
        rv.append(("kate avatar body3",20,106))
        rv.append(("kate avatar b3arm2",365,291))
        rv.append(("kate avatar face_cringe",241,195))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b3cheerleader_bra",219,365))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b3blue_bra",219,499))
        elif "_bra" in state:
          rv.append(("kate avatar b3bra",220,359))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b3cheerleader_panties",137,728))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b3blue_panties",136,716))
        elif "_panties" in state:
          rv.append(("kate avatar b3panty",131,733))
        if "_pants" in state:
          rv.append(("kate avatar b3pants",103,679))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b3cheerleader_skirt",85,722))
        elif "_skirt" in state:
          rv.append(("kate avatar b3skirt",91,655))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b3knee_high_boots",106,981))
        if "_shirt" in state:
          rv.append(("kate avatar b3top",105,345))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b3cheerleader_top",19,361))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b3bardot_top",17,368))
        if "_necklace" in state:
          rv.append(("kate avatar b3necklace",267,363))
        elif "_choker" in state:
          rv.append(("kate avatar b3choker",274,355))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((636,1080))
        rv.append(("kate avatar body3",20,106))
        if "_tired" in state:
          rv.append(("kate avatar b3messy_hair",169,89))
        rv.append(("kate avatar b3arm1",335,301))
        rv.append(("kate avatar face_excited",242,206))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b3cheerleader_bra",219,365))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b3blue_bra",219,499))
        elif "_bra" in state:
          rv.append(("kate avatar b3bra",220,359))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b3cheerleader_panties",137,728))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b3blue_panties",136,716))
        elif "_panties" in state:
          rv.append(("kate avatar b3panty",131,733))
        if "_pants" in state:
          rv.append(("kate avatar b3pants",103,679))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b3cheerleader_skirt",85,722))
        elif "_skirt" in state:
          rv.append(("kate avatar b3skirt",91,655))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b3knee_high_boots",106,981))
        if "_shirt" in state:
          rv.append(("kate avatar b3top",105,345))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b3cheerleader_top",19,361))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b3bardot_top",17,368))
        if "_necklace" in state:
          rv.append(("kate avatar b3necklace",267,363))
        elif "_choker" in state:
          rv.append(("kate avatar b3choker",274,355))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((636,1080))
        rv.append(("kate avatar body3",20,106))
        if "_tired" in state:
          rv.append(("kate avatar b3messy_hair",169,89))
        rv.append(("kate avatar b3arm2",365,291))
        rv.append(("kate avatar face_eyeroll",289,209))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b3cheerleader_bra",219,365))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b3blue_bra",219,499))
        elif "_bra" in state:
          rv.append(("kate avatar b3bra",220,359))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b3cheerleader_panties",137,728))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b3blue_panties",136,716))
        elif "_panties" in state:
          rv.append(("kate avatar b3panty",131,733))
        if "_pants" in state:
          rv.append(("kate avatar b3pants",103,679))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b3cheerleader_skirt",85,722))
        elif "_skirt" in state:
          rv.append(("kate avatar b3skirt",91,655))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b3knee_high_boots",106,981))
        if "_shirt" in state:
          rv.append(("kate avatar b3top",105,345))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b3cheerleader_top",19,361))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b3bardot_top",17,368))
        if "_necklace" in state:
          rv.append(("kate avatar b3necklace",267,363))
        elif "_choker" in state:
          rv.append(("kate avatar b3choker",274,355))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((636,1080))
        rv.append(("kate avatar body3",20,106))
        if "_tired" in state:
          rv.append(("kate avatar b3messy_hair",169,89))
        rv.append(("kate avatar b3arm2",365,291))
        rv.append(("kate avatar face_skeptical",242,206))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b3cheerleader_bra",219,365))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b3blue_bra",219,499))
        elif "_bra" in state:
          rv.append(("kate avatar b3bra",220,359))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b3cheerleader_panties",137,728))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b3blue_panties",136,716))
        elif "_panties" in state:
          rv.append(("kate avatar b3panty",131,733))
        if "_pants" in state:
          rv.append(("kate avatar b3pants",103,679))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b3cheerleader_skirt",85,722))
        elif "_skirt" in state:
          rv.append(("kate avatar b3skirt",91,655))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b3knee_high_boots",106,981))
        if "_shirt" in state:
          rv.append(("kate avatar b3top",105,345))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b3cheerleader_top",19,361))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b3bardot_top",17,368))
        if "_necklace" in state:
          rv.append(("kate avatar b3necklace",267,363))
        elif "_choker" in state:
          rv.append(("kate avatar b3choker",274,355))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body4",11,93))
        rv.append(("kate avatar face_afraid",176,181))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b4cheerleader_bra",172,373))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b4blue_bra",171,493))
        elif "_bra" in state:
          rv.append(("kate avatar b4bra",172,377))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b4cheerleader_panties",173,725))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b4blue_panties",171,708))
        elif "_panties" in state:
          rv.append(("kate avatar b4panty",171,722))
        if "_pants" in state:
          rv.append(("kate avatar b4pants",12,682))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b4cheerleader_skirt",110,712))
        elif "_skirt" in state:
          rv.append(("kate avatar b4skirt",110,653))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b4knee_high_boots",8,926))
        if "_shirt" in state:
          rv.append(("kate avatar b4top",155,332))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b4cheerleader_top",155,343))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4bardot_top",155,431))
        if "_necklace" in state:
          rv.append(("kate avatar b4necklace",211,373))
        elif "_choker" in state:
          rv.append(("kate avatar b4choker",270,370))
        rv.append(("kate avatar b4arm2_n",111,480))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b4arm2_cheerleader",105,479))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4arm2_bardot_top",110,476))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body4",11,93))
        rv.append(("kate avatar face_blush",159,172))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b4cheerleader_bra",172,373))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b4blue_bra",171,493))
        elif "_bra" in state:
          rv.append(("kate avatar b4bra",172,377))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b4cheerleader_panties",173,725))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b4blue_panties",171,708))
        elif "_panties" in state:
          rv.append(("kate avatar b4panty",171,722))
        if "_pants" in state:
          rv.append(("kate avatar b4pants",12,682))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b4cheerleader_skirt",110,712))
        elif "_skirt" in state:
          rv.append(("kate avatar b4skirt",110,653))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b4knee_high_boots",8,926))
        if "_shirt" in state:
          rv.append(("kate avatar b4top",155,332))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b4cheerleader_top",155,343))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b4angel_costume",10,327))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4bardot_top",155,431))
        if "_necklace" in state:
          rv.append(("kate avatar b4necklace",211,373))
        elif "_choker" in state:
          rv.append(("kate avatar b4choker",270,370))
        if "_blindfold" in state:
          if "_wig" in state:
            rv.append(("kate avatar b4blindfold_wig",107,270))
          else:
            rv.append(("kate avatar b4blindfold",107,270))
        else:
          rv.append(("kate avatar b4arm1_n",111,283))
          if "_marker" in state:
            rv.append(("kate avatar b4marker",141,293))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b4arm1_cheerleader",110,398))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4arm1_bardot_top",110,283))

      elif state.startswith("embarrassed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body4",11,93))
        if "_tired" in state:
          rv.append(("kate avatar b4messy_hair",181,79))
        rv.append(("kate avatar face_embarrassed",180,175))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b4cheerleader_bra",172,373))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b4blue_bra",171,493))
        elif "_bra" in state:
          rv.append(("kate avatar b4bra",172,377))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b4cheerleader_panties",173,725))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b4blue_panties",171,708))
        elif "_panties" in state:
          rv.append(("kate avatar b4panty",171,722))
        if "_pants" in state:
          rv.append(("kate avatar b4pants",12,682))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b4cheerleader_skirt",110,712))
        elif "_skirt" in state:
          rv.append(("kate avatar b4skirt",110,653))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b4knee_high_boots",8,926))
        if "_shirt" in state:
          rv.append(("kate avatar b4top",155,332))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b4cheerleader_top",155,343))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b4angel_costume",10,327))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4bardot_top",155,431))
        if "_necklace" in state:
          rv.append(("kate avatar b4necklace",211,373))
        elif "_choker" in state:
          rv.append(("kate avatar b4choker",270,370))
        if "_blindfold_wig" in state:
            rv.append(("kate avatar b4blindfold_wig",107,270))
        elif "_marker" in state:
          rv.append(("kate avatar b4arm1_n",111,283))
          rv.append(("kate avatar b4marker",141,293))
        else:
          rv.append(("kate avatar b4arm2_n",111,480))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b4arm2_cheerleader",105,479))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4arm2_bardot_top",110,476))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("kate avatar body4",11,93))
        if "_tired" in state:
          rv.append(("kate avatar b4messy_hair",181,79))
        rv.append(("kate avatar face_thinking",172,186))
        if "_cheerleader_bra" in state:
          rv.append(("kate avatar b4cheerleader_bra",172,373))
        elif "_blue_bra" in state:
          rv.append(("kate avatar b4blue_bra",171,493))
        elif "_bra" in state:
          rv.append(("kate avatar b4bra",172,377))
        if "_cheerleader_panties" in state:
          rv.append(("kate avatar b4cheerleader_panties",173,725))
        elif "_blue_panties" in state:
          rv.append(("kate avatar b4blue_panties",171,708))
        elif "_panties" in state:
          rv.append(("kate avatar b4panty",171,722))
        if "_pants" in state:
          rv.append(("kate avatar b4pants",12,682))
        elif "_cheerleader_skirt" in state:
          rv.append(("kate avatar b4cheerleader_skirt",110,712))
        elif "_skirt" in state:
          rv.append(("kate avatar b4skirt",110,653))
        if "_knee_high_boots" in state:
          rv.append(("kate avatar b4knee_high_boots",8,926))
        if "_shirt" in state:
          rv.append(("kate avatar b4top",155,332))
        elif "_cheerleader_top" in state:
          rv.append(("kate avatar b4cheerleader_top",155,343))
        elif "_angel_costume" in state:
          rv.append(("kate avatar b4angel_costume",10,327))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4bardot_top",155,431))
        if "_necklace" in state:
          rv.append(("kate avatar b4necklace",211,373))
        elif "_choker" in state:
          rv.append(("kate avatar b4choker",270,370))
        if "_blindfold_wig" in state:
            rv.append(("kate avatar b4blindfold_wig",107,270))
        else:
          rv.append(("kate avatar b4arm1_n",111,283))
        if "_cheerleader_top" in state:
          rv.append(("kate avatar b4arm1_cheerleader",110,398))
        elif "_bardot_top" in state:
          rv.append(("kate avatar b4arm1_bardot_top",110,283))

      if state.startswith("shoekiss"):
        rv.append((1920,1080))
        rv.append(("kate avatar events footkiss KateFeetBG",0,0))
        rv.append(("kate avatar events footkiss KateFeet_Body",654,5))
        rv.append(("kate avatar events footkiss KateFeet_undies",798,184))
        rv.append(("kate avatar events footkiss KateFeet_clothes",760,160))
        if "_shoe" in state:
          rv.append(("kate avatar events footkiss KateFeetBG_shoe",1011,765))
          rv.append(("kate avatar events footkiss KateFeet_face1",911,74))
        else:
          rv.append(("kate avatar events footkiss KateFeet_face2",911,73))
      if state=="KateBJPose01c":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KateBJPose01_hand",0,23))
        rv.append(("kate avatar events bj_gym KateKneelingPose02c",776,379))
        rv.append(("kate avatar events bj_gym KateBJ_Dick1",871,675))
      elif state=="KateBJPose01b":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head3",654,118))
        rv.append(("kate avatar events bj_gym KateBJPose01b_bx_hand",0,128))
        rv.append(("kate avatar events bj_gym KateBJPose01b",550,457))
      elif state=="KateBJPose01bx":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head3",654,118))
        rv.append(("kate avatar events bj_gym KateBJPose01b_bx_hand",0,128))
        rv.append(("kate avatar events bj_gym KateBJPose01bx",434,447))
      elif state=="KateBJPose01":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KateBJPose01_hand",0,23))
        rv.append(("kate avatar events bj_gym KateBJPose01",755,373))
        rv.append(("kate avatar events bj_gym KateBJ_Dick1",871,675))

        #rv.append((1920,1080))
        #rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        #rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        #rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        #rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        #rv.append(("kate avatar events bj_gym KateBJ_Head2",621,60))
        #rv.append(("kate avatar events bj_gym KateBJPose01x_hand",0,77))
        #rv.append(("kate avatar events bj_gym KateBJPose01",757,425))
      elif state=="KateBJPose01x":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head2",621,60))
        rv.append(("kate avatar events bj_gym KateBJPose01x_hand",0,77))
        rv.append(("kate avatar events bj_gym KateBJPose01x",757,425))
      elif state=="KateBJPose02":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head4",664,150))
        rv.append(("kate avatar events bj_gym KateBJPose02_hand",0,140))
        rv.append(("kate avatar events bj_gym KateBJPose02",353,432))
      elif state=="KateBJPose02b":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head4",664,150))
        rv.append(("kate avatar events bj_gym KateBJPose02_hand",0,140))
        rv.append(("kate avatar events bj_gym KateBJPose02b",353,432))
      elif state=="KateBJPose02c":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head5",664,150))
        rv.append(("kate avatar events bj_gym KateBJPose02_hand",0,140))
        rv.append(("kate avatar events bj_gym KateBJPose02c",338,344))
      elif state=="KateBJPose02d":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head4",664,150))
        rv.append(("kate avatar events bj_gym KateBJPose02_hand",0,140))
        rv.append(("kate avatar events bj_gym KateBJPose02d",353,432))
      elif state=="KateKneelingPose01":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KKP01",770,373))
      elif state=="KateKneelingPose01a1":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KateKneelingPose01a1",747,169))
        rv.append(("kate avatar events bj_gym KateBJ_Dick2",871,675))
      elif state=="KateKneelingPose01a2":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KateKneelingPose01a2",747,348))
        rv.append(("kate avatar events bj_gym KateBJ_Dick2",871,675))
      elif state=="KateKneelingPose01b":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1_2",604,7))
        rv.append(("kate avatar events bj_gym KateKneelingPose01b",750,169))
        rv.append(("kate avatar events bj_gym KateBJ_Dick2",871,675))
      elif state=="KateKneelingPose01c":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1_2",604,7))
        rv.append(("kate avatar events bj_gym KateKneelingPose01c",750,361))
        rv.append(("kate avatar events bj_gym KateBJ_Dick2",871,675))
      elif state=="KateKneelingPose01x":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KKP01x",770,360))
      elif state=="KateKneelingPose01x2":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KKP01x",770,360))
        rv.append(("kate avatar events bj_gym KateBJ_Dick1",871,675))
      elif state=="KateKneelingPose02":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_1",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_body",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KKP01",770,373))
        rv.append(("kate avatar events bj_gym KateBJ_Dick1",871,675))
      elif state=="KateKneelingPose01D":
        rv.append((1920,1080))
        rv.append(("kate avatar events bj_gym KateBJ_2",0,0))
        rv.append(("kate avatar events bj_gym KateBJ_bodyd",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_bra",674,553))
        rv.append(("kate avatar events bj_gym KateBJ_clothed_d",535,425))
        rv.append(("kate avatar events bj_gym KateBJ_Head1",604,7))
        rv.append(("kate avatar events bj_gym KKP01D",770,373))
      elif state=="AnalExam01":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse01",128,694))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm1",91,729))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair1",1048,320))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm1",1217,664))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm1",931,278))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve1",1151,381))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head1",1039,47))
        rv.append(("kate avatar events anal_exam AnalExam_Kate01",1072,204))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam02":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB2",764,443))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse02",128,694))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair1",1048,320))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm2",1100,360))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve2",1157,382))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head1",1039,47))
        rv.append(("kate avatar events anal_exam AnalExam_Kate02",1075,210))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam02b":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB2",764,443))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse02",128,694))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair2",1012,303))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm2",1100,360))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve2",1157,382))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head2",1016,49))
        rv.append(("kate avatar events anal_exam AnalExam_Kate02b",1048,191))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam02c":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB3",763,446))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse02c",128,691))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair2",1012,303))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm2",1100,360))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve2",1157,382))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head2",1016,49))
        rv.append(("kate avatar events anal_exam AnalExam_Kate02c",1054,187))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam03":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB3",763,446))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse03",128,691))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair1",1048,320))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm3",831,401))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve3",1164,383))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head1",1039,47))
        rv.append(("kate avatar events anal_exam AnalExam_Kate03",1075,199))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam04":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB3",763,446))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse04",128,691))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair1",1048,320))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm4",864,423))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve4",1137,381))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head1",1039,47))
        rv.append(("kate avatar events anal_exam AnalExam_Kate04",1066,199))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam04b":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB3",763,446))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse03",128,691))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair2",1012,303))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm4",864,423))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve4",1137,381))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head2",1016,49))
        rv.append(("kate avatar events anal_exam AnalExam_Kate02c",1054,187))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      elif state=="AnalExam05":
        rv.append((1920,1080))
        rv.append(("kate avatar events anal_exam AnalExam_BG",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_Bed",0,0))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB1",70,395))
        rv.append(("kate avatar events anal_exam AnalExam_NurseB3",763,446))
        rv.append(("kate avatar events anal_exam AnalExam_Nurse05",128,691))
        rv.append(("kate avatar events anal_exam AnalExam_NurseArm2",239,392))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Hair1",1048,320))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Backarm2",1192,680))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_n",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_undies",1165,422))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_c",1165,339))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Arm5",864,350))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Sleeve5",1089,342))
        rv.append(("kate avatar events anal_exam AnalExam_Kate_Head1",1039,47))
        rv.append(("kate avatar events anal_exam AnalExam_Kate05",1074,199))
        rv.append(("kate avatar events anal_exam AnalExam_drip",888,696))
        rv.append(("kate avatar events anal_exam AnalExam_bottomlight",0,102))
      if state=="soaked_wet":
        rv.append((1920,1080))
        #rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        #rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        #rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        #rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket2",158,206))
        #rv.append(("lindsey avatar events wet lindsey_soaked_gushedbase",742,118))
        #rv.append(("lindsey avatar events wet lindsey_soaked_gushedbra",941,430))
        #rv.append(("lindsey avatar events wet lindsey_soaked_gushedclothes",776,128))

        rv.append(("kate avatar events soaked lindsey_soaked_bg",0,0))
        rv.append(("kate avatar events soaked lindsey_soaked_puddle",736,780))
        rv.append(("kate avatar events soaked lindsey_soaked_fountain",177,143))
        rv.append(("kate avatar events soaked lindsey_soaked_mcjacket2",158,206))
        rv.append(("kate avatar events soaked kate_soaked_base",750,88))
        rv.append(("kate avatar events soaked kate_soaked_panties",960,972))
        rv.append(("kate avatar events soaked kate_soaked_clothes",943,416))
        rv.append(("lindsey avatar events wet lindsey_soaked_splash",474,0))

      if state.startswith("cheerleader_feet"):
        if state in ("cheerleader_feet","cheerleader_feet_shoeless_sockless","cheerleader_feet_noretinue"):
          rv.append((1920,1080))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_bg",0,0))
          if not "_noretinue" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_retinue_n",137,0))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_retinue_u",197,0))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_retinue_c",140,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_kate_n",572,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_kate_u",825,47))
          if kate.equipped_item("kate_cheerleader_skirt"):
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_kate_c",792,38))
            if not "_shoeless_sockless" in state:
              rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_kate_rightleg_c",590,550))
        elif state=="cheerleader_feet_nofeet":
          rv.append((1920,1080))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_bg",0,0))
        elif state.startswith("cheerleader_feet_nofeet_phoneground"):
          rv.append((1920,1080))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_bg",0,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_phone",642,963))
          if "_night" in state:
            rv.append(("school art_class night_overlay",0,0))
            rv.append(("school art_class night_overlay",0,0)) ## Twice to make it darker
        elif state=="cheerleader_feet_isabelle":
          rv.append((1920,1080))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_bg",0,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_isa_n",698,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_isa_u",785,23))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_isa_c",732,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_phone",642,963))
        elif "_kate" in state:
          rv.append((1920,1080))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_bg",0,0))
          if not "_nofeet" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_retinue_n",137,0))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_retinue_u",197,0))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_retinue_c",140,0))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_katecrouch_shadow",721,862))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_katecrouch_n",642,0))
          if "_confident" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_face_confident",873,45))
          elif "_smile" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_face_smile",873,45))
          elif "_laughing" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_face_laughing",873,45))
          else:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_face_neutral",873,45))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_katecrouch_u",807,213))
          rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_katecrouch_c",728,212))
          if not "_sockless" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_katecrouch_sock",685,615))
          if not "_shoeless" in state:
              rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_katecrouch_shoes",724,745))
          if "_ballgag" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_arm_ball_n",657,76))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_arm_ball_c",656,171))
          elif "_holdingsock" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_arm_sock_n",657,104))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_arm_sock_c",656,171))
          else:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_arm_idle_n",718,217))
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_arm_idle_c",718,216))
          if "_phoneground" in state:
            rv.append(("kate avatar events cheerleader_feet cheerleaderfeet_phone",642,963))

      elif state.startswith("grill_perspective"):
        rv.append((1920,1080))
        rv.append(("kate avatar events grill_perspective bgday",0,0))
        if "_girls" in state:
          rv.append(("kate avatar events grill_perspective girls",637,442))
        rv.append(("kate avatar events grill_perspective grillday",0,0))

      elif state.startswith("spanking") and not state.startswith("spanking_trial"):
        rv.append((1920,1080))
        rv.append(("kate avatar events spanking bg",0,0))
        if state.endswith(("bend_over","pants_down","chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy_up_next","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
          rv.append(("kate avatar events spanking mc_shadow",680,847))
        if state.endswith(("bend_over","pants_down","stacy_up_next","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
          rv.append(("kate avatar events spanking stacy1_shadow",231,866))
        elif state.endswith(("stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7")):
          rv.append(("kate avatar events spanking stacy2_shadow",610,914))
        if state.endswith(("bend_over","pants_down","stacy_up_next")):
          rv.append(("kate avatar events spanking kate1_shadow",590,856))
        elif state.endswith(("chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7")):
          rv.append(("kate avatar events spanking kate2_shadow",610,914))
        elif state.endswith("kick"):
          rv.append(("kate avatar events spanking kate3_shadow",358,935))
        if state.endswith(("bend_over","pants_down","chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy_up_next","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
          rv.append(("kate avatar events spanking casey1_shadow",1349,1023))
        if state.endswith(("casey1","casey2","casey3","casey4","casey5","casey6","casey7")):
          rv.append(("kate avatar events spanking casey2_shadow",610,914))
        if state.endswith(("bend_over","pants_down","chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy_up_next","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","step_back","kick")):
          rv.append(("kate avatar events spanking lacey1_shadow",1501,922))
        elif state.endswith(("lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7")):
          rv.append(("kate avatar events spanking lacey2_shadow",610,914))
        if state.endswith(("bend_over","pants_down","stacy_up_next")):
          rv.append(("kate avatar events spanking kate1_n",587,175))
          if state.endswith("stacy_up_next"):
            rv.append(("kate avatar events spanking kate1_arm1_n",637,275))
          else:
            rv.append(("kate avatar events spanking kate1_arm2_n",622,167))
          rv.append(("kate avatar events spanking kate1_u",713,264))
          rv.append(("kate avatar events spanking kate1_c",583,282))
          if state.endswith("stacy_up_next"):
            rv.append(("kate avatar events spanking kate1_arm1_c",637,275))
          else:
            rv.append(("kate avatar events spanking kate1_arm2_c",631,271))
          if state.endswith("bend_over"):
            rv.append(("kate avatar events spanking kate1_face1",799,226))
          else:
            rv.append(("kate avatar events spanking kate1_face2",799,224))
        if state.endswith(("bend_over","pants_down")):
          rv.append(("kate avatar events spanking mc_torso1",977,645))
          rv.append(("kate avatar events spanking mc_legs1",662,674))
          if state.endswith("bend_over"):
            rv.append(("kate avatar events spanking mc_pants1",853,675))
          else:
            rv.append(("kate avatar events spanking mc_boxers1",871,675))
            rv.append(("kate avatar events spanking mc_pants2",852,877))
        elif state.endswith(("chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7")):
          rv.append(("kate avatar events spanking mc_torso2",961,723))
        else:
          rv.append(("kate avatar events spanking mc_torso3",974,632))
          if "_tomato" in state:
            rv.append(("kate avatar events spanking mc_legs5_tomato",662,676))
          else:
            rv.append(("kate avatar events spanking mc_legs5_red",662,676))
        if state.endswith(("bend_over","pants_down","stacy_up_next","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
          rv.append(("kate avatar events spanking stacy1_n",281,145))
          if state.endswith(("bend_over","pants_down","stacy_up_next")):
            rv.append(("kate avatar events spanking stacy1_arm1_n",395,257))
          else:
            rv.append(("kate avatar events spanking stacy1_arm2_n",307,257))
          rv.append(("kate avatar events spanking stacy1_u",401,258))
          rv.append(("kate avatar events spanking stacy1_c",270,257))
          if state.endswith(("bend_over","pants_down","stacy_up_next")):
            rv.append(("kate avatar events spanking stacy1_arm1_c",395,257))
          else:
            rv.append(("kate avatar events spanking stacy1_arm2_c",307,258))
          if state.endswith("bend_over"):
            rv.append(("kate avatar events spanking stacy1_face1",501,225))
          elif state.endswith("fight_back"):
            rv.append(("kate avatar events spanking stacy1_face2",501,225))
          else:
            rv.append(("kate avatar events spanking stacy1_face3",501,225))
        elif state.endswith(("chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7")):
          rv.append(("kate avatar events spanking stacy2_n",893,398))
          rv.append(("kate avatar events spanking stacy2_u",1024,527))
          rv.append(("kate avatar events spanking stacy2_c",983,523))
          rv.append(("kate avatar events spanking stacy2_face",1041,454))
        if state.endswith(("chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7")):
          rv.append(("kate avatar events spanking kate2_n",612,454))
          rv.append(("kate avatar events spanking kate2_u",715,572))
          rv.append(("kate avatar events spanking kate2_c",604,561))
          if state.endswith("chair"):
            rv.append(("kate avatar events spanking kate2_face1",787,510))
          else:
            rv.append(("kate avatar events spanking kate2_face2",787,510))
        elif state.endswith(("stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7")):
          rv.append(("kate avatar events spanking kate3_n",920,407))
          rv.append(("kate avatar events spanking kate3_u",1044,531))
          rv.append(("kate avatar events spanking kate3_c",996,522))
          rv.append(("kate avatar events spanking kate3_face",1044,467))
        elif state.endswith("fight_back"):
          rv.append(("kate avatar events spanking kate4_n",761,328))
          rv.append(("kate avatar events spanking kate4_u",1115,453))
          rv.append(("kate avatar events spanking kate4_c",759,439))
        elif state.endswith("kick"):
          rv.append(("kate avatar events spanking kate5_n",168,27))
          rv.append(("kate avatar events spanking kate5_u",556,161))
          rv.append(("kate avatar events spanking kate5_c",160,152))
        if state.endswith(("stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7")):
          rv.append(("kate avatar events spanking stacy3_n",612,453))
          rv.append(("kate avatar events spanking stacy3_u",715,572))
          rv.append(("kate avatar events spanking stacy3_c",604,561))
          rv.append(("kate avatar events spanking stacy3_face",803,512))
        if state.endswith(("bend_over","pants_down","chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy_up_next","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
          rv.append(("kate avatar events spanking casey1_n",1290,44))
          if state.endswith(("lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
            rv.append(("kate avatar events spanking casey1_arm1_n",1341,211))
          else:
            rv.append(("kate avatar events spanking casey1_arm2_n",1219,220))
          rv.append(("kate avatar events spanking casey1_u",1315,227))
          rv.append(("kate avatar events spanking casey1_c",1287,213))
          if state.endswith(("lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7","fight_back","step_back","kick")):
            rv.append(("kate avatar events spanking casey1_arm1_c",1341,211))
          else:
            rv.append(("kate avatar events spanking casey1_arm2_c",1219,220))
          if state.endswith("bend_over"):
            rv.append(("kate avatar events spanking casey1_face1",1335,154))
          elif state.endswith("fight_back"):
            rv.append(("kate avatar events spanking casey1_face2",1335,156))
          else:
            rv.append(("kate avatar events spanking casey1_face3",1335,154))
        elif state.endswith(("casey1","casey2","casey3","casey4","casey5","casey6","casey7")):
          rv.append(("kate avatar events spanking casey2_n",612,415))
          rv.append(("kate avatar events spanking casey2_u",715,572))
          rv.append(("kate avatar events spanking casey2_c",604,561))
          rv.append(("kate avatar events spanking casey2_face",803,512))
        if state.endswith(("bend_over","pants_down","chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy_up_next","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","step_back","kick")):
          rv.append(("kate avatar events spanking lacey1_n",1503,236))
          if state.endswith(("step_back","kick")):
            rv.append(("kate avatar events spanking lacey1_arm1_n",1562,376))
          else:
            rv.append(("kate avatar events spanking lacey1_arm2_n",1556,379))
          rv.append(("kate avatar events spanking lacey1_u",1571,372))
          rv.append(("kate avatar events spanking lacey1_c",1502,367))
          if state.endswith(("step_back","kick")):
            rv.append(("kate avatar events spanking lacey1_arm1_c",1559,376))
          else:
            rv.append(("kate avatar events spanking lacey1_arm2_c",1556,378))
          if state.endswith("bend_over"):
            rv.append(("kate avatar events spanking lacey1_face1",1604,298))
          else:
            rv.append(("kate avatar events spanking lacey1_face2",1604,298))
        elif state.endswith(("lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7")):
          rv.append(("kate avatar events spanking lacey2_n",612,441))
          rv.append(("kate avatar events spanking lacey2_u",715,572))
          rv.append(("kate avatar events spanking lacey2_c",604,561))
          rv.append(("kate avatar events spanking lacey2_face",803,512))
        else:
          rv.append(("kate avatar events spanking lacey3_n",612,441))
          rv.append(("kate avatar events spanking lacey2_u",715,572))
          rv.append(("kate avatar events spanking lacey3_c",604,561))
          rv.append(("kate avatar events spanking lacey3_face",801,510))
          rv.append(("kate avatar events spanking lacey2_hand3",900,665))
          rv.append(("kate avatar events spanking lacey2_paddle2_n",593,356))
          rv.append(("kate avatar events spanking lacey2_paddle2_c",592,481))
        if state.endswith(("chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7","stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7","casey_up_next","casey1","casey2","casey3","casey4","casey5","casey6","casey7","lacey_up_next","lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7")):
          if state.endswith(("kate5","stacy5","casey5","lacey5")):
            if "_red_" in state:
              rv.append(("kate avatar events spanking mc_legs2_red",757,683))
            elif "_redder" in state:
              rv.append(("kate avatar events spanking mc_legs2_redder",757,683))
            elif "_reddest" in state:
              rv.append(("kate avatar events spanking mc_legs2_reddest",757,683))
            elif "_tomato" in state:
              rv.append(("kate avatar events spanking mc_legs2_tomato",757,683))
            else:
              rv.append(("kate avatar events spanking mc_legs2",757,683))
          elif state.endswith(("kate6","stacy6","casey6","lacey6")):
            if "_red_" in state:
              rv.append(("kate avatar events spanking mc_legs3_red",757,679))
            elif "_redder" in state:
              rv.append(("kate avatar events spanking mc_legs3_redder",757,679))
            elif "_reddest" in state:
              rv.append(("kate avatar events spanking mc_legs3_reddest",757,679))
            elif "_tomato" in state:
              rv.append(("kate avatar events spanking mc_legs3_tomato",757,679))
            else:
              rv.append(("kate avatar events spanking mc_legs3",757,679))
          else:
            if "_red_" in state:
              rv.append(("kate avatar events spanking mc_legs4_red",757,689))
            elif "_redder" in state:
              rv.append(("kate avatar events spanking mc_legs4_redder",757,689))
            elif "_reddest" in state:
              rv.append(("kate avatar events spanking mc_legs4_reddest",757,689))
            elif "_tomato" in state:
              rv.append(("kate avatar events spanking mc_legs4_tomato",757,689))
            else:
              rv.append(("kate avatar events spanking mc_legs4",757,689))
          rv.append(("kate avatar events spanking mc_pants3",834,873))
          if state.endswith("chair"):
            rv.append(("kate avatar events spanking mc_boxers2",845,689))
          else:
            rv.append(("kate avatar events spanking mc_boxers3",840,863))
        if state.endswith(("chair","kate1","kate2","kate3","kate4","kate5","kate6","kate7")):
          if state.endswith("kate5"):
            rv.append(("kate avatar events spanking kate2_hand1",902,671))
          elif state.endswith("kate6"):
            rv.append(("kate avatar events spanking kate2_hand2",900,665))
          else:
            rv.append(("kate avatar events spanking kate2_hand3",903,675))
          if state.endswith(("chair","kate1")):
            rv.append(("kate avatar events spanking kate2_paddle1_n",679,587))
            rv.append(("kate avatar events spanking kate2_paddle1_c",678,586))
          elif state.endswith("kate2"):
            rv.append(("kate avatar events spanking kate2_paddle2_n",593,356))
            rv.append(("kate avatar events spanking kate2_paddle2_c",592,481))
          elif state.endswith("kate3"):
            rv.append(("kate avatar events spanking kate2_paddle3_n",616,469))
            rv.append(("kate avatar events spanking kate2_paddle3_c",613,585))
          elif state.endswith("kate4"):
            rv.append(("kate avatar events spanking kate2_paddle4_n",668,586))
            rv.append(("kate avatar events spanking kate2_paddle4_c",666,586))
          elif state.endswith("kate5"):
            rv.append(("kate avatar events spanking kate2_paddle5_n",690,586))
            rv.append(("kate avatar events spanking kate2_paddle5_c",692,585))
          elif state.endswith("kate6"):
            rv.append(("kate avatar events spanking kate2_paddle6_n",668,586))
            rv.append(("kate avatar events spanking kate2_paddle6_c",669,587))
          else:
            rv.append(("kate avatar events spanking kate2_paddle7_n",559,584))
            rv.append(("kate avatar events spanking kate2_paddle7_c",584,583))
        if state.endswith(("stacy1","stacy2","stacy3","stacy4","stacy5","stacy6","stacy7")):
          if state.endswith("stacy5"):
            rv.append(("kate avatar events spanking stacy3_hand1",902,671))
          elif state.endswith("stacy6"):
            rv.append(("kate avatar events spanking stacy3_hand2",900,665))
          else:
            rv.append(("kate avatar events spanking stacy3_hand3",903,676))
          if state.endswith("stacy1"):
            rv.append(("kate avatar events spanking stacy3_paddle1_n",679,587))
            rv.append(("kate avatar events spanking stacy3_paddle1_c",678,586))
          elif state.endswith("stacy2"):
            rv.append(("kate avatar events spanking stacy3_paddle2_n",593,356))
            rv.append(("kate avatar events spanking stacy3_paddle2_c",592,481))
          elif state.endswith("stacy3"):
            rv.append(("kate avatar events spanking stacy3_paddle3_n",616,469))
            rv.append(("kate avatar events spanking stacy3_paddle3_c",613,585))
          elif state.endswith("stacy4"):
            rv.append(("kate avatar events spanking stacy3_paddle4_n",668,586))
            rv.append(("kate avatar events spanking stacy3_paddle4_c",666,586))
          elif state.endswith("stacy5"):
            rv.append(("kate avatar events spanking stacy3_paddle5_n",690,586))
            rv.append(("kate avatar events spanking stacy3_paddle5_c",692,585))
          elif state.endswith("stacy6"):
            rv.append(("kate avatar events spanking stacy3_paddle6_n",668,586))
            rv.append(("kate avatar events spanking stacy3_paddle6_c",669,587))
          elif state.endswith("stacy7"):
            rv.append(("kate avatar events spanking stacy3_paddle7_n",559,584))
            rv.append(("kate avatar events spanking stacy3_paddle7_c",584,583))
        if state.endswith(("casey1","casey2","casey3","casey4","casey5","casey6","casey7")):
          if state.endswith("casey5"):
            rv.append(("kate avatar events spanking casey2_hand1",902,671))
          elif state.endswith("casey6"):
            rv.append(("kate avatar events spanking casey2_hand2",900,666))
          else:
            rv.append(("kate avatar events spanking casey2_hand3",903,676))
          if state.endswith("casey1"):
            rv.append(("kate avatar events spanking casey2_paddle1_n",679,587))
            rv.append(("kate avatar events spanking casey2_paddle1_c",678,586))
          elif state.endswith("casey2"):
            rv.append(("kate avatar events spanking casey2_paddle2_n",593,356))
            rv.append(("kate avatar events spanking casey2_paddle2_c",592,481))
          elif state.endswith("casey3"):
            rv.append(("kate avatar events spanking casey2_paddle3_n",616,469))
            rv.append(("kate avatar events spanking casey2_paddle3_c",613,585))
          elif state.endswith("casey4"):
            rv.append(("kate avatar events spanking casey2_paddle4_n",668,586))
            rv.append(("kate avatar events spanking casey2_paddle4_c",666,586))
          elif state.endswith("casey5"):
            rv.append(("kate avatar events spanking casey2_paddle5_n",690,586))
            rv.append(("kate avatar events spanking casey2_paddle5_c",692,585))
          elif state.endswith("casey6"):
            rv.append(("kate avatar events spanking casey2_paddle6_n",668,586))
            rv.append(("kate avatar events spanking casey2_paddle6_c",669,587))
          elif state.endswith("casey7"):
            rv.append(("kate avatar events spanking casey2_paddle7_n",559,584))
            rv.append(("kate avatar events spanking casey2_paddle7_c",584,583))
        if state.endswith(("lacey1","lacey2","lacey3","lacey4","lacey5","lacey6","lacey7")):
          if state.endswith("lacey5"):
            rv.append(("kate avatar events spanking lacey2_hand1",902,670))
          elif state.endswith("lacey6"):
            rv.append(("kate avatar events spanking lacey2_hand2",900,665))
          else:
            rv.append(("kate avatar events spanking lacey2_hand3",903,675))
          if state.endswith("lacey1"):
            rv.append(("kate avatar events spanking lacey2_paddle1_n",679,587))
            rv.append(("kate avatar events spanking lacey2_paddle1_c",678,586))
          elif state.endswith("lacey2"):
            rv.append(("kate avatar events spanking lacey2_paddle2_n",593,356))
            rv.append(("kate avatar events spanking lacey2_paddle2_c",592,481))
          elif state.endswith("lacey3"):
            rv.append(("kate avatar events spanking lacey2_paddle3_n",616,469))
            rv.append(("kate avatar events spanking lacey2_paddle3_c",613,585))
          elif state.endswith("lacey4"):
            rv.append(("kate avatar events spanking lacey2_paddle4_n",668,586))
            rv.append(("kate avatar events spanking lacey2_paddle4_c",666,586))
          elif state.endswith("lacey5"):
            rv.append(("kate avatar events spanking lacey2_paddle5_n",690,586))
            rv.append(("kate avatar events spanking lacey2_paddle5_c",692,585))
          elif state.endswith("lacey6"):
            rv.append(("kate avatar events spanking lacey2_paddle6_n",668,586))
            rv.append(("kate avatar events spanking lacey2_paddle6_c",669,587))
          elif state.endswith("lacey7"):
            rv.append(("kate avatar events spanking lacey2_paddle7_n",559,584))
            rv.append(("kate avatar events spanking lacey2_paddle7_c",584,583))

      elif state.startswith("lacey_bet"):
        rv.append((1920,1080))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top","fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra","fingering_ass1","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet background1",0,0))
        else:
          rv.append(("kate avatar events lacey_bet background2",0,0))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top","fingering_ass1","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet kate_body1",450,291))
        elif state.endswith(("fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra")):
          rv.append(("kate avatar events lacey_bet kate_body2",450,291))
        elif state.endswith("licking_ass2"):
          rv.append(("kate avatar events lacey_bet kate_body3",800,195))
        else:
          rv.append(("kate avatar events lacey_bet kate_body4",800,195))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt")):
          rv.append(("kate avatar events lacey_bet kate_panties",960,621))
          if not state.endswith("removing_kate_skirt"):
            rv.append(("kate avatar events lacey_bet kate_skirt",856,584))
        if state.endswith(("anticipation","licking_panties","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top","fingering_pussy","removing_kate_top")):
          rv.append(("kate avatar events lacey_bet kate_bra",1083,350))
          if not state.endswith("removing_kate_top"):
            rv.append(("kate avatar events lacey_bet kate_top",970,346))
        if state.endswith("anticipation"):
          rv.append(("kate avatar events lacey_bet kate_head1",1103,97))
        elif state.endswith(("licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top")):
          rv.append(("kate avatar events lacey_bet kate_head2",1103,97))
        elif state.endswith(("fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet kate_head3",1103,97))
        elif state.endswith("fingering_ass1"):
          rv.append(("kate avatar events lacey_bet kate_head4",1103,97))
        elif state.endswith("spreading_ass"):
          rv.append(("kate avatar events lacey_bet kate_head5",1139,46))
        else:
          rv.append(("kate avatar events lacey_bet kate_head6",1139,46))
        if state.endswith(("squirting1","squirting2")):
          rv.append(("kate avatar events lacey_bet squirt",873,635))
        if state.endswith(("fingering_ass1","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet lacey_left_arm1",710,398))
        elif state.endswith(("spreading_ass","licking_ass1","licking_ass3","licking_ass2","licking_pussy","squirting1","squirting2")):
          rv.append(("kate avatar events lacey_bet lacey_left_arm2",712,506))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top","fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra","fingering_ass1","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet lacey_body1",461,635))
        else:
          rv.append(("kate avatar events lacey_bet lacey_body2",522,562))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top","fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra","fingering_ass1","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet lacey_panties",477,991))
          rv.append(("kate avatar events lacey_bet lacey_skirt",420,984))
          if not state.endswith(("removing_kate_and_lacey_bra","fingering_ass1","fingering_ass2","fingering_ass3")):
            rv.append(("kate avatar events lacey_bet lacey_bra",632,746))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1","removing_lacey_top")):
          rv.append(("kate avatar events lacey_bet lacey_right_arm1",737,760))
        elif state.endswith(("fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra")):
          rv.append(("kate avatar events lacey_bet lacey_right_arm2",765,696))
        elif state.endswith(("fingering_ass1","fingering_ass2","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet lacey_right_arm3",749,759))
        elif state.endswith("licking_ass2"):
          rv.append(("kate avatar events lacey_bet lacey_right_arm4",772,630))
        else:
          rv.append(("kate avatar events lacey_bet lacey_right_arm5",793,541))
        if state.endswith(("anticipation","licking_panties","removing_kate_skirt","removing_kate_panties","licking_pussy1")):
          rv.append(("kate avatar events lacey_bet lacey_top",613,703))
        if state.endswith(("anticipation","removing_kate_skirt","removing_kate_panties","removing_lacey_top","fingering_ass3")):
          rv.append(("kate avatar events lacey_bet lacey_head1",735,458))
        elif state.endswith(("licking_panties","licking_pussy1","fingering_pussy","removing_kate_top","removing_kate_and_lacey_bra","fingering_ass1","fingering_ass2")):
          rv.append(("kate avatar events lacey_bet lacey_head2",753,435))
        elif state.endswith(("licking_ass1","licking_ass2")):
          rv.append(("kate avatar events lacey_bet lacey_head3",733,372))
        elif state.endswith(("licking_pussy","squirting1")):
          rv.append(("kate avatar events lacey_bet lacey_head4",737,351))
        else:
          rv.append(("kate avatar events lacey_bet lacey_head5",704,375))

      elif state.startswith("posing"):
        rv.append((1920,1080))
        rv.append(("kate avatar events posing kate",0,0))

      elif state.startswith("paddling"):
        rv.append((1920,1080))
        rv.append(("kate avatar events paddling background",0,0))
        if "kateless" not in state:
          rv.append(("kate avatar events paddling kate_body",25,259))
          rv.append(("kate avatar events paddling kate_underwear",204,417))
          rv.append(("kate avatar events paddling kate_clothes",46,407))
        rv.append(("kate avatar events paddling casey_body",1001,0))
        rv.append(("kate avatar events paddling casey_underwear",1001,0))
        if "casey_leash_pull" in state:
          rv.append(("kate avatar events paddling casey_clothes",1001,0))
          rv.append(("kate avatar events paddling casey_arm2",939,0))
          rv.append(("kate avatar events paddling casey_sleeve2",937,0))
        else:
          rv.append(("kate avatar events paddling casey_arm1",874,0))
          rv.append(("kate avatar events paddling casey_sleeve1",874,0))
          rv.append(("kate avatar events paddling casey_clothes",1001,0))
        if "arrival" in state or "kateless" in state:
          rv.append(("kate avatar events paddling lacey_body1",1387,0))
          rv.append(("kate avatar events paddling lacey_underwear1",1389,0))
          rv.append(("kate avatar events paddling lacey_clothes1",1376,0))
        else:
          rv.append(("kate avatar events paddling lacey_body2",1152,165))
          rv.append(("kate avatar events paddling lacey_underwear2",1313,337))
          rv.append(("kate avatar events paddling lacey_clothes2",1147,336))
#         if "lacey_ballgag" in state:
#           pass
          if "lacey_hair_pull" in state:
            rv.append(("kate avatar events paddling lacey_right_arm2",920,380))
            rv.append(("kate avatar events paddling lacey_right_sleeve2",1086,379))
          elif "lacey_paddle_kiss" in state:
            rv.append(("kate avatar events paddling lacey_right_arm3",782,380))
            rv.append(("kate avatar events paddling lacey_right_sleeve3",1027,379))
          else:
            rv.append(("kate avatar events paddling lacey_right_arm1",1166,380))
            rv.append(("kate avatar events paddling lacey_right_sleeve1",1164,380))
          if "lacey_paddle1" in state:
            rv.append(("kate avatar events paddling lacey_left_arm2",1424,0))
            rv.append(("kate avatar events paddling lacey_left_sleeve2",1424,202))
          elif "lacey_paddle2" in state:
            rv.append(("kate avatar events paddling lacey_left_arm3",1125,331))
            rv.append(("kate avatar events paddling lacey_left_sleeve3",1351,331))
          elif "lacey_paddle3" in state:
            rv.append(("kate avatar events paddling lacey_left_arm4",986,226))
            rv.append(("kate avatar events paddling lacey_left_sleeve4",1209,331))
          elif "lacey_paddle4" in state:
            rv.append(("kate avatar events paddling lacey_left_arm5",1147,224))
            rv.append(("kate avatar events paddling lacey_left_sleeve5",1424,330))
          else:
            rv.append(("kate avatar events paddling lacey_left_arm1",1437,356))
            rv.append(("kate avatar events paddling lacey_left_sleeve1",1437,355))
        rv.append(("kate avatar events paddling stacy_body",1337,0))
        if "stacy_foot" not in state:
          rv.append(("kate avatar events paddling stacy_leg1",1685,161))
        rv.append(("kate avatar events paddling stacy_underwear",1749,69))
        rv.append(("kate avatar events paddling stacy_clothes",1700,0))
        if "stacy_foot" in state:
          rv.append(("kate avatar events paddling stacy_leg2",1263,136))
          rv.append(("kate avatar events paddling stacy_sleeve2",1229,67))
        else:
          rv.append(("kate avatar events paddling stacy_sleeve1",1685,67))
        if "lacey_paddle2" in state:
          rv.append(("kate avatar events paddling isabelle_body2",811,501))
          rv.append(("kate avatar events paddling isabelle_redness2",1057,591))
          rv.append(("kate avatar events paddling lacey_left_arm3_fix",1212,588))
          rv.append(("kate avatar events paddling isabelle_head3",808,531))
          if "isabelle_pain" in state:
            rv.append(("kate avatar events paddling isabelle_face9",840,608))
          elif "isabelle_more_pain" in state:
            rv.append(("kate avatar events paddling isabelle_face10",842,607))
          elif "isabelle_crying" in state:
            rv.append(("kate avatar events paddling isabelle_face11",842,605))
          rv.append(("kate avatar events paddling isabelle_bangs2",793,546))
        elif "lacey_paddle3" in state:
          rv.append(("kate avatar events paddling isabelle_body3",811,501))
          rv.append(("kate avatar events paddling isabelle_redness3",1160,599))
          rv.append(("kate avatar events paddling isabelle_head4",808,535))
          if "isabelle_pain" in state:
            rv.append(("kate avatar events paddling isabelle_face12",840,613))
          elif "isabelle_more_pain" in state:
            rv.append(("kate avatar events paddling isabelle_face13",842,612))
          elif "isabelle_crying" in state:
            rv.append(("kate avatar events paddling isabelle_face14",842,611))
          rv.append(("kate avatar events paddling isabelle_bangs3",789,552))
        elif "lacey_paddle4" in state:
          rv.append(("kate avatar events paddling isabelle_body4",811,501))
          rv.append(("kate avatar events paddling isabelle_redness4",1164,603))
          rv.append(("kate avatar events paddling isabelle_head5",811,536))
          if "isabelle_pain" in state:
            rv.append(("kate avatar events paddling isabelle_face15",842,615))
          elif "isabelle_more_pain" in state:
            rv.append(("kate avatar events paddling isabelle_face16",844,614))
          elif "isabelle_crying" in state:
            rv.append(("kate avatar events paddling isabelle_face17",844,613))
          rv.append(("kate avatar events paddling isabelle_bangs4",796,554))
        else:
          rv.append(("kate avatar events paddling isabelle_body1",811,501))
          if "arrival_red" in state or "lacey_ballgag_red" in state or "lacey_paddle1_red" in state or "lacey_paddle_kiss" in state or "kateless" in state:
            rv.append(("kate avatar events paddling isabelle_redness1",1158,615))
          if "lacey_hair_pull" in state:
            if "stacy_foot" in state:
              rv.append(("kate avatar events paddling stacy_leg2_fix",1305,615))
              rv.append(("kate avatar events paddling stacy_sleeve2_fix",1282,614))
            rv.append(("kate avatar events paddling isabelle_head2",832,534))
            rv.append(("kate avatar events paddling lacey_right_arm2_fix",920,525))
            rv.append(("kate avatar events paddling isabelle_face8",857,601))
          elif "lacey_paddle_kiss" in state:
            rv.append(("kate avatar events paddling isabelle_head6",807,589))
          else:
            rv.append(("kate avatar events paddling isabelle_head1",808,536))
            if "isabelle_defiant" in state:
              rv.append(("kate avatar events paddling isabelle_face1",839,611))
            elif "isabelle_pain" in state:
              rv.append(("kate avatar events paddling isabelle_face2",839,615))
            elif "isabelle_more_pain" in state:
              rv.append(("kate avatar events paddling isabelle_face3",841,614))
            elif "isabelle_crying" in state:
              rv.append(("kate avatar events paddling isabelle_face4",841,613))
            elif "isabelle_pleading" in state:
              rv.append(("kate avatar events paddling isabelle_face5",839,615))
            elif "isabelle_blush" in state:
              rv.append(("kate avatar events paddling isabelle_face6",839,615))
            if "isabelle_defiant_ballgag" in state or "isabelle_blush_ballgag" in state:
              rv.append(("kate avatar events paddling isabelle_ballgag",860,657))
            rv.append(("kate avatar events paddling isabelle_bangs1",807,557))

      elif state.startswith("strap_on"):
        rv.append((1920,1080))
        rv.append(("kate avatar events strap_on background",0,0))
        rv.append(("kate avatar events strap_on casey_body",0,0))
        rv.append(("kate avatar events strap_on casey_underwear",0,0))
        rv.append(("kate avatar events strap_on casey_clothes",0,0))
        if "casey_leash_pull" in state:
          rv.append(("kate avatar events strap_on casey_arm2",0,0))
          rv.append(("kate avatar events strap_on casey_sleeve2",0,0))
        else:
          rv.append(("kate avatar events strap_on casey_arm1",0,173))
          rv.append(("kate avatar events strap_on casey_sleeve1",0,170))
        rv.append(("kate avatar events strap_on lacey_body",1359,0))
        rv.append(("kate avatar events strap_on lacey_underwear",1385,0))
        rv.append(("kate avatar events strap_on lacey_clothes",1358,0))
        rv.append(("kate avatar events strap_on stacy_body",1590,0))
        rv.append(("kate avatar events strap_on stacy_underwear",1745,74))
        rv.append(("kate avatar events strap_on stacy_clothes",1737,45))
        if "anticipation" in state or "poke" in state or "penetration1" in state:
          rv.append(("kate avatar events strap_on kate_body1",772,31))
          rv.append(("kate avatar events strap_on kate_underwear1",989,297))
          rv.append(("kate avatar events strap_on kate_clothes1",912,290))
          if "anticipation" in state:
            rv.append(("kate avatar events strap_on kate_arm1",927,277))
            rv.append(("kate avatar events strap_on kate_sleeve1",1190,277))
          elif "poke" in state:
            rv.append(("kate avatar events strap_on kate_arm2",953,284))
            rv.append(("kate avatar events strap_on kate_sleeve2",1093,284))
        elif "penetration2" in state:
          rv.append(("kate avatar events strap_on kate_body2",754,31))
          rv.append(("kate avatar events strap_on kate_underwear2",989,297))
          rv.append(("kate avatar events strap_on kate_clothes2",920,290))
        elif "penetration3" in state:
          rv.append(("kate avatar events strap_on kate_body3",715,31))
          rv.append(("kate avatar events strap_on kate_underwear3",989,299))
          rv.append(("kate avatar events strap_on kate_clothes3",901,290))
        elif "penetration4" in state:
          rv.append(("kate avatar events strap_on kate_body4",695,31))
          rv.append(("kate avatar events strap_on kate_underwear4",988,299))
          rv.append(("kate avatar events strap_on kate_clothes4",884,289))
        elif "penetration5" in state:
          rv.append(("kate avatar events strap_on kate_body5",686,31))
          rv.append(("kate avatar events strap_on kate_underwear5",987,297))
          rv.append(("kate avatar events strap_on kate_clothes5",879,290))
        elif "entire_length" in state:
          rv.append(("kate avatar events strap_on kate_body6",675,86))
          rv.append(("kate avatar events strap_on kate_underwear6",881,371))
          rv.append(("kate avatar events strap_on kate_clothes6",678,292))
        if "anticipation" in state or "poke" in state or "penetration1" in state:
          rv.append(("kate avatar events strap_on isabelle_body1",215,314))
          if "anticipation" in state:
            rv.append(("kate avatar events strap_on isabelle_head1",297,283))
            rv.append(("kate avatar events strap_on isabelle_face1",452,400))
            rv.append(("kate avatar events strap_on isabelle_ballgag1",434,525))
          else:
            rv.append(("kate avatar events strap_on isabelle_head2",300,280))
            if "poke" in state:
              rv.append(("kate avatar events strap_on isabelle_face2",321,438))
            elif "isabelle_pain" in state:
              rv.append(("kate avatar events strap_on isabelle_face3",321,437))
            elif "isabelle_crying" in state:
              rv.append(("kate avatar events strap_on isabelle_face4",322,443))
            rv.append(("kate avatar events strap_on isabelle_bangs1",282,304))
        elif "penetration2" in state:
          rv.append(("kate avatar events strap_on isabelle_body2",216,314))
          rv.append(("kate avatar events strap_on isabelle_head3",313,287))
          if "isabelle_pain" in state:
            rv.append(("kate avatar events strap_on isabelle_face5",333,444))
          elif "isabelle_crying" in state:
            rv.append(("kate avatar events strap_on isabelle_face6",335,450))
          rv.append(("kate avatar events strap_on isabelle_bangs2",291,311))
        elif "penetration3" in state:
          if "casey_leash_pull" in state:
            rv.append(("kate avatar events strap_on isabelle_body4",335,112))
          else:
            rv.append(("kate avatar events strap_on isabelle_body3",216,314))
          rv.append(("kate avatar events strap_on isabelle_head4",328,307))
          if "isabelle_pain" in state:
            rv.append(("kate avatar events strap_on isabelle_face7",348,466))
          elif "isabelle_crying" in state:
            rv.append(("kate avatar events strap_on isabelle_face8",350,471))
          rv.append(("kate avatar events strap_on isabelle_bangs3",303,334))
        elif "penetration4" in state:
          rv.append(("kate avatar events strap_on isabelle_body5",216,314))
          if "isabelle_head_down" in state:
            rv.append(("kate avatar events strap_on isabelle_head6",252,523))
          else:
            rv.append(("kate avatar events strap_on isabelle_head5",349,348))
            if "isabelle_pain" in state:
              rv.append(("kate avatar events strap_on isabelle_face9",369,508))
            elif "isabelle_crying" in state:
              rv.append(("kate avatar events strap_on isabelle_face10",372,513))
            rv.append(("kate avatar events strap_on isabelle_bangs4",327,376))
        elif "penetration5" in state or "entire_length" in state:
          rv.append(("kate avatar events strap_on isabelle_body6",217,314))
          if "isabelle_head_down" in state:
            rv.append(("kate avatar events strap_on isabelle_head8",274,556))
          elif "entire_length" in state:
            rv.append(("kate avatar events strap_on isabelle_head9",371,382))
            rv.append(("kate avatar events strap_on isabelle_face13",533,484))
            if "no_ballgag" not in state:
              rv.append(("kate avatar events strap_on isabelle_ballgag2",512,618))
          else:
            rv.append(("kate avatar events strap_on isabelle_head7",376,378))
            if "isabelle_pain" in state:
              rv.append(("kate avatar events strap_on isabelle_face11",396,537))
            elif "isabelle_crying" in state:
              rv.append(("kate avatar events strap_on isabelle_face12",398,542))
            rv.append(("kate avatar events strap_on isabelle_bangs5",346,402))
        if "penetration1" in state:
          rv.append(("kate avatar events strap_on kate_arm3",1008,282))
          rv.append(("kate avatar events strap_on kate_sleeve3",1110,282))
        elif "penetration2" in state:
          rv.append(("kate avatar events strap_on kate_arm4",1024,285))
          rv.append(("kate avatar events strap_on kate_sleeve4",1131,285))
        elif "penetration3" in state:
          rv.append(("kate avatar events strap_on kate_arm5",1029,285))
          rv.append(("kate avatar events strap_on kate_sleeve5",1138,286))
        elif "penetration4" in state:
          rv.append(("kate avatar events strap_on kate_arm6",1037,285))
          rv.append(("kate avatar events strap_on kate_sleeve6",1144,289))
        elif "penetration5" in state:
          rv.append(("kate avatar events strap_on kate_arm7",1033,290))
          rv.append(("kate avatar events strap_on kate_sleeve7",1141,290))
        elif "entire_length" in state:
          rv.append(("kate avatar events strap_on kate_arm8",1033,289))
          rv.append(("kate avatar events strap_on kate_sleeve8",1076,289))

      elif state.startswith("spitroast"):
        rv.append((1920,1080))
        rv.append(("kate avatar events spitroast background",0,0))
        rv.append(("kate avatar events spitroast casey_feet",790,0))
        rv.append(("kate avatar events spitroast casey_shoes",973,0))
        if "stacy_backfoot" not in state and "stacy_buttplug" not in state:
          rv.append(("kate avatar events spitroast stacy_feet1",345,0))
          rv.append(("kate avatar events spitroast stacy_shoes1",347,0))
        rv.append(("kate avatar events spitroast isabelle_body",0,0))
        if "squeeze2" in state or "spanking2" in state or "penetration2" in state or "lacey_vibrator2" in state:
          rv.append(("kate avatar events spitroast isabelle_butt2",0,328))
          rv.append(("kate avatar events spitroast isabelle_boob2",807,666))
        elif "squeeze3" in state or "spanking3" in state or "penetration3" in state or "lacey_vibrator3" in state:
          rv.append(("kate avatar events spitroast isabelle_butt3",0,298))
          rv.append(("kate avatar events spitroast isabelle_boob3",809,666))
        elif "squeeze4" in state or "spanking4" in state or "penetration4" in state or "lacey_vibrator4" in state:
          rv.append(("kate avatar events spitroast isabelle_butt4",0,281))
          rv.append(("kate avatar events spitroast isabelle_boob4",812,667))
        else:
          rv.append(("kate avatar events spitroast isabelle_butt1",0,366))
          if "buttplug" in state and not "stacy_buttplug" in state:
            rv.append(("kate avatar events spitroast isabelle_buttplug",0,519))
          rv.append(("kate avatar events spitroast isabelle_boob1",805,666))
        if "blowjob" in state:
          rv.append(("kate avatar events spitroast isabelle_head2",1043,72))
          if "looking_up" in state:
            rv.append(("kate avatar events spitroast isabelle_face5",1445,366))
          if "eyes_closed" in state:
            rv.append(("kate avatar events spitroast isabelle_face6",1444,366))
          if "pleading" in state:
            rv.append(("kate avatar events spitroast isabelle_face7",1339,353))
          if "orgasm" in state:
            rv.append(("kate avatar events spitroast isabelle_face8",1358,371))
        elif "isabelle_head_down" in state:
          rv.append(("kate avatar events spitroast isabelle_head3",1079,241))
        else:
          rv.append(("kate avatar events spitroast isabelle_head1",1062,77))
          if "isabelle_nervous" in state:
            rv.append(("kate avatar events spitroast isabelle_face1",1262,341))
          if "isabelle_shock" in state:
            rv.append(("kate avatar events spitroast isabelle_face2",1262,341))
          if "isabelle_crying" in state:
            rv.append(("kate avatar events spitroast isabelle_face3",1262,341))
          if "isabelle_pleading" in state:
            rv.append(("kate avatar events spitroast isabelle_face4",1262,335))
        if "lacey_spread" in state:
          rv.append(("kate avatar events spitroast lacey_arms1",0,331))
          rv.append(("kate avatar events spitroast lacey_sleeves1",0,365))
        elif "lacey_vibrator1" in state:
          rv.append(("kate avatar events spitroast lacey_arm2",0,707))
          rv.append(("kate avatar events spitroast lacey_sleeve2",37,1009))
        elif "lacey_vibrator2" in state:
          rv.append(("kate avatar events spitroast lacey_arm3",9,683))
          rv.append(("kate avatar events spitroast lacey_sleeve3",7,983))
        elif "lacey_vibrator3" in state:
          rv.append(("kate avatar events spitroast lacey_arm4",0,645))
          rv.append(("kate avatar events spitroast lacey_sleeve4",0,945))
        elif "lacey_vibrator4" in state:
          rv.append(("kate avatar events spitroast lacey_arm5",0,635))
          rv.append(("kate avatar events spitroast lacey_sleeve5",0,934))
        if "stacy_backfoot" in state:
          rv.append(("kate avatar events spitroast stacy_feet2",345,0))
          rv.append(("kate avatar events spitroast stacy_shoes2",507,0))
        elif "stacy_buttplug" in state:
          rv.append(("kate avatar events spitroast stacy_arms",0,331))
          rv.append(("kate avatar events spitroast stacy_sleeve",0,668))
        if "spanking1" in state:
          rv.append(("kate avatar events spitroast mc_hand1",0,9))
        elif "spanking2" in state:
          rv.append(("kate avatar events spitroast mc_hand2",0,105))
        elif "spanking3" in state:
          rv.append(("kate avatar events spitroast mc_hand3",0,227))
        elif "spanking4" in state:
          rv.append(("kate avatar events spitroast mc_hand4",0,114))
        if "squeeze1" in state:
          rv.append(("kate avatar events spitroast mc_hands5",0,302))
        elif "squeeze2" in state:
          rv.append(("kate avatar events spitroast mc_hands6",0,268))
        elif "squeeze3" in state:
          rv.append(("kate avatar events spitroast mc_hands7",0,241))
        elif "squeeze4" in state:
          rv.append(("kate avatar events spitroast mc_hands8",0,225))
        if "anticipation" in state:
          rv.append(("kate avatar events spitroast mc_dick1",0,438))
        elif "penetration1" in state:
          rv.append(("kate avatar events spitroast mc_dick2",6,520))
        elif "penetration2" in state:
          rv.append(("kate avatar events spitroast mc_dick3",60,503))
        elif "penetration3" in state:
          rv.append(("kate avatar events spitroast mc_dick4",116,503))
        elif "penetration4" in state:
          rv.append(("kate avatar events spitroast mc_dick5",178,496))
        if "strap_on" in state or "blowjob1" in state:
          rv.append(("kate avatar events spitroast kate_body1",1108,0))
          if "blowjob1" in state:
            rv.append(("kate avatar events spitroast kate_strap_on",1386,467))
          rv.append(("kate avatar events spitroast kate_underwear1",1700,78))
          rv.append(("kate avatar events spitroast kate_clothes1",1642,78))
          if "strap_on" in state:
            rv.append(("kate avatar events spitroast kate_arm",1413,269))
            rv.append(("kate avatar events spitroast kate_sleeve",1650,266))
            rv.append(("kate avatar events spitroast kate_sleeve_fix",1698,616))
        elif "blowjob2" in state:
          rv.append(("kate avatar events spitroast kate_body2",1109,0))
          rv.append(("kate avatar events spitroast kate_underwear2",1682,34))
          rv.append(("kate avatar events spitroast kate_clothes2",1603,37))
        elif "blowjob3" in state:
          rv.append(("kate avatar events spitroast kate_body3",1108,0))
          rv.append(("kate avatar events spitroast kate_underwear3",1620,42))
          rv.append(("kate avatar events spitroast kate_clothes3",1556,17))
        elif "blowjob4" in state:
          rv.append(("kate avatar events spitroast kate_body4",1108,0))
          rv.append(("kate avatar events spitroast kate_underwear4",1593,0))
          rv.append(("kate avatar events spitroast kate_clothes4",1497,0))

      elif state == "chase":
        rv.append((1920,1080))
        rv.append(("kate avatar events chase background",0,0))
        rv.append(("kate avatar events chase isabelle_body",927,232))
        rv.append(("kate avatar events chase isabelle_underwear",1178,462))
        rv.append(("kate avatar events chase isabelle_blanket",925,231))
        rv.append(("kate avatar events chase isabelle_pig_mask",1060,256))
        rv.append(("kate avatar events chase kate_body",386,100))
        rv.append(("kate avatar events chase kate_underwear",494,416))
        rv.append(("kate avatar events chase kate_clothes",492,376))

      elif state == "trip":
        rv.append((1920,1080))
        rv.append(("kate avatar events trip background",0,0))
        rv.append(("kate avatar events trip isabelle_body",1179,316))
        rv.append(("kate avatar events trip isabelle_underwear",1356,428))
        rv.append(("kate avatar events trip isabelle_blanket",1178,316))
        rv.append(("kate avatar events trip isabelle_pig_mask",1319,325))
        rv.append(("kate avatar events trip kate_body",230,117))
        rv.append(("kate avatar events trip kate_underwear",845,374))
        rv.append(("kate avatar events trip kate_clothes",722,326))

      elif state.startswith("cowering"):
        rv.append((1920,1080))
        rv.append(("kate avatar events cowering background",0,0))
        if "_kateless" not in state:
          rv.append(("kate avatar events cowering kate_body",736,93))
          rv.append(("kate avatar events cowering kate_underwear",905,311))
          rv.append(("kate avatar events cowering kate_clothes",730,263))

      elif state.startswith("hate_sex_"):
        rv.append((1920,1080))
        rv.append(("kate avatar events hate_sex background",0,0))
        if state.endswith("_caught") or "_isabelle" in state:
          if "_surprised" in state or "_disgusted" in state or "_sad" in state:
            rv.append(("kate avatar events hate_sex isabelle_body",778,218))
            rv.append(("kate avatar events hate_sex isabelle_underwear",801,361))
            rv.append(("kate avatar events hate_sex isabelle_dress",790,354))
            if "_surprised" in state:
              rv.append(("kate avatar events hate_sex isabelle_face1",862,248))
            elif "_disgusted" in state:
              rv.append(("kate avatar events hate_sex isabelle_face2",857,248))
            elif "_sad" in state:
              rv.append(("kate avatar events hate_sex isabelle_face3",857,248))
          rv.append(("kate avatar events hate_sex door_open",852,0))
        else:
          rv.append(("kate avatar events hate_sex door_closed",873,76))
        if not state.endswith("_kateless"):
          rv.append(("kate avatar events hate_sex kate_legs",462,816))
          rv.append(("kate avatar events hate_sex kate_bottoms",533,816))
        if state.endswith(("_bent_over","_spanking1","_spanking2","_spanking3","_spanking4","_pulling_panties1","_pulling_panties2","_dick_reveal","_vaginal1","reluctance","_anal1","_aftermath","_scare","_caught")):
          rv.append(("kate avatar events hate_sex mc_right_sleeve1",980,514))
        elif state.endswith(("_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex mc_right_sleeve2",961,506))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex mc_right_sleeve3",954,506))
        elif state.endswith(("_vaginal4","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal4","_cum_inside","_cum_outside")):
          rv.append(("kate avatar events hate_sex mc_right_sleeve4",948,501))
        if state.endswith(("_bent_over","_dick_reveal","_reluctance","_scare")) or "_smile" in state:
          rv.append(("kate avatar events hate_sex kate_body1",483,319))
        elif state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2","_vaginal1","_anal1","_aftermath")) and "_smile" not in state:
          rv.append(("kate avatar events hate_sex kate_body2",483,370))
        elif state.endswith(("_spanking4","_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex kate_body3",483,346))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex kate_body4",483,318))
        elif state.endswith(("_vaginal4","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal4","_cum_inside","_cum_outside")):
          rv.append(("kate avatar events hate_sex kate_body5",483,280))
        elif state.endswith("_caught"):
          rv.append(("kate avatar events hate_sex kate_body6",483,375))
        for spankings in range(quest.isabelle_dethroning["kate_spanking"]):
          if state.endswith(("_bent_over","_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2","_dick_reveal","_vaginal1","_reluctance","_anal1","_aftermath","_scare","_caught")):
            rv.append(("kate avatar events hate_sex kate_redness1",846,649))
            rv.append(("kate avatar events hate_sex kate_redness1_fix",835,638))
          elif state.endswith(("_spanking4","_vaginal2","_anal2")):
            rv.append(("kate avatar events hate_sex kate_redness2",829,611))
            rv.append(("kate avatar events hate_sex kate_redness2_fix",818,601))
          elif state.endswith(("_vaginal3","_anal3")):
            rv.append(("kate avatar events hate_sex kate_redness3",819,572))
            rv.append(("kate avatar events hate_sex kate_redness3_fix",809,562))
          elif state.endswith(("_vaginal4","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal4","_cum_inside","_cum_outside")):
            rv.append(("kate avatar events hate_sex kate_redness4",807,515))
            rv.append(("kate avatar events hate_sex kate_redness4_fix",796,508))
        if state.endswith("_squirt"):
          rv.append(("kate avatar events hate_sex kate_squirt",956,657))
        if state.endswith("_bent_over") or "_smile" in state:
          if state.endswith("_spanking3"):
            rv.append(("kate avatar events hate_sex kate_underwear1b",639,340))
          else:
            rv.append(("kate avatar events hate_sex kate_underwear1a",639,340))
        elif state.endswith(("_dick_reveal","reluctance","_scare")):
          rv.append(("kate avatar events hate_sex kate_underwear2",639,340))
        elif state.endswith(("_spanking1","_spanking2","_pulling_panties1")) and "_smile" not in state:
          rv.append(("kate avatar events hate_sex kate_underwear3a",598,459))
        elif state.endswith("_spanking3"):
          rv.append(("kate avatar events hate_sex kate_underwear3b",598,459))
        elif state.endswith(("_pulling_panties2","_vaginal1","_anal1","_aftermath")):
          rv.append(("kate avatar events hate_sex kate_underwear4",598,459))
        elif state.endswith("_spanking4"):
          rv.append(("kate avatar events hate_sex kate_underwear5",576,438))
        elif state.endswith(("_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex kate_underwear6",576,438))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex kate_underwear7",569,421))
        elif state.endswith(("_vaginal4","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal4","_cum_inside","_cum_outside")):
          rv.append(("kate avatar events hate_sex kate_underwear8",542,377))
        elif state.endswith("_caught"):
          rv.append(("kate avatar events hate_sex kate_underwear9",601,464))
        if quest.isabelle_dethroning["kate_cum"] == "inside" and not state.endswith("_kateless"):
          rv.append(("kate avatar events hate_sex mc_cum1",993,724))
        elif quest.isabelle_dethroning["kate_cum"] == "outside" and not state.endswith("_kateless"):
          rv.append(("kate avatar events hate_sex mc_cum2",797,618))
        if state.endswith(("_bent_over","_dick_reveal","reluctance","_scare")) or "_smile" in state:
          rv.append(("kate avatar events hate_sex kate_clothes1",535,309))
        elif state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2","_vaginal1","_anal1","_aftermath")) and "_smile" not in state:
          rv.append(("kate avatar events hate_sex kate_clothes2",535,355))
        elif state.endswith(("_spanking4","_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex kate_clothes3",535,336))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex kate_clothes4",535,312))
        elif state.endswith(("_vaginal4","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal4","_cum_inside","_cum_outside")):
          rv.append(("kate avatar events hate_sex kate_clothes5",535,280))
        elif state.endswith("_caught"):
          rv.append(("kate avatar events hate_sex kate_clothes6",535,355))
        if state.endswith(("_bent_over","_dick_reveal","reluctance","_scare")) or "_smile" in state:
          rv.append(("kate avatar events hate_sex kate_left_arm1",410,313))
          rv.append(("kate avatar events hate_sex kate_left_sleeve1",704,312))
        elif state.endswith("_caught"):
          rv.append(("kate avatar events hate_sex kate_left_arm6",410,393))
          rv.append(("kate avatar events hate_sex kate_left_sleeve6",561,392))
        if state.endswith(("_bent_over","_dick_reveal","reluctance","_scare")) or "_smile" in state:
          rv.append(("kate avatar events hate_sex kate_head1",610,83))
        elif state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2","_vaginal1","_anal1","_aftermath")) and "_smile" not in state:
          rv.append(("kate avatar events hate_sex kate_head2",535,154))
        elif state.endswith(("_spanking4","_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex kate_head3",545,130))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex kate_head4",532,103))
        elif state.endswith(("_vaginal4","_anal4")) or "_pain" in state:
          rv.append(("kate avatar events hate_sex kate_head5",542,93))
        elif state.endswith(("_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_cum_inside","_cum_outside")) and "_pain" not in state:
          rv.append(("kate avatar events hate_sex kate_head6",556,94))
        elif state.endswith("_caught"):
          rv.append(("kate avatar events hate_sex kate_head7",517,146))
        if state.endswith(("_bent_over","_dick_reveal")) or "_smile" in state:
          rv.append(("kate avatar events hate_sex kate_face1",640,162))
        elif state.endswith(("reluctance","_scare")):
          rv.append(("kate avatar events hate_sex kate_face2",640,162))
        elif state.endswith("_aftermath"):
          rv.append(("kate avatar events hate_sex kate_face3",537,247))
        elif state.endswith("_vaginal1"):
          rv.append(("kate avatar events hate_sex kate_face4",537,244))
        elif state.endswith(("_spanking1","_pulling_panties1","_pulling_panties2")) and "_orgasm" in state:
          rv.append(("kate avatar events hate_sex kate_face5",537,247))
        elif state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2","_anal1")) and "_smile" not in state and "_tearing_up" not in state and "_crying" not in state and "_orgasm" not in state:
          rv.append(("kate avatar events hate_sex kate_face6",537,254))
        elif state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2")) and "_tearing_up" in state:
          rv.append(("kate avatar events hate_sex kate_face7",537,254))
        elif state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2")) and "_crying" in state:
          rv.append(("kate avatar events hate_sex kate_face8",537,247))
        elif state.endswith("_vaginal2"):
          rv.append(("kate avatar events hate_sex kate_face9",547,212))
        elif state.endswith(("_spanking4","_anal2")):
          rv.append(("kate avatar events hate_sex kate_face10",547,222))
        elif state.endswith("_vaginal3"):
          rv.append(("kate avatar events hate_sex kate_face11",537,179))
        elif state.endswith("_anal3"):
          rv.append(("kate avatar events hate_sex kate_face12",537,188))
        elif state.endswith("_vaginal4"):
          rv.append(("kate avatar events hate_sex kate_face13",545,158))
        elif state.endswith("_anal4") or "_pain" in state:
          rv.append(("kate avatar events hate_sex kate_face14",545,166))
        elif state.endswith(("_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_cum_inside","_cum_outside")) and "_pain" not in state:
          rv.append(("kate avatar events hate_sex kate_face15",553,131))
        if state.endswith(("_spanking1","_spanking2","_spanking3","_pulling_panties1","_pulling_panties2","_vaginal1","_anal1","_aftermath")) and "_smile" not in state:
          rv.append(("kate avatar events hate_sex kate_left_arm2",410,377))
          rv.append(("kate avatar events hate_sex kate_left_sleeve2",574,377))
        elif state.endswith(("_spanking4","_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex kate_left_arm3",410,366))
          rv.append(("kate avatar events hate_sex kate_left_sleeve3",576,366))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex kate_left_arm4",410,331))
          rv.append(("kate avatar events hate_sex kate_left_sleeve4",549,331))
        elif state.endswith(("_vaginal4","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal4","_cum_inside","_cum_outside")):
          rv.append(("kate avatar events hate_sex kate_left_arm5",410,280))
          rv.append(("kate avatar events hate_sex kate_left_sleeve5",521,278))
        if state.endswith(("_bent_over","_dick_reveal","reluctance","_scare")) or "_smile" in state:
          rv.append(("kate avatar events hate_sex kate_clothes1_fix",658,307))
        if state.endswith(("_dick_reveal","_squirt","reluctance")):
          rv.append(("kate avatar events hate_sex mc_dick1",994,594))
        elif state.endswith("_vaginal1"):
          rv.append(("kate avatar events hate_sex mc_dick2",1006,710))
        elif state.endswith("_vaginal2"):
          rv.append(("kate avatar events hate_sex mc_dick3",971,702))
        elif state.endswith("_vaginal3"):
          rv.append(("kate avatar events hate_sex mc_dick4",970,679))
        elif state.endswith("_vaginal4"):
          rv.append(("kate avatar events hate_sex mc_dick5",947,624))
        elif state.endswith(("_jerking_off1","_aftermath","_caught","_scare","_kateless")):
          rv.append(("kate avatar events hate_sex mc_dick6",975,610))
        elif state.endswith("_jerking_off2"):
          rv.append(("kate avatar events hate_sex mc_dick7",990,597))
        elif state.endswith("_jerking_off3"):
          rv.append(("kate avatar events hate_sex mc_dick8",990,597))
        elif state.endswith("_jerking_off4"):
          rv.append(("kate avatar events hate_sex mc_dick9",986,601))
        elif state.endswith("_cum_outside"):
          rv.append(("kate avatar events hate_sex mc_dick10",802,387))
        elif state.endswith("_anal1"):
          rv.append(("kate avatar events hate_sex mc_dick11",1017,711))
        elif state.endswith("_anal2"):
          rv.append(("kate avatar events hate_sex mc_dick12",982,682))
        elif state.endswith("_anal3"):
          rv.append(("kate avatar events hate_sex mc_dick13",981,648))
        elif state.endswith("_anal4"):
          rv.append(("kate avatar events hate_sex mc_dick14",962,586))
        elif state.endswith("_cum_inside"):
          rv.append(("kate avatar events hate_sex mc_dick15",961,586))
        if state.endswith(("_bent_over","_spanking1","_spanking2","_spanking3","_spanking4","_pulling_panties1","_pulling_panties2","_dick_reveal","_vaginal1","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","reluctance","_anal1","_cum_outside","_aftermath","_scare","_kateless","_caught")):
          rv.append(("kate avatar events hate_sex mc_pants1",809,517))
        elif state.endswith(("_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex mc_pants2",790,486))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex mc_pants3",759,491))
        elif state.endswith(("_vaginal4","_anal4","_cum_inside")):
          rv.append(("kate avatar events hate_sex mc_pants4",745,470))
        if state.endswith(("_bent_over","_spanking1","_spanking2","_spanking3","_spanking4","_pulling_panties1","_pulling_panties2","_dick_reveal","_squirt","reluctance","_aftermath","_scare","_kateless")):
          rv.append(("kate avatar events hate_sex mc_head1",1121,0))
        elif state.endswith(("_vaginal1","_vaginal2","_vaginal3","_vaginal4","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","_anal1","_anal2","_anal3","_anal4","_cum_inside","_cum_outside")):
          rv.append(("kate avatar events hate_sex mc_head2",1176,0))
        elif state.endswith("_caught"):
          rv.append(("kate avatar events hate_sex mc_head3",1124,0))
        if state.endswith(("_bent_over","_spanking1","_spanking2","_spanking3","_spanking4","_pulling_panties1","_pulling_panties2","_dick_reveal","_vaginal1","_squirt","_jerking_off1","_jerking_off2","_jerking_off3","_jerking_off4","reluctance","_anal1","_cum_outside","_aftermath","_scare","_kateless","_caught")):
          rv.append(("kate avatar events hate_sex mc_hoodie1",1088,155))
        elif state.endswith(("_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex mc_hoodie2",1075,155))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex mc_hoodie3",1064,155))
        elif state.endswith(("_vaginal4","_anal4","_cum_inside")):
          rv.append(("kate avatar events hate_sex mc_hoodie4",1046,155))
        if state.endswith(("_bent_over","_pulling_panties1","_pulling_panties2","_dick_reveal","_squirt","reluctance")):
          rv.append(("kate avatar events hate_sex mc_left_arm1",803,232))
        elif state.endswith("_spanking1"):
          rv.append(("kate avatar events hate_sex mc_left_arm2",1160,25))
        elif state.endswith("_spanking2"):
          rv.append(("kate avatar events hate_sex mc_left_arm3",1160,217))
        elif state.endswith("_spanking3"):
          rv.append(("kate avatar events hate_sex mc_left_arm4",801,228))
        elif state.endswith("_spanking4"):
          rv.append(("kate avatar events hate_sex mc_left_arm5",831,228))
        elif state.endswith(("_vaginal1","_anal1")):
          rv.append(("kate avatar events hate_sex mc_left_arm6",726,232))
        elif state.endswith(("_vaginal2","_anal2")):
          rv.append(("kate avatar events hate_sex mc_left_arm7",722,232))
        elif state.endswith(("_vaginal3","_anal3")):
          rv.append(("kate avatar events hate_sex mc_left_arm8",695,232))
        elif state.endswith(("_vaginal4","_anal4","_cum_inside")):
          rv.append(("kate avatar events hate_sex mc_left_arm9",674,232))
        elif state.endswith(("_jerking_off1","_cum_outside","_aftermath","_caught","_scare","_kateless")):
          rv.append(("kate avatar events hate_sex mc_left_arm10",1010,232))
        elif state.endswith("_jerking_off2"):
          rv.append(("kate avatar events hate_sex mc_left_arm11",1000,232))
        elif state.endswith("_jerking_off3"):
          rv.append(("kate avatar events hate_sex mc_left_arm12",993,232))
        elif state.endswith("_jerking_off4"):
          rv.append(("kate avatar events hate_sex mc_left_arm13",989,231))

      elif state.startswith("passed_out"):
        rv.append((1920,1080))
        rv.append(("kate avatar events passed_out background",0,0))
        rv.append(("kate avatar events passed_out kate_body",0,134))
        rv.append(("kate avatar events passed_out kate_underwear",741,504))
        rv.append(("kate avatar events passed_out kate_clothes",0,397))

      elif state.startswith("really_scared"):
        rv.append((1920,1080))
        rv.append(("kate avatar events really_scared background",0,0))
        rv.append(("kate avatar events really_scared kate_body",7,104))
        rv.append(("kate avatar events really_scared kate_underwear",654,458))
        rv.append(("kate avatar events really_scared kate_clothes",3,350))
        rv.append(("kate avatar events really_scared mc_body",935,623))

      elif state.startswith("isabelle_kiss"):
        rv.append((1920,1080))
        rv.append(("kate avatar events isabelle_kiss background",0,0))
        rv.append(("kate avatar events isabelle_kiss kate_body",384,0))
        rv.append(("kate avatar events isabelle_kiss kate_bra",1309,548))
        rv.append(("kate avatar events isabelle_kiss kate_tops",1018,219))
        rv.append(("kate avatar events isabelle_kiss isabelle_body",58,160))
        rv.append(("kate avatar events isabelle_kiss isabelle_bra",815,699))
        rv.append(("kate avatar events isabelle_kiss isabelle_tops",185,557))
        rv.append(("kate avatar events isabelle_kiss kate_right_hand",103,932))

      elif state.startswith("looking_down"):
        rv.append((1920,1080))
        rv.append(("kate avatar events looking_down background",0,0))
        rv.append(("kate avatar events looking_down kate_body",0,107))
        rv.append(("kate avatar events looking_down kate_underwear",586,242))
        rv.append(("kate avatar events looking_down kate_clothes",0,126))

      elif state.startswith("feet_worship"):
        rv.append((1920,1080))
        rv.append(("kate avatar events feet_worship background",0,0))
        rv.append(("kate avatar events feet_worship kate_upper_body",100,102))
        rv.append(("kate avatar events feet_worship kate_underwear",316,318))
        rv.append(("kate avatar events feet_worship kate_tops",203,293))
        if state.endswith(("right_shoe_kiss","left_shoe_kiss","left_shoe_removal","left_shoe_removed","right_shoe_removal","right_shoe_removed","right_foot_kiss","left_foot_kiss")):
          rv.append(("kate avatar events feet_worship kate_face1",199,184))
        elif state.endswith("right_foot_massage") and "skeptical" not in state:
          rv.append(("kate avatar events feet_worship kate_face2",199,184))
        elif state.endswith(("right_foot_lick","right_toe_suck")):
          rv.append(("kate avatar events feet_worship kate_face3",199,185))
        elif state.endswith("right_toe_choke"):
          rv.append(("kate avatar events feet_worship kate_face4",199,187))
        elif state.endswith("aftermath_confident"):
          rv.append(("kate avatar events feet_worship kate_face5",199,184))
        elif state.endswith(("aftermath_skeptical","failed")) or "skeptical" in state:
          rv.append(("kate avatar events feet_worship kate_face6",199,184))
        elif state.endswith("aftermath_laughing"):
          rv.append(("kate avatar events feet_worship kate_face7",199,184))
        if state.endswith(("right_shoe_kiss","right_shoe_removal","right_shoe_removed","right_foot_kiss","aftermath_confident","aftermath_skeptical","aftermath_laughing")):
          rv.append(("kate avatar events feet_worship kate_lower_body1",519,303))
        elif state.endswith(("left_shoe_kiss","left_shoe_removal","left_shoe_removed","left_foot_kiss")):
          rv.append(("kate avatar events feet_worship kate_lower_body2",585,229))
        elif state.endswith("right_foot_massage"):
          rv.append(("kate avatar events feet_worship kate_lower_body3",519,303))
        elif state.endswith(("right_foot_lick","right_toe_suck","right_toe_choke")):
          rv.append(("kate avatar events feet_worship kate_lower_body4",519,303))
        elif state.endswith("failed"):
          rv.append(("kate avatar events feet_worship kate_lower_body5",585,498))
        if state.endswith("right_shoe_kiss"):
          rv.append(("kate avatar events feet_worship kate_bottoms1",489,295))
        elif state.endswith("left_shoe_kiss"):
          rv.append(("kate avatar events feet_worship kate_bottoms2",489,225))
        elif state.endswith("left_shoe_removal"):
          rv.append(("kate avatar events feet_worship kate_bottoms3",489,271))
        elif state.endswith("left_shoe_removed"):
          rv.append(("kate avatar events feet_worship kate_bottoms4",489,420))
        elif state.endswith("right_shoe_removal"):
          rv.append(("kate avatar events feet_worship kate_bottoms5",489,386))
        elif state.endswith(("right_shoe_removed","right_foot_kiss","right_foot_massage","right_foot_lick","right_toe_suck","right_toe_choke","aftermath_confident","aftermath_skeptical","aftermath_laughing")):
          rv.append(("kate avatar events feet_worship kate_bottoms6",489,564))
        elif state.endswith("left_foot_kiss"):
          rv.append(("kate avatar events feet_worship kate_bottoms7",489,420))
        elif state.endswith("failed"):
          rv.append(("kate avatar events feet_worship kate_bottoms8",489,446))
        if state.endswith(("right_shoe_kiss","right_foot_kiss","right_foot_lick","right_toe_suck","right_toe_choke")):
          rv.append(("kate avatar events feet_worship mc_hands1",1181,797))
        elif state.endswith(("left_shoe_kiss","left_foot_kiss")):
          rv.append(("kate avatar events feet_worship mc_hands2",1249,715))
        elif state.endswith("left_shoe_removal"):
          rv.append(("kate avatar events feet_worship mc_hands3",795,0))
        elif state.endswith("right_shoe_removal"):
          rv.append(("kate avatar events feet_worship mc_hands4",659,0))
        elif state.endswith("right_foot_massage"):
          rv.append(("kate avatar events feet_worship mc_hands5",1346,0))
        if state.endswith(("right_shoe_kiss","right_foot_kiss")):
          rv.append(("kate avatar events feet_worship mc_head1",794,0))
        elif state.endswith(("left_shoe_kiss","left_foot_kiss")):
          rv.append(("kate avatar events feet_worship mc_head2",1065,0))
        elif state.endswith("right_foot_lick"):
          rv.append(("kate avatar events feet_worship mc_head3",838,0))
        elif state.endswith("right_toe_suck"):
          rv.append(("kate avatar events feet_worship mc_head4",878,0))
        elif state.endswith("right_toe_choke"):
          rv.append(("kate avatar events feet_worship mc_head5",846,0))
        elif state.endswith(("aftermath_confident","aftermath_skeptical","aftermath_laughing")):
          rv.append(("kate avatar events feet_worship mc_head6",1100,0))
        elif state.endswith("failed"):
          rv.append(("kate avatar events feet_worship mc_head7",963,0))

      elif state.startswith("spanking_trial"):
        rv.append((1920,1080))
        rv.append(("kate avatar events spanking_trial background",0,0))
        if not state.endswith("kateless"):
          rv.append(("kate avatar events spanking_trial kate_body",425,163))
          rv.append(("kate avatar events spanking_trial kate_underwear",1179,238))
          rv.append(("kate avatar events spanking_trial kate_clothes",1029,238))
        if state.endswith(("anticipation","spanking1","spanking2","spanking3","spanking4")):
          rv.append(("kate avatar events spanking_trial kate_face1",1578,198))
        elif state.endswith(("spanking1_red","spanking2_red","spanking3_red","spanking4_red")):
          rv.append(("kate avatar events spanking_trial kate_face2",1578,198))
        elif state.endswith(("spanking1_redder","spanking2_redder","spanking3_redder","spanking4_redder")):
          rv.append(("kate avatar events spanking_trial kate_face3",1575,198))
        elif state.endswith(("spanking1_reddest","spanking2_reddest","spanking3_reddest","spanking4_reddest")):
          rv.append(("kate avatar events spanking_trial kate_face4",1569,198))
        elif state.endswith("aftermath"):
          rv.append(("kate avatar events spanking_trial kate_face5",1570,198))
        elif state.endswith(("failed","failed_red","failed_redder")):
          rv.append(("kate avatar events spanking_trial kate_face6",1577,199))
        if state.endswith(("kateless","anticipation")) or "workaround" in state:
          rv.append(("kate avatar events spanking_trial mc_body1",0,156))
        elif state.endswith("spanking1"):
          rv.append(("kate avatar events spanking_trial mc_body2",0,156))
        elif state.endswith("spanking2"):
          rv.append(("kate avatar events spanking_trial mc_body3",0,156))
        elif state.endswith("spanking3"):
          rv.append(("kate avatar events spanking_trial mc_body4",0,156))
        elif state.endswith(("spanking4","failed")):
          rv.append(("kate avatar events spanking_trial mc_body5",0,156))
        elif state.endswith("spanking1_red"):
          rv.append(("kate avatar events spanking_trial mc_body6",0,156))
        elif state.endswith("spanking2_red"):
          rv.append(("kate avatar events spanking_trial mc_body7",0,156))
        elif state.endswith("spanking3_red"):
          rv.append(("kate avatar events spanking_trial mc_body8",0,156))
        elif state.endswith(("spanking4_red","failed_red")):
          rv.append(("kate avatar events spanking_trial mc_body9",0,156))
        elif state.endswith("spanking1_redder"):
          rv.append(("kate avatar events spanking_trial mc_body10",0,156))
        elif state.endswith("spanking2_redder"):
          rv.append(("kate avatar events spanking_trial mc_body11",0,156))
        elif state.endswith("spanking3_redder"):
          rv.append(("kate avatar events spanking_trial mc_body12",0,156))
        elif state.endswith(("spanking4_redder","failed_redder")):
          rv.append(("kate avatar events spanking_trial mc_body13",0,156))
        elif state.endswith("spanking1_reddest"):
          rv.append(("kate avatar events spanking_trial mc_body14",0,156))
        elif state.endswith("spanking2_reddest"):
          rv.append(("kate avatar events spanking_trial mc_body15",0,156))
        elif state.endswith("spanking3_reddest"):
          rv.append(("kate avatar events spanking_trial mc_body16",0,156))
        elif state.endswith("spanking4_reddest"):
          rv.append(("kate avatar events spanking_trial mc_body17",0,156))
        elif state.endswith("aftermath"):
          rv.append(("kate avatar events spanking_trial mc_body18",0,156))
        if state.endswith("anticipation"):
          rv.append(("kate avatar events spanking_trial kate_arms1",1210,200))
        elif state.endswith(("spanking1","spanking1_red","spanking1_redder","spanking1_reddest")):
          rv.append(("kate avatar events spanking_trial kate_arms2",1320,0))
        elif state.endswith(("spanking2","spanking2_red","spanking2_redder","spanking2_reddest")):
          rv.append(("kate avatar events spanking_trial kate_arms3",833,197))
        elif state.endswith(("spanking3","spanking3_red","spanking3_redder","spanking3_reddest")):
          rv.append(("kate avatar events spanking_trial kate_arms4",1309,141))
        elif state.endswith(("spanking4","spanking4_red","spanking4_redder","spanking4_reddest")):
          rv.append(("kate avatar events spanking_trial kate_arms5",1314,4))
        elif state.endswith("aftermath"):
          rv.append(("kate avatar events spanking_trial kate_arms6",352,200))
        elif state.endswith(("failed","failed_red","failed_redder")):
          rv.append(("kate avatar events spanking_trial kate_arms7",457,200))

      elif state.startswith("pegging"):
        rv.append((1920,1080))
        rv.append(("kate avatar events pegging background",0,0))
        if state.endswith(("insertion","penetration1_grinner","penetration1_grinnest")):
          rv.append(("kate avatar events pegging kate_body1",69,243))
        elif state.endswith(("penetration2_grin","penetration2_grinner","penetration2_grinnest")):
          rv.append(("kate avatar events pegging kate_body2",70,244))
        elif state.endswith(("penetration3_grin","penetration3_grinner","penetration3_grinnest")):
          rv.append(("kate avatar events pegging kate_body3",72,257))
        elif state.endswith(("penetration4_grinner","penetration4_grinnest","cum")):
          rv.append(("kate avatar events pegging kate_body4",71,260))
        elif state.endswith("aftermath"):
          rv.append(("kate avatar events pegging kate_body5",69,243))
        if state.endswith(("insertion","penetration1_grinner","penetration1_grinnest","aftermath")):
          rv.append(("kate avatar events pegging kate_bra1",960,393))
          rv.append(("kate avatar events pegging kate_tops1",856,354))
          rv.append(("kate avatar events pegging mc_body1",0,35))
        elif state.endswith(("penetration2_grin","penetration2_grinner","penetration2_grinnest")):
          rv.append(("kate avatar events pegging kate_bra2",961,393))
          rv.append(("kate avatar events pegging kate_tops2",856,354))
          rv.append(("kate avatar events pegging mc_body2",0,26))
        elif state.endswith(("penetration3_grin","penetration3_grinner","penetration3_grinnest")):
          rv.append(("kate avatar events pegging kate_bra3",957,393))
          rv.append(("kate avatar events pegging kate_tops3",853,354))
          rv.append(("kate avatar events pegging mc_body3",0,17))
        elif state.endswith(("penetration4_grinner","penetration4_grinnest","cum")):
          rv.append(("kate avatar events pegging kate_bra4",998,384))
          rv.append(("kate avatar events pegging kate_tops4",871,352))
          rv.append(("kate avatar events pegging mc_body4",0,41))
        if state.endswith("insertion"):
          rv.append(("kate avatar events pegging kate_arms1",32,197))
          rv.append(("kate avatar events pegging kate_sleeves1",533,197))
        elif state.endswith(("penetration1_grinner","penetration1_grinnest","aftermath")):
          rv.append(("kate avatar events pegging kate_arms2",32,227))
          rv.append(("kate avatar events pegging kate_sleeves2",372,259))
        elif state.endswith(("penetration2_grin","penetration2_grinner","penetration2_grinnest")):
          rv.append(("kate avatar events pegging kate_arms3",88,228))
          rv.append(("kate avatar events pegging kate_sleeves3",371,259))
        elif state.endswith(("penetration3_grin","penetration3_grinner","penetration3_grinnest")):
          rv.append(("kate avatar events pegging kate_arms4",116,239))
          rv.append(("kate avatar events pegging kate_sleeves4",371,266))
        elif state.endswith(("penetration4_grinner","penetration4_grinnest","cum")):
          rv.append(("kate avatar events pegging kate_arms5",175,237))
          rv.append(("kate avatar events pegging kate_sleeves5",391,268))
        if state.endswith(("insertion","penetration1_grinner","penetration1_grinnest")):
          rv.append(("kate avatar events pegging mc_dick1",551,460))
        elif state.endswith(("penetration2_grin","penetration2_grinner","penetration2_grinnest")):
          rv.append(("kate avatar events pegging mc_dick2",566,457))
        elif state.endswith(("penetration3_grin","penetration3_grinner","penetration3_grinnest")):
          rv.append(("kate avatar events pegging mc_dick3",573,463))
        elif state.endswith(("penetration4_grinner","penetration4_grinnest")):
          rv.append(("kate avatar events pegging mc_dick4",578,478))
        elif state.endswith("cum"):
          rv.append(("kate avatar events pegging mc_dick5",208,57))
        elif state.endswith("aftermath"):
          rv.append(("kate avatar events pegging mc_dick6",36,479))
        if state.endswith(("insertion","penetration1_grinner","penetration1_grinnest","aftermath")):
          rv.append(("kate avatar events pegging kate_head1",1206,27))
        elif state.endswith(("penetration2_grin","penetration2_grinner","penetration2_grinnest")):
          rv.append(("kate avatar events pegging kate_head2",1230,34))
        elif state.endswith(("penetration3_grin","penetration3_grinner","penetration3_grinnest")):
          rv.append(("kate avatar events pegging kate_head3",1270,46))
        elif state.endswith(("penetration4_grinner","penetration4_grinnest","cum")):
          rv.append(("kate avatar events pegging kate_head4",1290,38))
        if state.endswith("insertion"):
          rv.append(("kate avatar events pegging kate_face1",1358,153))
        elif state.endswith("penetration2_grin"):
          rv.append(("kate avatar events pegging kate_face2",1375,147))
        elif state.endswith("penetration3_grin"):
          rv.append(("kate avatar events pegging kate_face3",1409,155))
        elif state.endswith("penetration4_grinner"):
          rv.append(("kate avatar events pegging kate_face4",1417,142))
        elif state.endswith("penetration3_grinner"):
          rv.append(("kate avatar events pegging kate_face5",1409,155))
        elif state.endswith("penetration2_grinner"):
          rv.append(("kate avatar events pegging kate_face6",1375,147))
        elif state.endswith("penetration1_grinner"):
          rv.append(("kate avatar events pegging kate_face7",1358,153))
        elif state.endswith(("penetration4_grinnest","cum")):
          rv.append(("kate avatar events pegging kate_face8",1418,142))
        elif state.endswith("penetration3_grinnest"):
          rv.append(("kate avatar events pegging kate_face9",1409,155))
        elif state.endswith("penetration2_grinnest"):
          rv.append(("kate avatar events pegging kate_face10",1375,147))
        elif state.endswith("penetration1_grinnest"):
          rv.append(("kate avatar events pegging kate_face11",1358,153))
        elif state.endswith("aftermath"):
          rv.append(("kate avatar events pegging kate_face12",1358,153))

      return rv

  class Interactable_kate(Interactable):

    def title(cls):
      return kate.name

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):

      if quest.kate_desire["nude_model"] and game.hour in (16,17,18) and mc.at("school_art_class"):
        actions.append(["consume","\''Paint\''","quest_kate_desire_nude_model"])

      else:
        if ((quest.kate_blowjob_dream.in_progress and quest.kate_blowjob_dream == "school")
        or quest.lindsey_wrong in ("verywet","guard","mopforkate","cleanforkate")
        or (quest.kate_fate.in_progress and quest.kate_fate < "escaped")
        or (quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"])
        or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
        or quest.jo_washed.in_progress
        or quest.fall_in_newfall.in_progress):
          pass
        #if not quest.kate_blowjob_dream.in_progress or not quest.kate_blowjob_dream=="school":
        else:
          if kate.at("school_first_hall_west","sleeping"):
            actions.append(["talk","Talk","?kate_talk_sleep"])
          elif quest.isabelle_dethroning.finished:
            actions.append(["talk","Talk","?kate_talk_vulnerable"])
          elif not (kate.lust>1) or not kate["talk_limit_today"]<3:
            actions.append(["talk","Talk","?kate_talk_default"])
          else:
            actions.append(["talk","Talk","?kate_talk_one"])
            actions.append(["talk","Talk","?kate_talk_two"])
            actions.append(["talk","Talk","?kate_talk_three"])
            actions.append(["talk","Talk","?kate_talk_four"])
            actions.append(["talk","Talk","?kate_talk_five"])

        #####Interactions that can happen at any time#######
        if mc["focus"]:
          if mc["focus"] == "kate_blowjob_dream":
            if quest.kate_blowjob_dream == "school":
              actions.append(["quest","Quest","?kate_quest_kate_blowjob_dream_school"])
          if mc["focus"] == "lindsey_wrong":
            if quest.lindsey_wrong == "verywet":
              actions.append(["quest","Quest","?kate_quest_lindsey_wrong_verywet"])
            elif quest.lindsey_wrong == "cleanforkate":
              actions.append(["quest","Quest","kate_quest_lindsey_wrong_cleanforkate"])
          elif mc["focus"] == "kate_desire":
            if quest.kate_desire == "holdup" and game.location == "school_first_hall_west":
              actions.append(["quest","Quest","?quest_kate_desire_holdup"])
            elif quest.kate_desire == "delivery":
              actions.append(["quest","Quest","?quest_kate_desire_nude_delivery"])
          elif mc["focus"] == "maxine_hook":
            if quest.maxine_hook == "interrogation":
              actions.append(["quest","Quest","?quest_maxine_hook_interrogation_kate"])
          elif mc["focus"] == "kate_wicked":
            if quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"]:
              actions.append(["quest","Quest","?quest_kate_wicked_school_kate"])
          elif mc["focus"] == "isabelle_dethroning":
            if quest.isabelle_dethroning == "dinner" and game.hour == 19:
              if "kate" not in quest.isabelle_dethroning["dinner_conversations"]:
                actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_kate"])
              if "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
                actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_kate"])
          elif mc["focus"] == "jo_washed":
            if quest.jo_washed == "supplies" and not mc.owned_item("car_wash_sign"):
              actions.append(["quest","Quest","quest_jo_washed_supplies_car_wash_signs"])
            if quest.jo_washed.in_progress:
              actions.append(random.choice(quest_jo_washed_interact_lines))
          elif mc["focus"] == "kate_moment":
            if quest.kate_moment == "next_day":
              actions.append(["quest","Quest","quest_kate_moment_next_day"])
        else:

          #############stolen_hearts##########################
          if quest.isabelle_stolen  == "warnkate":
            actions.append(["quest","Quest","?isabelle_quest_stolen_warnkate"])
          elif quest.isabelle_stolen == "askmaxine":
            actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_kate"])

          ##########kate_search_for_nurse##############
          if quest.kate_search_for_nurse=="meet_kate":
            if game.location == "school_nurse_room":
              actions.append(["quest","Quest","kate_quest_search_for_nurse_meet_kate"])

          ##########kate_over_isabelle##############
          if quest.kate_over_isabelle.in_progress:

            if quest.kate_over_isabelle in ("the_winning_team","talking_to_kate"):
              actions.append(["quest","Quest","?kate_quest_isabelle_tour_kate_over_isabelle"])

            if quest.kate_over_isabelle=="report":
              actions.append(["quest","Quest","?kate_quest_isabelle_tour_kate_over_isabelle_cafeteria"])

          ##########isabelle_tour##############
          elif quest.isabelle_tour.in_progress and quest.isabelle_tour< "english_class":

            if quest.isabelle_tour == "confrontation_side_isabelle":
              actions.append(["quest","Quest","?kate_quest_isabelle_tour_confront_side_isabelle"])

            elif quest.isabelle_tour == "confrontation_side_kate":
              actions.append(["quest","Quest","?kate_quest_isabelle_tour_confront_side_kate"])

          ###########gathering storm###########
          if quest.isabelle_locker == "interrogate" and not quest.isabelle_locker["kate_interrogated"] and not quest.isabelle_locker["might_have_an_idea"]:
            actions.append(["quest","Quest","quest_isabelle_locker_interrogate_kate"])

          ############kate_desire##############
          if quest.kate_desire == "challenge":
            actions.append(["quest","Quest","?quest_kate_desire_challenge_kate"])
          elif quest.kate_desire == "gym" and game.location == "school_gym":
            actions.append(["quest","Quest","?quest_kate_desire_kate_quest_gym"])

          #########hurricane isabelle#########
          if quest.isabelle_hurricane == "blowjob" and not quest.isabelle_hurricane["cocksucker"]:
            actions.append(["quest","Quest","?quest_isabelle_hurricane_blowjob_kate"])

          ########stepping on the rose########
          if quest.kate_stepping == "gym" and game.location == "school_gym":
            actions.append(["quest","Quest","quest_kate_stepping_gym"])

          ############wicked game#############
          if quest.isabelle_dethroning == "newspaper" and mc.owned_item("newspaper") and not quest.isabelle_dethroning["disturbing_the_peace"]:
            actions.append(["quest","Quest","quest_isabelle_dethroning_newspaper_kate"])
          elif quest.isabelle_dethroning == "invitations" and "kate" not in quest.isabelle_dethroning["invitations"]:
            actions.append(["quest","Quest","quest_isabelle_dethroning_invitations_kate"])

          ##########fall in newfall##########
          if quest.isabelle_dethroning.finished and quest.fall_in_newfall == "late" and "kate" not in quest.fall_in_newfall["assembly_conversations"]:
            actions.append(["quest","Quest","quest_fall_in_newfall_late_school_homeroom_kate"])

          ##########spell you later##########
          if quest.maya_spell == "hair" and quest.isabelle_dethroning.finished and game.location == "school_first_hall":
            actions.append(["quest","Quest","quest_maya_spell_hair_kate"])

          ##########vicious intrigue##########
          if quest.kate_intrigue == "snitch":
            actions.append(["quest","Quest","quest_kate_intrigue_snitch"])
          elif quest.kate_intrigue == "report":
            actions.append(["quest","Quest","quest_kate_intrigue_report"])

          ########## quest interactions for picking up new quests go down here ############
          ########## so they don't pop up while another quest is in_progress ##############

          if game.season == 1:
            if quest.lindsey_nurse.ended and not quest.kate_search_for_nurse.started and game.location == "school_nurse_room":
              actions.append(["quest","Quest","?kate_quest_search_for_nurse_start"])

            if quest.isabelle_tour.finished and quest.kate_search_for_nurse.finished and quest.kate_fate.finished and not quest.kate_desire.started and game.location == "school_gym":
              actions.append(["quest","Quest","?quest_kate_desire_start"])

            if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and quest.maxine_eggs.finished and quest.isabelle_piano.finished and quest.nurse_photogenic.finished and quest.flora_handcuffs.finished and quest.kate_trick.finished and not quest.kate_stepping.started:
              actions.append(["quest","Quest","?quest_kate_stepping_start"])

        ##########talk flirt##############
        if ((quest.kate_blowjob_dream.in_progress and quest.kate_blowjob_dream == "school")
        or quest.lindsey_wrong in ("verywet","guard","mopforkate","cleanforkate")
        or (quest.kate_fate.in_progress and quest.kate_fate < "escaped")
        or (quest.kate_wicked == "school" and quest.kate_wicked["ironic_costume"])
        or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
        or quest.jo_washed.in_progress
        or quest.fall_in_newfall.in_progress):
          pass
        elif kate.at("school_first_hall_west","sleeping"):
          actions.append(["flirt","Flirt","?kate_flirt_sleep"])
        elif quest.isabelle_dethroning.finished:
          actions.append(["flirt","Flirt","?kate_flirt_vulnerable"])
        else:
          actions.append(["flirt","Flirt","?kate_flirt_default"])


label kate_talk_default:
  show kate cringe with Dissolve(.5)
  kate "Gross."
  kate "Don't talk to me."
  hide kate cringe with Dissolve(.5)
  return

label kate_talk_one:
  $kate["talk_limit_today"]+=1
  show kate smile with Dissolve(.5)
  kate smile "I'm hosting a party this weekend. Everyone's coming."
  kate confident "Well, everyone relevant."
  hide kate cringe with Dissolve(.5)
  return
label kate_talk_two:
  $kate["talk_limit_today"]+=1
  show kate confident with Dissolve(.5)
  kate confident "Do you have a lot of followers on Titter?"
  kate laughing "Oh, what am I saying? Of course, you don't."
  hide kate cringe with Dissolve(.5)
  return
label kate_talk_three:
  $kate["talk_limit_today"]+=1
  show kate confident with Dissolve(.5)
  kate confident "The last girl who challenged me for the crown is no longer around."
  kate confident "She dropped out and moved away from the city."
  kate neutral "The girls and I sometimes visit her farm to make sure she's still a good piggy."
  hide kate cringe with Dissolve(.5)
  return
label kate_talk_four:
  $kate["talk_limit_today"]+=1
  show kate cringe with Dissolve(.5)
  kate cringe "What the hell?"
  kate cringe "How long have you been staring at my ass?"
  kate eyeroll "I can't believe this school doesn't have stricter sexual harassment regulations."
  hide kate cringe with Dissolve(.5)
  return
label kate_talk_five:
  $kate["talk_limit_today"]+=1
  show kate cringe with Dissolve(.5)
  kate cringe "Some of the girls on the squad have complained about you."
  kate neutral "Stay out of the gym and stop being a creep."
  kate neutral "I don't ever want your name popping up in my group chat again."
  hide kate cringe with Dissolve(.5)
  return

label kate_flirt_default:
  show kate afraid with Dissolve(.5)
  kate "Yuck."
  kate "Don't look at me."
  hide kate cringe with Dissolve(.5)
  return

label kate_talk_vulnerable:
  show kate thinking with Dissolve(.5)
  window auto show
  show kate embarrassed with dissolve2
  kate embarrassed "Aaah!" with vpunch
  mc "Why so jumpy?"
  kate embarrassed "Don't creep up on people like that!"
  window hide
  hide kate with Dissolve(.5)
  return

label kate_flirt_vulnerable:
  show kate sad with Dissolve(.5)
  "[kate] has that look of perpetual misery on her face these days..."
  mc "You look tired."
  kate sad "Leave me alone."
  window hide
  hide kate with Dissolve(.5)
  return

label kate_talk_sleep:
  kate "ZzzZzzzzZz..."
  return

label kate_flirt_sleep:
  kate "ZzzZzzzzZz..."
  return
