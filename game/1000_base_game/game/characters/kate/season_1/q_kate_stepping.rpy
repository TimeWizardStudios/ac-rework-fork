label quest_kate_stepping_start:
  show kate skeptical with Dissolve(.5)
  kate skeptical "What have I told you about looking at me?"
  mc "..."
  kate skeptical "Well, what do you want?"
  mc "I've scheduled the meeting with [maxine]."
  kate excited "Good! Let's go, then!"
  mc "..."
  kate eyeroll "What is it, [mc]?"
  show kate eyeroll at move_to(.2)
  menu(side="right"):
    extend ""
    "\"Nothing, just admiring your perfect hair.\"":
      show kate eyeroll at move_to(.5)
      mc "Nothing, just admiring your perfect hair."
      kate skeptical "Well, quit gawking and start walking. I don't have all day."
      mc "Yes, ma'am..."
      kate excited "Your compliments are useless, anyway. Why should I care about your opinion?"
      show kate excited at move_to(.75)
      menu(side="left"):
        extend ""
        "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"You absolutely shouldn't,\nbut this isn't my opinion.\"":
          show kate excited at move_to(.5)
          mc "You absolutely shouldn't, but this isn't my opinion."
          mc "I was reading [flora]'s Cosmololitan last night and saw a model with your exact hairstyle, only less shiny."
          mc "It's been voted hairstyle of the year by a panel of distinguished fashion judges."
          kate thinking "Really?"
          mc "Yes, you're ahead of Paris in both style and glamour."
          $kate.lust+=2
          kate blush "I like that! I want others to know about it too!"
          kate blush "Print up that article and post it on the notice board."
          mc "Yes, ma'am. How about I take a photo of you and post it right\nnext to it?"
          kate gushing "Yes! I want you to merge a photo of me with the article."
          kate gushing "Take out your phone right now."
          window hide
          show black onlayer screens zorder 100
          show kate posing
          with Dissolve(.5)
          pause 1.0
          hide black onlayer screens with Dissolve(.5)
          window auto
          kate posing "One picture should be enough. It'll look fabulous regardless."
          kate posing "Go on, get the shot!"
          window hide
          pause 0.25
          show expression "kate avatar events posing camera_flash" with Dissolve(.125)
          play sound "camera_snap"
          hide expression "kate avatar events posing camera_flash" with Dissolve(.125)
          window auto
          mc "Got it!"
          window hide
          show black onlayer screens zorder 100
          show kate blush
          with Dissolve(.5)
          pause 1.0
          hide black onlayer screens with Dissolve(.5)
          window auto
          kate blush "Make sure you get that professionally edited. I deserve the best, don't you agree?"
          mc "Naturally, ma'am."
          kate blush "I expect to see it on a school wall somewhere soon. Right above the newspapers would be fine."
          "Hopefully, [flora] still has that magazine somewhere. Reading it last night was a bit of a lie."
          kate blush "We're not going to that meeting until you've taken care of this."
          hide kate with Dissolve(.5)
          if quest.kate_over_isabelle.in_progress:
            if game.quest_guide == "kate_over_isabelle":
              $game.quest_guide = "kate_stepping"
            $quest.kate_over_isabelle.finish(silent=True)
            $quest.kate_stepping.start("magazine", silent=True)
          else:
            $quest.kate_stepping.start("magazine")
          return
        "\"Because it's the truth!\"":
          show kate excited at move_to(.5)
          mc "Because it's the truth!"
          kate neutral "I already know it's the truth. Do you really think I need {i}you{/} to\ntell me that?"
          mc "I guess not..."
          kate neutral "So, unless I ask for your opinion in the future, you're going to\ndo what?"
          mc "Be quiet?"
          kate confident "Yes, a bad breath counts as air pollution."
          mc "Sorry, ma'am."
          kate confident "It's okay. Just let me do the talking in the meeting with [maxine]."
        "\"I was just trying to be nice!\"":
          show kate excited at move_to(.5)
          mc "I was just trying to be nice!"
          kate skeptical "Don't give me attitude. I don't care for it one bit."
          show kate skeptical at move_to(.2)
          menu(side="right"):
            extend ""
            "\"Sorry, ma'am.\"":
              show kate skeptical at move_to(.5)
              mc "Sorry, ma'am."
              kate neutral "There'll be no more of that."
              kate neutral "What do you have to say for yourself?"
              mc "That was very rude of me. I'll behave from now on."
              kate confident "Good. Thank me for setting you straight."
              mc "Thanks for setting me straight, ma'am."
              $kate.lust+=2
              kate confident "That's better. Now move."
              hide kate with dissolve2
              "Giving in to [kate] feels natural. She is always so controlled and sure of herself. I love it."
            "\"I think you're being unfair.\"":
              show kate skeptical at move_to(.5)
              mc "I think you're being unfair."
              kate eyeroll "If I want your opinion, I'll ask for it."
              mc "It was just a compliment..."
              kate annoyed "You're really starting to get on my nerves."
              mc "You could've just said, \"Thank you.\""
              $kate.lust-=1
              kate angry "Are you going to keep arguing?"
              show kate angry at move_to(.75)
              menu(side="left"):
                extend ""
                "\"No, ma'am.\"":
                  show kate angry at move_to(.5)
                  mc "No, ma'am."
                  kate neutral "Good. Apologize."
                  mc "I'm sorry, ma'am."
                  kate neutral "There'll be no more of that."
                  mc "No, ma'am."
                  kate neutral "We understand each other, then?"
                  mc "Yes, ma'am."
                  $kate.lust+=2
                  kate confident "That's better. Now move."
                  hide kate with dissolve2
                  "God, it's so hard to get a word in with [kate]. She absolutely dominates the conversation."
                  "Even when she's in the wrong, she handles it perfectly."
                  "It feels like every conversation puts me deeper under her spell..."
                  "Obeying is just so much easier than standing up for myself."
                "\"It's not really arguing...\"":
                  show kate angry at move_to(.5)
                  mc "It's not really arguing..."
                  $kate.lust-=2
                  kate embarrassed "That's it. I'm going to the meeting alone."
                  mc "What?"
                  kate embarrassed "You clearly don't know how to behave, so I can't bring you along."
                  kate embarrassed "You're not trained properly, and we'll have to correct that first."
                  kate embarrassed "Meet me in the gym for an attitude adjustment."
                  mc "Look, [kate], I set up the meeting and I'm going to it now. You can come with me or not."
                  kate angry "You're going to regret this..."
    "\"I was hoping for a reward.\"":
      show kate eyeroll at move_to(.5)
      mc "I was hoping for a reward."
      kate skeptical "Why am I not surprised?"
      kate excited "Fine. I suppose you've been quite good."
      kate excited "What do you want, then?"
      show kate excited at move_to(.75)
      menu(side="left"):
        extend ""
        "\"A hug.\"":
          show kate excited at move_to(.5)
          $mc.love+=1
          mc "A hug."
          kate blush "That's adorable."
          mc "..."
          $kate.lust-=1
          kate embarrassed "It's also revolting. I'm not touching you."
          "Oh, well. Didn't hurt to try."
          "Maybe hugs are [kate]'s weakness? Showing genuine affection is probably something she's incapable of."
          mc "I feel sorry for you."
          kate eyeroll "I don't give a shit about your feelings."
          kate eyeroll "Let's just go."
        "\"Your panties.\"":
          show kate excited at move_to(.5)
          $mc.lust+=1
          mc "Your panties."
          kate flirty "Who says I'm wearing any?"
          mc "!!!"
          kate laughing "Forget it, weirdo."
          "Crap. For a moment I thought... ugh, never lucky..."
          kate smile "Enough of this. We have a meeting to attend to."
        "\"Cash.\"":
          show kate excited at move_to(.5)
          mc "Cash."
          kate laughing "So, you're not just dense and ugly, you're also poor?"
          kate laughing "Luckily for you, I'm a very charitable person. Here you go."
          $mc.money+=50
          mc "..."
          kate neutral "What do you say?"
          mc "Thanks, ma'am."
          kate confident "That's better. Let's go to that meeting, you little gold digger."
        "?mc.lust>=3@[mc.lust]/3|{image=stats lust}|\"A slap in the\nface, please.\"":
          show kate excited at move_to(.5)
          mc "A slap in the face, please."
          $kate.lust+=3
          kate flirty "That'd be a good reward for someone like you."
          kate flirty "I'd be more than happy to give it to you... hard."
          kate neutral_slap "Present your cheek."
          mc "Yes, ma'am."
          window hide
          show kate neutral_slap:
            linear 0.25 zoom 2.5 yoffset 1250
          pause 0.25
          show white onlayer screens zorder 100 with Dissolve(.1)
          play sound "slap"
          $renpy.transition(vpunch, layer="screens")
          pause 0.25
          show kate neutral_slap:
            linear 0.25 zoom 1.0 yoffset 0
          hide white onlayer screens with Dissolve(.1)
          window auto
          "Ouch!"
          kate neutral_slap "What do you say?"
          mc "T-thank you, ma'am..."
          kate confident "Good boy."
          kate confident "Okay, let's get this meeting over with."
    "?mc.strength>=6@[mc.strength]/6|{image=stats str}|\"Allow me to clear a path for you.\"{space=-30}":
      show kate eyeroll at move_to(.5)
      mc "Allow me to clear a path for you."
      kate laughing "Do I look like I need help with that?"
      mc "Of course not!"
      kate neutral "People scurry out of my way if I give them a stern look."
      mc "I know, ma'am. I just thought you might want to relax for a moment and let me do all the work."
      $kate.lust+=2
      kate confident "Those hours at the gym did you some good, didn't they? You're no longer a complete wimp."
      kate confident "I actually have to make a call on the way there. Make sure these idiots keep quiet."
      mc "Absolutely, ma'am."
    "\"Just remember that we're partners.\"":
      show kate eyeroll at move_to(.5)
      mc "Just remember that we're partners."
      kate laughing "Partners! Whoa! That's the funniest thing I've heard!"
      mc "Ugh, let's just get this over with..."
      kate smile "Are you sure?"
      mc "I got you the meeting and all the items you asked for. And I've earned [isabelle]'s trust."
      kate neutral "Do you want a medal? Is that what this is about?"
      mc "No, but I would appreciate it if you stopped being a bitch."
      $kate.lust-=2
      kate neutral "As soon as you stop being an ugly loser, which I doubt will happen any time soon."
      mc "..."
      kate confident "What? Are you out of jokes already?"
      kate confident "I knew you were a shitty comedian."
      mc "Let's just go..."
      kate neutral "..."
      mc "..."
      mc "Please?"
      kate confident "Since you're asking so nicely."
  if renpy.showing("kate"):
    hide kate with Dissolve(.5)
  if quest.kate_over_isabelle.in_progress:
    if game.quest_guide == "kate_over_isabelle":
      $game.quest_guide = "kate_stepping"
    $quest.kate_over_isabelle.finish(silent=True)
    $quest.kate_stepping.start("meeting", silent=True)
  else:
    $quest.kate_stepping.start("meeting")
  return

label quest_kate_stepping_magazine_flora:
  "There's no way I'll be able to break two pacts, six laws, and nine treaties when [flora] is right there."
  return

label quest_kate_stepping_magazine:
  "Opening this door and setting foot inside the unhallowed ground of [flora]'s domain is how you end up as the sacrifice of a dark ritual."
  "Entering breaks at least three deals, nine armstices, and thirteen contracts..."
  "But [kate] must have her beauty displayed for everyone to see..."
  "..."
  "All right, here goes..."
  show flora angry_cop at appear_from_left
  flora angry_cop "Stop right there, criminal scum!"
  mc "What the hell?! Where did you come from?"
  flora concerned_cop "I'm asking the questions here, punk."
  flora concerned_cop "Now, why is your hand on this doorknob?"
  show flora concerned_cop at move_to(.2)
  menu(side="right"):
    extend ""
    "\"I'm just making sure it's locked.\"":
      show flora concerned_cop at move_to(.5)
      mc "I'm just making sure it's locked."
      flora skeptical_cop "And who said you could do that?"
      mc "I'm literally trying to keep your room safe."
      mc "Do I need a permit to be a good Samaritan?"
      flora skeptical_cop "Behind every smile hides a lie."
      mc "So, that's why you're smiling so much!"
      $mc.love+=1
      mc "I like it when you smile."
      flora skeptical_cop "Shut up."
    "\"I heard strange noises\ncoming from your room.\"":
      show flora concerned_cop at move_to(.5)
      mc "I heard strange noises coming from your room."
      flora skeptical_cop "What kind of noises?"
      mc "It sounded like someone was getting smashed by a donkey."
      flora afraid_cop "W-what?"
      mc "That's what it sounded like. Like, {i}hee-haw, hee-haw!{/}"
      flora embarrassed_cop "You're lying!"
      mc "I mean, I could be wrong. Someone might have smashed the donkey.{space=-15}"
      flora sad_cop "You're just making things up at this point. I don't want to hear it."
      mc "I mean, it was loud. You might not have a choice."
      flora sad_cop "Just leave my door alone..."
      $mc.lust+=1
      mc "{i}Hee-haw!{/} I mean, sure!"
    "?mc.strength>=4@[mc.strength]/4|{image=stats str}|\"{i}Your{/} hand is on the doorknob...\"":
      show flora concerned_cop at move_to(.5)
      mc "{i}Your{/} hand is on the doorknob..."
      flora afraid_cop "W-what?"
      mc "You heard me..."
      flora embarrassed_cop "No! Get away from me!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      mc "Sometimes you just have to take the law into your own hands, [flora]..."
      flora "Let go of my hand!"
      mc "We can do this the easy way or the hard way..."
      flora "Let go! I'm telling [jo]!"
      mc "What are you going to tell her? That you pretended to be a cop? That you claimed her authority for yourself?"
      mc "No, little [flora], you're not going to tell her a damn thing... you're just going to put your hand on this doorknob..."
      mc "Or so help me god!" with vpunch
      flora "Eeeek!"
      mc "There we go! Feel it!"
      mc "The brass against the palm of your hand... the smooth material, polished by a million hands before yours..."
      mc "Worn by time and use... oiled by generations of door oilers..."
      mc "No, [flora]. {i}Your{/} hand is on the doorknob."
      show black onlayer screens zorder 100
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      $flora.lust+=2
      $set_dialog_mode("")
      flora embarrassed_cop "What the hell, [mc]?!"
      mc "What?"
      flora sad_cop "I feel dirty and violated."
      mc "Why? You just touched a doorknob. Nothing weird about that."
  mc "Anyway, I need your help with something."
  flora angry_cop "I'm not giving you a boost to the attic so you can spy on the neighbor's pool area again."
  mc "What? Don't be ridiculous."
  mc "You'll help me with that after the sun comes out."
  flora concerned_cop "Fat chance."
  mc "It wasn't what I had in mind, though. I need to borrow a Cosmololitan issue."
  mc "Specifically, the one with the French hairstyles."
  flora concerned_cop "Why?"
  mc "I'm making a poster for [kate]."
  flora concerned_cop "Why?"
  mc "I'm just, err... helping her out?"
  flora skeptical_cop "It's not something creepy or perverted?"
  mc "Of course not!"
  flora skeptical_cop "If I had a cigar, I would be lighting it right now, and say that I don't believe you."
  mc "You think [kate] would let me get away with something like that?"
  flora smile_cop "I suppose not..."
  flora smile_cop "So, what do I get out of helping you?"
  mc "A good word with [kate] and my undying loyalty?"
  flora confident_cop "Yeah, right. I don't really need a good word, especially if it's\ncoming from you."
  flora confident_cop "And if I wanted undying loyalty, I would just ask [jo] for a dog."
  mc "Ugh, fine! I'll just owe you, okay? Next time you need something!"
  flora excited_cop "Nice and simple, I kind of like it. And I especially like being owed."
  flora excited_cop "But! It will come with interest."
  mc "Yes, my interest in helping you."
  flora confident_cop "Ha! Funny guy, don't quit your day job."
  flora concerned_cop "Anyway, I better not find any of the pages sticky. If I do, we're going to have a problem."
  mc "I'll make sure to be careful."
  show flora concerned_cop at disappear_to_left
  "Okay, best get started."
  "My computer is trash, but hopefully it can handle some mild\nphoto editing..."
  $quest.kate_stepping.advance("photoshop")
  return

label quest_kate_stepping_photoshop_wrong_computer:
  "The computer classroom has a printer, but no computer with Photoshop."
  "My bedroom has a computer with Photoshop, but no printer."
  "Life do be like that sometimes."
  return

label quest_kate_stepping_photoshop_computer:
  "I promised [kate] I'd get her picture professionally edited. Time to put my Photoshop skills to the test."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 1.0
  $game.advance(hours=2)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Phew, all done! That took way too many hours."
  "Now to print it out..."
  $quest.kate_stepping["print_it_out"] = True
  return

label quest_kate_stepping_photoshop_printer:
  "Hopefully, there's some ink left in this old lady..."
  "..."
  "It's making printing noises..."
  "..."
  "..."
  "Oh, it's working!"
  $mc.add_item("kate_fashion_poster")
  "Now, where should I hang it up?"
  "[kate] would want it hung in a place where lots of people would\nsee it..."
  $quest.kate_stepping.advance("poster")
  return

label quest_kate_stepping_poster:
  "Hmm... didn't [kate] say something about this wall? Right above the newspapers?"
  show kate confident at appear_from_right
  kate confident "Don't look so happy. It doesn't suit you."
  "Speak of the devil..."
  mc "I was actually looking for you."
  kate neutral "Of course you were."
  kate neutral "What do you want?"
  mc "I finished your poster."
  kate thinking "Did you, now?"
  kate thinking "Let's see it, then."
  $mc.remove_item("kate_fashion_poster")
  kate thinking "..."
  kate blush "Hey, it's not bad! Not bad at all."
  kate blush "Though making it bad would've taken some serious effort on\nyour part."
  mc "Thanks?"
  kate laughing "That wasn't a compliment."
  "Ugh..."
  mc "So, where do you want me to put it?"
  kate smile "Right here will do just fine."
  kate smile "Hang it up now. Chop, chop."
  mc "Okay..."
  show kate neutral with dissolve2
  "[kate]'s watchful gaze follows me as I hold the poster up to the wall."
  "Suddenly, the pressure is on... and I take an extra moment to eye the straightness."
  "It's lined up well. Just stick it in place. You can do—"
  kate neutral "Not there."
  "Shit, here we go..."
  kate skeptical "Don't give me that look."
  kate skeptical "Higher."
  mc "There?"
  kate excited "Higher. I want people to be able to see it, you know?"
  kate excited "Surely, you understand the concept of posters?"
  kate excited "It hardly involves reading, so I'd think you could manage it."
  mc "Yes, I do understand..."
  kate excited "Great. Then, {i}higher{/}."
  "My hands go even higher, straining to please her."
  "A bead of sweat rolls down my brow."
  kate eyeroll "Great, now it's crooked."
  kate eyeroll "Straighten it out."
  "If only I'd brought a leveler..."
  kate skeptical "Wrong way, poster bitch."
  mc "Whoops."
  "It looks perfectly straight to me, but I shift it a nanometer to\nplease her."
  mc "Like... that?"
  "[kate] just stands there for what feels like forever, inspecting\nmy work."
  "My arms begin to tremble, fatigued from holding them up for\nso long."
  kate neutral "I don't like it. Start over."
  mc "For real?"
  kate neutral "I realize that poster hanging is an advanced skill for you, and a chimp could probably do it better..."
  kate confident "...but yes, for real. It needs to be perfect."
  mc "..."
  "Never before has a wall seemed so daunting. So insurmountable."
  "Come on. Get it together."
  mc "Here?"
  kate neutral "More to the right."
  "Really? It looked perfect, but of course it wasn't..."
  mc "Here?"
  "Surely, this is—"
  kate neutral "More."
  mc "How about—"
  kate neutral "More."
  mc "What about—"
  kate neutral "Stop."
  kate neutral "Go back the other way some."
  "Sisyphus had nothing on me..."
  mc "Here?"
  kate thinking "There."
  kate thinking "And make sure it's straight, unlike you."
  mc "I am straight!"
  kate embarrassed "Well, my poster isn't!"
  mc "Fine..."
  "An artist's final touches... a year's worth of sweat, bloody, and dying arms..."
  "In the end, I'm not even sure I moved it at all."
  kate blush "That'll do. Paste it on."
  window hide
  pause 0.25
  $school_first_hall["kate_fashion_poster"] = True
  pause 0.5
  window auto
  "Finally, the poster sticks to the wall, and a sigh of relief fills my lungs with fresh air."
  kate blush "I look great, don't I?"
  mc "Flawless..."
  kate blush "Good boy."
  mc "Can we go to that meeting with [maxine] now?"
  kate skeptical "..."
  mc "..."
  mc "Please?"
  kate excited "Since you're asking so nicely."
  hide kate with Dissolve(.5)
  $quest.kate_stepping.advance("meeting", silent=True)
  return

label quest_kate_stepping_meeting_rope:
  if kate.at("school_homeroom"):
    show kate skeptical with Dissolve(.5)
  else:
    show kate skeptical at appear_from_right
  kate skeptical "What exactly are you doing?"
  mc "This is the only way..."
  kate skeptical "I'm not climbing that rope, you idiot."
  kate excited "Do you see these nails? I had them done this morning."
  mc "But—"
  kate eyeroll "Just make her come down."
  mc "Ugh, fine..."
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_clubroom"
  $mc["focus"] = "kate_stepping"
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_kate_stepping_meeting_window:
  "Some things in life just aren't worth it."
  "One of them is ending up on [kate]'s kill list for going back on\nmy word."
  return

label quest_kate_stepping_meeting_maxine:
  show maxine thinking with Dissolve(.5)
  maxine thinking "Yes?"
  mc "Hey, [maxine]. Do you have time for a meeting with me and [kate]?"
  maxine thinking "I most certainly don't."
  mc "But you promised!"
  maxine sad "I have no such memory."
  mc "Remember when I took that picture of the glowing spider eggs\nfor you?"
  maxine excited "Why, of course!"
  mc "Why did I do that?"
  maxine excited "Because I asked you to."
  mc "And under what conditions did I agree to do it?"
  maxine excited "I believe it was a sunny day with an eastern breeze."
  mc "..."
  mc "You will honor your deal even if I have to tie you up and drag you\nto [kate]."
  maxine concerned "Honor is a dead concept."
  mc "Why do you always have to be so difficult? I did what you asked for!{space=-10}"
  maxine skeptical "But did you do what I didn't ask for?"
  mc "No?"
  maxine confident "Well, there you have it."
  mc "How can I do something you didn't ask for? Pure chance?"
  maxine confident "By reading the room."
  mc "Well, I'm reading the room right now, and the only thing I'm getting is the urge to knock you out."
  maxine thinking "Violent fantasies beget violent actions."
  mc "You're asking for it..."
  maxine thinking "I thought we established that I was, in fact, not asking for it."
  mc "Ugh!"
  window hide
  play sound "notification" volume 0.5
# play sound "<from 0.25 to 1.0>phone_vibrate" volume 0.5
  pause 0.5
  show maxine skeptical_phone with Dissolve(.5)
  window auto
  maxine skeptical_phone "Huh?"
  maxine confident_phone "Oh, it seems I have a meeting to attend to! If you'll excuse me."
  mc "..."
  mc "Seriously?"
  maxine confident_phone "Sorry, it's mildly important."
  $mc["focus"] = ""
  $maxine["at_none_now"] = True
  show maxine confident_phone at disappear_to_left
  "Great. Just great."
  $quest.kate_stepping["mildly_important"] = True
  return

label quest_kate_stepping_meeting:
  show maxine neutral at Transform(xalign=.2)
  show kate neutral_hands_down at Transform(xalign=.75)
  with Dissolve(.5)
  maxine neutral "There you are, [mc]. You're late for the meeting."
  "Are you fucking kidding me?"
  kate laughing "He always was a bit slow."
  mc "Seriously?"
  maxine angry "Now, I'm a very busy lady and I would appreciate it if you would stop wasting everyone's time."
  kate smile "You heard her."
  mc "..."
  mc "Fine. I'm ready."
  kate blush "Okay, so here's the deal, Max."
  kate blush "I have a massive story for you."
  maxine confident_hands_down "I'm listening."
  kate embarrassed "I don't know if you've heard, but there's a perverted flasher running around the school."
  maxine skeptical "Hmm... I can't say I have."
  maxine skeptical "What about you, [mc]?"
  show maxine skeptical at move_to("left")
  show kate embarrassed at move_to("right")
  menu(side="middle"):
    extend ""
    "\"Uh, yeah... the flasher,\nof course! Who hasn't?\"":
      show maxine skeptical at move_to(.2)
      show kate embarrassed at move_to(.75)
      $kate.lust+=1
      mc "Uh, yeah... the flasher, of course! Who hasn't?"
      maxine annoyed "I'm highly skeptical of this. Very little escapes my watchful eye."
      mc "Seriously, it's a real thing. I've gotten flashed like three times already.{space=-25}"
      kate thinking "I wouldn't make this up, okay?"
      maxine skeptical "How come no one has reported this person yet?"
      mc "Because it's a woman. And the dudes like it."
      kate thinking "She's also wearing a mask, so you can't identify her."
    "\"I haven't heard of them\neither. Sounds made up.\"":
      show maxine skeptical at move_to(.2)
      show kate embarrassed at move_to(.75)
      mc "I haven't heard of them either. Sounds made up."
      $kate.lust-=1
      kate eyeroll "You're just never in the gym or sports wing."
      mc "Is it the [nurse]?"
      kate skeptical "No, you idiot."
      mc "Well, who is it, then?"
      kate annoyed "If I'd known you'd be such a nuisance, I would've left you by the door.{space=-30}"
      maxine skeptical "Since the person hasn't been caught or reported, I assume that they{space=-10}\nkeep their identity secret in some way?"
      kate excited "Yes, she's wearing a mask."
  maxine sad "You understand that I'm going to need proof, right?"
  kate excited "That won't be an issue. We'll get you a good picture for the\nfront page."
  maxine thinking "If it's a bigger story than the soap goblins, sure. I'll be the\njudge of that."
  kate skeptical "Of course it's bigger than some dumb soap goblins!"
  maxine thinking "Do you think it's bigger than the soap goblin story, [mc]?"
  kate eyeroll "Why are you asking him...?"
  show maxine thinking at move_to("left")
  show kate eyeroll at move_to("right")
  menu(side="middle"):
    extend ""
    "\"What the hell is a soap goblin?\"":
      show maxine thinking at move_to(.2)
      show kate eyeroll at move_to(.75)
      mc "What the hell is a soap goblin?"
      show kate thinking
      $maxine.lust-=1
      maxine concerned "Are you telling me you haven't seen them?"
      mc "Can't say I have."
      maxine concerned "Oh, dear."
      maxine excited "Luckily, I have a big exposé lined up."
      kate embarrassed "The soap goblins will have to wait. I need that front-page coverage."
      maxine concerned "This is a public matter, I'm afraid I can't jeopardize the school."
      kate angry "Listen, you weirdo. You'll run my story or I'm having your little newspaper thrown out."
      "Uh-oh. [kate] is getting pissed."
      maxine laughing "I knew this day would come!"
      "That's... not the response I was expecting..."
      kate skeptical "What day?"
      maxine laughing "The day my integrity as a journalist would be tested."
      maxine laughing "You can try to take away my voice, but the truth will always prevail."
      kate eyeroll "[mc], get her to cooperate."
      mc "What makes you think she'll listen to me?"
      kate eyeroll "What use are you to me if you can't?"
      mc "..."
      mc "[maxine], would you run the story if it's bigger than the glowing spider eggs?"
      maxine smile "Naturally. If it's front-page material, I would be a lousy editor to not treat it as such."
      mc "There you go, [kate]. That's the best deal you'll get."
      kate skeptical "Fine. We're done here, then."
      kate skeptical "[mc], meet me in the gym."
    "\"Oh, yeah. Definitely bigger than that.\"":
      show maxine thinking at move_to(.2)
      show kate eyeroll at move_to(.75)
      $kate.lust+=1
      mc "Oh, yeah. Definitely bigger than that."
      mc "It's probably even bigger than the glowing spider eggs story."
      maxine excited "Whoa! Okay, now I'm interested!"
      kate neutral "So, do we have a deal?"
      maxine blush "If the story is bigger, the story is bigger."
      kate confident "Good. We're done here, then."
      kate confident "[mc], meet me in the gym."
  $maxine["at_none_now"] = False
  hide maxine
  hide kate
  with Dissolve(.5)
  $quest.kate_stepping.advance("gym")
  return

label quest_kate_stepping_gym:
  show casey neutral flip at Transform(xalign=.055) behind kate
  show kate neutral at Transform(xalign=.375)
  show stacy neutral at Transform(xalign=.675) behind kate
  show lacey neutral at Transform(xalign=.95) behind stacy
  with Dissolve(.5)
  casey neutral flip "What is he doing here?"
  kate confident "He's going to help us bring [isabelle] to her knees. Aren't you, [mc]?"
  show casey confident flip
  show stacy confident
  lacey confident "Well, are you?"
  mc "I guess so..."
  stacy confident "You guess?"
  casey confident flip "He looks nervous."
  kate laughing "Are you nervous?"
  "How can I not be freaking nervous when four hot cheerleaders are sizing me up?!"
  mc "I, uh, I don't know..."
  show casey confident_mocking flip
  show stacy confident_mocking
  lacey confident_mocking "He's so nervous!"
  kate smile "He's a bit of a loser, but that's why [isabelle] trusts him."
  stacy confident_mocking "Losers always come in clumps."
  lacey confident_mocking "Ew!"
  kate neutral "Okay, here's the plan. And listen closely, because I'll only tell you once."
  show casey neutral flip
  show stacy neutral
  show lacey neutral
  with dissolve2
  kate neutral "On Independence Day, you're going to put the sleeping pills into the ice tea, and then have that stuck-up bitch drink it."
  kate neutral "Once she's knocked out, you're going to call me, and we'll come to pick her up."
  kate neutral "While everyone's busy, we're going to have a little party with [isabelle]{space=-15}\nin the gym."
  lacey neutral "We're inviting her to a party?"
  stacy neutral "No, silly. It's a figure of speech."
  if not mc.check_phone_contact("kate"):
    mc "How am I supposed to call you?"
    kate skeptical "Even though it's disgusting, I guess I'll have to give you my number...{space=-25}"
    kate skeptical "Here you go."
    $mc.add_phone_contact("kate")
    mc "..."
  "I don't really know how I feel about drugging [isabelle] and handing her over to [kate]. That seems very illegal."
  mc "How are you going to avoid ending up in jail?"
  kate excited "Trust me. I have it all planned out."
  show stacy confident
  show lacey confident
  casey confident flip "[kate] never loses."
  lacey confident "Queen!"
  mc "And what about me? Am I going to be safe?"
  kate eyeroll "So. Many. Questions."
  kate eyeroll "There's no way she would suspect us working together."
  stacy confident "Use your head, dumbo!"
  "There's no use arguing with them..."
  mc "All right, fine."
  kate excited "Good boy."
  kate excited "Did you bring me what I asked for?"
  mc "Here you go."
  $mc.remove_item("handcuffs")
  $mc.remove_item("dog_collar")
  kate excited "Okay, girls, go get everything in order. I can't wait to get my hands on that English ass."
  window hide
  show casey confident flip:
    pause 0.5
    easeout 0.75 xpos 0.0 xanchor 1.0
  show kate excited:
    pause 0.75
    ease 0.5 xpos 0.5 xanchor 0.5
  show stacy confident behind casey:
    easeout 1.5 xpos 0.0 xanchor 1.0
  show lacey confident behind stacy:
    pause 0.25
    easeout 1.25 xpos 0.0 xanchor 1.0
  pause 1.0
  show kate skeptical with Dissolve(.5)
  window auto
  kate skeptical "Don't mess this up, [mc]."
  mc "Wait! I want to make sure I get something out of this."
  kate annoyed "Why do you always have to be so selfish?"
  mc "I'm not going to take all these risks for nothing."
  kate skeptical "Well, what do you want, then?"
  show kate skeptical at move_to(.2)
  menu(side="right"):
    extend ""
    "\"I want to be left alone\nfor the rest of the year.\"":
      show kate skeptical at move_to(.5)
      mc "I want to be left alone for the rest of the year."
      kate laughing "If you stay out of my way, I promise I won't make your life miserable.{space=-10}"
      "That's probably the best deal I'm going to get..."
      mc "Deal."
      kate neutral_hands_down "Okay, get it done."
      window hide
      show kate neutral_hands_down at disappear_to_left
    "?mc.lust>=6@[mc.lust]/6|{image=stats lust}|\"I want to fuck [isabelle] too.\"":
      show kate skeptical at move_to(.5)
      mc "I want to fuck [isabelle] too."
      kate embarrassed "Why would you want that?"
      mc "I just do."
      kate thinking "Fine, but don't blame me if you get an STD."
      kate thinking "Now, get going."
      window hide
      show kate thinking at disappear_to_left
      $quest.kate_stepping["fuck_isabelle_too"] = True
    "?kate.lust>=8@[kate.lust]/8|{image= kate contact_icon}|{image= stats lust_3}|\"I want you as\nmy mistress.\"":
      show kate skeptical at move_to(.5)
      mc "I want you as my mistress."
      kate laughing "Of course you do, you little weirdo."
      mc "I'm serious!"
      kate smile "You are?"
      mc "Yes, ma'am."
      kate neutral "What makes you think you have what it takes?"
      kate neutral "I will bust your balls. I will step on you and make you squeal."
      kate neutral "Limits and safewords don't exist in my world."
      mc "Please, ma'am."
      kate confident "Well, if you're sure, I will consider it... but it's not going to happen overnight."
      mc "Thank you, ma'am."
      kate confident "Now, get going."
      window hide
      show kate confident at disappear_to_left
      $quest.kate_stepping["mistress"] = True
  pause 1.0
  $quest.kate_stepping.advance("independence_day")
  window auto
  return

label quest_kate_stepping_isabelle(item):
  if item == "spiked_ice_tea":
    if game.location == "school_english_class":
      show isabelle concerned with Dissolve(.5)
      isabelle concerned "Hey, are you okay?"
      mc "Uh, yeah. Why?"
      isabelle neutral "You look a little skittish, that's all."
      "Crap. [isabelle] is too nice."
      "Doing this to her is going to be tough..."
      isabelle neutral "So, what are you up to?"
      mc "Not much. I was just wondering..."
      show isabelle neutral at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Are you thirsty?\"":
          show isabelle neutral at move_to(.5)
          mc "Are you thirsty?"
          isabelle concerned "Not particularly, why?"
          mc "I, err, I got this bottle of ice tea for you."
          $mc.remove_item("spiked_ice_tea")
          isabelle blush "That's so nice of you! Cheers!"
          isabelle sad "Wait, is this the bottle I gave you?"
          show isabelle sad at move_to(.2)
          menu(side="right"):
            extend ""
            "\"Yeah, I actually don't like ice tea...\"":
              show isabelle sad at move_to(.5)
              mc "Yeah, I actually don't like ice tea..."
              isabelle thinking "Did you open it?"
              "Crap. I really didn't think this through."
              mc "Sort of? I took a sip to try it..."
              $isabelle.love-=1
              isabelle cringe "That's a bit weird, innit?"
              mc "Sorry, I just didn't want it to go to waste... and I know it's your favorite."
              isabelle laughing "Fair enough. Fine."
              isabelle confident "I have some studying to do now. Cheers, mate!"
              hide isabelle with dissolve2
              "Ugh, she called me mate again..."
              "I should probably just leave..."
            "\"No, this is an entirely different bottle!\"":
              show isabelle sad at move_to(.5)
              mc "No, this is an entirely different bottle!"
              isabelle eyeroll "Yeah? Why is it open, then?"
              mc "I, um... I bought one for me as well... but ended up opening yours... by accident?"
              isabelle laughing "Oh, okay. Cheers!"
              hide isabelle with dissolve2
              "I'm not sure how believable that was..."
              "Better leave before she starts to suspect something."
        "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|\"Could you help me\nfind a good book?\"":
          show isabelle neutral at move_to(.5)
          mc "Could you help me find a good book?"
          mc "I haven't been able to find one that engages me as a reader."
          isabelle sad "Oh, no! That's tragic!"
          mc "Quite."
          isabelle confident "Literature is the food of the mind."
          isabelle confident "I'm sure I can find something that fits you!"
          mc "My starving mind is in your hands."
          isabelle laughing "Okay! What sorts of things do you like?"
          show isabelle laughing at move_to(.2)
          menu(side="right"):
            extend ""
            "\"Swords and magic.\"":
              show isabelle laughing at move_to(.5)
              $mc.strength+=1
              mc "Swords and magic."
              isabelle confident "Who doesn't like a good fantasy novel every now and then?"
              isabelle confident "Have you read \"Loss of the Rings?\""
              mc "I don't think so..."
              isabelle blush "It's about a jeweler who forges rings for everyone, but loses his own ring."
              isabelle blush "It follows his struggles as he's trying to recover his most precious belonging."
              isabelle sad "But it's stolen again and again, and always kept just outside his reach."
            "\"People falling in love.\"":
              show isabelle laughing at move_to(.5)
              $mc.love+=1
              mc "People falling in love."
              isabelle blush "Romance is a genre that will never die!"
              isabelle blush "Have you read \"Hide Your Prejudice?\""
              mc "Yeah, I think I have, actually."
              isabelle confident "Oh, okay! What about \"The Hootbook?\""
              isabelle confident "It's about an old owl couple on the roof of a hospital, having a hoot at the expense of dementia patients."
            "\"Quests for deeper meaning.\"":
              show isabelle laughing at move_to(.5)
              $mc.intellect+=1
              mc "Quests for deeper meaning."
              isabelle confident "Oh! Have you read \"Thus Stroked Zarahustlah?\""
              isabelle confident "It's about a bunch of random things and animals musing about being hustled by the great Zarahustlah."
            "\"Sex.\"":
              show isabelle laughing at move_to(.5)
              $mc.lust+=1
              mc "Sex."
              isabelle blush "I suppose there are worse topics to read about!"
              isabelle blush "Have you heard about \"50 Shades of Hay?\""
              isabelle blush "It's about a wealthy farmer, searching for the softest and most bashful bale of hay to step on and make his own."
            "\"What do {i}you{/} like?\"":
              show isabelle laughing at move_to(.5)
              $mc.charisma+=1
              mc "What do {i}you{/} like?"
              isabelle blush "Oh, one of my favorite books is \"To Mock a Killingbird.\""
              isabelle blush "It's about standing up for what is right, even when it may kill you."
              isabelle blush "Thanks for asking!"
          mc "You know what? That sounds exactly like something I'd enjoy."
          isabelle laughing "Great! I'm glad!"
          mc "Thanks for the recommendation. I'm going to find it at once."
          mc "Please take this small token of appreciation."
          $mc.remove_item("spiked_ice_tea")
          isabelle blush "Aw, you shouldn't have!"
          mc "Honestly, I've been looking for the right book for ages and I think this is the one."
          isabelle blush "Okay! Well, I hope it doesn't disappoint!"
          mc "See you later! Thanks again!"
          hide isabelle with dissolve2
          "All right, that went better than expected..."
          "I should probably leave before she starts to suspect something."
      scene black
      show black onlayer screens zorder 100
      with Dissolve(.07)
      $isabelle["at_none_today"] = True
      $game.location = "school_first_hall_west"
      $quest.kate_stepping.advance("phone_call")
      $mc["focus"] = "kate_stepping"
      $renpy.pause(0.07)
      $renpy.get_registered_image("location").reset_scene()
      scene location
      hide black onlayer screens
      with Dissolve(.5)
    else:
      "This is too public of a place for [isabelle] to fall asleep in."
      "I'll have to wait for her to go to the English classroom. People fall asleep there all the time."
  else:
    "The only way my [item.title_lower] is putting [isabelle] to sleep is if I whack her in the head with it."
    $quest.kate_stepping.failed_item("spiked_drink",item)
  return

label quest_kate_stepping_phone_call:
  "Okay, [isabelle] is about to be knocked out. Better call [kate]."
  return

label quest_kate_stepping_phone_call_exit:
  "I need to call [kate] before [isabelle] wakes up..."
  return

label quest_kate_stepping_phone_call_english:
  "[isabelle] is fast asleep in there. I can hear her snoring."
  return

label quest_kate_stepping_phone_call_kate:
  $set_dialog_mode("phone_call_plus_textbox","kate")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","kate")
  if quest.kate_stepping["hung_up"]:
    kate "What do you want?"
    mc "I've put [isabelle] to sleep\nin the English classroom..."
    kate "Oh, good. We'll come\nand pick her up."
    mc "Did you hang up\non me before?"
    kate "Yeah, why?"
    mc "But why would—"
  else:
    kate "Who is this?"
    mc "It's [mc]..."
  play sound "end_call"
  $set_dialog_mode("")
  pause 0.5
  window auto
  if quest.kate_stepping["hung_up"]:
    "..."
    "Well, I guess the betrayal is complete."
    "[kate] said they're going to have a party with her in the gym after school. Maybe I should stop by?"
    $quest.kate_stepping.advance("party")
    $mc["focus"] = ""
  else:
    "What the hell?! She hung up on me!"
    "..."
    "Maybe it was a mistake? Let's try again..."
    $quest.kate_stepping["hung_up"] = True
  return

label quest_kate_stepping_party:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location != "school_gym":
    "It's almost party time. I better get to the gym before the [guard] throws me out."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    call goto_school_gym
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
  $kate["outfit_stamp"] = kate.outfit
  $kate.outfit = {"bra": "kate_cheerleader_bra", "necklace": None, "panties": "kate_cheerleader_panties", "pants": "kate_cheerleader_skirt", "shirt": "kate_cheerleader_top"}
  show kate skeptical at appear_from_right
  window auto
  kate skeptical "What do you think you're doing here?"
  if quest.kate_stepping["fuck_isabelle_too"]:
    mc "You said I could fuck [isabelle] too..."
    kate excited "I guess I did say that."
    kate excited "You'll have to wait until I'm done with her, though."
    mc "That's fine by me."
    kate confident_mask "You'll have to wear this, then."
  else:
    mc "I came to watch..."
    kate excited "Yeah? And what if I say no?"
    mc "Please? I helped you get her. I deserve to watch!"
    kate eyeroll "Fine, whatever."
    kate neutral_mask "You'll have to wear this, then."
  mc "Why?"
  kate confident_mask "Plausible deniability."
  mc "Okay, I guess."
  show kate confident with dissolve2
  mc "What about you?"
  if quest.kate_stepping["fuck_isabelle_too"]:
    kate neutral "I have a mask of my own, of course."
  else:
    kate confident "I have a mask of my own, of course."
  mc "Won't [isabelle] just recognize your voice?"
  kate laughing "Your concern is adorable."
  kate smile "She will indeed recognize my voice. I want her to know who put her in her place."
  mc "So, what's the point of the mask?"
  kate neutral "I just told you — plausible deniability."
  mc "There's no way that's going to hold up."
  kate confident "It will when there's a party at my house, and every single guest will testify that I'm there."
  "[kate] is an evil genius..."
  "Well, I guess it helps to have the entire school wrapped around her finger too."
  mc "And where is [isabelle]?"
  kate blush "Oh, here she comes!"
  kate blush "She's not very fast, though. Take a seat."
  window hide
  show black onlayer screens zorder 100
  show kate paddling_arrival_isabelle_defiant_ballgag
  with Dissolve(.5)
  pause 1.0
  $isabelle["at_none_today"] = False
  hide black onlayer screens with Dissolve(.5)
  window auto
  isabelle "Mmmph!"
  "Holy crap."
  "[kate] wasn't joking about putting [isabelle] in her place..."
  "She's completely helpless, her arms and legs folded and bound."
  "And even if she somehow managed to get free, they're four on one."
  "Still, there's fire in her eyes."
  "If only I knew where [maxine]'s secret popcorn dispenser is..."
  "Oh, well. I'm just going to sit back and enjoy the show."
  "I wonder if they'll be able to break her?"
  "It's going to be a battle of wills for sure, but [isabelle] seems to be at a slight disadvantage."
  kate paddling_arrival_isabelle_defiant_ballgag "She's still being defiant, I see."
  isabelle "Mmmph!"
  casey "Quiet, bitch."
  lacey "Yeah! No one told you to speak!"
  "God, they're so strict with her already..."
  "But maybe that's what she needs?"
  kate paddling_arrival_isabelle_defiant_ballgag "Listen carefully, bitch. You lost, and I'm going to enjoy breaking you.{space=-5}"
  kate paddling_arrival_isabelle_defiant_ballgag "I have been planning for this moment for weeks."
  kate paddling_arrival_isabelle_defiant_ballgag "You're exactly the type of girl I want as a pet."
  kate paddling_arrival_isabelle_defiant_ballgag "Dumb and ignorant subs are boring... but you're the opposite."
  kate paddling_arrival_isabelle_defiant_ballgag "You're fiery, headstrong, independent, and intelligent."
  kate paddling_arrival_isabelle_defiant_ballgag "You'll truly understand and appreciate every facet of humiliation that I have to offer."
  kate paddling_arrival_isabelle_defiant_ballgag "Now, what do you say when you get a compliment?"
  isabelle "..."
  stacy "Thank her, bitch!"
  lacey "Yeah! Thank her!"
  casey "We told you what happens if you're rude."
  "It's astonishing to watch the power struggle."
  "Despite being tied up, [isabelle] is proving a tough opponent."
  "She's just not submissive yet. Quite the opposite."
  "It's like watching the superbowl of domination, with the two top teams facing off."
  isabelle "Maammk-uuuh."
  kate paddling_arrival_isabelle_defiant_ballgag "Was that a \"thank you,\" or a \"fuck you?\""
  menu(side="middle"):
    extend ""
    "\"That was definitely a 'fuck you,'\nno doubt about it.\"":
      mc "That was definitely a \"fuck you,\" no doubt about it."
      "[isabelle] is a lot tougher than they think."
      $kate.lust+=2
      kate paddling_arrival_isabelle_defiant_ballgag "I think you're right."
      kate paddling_arrival_isabelle_defiant_ballgag "She always was a rude cunt."
      lacey "Yeah, always the worst!"
      stacy "Total hoebag."
      casey "She doesn't know what respect is."
      kate paddling_arrival_isabelle_defiant_ballgag "We'll teach her, then."
      lacey "Queen!"
    "\"That sounded like a 'thank you' to me.\"":
      mc "That sounded like a \"thank you\" to me."
      "They're being really harsh with [isabelle]. I feel a bit like Judas now..."
      kate paddling_arrival_isabelle_defiant_ballgag "Let's find out, then, shall we?"
      kate paddling_arrival_isabelle_defiant_ballgag "Would you unmuzzle the bitch for a bit, [lacey]?"
      lacey "With pleasure."
      window hide
      show kate paddling_lacey_ballgag_isabelle_defiant with Dissolve(.5)
      window auto
      kate paddling_lacey_ballgag_isabelle_defiant "Now, [isabelle], repeat what you just said."
      isabelle "Fuck you, you psychotic slag!"
      show kate paddling_lacey_hair_pull_stacy_foot_casey_leash_pull
      isabelle "Hnnngh!" with vpunch
      lacey "Bad bitch!"
      casey "Try again!"
      "Wow, they're really showing her who's in charge..."
      isabelle "T-thank... y-you..."
      stacy "Thank you, what?"
      lacey "Yeah! Show some respect!"
      isabelle "Thank you... ma'am..."
      kate paddling_lacey_hair_pull_stacy_foot_casey_leash_pull "What do you think, ladies? Was that sincere enough?"
  casey "I think she needs to be taught a real lesson."
  kate "You're absolutely right."
  kate "I normally warm my pets up with a flogger, but I'm not feeling particularly charitable today."
  kate "[lacey], give her ten hard strokes with the paddle."
  if renpy.showing("kate paddling_arrival_isabelle_defiant_ballgag"):
    kate "And take the ballgag out, I want to hear her scream."
  else:
    kate "Leave the ballgag out, I want to hear her scream."
  lacey "Yes!"
  window hide
  if renpy.showing("kate paddling_arrival_isabelle_defiant_ballgag"):
    show kate paddling_lacey_ballgag_isabelle_defiant with Dissolve(.5)
    pause 0.25
  show kate paddling_lacey_paddle1_isabelle_defiant with Dissolve(.5)
  window auto
  stacy "Should we make her count?"
  kate paddling_lacey_paddle1_isabelle_defiant "Nah, I just want to hear her beg."
  kate paddling_lacey_paddle1_isabelle_defiant "Are you going to beg for me, [isabelle]?"
  isabelle "..."
  kate paddling_lacey_paddle1_isabelle_defiant "How about you try right now?"
  kate paddling_lacey_paddle1_isabelle_defiant "Beg for twenty strokes instead of ten."
  casey "Beg, bitch!"
  isabelle defiant "Forget it."
  kate paddling_lacey_paddle1_isabelle_defiant "Fine. Give her ten, [lacey]."
  kate paddling_lacey_paddle1_isabelle_defiant "Then, we'll see if she's ready to beg for more."
  lacey "Yes!"
  "God, [lacey] looks hot wielding that paddle."
  "I'm glad it's not my ass..."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_pain with Dissolve(.175)
  window auto
  isabelle "Mmmmph."
  casey "That was one."
  show kate paddling_lacey_paddle1_red_isabelle_defiant with dissolve2
  stacy "Nine more until we let you beg again."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_pain with Dissolve(.175)
  pause 0.25
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_pain with Dissolve(.175)
  window auto
  isabelle "Hnnngh!"
  "Wow, [isabelle] is tough."
  "Her face is turning as red as her ass, but she's holding back the screams."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_more_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_more_pain with Dissolve(.175)
  pause 0.0
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_more_pain with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_more_pain with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_more_pain with Dissolve(.175)
  window auto
  isabelle "Fffff! Oh, g-god!"
  kate paddling_lacey_paddle1_red_isabelle_more_pain "Looks like you got her good with those."
  lacey "Haha! I love it!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Owwwwwww! Ahhh, noo!"
  kate paddling_lacey_paddle1_red_isabelle_crying "All you have to do is beg for more, [isabelle]."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Nooo! Stop!"
  casey "That's not begging."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaah! Please stop!"
  kate paddling_lacey_paddle1_red_isabelle_crying "Wrong. Beg for more."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaaaah! Oh, my god!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Noooo! Aaaah!"
  kate paddling_lacey_paddle1_red_isabelle_crying "That was ten."
  kate paddling_lacey_paddle1_red_isabelle_crying "Are you ready to beg for ten more, or should we keep going?"
  show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
  isabelle "..."
  isabelle "Please... more..."
  kate paddling_lacey_paddle1_red_isabelle_pleading "You can do better than that."
  kate paddling_lacey_paddle1_red_isabelle_pleading "Be respectful."
  isabelle "P-please, ma'am... can I have ten more strokes?"
  kate paddling_lacey_paddle1_red_isabelle_pleading "You absolutely can. Go ahead, [lacey]."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  pause 0.15
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Ayeeeeee!"
  isabelle "God! Oh, god!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  pause 0.0
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaah!"
  "They're really being ruthless..."
  "I guess that's what it takes to tame someone like [isabelle]."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  pause 0.2
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Ooooh my goood! Pleeease, nooo mooore!"
  casey "Take it, bitch!"
  lacey "Yeah, take it!"
  stacy "You asked for this, remember?"
  isabelle "Nooo!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  kate paddling_lacey_paddle1_red_isabelle_crying "Yes, you did."
  kate paddling_lacey_paddle1_red_isabelle_crying "Now ask for five more, or we start over."
  show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
  isabelle "C-can I... please have... five more... ma'am?"
  kate paddling_lacey_paddle1_red_isabelle_pleading "God, you're pathetic."
  kate paddling_lacey_paddle1_red_isabelle_pleading "You thought you could stand up to me. Now look at you begging to be punished."
  show kate paddling_lacey_paddle1_red_isabelle_blush with Dissolve(.5)
  isabelle "Screw you... ma'am..."
  kate paddling_lacey_paddle1_red_isabelle_blush "Haha! God, I love that you're still defiant."
  kate paddling_lacey_paddle1_red_isabelle_blush "But I'll let that slide because you were polite in your rudeness."
  kate paddling_lacey_paddle1_red_isabelle_blush "I'm really looking forward to this year with you at my feet, [isabelle]."
  kate paddling_lacey_paddle1_red_isabelle_blush "Maybe the girls and I will bring you along to my skiing cabin during Christmas break."
  lacey "We could walk you naked in the snow!"
  kate paddling_lacey_paddle1_red_isabelle_blush "Doesn't that sound like fun?"
  kate paddling_lacey_paddle1_red_isabelle_blush "You could be our pet bitch for two weeks straight!"
  "Fuck, I suddenly have an urge to learn how to ski..."
  kate paddling_lacey_paddle1_red_isabelle_blush "Now, ask for five more like a good bitch."
  isabelle "P-please, ma'am... Can I have... five more...?"
  kate paddling_lacey_paddle1_red_isabelle_blush "Haha! You sure can."
  kate paddling_lacey_paddle1_red_isabelle_blush "But since you're so eager, perhaps we should give you more than five? What do you think, [mc]?"
  menu(side="middle"):
    extend ""
    "\"She does seem eager\nfor more punishment.\"":
      $mc.lust+=2
      mc "She does seem eager for more punishment."
      kate paddling_lacey_paddle1_red_isabelle_blush "She does, doesn't she?"
      kate paddling_lacey_paddle1_red_isabelle_blush "I've always wanted a pain slut. Bitch, ask for ten more."
      "[isabelle] looks conflicted."
      "It's an unfair game, but it's not like she has a choice."
      isabelle "Um... can I please... have ten more instead, ma'am?"
      kate paddling_lacey_paddle1_red_isabelle_blush "Yes, you may. [lacey], go ahead."
      lacey "With pleasure..."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.25
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.15
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Aaaaaaaaaah! Ooooh!"
      "The paddle hits [isabelle] square across her buttcheeks, sending shockwaves through her ass."
      "For such a dimwit, [lacey] sure is wielding that paddle like a pro."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.2
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Ayeeeeeeh! Nooo!"
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Ooooh gooood!"
    "\"You're the expert, [kate].\"":
      mc "You're the expert, [kate]."
      $kate.lust+=2
      kate paddling_lacey_paddle1_red_isabelle_blush "That's true."
      kate paddling_lacey_paddle1_red_isabelle_blush "I would suggest another forty, but then she probably wouldn't feel anything after."
      kate paddling_lacey_paddle1_red_isabelle_blush "And I'm far from done with her..."
      kate paddling_lacey_paddle1_red_isabelle_blush "[lacey], go ahead and give [isabelle] the last five."
      lacey "Yes!"
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      pause 0.0
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Oooooooh, god!"
      kate paddling_lacey_paddle1_red_isabelle_crying "I like hearing you scream, but I think we can improve it."
      kate paddling_lacey_paddle1_red_isabelle_crying "For the next one, I want you to scream my name. Am I making myself clear?"
      show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
      isabelle "Y-yes... ma'am..."
      kate paddling_lacey_paddle1_red_isabelle_pleading "Good bitch."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Oooooooh, [kate]!"
      kate paddling_lacey_paddle1_red_isabelle_crying "Haha, yes! That has a better ring to it."
      window hide
      play sound "paddle_hit"
      $renpy.transition(vpunch, layer="master")
      show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
      show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
      window auto
      isabelle "Ayeeeeeeeeh!"
  kate paddling_lacey_paddle1_red_isabelle_crying "All right, you only have one more, [isabelle]."
  kate paddling_lacey_paddle1_red_isabelle_crying "After [lacey] has delivered it, you're going to kiss the paddle and thank her. Understood?"
  show kate paddling_lacey_paddle1_red_isabelle_pleading with Dissolve(.5)
  isabelle "Yes... ma'am..."
  kate paddling_lacey_paddle1_red_isabelle_pleading "Go ahead, [lacey]."
  lacey "This one is going to hurt."
  lacey "Are you ready?"
  isabelle "Oh, god..."
  casey "She asked you a question, bitch."
  isabelle "I'm... I'm ready, ma'am..."
  lacey "We'll see about that."
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate paddling_lacey_paddle2_isabelle_crying with Dissolve(.175)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle3_isabelle_crying with Dissolve(.05)
  show kate paddling_lacey_paddle1_red_isabelle_crying with Dissolve(.175)
  window auto
  isabelle "Aaaaaaaaaoooh!"
  lacey "There we go!"
  casey "Now kiss the paddle."
  stacy "Show the proper respect."
  window hide
  show kate paddling_lacey_paddle_kiss with Dissolve(.5)
  window auto
  isabelle "Thank you... Miss [lacey]..."
  isabelle "Thank you... Miss [casey]..."
  isabelle "Thank you... Miss [stacy]..."
  isabelle "Thank you... Miss [kate]..."
  kate paddling_lacey_paddle_kiss "Good girl."
  "It's weird seeing [isabelle] so humbled and docile..."
  "But I can't deny the boner in my pants."
  "It's not every day you get to see [kate] dominate someone who isn't submissive in nature."
  kate paddling_lacey_paddle_kiss "I've been fantasizing about this for a while now."
  kate paddling_lacey_paddle_kiss "You, down there, helpless and submissive."
  kate paddling_lacey_paddle_kiss "Accepting your punishment."
  kate paddling_lacey_paddle_kiss "And now that you understand your place a bit more, I think it's\ntime to... drive it home."
  kate paddling_lacey_paddle_kiss "[lacey], can you gag her again?"
  lacey "Of course!"
  window hide
  show kate paddling_lacey_ballgag_red_isabelle_blush with Dissolve(.5)
  pause 0.25
  show kate paddling_arrival_red_isabelle_blush_ballgag with Dissolve(.5)
  window auto
  kate paddling_arrival_red_isabelle_blush_ballgag "Be right back."
  window hide
  show kate paddling_kateless_isabelle_blush_ballgag with Dissolve(.5)
  window auto
  "What else does [kate] have in store?"
  "Knowing her, this was just the warm up."
  "And [isabelle]'s ass does look warm. It's practically glowing."
  "[lacey] sure did a number on her. She won't be sitting for the\nnext week."
  window hide
  show black onlayer screens zorder 100
  show kate strap_on_anticipation
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  kate strap_on_anticipation "A while back, I promised the girls that I would take your ass, [isabelle].{space=-25}"
  kate strap_on_anticipation "And, well... I never go back on my promises."
  kate strap_on_anticipation "I don't know if you've taken it in the ass before, but I guess there's a first time for everything."
  isabelle "Mmmph!"
  kate strap_on_anticipation "Don't be so upset. We did warm your ass up a bit with the paddle."
  kate strap_on_anticipation "I won't ask if you're ready; I can assure you that you're not."
  casey "Give it to her!"
  stacy "Fuck her ass!"
  lacey "Queen!"
  kate strap_on_anticipation "Here it comes, bitch."
  window hide
  show kate strap_on_poke with vpunch
  window auto
  isabelle "Mmmmph! Mmmph!"
  kate strap_on_poke "I know it hurts. That's the point."
  isabelle "Mmmph!"
  kate strap_on_poke "Good girl. Take it. Scream for me, and take it."
  window hide
  show kate strap_on_penetration1_isabelle_pain with vpunch
  window auto
  isabelle "Mmmph!"
  kate strap_on_penetration1_isabelle_pain "I'm going to get every last inch of it inside you."
  kate strap_on_penetration1_isabelle_pain "And then, I'm going to fuck you until you come."
  kate strap_on_penetration1_isabelle_pain "And then, if you ask nicely, I'm going to let you clean it with your mouth."
  window hide
  show kate strap_on_penetration2_isabelle_pain with vpunch
  window auto
  isabelle "Mmmmph!"
  kate strap_on_penetration2_isabelle_pain "Let me in, bitch."
  lacey "Make room for the queen!"
  "Fitting that whole thing inside her ass seems impossible, but [kate] seems very determined."
  "And unfortunately for [isabelle], [kate] usually gets what she wants..."
  window hide
  show kate strap_on_penetration3_isabelle_pain with vpunch
  window auto
  isabelle "Mmmmph! Mmmmph!"
  kate strap_on_penetration3_isabelle_pain "Hold her down!"
  show kate strap_on_penetration3_isabelle_pain_casey_leash_pull
  casey "Be still and take it, bitch." with vpunch
  stacy "Do as you're told!"
  lacey "Give in!"
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "[isabelle], I'm going to give you two more inches now."
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "Are you going to be a good girl for me?"
  isabelle "Uhmph-umph..."
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "No? You want to be my bad girl, then?"
  kate strap_on_penetration3_isabelle_pain_casey_leash_pull "I'm okay with that."
  window hide
  show kate strap_on_penetration4_isabelle_pain with vpunch
  window auto
  isabelle "Mmmph! Mmmph! Mmmph!"
  kate strap_on_penetration4_isabelle_pain "Almost there..."
  kate strap_on_penetration4_isabelle_pain "Considering the size of the stick you usually keep up your ass, I'm surprised you're even protesting."
  stacy "Come on, [kate], put her in her place!"
  casey "Make her take it all!"
  kate strap_on_penetration4_isabelle_pain "Oh, I'm planning on it. I just want to enjoy the moment."
  kate strap_on_penetration4_isabelle_pain "I find it so funny that she walked up to me that day, trying to\nstart shit."
  kate strap_on_penetration4_isabelle_pain "And now she's here, with my strap-on up her ass."
  kate strap_on_penetration4_isabelle_pain "That's a powerful statement if I ever saw one!"
  kate strap_on_penetration4_isabelle_pain "Anyway, are you ready for the last inches, bitch?"
  show kate strap_on_penetration4_isabelle_head_down with Dissolve(.5)
  isabelle "Mmmmph..."
  kate strap_on_penetration4_isabelle_head_down "Hey, don't look so crestfallen. That's no fun."
  kate strap_on_penetration4_isabelle_head_down "I want you to submit as I fuck your ass."
  kate strap_on_penetration4_isabelle_head_down "Here it comes..."
  window hide
  show kate strap_on_penetration5_isabelle_crying with vpunch
  window auto
  isabelle "Mmmmmmmmmmmmmmmmph!"
  kate strap_on_penetration5_isabelle_crying "There it is. Perfection."
  "I can't believe it. [kate] somehow got the entire length of that monster inside [isabelle]'s ass."
  isabelle "Mmmph! Mmmph!"
  kate strap_on_entire_length "God, it must be all the way up in your colon."
  kate strap_on_entire_length "Tell me, how does it feel to be so thoroughly owned?"
  kate strap_on_entire_length "You're sweating and crying from the pain, but how does it feel on a psychological level?"
  isabelle "..."
  kate strap_on_entire_length "Yeah, I don't imagine you have much to say now."
  kate strap_on_entire_length "You saw me as a rival. As your equal, in a way."
  kate strap_on_entire_length "But we're not equals, are we?"
  show kate strap_on_penetration5_isabelle_head_down with Dissolve(.5)
  isabelle "..."
  casey "No way, you're not!"
  stacy "One is a bitch, the other is a queen."
  lacey "Yeah! You're the bitch, [isabelle]! [kate]'s the queen!"
  kate strap_on_penetration5_isabelle_head_down "I'm going to master you, [isabelle]. And you will submit to me."
  kate strap_on_penetration5_isabelle_head_down "Tell me, are you ready to submit?"
  isabelle "Mmmmph..."
  kate strap_on_penetration5_isabelle_head_down "I think you're ready, but I'm still going to make sure..."
  window hide
  show kate strap_on_penetration4_isabelle_head_down with Dissolve(.2)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  pause 0.2
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.2)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.15)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.2)
  pause 0.15
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  window auto
  isabelle "Mmmmmph!"
  "Each thrust from that cock sends a shock wave of pain and pleasure through [isabelle]'s core."
  "No one has probably ever been as fucked as she is now..."
  window hide
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.1)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.1)
  pause 0.1
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  pause 0.2
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  window auto
  isabelle "Mmmmmph!"
  kate strap_on_penetration1_isabelle_pain "Do you know what I like the most about this?"
  isabelle "..."
  kate strap_on_penetration1_isabelle_pain "I'm fucking you, but there's zero pleasure for you."
  kate strap_on_penetration1_isabelle_pain "And there's almost zero physical pleasure for me."
  kate strap_on_penetration1_isabelle_pain "It's all about me asserting my dominance over your ass, and you submitting to it."
  kate strap_on_penetration1_isabelle_pain "It's kind of twisted, but the real pleasure is in the power exchange."
  window hide
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration5_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.075)
  show kate strap_on_penetration1_isabelle_pain with Dissolve(.075)
  pause 0.15
  show kate strap_on_penetration2_isabelle_pain with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_pain with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_pain with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.1
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  window auto
  isabelle "Mmmmmph!"
  "Wow, [kate] is really giving it to her..."
  "She moves her hips back, adding more power to each thrust."
  "Whenever I watch porn, girls always struggle to thrust their hips right while fucking someone with a strap-on."
  "But [kate] is an expert at it. She's as skilled at fucking people as\nany guy."
  "Which I guess shouldn't come as a surprise..."
  "Fucking people, in every sense of the word, is one of her favorite hobbies."
  window hide
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.15
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.2
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.1
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  pause 0.1
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  window auto
  isabelle "Mmmmmph!"
  kate strap_on_penetration1_isabelle_crying "Come on, [isabelle], don't hold back."
  kate strap_on_penetration1_isabelle_crying "I want to hear you scream."
  isabelle "..."
  casey "Scream for us, bitch!"
  lacey "Yeah, scream!"
  window hide
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration1_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration2_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with hpunch
  pause 0.2
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with hpunch
  pause 0.2
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.1)
  show kate strap_on_penetration3_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration4_isabelle_crying with Dissolve(.05)
  show kate strap_on_penetration5_isabelle_crying with hpunch
  window auto
  isabelle "Mmmmmph!"
  kate strap_on_penetration5_isabelle_crying "Maybe it's time to remove the gag..."
  kate strap_on_penetration5_isabelle_crying "Will you be on your best behavior if I take it out?"
  isabelle "..."
  stacy "Bitch, answer her!"
  isabelle "Mmmphmmm."
  casey "Sounds like she'll be a good girl for us now."
  kate strap_on_penetration5_isabelle_crying "It does, indeed."
  kate strap_on_penetration5_isabelle_crying "Now, when I unmuzzle you, I want to hear gratitude."
  window hide
  show kate strap_on_entire_length with Dissolve(.5)
  pause 0.25
  show kate strap_on_entire_length_no_ballgag with Dissolve(.5)
  pause 0.25
  window auto
  kate strap_on_entire_length_no_ballgag "What do you say, [isabelle]?"
  isabelle "Thank... you... ma'am..."
  kate strap_on_entire_length_no_ballgag "For what?"
  isabelle "For... setting me straight..."
  kate strap_on_entire_length_no_ballgag "It was about time someone did, right?"
  isabelle "Y-yes... ma'am..."
  kate strap_on_entire_length_no_ballgag "I already like your attitude better."
  kate strap_on_entire_length_no_ballgag "You're going to be obedient now, aren't you?"
  isabelle "Yes, ma'am..."
  kate strap_on_entire_length_no_ballgag "Good girl."
  if quest.kate_stepping["fuck_isabelle_too"]:
    kate strap_on_entire_length_no_ballgag "Unfortunately for you, I've promised [mc] something."
    isabelle "..."
    kate strap_on_entire_length_no_ballgag "I know. It's a bit gross, but he did play a part in your capture."
    kate strap_on_entire_length_no_ballgag "And just like Judas, he'll get paid in full."
    kate strap_on_entire_length_no_ballgag "[mc], come on over here."
    "Finally, the moment I've been waiting for!"
  window hide
  show black onlayer screens zorder 100
  show kate spitroast_strap_on_isabelle_nervous
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  if quest.kate_stepping["fuck_isabelle_too"]:
    kate spitroast_strap_on_isabelle_nervous "You can fuck her in the ass as well."
    kate spitroast_strap_on_isabelle_nervous "I haven't decided yet if I'll let her take dicks in her pussy again."
    kate spitroast_strap_on_isabelle_nervous "Maybe she'll be an ass-only girl from now on."
    isabelle "..."
    "As long as I get to fuck her, I'm happy."
    "Buggers can't be choosers, as the gay men say in England."
    kate spitroast_strap_on_isabelle_nervous "Just one more thing before we start."
    kate spitroast_strap_on_isabelle_nervous "I usually don't make first-time pets do this..."
    kate spitroast_strap_on_isabelle_nervous "But I think you deserve to taste your own ass, [isabelle]."
    kate spitroast_strap_on_isabelle_nervous "Wouldn't you agree?"
    isabelle "Y-yes... ma'am..."
    kate spitroast_strap_on_isabelle_nervous "Good girl. Open your mouth."
    window hide
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.5)
    pause 0.1
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.2
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.15
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    window auto
    kate spitroast_blowjob1_isabelle_eyes_closed "Now, isn't that the taste of utter defeat?"
    "Fuck, that's twisted, but also stupid hot..."
    mc "Can I fuck her now?"
    kate spitroast_blowjob1_isabelle_eyes_closed "Yeah, go ahead. Claim your prize."
    window hide
    show kate spitroast_blowjob1_anticipation_squeeze1_isabelle_eyes_closed with Dissolve(.5)
    window auto
    "Heat radiates off [isabelle]'s ass from the harsh spanking."
    "Her sphincter, winking and red from [kate]'s strap-on, beckons me."
    "It is indeed a prize, one that most can only dream of."
    window hide
    show kate spitroast_blowjob1_penetration1_squeeze1_isabelle_looking_up with Dissolve(.5)
    window auto
    "At first, a shock tightens her back as the tip of my dick meets her abused asshole."
    show kate spitroast_blowjob1_penetration2_squeeze2_isabelle_looking_up with dissolve2
    "A gargle escapes her mouth as the strap-on domesticates her throat."
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with dissolve2
    "The girls hold her down, making sure she knows her place."
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with dissolve2
    "And my brain completely floods with lust chemicals as I break through her backdoor."
    window hide
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    window auto
    "Her ass tightens around my cock, trying to push it out..."
    "But that only makes me want to go deeper."
    "It's a losing battle for [isabelle], and slowly but surely her defiance melts away."
    "She expected to get fucked by [kate], but she didn't think I would also have my way with her."
    "Nevertheless, she has little choice but to accept me into the hottest, most forbidden place in her body."
    window hide
    show kate spitroast_blowjob1_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.5)
    window auto
    "My hips slam into her bruised ass, forcing the strap-on deeper down her throat."
    "[kate] isn't letting up, and neither am I."
    "She pushes [isabelle] back onto my cock, and I push her forward onto hers."
    "After a while, our paces start to match."
    "We push into [isabelle] at the same time."
    window hide
    show kate spitroast_blowjob1_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.5)
    window auto
    "Her teary eyes bulge, she squirms, but the cheerleaders keep her\nin place."
    lacey "Stay, bitch!"
    casey "Yeah! Stay!"
    window hide
    show kate spitroast_penetration1_squeeze1_strap_on_stacy_backfoot_isabelle_crying with hpunch
    window auto
    "[kate] just smiles smugly, and smacks [isabelle] in the face with\nthe strap-on."
    kate spitroast_penetration1_squeeze1_strap_on_stacy_backfoot_isabelle_crying "Are you enjoying yourself?"
    "It's hard to say who the question is pointed at. Maybe it's me, maybe it's [isabelle]."
    window hide
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.5)
    show kate spitroast_blowjob1_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration4_squeeze4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration3_squeeze3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration2_squeeze2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_penetration1_squeeze1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    window auto
    "Her juices drip in strings down onto the gym floor, creating a small puddle."
    "Her pussy is flexing, and so is her ass. She's close to orgasming,\nI can feel it..."
    "And me..."
    "All at once the pleasure rips through me, this is going to be my best—{space=-40}"
    window hide None
    play sound "falling_thud"
    show black onlayer screens zorder 100
    show kate spitroast_strap_on_isabelle_shock
    with vpunch
    pause 0.5
    show black onlayer screens zorder 4
    $set_dialog_mode("default_no_bg")
    mc "Ouch..."
    "Just as orgasm hits me, [lacey] pushes me back."
    "My dick erupts, but it's less than a whimper of the titanic eruption I was about to release..."
    "Less satisfying."
    show black onlayer screens zorder 100
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    $set_dialog_mode("")
  else:
    kate spitroast_strap_on_isabelle_nervous "Now, I usually don't make first-time pets do this..."
    kate spitroast_strap_on_isabelle_nervous "But I think you deserve to taste your own ass, [isabelle]."
    kate spitroast_strap_on_isabelle_nervous "Wouldn't you agree?"
    isabelle "Y-yes... ma'am..."
    kate spitroast_strap_on_isabelle_nervous "Good girl. Open your mouth."
    window hide
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.5)
    pause 0.1
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.2
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    pause 0.15
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob4_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob3_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob2_isabelle_eyes_closed with Dissolve(.175)
    show kate spitroast_blowjob1_isabelle_eyes_closed with Dissolve(.2)
    window auto
    kate spitroast_blowjob1_isabelle_eyes_closed "Isn't that the taste of utter defeat?"
    "Fuck, that's twisted, but also stupid hot..."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_eyes_closed with Dissolve(.5)
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_eyes_closed with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with hpunch
    pause 0.25
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    window auto
    isabelle "Mmmmmm!"
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "You like that vibrator?"
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "I thought it would be fitting for you to orgasm while tasting your ass.{space=-20}"
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "So, go on, enjoy it."
    kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up "It might be your last orgasm in the foreseeable future."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.125)
    window auto
    "Despite her total degradation, [isabelle]'s body is responding to the touch of the vibrator."
    "Eyes downcast in shame, she cleans her ass off of [kate]'s strap-on."
    "Her cheeks burn, while waves of pleasure start to roll through her."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_looking_up with Dissolve(.1)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.5)
    window auto
    "It must be so humiliating to get dominated like this, and then be forced to enjoy it..."
    "She bobs her head up and down, grimacing from the taste, but also from the building lust."
    "Her abdomen spasms hard, her pussy leaks down her thighs."
    "It's only a matter of time before she tips over the edge."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_pleading with Dissolve(.075)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.5)
    window auto
    "Her hips move on instinct, trying to hump the vibrator..."
    "Her breath quickens..."
    "It builds and it builds..."
    window hide
    show kate spitroast_blowjob1_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    pause 0.0
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator4_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator3_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator2_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob4_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob3_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob2_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    show kate spitroast_blowjob1_lacey_vibrator1_stacy_backfoot_isabelle_orgasm with Dissolve(.05)
    window auto
    "Then suddenly, her eyes open wide, her muscles go stiff..."
    "A final gasp fills her lungs..."
    window hide
    show kate spitroast_blowjob1_isabelle_orgasm with Dissolve(.5)
    window auto
    "Before [lacey] calmly turns off the vibrator."
    show kate spitroast_strap_on_isabelle_shock with Dissolve(.5)
  isabelle "Noooooooooo!"
  if quest.kate_stepping["fuck_isabelle_too"]:
    "[isabelle] isn't as lucky."
    "The stimulus of my dick is lost at the very brink of her orgasm."
    "She struggles against her bonds in utter frustration. The cheerleaders{space=-45}\njust laugh."
  else:
    "[isabelle] struggles against her bonds in utter frustration. The cheerleaders just laugh."
  "The cruelty has reached a new high..."
  kate spitroast_strap_on_isabelle_crying "Silly girl! Did you really think we'd let you orgasm?"
  "Before she can fully gather herself from the denied orgasm, [stacy] pulls out a new item."
  window hide
  show kate spitroast_strap_on_stacy_buttplug_isabelle_pleading with Dissolve(.5)
  window auto
  stacy "The final piece of your costume."
  "[isabelle] just bows her head in submission. There's no fight left\nin her."
  "It's almost like she presents her ass for [stacy]."
  window hide
  show kate spitroast_strap_on_isabelle_pleading_buttplug with Dissolve(.5)
  window auto
  "As the plug slides home, and her face turns even redder, the girls all cheer."
  kate spitroast_strap_on_isabelle_pleading_buttplug "Moving forward, will you bark for me like a dog, or purr for me like a kitten?"
  kate spitroast_strap_on_isabelle_pleading_buttplug "Those are your options, and I'm excited to see what kind of pet you'll be for me."
  show kate spitroast_strap_on_isabelle_head_down_buttplug with Dissolve(.5)
  isabelle "..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "I know it's a hard choice. You're both catty and a total bitch."
  isabelle "..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "Make your choice."
  isabelle "A... dog..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "What was that?"
  isabelle "A dog... ma'am..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "Oh, a good choice!"
  kate spitroast_strap_on_isabelle_head_down_buttplug "Let's hear it, then. Bark for me."
  isabelle "..."
  kate spitroast_strap_on_isabelle_head_down_buttplug "Bark for me, or we'll bring out the paddle again."
  show kate spitroast_strap_on_isabelle_shock_buttplug with Dissolve(.5)
  isabelle "Woof!"
  kate spitroast_strap_on_isabelle_shock_buttplug "Again."
  show kate spitroast_strap_on_isabelle_crying_buttplug with Dissolve(.5)
  isabelle "Woof! Woof!"
  kate spitroast_strap_on_isabelle_crying_buttplug "Heel, whore."
  window hide
  show kate avatar events paddling background
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 0.5
  show black onlayer screens zorder 4
  window auto show None
  $set_dialog_mode("default_no_bg")
  "As [kate] leads [isabelle] out of the gym for a victory lap around the school, I rest my back against the bleachers."
  "It's been an interesting first quarter of the school year..."
  "Already, things are going better than last time."
  "My problem was always meekness and inhibition."
  "Perhaps, taking what you want is how you get it."
  "If you're not a pussy, you get pussy."
  "Maybe that's the law of the universe. Pussy comes to those in need."
  "It's hard to say if [kate] will tame [isabelle] completely..."
  "Maybe it's exactly what [isabelle] was looking for, and it's a lesbian match made in heaven?"
  "Perhaps [isabelle] will get her revenge?"
  "All I know is that I'm going to enjoy the autumn holidays."
  "This takedown has given me all sorts of fapping material."
  "Maybe I'll even call [kate] and see if I can borrow [isabelle]..."
  "She probably won't let me, but perhaps I can come watch?"
  window hide None
  $set_dialog_mode("")
  window auto
  $unlock_replay("isabelle_punishment")
# $quest.kate_stepping.finish(silent=True)
  $quest.kate_stepping.advance("fly_to_the_moon")
  $kate.outfit = kate["outfit_stamp"]
  show black onlayer screens zorder 100
  #
# show expression Text("To Be Continued...", font="fonts/Fresca-Regular.ttf", color="#fefefe", size=69) onlayer screens zorder 100 as to_be_continued:
#   xalign 0.5 yalign 0.5 alpha 0.0
#   pause 0.5
#   easein 3.0 alpha 1.0
# return
  #
  pause 1.5
  show black as kate
  hide black onlayer screens
  hide to_be_continued onlayer screens
  jump quest_isabelle_dethroning_panik_outside
  #
