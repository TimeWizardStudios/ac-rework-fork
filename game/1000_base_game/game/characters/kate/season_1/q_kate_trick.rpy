label quest_kate_trick_start:
  show kate eyeroll at Transform(xalign=.25)
  show isabelle angry_left at Transform(xalign=.75)
  with Dissolve(.5)
  "Uh-oh. Did I just step into the Colosseum?"
  "Looks like a fight is about to break out..."
  kate eyeroll "...and you think that concerns me in the slightest, why?"
  isabelle angry_left "It does concern you! You're the class president!"
  kate smile_right "Last time I checked, getting a good grade is every student's own responsibility."
  isabelle cringe_left "You're literally stopping students who aren't on a sports team or the cheerleading squad from getting an education!"
  kate laughing "That's just straight up false. The gym is open to everyone... between twelve and one."
  isabelle afraid_left "That's lunch hour!"
  kate eyeroll "Ever heard of bringing your own sandwich? I knew Londoners were cheap, but this is a bit much."
  isabelle annoyed_left "Ever heard of not being a complete twat?"
  kate annoyed_right "Bitch."
  mc "..."
  show isabelle annoyed
  kate excited "Oh, look. Perfect timing."
  isabelle annoyed "[mc], [jo] is the principal, right? Can you help put an end to this nonsense?"
  show kate excited at move_to("left")
  show isabelle annoyed at move_to("right")
  menu(side="middle"):
    extend ""
    "\"As the class president and the head cheerleader, it's [kate]'s job to ensure\nthat the sports teams get their\nallotted time at the gym.\"":
      show kate excited at move_to(.25)
      show isabelle annoyed at move_to(.75)
      $mc.intellect+=1
      mc "As the class president and the head cheerleader, it's [kate]'s job to ensure that the sports teams get their allotted time at the gym."
      isabelle annoyed "That's not how it works."
      kate excited "After the changes to the curriculum, that's exactly how it works."
      isabelle annoyed "[mc], can you talk to the principal? She'll listen to you."
      "[isabelle] underestimates how little [jo] cares about my opinion on school matters..."
      show kate excited at move_to("left")
      show isabelle annoyed at move_to("right")
      menu(side="middle"):
        extend ""
        "\"I can talk to her, but I doubt she'll listen.\"":
          show kate excited at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "I can talk to her, but I doubt she'll listen."
          isabelle sad "Please, try."
          kate eyeroll "It's already been done."
          mc "I'll talk to her, but no promises."
          $isabelle.lust+=1
          isabelle blush "Thanks. That's all I ask."
        "\"If the schedule has already been cleared, there's nothing I can do about it.\"":
          show kate excited at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "If the schedule has already been cleared, there's nothing I can do about it."
          $isabelle.love-=1
          isabelle angry "You could at least talk to her!"
          kate annoyed_right "You heard him. Now drop it before I put you in timeout."
          isabelle sad "I'm going to talk to the principal myself, then."
          kate annoyed_right "Good luck, sweetheart."
      window hide
      if renpy.showing("isabelle blush"):
        show isabelle blush at disappear_to_right
      elif renpy.showing("isabelle sad"):
        show isabelle sad at disappear_to_right
      pause 0.25
      if renpy.showing("kate eyeroll"):
        show kate eyeroll at move_to(.5,1.0)
      elif renpy.showing("kate annoyed_right"):
        show kate annoyed_right at move_to(.5,1.0)
      pause 1.0
      show kate neutral with Dissolve(.5)
      window auto
      jump quest_kate_trick_start_two
    "\"[kate] is right, and you need to start respecting her authority.\"":
      show kate excited at move_to(.25)
      show isabelle annoyed at move_to(.75)
      mc "[kate] is right, and you need to start respecting her authority."
      isabelle cringe "What about people with gym class as their focus? They'll have a hard time getting their education."
      mc "If [jo] signed off on it, then that's how it'll work from now on."
      isabelle afraid "That's outrageous!"
      kate annoyed_right "No, what's outrageous is you trying to talk back to me."
      kate annoyed_right "From now on, you'll only speak when spoken to. Understood?"
      $isabelle.lust-=1
      isabelle annoyed_left "Don't talk to me like that. It's absolutely disgusting."
      kate excited "She still needs some training, it seems."
      $kate.lust+=1
      kate excited "Speaking of which, how's everything coming together?"
      "Oh, shit. Talking so openly about the plan to humiliate [isabelle]. It's hard not to admire [kate]'s confidence."
      show kate excited at move_to("left")
      show isabelle annoyed_left at move_to("right")
      menu(side="middle"):
        extend ""
        "\"I'm still working on it.\"":
          show kate excited at move_to(.25)
          show isabelle annoyed_left at move_to(.75)
          mc "I'm still working on it."
          kate neutral "Well, you better step it up. I'm growing impatient."
          isabelle thinking "Maybe you should stop being so lazy and do your own dirty work?"
          kate laughing "Are you trying to fault me for being inclusive as well?"
          isabelle cringe "[mc], you know you don't have to do what she says, right?"
          show kate laughing at move_to("left")
          show isabelle cringe at move_to("right")
          menu(side="middle"):
            extend ""
            "\"I like picking the winning side.\"":
              show kate laughing at move_to(.25)
              show isabelle cringe at move_to(.75)
              mc "I like picking the winning side."
              isabelle afraid "There's no winning or losing! It's about being a decent person!"
              kate smile_right "Words of a true loser."
              show kate smile_right at move_to("left")
              show isabelle afraid at move_to("right")
              menu(side="middle"):
                extend ""
                "\"I have to agree with [kate] here.\"":
                  show kate smile_right at move_to(.25)
                  show isabelle afraid at move_to(.75)
                  $mc.intellect+=1
                  mc "I have to agree with [kate] here."
                  $isabelle.love-=3
                  isabelle thinking "About what?"
                  $kate.lust+=1
                  kate laughing "That you're a loser, of course."
                  kate laughing "The gym schedule has already been settled."
                  isabelle cringe "What did you mean, [mc]?"
                  show kate laughing at move_to("left")
                  show isabelle cringe at move_to("right")
                  menu(side="middle"):
                    extend ""
                    "\"That you're a loser.\"":
                      show kate laughing at move_to(.25)
                      show isabelle cringe at move_to(.75)
                      mc "That you're a loser."
                      isabelle afraid "!!!"
                      kate smile_right "See?"
                      $isabelle.love-=5
                      $isabelle.lust-=5
                      isabelle sad "Good to know. I guess I don't need to bother with you anymore, then.{space=-30}"
                      kate smile_right "Bye, bitch."
                      window hide
                      show isabelle sad at disappear_to_right
                      pause 0.25
                      show kate smile_right at move_to(.5,1.0)
                      pause 1.0
                      window auto
                      "Hmm... [isabelle] seemed really hurt by that. She's probably not used to being called a loser."
                      "It probably hurt a bit extra coming from one."
                    "\"If the schedule has already been cleared, there's nothing I can do about it.\"":
                      show kate laughing at move_to(.25)
                      show isabelle cringe at move_to(.75)
                      mc "If the schedule has already been cleared, there's nothing I can do about it."
                      $isabelle.love-=1
                      isabelle angry "You could at least talk to the principal!"
                      kate confident_right "You heard him. Now drop it before I put you in timeout."
                      isabelle sad "I'm going to talk to her myself, then."
                      kate confident_right "Good luck, sweetheart."
                      window hide
                      show isabelle sad at disappear_to_right
                      pause 0.25
                      show kate confident_right at move_to(.5,1.0)
                      pause 1.0
                      show kate neutral with Dissolve(.5)
                      window auto
                      jump quest_kate_trick_start_two
                    "?kate.lust>=6@[kate.lust]/6|{image=kate contact_icon}|{image=stats lust_3}|\"That you need to bow before the queen.\"":
                      show kate laughing at move_to(.25)
                      show isabelle cringe at move_to(.75)
                      mc "That you need to bow before the queen."
                      $isabelle.lust-=2
                      isabelle cringe "I bow to no one. Least of all bullies."
                      kate confident_right "Oh, but you will."
                      $kate.lust+=2
                      kate confident_right "I'll put you on your knees. You'll crawl at the end of my leash."
                      isabelle afraid "!!!"
                      kate laughing_hands_up "Maybe you like humiliation?"
                      kate laughing_hands_up "I'm starting to think these little outbursts are your way of signaling your need to be topped."
                      isabelle thinking "You've got a vivid fantasy."
                      kate smile_right "You have no idea. The things I'm going to do to you..."
                      isabelle cringe "You're sickening. I'm leaving."
                      kate laughing "Bye, pet!"
                      window hide
                      show isabelle cringe at disappear_to_right
                      pause 0.25
                      show kate laughing at move_to(.5,1.0)
                      pause 1.0
                      window auto
                      "That was a crushing defeat for [isabelle]. [kate] was born to rule."
                      kate flirty "Did you like that, hm?"
                      show kate flirty at move_to(.75)
                      menu(side="left"):
                        extend ""
                        "\"That was so hot! She's\ntotally begging for it!\"":
                          show kate flirty at move_to(.5)
                          mc "That was so hot! She's totally begging for it!"
                          kate laughing "That's what I'm thinking too."
                          kate confident "Probably one of her darkest fantasies to be put in her place."
                          kate confident "Rest assured that it will happen."
                        "\"I'm jealous, ma'am.\"":
                          show kate flirty at move_to(.5)
                          mc "I'm jealous, ma'am."
                          kate laughing "That's adorable. There'll be plenty of rewards for you once we've taken her down."
                          mc "I'm looking forward to it..."
                          kate confident "I bet you are, pet. I bet you are."
                      $achievement.the_queen_bee_decree.unlock()
                      jump quest_kate_trick_start_three
                "\"You have a point, [isabelle].\"":
                  show kate smile_right at move_to(.25)
                  show isabelle afraid at move_to(.75)
                  mc "You have a point, [isabelle]."
                  mc "I'll talk to [jo]. Maybe there's room for a compromise."
                  show kate neutral
                  $isabelle.love+=1
                  isabelle flirty "Thanks, [mc]! I knew you were a good person."
                  window hide
                  show isabelle flirty at disappear_to_right
                  pause 0.25
                  show kate neutral at move_to(.5,1.0)
                  pause 1.0
                  window auto
                  $kate.lust-=1
                  jump quest_kate_trick_start_two
            "\"That ship has sailed...\"":
              show kate laughing at move_to(.25)
              show isabelle cringe at move_to(.75)
              mc "That ship has sailed..."
              isabelle concerned "There's always time to change your ways."
              kate eyeroll "Spare me."
              isabelle neutral "Trust me. High school is just one more year."
              isabelle neutral "Fear is temporary, being decent is lasting."
              mc "Hmm..."
              kate annoyed_right "Are you done?"
              isabelle neutral "Ignore her. You'll help a lot of people to get the education they deserve."
              show kate annoyed_right at move_to("left")
              show isabelle neutral at move_to("right")
              menu(side="middle"):
                extend ""
                "\"I'll think about it.\"":
                  show kate annoyed_right at move_to(.25)
                  show isabelle neutral at move_to(.75)
                  mc "I'll think about it."
                  $isabelle.love+=1
                  isabelle excited "Great!"
                  kate eyeroll "You are too much, seriously."
                  isabelle excited "This is the right thing to do."
                  window hide
                  show isabelle excited at disappear_to_right
                  pause 0.25
                  show kate eyeroll at move_to(.5,1.0)
                  pause 1.0
                  show kate neutral with Dissolve(.5)
                  window auto
                  $kate.lust-=1
                  jump quest_kate_trick_start_two
                "\"I honestly don't care.\"":
                  show kate annoyed_right at move_to(.25)
                  show isabelle neutral at move_to(.75)
                  mc "I honestly don't care."
                  $isabelle.love-=1
                  $isabelle.lust-=2
                  isabelle sad "That's so selfish..."
                  isabelle sad "You have the chance to help others. I'm disappointed."
                  kate eyeroll "What a drama queen."
                  isabelle annoyed_left "Screw you!"
                  window hide
                  show isabelle annoyed_left at disappear_to_right
                  pause 0.25
                  show kate eyeroll at move_to(.5,1.0)
                  pause 1.0
                  show kate excited with Dissolve(.5)
                  window auto
        "\"Only a couple things missing, ma'am.\"":
          show kate excited at move_to(.25)
          show isabelle annoyed_left at move_to(.75)
          $kate.lust+=1
          mc "Only a couple things missing, ma'am."
          isabelle eyeroll "Seriously? That's the dumbest thing I've heard."
          mc "It makes her happy."
          isabelle annoyed "She's your classmate!"
          mc "Yeah, but..."
          kate eyeroll "Why do you always put your nose where it doesn't belong?"
          isabelle annoyed "What about your own dignity, [mc]?"
          show kate eyeroll at move_to("left")
          show isabelle annoyed at move_to("right")
          menu(side="middle"):
            extend ""
            "\"Nothing wrong with being polite.\"":
              show kate eyeroll at move_to(.25)
              show isabelle annoyed at move_to(.75)
              $mc.intellect+=1
              mc "Nothing wrong with being polite."
              kate annoyed_right "Exactly, bitch. Maybe you should try it some time?"
              isabelle angry_left "How dare you? I've been nothing but polite!"
              kate laughing "You have a funny understanding of that word."
              mc "You did just call me dumb a moment ago..."
              isabelle afraid "I didn't call {i}you{/} dumb, just the thing you said!"
              isabelle afraid "I didn't mean to insult you, [mc]."
              kate smile_right "Well, everything you've said so far has been really dumb, [isabelle]."
              isabelle thinking "No one asked for your opinion."
              isabelle cringe "You know what? I'm done here."
              window hide
              show isabelle cringe at disappear_to_right
              pause 0.25
              show kate smile_right at move_to(.5,1.0)
              pause 1.0
              show kate excited with Dissolve(.5)
              window auto
              kate excited "But we're far from done, aren't we?"
            "Let [kate] answer for you":
              show kate eyeroll at move_to(.25)
              show isabelle annoyed at move_to(.75)
              mc "..."
              kate neutral_right "Dignity is a privilege, not a right."
              kate neutral_right "It's a gift from those with power to those without."
              isabelle skeptical_left "That's a load of rubbish."
              $kate.lust+=2
              kate confident_right "We can try it out if you like? Let me have my way with you for an hour,{space=-45}\nand we'll see how much dignity you have left."
              isabelle concerned_left "..."
              kate confident_right "Fair warning. I'd be extra cruel to you."
              kate confident_right "But maybe you're into that? Is that why you keep coming at me?"
              isabelle cringe_left "..."
              kate laughing "She's imagining it now!"
              isabelle cringe_left "You're a despicable human being..."
              window hide
              show isabelle cringe_left at disappear_to_right
              pause 0.25
              show kate laughing at move_to(.5,1.0)
              pause 1.0
              show kate excited with Dissolve(.5)
              window auto
        "?mc.lust>=4@[mc.lust]/4|{image=stats lust}|\"We'll have her leashed and\npotty trained in no time.\"":
          show kate excited at move_to(.25)
          show isabelle annoyed_left at move_to(.75)
          mc "We'll have her leashed and potty trained in no time."
          $isabelle.love-=2
          $isabelle.lust-=3
          isabelle afraid "!!!"
          $kate.lust+=3
          kate laughing "I can hardly wait!"
          isabelle afraid "I can't believe you just said that, [mc]! You two are the worst!"
          kate smile_right "Sweetheart, you don't know the first thing about it. It'll be like waking{space=-30}\nup to a cold shower."
          isabelle cringe_left "Your scare-tactics don't work on me."
          kate confident_right "Oh, that's not scare-tactics. That's a promise."
          kate confident_right "I'm going to put you on a leash and parade you naked around\nthe school."
          isabelle cringe_left "..."
          kate laughing_hands_up "Maybe I'll make you bark like a dog or kiss my shoes? Maybe I'll spank you for being an insolent brat?"
          kate confident_right "Either way, everyone will know you're my bitch."
          isabelle cringe_left "Dream on, psycho."
          $achievement.the_queen_bee_stinger.unlock()
          window hide
          show isabelle cringe_left at disappear_to_right
          pause 0.25
          show kate confident_right at move_to(.5,1.0)
          pause 1.0
          show kate excited with Dissolve(.5)
          window auto

label quest_kate_trick_start_one:
  kate excited "I'm so looking forward to putting her in her place."
  kate eyeroll "She's really starting to become a nuisance."
  mc "Want me to do something about it?"
  kate skeptical "Nah, just keep doing what you're doing."
  mc "Have you given the aftermath of her takedown any thoughts?"
  kate excited "The school administration loves me. I could get away with murder."
  kate excited "And if anyone gives me trouble, you'll act as my witness."
  show kate excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I won't lie on your behalf.\"":
      show kate excited at move_to(.5)
      mc "I won't lie on your behalf."
      $kate.lust-=1
      kate neutral "I'd have half the school line up to defend me. So, as with most other things, you're inconsequential."
      kate neutral "But I'd of course make a point of ruining you for stepping out of line.{space=-15}"
      mc "..."
      kate confident "Siding with me has all the benefits. If it comes to that, you'll hopefully{space=-35}\nbe smart enough to do the right thing."
    "\"Yes, ma'am!\"":
      show kate excited at move_to(.5)
      mc "Yes, ma'am!"
      $kate.lust+=1
      kate confident "Good dog."
      kate neutral "Sit."
      mc "..."
      kate laughing "I'm just messing with you!"
  jump quest_kate_trick_start_three

label quest_kate_trick_start_two:
  kate neutral "You're not going to talk to [jo] about this."
  kate neutral "Am I making myself clear?"
  show kate neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yes, ma'am.\"":
      show kate neutral at move_to(.5)
      mc "Yes, ma'am."
      $kate.lust+=1
      kate confident "Good boy."
      kate confident "Things are finally starting to look up for you. There's no need to jeopardize that."
      mc "Earning your favor makes me happy."
      kate laughing "Favor is a bit of a stretch, but I like the sentiment."
    "\"If she already signed off on this,\nyou have nothing to worry about.\"":
      show kate neutral at move_to(.5)
      mc "If she already signed off on this, you have nothing to worry about."
      $kate.lust-=1
      kate neutral "Don't tell me what to do."
      kate neutral "If I hear you've talked to [jo], I'll have your balls. That's a promise."
      show kate neutral at move_to(.25)
      menu(side="right"):
        extend ""
        "\"You can have my balls any day of the week, if you know what I'm saying...\"":
          show kate neutral at move_to(.5)
          $mc.charisma+=2
          mc "You can have my balls any day of the week, if you know what I'm saying..."
          kate laughing "That's adorable."
          $kate.lust-=1
          kate neutral_hands_down "But sass doesn't suit you."
          kate neutral_hands_down "I want complete obedience, else you're going into the dumpster."
        "\"I'm sorry, ma'am.\"":
          show kate neutral at move_to(.5)
          mc "I'm sorry, ma'am."
          kate confident "That's better."
          kate confident "I might hang them on my christmas tree if you're not careful."
          "Ugh, she sounds serious. I better behave."
    "\"I'm all for shutting up [isabelle], but\nthis schedule is going to make it\ndifficult for a lot of other people...\"":
      show kate neutral at move_to(.5)
      mc "I'm all for shutting up [isabelle], but this schedule is going to make it difficult for a lot of other people..."
      $kate.lust-=1
      kate laughing "Since when do you care about other people?"
      "She has a point. Most of the students here deserve to get screwed over for how they treated me in the past."
      "But... this is my second chance. Being better is sort of the point."
      mc "I'm trying to change my ways."
      kate confident "Good luck with that. No one ever changes."
      "Maybe this is my chance to prove her wrong..."
      show kate confident at move_to(.25)
      menu(side="right"):
        extend ""
        "Try to change the schedule\nbehind [kate]'s back":
          show kate confident at move_to(.5)
          mc "Fine, I won't do anything."
          kate neutral "Good. And don't step out of line again."
          show kate neutral at disappear_to_right
          $mc.love+=1
          "It's not much, but talking to [jo] is the right thing to do. [kate] doesn't have to know."
          $quest.kate_trick.start("jo")
          return
        "Play it safe with [kate]":
          show kate confident at move_to(.5)
          $mc.lust+=1
          mc "You're right. Those most interested in sports should have priority."
          kate confident "Good choice."

label quest_kate_trick_start_three:
  kate thinking "Anyway, after [isabelle]'s little rebellion, I've decided I want a special collar for her."
  mc "What do you have in mind?"
  kate blush "I want it to be a proper slave collar — black leather, padlock, name tag — the whole shebang!"
  mc "And how are you going to get one of those?"
  kate blush "As a matter of fact..."
  kate blush "You're going to get it for me."
  mc "What? How?"
  kate annoyed "See, there was a bit of a mishap earlier."
  kate annoyed_right "I was trying to... give one to [mrsl]..."
  mc "Give one?"
  kate excited "Oh, just like a present."
  mc "Why?"
  kate eyeroll "Because I'm such a nice person and I thought it'd look good on her."
  kate skeptical "Instead, she confiscated it. Can you believe that?"
  mc "I think I can..."
  kate skeptical "Well, you're going to return it to me."
  mc "You want me to steal from a teacher?"
  kate laughing "It's mine, remember?"
  mc "Where even is it?"
  kate neutral "I don't know. Wherever she keeps confiscated items. Maybe in a desk drawer or something?"
  mc "Great. And how am I supposed to get into it?"
  kate neutral "Just break it open when she's not in the room."
  show kate neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"With what? My hands?\"":
      show kate neutral at move_to(.5)
      mc "With what? My hands?"
      kate laughing "I suppose you're too weak for that."
      kate laughing "Good luck!"
      show kate laughing at disappear_to_right
    "\"All right, no problem. I'm\nthe muscle you need.\"":
      show kate neutral at move_to(.5)
      mc "All right, no problem. I'm the muscle you need."
      kate confident "Well, you're certainly not the brains."
      show kate confident at disappear_to_right
  $quest.kate_trick.start("break_in")
  return

label quest_kate_trick_off_limits:
  if quest.kate_trick != "report":
    "The gym is currently off limits to everyone except [kate]'s posse."
  if quest.kate_trick in ("break_in","jo"):
    "I'll have to talk to [jo] if I want it reopened for more hours than one."
  elif quest.kate_trick == "maxine":
    "There's always an exception, though. I just need to find out what the rules say."
    "And there's one person who always bends the rules..."
  elif quest.kate_trick == "report":
    "I'd say that was the kind of physical activity worthy of a good grade..."
    "Now I just gotta tell [jo] how physical I've been and get the gym reopened."
  else:
    "However, the Newfall Constitution on Animal Rights, Chapter 87, subsection D, says \"a dog (and by extension its owner) has the right to access any public training facility.\""
    "I just need to find a dog."
  return

label quest_kate_trick_break_in(item):
  if mrsl.location == "school_homeroom":
    "I'm pretty damn stealthy, but breaking into [mrsl]'s desk while she's sitting there is above my paygrade."
    $quest.kate_trick["break_in_attempt"] = True
  else:
    if item == "high_tech_lockpick":
      "I've always wondered what [mrsl] keeps in this desk. It smells like pussy-scented incense."
      "Let's see here..."
      "..."
      play sound "lock_click"
      "..."
      "Well, that was simple enough."
      $mc.add_item("candle")
      "A candle... a broken fidget spinner... a bottle with some unknown liquid...  a wallet..."
      $mc.money+=44
      "Don't mind if I do..."
      $mc.add_item("dog_collar")
      "Aha! There it is!"
      if quest.kate_trick["blackmail"]:
        "All right, time to pay the [nurse] a visit."
        $quest.kate_trick.advance("nurse")
      else:
        "All right, that's that taken care of."
        "I'd let [kate] know I managed to retrieve her collar, but I guess I'm not welcome in the gym anymore either."
        "..."
        if lindsey["romance_disabled"]:
          "Come to think of it, no gym access means I won't be able to watch cheer practice ever again..."
          "Their long legs, dancing across the gym floor... Their short skirts, flapping in the wind..."
        else:
          "Come to think of it, no gym access means I won't be seeing [lindsey] nearly as much..."
        "Hell no. I need to talk to [jo]. [kate] doesn't have to know."
        $quest.kate_trick.advance("jo",silent=True)
    else:
      "Huh? That's odd. The lock is not a fit for my [item.title_lower]."
      $quest.kate_trick.failed_item("break_in",item)
  return

label quest_kate_trick_jo:
  show jo smile with Dissolve(.5)
  jo smile "Hi there, honey!"
  mc "[jo], I need your help with something."
  jo concerned "You didn't break anything expensive, did you?"
  mc "No, nothing like that!"
  jo confident "Okay, what is it?"
  mc "Did you sign off on the new gym schedule?"
  jo neutral "I have a vague memory of something like that crossing my desk, why?{space=-45}"
  show jo neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"[kate] is trying to ruin gym class for students who aren't on a sports team.\"":
      show jo neutral at move_to(.5)
      mc "[kate] is trying to ruin gym class for students who aren't on a\nsports team."
      jo thinking "That doesn't sound like [kate]."
      mc "I swear! Did you read the new schedule before signing off on it?"
      $jo.love-=1
      jo worried "I'm sure you're mistaken. [kate] is one of our brightest students. She's very active with the sports teams."
      jo worried "She's also the head cheerleader, so I trust her when it comes to\nthe gym."
      "Ugh, this is giving me flashbacks to the time when someone stole my backpack..."
      "[jo] will never listen to this line of reasoning."
    "\"There's a problem with it.\"":
      show jo neutral at move_to(.5)
      mc "There's a problem with it."
      jo thinking "Are you sure? It had the signatures of several team captains."
      mc "Regular students only have access during lunch."
      jo worried "We're trying to prioritize our sports teams this year. It was [kate]'s idea. She thinks it'll give the school better results."
      "Crap, [kate] is too smart. I need another approach."
    "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"I've been trying to take gym classes, but the gym seems\nto be off-limits.\"":
      show jo neutral at move_to(.5)
      mc "I've been trying to take gym classes, but the gym seems to be\noff-limits."
      jo cringe "Off-limits? That doesn't sound good. "
      jo laughing "I'm glad you're finally starting to think of your health, though!"
      mc "Yeah, I really want to better myself this year, but the gym seems to be closed at all hours except during lunch."
      jo thinking "I did recently sign off on a new schedule to give our sports team better access."
      mc "Don't you think there might be too few hours left for the other students?"
      jo excited "I'll look over the new schedule and make sure there's time for everyone."
      $jo.love+=2
      jo excited "Thanks for bringing this issue to my attention."
      mc "Anytime, [jo]!"
      hide jo with dissolve2
      "Well, I won some brownie points with her, but..."
      "Knowing [jo], she's not going to change it unless I show her I'm serious about taking gym classes."
      "Time to put in work."
      $quest.kate_trick.advance("training")
      return
  jo thinking "Anything else on your mind?"
  show jo thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"How am I supposed to get\nmy gym education?\"":
      show jo thinking at move_to(.5)
      mc "How am I supposed to get my gym education?"
      jo eyeroll "Let's be honest, [mc], you've never shown any interest in gym classes before. What has changed?"
      mc "Well, I'd like to improve myself this year."
      jo sarcastic "That's great news! I'll cut you a deal, then."
      jo sarcastic "Once you've shown actual physical improvement, I'll change the schedule back."
      jo sarcastic "And if you're serious about bettering yourself, the gym will be open for everyone."
      jo flirty_hands_to_the_side "How does that sound?"
      mc "I guess that's fine."
      jo blush "Perfect! I'm glad you're finally starting to take your health seriously."
    "\"I'd like to start my own\nsports team, then.\"":
      show jo thinking at move_to(.5)
      mc "I'd like to start my own sports team, then."
      jo laughing "That's not how it works, honey!"
      jo excited "I'm starting to suspect this has something to do with a girl..."
      "Shit, she's always been too good at reading my true intentions."
      jo flirty "I'll tell you what: if you start to show actual physical improvement, I'll change the schedule back."
      jo flirty "And if you're serious about bettering yourself, the gym will be open for everyone."
      jo sarcastic_hands_to_the_side "How does that sound?"
      mc "I guess that's fine."
      jo flirty "Great! But make sure you keep it up at the gym. You need to make it a habit!"
      "Ugh, now I have to put in work..."
  hide jo with Dissolve(.5)
  $quest.kate_trick.advance("training")
  return

label quest_kate_trick_training:
  "Okay, so I just need to get in shape. How hard can it be?"
  $kate["outfit_stamp"] = kate.outfit
  $kate.outfit = {"bra": "kate_cheerleader_bra", "necklace": None, "panties": "kate_cheerleader_panties", "pants": "kate_cheerleader_skirt", "shirt": "kate_cheerleader_top"}
  show kate neutral at appear_from_left
  kate neutral "Where do you think you're going?"
  mc "Do you mind if I use the gym for a bit? I've decided to get in shape."
  kate laughing "Hahahaha!"
  kate laughing "Now, that's funny!"
  "Whatever."
  kate confident "The gym is closed. No exceptions."
  mc "Seriously?"
  kate confident "Seriously."
  show kate confident at disappear_to_left
  "There's always an exception. I just need to find out what the\nrules say."
  "And there's one person who always bends the rules..."
  $quest.kate_trick.advance("maxine")
  $kate.outfit = kate["outfit_stamp"]
  return

label quest_kate_trick_maxine:
  show maxine thinking with Dissolve(.5)
  maxine thinking "You're late."
  mc "I didn't realize there was a time to keep."
  maxine thinking "There's always a time to keep."
  mc "I'll keep that in mind. Anyway, I need your help."
  mc "I need to—"
  maxine confident "You're looking to circumvent the new gym schedule."
  mc "No, I'm... well, yes. How did you know that?"
  maxine skeptical "I was watching you."
  mc "What the fuck? Why?"
  maxine skeptical "Wrong. The question is, \"How?\" And the answer is classified."
  mc "..."
  mc "I really don't have time for this. Can you help me or not?"
  maxine excited "Yes, I can."
  mc "..."
  maxine excited "..."
  mc "..."
  maxine excited "..."
  mc "Help me, please..."
  maxine concerned "Very well. But I'm not doing it to help you."
  maxine concerned "The secret popcorn dispenser is in the gym, so I need access\nto it too."
  mc "What do you have for me?"
  maxine smile "The Newfall Constitution on Animal Rights, Chapter 87,\nsubsection D."
  maxine smile "{i}\"A dog (and by extension its owner) has the right to access any public training facility.\"{/}"
  mc "Seriously?"
  maxine angry "Gravely so. Animal rights is an important topic."
  maxine neutral "Adieu now."
  hide maxine with dissolve2
  "Well, then. I just need to find a dog."
  "I'm pretty sure [flora] works with animals at her internship. Maybe she could lend me one?"
  $blackmail = mc.owned_item(("damning_document","compromising_photo"))
  if blackmail:
    "Or maybe... I could ask someone to be my dog. Someone who's full of obedience."
    menu(side="middle"):
      extend ""
      "Ask [flora] to lend you a dog":
        pass
      "{image=items [blackmail]}|Ask the [nurse] to be your dog":
        if mc.owned_item("dog_collar"):
          "All right, time to pay the [nurse] a visit."
          $quest.kate_trick.advance("nurse")
        else:
          "If I want the [nurse] to look the part, I'm going to need to retrieve [kate]'s collar."
          "Where would [mrsl] keep confiscated items?"
          $quest.kate_trick.advance("break_in",silent=True)
        $quest.kate_trick["blackmail"] = True
        return
  $quest.kate_trick.advance("flora")
  return

label quest_kate_trick_flora:
  if quest.kate_trick["no_deal"]:
    show flora angry with Dissolve(.5)
    flora angry "Fifty bucks or bust."
    jump quest_kate_trick_flora_deal
  show flora skeptical with Dissolve(.5)
  flora skeptical "You have that look."
  mc "What look?"
  flora skeptical "The look that says you're about to ask me for something."
  "Huh... seems like I need to work on my poker face. Mum-mum-mum-mah."
  "At least this helps me cut right to the chase."
  mc "Actually..."
  flora embarrassed "I knew it! It's always take with you."
  mc "That's hardly fair. I give, too."
  flora annoyed "Give me a headache, maybe."
  mc "I am both the ailment and the cure. The fire and the rain."
  flora eyeroll "The rain on my parade..."
  mc "I'm the peanut butter to your jelly."
  flora cringe "Gross."
  flora cringe "I think I'm developing a peanut allergy."
  mc "Come on, I know my voice is actually a soothing aloe to your burning ears."
  flora annoyed "The only thing burning here is my patience going up in smoke."
  mc "Fine, fine. I do have a small favor to ask of you..."
  flora eyeroll "Of course you do. We could've saved a lot of time if you'd admitted it in the start."
  flora sarcastic "So, what do I get in return?"
  show flora sarcastic at move_to(.75)
  menu(side="left"):
    extend ""
    "\"My undying love and loyalty?\"":
      show flora sarcastic at move_to(.5)
      mc "My undying love and loyalty?"
      flora concerned "What am I supposed to do with that?"
      mc "Cherish it forever?"
      flora confident "Can't put that in the piggy bank."
      if blackmail:
        show flora confident at move_to(.25)
        menu(side="right"):
          extend ""
          "\"Ugh, fine... I'll give you twenty bucks.\"":
            pass
          "{image=items [blackmail]}|\"You know what? Nevermind.\"":
            show flora confident at move_to(.5)
            mc "You know what? Nevermind."
            mc "I have someone else who would be... more than willing to help."
            flora angry "Ugh! You just like wasting my time, don't you?"
            mc "No, I just can't get enough of your sunny disposition."
            flora angry "I {i}am{/} a ray of sunshine, thank you very much."
            mc "You're welcome very much."
            flora concerned "Whatever, dork."
            flora concerned "Good luck with whatever ridiculous thing you're up to now."
            hide flora with dissolve2
            if mc.owned_item("dog_collar"):
              "All right, time to pay the [nurse] a visit."
              $quest.kate_trick.advance("nurse")
            else:
              "Meh, who needs an actual dog when you can just blackmail a human into acting like one?"
              "..."
              "If I want the [nurse] to look the part, I'm going to need to retrieve [kate]'s collar."
              "Where would [mrsl] keep confiscated items?"
              $quest.kate_trick.advance("break_in",silent=True)
            $quest.kate_trick["blackmail"] = True
            return
      show flora confident at move_to(.5)
      mc "Ugh, fine... I'll give you twenty bucks."
      flora excited "Make it fifty."
      mc "Fifty? That's kinda steep."
      flora angry "You're the one asking for help..."
      mc "You drive a hard bargain."
      flora angry "Beggars can't be choosers."
      label quest_kate_trick_flora_deal:
      show flora angry at move_to(.75)
      menu(side="left"):
        extend ""
        "?mc.money>=50@[mc.money]/50|{image=ui hud icon_money}|Pay up":
          show flora angry at move_to(.5)
          $mc.money-=50
          mc "Fine, here's fifty bucks."
          flora excited "Great! So, what's up?"
        "Come back later":
          show flora angry at move_to(.5)
          mc "I'll be back."
          hide flora with Dissolve(.5)
          $quest.kate_trick["no_deal"] = True
          return
    "\"You don't even know what I want yet...\"":
      show flora sarcastic at move_to(.5)
      mc "You don't even know what I want yet..."
      flora concerned "Is it creepy or disgusting?"
      mc "What do you think?"
      flora concerned "Knowing you? Both."
      mc "Ugh! No, it's not!"
      flora neutral "What do you want, then?"
    "{image=items [blackmail]}|\"It's always take with you, isn't it?\"" if blackmail:
      show flora sarcastic at move_to(.5)
      mc "It's always take with you, isn't it?"
      flora concerned "Very funny."
      flora concerned "'Scuse me for expecting some quid pro quo!"
      mc "You want to get paid in British money?"
      flora angry "Ugh! Just go away, [mc]."
      mc "Fine, whatever. I have someone else who would be... more than willing to help."
      hide flora with dissolve2
      if mc.owned_item("dog_collar"):
        "All right, time to pay the [nurse] a visit."
        $quest.kate_trick.advance("nurse")
      else:
        "Because who needs an actual dog when you can just blackmail a human into acting like one?"
        "..."
        "If I want the [nurse] to look the part, I'm going to need to retrieve [kate]'s collar."
        "Where would [mrsl] keep confiscated items?"
        $quest.kate_trick.advance("break_in",silent=True)
      $quest.kate_trick["blackmail"] = True
      return
  mc "Can I borrow a dog from your internship?"
  flora confused "For what?"
  mc "It's complicated, but it's for a good cause."
  flora annoyed "Since when do you care about good causes? Sounds sus."
  mc "It is!"
  flora confused "Sus?"
  mc "No! A good cause!"
  flora cringe "For who, you? You're not going to use an innocent dog for something perverted, are you?"
  mc "..."
  mc "I have standards, [flora]."
  flora cringe "I know, that's exactly why I ask."
  mc "Come on now!"
  flora eyeroll "Fine, whatever. I believe you."
  flora annoyed "I'm actually supposed to walk [hati] later today. Just stop by in\nthe park."
  mc "And you trust me with this dog?"
  flora sarcastic "I trust him with you."
  hide flora with Dissolve(.5)
  $quest.kate_trick.advance("park")
  return

label quest_kate_trick_park:
  show flora neutral_leash_retracted with Dissolve(.5)
  mc "Hey! So, where's the dog?"
  flora excited_leash_retracted "[hati]! Here, boy!"
  window hide
  show hati neutral at appear_from_left(.25)
  show flora excited_leash_retracted at move_to(.75,1.0)
  pause 1.0
  window auto
  "Holy fuck."
  show flora excited_leash with dissolve2
  flora excited_leash "There's a good boy!"
  mc "Err... couldn't you have picked a smaller one?"
  flora angry_leash "Wow! Seriously?"
  flora angry_leash "Here I am trying to help you, and what do I get? A choosing beggar!"
  mc "Sorry, it's just... it's so big!"
  flora laughing_leash "That's what she said!"
  mc "I can't believe you're cracking jokes around this beast..."
  hati neutral "Growl!" with vpunch
  mc "Aaah!"
  flora worried_leash "He doesn't like being called that."
  mc "I-I think I just peed a little..."
  flora angry_leash "Ew, gross!"
  flora angry_leash "[hati], kill."
  hati "Roar!" with vpunch
  mc "Nooo!"
  flora laughing_leash "Just kidding! He's not trained, don't worry."
  mc "I... I don't know if that's reassuring..."
  flora blush_leash "It'll be fine. [hati] is big, but kind."
  mc "Uh, maybe you should come along... just in case."
  flora skeptical_leash "Are you serious?"
  mc "Yes! I'm being very serious right now!"
  flora skeptical_leash "Fine, I guess. But you owe me double."
  flora skeptical_leash "What exactly are you planning to do, anyway?"
  mc "It's a long story, but basically I'm trying to get [jo] to return the gym schedule to normal."
  flora confused_leash "And of course, you're going to do it in the most absurd way possible...{space=-45}"
  mc "Pretty much, yeah."
  flora sarcastic_leash "This is why I'm her favorite, and you're a nuisance."
  mc "She just doesn't see my potential."
  flora sarcastic_leash "I don't think anyone does."
  mc "When Einstein was a kid—"
  flora eyeroll_leash "That's been debunked."
  mc "Whatever. Let's just go."
  flora sarcastic_leash "Lead the way!"
  $mc["focus"] = "kate_trick"
  $flora["hidden_now"] = True
  hide flora
  hide hati
  with Dissolve(.5)
  $quest.kate_trick.advance("school")
  return

label quest_kate_trick_school:
  show hati neutral at Transform(xalign=.25)
  show flora afraid_leash at Transform(xalign=.75)
  with Dissolve(.5)
  flora afraid_leash "Are you sure we're allowed to bring a dog inside the school?"
  mc "Yes!"
  flora afraid_leash "How do you know? Did you ask [jo]?"
  mc "I don't need to. The Newfall Constitution on Animal Rights, Chapter 87, subsection D, says I can."
  flora skeptical_leash "..."
  flora skeptical_leash "There's absolutely no way you've read through that."
  mc "Why not? Are you implying {i}you{/} haven't read up on animal rights?"
  flora afraid_leash "Well, I—"
  mc "Maybe I should take the leash for now, just so you don't accidentally break a rule."
  mc "You know, since you're a mere layman when it comes to our four-legged friends."
  mc "Animal rights is an important topic, [flora]."
  flora angry "I really will strangle you if you keep going..."
  mc "Sorry, sorry. Would you mind getting the [nurse] while I take [hati] to the gym?"
  mc "She's going to help me get my gym grade."
  flora concerned "I guess."
  window hide
  show flora concerned at disappear_to_left behind hati
  pause 0.25
  show hati neutral at move_to(.4,1.0)
  pause 1.0
  hide hati with Dissolve(.5)
  $quest.kate_trick.advance("gym")
  window auto
  return

label quest_kate_trick_nurse:
  show nurse neutral with Dissolve(.5)
  nurse neutral "Oh, gracious! Hello, [mc]."
  mc "Hello, [nurse]."
  nurse smile "You know, you shouldn't be cooped up inside when it's such a nice day out."
  nurse smile "Fresh air does wonders for the body and mind!"
  mc "Thanks for your concern, but I'm not here to discuss matters concerning my health."
  nurse afraid "Sorry, I didn't mean to presume... I just..."
  mc "I'm here because I need something from you."
  nurse annoyed "Goodness... I'm not sure what I could do for you..."
  mc "It's simple enough, really. You see, I need a dog..."
  mc "...and so I thought, what better place to find one who's full of obedience?"
  nurse annoyed "I... I don't know what you mean..."
  mc "Sure you do."
  mc "I need a well trained dog, and you're going to be just that for me, or else this little picture of you is going up everywhere."
  nurse sad "No, please! I'll be ruined!"
  nurse sad "But... what you're asking would be so humiliating..."
  mc "Great, so we'll both get something out of it!"
  nurse blush "I don't—"
  mc "Enough talking. Good dogs don't speak unless they're told."
  nurse concerned "..."
  mc "Perfect. Just like that."
  mc "Now get down on your knees so I can put this collar on you."
  nurse concerned "..."
  window hide
  show nurse concerned:
    easeout 1.0 yoffset 540
  pause 0.25
  show black onlayer screens zorder 100 with Dissolve(.5)
  $mc["focus"] = "kate_trick"
  $nurse["at_none_now"] = True
  pause 1.0
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  mc "Good dog."
  mc "Heel."
  hide nurse
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  $quest.kate_trick.advance("gym")
  return

label quest_kate_trick_gym:
  $kate["outfit_stamp"] = kate.outfit
  $kate.outfit = {"bra": "kate_cheerleader_bra", "necklace": None, "panties": "kate_cheerleader_panties", "pants": "kate_cheerleader_skirt", "shirt": "kate_cheerleader_top"}
  show kate neutral at appear_from_left
  kate neutral "Again? You're not allowed in the gym anymore."
  if quest.kate_trick["blackmail"]:
    kate neutral "How many times do I have to—"
    window hide
    show black onlayer screens zorder 100
    show nurse dog_mouth_closed
    with Dissolve(.5)
    pause 1.0
    hide kate
    hide black onlayer screens
    with Dissolve(.5)
    window auto
    "There's something supremely satisfying about the [nurse] crawling after me, doing my bidding."
    "Even better is the fact that [kate] is here, watching me use her little pet to meet my own needs."
    show nurse dog_kate_surprised with Dissolve(.5)
    kate "What the hell is this?"
    menu(side="middle"):
      extend ""
      "\"What do you mean? It's just\nme walking my dog.\"":
        $mc.charisma+=1
        mc "What do you mean? It's just me walking my dog."
        show nurse dog_kate_skeptical with Dissolve(.5)
        kate "Very funny."
        mc "I don't see anything funny here. Just me going about my day."
        kate "Well, go about your day somewhere else."
        mc "I will, right after I get my gym grade."
      "\"Did you think you were the\nonly one capable of this?\"":
        mc "Did you think you were the only one capable of this?"
        show nurse dog_kate_excited with Dissolve(.5)
        kate "What? Am I supposed to be impressed or something?"
        kate "She's the easiest one I've met."
        $nurse.love+=1
        mc "Hey, be nice to my dog!"
        show nurse dog_kate_skeptical with Dissolve(.5)
        kate "Don't tell me what to do, else I'll make {i}you{/} my dog."
        mc "As interesting as that would be, I have a gym grade to get right now.{space=-10}"
      "\"I'm looking for another bitch.\nAre you interested?\"":
        mc "I'm looking for another bitch. Are you interested?"
        show nurse dog_kate_excited with Dissolve(.5)
        kate "You sure have a lively fantasy."
        mc "Are you blushing a little? Maybe that's secretly {i}your{/} fantasy?"
        $kate.lust-=1
        show nurse dog_kate_skeptical with Dissolve(.5)
        kate "You need to get your eyes checked, you dumb clown."
        kate "Get the fuck out of here before I call the girls over."
        mc "Sorry, no can do. I actually have a gym grade to get."
    mc "Please, step out of the way."
    show nurse dog_kate_excited with Dissolve(.5)
    kate "I knew you were slow, but this is ridiculous."
    kate "The gym is closed. C-l-o-s-e-d. Got it?"
    mc "Not according to the Newfall Constitution on Animal Rights, Chapter 87, subsection D."
    show nurse dog_kate_neutral with Dissolve(.5)
    kate "What are you talking about?"
    mc "Oh, could it be that you're not familiar with the county rules?"
    mc "Please, do look it up. I'll wait."
    show nurse dog_kate_skeptical with Dissolve(.5)
    kate "Seriously?"
    mc "Seriously."
    show nurse dog_kate_neutral with Dissolve(.5)
    kate "..."
    kate "..."
    "Man, I sure hope [maxine] didn't make this up..."
    show nurse dog_kate_surprised with Dissolve(.5)
    kate "What the fuck?"
    mc "See? You're legally required to let me in."
    show nurse dog_kate_skeptical with Dissolve(.5)
    kate "This is not a dog, though! She doesn't even bark!"
    show nurse dog_mouth_open
    nurse dog_mouth_open "Woof! Woof!" with vpunch
    show nurse dog_kate_neutral with Dissolve(.5)
    kate "..."
    mc "Move or I'll get the [guard]. I know he's been looking to bust you ever since you put that special glaze on his doughnuts."
    show nurse dog_kate_annoyed with Dissolve(.5)
    kate "This is not over, [mc]!"
    kate "And you... you're in big trouble."
    window hide
    show expression "nurse avatar events dog kate_background" as background behind nurse
    show nurse dog_kate_annoyed_backgroundless at disappear_to_left
    pause 1.0
    hide background
    show nurse dog_mouth_closed_right at center
    with Dissolve(.5)
    window auto
    mc "Good girl! You did great."
    mc "People really need to stand up to [kate] more often."
    "And speaking of meeting needs... I think I'll get a little more use out of this situation."
    $unlock_replay("nurse_walk")
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    pause 1.0
    hide nurse
    hide black onlayer screens
    window auto
  else:
    kate neutral "How many times do I have to tell you?"
    mc "Actually, according to the Newfall Constitution on Animal Rights, Chapter 87—"
    window hide
    show kate neutral at move_to(.25)
    show hati neutral at appear_from_right(.75,.5)
    pause 0.5
    window auto show
    show kate embarrassed with dissolve2:
      easein 0.175 yoffset -10
      easeout 0.175 yoffset 0
      easein 0.175 yoffset -4
      easeout 0.175 yoffset 0
    hati neutral "Roar!" with vpunch
    show kate embarrassed:
      yoffset 0
    kate embarrassed "Aaaaaaaaah!"
    window hide
    show kate embarrassed at move_to(.1,.25)
    pause 0.5
    show kate embarrassed flip at move_to(.25,.25)
    pause 0.5
    show kate embarrassed at move_to(.1,.25)
    pause 0.5
    show kate embarrassed flip at disappear_to_right
    pause 0.5
    show hati neutral at move_to(.4,.75)
    pause 0.5
    window auto
    "Damn. There's always a bigger, badder wolf."
    "Hopefully, [kate] remembers that."
    show nurse blush at appear_from_right(.85)
    show flora blush at appear_from_right
    show hati neutral at move_to("left",1.0)
    nurse blush "Oh, my. That is a big dog..."
    flora laughing "Don't worry, he's just a big fluff ball!"
    mc "Yeah, a big fluff ball with five-inch fangs."
    flora thinking_leash "Anyway, I should probably get him back to the internship now."
    if mc.owned_item("dog_collar"):
      mc "Thanks for the help, [flora]! I appreciate it."
    else:
      mc "Before you go, could I borrow his collar for a bit?"
      flora worried_leash "What? Why?"
      mc "Because I need it to impress [kate] and make her my wife."
      flora laughing_leash "Nice joke."
      flora blush_leash "Okay, you can borrow it. This collar is mine, and I'm not tasked with babysitting [hati] for the next month. But you owe me triple."
      show hati neutral_collarless
      show flora blush_leash_retracted
      with dissolve2
      $mc.add_item("dog_collar")
      mc "Thanks, [flora]!"
      flora blush_leash_retracted "I'll collect the debt soon."
      mc "Fine, whatever."
    window hide
    if renpy.showing("flora thinking_leash"):
      show flora thinking_leash at disappear_to_right behind nurse
      show hati neutral at disappear_to_right
    elif renpy.showing("flora blush_leash_retracted"):
      show flora blush_leash_retracted at disappear_to_right behind nurse
      show hati neutral_collarless at disappear_to_right
    pause 0.25
    show nurse blush at move_to(.5,1.0)
    pause 1.0
    $flora["hidden_now"] = False
    show nurse thinking with Dissolve(.5)
    window auto
    nurse thinking "So, what is it that you needed my help with?"
    mc "I need your signature proving that I've been working out and improving my health."
    nurse concerned "Um, I don't know if I can do that... It would be unethical to lie."
    mc "Don't worry! I've actually planned to work out."
    mc "And you're going to help me."
    nurse concerned "Oh, okay..."
    hide nurse with Dissolve(.5)
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_gym"
  $quest.kate_trick.advance("workout")
  $kate.outfit = kate["outfit_stamp"]
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_kate_trick_workout:
  show nurse concerned with Dissolve(.5)
  mc "You know, I don't think I've been this excited about a gym session before."
  nurse concerned "I'm glad..."
  mc "You don't sound so convinced."
  nurse thinking "I just haven't done this sort of thing before."
  nurse thinking "I hope I don't disappoint you."
  "That's always been my line when it comes to athleticism."
  "Somehow, I feel more confident now."
  mc "It'll be fine. Lead the way!"
  nurse blush "As you wish."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $mc["focus"] = ""
  pause 1.0
  show nurse gym_sex_explanation
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  nurse gym_sex_explanation "So, the gastrocnemius muscle... just above the knee to the heel... meaning \"stomach of the leg...\""
  nurse gym_sex_explanation "...and the soleus muscle... just below the knee to the heel... meaning \"sandal...\""
  "It's always been a fantasy of mine to have a personal trainer."
  "A cute or hot one that doesn't bark orders at me."
  "Someone to just touch, and be touched by."
  "Letting the tension build and—"
  window hide
  show nurse gym_sex_stare with Dissolve(.5)
  window auto
  nurse gym_sex_stare "Are you even listening to me, [mc]?"
  "Crap. Perhaps I should be paying attention instead of daydreaming."
  mc "Of course! I'm ready when you are."
  nurse gym_sex_stare "Really? Let's get started, then."
  nurse gym_sex_stare "Please point out the calf muscle, and tell me its official name."
  window hide
  show nurse gym_sex_into_position with Dissolve(.5)
  window auto
  "With the [nurse] kneeling before me on the yoga mat, I take a moment{space=-15}\nto appreciate the view."
  "The way she's down on all fours, her round ass sticking out."
  "I get behind her and reach out to take her calf in my hands."
  mc "Well, you have your gastrocnemius..."
  "I trail my fingers down from the back of her knee, towards her ankle, caressing her outer leg."
  "Her body feels so good beneath my touch. So squeezable and easily bent to my will."
  mc "And then you have your soleus..."
  window hide
  show nurse gym_sex_leg_lift with vpunch
  window auto
  nurse gym_sex_leg_lift "Ah!"
  "I then lift her left leg and stretch it out towards my chest."
  "I dig my fingers into her thighs and she gives a satisfying little yelp."
  nurse gym_sex_leg_lift "[mc]! T-that hurts..."
  mc "I'm correct, though, aren't I? Tell me I've earned my grade."
  nurse gym_sex_leg_lift "Y-yes, you are correct! I can sign off on your grade now... If you'll just let me up."
  if quest.kate_trick["blackmail"]:
    mc "Let you up? I don't think so."
    mc "This whole thing has really turned you on, hasn't it?"
    "I spread her legs even further apart and admire her overflowing juices."
    "She just gives a little whine and shakes her ass back and forth for me, ever the silent, well trained pet."
    mc "You were a good dog earlier, and I think you deserve a little treat..."
    "Her head jerks around, her eyes big and pleading as she looks at me.{space=-20}"
    mc "Oh? You want to speak, girl?"
    "She nods her head, her tongue hanging out of her mouth as she kneels there panting."
    mc "That's too bad. You just keep obeying."
  else:
    menu(side="middle"):
      extend ""
      "\"Thank you, [nurse]!\"":
        mc "Thank you, [nurse]!"
        window hide
        show nurse gym_sex_into_position with Dissolve(.5)
        window auto
        nurse gym_sex_into_position "You're very welcome..."
        "Perhaps I could've gotten a little more use out of this situation, but that should be enough for a good grade."
        jump quest_kate_trick_workout_one
      "?nurse.love>=5@[nurse.love]/5|{image=nurse contact_icon}|{image=stats love}|\"Just a moment, I'm not quite finished yet.\"":
        pass
    mc "Just a moment, I'm not quite finished yet."
    mc "There's more of the anatomy I'd like to explore."
    nurse "Gosh, I—"
  window hide
  show expression "images/characters/nurse/avatar/events/gym_sex/fondle_firstframe.webp" as nurse with Dissolve(.5)
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/fondle.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/gym_sex/fondle_firstframe.webp") as nurse
  pause 0.5
  window auto
  "I drop her leg back down to the mat and take hold of her hips, pulling her closer to me."
  "Her ass —  teasing me this whole time — sways in front of me, juicy and full."
  "I take a handful, squeeze hard."
  if quest.kate_trick["blackmail"]:
    "With her face glowing pink and sweat beading on her forehead, she continues to watch me."
  else:
    mc "My favorite part of the human body."
    nurse "Gracious... I'm not so sure about this..."
    "With her face glowing pink and sweat beading on her forehead, she trembles under my touch."
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/pants_down.webm", side_mask=False, loop=False, image="images/characters/nurse/avatar/events/gym_sex/pants_down_lastframe.webp") as nurse
  if quest.kate_trick["blackmail"]:
    "She's begging for it with her eyes, so I eagerly give it to her."
    mc "There's a good girl. I'll give you what you want."
  else:
    "She doesn't struggle, despite her words. Her eyes are practically begging for it."
  "In one swift movement, I slip my fingers into the waistband of her pants and yank them down."
  nurse "O-oh!"
  mc "Here's the real workout lesson."
  window hide
  show expression "images/characters/nurse/avatar/events/gym_sex/penetration_slow_firstframe.webp" as nurse with Dissolve(.5)
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/penetration_slow.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/gym_sex/penetration_slow_firstframe.webp") as nurse
  $renpy.music.stop(channel=u'sprite', fadeout=0)
  pause 0.5
  window auto
  if quest.kate_trick["blackmail"]:
    "Before she can protest or let out another whimper, I push her panties aside, and thrust into her pussy, hard."
  else:
    "Before she can try uttering another weak protest, I push her panties aside, and thrust into her pussy."
  "When I'm halfway in, I place both hands on her hips once more and pull her back into me."
  "With a grunt, I push myself in the rest of the way."
  nurse "Oooh!"
  if quest.kate_trick["blackmail"]:
    mc "I thought I told you to keep quiet."
    "She moans and it turns into a little whine as she adjusts to me being inside of her."
  else:
    mc "Goddamn, that feels good."
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/penetration_steady.webm", side_mask=False) as nurse
  "This is the physical activity all these muscles were made for. Just pure, primal fucking."
  "The smell of the sweat and desire already fills the gym."
  if quest.kate_trick["blackmail"]:
    "Her body gives way beneath mine, giving in to my commands."
  else:
    "Her body gives way beneath mine."
  "After I adjust, I withdraw some, then slam my dick home again."
  "Rocking her ass into my pelvis."
  if quest.kate_trick["blackmail"]:
    nurse "Mmmmuuhhhh..."
    "The [nurse] is trying her best not to scream out, to let the words leave her lips."
    "She just continues to kneel there on all fours, taking whatever I choose to give her."
  else:
    nurse "Oh, gggod! This is..."
    nurse "We shouldn't be... not here..."
    "Despite her obvious wetness, the [nurse] still tries to maintain an innocent act."
  window hide
  show expression "images/characters/nurse/avatar/events/gym_sex/penetration_fast_firstframe.webp" as nurse with Dissolve(.5)
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/penetration_fast.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/gym_sex/penetration_fast_firstframe.webp") as nurse
  $renpy.music.stop(channel=u'sprite', fadeout=0)
  pause 0.5
  window auto
  "So, I reach out and grab her by the hair, twist a fistful of her dark locks around my fingers and yank her head back."
  if quest.kate_trick["blackmail"]:
    mc "I know you're loving this. Such a good treat for a good dog."
  else:
    mc "Quiet."
  "I hold her there like that, angling her head upwards with one hand, the other still holding her body in place."
  "I use my hold on her as leverage to go faster, to drive myself in harder."
  "The sound of our flesh smacking together mixes with her little cries.{space=-10}"
  "Everything outside of this moment disappears."
  "We're just two animals fucking on the floor."
  "Her tongue hangs out of her mouth as her pants and gasps get louder."
  nurse "P-please..."
  mc "What's that? You want some more?"
  mc "I'll give you more."
  mc "Every. Damn. Inch."
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/penetration_furious.webm", side_mask=False) as nurse
  "Each word is punctuated by a thrust and I watch as my cock goes in and out, in and out."
  "Finally, as I near the edge, I push her face down into the mat."
  "She whimpers as I use the handles of her hips to brace myself."
  "To go as deep as I can, until I bottom out on a final thrust."
  "She moans beneath me, her face stuck to the mat with her sweat and drool."
  "Her pussy spasms around my cock and I feel her juices soaking us, keeping us glued together."
  "I throw my head back and groan as the dam breaks at last."
  window hide
  show expression "images/characters/nurse/avatar/events/gym_sex/cum_firstframe.webp" as nurse with Dissolve(.5)
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/gym_sex/cum.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/gym_sex/cum_firstframe.webp", loop=False, image="images/characters/nurse/avatar/events/gym_sex/cum_lastframe.webp") as nurse
  $renpy.music.stop(channel=u'sprite', fadeout=0)
  pause 0.5
  window auto
  nurse "Ooooh... yes!"
  "I refuse to let up. I couldn't even if I wanted to, with the way she's squeezing me tight."
  "So as nature intended, I fill her up."
  "Releasing wave after wave of cum deep inside her."
  mc "Unnngh..."
  "I ride the wave of pleasure, until there's nothing left."
  "Then, as I finally catch my breath, I pull out and give her ass a light smack."
  if quest.kate_trick["blackmail"]:
    mc "Good girl."
  "I'd say that was the kind of physical activity worthy of a good grade."
  $unlock_replay("nurse_workout_lesson")

  label quest_kate_trick_workout_one:
  "Now I just gotta tell [jo] how physical I've been and get the gym reopened..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $quest.kate_trick.advance("report")
  pause 1.0
  hide nurse
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  return

label quest_kate_trick_workout_exit:
  "I've finally managed to get in. No point in leaving before I get my gym grade."
  return

label quest_kate_trick_report:
  show jo sarcastic with Dissolve(.5)
  jo sarcastic "Hello, kiddo!"
  mc "Hey, [jo]. How do I look?"
  jo flirty_hands_to_the_side "Oh, darling, you always look handsome to me."
  show jo flirty_hands_to_the_side at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.strength>=8@[mc.strength]/8|{image=stats str}|Flex":
      show jo flirty_hands_to_the_side at move_to(.5)
      mc "..."
      $jo.love+=2
      jo laughing "I see what this is about!"
      jo excited "Yes, I can see that you've been working out!"
      jo excited "Doesn't it feel good to get into shape?"
      mc "It does, but you also made a promise."
      jo confident "You're absolutely right."
      jo confident "Okay, I'm returning the gym schedule to normal."
      mc "Thanks, [jo]!"
      jo smile "Keep it up, kiddo."
    "\"Never mind...\"":
      show jo flirty_hands_to_the_side at move_to(.5)
      mc "Never mind..."
      "I'm clearly not buff enough to make an impression."
      mc "I wanted to talk to you about getting the gym schedule changed back, actually."
      jo afraid "Oh? Did you make your grade, then?"
      mc "Yep! Let's just say I'm at peak performance right now..."
      jo laughing "That's fantastic to hear!"
      jo excited "You know, I'm proud of you. Taking your studies more seriously, and an active interest in the school."
      mc "Thanks, [jo]!"
      jo excited "Keep it up, kiddo."
  window hide
  hide jo with Dissolve(.5)
  pause 0.25
  show isabelle excited at appear_from_right
  window auto
  isabelle excited "Sorry, [mc], I couldn't help but overhear your conversation with\nthe principal."
  isabelle excited "Is it happening, then? Are they going to fix the gym schedule?"
  mc "Yeah, it's true."
  isabelle laughing "I knew you could do it!"
  mc "It was nothing, really..."
  isabelle confident "So modest!"
  isabelle confident "Thank you so much for your help, truly. You really are one of a kind."
  mc "You inspire me to be better. I'm just happy to be part of doing what's right."
  $isabelle.lust+=2
  isabelle blush "Aw, that's sweet."
  isabelle blush "Now everyone has a fair and equal opportunity to get the education they deserve, thanks to you sticking it to that tyrant."
  mc "Nothing gets the blood pumping like sweet justice. That's the real workout."
  isabelle excited "You got that right!"
  isabelle neutral "I should probably get to class now. Thanks again for your help, [mc], it means a lot!"
  show isabelle neutral at disappear_to_right
  "[isabelle] is so sweet..."
  "It almost feels bad gaining her trust only to betray her later. But getting on [kate]'s good side is more important."
  $quest.kate_trick.finish()
  return

label quest_kate_trick_report_home:
  "[jo] doesn't like to discuss school matters at home. I should probably wait for a better moment."
  return
