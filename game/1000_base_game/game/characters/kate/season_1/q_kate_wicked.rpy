image kate_wicked_eating_out = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-275,-160),Transform("kate lacey_bet_fingering_pussy",size=(653,368))),"ui circle_mask"))
image kate_wicked_spanking = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-229,-176),Transform("kate spanking_red_kate5",size=(653,368))),"ui circle_mask"))


label quest_kate_wicked_start:
  stop music
  window hide
  show misc piano_play bg with Dissolve(.5)
  pause 1.0
# play music "dark_and_dramatic"
  play music "fast_but_dark"
  show screen music_notes
  pause 23.0
  play music "school_theme" fadein 0.5
  hide screen music_notes
  hide misc piano_play bg
  show kate confident
  with Dissolve(.5)
  window auto
  kate confident "I thought I heard someone being noisy over here."
  mc "It's just me and my piano. I can stop if it bothers you."
  kate confident "It was okay, I've heard worse."
  mc "So, you liked it?"
  kate neutral "Let's not get ahead of ourselves here."
  show kate neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I don't have time for\nyour negativity, [kate].\"":
      show kate neutral at move_to(.5)
      mc "I don't have time for your negativity, [kate]."
      $kate.lust-=1
      kate laughing "That's odd. Last time I checked, you had no life."
      mc "Why did you check that? Doesn't that mean you have boring interests?"
      "Ugh, that was the worst comeback ever..."
      "[kate] makes me nervous, so my thoughts get muddled..."
      kate smile "A few moments of morbid curiosity hasn't hurt anyone."
    "\"What do you think can be improved?\"":
      show kate neutral at move_to(.5)
      mc "What do you think can be improved?"
      kate neutral "All of it. It's not terrible, but it's not good either."
      kate neutral "Also, the intro is important in every song because it's the listener's first impression. Make sure you don't fuck that up next time."
      kate confident "I did like the choice of melody, though."
      "I knew I messed up a few notes here and there..."
      "[kate] has always been a great music student. Even though she's mean, I think that was genuinely good advice."
      $mc.intellect+=1
      "Sometimes the harshest critics provide the best feedback."
      mc "Thanks, [kate]."
      kate confident "You're very welcome."
    "\"I'm sorry it offended you, ma'am.\"":
      show kate neutral at move_to(.5)
      mc "I'm sorry it offended you, ma'am."
      kate neutral "Did I say it offended me?"
      mc "No, but—"
      kate neutral "Then, don't put words in my mouth."
      mc "Sorry..."
      kate neutral "Are we clear?"
      mc "Yes, ma'am."
      $kate.lust+=1
      kate confident "Good."
  kate thinking "Anyway, as much as it pains me, I do need your help with something.{space=-20}"
  mc "What is it?"
  kate blush "I'm happy you're so eager to help."
  mc "I didn't say I was going to do it."
  kate excited "Oh, you will."
  kate excited "You will."
  mc "..."
  kate excited "You're going to call [jo] over here. And when she gets here, you're going to ask her for the key to the music classroom."
  mc "Why?"
  kate excited "Because I said so."
  kate excited "And because there might be something in it for you."
  "I'm not sure what she's planning, but it's probably nothing good."
  "On the other hand, during our sophomore year, a couple of the other losers rose in status after helping her..."
  mc "Can you at least tell me what it's for?"
  kate annoyed "If you must know, [isabelle] is kissing the asses of teachers and staff. No surprise there, really."
  mc "What did she do?"
  kate skeptical "She's taken it upon herself to help out with the Independence Day celebrations. Gross, right?"
  kate excited "Well, I'm planning on outdoing her with a secret party of my own."
  mc "You do know that parties are against the school rules, right?"
  kate eyeroll "That's why it's a {i}secret{/} party, dumbo."
  show kate eyeroll at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Am I invited?\"":
      show kate eyeroll at move_to(.5)
      mc "Am I invited?"
      kate laughing "You're joking, right?"
      mc "I guess..."
      kate smile "Stop joking and get to it."
      show kate smile at disappear_to_right
    "\"You're having a party\nin the music classroom?\"":
      show kate eyeroll at move_to(.5)
      mc "You're having a party in the music classroom?"
      kate skeptical "Stop asking so many questions. All you need to do is listen and obey.{space=-25}"
      kate skeptical "It shouldn't be too hard, even for someone like you."
      mc "Fine..."
      kate skeptical "Is there a problem?"
      show kate skeptical at move_to(.75)
      menu(side="left"):
        extend ""
        "\"No, ma'am...\"":
          show kate skeptical at move_to(.5)
          mc "No, ma'am..."
          kate neutral "Are you sure? I don't like you giving me attitude."
          mc "I'm sorry, I'll get the key."
          $kate.lust+=1
          kate confident "That's better. Now get to it."
          show kate confident at disappear_to_right
        "\"I don't like you bossing me around.\"":
          show kate skeptical at move_to(.5)
          mc "I don't like you bossing me around."
          $kate.lust-=1
          kate laughing "What you like isn't much of my concern, is it?"
          kate neutral_hands_down "Now get that key before you really start to piss me off."
          show kate neutral_hands_down at disappear_to_right
    "\"I'll get the key for you, ma'am.\"":
      show kate eyeroll at move_to(.5)
      mc "I'll get the key for you, ma'am."
      $kate.lust+=1
      kate excited "Good boy. Now get to it."
      show kate excited at disappear_to_right
  "As much as [kate] is a demanding bitch, it's always best to do\nwhat she says."
  "I just need to come up with a good excuse when I call [jo]..."
  if quest.kate_wicked.in_progress:
    $quest.kate_wicked.advance("phone_call", silent=True)
  else:
    $quest.kate_wicked.start("phone_call")
  $mc["focus"] = "kate_wicked"
  return

label quest_kate_wicked_phone_call_exit:
  "[kate] asked me to call [jo] over here."
  "As much as she's a demanding bitch, it's always best to do\nwhat she says."
  return

label quest_kate_wicked_phone_call:
  $set_dialog_mode("phone_call_plus_textbox","jo")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","jo")
  jo "Hi, honey!"
  mc "Hey, [jo]. Are you\nbusy right now?"
  jo "I'm always busy,\nsweetheart."
  jo "What's wrong?"
  mc "Nothing's wrong,\nbut, uhm..."
  mc "Could you meet me\nin the fine arts wing\nin a bit?"
  jo "Sure, but are you going to\ntell me what this is about?"
  jo "You're not in trouble,\nare you?"
  mc "No, nothing like that!"
  jo "Okay. Meet you there."
  play sound "end_call"
  $set_dialog_mode("")
  pause 0.25
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 2.0
  show jo smile at appear_from_right
  hide black onlayer screens with Dissolve(.5)
  window auto
  jo smile "There you are!"
  jo smile "Are you going to tell me what this is about?"
  show jo smile at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"I'm working on a music project.\"":
      show jo smile at move_to(.5)
      mc "I'm working on a music project."
      $jo.love+=1
      jo laughing "You always were my creative boy!"
      jo excited "What are you working on?"
      mc "It's, err... a surprise?"
      jo excited "I see! So, what do you need help with?"
      mc "I don't really need help, but..."
    "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"I was having a rough day\nand needed a hug.\"":
      show jo smile at move_to(.5)
      mc "I was having a rough day and needed a hug."
      jo afraid "Oh, no! My poor baby! Come here!"
      jo flirty "And have a kiss as well, for good measure."
      window hide
      show black onlayer screens zorder 100
      show jo JoMorningKiss_repeat
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Man, I never get tired of feeling [jo]'s wet lips on my forehead, and her juicy breasts squeezing against my chest."
      "It's one of those forbidden pleasures that never really makes sense, but is always there in the back of your mind."
      window hide
      show black onlayer screens zorder 100
      show jo flirty_hands_to_the_side
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      $jo.lust+=1
      window auto
      jo flirty_hands_to_the_side "How's that? All better?"
      jo flirty_hands_to_the_side "Now, tell me what this is about."
      mc "Just the usual — I'm already falling behind in music class, and I was hoping you could help me."
      jo afraid "Of course! What do you need?"
    "\"[kate] asked me if she could borrow\nthe key to the music classroom.\"":
      show jo smile at move_to(.5)
      mc "[kate] asked me if she could borrow the key to the music classroom."
      jo concerned "[kate]? I didn't know you two were friends."
      mc "It's, err... a recent thing? I'm trying to be more sociable."
      jo laughing "That's great, [mc]!"
      jo excited "[kate] is a great student and very popular. I think you could learn\na lot from her."
      mc "Right..."
      jo excited "So, why didn't she come ask me herself?"
      mc "She's, uhm... she was shy?"
      jo laughing "Shy? She's never struck me as the shy type!"
      show jo laughing at move_to(.25)
      menu(side="right"):
        extend ""
        "\"The new curriculum is\ngiving her trouble.\"":
          show jo laughing at move_to(.5)
          mc "The new curriculum is giving her trouble."
          mc "She's ashamed to admit that she needs extra hours after dark."
          jo sarcastic_hands_to_the_side "I see! Okay, here's the key to the music classroom."
          $mc.add_item("key_music_class")
          $jo.love+=1
          jo flirty "I'm proud of you for helping her out!"
          jo flirty "I have to get back now. Let me know how it went with [kate]!"
          show jo flirty at disappear_to_right
        "\"Not everyone is how they\nappear on the surface...\"":
          show jo laughing at move_to(.5)
          mc "Not everyone is how they appear on the surface..."
          mc "I think you might have the wrong impression of her."
          jo sarcastic "That could very well be the case."
          $mc.intellect+=1
          mc "When it comes to [kate], I assure you it is."
          jo flirty_hands_to_the_side "Okay. Give her the key to the music classroom, and tell her that she can stop by my office next time. I don't bite!"
          $mc.add_item("key_music_class")
          jo flirty_hands_to_the_side "I have to get back now. Let me know how it went with [kate]!"
          show jo flirty_hands_to_the_side at disappear_to_right
      "That went easier than expected. Sometimes you just have to tell the truth..."
      "...or a partial truth."
      jump quest_kate_wicked_phone_call_kate
  mc "I'd like to stay after school and work here, if that's okay. It's sometimes hard to focus at home."
  jo confident "Very diligent! Okay, I'll let the [guard] know that you're staying late."
  jo confident "Here's the key to the classroom. You know we lock them up\nafter dark."
  $mc.add_item("key_music_class")
  mc "Thanks, [jo]!"
  jo confident "You're very welcome. See you after school!"
  window hide
  show jo confident at disappear_to_right
  pause 1.0

label quest_kate_wicked_phone_call_kate:
  show kate confident at appear_from_right
  window auto
  kate confident "Sounded like that went well."
  mc "It did, yes."
  mc "Can you tell me why you couldn't just pick up the key yourself?"
  kate confident "I needed to sneak into [jo]'s office to cancel all the music classes that day."
  kate confident "A party doesn't prepare itself."
  mc "Right."
  kate neutral "So, did you get the key?"
  show kate neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yes, ma'am. Here you go.\"":
      show kate neutral at move_to(.5)
      mc "Yes, ma'am. Here you go."
      $mc.remove_item("key_music_class")
      $kate.lust+=1
      kate blush "Very good."
      mc "What about my reward?"
      kate thinking "What reward?"
      mc "You said that there would be a reward for helping you."
      kate thinking "Did I? Well..."
      kate blush "I guess I have one of these to spare."
      kate blush "Congratulations! Here's your reward!"
      $mc.add_item("costume_shop_voucher")
      kate excited "You could use some new clothes. Or maybe a mask to hide\nyour face."
      "Ugh, I should've known it was all just a ploy to humiliate me..."
      "How does [kate] even come up with this stuff?"
      kate skeptical "Aren't you going to thank me?"
      mc "Thanks..."
      kate excited "Don't look so sad! You needed the makeover."
      kate excited "Anyhow! I have a party to plan. Smell you later!"
      show kate excited at disappear_to_right
    "\"You said there was something\nin it for me...\"":
      show kate neutral at move_to(.5)
      mc "You said there was something in it for me..."
      $kate.lust-=1
      kate eyeroll "Are you always this greedy? Why can't you do something just\nto be nice?"
      mc "You promised!"
      kate skeptical "I didn't promise anything."
      kate excited "But! I do have something that you might like... or not. I mean, it's hard to read your kind."
      mc "Fine, what is it?"
      kate excited "First, give me the key."
      mc "Here you go..."
      $mc.remove_item("key_music_class")
      kate gushing "With that out of the way... here's your reward!"
      $mc.add_item("costume_shop_voucher")
      kate gushing "I had a few of these to spare. I'm sure you'll find something in the costume shop that will improve your looks!"
      kate gushing "Maybe a set of fake teeth or a clown nose!"
      mc "..."
      "That's messed up, even for [kate]."
      "Why would she go through the trouble of getting this just to humiliate me?"
      "Maybe she's getting off on it, somehow..."
      "..."
      "Or maybe... her party is a costume party?"
      "That must be it!"
      kate laughing "What do you say?"
      mc "Thanks..."
      kate smile "Good. Now if you'll excuse me, I have a party to plan."
      show kate smile at disappear_to_right
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"Yes, but she said I couldn't\ngive it away.\"":
      show kate neutral at move_to(.5)
      mc "Yes, but she said I couldn't give it away."
      kate skeptical "What's that supposed to mean?"
      mc "It means that I need to come and unlock the music classroom for you at the start of the party."
      $kate.lust-=1
      kate skeptical "You're bluffing..."
      mc "Why would I? It's not like I want to stop by the school after dark, just to unlock a door to a party that I'm not invited to."
      kate eyeroll "Fine... just make sure you're not late."
      kate skeptical "The party is in two days. Write it down!"
      kate skeptical "And don't you dare mess this up. Get lost now."
      show kate skeptical at disappear_to_right
      "So, two days from now, I have the chance to either help her or ruin [kate]'s party..."
      "The power is exhilarating!"
      "Right, I better get out of here before—"
      "..."
      "Huh? She must've dropped this on her way out..."
      "It looks like a voucher of some sort."
      $mc.add_item("costume_shop_voucher")
      "Oh, it's for a costume shop! She's probably hosting a costume party!"
      $quest.kate_wicked.advance("wait")
      $mc["focus"] = ""
      return
  $quest.kate_wicked.finish("flaccid_done")
  return

label quest_kate_wicked_costume_shop:
  "All right, let's see what this site is about..."
  "..."
  "They have all sorts of interesting stuff."
  if not lindsey["romance_disabled"]:
    "A knight costume would certainly impress [lindsey]..."
  "But this voucher only covers small items..."
  if quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed:
    "Oh! They have the pig mask that [isabelle] asked for! Let's go\nwith that!"
  elif quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
    "Guess I'll just go with the pig mask. It's a classic."
  if False: ## Placeholder ## The player has started Last Breath ##
#   "That thing sure is creepy..."
#   "Maybe I can use it to scare [lindsey] into a trance?"
    pass
  $mc.remove_item("costume_shop_voucher")
  $home_computer["pig_mask_ordered"] = home_computer["pig_mask_ordered_today"] = True
  "Okay, order placed!"
  "Now to wait for the delivery..."
  return

label quest_kate_wicked_school:
  $kate["outfit_stamp"] = kate.outfit
  $kate.outfit = {"bra": "kate_bra", "necklace": "kate_necklace", "panties": "kate_panties", "pants": "kate_pants", "shirt": "kate_shirt"}
  show kate neutral at appear_from_left
  kate neutral "You there."
  mc "..."
  kate confident "Yes, I'm talking to you."
  kate confident "I get it. It's a big day for someone like you to talk to someone like me in such a public setting."
  kate laughing "Just try not to wet yourself over it."
  mc "..."
  mc "I'll do my best..."
  kate cringe "That is a loser's attitude."
  kate cringe "Trying is what matters, isn't that right? Applauding failure. Pathetic."
  kate excited "Anyway, tonight is my party. "
  kate excited "When school ends today, I expect to find you next to the music classroom with your little key and ready to open the door for me."
  kate excited "Am I making myself clear?"
  show kate excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Yes, ma'am. I'll be there.\"":
      show kate excited at move_to(.5)
      mc "Yes, ma'am. I'll be there."
      $kate.lust+=1
      kate flirty "Good. You're like a little puppy."
      kate flirty "I should train you to do more tricks for me."
      mc "Thanks... I guess?"
      kate neutral "Uh-uh. Try again."
      mc "Thank you, ma'am."
      kate confident "Better."
      kate confident "Okay, run along now."
      window hide
      show kate confident at disappear_to_left
    "\"Why in the music classroom?\"":
      show kate excited at move_to(.5)
      mc "Why in the music classroom?"
      kate neutral "What?"
      mc "Why not have the party somewhere else?"
      kate confident "Anyone can organize a party at the beach or in the backyard."
      kate confident "A party at school after dark, on the other hand..."
      kate confident "That'll be a hot topic long after we graduate."
      mc "I meant, why not at the pool or something? Why the music classroom?{space=-55}"
      kate laughing "Because, my little idiot, music is central to any party."
      $mc.intellect+=1
      mc "Sure, but you can bring music to the pool. You can't, however, bring the pool to the music classroom."
      kate thinking "You're not wrong..."
      kate blush "I'll save that idea for later. Maybe you're not as dumb as you look."
      mc "Gee, thanks."
      kate embarrassed "Don't give me attitude."
      kate embarrassed "And don't be late tonight."
      window hide
      show kate embarrassed at disappear_to_left
  pause 1.0
  $kate.outfit = kate["outfit_stamp"]
  $quest.kate_wicked["party_tonight"] = True
  window auto
  return

label quest_kate_wicked_school_guard:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location != "school_first_hall_west":
    "It's getting late. [kate] told me to meet her next to the music classroom."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    call goto_school_first_hall_west
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
  show guard suspicious_flashlight at appear_from_right
  window auto
  guard suspicious_flashlight "I'm making my final rounds now."
  guard suspicious_flashlight "I want you out of here when I return."
  show guard suspicious_flashlight at disappear_to_right
  "Whatever, dude."
  "Where the hell is [kate]?"
  "I swear this is just some prank to get me into trouble..."
  window hide
  pause 0.25
  $kate["outfit_stamp"] = kate.outfit
  $kate.outfit = {"bra": None, "necklace": None, "panties": None, "pants": None, "shirt": "kate_angel_costume"}
  show kate confident at appear_from_right
  pause 0.5
  window auto
  "Wow. Look at her."
  "Now that's an ironic costume..."
  "But somehow she still pulls it off in her own twisted angelic way."
  "Lucifer was an angel, after all."
  "And in [kate]'s case... a damn sexy one."
  kate confident "What are you waiting for?"
  kate confident "You have a door to open for me, remember?"
  $quest.kate_wicked["ironic_costume"] = True
  $mc["focus"] = "kate_wicked"
  hide kate with Dissolve(.5)
  return

label quest_kate_wicked_school_art_class:
  "Locked. But why does it sound like someone is moaning inside?"
  "Nah, it must be my imagination..."
  return

label quest_kate_wicked_school_english_class:
  "Locked. I'm not sure why, though."
  "Nothing in this room is really worth stealing."
  return

label quest_kate_wicked_school_library:
  "Locked. Also being renovated."
  "Last time around, the library didn't open until Christmas."
  return

label quest_kate_wicked_school_music_class:
  "Locked. Damn it."
  "I could've used some music right about now..."
  return

label quest_kate_wicked_school_key_music_class(item):
  if item == "key_music_class":
    $mc.remove_item("key_music_class")
    play audio "lock_click"
    mc "..."
    mc "There you go..."
    window hide
    show kate confident with Dissolve(.5)
    window auto
    kate confident "Good. Very good."
    mc "Um... I..."
    kate neutral "Out with it. You're wasting my time with your stuttering."
    mc "I was wondering if I could join the party..."
    kate laughing "..."
    mc "Because... you know..."
    kate laughing "..."
    mc "I got the key for you and everything..."
    kate laughing "..."
    kate smile "No. Any other questions?"
    mc "I guess not..."
    kate smile "If you want, you can open the door for me."
    mc "..."
    kate laughing "It's only polite!"
    mc "Fine..."
    kate confident "Good boy."
    mc "I hope you have a nice party..."
    $kate.lust+=1
    kate confident "You bet I will."
    window hide
    show kate confident at disappear_to_left
    pause 1.0
    window auto
    "Oh, well. What's a party in the music classroom, anyway?"
    "After all, I suppose it would be frightfully dull, and boring, and completely—"
    "..."
    "Fuck, did I just quote Cinderella?"
    "I never went to any parties last time around... not that I was ever invited."
    "But maybe this time around I should invite myself..."
    "It is a costume party, after all."
    "Maybe I can put something together quickly. Something all-covering.{space=-10}"
    "Hmm..."
    "How about a ghost? Being invisible is already kind of my thing."
    "Very simple, but very effective. I just need to find a bedsheet."
    $quest.kate_wicked.advance("costume")
  else:
    "My [item.title_lower] may be useful, but not for opening this door."
    $quest.kate_wicked.failed_item("key_music_class",item)
  return

label quest_kate_wicked_school_exit:
  "Some things in life just aren't worth it."
  "One of them is ending up on [kate]'s kill list for ditching her."
  return

label quest_kate_wicked_school_kate:
  show kate neutral with Dissolve(.5)
  kate neutral "What are you waiting for? Just open the door."
  hide kate with Dissolve(.5)
  return

label quest_kate_wicked_costume_music_class:
  "Oh, well. What's a party in the music classroom, anyway?"
  "After all, I suppose it would be frightfully dull, and boring, and completely—"
  "..."
  "Fuck, did I just quote Cinderella?"
  return

label quest_kate_wicked_costume_locked:
  "Locked."
  return

label quest_kate_wicked_costume_first_hall:
  "Damn, the flashlight of the [guard] is coming up the stairs..."
  "He's probably making his final rounds."
  $quest.kate_wicked["final_rounds"] = True
  return

label quest_kate_wicked_costume_first_hall_stairs:
  show guard suspicious_flashlight at appear_from_right
  guard suspicious_flashlight "You're not supposed to be here."
  mc "I have special permission from the principal..."
  guard suspicious_flashlight "Not to stay overnight. I'm closing up, so get your ass downstairs."
  mc "Fine..."
  $quest.kate_wicked["ass_downstairs"] = True
  jump goto_school_ground_floor

label quest_kate_wicked_costume_sports_wing:
  "Huh? What the hell?"
  $quest.kate_wicked["what_the_hell"] = True
  return

label quest_kate_wicked_costume_entrance_hall_stairs:
  show guard angry_flashlight at appear_from_right
  if quest.kate_wicked["throw_you_out_myself"]:
    guard angry_flashlight "Didn't I tell you to get out of here?"
    guard angry_flashlight "You're staring down the barrel of a week's detention..."
    "Crap."
    $mc["detention"]+=1
  else:
    guard angry_flashlight "Guess I'll have to throw you out myself..."
    "Well, shit."
    $quest.kate_wicked["throw_you_out_myself"] = True
  window hide
  play sound "falling_thud"
  scene black
  show black onlayer screens zorder 100
  with vpunch
  $game.location = "school_entrance"
  $renpy.pause(1.0)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  return

label quest_kate_wicked_costume_exterior:
  "Oh, well. So much for that party."
  "..."
  "Huh? Is that [maxine] working late?"
  "If only I could get her to throw down her rope for me..."
  "But how do I reach her?"
  $quest.kate_wicked["reach_her"] = True
  return

label quest_kate_wicked_costume_exterior_window:
  "[maxine]'s rope might be my only way back into the school."
  "Unfortunately, she always pulls it up after dark."
  "Technically, I could throw a rock at the window to get her attention...{space=-10}"
  "...but with my luck, I'd probably break it..."
  "...get caught and expelled..."
  "...fail to get a job due to lacking education..."
  "...end up on the street..."
  "...join a gang of delinquents..."
  "...get caught for arson..."
  "...spend ten years in prison..."
  "...get out..."
  "...get caught for arson again..."
  "...spend another ten years in prison..."
  "...get out..."
  "...clean up my act..."
  "...get my act dirty again..."
  "...get caught for arson one more time..."
  "...rather die than go back..."
  "...set myself on fire..."
  "...finally become hot, but in the wrong way..."
  "...and then die."
  "So, unless I want to burn to death, I need to find another way to contact [maxine]."
  if mc.check_phone_contact("maxine"):
    "For example, call her."
  else:
    "For example, call [flora] and ask her for [maxine]'s number."
  $quest.kate_wicked["another_way"] = True
  return

image phone_static_screen:
  Crop((0,0,455,810),"maxine avatar events surveillance_footage static_screen1")
  0.1
  Crop((0,0,455,810),"maxine avatar events surveillance_footage static_screen2")
  0.1
  Crop((0,0,455,810),"maxine avatar events surveillance_footage static_screen3")
  0.1
  repeat

label quest_kate_wicked_costume_exterior_flora:
  $set_dialog_mode("phone_call_plus_textbox","flora")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","flora")
  flora "This better be important."
  mc "As fate would have it—"
  flora "Talk normal or\nI'm hanging up."
  mc "Ugh, fine! I need\n[maxine]'s number!"
  flora "And why is that, exactly?"
  $set_dialog_mode("phone_call","flora")
  menu(side="middle"):
    extend ""
    "\"I'm trying to fuck her.\"":
      $set_dialog_mode("phone_call_centered","flora")
      mc "I'm trying to fuck her."
      $flora.love-=2
      flora "Good luck with that."
      mc "So, you'll give me\nher number?"
      flora "..."
      flora "I guess it would be fun\nto watch you flounder."
      mc "I'll have you know—"
      play sound "end_call"
      $set_dialog_mode("")
      pause 0.25
      window auto
      "Hmm... [flora] sounded a bit mad about that..."
      $mc.add_message_to_history("flora", flora, maxine.contact_number)
      $mc.add_phone_contact("maxine")
      play sound "<from 0.25 to 1.0>phone_vibrate" volume 0.66
      "But at least she texted me [maxine]'s number."
    "?mc.charisma>=3@[mc.charisma]/3|{image=stats cha}|\"I'm interviewing her.\"":
      $set_dialog_mode("phone_call_centered","flora")
      mc "I'm interviewing her."
      flora "About what?"
      mc "The, err... the plant\nin the admin wing?"
      play sound "walkie_talkie2" loop
      show phone_static_screen onlayer screens zorder 100:
        xpos 732 ypos 139
      pause 1.0
      stop sound
      hide phone_static_screen onlayer screens
      mc "[flora]?"
      mc "Are you there?"
      play sound "walkie_talkie2" loop
      show phone_static_screen onlayer screens zorder 100:
        xpos 732 ypos 139
      pause 0.5
      $set_dialog_mode("phone_call_centered","maxine")
      pause 0.5
      stop sound
      hide phone_static_screen onlayer screens
      $mc.add_phone_contact("maxine")
      maxine "[mc]."
      mc "What the fuck? [maxine]?"
      maxine "Indeed. And you've\nbroken confidentiality."
      maxine "The specimen in question\nis not to be discussed with\nanyone on an unsafe line."
      mc "Err... right.\nSorry about that."
      maxine "You've been pardoned\nthis time."
      play sound "end_call"
      $set_dialog_mode("")
      pause 0.25
      window auto
      "That was so weird..."
      "How did [maxine] hijack the phone call like that?"
      "..."
      "Shit, I forgot to ask her to throw down the rope..."
      "I guess I could just call her number back, though."
  $quest.kate_wicked["maxine_number"] = True
  return

label quest_kate_wicked_costume_exterior_maxine:
  $set_dialog_mode("phone_call_plus_textbox","maxine")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","maxine")
  maxine "You have reached the\nAssociation of Smooth\nStones."
  maxine "Please, leave a message\nafter the beep."
  maxine "Beep!"
  mc "[maxine], I know you're\nworking late."
  mc "Can you throw down\nyour rope for me?"
  maxine "How does this request\nconcern ASS?"
  mc "I'm not looking for ASS!"
  maxine "Are you sure?"
  maxine "Most young men are at least\na little bit interested in ASS."
  mc "Well, I'm interested\nin a party."
  maxine "That does not necessarily\nexclude ASS."
  maxine "In fact, ASS is a big part\nof many parties."
  mc "Ugh..."
  mc "Can you please just\nthrow down the rope?"
  maxine "That depends. Would you\nlike to enter ASS?"
  maxine "Our membership fees—"
  $set_dialog_mode("phone_call","maxine")
  menu(side="middle"):
    extend ""
    "\"Enough! I'm not interested\nin fucking ASS, okay?\"":
      $set_dialog_mode("phone_call_centered","maxine")
      mc "Enough! I'm not interested\nin fucking ASS, okay?"
      maxine "Is there something\nwrong with it?"
      maxine "I work very hard to keep it\nopen and welcoming."
      mc "Well, maybe it's too open?"
      maxine "..."
      maxine "I didn't think of that."
      maxine "Thank you! ASS will be\nless open from now on."
      mc "You're welcome, I guess..."
      maxine "Is there any way our humble\norganization can compensate\nyou for your insights?"
      mc "Sure, you can throw\nyour rope down."
      maxine "ASS doesn't deal in ropes, only\nin stones of the smooth kind."
      mc "God damn it, [maxine]!"
      mc "How about you shove these\nstones up your ass, then?"
      maxine "Certainly! How many?"
      mc "I don't know.\nAt least a dozen."
      maxine "I'm afraid I don't have\nthat many on hand..."
      $set_dialog_mode("phone_call","maxine")
      menu(side="middle"):
        extend ""
        "?mc.owned_item('rock')@|{image=items rock}|\"Well, I just happen\nto have one for you.\"":
          $set_dialog_mode("phone_call_centered","maxine")
          mc "Well, I just happen to\nhave one for you."
          mc "You'll have to come down\nand get it, though."
          maxine "Very well."
          play sound "end_call"
          $set_dialog_mode("")
          pause 0.75
          $school_entrance["rope"] = True
          pause 0.75
          show maxine skeptical with Dissolve(.5)
          window auto
          maxine skeptical "Hand over the stone."
          mc "Here you go..."
          $mc.remove_item("rock")
          $del school_first_hall.flags["trash_bin_interact"]
          mc "But I want to see you put it in."
          maxine concerned "That's against ASS policy."
          mc "Fine. I guess you can do it in the bush, then."
          maxine excited "Be right back."
          window hide
          show maxine excited at disappear_to_left
          pause 0.5
          window auto
          "All right, here's my chance!"
        "\"Ugh, forget it...\"":
          $set_dialog_mode("phone_call_centered","maxine")
          mc "Ugh, forget it..."
          mc "What are the perks\nof this ASS, anyway?"
          jump quest_kate_wicked_costume_exterior_perks
    "\"Forget about membership\nfees. What about the perks?\"":
      $set_dialog_mode("phone_call_centered","maxine")
      mc "Forget about membership\nfees. What about the perks?"
      label quest_kate_wicked_costume_exterior_perks:
      maxine "There are no perks."
      mc "What?"
      maxine "If perks is what you're after,\nperhaps I can interest you\nwith a membership in our\nsister organization, the Trust\nof Inverted Touchstones?"
      mc "You've got to be\nkidding me..."
      maxine "I never joke about perks."
      mc "Fine... sign me up."
      mc "But you better throw\ndown the rope after!"
      maxine "For TITs or ASS?"
      $set_dialog_mode("phone_call","maxine")
      menu(side="middle"):
        extend ""
        "\"TITs, of course. ASS seems full of shit.\"":
          $set_dialog_mode("phone_call_centered","maxine")
          mc "TITs, of course.\nASS seems full of shit."
          maxine "Very well! That will be\na thousand dollars."
          mc "A thousand?!"
          maxine "That's right."
          $set_dialog_mode("phone_call","maxine")
          menu(side="middle"):
            extend ""
            "?mc.money>=1000@[mc.money]/1000|{image=ui hud icon_money}|\"That's kinda steep, isn't it?\"{space=-25}":
              $set_dialog_mode("phone_call_centered","maxine")
              mc "That's kinda steep, isn't it?"
              $mc.money-=1000
              maxine "It is. It has also already been\nextracted from your bank\naccount."
              mc "But I didn't agree to—"
              maxine "Sorry, too late.\nThe money is gone."
              maxine "The membership certificate\nwill be mailed to your home\naddress."
              mc "What? How?"
              maxine "Through the Newfall\npostal service, of course!"
              maxine "It's been a thing for\nover two centuries."
              mc "..."
              mc "Can you please throw\ndown the rope now?"
              maxine "Before I do, allow me\nto welcome you to our\norganization."
              maxine "TITs has offices all over\nthe globe. Small ones,\nbig ones — we come in\nall sizes."
              maxine "As a member, you have to\nchoose between a bouncy\nor flat rate of returns."
              $set_dialog_mode("phone_call","maxine")
              menu(side="middle"):
                extend ""
                "\"Bouncy.\"":
                  $set_dialog_mode("phone_call_centered","maxine")
                  mc "Bouncy."
                  maxine "Our juiciest plan!"
                  maxine "A very fine choice, sir."
                  mc "Uh, thanks?"
                  maxine "We will reach out if\nanything changes."
                  $quest.kate_wicked["cheques"] = "bouncy"
                "\"Flat.\"":
                  $set_dialog_mode("phone_call_centered","maxine")
                  mc "Flat."
                  maxine "A curious choice!"
                  mc "I know what I like."
                  maxine "That certainly seems\nto be the case."
                  $quest.kate_wicked["cheques"] = "flat"
                "\"What about medium?\nI like medium.\"":
                  $set_dialog_mode("phone_call_centered","maxine")
                  mc "What about medium?\nI like medium."
                  mc "A size that fits my\nhands, you know?"
                  maxine "We do have that!"
                  maxine "Do you want a medium rate\nwith an increase or decrease?"
                  mc "Err... I don't know..."
                  mc "Increase, maybe?"
                  maxine "A wise choice!"
                  maxine "The last member that picked\na decreasing rate went bankrupt.\nVery unfortunate."
                  mc "Uh, I'm really not good\nwith finances..."
                  maxine "That's fine.\nWe got you covered!"
                  $quest.kate_wicked["cheques"] = "medium"
              maxine "Thank you for choosing TITs,\nand have a good day!"
              play sound "end_call"
              $set_dialog_mode("")
              pause 0.75
              $home_kitchen["cheque_taken_today"] = True
              $school_entrance["rope"] = True
              pause 0.5
              window auto
              "I have no idea what just happened... but at least [maxine] threw down the rope."
            "\"On second thought, I'll just go with ASS...\"{space=-10}":
              $set_dialog_mode("phone_call_centered","maxine")
              mc "On second thought,\nI'll just go with ASS..."
              jump quest_kate_wicked_costume_exterior_ASS
        "\"ASS, of course. TITs seems like\nthe breast way to go bankrupt.\"":
          $set_dialog_mode("phone_call_centered","maxine")
          mc "ASS, of course. TITs seems like\nthe breast way to go bankrupt."
          label quest_kate_wicked_costume_exterior_ASS:
          maxine "Ah, a man of culture!"
          mc "So, what's the price to join?"
          maxine "Price? ASS has been priceless\nfor the last several generations."
          mc "Sign me up,\nthen, I guess."
          maxine "You guess?"
          maxine "You don't seem\nvery enthusiastic..."
          maxine "Most of our clients are\nthrilled to enter ASS."
          mc "I guess I'm not like\nmost of your clients."
          maxine "That's fine! We appreciate\nclients of every size and\nshape in ASS."
          mc "I'm sure you do..."
          mc "Can you please throw\ndown the rope now?"
          maxine "I most certainly can."
          maxine "Thank you for choosing ASS,\nand have a good day!"
          play sound "end_call"
          $set_dialog_mode("")
          pause 0.75
          $home_kitchen["stones"] = home_kitchen["stone_interacted_today"] = 1
          $school_entrance["rope"] = True
          pause 0.5
          window auto
          "I have no idea what I've signed up for... but at least [maxine] tossed the rope down."
  return

label quest_kate_wicked_costume_exterior_rope:
  if not quest.kate_wicked["work_so_hard"]:
    "I never had to work so hard in my life to get into the school..."
    "Usually, I work to get out."
    $quest.kate_wicked["work_so_hard"] = True
  jump goto_school_homeroom

label quest_kate_wicked_costume_exterior_door:
  "The [guard] is watching the door like a cross-eyed hawk."
  "And unfortunately, that's enough to keep me out."
  return

label quest_kate_wicked_costume_homeroom:
  "..."
  "Everything feels so different after dark..."
  "Safer, somehow."
  "Although, I've always been nocturnal."
  $quest.kate_wicked["safer_somehow"] = True
  return

label quest_kate_wicked_costume_homeroom_door:
  "The [guard] is still out there patrolling. I'll have to be quick."
  $quest.kate_wicked["still_patrolling"] = True
  jump goto_school_ground_floor

label quest_kate_wicked_costume_entrance_hall_exit:
  "Playing it safe would be to leave now..."
  if game.location == "school_entrance":
    "The risky thing would be to sneak into the school, find a bedsheet, evade the [guard], and go to [kate]'s party."
  elif quest.kate_wicked == "party":
    "The risky thing would be to evade the [guard] and go to [kate]'s party."
  else:
    "The risky thing would be to find a bedsheet, evade the [guard], and go to [kate]'s party."
  menu(side="middle"):
    extend ""
    "Stay":
      "Pussies are like magnets — you'll only repel them if you are one yourself."
    "Leave":
      "What's even the point in going to [kate]'s party? It's just full of dumb normies."
      "I'll just pick up a pizza and play some xCube with [flora] instead."
      window hide
      $game.quest_guide = ""
      $game.notify_modal("quest", "Quest complete", "Wicked Game{hr}Missed something, but\nit's probably nothing.",5.0)
      pause 0.25
      show black onlayer screens zorder 100 with Dissolve(0.5)
      stop music
      $game.advance()
      $game.location = "home_bedroom"
      pause 2.0
      hide black onlayer screens with Dissolve(.5)
      if "event_player_force_go_home_at_night" in game.events_queue:
        $game.events_queue.remove("event_player_force_go_home_at_night")
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      play music "home_theme" fadein 0.5
      window auto
      $quest.kate_wicked.finish("flaccid_done", silent=True)
  return

label quest_kate_wicked_costume_admin_wing:
  "What the hell is going on here?"
  $quest.kate_wicked["going_on_here"] = True
  return

label quest_kate_wicked_costume_admin_wing_nurse:
  show nurse neutral with Dissolve(.5)
  window auto show
  show nurse afraid with dissolve2
  nurse afraid "Oh! [mc]! I didn't see you there!" with vpunch
  nurse annoyed "What are you doing here after dark?"
  show nurse annoyed at move_to(.75)
  $blackmail = mc.owned_item(("damning_document","compromising_photo")) or "compromising_photo"
  menu(side="left"):
    extend ""
    "?mc.owned_item(('damning_document','compromising_photo'))@|{image=items [blackmail]}|\"I'm asking the questions here.\"":
      show nurse annoyed at move_to(.5)
      mc "I'm asking the questions here."
      nurse concerned "..."
      mc "What are {i}you{/} doing here after dark?"
      nurse concerned "Nothing, just... finishing up... some things..."
      mc "I can tell that you're not being honest with me."
      nurse sad "Sorry..."
      if nurse["strike_book_activated"]:
        mc "That's another strike for you. Write it down."
        $nurse.lust+=2
        nurse concerned "Oh... okay."
        $nurse["strike_book"]+=1
      if quest.kate_desire["stakes"] == "release":
        mc "Does this have something to do with [kate]?"
        nurse annoyed "..."
        mc "I see."
        nurse annoyed "She's... paying me..."
        mc "She's paying you?"
        nurse afraid "Yeah! It's voluntary!"
        mc "What is?"
        nurse annoyed "I'm... going to be the entertainment at her party..."
        mc "That doesn't sound good."
        nurse afraid "It's fine! I promise!"
        mc "All right..."
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"Astronomy class.\"":
      show nurse annoyed at move_to(.5)
      mc "Astronomy class."
      nurse thinking "I didn't know we had that at the school."
      mc "It's, err... new for this year?"
      nurse blush "That's great! I've always been interested in the stars."
      nurse blush "They're so pretty, and whenever I look up at the night sky, small things don't seem to matter as much."
      mc "Huh... I've often felt the same way."
      mc "No matter what life throws at me, the stars remain the same."
      mc "Maybe some other time we could go stargazing together?"
      $nurse.love+=2
      nurse blush "I would like that very much."
      mc "It's a date!"
      nurse concerned "Oh, I don't know. I don't think that's a good idea."
      mc "Why not?"
      if quest.kate_desire["stakes"] == "release":
        if nurse.love >= 3:
          nurse sad "I haven't been on a date in so long... I'm sure I would just embarrass myself."
          mc "It'd be fine. I'd take good care of you."
          nurse blush "You're quite confident for your age, I will give you that."
          mc "So, it's a date?"
          nurse blush "If you insist, I suppose so."
          mc "I do... but I'll have to get back to you on that."
          nurse blush "No rush. I've heard there's a night every night."
          mc "Exactly!"
        else:
          nurse sad "I don't think it would be appropriate."
          nurse sad "You're a student... I'm way older..."
          mc "I don't care about those things."
          $nurse.love+=1
          nurse blush "That's very nice of you, [mc]."
          nurse blush "I'll think about it."
          mc "Okay, sounds good!"
      else:
        nurse annoyed "It's complicated. I don't think it would be... approved."
        mc "By whom?"
        nurse annoyed "I shouldn't say."
        mc "By [kate]?"
        nurse afraid "..."
        mc "You shouldn't let her control your life, you know?"
        nurse concerned "I... I..."
        nurse sad "Let's change the topic, please."
        mc "Okay, then."
    "\"I, err... forgot about the time.\"":
      show nurse annoyed at move_to(.5)
      mc "I, err... forgot about the time."
      nurse sad "The time? My goodness. Are you okay?"
      mc "Yeah, I just fell asleep, that's all."
      nurse concerned "Are you getting enough sleep, [mc]?"
      nurse concerned "You always look a bit tired."
      mc "Sure, just not last night..."
      "...or the night before that..."
      nurse concerned "Okay. Make sure you get enough sleep tonight, all right?"
      nurse thinking "You better get going before the [guard] sees you."
      mc "I probably should, yeah."
  mc "So, what's in the box?"
  nurse annoyed "Nothing..."
  mc "Nothing?"
  mc "What's it for?"
  nurse annoyed "It's... for [kate]'s party."
  if quest.kate_desire["stakes"] == "release":
    mc "I'm liking this less and less."
    mc "You know you don't have to deal with her anymore, right?"
    nurse neutral "I know, but..."
  mc "So, what exactly is the box for? Is that your costume?"
  nurse neutral "I guess you could say so."
  "Hmm... maybe I could borrow her costume? That would get me into the party unnoticed..."
  show nurse neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.lust>=4@[mc.lust]/4|{image=stats lust}|\"Would you mind if I\nborrow your costume?\"":
      show nurse neutral at move_to(.5)
      mc "Would you mind if I borrow your costume?"
      nurse concerned "I... don't know..."
      mc "Oh, come on! I need to get into the party!"
      nurse thinking "Sure, but..."
      mc "No buts! You can take the night off."
      nurse concerned "Are you sure?"
      mc "Yes, and keep the money too. I just need a ticket into the party and this is perfect."
      nurse thinking "I don't think—"
      kate "Come on, girls! This way!"
      mc "Get out of the way! Hurry!"
      nurse concerned "Err... okay."
      show nurse concerned at disappear_to_left
      "All right, let's try this bad boy out!"
      window hide
      show black onlayer screens zorder 100
      $mc["focus"] = ""
      with Dissolve(.5)
      pause 1.0
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      lacey "Oh, my god! What is this?"
      kate "It's a mystery box."
      casey "What does it do?"
      kate "All in good time, ladies."
      kate "[stacy], did you bring the lock?"
      stacy "Yeah, of course."
      "Uh-oh..."
      play audio "lock_click"
      pause 0.5
      lacey "This is so exciting! I love mysteries!"
      casey "Yesterday you said you hated Scooby Doo..."
      lacey "Is that a mystery?"
      stacy "Babe..."
      lacey "What? I thought it was a dog show! I hate dog shows."
      kate "Let's just get this show on the road."
      kate "Help me carry it upstairs, will you?"
      "Oh, shit. Here we go."
      lacey "It's so heavy! Why is it so heavy?"
      "Gee, thanks..."
      $game.location = "school_ground_floor"
      casey "Oh, quit whining. It's not that bad."
      lacey "It's because you're barely helping!"
      kate "Ladies! Focus! Let's get it up the stairs."
      stacy "Oof! I think [lacey] is right. This is really heavy..."
      $game.location = "school_first_hall"
      lacey "Couldn't we have asked Tanner to carry it?"
      casey "Or that loser, what's his name?"
      stacy "Oh, that could have been funny! I don't mind cracking the whip\nat that one."
      $game.location = "school_first_hall_west"
      casey "Yeah, losers should be put to work!"
      lacey "Queen!"
      "Damn, I can't believe they don't even know my name."
      "We've been in the same class for the last three years."
      $quest.kate_wicked.advance("party")
      $game.location = "school_music_class"
      kate "Okay, here we are!"
      "So far, the plan seems to be working..."
      kate "Everyone! Welcome to my yearly ladies-only costume party!{nw}"
      play sound "cheers_and_clapping"
      extend ""
      kate "Apart from booze, music, and the hottest gossip... this year we also have a mystery box!"
      kate "Are you bold enough to experience the mysteries that it holds? We shall see!"
      kate "Start the music!{nw}"
      play music "<to 16.0>party_music"
      extend ""
      "Hmm... I hope I can sneak a peek through one of the holes at\nsome point..."
      "[kate]'s parties are known to be wild."
      "I can't believe I got in, to be honest. It's always been on my\nbucket list."
      "..."
      "I wonder what the idea with this box is, though?"
      kate "Hey, [lacey]! Want to be the first to try out the mystery box?"
      "Uh-oh..."
      kate "Where did [lacey] go?"
      stacy "I don't know, probably face-sucking a dude somewhere."
      kate "This is a ladies-only party, remember?"
      stacy "Okay, probably face-sucking a girl somewhere, then."
      kate "Touché!"
      kate "And she says she's not bi."
      stacy "She says a lot of things."
      kate "So, do you want to try out the mystery box?"
      stacy "If you explain it."
      kate "What's there to explain? Stick your leg in, see what happens!"
      stacy "Spicy! Okay."
      "Oh, shit! Here it comes..."
      show black onlayer screens zorder 100
      show minigames simple_stepping background as minigame_background
      show minigames simple_stepping foreground as minigame_foreground:
        xpos 0 ypos 0
      pause 0.5
      $game.ui.hide_hud = True
      hide black onlayer screens
      hide screen interface_hider
      with Dissolve(.5)
      pause 0.25
      $renpy.transition(Dissolve(0.25))
      call start_simple_stepping_minigame(feet=["foot_stacy"], skip_instructions=False)
      show minigames simple_stepping background as minigame_background
      window auto
      $set_dialog_mode("")
      stacy "Oh, my god! That's so good!"
      stacy "I... I think I've unlocked a new kink..."
      kate "I told you it was good!"
      stacy "Where do I find someone to do this on the daily?"
      kate "I might just have an idea."
      lacey "My turn! My turn!"
      kate "Okay, take your shoes off."
      window hide
      call start_simple_stepping_minigame(feet=["foot_lacey"])
      show minigames simple_stepping background as minigame_background
      window auto
      lacey "God, yes! But I'll never kiss you on the mouth after this, you\ndirty slut."
      kate "Haha! How'd you like that, [lacey]?"
      lacey "I loved it!"
      lacey "She does need more practice, though."
      kate "I'm sure we can train her to do it right..."
      kate "Ladies! Any volunteers?"
      window hide
      call start_simple_stepping_minigame(feet=["foot_center"])
      show minigames simple_stepping background as minigame_background
      window auto
      $maya.default_name = "Girl"
      maya "Mmmhmm! Just like that!"
      $maya.default_name = "Maya"
      window hide
      call start_simple_stepping_minigame(feet=["foot_casey"])
      show minigames simple_stepping background as minigame_background
      window auto
      $maya.default_name = "Girl"
      maya "Does it taste good? I didn't shower today."
      $maya.default_name = "Maya"
      window hide
      call start_simple_stepping_minigame(feet=["foot_right"])
      show minigames simple_stepping background as minigame_background
      window auto
      $maya.default_name = "Girl"
      maya "Yes! Lap that up like it's your last meal!"
      $maya.default_name = "Maya"
      kate "I hope you're enjoying this, slut."
      kate "We'll keep going until you're able to satisfy all my guests."
      lacey "Queen!"
      lindsey "Hey, guys!"
      kate "[lindsey]! Glad you could make it!"
      lindsey "What's going on here?"
      kate "We're just playing with the mystery box."
      lindsey "What's inside it?"
      kate "That's a secret..."
      lindsey "Hehe! I guess it wouldn't be a mystery box otherwise..."
      kate "Exactly!"
      lindsey "So, how does it work?"
      kate "You take of your shoes and stick your leg in."
      lindsey "Oh, um... I don't know about that..."
      kate "It'll be fun! Trust me, it's like a pedicure."
      kate "Considering how important your feet are to you, you deserve\nsome pampering!"
      lindsey "Err, okay..."
      window hide
      $lindsey["outfit_stamp"] = lindsey.outfit
      $lindsey.outfit = {"bra": None, "panties": None, "pants": None, "shirt": "lindsey_mummy_costume"}
      show minigames simple_stepping feet lindsey_idle as minigame_lindsey behind minigame_foreground:
        xoffset 1200 yoffset -50 zoom 0.75
        parallel:
          easeout 2.0 zoom 1.0
        parallel:
          easein_circ 2.5 xoffset 818
        parallel:
          easein_expo 2.5 yoffset 402
      pause 2.5
      window auto
      "Uh-oh. This might not turn out well..."
      hide minigame_lindsey
      show minigames simple_stepping feet lindsey_idle as minigame_lindsey behind minigame_foreground:
        xpos 818 ypos 401
      if lindsey["romance_disabled"]:
        "Maybe I'll cut her foot off and ruin her dreams?"
        "That would be fitting."
        "But on the other hand, I could keep this over her head..."
        "Watching [lindsey] go down slowly is a better punishment."
      else:
        "It's one thing to worship the feet of those bitches, but [lindsey]\nis too pure for this."
        "It'd feel like taking advantage of her innocence. I'd feel like a creep."
        "On the other hand, it might be my only chance to touch her beautiful feet..."
        menu(side="middle"):
          extend ""
          "Worship":
            "God, she smells amazing... I've been waiting for this moment."
            "I'll make her love my mouth so much, she'll remember it forever."
          "Refuse":
            "I'd feel weird keeping this a secret from [lindsey]."
            $mc.love+=5
            $lindsey.love+=2
            "If I do something like this with her, I want it to be consensual."
            lindsey "Um... is anything supposed to happen?"
            kate "Yeah, don't you feel anything?"
            lindsey "Not really..."
            kate "Okay, let me handle this."
            "[kate] leans in closer, her voice but a whisper."
            kate "{i}Bitch, do your job.{/}"
            mc "..."
            kate "{i}I swear to god, you won't be able to walk for a month.{/}"
            mc "..."
            lindsey "Still nothing..."
            kate "Sorry, sweetie. We'll try again later, okay?"
            lindsey "All right! No worries!"
            window hide
            hide minigame_lindsey
            show minigames simple_stepping feet lindsey_curled as minigame_lindsey behind minigame_foreground:
              xoffset 818
              yoffset 402
              parallel:
                easeout 1.0 xoffset 1200
              parallel:
                easein 1.0 yoffset -500
            pause 0.5
            window auto
            "Their steps fade away, and the beat of the music takes over."
            "The taste of their feet still linger on my lips."
            "So very humiliating, and a night I'll never forget."
            jump quest_kate_wicked_ending
      "As I lean forward, mere inches from her wiggling toes, the scent hits me in full."
      "Cotton candy plus a light hint of feminine sweat and shoe leather — an intoxicating combination."
      "My breath tickles her skin and she jumps in surprise."
      lindsey "W-what's in there?!"
      kate "It's okay! Just enjoy."
      lindsey "But..."
      extend " eeek!" with vpunch
      window hide
      play sound "falling_thud"
      show black onlayer screens zorder 100 with vpunch
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "My tongue makes contact with her toes for the first time, and she almost pulls out her legs."
      "The kick hits me in the nose, and I suddenly see stars."
      if lindsey["romance_disabled"]:
        "Fucking bitch..."
      else:
        "But it's fine, she's skittish. I can't blame her."
      show black onlayer screens zorder 100
      pause 0.25
      hide black onlayer screens with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      kate "Calm down, [lindsey]! It's a foot bath!"
      lindsey "B-but I felt a tongue or something..."
      kate "Yes, it's a tongue-foot bath."
      lindsey "What?!"
      lindsey "Who is in there?!"
      kate "Don't worry about it! They want to do it."
      lindsey "Are... are you sure?"
      kate "I'm positive."
      lindsey "Um... whoever is in there... are you okay with this?"
      "I drag my tongue across her sole, from heel to toe, to show her\nthat I'm down."
      lindsey "Ooooh!"
      lindsey "I... I don't know if I like this..."
      kate "It'll grow on you."
      "The sole of her foot is coarse against my tongue."
      "It's a well-used sole, a runner girl's sole."
      if not lindsey["romance_disabled"]:
        "[lindsey] is a princess, but she's also an athlete."
        "And her feet and legs are where it shows the most."
      "Slowly, taking my time, I slip my tongue between her toes."
      "She squeals in surprise and delight."
      if lindsey["romance_disabled"]:
        "Always so fake in the way she acts..."
        "I almost feel an urge to bite her."
        "Instead, I nibble on her toes, and again she acts scandalized."
        "As if all girls aren't born whores."
      else:
        "When it comes to her, it doesn't feel like an act of submission\nto me."
        "She's so inexperienced, so pure... It almost feels wrong to do\nthis to her."
        "I nibble on her toes, and she makes the cutest noises."
      "As I start to kiss my way up the arch of her foot, her squirming abates."
      show minigames simple_stepping feet lindsey_curled as minigame_lindsey with dissolve2
      "A small moan escapes her lips, and she spreads her toes."
      "This invitation is all I need to put my tongue deeper between her toes, and apply some suction."
      "A shiver prickles the skin of her leg."
      "Her breathing increases."
      lindsey "Oh, my goodness..."
      "I can't see [lindsey] face, but she's clearly blushing at the sensation."
      "The taste of her foot fills me with hunger."
      "I need to have her. All of her."
      "But I'm locked inside this box..."
      "Instead, my lips increase their ravenous pace."
      "My hand strokes my cock."
      window hide
      hide minigame_lindsey
      show minigames simple_stepping feet lindsey_curled as minigame_lindsey behind minigame_foreground:
        xoffset 818
        yoffset 402
        parallel:
          easeout 1.0 xoffset 1200
        parallel:
          easein 1.0 yoffset -500
      pause 0.5
      window auto
      "And just as I'm about to come... she pulls her foot out."
      "I'm left licking my lips, her taste still thick in my mouth."
      if lindsey["romance_disabled"]:
        "Fucking tease! I knew it..."
        "Never a good time with her."
      else:
        "Damn it! Perhaps I went too hard or too fast on her?"
        "In the future, I need to be more gentle with [lindsey] if I want to\nhave a chance..."
      "Anyway..."
      "Still hard as a rock, I give my cock a few more strokes and cum\nall over the floor of the box."
      "I suppose things could've gone much worse."
      $unlock_replay("lindsey_foot_bath")
      jump quest_kate_wicked_ending
    "\"That's a weird costume.\"":
      show nurse neutral at move_to(.5)
      mc "That's a weird costume."
      nurse smile "I suppose it's a bit unusual."
      mc "So, you're going to be inside it?"
      nurse annoyed "Yes, that's the plan..."
      "Being locked into a box at the mercy of [kate]? This doesn't seem like a good idea."
      "But maybe that's the whole point..."
      kate "Come on, girls! This way!"
      show nurse afraid with dissolve2
      "Oh, shit! They're coming!"
      nurse afraid "Hide!"
      mc "Where?"
      nurse afraid "I'll unlock the door to my office!"
      play audio "lock_click"
      nurse afraid "Go!"
      window hide
      $school_nurse_room["curtain_off"] = True
      call goto_school_nurse_room
      pause 0.25
      window auto
      "Phew! That was close..."
      kate "What is this?"
      nurse "Err..."
      kate "Why aren't you in the box?"
      nurse "Sorry..."
      kate "You ruined the mystery!"
      nurse "Sorry..."
      stacy "Is she always this slow?"
      lacey "She is! Tanner once told me to bring him a bottle of elbow grease, and she took like five minutes to find it!"
      casey "Babe..."
      lacey "What?"
      casey "Nevermind."
      kate "Strip and get in the box right now."
      nurse "Yes, Miss [kate]."
      kate "You've ruined the mystery for these three, but we can still keep\nit up for the rest of the party."
      kate "I don't want to hear a sound from you for the rest of the night.\nIs that understood?"
      nurse "..."
      kate "Good girl."
      kate "[stacy], did you bring the lock?"
      stacy "Of course."
      kate "Okay, lock her in and we'll get this show on the road."
      lacey "Queen!"
      window hide
      play audio "lock_click" volume 0.33
      pause 0.5
      window auto
      casey "Oof, she's heavy..."
      kate "Maybe I need to put her on a diet."
      lacey "Couldn't we have brought Chad to help carry it?"
      kate "[lacey]-baby, you can't rely on men to do everything for you."
      lacey "Seems like a good use for them..."
      casey "Come on, [lacey], help us!"
      lacey "Fine..."
      "..."
      "..."
      "Okay, seems like they left."
      "I better get my own costume in order. Don't want to be late to\nthe party."
      $quest.kate_wicked["own_costume"] = True
  return

label quest_kate_wicked_costume_nurse_room_bed:
  "These bed sheets will do nicely, I just need to find a way to make eye-holes..."
  "Surely, the [nurse]'s office has a pair of scissors or something?"
  "..."
  "There we go. This should do the trick."
  $mc.add_item("ghost_sheet")
  "Aha! Excellent!"
  "Although... they will know it's me as soon as I talk..."
  "Hmm... maybe I can use a text to speech thing on my phone?"
  "Let's see here..."
  "Oh, this ghostly Halloween voice will do nicely!"
  "I'll just type something down and..."
  "..."
  mc "{👻}This party is about to take a dark turn...{/👻}"
  "Perfect! Okay, it's party time."
  $quest.kate_wicked.advance("party")
  return

label quest_kate_wicked_party_music:
  if game.location == "school_first_hall_west":
    play music "<to 16.0>muffled_party_music" volume 0.33
  else:
    play music "school_theme"
  return

label quest_kate_wicked_party:
  "Loud music is coming from inside... I guess the party has already started."
  "I better put on my costume before knocking."
  window hide
  $quest.kate_wicked["ghost_costume"] = True
  $renpy.transition(Dissolve(0.5))
  $mc.remove_item("ghost_sheet")
  pause 0.25
  window auto
  "Okay, here goes nothing..."
  window hide
  play sound "<from 1.7 to 2.7>door_knock"
  pause 1.5
  window auto
  "..."
  "They probably can't hear me over the—"
  window hide
  show kate surprised at appear_from_left
  pause 0.5
  window auto
  kate surprised "Huh?"
  "Oh, crap."
  kate surprised "Chloe? Is that you?"
  mc "{👻}I am the Ghost of Christmas Sins!{/👻}"
  kate gushing "That's awesome!"
  kate gushing "Tell me, then. What are my sins?"
  "Well, that should be easy enough..."
  show kate gushing at move_to(.25)
  menu(side="right"):
    extend ""
    "?quest.isabelle_red['sneaked_a_peek']@|{image=kate_wicked_eating_out}|\"Your lust\nfor revenge...\"":
      show kate gushing at move_to(.5)
      mc "{👻}Your lust for revenge...{/👻}"
      mc "{👻}...is what will eventually be your downfall.{/👻}"
      mc "{👻}You dream about it, even when you're awake.{/👻}"
      mc "{👻}It consumes you, and those around you.{/👻}"
      kate thinking "I don't think that's true..."
      mc "{👻}You make your friends worship at your altar.{/👻}"
      mc "{👻}With no regard for their own wishes.{/👻}"
      mc "{👻}Mere substitutes for your revenge fantasy.{/👻}"
      kate embarrassed "Who told you about that?"
      mc "{👻}You did.{/👻}"
      kate embarrassed "I know I didn't tell you, and I know for a fact that [lacey] didn't either.{space=-5}"
      mc "{👻}And yet, I know it all the same.{/👻}"
      kate blush "Huh... that's a neat trick you've got there."
      kate blush "If you have more of those dirty little secrets, then you'll be a fun addition to the party."
      mc "{👻👻}I am the Ghost of Christmas Sins, and I know\nthem all.{/👻👻}"
      kate blush "Well, then. Come on in!"
      $mc.charisma+=4
      "Puh! Easy as that!"
      "I'm sweating... I really need to get faster at typing."
    "\"Your fears...\"":
      show kate gushing at move_to(.5)
      mc "{👻}Your fears...{/👻}"
      mc "{👻👻}...is what drives you. It's what keeps you at\nthe top of the world.{/👻👻}"
      mc "{👻👻}Fear of losing what you have. Fear of becoming\nsomething else.{/👻👻}"
      mc "{👻👻}Fear of being judged. Fear of stepping out of\nthe spotlight.{/👻👻}"
      mc "{👻👻}Fear of being vulnerable. Fear of opening\nyourself.{/👻👻}"
      mc "{👻}Fear of opening your heart.{/👻}"
      mc "{👻👻}It rules your every waking hour. It gives you\npower, but it doesn't give you happiness.{/👻👻}"
      mc "{👻}That's your biggest sin.{/👻}"
      kate embarrassed "..."
      "[kate] looks completely flustered."
      kate embarrassed "That's..."
      "I've never seen her blush like this before."
      kate embarrassed "That's not..."
      "I've never seen her speechless."
      if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_insight"]:
        $mc["moments_of_glory"]+=1
        $mc["moments_of_glory_insight"] = True
        $game.notify_modal(None,"Guts & Glory","Moments of Glory: Insight\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#       if mc["moments_of_glory"] == 3:
#         $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
      "Anyway, here's my chance to get into the party!"
    "?quest.kate_desire['bent_over']@|{image=kate_wicked_spanking}|\"Your pride...\"":
      show kate gushing at move_to(.5)
      mc "{👻}Your pride...{/👻}"
      mc "{👻}...has always been your biggest sin.{/👻}"
      mc "{👻}What you want is all within your reach...{/👻}"
      mc "{👻}You're the queen of your own realm.{/👻}"
      mc "{👻👻}You make the rules. Without pride, you have\nno shackles.{/👻👻}"
      kate thinking "Huh..."
      kate thinking "I think I know what you're saying..."
      kate blush "Why should I care what others think? I should just take what I want."
#     kate blush "It brings me joy to step on bugs — one little bug in particular."
#     kate blush "I shouldn't be associated with lower life forms... but that's just my pride speaking."
      kate blush "What's the harm in letting your darkest desires take the wheel?"
      kate blush "I think I'm going to let them run freely from now on."
      $kate.lust+=3
      kate blush "Perhaps it's time to take it to the next level..."
      kate blush "Thanks, ghost! Welcome to the party!"
      "Shit. I may have just unlocked [kate]'s next level."
      "Do I actually want that?"
      "Maybe I should have thought this through..."
      $quest.kate_wicked["next_level"] = True
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  play music "<to 16.0>party_music"
  $game.location = "school_music_class"
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_kate_wicked_party_music_class:
  pause 0.25
  "Wow! This is really something..."
  "No wonder [kate] is so popular."
  "All this time, I wondered what a party with the popular kids would be like..."
  "Cute girls moving their bodies to the music."
  "The alcohol flowing freely."
  "Fun. Games. Gossip."
  "Yeah, I imagined all that, but still... it's something else actually being here."
  "It's like... being in the know. Like a secret handshake or something."
  "To be able to say you were there."
  "Now I just have to keep up appearances..."
  "Hopefully, I'll be left alone, and—"
  window hide
  $lindsey["outfit_stamp"] = lindsey.outfit
  $lindsey.outfit = {"bra": None, "panties": None, "pants": None, "shirt": "lindsey_mummy_costume"}
  show lindsey thinking with Dissolve(.5)
  window auto show
  show lindsey afraid with dissolve2
  lindsey afraid "Eeep! A ghost!" with vpunch
  lindsey laughing "Just kidding!"
  lindsey smile "How are you, Mrs. Ghost?"
  "Holy crap. I didn't know [lindsey] would be here."
  if lindsey["romance_disabled"]:
    "But of course she is..."
    "She's part of the popular, beautiful caste."
    "Of course [kate] would invite her."
    "Because, let's be honest, what would the party be without another shallow airhead?"
    "What a stupid costume..."
  else:
    "I thought she only had time for sports and being cute..."
    "But she's a jock, and they probably get invited automatically."
    "That is one adorable mummy, though."
  show kate gushing at appear_from_left(.25)
  show lindsey smile at move_to(.75,1.0)
  kate gushing "[lindsey]! Glad you could make it!"
  kate gushing "I see you've found our special guest..."
  lindsey smile "Thanks! I was just saying hi."
  kate confident "This ghost has a cool party trick!"
  "Uh-oh..."
  lindsey laughing "I love party tricks!"
  kate confident "It's not just any trick, though."
  kate confident "The ghost will tell you your biggest sin."
  kate confident "Isn't that right, ghost?"
  mc "{👻}I am the Ghost of Christmas Sins.{/👻}"
  lindsey skeptical "Sins? I don't know about that."
  kate laughing "Oh, come on! How bad can it be?"
  kate laughing "I bet your biggest sin is being too good at sports or something!"
  lindsey laughing "Haha! Did you already ask what yours was?"
  kate smile "I did, yeah!"
  lindsey flirty "Oh, really? And what was your biggest sin?"
  kate blush "I'll tell you after we know yours."
  lindsey thinking "Oh, I don't know..."
  kate blush "Come on, quit being a baby!"
  lindsey blush "Fine! Tell me my sins, Mrs. Ghost."
  show kate blush at move_to("left")
  show lindsey blush at move_to("right")
  menu(side="middle"):
    extend ""
    "\"That heart of yours is pure as snow...\"":
      show kate blush at move_to(.25)
      show lindsey blush at move_to(.75)
      mc "{👻}That heart of yours is pure as snow...{/👻}"
      mc "{👻👻}...and your biggest sin... is that you have no\nsins.{/👻👻}"
      mc "{👻👻}Locked in your tower, you watch the world from\nthe window.{/👻👻}"
      mc "{👻👻}You are fast, but you can't outrun the sands of\nthe hourglass.{/👻👻}"
      lindsey cringe "That's a bit depressing..."
      kate laughing "Don't take it to heart, sweetie."
      lindsey thinking "What do you mean?"
      kate smile_right "Oh, come on! You'll be a star."
      kate smile_right "Once you've won everything there is to win, you'll have time to explore the world."
      kate smile_right "This ghost clearly doesn't know anything about athletics and peak years."
      "Ugh, why does she have to be so encouraging, and so... right?"
      lindsey laughing "You're right, [kate]! Eyes on the prize!"
      kate gushing "Exactly!"
      kate gushing "With the exception of tonight, of course. Tonight, you let loose!"
      lindsey flirty "Yeah, yeah! Thanks again for inviting me!"
      kate gushing "Oh, you know you're on my auto-invite-list, babe!"
      "God, why do they have to get along so well?!"
      kate gushing "Come on, let's go grab a drink!"
      window hide
      show lindsey flirty at disappear_to_left(1.25)
      pause 0.25
      show kate gushing at disappear_to_left
      pause 0.75
      window auto
      "Alone again. There's so many people here, but still I'm alone."
      "That's how it always goes..."
      "..."
      "Maybe it's time to check out the mystery box? It seems to be popular."
    "?lindsey['romance_disabled']@|{image=lindsey contact_icon_evil}|\"Your mind...\"":
      show kate blush at move_to(.25)
      show lindsey blush at move_to(.75)
      mc "{👻}Your mind...{/👻}"
      mc "{👻}...is a sieve.{/👻}"
      mc "{👻}It's as empty as a barren field.{/👻}"
      mc "{👻}Nothing grows. Nothing takes root.{/👻}"
      mc "{👻👻}All you are is on your sleeve. One look at you,\nall is seen.{/👻👻}"
      mc "{👻👻}Your lack of depth will be your end, and thus\nit is your biggest sin.{/👻👻}"
      lindsey skeptical "Is that supposed to be funny?"
      "The truth hurts, doesn't it?"
      $lindsey.hate+=3
      lindsey skeptical "That's not funny at all."
      window hide
      show lindsey skeptical at disappear_to_left
      pause 0.25
      show kate blush at move_to(.5,1.0)
      pause 0.25
      window auto show
      show kate laughing with dissolve2
      kate laughing "What's her deal? I thought that was funny and spot on!"
      kate flirty "I like your sense of humor, ghost!"
      kate flirty "You should check out our mystery box. I think you might like it..."
      window hide
      show kate flirty at disappear_to_left
      pause 0.5
      window auto
      "[kate] drifts over to the next conversation, as easy as that..."
      "It really is a marvel how confidently she can insert herself in any conversation."
      "How she can make people appreciate her presence with just a few words and a smile."
      "The prime example of a social butterfly. We couldn't be more different."
      "..."
      "Anyway, she's right. I should check out the mystery box."
    "\"Your sin is that you care too much...\"":
      show kate blush at move_to(.25)
      show lindsey blush at move_to(.75)
      mc "{👻}Your sin is that you care too much...{/👻}"
      mc "{👻}...about the wishes of others.{/👻}"
      mc "{👻👻}Wake up from eternal slumber. Throw your hair\ndown. Kiss the frog.{/👻👻}"
      mc "{👻👻}Your heart is yours, and only yours... to give\naway or bury.{/👻👻}"
      kate neutral "What a bunch of nonsense."
      lindsey flirty "I thought it was sweet!"
      kate confident_right "Come on, let's get wasted!"
      lindsey laughing "I'm not supposed to drink..."
      kate confident_right "But you'll make an exception tonight, right?"
      kate confident_right "Pretty please?"
      lindsey laughing "I mean, I guess..."
      kate laughing "Don't worry, I've got your back!"
      window hide
      show lindsey laughing at disappear_to_left(1.25)
      pause 0.25
      show kate laughing at disappear_to_left
      pause 0.75
      window auto
      "[kate] will most definitely not take care of [lindsey]."
      "She'll get her drunk and then ditch her..."
      "..."
      "Oh, well. I guess that's all the attention I'm getting tonight."
      "Unless I go check out the mystery box..."
  window hide
  show black onlayer screens zorder 100
  show nurse mystery_box_close_up
  with Dissolve(.5)
  pause 1.0
  $quest.kate_wicked["ghost_costume"] = False
  hide black onlayer screens with Dissolve(.5)
  window auto
  "The box beckons to me and I willingly sway towards it to claim\nmy reward."
  "A fortune sin well told, means my dick to hold..."
  show nurse mystery_box_reveal with dissolve2
  extend " by the [nurse]."
  window hide
  show black onlayer screens zorder 100
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/mystery_box/blowjob_tease.webm", side_mask=False) as nurse
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "I choose a hole, undo my pants, and slide my dick inside."
  "Her breath on my dick sends a spike of uncontainable lust into\nmy brain."
  "Is she teasing me or am I teasing her?"
  "The tip of my dick is greeted with a warm, wet tongue darting out. A quick taste of what's to come."
  "She plays with me, tongue across my sensitive skin."
  "But as I stand there with my pants undone and the high of the night coursing through me, I can't help but also be enticed by the other hole...{space=-80}"
  "I mean, how often do you get a free-use nurse in the box?"
  "Perhaps, it's greed. Perhaps, it's my pussy-starved brain calling\nto me."
  window hide
  show expression "images/characters/nurse/avatar/events/mystery_box/penetration_tease_firstframe.webp" as nurse with Dissolve(.5)
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/mystery_box/penetration_tease.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/mystery_box/penetration_tease_firstframe.webp") as nurse
  $renpy.music.stop(channel=u'sprite', fadeout=0)
  pause 0.5
  window auto
  "In a flash, I'm behind the [nurse]. Pumping in and out of the box."
  "I feel her. The crack of her ass, the heat radiating from her valley\nof sin."
  "Again, I'm struck with the thought — is she teasing me?"
  "How does she have such power over me, despite being locked inside a box?"
  "..."
  "No, I'm the one in charge."
  "This is the party setting playing tricks on me."
  "In the flickering light of the disco ball, my brain melts into a mush of pure desire."
  "Guided by the hammering music, the beast inside me awakens,\nand it's hungry. Hungry for all of her."
  window hide
  show expression "images/characters/nurse/avatar/events/mystery_box/penetration_and_blowjob_firstframe.webp" as nurse with Dissolve(.5)
  show expression Movie(fps=24, channel=u'sprite', play="images/characters/nurse/avatar/events/mystery_box/penetration_and_blowjob.webm", side_mask=False, start_image="images/characters/nurse/avatar/events/mystery_box/penetration_and_blowjob_firstframe.webp") as nurse
  $renpy.music.stop(channel=u'sprite', fadeout=0)
  pause 0.5
  window auto
  "The room spins around me as I'm everywhere all at once. My dick pumps in and out of every hole."
  "The whole head is enveloped by those full, succulent lips."
  mc "God... yes..."
  "Her lips slide up my shaft as she takes my length into her mouth."
  "In a dream-like state, I'm fucking her pussy and mouth all at once."
  "Her vaginal muscles contract around me and squeeze my dick. It feels so fucking tight, like I'm caught in a vise."
  if mc.owned_item(("damning_document","compromising_photo")):
    mc "That's right. Use that mouth for the only thing it's good for."
    mc "Nothing but... a cocksleeve..."
  "She gives a little hum from deep inside her throat. It vibrates around my dick."
  "Or is that her pussy buzzing with excitement?"
  if mc.owned_item(("damning_document","compromising_photo")):
    mc "F-fuck... You're in your element, aren't you?"
    "This remark is met with her swallowing more of my shaft."
    "I guess we both are!"
  "I bump into the back of her throat as I feel her open it up, finally taking me all the way."
  "I can feel the rapid exhales from her nose tickling my pelvis and it sends goosebumps prickling along my flesh."
  mc "Goddamn..."
  "I use it to drive myself in deeper, pushing halfway into her pussy\nas well."
  nurse "P-please... It's... it's too much..."
  "The box muffles her voice, making her little more than a distant object to be used."
  "And then she starts to bob her head."
  "I slip almost all the way out of her mouth, before I'm swallowed whole, again and again."
  "I'm moving back and forth. Sticking my dick in every hole of\nthe box."
  "Craving, pushing, needing it all at once."
  "It's like I'm on cocaine, tearing into her from every side."
  if mc.owned_item(("damning_document","compromising_photo")) and quest.kate_desire["sucked_off"]:
    mc "God, your mouth is somehow even better when I can't see it..."
    mc "Nothing but... a warm hole for my dick..."
  "Up and down, in and out, the [nurse] works me with everything she has. Gagging on me, sucking the salt and sweat from me."
  "The effort leaves me panting and sweaty, droplets beading on\nmy forehead."
  "But I can't seem to stop. A strange force drives me to piston in and out, digging in deeper and deeper."
  "I'm caught in the sweet ecstasy of her muscles squeezing me, trapping me, the hint of meaty friction adding to the pleasure."
  "Her throaty groans and sporadic whimpers fall like soothing rain\non my burning ears."
  "They spur me to go harder, deeper. Until I feel like I'm splitting\nher open."
  "The musky scent of her juices wafts out of the box and permeates my nostrils."
  mc "F-fuck... You like having your... everything... used?"
  nurse "Mmmhmm..."
  "Despite her moans, or maybe because of them, I can feel her whole body trembling around my dick."
  "As her pussy sucks me in and grips me, an orgasm rocks through her and she lets out a shrill mewl of bliss."
  "The sound of the absolute animal need is enough to undo me."
  "When I hit the back of her throat once again, my balls quiver and\nI stiffen."
  mc "Oh god, here it is... It's about to come..."
  if mc.owned_item(("damning_document","compromising_photo")):
    mc "You better swallow every last drop..."
  "I thrust my hips forward and pump myself into her once."
  "It's all it takes before I'm erupting, going off like a geyser into her willing mouth."
  "Her throat rocks as she takes it like a pro, hardly even gagging, guzzling my cum like she's dying of thirst and I'm the hose."
  mc "Hnnngh!"
  "Hot ropes of cum explode into her pussy as well, and coat her insides with everything I have left in me."
  "I stay buried deep within her as my balls constrict from the\ndouble ejaculation."
  "I keep a firm grip on the box as I spend the last of my energy. Head thrown back, groans and grunts bursting from my chest."
  "In the daze of an otherworldly satisfaction, the sound of her sucking, smacking lips breaks through the music."
  "Our uneven pants for air is all that I hear."
  mc "Goddamn, what a night..."
  $unlock_replay("nurse_free_use_box")
  label quest_kate_wicked_ending:
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  stop music
  $game.advance()
  $game.location = "home_bedroom"
  pause 1.0
  hide nurse
  hide minigame_background
  hide minigame_lindsey
  hide minigame_foreground
  hide black onlayer screens
  with Dissolve(.5)
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  play music "home_theme" fadein 0.5
  window auto
  "What a night!"
  "And I even made it up the stairs without waking [jo]."
  "An unprecedented feat!"
  "I always thought parties would be shit, but damn I had fun..."
  "I need to make sure to join more parties this year."
  $quest.kate_wicked.finish()
  return
