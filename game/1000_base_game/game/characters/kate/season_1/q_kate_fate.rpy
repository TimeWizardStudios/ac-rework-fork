image isabelle_photo_small = Transform("school locker isabelle",zoom=.69)
image kate_photo_small = Transform("school locker kate",zoom=.69)
image open_vent_small = Transform("school first_hall_east openvent",zoom=.4)

label quest_kate_fate_start:
  python:
    kate.unequip("kate_necklace")
    kate.equip("kate_cheerleader_top")
    kate.equip("kate_cheerleader_bra")
    kate.equip("kate_cheerleader_skirt")
    kate.equip("kate_cheerleader_panties")
  show kate neutral:
    xpos 0.0 xanchor 1.0 yalign 1.0
    pause 0.5
    easein 1.5 xalign 0.5
  $quest.kate_fate.start()
  kate neutral "Excuse me. What do you think you're doing?"
  mc "Err... just passing through..."
  kate flirty "Have you come to perv on the cheerleaders again?"
  show kate flirty at move_to(.75)
  menu(side="left"):
    extend ""
    "\"No, I was just leaving.\"":
      show kate flirty at move_to(.5)
      mc "No, I was just leaving."
      kate laughing "Is that right?"
      kate laughing "To me it looked like you were ogling us."
      mc "You're hot and all, but I don't do that anymore."
      mc "Leave me alone."
      kate eyeroll "You don't tell me what to do. Got it?"
      mc "Listen, I'm not going to play your games, [kate]."
      mc "I'm leaving."
      $kate.lust-=1
      kate cringe "No one talks to me like this!"
      kate cringe "You've just—"
      window hide None
      play sound "<from 9.2 to 10.0>door_breaking_in"
      $game.location = "school_first_hall_east"
      hide kate with hpunch
      pause 0.25
      window auto
      "Slamming that door in her face felt so good! I should've done that ages ago..."
      $mc.love+=1
      "Whatever the consequences, I'll deal with later. Living in fear is something I'm done with."
      if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_resolve"]:
        $mc["moments_of_glory"]+=1
        $mc["moments_of_glory_resolve"] = True
        $game.notify_modal(None,"Guts & Glory","Moments of Glory: Resolve\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#       if mc["moments_of_glory"] == 3:
#         $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
      $quest.kate_fate.advance("escaped")
    "\"Yeah. What are you going to do about it?\"{space=-5}":
      show kate flirty at move_to(.5)
      mc "Yeah. What are you going to do about it?"
      $kate.lust-=1
      kate laughing "Attitude? That's new."
      show kate sad_hands_up at bounce with Dissolve(.5)
      kate sad_hands_up "Nothing a group of weak girls can do to stop you, right?"
      show kate sad_hands_up at bounce
      kate sad_hands_up "We'll just have to endure your horny gaze all practice, right?"
      hide kate
      show kate sad_hands_up
      mc "Seems that way."
      kate laughing "Haven't you learned anything from last year?"
      "Damn, I'd almost forgotten about that. Those carpet burns lasted for a week."
      "But if I back down now, she'll never learn to respect me..."
      mc "I did. I went to the gym. Try moving me now."
      kate neutral "I see how it is."
      kate confident_right "Girls!"
      kate confident_right "Bring the skipping ropes!"
      window hide
      show lacey confident at appear_from_right(.75)
      pause 1.0
      show kate confident_right_rope with Dissolve(.5)
      show lacey confident at disappear_to_right
      pause 0.5
      window auto
      mc "..."
      kate gushing_rope "Ready?"
      show kate gushing_rope at move_to(.25)
      menu(side="right"):
        extend ""
        "Fight it":
          show kate gushing_rope at move_to(.5)
          mc "I've waited years for this..."
          show casey confident flip at appear_from_left(.055)
          show kate gushing_rope at move_to(.375,1)
          show stacy confident at appear_from_right(.675)
          show lacey confident at appear_from_right(.95) behind stacy
          kate gushing_rope "Girls! Get him!"
          jump quest_kate_fate_fight
        "Let it happen":
          show kate gushing_rope at move_to(.5)
          mc "You should know that I'm a decent escape artist..."
          kate confident_rope "Is that a challenge?"
          show casey neutral flip at appear_from_left(.055)
          show kate confident_rope at move_to(.375,1)
          show stacy neutral at appear_from_right(.675)
          show lacey neutral at appear_from_right(.95) behind stacy
          kate confident_rope "Girls, make the knots extra tight."
          kate neutral_rope "[mc], hands behind your back."
          window hide
          show casey neutral flip:
            linear 0.5 zoom 7.5 xoffset -2500 yoffset 5000
          show stacy neutral:
            linear 0.5 zoom 7.5 xoffset 1000 yoffset 5000
          show lacey neutral:
            linear 0.5 zoom 7.5 xoffset 2500 yoffset 5000
          show black with Dissolve(.5)
          play sound "falling_thud"
          with vpunch
          pause 1.0
          $set_dialog_mode("default_no_bg")
          $renpy.sound.play("tying_knot",loop=True)
          $quest.kate_fate["complied"] = True
          "There's something absolutely thrilling about submitting to an entire squad of sadistic cheerleaders..."
          "Feeling their fit bodies pressing in from all sides as they force your hands together behind your back..."
          play sound "falling_thud"
          mc "Ouch!{w=.5}{nw}" with vpunch
          $renpy.sound.play("tying_knot",loop=True)
          extend ""
          "I never knew my elbows could be made to touch each other... Damn, they're welded together by rope."
          "[kate]'s cronies were all girl scouts, so tying knots and baking cookies is their speciality."
          "Not exactly sure where they learned about domination and humiliation, though..."
          "Probably the Equal Rights Amendment."
          kate "Put him on his stomach.{w=.5}{nw}"
          stop sound
          extend ""
          hide casey
          hide stacy
          hide lacey
          jump quest_kate_fate_caught
        "Run":
          show kate gushing_rope at move_to(.5)
          "Shit, I know what she wants! Run!"
          window hide
          play sound "fast_whoosh"
          show kate gushing_rope at move_to("left") with hpunch
          pause 0.25
          play sound "<from 9.2 to 10>door_breaking_in"
          $game.location = school_first_hall_east
          $game.pc.energy = game.pc.max_energy
          hide kate with Dissolve(.25)
          pause 0.25
          window auto
          $quest.kate_fate.advance("hunt")
          jump quest_kate_fate_hunt_first_hall_east
    "\"I'm sorry, ma'am... can I watch?\"":
      show kate flirty at move_to(.5)
      mc "I'm sorry, ma'am... can I watch?"
      $kate.lust+=1
      kate excited "Since you asked so nicely, I feel generous."
      kate excited "However, that privilege has certain conditions..."
      mc "What, um... conditions?"
      kate skeptical "Well, we'd have to make sure you don't touch your dick, wouldn't we?"
      mc "I... wouldn't! That would be—"
      kate eyeroll "That would be just like freshman year?"
      "Fuck. Her memory is too good."
      "I thought they didn't see me behind the bleachers..."
      kate skeptical "Look at me."
      mc "Yes, ma'am."
      kate skeptical "We would have to make sure nothing perverted goes down, wouldn't we?"
      "Man, when [kate] gets this strict and demanding, my confidence turns into putty."
      "All I can do is nod and obey."
      mc "Yes, ma'am."
      kate excited "I'm glad you agree!"
      kate excited "On your stomach. Hands behind your back."
      mc "Yes, ma'am..."
      "This can't be good. I should probably run, but..."
      $quest.kate_fate["complied"] = True
      window hide
      show black with Dissolve(.5)
      play sound "tying_knot"
      pause 4.0
      jump quest_kate_fate_caught
  return

label quest_kate_fate_fight:
  if mc.strength >= 8:
    window hide
    show casey confident flip:
      linear 0.5 zoom 7.5 xoffset -2500 yoffset 5000
    show stacy confident:
      linear 0.5 zoom 7.5 xoffset 1000 yoffset 5000
    show lacey confident:
      linear 0.5 zoom 7.5 xoffset 2500 yoffset 5000
    show black with Dissolve(.5)
    pause 0.5
    $set_dialog_mode("default_no_bg")
    "..."
    "I am a mountain. I am unstoppable."
    "Indomitable against the waves of boobs and pussy!"
    "Steadfast and unflinching!"
    show casey neutral flip behind kate:
      ease_back 0.75 zoom 1.0 xoffset 0 yoffset 0
    show stacy neutral behind kate:
      ease_back 0.5 zoom 1.0 xoffset 0 yoffset 0
    show lacey neutral behind stacy:
      ease_back 0.625 zoom 1.0 xoffset 0 yoffset 0
    play sound "fast_whoosh"
    if not mc.at(school_gym):
      show kate gushing
    hide black with Dissolve(.5)
    if mc.at(school_gym):
      show kate surprised_rope with Dissolve(.25)
    else:
      show kate surprised with Dissolve(.25)
    window auto
    $set_dialog_mode("")
    if mc.at(school_gym):
      kate surprised_rope "..."
    else:
      kate surprised "..."
    mc "What's wrong?"
    mc "Did you girls think you could just tie me up and have your way with me?{space=-80}"
    mc "Times have changed."
    mc "Good luck with the practice. I'm out."
    window hide
    if mc.at(school_gym):
      show casey neutral flip at move_to(-.25) behind kate
      show kate surprised_rope at move_to("left")
      show stacy neutral at move_to("right")
      show lacey neutral at move_to(1.25) behind stacy
      pause 1.0
      play sound "<from 9.2 to 10.0>door_breaking_in"
      $game.location = "school_first_hall_east"
      hide casey
      hide kate
      hide stacy
      hide lacey
      with Dissolve(.25)
      pause 0.5
    else:
      show casey neutral flip at disappear_to_right(2.0)
      show kate surprised at disappear_to_right(2.0)
      show stacy neutral at disappear_to_right(2.0)
      show lacey neutral at disappear_to_right(2.0)
      pause 2.0
    window auto
    "Man, the look of surprise and confusion on [kate]'s face! Priceless."
    "Last time around, I wouldn't have stood a chance, but the hours at the gym are paying off..."
    if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_strength"]:
      $mc["moments_of_glory"]+=1
      $mc["moments_of_glory_strength"] = True
      $game.notify_modal(None,"Guts & Glory","Moments of Glory: Strength\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#     if mc["moments_of_glory"] == 3:
#       $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
    $quest.kate_fate.advance("escaped")
    return
  else:
    "Huh?"
    show layer master:
      linear 0.25 yoffset -15
      linear 0.25 xoffset 15 yoffset 0
      linear 0.25 yoffset 15
      linear 0.25 xoffset 0 yoffset 0
      linear 0.25 xoffset -15 yoffset 15
      linear 0.25 yoffset 0
      linear 0.25 xoffset 0 yoffset -15
      linear 0.25 xoffset 15 yoffset 0
      linear 0.25 yoffset 15
      linear 0.25 xoffset -15 yoffset 0
      linear 0.25 yoffset -15
      linear 0.25 xoffset 0 yoffset 0
      repeat
    show stacy confident:
      linear 0.5 zoom 7.5 xoffset 500 yoffset 5000
    stacy confident "I've got his hands!" with vpunch
    kate "Push him down!"
    show casey confident flip:
      linear 0.5 zoom 7.5 xoffset -2500 yoffset 4250
    show stacy confident:
      linear 0.5 zoom 7.5 xoffset 1500 yoffset 4250
    mc "What the hell?!" with vpunch
    casey confident flip "Freak, you broke my nail! Stop struggling!"
    mc "Let go!"
    show casey confident flip:
      linear 0.5 zoom 7.5 xoffset -3500 yoffset 3500
    show stacy confident:
      linear 0.5 zoom 7.5 xoffset -500 yoffset 3500
    show lacey confident:
      linear 0.5 zoom 7.5 xoffset 2500 yoffset 3500
    lacey confident "Got you!" with vpunch
    mc "Ugh!"
    window hide
    show layer master
    show black
    with Dissolve(1.0)
    play sound "tying_knot"
    pause 4.0
    $game.location = school_gym
    hide casey
    hide lacey
    hide stacy
    hide black
    show kate cheerleader_feet
    with Dissolve(.5)
    window auto
    "Fuck... I can't move a muscle."
    "They tied those ropes tightly. Goddamn."
    kate cheerleader_feet_kate_smile "There you are! Trussed up like a pathetic rope burrito."
    kate cheerleader_feet_kate_smile "Now, what should we do with you...?"
    kate cheerleader_feet_kate_neutral "You're lucky you're such a pussy, else we'd take you to the roof and hang you by your balls."
    "Fuck me."
    kate cheerleader_feet_kate_laughing "Something wrong?"
    kate cheerleader_feet_kate_laughing "You don't like that idea?"
    stacy "How about we stuff him into the new girl's locker?"
    menu(side="middle"):
      extend ""
      "\"How about you suck my dick and I won't report you to the principal?\"":
        mc "How about you suck my dick and I won't report you to the principal?"
        $kate.lust-=1
        stacy "Watch your mouth, creep!"
        kate cheerleader_feet_kate_smile "Still such a tough guy..."
        mc "Still such a bitch."
        jump quest_kate_fate_socks
      "\"Please don't!\"":
        mc "Please don't!"
        mc "I'm sorry for stepping out of line, ma'am..."
        $kate.lust+=1
        kate cheerleader_feet_kate_smile "Now, that's a tone I like better."
        kate cheerleader_feet_kate_smile "Tell me something, [mc] — why did you try to fight us?"
        mc "I... err, I had a lapse in judgement..."
        kate cheerleader_feet_kate_neutral "I see... And you've come to your senses now, correct?"
        mc "Yes, ma'am..."
        kate cheerleader_feet_kate_neutral "Good."
        kate cheerleader_feet_kate_smile "So, what do you think your punishment should be?"
        mc "P-punishment?"
        kate cheerleader_feet_kate_neutral "You tried to spy on us, and then struggled."
        casey "I broke a nail because of you!"
        lacey "I'm all around annoyed."
        stacy "I don't like how you look..."
        kate cheerleader_feet_kate_confident "See? Everyone's taken an issue with you."
        "Damn it. I wasn't even going to watch them practice... {i}that{/} much..."
        mc "Can't you just leave me here or something?"
        kate cheerleader_feet_kate_neutral "Ever since school started, you've been acting weird."
        kate cheerleader_feet_kate_neutral "You've been running around the school. Talking to people you shouldn't."
        kate cheerleader_feet_kate_neutral "The truth is, we don't want you in the sports wing. You're tarnishing our image."
        kate cheerleader_feet_kate_neutral "There's also a rumor going around that you've been trying to flirt with different girls."
        casey "Weren't cartoon boobs enough for you anymore?"
        lacey "Unclean!"
        "Hmm... maybe they're right. Both [lindsey] and [isabelle] are way out of my league..."
        "Perhaps it would be best not to anger [kate] this early in the year."
        mc "I... err, I'll try to be better."
        kate cheerleader_feet_kate_smile "Yes, you will."
        kate cheerleader_feet_kate_smile "But you also need to be taught a lesson, understood?"
        mc "Yes, ma'am..."
        kate cheerleader_feet_kate_laughing "Very well!"
        kate cheerleader_feet_kate_confident "Now, be a good boy while we finish practice."
        kate cheerleader_feet_kate_neutral "I don't want to hear a word out of your mouth. Once we're done, we'll decide your fate."
        kate cheerleader_feet "Come on, girls."
      "?quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed@|{image=isabelle_photo_small}|\"[isabelle] is dangerous.\"":
        mc "[isabelle] is dangerous."
        mc "She's smart — not as smart as [kate], of course, but definitely smarter than you, [stacy]."
        stacy "What the fuck did you just—"
        kate cheerleader_feet_kate_neutral "[mc] is right."
        retinue "He's what?!" with vpunch
        kate cheerleader_feet_kate_neutral "She's the type that won't bow to my authority no matter what."
        kate cheerleader_feet_kate_smile "Remember Rebecca from sophomore year?"
        casey "Hah, yeah! That bitch looked like a tomato after we put bees in her locker!"
        lacey "Queen!"
        kate cheerleader_feet_kate_neutral "But you guys remember how stubborn she was, right?"
        kate cheerleader_feet_kate_neutral "It took us several weeks to make her drop out."
        stacy "Yeah, a real pain in the coocher..."
        casey "It's \"pain in the ass.\""
        stacy "Babe, if you dated Chad instead of Tanner, you'd know what I mean."
        casey "As if you're dating Chad!"
        kate cheerleader_feet_kate_laughing "Ladies! The point is that old Becky was difficult and the new girl is a lot like her."
        kate cheerleader_feet_kate_neutral "We need to be smart about [isabelle]."
        kate cheerleader_feet_kate_confident "Now, for our little pervert here... What would be a good punishment?"
        casey "We should put something in his mouth to stop the drooling!"
        kate cheerleader_feet_kate_confident "As it happens, I have the perfect tool for the job..."
        show kate cheerleader_feet_kate_smile_ballgag with Dissolve(.5)
        lacey "Queen!"
        stacy "Kinky."
        mc "I, um... I thought you'd leave me alone if I helped you with [isabelle]."
        kate cheerleader_feet_kate_laughing_ballgag "Did I say that? Well, how about this..."
        kate cheerleader_feet_kate_confident_ballgag  "You accept my ball in your mouth like a good little pet, and we won't make you drink toilet water after we're done here."
        casey "That's more than fair."
        mc "How's that leaving me alone...?"
        kate cheerleader_feet_kate_neutral_ballgag "It goes both ways. You came in here and perved on us, remember?"
        kate cheerleader_feet_kate_neutral_ballgag "Now, open up."
        "Fuck. Getting gagged by [kate]? Feels like I've had this fantasy before."
        "I'm getting effing hard from this... Maybe I did come here asking for it?"
        mc "Aaaah..."
        $kate.lust+=1
        kate cheerleader_feet_kate_smile_ballgag "Good bitch."
        window hide
        show black with Dissolve(.5)
        pause 1.0
        $quest.kate_fate["ballgag"] = True
        hide black
        show kate cheerleader_feet_kate_smile
        with Dissolve(.5)
        window auto
        kate cheerleader_feet_kate_smile "There! How's that?"
        mc "{i}Mmmph.{/}"
        casey "I think he likes it."
        stacy "He's a little freak, of course he likes it."
        kate cheerleader_feet_kate_neutral "Don't go anywhere."
        kate cheerleader_feet "Come on, girls. Let's get started with practice."
    show kate cheerleader_feet_nofeet with Dissolve(.5)
    jump quest_kate_fate_bound

label quest_kate_fate_socks:
  kate cheerleader_feet_kate_neutral "For someone in your position, that was a very stupid thing to say."
  kate cheerleader_feet_kate_laughing "Luckily, I know exactly how to deal with unruly prisoners!"
  window hide
  show kate cheerleader_feet_kate_shoeless_smile with Dissolve(.5)
  pause 0.5
  show kate cheerleader_feet_kate_shoeless_sockless_holdingsock_smile with Dissolve(.5)
  window auto
  mc "Wait just one minute!"
  kate cheerleader_feet_kate_shoeless_sockless_holdingsock_smile "What's wrong?"
  lacey "I think it's time to open wide, [mc]."
  casey "Yeah, you pervert! Say, \"aaaah!\""
  kate cheerleader_feet_kate_shoeless_sockless_holdingsock_neutral "They're going into your mouth whether you like it or not."
  kate cheerleader_feet_kate_shoeless_sockless_holdingsock_neutral "We're tired of your insolence."
  "Fuck! There's no way she's putting her dirty socks in my mouth..."
  "What do I do?!"
  "I can tell they're all itching to give me a wedgie if I don't comply..."
  "There's no winning here, only different levels of losing."
  menu(side="middle"):
    extend ""
    "Keep your mouth shut":
      mc "..."
      kate cheerleader_feet_kate_shoeless_sockless_holdingsock_neutral "Girls. You know what to do."
      stacy "You asked for this, loser!"
      window hide
      show black with Dissolve(.5)
      pause 1.0
      play sound "<from 1.0>nylon_rip"
      $set_dialog_mode("default_no_bg")
      mc "Ow, fuck!" with vpunch
      "It's been so long... I'd forgotten how painful that wedgie is..."
      lacey "Let's take it to the next level, yeah?"
      mc "Fuck me! You're tearing me—"
      mc "{i}Mmmph!{/}" with vpunch
      $quest.kate_fate["socks"] = True
      hide black
      show kate cheerleader_feet_kate_shoeless_sockless_laughing
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      kate cheerleader_feet_kate_shoeless_sockless_laughing "There you go! I told you those socks would go into your mouth..."
      mc "{i}Muhgugh!{/}"
      kate cheerleader_feet_kate_shoeless_sockless_confident "Didn't quite catch that."
    "Surrender":
      mc "Aaaah..."
      $kate.lust+=1
      kate cheerleader_feet_kate_shoeless_sockless_holdingsock_smile "Finally."
      window hide
      show black with Dissolve(.5)
      pause 1.0
      $quest.kate_fate["socks"] = True
      hide black
      show kate cheerleader_feet_kate_shoeless_sockless_confident
      with Dissolve(.5)
      window auto
      kate cheerleader_feet_kate_shoeless_sockless_confident "There's a good boy! That ought to shut you up..."
      mc "{i}Mmmph.{/}"
  "The taste of feet is overwhelming. Sweat, old and new."
  "Kate's feet..."
  "Fuck, why am I getting turned on all of a sudden?"
  "If they flip me over, I'll never hear the end of it."
  kate cheerleader_feet_kate_shoeless_sockless_smile "We're going to practice now."
  kate cheerleader_feet_kate_shoeless_sockless_smile "If you behave, we'll let you go once we're done."
  kate cheerleader_feet_kate_shoeless_sockless_neutral "If not, we'll let the janitor find you in a bathroom stall at the end of the day."
  kate cheerleader_feet_shoeless_sockless "Let's go, girls!"
  show kate cheerleader_feet_nofeet with Dissolve(.5)
  "..."
  "The upside is that I have a first row seat to a group of hot girls in skimpy outfits."
  "The downside is that I have [kate]'s socks in my mouth, and my dick is about to burst out of my pants."
  "Fuck me... I better reduce the swelling before they're done."
  window hide
  show black with Dissolve(.5)
  pause 1.5
  hide black with Dissolve(.5)
  window auto
  "..."
  "Damn it. And I don't even have a good view of them..."
  "Their long legs, dancing across the gym floor... Their short skirts, flapping in the wind..."
  "How long has it been since my imprisonment? Days? weeks?"
  "The only good thing about this is that I'm hard as a rock."
  "My dick keeps the blood circulation going."
  window hide
  show black with Dissolve(.5)
  pause 1.5
  hide black with Dissolve(.5)
  window auto
  "..."
  "Shit, they're coming over."
  show kate cheerleader_feet with Dissolve(.5)
  kate cheerleader_feet "Enjoying yourself down there?"
  mc "{i}Mmmph.{/}"
  kate cheerleader_feet "See, [lacey]? This is why I always keep an extra pair of gym socks."
  lacey "Your wisdom is a marvel to behold..."
  kate cheerleader_feet "Bitch, don't roll your eyes at me."
  lacey "Sorry, hah!"
  kate cheerleader_feet "We're halfway through practice, but we thought we'd take a break to inspect our prisoner."
  kate cheerleader_feet "[casey], check his ropes."
  casey "Why me? I don't want to touch him!"
  kate cheerleader_feet "No one wants to, okay?"
  lacey "Can't we make him check his own ropes?"
  stacy "Babe, you're doing that thing again..."
  lacey "What thing?"
  stacy "Being stupid."
  lacey "Okay, miss smartypants. You check his ropes, then."
  stacy "I'm not touching that!"
  kate cheerleader_feet "You know what? He wouldn't dare to escape."
  kate cheerleader_feet "I'm sure he'll stay just like this until we say otherwise."
  "The world has always been at the feet of beautiful women."
  "That's more clear to me than ever."
  "For some reason, there's a sort of calming clarity in letting go and submitting."
  "The bulge between my legs isn't something I control, but perhaps the lack of control is the real turn on."
  "Not sure if I'm ready to face such levels of humiliation, though..."
  "Maybe certain things should be left unsaid."
  menu(side="middle"):
    extend ""
    "Roll over":
      $mc.lust+=1
      window hide
      show kate cheerleader_feet:
        xalign 0.5 yalign 0.5
        linear 0.1 rotate -5
        parallel:
          linear 0.5 rotate 185
          linear 0.1 rotate 180
        parallel:
          linear 0.25 zoom 2.5
          linear 0.25 zoom 1
      pause 1.0
      hide kate cheerleader_feet
      show kate cheerleader_feet:
        zoom -1
      window auto
      casey "..."
      lacey "..."
      stacy "..."
      kate cheerleader_feet "Well, there's the proof."
      kate cheerleader_feet "What kind of slimy perverted dog gets horny in such a situation?"
      casey "He's truly one of a kind, this one."
      lacey "Ewwwwww!"
      stacy "Disgusting."
      kate cheerleader_feet "Turn back over. No one wants to see that."
      "Not sure if I should be happy that they even noticed the bulge, or just completely ashamed..."
      window hide
      show kate cheerleader_feet:
        xalign 0.5 yalign 0.5
        linear 0.1 rotate 5
        parallel:
          linear 0.5 rotate -185
          linear 0.1 rotate -180
        parallel:
          linear 0.25 zoom -2.5
          linear 0.25 zoom -1
      pause 1.0
      hide kate cheerleader_feet
      show kate cheerleader_feet
      window auto
      $kate.lust+=1
      kate cheerleader_feet "Well, if this turns you on, wait until you learn what else we have in store for you this year."
      "In a weird way, I'm both terrified and excited..."
      "Damn it, I need to get these thoughts out of my head."
      "..."
      "But maybe the only way is to embrace it?"
      kate cheerleader_feet "Come on, girls! Let's finish up practice."
      show kate cheerleader_feet_nofeet with Dissolve(.5)
    "Stay put":
      "There's no telling what would happen by showing [kate] that this kind of treatment is turning me on."
      $mc.love+=1
      "Why increase her hold over me?"
      kate cheerleader_feet "Come on, girls! Let's finish up practice."
      show kate cheerleader_feet_nofeet with Dissolve(.5)
      "Phew! Somehow made it through unnoticed..."

label quest_kate_fate_bound:
  window hide
  show black with Dissolve(.5)
  pause 2.0
  $game.advance()
  hide black with Dissolve(.5)
  window auto
  "..."
  "When I woke up this morning, I never thought I'd end up in bondage."
  "Hmm... does this actually count as bondage?"
  "If so, does that mean I just had sex with all the cheerleaders at once?"
  "No one except Chad could pull that off!"
  "..."
  "I can't believe they practice for this long..."
  "Maybe I should try to get loose?"
  "Hmm... probably not worth the risk."
  "..."
  "Huh, the music finally stopped. Maybe they're done?"
  show kate cheerleader_feet with Dissolve(.5)
  kate cheerleader_feet "We're done with practice for today."
  kate cheerleader_feet "We were supposed to go the whole day, but we've decided to take the rest of the day off."
  kate cheerleader_feet "I'm treating the girls to a night at the Newfall spa."
  lacey "Queen!"
  kate cheerleader_feet "Unfortunately for you, your fate will have to wait until the morning."
  kate cheerleader_feet "Let's go, girls!"
  show kate cheerleader_feet_nofeet with Dissolve(.5)
  "What the hell? Are they really going to leave me here overnight?!"
  "Fuck..."
  "And they said they had the gym booked for the rest of the day..."
  "..."
  "Huh? Someone's coming."
  show kate cheerleader_feet_noretinue with Dissolve(.5)
  pause 0.5
  kate cheerleader_feet_nofeet_kate_confident "Had a few more things to tell you before I go."
  kate cheerleader_feet_nofeet_kate_laughing "Those ropes look good on you! I like how they dig into your skin and keep you in a docile little package..."
  kate cheerleader_feet_nofeet_kate_confident "I've studied Japanese rope techniques such as shibari and kinbaku. Those ropes are expertly tied and won't be coming off until I say so.{space=-5}"
  kate cheerleader_feet_nofeet_kate_smile "And I say they stay on until the morning."
  kate cheerleader_feet_nofeet_kate_smile "I spoke to [jo], and told her you're staying over at my place tonight."
  "What the fuck. No way."
  kate cheerleader_feet_nofeet_kate_laughing "She sounded surprised, but happy..."
  kate cheerleader_feet_nofeet_kate_laughing "She didn't even question it! Just told me to have fun."
  kate cheerleader_feet_nofeet_kate_smile "I know that look, but don't worry. I quite like keeping you under my heel, and I wouldn't want anything serious to happen to you."
  "Somehow, I don't buy that for a second..."
  kate cheerleader_feet_nofeet_kate_smile "In case of an emergency, I've put my number into your phone. Here you go."
  $mc.add_phone_contact(kate)
  kate cheerleader_feet_nofeet_kate_phoneground_smile "So, just press call with your nose or something."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "But keep in mind that abusing this rare privilege would have dire consequences."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "Nod if you understand."
  menu(side="middle"):
    extend ""
    "Nod":
      window hide
      show layer master:
        xalign 0.5 yalign 0.5
        parallel:
          linear 0.25 yoffset -50
          linear 0.25 yoffset 50
          linear 0.25 yoffset -50
          linear 0.25 yoffset 50
          linear 0.25 yoffset 0
        parallel:
          linear 0.25 zoom 1.1
          pause 0.75
          linear 0.25 zoom 1.0
      pause 1.5
      show layer master
      $kate.lust+=1
      show kate cheerleader_feet_nofeet_kate_phoneground_confident with Dissolve(.5)
      window auto
      kate cheerleader_feet_nofeet_kate_phoneground_confident "That's a good pet!"
      kate cheerleader_feet_nofeet_kate_phoneground_confident "I love obedience. I can already tell you'll be very obedient."
      kate cheerleader_feet_nofeet_kate_phoneground_smile "Keep it up, and this might be a good year for you..."
    "Shake head":
      window hide
      show layer master:
        xalign 0.5 yalign 0.5
        parallel:
          linear 0.25 xoffset -50
          linear 0.25 xoffset 50
          linear 0.25 xoffset -50
          linear 0.25 xoffset 50
          linear 0.25 xoffset 0
        parallel:
          linear 0.25 zoom 1.1
          pause 0.75
          linear 0.25 zoom 1.0
      pause 1.5
      show layer master
      $kate.lust-=1
      window auto
      kate cheerleader_feet_nofeet_kate_phoneground_neutral "Bad pet! I see you require some training still..."
      kate cheerleader_feet_nofeet_kate_phoneground_confident "Luckily, we have all year and by the end of it, you're going to be my little bitch."
      kate cheerleader_feet_nofeet_kate_phoneground_confident "You're going to do everything I say. Even bark on command."
      kate cheerleader_feet_nofeet_kate_phoneground_smile "All in good time, though."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "Oh, and one more thing — no cumming."
  kate cheerleader_feet_nofeet_kate_phoneground_neutral "I'll check in the morning, so don't you dare try."
  kate cheerleader_feet_nofeet_kate_phoneground_laughing "Sleep well!"
  show kate cheerleader_feet_nofeet_phoneground with Dissolve(.5)
  "Man, I can't believe [kate] is actually going to go through with it..."
  "Never thought this is how I'd get her number either."
  "In fact, I never thought I'd get her number period."
  "In a weird way, by leaving her number she's showing that she's not rotten to the core."
  "Although, knowing her, it might just be a fake number to crush my hope one last time during an emergency."
  "Might as well try to get some sleep..."
  $unlock_replay("kate_punishment")
  jump quest_kate_fate_conversation

label quest_kate_fate_hunt_first_hall_east:
  "Fuck, they're coming after me!"
  "There's no way I'll outrun a group of fit cheerleaders..."
  menu(side="middle"):
    extend ""
    "Keep running":
      "Hiding is for pussies. Alpha athletes run. No shame in that."
      $mc["focus"] = "kate_fate"
      return
    "Hide":
      "Where do I hide?!"
      "Shit, shit, shit. Think fast!"
      menu(side="middle"):
        extend ""
        "Blend into the background":
          "Don't move..."
          window hide
          pause 0.5
          show kate thinking flip:
            xpos 0.0 xanchor 1.0
            easein 4.0 xpos 1.0 xanchor 0.0
          pause 4.0
          show kate laughing at appear_from_right
          window auto
          kate laughing "What are you doing?"
          "Don't make eye contact..."
          kate neutral "Hey, freak. I asked you a question."
          "Fuck."
          mc "Nothing..."
          kate confident "Were you trying to hide from me?"
          mc "Um... no?"
          kate neutral "Don't lie to me."
          mc "Fine... I did try to hide."
          kate confident "You know, that's three transgressions already."
          mc "Three...?"
          kate neutral "One, perving on the cheer team."
          kate neutral "Two, running away."
          kate neutral "Three, trying to hide."
          kate neutral "You're making my life difficult, and I don't appreciate that."
          mc "..."
          jump quest_kate_fate_fight_surrender
        "?mc.strength>=5 and school_first_hall_east['vent_open']@[mc.strength]/5|{image=stats str}|{color=#fefefe}+ {/}|{image=open_vent_small}|In the air duct":
          jump quest_kate_fate_air_duct

label quest_kate_fate_air_duct:
  window hide
  $school_first_hall_east["shoe1"] = school_first_hall_east["shoe2"] = school_first_hall_east["shoe3"] = True
  pause 0.5
  $school_first_hall_east["shoe4"] = school_first_hall_east["shoe5"] = True
  pause 0.5
  $school_first_hall_east["shoe6"] = school_first_hall_east["shoe7"] = school_first_hall_east["shoe8"] = True
  pause 0.5
  if school_first_hall_east["dollar3_spawned_today"] == True and not school_first_hall_east["dollar3_taken_today"]:
    $school_first_hall_east["dollar3_taken_today"] = True
    $mc.money+=20
  $school_first_hall_east["shoe9"] = school_first_hall_east["shoe10"] = school_first_hall_east["dollar3_taken"] = True
  pause 0.5
  $school_first_hall_east["shoe11"] = True
  pause 0.5
  label quest_kate_fate_air_duct_shortcut:
  show kate grill_perspective with Dissolve(.5)
  window auto
  "I hope they didn't see me climb up..."
  "They'd never expect me in here otherwise."
  "Mostly because they think I'm too fat."
  show kate grill_perspective_girls with Dissolve(.5)
  stacy "Where the hell did that pervert go?"
  casey "He must be around here somewhere!"
  lacey "Yeah, it feels like he's looking at my ass..."
  "She's not wrong. Hehe!"
  kate grill_perspective_girls "Let's go back... We'll deal with that creep later."
  show kate grill_perspective with Dissolve(.5)
  "Phew! They left."
  hide kate with Dissolve(.5)
  "Outsmarted. Outwitted. Out-brained."
  "No one can go up against this enlightened bundle of gray matter."
  if mc["vent_hide"]: ## Conditional for "Call Me, Beep Me!" achievement
    $achievement.call_me_beep_me.unlock()
  else:
    $mc["vent_hide"] = True ## In case quests are done in opposite order
  $quest.kate_fate.advance("escaped")
  return

label quest_kate_fate_fight_surrender:
  kate laughing "So, are you coming back to the gym willingly, or should I get the girls?"
  show kate laughing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'll come willingly...\"":
      show kate laughing at move_to(.5)
      mc "I'll come willingly..."
      kate flirty "There's a good boy."
      kate flirty "Okay, let's go back and determine your fate."
      window hide
      show black with Dissolve(.5)
      pause 1.0
      call goto_school_gym
      show kate gushing_rope with Dissolve(.5)
      window auto
      kate gushing_rope "Look what I caught!"
      kate gushing_rope "A slippery little pervert."
      show casey confident flip at appear_from_left(.055) behind kate
      show kate gushing_rope at move_to(.375,1)
      show stacy confident at appear_from_right(.675) behind kate
      show lacey confident at appear_from_right(.95) behind stacy
      stacy "Does he talk?"
      lacey "Can he do tricks?"
      kate confident_right_rope "Maybe we can teach him some..."
      casey "How about begging? I'd like to see that."
      kate confident_right_rope "I like that idea!"
      kate confident_rope "What do you think, [mc]?"
      mc "Uhh..."
      show casey neutral flip
      show stacy neutral
      show lacey neutral
      kate neutral_rope "Beg us to tie you up."
      "Oh, fuck! [kate] brought out her stern voice and her squad is backing her up..."
      "Why am I getting hard from being bossed around?"
      "I can't let them see it. That would be {i}very{/} bad."
      mc "Please... err..."
      kate neutral_rope "Start over."
      "God, she's so dominant!"
      mc "Please tie me up..."
      kate neutral_rope "That's better."
      kate neutral_rope "Hands behind your back, convict."
      window hide
      show black with Dissolve(.5)
      play sound "tying_knot"
      pause 3.0
      $set_dialog_mode("default_no_bg")
      kate neutral "Now, on your stomach."
      pause 1.0
      jump quest_kate_fate_caught
    "\"You better call your cronies, because I'm not going anywhere.\"":
      show kate laughing at move_to(.5)
      mc "You better call your cronies, because I'm not going anywhere."
      kate eyeroll "You'll regret this."
      show casey confident flip at appear_from_left(.055)
      show kate eyeroll at move_to(.375,1) behind lacey
      show stacy confident at appear_from_right(.675)
      show lacey confident at appear_from_right(.95) behind stacy
      pause 0.0
      kate excited "Girls! Get him!"
      jump quest_kate_fate_fight
  return

label quest_kate_fate_caught:
  hide black
  show kate cheerleader_feet
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  kate cheerleader_feet "Down where you belong."
  if not quest.kate_fate["complied"]:
    kate cheerleader_feet "Did you really think you could be disrespectful and get away with it?{space=-10}"
  lacey "He looks like a maggot!"
  stacy "He probably is one."
  casey "Is there maggot DNA in you?"
  mc "..."
  kate cheerleader_feet "Answer her."
  mc "No..."
  kate cheerleader_feet "No, what?"
  menu(side="middle"):
    extend ""
    "\"No, ma'am...\"":
      mc "No, ma'am..."
      $kate.lust+=1
      kate cheerleader_feet "Better."
      casey "You're so good at getting him in line, Katie!"
      kate cheerleader_feet "Hopefully, he's learning."
      stacy "Are you learning, little maggot?"
      mc "Yes, ma'am..."
      lacey "Queen!"
      kate cheerleader_feet "All right, we're going to get back to practice. You stay put, [mc]."
      show kate cheerleader_feet_nofeet with Dissolve(.5)
      "Nothing to do but staying put, I guess."
      "Damn, these ropes are tight..."
      jump quest_kate_fate_bound
    "\"No, you psychotic bitch.\"":
      mc "No, you psychotic bitch."
      lacey "{i}Gasp!{/}" with vpunch
      stacy "My god."
      casey "How dare..."
      $kate.lust-=2
      jump quest_kate_fate_socks

label quest_kate_fate_escaped:
  window hide
  play sound "phone_vibrate"
  pause 2.0
  $set_dialog_mode("phone_message","hidden_number")
  hidden_number "Congratulations."
  mc "Congratulations?"
  hidden_number "I watched you. You have altered your fate. You are on a good path."
  mc "Thanks, I guess..."
  $set_dialog_mode()
  window auto
  "..."
  window hide
  $set_dialog_mode("phone_message","hidden_number")
  mc "That's it? What's next?"
  hidden_number "I cannot tell you what lies ahead, but as long as you trust your heart, you will reach the other side."
  $set_dialog_mode()
  window auto
  "Great. Always cryptic, creepy, and crappy."
  $quest.kate_fate['escaped'] = True
  $quest.kate_fate.finish()
  return

label quest_kate_fate_conversation:
  window hide
  show black with Dissolve(3.0)
  while not game.hour == 1:
    $game.advance()
  pause 1.0
  hide black
  show kate cheerleader_feet_nofeet_phoneground_night
  with Dissolve(.5)
  window auto
  "Huh...? What time is it?"
  "The lights are off, so it's late."
  "Man, I'm getting hungry, and my leg is falling asleep..."
  "Not exactly an emergency, though."
  window hide
  play sound "<from 2.7 to 4.2>door_breaking_in"
  pause(1.5)
  play sound "bedroom_door"
  pause(1.25)
  window auto
  "Oh, fuck. Someone's coming!"
  "Please not the [guard]..."
  "Please not [flora]..."
  "Please not [isabelle]..."
  "Please not—"
  "???" "What do you want now?"
  mrsl "We have a spider problem."
  "???" "I am well aware."
  mrsl "Why haven't you fixed it, then?"
  "???" "Because most of my energy is spent watching you."
  mrsl "Me? I haven't even done anything."
  "???" "The new art teacher..."
  mrsl "What about her?"
  "???" "What kind of fool do you take me for?"
  mrsl "Miss Hyde seems like a perfectly fine young woman to me."
  "???" "What did you do to Mrs. Bloomer?"
  mrsl "Absolutely nothing."
  "???" "Liar!"
  mrsl "What's with the accusations? Are you scared?"
  "???" "I know no fear, but I can smell a lie from a mile away."
  mrsl "I'm surprised you can smell anything at this point..."
  "???" "My senses are as sharp as ever."
  mrsl "If that's the case, how come there's a spider problem again?"
  "???" "..."
  "???" "I will need to look into it."
  mrsl "You know what happened last time..."
  "???" "I took care of it last time."
  mrsl "Right. That's why there's a school and not a castle here."
  "???" "I should have cut out your tongue when I had the chance..."
  play sound "<from 9.2 to 10.0>door_breaking_in"
  "..."
  "Sounds like they left."
  "What the hell was that about?"
  "And who does the mysterious voice belong to?"
  "Spiders? Mrs. Bloomer? Castles? This all sounds like an interview with [maxine]..."
  "Why is [mrsl] in the school so late after dark?"
  "If only I could get loose..."
  window hide
  show black with Dissolve (3.0)
  while not game.hour==8:
    $game.advance()
  pause 1.0
  hide black
  show kate cheerleader_feet_nofeet_kate_smile_phoneground
  with Dissolve(.5)
  window auto
  kate cheerleader_feet_nofeet_kate_smile_phoneground "Wakey, wakey."
  if quest.kate_fate["socks"]:
    mc "{i}Ughmmph.{/}"
    kate cheerleader_feet_nofeet_kate_laughing_phoneground "A whole night with my socks in your mouth! How does it feel?"
    menu(side="middle"):
      extend ""
      "{i}\"Mmmph!\"{/}":
        mc "{i}Mmmph!{/}"
        kate cheerleader_feet_nofeet_kate_confident_phoneground "Did you like it?"
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "No?"
        kate cheerleader_feet_nofeet_kate_confident_phoneground "Well, I certainly did, and that's what matters."
        "Fuck... why am I getting hard again?"
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "Are you going to be a good boy if I take the socks out?"
        mc "{i}Mmmph!{/}"
        kate cheerleader_feet_nofeet_kate_laughing_phoneground "I get that you're upset, but if you don't behave when I take them out, you're going to be sorry."
        window hide
        show black with Dissolve(.5)
        pause 1.0
        hide black
        show kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground
        with Dissolve(.5)
        window auto
        kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground "And what do you say?"
        menu(side="middle"):
          extend ""
          "\"Thank you, ma'am...\"":
            mc "Thank you, ma'am..."
            "Probably best not to mess with [kate] anymore."
            "If tonight has been an indication of things to come, it'd be risky to upset her."
            $kate.lust+=1
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "That's better."
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "See? There's no need to be a brat about it. If you behave, I'll be nice.{space=-10}"
            kate cheerleader_feet_nofeet_kate_holdingsock_neutral_phoneground "Do you understand the dynamic here?"
            mc "Yes..."
            kate cheerleader_feet_nofeet_kate_holdingsock_neutral_phoneground "Yes, what?"
            mc "Yes, ma'am..."
            kate cheerleader_feet_nofeet_kate_holdingsock_laughing_phoneground "Very good!"
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "Okay, what do you think we should do next?"
          "?quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed@|{image=kate_photo_small}|\"Thanks for revealing your hand so early.\"":
            mc "Thanks for revealing your hand so early."
            kate cheerleader_feet_nofeet_kate_holdingsock_neutral_phoneground "Revealing my hand?"
            mc "It's early in the school year, and I thought I'd give you the chance to change this time around."
            mc "But clearly you're the same old bitch you've always been. Irredeemable."
            mc "This year, you'll be caught in the act and exposed."
            $kate.lust-=2
            kate cheerleader_feet_nofeet_kate_holdingsock_neutral_phoneground "Seems like you still haven't learned your lesson..."
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "In it goes again!"
            window hide
            show black with Dissolve(.5)
            $set_dialog_mode("default_no_bg")
            mc "{i}Mmmph!{/}" with vpunch
            hide black
            show kate cheerleader_feet_nofeet_kate_neutral_phoneground
            with Dissolve(.5)
            window auto
            $set_dialog_mode("")
            kate cheerleader_feet_nofeet_kate_neutral_phoneground "I like you better silent. Perhaps we should get a permanent muzzle for you?"
            kate cheerleader_feet_nofeet_kate_neutral_phoneground "No one likes a dog that barks."
            kate cheerleader_feet_nofeet_kate_neutral_phoneground "I was going to untie you, but I don't feel particularly gracious right now..."
            kate cheerleader_feet_nofeet_kate_neutral_phoneground "I have more important things to take care off. See you later."
            show kate cheerleader_feet_nofeet_phoneground with Dissolve(.5)
            "Fuck me... She actually left again."
            window hide
            show black with Dissolve(.5)
            pause 1.0
            play sound "<from 2.7 to 4.2>door_breaking_in"
            pause(1.5)
            play sound "bedroom_door"
            pause(1.5)
            $set_dialog_mode("default_no_bg")
            isabelle "[mc]?! Are you okay?"
            "Shit, I must've fallen asleep for a bit..."
            hide black
            show kate cheerleader_feet_isabelle
            with Dissolve(.5)
            window auto
            $set_dialog_mode("")
            isabelle "Who did this to you?!"
            mc "{i}Mmmph...{/}"
            isabelle "Here, let me untie you!"
            show kate cheerleader_feet_nofeet_phoneground with Dissolve(.5)
            isabelle "Man, these knots are bloody tight..."
            window hide
            show black with Dissolve(.5)
            play sound "tying_knot"
            pause 4.0
            hide kate
            hide black
            show isabelle concerned
            with Dissolve(.5)
            window auto
            isabelle concerned "What happened here?"
            isabelle skeptical "It was [kate], wasn't it?"
            show isabelle skeptical at move_to(.75)
            menu(side="left"):
              extend ""
              "Nod":
                window hide
                show isabelle skeptical at move_to(.5)
                show layer master:
                  xalign 0.5 yalign 0.5
                  parallel:
                    linear 0.25 yoffset -50
                    linear 0.25 yoffset 50
                    linear 0.25 yoffset -50
                    linear 0.25 yoffset 50
                    linear 0.25 yoffset 0
                  parallel:
                    linear 0.25 zoom 1.1
                    pause 0.75
                    linear 0.25 zoom 1.0
                pause 1.5
                show layer master
                show isabelle angry with Dissolve(.5)
                window auto
                isabelle angry "That repugnant twat!"
                isabelle angry "Did she hurt you?"
                mc "..."
                isabelle concerned "No? Okay..."
                isabelle skeptical "But she did tie you up."
                mc "..."
                isabelle concerned "Have you lost the ability to speak?"
                mc "..."
                isabelle concerned "Yeah, I'd be speechless too..."
                isabelle neutral  "Okay, you look fine in spite of everything."
                isabelle neutral "Let me talk to the principal. In the meantime, go and have a cup of tea in the cafeteria."
                window hide
                show isabelle neutral at disappear_to_right
                pause 1.5
                window auto
                show items kate_socks_dirty:
                  xalign 0.5 yalign 1.0 rotate 230 zoom 0.0
                  ease_back 0.75 zoom 1.5 xalign 0.3 yalign 0.9 rotate 30
                mc "Blegh!" with vpunch
                show items kate_socks_dirty:
                  linear 0.25 alpha 0.0
                $mc.add_item("kate_socks_dirty")
                "Not sure why, but I feel like [isabelle] would've exploded if she learned about the socks..."
                $mc.love+=1
                "Some things are best kept from her."
              "Spit out [kate]'s socks":
                $quest.kate_fate["spit_sock"] = True
                show isabelle skeptical at move_to(.6)
                show items kate_socks_dirty behind isabelle:
                  xalign 0.5 yalign 1.0 rotate 230 zoom 0.0
                  ease_back 0.75 zoom 1.5 xalign 0.3 yalign 0.9 rotate 30
                pause 0.25
                show isabelle skeptical at move_to(.7)
                mc "Blegh!" with vpunch
                isabelle concerned_left "What the hell is that?"
                mc "[kate]'s socks..."
                isabelle afraid "That is entirely unacceptable!"
                isabelle afraid "Give me those! I'm taking this to the principal!"
                mc "It won't accomplish anything..."
                mc "[kate] has immunity from the school board thanks to her dad."
                isabelle cringe "Bloody American corruption..."
                isabelle cringe "She won't get away with this. Not this time."
                mc "What are you going to do?"
                isabelle skeptical "Remember the plan. I'll take care of this incident."
                isabelle skeptical "She'll get her comeuppance."
                window hide
                show isabelle skeptical at move_to(.4)
                pause 0.5
                show items kate_socks_dirty:
                  linear 0.25 alpha 0.0
                pause 0.25
                show isabelle skeptical at disappear_to_right
                window auto
                "Hmm... [isabelle] was very quick to leave and go after [kate]..."
                "It's almost as if she cares more about getting revenge than making sure I'm okay."
                "Feels a bit shitty, but perhaps she has a good reason for it?"
            $quest.kate_fate.finish()
            return
      "\"...\"":
        mc "..."
        kate cheerleader_feet_nofeet_kate_confident_phoneground "I'll take your silence as a statement of approval."
        kate cheerleader_feet_nofeet_kate_laughing_phoneground "You liked it, didn't you?"
        kate cheerleader_feet_nofeet_kate_confident_phoneground "I can see it in your teary eyes — you love this treatment."
        kate cheerleader_feet_nofeet_kate_confident_phoneground "Luckily for you, I have a lot more in store for you this year."
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "I'm going to dress you up, make you trot around in heels and short skirts."
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "I will make you clean and cook and be a little maid for me."
        kate cheerleader_feet_nofeet_kate_smile_phoneground "And if you're good, maybe I'll put a collar on you..."
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "I'm going to take the socks out now, and if you drool on my hand, there'll be hell to pay."
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "Is that understood?"
        mc "..."
        kate cheerleader_feet_nofeet_kate_neutral_phoneground "Well then..."
        window hide
        show black with Dissolve(.5)
        pause 1.0
        hide black
        show kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground
        with Dissolve(.5)
        window auto
        kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground "How's that?"
        kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground "Feeling properly disciplined?"
        menu(side="middle"):
          extend ""
          "\"Yes, ma'am... I'm sorry for stepping out of line.\"":
            mc "Yes, ma'am... I'm sorry for stepping out of line."
            $kate.lust+=1
            kate cheerleader_feet_nofeet_kate_holdingsock_laughing_phoneground "That's what I like to hear!"
            kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground "Maybe you just need some proper training."
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "Maybe I'll be extra tough on you this year..."
            mc "That, err... won't be necessary."
            kate cheerleader_feet_nofeet_kate_holdingsock_neutral_phoneground "We'll see, won't we?"
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "If you behave, then perhaps you're right."
          "\"You're going to regret this.\"":
            mc "You're going to regret this."
            $kate.lust-=1
            kate cheerleader_feet_nofeet_kate_holdingsock_laughing_phoneground "We'll see about that!"
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "From where I'm standing, I'm not all too worried."
            kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "I mean, look at you — all tied up at my feet."
            kate cheerleader_feet_nofeet_kate_holdingsock_confident_phoneground "Yeah, I'm not worried..."
    mc "Untie me now."
    kate cheerleader_feet_nofeet_kate_holdingsock_neutral_phoneground "Ask nicely."
    "Ugh... she's going to leave me here again, isn't she?"
    mc "Untie me, please..."
    kate cheerleader_feet_nofeet_kate_holdingsock_laughing_phoneground "That wasn't so hard, was it?"
    kate cheerleader_feet_nofeet_kate_holdingsock_laughing_phoneground "Imagine if you'd always be this courteous."
    kate cheerleader_feet_nofeet_kate_holdingsock_smile_phoneground "Very well..."
  elif quest.kate_fate["ballgag"]:
    mc "{i}Mmmph.{/}"
    kate cheerleader_feet_nofeet_kate_neutral_phoneground "I hope that means \"thank you.\""
    kate cheerleader_feet_nofeet_kate_neutral_phoneground "If it's not, then the next words that come out of your mouth better be.{space=-40}"
    window hide
    show black with Dissolve(.5)
    pause 1.0
    hide black
    show kate cheerleader_feet_nofeet_kate_confident_ballgag_phoneground
    with Dissolve(.5)
    window auto
    kate cheerleader_feet_nofeet_kate_confident_ballgag_phoneground "There you go! Let's hear it."
    menu(side="middle"):
      extend ""
      "\"Thank you, ma'am...\"":
        mc "Thank you, ma'am..."
        $kate.lust+=1
        kate cheerleader_feet_nofeet_kate_smile_ballgag_phoneground "That's the right attitude."
        kate cheerleader_feet_nofeet_kate_smile_ballgag_phoneground "As long as you know your place, I'll give you what you deserve."
        "That... sounds ominous, but in a sort of twisted hot way..."
        "Fuck! Why am I such a sucker for [kate]?!"
        "Damn it. I need to push back, else she'll walk all over me..."
      "\"This year will be different. Mark my words.\"":
        mc "This year will be different. Mark my words."
        $kate.lust-=1
        kate cheerleader_feet_nofeet_kate_laughing_ballgag_phoneground "Oh, it will be different! Very different."
        "Shit... that doesn't sound good."
        "She has that look in her eyes — conviction and excitement."
        "A deadly combination when it comes to [kate]."
        "But if I back down now, she'll think I'm a pussy..."
    mc "Untie me now."
    kate cheerleader_feet_nofeet_kate_neutral_ballgag_phoneground "Ask nicely."
    "Ugh... she's going to leave me here again, isn't she?"
    mc "Untie me, please..."
    kate cheerleader_feet_nofeet_kate_laughing_ballgag_phoneground "That wasn't so hard, was it?"
    kate cheerleader_feet_nofeet_kate_laughing_ballgag_phoneground "Imagine if you'd always be this courteous."
    kate cheerleader_feet_nofeet_kate_smile_ballgag_phoneground "Very well..."
  else:
    mc "Untie me now."
    kate cheerleader_feet_nofeet_kate_neutral_phoneground "Ask nicely."
    "Ugh... she's going to leave me here again, isn't she?"
    mc "Untie me, please..."
    kate cheerleader_feet_nofeet_kate_laughing_phoneground "That wasn't so hard, was it?"
    kate cheerleader_feet_nofeet_kate_laughing_phoneground "Imagine if you'd always be this courteous."
    kate cheerleader_feet_nofeet_kate_smile_phoneground "Very well..."

label quest_kate_fate_ending:
  window hide
  show black with Dissolve(.5)
  play sound "tying_knot"
  pause 4.0
  hide kate
  hide black
  show kate smile
  with Dissolve(.5)
  window auto
  kate smile "You don't look too worn down."
  kate laughing "Not more than usual, at least."
  mc "..."
  kate confident "I know that it's over, but you don't have to look so sad."
  kate confident "There's plenty of time left before graduation to do this again."
  kate neutral "See you around, pet."
  hide kate with Dissolve(.5)
  "Revenge or not — the question of the ages..."
  "Either way, it's best served with a cool head and icy veins."
  $quest.kate_fate.finish()
  return

label quest_kate_fate_rescue:
  show isabelle concerned with Dissolve(.5)
  isabelle concerned "What's wrong?"
  mc "I guess you could say I'm in a sticky situation..."
  show isabelle concerned at move_to(.25,1.0)
  show kate excited at appear_from_right(.75)
  kate excited "There you are!"
  isabelle skeptical "Lovely weather, minger."
  kate eyeroll "Ugh, you."
  isabelle skeptical "What's going on?"
  kate skeptical "None of your goddamn business."
  kate skeptical "[mc], let's go."
  mc "Err..."
  kate eyeroll "[mc]. I'm not going to tell you again."
  isabelle concerned "You don't have to do what she says."
  kate skeptical "No one asked for your input."
  isabelle neutral "No one asked you to be a complete tosser, but here we are."
  show isabelle neutral at move_to("left")
  show kate skeptical at move_to("right")
  menu(side="middle"):
    extend ""
    "?quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed@|{image=kate_photo_small}|\"I think I'm going to stay here with [isabelle].\"":
      show isabelle neutral at move_to(.25)
      show kate skeptical at move_to (.75)
      mc "I think I'm going to stay here with [isabelle]."
      $kate.lust-=1
      kate eyeroll "Wrong answer."
      isabelle skeptical "Oh, bugger off, [kate]. Neither of us like your company."
      kate excited "Cute."
      kate excited "I'm so looking forward to putting you in your place."
      isabelle skeptical "And I'm looking forward to a decrease in overall bollocks once you leave."
      kate annoyed "You'll regret this, bitch."
      show isabelle skeptical at move_to(.5,1.0)
      show kate annoyed at disappear_to_right
      pause 1.0
      mc "Thanks for sticking up for me..."
      $isabelle.lust+=1
      isabelle excited "Anytime, mate! I'm always in the mood for sticking it to that trollop."
      isabelle neutral "Anyway, it's time for tea."
      isabelle neutral "Be careful, [mc]."
      show isabelle neutral at disappear_to_right
      "I was definitely lucky that [isabelle] was here."
      "Who knows what could've happened otherwise?"
      $quest.kate_fate.advance("escaped")
    "?quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed@|{image=isabelle_photo_small}|\"I just wanted to get [isabelle]'s thoughts on the school's gym equipment.\"":
      show isabelle neutral at move_to(.25)
      show kate skeptical at move_to (.75)
      mc "I just wanted to get [isabelle]'s thoughts on the school's gym equipment."
      kate skeptical "..."
      isabelle concerned "I believe they're quite decent. Why?"
      mc "Have you looked at them all?"
      mc "I think it would be cool with a comparison to other schools you've been to."
      mc "She'd probably be more fit for what you had in mind, [kate]..."
      kate excited "I see what you're doing."
      kate excited "Very well. [isabelle], care to join me and the squad in the gym?"
      isabelle skeptical "What's with the sudden change in tone?"
      kate eyeroll "People change."
      isabelle skeptical "Do they? I have my doubts."
      show isabelle skeptical at move_to("left")
      show kate eyeroll at move_to("right")
      menu(side="middle"):
        extend ""
        "Make your escape while they argue":
          show isabelle skeptical at move_to(.25)
          show kate eyeroll at move_to(.75)
          "No need to test my luck anymore here. This is where I disappear."
          "Become one with the shadows and the smell of lemon soap."
          window hide
          $game.location = school_ground_floor
          hide isabelle
          hide kate
          with Dissolve(.25)
          pause 0.25
          window auto
          "Perhaps this is the year I slip through [kate]'s fingers and carve my own fate..."
          $quest.kate_fate.advance("escaped")
        "Stay and help [kate]":
          show isabelle skeptical at move_to(.25)
          show kate eyeroll at move_to(.75)
          mc "I think you should go, [isabelle]."
          mc "Maybe you can patch things up with [kate]?"
          isabelle neutral "I'll come if she apologizes."
          kate skeptical "Are you serious?"
          isabelle excited "Quite serious."
          kate eyeroll "Fine... I'm sorry."
          isabelle laughing "Thank you, [kate]! I appreciate it."
          isabelle confident "I'm going to have a tea now. Laters."
          show isabelle confident at disappear_to_right
          show kate eyeroll at move_to(.5,1.0)
          pause 0.0
          kate angry "That little bitch!"
          "Shit. Now [kate] is mad for real."
          show kate angry at move_to(.25)
          menu(side="right"):
            extend ""
            "\"She got you good.\"":
              show kate angry at move_to(.5)
              mc "She got you good."
              kate neutral "What did you say?"
              mc "Uh, nothing..."
              kate confident "Oh, yeah?"
              kate confident "To me it sounded like you wanted trouble."
              mc "Not really..."
              jump quest_kate_fate_fight_surrender
            "Run":
              play sound "fast_whoosh"
              show kate angry at move_to(.75)
              mc "Meep! Meep!" with hpunch
              $quest.kate_fate.advance("run")
              hide kate with Dissolve(.5)
  return

label quest_kate_fate_wrong_int:
  show kate confident at appear_from_right
  kate confident "Thought you could hide among the art nerds?"
  kate confident "Nerd, sure. But there's nothing artistic about you."
  "Shit. Fuck. Cock."
  kate neutral "You thought you could just insult me and run off?"
  mc "I don't want any trouble..."
  kate neutral "Neither do I, but you insist on being troublesome."
  jump quest_kate_fate_fight_surrender

label quest_kate_fate_dead_end_kate:
  "..."
  "I think I can leave now..."
  play sound "<from 2.7 to 4.2>door_breaking_in"
  pause(1.5)
  show kate neutral with Dissolve(.5)
  kate neutral "Not so fast."
  mc "Crap."
  kate confident "Crap, indeed."
  kate confident "Thought you could get away, didn't you?"
  mc "I was, err... just looking for something to read..."
  kate confident "Yeah?"
  mc "Yeah, I found this title..."
  show kate confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Survival of the Fittest.\"":
      show kate confident at move_to(.5)
      mc "Survival of the Fittest."
      kate laughing "So, are you having an existential crisis now?"
      mc "Very funny."
      kate smile "Come on, you're hardly fit. In fact, you're something of a misfit."
      kate smile "And this is a dead end."
      jump quest_kate_fate_fight_surrender
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"The Taming of the Shrew.\"":
      show kate confident at move_to(.5)
      mc "The Taming of the Shrew."
      kate eyeroll "Isn't that a bit above your paygrade?"
      mc "You know, I used to think so too..."
      mc "Now, I'm not so sure. It might be just right."
      kate skeptical "No one likes that story."
      mc "Actually, I'm quite liking it so far."
      kate cringe "..."
      window hide
      show kate cringe at disappear_to_right
      pause 1.0
      window auto
      "Why did she leave like that?"
      "After chasing me down here, I expected a serious confrontation."
      "Strange..."
      "Well, I'm glad to be off the hook at least!"
      $quest.kate_fate.advance("escaped")
      return

label quest_kate_fate_hunt_first_hall_east_door_pool:
  "I'd rather not play drowning games with [kate] and the cheerleader squad..."
  return

label quest_kate_fate_hunt_first_hall_east_door_bathroom:
  "Hiding in a bathroom is how you get your head flushed in the toilet."
  return

label quest_kate_fate_hunt_first_hall_east_door_locker:
  "That's a dead end... and I'll probably end up inside a locker."
  return
