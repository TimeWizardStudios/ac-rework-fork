init python:
  q_kate_blowjob_dream_interact_lines = [
  "I never had a chance...",
  "Let me out...",
  "There's so many people here...",
  "I hope the little girl doesn't see it...",
  "I can't stay here any longer.",
  "I better tie my shoelaces. Tripping now would be embarrassing.",
  "I already threw up."
  ]

  q_kate_blowjob_dream_investigate_lines = [
  "It's red. Red is good.",
  # "It's late. Out of all days, why is it late today?",
  "It's late. Out of all days,\nwhy is it late today?",
  "No one can say I didn't try.",
  "Please, don't cry.",
  "It's finally here.",
  # "Why is time moving so slowly, but my heart so fast?",
  "Why is time moving so slowly,\nbut my heart so fast?",
  "Where did it all go wrong?"
  ]

label kate_blowjob_dream_random_interact_locked_location:
  "Everytime I go inside, I end up where I started."
  return

label kate_blowjob_dream_random_interact:
  $ line = random.choice(q_kate_blowjob_dream_interact_lines)
  "[line]"
  return

label quest_kate_blowjob_dream_mrsl:
  show mrsl neutral with Dissolve(.5)
  mrsl neutral "[mc], there you are!"
  mrsl thinking "How did the tour go?"
  show mrsl thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Apart from [isabelle] and [kate] almost ripping each other's heads off, pretty good.\"":
      show mrsl thinking at move_to(.5)
      mc "Apart from [isabelle] and [kate] almost ripping each other's heads off, pretty good."
      mrsl surprised "Oh, dear. That's no good."
      mc "Not as eventless as I expected."
      mrsl skeptical "Can you tell me what happened?"
      show mrsl skeptical at move_to(.75)
      menu(side="left"):
        extend ""
        "\"[kate] was picking on [isabelle].\"":
          show mrsl skeptical at move_to(.5)
          $isabelle.lust+=1
          mc "[kate] was picking on [isabelle]."
          mrsl blush "[kate] can be a little intense sometimes, but I'm sure it all comes from a good place."
          mc "Right..."
          "I'm sure it all comes from her rotten heart."
          mrsl smile "I know you haven't always seen eye to eye, but it's a new school year."
          mrsl smile "Who knows what can happen?"
        "\"[isabelle] was rude.\"":
          show mrsl skeptical at move_to(.5)
          $kate.lust+=1
          mc "[isabelle] was rude."
          mrsl surprised "That's odd! I didn't peg her for the rude type..."
          mc "She says she's English, but she's probably a hot-blooded Scot."
          mrsl thinking "I'd be careful to judge a book by its cover, if I were you. Maybe she had a bad day."
          mrsl neutral "You should give her another chance."
          "Hmm... that would be the fair thing to do, considering my own circumstances."
          "Then again, [kate]."
        "\"They were both being catty.\"":
          show mrsl skeptical at move_to(.5)
          mc "They were both being catty."
          mc "I don't care much for such nonsense."
          $mrsl.lust+=1
          mrsl excited "That is very mature of you."
          mrsl concerned "But I think you should try to get involved in that conflict more if you can."
          mrsl concerned "Maybe you can stop it."
          mc "You're probably right. Thanks for the advice!"
          mrsl excited "Don't mention it!"
    "\"[lindsey] had an accident.\"":
      show mrsl thinking at move_to(.5)
      mc "[lindsey] had an accident."
      mrsl surprised "Oh, no! That poor dear!"
      mc "Got her to the [nurse], safe and sound. She'll be okay."
      if quest.lindsey_nurse["carried"] and not lindsey["romance_disabled"]:
        $lindsey.love+=1
        mc "I think she likes me!"
        mrsl smile "I'm happy to hear it."
        mrsl smile "Young passion is by far the sweetest."
      elif lindsey["romance_disabled"]:
        mc "She probably hates me, though..."
        mrsl laughing "High school girls can be like storm clouds."
        mrsl flirty "You just wait until the sun peeks through."
        mc "Thanks for the advice."
        mrsl excited "You're very welcome."
    "\"I met [jacklyn] for the first time.\"":
      show mrsl thinking at move_to(.5)
      mc "I met [jacklyn] for the first time."
      $jacklyn.lust+=1
      mrsl excited "Ah, [jacklyn]... what a sweet young woman! We're lucky to have her filling in for Mrs. Bloomer."
      mc "Extremely lucky!"
      mrsl excited "I'm glad you agree."
      mrsl annoyed "I had some difficulty convincing the principal that [jacklyn] is the right woman for that job."
      "[jo] never really approved of women dressing provocatively, which is quite hypocritical of her."
      "[flora] probably suffers the most from that. She barely has any skirts, especially after one got stolen."
  mrsl excited "In any case, I just wanted to commend you for stepping out of your comfort zone."
  mrsl concerned "I know you've always had a hard time socially, so this was really good."
  mrsl concerned "And as thanks for showing [isabelle] around, I'd like you to have this badge."
  $mc.add_item("courage_badge")
  mc "What is it?"
  mrsl sad "I know what social anxiety is like. I hid underneath five layers of clothing for years."
  mrsl sad "By the end of last semester, I had enough and sought help."
  mrsl smile "This is the badge I got for stepping out of my comfort zone for the first time, and now it's yours."
  "That explains why she ditched the cardigan and the wool socks. Man, if she could turn herself around like this, maybe it's not over for me."
  mc "Thank you. I appreciate it."
  mrsl smile "You're very welcome. I hope it'll bring you the same confidence that it brought me."
  hide mrsl with Dissolve(.5)
  "That's so nice of her. She always stared right through me before. Something's definitely changed in her eyes. They're sparkling with a newfound light."
  "She knew about my social anxiety too. If her new style hadn't already made her my favorite teacher, this act of kindness certainly would have."
  "This day has far exceeded my expectations."
  $quest.kate_blowjob_dream.start()
  return

label quest_kate_blowjob_dream_home_bedroom:
  "I never won anything before, but this badge certainly feels like a trophy. It warms my heart and gives me new hope."
  "It deserves to be on display..."
  window hide
  $home_bedroom["courage_badge"] = True
  $mc.remove_item("courage_badge")
  pause 0.25
  window auto
  "Ah, that looks nice. Perfect!"
  $quest.kate_blowjob_dream.advance()
  return

label quest_kate_blowjob_dream_home_bedroom_flora_knocking:
  ## Door knock
  play sound "<from 0.0 to 1.0>door_knock"
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  flora "[mc]?"
  flora "Hello?"
  flora "..."
  ## Door knock
  play sound "<from 4.9 to 6.5>door_knock"
  show black onlayer screens zorder 4
  flora "[mc]? Are you up?"
  mc "Mmm...?"
  mc "[flora]? What time is it?"
  flora "Open up!"
  show black onlayer screens zorder 100
  pause 0.5
  $quest.kate_blowjob_dream.advance("open_door")
# $home_bedroom["alarm"] = "beeping"
  $mc["focus"] = "kate_blowjob_dream"
  $set_dialog_mode()
  hide black onlayer screens with Dissolve(.5)
  return

label kate_quest_kate_blowjob_dream_school:
  show kate neutral with Dissolve(.5)
  mc "[kate]?"
  kate neutral "Hm?"
  kate laughing "Oh, nice outfit!"
  if quest.kate_blowjob_dream["dress_choice"] == "skirt":
    kate laughing "I always knew you were a cross-dressing little sissy."
    mc "I didn't have anything else to put on..."
    kate confident "I think it's fitting."
    "Damn it, this is too humiliating..."
  if quest.kate_blowjob_dream["dress_choice"] == "kimono":
    kate confident "What is it? Some nerd shit, right?"
    mc "The kimono is a traditional Japanese garb."
    kate eyeroll "Oh, you're one of those people."
    mc "What's that supposed to mean?"
    kate excited "Don't play dumb with me, weeb."
  if quest.kate_blowjob_dream["dress_choice"] == "nothing":
    kate laughing "Is it like an emperor's new clothes thing, but starring a peasant instead?"
    mc "I was late and didn't have anything to put on..."
    kate gushing "Aw! Look at the little guy! Adorable!"
    "Ugh. Going naked was a mistake. What was I thinking?"
  kate excited "Well, this was entertaining, but I'm getting bored of you already."
  mc "Wait!"
  mc "What's going on? Where is everyone?"
  kate laughing "Didn't you hear?"
  mc "Hear what?"
  kate confident "Something bad happened to the new girl."
  mc "[isabelle]?"
  kate confident "I guess she had an accident. Very unfortunate."
  kate smile "They closed the school for the day."
  mc "Is she okay?"
  kate confident "Don't know. Don't care."
  mc "Why are you always being such a bitch?"
  kate neutral "Watch your tone when you talk to me, loser."
  mc "No! I've had enough of you and your fucking abuse!"
  kate surprised "..."
  mc "You've insulted me for the last time..."
  show kate surprised at move_to(.75)
  $quest.kate_blowjob_dream["bj_received"] = "yes"
  $unlock_replay("kate_apology")
  menu(side="left"):
    extend ""
    "?mc.strength>=1@[mc.strength]/1|{image=stats str}|\"There's no one else here.\nJust us. Alone.\"":
      show kate surprised at move_to(.5)
      mc "There's no one else here. Just us. Alone."
      kate sad "Umm... w-what are you doing?"
      show black with Dissolve(2)
      $set_dialog_mode("default_no_bg")
      mc "What I should've done years ago."
      kate "Get away from me! You can't—"
      kate "Oww! Let go!" with vpunch
      mc "Shut the hell up, whore."
      jump kate_quest_kate_blowjob_dream_school_rough_bj
    "\"I want you to apologize\nand then leave me alone.\"":
      show kate surprised at move_to(.5)
      mc "I want you to apologize and then leave me alone."
      kate sad "..."
      "[kate] looks concerned. She clearly didn't expect such an outburst."
      "Ice in my veins. Steel in my eyes."
      mc "I'm waiting."
      kate sad "Err..."
  kate sad "I guess I've been feeling guilty for how I've treated you the past few years."
  kate sad "So, I'd like to apologize."
  mc "Apologize how?"
  kate thinking "..."
  "This is a new side of her. Never thought I'd see [kate] embarrassed."
  kate blush "Are you ready?"
  mc "Ready for what?"
  kate KateKneelingPose01 "My apology... I never half-ass things."
  "Huh? That's... that's not what I expected in a million years. Not from [kate]!"
  "For years, she's paraded around in her high heels, forcing people to look up at her."
  "Seeing her on her knees has always been a fantasy. The evil queen humbled and begging for forgiveness."
  "Eagerly awaiting her judgement. Ready to wrap her lips around me. Ready to take me deep in her throat."
  "The guilt must've caught up with her at long last. Maybe seeing me together with [isabelle] earlier made her realize her mistakes."
  "Maybe her looking like this is all I need to move on from the past? But let's be honest, who would say no to a blowjob from [kate]?"
  menu(side="far_left"):
    extend ""
    "\"Sorry, but I don't trust your mouth\nwith my dick.\"":
#     $mc.love+=3
      $game.notify(None,"{color=#090}+{/color}|{image=stats love_3}",5.0) ## Fake stat gain since this interaction is just a dream
      mc "Sorry, but I don't trust your mouth with my dick."
      $unlock_stat_perk("love14")
      kate KateKneelingPose01 "What do you mean?"
      ## Flicker transition
      stop music fadeout 0.5
      call scene_flicker(2)
      mc "I don't want this."
      kate KateKneelingPose01 "What do you mean?"
      ## Flicker transition
      call scene_flicker(3)
      mc "..."
      kate KateKneelingPose01 "What do you mean, [mc]?"
      #SFX: Flickering screen
      #show KKP01D
      ## Flicker transition together with stinger at the end.
      call scene_flicker(4)
      play sound "stinger" fadein 0.1
      show kate KateKneelingPose01D with hpunch
      kate KateKneelingPose01D "WHAT DO YOU MEAN?"
      kate KateKneelingPose01D "WHAT... DO... YOU... MEAN?" #Make distorted text if possible.
      jump kate_quest_kate_blowjob_dream_school_nobj_ending
    "\"Oh... I guess I wouldn't mind that...\"":
      $unlock_stat_perk("lust14")
#     $mc.lust+=1
      $game.notify(None,"{color=#090}+{/color}|{image=stats lust}",5.0) ## Fake stat gain since this interaction is just a dream
      mc "Oh... I guess I wouldn't mind that..."
      kate KateKneelingPose01 "I know that. I promise I'll make it good."
      mc "Get to it, then."
      show kate KateKneelingPose02 with qdis
      kate KateKneelingPose02 "Okay, just relax and let me do all the work."
      show kate KateBJPose01 with qdis
      show kate KateBJPose01x with dissolve
      mc "Oh... my... god..."
      "The soft touch of her lips and tongue sends sparks through my veins."
      "The muscles of my abdomen spasming in excitement."
      "To be inside a girl's mouth... that's the stuff of fairytales and porn."
      "A blowjob means so much more than regular sex. It's the ultimate act of one-sided pleasure."
      "A gift that the average guy spends months fantasizing about, and only receives on anniversaries and birthdays from his unenthusiastic girlfriend."
      "Chad, of course, gets it on a daily basis. Fucking Chad and his perpetually sucked dick."
      "God... her lips wrap so tightly around me..."
      "Each lick — prickling shivers and lusty heat at the same time. She must've done this before."
      "The teasing. The tip of her tongue, dancing around the head of my cock, poking at the hole. Nudging it."
      "Her tongue has only ever spat insults my way. Who knew it could be so gentle and sweet?"
      "Who knew she could wrap it around my shaft, trace it around the hood, nudge the hole with such kindness!"
      "Up and down, teasing my urethra..."
      "Entering it; licking up every dribble of piss, cum, and sweat."
      "And happily doing so."
      "With those big blue eyes, staring up at me. "
      "Her soft, silky hair running through my fingers, softer than anything I've ever felt before."
      "The warmth from her temple, radiating up my arm and into my chest."
      kate KateBJPose01x "Mmmm..."
      kate KateBJPose01x "I'm so sorry..."
      kate KateBJPose01x "Please forgive me..."
      show kate KateBJPose01b with qdis
      kate KateBJPose01b "{i}*Glergh*{/}"
      mc "Hnnngh!"
      show kate KateBJPose01 with qdis
      kate KateBJPose01 "Mmmm... you like it?"
      "The only thing outshining her eyes is her sparkling smile."
      "The way she's looking up at me..."
      "She makes me feel like a conqueror. "
      "The lord of her mouth."
      mc "Y-yes!"
      show kate KateBJPose01bx with qdis
      "The wet warmth of her mouth. The light suction. The individual grooves and ridges of her little mouth."
      "Airtight, her lips squeeze around me. Sucking at my soul."
      "Her tongue keeps swirling around the head of my dick. My knees feel weak."
      "There's nothing quite like this. Nothing that springs to mind, at least. My first blowjob... and it's from [kate]!"
      "If I had any friends, they would never believe this."
      "She shifts my dick around, allowing me to explore every inch of her mouth. The insides of her cheeks, the roof of her mouth."
      "Teasing me with the opening of her throat. Oh god, give me that throat!"
      mc "K-keep going!"
      kate KateBJPose01 "So demanding!"
      show kate KateBJPose01b with qdis
      mc "Deeper."
      show kate KateBJPose01bx with qdis
      mc "Oh, much better!"
      show kate KateBJPose01b with dissolve
      show kate KateBJPose01bx with dissolve
      "My grip on her hair gets tighter."
      show kate KateBJPose01b with dissolve
      show kate KateBJPose01bx with dissolve
      show kate KateBJPose01b with dissolve
      "Using her as nothing but a cock sleeve as I thrust into her and slam my head against her throat."
      show kate KateBJPose01bx with dissolve
      "Oh, god. Her throat is so tight and soft. Squeezing. Pulling. Sucking. Her nails digging into my thighs."
      show kate KateBJPose01b with dissolve
      "Fuck, I can't... I'm going to..."
      menu(side="far_right"):
        extend ""
        "Finish on her face":
          show kate KateKneelingPose01a1 with fadehold
          mc "Sorry, I didn't mean to..."
          kate "It's fine... I deserved it."
          mc "Heh, I guess you did. Plus, I heard it's good for your skin!"
          kate "I don't know about that!"
          "[kate] is so cute like this. Not at all the big bad bully she usually is."
          "Maybe it's the—"
          jump kate_quest_kate_blowjob_dream_school_bj_ending
        "Finish in her mouth":
          show kate KateKneelingPose01a2 with fadehold
          kate "{i}*Gulp, gulp*{/}"
          kate "Ah!"
          mc "You, err... you didn't have to swallow, you know."
          kate "Spitting is quitting, [mc]. That's the rule."
          "Oh, god... [kate] is so confident even after sucking dick and swallowing cum!"
          "There's probably nothing that can faze—"
          jump kate_quest_kate_blowjob_dream_school_bj_ending
  return

label kate_quest_kate_blowjob_dream_school_rough_bj:
  $set_dialog_mode()
  show kate KateKneelingPose01x
  hide black with Dissolve(2)
  mc "What's wrong?"
  mc "Where's that smile of confidence?"
  kate KateKneelingPose01x "Screw you."
  mc "Years of abuse and humiliation..."
  mc "This is where it ends."
  kate KateKneelingPose01x "..."
  mc "You know what's coming, right?"
  show kate KateKneelingPose01x2 with Dissolve(.5)
  "There's something in her eyes. Even behind the anger and disgust. A combination of fear and excitement, perhaps?"
  "No one has ever challenged her like this. Even the jocks cower in fear when she gets angry."
  "If this were any other day, there'd be a group of them beating the snot out of me for even looking at her the wrong way."
  "Well, not today."
  "She knows it too. That twitch at the corner of her mouth, curling her lips in hatred."
  show kate KateBJPose01c with Dissolve(.5) #  show new hateful pose KateBJPose01c
  "The darkness in her eyes, pouring into the lines of her face, twisting it into a mask of disgust."
  "She's never going to plead with me. Not that it would matter."
  "She never cared when I was begging for mercy. She never cared when I cried."
  "She can give me the death-stare all she wants, it doesn't change anything."
  "There's nothing in this world that will stop me."
  "Today she'll get to know what it's like to suffer."
  "..."
  "I always wanted to feel what her hair is like. She probably spends hours getting it this soft and perfect."
  "Perfect for my grip. Tight. Forcing her forward. Down on my dick."
  show kate KateBJPose01x with Dissolve(.5)
  "There!"
  "There it is!"
  "Her tongue swirling around the head of my dick, coating it in her spit."
  "Her lips squeezing me softly."
  "The wet insides of her cheeks, massaging the tip of my cock."
  show kate KateBJPose01b with qdis
  show kate KateBJPose01x with qdis
  show kate KateBJPose01b with qdis
  show kate KateBJPose01x with qdis
  "The sharp danger of her teeth. The hard but silky top of her mouth. All the sensations that I've been deprived of."
  "All my life, the suffering has been unbearable. The sheer fucking injustice of it all."
  "How girls like her have always selfishly kept their mouths to themselves. Or slobbered over some jock's dick."
  "It's gross how some people just have to go all their lives without feeling a hot mouth around their dick."
  "Well, what is she tasting now?"
  "Hopefully, it's humiliation. That's the only flavor she deserves."
  "That and misery."
  show kate KateBJPose01b with qdis
  show kate KateBJPose01x with qdis
  show kate KateBJPose01b with qdis
  show kate KateBJPose01x with qdis
  "Her hair between my fingers is almost the best part — the control. It's what true power feels like."
  "Nothing quite like forcing your dick down the throat of someone you fucking hate."
  "She's keeping eye contact with her big blue eyes, making sure I'm happy. What a submissive thing to do."
  "Maybe she's finally starting to know her place."
  show kate KateBJPose01b with Dissolve(.5)
  mc "That's right, bitch. You're going to take it."
  "She makes me want to slap her so hard across the face that she won't need rouge for a month."
  "Fucking uppity whore who thinks she's better than everyone else."
  "Who thinks she can walk all over people."
  "Well, she'll only be walking her tongue down the shaft of my dick now."
  "Everyone will know she's nothing but a cocksucker."
  show kate KateBJPose01bx with qdis
  show kate KateBJPose01b with qdis
  show kate KateBJPose01bx with qdis
  "She'll choke on my dick until her face turns blue."
  "She'll take my load so deep in her throat that it comes out of her nose and eyes."
  "And the best part is she knows she deserves it. We both know that."
  "That's why she's trying so hard to please me."
  "Her tongue sliding up and down the bottom of my dick tells me she enjoys this."
  "The constant suction of her mouth and lips is her way of asking for forgiveness."
  "But that's simply too late."
  "..."
  show kate KateBJPose01b with qdis
  "That look."
  mc "Is that concern, [kate]?"
  "God, she must be worried about the hatred in my eyes."
  "And rightfully so, I'm going to take her all the way."
  show kate KateBJPose01bx with qdis
  show kate KateBJPose01b with qdis
  show kate KateBJPose01bx with qdis
  "A little deeper. Poking at the opening of her throat."
  "The fact that she's trying to keep me out of that tight passage only makes me want to conquer it more."
  "Her face turning red from the struggle just makes me hold her firmer. Show her even less mercy."
  "I love that little gasp every time my dick lines up with her throat and hits the back of her mouth."
  "She's scared I'll push myself deeper. With my tight grip on her hair, she wouldn't be able to put up much resistance."
  show kate KateBJPose01bx with qdis
  show kate KateBJPose01b with qdis
  show kate KateBJPose01bx with qdis
  "Maybe she knows it's coming. That's why she closed her eyes."
  "What thoughts are going through her head as she prepares for the inevitable?"
  "Is she already plotting her revenge or is she planning our wedding? "
  "It would be funny if this is the sort of thing it takes to break the ice queen."
  "Either way, it doesn't really matter. What matters is me invading her throat."
  "If she doesn't want to surrender it willingly, I have to take it by force."
  "Another hand on the back of her head. Increasing the power of my steel grip."
  show kate KateBJPose02 with Dissolve(.5)
  kate "Ggghh..."
  "This is... the most... satisfying thing... ever."
  "To just push my dick past her tonsils.  Slam it into her throat!"
  "Make her swallow it whole. Cork her up."
  "She's said so much shit to me over the years. Now there's just gurgles of discomfort."
  "My fingers tighten on her hair. There's no way I'm letting go now."
  "It's a steel grip practiced over years of miserable masturbation."
  "She tries to wiggle her head back and forth, but I've got her locked down completely."
  "Her nails dig into my legs, but that pain is drowned out by the sheer pleasure."
  "Her throat keeps spasming against my dick."
  "All her strength to get away isn't enough. We both know it. It's just a reflex at this point."
  show kate KateBJPose02b with Dissolve(.5)
  "There it is. The look of pleading resignation."
  "She has finally come to terms with who decides her fate."
  "That fucking bitch. After everything she's done. How dare she plead for mercy now?"
  "If my hands weren't welded to the back of her skull, I'd slap her for the sheer insolence."
  "Her throat keeps spasming around my cock as her body shakes."
  "Still weakly trying to break loose and pull my dick out."
  "She never expected me to go this far."
  "She probably never thought she'd have a dick this deep down her throat either."
  mc "You're not going anywhere until I'm done."
  "The only thing is... I'm not done. There's still another inch left."
  mc "Here it comes."
  show kate KateBJPose02c with qdis
  "There! All of it — every last inch — buried in her throat."
  "What a feeling when it finally slid home!"
  "Her nose against my pelvis, her chin against my balls."
  "Her tight hot throat wrapped around me. Squeezing me erratically. Tears of panic rising in her eyes."
  "She's nothing but a cocksleeve. The queen of Newfall High is nothing but a milking machine for my balls."
  "Hundreds of tiny muscles trying to push me out. Massaging me."
  "My grip tightens around her head. Her nails leave nasty marks on my wrists and thighs. But she's not getting away from this."
  "The true retribution for everything she's done."
  "Choke, bitch!"
  "My whole body wants me to go even deeper, but it's not physically possible."
  "The tip of my dick is probably poking at the opening of her stomach."
  "If only my dick were a little longer, I'd let her taste her last meal as I pull out."
  "Maybe if I pull out a bit and enter her with force."
  show kate KateBJPose02b with qdis
  show kate KateBJPose02c with qdis
  show kate KateBJPose02b with qdis
  show kate KateBJPose02c with qdis
  mc "That's it. Stop struggling."
  kate "{i}*Gurgle*{/}"
  "She's finally relaxing, allowing me to fuck her throat as deeply as I want."
  "Despite her obvious lack of oxygen, she sticks her tongue out, licking my balls."
  "Her mouth tightens even more. She really wants my jizz... or perhaps just a breath of fresh air."
  "It's a battle."
  "For all the women that have hurt me or denied me any sort of affection in the past, [kate]'s going to suffer."
  "She's going to please me for as long as humanly possible."
  show kate KateBJPose02d with qdis
  "God, that's so hot. Her eyeballs rolling back into her skull. The lack of oxygen is finally hitting her in full."
  "Her throat is constricting my cock like never before!"
  "It's like all those muscles are sucking the seminal fluid right out of my prostate and emptying my balls of jizz at the same time!"
  "Oh, shit! I'm going to..."
  "Oh, fuck!"
  "Oh my god!"
  "Ohhhh!"
  "I'm going to..."
  "I'm going to..."
  menu(side="far_right"):
    extend ""
    "Paint her face":
      #Cut to black
      #Fade in
      show kate KateKneelingPose01b with fadehold
      kate KateKneelingPose01b "{i}*Cough, cough*{/}"
      kate KateKneelingPose01b "Screw you, [mc]..."
      mc "Oh, shut up."
      mc "You look hot like that..."
      mc "On your knees..."
      mc "With your face covered in—"
      jump kate_quest_kate_blowjob_dream_school_bj_ending
    "Make her swallow":
      #Cut to black
      #Fade in
      show kate KateKneelingPose01c with fadehold
      kate KateKneelingPose01c "{i}*Cough, cough*{/}"
      kate KateKneelingPose01c "You fucking psycho..."
      mc "What's wrong? It looked like you enjoyed it."
      kate KateKneelingPose01c "{i}*Cough, cough*{/}"
      mc "Just give me a minute and I'll be ready for another—"
      jump kate_quest_kate_blowjob_dream_school_bj_ending
  return

label kate_quest_kate_blowjob_dream_school_bj_ending:
  #SFX: Alarm clock
  #$home_bedroom["alarm"]="beeping"
  play music "alarm"
  mc "...?"
  show black with fadehold
  hide kate
  $quest.kate_blowjob_dream.advance("awake")
  while not game.hour == 7:
    $game.advance()
  $game.day = quest.kate_blowjob_dream["dream_start"]
  jump goto_home_bedroom
  return

  label kate_quest_kate_blowjob_dream_school_nobj_ending:
  #SFX: Alarm clock
  #$home_bedroom["alarm"]="beeping"
  play music "alarm"
  $quest.kate_blowjob_dream["bj_received"] = "no"
  mc "...?"
  $set_dialog_mode("default_no_bg")
  hide kate
  show black
  with fadehold
  $quest.kate_blowjob_dream.advance("awake")
  while not game.hour == 7:
    $game.advance()
  $game.day = quest.kate_blowjob_dream["dream_start"]
  jump goto_home_bedroom
  return

label quest_kate_blowjob_dream_home_bedroom_awake:
  if quest.kate_blowjob_dream["bj_received"] == "no":
    #Black screen
    #SFX: Alarm clock
    mc "Thank god. It was just a nightmare."
    $ quest.kate_blowjob_dream.advance("alarm")
  elif quest.kate_blowjob_dream["bj_received"] == "yes":
    #Black screen
    #SFX: Alarm clock
    "What? No way!"
    "Of course, it was a dream!"
    "Fuck me. That clock!"
    $ quest.kate_blowjob_dream.advance("alarm")
  return

label home_bedroom_alarm_interact_kate_blowjob_dream_alarm_bj_received:
  "That dream, though. It felt more real than anything I've dreamt before."
  "More vivid somehow. It's almost as if [kate] was here."
  "But err... of course, she wasn't. That would be ridiculous."
  "Really thought I finally got a girl's lips to make contact with me."
  "Really thought my moment of redemption had come... "
  "Ugh, why do I feel so drained?"
  $school_gym["exclusive"] = 0
  $ quest.kate_blowjob_dream.finish()
  return

label home_bedroom_alarm_interact_kate_blowjob_dream_alarm_not_bj_received:
  "That was one messed up nightmare..."
  "Probably my brain trying to tempt me, only to rip the rug out from under me."
  "Somehow, it felt more vivid than a dream, though. Like [kate] wanted my soul for not letting her eat my dick."
  "Fuck that. There's no way I'm sticking my dick in a demon."
  "My soul be damned, but trying to be a better person certainly starts from within. "
  "It's not merely your choices and actions toward other people..."
  "It's the choices you make for yourself and how you approach every situation. "
  "At least, that's what Gandhi said... or was it Ash Ketchum? "
  "Hmm... either way."
  $school_gym["exclusive"] = 0
  $ quest.kate_blowjob_dream.finish()
  return

## Helper label for the nightmare flicker
label scene_flicker(repetition=1, with_sound=True):
  if with_sound:
    play sound "<from 0.4 to 1.0>light_flicker" fadein 0.5 loop

  $ i = 1
  while i <= repetition:
    show black with Dissolve(0.01)
    hide black with Dissolve(0.01)
    $ i += 1

  $ del i

  if with_sound:
    stop sound fadeout 0.5

  return
