label kate_quest_search_for_nurse_start:
  show kate neutral with Dissolve(.5)
  kate neutral "Excuse you. Why are you standing so near me?"
  show kate neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Sorry, I didn't mean to.\"":
      show kate neutral at move_to(.5)
      mc "Sorry, I didn't mean to."
      $kate.lust+=1
      kate smile "Well, at least you know it's appropriate to apologize."
      kate neutral "I think we need to start with a ten feet rule."
      mc "What happens if I break that rule?"
      kate confident "Severe punishments and misery."
      mc "..."
    "\"You smell nice.\"":
      $unlock_stat_perk("lust16")
      show kate neutral at move_to(.5)
      $mc.lust+=1
      $mc.charisma-=1
      mc "You smell nice."
      kate cringe "That's not creepy at all."
      kate cringe "Why do you have to be such a weirdo, [mc]?"
      mc "I meant it as a compliment..."
      kate eyeroll "Sniffing people is creepy. End of story."
    "\"So, I was wondering if you'd like to go out with me some time...\"":
      $unlock_stat_perk("love15")
      show kate neutral at move_to(.5)
      $mc.love+=1
      $mc.intellect-=1
      mc "So, I was wondering if you'd like to go out with me some time..."
      kate laughing "..."
      mc "..."
      $kate.lust-=1
      kate smile "Wait, you're serious?"
      mc "I guess?"
      kate laughing "That's comedy gold!"
      kate laughing "Hahaha! Oww, my ribs!"
      "Not sure what I expected there, and yet, the mocking laughter cuts deep."
      "Maybe things haven't changed here as much as I thought."
  kate smile "Anyway, have you seen the [nurse]? She's supposed to meet with me."
  mc "I saw her earlier, not sure where she is now..."
  kate neutral "That's... disappointing. She's a bit ditzy, but she should know better than to miss an appointment with me."
  "[kate] has that grim look on her face, like she's planning her revenge on the poor woman."
  "The school nurse is part of the staff, but [kate] probably doesn't care about that."
  "The only people she respects here are [jo] and [mrsl]. Not sure why."
  kate annoyed "She's probably hiding somewhere. She sometimes gets nervous."
  mc "Nervous?"
  kate excited "Don't worry about that."
  kate excited "Now if you'll excuse me, I have a wayward [nurse] to find."
  hide kate with Dissolve(.5)
  "She's excited. That means someone is going to suffer. From the sound of it, that's the [nurse]."
  "Being the hero isn't really my thing, but maybe the [nurse] needs a hand."
  "[kate] isn't exactly the best person to anger, I know that better than most."
  "Someone must've seen her. Better find her before [kate] does."
  $quest.kate_search_for_nurse.start()
  $quest.kate_search_for_nurse["wrappers_picked"]=0
  return

label kate_quest_search_for_nurse_flora_talk:
  show flora excited with Dissolve(.5)
  flora excited "I've been looking for you, [mc]!"
  mc "Funny. I've been looking for you."
  flora neutral "Well, I need you to answer another question."
  mc "I don't know about that."
  flora angry "I wasn't asking."
  flora excited "If you traveled back in time, what would be your biggest goal?"
  "Huh... that seems oddly fitting for the circumstances. Very suspicious."
  mc "Why would you ask me that?"
  flora neutral "It was part of the original questionnaire, I just forgot to ask you."
  show flora neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Well, I guess I would try to be nicer to people.\"":
      show flora neutral at move_to(.5)
      mc "Well, I guess I would try to be nicer to people."
      $flora.love+=1
      flora blush "Good answer! You should've started sooner, but there's no time like the present."
      "She has no idea how right she is about that."
      mc "I'll do my best."
      mc "Err... your hair looks nice today!"
      flora thinking "Okay, stop. That's weird."
      mc "Sorry..."
      "God. Why are girls so difficult?"
    "\"I'd focus on my education. High school life is irrelevant in the big scheme of things.\"":
      show flora neutral at move_to(.5)
      $mc.intellect+=1
      #$ +1 to all focus classes
      mc "I'd focus on my education. High school life is irrelevant in the big scheme of things."
      flora annoyed "I can't put that on here."
      mc "Why not?"
      flora confused "It looks like I fed you the answer!"
      mc "They won't think that. Besides, isn't it good that your academic ambitions are rubbing off on me?"
      flora sarcastic "I guess it is..."
    "\"Anal.\"":
      $unlock_stat_perk("lust17")
      show flora neutral at move_to(.5)
      $mc.lust+=1
      mc "Anal."
      flora worried "What?"
      mc "What?"
      flora worried "What did you just say?"
      mc "Err... I said... drain all, as in, my energy! Doing sports and stuff. I'd like to get fit, yeah. That's what I said."
      $flora.lust+=1
      flora laughing "Okay, I must've misheard!"
      flora thinking "And yes, that's a good idea for you. You're getting pretty fat."
      mc "Thanks a lot..."
      flora flirty "You're welcome!"
    "\"Try to find love. Life's too long to spend alone.\"":
      show flora neutral at move_to(.5)
      $mc.love+=1
      $unlock_stat_perk("love16")
      mc "Try to find love. Life's too long to spend alone."
      flora sarcastic "That's the most cliché thing I've heard. It's worse than a Christmas movie."
      mc "Well, that was my biggest regret."
      flora confused "Was? What do you mean?"
      mc "Oh, I actually did travel back in time. I'm going for love this time."
      flora annoyed "Whatever, dude."
  flora annoyed "Anyway, what was it that you wanted?"
  mc "What?"
  flora confused "You said you'd been looking for me."
  mc "Oh, right. I was just wondering if you've seen the [nurse]."
  flora eyeroll "What did you do this time?"
  mc "Nothing. I just need to find her."
  flora thinking "Well, I did see her getting something from a vending machine earlier."
  mc "Do you remember which one?"
  flora worried "Hmm... I actually don't remember..."
  mc "It's all right. Thanks, [flora]!"
  "There are only a number of vending machines in the school. And the ones in the cafeteria are out of order. It's a decent clue."
  $quest.kate_search_for_nurse["flora_seen"] = True
  hide flora with Dissolve(.5)
  return

label kate_quest_search_for_nurse_isabelle_talk:
  show isabelle neutral with Dissolve(.5)
  mc "Excuse me, have you seen the school nurse?"
  isabelle concerned "Are you quite all right?"
  show isabelle concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yeah, I just need to have a word with her.\"":
      show isabelle concerned at move_to(.5)
      mc "Yeah, I just need to have a word with her."
      isabelle blush "I'm glad!"
      isabelle sad "I'm afraid I still don't know this school all that well. I haven't seen her."
      mc "Okay, no worries."
      isabelle sad "I should probably get myself a checkup soon... I've been feeling a bit tired lately."
      show isabelle sad at move_to(.25)
      menu(side="right"):
        extend ""
        "\"I'll let you know if I find her.\"":
          show isabelle sad at move_to(.5)
          mc "I'll let you know if I find her."
          $isabelle.love+=1
          isabelle blush "Thanks, that's nice of you."
        "\"Yeah, you do look a bit tired.\"":
          show isabelle sad at move_to(.5)
          $mc.charisma-=1
          mc "Yeah, you do look a bit tired."
          isabelle annoyed_left "Gee, thanks."
          "Ugh, girls."
    "\"That's a deep question... is anyone ever all right?\"":
      show isabelle concerned at move_to(.5)
      $mc.intellect+=1
      mc "That's a deep question... is anyone ever all right?"
      isabelle laughing "I suppose you're right!"
      isabelle laughing "I think it boils down to your outlook on life."
      isabelle smile "Someone who has nothing can still feel all right."
      isabelle smile_left "And someone who hasn't had a bother their entire life and lives on a shrimp sandwich can still feel horrible."
      isabelle confident "It's all about attitude."
      mc "I meant mentally, but yeah that works too."
      isabelle neutral "I guess everyone has their own little flaws and problems. It's worth keeping in mind."
      mc "It is, but the reason I need to talk to the [nurse] has nothing to do with my current health."
      isabelle blush "That's good to hear!"
      isabelle sad "I'm afraid I still don't know this school all that well. I haven't seen her."
      mc "Okay, thanks! I'll keep looking."
    "\"It's a simple yes-or-no question. I don't need your sympathy.\"":
      show isabelle concerned at move_to(.5)
      mc "It's a simple yes-or-no question. I don't need your sympathy."
      $isabelle.love-=1
      isabelle angry "That answers my question, I reckon."
      isabelle angry "Have a wonderful day."
  hide isabelle with Dissolve(.5)
  $quest.kate_search_for_nurse["isabelle_seen"] = True
  return

label kate_quest_search_for_nurse_meet_kate:
  show kate neutral with Dissolve(.5)
  mc "[kate], I brought you something."
  show kate neutral at move_to(.25)
  show nurse concerned at appear_from_right(.75)
  pause(1)
  kate excited "What a great surprise!"
  kate annoyed_right "I've been looking everywhere for you."
  nurse concerned "Sorry..."
  kate confident "Do you see what I have to deal with here?"
  nurse sad "Sorry, I've been busy today."
  kate eyeroll "Busy hiding and flicking your bean?"
  nurse afraid "No! I've been out collecting herbs!"
  kate excited "What do you think? Should we believe her?"
  show kate excited at move_to("left")
  show nurse afraid at move_to("right")
  $x = 1 if mc.owned_item('stick') and school_entrance['bush_interacted'] else 0
  $unlock_replay("nurse_excavation")
  menu(side="middle"):
    extend ""
    "\"I don't think so.\"":
      show kate excited at move_to(.25)
      show nurse afraid at move_to(.75)
      mc "I don't think so."
      $kate.lust+=1
      kate confident "I don't think so either."
      show nurse concerned
      kate smile "I think she was being naughty."
      mc "I think so too."
      kate annoyed_right "What happens to naughty girls?"
      nurse sad "..."
      kate annoyed_right "Well?"
      nurse sad "They get punished..."
      "Hold up! What did she just say?"
      kate confident "That's right."
    "\"Well, she did come willingly when I found her.\"":
      show kate excited at move_to(.25)
      show nurse afraid at move_to(.75)
      $nurse.love+=1
      mc "Well, she did come willingly when I found her."
      show nurse annoyed
      kate smile "I guess good behavior should be rewarded."
      kate smile "I'll keep that in mind for later."
      mc "What are you going to do?"
      kate confident "Oh, I'll figure something out. Maybe I'll let her give me a medical massage."
      kate excited "You would like that, wouldn't you?"
      nurse blush "Yes..."
      kate excited "Okay, then. But for now..."
    "?school_entrance['bush_interacted']@|{image=items stick_1}|\"I found evidence that suggests otherwise.\"":
      show kate excited at move_to(.25)
      show nurse afraid at move_to(.75)
      mc "I found evidence that suggests otherwise."
      kate excited "Oh? Let's see it."
      mc "When I found this stick, it was all sticky and wet."
      $nurse.lust+=1
      $kate.lust+=1
      show nurse surprised
      kate laughing "Brilliant! That sounds exactly like something she'd get up to. Fucking a stick!"
      kate confident_right "So, not only have you been hiding from me, you've also been naughty."
      nurse afraid "I'm really sorry, miss!"
      kate eyeroll "What have I told you about calling me that when we have company?"
      kate eyeroll "It's like you want to be exposed."
      show nurse annoyed
      kate laughing "Oh, I forgot. She does want that."
      kate laughing "Anyway..."
  kate neutral "Strip."
  nurse afraid "W-what?"
  kate eyeroll "You heard me loud and clear. Do it. Let's give [mc] a reward for helping out."
  nurse sad "..."
  nurse sad "Okay..."
  pause(.2)
  $nurse.unequip("nurse_shirt")
  pause(.5)
  $nurse.unequip("nurse_bra")
  pause(.5)
  $nurse.unequip("nurse_pantys")
  pause(.5)
  show nurse annoyed with Dissolve(.5)
  show kate confident with Dissolve(.5)
  "Wow, how the hell did [kate] get her to strip so easily?"
  "It was like she snapped her fingers and the [nurse] obeyed immediately."
  "Where do you learn techniques like this? Life would be so much easier."
  kate neutral "Don't cover yourself."
  nurse sad "Sorry."
  kate confident "Cat got your tongue, [mc]?"
  mc "Oh, god."
  kate laughing "That's the reaction I was hoping for."
  show nurse blush
  kate laughing "Okay, are you ready for your physical?"
  nurse surprised "N-now?!"
  kate eyeroll "Why do you think we're here?"
  nurse thinking "But... [mc] is here."
  kate annoyed_right "Are you questioning me?"
  nurse sad "Sorry... no..."
  kate eyeroll "Okay, bend over."
  show black with Dissolve(.5)
  hide nurse
  show kate AnalExam01
  $quest.kate_search_for_nurse.advance("anal")
  hide black with Dissolve(.5)
  "Man, this is all sorts of messed up. [kate] must be blackmailing the [nurse]. Can't imagine what sort of things she has on her."
  "But maybe the [nurse] is enjoying it..."
  "There was something about the look in her eyes when [kate] told her to strip. Like an ashamed glow of arousal."
  "She didn't offer much resistance either, even when told to bend over."
  "[kate]'s always had this air of authority about her, and the [nurse] has always been timid. Maybe the match was inevitable."
  kate "Spread your legs and your ass. You know better than anyone how this works."
  nurse "Sorry, miss."
  kate AnalExam02 "All right. That's better."
  "Her asshole just opened like a rosebud about to bloom. Twitching in anticipation."
  "Her dew-kissed pussy looks ready and excited. That delicious quiver in her labia, like vibrations through a jell-o pudding."
  "Her nervous eyes looking over her shoulder at [kate], who seems to be taking her sweet time."
  "They're both clearly enjoying the moment in their own ways."
  "[kate]'s basking in the power, a wicked smile curling her lips."
  "The [nurse] sweats in humiliation, feeling the heat creep up on her cheeks and into her pussy."
  kate AnalExam02b "[mc], do you think we should give her a rectal examination or a vaginal one?"
  menu(side="left"):
    extend ""
    "\"I'd say a rectal one is in order.\"":
      mc "I'd say a rectal one is in order."
      kate "You'd be absolutely right."
      kate AnalExam02 "We have two medical professionals here in agreement, do you have any objections?"
      nurse "No, miss."
      nurse "If you both think it's required, then I trust your expertise."
      nurse "I'm just a school nurse."
      kate "That's correct."
    "\"Her pussy seems to be dripping... might be a condition worth checking out.\"":
      mc "Her pussy seems to be dripping... might be a condition worth checking out."
      kate "It's a condition, all right."
      kate "I find it disgusting that her pussy is drooling at the thought of a student giving her an exam."
      kate AnalExam02 "It's rather inappropriate, isn't it?"
      $nurse.lust+=1
      nurse "Yes, miss. I can't help it."
      kate "Well... in my medical opinion, there's little to be done about that."
      kate AnalExam02b "Besides, her asshole is probably cleaner than her pussy at this point."
      kate "We're going with the anal exam."
    "\"You know her medical history best.\"":
      mc "You know her medical history best."
      $kate.lust+=1
      kate "It's good that you know who the resident doctor is here."
      kate "You're merely here as a consultant."
      kate "I think her anal examination is long overdue."
  kate AnalExam02 "Relax your sphincter."
  kate AnalExam03 "That's good. Your ass is getting quite loose in your old days. Barely needs any lube at all."
  nurse "Ahh!"
  "[kate]'s fingers slipped right in."
  "Can't even imagine the pain and embarrassment the [nurse] must be feeling."
  "But she seems to be taking it like a champ. It almost sounds like she's enjoying it."
  kate "Stop trying to squeeze out my fingers."
  nurse "Sorry..."
  kate "You know, I think we need to look a little deeper."
  kate "Standard procedure."
  nurse "Oh..."
  kate "Are you ready to take the rest of my hand up your ass?"
  nurse "Yes..."
  kate "Yes, what?"
  nurse "Yes, miss!"
  "The [nurse] is so willing to take everything that [kate] is dishing out. That's the hottest part."
  "It almost seems like her asshole is sucking her hand in."
  kate AnalExam04 "Here it comes."
  nurse "Oooh!"
  nurse "Goodness me!"
  "Can't believe her asshole swallowed [kate]'s whole hand!"
  "What does the inside of an ass feel like?"
  "Probably all warm and squishy? Maybe tight with muscles squeezing you."
  "Fucking an ass must be really different from fucking a pussy."
  show kate AnalExam03 with Dissolve(1)
  show kate AnalExam04 with Dissolve(1)
  show kate AnalExam03 with Dissolve(1)
  show kate AnalExam04 with Dissolve(1)
  nurse "Oh! Oh! Oh!"
  "Anal sex is definitely put on a pedestal sometimes, but I'm sure it's amazing."
  "I can only imagine what it would do to my dick."
  "Pushing it inside her tight rectum. Seeing her sweat. Seeing her twist her face in pain and pleasure while taking it deep."
  show kate AnalExam03 with Dissolve(1)
  show kate AnalExam04 with Dissolve(1)
  show kate AnalExam03 with Dissolve(1)
  show kate AnalExam04 with Dissolve(1)
  nurse "God.."
  kate "Your anal cavity seems fine."
  kate "How about we check the entrance to your colon?"
  nurse "I... umm... I don't know..."
  kate AnalExam04b "Okay, let's get a second opinion."
  menu(side="left"):
    extend ""
    "\"Doesn't hurt to be thorough.\"":
      $unlock_stat_perk("lust18")
      $mc.lust+=1
      mc "Doesn't hurt to be thorough."
      $kate.lust+=1
      kate "Oh, it's definitely going to hurt."
      $nurse.lust+=1
      nurse "Oh, my..."
      mc "Sometimes pain is part of the procedure."
      kate "I couldn't agree more."
      kate AnalExam04 "The medical board is in agreement."
      kate "This calls for a colonoscopy."
      kate AnalExam05 "Relax and let me work my way in."
      nurse "Uhhhhhhh!"
      nurse "Lord... almighty..."
      nurse "That... is quite deep... for a rectal examination!"
      kate "Are you questioning my medical practices?"
      nurse "No... miss..."
      "That's..."
      "Well, I've never seen someone take half an arm up her ass."
      "Behind that meek appearance, she's definitely hiding some talents."
      "Must've taken her years to train her asshole to do that..."
      "I'd imagine screams of pain, but she's definitely taking it well."
      "She's only mildly flustered, and overall seems to be enjoying it."
      "Or well, maybe not enjoying it, but at least enduring it."
      "Her pussy is dripping wet, so there must be some kind of pleasure for her in this."
      show kate AnalExam04 with Dissolve(.75)
      show kate AnalExam05 with Dissolve(.75)
      show kate AnalExam04 with Dissolve(.75)
      show kate AnalExam05 with Dissolve(.75)
      show kate AnalExam03 with Dissolve(.75)
      show kate AnalExam02c with Dissolve(.75)
      kate "Does it feel good?"
      show kate AnalExam04 with Dissolve(1)
      show kate AnalExam05 with Dissolve(1)
      nurse "Ugh!"
      show kate AnalExam04 with Dissolve(.75)
      show kate AnalExam05 with Dissolve(.75)
      show kate AnalExam03 with Dissolve(.75)
      show kate AnalExam02c with Dissolve(.75)
      kate "Answer me."
      nurse "Yes, miss!"
      show kate AnalExam04 with Dissolve(.75)
      show kate AnalExam05 with Dissolve(.75)
      show kate AnalExam03 with Dissolve(.75)
      show kate AnalExam02c with Dissolve(.75)
      nurse "I'm going to..."
      nurse "I'm going to..."
      show kate AnalExam04 with Dissolve(1)
      show kate AnalExam03 with Dissolve(1)
      kate "No, you're not. This is the [nurse]'s office, not a whore house."
      kate "No orgasms today."
      nurse "..."
      show kate AnalExam02c with Dissolve(1)
      "Wow, what a gape."
      "It's one of those things that you just want to put your dick in."
      "But [kate]'s never going to allow that..."
      "For now, I'll just have to be happy with the show I received."
      "But maybe one day I'll get the pleasure of taking someone's ass."
      $school_nurse_room["curtain_off"] = False
      show kate confident at center with fadehold
      mc "Shouldn't you give her a lollipop or something for being so good?"
      kate flirty "I think she's had more than her fair share today."
      kate laughing "Take this so she doesn't eat it later."
      $mc.add_item("lollipop_2")
      hide nurse
      hide kate
      with Dissolve(.5)
      $quest.kate_search_for_nurse.finish()
    "\"I think we should give her a lollipop and call it a day.\"":
      $unlock_stat_perk("love17")
      $mc.love+=1
      $nurse.love+=1
      mc "I think we should give her a lollipop and call it a day."
      kate "She has been very good, but she definitely won't be getting any more lollipops today."
      kate "In fact, I think you should hold on to this one so she doesn't get any ideas."
      $mc.add_item("lollipop_2")
      $school_nurse_room["curtain_off"] = False
      show kate confident at center with fadehold
      kate confident "All right, that's enough gawking. We have stuff to do that requires more privacy."
      kate confident "Close the door on your way out."
      $mc["turn_off_location_transitions"] = True
      hide kate
      show black
      with fadehold
      call goto_school_ground_floor_west
      hide black with Dissolve(.5)
      "Well, that was both bizarre and exhilarating. It would be fucking awesome to learn how to order someone around like that."
      "Got to ask [kate] for some tips later."
      $quest.kate_search_for_nurse.finish()
      $school_ground_floor_west["nurse_room_locked_now"] = True
      $school_nurse_room["curtain_off"] = True
      $mc["turn_off_location_transitions"] = False
  $nurse.equip("nurse_shirt")
  $nurse.equip("nurse_bra")
  $nurse.equip("nurse_pantys")
  return
