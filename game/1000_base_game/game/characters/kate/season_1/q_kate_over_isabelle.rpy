label kate_quest_isabelle_tour_confront_side_isabelle:
  show kate neutral at Position(xalign=.25)
  show isabelle laughing at Position(xalign=.75)
  with Dissolve(.5)
  isabelle laughing "Hi, [kate]. Do you have a minute?" # (looking left)
  kate smile "For you two? Forget it."
  isabelle eyeroll  "What's your deal? Are you always this much of a wanker?" #(looking left)
  kate annoyed_right "Listen, bitch. I don't know who you think you are, but I couldn't care less."
  kate excited "Why don't you tuck those crooked English teeth back under that thick lip of yours and scuttle along, hm?" #(isabelle angry)
  "Crap. This is not going to end well..."
  "Old me would've jumped ship a long time ago, but this is going to turn into scratching and hair-pulling at any moment."
  show kate excited at move_to("left")
  show isabelle eyeroll at move_to("right")
  menu(side="middle"):
    extend ""
    "\"I'm sure this is all a big misunderstanding... let's all just go our separate ways.\"":
      show kate excited at move_to(.25)
      show isabelle eyeroll at move_to(.75)
      $mc.intellect+=1
      mc "I'm sure this is all a big misunderstanding... let's all just go our separate ways."
      $isabelle.lust-=1
      isabelle angry "What do you mean? She's way out of line!"
      kate laughing "The only misunderstanding here is you thinking I care."
      kate confident "A pair of lower life-forms disturbing the queen is what's out of line."
      kate confident "In this school, you start at the bottom and work your way up. I'm sorry you didn't get the memo."
      isabelle annoyed_left "I don't know what tragedy made you like this, but I feel sorry for you, [kate]."
      "Years and years of high social status, good looks, and terrible parenting is my guess..."
      "But if anyone was born a bitch, it would be [kate]."
      kate smile "The only tragedy here is the company."
      isabelle annoyed "You know what? I'm over it... come on, [mc], let's go."
      call screen remember_to_save(_with_none=False) with Dissolve(.25)
      with Dissolve(.25)
      $quest.isabelle_tour["remember_to_save"] = True
      show kate smile at move_to("left")
      show isabelle annoyed at move_to("right")
      menu(side="middle"):
        extend ""
        "Stay with [kate]":
          show kate smile at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Start the tour without me... the English classroom is right around the corner."
          kate laughing "See? Even this loser thinks you're too lame to hang out with."
          isabelle angry "Simply repulsive!"
          show kate laughing at move_to(.5,1)
          show isabelle angry at disappear_to_right
          "Even though [kate] hates me, staying in her presence is like a drug."
          "Plus, her smell is simply divine... apricots and cherries, with just a hint of Hell."
          $quest.isabelle_tour.advance("english_class")
          $quest.kate_over_isabelle.start("the_winning_team")
          hide kate with Dissolve(.5)
        "Leave with [isabelle]":
          show kate smile at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Never thought you'd say those words!"
          kate laughing "The drama queen and her spineless puppy, slinking off into the sunset..."
          isabelle annoyed_left "Screw you, [kate]!"
          kate smile_right "Keep dreaming, sweetheart."
          $quest.isabelle_tour.advance("english_class")
          $quest.isabelle_over_kate.start()
          $process_event("update_state")
          hide kate
          hide isabelle
          with Dissolve(.5)
          "It's weird... even though [kate] throws her usual insults my way, it feels different having someone to share them with. Less painful, somehow."
          "Hopefully, [isabelle] has a thick skin because [kate]'s not going to let this thing go."
    "\"So, there's this mud pool next to the football field, if you ladies are interested...\"":
      $unlock_stat_perk("lust15")
      show kate excited at move_to(.25)
      show isabelle eyeroll at move_to(.75)
      $mc.lust+=1
      mc "So, there's this mud pool next to the football field, if you ladies are interested..."
      $isabelle.love-=1
      show kate cringe
      isabelle cringe "..."
      kate cringe "Stop wasting oxygen."
      "Ah, the good old insults... I almost missed them. Almost."
      mc "How about arm wrestling?"
      kate eyeroll "How about shutting the hell up?"
      isabelle annoyed_left "Why do you have to be so rude, [kate]?"
      kate smile_right "The only rude one here is you, thinking it's okay to give me lip. Do you know who I am?"
      isabelle annoyed_left "I know you're a proper twat."
      "Honestly, watching these two go at each other is probably the most exciting thing that has happened to me in years."
      "Maybe there's a way to keep this fight going... something that will end in them getting physical."
      "Eyes burning... chests heaving... bodies glistening with sweat..."
      "Hot damn!"
      isabelle annoyed "[mc]! Are you listening?"
      kate confident_right "He probably got tired of your voice... I know I did."
      "Shit. Must've zoned out there for a moment."
      mc "Sorry, what?"
      isabelle annoyed "I said I'm leaving! Are you coming or not?"
      "Not sure what went down here, but [isabelle] looks a lot angrier than [kate]."
      "You daydream for a little while, and then suddenly you've lost sight of everything. If only there was a rewind button..."
      call screen remember_to_save(_with_none=False) with Dissolve(.25)
      with Dissolve(.25)
      $quest.isabelle_tour["remember_to_save"] = True
      show kate confident_right at move_to("left")
      show isabelle annoyed at move_to("right")
      menu(side="middle"):
        extend ""
        "Stay with [kate]":
          show kate confident_right at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Start the tour without me... the English classroom is right around the corner."
          kate laughing "See? Even this loser thinks you're too lame to hang out with."
          isabelle angry "Simply repulsive!"
          show kate laughing at move_to(.5,1)
          show isabelle angry at disappear_to_right
          "Even though [kate] hates me, staying in her presence is like a drug."
          "Plus, her smell is simply divine... apricots and cherries, with just a hint of Hell."
          $quest.isabelle_tour.advance("english_class")
          $quest.kate_over_isabelle.start("the_winning_team")
          hide kate with Dissolve(.5)
        "Leave with [isabelle]":
          show kate confident_right at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Never thought you'd say those words!"
          kate laughing "The drama queen and her spineless puppy, slinking off into the sunset..."
          isabelle annoyed_left  "Screw you, [kate]!"
          kate smile_right "Keep dreaming, sweetheart."
          $quest.isabelle_tour.advance("english_class")
          $quest.isabelle_over_kate.start()
          $process_event("update_state")
          hide kate
          hide isabelle
          with Dissolve(.5)
          "It's weird... even though [kate] throws her usual insults my way, it feels different having someone to share them with. Less painful, somehow."
          "Hopefully, [isabelle] has a thick skin because [kate]'s not going to let this thing go."
    "\"Teeth can be fixed, but a crooked personality is irredeemable.\"":
      show kate excited at move_to(.25)
      show isabelle eyeroll at move_to(.75)
      $mc.charisma+=1
      mc "Teeth can be fixed, but a crooked personality is irredeemable."
      $isabelle.lust+=1
      $isabelle.love+=1
      show kate angry
      isabelle laughing "You took the words right out of my thick-lipped mouth!"
      kate angry "You nerds are messing with the wrong person..."
      "She's right, there'll be hell to pay for this."
      "But standing up to her after all these years... what a feeling!"
      "Hopefully, [isabelle] has a thick skin because [kate]'s not going to let this thing go now."
      "It would probably be best to get out of this situation before all hell breaks loose..."
      mc "Let's get going. We have a tour to complete!"
      isabelle confident "Yeah, no point in wasting any more energy on her."
      kate angry "You have no idea what you've just gotten yourself into."
      show kate angry at disappear_to_left
      show isabelle confident at move_to(.5,1)
      isabelle confident "God, that felt good."
      mc "Couldn't agree more!"
      $achievement.standing_up.unlock()
      $quest.isabelle_tour.advance("english_class")
      $quest.isabelle_over_kate.start()
      $process_event("update_state")
      hide isabelle with Dissolve(.5)
    "\"I don't get why you're stirring up shit, [isabelle]. You're new here... apologize to [kate], and let's go.\"":
      show kate excited at move_to(.25)
      show isabelle eyeroll at move_to(.75)
      mc "I don't get why you're stirring up shit, [isabelle]. You're new here... apologize to [kate], and let's go."
      $isabelle.love-=1
      $isabelle.lust-=1
      isabelle angry "I can't believe you, [mc]!"
      kate confident_right "Listen to your pet, bitch. Get down on your knees and kiss my shoes, and I'll think about forgiving you."
      isabelle annoyed_left  "I've been to twenty-eight different countries, and you're the most detestable human being I've come across."
      "And she has met me! That's a compliment if I ever saw one."
      kate smile_right  "If you keep running into trouble, maybe you're the problem?"
      isabelle annoyed_left "Whatever."
      isabelle annoyed "[mc], I'll be in the English classroom."
      show kate smile_right at move_to(.5,1)
      show isabelle annoyed at disappear_to_right
      "That sure didn't go as planned..."
      $quest.isabelle_tour.advance("english_class")
      $quest.kate_over_isabelle.start("the_winning_team")
      hide kate with Dissolve(.5)
    "\"If you don't watch your mouth, [kate], I'll gouge your eyes out and fuck your skull.\"":
      show kate excited at move_to(.25)
      show isabelle eyeroll at move_to(.75)
      mc "If you don't watch your mouth, [kate], I'll gouge your eyes out and fuck your skull."
      $isabelle.love-=1
      $isabelle.lust-=1
      show kate cringe
      isabelle afraid "What?! What's wrong with you?"
      $kate.love-=1
      $kate.lust-=1
      kate cringe "Get some help, psycho."
      "They probably didn't get the FMJ reference, but that's fine."
      "It felt good... the practice in front of the mirror finally paid off! Totally worth it."
      isabelle annoyed_left  "Anyway, [kate]... you're way out of line. I don't know what makes you think this kind of behavior is acceptable."
      kate confident "A pair of lower life-forms disturbing the queen is what's out of line."
      kate confident "In this school, you start at the bottom and work your way up. I'm sorry you didn't get the memo."
      isabelle annoyed_left "I don't know what tragedy made you like this, but I feel sorry for you."
      "Years and years of high social status, good looks, and terrible parenting is my guess..."
      "But if anyone was born a bitch, it would be [kate]."
      kate smile "The only tragedy here is the company."
      isabelle annoyed "You know what? I'm over it... come on, [mc], let's go."
      call screen remember_to_save(_with_none=False) with Dissolve(.25)
      with Dissolve(.25)
      $quest.isabelle_tour["remember_to_save"] = True
      show kate smile at move_to("left")
      show isabelle annoyed at move_to("right")
      menu(side="middle"):
        extend ""
        "Stay with [kate]":
          show kate smile at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Start the tour without me... the English classroom is right around the corner."
          kate laughing "See? Even this loser thinks you're too lame to hang out with."
          isabelle angry "Simply repulsive!"
          show kate laughing at move_to(.5,1)
          show isabelle angry at disappear_to_right
          "Even though [kate] hates me, staying in her presence is like a drug."
          "Plus, her smell is simply divine... apricots and cherries, with just a hint of Hell."
          $quest.isabelle_tour.advance("english_class")
          $quest.kate_over_isabelle.start("the_winning_team")
          hide kate with Dissolve(.5)
        "Leave with [isabelle]":
          show kate smile at move_to(.25)
          show isabelle annoyed at move_to(.75)
          mc "Never thought you'd say those words!"
          kate laughing "The drama queen and her spineless puppy, slinking off into the sunset..."
          isabelle annoyed_left "Screw you, [kate]!"
          kate smile_right "Keep dreaming, sweetheart."
          $quest.isabelle_tour.advance("english_class")
          $quest.isabelle_over_kate.start()
          $process_event("update_state")
          hide kate
          hide isabelle
          with Dissolve(.5)
          "It's weird... even though [kate] throws her usual insults my way, it feels different having someone to share them with. Less painful, somehow."
          "Hopefully, [isabelle] has a thick skin because [kate]'s not going to let this thing go."
  return

label kate_quest_isabelle_tour_confront_side_kate:
  show kate neutral at Position(xalign=.25)
  show isabelle annoyed_left at Position(xalign=.75)
  with Dissolve(.5)
  isabelle annoyed_left "What's your deal, [kate]? Are you always this much of a wanker?"
  kate confident "Listen, bitch. I don't know who you think you are, but I couldn't care less."
  kate confident "Why don't you  tuck those crooked English teeth back under that thick lip of yours and scuttle along, hm?"
  "Seems like the confrontation didn't go as planned. [kate]'s definitely winning, but no real surprise there."
  "She probably spewed insults before she learned to walk..."
  "For once, it's nice to not be the target."
  isabelle annoyed_left "I don't know what tragedy made you like this, but I feel sorry for you."
  "Years and years of high social status, good looks, and terrible parenting is my guess..."
  "But if anyone was born a bitch, it would be [kate]."
  kate smile "The only tragedy here is the company."
  isabelle eyeroll "Good, because I'm leaving. See you in the English classroom, [mc]."
  show kate laughing at move_to(.5,1)
  show isabelle eyeroll at disappear_to_right
  kate laughing "Off you go! Was that so hard?"
  kate smile "..."
  mc "..."
  kate neutral "Why are you still here?"
  if not quest.isabelle_tour["remember_to_save"]:
    call screen remember_to_save(_with_none=False) with Dissolve(.25)
    with Dissolve(.25)
  $quest.isabelle_tour.advance("english_class")
  #$process_event("update_state")

label kate_over_isabelle_choosing_kate:
  show kate neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Sorry, I was just admiring your verbal takedown of the new girl...\"":
      show kate neutral at move_to(.5)
      mc "Sorry, I was just admiring your verbal takedown of the new girl..."
      kate laughing "She's having trouble understanding how things work around here."
      mc "Clearly."
      kate smile "You were always a loser and a freak, but at least you never tried something that stupid..."
      mc "Thanks, I guess..."
      kate thinking "So, you agree that she needs to be taught a lesson, right?"
      "This could be my ticket into the more popular circles of the school, and honestly, [isabelle] seems way too stuck up for her own good."
      "But on the other hand, she's new here and [kate]'s probably going to ruin her..."
      show kate thinking at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Yes, [isabelle] was out of line.\"":
          show kate thinking at move_to(.5)
          mc "Yes, [isabelle] was out of line."
          kate laughing "Oh my god, right?"
          kate smile "Okay, so here's what's going to happen..."
          kate confident "You're going to keep an eye on her and report back to me."
          mc "Okay..."
          "Sounds like she has some elaborate revenge plan in mind."
          kate thinking "To start with, I want to know what classes she picks and what her favorite drink is. Earn her trust."
          kate thinking "Think you can handle that?"
          mc "Yes, ma'am."
          kate blush "That has a nice ring to it!"
          $kate.lust+=1
          kate blush "Call me \"ma'am\" from now on."
          mc "Of course, ma'am."
          kate afraid "Don't talk to me again unless you have the information."
          show kate afraid at disappear_to_right
          "Never had much pride to swallow in the first place, and if throwing [isabelle] under the bus will get me out of bullying, then that's how it's going to be."
          $achievement.the_dark_side.unlock()
          $quest.kate_over_isabelle.start()
          $quest.kate_over_isabelle.advance("spy_focus")
          $quest.isabelle_tour.advance("english_class")
        "\"No, you should leave her alone.\"":
          show kate thinking at move_to(.5)
          mc "No, you should leave her alone."
          kate laughing "Leave her alone? No one talks to me like that and gets away with it!"
          kate confident "It's better she learns that sooner rather than later."
          mc "If you try anything, I'll report it to the principal."
          kate eyeroll "You're going to tell on me?"
          kate eyeroll "Jesus, what a pushover... you've only just met this girl and you're already trying to be her white knight."
          kate excited "Well, I'm going to ruin her, and there's nothing you can do about it."
          kate annoyed "Get lost, worm."
          show kate annoyed at disappear_to_right
          $quest.isabelle_tour.advance("english_class")
          $quest.kate_over_isabelle.fail()
          $quest.isabelle_over_kate.start()
        "\"What's in it for me?\"":
          show kate thinking at move_to(.5)
          mc "What's in it for me?"
          kate neutral "What do you mean? For once in your life, you get to be part of something!"
          kate laughing "Or do you prefer being a lonely loser for the rest of your life?"
          show kate laughing at move_to(.25)
          menu(side="right"):
            extend ""
            "\"Okay, what's the plan?\"":
              show kate laughing at move_to(.5)
              mc "Okay, what's the plan?"
              kate confident "Okay, so here's what's going to happen..."
              kate confident "You're going to keep an eye on her and report back to me."
              mc "Okay..."
              "Sounds like she has some elaborate revenge plan in mind."
              kate thinking "To start with, I want to know what classes she picks and what her favorite drink is. Earn her trust."
              kate thinking "Think you can handle that?"
              mc "Yes, ma'am."
              kate blush "That has a nice ring to it!"
              $kate.lust+=1
              kate blush "Call me \"ma'am\" from now on."
              mc "Of course, ma'am."
              kate afraid "Don't talk to me again unless you have the information."
              show kate afraid at disappear_to_right
              "Never had much pride to swallow in the first place, and if throwing [isabelle] under the bus will get me out of bullying, then that's how it's going to be."
              $achievement.the_dark_side.unlock()
              $quest.kate_over_isabelle.start()
              $quest.kate_over_isabelle.advance("spy_focus")
              $quest.isabelle_tour.advance("english_class")
            "\"Not good enough.\"":
              show kate laughing at move_to(.5)
              mc "Not good enough."
              kate smile "You think you have a bargaining chip here?"
              mc "Yes, I do think that. You've already wasted your chances with her."
              mc "[isabelle] doesn't seem like the type to care about words or insults."
              $mc.intellect+=1
              mc "If you want to take her down, you'll need someone to earn her trust... and she's not going to go near you or any of your cheerleader friends."
              kate neutral "...Maybe you have a point for once."
              kate neutral "Okay, what do you want?"
              show kate neutral at move_to(.75)
              menu(side="left"):
                extend ""
                "\"I want to be left alone.\"":
                  show kate neutral at move_to(.5)
                  mc "I want to be left alone."
                  kate smile "That's it?"
                  mc "Yes, that's it. No jocks. No nothing."
                  kate laughing "I'll let them know you're my little doggy now! As long as you do tricks for me, you'll be safe."
                  mc "Deal."
                  kate confident "Okay, so here's what's going to happen..."
                  kate confident "You're going to keep an eye on her and report back to me."
                  mc "Okay..."
                  kate thinking "To start with, I want to know what classes she picks and what her favorite drink is. Earn her trust."
                  kate thinking "Think you can handle that?"
                  mc "Yes, ma'am."
                  kate blush "That has a nice ring to it!"
                  $kate.lust+=1
                  kate blush "Call me \"ma'am\" from now on."
                  mc "Of course, ma'am."
                  kate afraid "Don't talk to me again unless you have the information."
                  show kate afraid at disappear_to_right
                  "Never had much pride to swallow in the first place, and if throwing [isabelle] under the bus will get me out of bullying, then that's how it's going to be."
                  $achievement.the_dark_side.unlock()
                  $quest.kate_over_isabelle.start()
                  $quest.kate_over_isabelle.advance("spy_focus")
                  $quest.isabelle_tour.advance("english_class")
                "\"I want to see her humiliated!\"":
                  show kate neutral at move_to(.5)
                  mc "I want to see her humiliated!"
                  kate laughing "Trust me, I'll make it into a spectacle for the entire school."
                  mc "Deal."
                  kate confident "Okay, so here's what's going to happen..."
                  kate confident "You're going to keep an eye on her and report back to me."
                  mc "Okay..."
                  kate thinking "To start with, I want to know what classes she picks and what her favorite drink is. Earn her trust."
                  kate thinking "Think you can handle that?"
                  mc "Yes, ma'am."
                  kate blush "That has a nice ring to it!"
                  $kate.lust+=1
                  kate blush "Call me \"ma'am\" from now on."
                  mc "Of course, ma'am."
                  kate afraid "Don't talk to me again unless you have the information."
                  show kate afraid at disappear_to_right
                  "Never had much pride to swallow in the first place, and if throwing [isabelle] under the bus will get me out of bullying, then that's how it's going to be."
                  $quest.kate_over_isabelle.start()
                  $quest.kate_over_isabelle.advance("spy_focus")
                  $quest.isabelle_tour.advance("english_class")
                  $achievement.the_dark_side.unlock()
                "\"On second thought... fuck you.\"":
                  show kate neutral at move_to(.5)
                  mc "On second thought... fuck you."
                  kate skeptical "Is this the cockroach uprising?"
                  kate skeptical "No wonder you don't have any friends."
                  kate cringe "Apart from the smell of sweat and failure, you're also a massive pussy."
                  kate eyeroll "You're just one of those losers who will live in your mom's basement until you're thirty, and then put a shotgun in your mouth and blow your brains out."
                  kate excited "Unless you're too much of a wimp for that, too!"
                  "If only [kate] knew how true that is, maybe she wouldn't say it... but knowing her, she probably would anyway, and then gloat about being right."
                  "The sad thing is, when high school ends, she'll still be just as popular and successful."
                  "For years, I used to stalk her on Twitter just to see what kind of person she'd become."
                  "I was there when she started her business, married her husband, and had her first kid."
                  "Life's just unfair."
                  mc "You'll name your son Achilles. And you'll fill your instagram with happy pictures of him. And everyone will leave comments saying you're an amazing mother."
                  kate afraid "What are you even on about?"
                  kate thinking "Actually, I don't want to know."
                  kate afraid "Get lost, weirdo!"
                  show kate afraid at disappear_to_right
                  "Not sure what my plan was with that, but it did stop [kate] in her tracks for a moment..."
                  "She probably doesn't think much of her future. Things just work out for people like her."
                  "But maybe she has a diary somewhere with the name Achilles penned down?"
                  "That would explain her reaction."
                  $quest.kate_over_isabelle.fail()
                  $quest.isabelle_over_kate.start()
                  $quest.isabelle_tour.advance("english_class")
    "\"You know, I've always found the way you talk to people repulsive.\"":
      show kate neutral at move_to(.5)
      mc "You know, I've always found the way you talk to people repulsive."
      kate skeptical "The opinion of a cockroach is worth nothing to me."
      kate cringe "And for the record, I find everything about you repulsive."
      kate cringe "The greasy hair, the smell of sweat and failure, and most of all... the empty stare."
      kate eyeroll "You're just one of those losers who will live in your mom's basement until you're thirty, and then put a shotgun in your mouth and blow your brains out."
      kate excited "Unless you're too much of a wimp for that, too!"
      "If only [kate] knew how true that is, maybe she wouldn't say it... but knowing her, she probably would anyway, and then gloat about being right."
      "The sad thing is, when high school ends, she'll still be just as popular and successful."
      "For years, I used to stalk her on Twitter just to see what kind of person she'd become."
      "I was there when she started her business, married her husband, and had her first kid."
      "Life's just unfair."
      mc "You'll name your son Achilles. And you'll fill your Instagram with happy pictures of him. And everyone will leave comments saying you're an amazing mother."
      kate afraid "What are you even on about?"
      kate thinking "Actually, I don't want to know."
      kate afraid "Get lost, weirdo!"
      show kate afraid at disappear_to_right
      "Not sure what my plan was with that, but it did stop [kate] in her tracks for a moment..."
      "She probably doesn't think much of her future. Things just work out for people like her."
      "But maybe she has a diary somewhere with the name Achilles penned down?"
      "That would explain her reaction."
      $quest.kate_over_isabelle.fail()
      $quest.isabelle_over_kate.start()
  return

label kate_quest_isabelle_tour_kate_over_isabelle:
  show kate confident with Dissolve(.5)
  kate confident "So, when it actually came down to it, you decided to pick the winning side? Not a bad move."
  kate confident "Spineless, sure... but not a bad move."
  mc "I never said anything about picking sides."
  kate gushing "But anyone would be lucky to have you on their side!"
  kate gushing "You bring so much to the table! You're not just a pretty face!"
  "God... out of all things, her sarcasm somehow hurts the most. It's like she can't even be bothered to insult you in earnest."
  kate laughing "You should see the look on your face!"
  "Staying with [kate] was perhaps not the best idea after all. All these years haven't changed a thing..."
  "All those moments in the shower, rewriting history in my head, planning scenes of revenge..."
  "Her presence makes every letter of my carefully prepared comebacks fade off the page. In reality, the script is useless."
  "Still, the way she carries herself is intoxicating. She's the worst kind of person, but that unshakable confidence is so hot..."
  "How could I possibly side with [isabelle] in this? She's cute, but still a nobody."
  "And at the same time, siding with [kate] is nothing short of reckless... if anyone could crush me even more, it's her."
  kate confident "Whenever you're done sniffing my perfume, you're free to leave."
  mc "I wasn't..."
  kate neutral "Then why are you still here? Do you have something else to say?"
  if not quest.isabelle_tour["remember_to_save"]:
    call screen remember_to_save(_with_none=False) with Dissolve(.25)
    with Dissolve(.25)
  jump kate_over_isabelle_choosing_kate

label kate_quest_isabelle_tour_kate_over_isabelle_cafeteria:
  show kate neutral with Dissolve(.5)
  kate neutral "What?"
  mc "I've got the information you asked for..."
  kate confident "Really? That went faster than expected."
  show kate confident at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'd hate to keep you waiting, ma'am.\"":
      show kate confident at move_to(.5)
      mc "I'd hate to keep you waiting, ma'am."
      $kate.lust+=1
      kate flirty "That's a good boy."
      mc "Thank you, ma'am! Your praise means everything to me."
      kate neutral "Okay, enough groveling. Out with it."
      mc "[isabelle]'s focus classes are English, Art, and Gym."
      mc "Her favorite drink is ice tea."
      kate laughing "What a nerd!"
      kate smile_right "Okay, let me think..."
      kate confident "Bring me a pair of handcuffs, a dog collar, a bottle of ice tea, and some sleeping pills."
      kate confident "And schedule a meeting with [maxine] for me."
      kate neutral "Think you can handle that?"
      "Those items... [kate] sure isn't messing around. I knew she had a mean streak, but this is some real dominatrix shit."
      "[isabelle] is so screwed..."
      mc "Yes, ma'am! I'm on it."
      kate laughing "Well, then. Go fetch!"
      "This feels all sorts of wrong... but at the same time, it's [kate]. She can do whatever she wants to me."
    "\"Do you have a reward for me?\"":
      show kate confident at move_to(.5)
      mc "Do you have a reward for me?"
      kate cringe "Your reward is being in my presence, freak."
      kate cringe "That's enough, isn't it?"
      show kate cringe at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Yes, ma'am... sorry for assuming.\"":
          show kate cringe at move_to(.5)
          mc "Yes, ma'am... sorry for assuming."
          kate excited "You're forgiven."
          kate skeptical "Although, you have been good so far... maybe you do deserve a little treat..."
          mc "Thank you so much for your kindness!"
          kate excited "Get down on all fours and kiss my shoe. That's your treat."
          jump kate_kiss_shoe_scene
        "\"I'd like something else, please.\"":
          show kate cringe at move_to(.5)
          mc "I'd like something else, please."
          kate skeptical "Something else, huh? And what would that be?"
          mc "Maybe... a kiss?"
          kate excited "Yeah?"
          kate excited "Okay, you may get down on all fours and kiss my shoe."
          jump kate_kiss_shoe_scene
    "\"You better treat me well now.\"":
      show kate confident at move_to(.5)
      mc "You better treat me well now."
      kate laughing "I have been treating you well!"
      kate smile "Tell me, has anyone bothered you since you started helping me?"
      mc "No..."
      kate confident "See? I think you're the one who should treat me better."
      kate neutral "Show me some goddamn respect!"
      show kate neutral at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Sorry, ma'am...\"":
          show kate neutral at move_to(.5)
          mc "Sorry, ma'am..."
          $kate.lust+=1
          kate confident "Good. Know your place."
          kate neutral "Now, tell me about [isabelle]."
          mc "Her focus classes are English, Art, and Gym, and her favorite drink is ice tea."
          kate laughing "A basic drink for a basic bitch."
          kate smile "Okay, I'll need you to bring me a few things."
          mc "Name them, ma'am."
          kate confident "Bring me a pair of handcuffs, a dog collar, a bottle of ice tea, and some sleeping pills."
          kate confident "Then schedule a meeting with [maxine] for me."
          kate neutral "Shouldn't be too hard, right?"
          mc "Not at all!"
          kate neutral "Okay, off you go."
          "God, [kate] really knows how to degrade people..."
          "It's hot, somehow."
        "\"You may be the queen of this school, but that doesn't mean I'll bow.\"":
          show kate neutral at move_to(.5)
          mc "You may be the queen of this school, but that doesn't mean I'll bow."
          $kate.lust-=1
          kate laughing "What is this pathetic rebellion?"
          kate flirty "It's almost cute. Almost."
          kate neutral "Now, cut the crap and tell me about [isabelle]."
          "[kate] makes me feel powerless..."
          "What little courage I muster fades as soon as she starts talking."
          mc "Her focus classes are English, Art, and Gym, and her favorite drink is ice tea."
          kate laughing "What an absolute nerd!"
          kate smile_right "Okay, let me think..."
          kate confident "Bring me a pair of handcuffs, a dog collar, a bottle of ice tea, and some sleeping pills."
          kate confident "And schedule a meeting with [maxine] for me."
          kate neutral "Think you can handle that, without peeing on the carpet?"
          mc "I guess..."
          "God, she really knows how to degrade people..."
          "Hopefully, I won't have to deal with her anymore after she's done with [isabelle]."
          kate neutral "Okay. Get lost now."
  $quest.kate_over_isabelle.advance("search_and_schedule")
# $quest.kate_over_isabelle.finish(silent=True)
  $process_event("update_state")
  hide kate with Dissolve(.5)
# $game.notify_modal(None,"Coming soon","{image=kate contact_icon}{space=25}|"+"You have reached the end of\nStepping on the Rose\nfor now!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image= items monkey_wrench}",5.0)
  return

label kate_kiss_shoe_scene:
  show kate excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Thank you, ma'am!\"":
      $unlock_replay("kate_shoe")
      show kate excited at move_to(.5)
      mc "Thank you, ma'am!"
      "This is the most humiliating thing I've ever experienced, but being so close to [kate] makes it all worth it. Even if it's just her foot..."
      kate shoekiss_shoe "Don't drool."
      mc "I won't! I promise!"
      $kate.lust+=1
      kate shoekiss_shoe "Okay, go ahead..."
      mc "Thank you, ma'am!"
      "Seeing her from this angle truly puts the meaning of dominance into perspective."
      "The shackling sharpness of her eyes is enough to send anyone to their knees."
      "Maybe people like her are meant to be admired from below... [kate] does look even more beautiful towering over you."
      "Her posture says it all — this is how it's meant to be."
      "She doesn't care if anyone sees us. She doesn't care that her shirt rides up her midriff, or that her pants stretch over her thighs."
      "Would she have put me through my paces this way if she was wearing her cheerleader uniform?"
      "What kind of panties hide beneath her pants? Strict black ones like her soul... or girly ones with little ribbons?"
      "God, the almost imperceptible scent of her feet makes my head swim!"
      "I just need to experience it fully..."
      menu(side="far_right"):
        extend ""
        "?kate.lust>=2@[kate.lust]/2|{image= kate contact_icon}|{image= stats lust_3}|\"Please, ma'am... could you... um... maybe, take off your shoe?\"":
          mc "Please, ma'am... could you... um... maybe, take off your shoe?"
          kate shoekiss_shoe "That's disgusting. You're disgusting."
          kate shoekiss_shoe "Why would I do that?"
          mc "I... I'd just really like it, ma'am."
          kate shoekiss_shoe "Well... I suppose you have been a good boy lately..."
          kate shoekiss_shoe "But don't get any funny ideas!"
          kate shoekiss_shoe "I do not want to feel your mouth or tongue slobbering all over my toes. Understood?"
          mc "Yes, ma'am! Thank you!"
          kate shoekiss "Go on, then... have a whiff, you creep."
          "Oh man, the aroma..."
          "The leather of her shoes... a tiny hint of sweat and salt... and that distinctive yet subtle fragrance of feet..."
          "Even [kate]'s toes smell heavenly. Those dainty little things."
          "If only she'd allow me to stick my tongue out... clean her toes and in between them..."
          "Run it up and down the length of her sole..."
          "Taste the wrinks of her arches..."
          "Kiss the rough flesh of her heel... have her step on my face!"
          kate shoekiss "I told you not to drool."
          mc "Sorry, ma'am!"
          kate shoekiss "At least, you didn't get it on me... but I think that's enough."
          mc "Please, ma'am! I'll keep my mouth away!"
          kate shoekiss "You're pathetic."
          kate shoekiss "Okay, tell me about [isabelle]."
          $kate["shoekiss_no_shoes"] = True
          jump kate_shoekiss_ending
        "\"Thank you for showing me my place, ma'am!\"":
          mc "Thank you for showing me my place, ma'am!"
          kate shoekiss_shoe "You're very welcome, pet."
          kate shoekiss_shoe "Now, tell me about [isabelle]."
          jump kate_shoekiss_ending
    "\"Forget it...\"":
      show kate excited at move_to(.5)
      mc "Forget it..."
      $kate.lust-=1
      kate laughing "Your loss."
      kate confident "Now, tell me about [isabelle]."
      "It feels so hopeless to rebel against [kate]. She holds all the cards..."
      "With a snap of her fingers, she can call a jock over to beat me up."
      "Better to just get this over with and leave."
      mc "Her focus classes are English, Art, and Gym, and her favorite drink is ice tea."
      kate neutral "Huh... okay."
      kate confident "Bring me a pair of handcuffs, a dog collar, a bottle of ice tea, and some sleeping pills."
      kate confident "And schedule a meeting with [maxine] for me."
      kate neutral "And don't mess it up, 'cause there'll be hell to pay."
      mc "Fine, I guess..."
      kate neutral "Okay, shoo!"
      $quest.kate_over_isabelle.advance("search_and_schedule")
#     $quest.kate_over_isabelle.finish(silent=True)
      $process_event("update_state")
      hide kate with Dissolve(.5)
#     $game.notify_modal(None,"Coming soon","{image=kate contact_icon}{space=25}|"+"You have reached the end of\nStepping on the Rose\nfor now!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image= items monkey_wrench}",5.0)
      return

label kate_shoekiss_ending:
  if kate["shoekiss_no_shoes"]:
    show kate shoekiss
  else:
    show kate shoekiss_shoe
  mc "Her focus classes are English, Art, and Gym, and her favorite drink is ice tea."
  kate "I bet she reads poetry in her spare time."
  kate "All right... bring me a pair of handcuffs, a dog collar, a bottle of ice tea, and some sleeping pills."
  kate "And schedule a meeting with [maxine] for me."
  kate "You can handle that, can't you?"
  mc "Of course, ma'am!"
  kate flirty "That's a good boy... go fetch now."
  "God, she really knows how to degrade people..."
  "Hopefully, there's another reward waiting for me!"
  $quest.kate_over_isabelle.advance("search_and_schedule")
# $quest.kate_over_isabelle.finish(silent=True)
  $process_event("update_state")
  hide kate with Dissolve(.5)
# $game.notify_modal(None,"Coming soon","{image=kate contact_icon}{space=25}|"+"You have reached the end of\nStepping on the Rose\nfor now!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image= items monkey_wrench}",5.0)
  return
