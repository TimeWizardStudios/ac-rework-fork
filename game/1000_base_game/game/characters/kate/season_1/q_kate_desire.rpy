image contact_info_call = Transform(LiveComposite((105,105),(0,0),AlphaMask("phone apps contact_info call",LiveComposite((105,105),(5,5),Transform("ui circle_mask",size=(105,105))))),size=(53,53))

label quest_kate_desire_start:
  "Whenever I see [kate] in the gym, my heart starts beating a little faster..."
  "I always thought it was fear, but maybe there's something else?"
  if quest.kate_fate["escaped"]:
    "Maybe there's a need to have what's outside my reach."
    "To conquer my fears and demons."
    "And mend the wounds she caused."
    "To conquer her."
  else:
    "Perhaps it's some newly awoken dark desire."
    "The need to give up control and let her have her way completely."
    "Maybe I fought it too hard last time around?"
  "Then again, maybe it's just my desire for revenge."
  "If I could prove to her and myself that I'm better than her at something..."
  "Something that isn't video games..."
  "Then maybe I could finally move on and put the years of misery behind me."
  $quest.kate_desire.start()
  return

label quest_kate_desire_challenge_kate:
  show kate neutral with Dissolve(.5)
  kate neutral "What's with the sullen face?"
  mc "This is how I usually look..."
  kate confident "Okay, it's hard to argue with that."
  if quest.kate_fate["escaped"]:
    kate confident "So, you've come back to tempt fate, haven't you?"
    kate neutral "You got away last time. So, naturally we have some unfinished business."
    mc "We're not going to do it your way this year, [kate]."
    kate confident "We'll see about that..."
  else:
    kate confident "You've come back for more, haven't you?"
    mc "More?"
    kate confident "You liked getting tied up in the gym so much, so now you've come to provoke me."
    mc "Err..."
    "Fuck. Talking to [kate] always makes me so nervous."
    "Maybe she's right...?"
  mc "Actually, I've come to challenge you."
  kate laughing "Challenge me?!"
  kate laughing "Hahaha!"
  mc "..."
  kate smile "Wait, you're serious?!"
  kate laughing "Hahaha!"
  mc "Yes, I'm serious."
  kate confident "And what exactly are you going to challenge me in?"
  mc "I'll let you decide."
  kate confident "This is among the most ridiculous things I've heard."
  mc "Are you scared?"
  if mc.at("school_gym"):
    kate neutral "Fine. Meet me here again tomorrow and you'll have your challenge."
  else:
    kate neutral "Fine. Meet me in the gym tomorrow and you'll have your challenge."
  mc "Deal!"
  kate neutral "Now, get lost."
  show kate neutral at disappear_to_right
  "This might be the dumbest thing I've done..."
  "But [isabelle] always says to take the bull by the horns, and this is the most head-on confrontation possible."
  "Maybe I should ask her for advice?"
  $quest.kate_desire.advance("advice")
  return

label quest_kate_desire_isabelle_advice:
  show isabelle annoyed with Dissolve(.5)
  "[isabelle] looks like someone just stole her favorite chocolates..."
  mc "Are you okay?"
  isabelle annoyed "Not quite."
  if quest.kate_fate["spit_sock"]:
    #isabelle annoyed "I told your... err, I mean the principal, about [kate]'s behavior." ## Incest Patch ##
    isabelle annoyed_left "I told the principal about [kate]'s behavior."
    isabelle annoyed_left "I even showed her the wet socks."
    "Uh-oh..."
    mc "And what did she say?"
    isabelle annoyed "She said that [kate] is a model student and that the only one to surpass her in grades and extra credits is [maxine]."
    mc "That's probably true."
    isabelle angry "Bloody hell! It's hardly the point, is it?"
    show isabelle angry at move_to(.75)
    menu(side="left"):
      extend ""
      "\"Sorry, but the truth is that successful and attractive people get away with things others don't.\"":
        show isabelle angry at move_to(.5)
        mc "Sorry, but the truth is that successful and attractive people get away with things others don't."
        isabelle sad "I refuse to let that be the case."
        isabelle sad "Equality means putting a stop to injustices like this."
        $mc.intellect+=1
        mc "The best you can do is keep your head down... I believe I told you that on the first day here."
        $isabelle.lust-=1
        isabelle angry "I refuse! I refuse! I refuse!"
      "\"I know it's not, but [kate] does have the entire school behind her. You only have me.\"{space=-25}":
        show isabelle angry at move_to(.5)
        mc "I know it's not, but [kate] does have the entire school behind her. You only have me."
        $isabelle.lust+=1
        isabelle blush "Us against the world, eh?"
        isabelle blush "I still like our odds."
        mc "I don't, but at least our chances are twice as good."
        isabelle annoyed "Maybe we just walk up to her and tell her off?"
        mc "Again? Hmm... no."
  else:
    mc "What's wrong?"
    isabelle annoyed_left "I somehow ended up with twelve assignments for [mrsl]'s class tomorrow."
    isabelle annoyed_left "If I hadn't checked my locker earlier, I would've missed it entirely."
    mc "Twelve? That seems like a mixup."
    isabelle sad "You'd think, but when I talked to [mrsl] she said it was odd but correct..."
    isabelle sad "I had to spend an hour waiting for the principal to get it sorted."
    mc "That seems odd..."
    isabelle annoyed "It seems deliberate to me. [kate] is probably behind this as well."
    isabelle annoyed "I'm going to have a word with her... a very sharp lethal word."
    mc "What makes you think it's [kate]?"
    isabelle angry "Who else would it be?"
    isabelle angry "I just need to gather evidence."
  mc "Sadly, I think you're attacking the problem from the wrong angle."
  mc "Even if you do get [jo] to take action against [kate], the worst punishment she'll get is detention."
  mc "I have another idea..."
  isabelle concerned "Another idea?"
  mc "Yeah, I challenged [kate] earlier and she accepted."
  isabelle concerned "Challenged how?"
  mc "I'm... not exactly sure yet."
  mc "But I thought maybe you can help me win it?"
  isabelle skeptical "What will that accomplish?"
  mc "See, the one thing [kate] cares about is her image. If I can somehow best her..."
  isabelle concerned_left "Hmm... I see what you're saying. Maybe that slag will piss off for good...{space=-70}"
  isabelle concerned "When is this challenge taking place?"
  mc "Tomorrow in the gym."
  isabelle excited "Okay, I'll see what I can do to help you!"
  show isabelle excited at disappear_to_right
  "Great! So, [isabelle] will also witness my humiliation."
  "She's smart, though... maybe she can actually help me somehow..."
  "Well, better go home and rest so that I'm ready for the challenge."
  $quest.kate_desire.advance("rest")
  return

label quest_kate_desire_bed_sleep:
  "..."
  "[kate]. Oh, crap."
  "I barely had time to rub the sand out of my eyes, and already the anxiety of facing her is hitting me hard."
  "Probably not the best idea to challenge her, but maybe it's the only way?{space=-90}"
  "She has everything to lose and I... well, just my dignity, I guess..."
  "Better get dressed."
  $quest.kate_desire.advance("dress")
  return

label quest_kate_desire_kate_quest_gym:
  show casey confident flip at Transform(xalign=.055) behind kate
  show kate confident at Transform(xalign=.375)
  show stacy confident at Transform(xalign=.675) behind kate
  show lacey confident at Transform(xalign=.95) behind stacy
  with Dissolve(.5)
  kate confident "Well, well, well, look who it is."
  kate confident "Your head looks bigger than usual... I'm guessing it's that inflated ego.{space=-50}"
  kate confident "Luckily, my heels are sharp. I'll pop it like a zit if you bow down."
  show casey confident flip at move_to(-.25) behind kate
  show kate confident at move_to("left")
  show stacy confident at move_to("right")
  show lacey confident at move_to(1.25) behind stacy
  menu(side="middle"):
    extend ""
    "\"Cut the crap, [kate]. Let's start the challenge.\"":
      show casey confident flip at move_to(.055) behind kate
      show kate confident at move_to(.375)
      show stacy confident at move_to(.675) behind kate
      show lacey confident at move_to(.95) behind stacy
      mc "Cut the crap, [kate]. Let's start the challenge."
      show casey neutral flip
      show stacy neutral
      show lacey neutral
      kate neutral "Watch your mouth, [mc]."
      kate neutral "Remember who you're talking to."
      casey neutral flip "Yeah, show some respect, vermin."
      mc "Respect is earned."
      kate eyeroll "No, respect is what those without power show those with power."
      show casey confident flip
      show lacey confident
      stacy confident "You tell him, girl!"
      lacey confident "Queen!"
      mc "Let's just get this over with..."
      kate skeptical "Hey, now. You challenged me, remember?"
      kate skeptical "Be grateful I even deigned to consider it."
      casey confident flip "Katie with the vocab smackdown!"
      casey confident flip "[mc], you're lucky the challenge isn't an English test, because she would wreck you."
    "?mc.owned_item('kate_socks_clean')@|{image=items kate_socks_clean}|\"I've got something for you.\"":
      show casey confident flip at move_to(.055) behind kate
      show kate confident at move_to(.375)
      show stacy confident at move_to(.675) behind kate
      show lacey confident at move_to(.95) behind stacy
      mc "I've got something for you."
      show casey neutral flip
      show stacy neutral
      show lacey neutral
      kate thinking "And what's that?"
      mc "I cleaned your socks for you..."
      kate thinking "Actually?"
      mc "Yes. Here you go. Sorry that I got drool on them."
      $mc.remove_item("kate_socks_clean")
      $kate.lust+=2
      kate blush "I've always wanted a maid."
      kate blush "You're the perfect level of pathetic for the job. Don't be surprised if I make you do my laundry in the future."
      kate blush "I can picture you on your knees in my basement, wringing the water out by hand..."
      kate blush "You would like that, wouldn't you?"
      "She can probably see it in my face. This blush hides no secrets."
      "There's something highly erotic about being walked all over by someone like [kate]."
      "I hope she doesn't notice how hard I am right now..."
      kate skeptical "Answer me."
      mc "Yes, ma'am."
      show casey confident flip
      show stacy confident
      show lacey confident
      kate excited "That's what I thought."
    "\"I'm going to enjoy owning you.\"":
      show casey confident flip at move_to(.055) behind kate
      show kate confident at move_to(.375)
      show stacy confident at move_to(.675) behind kate
      show lacey confident at move_to(.95) behind stacy
      mc "I'm going to enjoy owning you."
      $kate.lust-=1
      kate laughing "Oh, yeah? That's what you think will happen?"
      kate smile "Are you sure you won't trip over your own shoelaces and pee yourself, like in freshman year?"
      "Ugh, the only reason that happened was because the strawberry juice was free that day..."
      "Can you really fault me for trying to stay hydrated?"
      kate laughing "What's wrong? Did you lose your voice already?"
      mc "No, I just thought back on all the times you were a bitch to me."
      mc "Your reckoning is at hand."
      show casey neutral flip
      show stacy neutral
      show lacey neutral
      kate eyeroll "You sound like a total nerd, you know?"
      "I was going for a righteous knight, ready to vanquish evil."
      "If that's nerdy, I don't want to be cool."
      show casey confident flip
      show stacy confident
      show lacey confident
  kate excited "So, [mc], how confident are you?"
  show casey confident flip at move_to(-.25) behind kate
  show kate excited at move_to("left")
  show stacy confident at move_to("right")
  show lacey confident at move_to(1.25) behind stacy
  menu(side="middle"):
    extend ""
    "\"I don't even know the challenge.\"":
      show casey confident flip at move_to(.055) behind kate
      show kate excited at move_to(.375)
      show stacy confident at move_to(.675) behind kate
      show lacey confident at move_to(.95) behind stacy
      mc "I don't even know the challenge."
      kate excited "Well, you did show up. Was that just to sniff my perfume?"
      mc "No, but you do smell nice."
      stacy confident_mocking "Check out Mr. Charmer!"
      show stacy confident
      kate flirty "Sorry, Mr. Charmer, but I'm unavailable."
      kate laughing "For you!"
      lacey confident "Queen!"
      mc "Can we start?"
      kate neutral "So impatient..."
      kate confident "Okay, girls, take it away."
    "?kate.lust>=6@[kate.lust]/6|{image= kate contact_icon}|{image= stats lust_3}|\"Let's up the stakes.\"":
      show casey confident flip at move_to(.055) behind kate
      show kate excited at move_to(.375)
      show stacy confident at move_to(.675) behind kate
      show lacey confident at move_to(.95) behind stacy
      mc "Let's up the stakes."
      kate excited "I guess the answer is, \"overconfident.\""
      kate excited "What do you have in mind?"
      mc "Let's do the challenge in our underwear."
      "If [kate] is going to humiliate me, at least I'll have a good view..."
      show casey neutral flip
      show stacy neutral
      show lacey neutral
      kate flirty "I knew you were a raging pervert."
      kate flirty "How are you going to hide your boner?"
      mc "Who says I want to hide it?"
      kate laughing "Very well... I'm not afraid of showing a little skin. I look stunning regardless."
      window hide
      show kate confident with Dissolve(.5)
      $kate.unequip("kate_cheerleader_top")
      pause 1.0
      $kate.unequip("kate_cheerleader_skirt")
      pause 1.0
      window auto
      "Goddamn. She's so hot."
      "If only I had the balls to make her mine..."
      kate confident "Your turn, loser."
      "Here goes nothing..."
      window hide
      show black with Dissolve(.5)
      pause 1.0
      play sound "<from 0.2>bedsheet"
      pause 1.0
      show casey confident_mocking flip
      show kate laughing
      show stacy confident_mocking
      show lacey confident_mocking
      hide black
      with Dissolve(.5)
      window auto
      kate laughing "Ew! Why did I agree to this?"
      kate laughing "I guess I'll make the jocks happy... and you, [mc], try not to make anyone throw up."
      kate smile "Okay, girls, take it away."
      show casey confident flip
      show kate confident
      show stacy confident
      show lacey confident
      with Dissolve(.5)
    "\"Please, go easy on me...\"":
      show casey confident flip at move_to(.055) behind kate
      show kate excited at move_to(.375)
      show stacy confident at move_to(.675) behind kate
      show lacey confident at move_to(.95) behind stacy
      mc "Please, go easy on me..."
      kate neutral "Go easy? We both know you don't want that."
      kate neutral "You want me to be merciless."
      $kate.lust+=1
      kate confident "And I will be. Don't you worry about that."
      "..."
      "Deep down, maybe that is what I want?"
      "I probably would never have challenged [kate] if my goal was to actually win."
      "Maybe, subconsciously, my goal was to just get stepped on all along?{space=-40}"
      mc "Well, err... bring it!"
      kate laughing "Don't get too cocky now!"
      kate smile "Okay, girls, take it away."
      show kate confident with Dissolve(.5)
  stacy confident "Okay, this is how the challenge will work."
  stacy confident "There are five hidden stars in the school. You each have to figure out where they are."
  stacy confident "On the back of each star there's a clue to the next star."
  casey confident flip "The first person to return here with all five stars is the winner!"
  mc "So, it's like Super Mario?"
  lacey neutral "What the hell is that?"
  casey confident flip "Some kind of nerd game. My five year old cousin plays it."
  lacey confident "This is a challenge of wits and stamina, not some stupid video game.{space=-20}"
  kate confident "And the loser does a forfeit... winner's choice."
  mc "What kind of forfeit?"
  kate flirty "I'm a nice girl, so it won't be too painful or humiliating for you."
  mc "And what if I win?"
  kate laughing "You won't, silly!"
  stacy confident "Are you both ready for the first clue?"
  "Man, they've probably rigged this somehow, but at least it's not sports.{space=-70}"
  kate smile "I doubt there'll be much of a challenge, but let's go."
  mc "I guess."
  stacy confident "Okay. First clue..."
  stacy confident "{i}\"When you're waiting for the principal, bored enough to moan.\"{/}"
  stacy confident "{i}\"Come look at me! I host a school of my own.\"{/}"
  kate thinking flip "..."
  mc "..."
  kate blush flip "Well, that's easy! Gotta run!"
  show casey confident flip at move_to(.2,1.25)
  show kate blush flip at disappear_to_right
  show stacy confident at move_to(.5,1)
  show lacey confident at move_to(.8,1.5)
  "Shit. Okay, better put my thinking cap on..."
  $kate["at_none_now"] = kate["at_none_today"] = False
  $mc["focus"] = "kate_desire"
  hide casey
  hide stacy
  hide lacey
  with Dissolve(.5)
  $quest.kate_desire.advance("starblue")
  return

label quest_kate_desire_aquarium:
  "Ah, got it — an aquarium holds a school of fish!"
  "There's the star too!"
  $mc.add_item("paper_star_blue")
  "Seems like [kate] already picked up hers... Gotta get a move on."
  "The next clue should be on the backside."
  $quest.kate_desire.advance("starred")
  return

label quest_kate_desire_bus_interact:
  "This has to be it..."
  "Big, long, gives rides..."
  "Surely, this is it..."
  "..."
  "Aha!"
  $mc.add_item("paper_star_red")
  show kate confident at appear_from_left
  kate confident "So, that's where it was. Thanks for finding it."
  mc "You followed me?"
  kate smile_right "Yeah, I felt like grabbing a snack from the cafeteria."
  kate laughing "I can't believe it took you this long to find it, though!"
  mc "You're welcome..."
  kate confident "So, what's next, Sherlock?"
  show kate confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"This is cheating!\"":
      show kate confident at move_to(.5)
      mc "This is cheating!"
      kate neutral "No, this is me making you do all the work. Get used to it."
      mc "What do you mean by that?"
      kate neutral "You're lazy."
      kate neutral "I'm planning to put you to work this year. Hard labor."
      kate confident "Crack that whip!"
      "Ugh..."
      kate flirty "You would like that, wouldn't you?"
      show kate flirty at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Yes, ma'am...\"":
          show kate flirty at move_to(.5)
          mc "Yes, ma'am..."
          $kate.lust+=1
          kate laughing "Go on, then, bitch. Get to work!"
        "\"We'll see who holds the whip this year.\"":
          show kate flirty at move_to(.5)
          mc "We'll see who holds the whip this year."
          $kate.lust-=1
          kate eyeroll "You're being a complete nuisance right now."
          kate eyeroll "Why won't you just give in and accept your fate?"
          mc "I have other things in mind, that's all."
          kate skeptical "Whatever, loser."
      hide kate with Dissolve(.5)
    "\"I don't know. I need to read the backside of the star...\"":
      show kate confident at move_to(.5)
      mc "I don't know. I need to read the backside of the star..."
      kate neutral "Well, what are you waiting for?"
      mc "Um..."
      kate laughing "You're illiterate, aren't you?"
      mc "Yes... would you mind helping me out?"
      kate smile "You're pathetic, aren't you?"
      mc "Very much so."
      kate blush "All right. I'll help you with this one."
      kate thinking "{i}\"Touch me once—\"{/}"
      window hide
      play sound "fast_whoosh"
      show kate thinking at move_to(.85) with hpunch
      show black with Dissolve(.5)
      call goto_school_ground_floor
      play sound "<from 9.2 to 10.0>door_breaking_in"
      pause 0.25
      window auto
      $mc.intellect+=1
      "I can't believe [kate] fell for the oldest trick in the book!"
      "Now, I have a head start for the next clue..."
    "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|\"Is following me around the best idea you've got?\"":
      show kate confident at move_to(.5)
      mc "Is following me around the best idea you've got?"
      kate skeptical "I got here way before you did."
      mc "How come you didn't pick up the star, then?"
      kate eyeroll "I wanted to give you a chance."
      mc "That's very stupid of you."
      mc "Because if I win this, I'm going to—"
      kate excited "Win? You think you can actually win?"
      mc "I've made the calculations. I have a 9.7%% chance of winning."
      kate excited "That doesn't sound like very good odds."
      mc "Oh, they're fine odds... about a one in ten chance."
      mc "Before this interaction, I had about a 4.2%% chance."
      mc "Now I know you're overconfident and I can use that to my advantage."
      mc "I also have intel on your game-plan."
      mc "Before today, I had zero percent chance."
      mc "So, go ahead and improve my chances of winning. I don't mind that one bit."
      kate cringe "Whatever, weirdo."
      hide kate with Dissolve(.5)
      $mc.love+=1
      "And that's how you destroy a sassy cheerleader with facts and logic.{space=-20}"
  $quest.kate_desire.advance("stargreen")
  return

label quest_kate_desire_piano_interact:
  "Phew! There's another star."
  $mc.add_item("paper_star_green")
  "All this running is making me sweat balls..."
  $quest.kate_desire.advance("holdup")
  "Son of a bitch. There's [kate] again."
  return

label quest_kate_desire_holdup:
  if not renpy.showing("kate"):
    show kate excited with Dissolve(.5)
  mc "Are you going to follow me around this whole challenge?"
  kate excited "Maybe... What are you going to do about it?"
  show kate excited at move_to(.25)
  menu(side="right"):
    extend ""
    "?kate.lust>=8@[kate.lust]/8|{image= kate contact_icon}|{image= stats lust_3}|\"I'm going to up the stakes.\"":
      show kate excited at move_to(.5)
      mc "I'm going to up the stakes."
      kate eyeroll "If you think I'll get naked, you can forget it."
      mc "I was thinking something else entirely..."
      mc "How about a small property wager?"
      kate cringe "You want my underwear, don't you? You damn pervert."
      mc "Actually, no. I know you're blackmailing the [nurse]."
      kate excited "I don't know what you're talking about."
      mc "You can deny it all you want, but here's the deal."
      mc "If I win..."
      show kate excited at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I want you to release her.\"":
          show kate excited at move_to(.5)
          $mc.love+=1
          mc "I want you to release her."
          $quest.kate_desire["stakes"] = "release"
        "\"I want the dirt you have on her.\"":
          show kate excited at move_to(.5)
          $mc.lust+=1
          mc "I want the dirt you have on her."
          $quest.kate_desire["stakes"] = "take"
      kate excited "And what could you possibly have that I want?"
      show kate excited at move_to(.25)
      menu(side="right"):
        extend ""
        "\"I'll get you dirt on [jo].\"":
          show kate excited at move_to(.5)
          mc "I'll get you dirt on [jo]."
          kate skeptical "She's clean as a whistle."
          mc "I guess you don't know the things I do..."
          kate skeptical "I'm listening."
          mc "I heard a rumor that she used to get throat pains while she climbed the career ladder."
          mc "You know what that means, don't you?"
          kate excited "That's very interesting..."
          kate excited "Are you saying you could find proof of it?"
          mc "That's exactly what I'm saying."
          kate flirty "That's a deal, then. I'm looking forward to having the principal firmly under my heel."
          kate smile "Now, if you'll excuse me, I have a challenge to win."
          show kate smile at disappear_to_left
          $quest.kate_desire["kate_deal"] = "dirt"
        "?mc.owned_item('maxine_nude')@|{image=items maxine_nude}|\"I'll get you a nude of [maxine].\"" if not mc.owned_item("maxine_other_nude"):
          jump quest_kate_desire_holdup_maxine_nude
        "?mc.owned_item('maxine_other_nude')@|{image=items maxine_other_nude}|\"I'll get you a nude of [maxine].\"" if mc.owned_item("maxine_other_nude"):
          label quest_kate_desire_holdup_maxine_nude:
          show kate excited at move_to(.5)
          mc "I'll get you a nude of [maxine]."
          kate skeptical "First of all, how did you get a nude of her?"
          kate skeptical "Secondly, why do you think I care?"
          mc "She's your only competition for valedictorian at the end of the year."
          mc "Having dirt on her might give you an edge."
          kate thinking "You're not as dumb as you look..."
          kate blush "Fine. That's a deal."
          kate blush "See you at the finish line, turtle boy."
          show kate blush at disappear_to_left
          $quest.kate_desire["kate_deal"] = "nude"
      pause 1.0
    "?mc.strength>=8@[mc.strength]/8|{image=stats str}|\"I'm going to lock you up in the supply closet.\"":
      show kate excited at move_to(.5)
      mc "I'm going to lock you up in the supply closet."
      kate eyeroll "No, you're not."
      "She's calling my bluff..."
      "Time to either all-in or fold."
      show kate eyeroll at move_to(.75)
      menu(side="left"):
        extend ""
        "Lock that bitch up":
          show kate eyeroll at move_to(.5)
          mc "Watch me."
          window hide
          show black with Dissolve(.5)
          pause 0.25
          $set_dialog_mode("default_no_bg")
          kate "Hey, don't touch me, you weirdo!" with vpunch
          kate "Hey!"
          pause 1.0
          play sound "<from 9.2 to 10.0>door_breaking_in"
          pause 1.0
          play sound "lock_click"
          pause 0.25
          $school_art_class["exclusive"] = "kate"
          call goto_school_art_class
          play sound "<from 19.1 to 20.4>door_breaking_in"
          window auto
          kate "Let me out, you psycho!" with vpunch
          kate "This is cheating!"
          play sound "<from 19.1 to 20.4>door_breaking_in"
          "There's some form of poetic justice to this..."
          "And not only that, this feeling of power is intoxicating."
          "Robbing [kate] of her freedom, for once. Putting her in bondage."
          "Man, I'll be in so much trouble for this, but maybe it's worth it?"
          "After all, she does deserve to be put in her place."
          "Who would've thought that those hours at the gym would lead to something like this..."
          play sound "<from 19.1 to 20.4>door_breaking_in"
          kate "[mc], I'm going to tell everyone about this!" with vpunch
          kate "You'll get detention for life!"
          play sound "<from 19.1 to 20.4>door_breaking_in"
          "Maybe... but it was worth it."
          mc "Shut up, [kate]."
          mc "Shut the fuck up."
          $kate.lust-=2
          kate "How dare—"
          window hide None
          play sound "<from 9.2 to 10.0>door_breaking_in"
          $game.location = "school_first_hall_west"
          show kate with hpunch
          pause 0.25
          window auto
          "I don't have to listen to that."
          "Now to find the remaining stars..."
          $mc["detention"]+=1
        "Don't be stupid":
          show kate eyeroll at move_to(.5)
          mc "..."
          $kate.lust+=1
          kate excited "That's right, you little sissy."
          kate excited "You're not locking anyone up."
          kate skeptical "The audacity to threaten me like that..."
          kate skeptical "I'll have to come up with an appropriate punishment."
          "Shit, I'm in so much trouble..."
          "Talking back to [kate] is never a good idea."
          kate eyeroll "But it'll have to be after I've won this stupid challenge."
          show kate eyeroll at disappear_to_right
          pause 1.0
    "\"Who says I'm going to do anything?\"":
      show kate excited at move_to(.5)
      mc "Who says I'm going to do anything?"
      kate eyeroll "That was a rhetorical question. We both know you're not going to do shit.{space=-150}"
      kate flirty "You're probably just doing this to be in my presence. Even the smell of my dirty socks turn you on."
      kate laughing "You're like one of those untrained puppies, who constantly pee on the mat for attention."
      kate smile "But don't worry, I'll train you well..."
      kate confident "Just do as you're told and everything will turn out fine."
      kate confident "Admit defeat now, and we can move on to more interesting things."
      "Crap. My knees get all wobbly and weak when [kate] starts bossing me around..."
      show kate confident at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Fine, I give up...\"":
          show kate confident at move_to(.5)
          mc "Fine, I give up..."
          kate confident "Say that one more time."
          mc "I give up. You win."
          $kate.lust+=1
          kate flirty "That's better."
          kate laughing "And how utterly pathetic! You couldn't even complete the challenge."
          kate smile "Very well, let's report back to the squad."
          kate smile "You probably can't wait to bow down and kiss my feet."
          "There was no chance I'd win anyway... better just get this humiliation over with."
          jump quest_kate_desire_loser_end
        "\"Never!\"":
          show kate confident at move_to(.5)
          mc "Never!"
          kate eyeroll "Seriously? You know you're going to lose in the end."
          kate eyeroll "Why not spare yourself the effort?"
          mc "Because..."
          "I used to give up all the time before. At the first sign of struggle."
          "This time I'll fight to the bitter end!"
          mc "Because that would be cowardly."
          kate skeptical "What? Are you some kind of knight now with a code of honor?"
          mc "Good luck, [kate]."
          mc "See you at the finish line."
          call goto_school_first_hall
          "It felt so good to leave her in the dust like that!"
          "Now to find the next star before she can catch up..."
  $quest.kate_desire.advance("starblack")
  return

label quest_kate_desire_skeleton_interact:
  "Something hard inside you all life long?"
  "I was thinking a boner at first... but of course it's bones."
  "There's the star."
  $mc.add_item("paper_star_black")
  if school_art_class["exclusive"] == "kate":
    "[kate] has probably managed to escape the supply closet by now. I better be quick finding the last one."
  else:
    "[kate] is probably right outside. I better be quick finding the last one."
  $mrsl["at_none_now"] = mrsl["at_none_today"] = False
  $quest.kate_desire.advance("stargold")
  return

label quest_kate_desire_mrsl_talk:
  if quest.kate_desire == "stargold" and quest.kate_desire["greeted_mrsl"]:
    "[mrsl] hasn't seen the golden star. What did the last clue say again?{space=-5}"
    return
  if not quest.kate_desire["greeted_mrsl"]:
    show mrsl flirty with Dissolve(.5)
    mc "Hi, [mrsl]!"
    mrsl laughing "Hello, [mc]! How's it going? You look a little flustered."
    if kate.equipped_item("kate_cheerleader_top") and kate.equipped_item("kate_cheerleader_skirt"):
      mc "I've been running..."
      mrsl confident "That's good."
    else:
      mrsl flirty "And underdressed..."
      mc "Please, don't give me detention!"
      mrsl confident "That depends on the reason for your current state of dress... or lack thereof."
      show mrsl confident at move_to(.25)
      menu(side="right"):
        extend ""
        "?mrsl.lust>=4@[mrsl.lust]/4|{image= mrsl contact_icon}|{image= stats lust_3}|\"I was looking at you from across the room...\"":
          show mrsl confident at move_to(.5)
          mc "I was looking at you from across the room..."
          mc "And my heart went boom!"
          mrsl concerned "That is very flattering, but it's not really much of an explanation..."
          mc "Well, the [nurse] told me to take my shirt off so she could examine the crater in my chest."
          $mrsl.lust+=2
          mrsl excited "Well, then you better hurry up and find her!"
          mc "Will do!"
        "\"It's all [kate]'s fault!\"":
          show mrsl confident at move_to(.5)
          mc "It's all [kate]'s fault!"
          mrsl concerned "I'm hearing a lot of excuses."
          mc "That was just one excuse..."
          mrsl annoyed "Don't get cheeky with me. I'll see you after school."
          $mc["detention"]+=1
          "Well, shit."
      mc "Just one more thing..."
    mc "You haven't seen any paper stars by any chance, have you?"
    mrsl concerned "Unfortunately, no."
    mc "Damn. Oh, well."
    $quest.kate_desire["greeted_mrsl"] = True
  if quest.kate_desire == "bathroomhelp":
    if not renpy.showing("mrsl"):
      show mrsl flirty with Dissolve(.5)
    mc "So, what um... what are you doing here?"
    mrsl confident "I'm waiting for a student-teacher conference with one of the cheerleaders."
    mrsl confident "Why?"
    mc "Err, no reason. Just curious."
    "Crap, I knew her being here was no coincidence!"
    mc "When is that?"
    mrsl annoyed "Should be soon, I hope... I think she might be running late."
    "I can't really wait for that."
    "[kate] might walk into the women's bathroom at any moment and pick up the last star."
  hide mrsl with Dissolve(.5)
  return

label quest_kate_desire_stargold_bathroom_interact:
  "Okay, so the last star must be in there... In the freaking women's bathroom."
  "I should've known they'd rig it like this! That's why [kate] has been so relaxed!"
  "Fuck!"
  "And of course [mrsl] is right there..."
  "Double fuck!"
  "Time to put my thinking cap on..."
  $isabelle["at_none_now"] = isabelle["at_none_today"] = False
  $quest.kate_desire.advance("bathroomhelp")
  return

label quest_kate_desire_bathroomhelp_bathroom_interact:
  "I need to come up with a plan."
  "Going inside will get me in too much trouble..."
  return

label quest_kate_desire_bathroomhelp_isabelle:
  show isabelle neutral with Dissolve(.5)
  mc "There you are!"
  isabelle concerned "Are you quite all right, [mc]?"
  mc "Yes, but I need your help!"
  if not kate.equipped_item("kate_cheerleader_top") and not kate.equipped_item("kate_cheerleader_skirt"):
    isabelle concerned "With what? Finding your clothes?"
    mc "Um... not exactly..."
  mc "You said you'd help me with the challenge if you could..."
  isabelle excited "Absolutely! What can I do?"
  mc "Follow me to the sports wing! Hurry!"
  if not kate.equipped_item("kate_cheerleader_top") and not kate.equipped_item("kate_cheerleader_skirt"):
    isabelle concerned  "Are you sure we shouldn't find your clothes first?"
    mc "Yes, it's part of the challenge!"
    mc "Unless you mind it?"
    isabelle excited "No, no! I don't judge!"
    $mc.charisma+=1
    $mc.strength+=1
    mc "You can do some judging if you want!"
    isabelle skeptical "Did you just flex...?"
    mc "Of course not! I have a resting Greek-god-posture."
    isabelle excited "If you say so!"
    mc "By Zeus, do I ever! Come on!"
  isabelle excited "Okay, let's go!"
  window hide
  show black with Dissolve(.5)
  pause 2.0
  $quest.kate_desire["follow_me"] = True
  call goto_school_first_hall_east
  show isabelle excited flip with Dissolve(.5)
  window auto
  isabelle excited flip "Okay, what are we doing?"
  mc "There should be a paper star somewhere inside the women's bathroom!"
  mc "You need to find it before [kate] gets here..."
  isabelle skeptical flip "Paper star. Got it."
  mc "Hurry, please!"
  show isabelle skeptical flip at disappear_to_right
  "I hope they haven't hidden it..."
  "Come on, [isabelle]! You can do this!"
  "Shit. There's [kate]."
  show kate excited at appear_from_left(.5,1.5)
  kate excited "You look nervous."
  "I need to distract her somehow to buy [isabelle] more time..."
  show kate excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"And you look amazing!\"":
      show kate excited at move_to(.5)
      mc "And you look amazing!"
      kate excited "I know I do."
      mc "Although..."
      kate skeptical "..."
      kate skeptical "You better be careful with the next words out of your mouth."
      mc "Oh, nevermind."
      kate excited "No, go on. Speak your mind."
      mc "No, it's okay."
      kate excited "I insist."
      mc "I, um... I really don't want to."
      kate eyeroll "[mc]. Now."
      mc "Just... that you'd look even better on a throne with a horde of servants at your beck and call..."
      $kate.lust+=1
      kate laughing "Is that what you're fantasizing about?"
      kate laughing "Being a servant to the queen?"
      mc "I didn't say that..."
      kate smile "But you thought it, didn't you?"
      kate flirty "Well, if you play your cards right, that might be in your future."
      kate confident "Now, if you'll excuse me, I have a challenge to win."
      window hide
      show kate confident at disappear_to_right
    "\"And you look lost.\"":
      show kate excited at move_to(.5)
      mc "And you look lost."
      kate neutral "Oh, yeah? What makes you say that?"
      mc "The fact that you've been following me around throughout this whole challenge..."
      $kate.lust-=1
      kate laughing "I've just been following you around to see how slow you are."
      kate smile "It's been very entertaining."
      mc "There's no way you'd risk losing to me just for a laugh or two."
      kate laughing "Who said anything about risking it?"
      mc "What, um... do you mean?"
      "Come on, [kate], evil villain monologue, please..."
      kate neutral "Are you trying to stall me?"
      mc "I don't know what you're talking about..."
      kate confident_right "So, you don't mind if I go into the women's bathroom, do you?"
      mc "Not one bit..."
      kate confident "Okay, then..."
      window hide
      show kate confident at disappear_to_right
    "\"That's because I know what's coming...\"":
      show kate excited at move_to(.5)
      mc "That's because I know what's coming..."
      kate excited "I'm glad you've accepted your defeat."
      mc "That's not it at all."
      mc "I know that tensions are growing within the cheer squad."
      kate skeptical "..."
      "Last time around, [kate] ended up kicking several dissenters from the cheerleader ranks."
      mc "You'll have a massive fallout before Christmas."
      kate eyeroll "I don't know who told you that, but it's a false rumor."
      kate eyeroll "And it's definitely none of your business."
      kate skeptical "Don't stick your drippy nose where it doesn't belong. Got it?"
      kate excited "Anyway, time to win myself a challenge."
      window hide
      show kate excited at disappear_to_right
  pause 0.5
  window auto
  mc "Wait!"
  show kate confident at appear_from_right
  kate confident "What?"
  "Come on, [isabelle]!"
  mc "I was just wondering why you've never had a boyfriend..."
  kate neutral "Not sure why you think that concerns you?"
  mc "I know that the other cheerleaders are sleeping around."
  kate laughing "Haha! I knew you were an angry virgin!"
  kate smile "Don't try to talk shit about my sisters. Having sex is completely normal and fine."
  mc "If it's so normal, why aren't you doing it?"
  kate excited "Who says I'm not?"
  "Twice now, I've seen [kate] without a boyfriend. It is pretty weird that the most popular girl is single."
  mc "It's your choice, I'm just curious..."
  kate eyeroll "Well, go be curious somewhere else."
  "I never really thought of it, but maybe she's gay?"
  "The thing with the [nurse]... that sure seems to have a sexual component."
  "But then, she sure is dominant toward me as well."
  "Maybe she's bisexual..."
  "Or maybe she only gets off on having power over other people? That would make sense."
  mc "I guess you just haven't found the right person yet."
  kate cringe "Whatever, creep."
  kate cringe "It's not like you have a—"
  show kate cringe at move_to(.25,1.0)
  show isabelle neutral at appear_from_right(.75)
  pause 0.0
  kate annoyed_right "Oh, look, it's the ugly duckling."
  isabelle concerned_left "You do realize that the ugly duckling was a swan, right?"
  kate laughing "I just know that it was ugly like you."
  isabelle concerned_left "When do you reckon you'll start acting like a decent human being?"
  kate smile_right "As soon as you start showing me some respect."
  isabelle concerned_left "I've been plenty respectful."
  if kate.equipped_item("kate_cheerleader_top") and kate.equipped_item("kate_cheerleader_skirt"):
    kate laughing "Prove it, then. My shoes await your lips."
  else:
    kate laughing "Prove it, then. My feet await your lips."
  isabelle skeptical "You've lost the bloody plot, mate."
  kate confident_right "You'll do it eventually. Mark my words."
  isabelle skeptical "No one would."
  kate confident "[mc] would."
  kate confident "Right?"
  show kate confident at move_to("left")
  show isabelle skeptical at move_to("right")
  menu(side="middle"):
    extend ""
    "?quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed@|{image=isabelle_photo_small}|\"I mean, those are some nice shoes...\"{space=-60}" if kate.equipped_item("kate_cheerleader_top") and kate.equipped_item("kate_cheerleader_skirt"):
      show kate confident at move_to(.25)
      show isabelle skeptical at move_to(.75)
      mc "I mean, those are some nice shoes..."
      jump quest_kate_desire_bathroomhelp_isabelle_mc_would
    "?quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed@|{image=isabelle_photo_small}|\"I mean, those are some nice feet...\"{space=-35}" if not kate.equipped_item("kate_cheerleader_top") and not kate.equipped_item("kate_cheerleader_skirt"):
      show kate confident at move_to(.25)
      show isabelle skeptical at move_to(.75)
      mc "I mean, those are some nice feet..."
      label quest_kate_desire_bathroomhelp_isabelle_mc_would:
      $isabelle.love-=1
      isabelle angry "Seriously, [mc]?!"
      $kate.lust+=1
      kate laughing "See, English? Nothing to throw a fit over."
      isabelle annoyed_left "Don't call me \"English.\" I have a name."
      kate smile_right "Yeah? Everything about you is so boring, it just passes through my head without sticking."
      isabelle annoyed_left "Probably 'cause there's nothing in there to stick to."
      mc "Badum-tss!"
      kate angry "Shut up, [mc]!"
      mc "Sorry, ma'am..."
      "Shit. That was stupid."
      "[kate] doesn't like getting owned."
      "Her eyes are already turning into smoldering pits of malice..."
      isabelle sad "I hope you learn to stand up for yourself at some point, [mc]."
      isabelle blush "Anyway, here you go."
      $mc.add_item("paper_star_gold")
      mc "Thanks!"
      isabelle blush "See you around."
      show kate angry at move_to(.5)
    "?quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed@|{image=kate_photo_small}|\"You live in a fantasy world, [kate].\"{space=-10}":
      show kate confident at move_to(.25)
      show isabelle skeptical at move_to(.75)
      mc "You live in a fantasy world, [kate]."
      $kate.lust-=1
      kate skeptical "What did you just say?"
      $isabelle.lust+=1
      isabelle laughing "He said you're deluded, mate."
      kate annoyed_right "Are you his spokesperson now?"
      isabelle confident "[mc] can stand up for himself."
      mc "This is our senior year, [kate]... It's time to mature."
      kate eyeroll "Get off the soap box, will you?"
      kate eyeroll "I know that you've got cartoon posters on your bedroom walls."
      "How the hell did she know that?"
      if kate.equipped_item("kate_cheerleader_top") and kate.equipped_item("kate_cheerleader_skirt"):
        kate excited "And look at how you dress in your sweatpants and hoodie!"
      else:
        kate excited "And look at you running around in your crusty old boxers!"
      kate excited "It's honestly a joke that you're even talking about maturity."
      isabelle annoyed_left "You do realize that interests and fashion sense have nothing to do with maturity, right?"
      kate gushing "You do realize that I don't care what you think, right?"
      isabelle annoyed_left "[kate], please talk to someone."
      isabelle blush "[mc], here you go."
      $mc.add_item("paper_star_gold")
      mc "Thank you, [isabelle]!"
      isabelle blush "Talk to you later."
      show kate gushing at move_to(.5)
  hide isabelle with Dissolve(.5)
  pause 0.0
  kate surprised "What's that?"
  mc "Oh, this? This is my victory ticket."
  kate gushing "Really? Can I see it?"
  show kate gushing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yes, take a good look...\"":
      show kate gushing at move_to(.5)
      mc "Yes, take a good look..."
      mc "This is what defeat tastes—"
      show kate gushing with vpunch:
        linear .25 zoom 2.5 yoffset 1750
      show kate gushing_star:
        linear .25 zoom 1 yoffset 0
      $mc.remove_item("paper_star_gold")
      mc "Hey!"
      kate gushing_star "Thank you! It's so pretty..."
      kate gushing_star "So, this is game over, I guess."
      mc "You're not mad that I beat you?"
      kate gushing_star "Not at all! Sore losers are the worst!"
      kate gushing_star "Come on, let's go tell the girls who won!"
      mc "All right..."
      window hide
      show kate gushing_star at disappear_to_left
      pause 0.5
      jump quest_kate_desire_conclusion_one
    "\"Nope. I don't think so.\"":
      show kate gushing at move_to(.5)
      mc "Nope. I don't think so."
      mc "I'm not that stupid."
      kate angry "..."
      $mc.intellect+=1
      "Ha! I knew it was just an act to snatch it out of my hand!"
      $kate.lust-=1
      kate angry "You cheated, you little weasel!"
      mc "No one cheated."
      kate angry "You were meant to find the stars on your own!"
      mc "You just followed me around, so then you cheated as well..."
      kate sad "Okay, so we're both disqualified."
      mc "No way. I found the star on my own, [isabelle] simply picked it up for me.{space=-110}"
      mc "Nothing in the rules against that."
      kate angry "..."
      mc "See you in the gym, {i}loser{/}."
      jump quest_kate_desire_conclusion_two

label quest_kate_desire_conclusion_one:
  call goto_school_gym
  show casey confident flip at Transform(xalign=.055) behind kate
  show kate gushing_star at Transform(xalign=.375)
  show stacy confident at Transform(xalign=.675) behind kate
  show lacey confident at Transform(xalign=.95) behind stacy
  with Dissolve(.5)
  window auto
  kate gushing_star "Look what I have!"
  mc "Wait, what?"
  stacy confident "Well, well, well, it looks like we have a winner!"
  mc "No, hold on! I found that star!"
  show casey neutral flip
  show stacy neutral
  show lacey neutral
  kate eyeroll "Are you serious? Just accept your defeat."
  casey neutral flip "Yeah, dude. Don't be a bad sport."
  kate eyeroll "Sore losers are the worst."
  lacey neutral "Oh my god, right???"
  kate excited "You're not going to bitch about it, are you?"
  show casey neutral flip at move_to(-.25) behind kate
  show kate excited at move_to("left")
  show stacy neutral at move_to("right")
  show lacey neutral at move_to(1.25) behind stacy
  menu(side="middle"):
    extend ""
    "\"No, ma'am...\"":
      show casey neutral flip at move_to(.055) behind kate
      show kate excited at move_to(.375)
      show stacy neutral at move_to(.675) behind kate
      show lacey neutral at move_to(.95) behind stacy
      mc "No, ma'am..."
      "They're never going to believe that [kate] snatched it out of my hands.{space=-25}"
      "This is how it was meant to turn out, anyway..."
      show casey confident flip
      show lacey confident
      stacy confident "So polite!"
      $kate.lust+=1
      kate flirty "Yeah, I've been training him."
      lacey confident "Queen!"
      kate neutral "So, [mc], are you going to be a good boy and take the forfeit without complaints?"
      mc "Yes, ma'am."
      "Ugh, I hope it's nothing terrible..."
      casey confident flip "I want him to call me \"ma'am\" too!"
      kate confident "You'll address all of us with respect, won't you?"
      mc "Yes, ma'am."
      "They all look at me like I'm their newest toy."
      "In a weird way, that feels quite good... I never thought that I'd have a group of hot cheerleaders' full attention."
      kate confident "Very well, then. Let's get to it."
      jump quest_kate_desire_spanking
    "?mc.intellect>=8 and mc.check_phone_contact('isabelle')@[mc.intellect]/8|{image=stats int}|{color=#fefefe}+{/}|{image= isabelle contact_icon}|{space=-10}{image= contact_info_call}|\"Ladies and gentlemen of\nthe jury.\"":
      show casey neutral flip at move_to(.055) behind kate
      show kate excited at move_to(.375)
      show stacy neutral at move_to(.675) behind kate
      show lacey neutral at move_to(.95) behind stacy
      mc "Ladies and gentlemen of the jury."
      mc "Allow me to present you with a question..."
      mc "To your knowledge, is there a person in this school that likes me more than [kate]?"
      show casey confident flip
      show lacey confident
      stacy confident "You're joking, right?"
      casey confident flip "I remember even [flora] saying that she hates you."
      "Ugh, that's probably true too..."
      lacey confident "[kate] is the queen!"
      mc "So, it would be safe to assume that no one would have a bias toward me."
      mc "With that in mind, I'd like to call my first witness to the stand."
      window hide
      $set_dialog_mode("phone_call","isabelle")
      pause 0.5
      "{i}Calling...{/}"
      "{i}...{/}"
      "{i}Calling...{/}"
      "{i}...{/}"
      show kate skeptical
      $set_dialog_mode("")
      pause 0.5
      window auto
      kate skeptical "What the hell are you on about?"
      mc "Hold on, [kate]. I'm on the phone."
      "Come on, pick up! Pick up..."
      window hide
      $set_dialog_mode("phone_call","isabelle")
      pause 0.5
      isabelle "Hello?"
      mc "Hello, [isabelle]! You're on speaker."
      isabelle "Okay... what's going on?"
      mc "Humor me, please."
      mc "Who found the golden star and had\nit in their possession when you left\nthe sports wing earlier today?"
      isabelle "You did."
      mc "Thank you, [isabelle]! Goodbye."
      $set_dialog_mode("")
      pause 0.5
      window auto
      mc "Now, according to the witness's testimony, the golden star was in my possession."
      mc "That is, until [kate] snatched it out of my hands and tried to claim the victory."
      show casey neutral flip
      show stacy neutral
      show lacey neutral
      with Dissolve(.5)
      kate skeptical "..."
      lacey neutral "What did he just say?"
      stacy neutral "[kate] would never do that!"
      casey neutral flip "I think the witness is lying."
      mc "You all agreed that there's no reason for the witness to lie for me."
      casey neutral flip "..."
      stacy neutral "..."
      lacey neutral "..."
      mc "I rest my case."
      if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_wit"]:
        $mc["moments_of_glory"]+=1
        $mc["moments_of_glory_wit"] = True
        $game.notify_modal(None,"Guts & Glory","Moments of Glory: Wit\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#       if mc["moments_of_glory"] == 3:
#         $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
      jump quest_kate_desire_winner

label quest_kate_desire_winner:
  kate eyeroll "Whatever. Fine, you won it. Are you happy?"
  if not mc.owned_item("paper_star_gold"):
    $mc.add_item("paper_star_gold")
  "I've been waiting for this moment for years..."
  "[kate] admitting defeat — yeah, that does have a nice ring to it!"
  mc "So, now I get to pick a forfeit, right?"
  stacy neutral "Well, yeah..."
  casey neutral flip "Within reason."
  lacey neutral "[kate] has to be okay with it."
  mc "That doesn't sound like a forfeit to me..."
  kate skeptical "Well, those are the rules. Deal with it."
  show casey neutral flip at move_to(-.25) behind kate
  show kate skeptical at move_to("left")
  show stacy neutral at move_to("right")
  show lacey neutral at move_to(1.25) behind stacy
  label quest_kate_desire_winner_no_blowjob:
  menu(side="middle"):
    extend ""
    "\"I want you to volunteer for the art class.\"":
      show casey neutral flip at move_to(.055) behind kate
      if quest.kate_desire["blowjob_chosen"]:
        show kate eyeroll at move_to(.375)
      else:
        show kate skeptical at move_to(.375)
      show stacy neutral at move_to(.675) behind kate
      show lacey neutral at move_to(.95) behind stacy
      mc "I want you to volunteer for the art class."
      mc "Specifically, as a nude model."
      kate eyeroll "Forget it, perv."
      mc "It's for art! There's nothing perverted about art."
      lacey neutral "I did read that art is above scrutiny."
      kate annoyed_right "Ugh! Shut up, Lacey."
      lacey neutral "Sorry! Shouldn't I have said that? I was just trying to help..."
      stacy neutral "No more talking now, sweetheart."
      mc "Well, that's your forfeit, [kate]."
      mc "Shouldn't have accepted the challenge if you're too much of a wimp to pay up."
      kate skeptical "Fuck off, the only wimp here is you."
      mc "Then do it..."
      kate excited "You know what? Fine."
      kate excited "It gives a lot of extra points, which should cement my place as our valedictorian."
      show stacy confident
      show lacey confident
      casey confident flip "You've got nothing to be ashamed of, girl! You'll do great!"
      stacy confident "Yeah, if you've got it, flaunt it! And you've absolutely got it, babe!"
      lacey confident "Queen!"
      "Damn, I wish I had this kind of support..."
      "Life must be so much easier with a retinue of mindless cronies."
      kate excited "Well, then. I guess that's it."
      mc "I guess so!"
      kate skeptical "Stop smiling, you look like an idiot."
      lacey confident "Yeah!"
      kate eyeroll "Let's go, girls."
      $quest.kate_desire["nude_model"] = True
      if quest.kate_desire["stakes"]:
        window hide
        show casey confident flip:
          pause 0.15
          easeout 1.25 xpos 1.0 xanchor 0.0
        show kate eyeroll:
          easeout 1.0 xpos 1.0 xanchor 0.0
        show stacy confident behind casey:
          easeout 1.5 xpos 1.0 xanchor 0.0
        show lacey confident behind stacy:
          pause 0.3
          easeout 0.75 xpos 1.0 xanchor 0.0
        pause 0.5
        window auto
        mc "Oh, I almost forgot! [kate]!"
        if quest.kate_desire["stakes"] == "release":
          show kate skeptical at appear_from_right
          kate skeptical "What?"
          mc "Our little side wager..."
          mc "Set the [nurse] free."
          kate annoyed "Fuck off, she's my property."
          mc "Then you shouldn't have taken the wager!"
          kate annoyed "She doesn't even want to be released."
          mc "I don't give a shit. You're done blackmailing her."
          kate eyeroll "Why do you have to be such a pushover?"
          mc "Doing what's right is hardly being a pushover."
          kate excited "You think she'll thank you or something? She won't."
          mc "I don't care. Blackmail is illegal."
          kate eyeroll "Whatever. You can tell her she's free."
          kate skeptical "Now, piss off."
          $mc["focus"] = ""
          hide kate with Dissolve(.5)
          "Man, it feels good to antagonize her!"
          "All right, let's go see the [nurse]..."
        else:
          show kate neutral at appear_from_right
          kate neutral "What?"
          mc "Our little side wager..."
          mc "Give me the dirt you have on the [nurse]."
          kate confident "What makes you think I have any?"
          kate confident "She's a subby little exhibitionist. She gets off on doing what I tell her.{space=-25}"
          mc "I know you, [kate]. You wouldn't take such risks if you hadn't."
          kate laughing "Maybe you're not as dumb as you look..."
          kate smile "Very well. Enjoy your victory while it lasts."
          $mc.add_item("damning_document")
          kate neutral "Bye, loser."
          mc "Winner, you mean?"
          kate neutral "Shut up."
          $mc["focus"] = ""
          hide kate with Dissolve(.5)
          "Sucks to suck, [kate]."
          "Time to find the [nurse] and inform her of her new fate..."
        $quest.kate_desire.advance("endnurse")
      else:
        $mc["focus"] = ""
        hide casey
        hide kate
        hide stacy
        hide lacey
        with Dissolve(.5)
        $quest.kate_desire.finish()
    "\"I want a blowjob.\"" if not quest.kate_desire["blowjob_chosen"]:
      $quest.kate_desire["blowjob_chosen"] = True
      show casey neutral flip at move_to(.055) behind kate
      show kate skeptical at move_to(.375)
      show stacy neutral at move_to(.675) behind kate
      show lacey neutral at move_to(.95) behind stacy
      mc "I want a blowjob."
      lacey neutral "What the fuck? Disgusting!" with vpunch
      stacy neutral "Are you being serious right now?" with vpunch
      casey neutral flip "That's hardly within reason!" with vpunch
      kate skeptical "It's fine."
      "The what, now?"
      stacy neutral "Have you gone mad?"
      kate annoyed_right "Don't question me."
      kate excited "You want a blowjob, [mc]?"
      "Why does this feel like a threat...?"
      if "love14" in game.unlocked_stat_perks:
        "Her demonic face from that horrible nightmare is forever seared into my memory..."
        "It might be best not to tempt fate. I'm quite fond of my dick."
      show casey neutral flip at move_to(-.25) behind kate
      show kate excited at move_to("left")
      show stacy neutral at move_to("right")
      show lacey neutral at move_to(1.25) behind stacy
      menu(side="middle"):
        extend ""
        "\"Yes, I do!\"":
          show casey neutral flip at move_to(.055) behind kate
          show kate excited at move_to(.375)
          show stacy neutral at move_to(.675) behind kate
          show lacey neutral at move_to(.95) behind stacy
          mc "Yes, I do!"
          kate excited "Okay, come with me."
          if not kate.equipped_item("kate_cheerleader_top") and not kate.equipped_item("kate_cheerleader_skirt"):
            window hide
            $kate.equip("kate_cheerleader_skirt")
            pause 1.0
            $kate.equip("kate_cheerleader_top")
            pause 1.0
            window auto
          casey neutral flip "..."
          stacy neutral "..."
          lacey neutral "What is [kate] doing?"
          kate eyeroll "Ignore them. Come on."
          mc "Um, okay... where are we going?"
          kate excited "Somewhere private."
          kate excited "Close your eyes and take my hand."
          mc "All right..."
          window hide
          $mc["focus"] = ""
          show black with Dissolve(.5)
          pause 2.0
          $set_dialog_mode("default_no_bg")
          "Man, where is she taking me?"
          mc "Err... maybe I should specify? I want to be on the receiving end of the blowjob."
          mc "I'm not giving Chad a blowjob..."
          kate "Haha! If only I had thought of that... That would've been hilarious!"
          mc "Ugh..."
          kate "Don't worry. You'll get your blowjob, you filthy pervert."
          kate "Wait here, and don't you dare open your eyes."
          "..."
          mc "[kate]...?"
          "Ugh, she's gone..."
          "She probably left me someplace stupid and..."
          "...or maybe she's getting ready?"
          "Imagine if her hobby is giving blowjobs? That would be something!"
          "..."
          "..."
          "She just fucking left me, didn't she?"
          "Fuck it, I'm—"
          play sound "<from 9.2 to 10.0>door_breaking_in"
          kate "Okay, come with me."
          mc "[kate]? What's going on?"
          kate "Just take a seat and relax."
          kate "You did win, so here's your prize..."
          scene black with Dissolve(.07)
          $game.location = "school_nurse_room"
          $renpy.pause(0.07)
          $renpy.get_registered_image("location").reset_scene()
          scene location
          show nurse blowjob_reveal
          with Dissolve(.5)
          window auto
          $set_dialog_mode("")
          mc "What the..."
          "I knew [kate] had something planned, but I sure didn't expect this..."
          "I can't believe the [nurse] is naked on her knees in front of me. This has to be a dream."
          kate "As promised — a blowjob."
          if quest.kate_desire["stakes"]:
            mc "You lost the bet, [kate]. Why are you still abusing her?"
            if quest.kate_desire["stakes"] == "release":
              show nurse blowjob_concerned_reveal with Dissolve(.5)
              kate "So, you're saying no to a free blowjob?"
              kate "You know these will cost you normally, right?"
              mc "I'm not letting you control her anymore."
              kate "Well, you're not getting a blowjob from me."
              kate "It's this or nothing."
              menu(side="middle"):
                extend ""
                "\"Fine... I'll take the blowjob.\"":
                  mc "Fine... I'll take the blowjob."
                  kate "A wise choice!"
                "\"This is not happening.\"":
                  mc "This is not happening."
                  kate "Your loss."
                  window hide
                  show black with Dissolve(.5)
                  pause 0.5
                  $nurse["outfit_stamp"] = nurse.outfit
                  $nurse.outfit = {"bra": None, "outfit": "nurse_outfit", "pants": None, "shirt": None}
                  show nurse thinking
                  hide black
                  with Dissolve(.5)
                  play sound "<from 9.2 to 10.0>door_breaking_in"
                  window auto
                  nurse thinking "..."
                  nurse blush "Thank you, [mc]. That was very kind of you."
                  if mc.owned_item("compromising_photo"):
                    mc "Don't be too quick to thank me."
                    mc "You may be free from [kate], but I still have a very compromising photo of you."
                    nurse concerned "Oh, my goodness..."
                    mc "That's right."
                    mc "And I may come to collect at any time."
                    $nurse.lust+=3
                    nurse blush "Oh, dear. I don't know what to say..."
                    mc "You don't have to say anything, you just have to obey."
                    mc "I'll see you around."
                    $school_nurse_room["curtain_off"] = False
                    hide nurse with Dissolve(.25)
                    "It feels good to finally have the [nurse] all to myself."
                    "This is going to be a good year..."
                  else:
                    mc "Don't mention it!"
                    mc "I'm not about to let tyranny run rampant here anymore."
                    mc "Consider yourself a free woman from now on."
                    $nurse.love+=5
                    $nurse.lust-=1
                    nurse blush "T-thank you. I don't know what else to say..."
                    nurse blush "If you ever need anything, let me know!"
                    mc "Just doing my part."
                    $school_nurse_room["curtain_off"] = False
                    hide nurse with Dissolve(.25)
                    "That felt great! I'm basically Parry Hotter now, releasing Dobbster."
                    "Maybe I should give her my sock and see what happens?"
                    $achievement.sock_dispenser.unlock()
                    "Anyway, a truly good deed done. Hopefully, the universe recognizes this change in me..."
                  $school_nurse_room["curtain_off"] = True
                  $nurse.outfit = nurse["outfit_stamp"]
                  $quest.kate_desire.finish()
                  return
            else:
              kate "Well, consider this a transfer of ownership..."
              nurse blowjob_concerned_reveal "W-what?"
              show nurse blowjob_concerned_annoyed_reveal with Dissolve(.5)
              kate "Quiet while the grown-ups talk."
              nurse blowjob_concerned_annoyed_reveal "Yes, miss."
              mc "Well, I still want the dirt you have on her."
              show nurse blowjob_concerned_reveal with Dissolve(.5)
              kate "And what makes you think I have any?"
              kate "She's a subby little exhibitionist. She gets off on doing what I tell her.{space=-25}"
              mc "I know you, [kate]. You wouldn't take such risks if you hadn't."
              kate "Maybe you're not as dumb as you look..."
              kate "Fine. Take it."
              $mc.add_item("damning_document")
              kate "Now, do you want the blowjob or not?"
              mc "I think I just might."
              kate "Just sit back and relax, then. I'll take care of this."
          show nurse blowjob_concerned_annoyed_reveal with Dissolve(.5)
          kate "Ask [mc] if you can suck his dick."
          nurse blowjob_annoyed_reveal "..."
          nurse blowjob_annoyed_reveal "May I perform fellatio on you, [mc]?"
          show nurse blowjob_reveal with Dissolve(.5)
          kate "Look at her beg! Isn't she cute?"
          menu(side="middle"):
            nurse "May I perform fellatio on you, [mc]?"
            "\"Yes, you may.\"":
              mc "Yes, you may."
            "\"I'm not sure about this...\"":
              mc "I'm not sure about this..."
              kate "You asked for a blowjob as a forfeit. Don't pretend you have morals now.{space=-95}"
              mc "I just don't find you forcing her that hot."
              kate "Who says I'm forcing her?"
              show nurse blowjob_encouragement with Dissolve(.5)
              kate "Am I forcing you, little piggy?"
              nurse blowjob_encouragement "Err, no! Not at all! I'm here of my own free will."
              show nurse blowjob_reveal with Dissolve(.5)
              kate "See?"
              if mc.owned_item("compromising_photo") or mc.owned_item("damning_document"):
                mc "Very well, then..."
              else:
                menu(side="middle"):
                  extend ""
                  "\"I don't believe that for a second.\"":
                    mc "I don't believe that for a second."
                    mc "I'm out of here."
                    $nurse.lust-=1
                    $nurse.love+=3
                    kate "Well, your loss."
                    window hide
                    call goto_school_ground_floor_west
                    play sound "<from 9.2 to 10.0>door_breaking_in"
                    window auto
                    "[kate] needs to be stopped. This is getting absurd."
                    $mc.love+=2
                    "Not sure how or when I'll do it... but damn it I will."
                    $quest.kate_desire.finish()
                    return
                  "\"Very well, then...\"":
                    mc "Very well, then..."
          show nurse blowjob_encouragement with Dissolve(.5)
          kate "Come on, piggy. Don't be shy now."
          "God, the [nurse] looks so submissive. What has [kate] done to her?"
          "It's like she has trained her to be completely docile."
          "There's a sheen in her eyes. It's like she's in a trance or something..."
          "Maybe this is what subspace looks like?"
          "Seeing her naked and meek awakens stirs something in me."
          "A very special kind of excitement..."
          "My pants suddenly feel pretty cramped..."
          show nurse blowjob_dick_out with vpunch
          nurse blowjob_dick_out "Oh, my goodness."
          kate "Look at that! A special treat for a special pig."
          "[kate] is really turning up the humiliation..."
          "The [nurse] seems nervous, but also excited."
          "I wonder what's going through her head."
          "Is the [nurse] straight, bi, or gay? Is she even turned on by a sucking a dick?"
          "Perhaps it's [kate]'s dominance over her that is the turn-on?"
          show nurse blowjob_shove with vpunch
          kate "Come on, piggy! Get in there!"
          "Her face brushes against my cock and that somehow makes it even harder."
          "Or perhaps I'm enjoying [kate]'s display of authority over her pet?"
          "Because that's clearly what the [nurse] is — a piggy pet."
          show nurse blowjob_headpat with Dissolve(.5)
          kate "Give her an order, [mc]. She likes that."
          menu(side="middle"):
            extend ""
            "\"Kiss my dick.\"":
              mc "Kiss my dick."
              kate "You heard him."
              nurse blowjob_headpat "Yes, miss."
              show nurse blowjob_kiss with Dissolve(.5)
              "She puts her lips right on the tip of my dick, gently smooching it."
              show nurse blowjob_lick2 with Dissolve(.5)
              show nurse blowjob_lick1 with Dissolve(.5)
              show nurse blowjob_lick2 with Dissolve(.5)
              "Her lips part for a moment and her tongue pokes at my pee hole."
              show nurse blowjob_kiss with Dissolve(.5)
              "Then, she adds slight suction."
              mc "F-fuck..."
              kate "She's a good piggy, isn't she?"
              $nurse.love+=1
              mc "Y-yes. God."
              kate "I have class in ten minutes, so let's hurry this along."
              kate "Take that dick in your mouth now."
            "\"Lick my dick.\"":
              mc "Lick my dick."
              kate "Do it."
              nurse blowjob_headpat "Yes, miss."
              window hide
              show nurse blowjob_lick2 with Dissolve(.5)
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              pause 0.15
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              pause 0.15
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              window auto
              kate "Good girl."
              "Her tongue darts out, carefully licking the bottom of my dick."
              window hide
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              pause 0.15
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              pause 0.15
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              show nurse blowjob_kiss with Dissolve(.5)
              window auto
              "She swirls it around the tip, puts it back in her mouth to have a taste.{space=-25}"
              "Her face remains expressionless, so perhaps it wasn't so bad?"
              window hide
              show nurse blowjob_lick2 with Dissolve(.3)
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick2 with Dissolve(.3)
              show nurse blowjob_lick1 with Dissolve(.3)
              show nurse blowjob_lick4 with Dissolve(.3)
              show nurse blowjob_lick3 with Dissolve(.3)
              show nurse blowjob_lick4 with Dissolve(.3)
              show nurse blowjob_lick3 with Dissolve(.3)
              show nurse blowjob_lick4 with Dissolve(.3)
              window auto
              "The tongue pops out again, takes another few licks along the shaft."
              kate "Okay, that's enough teasing. Put it in your mouth now."
            "\"Stop wasting time and put it in your mouth, bitch!\"":
              mc "Stop wasting time and put it in your mouth, bitch!"
              $nurse.lust+=1
              nurse blowjob_headpat "..."
              show nurse blowjob_kate_touched_mcs_dick_lmao with vpunch
              kate "Stop being so damn shy!"
              kate "I don't have all day! Suck that dick!"
              "Oh, f-fuck! [kate] just touched me down there..."
              "That's hotter than any mouth..."
          show nurse blowjob_suck1 with Dissolve(.5)
          "Slowly, the [nurse] opens her mouth, letting my dick rest on her tongue.{space=-55}"
          "Her hot breath bathes on my dick and almost makes me blow my load...{space=-70}"
          "But years of edging in front of the computer has given me an unmatched control."
          "[kate] watches over her like a prison guard. Nothing happens without her saying so."
          window hide
          show nurse blowjob_suck2 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck1 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck2 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck1 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck2 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck1 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck2 with Dissolve(.3)
          window auto
          "Finally, she shifts forward on her knees, wrapping the tip of my dick in her lips..."
          "Waves of heat and pleasure course through my lower abdomen."
          mc "F-fuck, that's good!"
          kate "You hear that, piggy? He thinks you're doing a good job."
          "This comment seems to calm the [nurse] a bit."
          window hide
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.5)
          show nurse blowjob_suck3 with Dissolve(.5)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.5)
          show nurse blowjob_suck3 with Dissolve(.5)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.5)
          show nurse blowjob_suck3 with Dissolve(.5)
          window auto
          "She takes her time, sucking the tip. Seemingly lost in her own world.{space=-10}"
          "While on her knees, she seems to forget that this is completely unacceptable behavior."
          "She would be fired if [jo] found out she was giving blowjobs to students."
          window hide
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_suck4 with Dissolve(.15)
          show nurse blowjob_suck5 with Dissolve(.15)
          pause 0.3
          show nurse blowjob_suck4 with Dissolve(.3)
          show nurse blowjob_suck3 with Dissolve(.3)
          window auto
          "But in her mind, it's just her and my dick..."
          "...and possibly [kate]'s dominance."
          "..."
          "But god... she's going too slowly..."
          "I need her to bob her head faster, take my dick deeper..."
          if not quest.kate_desire["stakes"]:
            show nurse blowjob_handsy with Dissolve(.5)
            "Maybe I can give her a hand..."
            show nurse blowjob_no_touching with vpunch
            kate "What exactly do you think you're doing?"
            kate "Did I say you could touch my property?"
            mc "F-fuck. Sorry."
            "[kate] throws her weight around, showing everyone here who is the boss.{space=-80}"
            "Perhaps it was greedy of me to assume..."
          show nurse blowjob_hair_grab with Dissolve(.5)
          "Suddenly, [kate] pulls the [nurse]'s head back by her hair."
          if quest.kate_desire["stakes"]:
            kate "Is this really the best you can do, little piggy?"
          else:
            kate "And you, little piggy... Why would he want to touch you?"
            kate "Aren't you doing a good job?"
          nurse blowjob_hair_grab "S-sorry, miss! I'll try harder!"
          kate "Yes, you'll try harder..."
          kate "...and deeper."
          show nurse blowjob_deepthroat1 with vpunch
          "Without warning, [kate] pushes the [nurse]'s head forward."
          "My dick slides over her tongue, hits the back of her mouth, then pushes deep into her throat."
          "Despite all this, she doesn't gag, and her lips stay wrapped around the base of my shaft."
          kate "See? This is how you suck dick, piggy!"
          kate "This is what guys want!"
          "[kate] holds her down, and I can feel the [nurse]'s panic starting to grow.{space=-50}"
          show nurse blowjob_deepthroat1 with vpunch
          "Her throat contracts. She finally gags on my dick, and cough up hot air from her lungs."
          "[kate] still keeps her in place, a look of twisted excitement spreading across her face."
          kate "You stay there until I say so..."
          window hide
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.3)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.15)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.15)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.15)
          pause 1.0
          show nurse blowjob_deepthroat2a with Dissolve(.15)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.15)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.15)
          pause 1.0
          show nurse blowjob_deepthroat2a with Dissolve(.15)
          show nurse blowjob_deepthroat3a with Dissolve(.15)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.15)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.15)
          pause 1.0
          show nurse blowjob_deepthroat2b with Dissolve(.15)
          show nurse blowjob_deepthroat3b with Dissolve(.15)
          show nurse blowjob_deepthroat2b with Dissolve(.15)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_scolding_deepthroat5 with vpunch
          window auto
          "Just as I'm about to blow my load down her throat, the [nurse] breaks from and pushes herself off my dick."
          kate "Bad piggy! I did not tell you to take it out of your throat yet!"
          kate "You know what this means, right?"
          nurse blowjob_scolding_deepthroat5 "{i}I... I get p-punished...{/}"
          "Still with her mouth in place, her voice sends vibrations through my dick.{space=-95}"
          kate "That's right! You get punished!"
          show nurse blowjob_deepthroat5 with Dissolve(.5)
          kate "But that's for later..."
          kate "Now keep sucking!"
          show nurse blowjob_deepthroat6 with vpunch
          mc "Oh, fuck!"
          "Just like that, my dick slides right back into her warm throat."
          window hide
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          pause 0.5
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          pause 0.5
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat2a with Dissolve(.1)
          show nurse blowjob_deepthroat3a with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with vpunch
          window auto
          kate "Did you know that semen makes up 60%% of her diet now?"
          kate "If you want to make a donation, now's your chance."
          "Does this mean that [kate] makes the [nurse] suck dick on a daily basis?{space=-45}"
          "I always wondered why the jocks looked so satisfied leaving the infirmary..."
          window hide
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat2b with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          show nurse blowjob_deepthroat2b with Dissolve(.1)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with Dissolve(.1)
          pause 0.3
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat2b with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          show nurse blowjob_deepthroat2b with Dissolve(.1)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with Dissolve(.1)
          pause 0.3
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat2b with Dissolve(.1)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.1)
          show nurse blowjob_deepthroat2b with Dissolve(.1)
          show nurse blowjob_deepthroat3b with Dissolve(.1)
          show nurse blowjob_deepthroat4 with Dissolve(.1)
          show nurse blowjob_deepthroat5 with Dissolve(.1)
          pause 0.15
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat5 with Dissolve(.05)
          pause 0.15
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat5 with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat5 with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat5 with Dissolve(.05)
          show nurse blowjob_deepthroat4 with Dissolve(.05)
          show nurse blowjob_deepthroat3b with Dissolve(.05)
          show nurse blowjob_deepthroat2b with Dissolve(.05)
          show nurse blowjob_eyes_closed_deepthroat6 with Dissolve(.05)
          window auto
          "Oh, god! I'm going to—"
          show nurse blowjob_cum_in_mouth
          nurse blowjob_cum_in_mouth "!!!" with vpunch
          "Rope upon thick rope of cum shoots down her throat."
          "A tear rolls down the [nurse]'s face as I fill her stomach with spunk."
          "[kate] keeps her head firmly in place until every last drop of semen leaves my dick."
          kate "Very good girl. Swallow it all."
          nurse blowjob_ahegao "Fwah!"
          kate "Now, how's that, [mc]? Satisfied?"
          "Satisfied doesn't even begin to describe it..."
          mc "Yeah..."
          kate "She's too out of it to notice, but I'll tell her you gave her a good grade.{space=-45}"
          "Completely drained from both the challenge and the blowjob, I fall back on the hospital bed."
          window hide
          show black with vpunch
          pause 2.0
          $game.advance()
          pause 2.0
          hide black
          hide nurse
          with Dissolve(.5)
          window auto
          "Damn, that blowjob knocked me out completely..."
          "Maybe I should've challenged [kate] years ago... but I probably just got lucky this time..."
          "And even though it wasn't [kate]'s lips wrapped around my dick, I can't really complain..."
          "That was better than I ever dreamed of..."
          $unlock_replay("kate_forfeit")
          $quest.kate_desire["sucked_off"] = True
          $quest.kate_desire.finish()
        "\"On second thought, no...\"":
          show casey neutral flip at move_to(.055) behind kate
          show kate excited at move_to(.375)
          show stacy neutral at move_to(.675) behind kate
          show lacey neutral at move_to(.95) behind stacy
          mc "On second thought, no..."
          kate eyeroll "Make up your mind already."
          show casey neutral flip at move_to(-.25) behind kate
          show kate eyeroll at move_to("left")
          show stacy neutral at move_to("right")
          show lacey neutral at move_to(1.25) behind stacy
          jump quest_kate_desire_winner_no_blowjob
    "\"I want you to donate to a charity.\"":
      $quest.kate_desire["charitable"] = True
      show casey neutral flip at move_to(.055) behind kate
      if quest.kate_desire["blowjob_chosen"]:
        show kate eyeroll at move_to(.375)
      else:
        show kate skeptical at move_to(.375)
      show stacy neutral at move_to(.675) behind kate
      show lacey neutral at move_to(.95) behind stacy
      $mc.love+=3
      mc "I want you to donate to a charity."
      mc "I know you're filthy rich, [kate]. And I think it's time to make a difference.{space=-70}"
      kate thinking "That's what you want?"
      "She looks confused and relieved at the same time..."
      "She probably thought I'd pick something selfish. Something humiliating.{space=-75}"
      "But life is more than just high school and getting your rocks off."
      "There are people out there who could use the help."
      mc "That's what I want."
      "I read somewhere that the Newfall Hospital is struggling financially, so maybe that would be a good cause?"
      mc "I want you to donate to our local hospital."
      mc "And not just a couple of bucks — I want it to make a difference."
      kate thinking "Very well."
      "They're all so quiet now... they never expected this."
      kate thinking "Is that all?"
      mc "That is all."
      kate blush "All right, then..."
      show casey confident flip
      show lacey confident
      stacy confident "I then declare this challenge concluded."
      casey confident flip "Let's go."
      if quest.kate_desire["stakes"]:
        window hide
        show casey confident flip:
          pause 0.15
          easeout 1.25 xpos 1.0 xanchor 0.0
        show kate blush:
          easeout 1.0 xpos 1.0 xanchor 0.0
        show stacy confident behind casey:
          easeout 1.5 xpos 1.0 xanchor 0.0
        show lacey confident behind stacy:
          pause 0.3
          easeout 0.75 xpos 1.0 xanchor 0.0
        pause 0.5
        window auto
        mc "Oh! One more thing, [kate]!"
        if quest.kate_desire["stakes"] == "release":
          show kate skeptical at appear_from_right
          kate skeptical "What?"
          mc "Don't forget about our side wager..."
          kate eyeroll "Seriously?"
          mc "Yes, seriously."
          mc "You lost, [kate]."
          mc "It's time to release the poor [nurse]."
          kate excited "You do realize she likes it?"
          mc "I don't care. Blackmail is illegal."
          kate eyeroll "You're such a pushover..."
          kate skeptical "Okay, fine. You can tell her she's free."
          mc "Great!"
          $mc["focus"] = ""
          hide kate with Dissolve(.5)
          "Two good deeds in one day! That doesn't happen often for me."
          "Who knew being the good guy would feel so badass?"
          "[kate] sure won't be happy for a long time."
          "Okay, time to tell the [nurse] the good news..."
        else:
          show kate neutral at appear_from_right
          kate neutral "What?"
          mc "Don't forget about our side wager..."
          kate neutral "Seriously?"
          mc "Yes, seriously."
          mc "You lost, [kate]."
          mc "I want the dirt you have on the [nurse]."
          kate laughing "From philanthropist to extortionist just like that?"
          kate smile "I knew you were just trying to impress the girls with the donation bullshit.{space=-110}"
          mc "No, I'm very serious about that. But I also want something for myself.{space=-30}"
          mc "So, hand over whatever dirt you have on her..."
          kate neutral "And what makes you think I have any?"
          kate confident "Maybe she just likes to be my little piggy?"
          mc "You must have something. You wouldn't risk treating a school staff member like that without any insurance."
          kate laughing "Seems like you've given this a lot of thought."
          kate smile "Very well... enjoy."
          $mc.add_item("damning_document")
          kate neutral "Later, you two-faced fuck."
          $mc["focus"] = ""
          hide kate with Dissolve(.5)
          "Meh, who cares what she thinks anyway?"
          "I helped others, but I also helped myself!"
          "No one can blame me for not being entirely selfless."
          "..."
          "All right, time to pay the [nurse] a visit..."
        $quest.kate_desire.advance("endnurse")
      else:
        $mc["focus"] = ""
        hide casey
        hide kate
        hide stacy
        hide lacey
        with Dissolve(.5)
        "Quite the scalp to take so early in the year."
        "This certainly changes things..."
        "Maybe it'll make [kate] respect me more?"
        "Maybe people won't think I'm a complete loser anymore?"
        "Either way, it feels fantastic to stick it to her and reclaim some of my humanity in the process."
        $quest.kate_desire.finish()
  return

label quest_kate_desire_conclusion_two:
  call goto_school_gym
  show casey neutral flip at Transform(xalign=.055) behind kate
  show kate sad at Transform(xalign=.375)
  show stacy neutral at Transform(xalign=.675) behind kate
  show lacey neutral at Transform(xalign=.95) behind stacy
  with Dissolve(.5)
  kate sad "He cheated."
  mc "I did not cheat... Stop being a sore loser, [kate]."
  stacy neutral "Hush, [mc]."
  stacy neutral "How did he cheat?"
  kate sad "He got that bitchy exchange student to help him."
  lacey neutral "Cheater!"
  mc "[isabelle] only grabbed it for me from the women's bathroom. I figured out the location on my own."
  mc "That's not against the rules."
  stacy neutral "Hmm..."
  mc "Besides, [kate] followed me around for the entire challenge — that's even worse."
  casey "I don't know... I think you played a bit unfair..."
  stacy "Yeah, I feel like you should've picked up the star on your own..."
  mc "You said that we have to figure out where the stars are, and then bring them all to you."
  mc "That's what I did."
  jump quest_kate_desire_winner

label quest_kate_desire_spanking:
  if not kate.equipped_item("kate_cheerleader_top") and not kate.equipped_item("kate_cheerleader_skirt"):
    window hide
    $kate.equip("kate_cheerleader_skirt")
    pause 1.0
    $kate.equip("kate_cheerleader_top")
    pause 1.0
    show casey confident_paddle flip
    show kate confident_paddle
    show stacy confident_paddle
    show lacey confident_paddle
    with Dissolve(.5)
    window auto
  show casey confident_paddle flip
  show stacy confident_paddle
  show lacey confident_paddle
  kate confident_paddle "All right, [mc], pull your pants down and bend over."
  kate confident_paddle "This is going to hurt."
  "Is she serious?"
  show casey neutral_paddle flip
  show stacy neutral_paddle
  show lacey neutral_paddle
  kate neutral_paddle "Get down!"
  casey neutral_paddle flip "Assume the position!"
  stacy neutral_paddle "Down, bitch!"
  lacey neutral_paddle "On all fours!"
  "Shit, there's too many of them..."
  window hide
  $mc["focus"] = ""
  show black with Dissolve(.5)
  pause 0.5
  hide black
  show kate spanking_bend_over
  with Dissolve(.5)
  window auto
  kate spanking_bend_over "Now, isn't this so much better?"
  kate spanking_bend_over "Finally, at our feet where you belong."
  "Crap. This is not good."
  stacy "Answer her!"
  mc "Yes, ma'am. This is better..."
  casey "Oh, he agrees! You've trained him well!"
  lacey "Queen!"
  "God, this is so humiliating... I'm completely at their mercy."
  kate spanking_bend_over "So, girls, how many swats does he deserve?"
  lacey "Like, ten at least!"
  casey "Come on, [lacey]."
  lacey "What?"
  "That's probably as high as she can count..."
  kate spanking_bend_over "The answer is... all of them."
  "Shit. Fuck. Cock."
  kate "Don't you agree, [mc]?"
  mc "Yes, ma'am. All of them..."
  stacy "Oh, this is exciting! He's such a little bitch boy!"
  kate spanking_bend_over "Oh, he will be a total bitch after this, won't he?"
  lacey "He better be!"
  kate spanking_bend_over "Pants down, bitch."
  "I better do as they say..."
  window hide
  pause 0.5
  show kate spanking_pants_down with Dissolve(.5)
  window auto
  lacey "He totally did it!"
  kate spanking_pants_down "Of course he did. He knows he's not the one wearing the pants in this scenario."
  casey "Are you ready for your forfeit, [mc]?"
  mc "Yes, ma'am..."
  kate spanking_pants_down "Then, why aren't your shorts down too?"
  "Oh, god. They'll see everything!"
  stacy "Do as you're told, bitch."
  "Crap, this is not the right time to freeze up..."
  kate spanking_pants_down "I think he's nervous..."
  kate spanking_pants_down "[stacy], can you hold him?"
  stacy "With pleasure!"
  show kate spanking_chair with Dissolve(.5)
  kate spanking_chair "Are you nervous, [mc]?"
  mc "Y-yes..."
  kate spanking_chair "Yes, what?"
  mc "Yes, ma'am..."
  kate spanking_chair "Don't forget it again or we'll be here all night."
  mc "Sorry, ma'am."
  kate spanking_chair "Now, pull those shorts down. I won't ask you again."
  window hide
  pause 0.5
  show kate spanking_kate1 with Dissolve(.5)
  window auto
  "Oh, fuck. This is happening."
  "On some level I thought it was a joke, but clearly they mean business..."
  kate spanking_kate1 "Look at that juicy ass, just begging for some good discipline."
  mc "..."
  lacey "He looks scared!"
  casey "He should be!"
  kate spanking_kate1 "You don't mind if I hit as hard as I can, right?"
  mc "Um..."
  stacy "Speak clearly!"
  mc "No, ma'am!"
  kate spanking_kate2 "We'll see about that..."
  window hide
  show kate spanking_kate3 with Dissolve(.05)
  show kate spanking_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate7 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  window auto
  mc "Fuck me!"
  stacy "Not likely!"
  window hide
  show kate spanking_kate3 with Dissolve(.05)
  show kate spanking_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_kate3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate7 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_kate3 with Dissolve(.05)
  show kate spanking_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_kate5 with Dissolve(.05)
  show kate spanking_kate6 with Dissolve(.05)
  show kate spanking_kate7 with Dissolve(.05)
  show kate spanking_kate2 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.5)
  window auto
  "Each hit of the paddles sends shockwaves through my core..."
  "Instant pain. Lingering heat. An odd pleasure."
  window hide
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_kate3 with Dissolve(.05)
  show kate spanking_red_kate4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_kate5 with Dissolve(.05)
  show kate spanking_red_kate6 with Dissolve(.05)
  show kate spanking_red_kate7 with Dissolve(.05)
  show kate spanking_red_kate2 with Dissolve(.05)
  window auto
  "Stacy sits on my back, holding me down, while [kate] brings the pain.{space=-5}"
  "The heat from my ass flows through my skin and hits my face as well.{space=-40}"
  "But perhaps that's just the embarrassment of being spanked by a group of hot cheerleaders?"
  show kate spanking_stacy_up_next with Dissolve(.5)
  kate spanking_stacy_up_next "How's that, [mc]?"
  mc "It... burns..."
  lacey "He forgot again!"
  casey "Address us with respect, you maggot!"
  mc "I'm sorry, ma'am!"
  mc "C-can I go now?"
  kate spanking_stacy_up_next "Are you being serious right now?"
  kate spanking_stacy_up_next "No, you can't go!"
  kate spanking_stacy_up_next "But what you can do is ask for more..."
  "Oh, god..."
  menu(side="middle"):
    extend ""
    "\"Please, ma'am, can I have some more?\"":
      mc "Please, ma'am, can I have some more?"
      $kate.lust+=1
      kate spanking_stacy_up_next "You absolutely can."
      stacy "My turn, then..."
    "\"Please, no more...\"":
      mc "Please, no more..."
      casey "That is not what any of us want to hear."
      lacey "Nuh-uh! I haven't had my turn yet."
      $kate.lust-=1
      kate spanking_stacy_up_next "Maybe he'll be more agreeable after another round?"
      stacy "I think you're right."
  stacy "Would you mind holding him down for me, [kate]?"
  kate spanking_stacy_up_next "With pleasure!"
  show kate spanking_red_stacy1 with Dissolve(.5)
  stacy "The summer is way too long... I always miss putting losers back in their place."
  window hide
  show kate spanking_red_stacy2 with Dissolve(.5)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  window auto
  mc "Ow! Ow! Ow!"
  "God, that hurt! She's not hitting as hard as [kate], but her technique is painfully good..."
  "My ass is on fire!"
  "But the shame is much worse than the pain..."
  stacy "You won't be able to sit for a week after this!"
  window hide
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy4 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy4 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_red_stacy3 with Dissolve(.05)
  show kate spanking_red_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_red_stacy5 with Dissolve(.05)
  show kate spanking_red_stacy6 with Dissolve(.05)
  show kate spanking_red_stacy7 with Dissolve(.05)
  show kate spanking_red_stacy2 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.5)
  window auto
  "Each stroke of the paddle puts me further and further under their control..."
  "The pain fuels the lust of submission..."
  window hide
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_stacy3 with Dissolve(.05)
  show kate spanking_redder_stacy4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_stacy5 with Dissolve(.05)
  show kate spanking_redder_stacy6 with Dissolve(.05)
  show kate spanking_redder_stacy7 with Dissolve(.05)
  show kate spanking_redder_stacy2 with Dissolve(.05)
  window auto
  mc "Ouch!"
  "If I don't put a stop to this, I'll be their plaything for the rest of the year...{space=-90}"
  show kate spanking_redder_casey_up_next with Dissolve(.5)
  kate spanking_redder_casey_up_next "You got him really good, [stacy]!"
  stacy "As good as he deserves."
  lacey "Look at that red ass! Say something, [mc]!"
  mc "O-oww..."
  kate spanking_redder_casey_up_next "Not animal noises. Thank [stacy] for setting you straight with the paddle.{space=-75}"
  mc "T-thanks... for setting me straight, ma'am..."
  kate spanking_redder_casey_up_next "That's a good boy. We'll teach you some manners yet."
  kate spanking_redder_casey_up_next "Now, ask [casey] for more."
  menu(side="middle"):
    extend ""
    "\"Please, Miss [casey], can I have some more?\"{space=-35}":
      mc "Please, Miss [casey], can I have some more?"
      casey "Oh, I like the sound of that. Very polite."
      casey "I'll happily oblige."
      $kate.lust+=2
    "\"This is humiliating enough! I won't ask for more...\"{space=-120}":
      mc "This is humiliating enough! I won't ask for more..."
      lacey "Wow, rude! That's rude!"
      $kate.lust-=1
      kate spanking_redder_casey_up_next "Yeah, he still has a lot to learn it seems."
      kate spanking_redder_casey_up_next "Maybe we'll have to introduce weekly spankings this year..."
  kate spanking_redder_casey_up_next "Go ahead, [casey]."
  show kate spanking_redder_casey1 with Dissolve(.5)
  casey "I'll enjoy this..."
  window hide
  show kate spanking_redder_casey2 with Dissolve(.5)
  pause 0.1
  show kate spanking_redder_casey3 with Dissolve(.05)
  show kate spanking_redder_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey7 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  window auto
  "Fuck, that hurt!"
  "Each of them has their own style of spanking..."
  "[kate] is brutal and merciless."
  "[stacy] is skilled and precise."
  "[casey] is—"
  window hide
  show kate spanking_redder_casey3 with Dissolve(.05)
  show kate spanking_redder_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_casey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey7 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_redder_casey3 with Dissolve(.05)
  show kate spanking_redder_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_redder_casey5 with Dissolve(.05)
  show kate spanking_redder_casey6 with Dissolve(.05)
  show kate spanking_redder_casey7 with Dissolve(.05)
  show kate spanking_redder_casey2 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.5)
  window auto
  mc "Oww!"
  "—harsh and uncaring..."
  casey "Quiet, bitch boy."
  mc "Sorry—"
  window hide
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_casey3 with Dissolve(.05)
  show kate spanking_reddest_casey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_casey5 with Dissolve(.05)
  show kate spanking_reddest_casey6 with Dissolve(.05)
  show kate spanking_reddest_casey7 with Dissolve(.05)
  show kate spanking_reddest_casey2 with Dissolve(.05)
  window auto
  "Fuck!"
  casey "I said quiet."
  show kate spanking_reddest_lacey_up_next with Dissolve(.5)
  casey "You need to learn how to listen."
  casey "I don't care what you have to say."
  "She's cruel, goddamn..."
  lacey "My turn, my turn!"
  kate spanking_reddest_lacey_up_next "[mc], ask [lacey] for more."
  menu(side="middle"):
    extend ""
    "\"Can I please have some more, Miss [lacey]?\"{space=-30}":
      mc "Can I please have some more, Miss [lacey]?"
      lacey "Ah! I feel like a queen!"
      casey "You are a queen, babe!"
      stacy "He should worship the ground you walk on."
      $kate.lust+=3
      kate spanking_reddest_lacey_up_next "Damn straight."
    "\"This has gone way too far!\"":
      mc "This has gone way too far!"
      $kate.lust-=1
      kate spanking_reddest_lacey_up_next "No one asked for your opinion."
      lacey "Yeah!"
  stacy "Come on, [lacey]. Give it to him."
  lacey "Oh, I'm planning on it!"
  show kate spanking_reddest_lacey1 with Dissolve(.5)
  "Out of these four, [lacey] worries me the most..."
  "She's unpredictable and slightly unhinged."
  lacey "I've been looking forward to this..."
  "She strokes my ass gently with the paddle. Teasing me."
  "There's malice in her touch."
  "She lowers her voice to a whisper."
  lacey "{size=25}{i}I'm going to hurt you.{/}"
  window hide
  show kate spanking_reddest_lacey2 with Dissolve(.5)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_reddest_lacey3 with Dissolve(.05)
  show kate spanking_reddest_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_reddest_lacey5 with Dissolve(.05)
  show kate spanking_reddest_lacey6 with Dissolve(.05)
  show kate spanking_reddest_lacey7 with Dissolve(.05)
  show kate spanking_reddest_lacey2 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.5)
  window auto
  mc "Fuck!"
  "The searing pain rips through my asscheeks."
  "Red hot like burning coals."
  "A sob escapes my throat, and suddenly the gym is silent."
  window hide
  pause 1.0
  window auto
  "Then, [lacey] giggles."
  lacey "He's crying! I made him cry!"
  lacey "Ask for more!"
  kate spanking_tomato_lacey2 "Do it, bitch!"
  stacy "Beg!"
  casey "Beg like a dog!"
  "I try to speak, but another sob comes out instead."
  lacey "Good enough!"
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.1)
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.1)
  play sound "hit"
  show kate spanking_tomato_lacey4 with Dissolve(.1)
  show kate spanking_tomato_lacey7 with Dissolve(.1)
  show kate spanking_tomato_lacey2 with Dissolve(.1)
  window auto
  "She alternates between hard swats and playful teasing."
  "Hot tears stream down my face, but she's not done..."
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  window auto
  lacey "More?"
  stacy "I'd say so!"
  casey "Absolutely!"
  kate spanking_tomato_lacey2 "Definitely more!"
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  window auto
  "My throat is screwed shut."
  "More sobs roll out when I try to plead for mercy."
  "But there is no mercy to be had..."
  "They're fully in control of my punishment."
  window hide
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  pause 0.1
  show kate spanking_tomato_lacey3 with Dissolve(.05)
  show kate spanking_tomato_lacey4 with Dissolve(.05)
  play sound "paddle_hit"
  $renpy.transition(vpunch, layer="master")
  show kate spanking_tomato_lacey5 with Dissolve(.05)
  show kate spanking_tomato_lacey6 with Dissolve(.05)
  show kate spanking_tomato_lacey7 with Dissolve(.05)
  show kate spanking_tomato_lacey2 with Dissolve(.05)
  window auto
  stacy "Look at that red ass! It's like two ripe tomatoes!"
  kate spanking_tomato_lacey2 "How does it feel, [mc]?"
  kate spanking_tomato_lacey2 "Actually, I don't care."
  kate spanking_tomato_lacey2 "What matters is you know your place."
  "[kate]'s voice suddenly takes a darker tone."
  kate spanking_tomato_lacey2 "Now, beg me to finish you off."
  kate spanking_tomato_lacey2 "Show me you're a true bitch — beg me to bust your balls."
  $quest.kate_desire["bent_over"] = True
  $unlock_replay("kate_prize")
  menu(side="middle"):
    extend ""
    "?kate.lust>=6@[kate.lust]/6|{image= kate contact_icon}|{image= stats lust_3}|\"P-please...\"":
      mc "P-please..."
      kate spanking_tomato_lacey2 "That's not really it, but I'll take it."
      show kate spanking_tomato_step_back with Dissolve(.5)
      "Huh? What's happening?"
      kate spanking_tomato_step_back "I've been looking forward to this..."
      window hide
      pause 0.5
      show kate spanking_tomato_kick with Dissolve(.5)
      pause 0.5
      play sound "falling_thud"
      show black with vpunch
      pause 2.0
      $quest.kate_desire["balls_busted"] = True
      $game.advance()
      pause 2.0
      call goto_school_nurse_room
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
      "..."
      "Oh, god... this pain..."
      "My balls..."
      "I think I might throw up..."
      "..."
      "Well, at least they're still there... and the stickiness is... semen?"
      "Fuck me, did I just ejaculate from getting kicked?"
      show nurse concerned with Dissolve(.5)
      nurse concerned "Oh, good! You're awake."
      nurse thinking "[kate] tells me you passed out. That you hurt your crotch in the gym."
      nurse blush "The cheerleader squad brought you here — such lovely helpful little ladies!{space=-140}"
      "Right..."
      nurse blush "I examined your genitals, and everything seems to be fine down there.{space=-45}"
      nurse sad "I've given you soome painkillers, but it'll probably hurt for a few days...{space=-55}"
      nurse sad "If the pain gets worse, you need to go to the hospital for a full check-up."
      mc "Thanks, I'll keep it in mind..."
      hide nurse with Dissolve(.5)
      "I can't believe [kate] actually busted my balls."
      "She's more sadistic than I thought..."
    "\"No!\"":
      mc "No!"
      show kate spanking_tomato_fight_back
      kate spanking_tomato_fight_back "Huh?" with vpunch
      window hide
      play sound "falling_thud"
      show black with vpunch
      pause 1.0
      hide black
      show casey neutral flip
      show kate angry
      show stacy neutral
      show lacey neutral
      with Dissolve(.5)
      window auto
      kate angry "Oww! That hurt, you little weasel!"
      kate angry "Get back down!"
      window hide
      play sound "fast_whoosh"
      show casey neutral flip at move_to(-.25) behind kate
      show kate angry at move_to("left")
      show stacy neutral at move_to("right")
      show lacey neutral at move_to(1.25) behind stacy
      with hpunch
      show black with Dissolve(.5)
      $set_dialog_mode("default_no_bg")
      "Fuck no! I'm not into that!"
      $mc.love+=1
      "Spanking is one thing... but no one touches my balls."
      "Running. Running. Running."
      call goto_school_english_class
      play sound "<from 9.2 to 10.0>door_breaking_in"
      window auto
      $set_dialog_mode("")
      "I think I outran them..."
      "Nothing puts spring in your step like having your testicles threatened."
      "Hopefully, they won't find me here..."
      "..."
      "Maybe I should pull up my pants... Save what little dignity I have left.{space=-10}"
  if quest.kate_desire["kate_deal"]:
    $quest.kate_desire.advance("encounter")
    $mc["focus"] = "kate_desire"
  else:
    $quest.kate_desire.finish()
  return

label quest_kate_desire_nurse_interact:
  show nurse smile with Dissolve(.5)
  nurse smile "Hello, [mc]! How are you feeling today?"
  mc "Pretty great, thanks!"
  if quest.kate_desire["stakes"] == "release":
    mc "I have some good news..."
    nurse smile "And what might that be?"
    mc "I've convinced [kate] to release you from her service."
    nurse annoyed "W-what do you mean?"
    mc "Okay, cut the crap. I know that she's been blackmailing you."
    mc "And I've put a stop to that."
    nurse concerned "Oh..."
    mc "Aren't you happy about that?"
    $nurse.love+=5
    nurse blush "Goodness gracious, of course I am!"
    nurse blush "Thank you so much, [mc]. That was very kind of you."
    mc "Don't mention it. I hate seeing people under [kate]'s heel."
    nurse sad "How can I possibly thank you?"
    if mc.owned_item("compromising_photo"):
      mc "Actually, I'm more interested in your obedience than your gratitude."
      mc "Just because you're free from [kate] doesn't mean you're free from me.{space=-45}"
      nurse concerned "Oh..."
      mc "That's right. I am far from done with you."
      nurse blush "..."
      mc "Except for now. I am done with you now."
      nurse thinking "Oh, okay..."
      "Crap, I totally messed that up."
      hide nurse with Dissolve(.5)
    else:
      mc "Don't worry about it... just keeping the students here healthy is good enough for me."
      nurse thinking "Oh, that's my job!"
      nurse thinking "Speaking of which, I better get back..."
      nurse blush "Thanks again, [mc]."
      mc "You're very welcome!"
      hide nurse with Dissolve(.5)
      if quest.kate_desire["charitable"]:
        $achievement.philanthropist.unlock()
      "Saving the [nurse]. Saving my dignity."
      "Absolute greatness left, right, and center."
      "What does a hero even say?"
      "Nothing. Just another day in the office..."
  else:
    mc "I have some bad news for you..."
    nurse annoyed "W-what do you mean?"
    mc "I've recently acquired a document. Take a good look."
    nurse neutral "..."
    nurse surprised "Oh my lord! W-where did you find this?"
    mc "It doesn't matter."
    mc "What matters is that I have it and that I could go to the police if I wanted.{space=-100}"
    nurse afraid "Please, don't do that!"
    nurse afraid "I'd be ruined!"
    mc "Well, yeah. That's sort of the idea."
    mc "Of course, I won't do it for no reason..."
    mc "So, don't give me one, if you know what I'm saying?"
    nurse sad "I won't! I promise!"
    if mc.owned_item("compromising_photo"):
      mc "I basically have everything I need to own you now."
      mc "Don't you agree?"
      nurse concerned "I... yes."
      mc "Oh, I have so many ideas for games we could play..."
      nurse thinking "Games...?"
      mc "Yeah, I don't just plan on taking your clothes."
      mc "I'll take your freedom, your dignity, and maybe even your money."
      mc "It all depends on how well you behave from here on out."
      $nurse.lust+=3
      nurse blush "Oh, my goodness..."
      mc "That sums it up quite well."
      mc "I'll be seeing you around."
      hide nurse with Dissolve(.5)
    else:
      mc "We'll see about that. To start with, you'll do as I say."
      nurse sad "This is blackmail!"
      mc "Damn straight. I'm looking forward to holding this over your head."
      mc "For now, keep doing your job and everything will go swimmingly."
      nurse concerned "O-okay..."
      mc "Hey, don't look so distraught! [kate] said you like these sorts of games.{space=-45}"
      $nurse.lust+=3
      nurse blush "I don't know about that..."
      mc "Well, you better start liking it, then."
      mc "I'll see you around."
      hide nurse with Dissolve(.5)
      "This is shaping up to be a great start to the school year..."
      "I beat [kate] and got myself a new toy on the same day."
      "Great work me!"
  $quest.kate_desire.finish()
  return

label quest_kate_desire_loser_end:
  call goto_school_gym
  show casey neutral flip at Transform(xalign=.055) behind kate
  show kate confident at Transform(xalign=.375)
  show stacy neutral at Transform(xalign=.675) behind kate
  show lacey neutral at Transform(xalign=.95) behind stacy
  with Dissolve(.5)
  stacy neutral "Back already? Are you done with the challenge?"
  kate confident "Yes, [mc] has something to tell you..."
  mc "..."
  casey neutral flip "We don't have all day."
  lacey neutral "Yeah!"
  kate confident "Don't be shy. Tell them what you told me."
  mc "I give up... [kate] is the winner."
  show casey confident flip
  show stacy confident
  lacey confident "Woo! Queen!"
  kate confident "He knew he'd lose and probably wanted to get it over with."
  stacy confident "The loser mentality."
  casey confident flip "Truly!"
  kate neutral "All right, get on your knees while I decide your fate, [mc]."
  mc "Yes, ma'am."
  window hide
  $mc["focus"] = ""
  show black with Dissolve(.5)
  pause 0.5
  hide black
  show kate cheerleader_feet
  with Dissolve(.5)
  window auto
  kate cheerleader_feet "That's much better."
  casey "Yeah, down where you belong!"
  kate cheerleader_feet "Now, what are we going to do with you?"
  kate cheerleader_feet "Tsk, tsk... giving up early too..."
  kate cheerleader_feet "Maybe we should tie you up and leave you on the roof?"
  casey "Or in the men's locker room!"
  stacy "Oh, yeah! Let Chad have his way with him!"
  kate cheerleader_feet "Nah, I think we're going to go for something more... personal."
  kate cheerleader_feet "I had a bunch of ideas, but I know exactly what I want now."
  kate cheerleader_feet "Get up."
  window hide
  show black with Dissolve(.5)
  pause 0.5
  hide black
  show kate confident
  with Dissolve(.5)
  pause 0.5
  jump quest_kate_desire_spanking

label quest_kate_desire_nude_encounter:
  if quest.kate_desire["balls_busted"]:
    "Ouch! Even walking hurts!"
    "They really did a number on me..."
    "Hopefully, a cold glass of strawberry juice will ease—"
    window hide
    play sound "<from 2.7 to 4.2>door_breaking_in"
    pause(1.5)
    show kate confident with Dissolve(.5)
    window auto
    kate confident "How is your ass feeling?"
    mc "..."
    kate confident "Pretty sore, I imagine."
    kate confident "How about your balls?"
    mc "Ugh..."
    kate laughing "That bad?"
    kate smile "Well, I enjoyed it."
    mc "Have you seriously just come to gloat?"
    kate neutral "Gloat? I would never."
    kate confident "In fact, I'm here to collect my winnings of our little side wager."
  else:
    "Okay, I think the coast is clear..."
    window hide
    play sound "<from 2.7 to 4.2>door_breaking_in"
    pause(1.5)
    show kate confident with Dissolve(.5)
    window auto
    kate confident "Hello, fugitive."
    "Crap."
    kate confident "I didn't say we were done with you."
    kate neutral "And on top of that, I broke a nail when you escaped."
    kate neutral "Your violence is going cost you."
    mc "Please, [kate]... I'm already going to be sore for days..."
    mc "You said the forfeit wouldn't be too painful or humiliating..."
    kate neutral "Fortunately for you, the girls have already gone to the showers. So, the payback will have to wait."
    kate confident "I'm here for a different reason."
    kate confident "Remember our little side wager?"
  if quest.kate_desire["kate_deal"] == "dirt":
    kate confident "I believe you owe me dirt on [jo]."
  else:
    kate confident "I believe you owe me a nude of [maxine]."
  mc "Right, I forgot about that..."
  kate laughing "I'm sure you did. We got you good with those paddles, didn't we?"
  mc "..."
  kate neutral "Well, time to pay up."
  if quest.kate_desire["kate_deal"] == "dirt":
    mc "I... don't actually have it yet..."
    kate annoyed "Are you shitting me, [mc]?"
    if quest.kate_desire["balls_busted"]:
      kate annoyed_right "I guess I'll have to go get the girls again..."
    else:
      kate annoyed_right "I guess I'll have to go get the girls after all..."
    mc "N-no, don't! I'll get it! I promise!"
    kate skeptical "Yes, you will. And it better be soon... or else."
    show kate skeptical at disappear_to_right
    "God, I'm in so much trouble... and now, so is [jo]..."
    "Why don't I ever think things through?"
  else:
    mc "Fine... here you go."
    if mc.owned_item("maxine_other_nude"):
      $mc.remove_item("maxine_other_nude")
      kate thinking "..."
      kate blush "Well, would you look at that!"
      kate blush "This is certainly going to come in handy."
      kate blush "Thanks, loser."
      show kate blush at disappear_to_right
      "God, I'm so stupid. Why did I think I could beat [kate] in a challenge?"
      "Hopefully, [maxine] doesn't find out that it was me who sold her out..."
      "Oh, who am I kidding? She probably knew it before I did."
    else:
      $mc.remove_item("maxine_nude")
      kate skeptical "..."
      kate skeptical "Is this a joke? It's just a picture of a wrist!"
      mc "But... it's a nude wrist..."
      kate annoyed "You ripped me off!"
      kate annoyed "What the hell am I supposed to do with this?"
      kate annoyed_right "Did we not discipline you enough today? I guess I'll have to get the girls again..."
      mc "N-no, don't! I'll get you a different nude of [maxine]! I promise!"
      kate skeptical "Yes, you will. And it better be soon... or else."
      show kate skeptical at disappear_to_right
      $mc.add_item("maxine_nude")
      "Damn it, [maxine]! Why not send me an actual nude like a normal person?"
      show misc maxine_nude with Dissolve(.5)
      "..."
      "Hmm... wait a second..."
      "The backside has a scribbled message that reads: \"dark chamber.\""
      "Knowing her, it must have some sort of hidden meaning."
      "Maybe the internet knows?"
      hide misc maxine_nude with Dissolve(.5)
      $quest.kate_desire.advance("computer")
      return
  $quest.kate_desire.finish()
  return

label quest_kate_desire_nude_computer:
  "Okay, let's check the dark web for... \"dark chamber...\""
  "..."
  "Oh, it wasn't as cryptic as I thought — it's the latin word for camera!{space=-10}"
  "..."
  "Could [maxine] be hinting at the old one in her office?"
  $quest.kate_desire.advance("camera")
  return

label quest_kate_desire_nude_delivery:
  show kate neutral with Dissolve(.5)
  mc "Hey, [kate]! I have something for you."
  kate laughing "Is it your dignity? Oh, wait! I already took that."
  mc "It's your winnings from our side wager..."
  $mc.remove_item("maxine_other_nude")
  kate neutral "..."
  kate confident "Well, this is certainly going to come in handy."
  kate confident "I guess you are not so useless, after all."
  mc "..."
  kate confident "Buh-bye, now."
  hide kate with Dissolve(.5)
  "Phew! That's one less problem for me to worry about."
  "Hopefully, [maxine] doesn't find out that it was me who sold her out..."
  "Oh, who am I kidding? She probably knew it before I did."
  $quest.kate_desire.finish()
  return
