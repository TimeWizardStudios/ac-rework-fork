init python:
  class Character_cyberia(BaseChar):
    notify_level_changed=True
    default_name="Cyberia"
    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]
    contact_icon="cyberia contact_icon"
    default_outfit_slots=[]
    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}
      ###Forced Locations
      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return
      if cyberia['hidden_now']:
        self.location = None
        self.activity = None
        return
      if cyberia['hidden_today']:
        self.location = None
        self.activity = None
        return
      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):
        self.location = None
        self.activity = None
      ### Schedule


    def call_label(self):
      return None
    def message_label(self):
      return None
#    def contact_notes(self):
#      rv=[]
#      rv.append(["Bio","Character description and history"])
#      rv.append(["Likes",["Apples","Oranges"]])
#      rv.append(["Dislikes",["Pork"]])
#      return rv


    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("cyberia avatar "+state,True):
          return "cyberia avatar "+state
      rv=[]
      return rv


  class Interactable_cyberia(Interactable):
    def title(cls):
      return cyberia.name
    def description(cls):
      return random.choice(["Even though [cyberia] is not a particularly smart AI, whoever built her had some serious talent.","[cyberia] is good at keeping track of homework assignments and test results.","The computer teacher, Mr. Rory, lets [cyberia] do all the heavy lifting, which according to him is everything."])
    def actions(cls,actions):
      #if cyberia["talk_limit_today"]<3:
        #actions.append(["talk","Talk","?cyberia_talk_one"])

      if not mc["focus"]:
        if quest.maxine_eggs == "plugcy":
          actions.append(["use_item","Use item","select_inventory_item","$quest_item_filter:maxine_eggs,cyberia_backdoor|Use What?","quest_maxine_eggs_cyberia_backdoor"])
      if quest.maxine_eggs <= "plugpc":
        actions.append(["interact","Interact","?cyberia_interact"])
      elif quest.maxine_eggs >= "deliver":
        actions.append(["interact","Interact","?cyberia_interact_dead"])

      #if cyberia["flirt_limit_today"]<3:
        #actions.append(["flirt","Flirt","?cyberia_flirt_one"])


    #def circle(cls,actions,x,y):
    #  rv =super().circle(actions,x,y)
    #  if game.location == "school_cafeteria":
    #    rv.start_angle=328
    #    rv.angle_per_icon= 32
    #    rv.center=(0,-0)
    #  if game.location == "school_homeroom":  # iff more than 5 circles change 128 to 160
    #    rv.start_angle=-128
    #    rv.angle_per_icon= 32
    #    rv.center=(0,300)
    #  return rv


label cyberia_interact:
  mc "Professor [cyberia], what is the airspeed velocity of an unladen swallow?"
  show cyberia
  cyberia "The airspeed velocity of an unladen European Swallow is roughly 11 meters per second, or 24 miles an hour."
  "Damn, she's good."
  return

label cyberia_interact_dead:
  "This thing looks dead. Finally, some peace and quiet."
  return
