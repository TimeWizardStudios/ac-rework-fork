init python:
  class Character_stacy(BaseChar):

    notify_level_changed=True

    default_name="Stacy"

    default_stats=[
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon="stacy contact_icon"

    default_outfit_slots=["top","bra","skirt","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("stacy_cheerleader_top")
      self.add_item("stacy_cheerleader_bra")
      self.add_item("stacy_cheerleader_skirt")
      self.add_item("stacy_cheerleader_panties")
      self.equip("stacy_cheerleader_top")
      self.equip("stacy_cheerleader_bra")
      self.equip("stacy_cheerleader_skirt")
      self.equip("stacy_cheerleader_panties")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("stacy avatar "+state,True):
          return "stacy avatar "+state
      rv=[]

      if state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((680,1080))
        rv.append(("stacy avatar body1",147,123))
        if "_cheerleader_panties" in state:
          rv.append(("stacy avatar b1panties",153,718))
        if "_cheerleader_skirt" in state:
          rv.append(("stacy avatar b1skirt",121,707))
        if "_cheerleader_bra" in state:
          rv.append(("stacy avatar b1bra",183,369))
        if "_cheerleader_top" in state:
          rv.append(("stacy avatar b1top",182,364))
        if "_shifty" in state:
          rv.append(("stacy avatar face_shifty",268,207))
        else:
          rv.append(("stacy avatar face_neutral",269,207))
        rv.append(("stacy avatar b1arm1_n",64,369))
        if "_cheerleader_top" in state:
          rv.append(("stacy avatar b1arm1_c",63,367))
        if "_paddle" in state:
          rv.append(("stacy avatar b1paddle",281,355))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((680,1080))
        rv.append(("stacy avatar body1",147,123))
        if "_cheerleader_panties" in state:
          rv.append(("stacy avatar b1panties",153,718))
        if "_cheerleader_skirt" in state:
          rv.append(("stacy avatar b1skirt",121,707))
        if "_cheerleader_bra" in state:
          rv.append(("stacy avatar b1bra",183,369))
        if "_cheerleader_top" in state:
          rv.append(("stacy avatar b1top",182,364))
        rv.append(("stacy avatar face_confident",269,214))
        if "_mocking" in state:
          rv.append(("stacy avatar b1arm2_n",117,291))
          if "_cheerleader_top" in state:
            rv.append(("stacy avatar b1arm2_c",117,311))
        else:
          rv.append(("stacy avatar b1arm1_n",64,369))
          if "_cheerleader_top" in state:
            rv.append(("stacy avatar b1arm1_c",63,367))
        if "_paddle" in state:
          rv.append(("stacy avatar b1paddle",281,355))

      return rv


  class Interactable_stacy(Interactable):

    def title(cls):
      return stacy.name

    def actions(cls,actions):
      pass
