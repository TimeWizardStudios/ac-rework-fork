label berb_quest_fight_start:
  play sound "<from 0.5 to 1.5>squeaking_animal"
  if not quest.lindsey_book.failed:
    "Huh? I thought I killed that beaver."
    "I guess there's more than one beaver in the forest, but it does look awfully similar."
  "Not sure what the critter wants. Probably just attention. Those tiny black eyes are gleaming with malice."
  $quest.berb_fight.start()
  return

label berb_quest_fight_roof:
  play sound "<from 2.0 to 4.0>squeaking_animal"
  show berb at appear_from_left(.5):
    pause 1
    xzoom -1
    pause .5
    xzoom 1
    pause .5
    xzoom -1
    pause .5
    xzoom 1
  pause(1)
  "{i}*Beaver noises*{/}"
  hide berb
  show berb
  "Okay, according to the beaver encyclopedia..."
  "That little son of a bitch just flipped me off and called me a pussy!"
  if not quest.lindsey_book.failed:
    "It's probably because I killed his friend."
  play sound "<from 2.0 to 4.0>squeaking_animal"
  show berb:
    xzoom -1
    pause .5
    xzoom 1
    pause .5
    xzoom -1
    pause .5
    xzoom 1
  "{i}*Beaver noises*{/}"
  hide berb
  show berb
  show berb at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Try saying that to my face!\"":
      show berb at move_to(.5)
      mc "Try saying that to my face!"
      play sound "<from 1.0 to 3.0>squeaking_animal"
      show berb:
        xzoom -1
        pause .5
        xzoom 1
        pause .5
        xzoom -1
        pause .5
        xzoom 1
      "{i}*Beaver noises*{/}"
      hide berb
      show berb
      "Okay, let's see what he's saying now..."
      berb "You yellow-bellied hockerspock! You're lucky I'm up here!"
      mc "What are you going to do? Splash water on me?"
      berb "I'll gnaw off your fingers and clog your toilet with the bones!"
      "Holy crap. This creature is vile."
      play sound "<from 0.5 to 2.5>squeaking_animal"
      berb "Your kneecaps will decorate the walls of my nest!"
      hide berb with moveoutleft
      show lindsey skeptical at appear_from_right(.5)
      lindsey skeptical "What's that noise?"
      show lindsey skeptical at move_to(.75)
      menu(side="left"):
        extend ""
        "\"It's that fucking beaver.\"":
          show lindsey skeptical at move_to(.5)
          mc "It's that fucking beaver."
          lindsey cringe "Language, please!"
          mc "You should tell him that..."
          lindsey thinking "He looks fine to me."
          play sound "<from 1.0 to 3.0>squeaking_animal"
          mc "He just called you a spoiled princess and said he has a bucket of peas ready."
          lindsey cringe "Is that supposed to be funny?"
          mc "No! It was rude as hell!"
          $lindsey.love-=1
          lindsey angry "If I'm the princess, I need to find myself a new court jester. Your jokes are lame."
          show lindsey angry at disappear_to_right
        "\"What noise? I didn't hear anything.\"":
          show lindsey skeptical at move_to(.5)
          mc "What noise? I didn't hear anything."
          lindsey afraid "Look! There's a beaver on the roof!"
          mc "Yeah, I guess you're right."
          lindsey blush "Aren't you going to save him?"
          mc "Hell no! That furry devil just cursed at me."
          lindsey thinking "Really? What if he falls?"
          mc "I'd celebrate his demise."
          $lindsey.lust-=1
          lindsey cringe "That's not cool. Where is your sense of heroics?"
          show lindsey cringe at disappear_to_right
      "Ugh, she looked really disappointed."
      "And it's all that beaver's fault. He tricked me."
      play sound "<from 4.0>squeaking_animal"
      "That cocksucker is mocking me now. There are few things that rile me up as much as that noise."
      "My vengeance will be swift... as soon as I figure out how to get him down..."
    "\"Let's be friends, little buddy.\"":
      show berb at move_to(.5)
      mc "Let's be friends, little buddy."
      play sound "<from 1.0 to 3.0>squeaking_animal"
      show berb:
        xzoom -1
        pause .5
        xzoom 1
        pause .5
        xzoom -1
        pause .5
        xzoom 1
      "{i}*Beaver noises*{/}"
      hide berb
      show berb
      "Okay, let's see what he's saying now..."
      berb "Your bones will make great additions to my dam!"
      mc "What the hell?"
      berb "I'll fill your skull with seaweed and smoke it like pot!"
      mc "That's messed up."
      # berb "I'll spank your sister with my paddle tail and make her gnaw my log!" ## Incest patch ##
      # berb "I'll spank your roommate with my paddle tail and make her gnaw my log!"
      berb "I'll spank your roommate with my paddle tail and make her gnaw my log!{space=-85}"
      # mc "Seriously? Don't talk about my sister!" ## Incest patch ##
      mc "Seriously? Don't talk about [flora]!"
      hide berb with moveoutleft
      show flora skeptical flip at appear_from_right(.5)
      flora skeptical flip "Why are you yelling?"
      mc "Err... hi there, [flora]."
      mc "There's a foul-mouthed beaver on the roof!"
      flora afraid flip "What?"
      mc "I said... there's a fucking beaver on the roof..."
      flora thinking flip "Oh?"
      flora laughing flip "Oh! It's cute!"
      flora worried flip "I hope he doesn't fall."
      show flora worried flip at move_to(.75)
      menu(side="left"):
        extend ""
        "\"He said all sorts of nasty things.\"":
          show flora worried flip at move_to(.5)
          mc "He said all sorts of nasty things."
          flora annoyed "Like what?"
          # mc "He said my sister would gnaw on his log." ## Incest patch ##
          mc "He said you would gnaw on his log."
          flora confused "Seriously, dude?"
          mc "What?"
          flora confused "That's messed up."
          flora confused "Can you stop saying weird shit and blaming an innocent animal?"
          mc "I looked it up in the beaver encyclo—"
          flora eyeroll "Just stop, okay?"
          flora eyeroll "I'm tired of this."
          mc "I swear! The bastard said he'd use my bones for his dam!"
        "\"I wouldn't mind seeing that.\"":
          show flora worried flip at move_to(.5)
          mc "I wouldn't mind seeing that."
          flora cringe "Don't say that!"
          flora cringe "He's just a fluffy little critter!"
          mc "I still think he'd look better as a hat."
          flora angry "Stop it!"
          play sound "<from 1.0 to 3.0>squeaking_animal"
          mc "He just called you \"sexy mama\" and said that he wants you to \"guzzle his dam water.\""
          mc "Not sure what the last part means..."
          flora eyeroll "You're not funny."
          mc "I promise you, I have the translations right here!"
          flora annoyed "If you're going to sexually harass me, at least have the balls to stand for it."
          mc "I wasn't doing that. I was just—"
      flora angry "Whatever. I've heard enough."
      $flora.love-=2
      flora angry "Don't talk to me until you've grown up."
      show flora angry at disappear_to_right()
      "Fuck's sake..."
      play sound "<from 4.0>squeaking_animal"
      "Great, now he's gloating."
      "Need to find a way to shut him up."
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|Don't fall for the beaver's tricks":
      show berb at move_to(.5)
      play sound "<from 1.0 to 3.0>squeaking_animal"
      berb "Look at me when I'm talking to you, you lily-livered legume!"
      "Just avoid eye contact and keep walking..."
      berb "I'll cut off your legs and use your kneecaps as ashtrays!"
      # berb "Your mother and sister will swallow my wood chips!" ## Incest patch ##
      berb "Your landlady and roommate will swallow my wood chips!"
      "It was a mistake bringing the beaver encyclopedia."
      "This creature is a furry demon spawn."
      berb "I'll clog the water supply to your house and flood your basement!"
      "No one has more experience dealing with insults than me."
      "Just shrug it off and keep walking..."
      "They say that revenge is best served cold, which is true. But it's also served best with a cool head."
      "The right decision is probably to get the janitor involved, but knowing her, she'll just release the beaver into the wild."
      "No. This is a personal matter now."
      "Beaver vs. Predator, with me starring in the role of the masked hunter."
      "First step is knocking that critter off the roof."
      hide berb with moveoutleft
  $quest.berb_fight.advance("curveball")
  return

label berb_quest_fight_ground:
  play sound "<from 2.0 to 4.0>squeaking_animal"
  show berb at appear_from_left(.5):
    pause 1.35
    xzoom -1
    pause .5
    xzoom 1
    pause .5
    xzoom -1
    pause .5
    xzoom 1
  pause(1)
  "{i}*Beaver noises*{/}"
  hide berb
  show berb
  "Oh, hell no! That little cocksucker just called me a clown! We all heard it!"
  mc "Come here, you fuck!"
  $berb["chased_now"] = True
  $process_event("update_state")
  hide berb with moveoutleft
  mc "That's right, bitch. You better run."
  "He's too fast to catch with my bare hands... need to find a way to track him back to his nest."
  "I'm sure that bastard will be back here gloating in no time."
  #Every time you interact with the beaver, he'll run off, but he'll be back the next time you enter the School Exterior location.
  return



label berb_quest_items(item):
  if item == "book_of_the_dammed":
    if berb.at('school_entrance','roof') and quest.berb_fight == "communicate":
      "{i}\"The common beaver (Latin: Beavus Pestus) belongs to a family of large rodents that infests the world's forests.\"{/}"
      "{i}\"Throughout history, their distinct sounds and ways of communication have attracted the interests of many scientists and nature experts.\"{/}"
      "{i}\"In 1997, the naturologist and famous hunter J. J. Timberlake decoded the language of the beavers.\"{/}"
      "{i}\"This instantly worsened human-beaver relationships across the globe, and started what can only be described as a cold war.\"{/}"
      "{i}\"This encyclopedia contains the full translations of beaver tongue — a language so foul that the consensus among linguists is that it should join Latin, Coptic, Summerian, and Sanskrit on the list of dead languages.\"{/}"
      "Okay, let's see what that critter is actually saying. This book seems to have the translations."
      jump berb_quest_fight_roof
    if berb.at('school_entrance','roof') and quest.berb_fight == "curveball":
      "Throwing my [item.title_lower] at it would attract the scorn of all local beavers. Not risking my life this early in the week."
      $quest.berb_fight.failed_item("baseball",item)
      return
    elif berb.at('school_entrance','ground'):
      "{i}\"A beaver's natural habitat consists of wooded areas with streams of water.\"{/}"
      "{i}\"They use their sharp teeth to gnaw down trees to build their dams.\"{/}"
      "{i}\"They use their sharp tongues to spit insults at predators (or really, anyone else who happens to cross their path).\"{/}"
      "{i}\"They slap their tails against the surface of the water for attention. These rodents are known for their egocentric worldview.\"{/}"
      "{i}\"In the 1980s, there used to be over 60 million beavers in North America. Thankfully, most have been hunted down today.\"{/}"
      $quest.berb_fight.advance("corner")
      $process_event("update_state")
      "He escaped again, but perhaps I can track him into the forest this time."
      return
    elif berb.at('school_forest_glade','river'):
      "{i}\"Also known as 'floaters,' these pieces of nocturnal shit clog up most streams if left unchecked.\"{/}"
      "{i}\"The beavers' merciless deforestation of nature reserves and ecological systems is a danger to all wildlife.\"{/}"
      "{i}\"No forests means no oxygen, which makes their intent quite clear — suffocating the planet.\"{/}"
      "{i}\"Polluting a body of water is the simplest way of getting a beaver out of its fortified bunker (sometimes referred to as a 'dam').\"{/}"
      "That pretty much sums up my thoughts on beavers, as well."
      "According to that book, the only way of getting that vermin out of the water is polluting the stream."
      "Single-use plastics and toxic liquids are the go-to polluters."
      $quest.berb_fight.advance("corrupt")
      return
    elif berb.at('school_forest_glade','ground'):
      "{i}\"The beaver's deceitfully cuddly appearance is used to hide its malicious intentions from its victims.\"{/}"
      "{i}\"With the ability to fell trees with its powerful bite, the beaver is literally armed to the teeth.\"{/}"
      "{i}\"Often unprovoked, these creatures maul and slaughter without mercy or respite.\"{/}"
      "{i}\"Survivors of beaver attacks are often left with a mental trauma to go along with the physical one.\"{/}"
      "{i}\"Even a solitary beaver should be treated with extreme caution.\"{/}"
      return
  elif quest.berb_fight not in ("curveball","combat"):
    if berb.at('school_entrance','roof'):
      "While I'd like nothing more than to shove my [item.title_lower] down his furry throat, more research is required."
    elif berb.at('school_entrance','ground'):
      "Hitting that river slug with my [item.title_lower] would be satisfying, but it could also cost me my left nut."
      "First, I need to learn more about his evil ways."
    elif berb.at('school_forest_glade','river'):
      "Charging into battle with my [item.title_lower] drawn would make for a powerful image."
      "However, recklessness is how you lose wars."
      "Acquiring more information is the next logical step."
      "The Art of War says to first know your enemy."
      "Assessment before assassination."
    #elif berb.at('school_forest_glade','ground'):
    #  return
    #"I'd like nothing more than to club the beaver with my [item.title_lower], but the Art of War says to first know your enemy."
    #"Assessment before assassination."
    #"Few foes have survived my [item], but this beaver is powerful."
    #"How powerful? Only the Good Book knows."
    $quest.berb_fight.failed_item("book_of_the_damned",item)
    return
  elif quest.berb_fight == "curveball":
    if item == "baseball":
      $mc.remove_item("baseball")
      $quest.berb_fight.advance("chase")
      $process_event("update_state")
      "Got him!"
      #SFX: Ball flying through air (whistling noise)
      #SFX: Thud
      #SFX: beaver noises
      "You're mine now, you little bastard!"
      #Show the beaver falling off the roof
      #Show Baseball on the ground (as an item that you can pick up again)
      return
    else:
      "Throwing my [item.title_lower] at it would attract the scorn of all local beavers. Not risking my life this early in the week."
      $quest.berb_fight.failed_item("baseball",item)
      return
  elif quest.berb_fight == "combat":
    if item == "stick":
      show berb at appear_from_left(.5)
      mc "I've spent many hours at the dojo practicing the art of wooden sword fighting!"
      mc "Time to meet your maker, fur-fuck!"
      play sound "<from 3.0 to 5.0>squeaking_animal"
      if quest.lindsey_book.failed:
        berb "Foolish human! You are no match for—"
      else:
        berb "Foolish human! You thought you'd killed me, didn't you?"
        berb "The beaver that stole your easel and fucked your girlfriend was just my lesser clone!"
        berb "You are no match for—"
      show black onlayer overlay with Dissolve(.5)
      show black onlayer overlay
      play sound "hit"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "hit2"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "hit"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "hit2"
      $berb["dead"] = True
      $process_event("update_state")
      hide berb
      hide black with Dissolve(.5)
      mc "On the fields of crimson glory, the hero stood in triumph."
      mc "On this blessed day, he'd managed to defeat his lifelong rival... nay, Evil itself!"
      mc "And for that the realm would forever be in debt."
      "Hmm... it sounded way cooler in my head."
      $quest.berb_fight.finish()
    elif item == "rock":
      show berb at appear_from_left(.5)
      mc "Are you ready for survival of the fittest?"
      play sound "<from 3.0 to 5.0>squeaking_animal"
      if quest.lindsey_book.failed:
        berb "Look at your double-chin, pussy. Do you call that fit?"
      else:
        berb "Foolish human! You thought you'd killed me, didn't you?"
        berb "The beaver that stole your easel and fucked your girlfriend was just my lesser clone!"
      berb "You are no match for—"
      show black onlayer overlay with Dissolve(.5)
      show black onlayer overlay
      play sound "hit"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "hit2"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "hit"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "hit2"
      $berb["dead"] = True
      $process_event("update_state")
      hide berb
      hide black with Dissolve(.5)
      mc "Welcome to the Stone Age."
      "He's not moving anymore, and his unholy mouth is finally silenced."
      "This is a victory for all of humanity. A victory for freedom. And a first step toward world peace."
      "In the future, this will probably become a national holiday."
      "For now, let's call this day... fuck, I can't come up with anything good..."
      "Maybe Fur-Fuck-Friday?"
      "Ugh, I'm sure I'll think of something."
      $quest.berb_fight.finish()
    elif item == "poisoned_apple":
      show berb at appear_from_left(.5)
      if not quest.lindsey_book.failed:
        berb "Foolish human! You thought you'd killed me, didn't you?"
        berb "The beaver that stole your easel and fucked your girlfriend was just my lesser clone!"
        berb "You are no match for—"
      mc "Eat this!"
      $mc.remove_item("poisoned_apple")
      play sound "<from 3.0 to 5.0>squeaking_animal"
      berb "You think your poisoned apple has any effect on me?"
      berb "Poison only makes me stronger!"
      show berb:
        xzoom -1
        pause .5
        xzoom 1
        pause .5
        xzoom -1
        pause .5
        xzoom 1
      berb "Om nom nom..."
      hide berb
      show berb
      berb "Urk...?" with vpunch
      mc "Sorry, but that's not really poison."
      mc "That's a specially engineered cleanser."
      mc "It'll scrub away your evil and leave you with nothing!"
      show black onlayer overlay with Dissolve(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      show black onlayer overlay
      play sound "<from 4.0>squeaking_animal"
      pause(.5)
      $berb["dead"] = True
      $process_event("update_state")
      show black onlayer overlay
      hide black with Dissolve(.5)
      show berb:
        xzoom -1
        pause .5
        xzoom 1
        pause .5
        xzoom -1
        pause .5
        xzoom 1
      berb "{i}*Gurgle*{/}"
      hide berb with Dissolve(.5)
      "Not the most heroic of victories, but a victory nonetheless."
      $quest.berb_fight.finish()
    else:
      "Hmm, fighting the beaver with my [item.title_lower] would only work if I were a [item.title_lower]-master. Alas, it's suicide."
      $quest.berb_fight.failed_item("combat",item)
      return
  return


label quest_berb_fight_river_items(item):
  if item == "pepelepsi" or item == "spray_pepelepsi":
    if item == "pepelepsi":
      $item2 = "empty_bottle"
    elif item == "spray_pepelepsi":
      $item2 = "spray_empty_bottle"
    if not school_forest_glade['pollution']:
      $school_forest_glade['pollution'] = 1
      $mc.remove_item(item)
      $mc.add_item(item2)
      "Well, that probably killed off some fish and insects, but it's not enough to drive the beaver out of the water."
      "Going to need more pepelepsi to get this stream properly polluted."
    elif school_forest_glade['pollution'] == 1:
      $school_forest_glade['pollution']+=1
      $mc.remove_item(item)
      $mc.add_item(item2)
      "I don't have a microscope, but that's probably enough to get rid of all microorganisms."
      "Still, the beaver seems to be enjoying itself. Or at least, pretending to..."
      "Probably need another bottle of pepelepsi."
    elif school_forest_glade['pollution'] == 2:
      $school_forest_glade['pollution']+=1
      $mc.remove_item(item)
      $mc.add_item(item2)
      "There. That stream is more lifeless than the dead sea."
      if not school_forest_glade["wrappers"]:
        "Still, that fucking beaver refuses to get out."
        show berb at appear_from_left
        berb "The death of this ecosystem means nothing to me, fool!"
        berb "Only more beaver lebensraum!"
        hide berb with Dissolve(.5)
        "Hmm... tossing in a bunch of garbage should do the trick."
      else:
        "That fucking beaver is finally crawling out of the stream."
        $quest.berb_fight.advance("combat")
        $process_event("update_state")
        play sound "<from 4.0>squeaking_animal"
        "Out of the murky depth the creature rose..."
        "Gnarly meathook claws."
        "Fangs like a rusty wood chipper."
        "Eyes as cold and black as the eternal void."
        "The final boss. The ultimate test. Death turned flesh."
    return
  elif item == "wrapper":
    if not school_forest_glade["wrappers"]:
      if mc.owned_item_count("wrapper")>=5:
        $school_forest_glade["wrappers"] = True
        $mc.remove_item("wrapper",5)
        "Like leaves in the fall... of nature. Yes, putting nature in its place feels good."
        if school_forest_glade["pollution"] == 3:
          "That fucking beaver is finally crawling out of the stream."
          $quest.berb_fight.advance("combat")
          $process_event("update_state")
          play sound "<from 4.0>squeaking_animal"
          "Out of the murky depth the creature rose..."
          "Gnarly meathook claws."
          "Fangs like a rusty wood chipper."
          "Eyes as cold and black as the eternal void."
          "The final boss. The ultimate test. Death turned flesh."
      else:
        "Need to get more candy wrappers. My current amount is barely enough to pollute a gutter."
    else:
      "Filling the river with plastic trash is a good start, but there must be more ways to ruin this ecosystem."
  else:
    "I'll need something more toxic than my [item.title_lower] to pollute such a large body of water."
  return


label quest_berb_fight_glade_fight:
  play sound "<from 4.0>squeaking_animal"
  show berb at appear_from_left(.5)
  "Oh, shit. He's getting aggressive!"
  show berb at move_to(.25)
  menu(side="right"):
    "Fight the beaver with your bare hands":
      show berb at move_to(.5)
      if mc.strength >=9:
        mc "Time to meet your maker, fur-fuck!"
        play sound "<from 3.0 to 5.0>squeaking_animal"
        berb "Foolish human! You thought you'd killed me, didn't you?"
        berb "The beaver that stole your easel and fucked your girlfriend was just my lesser clone!"
        berb "You are no match for—"
        show black onlayer overlay with Dissolve(.5)
        show black onlayer overlay
        play sound "hit"
        pause(.5)
        show black onlayer overlay
        play sound "<from 4.0>squeaking_animal"
        pause(.5)
        show black onlayer overlay
        play sound "hit2"
        pause(.5)
        show black onlayer overlay
        play sound "<from 4.0>squeaking_animal"
        pause(.5)
        show black onlayer overlay
        play sound "hit"
        pause(.5)
        show black onlayer overlay
        play sound "<from 4.0>squeaking_animal"
        pause(.5)
        show black onlayer overlay
        play sound "hit2"
        $berb["dead"] = True
        $process_event("update_state")
        hide berb
        hide black with Dissolve(.5)
        "Like Hercules slew the Nemean lion, so have I completed this arduous but glorious task."
        "My hands are bloodied, but the blood belongs to my vanquished foe."
        "His vile words shall no longer reach the ears of the innocent."
        $quest.berb_fight.finish()
      else:
        mc "Come here, you little fuck!"
        play sound "<from 4.0>squeaking_animal"
        show blood_splatter with vpunch
        "Fuck! He bit me!"
        hide blood_splatter with Dissolve(.5)
        "How is he so powerful?"
        show layer master:
          linear .25 zoom 2 xoffset -900 yoffset -900
          xoffset -1200 yoffset -2000
          linear .375 rotate -180 xoffset -1000 yoffset -1200
          linear .375 rotate -360 xoffset -1200 yoffset -2000
          linear .25 zoom 1 xoffset -141 yoffset -561
          block:
            linear 0.05 yoffset -561+15
            linear 0.05 yoffset -561-15
            repeat 5
          linear 0.05 yoffset -561
        pause(1.5)
        hide layer master
        show layer master
        "He chokeslammed me like nothing!"
        "I feel like a weak twig tossed into his chainsaw-mouth!"
        "Need to hercules it up, or try to find a weapon or way to outsmart—"
        show berb:
          linear 1 zoom 1.5 yoffset 200
        "Oh, god! He's coming back to finish me off!"
        show berb at center:
          linear 1 zoom 2.5 yoffset 500
        "This is the end! I regret everything!"
        play sound "<from 4.0>squeaking_animal"
        show berb at center:
          linear .25 zoom 7.5 yoffset 1700
        pause(.25)
        $game.location = school_entrance
        hide berb
        show red_haze
        with Fade(.5,3,.5)
        "Ugh, what's wrong with me?"
        "I feel all woozy and dull."
        "That creature bit me! It probably has all sorts of diseases."
        "Sure hope there's a cure..."
        $mc["red_haze"]=True
        hide red_haze with Dissolve(.5)
    "Run away":
      show berb at move_to(.5)
      "That thing is scary up close!"
      hide berb with Dissolve(.5)
  return
