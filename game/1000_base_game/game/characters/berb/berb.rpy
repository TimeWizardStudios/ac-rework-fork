init python:
  class Character_berb(BaseChar):
    notify_level_changed=True
    default_name="Beaver"

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}
      #forced
      if quest.jo_washed.in_progress:
        self.location = None
        self.activity = None
        return
      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        self.alternative_locations={}
        return
      if quest.berb_fight in ("corner","corrupt"):
        self.location = "school_forest_glade"
        self.activity = "river"
        return
      elif quest.berb_fight>="combat":
        self.location = "school_forest_glade"
        self.activity = "ground"
        return
      elif quest.berb_fight=="chase":
        if not berb["chased_now"]:
          self.location = "school_entrance"
          self.activity = "ground"
        else:
          self.location = "home_bedroom"
        return
      if game.season == 2:
        self.location = None
        return
      #------
      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):

        if game.hour in (7,8,9,10,11,12,13,14,15,16,17,18) and quest.berb_fight<="curveball":
          self.location = "school_entrance"
          self.activity = "roof"
      ### Schedule

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      # if state and isinstance(state,basestring):
        # if renpy.has_image("berb avatar "+state,True):
          # return "berb avatar "+state
      rv=[]
      rv.append((100,1080))
      if not berb['reveal']:
        rv.append(("berb avatar body",4,450))
      else:
        rv.append(("berb avatar bob",4,450))
      return rv

  class Interactable_berb(Interactable):
    def title(cls):
      return berb.name
    def description(cls):
      if quest.berb_fight >= "combat":
        return "An excerpt from the Book of the Dammed: {i}\"When provoked (and often unprovoked), the beaver stands up on its hind legs. This isn't an intimidation tactic, it's to reach your vital organs.\"{/}"
      elif quest.berb_fight >= "corner":
        return "His tiny paws, twitching with dark energies and bloodlust, are truly the stuff of nightmares."
      elif quest.berb_fight >= "chase":
        return "There aren't really any words that can describe the pure evil in this creature's eyes."
      else:
        return "Not sure how that beaver got up on the roof, but the little bastard is giving me the stink eye."
    def actions(cls,actions):

      #quests
      if not mc["focus"]:
        if game.season == 1:
          if quest.lindsey_book.ended and not quest.berb_fight.started:
            actions.append(["quest","Quest","berb_quest_fight_start"])
          elif quest.berb_fight.in_progress:
            if quest.berb_fight in ("communicate", "chase", "corner","corrupt"):
              actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,book_of_the_damned|Use What?","berb_quest_items"])
            if quest.berb_fight == "curveball":
              actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,baseball|Use What?","berb_quest_items"])
              actions.append(["interact","Interact","?berb_on_roof_curveball_interact"])
            if quest.berb_fight == "combat":
              actions.append(["use_item","Use","select_inventory_item","$quest_item_filter:berb_fight,combat|Use What?","berb_quest_items"])

        if berb.at("school_forest_glade","river"):
          actions.append("?berb_quest_berb_river")
        elif quest.berb_fight == "curveball":
          actions.append(["interact","Interact","?berb_on_roof_curveball_interact"])
        else:
          actions.append(["interact","Interact","?berb_on_roof_interact"])

        if quest.berb_fight=="chase":
          actions.append(["interact","Chase","berb_quest_fight_ground"])
        elif quest.berb_fight=="combat":
          actions.append(["interact","Fight!","quest_berb_fight_glade_fight"])


label berb_on_roof_interact:
  "A creature of darkness."
  "The beavers had us fooled for ages until our lord and savior J. J. Timberlake decoded their language."
  "Some say his encyclopedia has had a bigger global impact than the Bible, and presidents swear on it during inauguration."
  return

label berb_quest_berb_river:
  "The beaver is floating across the stream with a special kind of hatred spilling out of his eyes — like he wants to grind my bones into dust and snort it off the back of a swan."
  return

label berb_on_roof_curveball_interact:
  "Knock him down. That's the only way this town will find peace."
  return
