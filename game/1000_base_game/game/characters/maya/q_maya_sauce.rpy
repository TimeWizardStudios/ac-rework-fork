label quest_maya_sauce_start:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5

label quest_maya_sauce_start_quick_start:
  stop music
  play ambient "<to 6.5>audio/music/siren_song.ogg" noloop volume 0.25
  pause 6.5
  stop ambient
  play music "home_theme"
  "Huh?"
  "Is that... singing?"
  "A welcome change from the blare of the alarm clock."
  "..."
  "But where is it coming from?"
  "This siren song..."
  window hide
  $renpy.transition(Dissolve(.125))
  $mc["focus"] = "maya_sauce"
  $quest.maya_sauce.start()
  return

label quest_maya_sauce_siren_song_upon_entering:
  stop music
  play ambient "<from 7.5 to 14.0>audio/music/siren_song.ogg" noloop volume 0.5
  pause 6.5
  stop ambient
  play music "home_theme"
  "Oh? The song is coming from the bathroom."
  "I've heard [flora]'s singing, and she sounds like a drowning duck."
  "I guess that means [maya] is still freeloading here..."
  return

label quest_maya_sauce_siren_song:
  "I never thought I'd hear [maya] singing."
  "She doesn't really seem like the type."
  "And she's actually good, too."
  "It's a shame it's muted by the walls and the shower."
  "..."
  "Except, she forgot to lock the door..."
  window hide
  scene black with Dissolve(.07)
  $game.location = "home_bathroom"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location with Dissolve(.5)
  stop music
  play ambient "<from 15.0>audio/music/siren_song.ogg" noloop
  pause 9.0
  stop ambient
  play music "home_theme"
  window auto
  "Like angels cumming in my ears."
  "I had no idea."
  "Her singing voice is so sweet. A little breathy."
  "It makes her sound softer, younger."
  "Almost vulnerable."
  "Something I was starting to think she'd never be."
  maya "Err, hello?"
  "Shit. Fuck. Cock."
  maya "Is someone in here?"
  mc "..."
  maya "Listen, fucker, if you're here to knife murder me..."
  maya "I'm telling you now — mine is bigger."
  "She's talking about her knife, right?"
  "Oh, man. I really should leave."
  "But since I'm already here..."
  menu(side="middle"):
    extend ""
    "Risk a peek":
      "Don't squander opportunities as they come, right?"
      "I may never get this chance again."
      "And curiosity did kill the pussy, but this one has nine lives."
      "..."
      $mc.lust+=2
      "Stalking ever closer, the steam parts around me."
      "Like a panther at dawn, creeping up and—"
      window hide
      $maya["outfit_stamp"] = maya.outfit
      $maya.outfit = {}
      show maya afraid_soapy at center with {"master": Dissolve(.5)}:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      pause 0.5
      window auto
      maya afraid_soapy "Aaah!"
      maya afraid_soapy "Get the fuck out, psycho!"
      window hide
      play sound "ink_splorts"
      show expression Flatten("maya soap_attack home_bathroom") as workaround:
        alpha 0.0
        ease 0.375 alpha 1.0
      show soap_attack with vpunch:
        zoom 0.0 alpha 0.0 xpos 960 ypos 540
        easein 0.175 zoom 1.0 alpha 1.0 xpos 0 ypos 0
      pause 0.25
      window auto
      mc "Aaaargh! My eyes!!"
      window hide
      hide screen interface_hider
      show maya soap_attack home_hall
      show black
      show black onlayer screens zorder 100
      with close_eyes
      pause 0.375
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      $maya.lust+=2
      maya "That's what you get!"
      "My eyes burn from the cowardly soap attack."
      "Blinded — no, wounded — my hands fumble for the door while she laughs villainously."
      show black onlayer screens zorder 100
      pause 0.25
      $game.location = "home_hall"
      play sound "<from 9.2 to 10.0>door_breaking_in"
      with hpunch
      pause 0.75
      hide workaround
      hide black
      hide black onlayer screens
      show screen interface_hider
      with open_eyes
      window auto
      $set_dialog_mode("")
      maya "And don't come back!"
      "Fuck me..."
      window hide
      pause 0.125
      hide maya
      hide soap_attack
      with Dissolve(.5)
      pause 0.25
      window auto
      "Maya sure is a menace, but a cute one at that."
      "She didn't seem all too mad about it, either... or maybe that's my imagination?"
      "Either way, time for some well-earned breakfast."
      $quest.maya_sauce["soap_attack"] = True
      $maya.outfit = maya["outfit_stamp"]
    "Give [maya] her privacy":
      "On second thought, I better not."
      "It would be wrong to take advantage of an unsuspecting woman in the shower."
      "Even hearing her sing already feels like an intrusion."
      "I should probably leave before she pokes her head out and shows me that knife..."
      "I wouldn't put it past [maya] to be showering with one."
      mc "Err, sorry! I didn't realize you were in here!"
      maya "Oh? [mc]? That's okay!"
      maya "I'll just let everyone know you're creeping on your guests!"
      window hide
      scene black with Dissolve(.07)
      $game.location = "home_hall"
      $renpy.pause(0.07)
      $renpy.get_registered_image("location").reset_scene()
      scene location with Dissolve(.5)
      window auto
      "Uh-oh. She wouldn't, right?"
      "..."
      "Why is my mouth so dry all of a sudden?"
      "[maya] makes me hungry for some reason... or maybe it's just breakfast time."
  $quest.maya_sauce.advance("breakfast")
  return

label quest_maya_sauce_breakfast_home_hall_door_bathroom:
  "If I bother [maya] again, she'll probably have my balls... and not in the fun way."
  return

label quest_maya_sauce_breakfast:
  show flora skeptical with Dissolve(.5)
  mc "Morning, [flora]."
  mc "Did you sleep well?"
  flora skeptical "Why? Did you slip me something last night?"
  mc "What? Of course not!"
  mc "Isn't that the polite thing to ask someone in the morning?"
  flora skeptical "Why are you being polite, then?"
  "God. There's just no winning with her sometimes, is there?"
  "Too mean for her. Too nice for her."
  "Always bratty and out to get me."
  mc "Did you hear [maya]—"
  window hide
  $maya["outfit_stamp"] = maya.outfit
  $maya.outfit = {"dress":"maya_towel", "hat":"maya_hair_towel"}
  show maya neutral flip at appear_from_left(.25)
  show flora skeptical:
    ease 1.0 xalign 0.75
  pause 0.5
  window auto
  "Goddamn! Of course she would come downstairs in just a towel."
  "Not that I'm complaining..."
  "I mean, just look at those legs."
  "Drops of water still clinging to her smooth, pale skin. So wet, so—"
  "..."
  "Oh, fuck. Don't stare, don't stare, don't stare!"
  mc "Err, good morning, [maya]."
  mc "Quite the concert you were putting on."
  maya annoyed flip "Excuse you?"
  if quest.maya_sauce["soap_attack"]:
    maya annoyed flip "Don't you mean \"peepshow,\" you lurking Larry?"
    mc "Well, I—"
    $flora.love-=2
    flora embarrassed "Ew, [mc]! Seriously?!"
    maya annoyed flip "Usually, I charge for something like that."
    maya annoyed flip "But I guess it's on the house this one time."
    mc "Right. My house."
    maya smile flip "Oh? Shall I let [jo] know how you're treating her guests?"
    mc "Look, I'm sorry, okay? I was just drawn in by the music."
    maya eyeroll flip "Right, of course. Nevermind the pair of naked tits."
    maya eyeroll flip "Besides, I don't know what music you're talking about."
    mc "Sure you do."
  mc "I heard you singing in the shower."
  maya afraid "Wh-what?"
  mc "I said, I heard you s—"
  maya afraid "Shut up!"
  flora smile "Wait, really? Do you sing, [maya]?"
  flora smile "That's so cool! I had no idea!"
  maya afraid "I said, shut up about it!!" with vpunch
  show flora sad with {"master": Dissolve(.5)}
  "Oh, wow. That's unexpectedly rude, even from her."
  "And she doesn't sound like she's joking, either..."
  flora sad "Jeez. I'm sorry."
  mc "What's the big deal?"
  mc "Do you not want people to know you have the voice of an angel, because it doesn't fit your demonic image?"
  maya concerned "I said... shut up..."
  "Not even a witty comeback?"
  "Holy crap. I can't believe it."
  "I don't think I've ever seen [maya] genuinely embarrassed, but she totally is right now."
  mc "Aw, don't be shy! It was really good!"
  maya concerned "Just... drop it, okay? Or, um, I will end your life right here on this kitchen floor!"
  maya concerned "God. Unbelievable."
  window hide
  show flora sad:
    ease 1.0 xalign 0.5
  show maya concerned at disappear_to_left
  show flora skeptical with Dissolve(.5)
  window auto
  flora skeptical "Look what you did, [mc]! You upset our house guest!"
  mc "What? I was giving her a compliment..."
  flora skeptical "She'll be staying with us longer than expected."
  flora skeptical "So, you better be nice to her, you hear me?"
  show flora skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'm always nice!\"":
      show flora skeptical at move_to(.5)
      mc "I'm always nice!"
      flora confident "Hah! Don't make me laugh."
      mc "You don't count."
      flora concerned "I count the most."
      flora concerned "Speaking of, you should be nicer to me, too."
      mc "That's not going to prepare you for the harsh reality of the world."
      flora confident "Like you know anything about the real world."
      "Oh, man. If only she knew..."
      mc "Err, fine. But don't come crying to me when I'm right."
      mc "I won't even say \"I told you so.\""
      $flora.love-=1
      flora angry "Just stop bothering [maya], will you?"
      window hide
      show flora angry at disappear_to_left
      pause 0.5
      window auto
      "Ugh, girls..."
      "Whatever. Classes are about to start. I need to get to school."
    "\"Like I said, I was complimenting her...\"":
      show flora skeptical at move_to(.5)
      mc "Like I said, I was complimenting her..."
      mc "How is that anything but nice?"
      flora angry "Well, she clearly didn't like it."
      mc "No kidding..."
      flora angry "Exactly. No one's kidding. So, cut it out."
      mc "Fine. I'm sorry."
      mc "You look beautiful today, by the way."
      flora concerned "..."
      flora concerned "Stop it."
      mc "Stop what?"
      flora concerned "Complimenting people. It's weird."
      mc "It's not weird!"
      flora confident "Coming from you? Yes, it is."
      mc "Seriously, now?"
      mc "Excuse me for trying to be nicer..."
      flora concerned "There's nice, and then there's creepy."
      mc "It's not creepy, okay?!"
      flora angry "It is to me!"
      flora angry "Anyway, we're going to be late for school if you keep babbling."
      window hide
      show flora angry at disappear_to_left
      pause 0.5
      window auto
      "Of course it's my fault. As usual."
      "..."
      "She's right, though. I better get a move on."
    # "\"Another girl around here? Oh, boy. Just what I need.\"":
    "\"Another girl around here?\nOh, boy. Just what I need.\"":
      show flora skeptical at move_to(.5)
      mc "Another girl around here? Oh, boy. Just what I need."
      flora concerned "I saw you ogling her in that towel."
      flora concerned "Don't pretend like this is oh so hard for you."
      mc "I'm a man with eyes, okay? Nature can't be helped."
      flora angry "Don't be gross!"
      mc "That's not gross. Hair clogging the shower is gross."
      flora concerned "An easy sacrifice compared to what she was dealing with."
      "Right. Whatever that is."
      "I never actually got a clear answer on why [maya] is staying with us..."
      mc "Fine. You're right."
      flora excited "Now, that's music to my ears!"
      mc "Yeah, yeah. Don't get used to it."
      $flora.lust+=1
      flora excited "Too late!"
      window hide
      show flora excited at disappear_to_left
      pause 0.5
      window auto
      "God, these girls are going to be the end of me..."
      "First school, now my house."
      "What else will they overtake?"
      "..."
      "Speaking of school, I better get going. I don't want to be late."
  $quest.maya_sauce.advance("school")
  $maya.outfit = maya["outfit_stamp"]
  $maya["at_none_this_scene"] = flora["at_none_this_scene"] = True
  $mc["focus"] = ""
  return

label quest_maya_sauce_school_upon_entering:
  scene black with Dissolve(.07)
  $game.location = "school_ground_floor"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  show expression "school ground_floor retinue" as retinue:
    xpos 1441 ypos 561
  with Dissolve(.5)
  "Oh? I wonder what this is about?"
  "They all sound very excited."
  "An excited, assembled crowd can quickly lead to a mob."
  "And nothing good ever comes from mobs."
  "..."
  "Unless you're a pitchfork salesman, I guess."
  casey "Do you see the prize for first place?"
  casey "Totes a dream destination!"
  stacy "But babe, like, what even is your talent?"
  lacey "Well, I've seen her do this thing with her throat where—"
  stacy "It's not a dick eating contest, ladies."
  stacy "That doesn't count."
  lacey "Nevermind, then..."
  casey "It does count if I blow the judges."
  lacey "Slay!"
  stacy "Babes, chill. We'll just do a routine or something."
  window hide
  pause 0.125
  hide retinue with Dissolve(.5)
  pause 0.25
  window auto
  "Interesting..."
  "I need to get a closer look at this competition those bimbos were talking about."
  return

label quest_maya_sauce_school:
  mc "Excuse me, I'm passing through!"
  window hide
  show misc talent_show
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Oh, damn. A talent contest?"
  "Err, does beating Castlevania in under an hour count?"
  "Probably not..."
  "A warm, sunny trip to the beach sounds amazing, though."
  "Skipping out on this depressing autumn atmosphere."
  "If only I had a voice like [maya]'s..."
  "..."
  "Oh, well. The cheerleaders are probably going to win it with a stupid dance routine."
  "What a waste."
  $quest.maya_sauce.advance("classes")
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 0.5
  hide misc talent_show
  hide black onlayer screens
  with Dissolve(.5)
  pause 0.0625
  window auto
  # "Anyway, class is about to start. I better head over to the homeroom."
  "Anyway, class is about to start. I better head over to the homeroom.{space=-5}"
  return

label quest_maya_sauce_classes:
  "Phew! Made it just in time!"
  window hide
  show mrsl confident with Dissolve(.5)
  window auto
  if game.hour in (7,8,9,10):
    # mrsl confident "Good morning, everyone! We have now put over half the fall semester behind us, and I hope it's been a productive one so far!"
    mrsl confident "Good morning, everyone! We have now put over half the fall semester{space=-40}\nbehind us, and I hope it's been a productive one so far!"
  elif game.hour in (11,12,13,14):
    # mrsl confident "Good afternoon, everyone! We have now put over half the fall semester behind us, and I hope it's been a productive one so far!"
    mrsl confident "Good afternoon, everyone! We have now put over half the fall semester{space=-70}\nbehind us, and I hope it's been a productive one so far!"
  elif game.hour in (15,16,17,18):
    # mrsl confident "Good evening, everyone! We have now put over half the fall semester behind us, and I hope it's been a productive one so far!"
    mrsl confident "Good evening, everyone! We have now put over half the fall semester{space=-30}\nbehind us, and I hope it's been a productive one so far!"
  "I guess that depends on how you look at it..."
  "Academically? Probably not."
  # mrsl confident "Of course, with a new system of education in place, we'll need to collect feedback, and that's why we're here today."
  mrsl confident "Of course, with a new system of education in place, we'll need to collect{space=-85}\nfeedback, and that's why we're here today."
  # mrsl confident "Each one of you will have to write a short essay on the pros and cons you've experienced so far."
  mrsl confident "Each one of you will have to write a short essay on the pros and cons{space=-35}\nyou've experienced so far."
  "That sounds simple enough."
  if game.hour in (7,8,9,10,11,12):
    # mrsl laughing "Oh! And after lunch, we have a surprise four hour examination on what you've learned so far!"
    mrsl laughing "Oh! And after lunch, we have a surprise four hour examination on what{space=-60}\nyou've learned so far!"
  else:
    mrsl laughing "Oh! And after that, we have a surprise examination on what you've learned so far!"
  "Son of a bitch."
  window hide
  show mrsl laughing at move_to(.25)
  show flora afraid at Transform(xalign=.75) with Dissolve(.5)
  window auto
  flora afraid "A surprise test?! No one told us about that!"
  mc "Duh. That's why it's a surprise."
  # flora embarrassed "Ugh! I knew I should have studied instead of playing that stupid xCube yesterday..."
  flora embarrassed "Ugh! I knew I should have studied instead of playing that stupid xCube{space=-55}\nyesterday..."
  mc "Relax, will you? If anyone can ace a surprise test, it's you."
  flora skeptical "Don't tell me to relax."
  mrsl confident "All right, students! Let's get started!"
  window hide
  hide mrsl
  hide flora
  with Dissolve(.5)
  pause 0.25
  show black onlayer screens zorder 100 with Dissolve(3.0)
  while game.hour != 19:
    $game.advance()
  $quest.maya_sauce.advance("dinner")
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Goddamn, a whole day stuck in this classroom..."
  "I'm exhausted. I can't wait to get home."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_kitchen"
  $mc["focus"] = "maya_sauce"
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  window auto
  return

label quest_maya_sauce_dinner:
  $flora.equip("flora_hat")
  show flora confident with Dissolve(.5)
  flora confident "[mc]."
  # "Uh-oh. It's never a good sign when [flora] is this pleased with herself."
  "Uh-oh. It's never a good sign when [flora] is this pleased with herself.{space=-10}"
  mc "Err, how's it going?"
  flora confident "Pretty great, actually."
  # flora confident "[jo]'s board meeting is running late, and she asked me to make dinner."
  flora confident "[jo]'s board meeting is running late, and she asked me to make dinner.{space=-30}"
  mc "So, did you poison it?"
  flora angry "No! I made my favorite dish!"
  mc "Ah, of course."
  "That infamous chili of hers..."
  "I wonder if [maya] will be able to handle it?"
  window hide
  show maya confident at appear_from_left(.25)
  show flora angry:
    ease 1.0 xalign 0.75
  pause 0.5
  window auto
  maya confident "Something smells good."
  "Speak of the devil."
  flora excited "I made chili! You have to try it!"
  maya blush "That will be my pleasure."
  "Dinner alone with these two, eh?"
  "Hopefully, if I start choking I'll get some mouth to mouth..."
  window hide
  show maya chili flora_smile_right maya_chew
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $mc["focus"] = ""
  pause 0.5
  hide flora
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  flora "So, what do you think, [maya]?"
  mc "It's [flora]'s culinary masterpiece."
  mc "...or so she says, at least."
  maya chili flora_smile_right maya_thinking_left "It's good!"
  maya chili flora_smile_right maya_thinking_left "I haven't had anything this delicious in a long, long time."
  show maya chili flora_excited_right maya_thinking_left with {"master": Dissolve(.5)}
  flora "Really?!"
  show maya chili flora_excited_right maya_flirty_left with {"master": Dissolve(.5)}
  maya chili flora_excited_right maya_flirty_left "Fuck yeah! The last good thing I remember eating was pussy."
  "God, that's even hotter than the chili..."
  "Assuming [maya] is being serious, that is."
  mc "Oh? Whose?"
  maya chili flora_skeptical maya_chew_half "Sorry, I don't munch and tell."
  mc "Mm-hmm..."
  flora "Can you cut it out, [mc]?"
  flora "Seriously. At least during dinner."
  mc "But [maya] started it!"
  show maya chili flora_smile_right maya_chew_half with {"master": Dissolve(.5)}
  flora "[maya] is our guest."
  mc "So? Does that just excuse her?"
  maya chili flora_smile_right maya_flirty_empty "Them's the rules!"
  mc "That you just made up."
  show maya chili flora_smile_right maya_smile_left_empty with {"master": Dissolve(.5)}
  maya chili flora_smile_right maya_smile_left_empty "...maybe."
  maya chili flora_smile_right maya_smile_left_empty "Anyway, can I get some more of that chili, [flora]?"
  show maya chili flora_excited_right maya_smile_left_empty with {"master": Dissolve(.5)}
  flora "Really? Oh, wow! I'm amazed you can handle seconds!"
  flora "Most people take a few bites, and then reach for the milk."
  window hide
  pause 0.125
  show maya chili flora_excited_right maya_smile_left mc_half with Dissolve(.5)
  pause 0.25
  window auto
  mc "Not me! I don't quit!"
  maya chili flora_neutral maya_thinking mc_half "Oh? I always did peg you for a swallower, [mc]."
  maya chili flora_neutral maya_thinking mc_half "Still, I'm pretty confident I can take the heat better than anyone I know.{space=-60}"
  "Now, that's a challenge if I ever saw one..."
  menu(side="middle"):
    extend ""
    "\"You don't know me too well, then.\"":
      mc "You don't know me too well, then."
      maya chili flora_neutral maya_thinking mc_half "Really? Do you think you can handle heat better than me?"
      maya chili flora_neutral maya_thinking mc_half "I would have thought you like it bland, [mc]."
      mc "See? What did I just say about you not knowing me?"
      mc "I like all kinds of unexpected things."
      maya chili flora_neutral maya_flirty mc_half "Like a dick in the ass?"
      show maya chili flora_skeptical_right_half maya_flirty mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_right_half maya_flirty mc_half "Sometimes, that's unexpected."
      mc "Very funny. I meant in my mouth."
      maya chili flora_skeptical_right_half maya_dramatic mc_half "Ah, I see!"
      show maya chili flora_skeptical_right_half maya_smile_chest mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_right_half maya_smile_chest mc_half "You like dicks in your mouth. That tracks."
      show maya chili flora_sarcastic_right_half maya_smile_chest mc_half with {"master": Dissolve(.5)}
      flora "It kind of does, doesn't it?"
      mc "No! Food! Stop twisting my words!"
      maya chili flora_sarcastic_right_half maya_chew_half mc_half "No twisting necessary on my part."
      $maya.love-=1
      maya chili flora_sarcastic_right_half maya_chew_half mc_half "You did that all on your own like some kind of perverted pretzel."
      show maya chili flora_smile_half maya_chew_half mc_half with {"master": Dissolve(.5)}
      flora "As amusing as it is to watch [mc] be this embarrassed..."
      flora "...you guys do realize there's only one way to settle this, right?"
    "\"Pfft! From the cops, maybe.\"":
      mc "Pfft! From the cops, maybe."
      maya chili flora_neutral maya_dramatic mc_half "How very dare you bring up my record, sir?!"
      mc "What? Handcuffs are probably your favorite accessory."
      show maya chili flora_skeptical_half maya_dramatic mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_dramatic mc_half "Gasp!" with vpunch
      show maya chili flora_skeptical_half maya_neutral_left mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_neutral_left mc_half "..."
      show maya chili flora_skeptical_half maya_smile_chest mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_smile_chest mc_half "Actually, they would be my second favorite."
      maya chili flora_skeptical_half maya_smile_chest mc_half "Right after accessory to murder."
      mc "Whose?"
      maya chili flora_skeptical_half maya_chew_half mc_half "If you keep this up? Yours."
      mc "Is this why you're so fiery all the time? Your diet?"
      show maya chili flora_skeptical_half maya_thinking_half mc_half with {"master": Dissolve(.5)}
      maya chili flora_skeptical_half maya_thinking_half mc_half "If you're not burping brimstone, are you really trying?"
      mc "Heh! So, that's why the house smells of sulfur."
      show maya chili flora_eyeroll_half maya_thinking_half mc_half with {"master": Dissolve(.5)}
      flora "Oh, my god! Gross!"
      flora "Stop flirting, you two."
      maya chili flora_eyeroll_half maya_dramatic_left_half mc_half "Oh, sweetheart. That wasn't flirting."
      show maya chili flora_eyeroll_half maya_smile_left_half mc_half with {"master": Dissolve(.5)}
      maya chili flora_eyeroll_half maya_smile_left_half mc_half "You'll definitely know when I'm doing that."
      mc "Knowing [maya], it's probably about as subtle as a hand on the dick..."
      $maya.love+=1
      maya chili flora_eyeroll_half maya_flirty_chest_half mc_half "Aww! You really do know me!"
      show maya chili flora_skeptical_half maya_flirty_chest_half mc_half with {"master": Dissolve(.5)}
      flora "Okay! Point taken! Moving on!"
      flora "You guys do realize there's only one way to settle this, right?"
    "\"Weird brag, but okay...\"":
      mc "Weird brag, but okay..."
      maya chili flora_neutral maya_thinking mc_half "I'm not bragging. Just stating facts."
      mc "I'm just saying, if I were you, I'd be bragging about my singing voice instead."
      $maya.lust-=1
      # maya chili flora_skeptical_half maya_bored mc_half "Bring that up again, and I'll break your throat and cut your hair. I mean it."
      maya chili flora_skeptical_half maya_bored mc_half "Bring that up again, and I'll break your throat and cut your hair.\nI mean it."
      mc "Threats in my own home?! Hallelujah!"
      maya chili flora_skeptical_half maya_bored mc_half "I can threaten you outside, if you'd prefer? It makes no difference to me."
      mc "Err, I'm good, thanks."
      show maya chili flora_annoyed_half maya_chew_half mc_half with Dissolve(.5)
      flora "What did I tell you about being nice, [mc]?"
      mc "Me? She's the one making threats!"
      flora "Well, you earned them."
      mc "Your betrayal wounds me, [flora]."
      show maya chili flora_skeptical_half maya_chew_half mc_half with {"master": Dissolve(.5)}
      flora "Stop being dramatic, will you?"
      "Oh, but it's fine when [maya] does it? Girls..."
      # flora "Anyway, you guys do realize there's only one way to settle this, right?"
      flora "Anyway, you guys do realize there's only one way to settle this, right?{space=-35}"
  if renpy.showing("maya chili flora_smile_half maya_chew_half mc_half"):
    maya chili flora_smile_half maya_flirty_left_half mc_empty "A gun duel, obviously."
    maya chili flora_smile_half maya_flirty_left_half mc_empty "Loser deep-throats the barrel."
  elif renpy.showing("maya chili flora_skeptical_half maya_flirty_chest_half mc_half"):
    maya chili flora_skeptical_half maya_thinking_left_bowl_half mc_empty "A gun duel, obviously."
    maya chili flora_skeptical_half maya_thinking_left_bowl_half mc_empty "Loser deep-throats the barrel."
  elif renpy.showing("maya chili flora_skeptical_half maya_chew_half mc_half"):
    maya chili flora_skeptical_half maya_flirty_left_half mc_empty "A gun duel, obviously."
    maya chili flora_skeptical_half maya_flirty_left_half mc_empty "Loser deep-throats the barrel."
  mc "That sounds a little extreme... and very dirty."
  if renpy.showing("maya chili flora_smile_half maya_flirty_left_half mc_empty"):
    show maya chili flora_excited_right_half maya_flirty_left_half mc_empty with {"master": Dissolve(.5)}
  elif renpy.showing("maya chili flora_skeptical_half maya_thinking_left_bowl_half mc_empty"):
    show maya chili flora_excited_right_half maya_thinking_left_bowl_half mc_empty with {"master": Dissolve(.5)}
  elif renpy.showing("maya chili flora_skeptical_half maya_flirty_left_half mc_empty"):
    show maya chili flora_excited_right_half maya_flirty_left_half mc_empty with {"master": Dissolve(.5)}
  flora "No, silly! A sauce-off!"
  maya chili flora_excited_right_half maya_dramatic_left_chest_half mc_empty "But my sauce is shy!"
  mc "A sauce-off?"
  show maya chili flora_smile_half maya_dramatic_left_chest_half mc_empty with {"master": Dissolve(.5)}
  flora "A hot sauce competition to decide which one of you is tougher."
  maya chili flora_smile_half maya_smile_half mc_empty "We already know who's tougher."
  maya chili flora_smile_half maya_smile_half mc_empty "I don't mind putting [mc] in his place, though."
  mc "We'll see about that. All right, deal."
  show maya chili flora_excited_half maya_smile_half mc_empty with {"master": Dissolve(.5)}
  flora "Great! I'll be the referee."
  maya chili flora_excited_half maya_thinking_left_bowl_empty mc_empty "You can be anything your little heart desires, sweetheart."
  maya chili flora_excited_half maya_thinking_left_bowl_empty mc_empty "Except not tonight, because I still have to go make a living on the corner."
  mc "How about tomorrow at high noon, then?"
  maya chili flora_excited_empty maya_flirty_empty mc_empty "You've got yourself a deal there, pardner!"
  window hide
  $unlock_replay("maya_taste")
  $flora.unequip("flora_hat")
  show maya sarcastic at Transform(xalign=.25)
  show flora smile at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  $game.advance()
  $home_kitchen["night"] = True
  pause 0.5
  show flora smile at disappear_to_left
  show maya sarcastic at disappear_to_left
  pause 0.0
  hide black onlayer screens with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Hopefully, I haven't gotten myself into anything I can't handle."
  "[maya] did just eat two bowls of that chili like it was oatmeal..."
  "But I grew up on that stuff. I should be fine, right?"
  "..."
  "I better make sure I'm well rested and in tip top stamina shape for tomorrow."
  $quest.maya_sauce.advance("preparation")
  return

label quest_maya_sauce_preparation:
  show maya sarcastic_pepper with Dissolve(.5)
  if game.hour in (7,8,9,10):
    mc "Err, good morning, [maya]."
  elif game.hour in (11,12,13,14):
    mc "Err, good afternoon, [maya]."
  elif game.hour in (15,16,17,18):
    mc "Err, good evening, [maya]."
  elif game.hour == 19:
    mc "Err, good night, [maya]."
  mc "What have you got there?"
  maya sarcastic_pepper "A spicy dildo, what else?"
  mc "..."
  mc "Are you eating raw chili peppers?"
  maya dramatic_pepper "What? Do you mean I'm supposed to be putting it in my mouth?!"
  maya dramatic_pepper "This changes everything!"
  mc "I bet it does. You're welcome."
  maya cringe_pepper "I didn't say it was a good thing."
  mc "Well, I'm still not sorry."
  maya sarcastic_pepper "Smart. Never admit fault."
  mc "Anyhow, are you preparing for our big competition or something?"
  maya thinking_pepper "Oh, right! I had forgotten about it, actually."
  maya thinking_pepper "I just wanted a light snack."
  window hide
  show maya flirty_pepper with Dissolve(.5)
  window auto
  "She gives me a cheery innocent smile, which somehow looks disturbing and out of place on her..."
  show maya flirty_half_eaten_pepper with {"master": Dissolve(.5)}
  "...before popping the pepper right into her mouth, and chewing it slowly."
  "She moves it around in her mouth, taking her time."
  "She looks at me and licks her lips."
  "It's hot and sensual... except for the fact that she's clearly trying to intimidate me."
  window hide
  show maya flirty with Dissolve(.5)
  window auto
  maya flirty "Yum! Delicious."
  show maya flirty at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Your scare tactics won't work on me.\"":
      show maya flirty at move_to(.5)
      mc "Your scare tactics won't work on me."
      maya thinking "Scare tactics?"
      mc "Yes. Eating raw peppers in front of me."
      mc "You're clearly trying to make me back out of our bet."
      maya afraid "Such an accusation!"
      maya concerned "I actually grew up on nothing but peppers from the church garden."
      maya concerned "It's all dear old mom could find to feed me."
      mc "Ah, shit. I had no idea."
      maya kiss "Maybe because that's the story of Dick Charleson's most famous character."
      maya kiss "Do I look like a poor orphan to you?"
      mc "Err... I mean..."
      mc "You {i}are{/} staying with us."
      maya dramatic "Wow. Unbelievable."
      maya dramatic "I can eat the hottest peppers in the world, but nothing can scorch me like you, [mc]."
      mc "I wasn't trying to be insensitive, okay? I'm sorry."
      maya cringe "I don't think my poor, fragile heart will ever recover."
      maya cringe "You should probably go ahead and just back out of this now."
      mc "Ah, I see!"
      mc "Nice try, but the hot sauce competition is still going down."
      mc "You can't manipulate me into giving up."
      # "To think I almost fell for the innocent act and those puppy dog eyes..."
      "To think I almost fell for the innocent act and those puppy dog eyes...{space=-30}"
      "[maya] is a dangerous one, not to be underestimated."
      $maya.love+=1
      maya sarcastic "Very well, [mc]. It's going down like a hooker on a Friday night."
      mc "Err, exactly?"
    "\"Can I have some?\"":
      show maya flirty at move_to(.5)
      mc "Can I have some?"
      maya thinking "Are you sure that's a good idea?"
      mc "Why wouldn't it be? You saw how I handle my chili."
      maya flirty "Oh, I saw it, all right."
      mc "Just give it to me, then."
      maya kiss "Now, you're speaking my language!"
      maya kiss "I'll give it to you good, baby."
      window hide
      show maya kiss_pepper with Dissolve(.5)
      window auto
      "She hands over one of the peppers, then watches me expectantly."
      "I can already feel my mouth salivating in anticipation of the heat."
      mc "Thank you."
      window hide
      show maya flirty with Dissolve(.5)
      window auto
      mc "That's a nice, juicy pepper..."
      maya flirty "Mm-hmm. Open wide."
      mc "Now, {i}you're{/} speaking my language!"
      mc "..."
      "Pop open the hatch. Pepper goes in. Boom!"
      "The juice explodes onto my tongue, setting it ablaze in an instant."
      $mc.strength+=2
      mc "Mmm! That is good stuff!"
      maya dramatic "Oh? Not bad, Not bad."
      maya dramatic "That is just the beginning, though."
      "Phew! Hot! She's really not joking around..."
      "But this was some good preparation."
      "There's no way I'm backing down."
      mc "Well, I can't wait for the middle and the end."
      maya sarcastic "Soon, it'll be on."
      mc "So on..."
    "\"I bet that's what you say about everything you put in your mouth.\"":
      show maya flirty at move_to(.5)
      mc "I bet that's what you say about everything you put in your mouth."
      maya dramatic "Don't be salty just because I won't put your dick in my mouth."
      maya dramatic "You would definitely be hearing something different then."
      mc "..."
      "Oh, fuck. All I can think about now is my dick sliding in between her soft, pillowy lips..."
      "Mmm... now, that's a distraction worthy of throwing me off my game..."
      "I'll just have to throw it right back at her."
      mc "Right. The sound of you gagging on my cock, that is."
      # maya sarcastic "So confident for someone who's about to lose a questionable-things-in-the-mouth contest!"
      maya sarcastic "So confident for someone who's about to lose a questionable-things-{space=-25}\nin-the-mouth contest!"
      $maya.lust+=1
      maya sarcastic "I can fake liking anything, honey."
      mc "Oh, I bet you can."
      mc "Except I won't lose to you."
  mc "In fact, what do you think about upping the stakes for this little competition of ours?"
  maya neutral "Upping them how?"
  mc "Loser owes the winner, of course."
  maya smile "Oh, I do like being owed."
  maya smile "If I win, I want your bedroom for as long as I remain staying with you guys."
  "[maya] gets right to the point, doesn't she?"
  "I do like the privacy of my room... for reasons... but I like the idea of being owed, too."
  mc "Err, fine. Deal."
  # maya eyeroll "And let me guess — if by some miracle you do win, you want a blowjob or some other sexual favor?"
  maya eyeroll "And let me guess — if by some miracle you do win, you want a blowjob{space=-60}\nor some other sexual favor?"
  maya eyeroll "You dudes are all so predic—"
  mc "Actually, I want you to sing in the upcoming talent competition."
  maya annoyed "Wh-what did you say?"
  mc "If I win, you're going to sing in the talent competition at the pancake brothel, and I'll take the reward should you win it."
  mc "An all expenses trip paid to Cabo."
  maya concerned "...mostly..."
  mc "Excuse me?"
  maya concerned "...mostly... paid..."
  # "Oh, wow. Even the mere mention of her singing sends her into a near state of shock."
  "Oh, wow. Even the mere mention of her singing sends her into a near{space=-25}\nstate of shock."
  "I never thought I'd see it."
  mc "What's wrong, [maya]?"
  mc "You look a little worried all of a sudden."
  maya afraid "O-of course not! I will totally beat you!"
  mc "I meant beat me in the hot sauce competition, not {i}beat me off.{/}"
  "There! A taste of her own medicine."
  "It's much easier when she's the one flustered for once."
  # "She looks hesitant. There might even be a bead of sweat rolling down her forehead."
  "She looks hesitant. There might even be a bead of sweat rolling down{space=-35}\nher forehead."
  maya concerned "Your, err.. enjoy your bedroom while you still can..."
  mc "You don't sound so sure about that."
  maya afraid "Sh-shut up!!"
  window hide
  show maya afraid at disappear_to_left
  pause 0.5
  window auto
  "Heh. Now, who's in whose head?"
  "I'm feeling better and better about this..."
  if game.hour == 12:
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $quest.maya_sauce.advance("sauces")
    $game.location = "home_kitchen"
    hide maya
    pause 1.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
    window auto
    jump quest_maya_sauce_wait_shortcut
  $quest.maya_sauce.advance("wait")
  $maya["at_none_this_scene"] = True
  return

label quest_maya_sauce_wait:
  if game.location == "home_kitchen":
    $quest.maya_sauce.advance("sauces")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location != "home_kitchen":
    "It's high noon. I can't wait to beat [maya]."
    if mc.owned_item("paper"):
      "I just have to put [maxine]'s priced paper back in the stack first."
      "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    if mc.owned_item("paper"):
      $mc.remove_item("paper")
      pause 0.25
    show black onlayer screens zorder 100 with Dissolve(.5)
    $quest.maya_sauce.advance("sauces")
    if game.location.id.startswith("home_"):
      $game.location = "home_kitchen"
      pause 1.0
    else:
      $game.location = "home_kitchen"
      pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
    window auto

label quest_maya_sauce_wait_shortcut:
  "Okay, I guess this is it. The big challenge."
  "Pride or humiliation — what's it going to be?"
  window hide
  $flora["outfit_stamp"] = flora.outfit
  $flora.outfit = {"panties":"flora_striped_panties", "bra":"flora_purple_bra", "pants":"flora_dress", "shirt":"flora_blouse", "hat":"flora_referee_cap", "blindfold":"flora_referee_shirt"}
  show flora excited with Dissolve(.5)
  window auto
  flora excited "Are you ready for your sauce-off with [maya]?"
  flora excited "Now is your chance to win something for once!"
  mc "I once saw a video of a fakir swallowing fire. I'm as ready as it gets."
  "I can't show fear. I can't hesitate."
  "The moment these girls sense any weakness, they'll pounce."
  window hide
  show flora excited at move_to(.25)
  with None
  show flora neutral
  show maya bored flip at Transform(xalign=.75)
  with Dissolve(.5)
  window auto
  maya bored flip "So, he decided to show up, after all."
  mc "This is my house..."
  mc "And, err... of course I did."
  mc "I desperately need that vacation."
  maya sad flip "Aw, poor baby! Being one of your kind must be hard these days."
  mc "What? A straight white male?"
  maya confident flip "No, a loser who's about to get his ass kicked."
  mc "Heh. We'll see about that."
  flora neutral "Are you two ready or what?"
  mc "I was born ready."
  maya blush flip "Using my mouth is the only way I know how to prove my worth!"
  flora confident "Excellent. Let's get saucy, then!"
  # flora confident "In the blue corner, weighing in at... way too many pounds of sadness, fat, and perversion... we have [mc]!"
  flora confident "In the blue corner, weighing in at... way too many pounds of sadness,{space=-25}\nfat, and perversion... we have [mc]!"
  mc "Now, that's an introduction."
  maya bored flip "So observant."
  # flora excited "And in the red corner, weighing in at an undisclosed amount of sugar, spice, and everything sassy..."
  flora excited "And in the red corner, weighing in at an undisclosed amount of sugar,{space=-35}\nspice, and everything sassy..."
  flora excited "She's bold, she's beautiful, she's bound to kick ass! It's [maya]!"
  maya blush flip "Thank you, thank you! I love my fans! And I love Jesus!"
  mc "How come her introduction was much nicer?"
  flora neutral "Well, you get what you pay for."
  mc "But we didn't p—"
  flora excited "Anywho!"
  flora excited "You will find I have arranged an assortment of fiery fineries."
  flora excited "Competitors, please take a moment to familiarize yourselves with the various hot sauces."
  flora excited "Once you are both positively salivating, we'll begin."
  window hide
  hide flora
  hide maya
  with Dissolve(.5)
  window auto
  "Okay, let's see what we're working with here..."
  $quest.maya_sauce["hot_sauces"] = set()
  $mc["focus"] = "maya_sauce"
  return

label quest_maya_sauce_sauces_home_kitchen_door:
  "Nope. They'd tease me endlessly if I ran away now."
  return

label quest_maya_sauce_sauces_home_kitchen_stairs:
  "As much as I'd like to go upstairs and play the xCube, silly games with girls is my life now."
  return

label quest_maya_sauce_sauces:
  mc "All right, done! Let's get this show on the road!"
  window hide
  show flora blush with Dissolve(.5)
  window auto
  flora blush "So self assured!"
  flora blush "[maya]?"
  window hide
  show flora blush at move_to(.25)
  show maya dramatic at Transform(xalign=.75) with Dissolve(.5)
  window auto
  maya dramatic "He is very self assured, I agree."
  flora thinking "I meant, are you ready?"
  maya cringe "If we wait any longer, [mc]'s clothes will go out of style."
  maya sarcastic "Oh, wait! It's too late already."
  mc "Very funny. You can't get into my head."
  maya dramatic "I don't think that's a place anyone wants to go."
  flora laughing "Ain't that the truth!"
  mc "Ugh..."
  show maya sarcastic
  flora flirty "Okay! First up, we have {i}Taco Hell Hot Sauce.{/}"
  flora flirty "Each of you will sample the hot sauce on its own."
  mc "On its own? Not even with chips, or something?"
  flora blush "Nope! No punking out with a chip barrier."
  flora blush "Whoever survives the longest wins!"
  mc "And what if it's a tie?"
  flora thinking "I'll be the judge of that, and make the final ruling as needed."
  mc "Fine, but none of that woman solidarity crap, okay?"
  flora laughing "I am a just and gracious judge!"
  show flora laughing at move_to("left")
  show maya sarcastic at move_to("right")
  menu(side="middle"):
    extend ""
    # "\"You're not confusing judge with judgemental, are you?\"":
    "\"You're not confusing judge\nwith judgemental, are you?\"":
      show flora laughing at move_to(.25)
      show maya sarcastic at move_to(.75)
      mc "You're not confusing judge with judgemental, are you?"
      $flora.love-=1
      flora concerned "Keep this up and I just might accept bribes from [maya]."
      maya flirty "Oh? I didn't know you're into my kind of bribes, [flora]."
      maya kiss "I'm totally down, though!"
      mc "Stop! No bribes, girls!"
      mc "Unless..."
      mc "[maya], do you eat pu—"
      flora angry "Ugh! Knock it off, you two!"
      mc "Err, sorry."
      maya thinking "Right. He's sorry."
    # "\"The most gracious {i}and{/} the most beautiful.\"":
    "\"The most gracious {i}and{/}\nthe most beautiful.\"":
      show flora laughing at move_to(.25)
      show maya sarcastic at move_to(.75)
      mc "The most gracious {i}and{/} the most beautiful."
      flora thinking "You're looking for favoritism, aren't you?"
      mc "Of course not! I want a fair contest!"
      mc "I'm just calling it how I see it."
      maya dramatic "Oh, god. The sweetness... the sincerity..."
      maya dramatic "I'm melting!"
      mc "Yeah, yeah. Whatever, witch."
      mc "I just want [flora] to know that we appreciate her doing this."
      mc "The effort, the poise, the—"
      flora laughing "Okay, that's enough! Stop it!"
      $flora.love+=1
      flora blush "...but, um, thank you."
  if renpy.showing("flora angry"):
    flora neutral "Anyway, here we go!"
    flora neutral "No spitting, no food, no drinks. Else you lose."
  elif renpy.showing("flora blush"):
    flora blush "Anyway, here we go!"
    flora blush "No spitting, no food, no drinks. Else you lose."
  mc "Got it."
  maya dramatic "Naturally."
  flora excited "Okay, then. Bottoms up!"
  maya sarcastic "Oh? I didn't know it was going to be that kind of contest."
  maya sarcastic "Good thing I wore a skirt. I'm so ready to get my ass scorched!"
  flora angry "[maya]!"
  mc "Yeah, [maya], behave."
  maya cringe "The death of self-expression! Woe is me!"
  mc "Whatever. I guess I'll go first."
  mc "..."
  show flora angry at move_to("left")
  show maya cringe at move_to("right")
  menu(side="middle"):
    extend ""
    "?mc.strength>=3@[mc.strength]/3|{image=stats str}|Channel your inner hothead":
      show flora angry at move_to(.25)
      show maya cringe at move_to(.75)
      "A mild burning sensation hits my mouth."
      "It's not bad. I might start using this one."
      mc "Easy as pie! Tastes like it, too."
      maya sarcastic "You swallow like a champ, [mc]."
      mc "Very cute. I bet you hear that one a lot."
      maya dramatic "I do! My fans love to chant it during the cock-sucking contests."
      maya dramatic "...where I'm also the champion, by the way."
      mc "Just take your turn, will you?"
      window hide
      show maya hot_sauce_competition taco_hell_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $mc["focus"] = ""
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "[maya] shrugs and takes the bottle from me."
      "Her eyes meet mine as she tips it back, lathering her tongue in the sauce."
      show maya hot_sauce_competition taco_hell_aftermath licking with {"master": Dissolve(.5)}
      "She swallows, then closes her eyes and licks her lips."
      show maya hot_sauce_competition taco_hell_aftermath bored with {"master": Dissolve(.5)}
      maya hot_sauce_competition taco_hell_aftermath bored "Like a champ."
      flora "Well done, both of you!"
      # flora "Take a moment to let it die down, and then we'll move to the next one."
      flora "Take a moment to let it die down, and then we'll move to the next one.{space=-45}"
    "Give it your best shot":
      show flora angry at move_to(.25)
      show maya cringe at move_to(.75)
      "Instantly, the heat of regret washes through me."
      "My tongue ignites into a fiery inferno."
      "Beads of sweat immediately spring to my forehead."
      show flora concerned
      show maya sarcastic
      with {"master": Dissolve(.5)}
      mc "Pfft!" with vpunch
      "It's just too much. I'm forced to spit it out."
      "My face on fire from the heat and the shame."
      maya sarcastic "Hah! I knew you were a spitter."
      window hide
      show maya hot_sauce_competition taco_hell_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $mc["focus"] = ""
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      pause 0.25
      show maya hot_sauce_competition taco_hell_aftermath licking with Dissolve(.5)
      window auto
      # "She takes the bottle from me, pours the sauce right onto her tongue and then swallows."
      "She takes the bottle from me, pours the sauce right onto her tongue{space=-10}\nand then swallows."
      show maya hot_sauce_competition taco_hell_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition taco_hell_aftermath confident "Yum! Piece of cake."
      "Fuck. She didn't even break a sweat."
      window hide
      show flora laughing at Transform(xalign=.25)
      show maya bored flip at Transform(xalign=.75)
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $quest.maya_sauce.advance("aftermath")
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      flora laughing "Well, that was fast! We have a winner!"
      flora laughing "Congratulations, [maya]!"
      flora worried "As for you, [mc]... I'm mildly disappointed."
      flora worried "I thought you would at least last a little longer."
      mc "You and me both..."
      maya bored flip "Isn't that always the case? They never last long."
      jump quest_maya_sauce_sauces_defeated
  window hide
  show flora excited at Transform(xalign=.25)
  show maya bored flip at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora excited "Okay, contestants! We are now moving onto sauce number two."
  flora excited "Prepare your tongues and secure your taste buds..."
  flora excited "Next up, we have {i}The Saucy Wench!{/}"
  maya bored flip "Just like my noisy ex-roommate."
  mc "Or [flora]."
  flora angry "Hey! Don't harass the judge!"
  maya skeptical flip "Yeah! He should get points deducted!"
  flora angry "I'm very tempted to straight up disqualify him."
  mc "That's the only way [maya] is going to win."
  maya confident flip "Nevermind. Let him play. I want to beat him."
  flora concerned "Tsk! Fine."
  flora concerned "Go ahead, [mc]."
  mc "Don't mind if I do..."
  "As the lid comes off, a sharp smell assaults my nose."
  "It makes my eyes water, but I can't show weakness now."
  mc "It smells... delicious."
  maya bored flip "Don't just stare at it. Eat it."
  mc "Careful, or I'll make you swallow the entire thing."
  maya blush flip "Don't start sweet talking me now!"
  mc "Heh. I knew you would like that."
  "Everyone is confident before the sauce hits their tongue."
  "And boy, it does."
  mc "..."
  show flora concerned at move_to("left")
  show maya blush flip at move_to("right")
  menu(side="middle"):
    extend ""
    # "?mc.strength>=6@[mc.strength]/6|{image=stats str}|Stronger sauces have treated me worse":
    "?mc.strength>=6@[mc.strength]/6|{image=stats str}|Stronger sauces have\ntreated me worse":
      show flora concerned at move_to(.25)
      show maya blush flip at move_to(.75)
      "A pulse of fire rushes through my mouth."
      "It's hot, all right... but not enough to make me quit."
      # "A wink at [maya]. A drop of sauce in my mouth. Fearless and cocksure."
      "A wink at [maya]. A drop of sauce in my mouth. Fearless and cocksure.{space=-30}"
      mc "You were saying?"
      maya bored flip "Oh, please. That's an easy one."
      maya bored flip "Hand it over."
      window hide
      show maya hot_sauce_competition wench_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "She snatches it out of my hands, and instantly knocks it back, as if downing a shot."
      show maya hot_sauce_competition wench_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition wench_aftermath confident "Mmm! Refreshing!"
      mc "Yeah, yeah."
      mc "What else have you got, [flora]?"
      flora "We are about to enter masochism territory now."
      flora "Are you guys ready?"
      show maya hot_sauce_competition wench_aftermath dramatic with {"master": Dissolve(.5)}
      maya hot_sauce_competition wench_aftermath dramatic "Alas, what is pain but a reminder that you are alive?"
      mc "Bring it on."
      flora "Very well!"
    "Try not to cry":
      show flora concerned at move_to(.25)
      show maya blush flip at move_to(.75)
      "Fuck, fuck, fuck!"
      "That's way hotter than I was expecting..."
      "I'll never live it down if I puss out, but—"
      show maya confident flip with {"master": Dissolve(.5)}
      mc "Pfft!" with vpunch
      "The pain overwhelms me, my eyes water, and the spit-sauce mix dribbles out of my mouth."
      maya confident flip "He spits, he quits."
      flora concerned "Really, [mc]? That was lowkey pathetic."
      mc "Sh-shut up... [maya] still has to try it..."
      maya confident flip "That won't be a problem."
      window hide
      show maya hot_sauce_competition wench_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      # "She takes the bottle from me and squirts it straight onto her tongue."
      "She takes the bottle from me and squirts it straight onto her tongue.{space=-15}"
      # "She holds it there for what feels like forever, before finally swallowing."
      "She holds it there for what feels like forever, before finally swallowing.{space=-45}"
      show maya hot_sauce_competition wench_aftermath bored with {"master": Dissolve(.5)}
      maya hot_sauce_competition wench_aftermath bored "Easier than a load of cum."
      maya hot_sauce_competition wench_aftermath bored "Tastier, too."
      mc "Ugh! No way!"
      flora "Don't be a sore loser, [mc]."
      flora "You did it, [maya]!"
      jump quest_maya_sauce_sauces_defeated
  window hide
  show flora flirty at Transform(xalign=.25)
  show maya dramatic at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora flirty "Then, I present to you... {i}Bust a Ghost!{/}"
  maya sarcastic "Oh! Just like [mc] last night!"
  mc "True, I did hear someone moaning."
  maya cringe "If you're implying what I think you're implying, it wasn't me."
  maya sarcastic "I take it in silence like a good girl."
  mc "Heh. I bet you do."
  # "[maya] is probably just messing around, but now I can't get the image of her struggling to stay quiet while getting railed out of my head..."
  "[maya] is probably just messing around, but now I can't get the image of{space=-55}\nher struggling to stay quiet while getting railed out of my head..."
  flora worried "I sincerely hope you're drooling because of the hot sauce."
  mc "Err, yes! Let's have a taste, shall we?"
  maya dramatic "Sorry, I'm off the clock."
  mc "Maybe next time, then?"
  flora worried "..."
  "Uh-oh. [flora] seems to be getting annoyed by the banter."
  "I better tone it down if I don't want to jeopardize my chances of winning this contest."
  mc "I meant, err... a taste of the sauce, of course! [flora]?"
  flora thinking "Here you go."
  flora thinking "Let's see if you can handle it, [mc]."
  mc "Happily."
  "Except when I grab this one, my hand shakes slightly."
  "Probably anticipation for the spice that's about to hit."
  mc "..."
  show flora thinking at move_to("left")
  show maya dramatic at move_to("right")
  menu(side="middle"):
    extend ""
    "?mc.strength>=9@[mc.strength]/9|{image=stats str}|Be like hot sauce":
      show flora thinking at move_to(.25)
      show maya dramatic at move_to(.75)
      "It can flow, it can crash, and it can hit you right in the face."
      "And hit it certainly does."
      # "Like a wildfire raging through my mouth and sinuses, making me sweat on the inside."
      "Like a wildfire raging through my mouth and sinuses, making me sweat{space=-65}\non the inside."
      # "My face goes red, my ears thud, my eyes bleed... but I fight through it and then swallow."
      "My face goes red, my ears thud, my eyes bleed... but I fight through it{space=-30}\nand then swallow."
      "Even pulling off a weak smile at the end."
      mc "D-done."
      show maya sarcastic
      flora worried "Are you okay?"
      mc "I have... never... been better..."
      maya sarcastic "Still hanging in there, are we?"
      "As she takes the bottle from me, my hoodie practically falls off my shoulders by itself."
      "The kitchen suddenly feels a lot warmer."
      maya sarcastic "Whoa, there! I don't remember ordering a stripper."
      mc "Don't pretend like you don't enjoy the show..."
      maya dramatic "Be still, my beating heart!"
      window hide
      show maya hot_sauce_competition bust_a_ghost_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      pause 0.25
      show maya hot_sauce_competition bust_a_ghost_aftermath licking with Dissolve(.5)
      window auto
      "After she has a taste, her own hoodie comes off."
      mc "Oh, my god! Look! Naked arms!"
      $maya.love+=1
      show maya hot_sauce_competition bust_a_ghost_aftermath dramatic with {"master": Dissolve(.5)}
      maya hot_sauce_competition bust_a_ghost_aftermath dramatic "I'm so sorry! I know this much flesh scandalizes you terribly!"
      "Seeing [maya] like that does do something to me, all right."
      "The way the clothes cling to her moist skin and accentuates her petite curves..."
      "Her chest heaving from the heat of the moment..."
      "She's so tiny... very much unlike her personality."
      flora "You could definitely say things are heating up now!"
      flora "Clothes coming off and fists coming out!"
      flora "Let's see how you handle this next one..."
    "Believe in yourself":
      show flora thinking at move_to(.25)
      show maya dramatic at move_to(.75)
      "And the hit sends me straight to my knees." with vpunch
      # "For a moment, my spirit leaves my body, hovering over the young man in pain."
      "For a moment, my spirit leaves my body, hovering over the young man{space=-50}\nin pain."
      "He writhes on the floor, coughing, gasping, eyes and nose leaking."
      flora worried "[mc]? Are you okay?"
      # "When I return to my body, my head is pounding, my vision is blurred, and my throat is on fire."
      "When I return to my body, my head is pounding, my vision is blurred,{space=-20}\nand my throat is on fire."
      maya dramatic "He looks like he's going to explode."
      mc "F-fuck! Move!"
      window hide
      play sound "fast_whoosh"
      show flora worried at move_to("left")
      show maya dramatic at move_to("right")
      pause 0.0
      hide screen interface_hider
      show maya hot_sauce_competition bust_a_ghost_taste
      show black
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "The dash to the fridge feels like a marathon."
      "The fumbling search for the milk is like a desperate robbery."
      "Finally, it hits my tongue, blessedly soothing the raging fire."
      flora "Disqualified!"
      show black onlayer screens zorder 100
      pause 0.5
      hide flora
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      pause 0.25
      show maya hot_sauce_competition bust_a_ghost_aftermath licking with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      flora "Damn, [maya]! Look at you go!"
      flora "That's a real winner, right there!"
      # "When the burning finally subsides, [maya] is standing there in her skirt and top, her jacket discarded."
      "When the burning finally subsides, [maya] is standing there in her skirt{space=-25}\nand top, her jacket discarded."
      "There's a faint bit of sweat clinging to her neck, making the already tight top stick to her even more."
      "It leaves very little to the imagination."
      show maya hot_sauce_competition bust_a_ghost_aftermath bored with {"master": Dissolve(.5)}
      maya hot_sauce_competition bust_a_ghost_aftermath bored "It's getting toasty, isn't it?"
      maya hot_sauce_competition bust_a_ghost_aftermath bored "Except I managed to keep it down without turning into a little bitch."
      mc "You should... keep going..."
      "At least give me some sort of consolation prize..."
      show maya hot_sauce_competition bust_a_ghost_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition bust_a_ghost_aftermath confident "Never do more work than you absolutely have to."
      flora "True! She's our winner with that one."
      jump quest_maya_sauce_sauces_defeated
  window hide
  $maya.unequip("maya_jacket")
  show flora confident at Transform(xalign=.25)
  show maya bored flip at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora confident "It's the infamous {i}Carolina Jeepers Creeper!{/}"
  flora confident "It is said to have once killed a group of teenagers during a bus ride!"
  flora confident "Take a crack at it, if you dare!"
  mc "Oh, I most certainly do."
  mc "I'm sure it's nothing more than a scarecrow. Hand it over."
  flora excited "Enjoy it, big boy!"
  # "My throat still burns and my tongue is starting to feel numb, but I can't show any weakness."
  "My throat still burns and my tongue is starting to feel numb, but I can't{space=-60}\nshow any weakness."
  # "Instead, the cap comes off with a pop, and the sauce hits the back of my throat."
  "Instead, the cap comes off with a pop, and the sauce hits the back of{space=-25}\nmy throat."
  mc "..."
  show flora excited at move_to("left")
  show maya bored flip at move_to("right")
  menu(side="middle"):
    extend ""
    # "?mc.strength>=12@[mc.strength]/12|{image=stats str}|The flesh is weak, but the mind endures":
    "?mc.strength>=12@[mc.strength]/12|{image=stats str}|The flesh is weak,\nbut the mind endures":
      show flora excited at move_to(.25)
      show maya bored flip at move_to(.75)
      "It hits me like a fire truck, right in the face."
      "Slamming into my taste buds and screaming through my sinuses."
      "My brain vibrates in my head, my eyes and nose start to gush."
      mc "Hoooly—"
      show flora neutral
      maya confident flip "You're looking a little red there, [mc]."
      maya confident flip "Do you need some water?"
      "Y-yes!"
      mc "N-no..."
      mc "That was... nothing..."
      maya blush flip "Are you sure? Because it looks like you're about to shit your pants."
      mc "If I do, I'll let you clean them after I win."
      # maya bored flip "Bitch, please. Hand that over and watch what a shitless win looks like."
      maya bored flip "Bitch, please. Hand that over and watch what a shitless win looks like.{space=-45}"
      mc "Good luck. You'll need it."
      window hide
      show maya hot_sauce_competition carolina_taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "She takes the bottle from me and douses her tongue with the fiery hell pepper."
      show maya hot_sauce_competition carolina_aftermath thinking with {"master": Dissolve(.5)}
      maya hot_sauce_competition carolina_aftermath thinking "Oh? I detect a hint of... is that semen?"
      maya hot_sauce_competition carolina_aftermath thinking "Yum. Delicious."
      mc "What? No, you don't!"
      show maya hot_sauce_competition carolina_aftermath confident with {"master": Dissolve(.5)}
      maya hot_sauce_competition carolina_aftermath confident "It's okay to like the sauce! No homo."
      "[maya] plays it perfectly cool, but even as she speaks, she removes her top."
      "As if it's the most casual thing in the world."
      "My mouth opens and then closes, and then opens again, but the words fail me."
      "She just stands there with her perky tits on display."
      "Her skin flushed, dewy beads of sweat subtly clinging to her."
      "The fire in my mouth is replaced with a fire somewhere much lower."
      maya hot_sauce_competition carolina_aftermath confident "What's the matter? Pepper got your tongue?"
      mc "Err, nope! I'm just preparing for the next one..."
      "...and whatever it is she plans on removing next."
      flora "Very well! Last sauce coming right up!"
      flora "Are you two ready?"
      maya hot_sauce_competition carolina_aftermath confident "Bring the heat, baby."
      mc "Hell yeah!"
    "Do your damndest":
      show flora excited at move_to(.25)
      show maya bored flip at move_to(.75)
      "And it's one of the biggest mistakes of this new life of mine."
      window hide
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      hide black onlayer screens with open_eyes
      pause 0.25
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      hide black onlayer screens with open_eyes
      window auto
      "The world seems to spin and blink around me."
      "I don't know why I agreed to this."
      "What was I even trying to prove?"
      "That I'm somehow better than [maya]?"
      "That I'm worthy?"
      window hide
      hide screen interface_hider
      show flora worried
      show maya skeptical_skirt flip
      show black
      show black onlayer screens zorder 100
      with close_eyes
      pause 0.5
      show black onlayer screens zorder 4
      $quest.maya_sauce.advance("aftermath")
      $maya.unequip("maya_dress")
      $set_dialog_mode("default_no_bg")
      "That's something the old me would've done."
      "The new me shouldn't be trying so hard to prove something to the world."
      flora worried "Hellooo? [mc]?"
      maya skeptical_skirt flip "I think he's going to faint."
      mc "N-no! I won't!"
      show black onlayer screens zorder 100
      pause 0.5
      hide black
      hide black onlayer screens
      show screen interface_hider
      with open_eyes
      window auto
      $set_dialog_mode("")
      mc "I'm... perfectly fine..."
      # maya skeptical_skirt flip "Are you, now? Because you just spewed a mouthful of sauce across the kitchen."
      maya skeptical_skirt flip "Are you, now? Because you just spewed a mouthful of sauce across{space=-10}\nthe kitchen."
      # "Huh? I don't remember losing consciousness... but I would definitely remember seeing [maya] take off her shirt..."
      "Huh? I don't remember losing consciousness... but I would definitely{space=-10}\nremember seeing [maya] take off her shirt..."
      "Yet she's standing there in just her bra and skirt, her face flushed..."
      "Maybe I did actually faint..."
      flora worried "She's right. You spit it out instantly, and then stumbled around."
      flora blush "[maya], however, took her turn and did it like a pro!"
      maya blush_skirt flip "And that's why I charge pro rates out on the streets!"
      "Fuck me... I can't believe I lost..."
      "Honestly, though... this whole contest was worth it just to see her standing there in her bra."
      "A faint sheen of sweat coating her pale skin."
      "Her breasts rising and falling with her increased breathing."
      # "The heat surely coursing through her and spiking her own heart rate."
      "The heat surely coursing through her and spiking her own heart rate.{space=-20}"
      jump quest_maya_sauce_sauces_defeated
  window hide
  $maya.unequip("maya_dress")
  show flora excited at Transform(xalign=.25)
  show maya bored_skirt flip at Transform(xalign=.75)
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora excited "Okay, this is the one!"
  flora excited "The sauce of all sauces!"
  flora excited "The pepper that will get your goat!"
  flora excited "The—"
  mc "Can we please just get on with it?"
  flora concerned "Where is your sense of pageantry?"
  maya confident_skirt flip "It must've died with his sense of style."
  mc "Hey! I'm just ready to kick your ass, is all."
  maya confident_skirt flip "Kiss my ass, you mean?"
  mc "You'd like that, wouldn't you? Especially once my lips are on fire."
  flora angry "Okay, that's enough!"
  flora angry "I don't want to hear about your gross fantasies."
  # flora confident "Instead, are you ready for the one and only... {i}Fahrenheit-551{/} hot sauce?!"
  flora confident "Instead, are you ready for the one and only... {i}Fahrenheit-551{/} hot sauce?!{space=-90}"
  flora confident "It contains actual kerosene!"
  flora confident "[mc]. Your move."
  # "A deep breath to steel my nerves and my bowels is all that I can afford."
  "A deep breath to steel my nerves and my bowels is all that I can afford.{space=-65}"
  "The bottle feels heavy in my hand."
  "This is a path where there is no turning back."
  "Life and choices, right?"
  "This is one I have to live with."
  "Death sauce straight to the tongue."
  mc "..."
  show flora confident at move_to("left")
  show maya confident_skirt flip at move_to("right")
  menu(side="middle"):
    extend ""
    "?mc.strength>=15@[mc.strength]/15|{image=stats str}|Transcend":
      show flora confident at move_to(.25)
      show maya confident_skirt flip at move_to(.75)
      "As soon as it hits, it tears right through me."
      "Like a current of superheated electricity."
      window hide
      hide screen interface_hider
      show maya hot_sauce_competition fahrenheit_taste
      show black
      show black onlayer screens zorder 100
      with close_eyes
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "My eyes burn, and my nose starts to run."
      "This heat is on a psychological level."
      "My mind reels as the desperation hits me."
      "It's like touching snow for too long, or holding your breath."
      "It's a primal sort of suffering."
      "..."
      "The only escape is to think of happy things."
      "Of [maya] topless. Of [flora] topless."
      "And somehow... it's working."
      show black onlayer screens zorder 100
      pause 0.5
      hide flora
      hide black
      hide black onlayer screens
      show screen interface_hider
      with open_eyes
      window auto
      $set_dialog_mode("")
      "Holy shit."
      "Am I dreaming, or...?"
      show maya hot_sauce_competition fahrenheit_aftermath with {"master": Dissolve(.5)}
      "[maya] stands there completely topless."
      "Her face is red, sweat dripping down her forehead."
      "She looks ready to explode."
      "Her small breasts heave as she pants."
      window hide
      play sound "fast_whoosh"
      hide screen interface_hider
      show maya sink bending_over
      show black
      show black onlayer screens zorder 100
      with Move((45, 0), (-45, 0), .10, bounce=True, repeat=True, delay=.275)
      pause 0.25
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "Then, she pushes me out of the way, those beautiful, perfect tits bouncing..."
      "...before running straight for the sink."
      show black onlayer screens zorder 100
      pause 0.5
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      "She bends over, and it's beautiful."
      window hide
      show maya sink spitting with vpunch
      window auto
      "In an instant, she retches and spits out the sauce."
      "Goddamn!"
      "That view right there has made me a winner already."
      flora "Well, I guess that does it..."
      flora "I'm shocked to say it, but [mc], you are the winner."
      mc "There's no need to sound so surprised."
      mc "But thank you, [flora]."
      # mc "Hey, [maya]? I guess I'll be signing you up for that talent contest, after all."
      mc "Hey, [maya]? I guess I'll be signing you up for that talent contest, after all.{space=-85}"
      maya sink looking_back "...!"
      "For a moment, it looks like she's about to cry, but maybe that's just the tears from the hot sauce."
      "She looks so pretty standing there all red-faced and vulnerable."
      "But after a moment of catching her breath, she finally straightens and comes back over to us."
      window hide
      $unlock_replay("maya_sauce_off")
      $unlock_replay("maya_defeat")
      $maya.unequip("maya_bra")
      show flora thinking at Transform(xalign=.25)
      show maya cringe_skirt at Transform(xalign=.75)
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      maya cringe_skirt "That's crazy..."
      maya cringe_skirt "I... I can't believe it..."
      "Music to my ears."
      "And soon, her voice will be, too."
      "..."
      # "Although, seeing her like this, I do regret not going for that blowjob..."
      "Although, seeing her like this, I do regret not going for that blowjob...{space=-25}"
      # "Red and flustered. Her nipples erect and pink against her supple breasts."
      "Red and flustered. Her nipples erect and pink against her supple breasts.{space=-100}"
      maya dramatic_skirt "Ugh... I need a cold shower..."
      window hide
      show flora thinking:
        ease 1.0 xalign 0.5
      show maya dramatic_skirt behind flora at disappear_to_left
      show flora worried with Dissolve(.5)
      window auto
      # "Oh, man. I have never seen her so defeated and, ultimately, humiliated."
      "Oh, man. I have never seen her so defeated and, ultimately, humiliated.{space=-60}"
      "She really thought she would beat me, didn't she?"
      "There's something so completely satisfying about putting her in her place..."
      "God, this victory is making me hard."
      flora laughing "Congrats, [mc]! You did it!"
      flora laughing "I can't believe you actually pulled it off."
      flora flirty "I must say, I am kind of impressed..."
      mc "Do you really mean it?"
      flora flirty "Well, it wasn't the Olympics or anything, but still..."
      "It's rare to see [flora] look at me with something close to admiration."
      "It makes my chest swell, and the win feel that much sweeter."
      flora blush "Anyway, I should probably check on [maya] and then clean this up."
      window hide
      show flora blush at disappear_to_left
      pause 0.5
      window auto
      "Whew! Winning really does feel good."
      "It's an unusual sensation. Uplifting, in a way."
      # "And I got to see [maya] in her most compromised, vulnerable state yet."
      "And I got to see [maya] in her most compromised, vulnerable state yet.{space=-30}"
      "I can't wait to see her blushing on that stage next..."
      window hide
      $quest.maya_sauce.finish()
      return
    "You've come this far...":
      show flora confident at move_to(.25)
      show maya confident_skirt flip at move_to(.75)
      "So far, I don't—"
      window hide
      play sound "falling_thud"
      hide screen interface_hider
      show maya looking_down concerned
      show black
      show black onlayer screens zorder 100
      show teary_eyes2 as half_open_eyes:
        ysize 1080
      with Move((0,30), (0,-30), .10, bounce=True, repeat=True, delay=.275)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "The hit knocks me out cold."
      "For a moment, I drift over a landscape of burning trees."
      "Fires roaring down the hills."
      "Ashes and smoke billow into the dark sky."
      show black onlayer screens zorder 100
      pause 0.5
      $game.ui.hide_hud = True
      hide flora
      hide black
      hide black onlayer screens
      with open_eyes
      window auto
      $set_dialog_mode("")
      "Then, my senses start to return, and I realize that this is one of the biggest mistakes of my life."
      "The room spins around me as the heat consumes me from within."
      window hide
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      hide black onlayer screens with open_eyes
      pause 0.25
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      hide black onlayer screens with open_eyes
      window auto
      "My old life seems to flash in rapid bursts behind my eyelids."
      "Alienating everyone."
      "Ending up alone and lonely."
      "I'm reminded of all of my regrets, big and small."
      # "I choke on them, like I choke on this fire scorching through my mouth and throat."
      "I choke on them, like I choke on this fire scorching through my mouth{space=-35}\nand throat."
      flora "[mc]! Talk to me!!"
      flora "Oh, god. We killed him."
      mc "Mmm...?"
      window hide
      $game.ui.hide_hud = False
      show expression Flatten("maya looking_down sarcastic1") as workaround behind half_open_eyes:
        alpha 0.0
        pause 0.25
        ease 0.75 alpha 1.0
      hide half_open_eyes with open_eyes
      $renpy.transition(Dissolve(.25))
      show screen interface_hider
      window auto show
      show maya looking_down sarcastic1 with {"master": Dissolve(.5)}
      maya looking_down sarcastic1 "That may be the most tragic thing I have ever seen."
      maya looking_down sarcastic1 "It even tops cult demon sacrifice tragic."
      hide workaround
      "Huh? Am I still dreaming?"
      # "Seeing [maya] in front of me, completely naked from the top up, I must be, right?"
      "Seeing [maya] in front of me, completely naked from the top up,\nI must be, right?"
      maya looking_down squeeze3 "Maybe this will wake you..."
      window hide
      pause 0.125
      show maya looking_down squeeze2 with Dissolve(.125)
      show maya looking_down squeeze1 with Dissolve(.125)
      pause 0.25
      show maya looking_down squeeze2 with Dissolve(.0625)
      show maya looking_down squeeze3 with Dissolve(.0625)
      show maya looking_down squeeze4 with Dissolve(.0625)
      show maya looking_down squeeze3 with Dissolve(.125)
      pause 0.5
      show maya looking_down squeeze2 with Dissolve(.125)
      show maya looking_down squeeze1 with Dissolve(.125)
      pause 0.25
      show maya looking_down squeeze2 with Dissolve(.0625)
      show maya looking_down squeeze3 with Dissolve(.0625)
      show maya looking_down squeeze4 with Dissolve(.0625)
      show maya looking_down squeeze3 with Dissolve(.125)
      pause 0.5
      show maya looking_down squeeze2 with Dissolve(.125)
      show maya looking_down squeeze1 with Dissolve(.125)
      pause 0.25
      show maya looking_down squeeze2 with Dissolve(.0625)
      show maya looking_down squeeze3 with Dissolve(.0625)
      show maya looking_down squeeze4 with Dissolve(.0625)
      show maya looking_down squeeze3 with Dissolve(.125)
      pause 0.25
      window auto
      # "She squeezes her tits together and then shrugs, letting them bounce back to their unbelievably perky state."
      "She squeezes her tits together and then shrugs, letting them bounce{space=-20}\nback to their unbelievably perky state."
      mc "Err, what happened?"
      maya looking_down confident "I took my turn while you were moaning and squirming on the floor."
      maya looking_down confident "It was pretty hot. So, I decided to cool down."
      show maya looking_down sarcastic2 with {"master": Dissolve(.5)}
      maya looking_down sarcastic2 "The sauce, I mean. Definitely not your moaning."
      mc "Ugh, god... just let me die here..."
      "Even a topless [maya] is little consolation for that absolute embarrassment..."
      $unlock_replay("maya_consolation")

label quest_maya_sauce_sauces_defeated:
  if renpy.showing("maya bored flip"):
    maya bored flip "Anyway, you have learned a hard lesson today, [mc]."
  elif (renpy.showing("maya hot_sauce_competition wench_aftermath bored")
  or renpy.showing("maya hot_sauce_competition bust_a_ghost_aftermath confident")
  or renpy.showing("maya looking_down sarcastic2")):
    window hide
    $maya.equip("maya_bra")
    $maya.equip("maya_dress")
    $maya.equip("maya_jacket")
    show flora blush at Transform(xalign=.25)
    show maya bored flip at Transform(xalign=.75)
    show black onlayer screens zorder 100
    with Dissolve(3.0)
    $quest.maya_sauce.advance("aftermath")
    pause 1.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    maya bored flip "You have learned a hard lesson today, [mc]."
  elif renpy.showing("maya blush_skirt flip"):
    window hide
    pause 0.25
    $maya.equip("maya_dress")
    show maya blush flip with Dissolve(.5)
    pause 0.5
    $maya.equip("maya_jacket")
    pause 0.75
    window auto show
    show maya bored flip with {"master": Dissolve(.5)}
    maya bored flip "You have learned a hard lesson today, [mc]."
  mc "Don't pour hot sauce directly onto your tongue?"
  maya confident flip "Don't challenge me, because you're guaranteed to lose."
  maya confident flip "I'll begin moving into your room, posthaste."
  mc "Oh, well. A deal's a deal, I suppose."
  maya blush flip "Just like I always tell the sex demons."
  mc "..."
  flora blush "Come on, [maya]! Let's go move his stuff out!"
  flora blush "We'll even get you the spare bed down from the attic!"
  # maya blush flip "My very own bed? I'm already excited to hear what noises it can make!"
  maya blush flip "My very own bed? I'm already excited to hear what noises it can make!{space=-50}"
  mc "Seriously? You could at least pretend to be sad for me, [flora]."
  flora laughing "Sorry, sorry! Girl power and all that, you know?"
  maya blush flip "Bringing down the system, one cock at a time!"
  window hide
  show flora laughing at disappear_to_left
  show maya blush flip at disappear_to_left
  pause 0.5
  window auto
  "They say that the dildo of consequences rarely arrives lubed."
  "I guess I have no one to blame but myself..."
  $unlock_replay("maya_sauce_off")
  $quest.maya_sauce["bedroom_taken_over"] = True
  $flora.outfit = flora["outfit_stamp"]
  return

label quest_maya_sauce_aftermath:
  "You have got to be kidding me."
  "They moved my whole bed out here?!"
  "I guess I do need somewhere to sleep... but the hallway?"
  "God, I need my privacy."
  "I can't jack off with [jo]'s bedroom just on the other side of that door..."
  "..."
  "I know a deal's a deal, but you know what?"
  "I'm signing [maya] up for that contest and taking my ass to Cabo next year."
  "She'll thank me for it after everyone tells her how great she is, anyhow."
  "That's it. It's decided."
  window hide
  $quest.maya_sauce.finish("done_but_bedroom_taken_over")
  return

label quest_maya_sauce_aftermath_home_hall_door_bedroom:
  "What the hell? It's locked!"
  "And worse, [maya] has clearly marked her territory with this hideous cat poster..."
  return
