init python:
  class Character_maya(BaseChar):
    notify_level_changed=True

    default_name="Maya"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="maya contact_icon"

    default_outfit_slots=["hat","jacket","dress","bra","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("maya_hair_towel")
      self.add_item("maya_armor")
      self.add_item("maya_jacket")
      self.add_item("maya_dress")
      self.add_item("maya_towel")
      self.add_item("maya_bra")
      self.add_item("maya_panties")
      self.equip("maya_jacket")
      self.equip("maya_dress")
      self.equip("maya_bra")
      self.equip("maya_panties")

    def ai(self):
      ## Daily Schedule ##
      self.location = self.activity = None

      if game.season > 1 and quest.maya_witch > "wait":
        if game.hour in (7,8) and quest.maya_quixote.finished:
          self.location = "home_kitchen"
          self.activity = "staring"
        elif game.hour in (7,8):
          self.location = "school_first_hall"
          self.activity = "sitting"
        elif game.hour in (9,10):
          self.location = "school_gym"
          self.activity = "sitting"
        elif game.hour in (11,12):
          self.location = "school_cafeteria"
          self.activity = "kicking"
        elif game.hour in (13,14):
          self.location = "school_computer_room"
          self.activity = "sitting"
        elif game.hour in (15,16):
          self.location = "school_first_hall_west"
          self.activity = "sitting"
        elif game.hour in (17,18):
          self.location = "school_homeroom"
          self.activity = "sitting"
        elif game.hour == 19 and quest.maya_quixote.finished:
          self.location = "home_hall"
          self.activity = "trophy_lift"

      ## Alternative Locations ##
      self.alternative_locations = {}

      if quest.maya_quixote == "xCube":
        self.alternative_locations["home_bedroom"] = "sitting"
      elif quest.maya_quixote == "shower":
        self.alternative_locations["home_bathroom"] = "drawing"

      if quest.maya_sauce in ("classes","dinner"):
        self.alternative_locations["school_homeroom"] = "sitting"
      elif quest.maya_sauce == "preparation" and 6 < game.hour < 19:
        self.alternative_locations["home_hall"] = "trophy_lift"
      elif quest.maya_sauce == "sauces":
        self.alternative_locations["home_kitchen"] = "staring"

      ## Forced Locations ##
      if maya["at_none_this_scene"] or maya["at_none_now"] or maya["at_none_today"]:
        self.location = self.activity = None
        self.alternative_locations = {}
        return

      if quest.fall_in_newfall.in_progress and quest.fall_in_newfall < "stick_around":
        self.location,self.activity = "school_homeroom","sitting"
        self.alternative_locations = {}
        return

      if quest.fall_in_newfall >= "stick_around" and not quest.maya_witch.started:
        self.location,self.activity = "school_first_hall","sitting"
        self.alternative_locations = {}
        return
      elif quest.maya_witch == "english_class":
        self.location,self.activity = "school_first_hall_west","sitting"
        self.alternative_locations = {}
        return
      elif quest.maya_witch == "gym_class":
        self.location,self.activity = "school_gym","sitting"
        self.alternative_locations = {}
        return
      elif quest.maya_witch in ("computer_class","locked","phone_call","wait"):
        self.location,self.activity = "school_computer_room","sitting"
        self.alternative_locations = {}
        return

      if quest.maya_spell == "maya":
        self.location,self.activity = "school_cafeteria","kicking"
        self.alternative_locations = {}
        return

      if quest.maya_quixote == "power_outage":
        self.location,self.activity = "home_hall","trophy_lift"
        self.alternative_locations = {}
        return
      elif quest.maya_quixote in ("attic","board_game"):
        self.location,self.activity = "home_bedroom","sitting"
        self.alternative_locations = {}
        return

      if quest.lindsey_voluntary == "dream":
        self.location = self.activity = None
        self.alternative_locations = {}
        return

      if quest.mrsl_bot == "dream":
        self.location = self.activity = None
        self.alternative_locations = {}
        return
      elif quest.mrsl_bot == "diversion":
        self.location,self.activity = "school_ground_floor","sitting"
        self.alternative_locations = {}
        return

      if quest.maya_sauce in ("siren_song","breakfast"):
        self.location = self.activity = None
        self.alternative_locations = {}
        return

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("maya avatar "+state,True):
          return "maya avatar "+state
      rv=[]

      if state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_hair_towel" in state:
          rv.append(("maya avatar body1_water_droplets",121,226))
        else:
          rv.append(("maya avatar body1",121,216))
        rv.append(("maya avatar face_annoyed",178,312))
        rv.append(("maya avatar b1arm1_n",1,514))
        if "_panties" in state:
          rv.append(("maya avatar b1panties",114,861))
        if "_bra" in state:
          rv.append(("maya avatar b1bra",151,483))
        if "_dress" in state:
          rv.append(("maya avatar b1dress",80,476))
        elif "_towel" in state:
          rv.append(("maya avatar b1towel",111,601))
          rv.append(("maya avatar b1arm1_towel",1,514))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1water_droplets1",126,521))
        if "_jacket" in state:
          rv.append(("maya avatar b1jacket",92,451))
          rv.append(("maya avatar b1arm1_c",1,514))
        elif "_towel" not in state:
          rv.append(("maya avatar b1arm1_n_jacketless",1,514))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1hair_towel",147,162))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_hair_towel" in state:
          rv.append(("maya avatar body1_water_droplets",121,226))
        else:
          rv.append(("maya avatar body1",121,216))
        rv.append(("maya avatar face_eyeroll",178,316))
        if "_hands_down" in state:
          rv.append(("maya avatar b1arm1_n",1,514))
        else:
          rv.append(("maya avatar b1arm2_n",98,514))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1water_droplets2",131,521))
        if "_panties" in state:
          rv.append(("maya avatar b1panties",114,861))
        if "_bra" in state:
          rv.append(("maya avatar b1bra",151,483))
        if "_dress" in state:
          rv.append(("maya avatar b1dress",80,476))
          if "_jacket" not in state:
            if "_hands_down" in state:
              rv.append(("maya avatar b1arm1_n_jacketless",1,514))
            else:
              rv.append(("maya avatar b1arm2_n_jacketless",98,514))
        elif "_towel" in state:
          rv.append(("maya avatar b1towel",111,601))
        if "_jacket" in state:
          rv.append(("maya avatar b1jacket",92,451))
          if "_hands_down" in state:
            rv.append(("maya avatar b1arm1_c",1,514))
          else:
            rv.append(("maya avatar b1arm2_c",66,521))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1hair_towel",147,162))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_hair_towel" in state:
          rv.append(("maya avatar body1_water_droplets",121,226))
        else:
          rv.append(("maya avatar body1",121,216))
        rv.append(("maya avatar face_neutral",178,311))
        rv.append(("maya avatar b1arm2_n",98,514))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1water_droplets2",131,521))
        if "_panties" in state:
          rv.append(("maya avatar b1panties",114,861))
        if "_bra" in state:
          rv.append(("maya avatar b1bra",151,483))
        if "_dress" in state:
          rv.append(("maya avatar b1dress",80,476))
          if "_jacket" not in state:
            rv.append(("maya avatar b1arm2_n_jacketless",98,514))
        elif "_towel" in state:
          rv.append(("maya avatar b1towel",111,601))
        if "_jacket" in state:
          rv.append(("maya avatar b1jacket",92,451))
          rv.append(("maya avatar b1arm2_c",66,521))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1hair_towel",147,162))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_hair_towel" in state:
          rv.append(("maya avatar body1_water_droplets",121,226))
        else:
          rv.append(("maya avatar body1",121,216))
        rv.append(("maya avatar face_smile",178,322))
        if "_hands_in_pockets" in state:
          rv.append(("maya avatar b1arm2_n",98,514))
        else:
          rv.append(("maya avatar b1arm1_n",1,514))
        if "_panties" in state:
          rv.append(("maya avatar b1panties",114,861))
        if "_bra" in state:
          rv.append(("maya avatar b1bra",151,483))
        if "_dress" in state:
          rv.append(("maya avatar b1dress",80,476))
          if "_hands_in_pockets" in state and "_jacket" not in state:
            rv.append(("maya avatar b1arm2_n_jacketless",98,514))
        elif "_towel" in state:
          rv.append(("maya avatar b1towel",111,601))
          rv.append(("maya avatar b1arm1_towel",1,514))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1water_droplets1",126,521))
        if "_jacket" in state:
          rv.append(("maya avatar b1jacket",92,451))
          if "_hands_in_pockets" in state:
            rv.append(("maya avatar b1arm2_c",66,521))
          else:
            rv.append(("maya avatar b1arm1_c",1,514))
        elif all(suffix not in state for suffix in ("_hands_in_pockets","_towel")):
          rv.append(("maya avatar b1arm1_n_jacketless",1,514))
        if "_hair_towel" in state:
          rv.append(("maya avatar b1hair_towel",147,162))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_hair_towel" in state:
          rv.append(("maya avatar body2_water_droplets",123,232))
        else:
          rv.append(("maya avatar body2",113,219))
        rv.append(("maya avatar face_afraid",198,323))
        if "_soapy" in state:
          rv.append(("maya avatar b2soapy",125,224))
        if "_panties" in state:
          rv.append(("maya avatar b2panties",111,862))
        if "_bra" in state:
          rv.append(("maya avatar b2bra",164,474))
        if "_dress" in state:
          rv.append(("maya avatar b2dress",85,469))
        elif "_towel" in state:
          rv.append(("maya avatar b2towel",107,575))
        rv.append(("maya avatar b2arm1_n",127,486))
        if "_soda" in state:
          rv.append(("maya avatar b2soda_n",270,439))
        else:
          rv.append(("maya avatar b2hand1_n",240,398))
        if "_hair_towel" in state:
          rv.append(("maya avatar b2water_droplets",158,497))
        if "_jacket" in state:
          rv.append(("maya avatar b2jacket",102,443))
          rv.append(("maya avatar b2arm1_c",124,484))
          if "_soda" in state:
            rv.append(("maya avatar b2soda_c",270,439))
          else:
            rv.append(("maya avatar b2hand1_c",240,398))
        elif "_armor" in state:
          rv.append(("maya avatar b2armor",95,466))
          rv.append(("maya avatar b2arm1_armor",127,522))
          rv.append(("maya avatar b2hand1_n",240,398))
        if "_soda" in state:
          rv.append(("maya avatar b2soda_tab",320,429))
          if "_spraying" in state:
            rv.append(("maya avatar b2soda_spraying",139,148))
        if "_hair_towel" in state:
          rv.append(("maya avatar b2hair_towel",154,168))

      elif state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_hair_towel" in state:
          rv.append(("maya avatar body2_water_droplets",123,232))
        else:
          rv.append(("maya avatar body2",113,219))
        rv.append(("maya avatar face_concerned",198,328))
        if "_panties" in state:
          rv.append(("maya avatar b2panties",111,862))
        if "_bra" in state:
          rv.append(("maya avatar b2bra",164,474))
        if "_dress" in state:
          rv.append(("maya avatar b2dress",85,469))
        elif "_towel" in state:
          rv.append(("maya avatar b2towel",107,575))
        rv.append(("maya avatar b2arm1_n",127,486))
        rv.append(("maya avatar b2hand2_n",281,414))
        if "_hair_towel" in state:
          rv.append(("maya avatar b2water_droplets",158,497))
        if "_jacket" in state:
          rv.append(("maya avatar b2jacket",102,443))
          rv.append(("maya avatar b2arm1_c",124,484))
          rv.append(("maya avatar b2hand2_c",281,414))
        elif "_armor" in state:
          rv.append(("maya avatar b2armor",95,466))
          rv.append(("maya avatar b2arm1_armor",127,522))
          rv.append(("maya avatar b2hand2_n",281,414))
        if "_hair_towel" in state:
          rv.append(("maya avatar b2hair_towel",154,168))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body2",113,219))
        if "_half_eaten_pepper" in state:
          rv.append(("maya avatar face_flirty_chewing",191,323))
        else:
          rv.append(("maya avatar face_flirty",192,323))
        if "_panties" in state:
          rv.append(("maya avatar b2panties",111,862))
        if "_bra" in state:
          rv.append(("maya avatar b2bra",164,474))
        if "_dress" in state:
          rv.append(("maya avatar b2dress",85,469))
        elif "_towel" in state:
          rv.append(("maya avatar b2towel",107,575))
        rv.append(("maya avatar b2arm1_n",127,486))
        if "_soda" in state:
          rv.append(("maya avatar b2soda_n",270,439))
        elif "_half_eaten_pepper" in state:
          rv.append(("maya avatar b2half_eaten_pepper_n",325,392))
        elif "_pepper" in state:
          rv.append(("maya avatar b2pepper_n",305,392))
        else:
          rv.append(("maya avatar b2hand3_n",224,434))
        if "_jacket" in state:
          rv.append(("maya avatar b2jacket",102,443))
          rv.append(("maya avatar b2arm1_c",124,484))
          if "_soda" in state:
            rv.append(("maya avatar b2soda_c",270,439))
          elif "_half_eaten_pepper" in state:
            rv.append(("maya avatar b2half_eaten_pepper_c",325,392))
          elif "_pepper" in state:
            rv.append(("maya avatar b2pepper_c",305,392))
          else:
            rv.append(("maya avatar b2hand3_c",224,434))
        elif "_armor" in state:
          rv.append(("maya avatar b2armor",95,466))
          rv.append(("maya avatar b2arm1_armor",127,522))
          rv.append(("maya avatar b2hand3_n",224,434))
        if "_soda" in state:
          rv.append(("maya avatar b2soda_tab",320,429))

      elif state.startswith("kiss"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body2",113,219))
        rv.append(("maya avatar face_kiss",192,323))
        if "_panties" in state:
          rv.append(("maya avatar b2panties",111,862))
        if "_bra" in state:
          rv.append(("maya avatar b2bra",164,474))
        if "_dress" in state:
          rv.append(("maya avatar b2dress",85,469))
        elif "_towel" in state:
          rv.append(("maya avatar b2towel",107,575))
        rv.append(("maya avatar b2arm1_n",127,486))
        if "_pepper" in state:
          rv.append(("maya avatar b2pepper_n",305,392))
        if "_jacket" in state:
          rv.append(("maya avatar b2jacket",102,443))
          rv.append(("maya avatar b2arm1_c",124,484))
        elif "_armor" in state:
          rv.append(("maya avatar b2armor",95,466))
          rv.append(("maya avatar b2arm1_armor",127,522))
        if "_pepper" in state:
          rv.append(("maya avatar b2pepper_c",305,392))
        else:
          rv.append(("maya avatar b2hand4",289,499))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body2",113,219))
        rv.append(("maya avatar face_thinking",198,324))
        if "_panties" in state:
          rv.append(("maya avatar b2panties",111,862))
        if "_bra" in state:
          rv.append(("maya avatar b2bra",164,474))
        if "_dress" in state:
          rv.append(("maya avatar b2dress",85,469))
        elif "_towel" in state:
          rv.append(("maya avatar b2towel",107,575))
        rv.append(("maya avatar b2arm1_n",127,486))
        if "_soda" in state:
          rv.append(("maya avatar b2soda_n",270,439))
        elif "_pepper" in state:
          rv.append(("maya avatar b2pepper_n",305,392))
        else:
          rv.append(("maya avatar b2hand2_n",281,414))
        if "_jacket" in state:
          rv.append(("maya avatar b2jacket",102,443))
          rv.append(("maya avatar b2arm1_c",124,484))
          if "_soda" in state:
            rv.append(("maya avatar b2soda_c",270,439))
          elif "_pepper" in state:
            rv.append(("maya avatar b2pepper_c",305,392))
          else:
            rv.append(("maya avatar b2hand2_c",281,414))
        elif "_armor" in state:
          rv.append(("maya avatar b2armor",95,466))
          rv.append(("maya avatar b2arm1_armor",127,522))
          rv.append(("maya avatar b2hand2_n",281,414))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_soaked" in state:
          rv.append(("maya avatar body3_soaked",40,222))
        else:
          rv.append(("maya avatar body3",40,222))
        rv.append(("maya avatar face_cringe",267,303))
        if "_panties" in state:
          rv.append(("maya avatar b3panties",114,849))
        if "_bra" in state:
          rv.append(("maya avatar b3bra",140,474))
        if "_skirt" in state:
          rv.append(("maya avatar b3skirt",71,667))
        elif "_dress" in state:
          if "_soaked" in state:
            rv.append(("maya avatar b3dress_soaked",71,462))
          else:
            rv.append(("maya avatar b3dress",71,462))
        if "_jacket" in state and not self.equipped_item("maya_jacket"):
          rv.append(("maya avatar b3jacket_off",13,649))
        if "_soaked" in state:
          rv.append(("maya avatar b3soda1_n",194,228))
        elif "_pepper" in state:
          rv.append(("maya avatar b3pepper1_n",187,228))
        else:
          rv.append(("maya avatar b3arm1_n",200,228))
        if "_jacket" in state and self.equipped_item("maya_jacket"):
          if "_soaked" in state:
            rv.append(("maya avatar b3jacket_soaked",17,307))
            rv.append(("maya avatar b3soda1_c",194,228))
          else:
            rv.append(("maya avatar b3jacket",17,307))
            if "_pepper" in state:
              rv.append(("maya avatar b3pepper1_c",187,228))
            else:
              rv.append(("maya avatar b3arm1_c",200,228))
        elif "_armor" in state:
          rv.append(("maya avatar b3armor",83,303))
          rv.append(("maya avatar b3arm1_armor",200,228))

      elif state.startswith("dramatic"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        if "_soaked" in state:
          rv.append(("maya avatar body3_soaked",40,222))
        else:
          rv.append(("maya avatar body3",40,222))
        rv.append(("maya avatar face_dramatic",267,296))
        if "_panties" in state:
          rv.append(("maya avatar b3panties",114,849))
        if "_bra" in state:
          rv.append(("maya avatar b3bra",140,474))
        if "_skirt" in state:
          rv.append(("maya avatar b3skirt",71,667))
        elif "_dress" in state:
          if "_soaked" in state:
            rv.append(("maya avatar b3dress_soaked",71,462))
          else:
            rv.append(("maya avatar b3dress",71,462))
        if "_soaked" in state:
          rv.append(("maya avatar b3soda2_n",194,216))
        elif "_pepper" in state:
          rv.append(("maya avatar b3pepper2_n",234,216))
        else:
          rv.append(("maya avatar b3arm2_n",234,216))
        if "_jacket" in state:
          if "_soaked" in state:
            rv.append(("maya avatar b3jacket_soaked",17,307))
            rv.append(("maya avatar b3soda2_c",194,216))
          else:
            rv.append(("maya avatar b3jacket",17,307))
            if "_pepper" in state:
              rv.append(("maya avatar b3pepper2_c",234,216))
            else:
              rv.append(("maya avatar b3arm2_c",234,216))
        elif "_armor" in state:
          rv.append(("maya avatar b3armor",83,303))
          rv.append(("maya avatar b3arm2_armor",234,216))

      elif state.startswith("sarcastic"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body3",40,222))
        rv.append(("maya avatar face_sarcastic",267,306))
        if "_panties" in state:
          rv.append(("maya avatar b3panties",114,849))
        if "_bra" in state:
          rv.append(("maya avatar b3bra",140,474))
        if "_dress" in state:
          rv.append(("maya avatar b3dress",71,462))
        if "_pepper" in state:
          rv.append(("maya avatar b3pepper1_n",187,228))
        else:
          rv.append(("maya avatar b3arm1_n",200,228))
        if "_jacket" in state:
          rv.append(("maya avatar b3jacket",17,307))
          if "_pepper" in state:
            rv.append(("maya avatar b3pepper1_c",187,228))
          else:
            rv.append(("maya avatar b3arm1_c",200,228))
        elif "_armor" in state:
          rv.append(("maya avatar b3armor",83,303))
          rv.append(("maya avatar b3arm1_armor",200,228))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body4",54,227))
        rv.append(("maya avatar face_blush",207,296))
        if "_panties" in state:
          rv.append(("maya avatar b4panties",186,854))
        if "_bra" in state:
          rv.append(("maya avatar b4bra",200,461))
        if "_skirt" in state:
          rv.append(("maya avatar b4skirt",130,678))
        elif "_dress" in state:
          rv.append(("maya avatar b4dress",130,455))
        rv.append(("maya avatar b4arm1_n",187,657))
        if "_jacket" in state:
          rv.append(("maya avatar b4jacket",135,436))
          rv.append(("maya avatar b4arm1_c",169,656))

      elif state.startswith("bored"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body4",54,227))
        rv.append(("maya avatar face_bored",206,296))
        if "_panties" in state:
          rv.append(("maya avatar b4panties",186,854))
        if "_bra" in state:
          rv.append(("maya avatar b4bra",200,461))
        if "_skirt" in state:
          rv.append(("maya avatar b4skirt",130,678))
        elif "_dress" in state:
          rv.append(("maya avatar b4dress",130,455))
        rv.append(("maya avatar b4arm2_n",179,609))
        if "_jacket" in state:
          rv.append(("maya avatar b4jacket",135,436))
          rv.append(("maya avatar b4arm2_c",161,609))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body4",54,227))
        rv.append(("maya avatar face_confident",207,297))
        if "_panties" in state:
          rv.append(("maya avatar b4panties",186,854))
        if "_bra" in state:
          rv.append(("maya avatar b4bra",200,461))
        if "_skirt" in state:
          rv.append(("maya avatar b4skirt",130,678))
        elif "_dress" in state:
          rv.append(("maya avatar b4dress",130,455))
        if "_hands_down" in state:
          rv.append(("maya avatar b4arm1_n",187,657))
        else:
          rv.append(("maya avatar b4arm2_n",179,609))
        if "_jacket" in state:
          rv.append(("maya avatar b4jacket",135,436))
          if "_hands_down" in state:
            rv.append(("maya avatar b4arm1_c",169,656))
          else:
            rv.append(("maya avatar b4arm2_c",161,609))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body4",54,227))
        rv.append(("maya avatar face_sad",207,297))
        if "_panties" in state:
          rv.append(("maya avatar b4panties",186,854))
        if "_bra" in state:
          rv.append(("maya avatar b4bra",200,461))
        if "_dress" in state:
          rv.append(("maya avatar b4dress",130,455))
        rv.append(("maya avatar b4arm1_n",187,657))
        if "_jacket" in state:
          rv.append(("maya avatar b4jacket",135,436))
          rv.append(("maya avatar b4arm1_c",169,656))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((562,1080))
        rv.append(("maya avatar body4",54,227))
        rv.append(("maya avatar face_skeptical",207,296))
        if "_panties" in state:
          rv.append(("maya avatar b4panties",186,854))
        if "_bra" in state:
          rv.append(("maya avatar b4bra",200,461))
        if "_skirt" in state:
          rv.append(("maya avatar b4skirt",130,678))
        elif "_dress" in state:
          rv.append(("maya avatar b4dress",130,455))
        rv.append(("maya avatar b4arm2_n",179,609))
        if "_jacket" in state:
          rv.append(("maya avatar b4jacket",135,436))
          rv.append(("maya avatar b4arm2_c",161,609))

      elif state.startswith("piano_peek"):
        rv.append((1920,1080))
        rv.append(("maya avatar events piano_peek background",0,0))
        if "mrsl" in state:
          rv.append(("maya avatar events piano_peek mrsl_body",925,0))
          rv.append(("maya avatar events piano_peek mrsl_clothes",1143,89))
        if "empty" not in state:
          rv.append(("maya avatar events piano_peek maya_body",737,18))
          rv.append(("maya avatar events piano_peek maya_clothes",914,70))
        rv.append(("maya avatar events piano_peek foreground",204,0))

      elif state.startswith("music_class_peek"):
        rv.append((1920,1080))
        rv.append(("maya avatar events music_class_peek background",0,0))
        if "empty" not in state:
          rv.append(("maya avatar events music_class_peek maya_body",381,205))
          rv.append(("maya avatar events music_class_peek maya_underwear",707,319))
          rv.append(("maya avatar events music_class_peek maya_clothes",681,304))
          rv.append(("maya avatar events music_class_peek isabelle_body",547,155))
          rv.append(("maya avatar events music_class_peek isabelle_underwear",739,303))
          rv.append(("maya avatar events music_class_peek isabelle_clothes",711,158))
        rv.append(("maya avatar events music_class_peek foreground",0,0))

      elif state.startswith("fire"):
        rv.append((1920,1080))
        if "_extinguished" in state:
          rv.append(("maya avatar events fire background1",0,0))
          if "mc" in state:
            rv.append(("maya avatar events fire mc_pee1",687,395))
            rv.append(("maya avatar events fire mc_body1",342,0))
          else:
            rv.append(("maya avatar events fire maya_body1",571,70))
            rv.append(("maya avatar events fire maya_underwear1",630,295))
            rv.append(("maya avatar events fire maya_dress1",584,295))
            rv.append(("maya avatar events fire maya_jacket1",619,202))
        else:
          rv.append(("maya avatar events fire background2",0,0))
          if "mc" in state:
            rv.append(("maya avatar events fire mc_pee2",687,395))
            rv.append(("maya avatar events fire mc_body2",342,0))
          else:
            rv.append(("maya avatar events fire maya_body2",571,70))
            rv.append(("maya avatar events fire maya_underwear2",630,295))
            rv.append(("maya avatar events fire maya_dress2",584,295))
            rv.append(("maya avatar events fire maya_jacket2",619,202))
        if "_spread" in state:
          rv.append(("maya avatar events fire fire1",771,0))
        elif "_extinguished" not in state:
          rv.append(("maya avatar events fire fire2",742,0))

      elif state.startswith("pentagram_drawing"):
        rv.append((1920,1080))
        rv.append(("maya avatar events pentagram_drawing background",0,0))
        rv.append(("maya avatar events pentagram_drawing maya_body",0,98))
        rv.append(("maya avatar events pentagram_drawing maya_underwear",232,265))
        rv.append(("maya avatar events pentagram_drawing maya_clothes",0,260))

      elif state.startswith("soap_attack"):
        rv.append((1920,1080))
        if state.endswith("home_bathroom"):
          rv.append(("maya avatar events soap_attack home_bathroom miscellaneous",0,0))
          if not home_bathroom["dirty_clothes_taken"]:
            rv.append(("maya avatar events soap_attack home_bathroom laundry",529,801))
          if not home_bathroom["tissues_taken_today"]:
            rv.append(("maya avatar events soap_attack home_bathroom tissues",1546,570))
          if quest.clubroom_access.finished and not home_bathroom["got_pocket_mirror"]:
            rv.append(("maya avatar events soap_attack home_bathroom pocket_mirror",1360,479))
          if home_bathroom["dollar1_spawned_today"] == True and not home_bathroom["dollar1_taken_today"]:
            rv.append(("maya avatar events soap_attack home_bathroom dollar1",819,533))
          if home_bathroom["dollar2_spawned_today"] == True and not home_bathroom["dollar2_taken_today"]:
            rv.append(("maya avatar events soap_attack home_bathroom dollar2",1467,33))
          rv.append(("maya avatar events soap_attack home_bathroom steam_n_fog",0,0))
          rv.append(("maya avatar events soap_attack home_bathroom maya_afraid",758,181))
        elif state.endswith("home_hall"):
          rv.append(("maya avatar events soap_attack home_hall miscellaneous1",0,0))
          if not home_hall["table_taken"]:
            rv.append(("maya avatar events soap_attack home_hall table",1310,512))
            if home_hall["note"]:
              rv.append(("maya avatar events soap_attack home_hall note",1371,520))
            if not home_hall["lamp_taken"]:
              rv.append(("maya avatar events soap_attack home_hall lamp",1341,410))
          else:
            rv.append(("maya avatar events soap_attack home_hall table_back",650,445))
            if not home_hall["lamp_taken"]:
              rv.append(("maya avatar events soap_attack home_hall lamp_ground",1371,599))
          if not home_hall["umbrella_taken"]:
            rv.append(("maya avatar events soap_attack home_hall black_umbrella",592,420))
          rv.append(("maya avatar events soap_attack home_hall miscellaneous2",0,87))
          if home_hall["shoebox"] and not home_hall["shoebox_taken"]:
            rv.append(("maya avatar events soap_attack home_hall shoebox",1488,785))
          if not home_hall["machete_taken"]:
            rv.append(("maya avatar events soap_attack home_hall sword",688,373))
          if home_hall["dollar1_spawned_today"] == True and not home_hall["dollar1_taken_today"]:
            rv.append(("maya avatar events soap_attack home_hall dollar1",1480,694))
          if home_hall["dollar2_spawned_today"] == True and not home_hall["dollar2_taken_today"]:
            rv.append(("maya avatar events soap_attack home_hall dollar2",648,397))
          if not home_hall["book_taken"]:
            rv.append(("maya avatar events soap_attack home_hall book",966,576))

      elif state.startswith("chili"):
        rv.append((1920,1080))
        rv.append(("maya avatar events chili background",0,0))
        rv.append(("maya avatar events chili flora_body",321,96))
        rv.append(("maya avatar events chili flora_underwear",463,381))
        rv.append(("maya avatar events chili flora_clothes",317,340))
        if "flora_smile_right" in state:
          rv.append(("maya avatar events chili flora_face1",501,158))
        elif "flora_excited_right" in state:
          rv.append(("maya avatar events chili flora_face2",501,158))
        elif "flora_skeptical" in state and not "flora_skeptical_right" in state:
          rv.append(("maya avatar events chili flora_face3",501,158))
        elif "flora_neutral" in state:
          rv.append(("maya avatar events chili flora_face4",501,158))
        elif "flora_skeptical_right" in state:
          rv.append(("maya avatar events chili flora_face5",501,158))
        elif "flora_sarcastic_right" in state:
          rv.append(("maya avatar events chili flora_face6",501,158))
        elif "flora_smile" in state:
          rv.append(("maya avatar events chili flora_face7",501,158))
        elif "flora_eyeroll" in state:
          rv.append(("maya avatar events chili flora_face8",501,158))
        elif "flora_annoyed" in state:
          rv.append(("maya avatar events chili flora_face9",501,158))
        elif "flora_excited" in state:
          rv.append(("maya avatar events chili flora_face10",501,158))
        if any(expression in state for expression in ("maya_chew","maya_thinking_left_bowl")) or ("maya_thinking" in state and not "maya_thinking_left" in state):
          rv.append(("maya avatar events chili maya_body1",1139,182))
          rv.append(("maya avatar events chili maya_underwear1",1371,468))
          rv.append(("maya avatar events chili maya_clothes1",1139,449))
        elif any(expression in state for expression in ("maya_thinking_left","maya_flirty_left","maya_flirty","maya_smile_left","maya_dramatic","maya_smile","maya_neutral_left","maya_dramatic_left","maya_bored")):
          rv.append(("maya avatar events chili maya_body2",1139,182))
          rv.append(("maya avatar events chili maya_underwear2",1371,468))
          rv.append(("maya avatar events chili maya_clothes2",1139,447))
        if any(expression in state for expression in ("maya_chew","maya_thinking_left_bowl")) or ("maya_thinking" in state and "maya_thinking_left" not in state):
          rv.append(("maya avatar events chili maya_arms1",1125,458))
          rv.append(("maya avatar events chili maya_sleeves1",1210,458))
        elif any(expression in state for expression in ("maya_thinking_left","maya_flirty_left","maya_smile_left","maya_bored")) or ("maya_flirty" in state and not "maya_flirty_chest" in state) or ("maya_smile" in state and not "maya_smile_chest" in state) or ("maya_dramatic_left" in state and not "maya_dramatic_left_chest" in state):
          rv.append(("maya avatar events chili maya_arms2",1212,458))
          rv.append(("maya avatar events chili maya_sleeves2",1281,456))
        elif any(expression in state for expression in ("maya_dramatic","maya_neutral_left","maya_smile_chest","maya_flirty_chest","maya_dramatic_left_chest")):
          rv.append(("maya avatar events chili maya_arms3",1223,458))
          rv.append(("maya avatar events chili maya_sleeves3",1290,456))
        if "maya_chew" in state:
          rv.append(("maya avatar events chili maya_face1",1243,302))
        elif "maya_thinking_left" in state and not "maya_thinking_left_bowl" in state:
          rv.append(("maya avatar events chili maya_face2",1280,273))
        elif "maya_flirty_left" in state:
          rv.append(("maya avatar events chili maya_face3",1280,273))
        elif "maya_flirty" in state:
          rv.append(("maya avatar events chili maya_face4",1280,273))
        elif "maya_smile_left" in state:
          rv.append(("maya avatar events chili maya_face5",1280,273))
        elif "maya_thinking" in state and not "maya_thinking_left" in state:
          rv.append(("maya avatar events chili maya_face6",1243,302))
        elif "maya_dramatic" in state and not "maya_dramatic_left" in state:
          rv.append(("maya avatar events chili maya_face7",1280,273))
        elif "maya_smile" in state:
          rv.append(("maya avatar events chili maya_face8",1280,273))
        elif "maya_neutral_left" in state:
          rv.append(("maya avatar events chili maya_face9",1280,273))
        elif "maya_dramatic_left" in state:
          rv.append(("maya avatar events chili maya_face10",1280,273))
        elif "maya_bored" in state:
          rv.append(("maya avatar events chili maya_face11",1280,273))
        elif "maya_thinking_left_bowl" in state:
          rv.append(("maya avatar events chili maya_face12",1243,302))
        if "flora_excited_empty" in state:
          rv.append(("maya avatar events chili flora_bowl1",447,530))
        elif any(expression in state for expression in ("flora_skeptical_right_half","flora_sarcastic_right_half","flora_smile_half","flora_skeptical_half","flora_eyeroll_half","flora_annoyed_half","flora_excited_right_half","flora_excited_half")):
          rv.append(("maya avatar events chili flora_bowl2",447,451))
        else:
          rv.append(("maya avatar events chili flora_bowl3",447,450))
        if any(expression in state for expression in ("maya_flirty_empty","maya_smile_left_empty","maya_thinking_left_bowl_empty")):
          rv.append(("maya avatar events chili maya_bowl1",1227,583))
        elif any(expression in state for expression in ("maya_chew_half","maya_thinking_half","maya_dramatic_left_half","maya_smile_left_half","maya_flirty_chest_half","maya_flirty_left_half","maya_thinking_left_bowl_half","maya_dramatic_left_chest_half","maya_smile_half")):
          rv.append(("maya avatar events chili maya_bowl2",1227,511))
        else:
          rv.append(("maya avatar events chili maya_bowl3",1227,511))
        if all(expression not in state for expression in ("maya_chew","maya_thinking_left_bowl")) and ("maya_thinking" not in state or "maya_thinking_left" in state):
          rv.append(("maya avatar events chili maya_spoon",1181,602))
        if "mc_empty" in state:
          rv.append(("maya avatar events chili mc_bowl1",676,917))
        elif "mc_half" in state:
          rv.append(("maya avatar events chili mc_bowl2",676,780))
        else:
          rv.append(("maya avatar events chili mc_bowl3",676,768))

      elif state.startswith("hot_sauce_competition"):
        expression = None if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste","bust_a_ghost_aftermath","carolina_taste","carolina_aftermath","fahrenheit_taste","fahrenheit_aftermath")) else state.rsplit(" ", 1)[1]
        state = state if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste","bust_a_ghost_aftermath","carolina_taste","carolina_aftermath","fahrenheit_taste","fahrenheit_aftermath")) else state.rsplit(" ", 1)[0]
        rv.append((1920,1080))
        rv.append(("maya avatar events hot_sauce_competition background",0,0))
        rv.append(("maya avatar events hot_sauce_competition flora_body",326,234))
        rv.append(("maya avatar events hot_sauce_competition flora_underwear",334,508))
        rv.append(("maya avatar events hot_sauce_competition flora_bottoms",326,489))
        if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste","bust_a_ghost_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition flora_face1",415,296))
        elif state.endswith(("carolina_taste","carolina_aftermath","fahrenheit_taste","fahrenheit_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition flora_face2",415,296))
        rv.append(("maya avatar events hot_sauce_competition flora_tops",324,223))
        if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste")):
          rv.append(("maya avatar events hot_sauce_competition maya_body1",979,110))
        elif state.endswith(("bust_a_ghost_aftermath","carolina_taste")):
          rv.append(("maya avatar events hot_sauce_competition maya_body2",979,110))
        elif state.endswith(("carolina_aftermath","fahrenheit_taste")):
          rv.append(("maya avatar events hot_sauce_competition maya_body3",979,110))
        elif state.endswith("fahrenheit_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_body4",979,110))
        if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste","bust_a_ghost_aftermath","carolina_taste","carolina_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition maya_underwear1",979,442))
        elif state.endswith(("fahrenheit_taste","fahrenheit_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition maya_underwear2",988,1042))
        if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste","bust_a_ghost_aftermath","carolina_taste")) or (state.endswith("carolina_aftermath") and not expression.endswith("confident")):
          rv.append(("maya avatar events hot_sauce_competition maya_top",979,439))
        rv.append(("maya avatar events hot_sauce_competition maya_skirt",975,759))
        if state.endswith(("taco_hell_taste","taco_hell_aftermath","wench_taste","wench_aftermath","bust_a_ghost_taste")):
          rv.append(("maya avatar events hot_sauce_competition maya_jacket",964,417))
        if state.endswith(("taco_hell_taste","wench_taste","bust_a_ghost_taste")):
          rv.append(("maya avatar events hot_sauce_competition maya_face1",1037,151))
        elif expression and expression.endswith("licking") and state.endswith("taco_hell_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face2",1037,151))
        elif expression and expression.endswith("bored") and state.endswith(("taco_hell_aftermath","wench_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition maya_face3",1037,151))
        elif expression and expression.endswith("confident") and state.endswith(("taco_hell_aftermath","wench_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition maya_face4",1037,151))
        elif expression and expression.endswith("dramatic") and state.endswith("wench_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face5",1037,151))
        elif expression and expression.endswith("licking") and state.endswith("bust_a_ghost_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face6",1037,151))
        elif expression and expression.endswith("dramatic") and state.endswith("bust_a_ghost_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face7",1037,151))
        elif expression and expression.endswith("bored") and state.endswith("bust_a_ghost_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face8",1037,151))
        elif expression and expression.endswith("confident") and state.endswith("bust_a_ghost_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face9",1037,151))
        elif state.endswith("carolina_taste"):
          rv.append(("maya avatar events hot_sauce_competition maya_face10",1037,151))
        elif expression and expression.endswith("thinking"):
          rv.append(("maya avatar events hot_sauce_competition maya_face11",1037,151))
        elif expression and expression.endswith("confident") and state.endswith("carolina_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face12",1037,151))
        elif state.endswith("fahrenheit_taste"):
          rv.append(("maya avatar events hot_sauce_competition maya_face13",1037,151))
        elif state.endswith("fahrenheit_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_face14",1037,151))
        if state.endswith("taco_hell_taste"):
          rv.append(("maya avatar events hot_sauce_competition sauce_taco_hell1",880,76))
        elif state.endswith("taco_hell_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition sauce_taco_hell2",825,136))
        elif state.endswith("wench_taste"):
          rv.append(("maya avatar events hot_sauce_competition sauce_wench1",880,76))
        elif state.endswith("wench_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition sauce_wench2",825,136))
        elif state.endswith("bust_a_ghost_taste"):
          rv.append(("maya avatar events hot_sauce_competition sauce_bust_a_ghost1",880,76))
        elif state.endswith("bust_a_ghost_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition sauce_bust_a_ghost2",825,136))
        elif state.endswith("carolina_taste"):
          rv.append(("maya avatar events hot_sauce_competition sauce_carolina1",880,76))
        elif state.endswith("carolina_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition sauce_carolina2",825,136))
        elif state.endswith("fahrenheit_taste"):
          rv.append(("maya avatar events hot_sauce_competition sauce_fahrenheit1",880,76))
        elif state.endswith("fahrenheit_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition sauce_fahrenheit2",825,136))
        if state.endswith(("taco_hell_taste","wench_taste","bust_a_ghost_taste")):
          rv.append(("maya avatar events hot_sauce_competition maya_arms1",717,111))
          rv.append(("maya avatar events hot_sauce_competition maya_sleeves1",711,234))
        elif state.endswith(("taco_hell_aftermath","wench_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition maya_arms2",717,110))
          rv.append(("maya avatar events hot_sauce_competition maya_sleeves2",711,235))
        elif state.endswith("bust_a_ghost_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_arms3",717,110))
        elif state.endswith("carolina_taste"):
          rv.append(("maya avatar events hot_sauce_competition maya_arms4",717,111))
        elif state.endswith("carolina_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_arms5",717,110))
        elif state.endswith("fahrenheit_taste"):
          rv.append(("maya avatar events hot_sauce_competition maya_arms6",717,111))
        elif state.endswith("fahrenheit_aftermath"):
          rv.append(("maya avatar events hot_sauce_competition maya_arms7",717,110))
        if state.endswith(("carolina_taste","carolina_aftermath","fahrenheit_taste","fahrenheit_aftermath")):
          rv.append(("maya avatar events hot_sauce_competition action_lines",0,0))

      elif state.startswith("sink"):
        rv.append((1920,1080))
        rv.append(("maya avatar events sink background",0,0))
        rv.append(("maya avatar events sink maya_body",412,34))
        if state.endswith("bending_over"):
          rv.append(("maya avatar events sink maya_face1",1078,181))
        elif state.endswith("spitting"):
          rv.append(("maya avatar events sink maya_face2",1078,181))
        elif state.endswith("looking_back"):
          rv.append(("maya avatar events sink maya_face3",1078,181))
        rv.append(("maya avatar events sink maya_underwear",450,520))
        rv.append(("maya avatar events sink maya_skirt",360,458))

      elif state.startswith("looking_down"):
        rv.append((1920,1080))
        if state.endswith("concerned"):
          rv.append(("maya avatar events looking_down blur",0,0))
        else:
          rv.append(("maya avatar events looking_down background",0,0))
          rv.append(("maya avatar events looking_down flora_body",421,98))
          rv.append(("maya avatar events looking_down flora_underwear",429,393))
          rv.append(("maya avatar events looking_down flora_clothes",405,333))
          if state.endswith(("concerned","sarcastic1")):
            rv.append(("maya avatar events looking_down flora_face1",517,163))
          elif state.endswith(("squeeze1","squeeze2","squeeze3","squeeze4","confident","sarcastic2")):
            rv.append(("maya avatar events looking_down flora_face2",517,163))
          rv.append(("maya avatar events looking_down flora_cap",481,82))
          rv.append(("maya avatar events looking_down maya_body",1143,237))
          rv.append(("maya avatar events looking_down maya_underwear",1200,654))
          rv.append(("maya avatar events looking_down maya_skirt",1172,554))
          if state.endswith(("concerned","sarcastic1","confident","sarcastic2")):
            rv.append(("maya avatar events looking_down maya_arms1",1068,443))
          elif state.endswith("squeeze1"):
            rv.append(("maya avatar events looking_down maya_arms2",1046,440))
          elif state.endswith("squeeze2"):
            rv.append(("maya avatar events looking_down maya_arms3",1034,423))
          elif state.endswith("squeeze3"):
            rv.append(("maya avatar events looking_down maya_arms4",1024,401))
          elif state.endswith("squeeze4"):
            rv.append(("maya avatar events looking_down maya_arms5",1024,401))
          if state.endswith("concerned"):
            rv.append(("maya avatar events looking_down maya_face1",1162,268))
          elif state.endswith(("sarcastic1","sarcastic2")):
            rv.append(("maya avatar events looking_down maya_face2",1162,268))
          elif state.endswith(("squeeze1","squeeze2","squeeze3","squeeze4")):
            rv.append(("maya avatar events looking_down maya_face3",1162,268))
          elif state.endswith("confident"):
            rv.append(("maya avatar events looking_down maya_face4",1162,268))

      return rv


init python:
  class Interactable_maya(Interactable):

    def title(cls):
      return maya.name

    def actions(cls,actions):
      ## Talks ##
      if game.season > 1 and quest.maya_witch > "wait":
        actions.append(["talk","Talk","?maya_talk_default"])

      ## Quests ##
      if mc["focus"]:
        ## Fall in Newfall ##
        if mc["focus"] == "maya_witch":
          if quest.maya_witch == "wait":
            actions.append(["quest","Quest","quest_maya_witch_wait"])

        ## Hot My Bot ##
        elif mc["focus"] == "mrsl_bot":
          if quest.mrsl_bot == "diversion":
            actions.append(["quest","Quest","quest_mrsl_bot_diversion"])

      else:
        ## Fall in Newfall ##
        if quest.fall_in_newfall == "late" and "maya" not in quest.fall_in_newfall["assembly_conversations"]:
          actions.append(["quest","Quest","quest_fall_in_newfall_late_school_homeroom_maya"])

        ## Suitable Romance ##
        if quest.jacklyn_romance == "maya":
          actions.append(["quest","Quest","quest_jacklyn_romance_maya"])
        elif quest.jacklyn_romance == "hide_and_seek":
          actions.append(["quest","Quest","?quest_jacklyn_romance_hide_and_seek"])
        elif quest.jacklyn_romance == "found_them":
          actions.append(["quest","Quest","quest_jacklyn_romance_found_them"])
        elif quest.jacklyn_romance == "wait" and quest.jacklyn_romance["cat_tail_returned"]:
          actions.append(["quest","Quest","?quest_jacklyn_romance_wait_cat_tail"])
        elif quest.jacklyn_romance == "wait":
          actions.append(["quest","Quest","?quest_jacklyn_romance_wait"])

        ## Spell You Later ##
        if quest.maya_spell == "maya" and game.location == "school_cafeteria":
          actions.append(["quest","Quest","quest_maya_spell_maya"])

        ## Voluntary Good ##
        if quest.lindsey_voluntary == "interrogation":
          actions.append(["quest","Quest","quest_lindsey_voluntary_interrogation"])

        ## Hot My Bot ##
        if quest.mrsl_bot == "help_2_electric_boogaloo":
          actions.append(["quest","Quest","quest_mrsl_bot_help_2_electric_boogaloo"])

        ## The Dive ##
        if quest.maxine_dive == "line":
          actions.append(["quest","Quest","quest_maxine_dive_line"])

        #
        #

        ## Spell You Later ##
        if quest.jacklyn_art_focus.actually_finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and not quest.maya_spell.started:
          actions.append(["quest","Quest","quest_maya_spell_start"])

      ## Flirts ##
      if game.season > 1 and quest.maya_witch > "wait":
        actions.append(["flirt","Flirt","?maya_flirt_default"])


label maya_talk_default:
  show maya annoyed with Dissolve(.5)
  maya annoyed "At least give me roses if you want a blowjob."
  maya eyeroll "Man, chivalry truly is dead."
  window hide
  hide maya with Dissolve(.5)
  return

label maya_flirt_default:
  show maya bored with Dissolve(.5)
  maya bored "By all means, waste my time with a pick-up line. I can't wait to be wooed.{space=-90}"
  window hide
  hide maya with Dissolve(.5)
  return
