label quest_maya_witch_start:
  "Oh, man. There's [maya] again."
  "Looking hot and somehow frightening all in one."
  "You would think someone that cute would be incapable of sarcastic disdain..."
  "Yet I'm pretty sure it's the only language she knows."
  window hide
  show maya thinking with Dissolve(.5)
  pause 0.25
  show maya thinking at move_to(.25,.75)
  pause 1.0
  show maya thinking flip at move_to(.75,1.0)
  pause 1.25
  show maya thinking at move_to(.5,.5)
  pause 0.75
  window auto
  "Eyes dark, she twirls a strand of fiery hair round and round her finger."
  "Seems risky to engage when she's already in a mood, but there's something so alluring about playing with fire."
  "It whispers at a hint of beauty... the promise of danger... the thrill of losing a limb..."
  mc "What's up, [maya]?"
  maya afraid "It's you, isn't it?"
  "Ugh..."
  mc "Of course not..."
  maya skeptical "So, why are you sniffing around here?"
  mc "I wasn't sniffing around!"
  maya skeptical "Why do you look so guilty, then?"
  mc "I, err... just wanted to know if everything was okay..."
  maya confident_hands_down "Oh! In that case, totally. My morning has just been all unicorn farts and rainbow jizz."
  mc "I feel like I'm sensing some hostility."
  maya bored "Maybe because I'm hostile."
  maya bored "And you sense it."
  mc "...that could be it."
  maya eyeroll "It's whatever. It already feels like a long day."
  mc "Err, how come?"
  maya eyeroll "I've been cursed."
  "There's something you don't hear everyday..."
  mc "Excuse me?"
  maya annoyed "C-u-r-s-e-d."
  mc "Thanks for spelling it out, I guess."
  maya sarcastic "Happy to alleviate any confusion."
  maya dramatic "Anyway, I don't know what witch my great great great grandfather fucked over, but there you have it. Cursed."
  mc "I have no idea why anyone would put a curse on you..."
  maya dramatic "I know! I'm like a perfect angel!"
  mc "Right. But how do you know you're cursed?"
  maya thinking "Watch this shit..."
  window hide
  show maya thinking at move_to(.85,.75)
  pause 0.25
  play sound "<from 3.1 to 5.1>vending_machine"
  pause 1.0
  show maya thinking_soda with Dissolve(.5)
  pause 0.25
  window auto
  "[maya] goes over to the vending machine and selects a soda option. Grabs it when it rolls down."
  "She holds it out away from her body, a grimace twisting her face as she opens the can."
  play sound "soda_can"
  play ambient "audio/sound/soda_spraying.ogg" noloop
  show maya afraid_soda_spraying with dissolve2
  maya afraid_soda_spraying "Eeep!" with vpunch
  "As soon as she does, the soda explodes everywhere, fizzing out of the can and bubbling down to the ground."
  show maya afraid_soda with dissolve2
  mc "Okay, I think I'm missing something here..."
  maya afraid_soda "It's the same every day! Every time I try to get a soda, it comes out shaken and explodes all over me!"
  mc "Maybe it's busted? I could try if you want."
  maya flirty_soda "Your damsel in distress senses tingling, [mc]?"
  "Her eyes flick down to my crotch and a razor sharp smile glints on her full, pink lips."
  "It burns a hole right into my heart and sends heat piping through my skin."
  "But most of all, I'm surprised she even knows my name."
  mc "No! Of course not!"
  "But when she looks at me like that, I can't deny it does something to me..."
  mc "Come on, let me give it a go!"
  maya eyeroll "Fine."
  show maya eyeroll at move_to(.5,.75)
  show maya smile_hands_in_pockets with dissolve2
  maya smile_hands_in_pockets "I've always wanted my own personal soda fetcher, anyway. I finally made it!"
  "[maya] bats her long lashes at me and offers a bright, heart melting smile."
  "She could probably make me do much more for her than that..."
  mc "Here goes nothing..."
  window hide
  pause 0.25
  play sound "<from 3.1 to 5.1>vending_machine"
  pause 1.0
  window auto
  "Together, we watch as the soda rolls down and bumps innocently into the little compartment."
  "I reach my hand inside, and find I'm holding my breath when I withdraw the can."
  play sound "soda_can"
  "Slowly, I open it and..."
  "..."
  "..."
  "...nothing."
  "Just a little release of air as it's opened, but no explosion."
  mc "It seems fine to me."
  maya annoyed "Now do it again, but this time let me open it."
  mc "I don't—"
  maya annoyed "Just do it!"
  mc "All right! Fine!"
  mc "What's the sense in testing a hypothesis if you don't run multiple experiments, right?"
  "God, I sound like [maxine]. What a terrifying thought."
  maya blush "Oh, yeah, just like that! Talk nerdy to me!"
  mc "..."
  mc "I don't need your sass."
  maya confident "What? You mean you're not a sass vampire? I thought it was the only thing sustaining you day after day."
  mc "The only thing I suck is... at making good grades."
  "I can't deny she's kind of cute when she gets that look in her eye, the one that says she just can't help herself."
  "Her tongue seems to have a will of its own. I'm sure it's gotten her into some trouble."
  "I wonder if sarcasm is the main defense mechanism she uses to ensure her walls stay built up?"
  "And I wonder what it looks like when those walls are knocked down...{space=-25}"
  maya bored "Anyway, continue, if you would."
  mc "Okay, here goes nothing... again..."
  window hide
  hide screen interface_hider
  show maya cringe_soaked
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "We run the experiment again. We keep the same conditions."
  play sound "<from 3.1 to 5.1>vending_machine"
  "We choose the same soda and I take it out with the same hand I used before."
  "The only difference is I give it to [maya] this time. Hand it off like it's a bomb about to go off..."
  play sound "soda_can"
  play ambient "audio/sound/soda_spraying.ogg" noloop
  maya cringe_soaked "Aaaaaah!" with vpunch
  "...and that's exactly what it does. As soon as she opens it, it erupts like a sugary volcano."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "She stands there drenched, the syrupy concoction dripping from her hair and soaking into her top."
  mc "I... did not actually see that coming..."
  mc "Are you okay?"
  maya cringe_soaked "Oh, def. Being drenched in soda is one of my favorite things, didn't you know?"
  mc "I'm sorry. I didn't think that would happen."
  maya cringe_soaked "Yeah, well, I know what I know."
  maya dramatic_soaked "Anyway, now I have to go get cleaned up."
  maya dramatic_soaked "Thanks for the help, [mc]. Lates!"
  window hide
  show maya dramatic_soaked at disappear_to_right
  pause 0.5
  window auto
  "I don't know what all that was about, but if someone is really pranking{space=-40}\n[maya], they need to be stopped."
  "Maybe I should keep an eye out, just in case..."
  "I'll just take a peek at her schedule to see where she'll be today."
  window hide
  $quest.maya_witch.start()
  return

label quest_maya_witch_schedule:
  "Hmm... where does [mrsl] keep everyone's schedule?"
  "Probably as simple as in her desk, right?"
  "..."
  "It's not like I'm stealing the Declaration of Independence or something.{space=-55}\nNo need to overthink it."
  if mrsl.location == "school_homeroom":
    "I just have to wait for her to leave the homeroom first..."
  $quest.maya_witch["overthink_it"] = True
  return

label quest_maya_witch_schedule_school_homeroom_teacher_desk_interact_mrsl:
  "I'm pretty damn stealthy, but breaking into [mrsl]'s desk while she's sitting there is above my paygrade."
  return

label quest_maya_witch_schedule_school_homeroom_teacher_desk_interact:
  if not quest.maya_witch["locked_of_course"]:
    "I'll just open this drawer here, and..."
    "..."
  "Locked, of course."
  $quest.maya_witch["locked_of_course"] = True
  return

label quest_maya_witch_schedule_school_homeroom_teacher_desk_use_item_mrsl:
  "I'm pretty damn stealthy, but breaking into [mrsl]'s desk while she's sitting there is above my paygrade."
  return

label quest_maya_witch_schedule_school_homeroom_teacher_desk_use_item(item):
  if item == "high_tech_lockpick":
    $mc.remove_item("high_tech_lockpick")
    pause 0.25
    "Just gotta jiggle it this way..."
    extend " and finagle it that way..."
    play sound "lock_click"
    extend " and bam!"
    window hide
    $mc.add_item("high_tech_lockpick")
    $mc.add_item("maya_schedule")
  else:
    "I can't pick my nose with my [item.title_lower], much less a lock."
    $quest.maya_witch.failed_item("schedule",item)
  return

label quest_maya_witch_english_class:
  "There's [maya], fresh out of English class."
  "So far, I don't see anything suspicious..."
  "But I guess it wouldn't hurt to investigate further."
  "Hmm... I need a good place to hide so I can observe uninterrupted..."
  menu(side="middle"):
    extend ""
    "Hide in the art classroom":
      "The art of spying is a lost form. Time to bring it back."
      call goto_school_art_class
      "Oh, silent canvases, what secrets do you keep? What truths do\nyou know?"
      show jacklyn smile_hands_down at appear_from_left
      jacklyn smile_hands_down "Hey, hey, [mc]! Is the bacon sizzling?"
      mc "I, err... I had oatmeal for breakfast..."
      jacklyn thinking "Lumpy and sad. Hard to vibe with that."
      show jacklyn thinking at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Yeah, well, that tracks with my life.\"":
          show jacklyn thinking at move_to(.5)
          mc "Yeah, well, that tracks with my life."
          jacklyn cringe "No dice on that blank canvas, homeslice."
          mc "Are you saying I should paint with all the colors?"
          jacklyn annoyed "Can't force a purple if you're feeling a yellow."
          mc "I guess..."
          jacklyn annoyed "You have to listen to the paintbrush when it tells you the direction\nit wants to go."
          mc "Err, right."
          mc "So, uh, have you happened to notice anything weird going on with [maya] lately?"
          jacklyn neutral "Sorry, but the paintbrush only speaks to the artist, my man."
          "Great. Helpful as always."
          mc "Thanks... I guess I'll keep that in mind..."
          jacklyn smile "Stick it in your back pocket and keep the change loose."
          jacklyn smile "And if you're into [maya], don't waste time peeling the banana. Not that{space=-35}\nI personally dabble."
          mc "I'm not—"
          jacklyn excited "Oh, wait! There was something!"
          mc "What's that?"
          jacklyn excited "The other day, in the piss box, the lights flipped shit when she entered!{space=-70}\nStraight epileptic."
          mc "Huh, interesting. Thank you, [jacklyn]."
          jacklyn laughing "Sure thing. It's no hair off my vag."
          $quest.maya_witch["straight_epileptic"] = True
        "\"I have something hard you can vibe with...\"{space=-15}":
          show jacklyn thinking at move_to(.5)
          mc "I have something hard you can vibe with..."
          $jacklyn.lust+=1
          jacklyn laughing "You shoot as straight as you talk, slinger?"
          mc "Why don't we meet up sometime and I'll show you how straight\nI shoot?"
          jacklyn excited "Aces! Just light me up and call me Mary."
          "Seriously? I didn't think that would work that well..."
          "I do have to admire how open and free spirited [jacklyn] is."
          mc "Err, like the Fourth of July!"
          jacklyn annoyed "Easy there, Hornysaurus Rex. Don't spangle your banner too hard."
          mc "Right, sorry."
          mc "I got to run now, but I'm looking forward to... vibing."
          jacklyn laughing "Here, there, and everywhere, baby legs."
          jacklyn laughing "Now, run free!"
        "\"Do you know anything about someone pranking people around here?\"":
          show jacklyn thinking at move_to(.5)
          mc "Do you know anything about someone pranking people around here?{space=-20}"
          jacklyn excited "Life is one big prank. You just have to be bold enough to prank\nher back."
          "Huh, that sounds surprisingly deep..."
          "Still, not quite what I was after."
          mc "So, you haven't noticed anything weird going on lately?"
          jacklyn laughing "Weird is a state of mind, brohammon."
          jacklyn laughing "It's not all feathers and cancan dancers, you know?"
          mc "I guess..."
          jacklyn annoyed "It's cheap tricks and theatrics. The only thing real is art."
          mc "You know what they say — life is just an imitation of it."
          mc "And I agree."
          jacklyn excited "That's wicked, [mc]! Your thought train just creamed into mine!"
          mc "Heh. Just paint me starry and call me Van Gogh, right?"
          $jacklyn.love+=1
          jacklyn laughing "That's exactly right!"
          "As much as I love watching [jacklyn] come to life with this kind of talk, I'm not really getting anywhere about [maya]..."
          "I need to stay focused."
          mc "Sorry, I'd love to talk more about it sometime, but I really should get going."
          jacklyn laughing "Stellar. Keep it cherried, [mc]."
          mc "Err, I will."
      window hide
      hide jacklyn with Dissolve(.5)
      window auto
      if quest.maya_witch["straight_epileptic"]:
        "Well, nothing supremely weird about those flashing lights in the bathroom. The wiring in this place is a joke."
        "I'll tail [maya] a little longer, just to be sure."
      else:
        "Hmm... that didn't give me much..."
        "I guess I'll tail [maya] a little longer."
      "..."
      "Schedule says she has gym class soon."
    "?quest.isabelle_dethroning.finished@|{image=isabelle contact_icon}|{image=stats love}|Hide in the\nmusic classroom":
      "No band in session. I'll just listen from there."
      call goto_school_music_class
      "Phew! It's empty."
      "I'll just see if I can hear anything going on out in the hall..."
      window hide
      show maya music_class_peek
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      isabelle "Hi! [maya], right? Everything okay?"
      maya music_class_peek "Oh, my god! You know who I am! What a blessed day!"
      isabelle "No need to be rude, mate. I've been slowly getting to know everyone.{space=-20}"
      maya music_class_peek "Shit, I'm so sorry! How can I ever make it up to you?"
      isabelle "You could start by dropping the sarcasm."
      maya music_class_peek "Okay, fine. If you must know, I keep getting these weird texts."
      "Huh, [maya] sounds serious for once."
      "Also a little worried... and is that contrition I hear for talking to [isabelle] that way?"
      isabelle "Threatening texts?"
      maya music_class_peek "Nah. At least not yet."
      isabelle "It could be worth reporting..."
      maya music_class_peek "Yeah, maybe."
      isabelle "What if it's some creepy stalker? You don't want to mess around if it's serious."
      maya music_class_peek "I guess, but online creeps aren't something I haven't dealt with before.{space=-50}"
      maya music_class_peek "Once they find out there's a woman on the other side, their minds go static."
      isabelle "Ugh, typical."
      maya music_class_peek "Yeah..."
      isabelle "Anyway, I've got to run to class now, but good luck, and don't be a stranger!"
      window hide
      pause 0.125
      show maya music_class_peek empty with Dissolve(.5)
      pause 0.125
      window auto
      "Interesting... those texts could perhaps be prank related..."
      "And is this a budding friendship between [maya] and [isabelle]?"
      "[isabelle] sure knows how to pick the most difficult people... but if anyone could handle [maya]'s thorns, it's her."
      "Still, nothing about that sounded too out of the ordinary. It's always a jungle in the virtual world."
      "How did this person get a hold of [maya]'s number, anyway?"
      "Maybe it's someone she knows..."
      "I'll tail her a little longer, just to be sure."
      "..."
      "Schedule says she has gym class soon."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.5
      hide maya
      hide black onlayer screens
      with Dissolve(.5)
      $quest.maya_witch["weird_texts"] = True
    "Hide behind the piano":
      "The most risky spot. Just how I like it."
      window hide
#     show maya piano_peek
      show black onlayer screens zorder 100
      with Dissolve(.5)
      show maya piano_peek
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Hmm... bit of a tight fit, but it will have to do."
      "Beethoven was deaf. Maybe [maya] is blind and won't notice me?"
      "..."
      "How can you even tell if a person is cursed, anyway?"
      "She looks fine to me. And nothing else seems out of the ordinary."
      "Maybe she's just crazy..."
      show maya piano_peek mrsl with Dissolve(.5)
      mrsl "Hello, [maya]! Classes off to a good start so far this semester?"
      maya piano_peek mrsl "They're off all right, like the races. Jury's still out on whether they're{space=-10}\nany good or not."
      mrsl "Well, I have full faith you will make the most of each day and make them good!"
      maya piano_peek mrsl "Spoken like a true motivational poster! I feel more motivated already.{space=-20}"
      mrsl "Just keep up the good work. Make each day count."
      maya piano_peek mrsl "Count the days. Got it."
      mrsl "Everything else going well?"
      "Why is [mrsl] so interested in her?"
      "Maybe she's just being nice and teacherly, but it's still a bit odd..."
      "There has been a lot going on this year."
      maya piano_peek mrsl "Oh, you know me — happiest girl in the world."
      mrsl "I'm glad to hear it."
      mrsl "Do stop by if you ever... need anything."
      "Mmm... the way she said that makes me want to stop by and need lots of things..."
      maya piano_peek mrsl "Yeah, sure."
      window hide
      pause 0.125
      show maya piano_peek empty with Dissolve(.5)
      pause 0.125
      window auto
      "Well, nothing supremely weird about that."
      "...other than [maya]'s cool ability to blow off figures of authority and their supposed interest in you."
      "I've always found her intimidating in the past, but maybe there's more to her?"
      "She's always been a quiet girl... until someone pisses her off, that is.{space=-10}"
      "I wonder what else she gets up to in a day..."
      "..."
      "Schedule says she has gym class soon."
      window hide
      show maya
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
  $quest.maya_witch.advance("gym_class")
  return

label quest_maya_witch_gym_class_school_first_hall_east:
  "Ah, gotta love the smell of gym socks and sweat in the afternoon..."
  "...yet, there's also the subtle hint of wildflowers."
  window hide
  show maya eyeroll:
    xanchor 0.075 xpos 1.0 yalign 1.0
    linear 1.5 xanchor 0.925 xpos 0.0
  pause 1.5
  window auto
  "Okay, just follow the scent and act casual."
  "Maybe run a lap, or at the very least, give my muscles a stretch."
  "Perfectly normal."
  $quest.maya_witch["act_casual"] = True
  return

label quest_maya_witch_gym_class_school_gym_score_board:
  "[jacklyn] wouldn't be happy about the design of this piece of art. Not nearly esoteric enough."
  return

label quest_maya_witch_gym_class:
  "Here we go. Just going to work out my hamstring. No big deal."
  "It looks like [maya] is settling in to be entertained by the jocks."
  "Gym class... sports... it really is basically the 21st Century equivalent{space=-20}\nof the Colosseum."
  "..."
  if quest.kate_stepping.finished:
    "Oh, crap. [kate] is approaching her."
    window hide
    $kate["outfit_stamp"] = kate.outfit
    $kate.outfit = {"shirt":"kate_cheerleader_top", "bra":"kate_cheerleader_bra", "pants":"kate_cheerleader_skirt", "panties":"kate_cheerleader_panties"}
    show kate smile at Transform(xalign=.25)
    show maya eyeroll at Transform(xalign=.75)
    with Dissolve(.5)
    window auto
    kate smile "Oh, my god, [maya]! Is it true?"
    maya eyeroll "Only one way to find out, [kate]."
    "Hmm... I'm not entirely sure what's going on here..."
    "It's like a couple of hissing cats prepared to fight over an alley territory."
    "Normally, I'd bet on [kate], but [maya] has a steel edge to her look that dares someone to fuck with her."
    "I can't help but admire her ability to stare straight into the eye of\nthe snake."
    kate laughing "Ew! No, thanks."
    kate flirty_hands_on_hips "What do you think, [mc], you pervy little eavesdropper?"
    mc "Huh? M-me?"
    mc "I was just, err... minding my own business! Doing some pre-workout stretches!"
    kate eyeroll "Please, the only thing you work out is that jaw of yours."
    maya flirty "Oh? Sucking too much cock too, [mc]?"
    mc "Ugh, don't encourage her!"
    kate excited "She just said what's on everyone's mind."
    mc "On everyone's mind?"
    kate excited "Just look at the scoreboard!"
    window hide
    show kate excited at move_to(.5)
    pause 0.75
    window auto
    "What the hell?"
    kate excited "Now, what do you think about that?"
    show kate excited at move_to("left")
    show maya flirty at move_to("right")
    menu(side="middle"):
      extend ""
      "\"I think bullying is gross.\"":
        show kate excited at move_to(.25)
        show maya flirty at move_to(.75)
        mc "I think bullying is gross."
        kate neutral "Don't be such a drama queen."
        kate neutral "It's just someone poking a bit of fun."
        mc "And that someone is you?"
        kate laughing "Oh, please. I have better things to do with my time."
        maya neutral "It's fine, [mc]. If I wanted a white knight, I would sound the bugle."
        mc "I wasn't trying to—"
        kate smile "Yeah, bitch boy. Go stick your lance somewhere else."
        mc "Ugh, whatever..."
        maya smile "Don't worry. If I ever need you, I'll be sure to flash the signal."
        mc "And what's the signal?"
        maya smile "It's a novel idea, but... it looks like a bat, I flash it into the sky, and then you come."
        mc "..."
        mc "{i}I'm Batman.{/}"
        $maya.love+=1
        maya confident "That's surprisingly not the worst impression I've ever heard!"
        kate annoyed "God, nerds take the fun out of everything."
        window hide
        show maya confident at move_to(.5)
        hide kate with Dissolve(.5)
        $kate.outfit = kate["outfit_stamp"]
        window auto show
        show maya bored with dissolve2
        maya bored "Anyway..."
        window hide
        show maya bored at disappear_to_right
        pause 0.5
        window auto
        "I don't know what's going on, but [maya] sure does take it with grace."
        "I don't think everyone would be as brave."
      "\"As long as it's not bigger than mine...\"":
        show kate excited at move_to(.25)
        show maya flirty at move_to(.75)
        $mc.charisma+=1
        mc "As long as it's not bigger than mine..."
        kate embarrassed "Ew! You're such a little creep."
        maya sarcastic "Let's get together and measure sometime, [mc]."
        mc "Heh, let's do it!"
        kate thinking "Whatever. You two are freaks."
        window hide
        show maya sarcastic at move_to(.5)
        hide kate with Dissolve(.5)
        $kate.outfit = kate["outfit_stamp"]
        window auto show
        show maya cringe with dissolve2
        maya cringe "Weird shit going down, eh?"
        mc "Yeah. Not worth paying any actual attention to dumb stuff like that."
        maya sarcastic "Anyway, thanks for the laugh, [mc]."
        mc "No worries..."
        window hide
        show maya sarcastic at disappear_to_right
        pause 0.5
        window auto
      "\"Sounds like fake news.\"":
        show kate excited at move_to(.25)
        show maya flirty at move_to(.75)
        mc "Sounds like fake news."
        kate eyeroll "Whatever. It's funny."
        maya dramatic "As funny as a heart attack."
        mc "I think that's serious."
        maya dramatic "Very."
        mc "No, I mean—"
        maya dramatic "I know what you mean. It's my idiom, and I'll twist it if I want to."
        kate skeptical "I don't know about any idioms, but you're both idiots."
        window hide
        show maya dramatic at move_to(.5)
        hide kate with Dissolve(.5)
        $kate.outfit = kate["outfit_stamp"]
        window auto show
        show maya sarcastic with dissolve2
        maya sarcastic "Later, [mc]. Off to beat my fake news."
        window hide
        show maya sarcastic at disappear_to_right
        pause 0.5
        window auto
        "I swear, I can never tell when she's joking..."
  elif quest.isabelle_dethroning.finished:
    "Oh, crap. The cheerleaders are approaching her."
    window hide
    show casey confident flip at Transform(xalign=.055) behind maya
    show maya afraid at Transform(xalign=.375)
    show stacy confident at Transform(xalign=.675) behind maya
    show lacey confident at Transform(xalign=.95) behind stacy
    with Dissolve(.5)
    window auto
    maya afraid "Am I about to be triple penetrated?"
    stacy confident "How would that work, hm? If the scoreboard isn't lying."
    window hide
    show casey confident flip at move_to(.475,0.75) behind maya
    show maya afraid at move_to(.625,0.75)
    show stacy confident at move_to(.8,0.75) behind maya
    show lacey confident at move_to(1.0,0.75) behind stacy
    pause 1.0
    window auto
    "What the hell?"
    show casey confident flip at move_to(.055,0.75) behind maya
    show maya afraid at move_to(.375,0.75)
    show stacy confident at move_to(.675,0.75) behind maya
    show lacey confident at move_to(.95,0.75) behind stacy
    show maya thinking with dissolve2
    maya thinking "Oh, right. You would have to take turns."
    casey confident flip "So, you're saying it's true?"
    maya flirty "Only one way to find out, girls."
    lacey neutral "And what way would that be?"
    show casey neutral flip
    stacy neutral "Babe, she was making a sexual innuendo."
    lacey neutral "A what?"
    maya kiss "I'm just saying, if I had a dick, I would probably fuck you first, [lacey].{space=-10}"
    casey neutral flip "Why her first?"
    lacey confident "Because I'm the prettiest, duh!"
    stacy neutral "You are not the prettiest! Have you seen me?"
    show lacey neutral with dissolve2
    casey neutral flip "You are both such divas."
    casey confident flip "Take a good look at me, [maya]. I'll make you reconsider."
    maya bored "At least hit me with a pick-up line. That's what all the guys do."
    casey confident flip "Did it hurt when you... err..."
    casey neutral flip "...wait, how does it go again?"
    "[maya] sure knows how to handle herself in tough situations."
    "They all look completely lost."
    maya bored "Anyway..."
    window hide
    show maya bored at disappear_to_right
    pause 0.5
    show casey neutral flip at move_to(.18)
    show stacy neutral at move_to(.5)
    show lacey neutral at move_to(.775)
    pause 0.5
    hide casey
    hide stacy
    hide lacey
    with Dissolve(.5)
    window auto
  if quest.maya_witch["straight_epileptic"]:
    "The soda, the bathroom lights, the scoreboard..."
  elif quest.maya_witch["weird_texts"]:
    "The soda, the texts, the scoreboard..."
  else:
    "First the soda, now the scoreboard..."
  if renpy.showing("maya bored"):
    "Someone is definitely giving her a hard time."
  else:
    "Someone is definitely giving [maya] a hard time."
  "I better keep an eye on her until I figure this out."
  "..."
  "According to her schedule, she has computer class next."
  $quest.maya_witch.advance("computer_class")
  return

label quest_maya_witch_computer_class:
  scene black with Dissolve(.07)
  $game.location = "school_computer_room"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  show maya annoyed
  with Dissolve(.5)
  maya annoyed "Aha!" with vpunch
  mc "Aha? Like the band?"
  maya neutral "No, {i}\"aha!\"{/} like every dastardly villain says when they catch the sneaky{space=-45}\ngoody two shoes."
  mc "So, you're the villain?"
  maya smile_hands_in_pockets "Of course. They're cooler by far."
  mc "I can't really argue with that."
  maya annoyed "But that's not the point! The point is you've been following me."
  maya annoyed "Why?"
  show maya annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I think you're really cute.\"":
      show maya annoyed at move_to(.5)
      mc "I think you're really cute."
      maya cringe "So, what, you've been perving on me?"
      mc "I just wanted to know what sort of things you like for when I ask you out."
      maya cringe "..."
      maya cringe "I can smell it from here."
      mc "Smell what?"
      $maya.love-=1
      maya cringe "The desperation."
      "Ouch. I should probably have played it more cool with her."
      "[maya] does seem like the sort where less is more..."
      "In this case, less of me altogether."
      mc "You're right, I'm sorry. Maybe I was trying too hard."
      maya dramatic "\"Maybe\" is selling it short."
      maya dramatic "And anyway, it's pointless. I don't really date."
      mc "Why not?"
      maya dramatic "Am I only supposed to have one sugar daddy? No money in that."
      mc "..."
      mc "You don't actually—"
      maya sarcastic "Of course not."
      maya sarcastic "It's all about the mommies."
      mc "Very funny..."
      "...and weirdly kind of hot..."
    "\"I wanted to know who's pranking you.\"":
      show maya annoyed at move_to(.5)
      mc "I wanted to know who's pranking you."
      maya eyeroll "Why? What's it to you?"
      mc "I just don't think it's cool."
      maya neutral "Well, it's not pranks. Someone actually has it out for me."
      maya neutral "But I can handle it myself."
      mc "Are you sure?"
      maya dramatic "Gosh, on second thought, I really need to double check with you that it's okay!"
      mc "Excuse me for showing concern..."
      maya cringe "Is that what they're calling it these days?"
      mc "Calling what?"
      maya cringe "Being all up in someone's business."
      mc "..."
    "\"I always come to the computer\nroom at this time.\"":
      show maya annoyed at move_to(.5)
      mc "I always come to the computer room at this time."
      maya thinking "Is that right? Come to beat off to furry porn or something?"
      mc "What? No! Just, err... homework..."
      maya flirty "Sure, \"homework.\" Can't get through the whole day without busting a nut, huh?"
      maya kiss "Maybe I could help bust it for you?"
      mc "Yes! I mean, no! I mean—"
      "Great. Now she is about to turn me on and make me look like an actual creep."
      maya flirty "Relax, I'm just fucking with you!"
      maya thinking "Still not to be confused with fucking you."
      mc "Err, I knew that..."
  maya bored "Anyway, feel free to stop stalking me now."
  mc "I wasn't... ugh... fine, I'll go..."
  window hide
  $mc["focus"] = "maya_witch"
  hide maya with Dissolve(.5)
  $quest.maya_witch.advance("locked")
  return

label quest_maya_witch_locked:
  "..."
  "Huh? That's weird."
  "..."
  "Why won't the door open?"
  window hide
  show maya dramatic with Dissolve(.5)
  window auto
  maya dramatic "Are you having trouble operating a door? I know it's the next level after rocket science, but still."
  mc "I know how to work a door. It's just locked."
  maya dramatic "Locked?"
  mc "L-o-c-k-e-d."
  $maya.love+=1
  maya sarcastic "Touché, [mc]. I suppose I deserved that one."
  mc "Heh! You really did."
  maya thinking "But why would it be locked now?"
  mc "Yeah, it's weird that whoever did it didn't even bother to check the room first..."
  maya thinking "And why lock it in the middle of the day when school is still in session?{space=-65}"
  mc "Maybe the janitor has finally lost it? Or could this be the prankster?"
  maya afraid "Well, try screaming for help or something!"
  mc "Me? Shouldn't you be the one screaming?"
  mc "I mean, somebody might actually come if they hear a girl calling\nfor help."
  maya eyeroll "Neat. Reverse sexism."
  mc "I wasn't trying to be, err... reverse sexist."
  maya neutral "It just comes naturally, I get it."
  mc "That's not what I meant!"
  maya smile "Look, don't sell yourself short, okay? And your voice is kind of high pitched, anyway."
  mc "No, it's not!"
  maya smile "I can hear the dogs howling already..."
  mc "Ugh! Fine!"
  "What is dignity, anyway?"
  "Whatever pride I once had has long since left my body."
  "My fists bang against the door, a steady tattoo of {i}help, help, help.{/}"
  mc "HEEEEEEEEELP!" with vpunch
  maya sarcastic "You're a natural! You belong in a theater!"
  mc "Really, now?"
  maya sarcastic "I'm going to call up my agent right now!"
  mc "How about you try it, then?"
  maya dramatic "Sorry, but I screamed for 10 minutes earlier. I've hit my screaming quota for the day."
  mc "That's not a thing."
  maya afraid "You're joking! I'm owed so many screams!"
  "God damn it. [maya] really makes it so you have to think before you open your dumb mouth."
  "Of course screaming quotas aren't a thing..."
  mc "I guess we're stuck in here, then."
  maya concerned "Be honest, were you the one who locked it? Is this the part where you{space=-40}\nmurder me and wear my skin?"
  show maya concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.charisma>=8@[mc.charisma]/8|{image=stats cha}|\"I'm actually not [mc] at all,\nI'm just wearing his skin.\"":
      show maya concerned at move_to(.5)
      mc "I'm actually not [mc] at all, I'm just wearing his skin."
      maya thinking "Really? In that case, I would have picked a prettier one."
      mc "Well, I had to make due in a pinch."
      mc "But you're right... that's why I've come for you now!"
      $maya.love+=1
      maya sarcastic "Hahaha! Not bad!"
      maya cringe "But it doesn't change the fact that we're still stuck in here."
    "\"This is serious! What if we're\nstuck in here all night?\"":
      show maya concerned at move_to(.5)
      mc "This is serious! What if we're stuck in here all night?"
      $maya.love-=1
      maya eyeroll "Oh, relax, Nancy. Don't get your panties in a twist."
      mc "My blood sugar is low..."
      maya neutral "We'll get you a cookie and juicebox in no time."
      mc "I don't want juice... unless it's strawberry juice..."
      maya neutral "Whatever the flavor, it doesn't change the fact that we're trapped."
  mc "I guess not..."
  maya bored "Oh, well. I'll just waste my time and life on the internet like I came here to do in the first place."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "Hmm... as much as I don't mind being stuck with [maya], I should probably try calling [jo] to see if she can help us get out of here."
  $quest.maya_witch.advance("phone_call")
  return

label quest_maya_witch_phone_call_school_computer_room_door:
  "I wonder what sort of madman would lock us away in here?"
  return

label quest_maya_witch_phone_call:
  $set_dialog_mode("phone_call_plus_textbox","jo")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  "Come on, [jo]! Please pick up!"
  "Every minute that passes, my stomach grows empty and my bladder{space=-15}\ngrows full."
  "It would be super embarrassing if we end up having to designate piss corners..."
  "What if we're stuck for days?"
  "Then I'll have to resort to drinking my own—"
  window hide
  $set_dialog_mode("phone_call_centered","jo")
  jo "Yes, [mc]?"
  mc "Oh, thank god, [jo]!"
  jo "What on the earth\nis the matter?"
  jo "You're not hurt, are you?"
  mc "No, no! Nothing like that!"
  jo "You scared me!"
  $set_dialog_mode("phone_call_plus_textbox","jo")
  window auto
  "Aw, she really does care!"
  "Despite how hard she can be on me, that relief in her voice is surprisingly heartwarming."
  window hide
  $set_dialog_mode("phone_call_centered","jo")
  mc "Sorry, it's just that I'm locked\nin the computer classroom..."
  jo "Why would you\ngo and do that?"
  $set_dialog_mode("phone_call","jo")
  menu(side="middle"):
    extend ""
    "\"I didn't do it on purpose!\"":
      $set_dialog_mode("phone_call_centered","jo")
      mc "I didn't do it on purpose!"
      jo "This isn't one of those\nsilly internet trends, is it?"
      mc "What? Getting\nlocked in a room?"
      jo "Well, I don't know what\nyou kids get up to these\ndays when you're bored."
      mc "I can promise you\nit's nothing like that."
      jo "Good, because you need to\nbe focusing on your studies."
      mc "Ugh... yes... I promise I am..."
      jo "Because your future is\nvery important, you know?"
      mc "[jo]!"
    "\"Yeah, because I totally did it\non purpose...\"":
      $set_dialog_mode("phone_call_centered","jo")
      mc "Yeah, because I totally\ndid it on purpose..."
      $jo.love-=1
      jo "Don't take that tone\nwith me, mister."
      mc "Sorry, [jo]..."
      jo "I've had a busy day and\nI don't need the extra\nstress and worry."
      mc "I know, I know!"
      mc "Can you just help, please?"
    "\"There's a sinister plot unfolding here.\"":
      $set_dialog_mode("phone_call_centered","jo")
      mc "There's a sinister\nplot unfolding here."
      jo "Very funny, [mc]."
      mc "Okay, not really."
      mc "But I do need you."
      mc "Remember when I was younger\nand you were always there to\npatch me up when I fell?"
      mc "Or nurse me back to\nhealth if I got sick?"
      $jo.lust+=1
      jo "Aw, sweetheart,\nof course I do!"
      jo "You'll always be my boy,\nno matter what!"
      mc "Well, I need your\nhelp again..."
  jo "All right, I can give the [guard]\na call and have him come\nunlock the door."
  mc "Great! Please, hurry! I don't\nknow how long we'll survive."
  jo "...we?"
  $set_dialog_mode("phone_call_plus_textbox","jo")
  window auto
  "She didn't even react to the word \"survive.\" That just goes to show...{space=-20}"
  window hide
  $set_dialog_mode("phone_call_centered","jo")
  mc "[maya] is locked\nin here with me."
  jo "Oh, okay. Sit tight."
  jo "And [mc]?"
  mc "Yes?"
  jo "Don't do anything stupid...\nor, you know, unprotected."
  mc "Like, remembering to\nsafely eject my USB?"
  jo "I'm serious! I know how you\nhormonal teenagers can be."
  mc "Fine, fine! It's nothing like\nthat, okay? Just... thank\nyou for helping."
  jo "You're very welcome."
  play sound "end_call"
  $set_dialog_mode("")
  pause 0.5
  window auto
  "Man, that was more difficult than usual."
  "[jo] must be stressed after the fall break... or due to what happened before it."
  $quest.maya_witch.advance("wait")
  return

label quest_maya_witch_wait:
  show maya neutral with Dissolve(.5)
  mc "Okay, [jo] said she would call the [guard] and have him come unlock the door."
  mc "So, we should be out of here soon... as long as he's not waylaid\nby donuts."
  maya smile_hands_in_pockets "Cool. It must be nice to have support like that."
  maya smile_hands_in_pockets "Somebody on standby that you can call to ask for help whenever you need it."
  "Huh... I hadn't really thought of it like that, but I guess I am lucky to have [jo], even if she is hard on me sometimes."
  mc "You're right. Sometimes it's hard with her being the principal, but I know she cares."
  maya neutral "Anyway..."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "I suppose there's nothing to do but wait now."
  "Count down the ticking seconds until our liberation."
  "..."
  "There are definitely worse things than being trapped with a pretty girl in a room full of computers..."
  "And yet, I'm not sure what to do with myself."
  "I wonder what [maya] is up to, anyway? It's hard not to stare at her when her guard is down."
  "Or rather, not as high..."
  "..."
  "The way she sits there with her chin in her hand, her eyes on the screen..."
  "The subtle, slender curve to the back of her neck..."
  "That smell of crisp winter air and earthy wildflowers..."
  window hide
  show maya skeptical with Dissolve(.5)
  window auto
  maya skeptical "What?"
  mc "What, what?"
  maya skeptical "I could toast a piece of bread with the heat of your stare in the back of my neck."
  mc "Err, I wasn't staring..."
  maya confident "Right, and I'm a soap heiress with a loving family."
  mc "I was just curious what you were up to, okay?"
  maya smile "Oh, you know. Just placing a bid on a new pair of lungs."
  mc "..."
  maya eyeroll "That was a joke."
  maya smile_hands_in_pockets "It's actually a new kidney."
  mc "You're hilarious."
  maya smile_hands_in_pockets "What can I say? I like my humor how I like my coffee."
  mc "Black and scalding?"
  maya annoyed "I don't like coffee."
  mc "..."
  mc "I've seen you drink coffee before."
  maya cringe "So, you admit you're a stalker?"
  mc "I admit nothing of the sort!"
  maya dramatic "Well, if you must know, I have to pass the time somehow."
  maya dramatic "And time goes by fastest on the internet. One hour easily turns into five.{space=-85}"
  mc "Don't I know it..."
  maya sarcastic "Anyway, back to the black market."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "Hmm... maybe I really should do some homework to pass the time."
  "Video games sound much more appealing, though..."
  "..."
  "Fuck it, I'll just take a seat next to [maya] and—"
  window hide
  $school_computer_room["screen1"] = True
  show maya thinking with Dissolve(.5)
  $school_computer_room["sparks"] = True
  window auto show
  show maya afraid with dissolve2:
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  maya afraid "Eeep!"
  show maya afraid:
    yoffset 0
  mc "Ouch. Is my presence that revolting?"
  maya afraid "That's not it! Look!"
  window hide
  $school_computer_room["dark"] = False
  $school_computer_room["sparks"] = False
  $school_computer_room["fire"] = True
  play ambient "audio/sound/fire_burning.ogg" volume 0.5
  show location with vpunch
  pause 0.25
  window auto
  mc "Whoa! What the hell?!"
  "[maya] jumps to her feet as the flame shimmies its way up along the computer monitor."
  "The smell of burning plastic and electronic residue quickly fills the room."
  $school_computer_room["smoke"] = True
  "Smoke wafts up to the ceiling and spreads its fingers, reaching out in all directions."
  "There's a pop and a hiss and the fire seems to jump to life, growing bigger."
  maya afraid "Do something!"
  show maya afraid at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Like what?!\"":
      show maya afraid at move_to(.5)
      mc "Like what?!"
      maya dramatic "Oh, I don't know, settle in for a cozy nap?"
      maya dramatic "Put the fire out, of course!"
      mc "Do I look like a fireman to you?"
      $maya.lust-=1
      maya cringe "No, you look like a useless [mc]!"
      "Well, she's not completely wrong. I certainly feel useless right now."
      "Story of my life... even when the room is literally on fire."
      maya cringe "Ugh, move! I'll take care of it!"
      window hide
      pause 0.25
      $maya.unequip("maya_jacket")
      show maya cringe_jacket with Dissolve(.5)
      pause 0.25
      show maya cringe_jacket at move_to(.25)
      pause 0.75
      show maya cringe_jacket:
        xalign 0.25 yoffset 0
      show maya cringe_jacket:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      window auto
      "[maya] whips off her hoodie and uses it to start beating out the flame.{space=-15}"
      show maya cringe_jacket:
        xalign 0.25 yoffset 0
      show maya cringe_jacket:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      "I can't help but stare in admiration."
      show maya cringe_jacket:
        xalign 0.25 yoffset 0
      show maya cringe_jacket:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      "The way she jumps into action, the flex of her arms as she attempts{space=-5}\nto smother the fire."
      show maya cringe_jacket:
        xalign 0.25 yoffset 0
      show maya cringe_jacket:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      "The room is awash in a glow of orange and yellow light as the flames{space=-20}\nduck and dive beneath her onslaught."
      show maya cringe_jacket:
        xalign 0.25 yoffset 0
      show maya cringe_jacket:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      $school_computer_room["fire"] = False
      stop ambient
      "At first, it seems to be working..."
      window hide
      $school_computer_room["smoke"] = False
      $school_computer_room["fire_spread"] = True
      $school_computer_room["smoke_spread"] = True
      $school_computer_room["burns"] = True
      play ambient "audio/sound/fire_burning.ogg"
      show maya cringe_jacket:
        xalign 0.25 yoffset 0
      show maya cringe_jacket:
        parallel:
          easein 0.5 xalign 0.625
        parallel:
          easein 0.175 yoffset -10
          easeout 0.175 yoffset 0
          easein 0.175 yoffset -4
          easeout 0.175 yoffset 0
      show location with vpunch
      pause 0.25
      window auto
      "...but then the fire takes on a life of its own and stretches its reach towards the ceiling!"
      "It snaps greedily as it consumes more wire and plastic, jumping to the next computer over."
      window hide
      show maya fire_spread
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
#     show maya cringe_jacket at move_to(.375)
#     pause 0.75
#     show maya cringe_jacket:
#       xalign 0.375 yoffset 0
#     show maya cringe_jacket:
#       easein 0.175 yoffset -10
#       easeout 0.175 yoffset 0
#       easein 0.175 yoffset -4
#       easeout 0.175 yoffset 0
      window auto
      "Still, [maya] never relents."
      "She whips her sweater at the flames, coughing as the smoke hits her lungs."
      "And, somehow, the fire in her eyes burns stronger than the flames in front of her."
#     show maya cringe_jacket:
#       xalign 0.375 yoffset 0
#     show maya cringe_jacket:
#       easein 0.175 yoffset -10
#       easeout 0.175 yoffset 0
#       easein 0.175 yoffset -4
#       easeout 0.175 yoffset 0
      show maya fire with dissolve2
      "While not quite built as a firefighter, [maya] sure is a warrior in her own right."
      "Sweep after sweep, her determination never falters."
#     show maya cringe_jacket:
#       xalign 0.375 yoffset 0
#     show maya cringe_jacket:
#       easein 0.175 yoffset -10
#       easeout 0.175 yoffset 0
#       easein 0.175 yoffset -4
#       easeout 0.175 yoffset 0
      show maya fire_extinguished with dissolve2
      $school_computer_room["fire_spread"] = False
      $school_computer_room["ashes"] = True
      stop ambient
      "And eventually, the unruly inferno shrinks and dies."
      "And out of the smoke, singed and sooty, the girl that can't be charmed emerges."
      window hide
      show maya cringe_jacket at Transform(xalign=.625)
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
#     show maya cringe_jacket:
#       xalign 0.375 yoffset 0
#     show maya cringe_jacket at move_to(.625,.75)
#     pause 1.0
      window auto
      mc "That... was... impressive."
      maya cringe_jacket "Don't mention it."
      play sound "lock_click"
      "While we stand there in the aftermath of the crackling, smoking chaos,{space=-55}\nwe hear a soft click."
      maya annoyed "Are you fucking kidding me? Try the door now."
      "I approach the door with caution, like it could also catch on fire at any moment."
      "But when I reach it and press down on the handle, it happily gives way."
      mc "It's unlocked..."
      maya eyeroll_hands_down "Of course it is..."
      maya eyeroll_hands_down "Let's hurry up and fuck off before it changes its mind."
    "Pee on the fire":
      show maya afraid at move_to(.5)
      mc "Stand back!"
      maya afraid "What are you—"
      window hide
      play sound "unzipping_pants"
      pause 1.0
      $school_computer_room["pee"] = True
      play sound "<to 1.0>peeing" volume 0.33
      queue sound "<from 1.0 to 17.0>peeing" volume 0.33 loop
      show maya afraid:
        parallel:
          easein 0.5 xalign 0.625
        parallel:
          easein 0.175 yoffset -10
          easeout 0.175 yoffset 0
          easein 0.175 yoffset -4
          easeout 0.175 yoffset 0
      show location with vpunch
      pause 0.5
      window auto
      "Never in my wildest dreams did I imagine I would whip out my dick and pee in front of [maya]..."
      "Yet here I am, doing it to save her life."
      "From zero to hero, all in the blink of a golden shower."
      maya annoyed "Oh, my god. You're actually pissing on the fire."
      mc "Desperate times!"
      play sprite "audio/sound/fire_hiss.ogg" volume 0.33 noloop
      "The flames hiss in protest as they're met with my bodily fluid."
      mc "Heh, see?"
      maya smile "Oh, I see, all right."
      maya smile "That this place totally needs to be brought up to code on its sprinkler system."
      mc "It's working, isn't it?"
      "And to think I was just getting worried about designated piss corners..."
      maya neutral "You really should drink more water, [mc]."
      mc "Thanks for the analysis of my urine, I guess."
      maya eyeroll "Trust me, it's exactly what I wanted to do today."
      window hide
      $school_computer_room["fire"] = False
      $school_computer_room["smoke"] = False
      $school_computer_room["fire_spread"] = True
      $school_computer_room["smoke_spread"] = True
      $school_computer_room["burns"] = True
      stop ambient fadeout 0
      play ambient "audio/sound/fire_burning.ogg"
      show location with vpunch
      pause 0.25
      window auto show
      show maya annoyed with dissolve2
      mc "Fuck!"
      "Just as I'm emptying the last droplets of pee, the fire suddenly snarls to life again."
      "It flickers and then jumps to the next computer."
      maya annoyed "Don't stop! Keep peeing! I'm gonna try something!"
      window hide
      show maya annoyed at disappear_to_right
      pause 0.5
      show maya fire_spread mc at center
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "[maya] disappears from my line of vision, and all I can do is watch as the fire consumes the last of my pee."
      "But I can't let her down. Not now."
      "Come on, [mc]... just a little more..."
      "From the darkest depths of my soul, I summon the will and whatever{space=-20}\nmoisture left in my body."
      "And like a miraculous wellspring of urine, a new beam of golden yellow sizzles into the fire."
#     window hide
#     hide screen interface_hider
#     show black
#     show black onlayer screens zorder 100
#     with close_eyes
#     pause 0.5
#     show black onlayer screens zorder 4
#     $set_dialog_mode("default_no_bg")
      show maya fire mc with dissolve2
      "Everyone always told me I was full of shit, but in reality, I'm full\nof piss."
      "All those hours practicing my aim, practicing the power and flow..."
      "My time to shine is finally here."
      "My hips gyrate from side to side, firehosing the flames."
      "Nothing — and I mean nothing — can stay my spray."
      show maya fire_extinguished mc with dissolve2
      $school_computer_room["fire_spread"] = False
      $school_computer_room["ashes"] = True
      $school_computer_room["pee"] = False
      stop ambient
#     show black onlayer screens zorder 100
#     pause 0.5
#     hide black
#     hide black onlayer screens
#     show screen interface_hider
#     with open_eyes
#     pause 0.25
#     window auto
#     $set_dialog_mode("")
      "One by one, the flames choke on my urine, spitting and hissing angrily as they die."
      mc "Ha! Take that!"
      window hide
      stop sound fadeout 3.0
      show maya
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      play sound "lock_click"
      window auto
      "As I shake out the last drops, I hear a click and [maya] lets out a little whoop of victory."
      show maya sarcastic at appear_from_right(.625)
      maya sarcastic "Got it unlocked with my hairpin."
      "She grins at me, and the unprecedented success of our teamwork swells my chest with pride and happiness."
      maya dramatic "Come on, Piss King! Let's fuck off out of here before it happens again!"
      $quest.maya_witch["piss_king"] = True
  window hide
  if renpy.showing("maya eyeroll_hands_down"):
    pause 0.25
    $maya.equip("maya_jacket")
    pause 0.75
    show maya eyeroll_hands_down at disappear_to_right
  elif renpy.showing("maya dramatic"):
    show maya dramatic at disappear_to_right
  pause 0.5
  show black onlayer screens zorder 100 with Dissolve(.5)
  $school_computer_room["smoke_spread"] = False
  $school_computer_room["screen1"] = False
  $school_computer_room["screen2"] = False
  $game.location = "school_ground_floor"
  $mc["focus"] = ""
  $quest.maya_witch.advance("help")
  pause 0.25
  show maya bored at appear_from_left
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "Welp, I think I'm starting to believe you when you say you're cursed."
  maya bored "And all it took was spontaneous combustion?"
  mc "I'm serious! I've never seen a computer just catch fire like that out of nowhere."
  mc "I thought maybe someone could be playing tricks on you... but how could they have done that?"
  maya skeptical "I'm telling you — witches."
  mc "Maybe we should talk to [maxine]? She knows all about weird stuff like this."
  maya confident "I'll take just about any help at this point. I mean, you're here, aren't you?"
  mc "Fair enough!"
  mc "Though [maxine] can be a bit difficult to deal with. Do you want me to handle her?"
  maya bored "Yeah, sure. I appreciate it."
  maya bored "Later, [mc]."
  window hide
  show maya bored at disappear_to_right
  pause 1.0
  return

label quest_maya_witch_help:
  "There she is in her unnatural habitat, lying in wait. Ready to pounce on her next unsuspecting victim."
  "Yet, willfully, I go into her den of insanity once again. As suspecting as they come."
  window hide
  show maxine skeptical with Dissolve(.5)
  window auto
  maxine skeptical "Did you know?"
  mc "Know what?"
  maxine skeptical "About the thing in the school basement."
  mc "That sounds ominous... but the school has no basement."
  maxine eyeroll "Where do the stairs lead, then?"
  mc "What stairs?"
  maxine annoyed "You're not ready."
  mc "What's that supposed to mean?"
  maxine annoyed "Like an apple, you're still green and sour. You need to be ripe."
  mc "Ugh, I don't even know what we're talking about anymore!"
  maxine skeptical "So, it's safe to say that you did not, in fact, know."
  mc "Right, I know nothing."
  mc "Just like a certain winter bastard."
  "Too bad she'll never understand that reference."
  "Modern art is lost on her."
  maxine smile "It's good that you're becoming more self-aware."
  "That makes one of us..."
  mc "Anyway, I have a question."
  maxine neutral "Just one?"
  mc "At this moment in time, yes."
  mc "Do you—"
  maxine neutral "Is it for me?"
  mc "..."
  mc "Yes, [maxine]. That's why I'm here."
  maxine neutral "Surely, your purpose is greater than that?"
  mc "I'm not talking about my purpose on earth..."
  maxine smile "Neither was I."
  mc "Anyway! Do you know anything about curses?"
  maxine excited "Casting? Lifting? Containing? Selling? Distilling?"
  maxine excited "They {i}can{/} make excellent perfumes."
  maxine excited "Or maybe pickling? Not to be confused with cucum—"
  mc "Please stop."
  maxine concerned "It's your funeral."
  show maxine concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You know what? Nevermind.\"":
      show maxine concerned at move_to(.5)
      mc "You know what? Nevermind."
      mc "I'll just figure it out on my own."
      maxine thinking "Please do not cast any curses, hexes, jinxes, or otherwise, without supervision."
      mc "Whose supervision?"
      maxine thinking "A certified Hexalogist, obviously."
      mc "That's not a thing."
      maxine thinking "It wouldn't exist if it wasn't a thing."
      "I guess I don't know how to argue with that kind of circular logic..."
      maxine thinking "Says it right under Article II, subsection 1C, clause 592 that unsupervised{space=-110}\npractices are strictly forbidden."
      mc "...of what?"
      maxine thinking "The school handbook, of course."
      mc "Right. I must have missed it in between all the normal rules."
      maxine sad "You have to read in between the lines."
      mc "Of course you do."
      maxine sad "Invisible ink."
      mc "Right. Whatever."
      mc "No place is more magical than the internet, anyway."
      maxine afraid "Or more dangerous!"
      window hide
      $quest.maya_witch.advance("black_market")
      if maxine.location == game.location:
        hide maxine with Dissolve(.5)
      else:
        show maxine afraid at (disappear_to_left if game.location in ("school_ground_floor","school_ground_floor_west") else disappear_to_right)
        pause 0.5
      window auto
      "Pfft! Dangerous? I'm sure it will be just fine..."
      return
    "\"Is that a threat?\"":
      show maxine concerned at move_to(.5)
      mc "Is that a threat?"
      maxine eyeroll "It's the truth, if you play around with curses."
      mc "Pfft! It's curses that better not play around with me."
      $mc.strength+=1
      mc "I've been taking the gym more seriously. Look at these guns!"
      maxine annoyed "It's impossible to hit a curse. The necessary velocity can't be obtained.{space=-55}"
      mc "Well, that's why I'm asking for your help."
      maxine confident "I see. You were wise to seek my council."
      maxine confident "I'll gather them together and—"
      mc "Sorry, but it's kind of time sensitive..."
      maxine skeptical "You play a dangerous game, [mc]."
      mc "The most dangerous one."
      maxine confident_hands_down "I didn't realize you hunt."
      mc "I, err... I don't."
    "\"I'll let you lead the procession, then.\"":
      show maxine concerned at move_to(.5)
      mc "I'll let you lead the procession, then."
      maxine smile "Oh, no, if you're dead, then I'm already long gone."
      mc "...what is that even supposed to mean?"
      maxine smile "Exactly what I just said."
      mc "So, you know how I die?"
      maxine laughing "Don't be absurd! How could I know that?"
      maxine neutral "I know {i}when{/} you die."
      "If that's not enough to send a chill down the spine, I don't know what is..."
      mc "I don't like where this conversation is going."
      maxine neutral "You asked."
      mc "And I regret it."
  mc "Anyway, can you help me out or not?"
  maxine sad "I may have a mutually beneficial solution."
  if game.hour > 15:
    $timeframe = time_str(7,0) + " and " + time_str(8,0)
    $quest.maya_witch["absurd_timeframe"] = (7,8)
    maxine sad "I grant you permission to come to my office tomorrow between the hours of [timeframe]."
  else:
    $timeframe = time_str(game.hour+2,0) + " and " + time_str(game.hour+3,0)
    $quest.maya_witch["absurd_timeframe"] = (game.hour+2,game.hour+3)
    maxine sad "I grant you permission to come to my office between the hours of [timeframe]."
  mc "That's a tiny window..."
  maxine thinking "I'm a very busy woman, [mc]."
  maxine thinking "The secret popcorn dispenser isn't going to oil itself."
  "I'm not sure if that's some kind of euphemism, but knowing [maxine], probably not."
  "Still, I don't want to know."
  if game.hour > 15:
    mc "Very well. I'll meet you tomorrow within that absurd timeframe."
  else:
    mc "Very well. I'll meet you within that absurd timeframe."
  maxine thinking "It's a pretty standard time, actually."
  mc "Whatever you say."
  window hide
  $quest.maya_witch.advance("meet_up")
  if maxine.location == game.location:
    $mc["focus"] = "maya_witch"
    hide maxine with Dissolve(.5)
  else:
    show maxine thinking at (disappear_to_left if game.location in ("school_ground_floor","school_ground_floor_west") else disappear_to_right)
    pause 0.5
    $mc["focus"] = "maya_witch"
    $renpy.transition(Dissolve(.5),layer="screens")
    pause 0.5
  return

label quest_maya_witch_meet_up_school_homeroom_rope:
  $quest.maya_witch["pulled_up"] = True
  show location with vpunch
  "What the hell? Did [maxine] just pull up the rope?"
  "You've got to be kidding me..."
  return

label quest_maya_witch_meet_up:
  show maxine thinking with Dissolve(.5)
  maxine thinking "I may have something that can help you."
  mc "That's exactly why I'm here."
  maxine sad "But..."
  "Great. Of course there's a but."
  maxine thinking "First I need your assistance."
  maxine thinking "I've recently switched some of the hardware on my computer, and\nI need help bringing it to life again."
  mc "Have you tried plugging it in?"
  maxine afraid "No, no! It's not safe!"
  "I really should get her a tinfoil hat for Christmas..."
  mc "Well, how do you plan on using it, then?"
  maxine confident "That's where you come in."
  maxine confident "You see, I need my electricity kinetically generated to this generator and kept solely to this room."
  mc "Right, that makes perfect sense. Absolutely."
  mc "So, what do you want me to do?"
  maxine confident "You'll hook yourself up to these electrodes and create the power required."
  mc "And in return, you'll help me with the curse?"
  maxine skeptical "I'll give you the necessary tools and ensure you are not tried under Article II, subsection 1C, clause 592."
  mc "Tried? For what?"
  maxine angry "For practicing without the supervision of a certified Hexologist!"
  mc "Right, right!"
  "The things I go along with sometimes..."
  mc "Fine. You have a deal."
  maxine laughing "Excellent!"
  show maxine laughing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"So, I just run in place or what?\"":
      show maxine laughing at move_to(.5)
      mc "So, I just run in place or what?"
      maxine excited_electrodes "That's exactly right!"
      maxine excited_electrodes "Just remove your shirt and attach these electrodes to your chest and the sides of your head."
      window hide
      show maxine electrodes_run smile
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $mc["focus"] = ""
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "When I woke up this morning, I never thought I'd be doing physical labor in exchange for curse help."
      "Life really is full of unexpected twists sometimes."
      "Hopefully, this can help [maya] and I won't keel over before then."
      maxine electrodes_run smile "Whenever you're ready."
      mc "Okay, here goes nothing..."
      window hide
      pause 0.125
      show maxine electrodes_run smile jog with Dissolve(.5)
      pause 0.125
      window auto
      "I start with a light jog, my heart rate spiking as the generator gives a little hiss of life."
      maxine electrodes_run annoyed jog "Faster!"
      mc "All right, all right!"
      show maxine electrodes_run annoyed run with dissolve2
      "Against the weak protests of my aching lungs, I take the jog to a run.{space=-25}"
      "The sweat gathers at my temples and drips down my head."
      window hide
      pause 0.125
      show maxine electrodes_run annoyed run generator_lights with Dissolve(.5)
      pause 0.125
      window auto
      "The generator kicks into gear then and [maxine]'s computer flickers to life."
      mc "Why... am I... doing this... again...?"
      show maxine electrodes_run neutral run generator_lights with dissolve2
      maxine electrodes_run neutral run generator_lights "To generate power, of course."
      mc "But... why not... use... the school... computers...?"
      maxine electrodes_run neutral run generator_lights "Technology can never be trusted."
      mc "Whatever... you say..."
      show maxine electrodes_run neutral faster_run generator_lights with dissolve2
      "I pant from the excursion, my out of shape body screaming in protest.{space=-40}"
      "My skinny legs burn, but I know I'm running towards something."
      "I'm running towards [maya], towards being a better, more selfless person."
      "I would run up any hill, power any weird [maxine] experiment to help a friend."
      maxine electrodes_run excited faster_run generator_lights "That's it! Keep going!"
      mc "I am... {i}huff...{/} trying..."
      window hide
      pause 0.125
      show maxine electrodes_run excited faster_run lights with Dissolve(.5)
      pause 0.125
      window auto
      "I pant through the pain in my lungs, and watch as her computer actually blinks awake."
      mc "It's... actually... {i}huff...{/} working...?"
      maxine electrodes_run excited faster_run lights "Of course it's working! Run, [mc], run!"
      show maxine electrodes_run excited fastest_run lights with dissolve2
      mc "I'm going... {i}huff...{/} as fast... {i}puff...{/} as I can...!"
      maxine electrodes_run neutral fastest_run lights "Your physical stamina is a bit worrisome. You should probably have that checked."
      mc "Gee... {i}huff...{/} thanks..."
      maxine electrodes_run smile fastest_run lights "You're welcome."
      "As I go, the generator picks up speed and the computer lights up\nin full."
      mc "I can't... {i}huff...{/} go... any... {i}puff...{/} more...!"
      maxine electrodes_run smile fastest_run lights "That will be sufficient for today."
      mc "Thank... {i}huff...{/} god..."
      $unlock_replay("maxine_solo_exercise")
      window hide
      show maxine smile
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "I feel ready to pass out as [maxine] unhooks me, but somehow I manage to stay on my feet."
    "?maxine.lust>=8@[maxine.lust]/8|{image=maxine contact_icon}|{image=stats lust_3}|\"Actually, how about you and I generate the power together?\"":
      show maxine laughing at move_to(.5)
      mc "Actually, how about you and I generate the power together?"
      maxine concerned "Hmm... that would generate the sort of lasting electricity I need... and twice as fast..."
      mc "Err, exactly what I was thinking."
      maxine excited "Good idea, [mc]."
      mc "Soooo, do you think we could maybe f—"
      maxine excited "The act of intercourse is actually quite the exercise. It should be more than sufficient."
      mc "Agreed! I mean, I guess. Sure."
      maxine flirty "Very well. Let's commence."
      maxine flirty "Remove your clothing, please."
      "Seriously? Is [maxine] as horny as I am? Or is she really only doing this for science?"
      "Either way, who am I to stand in the way of the advancement of\nour species?"
      window hide
      pause 0.25
      $maxine.unequip("maxine_hat")
      pause 0.75
      window auto
      "I strip down as fast as I can, and I watch her do the same, but\nwith controlled precision."
      window hide
      pause 0.25
      $maxine.unequip("maxine_sweater")
      pause 0.75
      window auto
      "She sheds her sweater, and I take the moment to appreciate\nher body."
      window hide
      pause 0.25
      $maxine.unequip("maxine_necklace")
      pause 0.75
      window auto
      maxine flirty "The more freely our sweat can flow, the better."
      window hide
      pause 0.25
      $maxine.unequip("maxine_black_bra")
      pause 0.75
      window auto
      "Goddamn! This is actually happening!"
      window hide
      pause 0.25
      $maxine.unequip("maxine_skirt")
      pause 0.75
      window auto
      mc "I definitely agree..."
      window hide
      pause 0.25
      $maxine.unequip("maxine_black_panties")
      pause 0.75
      window auto
      maxine flirty "Then, we will hook up the electrodes and let the electricity flow."
      window hide
      pause 0.25
      show maxine excited_electrodes with Dissolve(.5)
      pause 0.25
      window auto
      "She comes closer to me, her eyes on mine, lips slightly parted."
      "She looks so kissable, so cute and flushed with the excitement\nof what's happening."
      "I lean in towards her, but instead of a kiss or tender touch, she places the electrodes on my chest and temples."
      maxine excited_electrodes "Perfect."
      window hide
      show maxine electrodes_sex surprised
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $mc["focus"] = ""
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Then, she does the same to herself before taking a seat on her desk.{space=-20}"
      "She spreads her legs and I see that her pussy is already moist."
      "Whether it's from me or her little experiment, the sight makes me fully erect."
      window hide
      pause 0.125
      show maxine electrodes_sex lip_bite grabbing_dick with Dissolve(.5)
      pause 0.125
      window auto
      "I stand in front of her, and before I can quite comprehend what's happening, [maxine] reaches down and takes hold of my dick."
      "Her hand is slightly rough — probably from all the science stuff she gets up to — but warm."
      "The sensation makes my skin tingle and my abdomen clench."
      window hide
      pause 0.125
      show maxine electrodes_sex lip_bite lining_dick_up with Dissolve(.5)
      pause 0.125
      window auto
      "Slowly, she's lining me up with her pussy, the tip of my head sliding along her opening."
      "She grips me harder as she guides me into herself."
      window hide
      pause 0.125
      show maxine electrodes_sex concerned inserting_dick with Dissolve(.5)
      pause 0.125
      window auto
      maxine electrodes_sex concerned inserting_dick "Mmmm..."
      "I'm met with some resistance at first — she's surprisingly tight."
      window hide
      pause 0.125
      show maxine electrodes_sex startled full_penetration with Dissolve(.5)
      pause 0.125
      window auto
      "But I take hold of her thighs and use them as leverage as I thrust myself the rest of the way in."
      "I sink deep into her warm, wet hole and then just hold myself there for a moment as she squeezes me."
      "I feel her trembling against me, her quick little breaths tickling the side of my neck."
      "When I bury my face in her own neck, the faint smell of incense and sweat pervades my nostrils."
      window hide
      pause 0.125
      show maxine electrodes_sex startled partial_penetration with Dissolve(.1)
      show maxine electrodes_sex startled no_penetration with Dissolve(.1)
      show maxine electrodes_sex startled partial_penetration with Dissolve(.1)
      show maxine electrodes_sex eyes_closed full_penetration with vpunch
      pause 0.125
      window auto
      "Then, remembering why we're here, I pull my hips back slightly and slam my dick into her again, hard."
      maxine electrodes_sex eyes_closed full_penetration "Oooh!"
      window hide
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration with Dissolve(.075)
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration with Dissolve(.075)
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration with Dissolve(.075)
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration with Dissolve(.075)
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration with vpunch
      pause 0.025
      show maxine electrodes_sex excited full_penetration generator_lights with Dissolve(.5)
      pause 0.125
      window auto
      "I piston in and out of her, the friction of her pussy reluctantly releasing me sends shivers of pleasure down my spine."
      "The generator bucks into life then and its whir fills the room."
      maxine electrodes_sex excited full_penetration generator_lights "T-that's it! Faster!"
      window hide
      pause 0.125
      show maxine electrodes_sex excited partial_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration generator_lights with vpunch
      pause 0.025
      show maxine electrodes_sex excited partial_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration generator_lights with vpunch
      pause 0.075
      show maxine electrodes_sex excited partial_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration generator_lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration generator_lights with vpunch
      pause 0.025
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with Dissolve(.5)
      pause 0.125
      window auto
      "I give into the sound of her cries and happily obey, thrusting myself into her faster and faster."
      "The table shakes beneath us even as [maxine] grips the edge of it for support as I hammer into her."
      "The air is now thick with the salty tang of our sweat as it drips down{space=-10}\nour bare bodies."
      "Her pussy juices soak my cock as I speed up into long, deep strokes.{space=-20}"
      window hide
      pause 0.125
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.125
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration generator_lights gripping_table with vpunch
      pause 0.025
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.5)
      pause 0.125
      window auto
      mc "Is that... {i}huff...{/} fast enough... {i}puff...{/} for you?"
      maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table "Mmm, yes! Don't stop! P-please, don't stop!"
      "Her words come out in ragged, desperate pants. I like to think that it's me she's begging for now, even if it's not the case."
      window hide
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with vpunch
      pause 0.175
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.175
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.125
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.175
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.175
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.175
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.2)
      pause 0.35
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed no_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed partial_penetration generator_lights gripping_table with Dissolve(.05)
      show maxine electrodes_sex eyes_closed full_penetration generator_lights gripping_table with vpunch
      pause 0.025
      show maxine electrodes_sex excited full_penetration gripping_table lights with Dissolve(.5)
      pause 0.125
      window auto
      "Willingly, I do as she asks. I go from slow to fast again, slamming into her tight little flower as it opens more and more for me."
      "Out of the corner of my eye, I see the computer flash to life."
      "It seems to spur [maxine] on and she begins to gyrate into me, with me.{space=-45}"
      window hide
      pause 0.125
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration gripping_table lights with Dissolve(.075)
      pause 0.075
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration gripping_table lights with Dissolve(.075)
      pause 0.075
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration gripping_table lights with Dissolve(.075)
      pause 0.125
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration gripping_table lights with Dissolve(.075)
      pause 0.075
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited no_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited partial_penetration gripping_table lights with Dissolve(.075)
      show maxine electrodes_sex excited full_penetration gripping_table lights with vpunch
      pause 0.025
      show maxine electrodes_sex eyes_closed full_penetration gripping_table lights juices with Dissolve(.5)
      pause 0.125
      window auto
      "We're in sync now, our hearts hammering together, our breaths mingling, one's sweat slicking the other."
      "Her juices leak out of her and coat the table beneath her, and the musky odor clogs my senses."
      "We've nearly reached full power and I can feel the climax approaching{space=-35}\nfor us both."
      "She gives a little moan and her pussy walls squeeze me tight."
      window hide
      pause 0.125
      show maxine electrodes_sex eyes_closed partial_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration gripping_table lights juices with vpunch
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration gripping_table lights juices with vpunch
      pause 0.075
      show maxine electrodes_sex eyes_closed partial_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed no_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed partial_penetration gripping_table lights juices with Dissolve(.075)
      show maxine electrodes_sex eyes_closed full_penetration gripping_table lights juices with vpunch
      pause 0.025
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.5)
      pause 0.125
      window auto
      mc "F-fuck, I'm getting close!"
      maxine electrodes_sex pleading full_penetration gripping_table lights juices "Just... mmmm... a little more..."
      mc "Yeah? Want me to give it to you?"
      maxine electrodes_sex pleading full_penetration gripping_table lights juices "Y-yes! Keep going! Don't stop!"
      mc "I'm not... gonna stop..."
      window hide
      pause 0.125
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.075
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex pleading partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm full_penetration gripping_table lights juices with vpunch
      pause 0.125
      window auto
      maxine electrodes_sex orgasm full_penetration gripping_table lights juices "Ohhhh!"
      "I bottom out against her cervix and my whole body stiffens."
      "Her head falls back and her eyes flutter shut as her orgasm hits just before mine."
      "I can feel it rolling through every inch of her as she shakes and shudders against me."
      window hide
      pause 0.125
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm full_penetration gripping_table lights juices with Dissolve(.05)
      pause 0.025
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm no_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm partial_penetration gripping_table lights juices with Dissolve(.05)
      show maxine electrodes_sex orgasm full_penetration gripping_table lights juices cum with vpunch
      pause 0.125
      window auto
      mc "Uhhhhhhh!"
      "Unable to withdraw — and with no desire to —, I explode deep within [maxine]."
      "I fill her up as I come hard, releasing a steady stream of my hot cum deep into her womb."
      "There's something so hot, so primal, about coming inside of her..."
      "Everything for science, and this is the most basic, instinctual science there is."
      "A man filling a woman with his seed. Survival of the human race."
      "Not that I want to get her pregnant, but... damn if that need to stay buried deep within her doesn't drown out all other thoughts."
      window hide
      pause 0.125
      show maxine electrodes_sex aftermath no_penetration lights juices cum with Dissolve(.5)
      pause 0.125
      window auto
      "I groan as my stream turns to spurts and the convulsions stop racking through me."
      "Our hearts are still galloping as I finally pull out of her, and the computer pulses with life now."
      mc "H-how's that... for kinetic energy...?"
      "Her breathing is still uneven and her glasses are slightly fogged over, but she offers me a smile."
      maxine electrodes_sex aftermath no_penetration lights juices cum "Exactly what I needed..."
      "Me too, it turns out."
      $unlock_replay("maxine_pair_exercise")
      window hide
      show maxine smile
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      $maxine.outfit = {"hat":"maxine_hat", "glasses":"maxine_glasses", "necklace":"maxine_necklace", "shirt":"maxine_sweater", "bra":"maxine_black_bra", "pants":"maxine_skirt", "panties":"maxine_black_panties"}
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "[maxine] finishes catching her breath and removes our electrodes before getting dressed, back to business as usual."
      "I follow suit and pull my own clothes back on."
  mc "So, about that tool you said could help me?"
  maxine laughing "Ah, yes! There's nothing like a good book to help one in need."
  mc "What kind of book?"
  maxine smile "It's a spell book. Perfect for all your casting, lifting, containing, selling, distilling, and/or pickling needs."
  maxine smile "Here you go."
  window hide
  $mc.add_item("bippity_boppity_bitch")
  pause 0.25
  window auto
  mc "A spell book? Really?"
  maxine smile "I've read it three times. It's very good."
  "I suppose I did come here asking about curses..."
  "What other option do I have, anyway?"
  mc "Very well. I'll give it a try."
  maxine laughing "Have fun!"
  mc "Thanks, I guess?"
  window hide
  hide maxine with Dissolve(.5)
  window auto
  "I can't believe I'm actually going to try breaking a curse with this dusty old thing."
  "It's all fun and games until you're in a face-off with an ancient witchy spell or something."
  "This is how you fall deeper into the rabbit hole."
  "..."
  "But if it helps [maya]..."
  "I just need to read through this thing, and probably give her some space."
  "After all, she's not the most social type, and I basically stalked her the entire day."
  "Those who wait for something good, and all that jazz."
  window hide
  $quest.maya_witch.finish()
  return

label quest_maya_witch_black_market_home_bedroom_computer:
  "Who needs [maxine] when the internet is more reliable and coherent?"
  "I'm sure there's all kinds of books on how to deal with this crap."
  "Let's see here... I just need to get that shady site pulled up..."
  "..."
  "..."
  "Ah, perfect! Here we go."
  jump goto_home_computer

label quest_maya_witch_black_market:
  show screen black_market(order_shipped="hex_vex_and_texmex")
  "One anti-hex, magical spell recipe book coming right up!"
  "..."
  "I may have just gipped myself big time."
  "Or I'm about to swoop in and be the hero [maya] didn't want but maybe needs."
  "Either way, time to wait for the delivery..."
  $quest.maya_witch.advance("package")
  return
