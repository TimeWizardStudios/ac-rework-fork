image flora_bonsai_vines = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-211,-76),Transform(LiveComposite((1920,1080), (-186,0),"school nurse_room nurse_wall", (637,248),"school nurse_room bookshelf_center", (831,276),"school nurse_room skeleton", (958,540),"school nurse_room bin", (1071,272),"school nurse_room beach", (1084,369),"school nurse_room rainbow", (1097,447),"school nurse_room bed", (0,0),"school nurse_room vines bigtree_strawberry", (844,331),"school nurse_room vines flora", (197,189),"school nurse_room vines flora_leftarm_vine_strawberry", (816,433),"school nurse_room vines flora_leftleg_vine_strawberry", (717,440),"school nurse_room vines flora_torso_vine_strawberry", (1106,414),"school nurse_room vines flora_rightarm_vine_strawberry", (1160,491),"school nurse_room vines flora_rightleg_vine_strawberry"),size=(480,270))),"ui circle_mask"))


label quest_maya_quixote_start:
  show flora annoyed with Dissolve(.5)
  flora annoyed "What are you doing here?"
  mc "..."
  mc "I live here."
  flora confused "You do? Since when?"
  mc "Very funny."
  flora annoyed "Can you move out or something?"
  mc "Why?"
  flora eyeroll "A friend of mine will be staying over for a few days."
  flora eyeroll "And I don't want you hanging around being weird about it."
  show flora eyeroll at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You have friends?\"":
      show flora eyeroll at move_to(.5)
      mc "You have friends?"
      $flora.love-=1
      flora annoyed "You're one to talk."
      mc "I do have friends!"
      flora annoyed "Your xCube trolls don't count."
      mc "I have, err... a friend."
      flora sarcastic "Your imaginary one from when we were little doesn't count either."
      # "Ugh, you try to burn someone and they just throw it right back at you..."
      "Ugh, you try to burn someone and they just throw it right back at you...{space=-60}"
      mc "Whatever."
    # "?quest.flora_bonsai.actually_finished@|{image=flora_bonsai_vines}|\"Does [jo] know about this?\"":
    "?quest.flora_bonsai.actually_finished@|{image=flora_bonsai_vines}|\"Does [jo] know\nabout this?\"":
      show flora eyeroll at move_to(.5)
      mc "Does [jo] know about this?"
      flora thinking "Of course she does."
      flora thinking "I tell her everything."
      mc "{i}Everything?{/}"
      mc "Even about that time with the vines and the—"
      if quest.flora_bonsai["hug"]:
        $flora.love+=1
      else:
        $flora.lust+=1
      flora blush "Okay! Maybe not everything!"
      mc "I didn't think so."
    "\"I'm never weird.\"":
      show flora eyeroll at move_to(.5)
      mc "I'm never weird."
      flora annoyed "I've lived with you all my life."
      flora annoyed "You're as weird as they come."
      mc "In an endearing sort of way, though, right?"
      flora confused "If by \"endearing\" you mean downright smotherable... then yes."
      mc "Sounds hot. I'd let you smother me."
      $flora.lust-=1
      flora cringe "See? Case in point!"
      mc "I'm kidding!"
  flora neutral "Anywho..."
  flora neutral "Do you think you can be cool and stay out of our way?"
  mc "I'm the coolest."
  flora angry "I'm serious!"
  mc "Who even is this friend, anyway?"
  flora excited "It's [maya]."
  if quest.mrsl_table.actually_finished:
    "Oh? I guess that makes sense."
    "It's not like [flora] hasn't had her over before..."
    "...and that went horribly."
    "But maybe it'll be better this time?"
  mc "Why?"
  flora angry "It's none of your business."
  mc "It's my house, too. I have a right to know."
  flora concerned "Ugh, fine! She's just having some trouble with her current living situation, okay?"
  flora concerned "I don't want to see you bugging her about it."
  mc "I wouldn't dream of it..."
  flora concerned "I don't want to know what sort of things you'll be dreaming about, either."
  mc "[flora]! Don't make it dirty!"
  flora angry "I mean it, [mc]. Just be cool about this."
  window hide
  hide flora with Dissolve(.5)
  window auto
  "Well, this should be interesting..."
  "How many times have I fantasized about something like this?"
  # "Teenage girl sleepovers... them in their panties and oversized t-shirts for pjs... sexy pillow fights... late night secrets..."
  "Teenage girl sleepovers... them in their panties and oversized t-shirts{space=-25}\nfor pjs... sexy pillow fights... late night secrets..."
  "..."
  "This might be the closest I'll ever get to a threesome."
  "And honestly, I can't wait."
  "I need to make sure I'm well rested and in tip top stamina shape!"
  window hide
  $quest.maya_quixote.start()
  return

label quest_maya_quixote_arrival:
  "I wonder what [jo] has made for breakfast today..."
  "..."
  "Welp. It looks like a big heaping bowl of nothing."
  "I guess I'll just have to—"
  window hide
  show maya bored at appear_from_left
  pause 0.5
  window auto
  maya bored "What's cooking, [mc]?"
  mc "Jesus Christ!"
  # maya dramatic "Thanks, but I'm not into that whole body of Christ cannibalism thing."
  maya dramatic "Thanks, but I'm not into that whole body of Christ cannibalism thing.{space=-15}"
  mc "No! I meant, you scared the crap out of me."
  maya sarcastic "Ah, yes! One of my favorite effects on people."
  maya sarcastic "Something about my inviting disposition."
  mc "Err, right. Something like that."
  # mc "I didn't expect you to be in here. I could have been in my underwear or something."
  mc "I didn't expect you to be in here. I could have been in my underwear or{space=-45}\nsomething."
  # maya blush "Thank god you weren't! I don't know if I would've been able to control myself!"
  maya blush "Thank god you weren't! I don't know if I would've been able to control{space=-35}\nmyself!"
  mc "I know. It would have been very embarrassing."
  maya blush "And appetite murdering!"
  maya blush "I have a very healthy appetite, you know?"
  "God, she does look like she could devour a person's soul whole."
  "There's something incredibly hot and enticing about that..."
  maya confident "So, are you going to fry my bacon or what?"
  show maya confident at move_to(.25)
  menu(side="right"):
    extend ""
    # "?mc.owned_item('cat_tail')@|{image=items cat_tail}|\"Actually, I think there's something we should talk about.\"":
    "?mc.owned_item('cat_tail')@|{image=items cat_tail}|\"Actually, I think there's something{space=-5}\nwe should talk about.\"":
      show maya confident at move_to(.5)
      mc "Actually, I think there's something we should talk about."
      mc "Last time you were here, to be specific."
      maya thinking "Hmm... I don't recall it."
      mc "Sure you do. You got me into trouble, remember?"
      maya kiss "You'll have to elaborate on that."
      maya kiss "I seem to cause trouble wherever I go."
      "Now that I can believe..."
      mc "You clogged the bathroom sink with that... tail of yours."
      mc "And [jo] thought I did it."
      maya concerned "Ah. That."
      mc "Yeah. That."
      mc "The whole bathroom was a mess."
      maya sad "I was upset, okay?"
      mc "About what?"
      maya sad "Just... stuff at home."
      mc "Is it bad?"
      maya bored "It's whatever."
      mc "..."
      mc "How did it even end up in the sink?"
      maya skeptical "I was cleaning it and forgot about it."
      maya skeptical "Can I have it back, please?"
      mc "Since you're asking so nicely..."
      maya confident "I'm nothing if not nice!"
      mc "It's a start, I suppose."
      mc "Here you go."
      window hide
      $mc.remove_item("cat_tail")
      pause 0.25
      window auto show
      show maya blush with dissolve2
      $maya.lust+=3
      maya blush "Thanks!"
    "\"Are you coming onto me right now?\"":
      show maya confident at move_to(.5)
      mc "Are you coming onto me right now?"
      maya dramatic "[mc], please! I take bacon very seriously!"
      maya dramatic "It's one of my four main food groups!"
      mc "Oh? What are the other three?"
      maya sarcastic "Dick, dickhole, and spotted dick."
      mc "Luckily for you, we happen to have all three."
      maya sarcastic "It is very nutritious."
      $maya.love+=1
      maya sarcastic "I'm glad you know what's good for you."
      mc "Heh. Likewise."
    "\"Of course! Tell me how you like it.\"":
      show maya confident at move_to(.5)
      mc "Of course! Tell me how you like it."
      maya dramatic "Hot and dripping. How else?"
      mc "..."
      mc "You're talking about the bacon, right?"
      $maya.lust-=1
      maya cringe "Of course I'm talking about the bacon."
      maya cringe "I only ever think and say pure and wholesome things."
      maya cringe "I'm an angel, don't you know?"
      mc "I bet you are..."
    "\"Make your own bacon.\"":
      show maya confident at move_to(.5)
      mc "Make your own bacon."
      maya sarcastic "How do you think I can afford these shoes?"
      $maya.lust+=1
      maya sarcastic "Suck dick. Bring home the bacon."
      mc "Uh, gross."
      maya cringe "What? Is that not how you do it?"
      maya cringe "You certainly look like you've got the lips for it."
      mc "Ugh! Stop!"
      mc "Put some bacon in your mouth and be quiet for once!"
      maya dramatic "I simply do not know the meaning of the word!"
      mc "Of course you don't..."
  mc "So, how long do you think you'll be staying here?"
  maya annoyed "Until the cops stop looking for me."
  mc "I knew it!"
  "[maya] does look like she'd be right at home in a pair of handcuffs."
  "I bet she and [flora] could get up to all sorts of trouble."
  "Yummy..."
  maya smile "It's fine. I've gotten away with murder before."
  mc "I bet you have."
  # maya eyeroll "It's whatever, honestly. I was just kicked out of the house for a while."
  maya eyeroll "It's whatever, honestly. I was just kicked out of the house for a while.{space=-20}"
  maya eyeroll "No big deal, really."
  mc "Ah, and [flora] is hiding you?"
  mc "Well, mi casa es su casa!"
  maya neutral "[flora]'s casa es su casa, you mean?"
  mc "Yeah, that sounds more accurate."
  window hide
  if flora.location == "home_kitchen":
    show maya neutral at move_to(.25)
    show flora cringe at Transform(xalign=.75) with Dissolve(.5)
    window auto
    flora cringe "What did I tell you, [mc]?!"
    mc "Err, lots of things?"
    mc "It's hard to keep up with everything..."
    flora annoyed "Is he bothering you?"
    maya smile_hands_in_pockets "Eh, I'll allow it as part of my mandated charity."
    flora sarcastic "That is very generous."
    flora sarcastic "Come on, [maya]! Let's go!"
    window hide
    show flora sarcastic at disappear_to_left
    pause 0.25
    show maya smile_hands_in_pockets at disappear_to_left behind flora
  else:
    show flora cringe flip at appear_from_left(.25) behind maya
    show maya neutral:
      ease 1.0 xalign 0.75
    pause 0.5
    window auto
    flora cringe flip "What did I tell you, [mc]?!"
    mc "Err, lots of things?"
    mc "It's hard to keep up with everything..."
    flora annoyed flip "Is he bothering you?"
    maya smile_hands_in_pockets "Eh, I'll allow it as part of my mandated charity."
    flora sarcastic flip "That is very generous."
    flora sarcastic flip "Come on, [maya]! Let's go!"
    window hide
    show maya smile_hands_in_pockets at disappear_to_left
    pause 0.25
    show flora sarcastic flip at disappear_to_left
  pause 0.25
  window auto
  "Ugh, girls. So catty..."
  "I'll just go play with myself, as usual."
  "..."
  "Games, I mean. Video games by myself."
  $quest.maya_quixote.advance("xCube")
  return

label quest_maya_quixote_xCube:
  "Finally, some peace and quiet in my own r—"
  window hide
  show flora neutral at Transform(xalign=.25)
  show maya neutral at Transform(xalign=.75)
  with Dissolve(.5)
  window auto
  mc "What the hell?!"
  mc "How did you get in my room?"
  maya smile "Let's see... we used our feet to walk here, then we opened the door, and I'm pretty sure that's how we got here."
  flora excited "Yep. That's exactly how I remember it."
  mc "You know what I mean! Why are you in here?"
  flora confident_hands_concealed "Well, we were bored."
  mc "And how is that my problem?"
  flora angry "Don't be rude! We have a guest!"
  flora concerned "Anyway, we decided to play on your xCube."
  mc "..."
  mc "I told you not to touch my things without permission."
  maya annoyed "We did ask!"
  maya annoyed "The xCube said it was fine. Consent was obtained."
  mc "I meant my consent!"
  flora concerned "Chill out, [mc]. Don't be such a drama."
  mc "I'm not being—"
  maya eyeroll "I, for one, am inspired by your take on drama queens."
  mc "Ugh, whatever!"
  mc "I guess it's fine. Maybe we can all play."
  maya thinking "So, about that..."
  maya thinking "It's not working anymore, unfortunately."
  mc "..."
  mc "Did you two break it?"
  maya afraid "I didn't say that!"
  maya concerned "...but yes."
  mc "Damn it, [flora]! This is why you're not allowed to touch my things!"
  flora angry "It wasn't my fault!"
  show flora angry at move_to("left")
  show maya concerned at move_to("right")
  menu(side="middle"):
    extend ""
    # "\"Oh? So, I suppose the destruction fairies did it?\"":
    "\"Oh? So, I suppose the\ndestruction fairies did it?\"":
      show flora angry at move_to(.25)
      show maya concerned at move_to(.75)
      mc "Oh? So, I suppose the destruction fairies did it?"
      flora thinking "..."
      flora thinking "Maybe."
      mc "Seriously? Can't you ever take responsibility for stuff?"
      maya dramatic "How do you know it wasn't destruction fairies?"
      mc "How about because they don't exist?"
      maya sarcastic "Prove it."
      mc "Ugh! No!"
      mc "..."
      mc "I guess I have to find a way to fix this, now..."
      mc "You two can get out."
      flora worried "God, it's not like we killed your dog or something, okay?"
      flora worried "Let's go, [maya]."
      window hide
      show maya sarcastic at disappear_to_left
      pause 0.25
      show flora worried at disappear_to_left
      pause 0.25
      window auto
      "Well, this is just great."
      "Now I need to find some other way to wind down and relax..."
      "..."
      "Maybe a nice, hot shower?"
    # "?flora.love>=8@[flora.love]/8|{image=flora contact_icon}|{image=stats love_3}|\"It's okay. I'm sure I can fix it.\"":
    "?flora.love>=8@[flora.love]/8|{image=flora contact_icon}|{image=stats love_3}|\"It's okay. I'm\nsure I can fix it.\"":
      show flora angry at move_to(.25)
      show maya concerned at move_to(.75)
      mc "It's okay. I'm sure I can fix it."
      flora worried "Oh? Err, okay."
      flora worried "I'm sorry we broke it. It really was an accident."
      mc "I know. Technology can be quite fickle."
      flora blush "And I'm sorry for not asking for permission."
      flora blush "Next time I'll make sure to."
      mc "Thank you. I appreciate it."
      $flora.love+=1
      $mc.love+=1
      maya dramatic "Aw, what an anticlimactic, unexpectedly sweet resolution."
      maya dramatic "How sickening."
      flora laughing "Whatever you say!"
      flora laughing "Come on, [maya]. Let's go hang out in my room instead."
      window hide
      show maya dramatic at disappear_to_left
      pause 0.25
      show flora laughing at disappear_to_left
      pause 0.25
      window auto
      "Well, that's a bummer."
      "Now I need to find some other way to wind down and relax..."
      "..."
      "Maybe a nice, hot shower?"
    "\"Are you blaming your guest, then?\"":
      show flora angry at move_to(.25)
      show maya concerned at move_to(.75)
      mc "Are you blaming your guest, then?"
      maya dramatic "Who? Me?"
      mc "Well, you are her guest, aren't you?"
      maya sarcastic "Guest. Stowaway. Partner in xCube-related-crime."
      flora eyeroll "Accidents happen, you know, [mc]?"
      mc "I do know. That's why you exist."
      $flora.love-=1
      flora annoyed "Oh, please! I'm obviously [jo]'s favorite."
      mc "I wouldn't be so sure..."
      mc "Anyway, now I have to find a way to fix the xCube. Thanks a lot."
      flora sarcastic "You're welcome."
      maya sarcastic "Let's ditch this popsicle stand?"
      flora sarcastic "Let's go!"
      window hide
      show maya sarcastic at disappear_to_left
      pause 0.25
      show flora sarcastic at disappear_to_left
      pause 0.25
      window auto
      "Well, this is just great."
      "They're really spiking my blood pressure."
      "Now I need to find some other way to wind down and relax..."
      "..."
      "Maybe a nice, hot shower?"
  $quest.maya_quixote.advance("shower")
  return

label quest_maya_quixote_shower:
  "The house is feeling a lot more crowded than usual."
  "Not that I don't like [maya]... but some me time will do me good."
  "Just me, the shower, and my di—"
  window hide
  $maya["outfit_stamp"] = maya.outfit
  $maya.outfit = {"dress":"maya_towel"}
  show maya neutral with Dissolve(.5)
  window auto show
  show maya annoyed with dissolve2
  maya annoyed "[mc]? What are you doing in here?!" with vpunch
  mc "Oh, shit! I'm really sorry!"
  mc "I, err... I didn't know anyone was in here..."
  maya annoyed "Actually, it's a good thing that you are here."
  maya annoyed "I can't get the shower to work."
  mc "Oh? Err, I guess I can take a look..."
  if quest.mrsl_table.actually_finished:
    mc "...as long as you're not here to trash the bathroom again."
    maya smile "I make no promises."
    show maya smile at move_to(.25)
    menu(side="right"):
      extend ""
      "\"I mean it. Else I'm leaving.\"":
        show maya smile at move_to(.5)
        mc "I mean it. Else I'm leaving."
        $maya.lust+=1
        maya smile "All right, all right!"
        maya smile "On my honor as a house guest — no mess making."
        mc "Thank you..."
      "\"Good enough, I guess.\"":
        show maya smile at move_to(.5)
        mc "Good enough, I guess."
        mc "I don't want to impede your comfortability as our guest."
        $maya.lust-=1
        maya eyeroll "Ah, yes. I ache for destruction wherever I go."
        mc "I didn't say that!"
        maya annoyed "It's fine. I just had an off day."
        mc "Hey, I get it."
  maya thinking "I must say, it is kind of nice having such a private bathroom."
  maya thinking "And hot water that doesn't run out after five minutes."
  mc "...aren't most bathrooms private?"
  maya concerned "You would think so, but foster homes tend to get crowded."
  maya concerned "Or, at least, the ones I've been in."
  mc "Ah. Fuck. I'm sorry."
  maya thinking "For what?"
  maya afraid "Unless you're the one who brutally murdered my parents?!"
  mc "Oh, god. Were your parents—"
  maya neutral "Nah. They were eaten by sharks."
  mc "I'm... I'm really sorry to hear that..."
  maya smile "Just kidding! They actually started a demonic sex cult, and then left me on the step of a satanic church."
  mc "..."
  mc "What really happened to them, [maya]?"
  maya annoyed "That is what really happened."
  "Hmm... she really does look completely serious..."
  "...not that that means much when it comes to her."
  mc "Err, I see."
  maya annoyed "You haven't seen anything until you've seen your mother suck the toe of a dark priest."
  show maya annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    # "\"I'm sorry you had to grow up with that...\"":
    "\"I'm sorry you had to\ngrow up with that...\"":
      show maya annoyed at move_to(.5)
      mc "I'm sorry you had to grow up with that..."
      maya smile "Alas! A life of woe builds character."
      mc "Well, you certainly have a lot of that."
      maya smile "I know, right? So much woe..."
      mc "Ha! I meant character!"
      maya thinking "Right, right."
      maya thinking "I'm orphan Fannie on crack."
      mc "Pfft! Orphan Fannie wishes she was you."
      mc "You're much cooler than her."
      maya flirty "Ever the sweet talker, aren't you?"
      mc "I must've had too much candy for breakfast."
      $maya.love+=1
      maya kiss "Now that's a solid base for any food pyramid!"
      mc "Heh. I thought so."
    # "\"That's just another Tuesday evening around here.\"":
    "\"That's just another Tuesday\nevening around here.\"":
      show maya annoyed at move_to(.5)
      mc "That's just another Tuesday evening around here."
      maya flirty "Ha! The principal and the priest. Wouldn't that be something?"
      mc "It's the name of my next memoir."
      maya thinking "Oh? Do you already have one?"
      mc "Yeah, I do."
      $mc.charisma+=1
      mc "It's called {i}\"Devilishly Handsome and Clever: How Satan Helped Me.\"{/}"
      maya kiss "Sounds legit."
    "\"Yikes. That's messed up.\"":
      show maya annoyed at move_to(.5)
      mc "Yikes. That's messed up."
      maya eyeroll "I guess they thought so, too. Hence the doorstep of the church."
      maya smile "Because there's definitely nothing fucked up about that."
      mc "Well, you're welcome to stay here whenever, okay?"
      maya concerned "That's nice of you... but surely, that's [jo]'s call?"
      mc "Err, I doubt she minds."
      maya kiss "I appreciate it, anyway."
  maya thinking "So, about the shower?"
  mc "Oh, right!"
  if mc.intellect >= 6:
    mc "You just have to twist the handle a bit."
    mc "Left..."
    extend " right..."
    extend " left..."
    extend " and then it comes on!"
    maya smile "Wow, [mc]! You have the magic touch!"
    mc "Heh, well."
    maya smile "This is almost as simple as a handjob!"
    mc "Oh? Is that your magic touch?"
    maya smile "Quick and easy. The perfect lazy approach."
    mc "If it's so simple, maybe you could—"
    maya annoyed "Sorry, my carpal tunnel is acting up. You're shit out of luck."
    mc "What? Me? I was just going to say we could make a killing in the shower industry."
    mc "{i}The handyman and the handyhand.{/}"
    maya flirty "Only if I'm the handyman."
    mc "I thought that was implied?"
    $maya.love+=1
    maya kiss "It's a deal, then!"
    maya thinking "Now get out so I can shower."
    "There's an image I don't mind holding onto..."
    mc "Err, you got it."
    window hide
    scene black with Dissolve(.07)
    $quest.maya_quixote["maya_showering_now"] = True
    $game.location = "home_hall"
    $renpy.pause(0.07)
    $renpy.get_registered_image("location").reset_scene()
    scene location with Dissolve(.5)
    window auto
    "Speaking of holding onto images... maybe if I go to sleep quickly, I'll have some extra wet dreams?"
    $quest.maya_quixote.advance("sleep_again")
  else:
    mc "Let's see here..."
    mc "..."
    mc "..."
    mc "..."
    mc "Err, I'll have to ask [jo] to call a plumber..."
    mc "It might take a few days, though."
    maya eyeroll "That's fantastic."
    maya eyeroll "I guess I'll just stay filthy, then."
    mc "It seems like the body can match the mind."
    $maya.lust+=1
    maya flirty "You see right into my dark and dirty soul, don't you?"
    mc "Heh. I try."
    window hide
    show maya flirty at disappear_to_left
    pause 0.5
    window auto
    "As hot as [maya] calling herself dirty is... I better hurry up before I start to stink, too."
    "That would probably just scare her off."
    $mc["focus"] = "maya_quixote"
    $home_bathroom["lipstick_writing_this_scene"] = True
    $quest.maya_quixote.advance("text")
  $maya.outfit = maya["outfit_stamp"]
  return

label quest_maya_quixote_text_home_bathroom_door:
  # "If I want even the slightest chance of peeping on [maya] taking a shower while she's here, I first need to let [jo] know about the shower situation."
  "If I want even the slightest chance of peeping on [maya] taking a shower{space=-55}\nwhile she's here, I need to let [jo] know about the shower situation first.{space=-55}"
  return

label quest_maya_quixote_text:
  $set_dialog_mode("phone_message_centered","jo")
  mc "Hello, [jo]!{nw}"
  pause 0.75
  mc "So, our shower doesn't seem to be working..."
  jo "What did you do to it, [mc]?"
  mc "I didn't do anything!"
  window hide None
  window auto
  $set_dialog_mode("phone_message_plus_textbox")
  "Ugh, why must she always assume the worst?"
  window hide
  $set_dialog_mode("phone_message_centered","jo")
  jo "Very well.{nw}"
  pause 0.75
  jo "I'll call a plumber to come take a look."
  mc "Thanks..."
  window hide None
  $set_dialog_mode("")
  pause 0.5
  window auto
  "Getting blamed for random shit always makes me tired and hungry."
  "But I really should watch my weight, so sleep it is."
  $mc["focus"] = ""
  $quest.maya_quixote.advance("sleep_again")
  return

label quest_maya_quixote_sleep_again_home_hall_door_bathroom:
  "[maya] is taking a shower right now. As enticing as perving on her is, I should probably give her some much need privacy."
  return

label quest_maya_quixote_power_outage:
  "Ah, great. I wonder what these two are up to now."
  "Those cute dimples and bright eyes can only mean trouble..."
  "...specifically, getting me into it."
  window hide
  show flora smile with Dissolve(.5)
  window auto
  flora smile "Hello, [mc]!"
  flora smile "You're looking... well... less rundown than usual?"
  "Huh. She's almost being sweet."
  "I'll take it... though I wonder what her real motive is."
  mc "What do you want, [flora]?"
  window hide
  show flora smile at move_to(.25)
  with None
  show flora skeptical
  show maya dramatic at Transform(xalign=.75)
  with Dissolve(.5)
  window auto
  maya dramatic "Can't a girl give a half-hearted almost-compliment just because?"
  mc "I mean, I guess..."
  flora skeptical "That's what I get for trying to be nice!"
  flora skeptical "And we didn't want anything, okay?"
  flora skeptical "We were just heading to my room to gossip."
  mc "Oh? About me?"
  flora smile "Yeah, right. That would be boring."
  "I'd be lying if I said I'm not mildly jealous that [maya] just gets to go into [flora]'s room and hang out with her."
  "One day I'll be invited in..."
  "One day..."
  mc "Very well. Have f—"
  window hide None
  play sound "light_switch"
  hide screen interface_hider
  show flora afraid
  show maya sarcastic
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.25
  show black onlayer screens zorder 4
  $quest.maya_quixote.advance("attic")
  $set_dialog_mode("default_no_bg")
  flora afraid "Aaah!"
  maya sarcastic "Sick."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  flora afraid "What the hell?"
  maya sarcastic "Maybe it's the witch?"
  flora afraid "Witch?"
  mc "It's a long story..."
  flora eyeroll "Whatever."
  flora eyeroll "Can you fix this?"
  mc "Err, it must be due to the storm..."
  flora cringe "Ugh! Oh, well. At least there's cell phones."
  maya bored "Actually, I'd check your fact factor on that one."
  flora cringe "Huh?"
  flora annoyed_texting "..."
  mc "[maya]'s right. There's no cell service."
  mc "The cell towers must be down."
  flora cringe "What?! Oh, my god! What are we supposed to do?"
  flora cringe "Can people literally die of boredom?"
  maya bored "I've definitely heard of a case or two."
  # maya bored "Worse, though — we could be stuck with nothing but our own thoughts for hours."
  maya bored "Worse, though — we could be stuck with nothing but our own thoughts{space=-70}\nfor hours."
  flora annoyed "Great. So, what are we going to do?"
  show flora annoyed at move_to("left")
  show maya bored at move_to("right")
  menu(side="middle"):
    extend ""
    "\"I know one thing we could do that involves sheets and pillows...\"":
      show flora annoyed at move_to(.25)
      show maya bored at move_to(.75)
      mc "I know one thing we could do that involves sheets and pillows..."
      flora cringe "Really, [mc]? Gross."
      $mc.charisma+=1
      mc "What?! I was talking about building a fort!"
      mc "Get your mind out of the gutter, [flora]!"
      flora eyeroll "Sure you were."
      maya confident "Interesting and unexpected form of creativity from you, [mc]."
      mc "Thanks, I think?"
      flora annoyed "Either way, I don't think [jo] would appreciate the mess."
      mc "Yeah, you're probably right..."
    "\"Each other?\"":
      show flora annoyed at move_to(.25)
      show maya bored at move_to(.75)
      mc "Each other?"
      $flora.love-=1
      flora cringe "..."
      flora cringe "Is that seriously what you're going with?"
      mc "I can't be all wit all the time, you know?"
      flora annoyed "You're a halfwit short of a candle."
      mc "I think you mean \"wick.\""
      flora sarcastic "I stand corrected."
      flora sarcastic "A whole wit short."
      maya confident "One whole wit! That's a lot."
      maya confident "You should probably look into that."
      mc "Ugh... you guys suck..."
      maya confident "Only when my hand is tired."
    # "\"We could tell each other stories about eldritch horrors.\"":
    "\"We could tell each other stories\nabout eldritch horrors.\"":
      show flora annoyed at move_to(.25)
      show maya bored at move_to(.75)
      mc "We could tell each other stories about eldritch horrors."
      $flora.lust+=1
      flora flirty "Real ones, or made up ones?"
      mc "Maybe both?"
      maya dramatic "Oh, wow. That sounds like a really lit time."
      maya dramatic "Definitely the most fun activity to do in the dark."
      maya dramatic "I can't think of anything more exciting."
      flora laughing "Err, maybe we could do something else..."
      mc "I'm down for whatever you guys want."
  if renpy.showing("flora laughing"):
    # flora thinking "How about a board game? Could you get one for us out of the attic?"
    flora thinking "How about a board game? Could you get one for us out of the attic?{space=-10}"
    mc "Does that mean you won't exclude me?"
    flora worried "Well, do you want to be included?"
  else:
    # flora confused "Can't you just get a board game for us out of the attic or something?"
    flora confused "Can't you just get a board game for us out of the attic or something?{space=-25}"
    mc "Does that mean you won't exclude me?"
    flora confused "Well, do you want to be included?"
  mc "It would be nice not to feel left out..."
  if flora.love >= 6:
    flora smile "Very well. You're in."
    flora smile "Get a game down for us and we'll all play!"
    "This might be a good opportunity for us to hang out..."
    "I could be imagining it, but there even seems to be a hint of excitement in [flora]'s voice."
    mc "All right! Deal!"
    flora smile "Thanks! We'll wait for you in your room."
  else:
    flora annoyed "Fine, then. You're in."
    flora annoyed "Get a game down for us and we'll all play."
    "Hmm... [flora] doesn't sound all too excited about the prospect..."
    "Still, it could end up being some quality bonding time for us."
    mc "All right! Deal!"
    flora sarcastic "Thanks. We'll wait for you in your room."
  window hide
  if renpy.showing("flora smile"):
    show flora smile at disappear_to_right
  elif renpy.showing("flora sarcastic"):
    show flora sarcastic at disappear_to_right
  pause 0.25
  if renpy.showing("maya confident"):
    show maya confident at disappear_to_right behind flora
  elif renpy.showing("maya dramatic"):
    show maya dramatic at disappear_to_right behind flora
  pause 1.0
  return

label quest_maya_quixote_attic_home_kitchen_door:
  "Storm outside versus power outage inside?"
  "Hmm... I'll stay inside."
  return

label quest_maya_quixote_attic_home_hall_hole:
  if quest.mrsl_HOT.actually_finished:
    "Still a gaping useless hole..."
    "Heh. Never thought I'd say that before."
    "But without the ladder, it's kind of true."
    "Time for my old tricks... tried and tested."
  else:
    "Ever since the ladder to the attic's outdoor entrance broke and the floor around it collapsed, this hole is the only access point."
    # "Retrieving stuff from up there would require something to stand on, as well as a lasso of sorts..."
    "Retrieving stuff from up there would require something to stand on,{space=-5}\nas well as a lasso of sorts..."
  $quest.maya_quixote["old_tricks"] = True
  return

label quest_maya_quixote_attic(item):
  if item == "xcube_controller":
    python:
      attic_items = [["Dungeons and Damsels","hole_game"],["a comic book","hole_comic"],["a cowboy hat","hole_hat"],["a plastic bag","hole_bag"],["a shoebox","hole_diary"],["an old book","hole_photo"]]
      while True:
        item_caught = attic_items[random.randint(0,5)]
        if not home_hall[item_caught[1]]:
          break
        if False not in {home_hall[item_caught[1]] for item_caught in attic_items}:
          break
    if not quest.maya_quixote["digged_through"]:
      "Welp. Time to go digging through the attic."
      "All for the entertainment of a couple of girls..."
      "..."
      "Totally worth it."
    "Okay, let's see here..."
    "..."
    "Come on..."
    "..."
    if False not in {home_hall[item_caught[1]] for item_caught in attic_items}:
      "Hmm... there's nothing else really interesting up here."
    else:
      "Aha! Got something!"
      "It's [item_caught[0]]!"
      menu(side="middle"):
        extend ""
        "Keep it":
          if item_caught[0] == "Dungeons and Damsels":
            "Awesome! Here it is!"
            window hide
            $home_hall["hole_game"] = True
            $mc.add_item("dungeons_and_damsels")
            pause 0.25
            window auto
            "Man, I haven't played a board game in ages..."
            "Since [flora] and I were kids, it feels like."
            "I should really be thanking the storm for this chance at some quality time with her."
            "And a chance to get to know [maya] better, too."
            $quest.maya_quixote.advance("board_game")
          elif item_caught[0] == "a comic book":
            "Oh, man! I remember this one!"
            "I used to read it all the time back in middle school..."
            "{i}\"Captain United States and the Fight Against Communism.\"{/}"
            "Huh. I guess I was pretty political."
            window hide
            $home_hall["hole_comic"] = True
            $mc.add_item("comic_book")
          elif item_caught[0] == "a cowboy hat":
            "Yeehaw!"
            "This'll definitely make me look like a badass."
            window hide
            $home_hall["hole_hat"] = True
            $mc.add_item("cowboy_hat")
          elif item_caught[0] == "a plastic bag":
            "Oh? What do we have here?"
            "A plastic bag with..."
            "..."
            "Is this what I think it is?"
            window hide
            $home_hall["hole_bag"] = True
            $mc.add_item("bag_of_weed")
          elif item_caught[0] == "a shoebox":
            "Hmm... there seems to be something in it..."
            window hide
            $home_hall["hole_diary"] = True
            $mc.add_item("diary_page")
          elif item_caught[0] == "an old book":
            "Oh! It's actually a photo album."
            "There are lots of boring family photos here."
            "..."
            "Wait a minute..."
            "I forgot about the last photo, from our beach day this spring."
            window hide
            show misc beach_photo
            show school computer_room overlay_night
            with Dissolve(.5)
            window auto
            "I had forgotten how cute they looked that day."
            "Well, [flora] is cute. [jo] is more... sexy? Is that weird to think?"
            "..."
            "The smell of sand and ocean water, bodies sweating in the sun..."
            # "[flora] doing that little shimmy towel dance to get out of her clothes..."
            "[flora] doing that little shimmy towel dance to get out of her clothes...{space=-15}"
            "[jo] wrapping her lips tight around a popsicle..."
            "Me trying to hide my raging erection by lying on my stomach..."
            "[jo] rubbing sunscreen on my back and legs..."
            "Man, what a time to be alive."
            "Right after this photo, I stole the sunscreen and [flora] chased me into the water."
            "She fell with a splash and lost her top."
            "The cute little thing floated right into my hand and pocket before she even realized."
            "She stayed in the water for an hour that day, trying to hide her perky nipples."
            "If I'd been more confident, I could probably have traded the bikini for a blowjob behind the pier..."
            "A good memory, nonetheless."
            window hide
            hide misc beach_photo
            hide school computer_room overlay_night
            with Dissolve(.5)
            pause 0.125
            $home_hall["hole_photo"] = True
            $mc.add_item("beach_photo")
        "Toss it back":
          "Hmm... it's not quite what I had in mind."
    $quest.maya_quixote["digged_through"] = True
  elif item == "ball_of_yarn":
    if quest.maya_quixote._failed_items and "ball_of_yarn" in quest.maya_quixote._failed_items["dungeons_and_damsels"]:
      "Still weak, still flimsy. Almost like me."
    else:
      "The thread is too weak and flimsy."
      "I won't catch anything with a lasso like this..."
      $quest.maya_quixote.failed_item("dungeons_and_damsels",item)
  else:
    "Will my [item.title_lower] work for this?"
    "..."
    "Nope. Almost as useless as brushing your teeth."
    "Just kidding! I do that. Most of the time."
    $quest.maya_quixote.failed_item("dungeons_and_damsels",item)
  return

label quest_maya_quixote_board_game:
  "Two girls in my room at the same time and I'm not even dreaming..."
  "I never thought I'd see the day."
  window hide
  show maya bored at Transform(xalign=.25)
  show flora annoyed at Transform(xalign=.75)
  with Dissolve(.5)
  window auto
  mc "Hello, ladies!"
  mc "I have something good for you."
  flora sarcastic "The ability to get the power back on?"
  maya confident "Chocolate?"
  extend " A captured witch?"
  extend " A do over for my ruined youth?"
  mc "What? I mean, no!"
  maya bored "Worth a shot."
  flora annoyed "Oh, my god! Just tell us what game it is!"
  mc "Look what I found! My old Dungeons and Damsels setup!"
  flora eyeroll "Seriously? Of all the games you could have found?"
  mc "Come on! It'll be fun!"
  mc "I'll be the game master and you two can go on an adventure!"
  maya bored "It's either that or go outside and hope to get struck by lightning..."
  maya bored "I'm currently leaning toward the latter."
  mc "Oh, come on! It'll be fun! I promise!"
  maya skeptical "That's what the cultists said about summoning a sex demon."
  mc "Well, was it?"
  maya confident "Not so much for the demon."
  mc "..."
  flora annoyed "Fine. We'll play your nerd game."
  flora sarcastic "You better thank your lucky storm tonight."
  "Who am I to scorn a blessing? Even if the party is reluctant."
  mc "Great! Just give me a moment to set everything up!"
  window hide
  hide maya
  hide flora
  with Dissolve(.5)
  window auto
  "It feels like I haven't set up a game in forever..."
  "I can't wait to see how they like it."
  "..."
  "..."
  "..."
  window hide
  show maya bored with Dissolve(.5)
  window auto
  maya bored "So, are we going to do this or what?"
  "Oh, man. If only it wasn't DnD she was talking about..."
  "Still pretty good, though."
  mc "Prepare to be transported into the best fantasy ever!"
  maya bored "I can think of many fantasies sure to top it."
  "And I would love to hear all about those fantasies..."
  mc "Yeah?"
  maya blush "Hold on, I was wrong!"
  maya blush "This might actually be my top fantasy ever! Like ever-ever!"
  mc "Ugh... let's just play..."
  maya confident "Okay, transport me hard, [mc]! Show me how it's done!"
  show maya confident at move_to(.25)
  menu(side="right"):
    extend ""
    "?maya.love>=6@[maya.love]/6|{image=maya contact_icon}|{image=stats love_3}|\"Do you use sarcasm to mask your true feelings?\"":
      show maya confident at move_to(.5)
      mc "Do you use sarcasm to mask your true feelings?"
      maya dramatic "Ah! You caught me!"
      maya sarcastic "Only problem is, I have no feelings."
      mc "I'm serious!"
      mc "Whereas you rarely are."
      maya cringe "So what?"
      mc "So, I would like to know more about you."
      maya concerned "..."
      maya concerned "I guess that's just the way I am."
      maya concerned "It's easier when you pretend like nothing anyone says or does bothers you."
      mc "I definitely get that."
      mc "For what it's worth, I think you're really strong."
      maya flirty "A bodybuilder of anti-emotion..."
      mc "Not at all!"
      $maya.love+=1
      maya kiss "Well, I appreciate it."
      maya kiss "But enough of this schmaltzy shit! Let's nerd out or whatever!"
      mc "As my Lady commands!"
    # "\"I'll smack you right into medieval times, baby.\"":
    "\"I'll smack you right into\nmedieval times, baby.\"":
      show maya confident at move_to(.5)
      mc "I'll smack you right into medieval times, baby."
      $maya.love+=1
      maya kiss "Just how I like it!"
      mc "I knew you could appreciate a big sword."
      maya thinking "Don't get carried away now."
      maya flirty "Though I do handle them like an expert."
      mc "I can appreciate that."
      maya flirty  "Oh, I bet you can."
      "Heh. It feels nice to be spending time with [maya] like this."
      "Maybe her walls are finally breaking down a bit?"
      mc "Okay, let's do it!"
    # "\"I'm really excited to spend some time with you.\"":
    "\"I'm really excited to spend\nsome time with you.\"":
      show maya confident at move_to(.5)
      mc "I'm really excited to spend some time with you."
      $maya.love-=1
      # maya dramatic "Nothing like forced shared space and a power outage to bring people together, right?"
      maya dramatic "Nothing like forced shared space and a power outage to bring people{space=-20}\ntogether, right?"
      "Ouch."
      mc "Err, when you put it like that..."
      maya cringe "Oh, man. Kicked puppy much?"
      maya cringe "It's not you. It's me. I am... incapable of feeling."
      mc "A modern tragedy."
      maya dramatic "It's true. Shakespeare has nothing on me."
      mc "I don't believe it."
      maya sarcastic "Smart. Don't ever believe anything."
      maya sarcastic "Now, let's play! I'm sooo excited!"
      mc "..."
      mc "I guess I shouldn't believe you?"
      maya sarcastic "Oh! You {i}can{/} be taught!"
      "Heh. This is fun."
      "I better make sure to keep [flora] involved, too."
  window hide
  if renpy.showing("maya kiss"):
    show maya kiss at move_to(.75)
  elif renpy.showing("maya flirty"):
    show maya flirty at move_to(.75)
  elif renpy.showing("maya sarcastic"):
    show maya sarcastic at move_to(.75)
  show flora skeptical at Transform(xalign=.25) with Dissolve(.5)
  window auto
  flora skeptical "Are you done yet? I'm getting bored over here."
  mc "Yes! The scene is set!"
  window hide
  show fantasy_feeling
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $game.location = "home_dungeons_and_damsels"
  $mc["focus"] = ""
  $flora["outfit_stamp"] = flora.outfit
  $flora.outfit = {"shirt":"flora_armor"}
  $maya["outfit_stamp"] = maya.outfit
  $maya.outfit = {"jacket":"maya_armor"}
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "You're a pair of warrior maidens."
  mc "[flora] the Fine and [maya] the—"
  maya sarcastic "...Dickbreaker."
  mc "What?"
  maya dramatic "I want to be called [maya] the Dickbreaker."
  maya dramatic "The singing, dickbreaking bardess."
  mc "..."
  mc "I don't—"
  flora skeptical "Let her choose her name!"
  flora smile "As for me, I want to be Lenora, Lady of the Water."
  flora smile "And my house pet is a squid."
  mc "House pet? Do you mean familiar?"
  flora smile "I guess!"
  mc "Err, okay."
  mc "The pair of you are on the hunt for vengeance after your small town has been savaged by a bloodthirsty dragon."
  mc "Your children — incinerated. Your manly lovers — crunched in half."
  maya cringe "Nah. No kids for me."
  mc "Ugh! Your cats, then!"
  maya sarcastic "That's better. Now I really want vengeance!"
  mc "So, you're hunting the dragon—"
  maya dramatic "A dragon! How very original! I'm quaking in my maidenly boots."
  mc "Stop interrupting me!"
  maya dramatic "Or what?"
  show flora smile at move_to("left")
  show maya dramatic at move_to("right")
  menu(side="middle"):
    extend ""
    # "\"Or the dragon will swoop down and char you, too.\"":
    "\"Or the dragon will swoop\ndown and char you, too.\"":
      show flora smile at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Or the dragon will swoop down and char you, too."
      maya thinking "Sounds like that would be very bad for my skin."
      mc "Yes. Very."
      maya thinking "Unless I can make a cream for that?"
      mc "Nope. Your skills lie elsewhere."
      maya flirty "Oh, I bet they do."
      "Oh, god..."
    "\"Or I'll end the game right now.\"":
      show flora smile at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Or I'll end the game right now."
      maya afraid "But then we'd have to sit and stare at each other in silence!"
      mc "Exactly. Do you really want to stare at this face all day?"
      $maya.lust+=1
      maya kiss "Good point."
      maya kiss "Please do continue."
      mc "I thought so."
    "\"Or I'll turn you into a goblin.\"":
      show flora smile at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Or I'll turn you into a goblin."
      maya flirty "Oh? Sounds sexy."
      mc "Really? Have you ever seen a sexy goblin?"
      maya thinking "What else would you call Chad?"
      mc "Ha! Good one."
      mc "Though I'd consider him more of a troll..."
      $maya.love+=1
      maya flirty "He definitely has the brains of one."
      $mc.love+=1
      mc "Ain't that the truth!"
  mc "Anyway, with your massive swords—"
  flora sarcastic "Swords? No, thanks. I want a crossbow!"
  mc "Ugh, fine! With your {i}weapons{/} in hand, you set out on the trail."
  flora confused "How does a dragon leave a trail when it flies, anyway?"
  mc "It's just a regular trail! Through the forest, toward its mountain lair!"
  maya dramatic "A mountain? Now, that's original. How about a bungalow instead?"
  mc "Are you two going to let me set this up or not?"
  maya cringe "I will never apologize for questioning the world around me."
  flora sarcastic "Amen, sister!"
  mc "It's a fictional world!"
  mc "Anyhow..."
  # mc "You're walking through the Cumdrop Forest when suddenly a band of horny bandits falls upon you."
  mc "You're walking through the Cumdrop Forest when suddenly a band of{space=-30}\nhorny bandits falls upon you."
  # mc "The biggest, muscleiest, manliest of the bunch lets out a bellow and grabs hold of [flora] the Fine."
  mc "The biggest, muscleiest, manliest of the bunch lets out a bellow and{space=-10}\ngrabs hold of [flora] the Fine."
  flora annoyed "Lenora, Lady of the Water!"
  mc "Err, right."
  window hide
  # show flora dnd_bandits held_hostage
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show flora dnd_bandits held_hostage: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  pause 0.0
  hide black onlayer screens
  with Dissolve(.5)
  hide flora
  show flora dnd_bandits held_hostage behind fantasy_feeling ## This is just to avoid the screen shake if the player is skipping
  window auto
  # mc "He rips the hem of your undergarment, exposing your soft, supple thigh."
  mc "He rips the hem of your undergarment, exposing your soft, supple thigh.{space=-90}"
  flora dnd_bandits held_hostage "I kill him!"
  mc "You can't use your crossbow in close combat!"
  maya "Should have picked the sword, eh?"
  flora dnd_bandits held_hostage "Whatever."
  mc "Anyway, you let out a scream as he tries to push you down onto the moss laden ground."
  # mc "Your heart quickens at his violent touch, you feel his manhood straining against your—"
  mc "Your heart quickens at his violent touch, you feel his manhood straining{space=-75}\nagainst your—"
  flora dnd_bandits held_hostage "Hold on, [mc]! What are you trying to do?"
  mc "What? It's just part of the story."
  flora dnd_bandits held_hostage "Well, I don't like the direction this is going."
  maya "It's cool, [flora]. I got you."
  window hide
  pause 0.125
  show flora dnd_bandits stabbed with hpunch
  pause 0.25
  window auto
  maya "I whip out my massive... sword, and fuck that guy's shit up."
  # maya "Stab him right in the back. Impale him. Then, ask him how he likes it."
  maya "Stab him right in the back. Impale him. Then, ask him how he likes it.{space=-15}"
  maya "He can't answer, of course, because he's choking on his own blood."
  maya "Afterward, I compose a song about it."
  maya "Naturally, it's an instant hit single."
  mc "..."
  mc "Roll for it."
  maya "..."
  maya "20."
  mc "Jesus."
  $unlock_replay("flora_ambush")
  window hide
  show flora smile at Transform(xalign=.25)
  show maya sarcastic
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "Err, I guess the leader is dead."
  flora smile "Oh! And then I kill the rest of the pigs with my bow and arrow!"
  mc "Roll—"
  flora smile "20!"
  maya sarcastic "Boom!"
  mc "Fine. The bandits are all dead. Congratulations."
  flora smile "Thanks!"
  # mc "So, you leave a mountain of bodies and a river of blood in your wake as you continue your journey..."
  mc "So, you leave a mountain of bodies and a river of blood in your wake{space=-10}\nas you continue your journey..."
  mc "In the distance, you see smoke wafting into the sky, just before the smell of burning hits your nostrils."
  mc "You know you're getting close. Vengeance will soon be yours."
  mc "But night approaches quickly, and you haven't slept in days."
  # mc "So, when you come across an inn on the side of the road, you decide to go in."
  mc "So, when you come across an inn on the side of the road, you decide{space=-20}\nto go in."
  maya flirty "Thank god! I could definitely use a bath after running around in the woods, getting all dirty."
  mc "Sure! You decide the first thing you'll do there is bathe."
  maya thinking "That's what I just said."
  mc "..."
  "This is not quite going the way I thought it would, is it?"
  "Leave it to [flora] to be cheeky... and [maya] too, for that matter."
  "Still, they're here, in my bedroom, indulging in fantasy with me..."
  "I guess I shouldn't complain too much."
  mc "Anyway, in the washroom, you're greeted by a shy maid."
  # mc "She pours hot water into the tub for you, and then offers to help you undress."
  mc "She pours hot water into the tub for you, and then offers to help you{space=-15}\nundress."
  mc "She starts with your warrior maiden armor, discarding the pieces to the side."
  # mc "Then, she moves to your bodice, undoing the strings with deft fingers."
  mc "Then, she moves to your bodice, undoing the strings with deft fingers.{space=-50}"
  # mc "She slips it from your shoulders and your round, firm breasts spring free."
  mc "She slips it from your shoulders and your round, firm breasts spring{space=-10}\nfree."
  mc "She admires the sight, and offers to sponge you clean."
  mc "As you step into the tub, she—"
  maya thinking "Hold up!"
  maya thinking "I'm more than capable of washing myself, thank you very much."
  # maya thinking "I shoo the maid from the room, then lather up the soap in my hands."
  maya thinking "I shoo the maid from the room, then lather up the soap in my hands.{space=-10}"
  maya flirty "I slide my sudsy hands down my body, before slipping my fingers in between my..."
  "Yes! This is getting good!"
  maya flirty "...toes."
  "Ah. Of course."
  mc "Very funny."
  "...and still kinda hot."
  mc "So, the bath is done, then."
  mc "Meanwhile, Lenora, Lady of the Water is downstairs, bargaining for your dinner."
  flora skeptical "What? Am I broke or something?"
  mc "The dragon stole all your money."
  maya thinking "That's impossible. Dragons don't have pockets."
  mc "They don't need them!"
  mc "Anyway, you're downstairs, flirting with the innkeep for your bread."
  mc "You lean over the counter, exposing your milky white bosom."
  flora skeptical "How original."
  mc "You then bite your rosy lip."
  flora skeptical "Haven't heard that one before."
  # mc "You completely mesmerize him. He'll do anything you want just to have a taste."
  mc "You completely mesmerize him. He'll do anything you want just to have{space=-60}\na taste."
  # mc "You pucker your full lips... and he goes in for it! You taste of strawberry wine—"
  mc "You pucker your full lips... and he goes in for it! You taste of strawberry{space=-65}\nwine—"
  flora afraid "Hang on! Don't I get to choose?"
  show flora afraid at move_to("left")
  show maya thinking at move_to("right")
  menu(side="middle"):
    extend ""
    # "\"You're right. You should be the one deciding.\"":
    "\"You're right. You should\nbe the one deciding.\"":
      show flora afraid at move_to(.25)
      show maya thinking at move_to(.75)
      mc "You're right. You should be the one deciding."
      flora smile "Thank you."
      mc "Of course! I'm just happy to be playing a game with you."
      mc "It's been a long time since we just hung out..."
      mc "I'm trying to do things differently. To be nicer."
      $flora.love+=1
      flora embarrassed "Geez! Don't get all sappy, [mc]!"
      # flora embarrassed "It's just a game, and we're only playing it because the power is out, okay?"
      flora embarrassed "It's just a game, and we're only playing it because the power is out, okay?{space=-110}"
      mc "Still..."
      flora skeptical "Anyway! I decide to let the innkeep try to kiss me..."
      flora smile "...but then I stab him with my hidden dagger and take all the bread!"
      mc "Damn! Brutal."
      flora smile "Efficient."
      mc "I can't argue with that."
      # mc "So, you take the food upstairs and share with [maya] the Dickbreaker."
      mc "So, you take the food upstairs and share with [maya] the Dickbreaker.{space=-5}"
      # mc "Then come morning, the pair of you set out once more on your journey."
      mc "Then come morning, the pair of you set out once more on your journey.{space=-65}"
    # "\"Nope. I'm the game master here, and what I say goes.\"":
    "\"Nope. I'm the game master\nhere, and what I say goes.\"":
      show flora afraid at move_to(.25)
      show maya thinking at move_to(.75)
      mc "Nope. I'm the game master here, and what I say goes."
      $flora.love-=1
      flora annoyed "The master of making up stupid stuff, more like."
      mc "It's not stupid! It's part of the fantasy genre!"
      flora eyeroll "Your fantasy, maybe."
      mc "Stop being a brat and keep playing, will you?"
      flora annoyed "I'll keep playing only because the power is still out."
      mc "That's good enough for me."
      mc "So, the innkeeper sucks on your strawberry lips, kissing you deeply."
      # mc "You kiss him back, enjoying the feel of a man... before abruptly ending it there."
      mc "You kiss him back, enjoying the feel of a man... before abruptly ending{space=-45}\nit there."
      mc "He's disappointed, but gives you the bread all the same."
      flora sarcastic "Great. I always wanted to be a medieval whore."
      mc "Well, it keeps your belly full."
      # mc "Once you eat and recuperate, you and [maya] the Dickbreaker are ready to continue your journey."
      mc "Once you eat and recuperate, you and [maya] the Dickbreaker are ready{space=-45}\nto continue your journey."
    "?flora.lust>=6@[flora.lust]/6|{image=flora contact_icon}|{image=stats lust_3}|\"Nope. The innkeep grabs you and kisses you hard.\"":
      show flora afraid at move_to(.25)
      show maya thinking at move_to(.75)
      mc "Nope. The innkeep grabs you and kisses you hard."
      flora skeptical "Gross!"
      mc "He pulls up the hem of your nightgown, his fingers finding their way inside."
      mc "They feel strange as they move up your inner thigh..."
      mc "Slimy, almost."
      flora smile "Oh?"
      window hide
      show flora dnd_tentacles
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      # mc "His fingers turn to tentacles, and one slides along the slit of your pussy."
      mc "His fingers turn to tentacles, and one slides along the slit of your pussy.{space=-80}"
      # mc "He soaks in the wetness he finds there, then forces one finger-tentacle inside of you."
      mc "He soaks in the wetness he finds there, then forces one finger-tentacle{space=-60}\ninside of you."
      mc "It goes in deep and you let out a surprised moan."
      flora dnd_tentacles "Oh, my god..."
      mc "Exactly like that! Thanks for staying in character!"
      flora dnd_tentacles "Ugh..."
      mc "So, he tentacle fucks you right there in the middle of the common room and your world turns to fog."
      # mc "Your head falls back and your eyes flutter as the sensation sends you to the edge."
      mc "Your head falls back and your eyes flutter as the sensation sends you{space=-35}\nto the edge."
      mc "You faint from the ecstasy of it."
      $unlock_replay("flora_run_in")
      window hide
      show flora smile
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      flora smile "That's..."
      "Judging from the way she's blushing, exactly what she wanted."
      # mc "Come morning, you find yourself in your rented bed upstairs, completely soaked."
      mc "Come morning, you find yourself in your rented bed upstairs, completely{space=-90}\nsoaked."
      # mc "It's salt water, and though you can't be sure, you're pretty sure you've just had a run-in with an ancient god."
      mc "It's salt water, and though you can't be sure, you're pretty sure you've{space=-40}\njust had a run-in with an ancient god."
      flora smile "And then what...?"
      mc "And then the quest continues."
      mc "You decide not to mention your encounter from the night before to [maya] the Dickbreaker."
      mc "You just set off on the trail once more."
  mc "You're out there, nearing the dragon's mountain lair—"
  maya cringe "I still say a bungalow would be more unique."
  mc "Do you seriously think a dragon could fit in a bungalow?"
  maya dramatic "A dragon sized one, obviously!"
  mc "..."
  mc "Anyway, the smoke hangs heavy in the air now, an endless black cloud roiling down from the mountain."
  flora eyeroll "Gosh, I'm quaking with fear."
  mc "Oh, you should be!"
  # mc "You draw your weapons. [maya] the Dickbreaker and her trusty sword—"
  mc "You draw your weapons. [maya] the Dickbreaker and her trusty sword—{space=-45}"
  maya sarcastic "...Vagina Puncher."
  mc "What? Absolutely not."
  maya cringe "It's my sword, and I'll name it whatever I want."
  mc "Ugh! Fine!"
  mc "[maya] the Dickbreaker with Vagina Puncher, and Lenora Lady of the Water, bow in hand—"
  flora sarcastic "I want to name my weapon, too!"
  mc "Fine! What is it called?"
  flora sarcastic "Dragon Ripper!"
  mc "So, Lenora Lady of the Water, with Dragon Ripper in hand—"
  flora sarcastic "Dragon Ripper is a +10 flaming, ghost touch, vorpal longbow of mighty cleaving."
  mc "What? No, it's not."
  flora annoyed "It says right here on my character sheet!"
  mc "You can't just add random items to your character sheet!"
  flora sarcastic "You probably added it and just forgot about it."
  mc "Ugh! Whatever!"
  mc "The ground shakes beneath your feet... then, suddenly!"
  # mc "Into the gray and yellow sulfur sky bursts the dragon, massive black wings spread, hungry maw open and ready to devour."
  mc "Into the gray and yellow sulfur sky bursts the dragon, massive black{space=-10}\nwings spread, hungry maw open and ready to devour."
  mc "Before he sees you, you run into his lair."
  mc "Inside are mountains of bones, hoards of gold, stashes of meat."
  mc "Do you hide and wait, or draw the beast inside?"
  maya dramatic "Easy! Hide and wait."
  maya dramatic "I grew up doing that as a child. It's easier to wait out the beast in its own home."
  show flora sarcastic at move_to("left")
  show maya dramatic at move_to("right")
  menu(side="middle"):
    extend ""
    # "?maya.love>=8@[maya.love]/8|{image=maya contact_icon}|{image=stats love_3}|\"Did you have to do that a lot?\"":
    "?maya.love>=8@[maya.love]/8|{image=maya contact_icon}|{image=stats love_3}|\"Did you have to\ndo that a lot?\"":
      show flora sarcastic at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Did you have to do that a lot?"
      maya concerned "Err, I don't know why I said that."
      maya concerned "Who has a perfect home life, right?"
      # maya flirty "If anything, it made me more resilient. Therefore, [maya] the Dickbreaker."
      maya flirty "If anything, it made me more resilient. Therefore, [maya] the Dickbreaker.{space=-70}"
      maya flirty "It's all just a game."
      # "Damn. I think I just caught a glimpse of [maya] letting her guard down."
      "Damn. I think I just caught a glimpse of [maya] letting her guard down.{space=-20}"
      "Maybe she's starting to feel more comfortable around me?"
      "Either way, it's nice to see that softer side of hers."
      mc "Err, right. Just a game."
      # mc "For the record, [flora] and I are glad to have you here playing with us."
      mc "For the record, [flora] and I are glad to have you here playing with us.{space=-10}"
      maya kiss "What a lucky damsel I am to be rescued by such fine folk!"
      "Aaaand we're back."
      mc "Ha! Not a damsel! A warrior maiden!"
      $maya.love+=1
      maya kiss "Of course! Let's fuck this beast up!"
    # "\"Oh, you haven't seen a beast like this before.\"":
    "\"Oh, you haven't seen\na beast like this before.\"":
      show flora sarcastic at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Oh, you haven't seen a beast like this before."
      maya flirty "Pfft! That's what they all say."
      maya flirty "But then I break out Vagina Puncher, and they all get intimidated."
      mc "Heh. Insecure beasts, aren't they?"
      maya flirty "With a name like \"Dickbreaker,\" you can probably see why."
      mc "Very true. A name that strikes fear in dicks everywhere."
      $maya.love+=1
      maya kiss "Exactly!"
    "\"Aw, you choose to hide? That's no fun.\"":
      show flora sarcastic at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Aw, you choose to hide? That's no fun."
      maya thinking "Life is all about hiding. Even in plain sight."
      maya thinking "That's why I'm hiding out here."
      mc "But this is a game..."
      # maya afraid "What? Do you mean I'm not actually a warrior bardess with a massive sword?"
      maya afraid "What? Do you mean I'm not actually a warrior bardess with a massive{space=-30}\nsword?"
      maya afraid "God, why didn't someone tell me I was this delusional?!"
      maya afraid "My world is crumbling right before my eyes!"
      mc "Okay, okay! I'm sorry!"
      $maya.lust-=1
      maya thinking "Anyway..."
      maya thinking "Please, continue with the lie that is my life."
      mc "Err, as you wish."
  # mc "So, the dragon flies around his mountain a few times, before eventually coming back inside."
  mc "So, the dragon flies around his mountain a few times, before eventually{space=-70}\ncoming back inside."
  mc "He lands upon his hoard with a massive thud."
  # mc "The pair of you hide behind a small hill of skulls, watching and waiting for an opportunity."
  mc "The pair of you hide behind a small hill of skulls, watching and waiting{space=-45}\nfor an opportunity."
  flora skeptical "Can you hurry up? I'm getting bored."
  mc "Rude."
  flora skeptical "Facts."
  # mc "Anyway, the dragon takes one whiff of the air and catches the scent of pus—"
  mc "Anyway, the dragon takes one whiff of the air and catches the scent{space=-5}\nof pus—"
  flora afraid "Don't be gross!"
  mc "Err, catches the scent of flesh."
  mc "He opens his mouth, and {i}whoooosh!{/}"
  mc "He incinerates the skulls you hide behind."
  # mc "The flame turns your armor and underthings to ash, leaving you both completely naked and exposed."
  mc "The flame turns your armor and underthings to ash, leaving you both{space=-25}\ncompletely naked and exposed."
  maya dramatic "Well, that's convenient."
  flora embarrassed "What the hell, [mc]?!"
  show flora embarrassed at move_to("left")
  show maya dramatic at move_to("right")
  menu(side="middle"):
    extend ""
    # "\"What? As the dungeon master, I rule everything.\"":
    "\"What? As the dungeon\nmaster, I rule everything.\"":
      show flora embarrassed at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "What? As the dungeon master, I rule everything."
      $flora.lust-=1
      flora embarrassed "That's messed up on so many levels..."
      maya cringe "There's dungeon master, and then there's {i}dungeon master.{/}"
      maya cringe "We both know which one you are."
      mc "Careful, or I'll lock you two up."
      maya sarcastic "Don't threaten me with a good time."
      mc "Oh, I absolutely will."
    "\"Don't be so dramatic, [flora].\"":
      show flora embarrassed at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Don't be so dramatic, [flora]."
      $flora.love-=1
      flora skeptical "I'm not being dramatic! I just don't want to play weird games!"
      maya sarcastic "Oh, honey, you're playing DnD. It's too late for that."
      flora smile "Ain't that the truth!"
      mc "DnD is not weird!"
      flora smile "Sure it is. And that's why you like to play."
      mc "Ugh! No, it's not!"
      flora skeptical "Don't be so dramatic, [mc]."
      mc "..."
      mc "I'm just going to continue..."
    "\"Do you object too, [maya]?\"":
      show flora embarrassed at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "Do you object too, [maya]?"
      maya cringe "Like a lawyer in court."
      maya cringe "You'll have to try harder than that to get my clothes off."
      mc "It's just a fantasy..."
      maya sarcastic "Exactly."
      mc "Don't be so serious. It's just a bit of fun."
      maya dramatic "Moi? Serious?!"
      maya dramatic "How dare you, sir!"
      mc "Quit screwing around and let's play, will you?"
      $maya.lust+=1
      maya sarcastic "Play? Or {i}play?{/}"
      mc "..."
      mc "Yes."
    "\"What's the big deal, girls?\"":
      show flora embarrassed at move_to(.25)
      show maya dramatic at move_to(.75)
      mc "What's the big deal, girls?"
      maya cringe "Anything sexual is against my religion."
      mc "Seriously?"
      maya cringe "Seriously."
      mc "Damn. I'm sorry. I didn't know."
      maya sarcastic "Just kidding! It's actually the focal point of my religion."
      mc "..."
      "I don't know what's real anymore..."
      $maya.love-=1
      maya dramatic "Not dragon sex, though."
      mc "It's not dragon sex!"
      maya dramatic "Sure it's not."
      mc "Anyway, where was I?"
  window hide
  show flora dnd_kiss anticipation
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "Your armor and clothes get blown away in a huff of searing heat."
  mc "The pair of you stand there, naked as the day you were born."
  mc "Your eyes meet over the smoldering pile of ash, and you know your time has come."
  mc "You only have a short time left to live."
  window hide
  pause 0.125
  show flora dnd_kiss stare with Dissolve(.5)
  pause 0.25
  window auto
  flora dnd_kiss stare "\"I, um... I have a confession to make...\""
  mc "Says [flora]. I mean, Lenora."
  maya "\"What is it?\""
  mc "Comes the breathy reply from [maya] the Dickbreaker."
  flora dnd_kiss stare "\"It's you who I've always loved.\""
  flora dnd_kiss stare "\"The way you wield that sword with such confidence...\""
  flora dnd_kiss stare "\"Your smoking warrior body...\""
  maya "\"Oh?\""
  flora dnd_kiss stare "\"I want you.\""
  window hide
  pause 0.125
  show flora dnd_kiss kiss with Dissolve(.5)
  pause 0.25
  window auto
  # mc "And just like that, you crash together, your burning lips seeking each other out."
  mc "And just like that, you crash together, your burning lips seeking each{space=-20}\nother out."
  mc "They're hot and sweaty, hungry as you devour each other's mouths."
  # mc "[flora], err... Lenora, your tongue darts desperately into [maya]'s mouth, soft groans gurgling from your throat."
  mc "[flora], err... Lenora, your tongue darts desperately into [maya]'s mouth,{space=-30}\nsoft groans gurgling from your throat."
  # mc "She pulls you closer, and your naked bodies slip against one another."
  mc "She pulls you closer, and your naked bodies slip against one another.{space=-30}"
  window hide
  show flora dnd_taste kiss
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  # mc "You stumble to the wrecked floor of the cave, [maya] now on top of [flora]."
  mc "You stumble to the wrecked floor of the cave, [maya] now on top\nof [flora]."
  # mc "Your hands fumble along her slim, firm body, up to her round, full breasts."
  mc "Your hands fumble along her slim, firm body, up to her round,\nfull breasts."
  window hide
  pause 0.125
  show flora dnd_taste nipple with Dissolve(.5)
  pause 0.25
  window auto
  # mc "[maya] then breaks the kiss to take one of those breasts in her mouth."
  mc "[maya] then breaks the kiss to take one of those breasts in her mouth.{space=-15}"
  # mc "She sucks on the hard nipple and draws desperate pants from [flora] as she wraps her arms around her."
  mc "She sucks on the hard nipple and draws desperate pants from [flora]{space=-5}\nas she wraps her arms around her."
  mc "It's so much better than you ever could have imagined, [flora]."
  mc "Those soft, warm lips tasting your body..."
  window hide
  pause 0.125
  show flora dnd_taste stomach with Dissolve(.5)
  pause 0.25
  window auto
  mc "You urge [maya] to go lower, and she greedily obliges."
  mc "She trails hot, frantic kisses down your stomach and you twist your fingers in her fiery hair."
  window hide
  pause 0.125
  show flora dnd_taste licking1 with Dissolve(.5)
  pause 0.25
  window auto
  # mc "When she reaches your pussy, she can smell the sweet pungent odor of your juices."
  mc "When she reaches your pussy, she can smell the sweet pungent odor{space=-25}\nof your juices."
  window hide
  pause 0.125
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking3 with Dissolve(.15)
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking1 with Dissolve(.15)
  pause 0.2
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking3 with Dissolve(.15)
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking1 with Dissolve(.15)
  pause 0.2
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking3 with Dissolve(.15)
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking1 with Dissolve(.15)
  pause 0.2
  window auto
  mc "She runs her tongue along your slit, having a taste."
  # mc "She savors it on her tongue for a moment, before parting your swollen lips and digging into the delicious folds."
  mc "She savors it on her tongue for a moment, before parting your swollen{space=-50}\nlips and digging into the delicious folds."
  # mc "Then, she breaks away for a moment, her own breaths coming unevenly."
  mc "Then, she breaks away for a moment, her own breaths coming unevenly.{space=-85}"
  mc "She's never tasted anything so perfect."
  window hide
  pause 0.125
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.25
  window auto
  # mc "She nibbles on your labia, then drags her tongue up along your slick slit, before diving back in."
  mc "She nibbles on your labia, then drags her tongue up along your slick{space=-5}\nslit, before diving back in."
  mc "She drinks the nectar of your sweet flower as you open up to her."
  mc "She shoves her tongue in deeper, and your body begins to shudder and shake beneath her."
  mc "So, she exposes your clit by pushing back the hood with her lips."
  mc "She licks and sucks that tender nub, flicking it with her tongue."
  window hide
  pause 0.125
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking3 with Dissolve(.15)
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking1 with Dissolve(.15)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking3 with Dissolve(.15)
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking1 with Dissolve(.15)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.05
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking3 with Dissolve(.15)
  show flora dnd_taste licking2 with Dissolve(.15)
  show flora dnd_taste licking1 with Dissolve(.15)
  pause 0.1
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.05
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.05
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking4 with Dissolve(.1)
  show flora dnd_taste licking3 with Dissolve(.1)
  show flora dnd_taste licking2 with Dissolve(.1)
  show flora dnd_taste licking1 with Dissolve(.1)
  pause 0.25
  window auto
  mc "You can feel yourself coming undone."
  mc "You cry out her name, and it tastes so sweet on your lips."
  # mc "You arch your back and curl your toes, mouth open in a silent scream as the full force of your pleasure tears through you."
  mc "You arch your back and curl your toes, mouth open in a silent scream{space=-30}\nas the full force of your pleasure tears through you."
  # mc "Just as it does, [maya] shoves her tongue back into your pussy as deep as it will go."
  mc "Just as it does, [maya] shoves her tongue back into your pussy as deep{space=-45}\nas it will go."
  window hide
  pause 0.125
  show flora dnd_taste orgasm with hpunch
  pause 0.25
  window auto
  mc "You unleash a torrent of wetness, squirting into her mouth."
  mc "She drinks it up, sucking down all of your juices with fevered need."
  # mc "You go rigid for what feels like forever as you convulse there on the ground."
  mc "You go rigid for what feels like forever as you convulse there on\nthe ground."
  window hide
  pause 0.125
  show flora dnd_taste aftermath with Dissolve(.5)
  pause 0.25
  window auto
  mc "[maya] withdraws her tongue, but continues to slurp up the mess you've made."
  mc "Running her tongue up and down your folds, down to your ass."
  mc "Enjoying the salt of your skin and the sweetness of your pussy."
  mc "You smile up at her, your eyes half lidded, your body spent from the ecstasy."
  mc "That's the best you've ever had. No one has made you cum like that before."
  window hide
  pause 0.125
  show flora dnd_taste mayaless with Dissolve(.5)
  pause 0.25
  window auto
  # mc "[maya] falls back next to you, also spent, and also savoring the moment."
  mc "[maya] falls back next to you, also spent, and also savoring the moment.{space=-60}"
  mc "But you're not quite done."
  mc "There's something else you've always wanted to try..."
  window hide
  show flora dnd_scissor straddle
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "You get up, [flora], and climb on over [maya]."
  # mc "You hook one of her legs around your hip, then you straddle her pussy, feeling her wetness against your own."
  mc "You hook one of her legs around your hip, then you straddle her pussy,{space=-60}\nfeeling her wetness against your own."
  window hide
  pause 0.125
  show flora dnd_scissor kiss with Dissolve(.5)
  pause 0.25
  window auto
  mc "You lean forward and kiss her slowly, tasting yourself on her lips."
  window hide
  pause 0.125
  show flora dnd_scissor french_kiss with Dissolve(.5)
  pause 0.25
  window auto
  # mc "She groans gently beneath you, then forces your lips apart with her tongue."
  mc "She groans gently beneath you, then forces your lips apart with\nher tongue."
  mc "She darts it inside of your mouth, making you taste everything."
  mc "That's when you begin to grind your hips down into her."
  window hide
  pause 0.125
  show flora dnd_scissor scissoring1 with Dissolve(.5)
  pause 0.25
  show flora dnd_scissor scissoring2 with Dissolve(.2)
  show flora dnd_scissor scissoring4 with Dissolve(.2)
  show flora dnd_scissor scissoring2 with Dissolve(.2)
  show flora dnd_scissor scissoring1 with Dissolve(.2)
  pause 0.1
  show flora dnd_scissor scissoring3 with Dissolve(.2)
  show flora dnd_scissor scissoring4 with Dissolve(.2)
  show flora dnd_scissor scissoring3 with Dissolve(.2)
  show flora dnd_scissor scissoring1 with Dissolve(.2)
  pause 0.1
  show flora dnd_scissor scissoring2 with Dissolve(.2)
  show flora dnd_scissor scissoring4 with Dissolve(.2)
  show flora dnd_scissor scissoring2 with Dissolve(.2)
  show flora dnd_scissor scissoring1 with Dissolve(.2)
  pause 0.0
  show flora dnd_scissor scissoring3 with Dissolve(.2)
  show flora dnd_scissor scissoring4 with Dissolve(.2)
  show flora dnd_scissor scissoring3 with Dissolve(.2)
  show flora dnd_scissor scissoring1 with Dissolve(.2)
  pause 0.25
  window auto
  mc "Your slick, sex lubricated pussy slides against her own swollen bud."
  mc "You grind your clit against her clit, riding her vagina."
  # mc "[maya] gasps and bucks beneath you, thrusting up at you even as you continue to gyrate on her."
  mc "[maya] gasps and bucks beneath you, thrusting up at you even as you{space=-10}\ncontinue to gyrate on her."
  window hide
  pause 0.125
  show flora dnd_scissor scissoring5 with Dissolve(.5)
  pause 0.25
  show flora dnd_scissor scissoring6 with Dissolve(.2)
  show flora dnd_scissor scissoring8 with Dissolve(.2)
  show flora dnd_scissor scissoring6 with Dissolve(.2)
  show flora dnd_scissor scissoring5 with Dissolve(.2)
  pause 0.0
  show flora dnd_scissor scissoring6 with Dissolve(.2)
  show flora dnd_scissor scissoring8 with Dissolve(.2)
  show flora dnd_scissor scissoring6 with Dissolve(.2)
  show flora dnd_scissor scissoring5 with Dissolve(.2)
  pause 0.1
  show flora dnd_scissor scissoring7 with Dissolve(.2)
  show flora dnd_scissor scissoring8 with Dissolve(.2)
  show flora dnd_scissor scissoring7 with Dissolve(.2)
  show flora dnd_scissor scissoring5 with Dissolve(.2)
  pause 0.0
  show flora dnd_scissor scissoring7 with Dissolve(.2)
  show flora dnd_scissor scissoring8 with Dissolve(.2)
  show flora dnd_scissor scissoring7 with Dissolve(.2)
  show flora dnd_scissor scissoring5 with Dissolve(.2)
  pause 0.1
  show flora dnd_scissor scissoring6 with Dissolve(.175)
  show flora dnd_scissor scissoring8 with Dissolve(.175)
  show flora dnd_scissor scissoring6 with Dissolve(.175)
  show flora dnd_scissor scissoring5 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring7 with Dissolve(.175)
  show flora dnd_scissor scissoring8 with Dissolve(.175)
  show flora dnd_scissor scissoring7 with Dissolve(.175)
  show flora dnd_scissor scissoring5 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring6 with Dissolve(.175)
  show flora dnd_scissor scissoring8 with Dissolve(.175)
  show flora dnd_scissor scissoring6 with Dissolve(.175)
  show flora dnd_scissor scissoring5 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring7 with Dissolve(.175)
  show flora dnd_scissor scissoring8 with Dissolve(.175)
  show flora dnd_scissor scissoring7 with Dissolve(.175)
  show flora dnd_scissor scissoring5 with Dissolve(.175)
  pause 0.25
  window auto
  mc "She reaches up with her hands and squeezes your tits, massaging the tender flesh."
  # mc "Up, down, together, she plays with your breasts, before letting them fall again."
  mc "Up, down, together, she plays with your breasts, before letting them{space=-5}\nfall again."
  window hide
  pause 0.125
  show flora dnd_scissor scissoring9 with Dissolve(.5)
  pause 0.25
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.05
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.05
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring10 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.0
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring12 with Dissolve(.175)
  show flora dnd_scissor scissoring11 with Dissolve(.175)
  show flora dnd_scissor scissoring9 with Dissolve(.175)
  pause 0.25
  window auto
  # mc "From there, she trails hands down your body and stops at your hips."
  mc "From there, she trails hands down your body and stops at your hips.{space=-15}"
  mc "She grabs hold and urges you on, slamming your pussy down onto her own."
  mc "You can feel another surge of pleasure cresting, so you go faster, riding it out."
  mc "It hits you both at the same time as your sensitive clits rub together faster and faster."
  window hide
  pause 0.125
  show flora dnd_scissor orgasm with hpunch
  pause 0.25
  window auto
  # mc "You cry out her name and throw your head back when the dam finally breaks."
  mc "You cry out her name and throw your head back when the dam finally{space=-25}\nbreaks."
  # mc "She bucks upwards like a wild mustang and together you hit the peak of pleasure."
  mc "She bucks upwards like a wild mustang and together you hit the peak{space=-30}\nof pleasure."
  mc "Your eyes flutter shut as you get lost in complete bliss."
  $unlock_replay("flora_last_wish")
  window hide
  hide screen interface_hider
  # pause 0.0
  show jo concerned
  show black
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 0.125
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  mc "The pair of you let it roll through you for what feels like ages."
  # mc "Nothing else exists but the two of you. Everything else has fallen away."
  mc "Nothing else exists but the two of you. Everything else has fallen away.{space=-60}"
  mc "The world... the dragon... the cave..."
  # mc "Nothing matters to you anymore but the feel of [maya]'s skin on your own."
  mc "Nothing matters to you anymore but the feel of [maya]'s skin on\nyour own."
  mc "You could stay tangled up in her forever, and when you open your eyes, you—"
  show black onlayer screens zorder 100
  $game.advance(hours=2)
  $quest.maya_quixote.advance("power_back")
  $game.location = "home_bedroom"
  pause 0.5
  hide flora
  hide maya
  hide fantasy_feeling
  hide black
  hide black onlayer screens
  # pause 0.0
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  jo concerned "[mc]? What are you doing?"
  show jo concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I was, err... playing.\"":
      show jo concerned at move_to(.5)
      mc "I was, err... playing."
      jo neutral "By yourself?"
      mc "No! The girls were here!"
      jo concerned_hands_down "Were they? Huh."
      "She doesn't have to sound so shocked..."
      mc "They were. It was really nice hanging out with [flora]."
      mc "Like the good old days when we were younger."
      jo smile "Aw, sweetheart! That's nice."
      $jo.lust+=1
      jo smile "I'm glad you two were spending some quality time."
      mc "Yeah, me too..."
    "\"Nothing!\"":
      show jo concerned at move_to(.5)
      mc "Nothing!"
      jo skeptical "Well, that's the problem, isn't it?"
      # jo skeptical "There are plenty of chores you could be doing to help out around here."
      jo skeptical "There are plenty of chores you could be doing to help out around here.{space=-55}"
      jo skeptical "I won't tolerate any laziness."
      mc "Ugh, can't you get off my case for once?"
      mc "I was just waiting out the storm, okay?"
      $jo.love-=1
      jo annoyed "Don't take that tone of voice with me, young man."
      jo annoyed "And the storm has already passed and the power is back."
      jo annoyed "So, stop messing around."
      "God, never a happy moment."
      mc "Sorry, [jo]..."
      jo sad "Thank you."
    "\"Huh?\"":
      show jo concerned at move_to(.5)
      mc "Huh?"
      jo neutral "I said, what are you doing?"
      mc "Oh, uh. I was... meditating."
      jo neutral "Meditating?"
      $mc.intellect+=1
      mc "Err, yes! It's supposed to really help with focus and overall health!"
      mc "I figured it could help me focus more in school. Improve my grades."
      $jo.love+=1
      jo smile "Oh! What a wonderful idea!"
      jo smile "I'll just leave you to it then, sweetheart."
      mc "Thanks, [jo]!"
  window hide
  if renpy.showing("jo smile"):
    show jo smile at disappear_to_left
  elif renpy.showing("jo sad"):
    show jo sad at disappear_to_left
  pause 0.5
  window auto
  "Goddamn. I was really getting into that before [jo] came and interrupted me."
  "I can't believe the girls just left in the middle of the game like that!"
  "..."
  "Or that they even played with me in the first place..."
  "That's something [flora] would have never done before."
  "It feels good getting to know her better and spending some quality time together."
  "And [maya] too, for that matter."
  "In my old life, I didn't even dare to look her way, much less have a conversation with her."
  "Less still, do something enjoyable together."
  "And even though they both got mad at the end and left, they did seem to enjoy it to some extent."
  "Perhaps I did go overboard with the sex?"
  "Maybe that's a lesson worth learning..."
  "Tone it down and adjust. Adapt and overcome."
  "Maybe next time, I'll focus more on what they want, instead of selfishly taking the spotlight."
  "They did seem to enjoy the parts where they had a say."
  "..."
  "It's crazy to think that this all happened by chance."
  "If it weren't for the power going out, we would never have played."
  "Maybe that's something I need to keep in mind, as well."
  # "When an opportunity presents itself, you can't just let it pass you by."
  "When an opportunity presents itself, you can't just let it pass you by.{space=-20}"
  "That's what I used to do, and it led to misery."
  "Sometimes you'll make mistakes, like I just did, but it's still better than doing nothing."
  "It's better than watching, waiting, hoping, praying that you by some miracle live a perfect life."
  "Because that's not living at all."
  window hide
  $quest.maya_quixote.finish()
  $flora.outfit = flora["outfit_stamp"]
  $maya.outfit = maya["outfit_stamp"]
  $home_hall["table_taken"] = False
  pause 0.25
  return
