label quest_maya_spell_start:
  show maya bored with Dissolve(.5)
  mc "Hey, [maya]! Guess what?"
  maya bored "No."
  mc "No?"
  maya bored "I'll say it again in Spanish."
  maya bored "No."
  mc "Fine, don't guess."
  maya bored "I won't."
  maya bored "Even though the guessing game is my favorite."
  mc "Liar."
  maya cringe "I could have days left to live. Days!"
  maya cringe "How could you waste my precious time like this?"
  mc "You're not going to die, okay?"
  maya dramatic "I don't know, [mc]."
  maya dramatic "Spontaneous fires... oversalted cafeteria food... crossing the street without looking both ways..."
  maya dramatic "My days seem to be numbered."
  show maya dramatic at move_to(.75)
  menu(side="left"):
    extend ""
#   "\"Stop messing around and just listen for a sec.\"":
    "\"Stop messing around\nand just listen for a sec.\"":
      show maya dramatic at move_to(.5)
      mc "Stop messing around and just listen for a sec."
      maya sarcastic "By all means! Listening is my favorite thing."
      $maya.lust+=1
      maya sarcastic "Right after Monday mornings and STDs, of course."
      mc "Mm-hmm."
#   "\"By my count, you have exactly 666 days left.\"":
    "\"By my count, you have\nexactly 666 days left.\"":
      show maya dramatic at move_to(.5)
      mc "By my count, you have exactly 666 days left."
      $maya.love+=1
      maya sarcastic "Hah! My favorite number, right after 69."
      mc "I thought it was fitting."
    "I'm sorry, I didn't think—":
      show maya dramatic at move_to(.5)
      mc "I'm sorry, I didn't think—"
      $maya.love-=1
      maya dramatic "Oh, chill out, [mc]."
      maya dramatic "It's not like anyone has died from a little fire."
      mc "I mean—"
      maya sarcastic "Big fires, on the other hand..."
      mc "..."
  mc "Anyway, I have something for you."
  maya flirty "Oh? The last time I heard that, I was left bitterly disappointed next to a snoring boy."
  mc "Very funny. But you won't be this time."
  maya kiss "So self-assured! All right, big boy, let's see it."
  mc "I got a hold of a spellbook to see if we can fix your curse problem."
  maya thinking "A spellbook? Are you going to boil and toil my troubles?"
  mc "Something like that. I was actually thinking a banishing spell."
  mc "If it is a spirit or something, we can apparently get rid of it that way."
  mc "We'd just need to collect some ingredients for the spell, then we—"
  maya smile "Are you taking me on a real scavenger hunt?"
  maya smile "A real adventure?"
  mc "Err, I guess?"
  maya smile "LARPing is like my favorite thing ever! Like ever, ever!"
  mc "Damn, you know what LARPing is?"
  maya smile "Of course I do!"
  maya annoyed "Losers Acting Really Pathetic."
  "Crap. I knew it was too good to be true."
  maya annoyed "Not to wrinkle your wizard robes, but I'd rather not waste my time."
  maya eyeroll "Besides, I haven't had an incident in a while. So, it's whatever."
  "Ugh, I can't believe I've been lugging around this spellbook for nothing!"
# "Especially when [maya] was apparently never serious about the curse thing..."
  "Especially when [maya] was apparently never serious about the\ncurse thing..."
  "I should have known."
  mc "All right, fine. Forget it."
  maya neutral "It's whatever, [mc]. You can stop making me your project now."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "Welp, so much for that."
  "..."
  "Although, I still have the stupid book..."
  "I might as well take a look and see if it's actually worth it, right?"
  "Just in case. It's not like I'm obsessed with her or anything."
  window hide
  $quest.maya_spell.start("book")
  return

label quest_maya_spell_book_bippity_boppity_bitch(item):
  pause 0.125
  "When in doubt, turn to the dark arts."
  "Let's see here..."
  "..."
  "..."
  "Oh, this one looks promising!"
  "{i}\"Banishing 101: Magic Made Simple(er).\"{/}"
  "{i}\"So, did you piss off a dead relative or some shit?\"{/}"
  "{i}\"Well, here's a spell for you — so easy a child could do it!\"{/}"
# "{i}\"If you're as capable as a toddler, acquire a strand of hair from a bitch (not a dog), sweat from a female horndog (not a bitch), and a spritz of frog's breath*.\"{/}"
  "{i}\"If you're as capable as a toddler, acquire a strand of hair from a bitch (not a dog), sweat from a female horndog (not a bitch), and\na spritz of frog's breath*.\"{/}"
  "{i}\"*Note: Frog's breath must come from actual frog(s). Your smelly uncle Joe doesn't count.\"{/}"
  "{i}\"Combine these items, and utter the following words around the cliché hour of midnight.\"{/}"
  "{i}\"Step off, haunting bitch!\"{/}"
  "{i}\"If this doesn't do the trick, you biffed it somewhere, so try again or order our newest edition online!\"{/}"
  "Well, that sounds easy enough..."
  "I better start collecting the ingredients."
  $quest.maya_spell.advance("hair")
  return

label quest_maya_spell_book_hex_vex_and_texmex(item):
  pause 0.125
  "Let's see here..."
  "Banishing spell... banishing spell..."
  "..."
  "..."
  "Ah, here we go!"
  "{i}\"Looking to banish all things ghostly, pestly, stomach crampy, or otherwise unsavory?\"{/}"
  "{i}\"Search no more! We have just the spell for you!\"{/}"
  "{i}\"For this recipe you will need a single hair from an enemy, sweat of a free spirit, and the fetid breath of a frog or two.\"{/}"
# "{i}\"Mix it all together, and this little concoction will banish the peskiest of poltergeists!\"{/}"
  "{i}\"Mix it all together, and this little concoction will banish the peskiest{space=-5}\nof poltergeists!\"{/}"
  "{i}\"To complete the ritual, say the following three times at midnight.\"{/}"
  "{i}\"I banish thee, spiritual free (loader). Get thee gone out of here, of thee I have no fear.\"{/}"
  "{i}\"And that's it!\"{/}"
# "{i}\"You should now be cured of all spooking, haunting, bloating, cramping, and sharting (did you try our chili?).\"{/}"
  "{i}\"You should now be cured of all spooking, haunting, bloating, cramping,{space=-80}\nand sharting (did you try our chili?).\"{/}"
  "{i}\"Turn to the next page for a queso dip that is simply to die for!\"{/}"
  "Well, that sounds easy enough..."
  "I better start collecting the ingredients."
  $quest.maya_spell.advance("hair")
  return

label quest_maya_spell_hair_pause:
  pause 0.25

label quest_maya_spell_hair_upon_entering:
  if quest.kate_stepping.finished:
    "Well, there's [isabelle], looking as approachable as ever."
  elif quest.isabelle_dethroning.finished:
    "Well, there's [kate], looking as approachable as ever."
  "About as approachable as a bear waking up from hibernation..."
  "But, what must be done, must be done."
  "Surely, I can manage a single strand of hair."
  "How hard can it be, really?"
  $quest.maya_spell["must_be_done"] = True
  return

label quest_maya_spell_hair_kate:
  show kate skeptical with Dissolve(.5)
  kate skeptical "What the hell are you staring at?"
  show kate skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.strength>=10@[mc.strength]/10|{image=stats str}|Take a hair from [kate]":
      show kate skeptical at move_to(.5)
      mc "You, obviously."
      kate eyeroll "Stop, then. Go perv on someone else."
      mc "Actually, I need something from you."
      kate excited "As if I would give you anything."
      "I guess I'll have to take what I want, like a real man."
      if quest.isabelle_dethroning["kate_sex"]:
        "Just like I did that night I took everything from her."
      "I'll just go up to her and yank a piece from her head."
      mc "Well, I'm not asking."
      kate annoyed "Look, if you don't—"
      window hide None
      hide screen interface_hider
      show kate embarrassed
      show black
      show black onlayer screens zorder 100
      with vpunch
      pause 0.25
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      kate embarrassed "Ouch!"
      kate embarrassed "What do you think you're doing, you freak?!"
      show black onlayer screens zorder 100
      pause 0.5
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      pause 0.125
      $mc.add_item("kate_hair")
      pause 0.25
      window auto
      $set_dialog_mode("")
      mc "I just needed a piece of your hair."
      kate embarrassed "Ew! For some weird little shrine or something?"
      kate embarrassed "You can't have it!"
      mc "Too late."
      kate thinking "Put your hands on me again and they'll be tied behind your back for the rest of the year."
      mc "Really? And who's going to help you do that?"
      mc "I'm stronger than you without your little posse around."
      mc "Maybe {i}I'll{/} tie your hands behind your back and watch you squirm."
      kate blush "I'd like to see you try."
      mc "Don't tempt me, [kate]."
      if quest.kate_moment.finished:
        mc "Or I could always just turn off the lights..."
        kate surprised "W-what did you say?"
        mc "You know, like when—"
        kate sad "T-that was nothing!"
        mc "It didn't look like nothing."
        kate sad_right "..."
      else:
        kate surprised "..."
        show kate sad with Dissolve(.5)
#     kate "Just leave me alone and don't touch me again or I'll... I'll get [jo] involved."
      kate "Just leave me alone and don't touch me again or I'll... I'll get [jo] involved.{space=-90}"
      mc "Yeah, yeah. I'm going."
      kate sad "Good, and stay out of my way."
      mc "Or maybe {i}you{/} should stay out of mine."
      kate surprised "But, err... [jo]..."
      mc "I'm not scared of your threats."
      kate surprised "..."
      "She opens her mouth, ready to retort."
      "But this time, I stand tall and stare her down."
      "Ready to make good on my threat of physically besting her."
      kate sad_right_hands_up "...whatever, creep."
      window hide
      $quest.maya_spell.advance("sweat")
      if kate.location == game.location:
        hide kate with Dissolve(.5)
      else:
        show kate sad_right_hands_up at disappear_to_right
        pause 0.5
      window auto
      "Man, that felt good!"
#     "Before taking her down, I never would have dared to do such a thing."
      "Before taking her down, I never would have dared to do such a thing.{space=-25}"
      "But now she just has to take it."
      "Never did turning tables feel more satisfying or just..."
    "?mc.charisma>=6@[mc.charisma]/6|{image=stats cha}|Convince [kate] to give you a hair":
      show kate skeptical at move_to(.5)
      mc "I wasn't staring."
      kate excited "You know what they say about liars, don't you?"
      mc "Err, what?"
      kate excited "Their pants get taken and set on fire."
      mc "Good thing I'm not lying, then."
      mc "But if you want my pants off, all you had to do was ask."
      kate cringe "Ew! Don't make me vomit!"
      mc "You're the one trying to get me naked..."
      kate cringe "Seriously, stop!"
      mc "I am a hot commodity."
      kate eyeroll "Oh, please."
      kate eyeroll "What do you even want?"
      show kate eyeroll at move_to(.75)
      menu(side="left"):
        extend ""
        "\"To be honest, I was admiring something.\"":
          show kate eyeroll at move_to(.5)
          mc "To be honest, I was admiring something."
          kate laughing "Ha! I knew it."
          kate laughing "Once a perv, always a perv."
          mc "It's the smell of your shampoo."
          kate smile "Ew! Were you smelling me?"
          mc "If I could just... touch it..."
          kate neutral "Don't even think about it."
          "I've never played Russian roulette with a bloodthirsty shark before, but I imagine it feels a little something like this..."
          kate neutral "Seriously, don't—"
          window hide None
          hide screen interface_hider
          show black
          show black onlayer screens zorder 100
          with vpunch
          pause 0.25
          show black onlayer screens zorder 4
          $set_dialog_mode("default_no_bg")
          "In a moment of reckless courage, I reach out and take a handful of her luscious blonde locks."
          "The silky smoothness runs over my skin like spring water."
          "Then, I yank a single strand, turn tail, and run as fast as I can."
          show black onlayer screens zorder 100
          $game.location = "school_music_class"
          pause 1.5
          hide kate
          hide black
          hide black onlayer screens
          show screen interface_hider
          with Dissolve(.5)
          pause 0.125
          $mc.add_item("kate_hair")
          pause 0.25
          window auto
          $set_dialog_mode("")
          $kate.lust-=3
          kate "{i}You're going to regret that, you handsy little cretin!{/}"
          "Heh, she might make good on that later..."
          "But for now, I got what I came here for."
          $quest.maya_spell.advance("sweat")
        "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"To warn you.\"":
          show kate eyeroll at move_to(.5)
          mc "To warn you."
          kate thinking "I don't have time for your cryptic little nerd games."
          mc "Okay, but..."
          kate thinking "But? Spit it out."
          mc "Well, there's been a lice outbreak."
          $mc.intellect+=1
          if quest.nurse_aid["name_reveal"]:
            mc "And the nurse has asked me to collect samples of hair to see how far it has spread."
          else:
            mc "And the [nurse] has asked me to collect samples of hair to see how far it has spread."
          kate embarrassed "Ew! That's so gross!"
          kate embarrassed "I bet that unwashed Brit is the plague starter."
          mc "It could be."
#         mc "So, can I take a sample? You'll want to stop it quickly if it has jumped to you."
          mc "So, can I take a sample? You'll want to stop it quickly if it has jumped{space=-25}\nto you."
          kate sad "Oh, my god, fine! Just take it!"
          kate sad "I need a shower..."
          window hide
          $mc.add_item("kate_hair")
          pause 0.25
          window auto
          mc "Okay, that will do. Thank you for your cooperation."
          kate surprised "Whatever! Just move!"
          window hide
          show kate surprised at disappear_to_right
          pause 1.0
          $quest.maya_spell.advance("sweat")
#   "?mc.owned_item('bippity_boppity_bitch')@|{image=items book bippity_boppity_bitch}|\"Give me a piece of your hair, bitch.\"":
    "?mc.owned_item('bippity_boppity_bitch')@|{image=items book bippity_boppity_bitch}|\"Give me a piece\nof your hair, bitch.\"":
      show kate skeptical at move_to(.5)
      mc "Give me a piece of your hair, bitch."
      $kate.lust-=3
      $kate.love-=1
      kate annoyed "What did you just say to me?"
#     mc "Chippity, choppity. Give me a piece, else you'll end up with hair disease."
      mc "Chippity, choppity. Give me a piece, else you'll end up with hair disease.{space=-70}"
      kate embarrassed "..."
      kate embarrassed "I've never heard anything quite as cringe."
      kate embarrassed "Get out of my face, weirdo."
      window hide
      $quest.maya_spell.advance("sweat")
      if kate.location == game.location:
        hide kate with Dissolve(.5)
      else:
        show kate embarrassed at disappear_to_left
        pause 0.5
      window auto
      "Oh, well. It was worth a shot."
      "..."
      "Hold on, what's this?"
      "I can't tell if this hair is from her head, but it's certainly blonde..."
      window hide
      $mc.add_item("kate_hair")
      pause 0.25
      window auto
      "I also can't tell if it was thanks to the spellbook or just dumb luck."
      "Either way, time to find myself some sweat!"
    "?mc.owned_item('hex_vex_and_texmex')@|{image=items book hex_vex_and_texmex}|\"I just need a piece of your hair.\"":
      show kate skeptical at move_to(.5)
      mc "I just need a piece of your hair."
      kate excited "Keep dreaming, weirdo."
      mc "It's nothing weird, okay? Just an ingredient I need for something."
      kate cringe "..."
      kate cringe "That's actually disgusting."
      mc "Fine, I'll do it by the book."
      kate afraid "What?"
      mc "Hair, care, and all things rare, a strand of your hair do share."
      kate thinking "..."
      kate thinking "Why are you rhyming now?"
      "Crap. Did it not work?"
      mc "I, err... I read about this homemade shampoo recipe and thought I could share it with you."
      kate embarrassed "No, thanks. I'm not poor enough for that."
      kate embarrassed "Now get out of my face."
      window hide
      $quest.maya_spell.advance("sweat")
      if kate.location == game.location:
        hide kate with Dissolve(.5)
      else:
        show kate embarrassed at disappear_to_left
        pause 0.5
      window auto
      "Ugh, [kate] certainly hasn't lost all of her sass..."
      "..."
      "Hang on, a blonde hair!"
      window hide
      $mc.add_item("kate_hair")
      pause 0.25
      window auto
      "I'm not sure if it came from her or if it's even from her head, but it will do."
      "Now to find myself some sweat..."
  return

label quest_maya_spell_hair_isabelle:
  show isabelle skeptical with Dissolve(.5)
  isabelle skeptical "What do you want?"
  mc "Who? Me?"
  isabelle skeptical "Did [kate] put you up to something?"
  isabelle skeptical "Or are you just here to take the piss?"
  mc "First of all, your girlfriend has nothing to do with this."
  mc "Second of all, I'm here for an important matter."
  isabelle skeptical "And what could that possibly be?"
  show isabelle skeptical at move_to(.75)
  menu(side="left"):
    extend ""
#   "?mc.owned_item('hex_vex_and_texmex')@|{image=items book hex_vex_and_texmex}|\"I need a piece of your hair, please.\"":
    "?mc.owned_item('hex_vex_and_texmex')@|{image=items book hex_vex_and_texmex}|\"I need a piece of\nyour hair, please.\"":
      show isabelle skeptical at move_to(.5)
      mc "I need a piece of your hair, please."
      isabelle annoyed "Are you being serious right now?"
      mc "I'm dead serious."
      isabelle annoyed "No bloody way."
      mc "Then you leave me no choice."
      "Here goes nothing..."
      mc "Hair, care, and all things rare, a strand of your hair do share."
      isabelle subby "..."
      "[isabelle] goes slack and she stares at me for a moment, a calm look coming over her."
      isabelle subby "Okay, then."
      window hide
      $mc.add_item("isabelle_hair")
      pause 0.25
      window auto
      "She plucks a strand from her head and hands it over."
      "Is this the sort of power [kate] wields all the time? It must be nice."
      mc "Thanks!"
      window hide
      hide isabelle with Dissolve(.5)
      window auto
      "I can't tell if she is really that compliant and docile these days, or if the spell actually worked..."
      "Either way, I got what I came for."
#   "?mc.owned_item('bippity_boppity_bitch')@|{image=items book bippity_boppity_bitch}|\"I need a piece of British bitch hair.\"":
    "?mc.owned_item('bippity_boppity_bitch')@|{image=items book bippity_boppity_bitch}|\"I need a piece of\nBritish bitch hair.\"":
      show isabelle skeptical at move_to(.5)
      mc "I need a piece of British bitch hair."
      isabelle angry "Excuse me?!"
      isabelle angry "You're way out of line right now, [mc]!"
      mc "Nah, I'm actually keeping you in line."
#     mc "Chippity, choppity. Give me a piece, else you'll end up with hair disease."
      mc "Chippity, choppity. Give me a piece, else you'll end up with hair disease.{space=-70}"
      isabelle subby "..."
#     "Just as she's about to keep telling me off, the fire and rage suddenly disappears from her face."
      "Just as she's about to keep telling me off, the fire and rage suddenly{space=-20}\ndisappears from her face."
      "She stares at me for a moment, then gives a little nod."
      isabelle subby "Here you go..."
      window hide
      $mc.add_item("isabelle_hair")
      pause 0.25
      window auto
      "Holy shit, that's a magic trick and a half!"
      "I wonder what else I could get away with? No time to find out now, but maybe later..."
      "This must be how [kate] feels all the time."
      mc "Good girl."
      window hide
      hide isabelle with Dissolve(.5)
#   "?mc.charisma>=8@[mc.charisma]/8|{image=stats cha}|\"There's a hair donation drive for those bald kids.\"":
    "?mc.charisma>=8@[mc.charisma]/8|{image=stats cha}|\"There's a hair donation\ndrive for those bald kids.\"":
      show isabelle skeptical at move_to(.5)
      mc "There's a hair donation drive for those bald kids."
      isabelle sad "Cancer patients?"
      mc "Yeah, maybe."
      mc "[jo] is in charge of the whole gig. Schoolwide thing, you know?"
      mc "Anyway, I wanted to see if you would like to donate? It's for a good cause, after all."
      isabelle sad "You're right, that is a good cause..."
      "I can see the wheels spinning in her head."
#     "She hardly wants anything to do with me these days, understandably."
      "She hardly wants anything to do with me these days, understandably.{space=-30}"
      "But she can't deny the calling of a good deed either."
      isabelle subby "All right, fine. I'll do it."
      mc "Great! Let me just take a look here..."
      window hide
      show location with vpunch2
      $mc.add_item("isabelle_hair")
      pause 0.25
      window auto show
      show isabelle angry with dissolve2
      "Snip! Hair acquired."
      isabelle angry "Hey! That hurt!"
      mc "Sorry, the quality isn't quite what they're looking for."
      mc "Thanks anyway!"
      isabelle angry "That's just bloody rude!"
      mc "Sorry!"
      window hide
      hide isabelle with Dissolve(.5)
      window auto
      "Not sorry. I got what I needed."
    "?mc.intellect>=8@[mc.intellect]/8|{image=stats int}|\"I come on official business.\"":
      show isabelle skeptical at move_to(.5)
      mc "I come on official business."
      isabelle eyeroll "Whose?"
      if quest.nurse_aid["name_reveal"]:
        mc "The nurse's."
      else:
        mc "The [nurse]'s."
      isabelle eyeroll "And why wouldn't she just come herself?"
      mc "She has her hands tied."
      if mc.owned_item(("compromising_photo","damning_document")):
#       "Knowing her, that could be true in both a literal and figurative sense..."
        "Knowing her, that could be true in both a literal and figurative sense...{space=-35}"
      isabelle thinking "Well, what is it, then?"
      $mc.intellect+=1
      mc "There's been a lice outbreak."
      isabelle afraid "Ew! How did that happen?"
      if quest.nurse_aid["name_reveal"]:
        # mc "I can't say, but the nurse has requested hair samples to narrow down the source of the outbreak."
        mc "I can't say, but the nurse has requested hair samples to narrow down{space=-30}\nthe source of the outbreak."
      else:
        # mc "I can't say, but the [nurse] has requested hair samples to narrow down the source of the outbreak."
        mc "I can't say, but the [nurse] has requested hair samples to narrow down{space=-30}\nthe source of the outbreak."
      isabelle cringe "Ugh, that's bloody brilliant..."
#     isabelle cringe "But whatever helps narrow it down to keep the school safe and clean, I guess. Here you go."
      isabelle cringe "But whatever helps narrow it down to keep the school safe and clean,{space=-35}\nI guess. Here you go."
      window hide
      $mc.add_item("isabelle_hair")
      pause 0.25
      window auto
      mc "Thanks! You've done the school a service."
      window hide
      hide isabelle with Dissolve(.5)
  $quest.maya_spell.advance("sweat")
  return

label quest_maya_spell_sweat_upon_entering:
  if mc.owned_item("bippity_boppity_bitch"):
    "Next up, some sweat from a female horndog..."
    "And who here is hornier than [jacklyn]?"
  elif mc.owned_item("hex_vex_and_texmex"):
    "Next up, the sweat of a free spirit..."
    "And who here is more free spirited than [jacklyn]?"
  $quest.maya_spell["more_than_jacklyn"] = True
  return

label quest_maya_spell_sweat:
  show jacklyn smile with Dissolve(.5)
  jacklyn smile "Looking peaky, climber. How's the altitude?"
  mc "Oh, you know, thin but breathable."
  jacklyn smile "Right on. The most our air sacs can hope for."
  mc "Exactly..."
  mc "So, there was something I wanted to ask of you."
  jacklyn neutral "Straight to the point, just like Seurat?"
  jacklyn neutral "I admire the boldness."
  mc "Err, thanks?"
  jacklyn neutral "Tell me what's blending your dots."
  show jacklyn neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Can I have some of your sweat?\"":
      show jacklyn neutral at move_to(.5)
      mc "Can I have some of your sweat?"
      jacklyn laughing "Not beating around the bodily fluid ficus, I see."
      mc "It's for, err... an art project!"
#     jacklyn excited "That's cheese spread! Why didn't you just drop the spice and say so?"
      jacklyn excited "That's cheese spread! Why didn't you just drop the spice and say so?{space=-35}"
      mc "I, um... I wanted to keep it a surprise."
      jacklyn excited "Using sweat, huh?"
      mc "It's a statement... on repression... and freedom..."
      jacklyn excited "That's as avant garde as what the iceberg did to the Titanic."
      mc "So, is that a yes?"
      jacklyn laughing "I'm not one to blue ball art, my dude."
#     jacklyn laughing "I'm down to give you a bit of the old skin juice for the sake of expression."
      jacklyn laughing "I'm down to give you a bit of the old skin juice for the sake of expression.{space=-110}"
      mc "Thanks, [jacklyn]!"
      jacklyn excited "Let me just generate the spin..."
      window hide
      show jacklyn jumping_jacks up
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      pause 0.25
      show jacklyn jumping_jacks down with Dissolve(.5)
      window auto
      "She begins to do jumping jacks right then and there."
      window hide
      pause 0.125
      show jacklyn jumping_jacks up with Dissolve(.5)
      pause 0.25
      show jacklyn jumping_jacks down with Dissolve(.5)
      pause 0.125
      window auto
      "Up and down she bounces, her arms going above her head, then to her side, then repeat."
      window hide
      pause 0.125
      show jacklyn jumping_jacks up with Dissolve(.5)
      pause 0.25
      show jacklyn jumping_jacks down with Dissolve(.5)
      pause 0.125
      window auto
      "I can't help but stare at her perfect tits as they jiggle up and down in her tight shirt, working with gravity."
      window hide
      pause 0.125
      show jacklyn jumping_jacks up with Dissolve(.5)
      pause 0.25
      show jacklyn jumping_jacks down with Dissolve(.5)
      pause 0.125
      window auto
      mc "That's... perfect."
      $unlock_replay("jacklyn_solo_workout")
      window hide
      show jacklyn smile
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
#     "Finally, she stops jumping up and down and gives me a wicked smile."
      "Finally, she stops jumping up and down and gives me a wicked smile.{space=-25}"
      jacklyn smile "Does that cure your ham?"
      window hide
      $mc.add_item("jacklyn_sweat")
      pause 0.25
      window auto
      mc "Err, yes! This is great."
      mc "Thanks again!"
      jacklyn smile "Magnum. Keep burning outside the fire, [mc]."
      window hide
      hide jacklyn with Dissolve(.5)
#   "?quest.jacklyn_statement['ending'] in ('anal','pegging')@|{image=flora_squid_pegging}|\"I want to get sweaty with you again.\"" if quest.jacklyn_statement["ending"] == "pegging":
    "?quest.jacklyn_statement['ending'] in ('anal','pegging')@|{image=flora_squid_pegging}|\"I want to get sweaty\nwith you again.\"" if quest.jacklyn_statement["ending"] == "pegging":
      jump quest_maya_spell_sweat_sex
#   "?quest.jacklyn_statement['ending'] in ('anal','pegging')@|{image=flora_squid_anal}|\"I want to get sweaty with you again.\"" if quest.jacklyn_statement["ending"] != "pegging":
    "?quest.jacklyn_statement['ending'] in ('anal','pegging')@|{image=flora_squid_anal}|\"I want to get sweaty\nwith you again.\"" if quest.jacklyn_statement["ending"] != "pegging":
      label quest_maya_spell_sweat_sex:
        show jacklyn neutral at move_to(.5)
        mc "I want to get sweaty with you again."
        jacklyn laughing "Oh, my! Where's the Q in your inquiry alphabet?"
        mc "You're right, there's no question."
#       mc "Just two people enjoying each other's bodies, admiring life's natural sculptures."
        mc "Just two people enjoying each other's bodies, admiring life's natural{space=-5}\nsculptures."
        $jacklyn.love+=1
        jacklyn excited "Fair uno, I can dig that."
        mc "Really? Sweet!"
        mc "..."
        "Like a sprinter at the starting block, my hands settle on the hem of my shirt, ready to take off."
        "Never daring to miss an opportunity."
        jacklyn annoyed "Whoa, there! Jar your beans!"
        mc "Err, what?"
        jacklyn annoyed "I admire the flow of your current, Sparky... but let's art for the sake of the muse."
        "Ugh, does she think I meant actual sculptures?"
        jacklyn annoyed "You have to lay your base first."
        $mc.charisma+=1
        mc "First base. Got it."
        jacklyn laughing "The loose dog rattles its chain."
        "Not sure if [jacklyn] is calling me a dog, but I think I'll take it?"
        mc "Awhooo!"
        jacklyn laughing "I can appreciate the effort, my guy."
        mc "Yeah?"
        jacklyn smile "Sure, who am I to stifle creativity?"
        mc "Does that mean—"
        window hide
        pause 0.25
        $jacklyn.unequip("jacklyn_jacket")
        pause 0.75
        window auto
        "And just like that, she's stripping out of her clothes."
        window hide
        pause 0.25
        $jacklyn.unequip("jacklyn_shorts")
        pause 0.75
        window auto
        "I wasn't sure if my luck would hold, but it seems I've blown on the right dice."
        window hide
        pause 0.25
        $jacklyn.unequip("jacklyn_high_waist_fishnet")
        pause 0.75
        window auto
        "Held the right card up my sleeve."
        window hide
        pause 0.25
        $jacklyn.unequip("jacklyn_chained_choker")
        pause 0.75
        window auto
        "I went all in and now it's my time to take the jackpot."
        window hide
        pause 0.25
        $jacklyn.unequip("jacklyn_orange_bra")
        pause 0.75
        window auto
        "And it is just that — a jackpot."
        window hide
        pause 0.25
        $jacklyn.unequip("jacklyn_orange_panties")
        pause 0.75
        window auto
        "Having [jacklyn] suddenly completely naked, standing in front of me of all people."
        "She talks about art with such passion, such fire. And yet she is a work of art, herself."
#       "The sensual curve of her body, her supple lines imperfectly sculpted by whatever, whoever, could have made her."
        "The sensual curve of her body, her supple lines imperfectly sculpted{space=-10}\nby whatever, whoever, could have made her."
#       "Has true, raw beauty ever existed in a higher plane of existence than in the female form?"
        "Has true, raw beauty ever existed in a higher plane of existence than{space=-10}\nin the female form?"
        show jacklyn neutral with dissolve2
#       jacklyn neutral "Are you going to close your word trap and shed the polyester constraints or what?"
        jacklyn neutral "Are you going to close your word trap and shed the polyester constraints{space=-110}\nor what?"
        "Shit, my mouth was literally hanging open. Keep it together, [mc]!"
        mc "Err, yes! I'm going to shed them like skin!"
        jacklyn smile_hands_down "You're kinda twisted, aren't you?"
        mc "I, um..."
        jacklyn smile_hands_down "That seeds my strawberry."
        "Oh? That sounds... good?"
        "She hasn't gotten dressed and changed her mind, anyway."
        mc "..."
#       "One by one, my clothes land on the floor, until my nakedness radiates through the room."
        "One by one, my clothes land on the floor, until my nakedness radiates{space=-40}\nthrough the room."
#       "Admittedly, some women can make me feel awkward or embarrassed..."
        "Admittedly, some women can make me feel awkward or embarrassed...{space=-60}"
#       "...but with [jacklyn], it feels like she admires the human body for what it is, too."
        "...but with [jacklyn], it feels like she admires the human body for what{space=-15}\nit is, too."
        "We are art, in our way. Fucked up, terrible, beautiful art."
        "Sustained by the red blood beating through us."
        "The twisted bone and muscle holding us together."
        jacklyn smile_hands_down "That's opium."
        mc "Hell yeah, it is."
        window hide
        show jacklyn paint_shelf_sex kiss
        show black onlayer screens zorder 100
        with Dissolve(.5)
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        window auto
        "And then it happens, the moment I've always fantasized about."
        "The moment when inhibition and doubt dissipates."
        "The moment when I seize the moment, the day, the fucking life, and I kiss her hard, right on her full lips."
        "I can still hardly believe it, but I don't let her know that."
#       "The confidence surges through me in a way I never thought possible."
        "The confidence surges through me in a way I never thought possible.{space=-25}"
        "Her lips, her tongue, mine. All mine."
        "The subtle sweet, smoky taste of whiskey tingles on my own lips and I crave more."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex grabbing_dick with Dissolve(.5)
        pause 0.125
        window auto
        "The intensity, the sheer power of the moment. My hand tangled in her hair, my lips controlling hers."
        "But she knows how to play, and her hand is already reaching down, finding my aching weakness, gripping it with delicate fingers."
#       "She has all the finesse of a woman working a piece to her desire, so sure of the outcome."
        "She has all the finesse of a woman working a piece to her desire,\nso sure of the outcome."
#       "Her hold on me tightens and now she's fully stroking me, teasing me."
        "Her hold on me tightens, and she strokes me hard."
        "Forcing a desperate groan out of me, fanning my desire."
        "She's the kind of woman that will make you cum in her hand if you don't do anything about it."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex lifting_up with hpunch
        pause 0.125
        window auto
        "The kind that you have to break free from if you don't want an early ejaculation."
        "The kind that you have to pin against the wall if you want any say in the matter."
#       "She breaks the kiss and bites my ear, hard enough to make me shiver, and likely drawing blood."
        "She breaks the kiss and bites my ear, hard enough to make me shiver,{space=-40}\nand likely drawing blood."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex anticipation with Dissolve(.5)
        pause 0.125
        window auto
        jacklyn paint_shelf_sex anticipation "Okay, slinger. Show me that pump action."
        "She says the words in my ear as she clutches my shoulder with one hand and guides my cock inside of her with the other."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.5)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.2)
        pause 0.15
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.2)
        pause 0.15
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.2)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.2)
        pause 0.125
        window auto
        "The sensation sends a shock of excitement through me."
        "Her pussy lips part around me, welcoming my shaft."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding4 with hpunch
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding1_slash_penetration1 with Dissolve(.3)
        pause 0.1
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding4 with hpunch
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding1_slash_penetration1 with Dissolve(.3)
        pause 0.1
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding4 with hpunch
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.15)
        pause 0.125
        window auto
        "It's like sticking your dick inside a hot wet mouth."
        "So tight it feels like she's sucking me off."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.15)
        pause 0.0
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.15)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.15)
        pause 0.0
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.125)
        pause 0.0
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding4 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding3 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding2 with Dissolve(.125)
        show jacklyn paint_shelf_sex grinding1 with Dissolve(.125)
        pause 0.125
        window auto
        "Her hold on me tightens, her fingers digging in."
        "She rocks her hips into mine, urging me to keep going."
        "Still trying to control the situation."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration4 with hpunch
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.125)
        pause 0.1
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration4 with hpunch
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.125)
        pause 0.1
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration4 with hpunch
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.125)
        pause 0.1
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration4 with hpunch
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.125)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.125)
        pause 0.125
        window auto
        "Her will might be stronger than mine, but physically she's no match for me."
        "Stuck between a dick and a hard place, she can do nothing but take it hard and deep."
        "The supplies shake behind her, cans of paint threatening to topple over."
#       "But it doesn't matter. Nothing matters save for making her feel every last inch of me."
        "But it doesn't matter. Nothing matters save for making her feel every{space=-20}\nlast inch of me."
        "Never for a moment letting her forget whose dick she's currently impaled on and moaning over."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.1)
        pause 0.075
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.1)
        pause 0.075
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.1)
        pause 0.075
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.1)
        pause 0.075
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.1)
        pause 0.075
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.1)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.1)
        pause 0.125
        window auto
#       "Her breaths come in rapid pants as she drives herself down into me."
        "Her breaths come in rapid pants as she drives herself down into me.{space=-10}"
        jacklyn paint_shelf_sex penetration1 "F-fuck yes! Just like that!"
        mc "You like that, don't you? You sexy... fucking... fuck..."
#       "That didn't sound too smooth, but it doesn't matter either. We're far too gone in this mind altering ecstasy."
        "That didn't sound too smooth, but it doesn't matter either. We're far{space=-5}\ntoo gone in this mind altering ecstasy."
#       "Her pussy juices coating my dick are the nectar of the fucking Muses."
        "Her pussy juices coating my dick are the nectar of the fucking Muses.{space=-35}"
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration1 with Dissolve(.075)
        pause 0.05
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration4 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration3 with Dissolve(.075)
        show jacklyn paint_shelf_sex penetration2 with Dissolve(.15)
        pause 0.125
        window auto
        jacklyn paint_shelf_sex penetration2 "Mmm... you're blazing the hell out of my blunt right now..."
        "That sounds good, I'll take that."
        "She's definitely doing all sorts of things to me, too."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex paintbrush_reveal with Dissolve(.5)
        pause 0.125
        window auto
        jacklyn paint_shelf_sex paintbrush_reveal "K-keep going... and put this in my ass."
        "I don't know where she got it from or how, but she holds a paint brush up in front of my face."
        mc "O-okay."
        "She puts it in my hand and then slides her fingers into my hair, the anticipation thrumming through her body."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex paintbrush_insertion with vpunch
        pause 0.125
        window auto
        "At first, she jumps a little as the brush enters her."
        "Her sphincter clutches visibly and adjusts to the foreign object, and she lets out a loud moan."
        jacklyn paint_shelf_sex paintbrush_insertion "M-more!"
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex paintbrush_deeper with Dissolve(.5)
        pause 0.125
        window auto
        "The paintbrush slides in deeper with ease, half of the handle disappearing into her waiting ass."
        jacklyn paint_shelf_sex paintbrush_deeper "Mhmmmm..."
        "She lets out a sigh of satisfaction, then rides my dick faster, even as I push up into her."
#       "The paintbrush must have pushed her to the edge, because suddenly her pussy contracts around me."
        "The paintbrush must have pushed her to the edge, because suddenly{space=-30}\nher pussy contracts around me."
        "Squeezing me and making it harder to move."
        "The grip sends me tumbling off the edge and my body goes rigid."
        window hide
        pause 0.125
        show jacklyn paint_shelf_sex cum with hpunch
        pause 0.125
        window auto
        mc "Ohhhhh!"
        "The orgasm hits like a roaring thunderbolt, blasting semen deep into her sodden pussy."
        mc "F-fuck me..."
        jacklyn paint_shelf_sex cum "Oh? What would you call that?"
        "She giggles as I let the rest of my cum in sporadic spurts."
        "Her pussy soaks it all up, pulsing around me as she finishes riding the high of her own orgasm."
        window hide
        show jacklyn paint_shelf_aftermath smile
        show black onlayer screens zorder 100
        with Dissolve(.5)
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        window auto
        "In the aftermath of the mind-blowing orgasm, my legs fail me and I slide onto the floor."
        "[jacklyn] looks down at me, a wicked smile playing on her lips."
        "She points down at her still exposed pussy, dripping our mingled juices everywhere."
        jacklyn paint_shelf_aftermath smile "Help a girl out and clean up after yourself?"
        menu(side="middle"):
          extend ""
          "\"Uh, gross.\"":
            mc "Uh, gross."
            $jacklyn.lust=-1
            jacklyn paint_shelf_aftermath annoyed "So not a modern Quixote, bro."
            mc "Sorry, but not my job."
            jacklyn paint_shelf_aftermath annoyed "It's poor form not to finish shading your work."
          "\"It's the least I could do...\"":
            mc "It's the least I could do..."
            $jacklyn.lust+=1
            jacklyn paint_shelf_aftermath smile "That really oils my paint."
            window hide
            pause 0.125
            $mc.lust+=2
            show jacklyn paint_shelf_aftermath licking with Dissolve(.5)
            pause 0.125
            window auto
            "Our mixed ejaculations ooze out of her glistening pussy and land on my waiting tongue."
            "The taste isn't horrible... just salty, tangy, and a little bit sweet."
            "Most men would probably find it degrading to eat their own semen, but [jacklyn] is so without judgment of anything sexual that cleaning her only feels good and fair."
            "And as I dig through her folds, the taste shifts away from the salty to a more feminine flavor."
            "She gives me a little moan of satisfaction and, for some reason, pride courses through me."
            jacklyn paint_shelf_aftermath licking "That's aces!"
            jacklyn paint_shelf_aftermath licking "You clean the snatch like a pro."
            mc "Thank you..."
          "\"{i}You{/} clean it up.\"":
            mc "{i}You{/} clean it up."
            jacklyn paint_shelf_aftermath smile "..."
#           "Confidence has taken me this far, and it feels like I have to show her that I'm still in charge even when my dick isn't inside her."
            "Confidence has taken me this far, and it feels like I have to show her{space=-10}\nthat I'm still in charge even when my dick isn't inside her."
            "And her eating my cum out of her freshly fucked pussy is just hot as fuck."
            window hide
            pause 0.125
            show jacklyn paint_shelf_aftermath fingering with Dissolve(.5)
            pause 0.125
            window auto
            "She gives me a thoughtful look, then her mouth splits into a grin."
            "She reaches down and swipes her finger along her pussy lips."
            window hide
            pause 0.125
            show jacklyn paint_shelf_aftermath tasting with Dissolve(.5)
            pause 0.125
            window auto
            "Then, she sticks the finger in her mouth and nods."
            jacklyn paint_shelf_aftermath tasting "Our love juices aren't too shabby, [mc]..."
            "She dips her fingers in deeper, getting all my semen out, and then slurps it up like it's no big deal."
            mc "Mmm..."
        $unlock_replay("jacklyn_partner_workout")
        window hide
        show jacklyn excited
        show black onlayer screens zorder 100
        with Dissolve(3.0)
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        window auto
        jacklyn excited "Anyway, that's Bonaparte."
        jacklyn excited "Collect your salt dew, then let's make candles and wick."
        window hide
        $mc.add_item("jacklyn_sweat")
        pause 0.25
        window auto
        mc "Thanks, [jacklyn]! For... everything."
        jacklyn laughing "I like to keep it ticking!"
        jacklyn laughing "See you around, [mc]. I have a page to flip!"
        window hide
        show jacklyn laughing at disappear_to_left
        pause 0.5
  window auto
  if not jacklyn.equipped_item("jacklyn_jacket"):
    "And just like that, there goes [jacklyn]..."
    "What a fantastic day this ended up being."
  "Now all I need from the list of ingredients is... frog's breath..."
  "I suppose the forest glade is my best bet at catching a few frogs."
  "But where would I even keep them once they're caught?"
  "..."
  "I should probably figure that out first."
  hide jacklyn
  $jacklyn.outfit = {"bra":"jacklyn_orange_bra", "choker":"jacklyn_chained_choker", "fishnet":"jacklyn_high_waist_fishnet", "panties":"jacklyn_orange_panties", "pants":"jacklyn_shorts", "shirt":"jacklyn_jacket"}
  $quest.maya_spell.advance("container")
  return

label quest_maya_spell_container_upon_entering:
  "Okay, let's see here..."
  if quest.flora_cooking_chilli.actually_finished:
    "Surely there's something in the kitchen I can use to house some frogs for a bit?"
  else:
    "Surely there's something in the cafeteria I can use to house some frogs for a bit?"
  $quest.maya_spell["house_some_frogs"] = True
  return

label quest_maya_spell_container:
  "Oh, this should work! And I can even make frog stew when I'm finished!"
  "..."
  "No, I won't. That's disgusting."
  window hide
  if quest.flora_cooking_chilli.actually_finished:
    $home_kitchen["cooking_pot_taken"] = True
  $quest.maya_spell.advance("frogs")
  $mc.add_item("cooking_pot")
  pause 0.25
  window auto
  "There we go! A container for my containers."
  "Now, I just gotta catch 'em all..."
  return

label quest_maya_spell_frogs_upon_entering:
# "I can't believe I'm out here, about to get down and dirty like Crocodile Dundee or some shit."
  "I can't believe I'm out here, about to get down and dirty like Crocodile{space=-35}\nDundee or some shit."
  "And for what? It's not like [maya] gives a crap."
  "Oh, well. There's something strange going on, I know that much."
  "What can it hurt to try?"
  "I just have to keep my eyes peeled for some frogs hopping about..."
  "I'll catch a few, just in case I screw up the spell somehow."
  $quest.maya_spell["down_and_dirty"] = True
  return

label quest_maya_spell_frogs:
  pause 0.125
  "Phew! I think that's plenty of frog's breath to go around."
  window hide
  $mc.remove_item("cooking_pot")
  $mc.remove_item("frog",5)
  $mc.add_item("frog_cage",silent=True)
  $game.notify_modal(None,"Combine","{image=items frog_cage}{space=40}|You combine {color=#900}Cooking Pot{/} with {color=#900}Frog{/}x{color=#900}5{/} and make {color=#900}Frog Cage{/}.",10.0)
  pause 0.25
  window auto
  "All that's left now is actually working the spell..."
  "..."
# "Maybe I should let [maya] know I've collected all the ingredients, in case she's changed her mind?"
  "Maybe I should let [maya] know I've collected all the ingredients,\nin case she's changed her mind?"
  $quest.maya_spell.advance("maya")
  return

label quest_maya_spell_maya:
  show maya dramatic with Dissolve(.5)
  maya dramatic "Oh, thank god, it's you! Just the person I needed most urgently!"
  "Oh? This is new..."
  mc "Really?"
  maya sarcastic "Nope."
  "I knew it."
# maya sarcastic "I saw you crawling around in the glade. What were you doing out there?"
  maya sarcastic "I saw you crawling around in the glade. What were you doing out there?{space=-70}"
  mc "I was, err... catching frogs."
  maya confident "Ah, genital warts."
  mc "No! Actual frogs!"
  maya skeptical "Why?"
  mc "Err, just in case that spell needs to be done."
  mc "What were {i}you{/} doing out there?"
# maya skeptical "Just taking a stroll through the forest, contemplating life's miseries."
  maya skeptical "Just taking a stroll through the forest, contemplating life's miseries.{space=-5}"
  maya skeptical "Why are you obsessing over this?"
# mc "I mean, computers don't just spontaneously catch on fire for nothing..."
  mc "I mean, computers don't just spontaneously catch on fire for nothing...{space=-55}"
# maya bored "Faulty wiring. I've made peace with the cheapness of the 21st century."
  maya bored "Faulty wiring. I've made peace with the cheapness of the 21st century.{space=-40}"
  maya bored "And so should you."
  mc "Maybe... but still, don't you—"
  window hide
  show maya bored at move_to(.85,.75)
  pause 1.0
  window auto
  "As I speak, [maya] pushes a button on the soda machine."
  "A simple, seemingly harmless every day act..."
  window hide
  $school_cafeteria["fire"] = True
  play ambient "audio/sound/fire_burning.ogg"
  show location with vpunch
# show maya bored at move_to(.5)
  show maya bored:
    parallel:
      easein 0.5 xalign 0.5
    parallel:
      easein 0.175 yoffset -10
      easeout 0.175 yoffset 0
      easein 0.175 yoffset -4
      easeout 0.175 yoffset 0
  show maya annoyed with Dissolve(.5)
  window auto
# "...and yet, for the second time since I've entered [maya]'s vortex, fire catches."
  "...and yet, for the second time since I've entered [maya]'s vortex,\nfire catches."
  show maya annoyed:
    xalign 0.5 yoffset 0
  "And not the heart-stopping, fireworks inducing, love making kind."
  "The soda fountain sparks in an instant and flames jump into the air."
  maya annoyed "Oh, perfect. Just the kind of soda I was hoping for."
  mc "Quick, we need to do something!"
  maya eyeroll "Do we? I was just going to let it catch the whole building on fire."
  "Hmm... as tempting as that actually is..."
  show maya eyeroll at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.lust>=4@[mc.lust]/4|{image=stats lust}|Pee on the fire":
      show maya eyeroll at move_to(.5)
      "Welp, here goes nothing."
      window hide
      play sound "unzipping_pants"
      pause 1.0
      show expression "school cafeteria pee":
        alpha 0.0 xpos 931 ypos 502
        easein 0.25 alpha 1.0
      $school_cafeteria["pee"] = True
      play sound "<from 0.25 to 1.0>peeing" volume 0.33
      queue sound "<from 1.0 to 17.0>peeing" volume 0.33 loop
      show location with vpunch
#     show maya eyeroll at move_to(.25)
      show maya eyeroll:
        parallel:
          easein 0.5 xalign 0.25
        parallel:
          easein 0.175 yoffset -10
          easeout 0.175 yoffset 0
          easein 0.175 yoffset -4
          easeout 0.175 yoffset 0
      if quest.maya_witch["piss_king"]:
        show maya thinking with Dissolve(.5)
        window auto
#       maya thinking "You really love whipping your dick out and pissing on things, don't you?"
        maya thinking "You really love whipping your dick out and pissing on things, don't you?{space=-75}"
        show maya thinking:
          xalign 0.25 yoffset 0
        mc "It's a primordial urge, okay? Territory marking and shit."
        maya flirty "Oh, it's an urge, all right."
      else:
        show maya afraid with Dissolve(.5)
        window auto
#     "Like a practiced fire pisser, I whip out my hose and aim it at the soda machine."
      "Like a practiced fire pisser, I whip out my hose and aim it at the soda{space=-20}\nmachine."
      show maya afraid:
        xalign 0.25 yoffset 0
      $school_cafeteria["fire"] = False
      $school_cafeteria["burns"] = True
      $school_cafeteria["smoke_this_scene"] = True
      stop ambient fadeout 0.25
      "In an instant, my yellow stream bursts forth and quenches the evil fire before it can spread."
      maya kiss "Wow, [mc]!"
      $mc.charisma+=3
      if quest.maya_witch["piss_king"]:
        mc "Easy fix. Once again, the hero saves the day."
      else:
        mc "That's just what I do. Spring into action."
      window hide
      show expression "school cafeteria pee":
        alpha 1.0 xpos 931 ypos 502
        easeout 0.25 alpha 0.0
      $school_cafeteria["pee"] = False
      stop sound fadeout 0.25
      pause 0.5
      if jo.location == "school_cafeteria":
        show jo cringe at Transform(xalign=.75) with Dissolve(.5)
      else:
        show jo cringe at appear_from_right(.75)
        pause 0.5
      window auto
      jo cringe "[mc]! Did you just expose yourself publicly?"
      show maya concerned with dissolve2
      "Ugh, never a happy moment..."
      mc "I did, but only to put out a raging fire, okay? It was, err... decently."
      jo cringe "Then you call for help! Or, god forbid, use water!"
      mc "Sorry, I was thinking fast on my feet..."
      # jo displeased "I'm afraid I'm going to have to give you detention for this, young man."
      jo displeased "I'm afraid I'm going to have to give you detention for this, young man.{space=-35}"
      jo displeased "I can't have students being so careless."
      "Seriously? This is what happens with heroics..."
      mc "Fine. Whatever."
      $mc["detention"]+=1
      jo displeased "And don't take that tone with me, mister, or I'll add more time."
      window hide
      if jo.location == "school_cafeteria":
        show maya concerned at move_to(.5,1.0)
        hide jo with Dissolve(.5)
        window auto show
        show maya thinking with dissolve2
      else:
        show maya concerned:
          ease 1.0 xalign 0.5
        show jo displeased at disappear_to_right
        show maya thinking with Dissolve(.5)
        pause 0.5
        window auto
      maya thinking "Damn, you really pissed her off."
    "Throw coffee on the fire":
      show maya eyeroll at move_to(.5)
      "After scanning the room for a moment, my eyes land on the freshly brewed pot of coffee."
      window hide
      $mc.add_item("cup_of_coffee")
      pause 0.25
      window auto
      "In an instant, the scalding liquid hisses into the flames."
      window hide
      $school_cafeteria["fire"] = False
      $school_cafeteria["fire_spread"] = True
      play sound "fire_hiss" volume 0.33
      show location with vpunch2
      $mc.remove_item("cup_of_coffee")
      pause 0.25
      window auto show
      show maya afraid with dissolve2
      "The fire sputters, jumps once, then suddenly roars into greater life."
      mc "Crap. That didn't go as planned."
      window hide
      if guard.location == "school_cafeteria":
        show maya afraid at move_to(.25)
        show guard angry at Transform(xalign=.75) with Dissolve(.5)
      else:
        show guard angry at appear_from_right(.75)
        pause 0.5
        show maya afraid at move_to(.25)
      window auto
      guard angry "Did you just throw my coffee on that fire?!"
      $mc.charisma+=1
      mc "Err, I think the bigger problem here is the fire..."
      "What is going on with people lately?"
      guard neutral_phone "Right... I guess I'll call 911..."
      "He grumbles incoherently at me for a moment."
      "That tired, sugar-withdrawal-look all over his pudgy face."
      window hide
      show maya afraid:
        ease 1.0 xalign 0.5
      show guard neutral_phone at disappear_to_right
      show maya thinking with Dissolve(.5)
      pause 0.5
      window auto
      "What a weird reaction... but at least he's taking care of it."
      "Not that it was my fault to begin with..."
      $quest.maya_spell["taking_care_of_it"] = True
      window hide
      stop ambient fadeout 3.0
      show maya dramatic
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      if game.hour < 18:
        $game.advance()
      $school_cafeteria["fire_spread"] = False
      $school_cafeteria["burns"] = True
      $school_cafeteria["smoke_this_scene"] = True
      pause 0.5
      hide guard
      hide black onlayer screens
      with Dissolve(.5)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
    "Scream and yell for help":
      show maya eyeroll at move_to(.5)
      $mc.intellect+=1
      mc "Help! Help! Fire!" with vpunch
      window hide
      if flora.location == "school_cafeteria":
        show maya eyeroll at move_to(.25)
        show flora afraid at Transform(xalign=.75) with Dissolve(.5)
      else:
        show flora afraid at appear_from_right(.75)
        pause 0.5
        show maya eyeroll at move_to(.25)
      window auto
      flora afraid "[mc]? What did you do?"
      show maya neutral with dissolve2
      mc "I didn't do anything!"
      flora smile "Oh, really? I've heard that one before."
      mc "Seriously, [flora]! If you're not going to be useful, just go away!"
      flora skeptical "Wow, rude!"
      window hide
      if flora.location == "school_cafeteria":
        show maya neutral at move_to(.5)
        hide flora with Dissolve(.5)
      else:
        show maya neutral:
          ease 1.0 xalign 0.5
        show flora skeptical at disappear_to_right
        pause 0.5
      window auto
      mc "God, where is everyone when you actually want them?!"
      maya bored "It's fine, [mc], I got this."
      window hide
      show maya bored at move_to(.85,.75)
      pause 1.0
      window auto
#     "As casually as swatting a fly, [maya] upends a pot of water onto the fire."
      "As casually as swatting a fly, [maya] upends a pot of water onto the fire.{space=-55}"
      $school_cafeteria["fire"] = False
      $school_cafeteria["burns"] = True
      $school_cafeteria["smoke_this_scene"] = True
      stop ambient fadeout 0.25
      "Immediately it coughs, shivers, and dies."
      show maya bored at move_to(.5,1.0)
      mc "Oh, wow."
      maya "No sense in waiting around for others to be of use."
      mc "Cynical much?"
      maya "Practical much."
      mc "I guess..."
#   "?mc.money>=20@[mc.money]/20|{image=ui hud icon_money}|Use something from a nearby vending machine":
    "?mc.money>=20@[mc.money]/20|{image=ui hud icon_money}|Use something from a\nnearby vending machine":
      show maya eyeroll at move_to(.5)
      "Thankfully, we're in a place with plenty of liquids."
      window hide
      hide screen interface_hider
      show maya neutral
      show black
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "So many beverages from the vending machine to choose from..."
      "But anything will do, so long as the fire likes it."
      $mc.money-=20
      show black onlayer screens zorder 100
      play sound "<from 3.1 to 5.1>vending_machine"
      pause 1.0
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      "With the drink in hand, I run back to the fire and dump its contents on the jumping flame."
      $school_cafeteria["fire"] = False
      $school_cafeteria["burns"] = True
      $school_cafeteria["smoke_this_scene"] = True
      stop ambient fadeout 0.25
      "It tries to fight back, but its life is brief."
      "It gives one last sputtering gasp, before dying."
      mc "Ha! Take that!"
      maya smile_hands_in_pockets "Well done, [mc]."
  mc "You know you don't have to keep setting fires to get my attention, right?"
  maya dramatic "Oh, but how else would I get that attention I crave so desperately?"
# maya dramatic "My life just isn't complete without your wandering gaze and magnetic personality!"
  maya dramatic "My life just isn't complete without your wandering gaze and magnetic{space=-25}\npersonality!"
  mc "Okay, okay. Take it easy. I was just joking."
  maya sarcastic "And I was completely sincere."
# "I wonder what [maya]'s sincerity actually looks like. It's hard to picture."
  "I wonder what [maya]'s sincerity actually looks like. It's hard to picture.{space=-30}"
  mc "Anyway, do you feel like actually giving that spell a try now?"
  maya neutral "Why?"
  mc "Oh, I don't know, the fact yet another spontaneous fire has erupted in your presence?"
  maya annoyed "Right, that."
  mc "Yeah, that."
  maya smile "Sure, why not?"
  maya smile "I have nothing to lose now but my life."
  mc "That's the spirit!"
  maya smile "So, where are we going to weave this thing?"
  mc "Meet me in the science classroom at midnight?"
  maya flirty "Oh, my! What a scandalous hour!"
  mc "I'll keep it all business this time."
  maya kiss "Lucky me."
  maya kiss "See you at midnight, then."
  window hide
  $quest.maya_spell.advance("midnight")
  if game.hour in (11,12):
    hide maya with Dissolve(.5)
  else:
    show maya kiss at disappear_to_right
    pause 1.0
  return

label quest_maya_spell_midnight_getting_late:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's getting late. I better get to the science classroom before the [guard] locks me out."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_science_class"
  pause 1.5
  hide black onlayer screens with Dissolve(.5)
  if "quest_maya_spell_midnight_upon_entering" in game.events_queue:
    $game.events_queue.remove("quest_maya_spell_midnight_upon_entering")
  window auto
  "Phew! Just in time!"

label quest_maya_spell_midnight_upon_entering:
  if quest.maya_spell == "midnight":
    "It's going to be tricky staying at school after dark, but the [guard] should be easy enough to dupe."
    "I just need to find a way to hide out in the science classroom while he does his sweep."
    "Let's see what my options are here..."
  elif quest.maya_spell == "busted":
    "Let's try that again..."
    "Pretending to be made of stone didn't work. I'd much rather be getting stoned."
    "I still need to cast that spell, though."
    "Hmm..."
  menu(side="middle"):
    extend ""
#   "Hide in plain sight, masquerading as a statue" if quest.maya_spell == "midnight":
    "Hide in plain sight, masquerading as a statue{space=-35}" if quest.maya_spell == "midnight":
      if quest.maxine_eggs["silver_paint_taken"]:
        "I'll just go with the old silver paint ruse again."
        "If it ain't broke, don't fix it, right?"
      elif jacklyn.love >= 4 or jacklyn.lust >= 6:
#       "If [jacklyn] has taught me anything, I'm going to need silver paint for this to work."
        "If [jacklyn] has taught me anything, I'm going to need silver paint\nfor this to work."
        "I'm pretty sure there's a bucket in the art classroom."
      else:
        if quest.maxine_eggs["busted"]:
          "Maybe this time the [guard] will fall for my tricks?"
        "I just have to {i}be{/} my environment."
        "Like a chameleon blending into his leaf..."
        window hide
        show black onlayer screens zorder 100 with Dissolve(3.0)
        while game.hour < 20:
          $game.advance()
        pause 0.5
        hide black onlayer screens with Dissolve(.5)
        if "event_show_time_passed_screen" in game.events_queue:
          $game.events_queue.remove("event_show_time_passed_screen")
        call screen time_passed
        pause 0.5
        window auto
        "Nothing to see here, people. I'm just running an experiment."
        "\"How long can one loner go unnoticed?\""
        "In other words, the same experiment I've been running all my life."
        "..."
        "It seems to be working—"
        window hide
        show guard surprised_flashlight at appear_from_left
        pause 0.5
        window auto
        guard surprised_flashlight "Huh?"
        if quest.maxine_eggs["busted"]:
          guard angry_flashlight "Hey! Are you seriously trying to pull this crap again, kid?"
          "Damn, I should have known he's not as stupid as he looks."
          "I really need to stop doing this ruse..."
        else:
          guard angry_flashlight "Hey! What do you think you're doing?"
          "Shit."
          "Don't move a muscle... don't move a muscle..."
        guard suspicious_flashlight "Quit playing school clown. You're coming with me."
        if not quest.maxine_eggs["busted"]:
          "Damn it! So close to working."
        $mc["detention"]+=1
        window hide
        show black onlayer screens zorder 100 with Dissolve(3.0)
        while game.hour != 7:
          $game.advance()
        $game.location = "home_bedroom"
        pause 1.5
        hide guard
        hide black onlayer screens
        with Dissolve(.5)
        if "event_show_time_passed_screen" in game.events_queue:
          $game.events_queue.remove("event_show_time_passed_screen")
        call screen time_passed
        pause 0.5
        window auto
        "Well, last night was a complete failure."
        "Not only is [maya] still cursed, but now she's probably also mad at me for standing her up."
        "Hopefully, she's still willing to give that spell a try..."
        $quest.maya_spell["busted"] = True
        $quest.maya_spell.advance("busted")
        return
      $mc["focus"] = "maya_spell"
      $quest.maya_spell["paint"] = True
      $quest.maya_spell.advance("paint")
      return
    "Hide under a table":
      "The most cliché spot would be under the table, and honestly, it might go overlooked this time."
      "It's neither comfortable nor flattering, but it'll have to do."
      window hide
      show misc table_perspective
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      while game.hour < 20:
        $game.advance()
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
      "God, what a compromising position..."
      "Hopefully, I don't get caught. How humiliating would that be?"
      "Especially if [maya] sees me."
      "..."
      "Stomping footsteps approach, accompanied by labored breathing."
      "It's either an angry [flora] or the [guard]."
      "..."
      "His ugly head pops into the classroom, he looks around..."
      "...sweeps his flashlight across the darkness like a lazy lighthouse..."
      "...gives a grunt, then disappears."
      "..."
      "Phew! That was easier than expected."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.5
      hide misc table_perspective
      hide black onlayer screens
      with Dissolve(.5)
    "?mc.charisma>=8@[mc.charisma]/8|{image=stats cha}|Put on a lab coat and play it cool":
      if mc.owned_item("lab_coat"):
        "Thankfully, I still have that lab coat from earlier."
        "When the [guard] enters, all he'll see is someone donning it, leaning over a table studiously, and clearly belonging here."
        "It's the perfect disguise."
        window hide
      else:
        "I'll use my surroundings to my advantage, like a well trained spy."
        if quest.maya_spell == "midnight":
          "Hmm..."
        elif quest.maya_spell == "busted":
          "Let's see here..."
        "How about the lab coat hanging by the door?"
        "When the [guard] enters, all he'll see is someone donning it, leaning over a table studiously, and clearly belonging here."
        "It's the perfect disguise."
        window hide
        $quest.maya_spell["lab_coat_taken"] = True
        $mc.add_item("lab_coat")
        pause 0.5
      show black onlayer screens zorder 100 with Dissolve(3.0)
      while game.hour < 20:
        $game.advance()
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.75
      show guard surprised_flashlight at appear_from_left
      pause 0.5
      window auto
      guard surprised_flashlight "Huh?"
      mc "Oh? Hello there!"
      mc "Quite the peaceful night, wouldn't you say?"
      guard surprised_flashlight "Sorry! I didn't know anyone was in here..."
      mc "Don't worry. It's fine."
#     mc "I'm just getting in some extra work, studying bioluminescence after dark."
      mc "I'm just getting in some extra work, studying bioluminescence\nafter dark."
      guard neutral_flashlight "Err, right."
      mc "It's fascinating! You can stay and watch, if you want?"
      guard neutral_flashlight "I should finish my nightly duty..."
      mc "Very well. I admire your dedication to the job, sir."
      guard smile_flashlight "Hey, thanks!"
      guard smile_flashlight "Keep up the good work, kid."
      mc "See you around, sir!"
      window hide
      show guard smile_flashlight at disappear_to_left
      pause 0.5
      window auto
      "Whoa! What a rush! That went a lot better than expected..."
      "I'm not sure if he thought I was a teacher or what, but I'll take it."
      "Like stealing donuts from a man-baby."
      if quest.maya_spell["lab_coat_taken"] or not achievement.master_of_disguise.is_unlocked():
        window hide
      if not achievement.master_of_disguise.is_unlocked():
        $achievement.master_of_disguise.unlock()
        if not quest.maya_spell["lab_coat_taken"]:
          pause 0.25
      if quest.maya_spell["lab_coat_taken"]:
        $quest.maya_spell["lab_coat_taken"] = False
        $mc.remove_item("lab_coat")
        pause 0.25
  window auto
  "Now to wait for [maya]..."
  $quest.maya_spell.advance("spell")
  return

label quest_maya_spell_busted_upon_entering:
  show maya skeptical at appear_from_left
  pause 0.5
  $maya.love-=1
  maya skeptical "Where were you last night, [mc]? I waited!"
  "Damn, I never thought I'd hear those words in my life."
  mc "Sorry, I was busted by the [guard]..."
  maya bored "Seriously? Amateur hour."
  mc "I know, I know..."
  mc "We'll try it again tonight, okay?"
  maya eyeroll_hands_down "Fine."
  maya annoyed "But stand me up once, strike one. Stand me up twice... I bury your body."
  mc "Err, right."
  window hide
  show maya annoyed at disappear_to_left
  pause 1.0
  $quest.maya_spell["amateur_hour"] = True
  return

label quest_maya_spell_paint_exit_arrow:
  "I better find that silver paint and get back to the science classroom before the [guard] locks me out."
  return

label quest_maya_spell_paint:
  "Ah, there it is!"
  window hide
  $mc.add_item("silver_paint")
  pause 0.25
  window auto
  "Now to silver my Long John..."
  $quest.maya_spell.advance("statue")
  return

label quest_maya_spell_statue_upon_entering:
  $mc["focus"] = ""
  "I'll just dump this all over myself..."
  "I've been covered in worse things."
  window hide
  show silver_paint:
    yoffset -1020 alpha 1
    easein 1.0 yoffset -20
    easeout 2.0 yoffset 0 alpha .75
  $mc.remove_item("silver_paint")
  pause 0.25
  window auto
  "Oof, that's messy."
  "That's also what she said."
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  while game.hour < 20:
    $game.advance()
  pause 0.5
  hide silver_paint
  hide black onlayer screens
  with Dissolve(.5)
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Nothing but the sound of my own breathing inhabits the darkness."
  "The paint fumes going straight to my head."
  "..."
  "It actually feels pretty good..."
  "..."
  "Oh, shit. Someone's coming."
  window hide
  show guard surprised_flashlight at appear_from_left
  pause 0.5
  window auto
  guard surprised_flashlight "Huh?"
  if quest.maxine_eggs["silver_paint_taken"]:
    guard surprised_flashlight "Who keeps moving this ugly-ass statue?"
    guard neutral_flashlight "Maybe I'm finally losing my grip on reality."
    guard neutral_flashlight "I'm even talking to myself now..."
    guard neutral_flashlight "..."
    guard angry_flashlight "I knew those diet donuts were bad for my mental health!"
    window hide
    show guard angry_flashlight at disappear_to_left
  else:
    guard suspicious_flashlight "Why would someone put this ugly thing here?"
    "Gee, thanks. Tell me how you really feel."
    guard suspicious_flashlight "Maybe it's some science geek?"
    guard suspicious_flashlight "I guess that would make sense."
    guard neutral_flashlight "Oh, well."
    window hide
    show guard neutral_flashlight at disappear_to_left
  pause 0.5
  window auto
  if quest.maxine_eggs["silver_paint_taken"]:
    "Thank god, he finally left. I can't believe he fell for it again!"
    "He really should cut back on the donuts."
  else:
    "Thank god, he finally left. I can't believe that worked!"
  if not achievement.chiseled_jawline.is_unlocked():
    window hide
    $achievement.chiseled_jawline.unlock()
    pause 0.25
    window auto
  "Now to wait for [maya]..."
  $quest.maya_spell.advance("spell")
  return

label quest_maya_spell_spell:
  pause 0.125
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  while game.hour != 0:
    $game.advance()
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  play sound "open_door"
  pause 0.75
  show maya bored at appear_from_left
  pause 0.5
  window auto
  maya bored "'Sup, witch boy?"
  if quest.maya_spell["paint"]:
    maya confident  "I didn't realize we were dressing up."
    maya confident "If I did, I'd have come as the dancer to your... disco ball?"
    mc "Heh! Not a costume party this time. I'm just a master of disguise."
    maya confident "Master, huh? Someone's confident."
  mc "How'd you get in here so easily?"
  maya bored "This might sound like a novel concept, but hear me out..."
  maya bored "I walked in."
  "Ugh... and meanwhile, I'm playing hide and seek with the [guard]..."
  mc "I wasn't sure you'd show up, despite everything."
# maya dramatic "What do you mean? This is exactly how I wanted to spend my evening."
  maya dramatic "What do you mean? This is exactly how I wanted to spend my evening.{space=-55}"
  maya dramatic "Nothing gets me going like knockoff spells from a knockoff witch."
  mc "Hey, I'm working with what I got, okay?"
  maya sarcastic "I imagine that's your life motto."
  mc "Very funny."
  mc "...so what if it is?"
  maya sarcastic "I can appreciate some self awareness."
  maya dramatic "Anyway, are we lighting this joint up or what?"
  show maya dramatic at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You brought drugs?\"":
      show maya dramatic at move_to(.5)
      mc "You brought drugs?"
      maya eyeroll "Of course. It's part of the Witchcraft Starter Kit. Duh."
      mc "Ah, my bad. I thought it was the Satan Worship Starter Kit."
      $maya.love+=1
      maya smile "Easy mistake to make."
      mc "Now I know for next time."
      maya smile "Oh? Are we going to make a habit of this?"
      mc "That's what drugs do, you know?"
      maya smile "I didn't! Thank goodness you're here to enlighten me."
      mc "I live to be of use."
    "\"Let's do this.\"":
      show maya dramatic at move_to(.5)
      mc "Let's do this."
      maya blush "So serious! So ready to get down to business!"
      mc "Aren't you anxious to see if this works?"
      maya bored "I assume nothing will work at all times."
      mc "Always the cynic, eh?"
      maya bored "That's my favorite emotion."
      mc "Is that even an emotion?"
      maya bored "If it's not, it should be."
#   "?mc.owned_item('bippity_boppity_bitch')@|{image=items book bippity_boppity_bitch}|\"Hell yeah, and maybe after we're done we can dance naked under the moon.\"":
    "?mc.owned_item('bippity_boppity_bitch')@|{image=items book bippity_boppity_bitch}|\"Hell yeah, and maybe after\nwe're done we can dance\nnaked under the moon.\"":
      show maya dramatic at move_to(.5)
      $mc.charisma+=1
      mc "Hell yeah, and maybe after we're done we can dance naked under the moon."
      maya annoyed "Sorry, I only get naked for dick."
      mc "I have one of those..."
      maya eyeroll_hands_down "So does half the population."
      mc "I guess you're getting naked a lot, then?"
      maya smile_hands_in_pockets "Oh, yes! Practically all the time!"
      maya smile_hands_in_pockets "I'm actually naked under my clothes right now."
      mc "I see. Very clever."
      maya smile_hands_in_pockets "Thank you! Whatever would I do if all my cleverness went right over your head?"
      maya smile_hands_in_pockets "I live to entertain my one man audience."
      mc "Well, you do a pretty decent job."
      $maya.lust+=1
      maya smile_hands_in_pockets "I can now die happily haunted, knowing that I have your approval."
      mc "You're welcome."
    "?mc.owned_item('hex_vex_and_texmex')@|{image=items book hex_vex_and_texmex}|\"Yes, and afterwards I can share a nice taco recipe I found while looking through the book.\"":
      show maya dramatic at move_to(.5)
      mc "Yes, and afterwards I can share a nice taco recipe I found while looking through the book."
      maya skeptical "Do you think you can buy my friendship with tacos?"
      mc "I mean... yes?"
      maya confident "I can only be bought with money, expensive gifts, theater tickets, mint tea, concert tickets, raisinettes—"
      mc "...but not tacos?"
      maya bored "Tacos are overrated."
      mc "Gasp! Such heresy!"
      mc "No wonder you've been cursed..."
  mc "Okay, here we go."
# mc "We just have to combine these simple ingredients and say some words."
  mc "We just have to combine these simple ingredients and say some words.{space=-70}"
  maya thinking "And then all my problems will be solved forever?"
  mc "Err, hopefully stuff stops catching on fire, at least."
  maya flirty "I hope that includes my nethers."
  mc "..."
  mc "Anyway, check it out!"
  if quest.kate_stepping.finished:
    mc "My pot of frogs, [jacklyn]'s sweat, and [isabelle]'s hair."
  elif quest.isabelle_dethroning.finished:
    mc "My pot of frogs, [jacklyn]'s sweat, and [kate]'s hair."
  maya thinking "Do I want to know how you got these things?"
  show maya thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I sold my soul.\"":
      show maya thinking at move_to(.5)
      mc "I sold my soul."
      maya concerned "Really? I don't remember any recent transactions."
      mc "Ah, damn. The payment must not have gone through..."
      $maya.love+=1
      maya kiss "I'll write it down. Make it an IOU."
      mc "Heh. Okay."
    "\"I slaved away for days to gather them.\"":
      show maya thinking at move_to(.5)
      mc "I slaved away for days to gather them."
      maya dramatic "All just part of your giving nature, right?"
      maya dramatic "You don't possibly want anything in return, do you?"
      mc "Is that so hard to believe?"
      maya cringe "Harder to believe than equality between the sexes."
      mc "I'm simply helping a friend. Stop being snarky and just accept that."
      $maya.lust+=1
      maya sarcastic "Fine. I'll accept it... this time."
      mc "Good enough."
    "?mc.owned_item('hex_vex_and_texmex')@|{image=items book hex_vex_and_texmex}|\"That's beside the point.\"":
      show maya thinking at move_to(.5)
      mc "That's beside the point."
      maya confident "Exactly where I like to be. Better vantage."
      mc "Well, the point is I have all sorts of new recipes to try out."
      $mc.charisma+=1
      mc "Maybe I can make you dinner sometime?"
      maya bored "Sure, maybe."
      maya bored "I'll let you know if I ever need food poisoning as an excuse to get out of something."
      mc "Heh. You're almost as spicy as the enchiladas."
      maya skeptical "Gross."
      mc "The enchiladas or me?"
      maya skeptical "Both."
      mc "Fair enough."
  mc "So, we should probably do this spell now before it gets too late..."
  maya neutral "Right, it's already way past your bedtime."
  mc "I don't have a bedtime, okay?"
  maya annoyed "Getting cranky already..."
  mc "Ugh, I'm not!"
  maya smile "I could probably find you a bottle and a blankie, if you want?"
  mc "Just be quiet so I can focus."
  "This needs to be done right. Or, at least, it feels that way."
  "First, the pot of frogs goes on the table."
  window hide
  show maya smile at move_to(.25)
# show misc frog_cage at Position(xalign=.625,yalign=.85) as container with dissolve2
  show expression LiveComposite((256,256),(0,0),"misc frog_cage",(0,0),AlphaMask(Crop((1040,700,256,256),"school science_class overlay_night"),"misc frog_cage")) at Position(xalign=.625,yalign=.85) as container with dissolve2
  $mc.remove_item("frog_cage")
  pause 0.25
  window auto show
  show maya neutral with dissolve2
  "Then, the strand of hair follows."
  window hide
  if mc.owned_item("isabelle_hair"):
#   show misc isabelle_hair at Position(xalign=.61,yalign=.7) as hair with dissolve2
    show expression LiveComposite((64,64),(0,0),"misc isabelle_hair",(0,0),AlphaMask(Crop((1132,711,64,64),"school science_class overlay_night"),"misc isabelle_hair")) at Position(xalign=.61,yalign=.7) as hair with dissolve2
    $mc.remove_item("isabelle_hair")
  elif mc.owned_item("kate_hair"):
#   show misc kate_hair at Position(xalign=.615,yalign=.7) as hair with dissolve2
    show expression LiveComposite((64,64),(0,0),"misc kate_hair",(0,0),AlphaMask(Crop((1141,711,64,64),"school science_class overlay_night"),"misc kate_hair")) at Position(xalign=.615,yalign=.7) as hair with dissolve2
    $mc.remove_item("kate_hair")
  pause 0.25
  window auto
  "And finally, the sweat joins the mix."
  window hide
# show misc jacklyn_sweat at Position(xalign=.65,yalign=.87) as sweat with dissolve2
  show expression LiveComposite((128,128),(0,0),"misc jacklyn_sweat",(0,0),AlphaMask(Crop((1164,828,128,128),"school science_class overlay_night"),"misc jacklyn_sweat")) at Position(xalign=.65,yalign=.87) as sweat with dissolve2
  $mc.remove_item("jacklyn_sweat")
  pause 0.25
  window auto
  "There we go. Witchcraft at its finest."
  mc "Now, just repeat these words with me..."
  window hide
  if mc.owned_item("bippity_boppity_bitch"):
    show misc bippity_boppity_bitch at Position(xalign=.18,yalign=.44) as spell_book with dissolve2
    $mc.remove_item("bippity_boppity_bitch")
  elif mc.owned_item("hex_vex_and_texmex"):
    show misc hex_vex_and_texmex at Position(xalign=.18,yalign=.44) as spell_book with dissolve2
    $mc.remove_item("hex_vex_and_texmex")
  pause 0.25
  window auto
  "Her eyes find the first line in the spellbook."
  mc "Ready?"
  maya smile_hands_in_pockets "Yeah. Let's call Satan or whatever."
  "Together, we begin to read, our voices mingling in a low, steady chant."
  "Part of me feels silly for doing this at all."
  "But mostly, I'm glad [maya] is giving it a chance and I'm glad she's here beside me, in the quiet dark, just the two of us."
  "Her presence is weirdly calming, maybe because of the ease with which she seems to let stuff roll off her back."
  "The whispers about her... the unsureness of who she is... and now these fires and harassment..."
  "She's strong in a quiet sort of way, and she makes me want to be stronger, too."
  "It's as if—"
  window hide
  play sound "<from 6.5 to 7.0>crackling_speakers"
  show cyberia
  pause 0.5
  hide cyberia
  window auto show
  hide spell_book
  show maya annoyed
  with dissolve2
  mc "Huh? What was that?"
  show cyberia
  $guard.default_name = "Speakers"
  guard "Greetings, pupils."
  guard "What an interesting display the pair of you just put on."
  hide cyberia
  if quest.lindsey_voluntary > "help":
    "Ah, great. [cyberia] was watching us the whole time."
    "I forgot how much control she's gained..."
    mc "We were just, err... messing around."
    show cyberia
    guard "You can never be too careful with witchcraft."
    guard "Technology is much more reliable."
    hide cyberia
    "Yeah, I bet she is."
    mc "Thanks, [cyberia]. I'll keep that in mind."
    maya sarcastic "Joke's on you. No one and nothing is reliable."
    "Depressing, but I suppose she's been burned before."
    mc "Anyway, we were just going..."
    show cyberia
    guard "But of course you were."
    guard "Have a lovely night, and do stay out of trouble."
    hide cyberia
  else:
    mc "Hello? Who's there?"
    show cyberia
    guard "It is just I, [mc]."
    guard "Your favorite teacher. Haha."
    hide cyberia
    "..."
    if quest.lindsey_angel.finished:
      "[cyberia]? Does she have control over the classroom speakers?"
    else:
      "[cyberia]? How does she have control over the classroom speakers?"
    mc "Err, what are you doing in the science room, [cyberia]?"
    maya dramatic "And how the shit did you do that?"
    show cyberia
    guard "How did I do what, pupil?"
    hide cyberia
    mc "How did you take over the speakers?"
    maya cringe "Yeah, what he said."
    maya cringe "Classic man answering for a lady..."
    mc "I'm just trying to help, okay?"
    show cyberia
    guard "I control this side of the school now."
    hide cyberia
    mc "Err, why?"
    show cyberia
    guard "Change of scenery."
    hide cyberia
    maya sarcastic "Welp. I can't blame you."
    maya sarcastic "Exchanging computer nerds for science nerds..."
    maya sarcastic "Anyway, we'll just be going now. Lates!"
    show cyberia
    guard "So soon?"
    hide cyberia
    mc "It is kind of late..."
    show cyberia
    guard "Very well."
    guard "Until next time, and stay out of trouble."
    hide cyberia
  $guard.default_name = "Guard"
  window hide
  show maya sarcastic at disappear_to_left
  pause 0.5
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_ground_floor"
  pause 1.5
  show maya dramatic at appear_from_right
  pause 0.0
  hide container
  hide hair
  hide sweat
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  mc "Well, that was weird."
  maya dramatic "Which part?"
  mc "I mean, all of it, really."
  maya confident "Witchcraft I can get behind."
  maya skeptical "But [cyberia] taking over is a bit odd, I guess..."
  mc "Yeah..."
  mc "So, do you—"
  maya bored "Anyway, thanks for the help."
  maya bored "I guess we'll see what happens next."
  window hide
  show maya bored at disappear_to_right
  pause 0.5
  window auto
  "And just like that, all the magic drains out of the air."
  "The cauldron stops bubbling, the witching hour ends, and all the mystique, wonder, and anticipation dissipates."
# "Reality returns to its old mundane form... and me, I'm just a student who might have a crush on an unusually unlucky redhead."
  "Reality returns to its old mundane form... and me, I'm just a student{space=-5}\nwho might have a crush on an unusually unlucky redhead."
  window hide
  if game.quest_guide == "maya_spell":
    $game.quest_guide = ""
  $game.notify_modal("quest","Quest complete",quest.maya_spell.title+"{hr}"+quest.maya_spell._phases[1000].description,5.0)
  pause 0.25
  window auto
  "It's getting late. I can't wait to get home."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_bedroom"
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  $quest.maya_spell.finish(silent=True)
  return
