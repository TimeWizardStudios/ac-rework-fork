init python:

  class Character_flora(BaseChar):

    notify_level_changed=True

    default_name="Flora"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="flora contact_icon"

    default_outfit_slots=["hat","shirt","bra","pants","panties","blindfold","cum","vines"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("flora_hat")
      self.add_item("flora_sun_hat")
      self.add_item("flora_referee_cap")
      self.add_item("flora_referee_shirt")
      self.add_item("flora_armor")
      self.add_item("flora_trench_coat")
      self.add_item("flora_shirt")
      self.add_item("flora_business_shirt")
      self.add_item("flora_blouse")
      self.add_item("flora_pants")
      self.add_item("flora_skirt")
      self.add_item("flora_dress")
      self.add_item("flora_bra")
      self.add_item("flora_purple_bra")
      self.add_item("flora_panties")
      self.add_item("flora_striped_panties")
      self.add_item("flora_blindfold")
      self.add_item("flora_vines")
      self.add_item("flora_cum")
      self.equip("flora_panties")
      self.equip("flora_bra")
      self.equip("flora_shirt")
      self.equip("flora_pants")

    def ai(self, hidden = False):

      if game.season == 1 and mc.at("school_*") and not flora.talking:
        self.unequip("flora_hat")
        if quest.flora_handcuffs > "package" and not quest.jo_washed.in_progress:
          self.equip("flora_skirt")
        else:
          self.equip("flora_pants")

      if hidden:
        self.location=None
        self.activity=None
        self.alternative_locations={}
        return

      self.location=None
      self.activity=None
      self.alternative_locations={}

      ### Forced Locations
      if quest.jo_washed.in_progress:
        if quest.jo_washed["flora_location"] == "school_entrance":
          self.location = "school_entrance"
          self.activity = "car_wash"
        else:
          self.location = None
          self.activity = None
        return

      if quest.back_to_school_special == "talk_to_flora":
        self.location="home_kitchen"
        self.activity="eating"
        return

      if quest.back_to_school_special == "talk_to_jo" or quest.back_to_school_special == "ready_to_leave":
        self.location = None
        self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if quest.kate_fate in ("peek","hunt","run","hunt2","deadend","rescue"):
        self.location = None
        self.alternative_locations={}
        return

      if quest.flora_cooking_chilli == "enter_bathroom":
        self.location = None
        self.activity = None
        return

      if flora["at_none_this_scene"] or flora["at_none_now"] or flora["at_none_today"] or flora["hidden_now"] or flora["hidden_today"]:
        self.location = None
        self.activity = None
        return

      #if quest.jacklyn_statement.in_progress:
      #  pass
        #if quest.jacklyn_statement == "ingredients" and home_kitchen["croutons_activated"]:
        #  self.location = None
        #  self.activity = None
        #  return

      if quest.flora_bonsai.in_progress:
        if quest.flora_bonsai in ("bake","chef","check","sugar","flora","refreshments","fetch"):
          self.location = "home_kitchen"
          if quest.flora_bonsai["florabaking"]:
            self.activity = "cooking_hat"
          else:
            self.activity = 'eating'
        elif quest.flora_bonsai in ("florahelp","vines"):
          self.location = 'school_ground_floor_west'
          self.activity = 'vines'
        elif quest.flora_bonsai in ("attack","tinybigtree"):
          self.location = 'school_nurse_room'
          self.activity = 'vines'
        elif quest.flora_bonsai in ("nightvisit","sleep","water","pads"):
          self.location = None
          self.activity = None
        return

      if quest.flora_cooking_chilli == "return_phone":
        self.location = "home_kitchen"
        self.activity = "pain"
        return

      if quest.isabelle_haggis>="trouble" and quest.isabelle_haggis.in_progress:
        self.location="school_homeroom"
        self.activity="sitting"
        return

      if quest.flora_cooking_chilli >= "follow_bathroom" and quest.flora_cooking_chilli <= "got_liquid":
        self.location = "home_bathroom"
        self.activity = "shower"
        return

      if quest.flora_jacklyn_introduction == "bathroom":
        self.location = "home_bathroom"
        return

      if quest.lindsey_wrong == "florahelp" and game.hour in (9,10):
        self.location="school_homeroom"
        self.activity="sitting"
        return

      if quest.lindsey_wrong == "floralocker":
        self.location="school_first_hall_east"
        self.activity="standing"
        return

      if quest.mrsl_table in ("flora","sleep") and game.hour in (7,8,17,18,19):
        self.location = "home_hall"
        self.activity = "texting"
        return

      if game.season == 1 and quest.isabelle_tour.finished and quest.berb_fight.finished and quest.flora_bonsai.finished and quest.jacklyn_statement.finished and quest.maxine_eggs.finished and game.hour == 20 and not quest.flora_squid.started:
        self.location = "home_hall"
        self.activity = "flora_room" ## This is just a workaround to keep the hallway lights on when the player knocks on the door to Flora's room
        return

      if quest.flora_squid == "trail":
        self.location = "school_ground_floor_west"
        self.activity = "standing"
        return
      elif quest.flora_squid == "search":
        self.location = None
        self.activity = None
        return
      elif quest.flora_squid == "ready":
        self.location = "school_entrance"
        self.activity = "standing"
        return

      if ((quest.jo_day == "flora" and game.hour in (7,8,17,18))
      or (quest.jo_day == "picnic" and game.hour == 7)):
        self.location = "home_hall"
        self.activity = "texting"
        return

      if quest.fall_in_newfall.in_progress and quest.fall_in_newfall < "stick_around":
        self.location = "school_homeroom"
        self.activity = "sitting"
        return

      if quest.maya_spell == "sweat" and game.hour in (9,10) and quest.jacklyn_statement["ending"] in ("anal","pegging"):
        self.location = "school_first_hall_east"
        self.activity = "standing"
        return

      if quest.flora_walk == "xcube":
        self.location = "home_bedroom"
        self.activity = "playing"
        return
      elif quest.flora_walk == "park":
        self.location = "school_park"
        self.activity = "standing"
        return
      elif quest.flora_walk == "forest_glade":
        self.location = "school_forest_glade"
        self.activity = "standing"
        return

      if quest.maya_quixote == "xCube" and game.hour in (7,8,17,18,19):
        self.location = "home_bedroom"
        self.activity = "playing"
        return
      elif quest.maya_quixote == "power_outage":
        self.location = "home_hall"
        self.activity = "texting"
        return
      elif quest.maya_quixote in ("attic","board_game"):
        self.location = "home_bedroom"
        self.activity = "sitting"
        return

      if (quest.lindsey_voluntary == "wait" and game.hour in (7,8,17,18,19)
      or flora["at_kitchen_this_scene"]):
        self.location = "home_kitchen"
        self.activity = "eating"
        return
      elif quest.lindsey_voluntary == "dream":
        self.location = None
        self.activity = None
        return

      if quest.jacklyn_town in ("marina","gallery","pancake_brothel") and (flora.love >= 20 or flora.lust >= 15):
        self.location = "pancake_brothel"
        self.activity = "spying"
        return
      elif ((quest.jacklyn_town == "wait" and game.hour == 19)
      or quest.jacklyn_town in ("suit","hairdo","ready","marina")):
        self.location = None
        self.activity = None
        return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return
      ### Forced Locations

      ### ALT Locations
      if quest.flora_cooking_chilli >= "bring_pot" and  quest.flora_cooking_chilli < "return_phone":
        self.alternative_locations["home_kitchen"]="cooking_hat"

      if quest.mrsl_table in ("flora","sleep") and 17 > game.hour > 8:
        self.alternative_locations["home_hall"] = "texting"

      if quest.kate_trick == "park":
        self.alternative_locations["school_park"] = "standing"

      if (quest.isabelle_dethroning == "dinner" and game.hour == 19) or quest.isabelle_dethroning in ("trap","revenge_done") or quest.isabelle_dethroning["kitchen_shenanigans_replay"]:
        self.alternative_locations["school_cafeteria"] = "eating"

      if quest.maya_quixote == "xCube" and game.hour not in (7,8,17,18,19):
        self.alternative_locations["home_bedroom"] = "playing"

      if quest.lindsey_voluntary == "wait" and 17 > game.hour > 8:
        self.alternative_locations["home_kitchen"] = "eating"

      if quest.maya_sauce == "classes":
        self.alternative_locations["school_homeroom"] = "sitting"
      elif quest.maya_sauce == "dinner":
        self.alternative_locations["home_kitchen"] = "cooking_hat"
      elif quest.maya_sauce == "sauces":
        self.alternative_locations["home_kitchen"] = "referee"
      ### ALT Locations

      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):

        if game.hour in (7,8) and not "home_kitchen" in self.alternative_locations:
          self.location = "home_kitchen"
          self.activity = "eating"
          if game.season == 1 and not flora.talking:
            self.equip("flora_panties")
            self.equip("flora_bra")
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
            self.equip("flora_shirt")
            if mc.at("home_*"):
              if quest.flora_handcuffs > "package":
                self.unequip("flora_skirt")
              else:
                self.unequip("flora_pants")
          return

        elif game.hour in (9,10):
          self.location = "school_art_class"
          self.activity = "selfie"
          if game.season == 1 and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
          return

        elif game.hour in (11,12):
          self.location = "school_cafeteria"
          self.activity = "eating"
          if game.season == 1 and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
          return

        elif game.hour in (13,14):
          self.location = "school_homeroom"
          self.activity = "sitting"
          if game.season == 1 and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
          return

        elif game.hour == 15:
          self.location = "school_first_hall_east"
          self.activity = "standing"
          if game.season == 1 and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
          return

        elif game.hour == 16:
          self.location = "school_ground_floor_west"
          self.activity = "standing"
          if game.season == 1 and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
          return

        elif game.hour == 17 and not ((quest.flora_cooking_chilli >= "chilli_recipe_step_one" and quest.flora_cooking_chilli <= "return_phone") or quest.mrsl_table == "laundry"):
          self.location = "home_bathroom"
          self.activity = "shower"
          return

        elif game.hour == 18 and not "home_kitchen" in self.alternative_locations:
          self.location = "home_kitchen"
          self.activity = "eating"
          if game.season == 1 and mc.at("home_*") and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.unequip("flora_skirt")
            else:
              self.unequip("flora_pants")
          return

        elif game.hour == 19 and not "home_kitchen" in self.alternative_locations:
          self.location = "home_hall"
          self.activity = "texting"
          if game.season == 1 and mc.at("home_*") and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.unequip("flora_skirt")
            else:
              self.unequip("flora_pants")
          return

        if quest.flora_jacklyn_introduction == "outside":
          self.location = "school_entrance"
          if game.season == 1 and not flora.talking:
            if quest.flora_handcuffs > "package":
              self.equip("flora_skirt")
            else:
              self.equip("flora_pants")
      ### Schedule

    def call_label(self):
      if mc["focus"]:
        if mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "chilli_done":
            return "flora_quest_flora_cooking_chilli_call_flora" if game.location == "home_bathroom" else None
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume" and game.location == "school_entrance" and not quest.kate_wicked["maxine_number"]:
            return "quest_kate_wicked_costume_exterior_flora"
      else:
        if quest.isabelle_buried == "callforhelp":
          return "quest_isabelle_buried_callforhelp_flora"
        if quest.flora_handcuffs == "phone_call":
          return "quest_flora_handcuffs_phone_call" if game.location == "school_ground_floor" else "quest_flora_handcuffs_phone_call_wrong_location"
      return None

    def message_label(self):
      if quest.mrsl_HOT == "text_flora":
        return "mrsl_quest_HOT_flora_text_same_room" if game.location == flora.location else "mrsl_quest_HOT_flora_text"
      if quest.jacklyn_sweets == "text":
        return "quest_jacklyn_sweets_text"
      return None

#    def contact_notes(self):
#      rv=[]
#      rv.append(["Bio","Character description and history"])
#      rv.append(["Likes",["Apples","Oranges"]])
#      rv.append(["Dislikes",["Pork"]])
#      return rv

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("flora avatar "+state,True):
          return "flora avatar "+state
      rv=[]

      if state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("flora avatar body1",118,153))
        rv.append(("flora avatar face_angry",230,259))
        if "_striped_panties" in state:
          rv.append(("flora avatar b1striped_panties",125,824))
        elif "_panties" in state:
          rv.append(("flora avatar b1panty",128,825))
        if "_purple_bra" in state:
          rv.append(("flora avatar b1purple_bra",159,437))
        elif "_bra" in state:
          rv.append(("flora avatar b1bra",160,434))
        if "_pants" in state:
          rv.append(("flora avatar b1pants",117,803))
        elif "_skirt" in state:
          rv.append(("flora avatar b1skirt",105,716))
        if "_leash" in state:
          rv.append(("flora avatar b1arm1_n",21,432))
          rv.append(("flora avatar b1leash",64,834))
        else:
          rv.append(("flora avatar b1arm2_n",21,433))
        if "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b1shirt_knotted",160,417))
          else:
            rv.append(("flora avatar b1shirt",139,417))
          if "_leash" in state:
            rv.append(("flora avatar b1arm1_c",16,432))
            rv.append(("flora avatar b1leash",64,834))
          else:
            rv.append(("flora avatar b1arm2_c",16,432))
        elif "_blouse" in state:
          rv.append(("flora avatar b1blouse",139,400))
          if "_dress" in state:
            rv.append(("flora avatar b1dress",116,427))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b1referee_shirt",121,401))
            rv.append(("flora avatar b1arm2_referee_shirt",13,456))
          else:
            rv.append(("flora avatar b1arm2_blouse",13,460))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b1trench_coat",78,426))
          # rv.append(("flora avatar b1arm1_trench_coat",7,457))
          rv.append(("flora avatar b1arm2_trench_coat",7,457))
        if "_sun_hat" in state:
          rv.append(("flora avatar b1sun_hat",57,110))
        elif "_hat" in state:
          rv.append(("flora avatar b1chefhat",141,75))
        elif "_cop" in state:
          rv.append(("flora avatar b1cop_hat",167,106))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b1referee_cap",190,139))
        if "_blindfold" in state:
          rv.append(("flora avatar b1bandage",244,265))

      elif state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("flora avatar body1",118,153))
        rv.append(("flora avatar face_concerned",237,258))
        if "_striped_panties" in state:
          rv.append(("flora avatar b1striped_panties",125,824))
        elif "_panties" in state:
          rv.append(("flora avatar b1panty",128,825))
        if "_purple_bra" in state:
          rv.append(("flora avatar b1purple_bra",159,437))
        elif "_bra" in state:
          rv.append(("flora avatar b1bra",160,434))
        if "_pants" in state:
          rv.append(("flora avatar b1pants",117,803))
        elif "_skirt" in state:
          rv.append(("flora avatar b1skirt",105,716))
        rv.append(("flora avatar b1arm2_n",21,433))
        if "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b1shirt_knotted",160,417))
          else:
            rv.append(("flora avatar b1shirt",139,417))
          rv.append(("flora avatar b1arm2_c",16,432))
        elif "_blouse" in state:
          rv.append(("flora avatar b1blouse",139,400))
          if "_dress" in state:
            rv.append(("flora avatar b1dress",116,427))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b1referee_shirt",121,401))
            rv.append(("flora avatar b1arm2_referee_shirt",13,456))
          else:
            rv.append(("flora avatar b1arm2_blouse",13,460))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b1trench_coat",78,426))
          # rv.append(("flora avatar b1arm1_trench_coat",7,457))
          rv.append(("flora avatar b1arm2_trench_coat",7,457))
        if "_sun_hat" in state:
          rv.append(("flora avatar b1sun_hat",57,110))
        elif "_hat" in state:
          rv.append(("flora avatar b1chefhat",141,75))
        elif "_cop" in state:
          rv.append(("flora avatar b1cop_hat",167,106))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b1referee_cap",190,139))
        if "_blindfold" in state:
          rv.append(("flora avatar b1bandage",244,265))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("flora avatar body1",118,153))
        rv.append(("flora avatar face_confident",240,249))
        if "_striped_panties" in state:
          rv.append(("flora avatar b1striped_panties",125,824))
        elif "_panties" in state:
          rv.append(("flora avatar b1panty",128,825))
        if "_purple_bra" in state:
          rv.append(("flora avatar b1purple_bra",159,437))
        elif "_bra" in state:
          rv.append(("flora avatar b1bra",160,434))
        if "_pants" in state:
          rv.append(("flora avatar b1pants",117,803))
        elif "_skirt" in state:
          rv.append(("flora avatar b1skirt",105,716))
        if "_hands_concealed" in state:
          rv.append(("flora avatar b1arm1_n",21,432))
        else:
          rv.append(("flora avatar b1arm2_n",21,433))
        if "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b1shirt_knotted",160,417))
          else:
            rv.append(("flora avatar b1shirt",139,417))
          rv.append(("flora avatar b1arm2_c",16,432))
        elif "_blouse" in state:
          rv.append(("flora avatar b1blouse",139,400))
          if "_dress" in state:
            rv.append(("flora avatar b1dress",116,427))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b1referee_shirt",121,401))
            rv.append(("flora avatar b1arm2_referee_shirt",13,456))
          elif "_hands_concealed" in state:
            rv.append(("flora avatar b1arm1_blouse",13,460))
          else:
            rv.append(("flora avatar b1arm2_blouse",13,460))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b1trench_coat",78,426))
          # rv.append(("flora avatar b1arm1_trench_coat",7,457))
          rv.append(("flora avatar b1arm2_trench_coat",7,457))
        if "_sun_hat" in state:
          rv.append(("flora avatar b1sun_hat",57,110))
        elif "_hat" in state:
          rv.append(("flora avatar b1chefhat",141,75))
        elif "_cop" in state:
          rv.append(("flora avatar b1cop_hat",167,106))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b1referee_cap",190,139))
        if "_blindfold" in state:
          rv.append(("flora avatar b1bandage",244,265))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("flora avatar body1",118,153))
        rv.append(("flora avatar face_excited",244,254))
        if "_striped_panties" in state:
          rv.append(("flora avatar b1striped_panties",125,824))
        elif "_panties" in state:
          rv.append(("flora avatar b1panty",128,825))
        if "_purple_bra" in state:
          rv.append(("flora avatar b1purple_bra",159,437))
        elif "_bra" in state:
          rv.append(("flora avatar b1bra",160,434))
        if "_pants" in state:
          rv.append(("flora avatar b1pants",117,803))
        elif "_skirt" in state:
          rv.append(("flora avatar b1skirt",105,716))
        rv.append(("flora avatar b1arm1_n",21,432))
        if "_leash" in state:
          if "_retracted" in state:
            rv.append(("flora avatar b1leash_retracted",64,834))
          else:
            rv.append(("flora avatar b1leash",64,834))
        if "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b1shirt_knotted",160,417))
          else:
            rv.append(("flora avatar b1shirt",139,417))
          rv.append(("flora avatar b1arm1_c",16,432))
          if "_leash" in state:
            if "_retracted" in state:
              rv.append(("flora avatar b1leash_retracted",64,834))
            else:
              rv.append(("flora avatar b1leash",64,834))
        elif "_blouse" in state:
          rv.append(("flora avatar b1blouse",139,400))
          if "_dress" in state:
            rv.append(("flora avatar b1dress",116,427))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b1referee_shirt",121,401))
            rv.append(("flora avatar b1arm1_referee_shirt",13,456))
          else:
            rv.append(("flora avatar b1arm1_blouse",13,460))
        if "_hat" in state:
          rv.append(("flora avatar b1chefhat",141,75))
        elif "_cop" in state:
          rv.append(("flora avatar b1cop_hat",167,106))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b1referee_cap",190,139))
        if "_blindfold" in state:
          rv.append(("flora avatar b1bandage",244,265))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("flora avatar body1",118,153))
        rv.append(("flora avatar face_neutral",237,254))
        if "_striped_panties" in state:
          rv.append(("flora avatar b1striped_panties",125,824))
        elif "_panties" in state:
          rv.append(("flora avatar b1panty",128,825))
        if "_purple_bra" in state:
          rv.append(("flora avatar b1purple_bra",159,437))
        elif "_bra" in state:
          rv.append(("flora avatar b1bra",160,434))
        if "_pants" in state:
          rv.append(("flora avatar b1pants",117,803))
        elif "_skirt" in state:
          rv.append(("flora avatar b1skirt",105,716))
        rv.append(("flora avatar b1arm1_n",21,432))
        if "_leash" in state:
          if "_retracted" in state:
            rv.append(("flora avatar b1leash_retracted",64,834))
          else:
            rv.append(("flora avatar b1leash",64,834))
        if "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b1shirt_knotted",160,417))
          else:
            rv.append(("flora avatar b1shirt",139,417))
          rv.append(("flora avatar b1arm1_c",16,432))
          if "_leash" in state:
            if "_retracted" in state:
              rv.append(("flora avatar b1leash_retracted",64,834))
            else:
              rv.append(("flora avatar b1leash",64,834))
        elif "_blouse" in state:
          rv.append(("flora avatar b1blouse",139,400))
          if "_dress" in state:
            rv.append(("flora avatar b1dress",116,427))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b1referee_shirt",121,401))
            rv.append(("flora avatar b1arm1_referee_shirt",13,456))
          else:
            rv.append(("flora avatar b1arm1_blouse",13,460))
        if "_hat" in state:
          rv.append(("flora avatar b1chefhat",141,75))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b1referee_cap",190,139))
        if "_blindfold" in state:
          rv.append(("flora avatar b1bandage",244,265))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        if "_wet" in state:
          rv.append(("flora avatar body2_wet",140,160))
          rv.append(("flora avatar face_afraid_wet",185,261))
        else:
          rv.append(("flora avatar body2",136,155))
          rv.append(("flora avatar face_afraid",184,261))
        if "_striped_panties" in state:
          rv.append(("flora avatar b2striped_panties",146,830))
        elif "_panties" in state:
          rv.append(("flora avatar b2panty",146,820))
        if "_purple_bra" in state:
          rv.append(("flora avatar b2purple_bra",162,437))
        elif "_bra" in state:
          rv.append(("flora avatar b2bra",163,441))
        if "_pants" in state:
          rv.append(("flora avatar b2pants",136,825))
        elif "_skirt" in state:
          rv.append(("flora avatar b2skirt",128,725))
        if "_leash" in state:
          rv.append(("flora avatar b2arm1_n",119,420))
          rv.append(("flora avatar b2leash",368,784))
        else:
          rv.append(("flora avatar b2arm2_n",123,428))
        if "_shirt" in state:
          if "_skirt" in state:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_knotted_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt_knotted",137,419))
          else:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt",137,419))
          if "_leash" in state:
            rv.append(("flora avatar b2arm1_c",119,420))
            rv.append(("flora avatar b2leash",368,784))
          else:
            rv.append(("flora avatar b2arm2_c",121,426))
        elif "_blouse" in state:
          rv.append(("flora avatar b2blouse",145,411))
          if "_dress" in state:
            rv.append(("flora avatar b2dress",129,417))
          rv.append(("flora avatar b2arm2_blouse",115,425))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b2trench_coat",64,417))
          rv.append(("flora avatar b2arm2_trench_coat",107,448))
        elif "_armor" in state:
          rv.append(("flora avatar b2armor",100,426))
          rv.append(("flora avatar b2arm2_armor",123,459))
        if "sun_hat" in state:
          rv.append(("flora avatar b2sun_hat",35,135))
        elif "_hat" in state:
          rv.append(("flora avatar b2chefhat",172,83))
        elif "_cop" in state:
          rv.append(("flora avatar b2cop_hat",130,117))
        if "_blindfold" in state:
          rv.append(("flora avatar b2bandage",189,273))
        if "_vines" in state:
          rv.append(("flora avatar b2vine",0,0))
        if "_cum" in state:
          rv.append(("flora avatar b2cum",0,0))
        if "_wet" in state:
          rv.append(("flora avatar b2water_drops",154,180))

      elif state.startswith("crying"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        rv.append(("flora avatar body2",136,155))
        rv.append(("flora avatar face_crying",179,268))
        if "_striped_panties" in state:
          rv.append(("flora avatar b2striped_panties",146,830))
        elif "_panties" in state:
          rv.append(("flora avatar b2panty",146,820))
        if "_purple_bra" in state:
          rv.append(("flora avatar b2purple_bra",162,437))
        elif "_bra" in state:
          rv.append(("flora avatar b2bra",163,441))
        if "_grimy" in state:
          rv.append(("flora avatar body2_grimy",140,456))
        if "_pants" in state:
          rv.append(("flora avatar b2pants",136,825))
        elif "_skirt" in state:
          rv.append(("flora avatar b2skirt",128,725))
        if "_grimy" in state:
          rv.append(("flora avatar b2arm1_n_grimy",119,420))
          # rv.append(("flora avatar b2arm2_n_grimy",123,428))
        else:
          rv.append(("flora avatar b2arm1_n",119,420))
        if "_shirt" in state:
          if "_skirt" in state:
            rv.append(("flora avatar b2shirt_knotted",137,419))
          else:
            rv.append(("flora avatar b2shirt",137,419))
          rv.append(("flora avatar b2arm1_c",119,420))
        elif "_blouse" in state:
          rv.append(("flora avatar b2blouse",145,411))
          if "_dress" in state:
            rv.append(("flora avatar b2dress",129,417))
          rv.append(("flora avatar b2arm1_blouse",115,425))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b2trench_coat",64,417))
          rv.append(("flora avatar b2arm1_trench_coat",107,448))
        if "_sun_hat" in state:
          rv.append(("flora avatar b2sun_hat",35,135))
        elif "_hat" in state:
          rv.append(("flora avatar b2chefhat",172,83))
        if "_blindfold" in state:
          rv.append(("flora avatar b2bandage",189,273))
        if "_vines" in state:
          rv.append(("flora avatar b2vine",0,0))
        if "_cum" in state:
          rv.append(("flora avatar b2cum",0,0))

      elif state.startswith("embarrassed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        if "_wet" in state:
          rv.append(("flora avatar body2_wet",140,160))
          rv.append(("flora avatar face_embarrassed_wet",185,259))
        else:
          rv.append(("flora avatar body2",136,155))
          rv.append(("flora avatar face_embarrassed",185,267))
        if "_striped_panties" in state:
          rv.append(("flora avatar b2striped_panties",146,830))
        elif "_panties" in state:
          rv.append(("flora avatar b2panty",146,820))
        if "_purple_bra" in state:
          rv.append(("flora avatar b2purple_bra",162,437))
        elif "_bra" in state:
          rv.append(("flora avatar b2bra",163,441))
        if "_pants" in state:
          rv.append(("flora avatar b2pants",136,825))
        elif "_skirt" in state:
          rv.append(("flora avatar b2skirt",128,725))
        if "_bucket" in state:
          rv.append(("flora avatar b2bucket_n",123,426))
        else:
          rv.append(("flora avatar b2arm2_n",123,428))
        if "_shirt" in state:
          if "_skirt" in state:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_knotted_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt_knotted",137,419))
          else:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt",137,419))
          rv.append(("flora avatar b2arm2_c",121,426))
        elif "_blouse" in state:
          rv.append(("flora avatar b2blouse",145,411))
          if "_dress" in state:
            rv.append(("flora avatar b2dress",129,417))
          if "_bucket" in state:
            rv.append(("flora avatar b2bucket_blouse",121,426))
          else:
            rv.append(("flora avatar b2arm2_blouse",115,425))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b2trench_coat",64,417))
          rv.append(("flora avatar b2arm2_trench_coat",107,448))
        elif "_armor" in state:
          rv.append(("flora avatar b2armor",100,426))
          rv.append(("flora avatar b2arm2_armor",123,459))
        if "_sun_hat" in state:
          rv.append(("flora avatar b2sun_hat",35,135))
        elif "_hat" in state:
          rv.append(("flora avatar b2chefhat",172,83))
        elif "_cop" in state:
          rv.append(("flora avatar b2cop_hat",130,117))
        if "_blindfold" in state:
          rv.append(("flora avatar b2bandage",189,273))
        if "_vines" in state:
          rv.append(("flora avatar b2vine",0,0))
        if "_cum" in state:
          rv.append(("flora avatar b2cum",0,0))
        if "_wet" in state:
          rv.append(("flora avatar b2water_drops",154,180))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        if "_wet" in state:
          rv.append(("flora avatar body2_wet",140,160))
          rv.append(("flora avatar face_sad_wet",185,261))
        else:
          rv.append(("flora avatar body2",136,155))
          rv.append(("flora avatar face_sad",185,261))
        if "_striped_panties" in state:
          rv.append(("flora avatar b2striped_panties",146,830))
        elif "_panties" in state:
          rv.append(("flora avatar b2panty",146,820))
        if "_purple_bra" in state:
          rv.append(("flora avatar b2purple_bra",162,437))
        elif "_bra" in state:
          rv.append(("flora avatar b2bra",163,441))
        if "_grimy" in state:
          rv.append(("flora avatar body2_grimy",140,456))
        if "_pants" in state:
          rv.append(("flora avatar b2pants",136,825))
        elif "_skirt" in state:
          rv.append(("flora avatar b2skirt",128,725))
        if "_bucket" in state:
          rv.append(("flora avatar b2bucket_n",123,426))
        elif "_grimy" in state:
          rv.append(("flora avatar b2arm1_n_grimy",119,420))
          # rv.append(("flora avatar b2arm2_n_grimy",123,428))
        else:
          rv.append(("flora avatar b2arm1_n",119,420))
        if "_shirt" in state:
          if "_skirt" in state:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_knotted_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt_knotted",137,419))
          else:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt",137,419))
          rv.append(("flora avatar b2arm1_c",119,420))
        elif "_blouse" in state:
          rv.append(("flora avatar b2blouse",145,411))
          if "_dress" in state:
            rv.append(("flora avatar b2dress",129,417))
          if "_bucket" in state:
            rv.append(("flora avatar b2bucket_blouse",121,426))
          else:
            rv.append(("flora avatar b2arm1_blouse",115,425))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b2trench_coat",64,417))
          rv.append(("flora avatar b2arm1_trench_coat",107,448))
        if "_sun_hat" in state:
          rv.append(("flora avatar b2sun_hat",35,135))
        elif "_hat" in state:
          rv.append(("flora avatar b2chefhat",172,83))
        elif "_cop" in state:
          rv.append(("flora avatar b2cop_hat",130,117))
        if "_blindfold" in state:
          rv.append(("flora avatar b2bandage",189,273))
        if "_vines" in state:
          rv.append(("flora avatar b2vine",0,0))
        if "_cum" in state:
          rv.append(("flora avatar b2cum",0,0))
        if "_wet" in state:
          rv.append(("flora avatar b2water_drops",154,180))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        if "_wet" in state:
          rv.append(("flora avatar body2_wet",140,160))
          rv.append(("flora avatar face_skeptical_wet",185,257))
        else:
          rv.append(("flora avatar body2",136,155))
          rv.append(("flora avatar face_skeptical",185,256))
        if "_striped_panties" in state:
          rv.append(("flora avatar b2striped_panties",146,830))
        elif "_panties" in state:
          rv.append(("flora avatar b2panty",146,820))
        if "_purple_bra" in state:
          rv.append(("flora avatar b2purple_bra",162,437))
        elif "_bra" in state:
          rv.append(("flora avatar b2bra",163,441))
        if "_pants" in state:
          rv.append(("flora avatar b2pants",136,825))
        elif "_skirt" in state:
          rv.append(("flora avatar b2skirt",128,725))
        if "_hands_up" in state:
          rv.append(("flora avatar b2arm2_n",123,428))
        elif "_bucket" in state:
          rv.append(("flora avatar b2bucket_n",123,426))
        else:
          rv.append(("flora avatar b2arm1_n",119,420))
          if "_leash" in state:
            rv.append(("flora avatar b2leash",368,784))
        if "_shirt" in state:
          if "_skirt" in state:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_knotted_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt_knotted",137,419))
          else:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt",137,419))
          if "_hands_up" in state:
            rv.append(("flora avatar b2arm2_c",121,426))
          else:
            rv.append(("flora avatar b2arm1_c",119,420))
            if "_leash" in state:
              rv.append(("flora avatar b2leash",368,784))
        elif "_blouse" in state:
          rv.append(("flora avatar b2blouse",145,411))
          if "_dress" in state:
            rv.append(("flora avatar b2dress",129,417))
          if "_bucket" in state:
            rv.append(("flora avatar b2bucket_blouse",121,426))
          else:
            rv.append(("flora avatar b2arm1_blouse",115,425))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b2trench_coat",64,417))
          rv.append(("flora avatar b2arm1_trench_coat",107,448))
        elif "_armor" in state:
          rv.append(("flora avatar b2armor",100,426))
          rv.append(("flora avatar b2arm1_armor",119,570))
        if "_sun_hat" in state:
          rv.append(("flora avatar b2sun_hat",35,135))
        elif "_hat" in state:
          rv.append(("flora avatar b2chefhat",172,83))
        elif "_cop" in state:
          rv.append(("flora avatar b2cop_hat",130,117))
        if "_blindfold" in state:
          rv.append(("flora avatar b2bandage",189,273))
        if "_vines" in state:
          rv.append(("flora avatar b2vine",0,0))
        if "_cum" in state:
          rv.append(("flora avatar b2cum",0,0))
        if "_wet" in state:
          rv.append(("flora avatar b2water_drops",154,180))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        if "_wet" in state:
          rv.append(("flora avatar body2_wet",140,160))
          rv.append(("flora avatar face_smile_wet",185,262))
        else:
          rv.append(("flora avatar body2",136,155))
          rv.append(("flora avatar face_smile",185,267))
        if "_striped_panties" in state:
          rv.append(("flora avatar b2striped_panties",146,830))
        elif "_panties" in state:
          rv.append(("flora avatar b2panty",146,820))
        if "_purple_bra" in state:
          rv.append(("flora avatar b2purple_bra",162,437))
        elif "_bra" in state:
          rv.append(("flora avatar b2bra",163,441))
        if "_grimy" in state:
          rv.append(("flora avatar body2_grimy",140,456))
        if "_pants" in state:
          rv.append(("flora avatar b2pants",136,825))
        elif "_skirt" in state:
          rv.append(("flora avatar b2skirt",128,725))
        if "_hands_up" in state:
          rv.append(("flora avatar b2arm2_n",123,428))
        elif "_bucket" in state:
          rv.append(("flora avatar b2bucket_n",123,426))
        elif "_grimy" in state:
          rv.append(("flora avatar b2arm1_n_grimy",119,420))
          # rv.append(("flora avatar b2arm2_n_grimy",123,428))
        else:
          rv.append(("flora avatar b2arm1_n",119,420))
        if "_shirt" in state:
          if "_skirt" in state:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_knotted_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt_knotted",137,419))
          else:
            if "_wet" in state:
              rv.append(("flora avatar b2shirt_wet",137,419))
            else:
              rv.append(("flora avatar b2shirt",137,419))
          if "_hands_up" in state:
            rv.append(("flora avatar b2arm2_c",121,426))
          else:
            rv.append(("flora avatar b2arm1_c",119,420))
        elif "_blouse" in state:
          rv.append(("flora avatar b2blouse",145,411))
          if "_dress" in state:
            rv.append(("flora avatar b2dress",129,417))
          if "_bucket" in state:
            rv.append(("flora avatar b2bucket_blouse",121,426))
          else:
            rv.append(("flora avatar b2arm1_blouse",115,425))
        elif "_trench_coat" in state:
          rv.append(("flora avatar b2trench_coat",64,417))
          rv.append(("flora avatar b2arm1_trench_coat",107,448))
        elif "_armor" in state:
          rv.append(("flora avatar b2armor",100,426))
          rv.append(("flora avatar b2arm1_armor",119,570))
        if "_sun_hat" in state:
          rv.append(("flora avatar b2sun_hat",35,135))
        elif "_hat" in state:
          rv.append(("flora avatar b2chefhat",172,83))
        elif "_cop" in state:
          rv.append(("flora avatar b2cop_hat",130,117))
        if "_blindfold" in state:
          rv.append(("flora avatar b2bandage",189,273))
        if "_vines" in state:
          rv.append(("flora avatar b2vine",0,0))
        if "_cum" in state:
          rv.append(("flora avatar b2cum",0,0))
        if "_wet" in state:
          rv.append(("flora avatar b2water_drops",154,180))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((620,1080))
        rv.append(("flora avatar body3",49,150))
        if "_texting" in state and not "_eyes_up" in state:
          rv.append(("flora avatar face_annoyed_phone",190,257))
        else:
          rv.append(("flora avatar face_annoyed",191,257))
        if "_striped_panties" in state:
          rv.append(("flora avatar b3striped_panties",166,808))
        elif "_panties" in state:
          rv.append(("flora avatar b3panty",151,818))
        if "_purple_bra" in state:
          rv.append(("flora avatar b3purple_bra",138,443))
        elif "_bra" in state:
          rv.append(("flora avatar b3bra",138,445))
        if "_pants" in state:
          rv.append(("flora avatar b3pants",49,821))
        elif "_skirt" in state:
          rv.append(("flora avatar b3skirt",112,697))
        if "_texting" not in state:
          rv.append(("flora avatar b3arm2_n",67,420))
        if "_shirt" in state:
          if "_skirt" in state:
            rv.append(("flora avatar b3shirt_knotted",138,418))
          else:
            rv.append(("flora avatar b3shirt",138,418))
          if "_texting" in state:
            rv.append(("flora avatar b3armphone_n",83,428))
            rv.append(("flora avatar b3armphone_c",153,428))
          else:
            rv.append(("flora avatar b3arm2_c",67,419))
        elif "_blouse" in state:
          rv.append(("flora avatar b3blouse",137,422))
          if "_dress" in state:
            rv.append(("flora avatar b3dress",75,442))
          if "_texting" in state:
            rv.append(("flora avatar b3armphone_n",83,428))
            rv.append(("flora avatar b3armphone_blouse",83,426))
          else:
            rv.append(("flora avatar b3arm2_blouse",67,418))
        elif "_armor" in state:
          rv.append(("flora avatar b3armor",92,533))
          rv.append(("flora avatar b3arm2_armor",80,419))
        if "_hat" in state:
          rv.append(("flora avatar b3chefhat",172,77))
        if "_blindfold" in state:
          rv.append(("flora avatar b3bandage",191,277))

      elif state.startswith("confused"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((620,1080))
        rv.append(("flora avatar body3",49,150))
        rv.append(("flora avatar face_confused",191,252))
        if "_striped_panties" in state:
          rv.append(("flora avatar b3striped_panties",166,808))
        elif "_panties" in state:
          rv.append(("flora avatar b3panty",151,818))
        if "_purple_bra" in state:
          rv.append(("flora avatar b3purple_bra",138,443))
        elif "_bra" in state:
          rv.append(("flora avatar b3bra",138,445))
        if "_pants" in state:
          rv.append(("flora avatar b3pants",49,821))
        elif "_skirt" in state:
          rv.append(("flora avatar b3skirt",112,697))
        rv.append(("flora avatar b3arm1_n",20,193))
        if "_leash" in state:
          rv.append(("flora avatar b3leash",437,840))
        if "_shirt" in state:
          if "_skirt" in state:
            rv.append(("flora avatar b3shirt_knotted",138,418))
          else:
            rv.append(("flora avatar b3shirt",138,418))
          rv.append(("flora avatar b3arm1_c",19,242))
          if "_leash" in state:
            rv.append(("flora avatar b3leash",437,840))
        elif "_blouse" in state:
          rv.append(("flora avatar b3blouse",137,422))
          if "_dress" in state:
            rv.append(("flora avatar b3dress",75,442))
          rv.append(("flora avatar b3arm1_blouse",16,244))
        elif "_armor" in state:
          rv.append(("flora avatar b3armor",92,533))
          rv.append(("flora avatar b3arm1_armor",20,243))
        if "_hat" in state:
          rv.append(("flora avatar b3chefhat",172,77))
        if "_blindfold" in state:
          rv.append(("flora avatar b3bandage",191,277))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((620,1080))
        rv.append(("flora avatar body3",49,150))
        rv.append(("flora avatar face_cringe",191,256))
        if "_striped_panties" in state:
          rv.append(("flora avatar b3striped_panties",166,808))
        elif "_panties" in state:
          rv.append(("flora avatar b3panty",151,818))
        if "_purple_bra" in state:
          rv.append(("flora avatar b3purple_bra",138,443))
        elif "_bra" in state:
          rv.append(("flora avatar b3bra",138,445))
        if "_pants" in state:
          rv.append(("flora avatar b3pants",49,821))
        elif "_skirt" in state:
          rv.append(("flora avatar b3skirt",112,697))
        rv.append(("flora avatar b3arm2_n",67,420))
        if "_shirt" in state:
          if "_skirt" in state:
            rv.append(("flora avatar b3shirt_knotted",138,418))
          else:
            rv.append(("flora avatar b3shirt",138,418))
          rv.append(("flora avatar b3arm2_c",67,419))
        elif "_blouse" in state:
          rv.append(("flora avatar b3blouse",137,422))
          if "_dress" in state:
            rv.append(("flora avatar b3dress",75,442))
          rv.append(("flora avatar b3arm2_blouse",67,418))
        if "_hat" in state:
          rv.append(("flora avatar b3chefhat",172,77))
        if "_blindfold" in state:
          rv.append(("flora avatar b3bandage",191,277))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((620,1080))
        rv.append(("flora avatar body3",49,150))
        rv.append(("flora avatar face_eyeroll",191,254))
        if "_striped_panties" in state:
          rv.append(("flora avatar b3striped_panties",166,808))
        elif "_panties" in state:
          rv.append(("flora avatar b3panty",151,818))
        if "_purple_bra" in state:
          rv.append(("flora avatar b3purple_bra",138,443))
        elif "_bra" in state:
          rv.append(("flora avatar b3bra",138,445))
        if "_pants" in state:
          rv.append(("flora avatar b3pants",49,821))
        elif "_skirt" in state:
          rv.append(("flora avatar b3skirt",112,697))
        if "_texting" in state:
          rv.append(("flora avatar b3armphone_n",83,428))
        else:
          if "_leash" in state:
            rv.append(("flora avatar b3arm1_n",20,193))
            rv.append(("flora avatar b3leash",437,840))
          else:
            rv.append(("flora avatar b3arm2_n",67,420))
        if "_shirt" in state:
          if "_skirt" in state:
            rv.append(("flora avatar b3shirt_knotted",138,418))
          else:
            rv.append(("flora avatar b3shirt",138,418))
          if "_texting" in state:
            rv.append(("flora avatar b3armphone_n",83,428))
            rv.append(("flora avatar b3armphone_c",153,428))
          else:
            if "_leash" in state:
              rv.append(("flora avatar b3arm1_c",19,242))
              rv.append(("flora avatar b3leash",437,840))
            else:
              rv.append(("flora avatar b3arm2_c",67,419))
        elif "_blouse" in state:
          rv.append(("flora avatar b3blouse",137,422))
          if "_dress" in state:
            rv.append(("flora avatar b3dress",75,442))
          rv.append(("flora avatar b3arm2_blouse",67,418))
        elif "_armor" in state:
          rv.append(("flora avatar b3armor",92,533))
          rv.append(("flora avatar b3arm2_armor",80,419))
        if "_hat" in state:
          rv.append(("flora avatar b3chefhat",172,77))
        if "_blindfold" in state:
          rv.append(("flora avatar b3bandage",191,277))

      elif state.startswith("sarcastic"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((620,1080))
        rv.append(("flora avatar body3",49,150))
        rv.append(("flora avatar face_sarcastic",191,253))
        if "_striped_panties" in state:
          rv.append(("flora avatar b3striped_panties",166,808))
        elif "_panties" in state:
          rv.append(("flora avatar b3panty",151,818))
        if "_purple_bra" in state:
          rv.append(("flora avatar b3purple_bra",138,443))
        elif "_bra" in state:
          rv.append(("flora avatar b3bra",138,445))
        if "_pants" in state:
          rv.append(("flora avatar b3pants",49,821))
        elif "_skirt" in state:
          rv.append(("flora avatar b3skirt",112,697))
        if "_leash" in state:
          rv.append(("flora avatar b3arm1_n",20,193))
          rv.append(("flora avatar b3leash",437,840))
        else:
          rv.append(("flora avatar b3arm2_n",67,420))
        if "_shirt" in state:
          if "_skirt" in state:
            rv.append(("flora avatar b3shirt_knotted",138,418))
          else:
            rv.append(("flora avatar b3shirt",138,418))
          if "_leash" in state:
            rv.append(("flora avatar b3arm1_c",19,242))
            rv.append(("flora avatar b3leash",437,840))
          else:
            rv.append(("flora avatar b3arm2_c",67,419))
        elif "_blouse" in state:
          rv.append(("flora avatar b3blouse",137,422))
          if "_dress" in state:
            rv.append(("flora avatar b3dress",75,442))
          rv.append(("flora avatar b3arm2_blouse",67,418))
        elif "_armor" in state:
          rv.append(("flora avatar b3armor",92,533))
          rv.append(("flora avatar b3arm2_armor",80,419))
        if "_hat" in state:
          rv.append(("flora avatar b3chefhat",172,77))
        if "_blindfold" in state:
          rv.append(("flora avatar b3bandage",191,277))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        rv.append(("flora avatar body4",125,149))
        rv.append(("flora avatar face_blush",226,260))
        if "_striped_panties" in state:
          rv.append(("flora avatar b4striped_panties",140,799))
        elif "_panties" in state:
          rv.append(("flora avatar b4panty",141,789))
        if "_purple_bra" in state:
          rv.append(("flora avatar b4purple_bra",187,419))
        elif "_bra" in state:
          rv.append(("flora avatar b4bra",189,419))
        if "_grimy" in state:
          rv.append(("flora avatar body4_grimy",125,327))
        if "_pants" in state:
          rv.append(("flora avatar b4pants",122,757))
        elif "_skirt" in state:
          rv.append(("flora avatar b4skirt",108,690))
        if "_grimy" in state:
          rv.append(("flora avatar b4arm1_n_grimy",119,353))
        else:
          rv.append(("flora avatar b4arm1_n",119,353))
        if "_leash" in state:
          if "_retracted" in state:
            rv.append(("flora avatar b4leash_retracted",392,673))
          else:
            rv.append(("flora avatar b4leash",392,673))
        if "_business_shirt" in state:
          rv.append(("flora avatar b4shirt_business",137,409))
          rv.append(("flora avatar b4arm1_business_c",116,353))
        elif "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b4shirt_knotted",136,418))
          else:
            rv.append(("flora avatar b4shirt",136,418))
          rv.append(("flora avatar b4arm1_c",115,353))
          if "_leash" in state:
            if "_retracted" in state:
              rv.append(("flora avatar b4leash_retracted",392,673))
            else:
              rv.append(("flora avatar b4leash",392,673))
        elif "_blouse" in state:
          rv.append(("flora avatar b4blouse",136,407))
          if "_dress" in state:
            rv.append(("flora avatar b4dress",122,460))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b4referee_shirt",120,407))
            rv.append(("flora avatar b4arm1_referee_shirt",111,343))
          else:
            rv.append(("flora avatar b4arm1_blouse",115,353))
        if "_hat" in state:
          rv.append(("flora avatar b4chefhat",119,79))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b4referee_cap",188,137))
        if "_blindfold" in state:
          rv.append(("flora avatar b4bandage",224,280))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        rv.append(("flora avatar body4",125,149))
        rv.append(("flora avatar face_flirty",226,265))
        if "_striped_panties" in state:
          rv.append(("flora avatar b4striped_panties",140,799))
        elif "_panties" in state:
          rv.append(("flora avatar b4panty",141,789))
        if "_purple_bra" in state:
          rv.append(("flora avatar b4purple_bra",187,419))
        elif "_bra" in state:
          rv.append(("flora avatar b4bra",189,419))
        if "_grimy" in state:
          rv.append(("flora avatar body4_grimy",125,327))
        if "_pants" in state:
          rv.append(("flora avatar b4pants",122,757))
        elif "_skirt" in state:
          rv.append(("flora avatar b4skirt",108,690))
        if "_grimy" in state:
          rv.append(("flora avatar b4arm2_n_grimy",119,370))
        else:
          rv.append(("flora avatar b4arm2_n",119,370))
        if "_business_shirt" in state:
          rv.append(("flora avatar b4shirt_business",137,409))
          rv.append(("flora avatar b4arm2_business_c",116,370))
        elif "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b4shirt_knotted",136,418))
          else:
            rv.append(("flora avatar b4shirt",136,418))
          rv.append(("flora avatar b4arm2_c",115,369))
        elif "_blouse" in state:
          rv.append(("flora avatar b4blouse",136,407))
          if "_dress" in state:
            rv.append(("flora avatar b4dress",122,460))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b4referee_shirt",120,407))
            rv.append(("flora avatar b4arm2_referee_shirt",111,370))
          else:
            rv.append(("flora avatar b4arm2_blouse",115,370))
        if "_hat" in state:
          rv.append(("flora avatar b4chefhat",119,79))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b4referee_cap",188,137))
        if "_blindfold" in state:
          rv.append(("flora avatar b4bandage",224,280))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        rv.append(("flora avatar body4",125,149))
        rv.append(("flora avatar face_laughing",219,254))
        if "_striped_panties" in state:
          rv.append(("flora avatar b4striped_panties",140,799))
        elif "_panties" in state:
          rv.append(("flora avatar b4panty",141,789))
        if "_purple_bra" in state:
          rv.append(("flora avatar b4purple_bra",187,419))
        elif "_bra" in state:
          rv.append(("flora avatar b4bra",189,419))
        if "_grimy" in state:
          rv.append(("flora avatar body4_grimy",125,327))
        if "_pants" in state:
          rv.append(("flora avatar b4pants",122,757))
        elif "_skirt" in state:
          rv.append(("flora avatar b4skirt",108,690))
        if "_grimy" in state:
          rv.append(("flora avatar b4arm1_n_grimy",119,353))
        else:
          rv.append(("flora avatar b4arm1_n",119,353))
        if "_leash" in state:
          rv.append(("flora avatar b4leash",392,673))
        if "_business_shirt" in state:
          rv.append(("flora avatar b4shirt_business",137,409))
          rv.append(("flora avatar b4arm1_business_c",116,353))
        elif "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b4shirt_knotted",136,418))
          else:
            rv.append(("flora avatar b4shirt",136,418))
          rv.append(("flora avatar b4arm1_c",115,353))
          if "_leash" in state:
            rv.append(("flora avatar b4leash",392,673))
        elif "_blouse" in state:
          rv.append(("flora avatar b4blouse",136,407))
          if "_dress" in state:
            rv.append(("flora avatar b4dress",122,460))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b4referee_shirt",120,407))
            rv.append(("flora avatar b4arm1_referee_shirt",111,343))
          else:
            rv.append(("flora avatar b4arm1_blouse",115,353))
        if "_hat" in state:
          rv.append(("flora avatar b4chefhat",119,79))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b4referee_cap",188,137))
        if "_blindfold" in state:
          rv.append(("flora avatar b4bandage",224,280))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        rv.append(("flora avatar body4",125,149))
        rv.append(("flora avatar face_thinking",216,259))
        if "_striped_panties" in state:
          rv.append(("flora avatar b4striped_panties",140,799))
        elif "_panties" in state:
          rv.append(("flora avatar b4panty",141,789))
        if "_purple_bra" in state:
          rv.append(("flora avatar b4purple_bra",187,419))
        elif "_bra" in state:
          rv.append(("flora avatar b4bra",189,419))
        if "_grimy" in state:
          rv.append(("flora avatar body4_grimy",125,327))
        if "_pants" in state:
          rv.append(("flora avatar b4pants",122,757))
        elif "_skirt" in state:
          rv.append(("flora avatar b4skirt",108,690))
        if "_grimy" in state:
          rv.append(("flora avatar b4arm2_n_grimy",119,370))
        else:
          rv.append(("flora avatar b4arm2_n",119,370))
        if "_leash" in state:
          rv.append(("flora avatar b4leash",392,673))
        if "_business_shirt" in state:
          rv.append(("flora avatar b4shirt_business",137,409))
          rv.append(("flora avatar b4arm2_business_c",116,370))
        elif "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b4shirt_knotted",136,418))
          else:
            rv.append(("flora avatar b4shirt",136,418))
          rv.append(("flora avatar b4arm2_c",115,369))
          if "_leash" in state:
            rv.append(("flora avatar b4leash",392,673))
        elif "_blouse" in state:
          rv.append(("flora avatar b4blouse",136,407))
          if "_dress" in state:
            rv.append(("flora avatar b4dress",122,460))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b4referee_shirt",120,407))
            rv.append(("flora avatar b4arm2_referee_shirt",111,370))
          else:
            rv.append(("flora avatar b4arm2_blouse",115,370))
        if "_hat" in state:
          rv.append(("flora avatar b4chefhat",119,79))
        elif "_referee_cap" in state:
          rv.append(("flora avatar b4referee_cap",188,137))
        if "_blindfold" in state:
          rv.append(("flora avatar b4bandage",224,280))

      elif state.startswith("worried"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((580,1080))
        rv.append(("flora avatar body4",125,149))
        rv.append(("flora avatar face_worried",226,260))
        if "_striped_panties" in state:
          rv.append(("flora avatar b4striped_panties",140,799))
        elif "_panties" in state:
          rv.append(("flora avatar b4panty",141,789))
        if "_purple_bra" in state:
          rv.append(("flora avatar b4purple_bra",187,419))
        elif "_bra" in state:
          rv.append(("flora avatar b4bra",189,419))
        if "_grimy" in state:
          rv.append(("flora avatar body4_grimy",125,327))
        if "_pants" in state:
          rv.append(("flora avatar b4pants",122,757))
        elif "_skirt" in state:
          rv.append(("flora avatar b4skirt",108,690))
        if "_grimy" in state:
          rv.append(("flora avatar b4arm2_n_grimy",119,370))
        else:
          rv.append(("flora avatar b4arm2_n",119,370))
        if "_leash" in state:
          rv.append(("flora avatar b4leash",392,673))
        if "_business_shirt" in state:
          rv.append(("flora avatar b4shirt_business",137,409))
          rv.append(("flora avatar b4arm2_business_c",116,370))
        elif "_shirt" in state and "_referee_shirt" not in state:
          if "_skirt" in state:
            rv.append(("flora avatar b4shirt_knotted",136,418))
          else:
            rv.append(("flora avatar b4shirt",136,418))
          rv.append(("flora avatar b4arm2_c",115,369))
          if "_leash" in state:
            rv.append(("flora avatar b4leash",392,673))
        elif "_blouse" in state:
          rv.append(("flora avatar b4blouse",136,407))
          if "_dress" in state:
            rv.append(("flora avatar b4dress",122,460))
          if "_referee_shirt" in state:
            rv.append(("flora avatar b4referee_shirt",120,407))
            rv.append(("flora avatar b4arm2_referee_shirt",111,370))
          else:
            rv.append(("flora avatar b4arm2_blouse",115,370))
        if "_hat" in state:
          rv.append(("flora avatar b4chefhat",119,79))
        if "_referee_cap" in state:
          rv.append(("flora avatar b4referee_cap",188,137))
        if "_blindfold" in state:
          rv.append(("flora avatar b4bandage",224,280))

      elif state=="kitchen_cereals_1":
        rv.append((1920,1080))
        rv.append(("flora avatar events cereal floracereal_bg",0,0))
        rv.append(("flora avatar events cereal floracereal_n",358,2))
        rv.append(("flora avatar events cereal floracereal_head1",481,272))
        rv.append(("flora avatar events cereal floracereal_panty",543,869))
        rv.append(("flora avatar events cereal floracereal_bra",470,493))
        rv.append(("flora avatar events cereal floracereal_top",389,228))
      elif state=="kitchen_cereals_2":
        rv.append((1920,1080))
        rv.append(("flora avatar events cereal floracereal_bg",0,0))
        rv.append(("flora avatar events cereal floracereal_n",358,2))
        rv.append(("flora avatar events cereal floracereal_head2",481,273))
        rv.append(("flora avatar events cereal floracereal_panty",543,869))
        rv.append(("flora avatar events cereal floracereal_bra",470,493))
        rv.append(("flora avatar events cereal floracereal_top",389,228))

      elif state=="FloraMastPose01_1":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Milk",606,835))
        rv.append(("flora avatar events shower_chili FloraMastPose_Body",408,5))
        rv.append(("flora avatar events shower_chili FloraMastPose01_1",739,124))
      elif state=="FloraMastPose01_2":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Milk",606,835))
        rv.append(("flora avatar events shower_chili FloraMastPose_Body",408,5))
        rv.append(("flora avatar events shower_chili FloraMastPose01_2",739,124))
      elif state=="FloraMastPose01_3":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Milk",606,835))
        rv.append(("flora avatar events shower_chili FloraMastPose_Body",408,5))
        rv.append(("flora avatar events shower_chili FloraMastPose01_3",737,124))
      elif state=="FloraShowerPoseA":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA",996,120))
      elif state=="FloraShowerPoseA1":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA2_Body",342,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseB",995,120))
      elif state=="FloraShowerPoseB":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseB_Body",342,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseB",995,120))
      elif state=="FloraShowerPoseC1":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseB",995,120))
        rv.append(("flora avatar events shower_chili FloraShowerPoseC1",524,296))
      elif state=="FloraShowerPoseC2":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Milk",606,835))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseB",995,120))
        rv.append(("flora avatar events shower_chili FloraShowerPoseC2",607,296))
      elif state=="FloraShowerPoseD_1":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseD",996,120))
        rv.append(("flora avatar events shower_chili FloraShowerPoseD_1_hand",849,493))
      elif state=="FloraShowerPoseD_2":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseD",996,120))
        rv.append(("flora avatar events shower_chili FloraShowerPoseD_2_hand",864,464))
      elif state=="FloraShowerPoseD_3":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseD",996,120))
        rv.append(("flora avatar events shower_chili FloraShowerPoseD_3_hand",895,427))
      elif state=="FloraShowerPoseE1":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseE1",996,124))
        rv.append(("flora avatar events shower_chili FloraShowerPoseE1_hand",536,314))
      elif state=="FloraShowerPoseE1b":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseE1b",996,124))
        rv.append(("flora avatar events shower_chili FloraShowerPoseE1b_hand",840,493))
      elif state.startswith("FloraShowerPoseA1_angry"):
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        if "_pepelepsi" in state:
          rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_angry",996,130))
      elif state.startswith("FloraShowerPoseA1_concerned"):
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        if "_pepelepsi" in state:
          rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_concerned",996,123))
      elif state.startswith("FloraShowerPoseA1_confident"):
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        if "_pepelepsi" in state:
          rv.append(("flora avatar events shower_chili FloraShowerPose_Coke",609,843))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_Body",156,0))
        rv.append(("flora avatar events shower_chili FloraShowerPoseA1_confident",996,116))
      elif state=="FloraMastPose01_4":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Milk",606,835))
        rv.append(("flora avatar events shower_chili FloraMastPose_Body",408,5))
        rv.append(("flora avatar events shower_chili FloraMastPose01_4",739,124))
        rv.append(("flora avatar events shower_chili FloraMastPose_squirt",841,466))
      elif state=="FloraMastPose01_4a":
        rv.append((1920,1080))
        rv.append(("flora avatar events shower_chili FloraShowerPoseBG",0,0))
        rv.append(("flora avatar events shower_chili FloraShowerPose_Milk",606,835))
        rv.append(("flora avatar events shower_chili FloraMastPose_Body",408,5))
        rv.append(("flora avatar events shower_chili FloraMastPose01_4",739,124))

      elif state=="FloraIsabelleKiss":
        rv.append((1920,1080))
        rv.append(("flora avatar events isakiss floraisakiss_n",0,0))
        rv.append(("flora avatar events isakiss floraisakiss_undies",188,835))
        rv.append(("flora avatar events isakiss floraisakiss_c",188,86))

      elif state.startswith("hugging_flora"):
        rv.append((1920,1080))
        rv.append(("flora avatar events bonsaihug huggingflora_bg",0,0))
        rv.append(("flora avatar events bonsaihug huggingflora_pjs",312,0))
        if "sad" in state:
          rv.append(("flora avatar events bonsaihug face_huggingflora_sad",806,410))
        if "openmouth" in state:
          rv.append(("flora avatar events bonsaihug face_huggingflora_openmouth",806,409))
        if "admiring" in state:
          rv.append(("flora avatar events bonsaihug face_huggingflora_admiring",728,289))
        rv.append(("flora avatar events bonsaihug huggingflora_blush",675,307))
        if "bandaid" in state:
          rv.append(("flora avatar events bonsaihug huggingflora_bandage",657,276))

      elif state.startswith("bed") and not state.startswith("bedroom"):
        #Flora bed scene
        rv.append((1920,1080))
        rv.append(("flora avatar events bed florabed_bg",0,0))
        rv.append(("flora avatar events bed florabed_base",4,31))
        rv.append(("flora avatar events bed florabed_panty",1328,170))
        rv.append(("flora avatar events bed florabed_bra",569,261))
        rv.append(("flora avatar events bed florabed_blanket",1431,180))
        if "neutral" in state:
          rv.append(("flora avatar events bed florabed_neutral",241,198))
        elif "content" in state:
          rv.append(("flora avatar events bed florabed_content",241,198))
        elif "flirty" in state:
          rv.append(("flora avatar events bed florabed_flirty",250,189))
        elif "sad" in state:
          rv.append(("flora avatar events bed florabed_sad",234,204))
        elif "smile" in state:
          rv.append(("flora avatar events bed florabed_smile",240,197))
        rv.append(("flora avatar events bed florabed_bandage",260,167))
        rv.append(("flora avatar events bed florabed_l",52,125))
        if "hand" in state:
          rv.append(("flora avatar events bed florabed_mchand",101,700))
          rv.append(("flora avatar events bed florabed_florahand",187,676))
        else:
          rv.append(("flora avatar events bed florabed_r",199,638))

      elif state.startswith("bonsai_rape"):
        if school_nurse_room["vine"]:
          vcolor = school_nurse_room["vine"]
        else:
          vcolor = ""
        rv.append((1920,1080))
        rv.append(("flora avatar events bonsai bg",0,0))
        if "preview" in state:
          #preview scene
          rv.append(("flora avatar events bonsai flora_base",0,0))
          #rv.append(("flora avatar events bonsai shirt",0,0))
          rv.append(("flora avatar events bonsai flora_boob",0,0))
          if quest.flora_bonsai['a_vine_1']:
            rv.append(("flora avatar events bonsai flora_leftboob_vine"+vcolor,0,0))
          if quest.flora_bonsai['a_vine_2']:
            rv.append(("flora avatar events bonsai flora_pussy_vine_entrance"+vcolor,0,0))
          else:
            rv.append(("flora avatar events bonsai pussy",0,0))
          if quest.flora_bonsai['a_vine_3']:
            rv.append(("flora avatar events bonsai flora_rightboob_vine"+vcolor,0,0))
          if quest.flora_bonsai['a_vine_4']:
            rv.append(("flora avatar events bonsai flora_ass_vine"+vcolor,0,0))
          if quest.flora_bonsai['a_vine_5']:
            rv.append(("flora avatar events bonsai flora_mouth_vine"+vcolor,0,0))
          else:
            rv.append(("flora avatar events bonsai mouth",0,0))
          if not quest.flora_bonsai['r_vine_1']:
            rv.append(("flora avatar events bonsai flora_leftarm_vine"+vcolor,0,0))
          if not quest.flora_bonsai['r_vine_2']:
            rv.append(("flora avatar events bonsai flora_rightarm_vine"+vcolor,0,0))
          if not quest.flora_bonsai['r_vine_3']:
            rv.append(("flora avatar events bonsai flora_leftleg_vine"+vcolor,0,0))
          if not quest.flora_bonsai['r_vine_4']:
            rv.append(("flora avatar events bonsai flora_rightleg_vine"+vcolor,0,0))
          if not quest.flora_bonsai['r_vine_5']:
            rv.append(("flora avatar events bonsai flora_torso_vine"+vcolor,0,0))
        else: #end scene
          rv.append(("flora avatar events bonsai flora_base",0,0))
          #rv.append(("flora avatar events bonsai shirt",0,0))
          rv.append(("flora avatar events bonsai flora_boob",0,0))
          rv.append(("flora avatar events bonsai flora_torso_vine"+vcolor,0,0))
          rv.append(("flora avatar events bonsai flora_leftleg_vine"+vcolor,0,0))
          rv.append(("flora avatar events bonsai flora_rightleg_vine"+vcolor,0,0))
          rv.append(("flora avatar events bonsai flora_leftarm_vine"+vcolor,0,0))
          rv.append(("flora avatar events bonsai flora_rightarm_vine"+vcolor,0,0))
          #boobs
          rv.append(("flora avatar events bonsai flora_leftboob_vine"+vcolor,0,0))
          rv.append(("flora avatar events bonsai flora_rightboob_vine"+vcolor,0,0))
          if "left" in state:
            rv.append(("flora avatar events bonsai flora_leftboob_vine_squeeze"+vcolor,0,0))
          if "right" in state:
            rv.append(("flora avatar events bonsai flora_rightboob_vine_squeeze"+vcolor,0,0))
          #mouth
          if "mouth_cum" in state:
            rv.append(("flora avatar events bonsai flora_mouth_vine_bulging"+vcolor,0,0))
          else:
            rv.append(("flora avatar events bonsai flora_mouth_vine"+vcolor,0,0))
          #ass
          if "ass_cum" in state:
            rv.append(("flora avatar events bonsai flora_ass_climax"+vcolor,0,0))
          elif "ass" in state:
            rv.append(("flora avatar events bonsai flora_ass_vine_deep"+vcolor,0,0))
          else:
            rv.append(("flora avatar events bonsai flora_ass_vine"+vcolor,0,0))
          #pussy
          if "pussy_cum" in state:
            rv.append(("flora avatar events bonsai flora_pussy_vine_climax"+vcolor,0,0))
          elif "pussy_deep" in state:
            rv.append(("flora avatar events bonsai flora_pussy_vine_deep"+vcolor,0,0))
          elif "pussy" in state:
            rv.append(("flora avatar events bonsai flora_pussy_vine_in"+vcolor,0,0))
          else:
            rv.append(("flora avatar events bonsai flora_pussy_vine_entrance"+vcolor,0,0))

      elif state.startswith("vine_fight"):
        rv.append((1920,1080))
        rv.append(("flora avatar events vine_fight background",0,0))
        rv.append(("flora avatar events vine_fight flora_body",255,312))
        if "_face1" in state:
          rv.append(("flora avatar events vine_fight flora_face1",525,418))
        elif "_face2" in state:
          rv.append(("flora avatar events vine_fight flora_face2",519,419))
        elif "_face3" in state:
          rv.append(("flora avatar events vine_fight flora_face3",518,418))
        elif "_face4" in state:
          rv.append(("flora avatar events vine_fight flora_face4",518,419))
        if "_braless" not in state:
          rv.append(("flora avatar events vine_fight flora_bra",651,425))
        rv.append(("flora avatar events vine_fight flora_panties",1097,331))
        if "_cuthbert" in state:
          rv.append(("flora avatar events vine_fight water_drops",308,345))
        rv.append(("flora avatar events vine_fight grass",639,507))
        if "_cuthbert1" in state:
          rv.append(("flora avatar events vine_fight cuthbert1",1253,55))
          rv.append(("flora avatar events vine_fight water_splash",1139,0))
        if "_vine1" in state:
          rv.append(("flora avatar events vine_fight vine1_grabbing",521,0))
        if "_vine2" in state:
          rv.append(("flora avatar events vine_fight vine2_grabbing",954,0))
        if "_vine3" in state:
          rv.append(("flora avatar events vine_fight vine3_grabbing",1327,0))
        if "_vine4" in state:
          if "_cuthbert2" in state:
            rv.append(("flora avatar events vine_fight vine4_reaching",925,318))
          elif "_cuthbert3" in state:
            rv.append(("flora avatar events vine_fight vine4_chopped",923,483))
          elif "_cuthbert4" in state:
            rv.append(("flora avatar events vine_fight vine4_defeated",923,744))
          else:
            rv.append(("flora avatar events vine_fight vine4_grabbing",925,310))
        if "_cuthbert1" in state:
          pass
        elif "_cuthbert2" in state:
          rv.append(("flora avatar events vine_fight cuthbert2",1003,429))
        elif "_cuthbert3" in state:
          rv.append(("flora avatar events vine_fight cuthbert3",894,470))
        elif "_cuthbert4" in state:
          rv.append(("flora avatar events vine_fight cuthbert4",1233,291))
        rv.append(("flora avatar events vine_fight bushes",0,0))

      elif state.startswith("shower"):
        rv.append((1920,1080))
        if state.endswith(("hug","kiss")):
          rv.append(("flora avatar events shower_squid kiss upper_head background",0,0))
          rv.append(("flora avatar events shower_squid kiss upper_head water",0,16))
          if state.endswith("hug"):
            rv.append(("flora avatar events shower_squid kiss upper_head flora1",461,85))
            rv.append(("flora avatar events shower_squid kiss upper_head mc1",549,128))
          else:
            rv.append(("flora avatar events shower_squid kiss upper_head flora2",461,84))
            rv.append(("flora avatar events shower_squid kiss upper_head mc2",549,167))
          rv.append(("flora avatar events shower_squid kiss upper_head more_water",394,16))
        elif state.endswith(("blowjob","blurred")):
          if "blurred" in state:
            rv.append(("flora avatar events shower_squid kiss lower_head blurred",0,0))
          else:
            rv.append(("flora avatar events shower_squid kiss lower_head background",0,0))
            rv.append(("flora avatar events shower_squid kiss lower_head flora",627,59))
            rv.append(("flora avatar events shower_squid kiss lower_head mc",373,632))
            rv.append(("flora avatar events shower_squid kiss lower_head water_drops",453,80))
            rv.append(("flora avatar events shower_squid kiss lower_head water",0,0))
        else:
          rv.append(("flora avatar events shower_squid sex background",0,0))
          if state.endswith("anticipation"):
            rv.append(("flora avatar events shower_squid sex flora_body1",364,73))
            rv.append(("flora avatar events shower_squid sex flora_face1",1283,213))
            rv.append(("flora avatar events shower_squid sex flora_arms1",1002,388))
          elif state.endswith(("penetration1","penetration5")):
            rv.append(("flora avatar events shower_squid sex flora_body2",311,100))
            if state.endswith("penetration1"):
              rv.append(("flora avatar events shower_squid sex flora_face2",1272,230))
            elif state.endswith("penetration5"):
              rv.append(("flora avatar events shower_squid sex flora_face3",1272,230))
            if quest.flora_squid["shower"] == "hug":
              rv.append(("flora avatar events shower_squid sex flora_arms2",553,154))
              rv.append(("flora avatar events shower_squid sex mc_arms2",0,171))
            elif quest.flora_squid["shower"] == "ass":
              rv.append(("flora avatar events shower_squid sex flora_arms3",591,237))
              rv.append(("flora avatar events shower_squid sex mc_arms3",0,240))
          elif state.endswith(("penetration2","penetration6")):
            rv.append(("flora avatar events shower_squid sex flora_body3",323,66))
            if state.endswith("penetration2"):
              rv.append(("flora avatar events shower_squid sex flora_face4",1313,187))
            elif state.endswith("penetration6"):
              rv.append(("flora avatar events shower_squid sex flora_face5",1313,177))
            if quest.flora_squid["shower"] == "hug":
              rv.append(("flora avatar events shower_squid sex flora_arms4",567,137))
              rv.append(("flora avatar events shower_squid sex mc_arms4",0,147))
            elif quest.flora_squid["shower"] == "ass":
              rv.append(("flora avatar events shower_squid sex flora_arms5",568,212))
              rv.append(("flora avatar events shower_squid sex mc_arms5",0,222))
          elif state.endswith(("penetration3","penetration7")):
            rv.append(("flora avatar events shower_squid sex flora_body4",317,50))
            if state.endswith("penetration3"):
              rv.append(("flora avatar events shower_squid sex flora_face6",1342,158))
            elif state.endswith("penetration7"):
              rv.append(("flora avatar events shower_squid sex flora_face7",1343,155))
            if quest.flora_squid["shower"] == "hug":
              rv.append(("flora avatar events shower_squid sex flora_arms6",555,165))
              rv.append(("flora avatar events shower_squid sex mc_arms6",0,171))
            elif quest.flora_squid["shower"] == "ass":
              rv.append(("flora avatar events shower_squid sex flora_arms7",603,230))
              rv.append(("flora avatar events shower_squid sex mc_arms7",0,235))
          elif state.endswith(("penetration4","penetration8")):
            rv.append(("flora avatar events shower_squid sex flora_body5",383,34))
            if state.endswith("penetration4"):
              rv.append(("flora avatar events shower_squid sex flora_face8",1361,134))
            elif state.endswith("penetration8"):
              rv.append(("flora avatar events shower_squid sex flora_face9",1361,134))
            if quest.flora_squid["shower"] == "hug":
              rv.append(("flora avatar events shower_squid sex flora_arms8",562,175))
              rv.append(("flora avatar events shower_squid sex mc_arms8",0,182))
            elif quest.flora_squid["shower"] == "ass":
              rv.append(("flora avatar events shower_squid sex flora_arms9",619,239))
              rv.append(("flora avatar events shower_squid sex mc_arms9",0,238))
          elif state.endswith(("flora_cum1","flora_cum2")):
            rv.append(("flora avatar events shower_squid sex flora_body6",311,119))
            if state.endswith("flora_cum1"):
              rv.append(("flora avatar events shower_squid sex flora_face10",1364,166))
            elif state.endswith("flora_cum2"):
              rv.append(("flora avatar events shower_squid sex flora_face11",1325,166))
            if quest.flora_squid["shower"] == "hug":
              rv.append(("flora avatar events shower_squid sex flora_arms10",553,154))
              rv.append(("flora avatar events shower_squid sex mc_arms2",0,171))
            elif quest.flora_squid["shower"] == "ass":
              rv.append(("flora avatar events shower_squid sex flora_arms11",553,237))
              rv.append(("flora avatar events shower_squid sex mc_arms3",0,240))
          else:
            rv.append(("flora avatar events shower_squid sex flora_body7",364,109))
            rv.append(("flora avatar events shower_squid sex flora_face12",1306,279))
            rv.append(("flora avatar events shower_squid sex flora_arms12",878,611))
          if state.endswith(("anticipation","penetration1","penetration5","flora_cum1","flora_cum2","mc_cum1","mc_cum2","aftermath")):
            rv.append(("flora avatar events shower_squid sex mc_body1",0,371))
            if state.endswith(("anticipation","mc_cum1","mc_cum2","aftermath")):
              rv.append(("flora avatar events shower_squid sex mc_dick1",404,719))
              rv.append(("flora avatar events shower_squid sex mc_arms1",0,217))
              if state.endswith("mc_cum2"):
                rv.append(("flora avatar events shower_squid sex cum1",632,408))
              elif state.endswith("aftermath"):
                rv.append(("flora avatar events shower_squid sex cum2",661,445))
            else:
              rv.append(("flora avatar events shower_squid sex mc_dick2",415,804))
          elif state.endswith(("penetration2","penetration6")):
            rv.append(("flora avatar events shower_squid sex mc_body2",0,371))
            rv.append(("flora avatar events shower_squid sex mc_dick3",500,784))
          elif state.endswith(("penetration3","penetration7")):
            rv.append(("flora avatar events shower_squid sex mc_body3",0,377))
            rv.append(("flora avatar events shower_squid sex mc_dick4",542,773))
          elif state.endswith(("penetration4","penetration8")):
            rv.append(("flora avatar events shower_squid sex mc_body4",0,388))
            rv.append(("flora avatar events shower_squid sex mc_dick5",547,770))
          if state.endswith(("penetration2","penetration6")) and quest.flora_squid["shower"] == "ass":
            rv.append(("flora avatar events shower_squid sex penetration_fix",577,783))
          elif state.endswith(("flora_cum1","flora_cum2")) and quest.flora_squid["shower"] == "ass":
            rv.append(("flora avatar events shower_squid sex flora_cum_fix",565,803))
          rv.append(("flora avatar events shower_squid sex water",0,0))

      elif state.startswith("streaking"):
        rv.append((1920,1080))
        rv.append(("flora avatar events streaking background",0,0))
        if state.endswith("1"):
          rv.append(("flora avatar events streaking guard1",1229,381))
          rv.append(("flora avatar events streaking flora1",109,108))
        elif state.endswith("2"):
          rv.append(("flora avatar events streaking guard2",1231,386))
          rv.append(("flora avatar events streaking flora2",341,135))
        elif state.endswith("3"):
          rv.append(("flora avatar events streaking guard3",1049,262))
          rv.append(("flora avatar events streaking flora3",584,18))
        elif state.endswith("4"):
          rv.append(("flora avatar events streaking flora4",697,155))
          rv.append(("flora avatar events streaking guard4",898,365))

      elif state.startswith("car_wash"):
        rv.append((1920,1080))
        rv.append(("flora avatar events car_wash background",0,0))
        rv.append(("flora avatar events car_wash stool",1523,576))
        if "jacklyn" not in state:
          rv.append(("flora avatar events car_wash bucket",1503,511))
        if ("maxine" not in state or "maxine" not in quest.jo_washed["car_wash_volunteers"]) and "isabelle" not in state:
          rv.append(("flora avatar events car_wash sponges",1579,571))
        rv.append(("flora avatar events car_wash hose",0,867))
        if "new_cars" in state:
          rv.append(("flora avatar events car_wash cars2",376,353))
        else:
          rv.append(("flora avatar events car_wash cars1",376,353))
        if "wet" in state:
          rv.append(("flora avatar events car_wash water_puddles",176,465))
        if "kate" in state and isinstance(quest.jo_washed["car_wash_volunteers"],set) and "kate" in quest.jo_washed["car_wash_volunteers"]:
          rv.append(("flora avatar events car_wash kate_body",1515,312))
          rv.append(("flora avatar events car_wash kate_bikini",1701,361))
          if "hands_on_hips" in state:
            rv.append(("flora avatar events car_wash kate_arms1",1661,365))
          else:
            rv.append(("flora avatar events car_wash kate_arms2",1698,365))
          if "kate annoyed" in state:
            rv.append(("flora avatar events car_wash kate_face1",1706,312))
          elif "kate smile" in state:
            rv.append(("flora avatar events car_wash kate_face2",1706,312))
          rv.append(("flora avatar events car_wash kate_hair",1697,303))
        if "jacklyn" in state:
          if "wet" in state:
            rv.append(("flora avatar events car_wash jacklyn_body2",283,343))
          else:
            rv.append(("flora avatar events car_wash jacklyn_body1",283,343))
          rv.append(("flora avatar events car_wash jacklyn_bikini",582,354))
          if "jacklyn smile_left" in state:
            rv.append(("flora avatar events car_wash jacklyn_arms3",540,358))
            rv.append(("flora avatar events car_wash jacklyn_face5",607,292))
            rv.append(("flora avatar events car_wash jacklyn_hair2",595,278))
          elif "jacklyn annoyed" in state or "jacklyn smile" in state:
            if "wet" in state:
              rv.append(("flora avatar events car_wash jacklyn_arms2",509,358))
            else:
              rv.append(("flora avatar events car_wash jacklyn_arms1",509,358))
            if "jacklyn annoyed" in state:
              rv.append(("flora avatar events car_wash jacklyn_face3",610,291))
            elif "jacklyn smile" in state:
              rv.append(("flora avatar events car_wash jacklyn_face1",610,291))
            rv.append(("flora avatar events car_wash jacklyn_hair1",601,279))
        if "isabelle" in state:
          if "wet" in state:
            rv.append(("flora avatar events car_wash isabelle_body2",508,538))
          else:
            rv.append(("flora avatar events car_wash isabelle_body1",508,538))
          rv.append(("flora avatar events car_wash isabelle_bikini",626,691))
          if "isabelle smile" in state:
            rv.append(("flora avatar events car_wash isabelle_face1",722,575))
          elif "isabelle surprised" in state:
            rv.append(("flora avatar events car_wash isabelle_face2",727,574))
          rv.append(("flora avatar events car_wash isabelle_glasses",720,541))
        if "maxine" in state and isinstance(quest.jo_washed["car_wash_volunteers"],set) and "maxine" in quest.jo_washed["car_wash_volunteers"]:
          if "wet" in state:
            rv.append(("flora avatar events car_wash maxine_body2",1263,319))
          else:
            rv.append(("flora avatar events car_wash maxine_body1",1263,319))
          rv.append(("flora avatar events car_wash maxine_bikini",1420,437))
          if "maxine surprised" in state:
            rv.append(("flora avatar events car_wash maxine_face2",1387,353))
          elif "maxine smile" in state:
            rv.append(("flora avatar events car_wash maxine_face1",1388,356))
          rv.append(("flora avatar events car_wash maxine_glasses",1384,369))
        if "flora" in state:
          if "flora hose" in state:
            rv.append(("flora avatar events car_wash flora_body3",115,231))
            rv.append(("flora avatar events car_wash flora_bikini2",260,452))
          else:
            if "wet" in state:
              rv.append(("flora avatar events car_wash flora_body2",0,360))
            else:
              rv.append(("flora avatar events car_wash flora_body1",0,360))
            rv.append(("flora avatar events car_wash flora_bikini1",169,385))
            if "flora surprised_left" in state or "flora smile_left" in state:
              rv.append(("flora avatar events car_wash flora_arms3",0,391))
            else:
              if "wet" in state:
                rv.append(("flora avatar events car_wash flora_arms2",0,44))
              else:
                rv.append(("flora avatar events car_wash flora_arms1",0,44))
            if "flora surprised_left" in state or "flora smile_left" in state:
              if "flora surprised_left" in state:
                rv.append(("flora avatar events car_wash flora_face12",216,297))
              elif "flora smile_left" in state:
                rv.append(("flora avatar events car_wash flora_face11",216,297))
              rv.append(("flora avatar events car_wash flora_hair2",169,263))
            else:
              if "flora excited_right" in state:
                rv.append(("flora avatar events car_wash flora_face5",190,287))
              elif "flora surprised" in state:
                rv.append(("flora avatar events car_wash flora_face6",190,287))
              elif "flora smile" in state:
                rv.append(("flora avatar events car_wash flora_face3",190,287))
              elif "flora embarrassed_down" in state:
                rv.append(("flora avatar events car_wash flora_face8",190,287))
              elif "flora embarrassed" in state:
                rv.append(("flora avatar events car_wash flora_face7",190,287))
              elif "flora annoyed" in state:
                rv.append(("flora avatar events car_wash flora_face9",190,287))
              elif "flora cringe" in state:
                rv.append(("flora avatar events car_wash flora_face10",190,287))
              elif "flora neutral" in state:
                rv.append(("flora avatar events car_wash flora_face1",190,287))
              rv.append(("flora avatar events car_wash flora_hair1",171,261))
        if "jo" in state:
          rv.append(("flora avatar events car_wash jo_body",556,265))
          rv.append(("flora avatar events car_wash jo_bikini",983,397))
          if "jo excited" in state:
            rv.append(("flora avatar events car_wash jo_face",1019,294))
          rv.append(("flora avatar events car_wash jo_glasses",1020,324))
        if "hose_anticipation" in state:
          rv.append(("flora avatar events car_wash mc_hand1",1117,863))
        elif "hose_spraying" in state:
          rv.append(("flora avatar events car_wash mc_hand2",883,407))
        elif "hose_everyhwere" in state:
          rv.append(("flora avatar events car_wash mc_hand5",167,257))
        elif "hose_dripping" in state:
          rv.append(("flora avatar events car_wash mc_hand6",1105,856))
        elif "hose_flora_mouth" in state:
          rv.append(("flora avatar events car_wash mc_hand4",187,334))

      elif state.startswith("bedroom_hug"):
        rv.append((1920,1080))
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(("flora avatar events bedroom_hug background_alt",0,0))
        else:
          rv.append(("flora avatar events bedroom_hug background",0,0))
        rv.append(("flora avatar events bedroom_hug mc_body",547,307))
        if state.endswith(("laughing","blush","concerned","surprised")):
          rv.append(("flora avatar events bedroom_hug mc_head2",654,0))
          rv.append(("flora avatar events bedroom_hug flora_body2",926,53))
          rv.append(("flora avatar events bedroom_hug flora_underwear2",1039,486))
          rv.append(("flora avatar events bedroom_hug flora_clothes2",1031,471))
          if state.endswith("laughing"):
            rv.append(("flora avatar events bedroom_hug flora_face1",1116,161))
          elif state.endswith("blush"):
            rv.append(("flora avatar events bedroom_hug flora_face2",1087,161))
          elif state.endswith("concerned"):
            rv.append(("flora avatar events bedroom_hug flora_face3",1087,163))
          elif state.endswith("surprised"):
            rv.append(("flora avatar events bedroom_hug flora_face4",1087,158))
        elif state.endswith("kiss"):
          rv.append(("flora avatar events bedroom_hug flora_body3",926,86))
          rv.append(("flora avatar events bedroom_hug flora_underwear3",1034,516))
          rv.append(("flora avatar events bedroom_hug flora_clothes3",1033,502))
          rv.append(("flora avatar events bedroom_hug mc_head3",675,0))
        else:
          rv.append(("flora avatar events bedroom_hug mc_head1",697,0))
          rv.append(("flora avatar events bedroom_hug flora_body1",777,139))
          rv.append(("flora avatar events bedroom_hug flora_underwear1",985,576))
          rv.append(("flora avatar events bedroom_hug flora_clothes1",979,519))

      elif state.startswith("bedroom_sex"):
        rv.append((1920,1080))
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(("flora avatar events bedroom_sex background_alt",0,0))
        else:
          rv.append(("flora avatar events bedroom_sex background",0,0))
        if state.endswith(("anticipation","insertion","aftermath")):
          rv.append(("flora avatar events bedroom_sex flora_body1",226,299))
        elif state.endswith("penetration1"):
          rv.append(("flora avatar events bedroom_sex flora_body2",282,295))
        elif state.endswith(("penetration2","penetration3","penetration4")):
          rv.append(("flora avatar events bedroom_sex flora_body3",338,287))
        elif state.endswith("cum"):
          rv.append(("flora avatar events bedroom_sex flora_body4",376,282))
        if state.endswith(("anticipation","insertion","aftermath")):
          rv.append(("flora avatar events bedroom_sex flora_boobs1",868,320))
        elif state.endswith("penetration1"):
          rv.append(("flora avatar events bedroom_sex flora_boobs2",892,317))
        elif state.endswith(("penetration2","penetration3","penetration4")):
          rv.append(("flora avatar events bedroom_sex flora_boobs3",938,309))
        elif state.endswith("cum"):
          rv.append(("flora avatar events bedroom_sex flora_boobs4",924,317))
        if state.endswith(("anticipation","aftermath")):
          rv.append(("flora avatar events bedroom_sex flora_head1",1060,40))
          if state.endswith("anticipation"):
            rv.append(("flora avatar events bedroom_sex flora_face1",1377,199))
          elif state.endswith("aftermath"):
            rv.append(("flora avatar events bedroom_sex flora_face2",1363,172))
        elif state.endswith("insertion"):
          rv.append(("flora avatar events bedroom_sex flora_head2",1054,71))
          rv.append(("flora avatar events bedroom_sex flora_face3",1411,155))
        elif state.endswith("penetration1"):
          rv.append(("flora avatar events bedroom_sex flora_head3",1054,49))
          if "wrapped_legs" in state:
            rv.append(("flora avatar events bedroom_sex flora_face6",1329,114))
          elif "wrapped_arms" in state:
            rv.append(("flora avatar events bedroom_sex flora_face5",1327,160))
          else:
            rv.append(("flora avatar events bedroom_sex flora_face4",1338,164))
        elif state.endswith(("penetration2","penetration3","penetration4")):
          rv.append(("flora avatar events bedroom_sex flora_head4",1056,33))
          if "wrapped_legs" in state:
            rv.append(("flora avatar events bedroom_sex flora_face9",1343,115))
          elif "wrapped_arms" in state:
            rv.append(("flora avatar events bedroom_sex flora_face8",1343,151))
          else:
            rv.append(("flora avatar events bedroom_sex flora_face7",1352,160))
        elif state.endswith("cum"):
          rv.append(("flora avatar events bedroom_sex flora_head5",1058,72))
          rv.append(("flora avatar events bedroom_sex flora_face10",1405,135))
        if state.endswith(("anticipation","aftermath")):
          rv.append(("flora avatar events bedroom_sex flora_arms1",797,0))
        elif state.endswith("insertion"):
          rv.append(("flora avatar events bedroom_sex flora_arms2",797,0))
        elif state.endswith("penetration1"):
          if "wrapped_arms" in state:
            rv.append(("flora avatar events bedroom_sex flora_arms4",1081,0))
          else:
            rv.append(("flora avatar events bedroom_sex flora_arms3",794,0))
        elif state.endswith(("penetration2","penetration3","penetration4")):
          if "wrapped_arms" in state:
            rv.append(("flora avatar events bedroom_sex flora_arms6",1105,0))
          else:
            rv.append(("flora avatar events bedroom_sex flora_arms5",780,0))
        elif state.endswith("cum"):
          rv.append(("flora avatar events bedroom_sex flora_arms7",1110,0))
        if state.endswith(("anticipation","insertion","aftermath")):
          rv.append(("flora avatar events bedroom_sex flora_legs1",163,0))
        elif state.endswith("penetration1"):
          if "wrapped_legs" in state:
            rv.append(("flora avatar events bedroom_sex flora_legs3",0,0))
          else:
            rv.append(("flora avatar events bedroom_sex flora_legs2",186,0))
        elif state.endswith(("penetration2","penetration3","penetration4")):
          if "wrapped_legs" in state:
            rv.append(("flora avatar events bedroom_sex flora_legs5",0,0))
          else:
            rv.append(("flora avatar events bedroom_sex flora_legs4",264,0))
        elif state.endswith("cum"):
          rv.append(("flora avatar events bedroom_sex flora_legs6",0,0))
        if state.endswith("anticipation"):
          rv.append(("flora avatar events bedroom_sex genitals1",0,509))
        elif state.endswith("insertion"):
          rv.append(("flora avatar events bedroom_sex genitals2",0,582))
        elif state.endswith("penetration1"):
          rv.append(("flora avatar events bedroom_sex genitals3",49,558))
        elif state.endswith("penetration2"):
          rv.append(("flora avatar events bedroom_sex genitals4",276,510))
        elif state.endswith("penetration3"):
          rv.append(("flora avatar events bedroom_sex genitals5",330,510))
        elif state.endswith("penetration4"):
          rv.append(("flora avatar events bedroom_sex genitals6",344,510))
        elif state.endswith("cum"):
          rv.append(("flora avatar events bedroom_sex genitals7",423,385))
        elif state.endswith("aftermath"):
          rv.append(("flora avatar events bedroom_sex genitals8",0,473))

      elif state.startswith("bedroom_aftermath"):
        rv.append((1920,1080))
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(("flora avatar events bedroom_aftermath background_alt",0,0))
        else:
          rv.append(("flora avatar events bedroom_aftermath background",0,0))
        if any(expression in state for expression in ("blush","tired")):
          rv.append(("flora avatar events bedroom_aftermath flora_body1",0,99))
          if "blush" in state:
            rv.append(("flora avatar events bedroom_aftermath flora_face1",1294,192))
          elif "tired" in state:
            rv.append(("flora avatar events bedroom_aftermath flora_face2",1208,190))
          if "cum" in state and "cumming" not in state:
            rv.append(("flora avatar events bedroom_aftermath cum1",752,453))
        elif "asleep" in state:
          rv.append(("flora avatar events bedroom_aftermath flora_body2",163,86))
          if "cum" in state and "cumming" not in state:
            rv.append(("flora avatar events bedroom_aftermath cum2",855,528))
        if "dick" in state:
          rv.append(("flora avatar events bedroom_aftermath mc_dick1",784,681))
        elif "cumming" in state:
          rv.append(("flora avatar events bedroom_aftermath mc_dick2",745,414))

      elif state.startswith("mushroom_kiss"):
        rv.append((1920,1080))
        rv.append(("flora avatar events mushroom_kiss background",0,0))
        rv.append(("flora avatar events mushroom_kiss mc_body",63,379))
        rv.append(("flora avatar events mushroom_kiss flora_body",686,295))
        if state.endswith(("kiss","blush","surprised")) and "bottomless" not in state:
          rv.append(("flora avatar events mushroom_kiss flora_panties",1368,462))
          rv.append(("flora avatar events mushroom_kiss flora_skirt",1341,453))
          if "topless" not in state:
            rv.append(("flora avatar events mushroom_kiss flora_bra",799,296))
            if state.endswith(("kiss","blush")):
              rv.append(("flora avatar events mushroom_kiss flora_blouse1",681,286))
            elif state.endswith("surprised"):
              rv.append(("flora avatar events mushroom_kiss flora_blouse2",681,286))
        if state.endswith("kiss"):
          rv.append(("flora avatar events mushroom_kiss flora_head1",246,0))
        else:
          rv.append(("flora avatar events mushroom_kiss flora_head2",328,0))
          if state.endswith("blush"):
            rv.append(("flora avatar events mushroom_kiss flora_face1",424,179))
          elif state.endswith("surprised"):
            rv.append(("flora avatar events mushroom_kiss flora_face2",424,178))
          elif state.endswith("afraid"):
            rv.append(("flora avatar events mushroom_kiss flora_face3",425,193))
          elif state.endswith("smile"):
            rv.append(("flora avatar events mushroom_kiss flora_face4",425,179))
        rv.append(("flora avatar events mushroom_kiss foreground",0,831))

      elif state.startswith("mushroom_anal_sex"):
        rv.append((1920,1080))
        rv.append(("flora avatar events mushroom_anal_sex background",0,0))
        if state.endswith(("fingering1","smearing_asshole","fingering2","smearing_dick","anticipation","penetration1","cum1","cum0","squirt")):
          rv.append(("flora avatar events mushroom_anal_sex flora_body1",611,362))
          rv.append(("flora avatar events mushroom_anal_sex flora_blouse1",607,345))
          if state.endswith(("fingering2","smearing_dick","anticipation","penetration1","cum1","cum0")):
            rv.append(("flora avatar events mushroom_anal_sex flora_right_arm3",667,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_boobs1",783,429))
          rv.append(("flora avatar events mushroom_anal_sex flora_legs1",65,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_panties1",172,474))
          rv.append(("flora avatar events mushroom_anal_sex flora_dress1",655,421))
        elif state.endswith(("penetration2","cum2")):
          rv.append(("flora avatar events mushroom_anal_sex flora_body2",610,362))
          rv.append(("flora avatar events mushroom_anal_sex flora_blouse2",606,346))
          rv.append(("flora avatar events mushroom_anal_sex flora_right_arm4",669,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_boobs2",798,418))
          rv.append(("flora avatar events mushroom_anal_sex flora_head3",1040,62))
          if "pain" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face4",1334,187))
          elif "angry" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face5",1331,187))
          elif "pleasure" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face6",1333,187))
          elif "ecstasy" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face7",1338,187))
          elif "orgasm" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face8",1327,187))
          elif "disappointed" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face9",1338,187))
          rv.append(("flora avatar events mushroom_anal_sex flora_legs2",94,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_panties2",201,451))
          rv.append(("flora avatar events mushroom_anal_sex flora_dress2",684,384))
        elif state.endswith(("penetration3","cum3")):
          rv.append(("flora avatar events mushroom_anal_sex flora_body3",617,356))
          rv.append(("flora avatar events mushroom_anal_sex flora_blouse3",613,340))
          rv.append(("flora avatar events mushroom_anal_sex flora_right_arm5",668,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_boobs3",810,405))
          rv.append(("flora avatar events mushroom_anal_sex flora_head4",1041,58))
          if "pain" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face4",1340,183))
          elif "angry" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face5",1337,183))
          elif "pleasure" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face6",1339,183))
          elif "ecstasy" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face7",1344,183))
          elif "orgasm" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face8",1333,183))
          elif "disappointed" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face9",1344,183))
          rv.append(("flora avatar events mushroom_anal_sex flora_legs3",116,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_panties3",223,432))
          rv.append(("flora avatar events mushroom_anal_sex flora_dress3",706,366))
        elif state.endswith(("penetration4","cum4")):
          rv.append(("flora avatar events mushroom_anal_sex flora_body4",621,354))
          rv.append(("flora avatar events mushroom_anal_sex flora_blouse4",617,338))
          rv.append(("flora avatar events mushroom_anal_sex flora_right_arm6",667,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_boobs4",811,404))
          rv.append(("flora avatar events mushroom_anal_sex flora_head5",1046,56))
          if "pain" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face4",1348,181))
          elif "angry" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face5",1345,181))
          elif "pleasure" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face6",1347,181))
          elif "ecstasy" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face7",1352,181))
          elif "orgasm" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face8",1341,181))
          elif "disappointed" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face9",1352,181))
          rv.append(("flora avatar events mushroom_anal_sex flora_legs4",129,0))
          rv.append(("flora avatar events mushroom_anal_sex flora_panties4",233,421))
          rv.append(("flora avatar events mushroom_anal_sex flora_dress4",716,351))
        if state.endswith(("fingering1","squirt")):
          rv.append(("flora avatar events mushroom_anal_sex flora_right_arm1",237,356))
        elif state.endswith("smearing_asshole"):
          rv.append(("flora avatar events mushroom_anal_sex flora_right_arm2",46,346))
        if state.endswith(("fingering1","smearing_asshole","fingering2","smearing_dick","anticipation")):
          rv.append(("flora avatar events mushroom_anal_sex flora_head1",1040,60))
          if state.endswith("fingering1"):
            rv.append(("flora avatar events mushroom_anal_sex flora_face1",1256,236))
          elif state.endswith("smearing_asshole"):
            rv.append(("flora avatar events mushroom_anal_sex flora_face2",1256,236))
          elif state.endswith(("fingering2","smearing_dick","anticipation")):
            rv.append(("flora avatar events mushroom_anal_sex flora_face3",1256,237))
        elif state.endswith(("penetration1","cum1","cum0","squirt")):
          rv.append(("flora avatar events mushroom_anal_sex flora_head2",1040,66))
          if "pain" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face4",1325,191))
          elif "angry" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face5",1322,191))
          elif "pleasure" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face6",1324,191))
          elif "ecstasy" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face7",1329,191))
          elif "orgasm" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face8",1318,191))
          elif "disappointed" in state:
            rv.append(("flora avatar events mushroom_anal_sex flora_face9",1329,191))
        if state.endswith("fingering2"):
          rv.append(("flora avatar events mushroom_anal_sex mc_hand",0,501))
        elif state.endswith("anticipation"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick1",0,676))
        elif state.endswith("penetration1"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick2",0,666))
        elif state.endswith("penetration2"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick3",0,642))
        elif state.endswith("penetration3"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick4",0,593))
        elif state.endswith("penetration4"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick5",0,610))
        elif state.endswith("cum4"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick6",0,610))
        elif state.endswith("cum3"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick7",0,593))
        elif state.endswith("cum2"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick8",0,642))
        elif state.endswith("cum1"):
          rv.append(("flora avatar events mushroom_anal_sex mc_dick9",0,664))
        elif state.endswith(("cum0","squirt")):
          rv.append(("flora avatar events mushroom_anal_sex oozing_cum",182,739))

      elif state.startswith("mushroom_sex"):
        rv.append((1920,1080))
        rv.append(("flora avatar events mushroom_sex background",0,0))
        if state.endswith(("line_up","penetration1","clit_rub1","orgasm1","aftermath_inside","cum_outside","aftermath_outside")):
          rv.append(("flora avatar events mushroom_sex flora_body1",82,139))
          rv.append(("flora avatar events mushroom_sex flora_boobs1",747,456))
          if state.endswith(("line_up","aftermath_inside","aftermath_outside")):
            rv.append(("flora avatar events mushroom_sex flora_head1",616,8))
            if state.endswith("line_up"):
              rv.append(("flora avatar events mushroom_sex flora_face1",877,157))
            elif state.endswith("aftermath_inside"):
              rv.append(("flora avatar events mushroom_sex flora_face10",877,157))
            elif state.endswith("aftermath_outside"):
              rv.append(("flora avatar events mushroom_sex flora_face11",877,153))
          elif state.endswith(("penetration1","clit_rub1","orgasm1","cum_outside")):
            rv.append(("flora avatar events mushroom_sex flora_head2",616,5))
            if state.endswith(("penetration1","clit_rub1")):
              rv.append(("flora avatar events mushroom_sex flora_face2",882,129))
            elif state.endswith(("orgasm1","cum_outside")):
              rv.append(("flora avatar events mushroom_sex flora_face6",882,129))
          rv.append(("flora avatar events mushroom_sex flora_legs1",0,0))
          if state.endswith(("line_up","cum_outside","aftermath_outside")):
            rv.append(("flora avatar events mushroom_sex flora_pussy1",1026,672))
          elif state.endswith(("penetration1","clit_rub1","orgasm1")):
            rv.append(("flora avatar events mushroom_sex flora_pussy2",1025,674))
          elif state.endswith("aftermath_inside"):
            rv.append(("flora avatar events mushroom_sex flora_pussy6",1026,672))
          if state.endswith("line_up"):
            rv.append(("flora avatar events mushroom_sex mc_dick1",1063,724))
          elif state.endswith(("penetration1","clit_rub1","orgasm1")):
            rv.append(("flora avatar events mushroom_sex mc_dick2",1018,844))
          elif state.endswith("aftermath_inside"):
            rv.append(("flora avatar events mushroom_sex mc_dick7",1064,724))
          elif state.endswith("cum_outside"):
            rv.append(("flora avatar events mushroom_sex mc_dick8",996,513))
          elif state.endswith("aftermath_outside"):
            rv.append(("flora avatar events mushroom_sex mc_dick9",949,612))
          if state.endswith("line_up"):
            rv.append(("flora avatar events mushroom_sex mc_hand1",518,419))
          elif state.endswith("clit_rub1"):
            rv.append(("flora avatar events mushroom_sex mc_hand2",527,447))
        elif state.endswith(("penetration2","clit_rub2","orgasm2")):
          rv.append(("flora avatar events mushroom_sex flora_body2",88,140))
          rv.append(("flora avatar events mushroom_sex flora_boobs2",752,460))
          rv.append(("flora avatar events mushroom_sex flora_head3",617,7))
          if state.endswith(("penetration2","clit_rub2")):
            rv.append(("flora avatar events mushroom_sex flora_face3",882,131))
          elif state.endswith("orgasm2"):
            rv.append(("flora avatar events mushroom_sex flora_face7",882,131))
          rv.append(("flora avatar events mushroom_sex flora_legs2",0,0))
          rv.append(("flora avatar events mushroom_sex flora_pussy3",1021,646))
          rv.append(("flora avatar events mushroom_sex mc_dick3",1008,815))
          if state.endswith("clit_rub2"):
            rv.append(("flora avatar events mushroom_sex mc_hand3",527,437))
        elif state.endswith(("penetration3","clit_rub3","orgasm3")):
          rv.append(("flora avatar events mushroom_sex flora_body3",91,135))
          rv.append(("flora avatar events mushroom_sex flora_boobs3",757,456))
          rv.append(("flora avatar events mushroom_sex flora_head4",616,10))
          if state.endswith(("penetration3","clit_rub3")):
            rv.append(("flora avatar events mushroom_sex flora_face4",883,131))
          elif state.endswith("orgasm3"):
            rv.append(("flora avatar events mushroom_sex flora_face8",883,131))
          rv.append(("flora avatar events mushroom_sex flora_legs3",0,0))
          rv.append(("flora avatar events mushroom_sex flora_pussy4",1025,632))
          rv.append(("flora avatar events mushroom_sex mc_dick4",1002,801))
          if state.endswith("clit_rub3"):
            rv.append(("flora avatar events mushroom_sex mc_hand4",531,427))
        elif state.endswith(("penetration4","clit_rub4","orgasm4","cum_inside")):
          rv.append(("flora avatar events mushroom_sex flora_body4",101,135))
          rv.append(("flora avatar events mushroom_sex flora_boobs4",769,448))
          rv.append(("flora avatar events mushroom_sex flora_head5",616,12))
          if state.endswith(("penetration4","clit_rub4")):
            rv.append(("flora avatar events mushroom_sex flora_face5",884,133))
          elif state.endswith(("orgasm4","cum_inside")):
            rv.append(("flora avatar events mushroom_sex flora_face9",884,133))
          rv.append(("flora avatar events mushroom_sex flora_legs4",0,0))
          rv.append(("flora avatar events mushroom_sex flora_pussy5",1024,618))
          if state.endswith(("penetration4","clit_rub4","orgasm4")):
            rv.append(("flora avatar events mushroom_sex mc_dick5",997,786))
          if state.endswith("clit_rub4"):
            rv.append(("flora avatar events mushroom_sex mc_hand5",529,416))
          elif state.endswith("cum_inside"):
            rv.append(("flora avatar events mushroom_sex mc_dick6",988,778))

      elif state.startswith("pond"):
        rv.append((1920,1080))
        rv.append(("flora avatar events pond background",0,0))
        if state.endswith(("mc_pulled","smile","embarrassed")):
          rv.append(("flora avatar events pond mc_body",773,134))
        rv.append(("flora avatar events pond flora_body",75,172))
        rv.append(("flora avatar events pond flora_underwear",553,373))
        rv.append(("flora avatar events pond flora_clothes",408,305))
        if state.endswith(("flora_pulled","mc_help","smile","embarrassed")):
          rv.append(("flora avatar events pond flora_left_arm1",711,327))
          rv.append(("flora avatar events pond flora_left_sleeve1",709,324))
        elif state.endswith("mc_pulled"):
          rv.append(("flora avatar events pond flora_left_arm2",589,311))
          rv.append(("flora avatar events pond flora_left_sleeve2",698,310))
        if state.endswith(("flora_pulled","embarrassed")):
          rv.append(("flora avatar events pond flora_face1",563,282))
        elif state.endswith("mc_help"):
          rv.append(("flora avatar events pond flora_face2",563,280))
        elif state.endswith(("mc_pulled","smile")):
          rv.append(("flora avatar events pond flora_face3",563,283))

      elif state.startswith("dnd_bandits"):
        rv.append((1920,1080))
        rv.append(("flora avatar events dnd_bandits background",0,0))
        rv.append(("flora avatar events dnd_bandits flora_body",0,263))
        if state.endswith("held_hostage"):
          rv.append(("flora avatar events dnd_bandits flora_face1",74,406))
        elif state.endswith("stabbed"):
          rv.append(("flora avatar events dnd_bandits flora_face2",78,408))
        rv.append(("flora avatar events dnd_bandits bandit_body",0,0))
        if state.endswith("stabbed"):
          rv.append(("flora avatar events dnd_bandits maya_body",99,0))
          rv.append(("flora avatar events dnd_bandits maya_clothes",1409,0))

      elif state.startswith("dnd_tentacles"):
        rv.append((1920,1080))
        rv.append(("flora avatar events dnd_tentacles background",0,0))
        rv.append(("flora avatar events dnd_tentacles flora",0,0))
        rv.append(("flora avatar events dnd_tentacles tentacles",0,0))

      elif state.startswith("dnd_kiss"):
        rv.append((1920,1080))
        rv.append(("flora avatar events dnd_kiss background",0,0))
        rv.append(("flora avatar events dnd_kiss maya_body",877,333))
        if state.endswith(("anticipation","stare")):
          rv.append(("flora avatar events dnd_kiss maya_left_arm1",1019,417))
        if state.endswith("anticipation"):
          rv.append(("flora avatar events dnd_kiss maya_head1",1017,107))
        elif state.endswith("stare"):
          rv.append(("flora avatar events dnd_kiss maya_head2",1013,112))
        elif state.endswith("kiss"):
          rv.append(("flora avatar events dnd_kiss maya_head3",997,119))
        rv.append(("flora avatar events dnd_kiss flora_body",279,285))
        if state.endswith("kiss"):
          rv.append(("flora avatar events dnd_kiss maya_left_arm2",765,325))
        if state.endswith("anticipation"):
          rv.append(("flora avatar events dnd_kiss flora_right_arm1",668,362))
        elif state.endswith(("stare","kiss")):
          rv.append(("flora avatar events dnd_kiss flora_right_arm2",685,254))
        if state.endswith("anticipation"):
          rv.append(("flora avatar events dnd_kiss flora_head1",653,59))
        elif state.endswith("stare"):
          rv.append(("flora avatar events dnd_kiss flora_head2",653,66))
        elif state.endswith("kiss"):
          rv.append(("flora avatar events dnd_kiss flora_head3",651,110))

      elif state.startswith("dnd_taste"):
        rv.append((1920,1080))
        rv.append(("flora avatar events dnd_taste background",0,0))
        if state.endswith(("kiss","stomach","aftermath","mayaless")):
          rv.append(("flora avatar events dnd_taste flora_body1",0,0))
        elif state.endswith("nipple"):
          rv.append(("flora avatar events dnd_taste flora_body2",0,0))
        elif state.endswith(("licking1","orgasm")):
          rv.append(("flora avatar events dnd_taste flora_body3",0,0))
        elif state.endswith("licking2"):
          rv.append(("flora avatar events dnd_taste flora_body4",0,0))
        elif state.endswith("licking3"):
          rv.append(("flora avatar events dnd_taste flora_body5",0,0))
        elif state.endswith("licking4"):
          rv.append(("flora avatar events dnd_taste flora_body6",0,0))
        if state.endswith("kiss"):
          rv.append(("flora avatar events dnd_taste flora_head1",1219,390))
        elif state.endswith(("nipple","stomach","aftermath")):
          rv.append(("flora avatar events dnd_taste flora_head2",1207,178))
        elif state.endswith(("licking1","licking2","licking3","licking4","mayaless")):
          rv.append(("flora avatar events dnd_taste flora_head3",1219,390))
        elif state.endswith("orgasm"):
          rv.append(("flora avatar events dnd_taste flora_head4",1232,411))
        if state.endswith("kiss"):
          rv.append(("flora avatar events dnd_taste maya_body1",38,0))
        elif state.endswith("nipple"):
          rv.append(("flora avatar events dnd_taste maya_body2",38,0))
        elif state.endswith("stomach"):
          rv.append(("flora avatar events dnd_taste maya_body3",38,48))
        elif state.endswith(("licking1","orgasm","aftermath")):
          rv.append(("flora avatar events dnd_taste maya_body4",0,9))
        elif state.endswith("licking2"):
          rv.append(("flora avatar events dnd_taste maya_body5",0,9))
        elif state.endswith("licking3"):
          rv.append(("flora avatar events dnd_taste maya_body6",0,9))
        elif state.endswith("licking4"):
          rv.append(("flora avatar events dnd_taste maya_body7",0,9))
        if state.endswith("licking1"):
          rv.append(("flora avatar events dnd_taste maya_head1",280,162))
        elif state.endswith("licking2"):
          rv.append(("flora avatar events dnd_taste maya_head2",268,150))
        elif state.endswith("licking3"):
          rv.append(("flora avatar events dnd_taste maya_head3",255,139))
        elif state.endswith("licking4"):
          rv.append(("flora avatar events dnd_taste maya_head4",247,131))
        elif state.endswith("orgasm"):
          rv.append(("flora avatar events dnd_taste maya_head5",280,162))
        elif state.endswith("aftermath"):
          rv.append(("flora avatar events dnd_taste maya_head6",255,139))
        if state.endswith(("licking1","orgasm","aftermath")):
          rv.append(("flora avatar events dnd_taste maya_right_hand1",502,489))
        elif state.endswith("licking2"):
          rv.append(("flora avatar events dnd_taste maya_right_hand2",503,501))
        elif state.endswith("licking3"):
          rv.append(("flora avatar events dnd_taste maya_right_hand3",503,515))
        elif state.endswith("licking4"):
          rv.append(("flora avatar events dnd_taste maya_right_hand4",502,504))

      elif state.startswith("dnd_scissor"):
        rv.append((1920,1080))
        rv.append(("flora avatar events dnd_scissor background",0,0))
        if state.endswith(("straddle","scissoring1","scissoring5","scissoring9","orgasm")):
          rv.append(("flora avatar events dnd_scissor flora_body1",516,180))
        elif state.endswith(("kiss","french_kiss")):
          rv.append(("flora avatar events dnd_scissor flora_body2",356,0))
        elif state.endswith(("scissoring2","scissoring6","scissoring10")):
          rv.append(("flora avatar events dnd_scissor flora_body3",510,143))
        elif state.endswith(("scissoring3","scissoring7","scissoring11")):
          rv.append(("flora avatar events dnd_scissor flora_body4",507,115))
        elif state.endswith(("scissoring4","scissoring8","scissoring12")):
          rv.append(("flora avatar events dnd_scissor flora_body5",510,87))
        if state.endswith(("straddle","scissoring1","scissoring5","scissoring9")):
          rv.append(("flora avatar events dnd_scissor flora_head1",1443,55))
        elif state.endswith(("scissoring2","scissoring6","scissoring10")):
          rv.append(("flora avatar events dnd_scissor flora_head2",1456,56))
        elif state.endswith(("scissoring3","scissoring7","scissoring11")):
          rv.append(("flora avatar events dnd_scissor flora_head3",1475,56))
        elif state.endswith(("scissoring4","scissoring8","scissoring12")):
          rv.append(("flora avatar events dnd_scissor flora_head4",1470,63))
        elif state.endswith("orgasm"):
          rv.append(("flora avatar events dnd_scissor flora_head5",1532,92))
        if state.endswith(("straddle","scissoring1")):
          rv.append(("flora avatar events dnd_scissor flora_face1",1478,169))
        elif state.endswith("kiss") and not state.endswith("french_kiss"):
          rv.append(("flora avatar events dnd_scissor flora_face2",1109,152))
        elif state.endswith("french_kiss"):
          rv.append(("flora avatar events dnd_scissor flora_face3",1096,152))
        elif state.endswith("scissoring2"):
          rv.append(("flora avatar events dnd_scissor flora_face4",1483,170))
        elif state.endswith("scissoring3"):
          rv.append(("flora avatar events dnd_scissor flora_face5",1501,165))
        elif state.endswith("scissoring4"):
          rv.append(("flora avatar events dnd_scissor flora_face6",1495,177))
        elif state.endswith("scissoring5"):
          rv.append(("flora avatar events dnd_scissor flora_face7",1478,169))
        elif state.endswith("scissoring6"):
          rv.append(("flora avatar events dnd_scissor flora_face8",1483,170))
        elif state.endswith("scissoring7"):
          rv.append(("flora avatar events dnd_scissor flora_face9",1502,166))
        elif state.endswith("scissoring8"):
          rv.append(("flora avatar events dnd_scissor flora_face10",1495,177))
        elif state.endswith("scissoring9"):
          rv.append(("flora avatar events dnd_scissor flora_face11",1477,167))
        elif state.endswith("scissoring10"):
          rv.append(("flora avatar events dnd_scissor flora_face12",1482,168))
        elif state.endswith("scissoring11"):
          rv.append(("flora avatar events dnd_scissor flora_face13",1500,164))
        elif state.endswith("scissoring12"):
          rv.append(("flora avatar events dnd_scissor flora_face14",1494,175))
        if state.endswith("straddle"):
          rv.append(("flora avatar events dnd_scissor maya_body1",0,237))
        elif state.endswith(("kiss","french_kiss")):
          rv.append(("flora avatar events dnd_scissor maya_body2",408,0))
        elif state.endswith(("scissoring1","scissoring5","scissoring9","orgasm")):
          rv.append(("flora avatar events dnd_scissor maya_body3",0,237))
        elif state.endswith(("scissoring2","scissoring6","scissoring10")):
          rv.append(("flora avatar events dnd_scissor maya_body4",0,236))
        elif state.endswith(("scissoring3","scissoring7","scissoring11")):
          rv.append(("flora avatar events dnd_scissor maya_body5",0,236))
        elif state.endswith(("scissoring4","scissoring8","scissoring12")):
          rv.append(("flora avatar events dnd_scissor maya_body6",0,236))
        if state.endswith("scissoring5"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm1",409,246))
        elif state.endswith("scissoring6"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm2",409,246))
        elif state.endswith("scissoring7"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm3",409,242))
        elif state.endswith("scissoring8"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm4",409,242))
        elif state.endswith(("scissoring9","orgasm")):
          rv.append(("flora avatar events dnd_scissor maya_left_arm5",409,259))
        elif state.endswith("scissoring10"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm6",409,259))
        elif state.endswith("scissoring11"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm7",409,258))
        elif state.endswith("scissoring12"):
          rv.append(("flora avatar events dnd_scissor maya_left_arm8",409,258))
        if state.endswith(("straddle","scissoring1","scissoring5","scissoring9")):
          rv.append(("flora avatar events dnd_scissor maya_head1",34,8))
        elif state.endswith(("scissoring2","scissoring6","scissoring10")):
          rv.append(("flora avatar events dnd_scissor maya_head2",37,7))
        elif state.endswith(("scissoring3","scissoring7","scissoring11")):
          rv.append(("flora avatar events dnd_scissor maya_head3",25,0))
        elif state.endswith(("scissoring4","scissoring8","scissoring12")):
          rv.append(("flora avatar events dnd_scissor maya_head4",41,5))
        elif state.endswith("orgasm"):
          rv.append(("flora avatar events dnd_scissor maya_head5",0,28))
        if state.endswith(("straddle","scissoring1")):
          rv.append(("flora avatar events dnd_scissor maya_face1",312,150))
        elif state.endswith("kiss") and not state.endswith("french_kiss"):
          rv.append(("flora avatar events dnd_scissor maya_face2",1006,153))
        elif state.endswith("french_kiss"):
          rv.append(("flora avatar events dnd_scissor maya_face3",952,149))
        elif state.endswith("scissoring2"):
          rv.append(("flora avatar events dnd_scissor maya_face4",316,155))
        elif state.endswith("scissoring3"):
          rv.append(("flora avatar events dnd_scissor maya_face5",303,145))
        elif state.endswith("scissoring4"):
          rv.append(("flora avatar events dnd_scissor maya_face6",323,154))
        elif state.endswith("scissoring5"):
          rv.append(("flora avatar events dnd_scissor maya_face7",313,150))
        elif state.endswith("scissoring6"):
          rv.append(("flora avatar events dnd_scissor maya_face8",316,154))
        elif state.endswith("scissoring7"):
          rv.append(("flora avatar events dnd_scissor maya_face9",303,144))
        elif state.endswith("scissoring8"):
          rv.append(("flora avatar events dnd_scissor maya_face10",323,154))
        elif state.endswith("scissoring9"):
          rv.append(("flora avatar events dnd_scissor maya_face11",313,149))
        elif state.endswith("scissoring10"):
          rv.append(("flora avatar events dnd_scissor maya_face12",316,153))
        elif state.endswith("scissoring11"):
          rv.append(("flora avatar events dnd_scissor maya_face13",303,143))
        elif state.endswith("scissoring12"):
          rv.append(("flora avatar events dnd_scissor maya_face14",323,153))

      elif state.startswith("bathroom_hug"):
        rv.append((1920,1080))
        rv.append(("flora avatar events bathroom_hug background",0,0))
        rv.append(("flora avatar events bathroom_hug flora_body",168,6))
        rv.append(("flora avatar events bathroom_hug flora_underwear",874,273))
        rv.append(("flora avatar events bathroom_hug flora_clothes",879,220))

      return rv


  class Interactable_flora(Interactable):

    def title(cls):
      return flora.name

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
      if not (quest.isabelle_haggis in ("instructions","puzzle","escape")
      or quest.flora_bonsai.in_progress
      or quest.flora_squid in ("bathroom","forest_glade")
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or quest.jo_washed.in_progress
      or quest.fall_in_newfall.in_progress
      or quest.flora_walk in ("xcube","forest_glade")
      or quest.maya_sauce in ("dinner","sauces")):
        if not flora.at("home_bathroom","shower"):
          if quest.back_to_school_special.in_progress:
            actions.append(["talk","Talk","?flora_talk_back_to_school_special"])
          elif flora["romance_disabled"]:
            actions.append(["talk","Talk","?flora_talk_romance_disabled"])
          elif quest.jacklyn_town == "done_challenge":
            actions.append(["talk","Talk","?flora_talk_challenge"])
          elif flora["talk_limit_today"]<3:
            actions.append(["talk","Talk","?flora_talk_one"])
            actions.append(["talk","Talk","?flora_talk_two"])
            actions.append(["talk","Talk","?flora_talk_three"])
            actions.append(["talk","Talk","?flora_talk_four"])
            actions.append(["talk","Talk","?flora_talk_five"])
          else:
            actions.append(["talk","Talk","?flora_talk_over"])

      #####Interactions that can happen at any time#######
      if mc["focus"]:
        if mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "got_liquid":
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_cooking_chilli|Give What?","flora_quest_flora_cooking_got_liquid"])
          if quest.flora_cooking_chilli == "chilli_recipe_step_one" and not quest.flora_cooking_chilli["drawers_interacted_with"]:
            actions.append(["quest","Quest","flora_quest_flora_cooking_chilli_chilli_recipe_step_one"])
          elif quest.flora_cooking_chilli=="check_with_flora":
            actions.append(["quest","Quest","flora_quest_flora_cooking_chilli_step_one_evaluate"])
          elif quest.flora_cooking_chilli == "try_again":
            actions.append(["quest","Quest","flora_quest_flora_cooking_try_again"])
          elif quest.flora_cooking_chilli == "return_phone":
            actions.append(["quest","Quest","flora_quest_flora_cooking_return_phone"])
          elif quest.flora_cooking_chilli == "get_milk":
            actions.append(["quest","Quest","flora_cooking_get_milk"])
        elif mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis in ("instructions","puzzle","escape"):
            if quest.isabelle_haggis == "instructions":
              actions.append(["quest","Quest","?isabelle_quest_haggis_detention_start"])
            elif quest.isabelle_haggis >= "puzzle":
              actions.append(["quest","Quest","isabelle_quest_haggis_flora_detention"])
            if mc.owned_item(("haggis_lunchbox","greasy_bolt")) and flora.at("school_homeroom"):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:isabelle_haggis,puzzle_flora|Give What?","isabelle_quest_haggis_give_flora"])
        elif mc["focus"] == "flora_bonsai":
          if quest.flora_bonsai == "flora":
            actions.append(["quest","Quest","quest_flora_bonsai_help_home"])
          elif quest.flora_bonsai == "refreshments":
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_bonsai,refreshments|Give What?","quest_flora_bonsai_gift"])
          elif quest.flora_bonsai == "chef":
            actions.append(["quest","Quest","quest_flora_bonsai_bake"])
          elif quest.flora_bonsai == "bake":
            actions.append(["quest","Quest","quest_flora_bonsai_bake_check"])
          elif quest.flora_bonsai == "check":
            actions.append(["quest","Quest","quest_flora_bonsai_bake_confirm"])
          elif quest.flora_bonsai == "sugar":
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_bonsai|Give What?","quest_flora_bonsai_sugar"])
          elif quest.flora_bonsai == "fetch":
            if quest.flora_bonsai['fetch'] == "sweets" and not quest.flora_bonsai['current_complete']:
              #if mc.owned_item(("lollipop","lollipop_2")):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_bonsai,sweets|Give What?","quest_flora_bonsai_fetch_sweets_gift"])
            elif quest.flora_bonsai['fetch'] == "drink" and not quest.flora_bonsai['current_complete']:
              #if mc.owned_item("water_bottle"):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_bonsai,drink|Give What?","quest_flora_bonsai_fetch_drink_gift"])
            elif quest.flora_bonsai['fetch'] == "bathroom" and not quest.flora_bonsai['current_complete']:
              actions.append(["quest","Quest","quest_flora_bonsai_fetch_bathroom_interact"])
            elif quest.flora_bonsai['fetch'] == "book" and not quest.flora_bonsai['current_complete']:
              #if mc.owned_item("catch_thirty_four"):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_bonsai,book|Give What?","quest_flora_bonsai_fetch_book_gift"])
            elif quest.flora_bonsai['fetch'] == "onions" and not quest.flora_bonsai['current_complete']:
              actions.append(["quest","Quest","quest_flora_bonsai_fetch_onions_interact"])
          elif quest.flora_bonsai == "vines":
            actions.append(["quest","Quest","quest_flora_bonsai_vines"])
          elif quest.flora_bonsai == "tinybigtree":
            actions.append("quest_flora_bonsai_attack")
        elif mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "florahelp":
            actions.append(["quest","Quest","?flora_quest_lindsey_wrong_florahelp"])
          elif quest.lindsey_wrong == "floralocker" and flora.at("school_first_hall_east"):
            actions.append(["quest","Quest","?flora_quest_lindsey_wrong_floralocker"])
        elif mc["focus"] == "flora_squid":
          if quest.flora_squid == "bathroom":
            if quest.flora_squid["bathroom_interactables"] == 3:
              actions.append(["quest","Quest","quest_flora_squid_bathroom_flora2"])
            else:
              actions.append(["quest","Quest","?quest_flora_squid_bathroom_flora1"])
          elif quest.flora_squid == "forest_glade":
            actions.append(["quest","Quest","?quest_flora_squid_forest_glade_flora"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19:
            if "flora" not in quest.isabelle_dethroning["dinner_conversations"]:
              actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_flora"])
            if "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
              actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_flora"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed == "supplies":
            actions.append(["quest","Quest","quest_jo_washed_supplies_reminder"])
          elif quest.jo_washed == "car_wash":
            actions.append(["quest","Quest","quest_jo_washed_car_wash"])
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "flora_walk":
          if quest.flora_walk == "xcube":
            actions.append(["quest","Quest","quest_flora_walk_xcube"])
          elif quest.flora_walk == "caught":
            actions.append(["quest","Quest","quest_flora_walk_caught"])
          elif quest.flora_walk == "forest_glade":
            actions.append(["quest","Quest","quest_flora_walk_forest_glade"])
        elif mc["focus"] == "maya_sauce":
          if quest.maya_sauce == "breakfast":
            actions.append(["quest","Quest","quest_maya_sauce_breakfast"])
          elif quest.maya_sauce == "dinner":
            actions.append(["quest","Quest","quest_maya_sauce_dinner"])
      else:
          ##########flora_in_the_shower##############
          if game.location == "home_bathroom" and flora.activity == "shower" and (not quest.flora_cooking_chilli.in_progress or quest.flora_cooking_chilli <= "bring_pot"):
            actions.append("flora_showering_interact")

          ##########flora_cooking_chilli##############
          if quest.flora_cooking_chilli == "bring_pot" and game.location == "home_kitchen":
            actions.append(["quest","Quest","?flora_quest_flora_cooking_chilli_give_pot"])

          ##########flora kate_search##############
          if quest.kate_search_for_nurse.in_progress and not quest.kate_search_for_nurse["flora_seen"]:
            if not game.location.id.startswith("home_"):
              actions.append(["quest","Quest","?kate_quest_search_for_nurse_flora_talk"])

          ##############Isabelle Stolen################
          if quest.isabelle_stolen.in_progress:
            if quest.isabelle_stolen == "askmaxine" and mc.at("school_*"):
              actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_flora"])

          ######### jacklyn_statement ######
          if quest.jacklyn_statement.in_progress:

            if quest.jacklyn_statement == "remove" and not quest.jacklyn_statement["flora_removed"]:
              actions.append(["quest","Quest","?jacklyn_statement_remove_flora"])

            if quest.jacklyn_statement == "ingredients" and not home_kitchen["croutons_activated"]:
              actions.append(["quest","Quest","?jacklyn_statement_flora_ingredients"])

            if quest.jacklyn_statement == "chef":
              actions.append(["quest","Quest","?jacklyn_statement_flora_chef"])

          ############## Maxine Wine ##############
          if quest.maxine_wine in ("pins","florareward"):
            if quest.maxine_wine=="pins":
              actions.append(["quest","Quest","?quest_maxine_wine_flora_deal"])

            if quest.maxine_wine=="florareward" and not mc.at("home_bathroom"):
              actions.append(["quest","Quest","?quest_maxine_wine_flora_pin"])

          ############## mrsl table ##############
          if quest.mrsl_table == "flora" and mc.at("home_hall"):
            actions.append(["quest","Quest","quest_mrsl_table_flora"])

          ############ Isabelle Locker ############
          if quest.isabelle_locker == "interrogate" and not quest.isabelle_locker["flora_interrogated"] and not quest.isabelle_locker["might_have_an_idea"]:
            actions.append(["quest","Quest","quest_isabelle_locker_interrogate_flora"])

          ############## Flora Squid ##############
          if quest.flora_squid == "trail":
            actions.append(["quest","Quest","quest_flora_squid_trail_flora"])
          elif quest.flora_squid == "ready":
            actions.append(["quest","Quest","quest_flora_squid_ready"])

          ############ Flora Handcuffs ############
          if (quest.flora_handcuffs == "package" and mc.owned_item("cutie_harlot")
          and game.location in ("home_kitchen","home_hall")):
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:flora_handcuffs,cutie_harlot|Give What?","quest_flora_handcuffs_package"])

          ################# Jo Day ################
          if quest.jo_day == "flora":
            actions.append(["quest","Quest","quest_jo_day_flora"])

          ############### Kate Trick ##############
          if quest.kate_trick == "flora":
            actions.append(["quest","Quest","quest_kate_trick_flora"])
          elif quest.kate_trick == "park" and game.location == "school_park":
            actions.append(["quest","Quest","quest_kate_trick_park"])

          ########## Isabelle Dethroning ##########
          if quest.isabelle_dethroning == "invitations" and "flora" not in quest.isabelle_dethroning["invitations"]:
            actions.append(["quest","Quest","quest_isabelle_dethroning_invitations_flora"])

          ############ Fall in Newfall ############
          if quest.fall_in_newfall == "late" and "flora" not in quest.fall_in_newfall["assembly_conversations"]:
            actions.append(["quest","Quest","quest_fall_in_newfall_late_school_homeroom_flora"])

          ############ Jacklyn Romance ############
          if quest.jacklyn_romance == "flora":
            actions.append(["quest","Quest","quest_jacklyn_romance_flora"])

          ############### MrsL Bot ################
          if quest.mrsl_bot == "help":
            actions.append(["quest","Quest","quest_mrsl_bot_help"])

          ########## quest interactions for picking up new quests go down here ############
          ########## so they don't pop up while another quest is in_progress ##############

          if game.season == 1:
            if quest.back_to_school_special=="talk_to_flora":
              actions.append(["quest","Quest","flora_quest_back_to_school_special"])

            if quest.isabelle_tour > "art_class" and not quest.flora_jacklyn_introduction.started and not (quest.flora_cooking_chilli >= "bring_pot" and quest.flora_cooking_chilli <= "return_phone"):
              if game.location == "home_kitchen":
                actions.append(["quest","Quest","flora_quest_flora_jacklyn_introduction_start"])

            if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and not quest.flora_handcuffs.started:
              actions.append(["quest","Quest","?quest_flora_handcuffs_start"])

            if not quest.flora_cooking_chilli.started:
              if not game.location.id.startswith("home_"):
                actions.append(["quest","Quest","?flora_quest_flora_cooking_chilli_start"])

          elif game.season == 2:
            if (quest.flora_squid.actually_finished and quest.flora_squid["shower"] in ("hug","ass")) and quest.fall_in_newfall.finished and game.location in ("home_kitchen","home_hall") and not quest.flora_walk.started:
              actions.append(["quest","Quest","quest_flora_walk_start"])

            if quest.fall_in_newfall.finished and quest.maya_spell.finished and game.location in ("home_kitchen","home_hall") and not quest.maya_quixote.started:
              actions.append(["quest","Quest","quest_maya_quixote_start"])

      ############flirt interactions##########
      if not (quest.isabelle_haggis in ("instructions","puzzle","escape")
      or quest.flora_bonsai.in_progress
      or quest.flora_squid in ("bathroom","forest_glade")
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or quest.jo_washed.in_progress
      or quest.fall_in_newfall.in_progress
      or quest.flora_walk in ("xcube","forest_glade")
      or quest.maya_sauce in ("dinner","sauces")):
        if not flora.at("home_bathroom","shower"):
          if quest.back_to_school_special.in_progress:
            actions.append(["flirt","Flirt","?flora_flirt_back_to_school_special"])
          elif flora["romance_disabled"]:
            pass
          elif quest.jacklyn_town == "done_challenge":
            actions.append(["flirt","Flirt","?flora_flirt_challenge"])
          elif (flora["flirt_limit_today"]<3):
            actions.append(["flirt","Flirt","?flora_flirt_one"])
            actions.append(["flirt","Flirt","?flora_flirt_two"])
            actions.append(["flirt","Flirt","?flora_flirt_three"])
            actions.append(["flirt","Flirt","?flora_flirt_four"])
            actions.append(["flirt","Flirt","?flora_flirt_five"])
          else:
            actions.append(["flirt","Flirt","?flora_flirt_over"])



    #def circle(cls,actions,x,y):
    #  rv =super().circle(actions,x,y)
    #  if game.location == "school_cafeteria":
    #    rv.start_angle=328
    #    rv.angle_per_icon= 32
    #    rv.center=(0,-0)
    #  if game.location == "school_homeroom":  # iff more than 5 circles change 128 to 160
    #    rv.start_angle=-128
    #    rv.angle_per_icon= 32
    #    rv.center=(0,300)
    #  return rv



label flora_talk_romance_disabled:
  "I don't think [flora] wants to talk to me after what I did to her..."
  return

label flora_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label flora_flirt_over:
  "She must be sick of my poor attempts at flirting by now..."
  return

label flora_talk_one:
  show flora annoyed with Dissolve(.5)
  flora annoyed "I'm watching you, [mc]."
  flora annoyed "And just so we're clear, that does not mean you have to do a silly dance."
  mc "You mean like this?"
  flora embarrassed "..."
  mc "..."
  flora confused "You didn't even move!"
  $flora["talk_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_talk_two:
  show flora excited with Dissolve(.5)
  flora excited "[jo] said I'm getting a new terrarium for my ant farm this year!"
  flora neutral "It's too bad you're banned indefinitely from my room."
  $flora["talk_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_talk_three:
  show flora neutral with Dissolve(.5)
  flora neutral "Do you remember the time we stayed at a cottage up north?"
  mc "Yeah, I was sick for most of the trip..."
  flora sad "I really miss the endless forests and untouched wilderness."
  $flora["talk_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_talk_four:
  show flora sarcastic with Dissolve(.5)
  flora sarcastic "I saw that."
  mc "Saw what?"
  flora sarcastic "The way you looked at the TA."
  mc "I don't know what you're talking about."
  flora excited "It's fine, I get it."
  flora neutral "I also like her style. Do you think I could pull off a punk look?"
  $flora["talk_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_talk_five:
  show flora annoyed with Dissolve(.5)
  flora neutral "Did you know there's going to be an art competition this year?"
  flora excited "I'm totally going to make a Kraken sculpture!"
  $flora["talk_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_talk_six:
  show flora angry with Dissolve(.5)
  flora angry "Did you steal my squid comrade?"
  mc "I did not."
  flora angry "I've told you not to touch my plushies."
  mc "Isn't it in the kitchen above the stove?"
  flora annoyed "It better be."
  $flora["talk_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_flirt_one:
  show flora neutral with Dissolve(.5)
  flora neutral "What is my favorite food?"
  show flora neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Calamari.\"":
      show flora neutral at move_to(.5)
      mc "Calamari."
      if not flora["flirt_bonuses_chosen"]:
        $flora.lust-=1
      flora angry "I'll kill you."
    "\"French snails.\"":
      show flora neutral at move_to(.5)
      mc "French snails."
      if not flora["flirt_bonuses_chosen"]:
        $flora.love-=1
      flora cringe "Ew. No!"
    "\"Haggis.\"":
      show flora neutral at move_to(.5)
      mc "Haggis."
      if quest.isabelle_haggis>"trouble":
        if not flora["flirt_bonuses_chosen"]:
          $flora.love+=1
        flora blush "Yum!"
        mc "I thought it was chili?"
        flora annoyed "You can have more than one favorite!"
      else:
        flora thinking "What's that?"
    "\"Chili con carne!\"":
      show flora neutral at move_to(.5)
      mc "Chili con carne!"
      if not flora["flirt_bonuses_chosen"]:
        $flora.lust+=1
      flora blush "Mmm, yes!"
  $flora["flirt_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_flirt_two:
  show flora smile with Dissolve(.5)
  flora smile "Do you know what my favorite animals are?"
  show flora smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Ants!\"":
      show flora smile at move_to(.5)
      mc "Ants!"
      flora neutral "Close, but no!"
    "\"Giant squids.\"":
      show flora smile at move_to(.5)
      mc "Giant squids."
      if not flora["flirt_bonuses_chosen"]:
        $flora.lust+=1
      flora laughing "What? No! That's not true at all... heh!"
    "\"Cats.\"":
      show flora smile at move_to(.5)
      mc "Cats."
      flora neutral "They're cute, but no."
    "\"Badgers.\"":
      show flora smile at move_to(.5)
      mc "Badgers."
      flora neutral "I am a Hufflepuff, but no, not badgers."
  $flora["flirt_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_flirt_three:
  show flora neutral with Dissolve(.5)
  flora neutral "What kind of date do you think I'd like the most?"
  show flora neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"A dinner at a fancy restaurant.\"":
      show flora neutral at move_to(.5)
      mc "A dinner at a fancy restaurant."
      flora annoyed "I don't even own a fancy dress..."
    "\"A walk on a beach at sunset.\"":
      show flora neutral at move_to(.5)
      mc "A walk on a beach at sunset."
      flora confused "I mean, I do like the ocean a lot, but beaches are a bit gross."
    "\"A hike through an ancient forest.\"":
      show flora neutral at move_to(.5)
      mc "A hike through an ancient forest."
      if not flora["flirt_bonuses_chosen"]:
        $flora.lust+=1
      flora excited "Yes! I love traveling the unbeaten path!"
    "\"Video games and pizza at home.\"":
      show flora neutral at move_to(.5)
      mc "Video games and pizza at home."
      flora angry "No, that's your ideal date!"
  $flora["flirt_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_flirt_four:
  show flora annoyed with Dissolve(.5)
  flora annoyed "So, you're used to telling lies about me..."
  flora annoyed "Tell me something that's true."
  show flora annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You've banned me from your room indefinitely, but you secretly regret it.\"":
      show flora annoyed at move_to(.5)
      mc "You've banned me from your room indefinitely, but you secretly regret it."
      flora laughing "I told you to stop telling lies!"
    "\"You like watching me dance.\"":
      show flora annoyed at move_to(.5)
      mc "You like watching me dance."
      flora embarrassed "I don't think anyone on this planet likes your moves..."
    "\"You secretly wish you could dress more provocatively.\"":
      show flora annoyed at move_to(.5)
      mc "You secretly wish you could dress more provocatively."
      flora laughing "I don't know about that!"
      mc "I meant punk-provocative. Not slutty-provocative."
      if not flora["flirt_bonuses_chosen"]:
        $flora.love+=1
      flora neutral "Oh. Yeah... I'd like to try, at least."
    "\"You'd like to cuddle with me in a cottage up north.\"":
      show flora annoyed at move_to(.5)
      mc "You'd like to cuddle with me in a cottage up north."
      flora cringe "Gross."
  $flora["flirt_limit_today"]+=1
  hide flora with Dissolve(.5)
  return

label flora_flirt_five:
  show flora concerned with Dissolve(.5)
  flora concerned "Since you enjoy embarrassing me so much..."
  flora concerned "Tell me something about me that is wholesome but embarrassing."
  show flora concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You're eighteen and still have an army of plushies guarding your bed.\"":
      show flora concerned at move_to(.5)
      mc "You're eighteen and still have an army of plushies guarding your bed."
      if not flora["flirt_bonuses_chosen"]:
        $flora.love+=1
      flora laughing "I guess that's true."
    "\"You sometimes leave the door to the bathroom unlocked on purpose.\"":
      show flora concerned at move_to(.5)
      mc "You sometimes leave the door to the bathroom unlocked on purpose."
      flora angry "Even if that wasn't a complete lie, how is it wholesome in any way?"
      mc "Charity work is wholesome, [flora]."
    "\"You always give me a hard time because you secretly like me.\"":
      show flora concerned at move_to(.5)
      mc "You always give me a hard time because you secretly like me."
      flora eyeroll "Don't flatter yourself."
    "\"You act all prim and proper around [jo] to get her on my ass.\"":
      show flora concerned at move_to(.5)
      mc "You act all prim and proper around [jo] to get her on my ass."
      flora excited "That's just war strategy."
  $flora["flirt_limit_today"]+=1
  $flora["flirt_bonuses_chosen"] = True
  hide flora with Dissolve(.5)
  return

label flora_talk_challenge:
  show flora angry with Dissolve(.5)
  flora angry "Don't talk to me."
  window hide
  hide flora with Dissolve(.5)
  return

label flora_flirt_challenge:
  show flora angry with Dissolve(.5)
  flora angry "Don't talk to me."
  window hide
  hide flora with Dissolve(.5)
  return
