image flora_walk_blowjob = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-174,-102), Transform("flora shower_blowjob", size=(480,270))), "ui circle_mask"))
image isabelle_collar = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-224,-345),("isabelle neutral_collar")),"ui circle_mask"))


label quest_flora_walk_start:
  "After everything that happened before the break, I never had the chance to talk to [flora]."
  "About... the thing that happened in the shower."
  "She's been pretending like it never happened, and maybe she wants to keep it that way."
  "But I need to know what she's thinking and feeling."
  "And I guess I can't ignore my own urges either."
  mc "Hey, [flora]?"
  window hide
  show flora neutral with Dissolve(.5)
  window auto
  flora neutral "What?"
  mc "So, I've been thinking..."
  flora angry "Not again! You promised to stop!"
  mc "Very funny."
  flora confident "Thank you. One of my many talents."
  mc "You've been spending too much time with [maya]."
  mc "She's a bad influence on you."
  flora annoyed_texting "Hold on. Let me just text her you said that."
  mc "Seriously?"
  flora annoyed_texting "..."
  mc "You can't be serious."
  flora annoyed_texting "She says that you're right, and that she'll soon teach me how to properly eat—"
  flora excited "...err, nope! That was all!"
  mc "Sounds like she has a lot to teach."
  flora angry "That was unrelated!"
  mc "Totally."
  flora concerned "What did you want, anyway?"
  show flora concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"We need to talk about what happened.\"":
      show flora concerned at move_to(.5)
      mc "We need to talk about what happened."
      flora confused "You'll have to be a bit more specific than that."
      mc "You. Me. In the shower."
      if jo.location == game.location:
        flora cringe "Be quiet! [jo] is right there!"
      else:
        flora cringe "Be quiet! [jo] could hear it!"
      mc "My bad, but we really need to talk about it."
      flora cringe "What's there to talk about?"
      mc "Lots of things."
      mc "For example... err..."
      "Shit, I'm so bad at this."
      mc "Did you, um, like it?"
      flora blush "Maybe."
      mc "Wait, really?"
      flora thinking "What do you mean?"
      mc "Err, nothing!"
      flora concerned "You're being really annoying right now."
      mc "I'm not even doing anything!"
      flora concerned "You have that look in your face."
      mc "I don't know what you're talking about."
      "Smirk."
      flora angry "You!"
      window hide
      show flora angry:
        linear .25 zoom 1.5 yoffset 750
      pause 0.25
      show flora angry with Move((0,20), (0,-20), 0.10, bounce=True, repeat=True, delay=0.275):
        linear .25 zoom 1 yoffset 0
      window auto
      mc "Ouch!"
      flora angry "Deserved."
      mc "..."
      mc "Again, please."
      flora eyeroll "Shut up."
      mc "You hit like a girl."
      flora annoyed "I am a girl!"
      mc "Okay, dude. Chill."
      flora cringe "That's it! You're getting the pillow death tonight!"
      mc "With those? There are worse ways to go, I suppose."
      flora afraid "..."
      mc "You're thinking about it."
      flora embarrassed "No! I'm leaving!"
      mc "Wait! Stay!"
      flora skeptical "What?"
      mc "You're really cute."
      flora skeptical "..."
      mc "You know that, right?"
      flora skeptical "Mm-hmm. What else?"
      mc "You're funny and adorable, and... smart?"
      flora skeptical "Keep going."
      mc "Err, you're hardworking, and... cute!"
      flora skeptical "You already said that."
      mc "Well, how many words do you think I know?"
      $flora.lust+=1
      flora smile "Fair point."
#   "?quest.flora_squid['shower'] == 'ass'@|{image=flora_walk_blowjob}|\"I think there's still some ink that needs extracting.\"":
    "?quest.flora_squid['shower'] == 'ass'@|{image=flora_walk_blowjob}|\"I think there's still some\nink that needs extracting.\"":
      show flora concerned at move_to(.5)
      mc "I think there's still some ink that needs extracting."
      flora cringe "..."
      mc "Maybe you can take a look?"
      flora cringe "That's not funny."
      mc "Oh, come on. It's a little bit funny."
      flora annoyed "Keep making jokes like that and it'll never happen again."
      mc "Oh? So, it might happen again if I make fewer jokes?"
      flora annoyed "Not if you're being annoying."
      mc "Check. Anything else?"
      flora eyeroll "Yes. Stop pestering me about it."
      flora eyeroll "You're being really immature."
      "Ugh, she's right. That was pretty crude of me."
      mc "I'm sorry, [flora]."
      flora annoyed "Fine."
    "\"Do you want to go out some time?\"":
      show flora concerned at move_to(.5)
      mc "Do you want to go out some time?"
      flora thinking "Go out?"
      mc "On a date."
      flora worried "What are you talking about? We can't!"
      mc "Why not?"
      flora worried "You're messing with me, aren't you?"
      flora worried "What if someone sees us?"
#     mc "We won't do anything naughty in public, but we could still go on a date."
      mc "We won't do anything naughty in public, but we could still go on a date.{space=-65}"
      flora worried "What kind of date?"
      mc "Well, what do you like?"
      flora thinking "Hmm... I don't know..."
      flora blush "Maybe a really expensive restaurant?"
      mc "Err, how about the Prawn Star?"
      flora angry "That is not expensive!"
      mc "It looks pretty fancy to me."
      $flora.love-=1
      flora angry "You're an idiot."
      mc "I've been called worse."
      flora concerned "Probably deserved."
      mc "Probably true."
  mc "So, do you want to hang out?"
  flora thinking "Doing what?"
  mc "How about in my room? And maybe later I'll take you out?"
  if quest.maya_sauce["bedroom_taken_over"]:
    flora laughing "Don't you mean, [maya]'s room?"
    mc "I actually meant the hallway."
    flora blush "I want to play the xCube, though."
    flora blush "Can I play on it?"
    mc "Sure, I guess. As long as I can watch you."
  else:
    flora blush "Can I play on the xCube?"
    mc "Sure, I guess."
  flora laughing "Okay!"
  $mc["focus"] = "flora_walk"
  window hide
  if game.location == "home_kitchen":
    show flora laughing at disappear_to_left
  elif game.location == "home_hall":
    show flora laughing at disappear_to_right
  pause 0.5
  $quest.flora_walk.start()
  return

label quest_flora_walk_xcube_upon_entering:
  "Is it weird that this is everything I've ever wanted?"
  "Just the two of us, hanging out."
  "..."
  "I could watch [flora] for hours."
  "She's so cute when she's biting her lip and squinting her eyes in full concentration."
  "I just want to squeeze her, hold her, let the world fade away."
  "But that's the problem. I can't."
  if quest.maya_sauce["bedroom_taken_over"]:
    "Both [jo] and [maya] could walk in at any moment, forgetting to knock as usual."
    "If only I could take them away from all of this..."
  else:
    "[jo] could walk in at any moment, forgetting to knock as usual."
    "If only I could take her away from all of this..."
  $quest.flora_walk["hanging_out"] = True
  return

label quest_flora_walk_xcube_door:
  "[flora] will probably give me the pillow death if I walk out on her after asking to hang out."
  return

label quest_flora_walk_xcube:
  flora "This level is so stupid!"
  mc "You need to use the sword."
  flora "I am using the sword!"
  mc "The {i}other{/} sword..."
  window hide
  show flora annoyed with Dissolve(.5)
  window auto
  flora annoyed "Ugh! Why aren't there bombs so I can just blow them all up?"
  mc "It's a fantasy game."
  flora annoyed "Well, it's a bad fantasy."
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
#   "\"It's one of the most widely acclaimed games of our time.\"":
    "\"It's one of the most widely\nacclaimed games of our time.\"":
      show flora annoyed at move_to(.5)
      mc "It's one of the most widely acclaimed games of our time."
      flora sarcastic "Overrated."
      mc "What? You take that back!"
      flora annoyed "No, I'm allowed to have an opinion."
      mc "Well, just know that all the game critics would disagree with you."
      flora eyeroll "Don't you think I've read your old PC Gamer magazines?"
      flora eyeroll "The reviews are all a sham! \"10/10, it was okay.\""
      "Hmm... she does have a point there..."
    "\"I have another fantasy we could try...\"":
      show flora annoyed at move_to(.5)
      $mc.lust+=1
      mc "I have another fantasy we could try..."
      flora cringe "It's something gross, isn't it?"
      mc "Err, no! Of course not!"
      flora annoyed "Well, what is it?"
      mc "Okay, fine. It was something you'd probably find gross."
      flora eyeroll "Shocker."
    "\"Do you want to go to the park instead?\"":
      show flora annoyed at move_to(.5)
      mc "Do you want to go to the park instead?"
      flora confused "What are we going to do there?"
      mc "I don't know, get some fresh air? Touch grass? Maybe play with your squid?"
      "Smirk."
      flora skeptical "I don't know what you're thinking, but I don't like that look."
      mc "You don't like playing with your squid?"
      flora skeptical "I love playing with my squid."
      mc "Right, that's what I thought."
      mc "Would you let me play with it too?"
      flora smile "If you're gentle."
      mc "I'm nothing if not gentle."
      mc "I'll treat your squid with all the love and care it deserves."
      $flora.lust+=2
      flora smile "Okay! I'll go get [cuthbert]!"
      window hide
      show flora smile at disappear_to_left
      pause 1.0
      $quest.flora_walk.advance("park")
      return
  mc "Anyway, do you want to go outside?"
  flora thinking "Why?"
# mc "You've been glued to that screen for hours. Time to get some fresh air."
  mc "You've been glued to that screen for hours. Time to get some fresh air.{space=-55}"
  flora thinking "This is the most suspicious thing I've ever heard."
  mc "I just want to get out of the house, okay? I feel... watched."
  flora worried "I know what you mean."
  mc "You do?"
  flora worried "Yeah. [jo] is always keeping tabs on us."
  "Damn. She gets it."
  mc "Let's go outside, then?"
  flora blush "Okay! I'll just go get my squid. He needs a walk too."
  mc "Err, do squids really need to go on walks?"
  flora thinking "Of course. They have so many legs that need exercise."
  mc "..."
  mc "And it'll survive that?"
  flora laughing "I'll bring him in a bucket!"
  mc "If you say so. I'm no squid expert."
  flora blush "Don't worry, I do this all the time! [cuthbert] loves his walks."
  window hide
  show flora blush at disappear_to_left
  pause 1.0
  $quest.flora_walk.advance("park")
  return

label quest_flora_walk_park_upon_entering:
  scene black with Dissolve(.07)
  $game.location = "school_park"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  show flora
  with Dissolve(.5)
  "It's a mild autumn day. No harsh winds or biting cold."
  "Perfect weather for a walk in the park."
  window hide
  show flora smile_bucket at appear_from_right
  pause 0.5
  window auto
  flora smile_bucket "Here, [cuthbert]! Do you want to go for a swim?"
  cuthbert "Splash, splash!" with vpunch
  flora smile_bucket "He's excited!"
  mc "How can you tell?"
  flora smile_bucket "He's splashing!"
  mc "And what does he do when he's angry?"
  mc "Let me guess... splash?"
  flora skeptical_bucket "..."
  mc "But hey, as long as you understand him!"
  cuthbert "Splash, splash!" with vpunch
  flora embarrassed_bucket "Now you made him angry!"
  mc "Oh, well. That's too bad."
  flora sad_bucket "You should be nice to him. He saved me from those nasty vines."
  mc "What?! I did that!"
  flora smile_bucket "Not how I remember it!"
  mc "You're such a brat. Next time I'll just watch."
  flora smile_bucket "There will be no next time. [cuthbert] will protect me."
  flora smile_bucket "Come on, [cuthbert]! Let's go for a swim?"
  window hide
  $school_park["cuthbert"] = True
  hide flora with Dissolve(.5)
  window auto
  "[flora] doesn't seem to mind the weird looks we're getting."
  "She's as cool and unbothered as ever."
  play sound "<from 1.2 to 3.2>water_splash"
  flora "There you go!"
  "I've always found that alluring about her."
  "She never seems to care what people think."
  play sound "<to 3.2>water_splash"
  flora "Is the water nice? I bet it is!"
  "Except, of course, when it comes to the one thing I want the most..."
  "And that's understandable. We both know what the consequences would be if someone found out."
  window hide
  show flora laughing with Dissolve(.5)
  window auto
  flora laughing "Isn't he so adorable?"
  mc "Uh, yeah. Totally."
  flora blush "I love my squid!"
  mc "..."
  flora thinking "What?"
  show flora thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I really like you.\"":
      show flora thinking at move_to(.5)
      mc "I really like you."
      flora laughing "Let's change the subject!"
      mc "Why? I like talking about you."
      flora laughing "You're just doing it on purpose, aren't you?"
      mc "Doing what?"
      flora laughing "Making me embarrassed!"
      mc "I'm not!"
      mc "..."
      mc "It is pretty cute, though."
      $flora.love+=1
      flora blush "[mc]!"
      mc "Heh. Okay, okay."
      flora blush "Thank you."
    "\"Did you know that squids have a life expectancy of three to five years?\"":
      show flora thinking at move_to(.5)
#     mc "Did you know that squids have a life expectancy of three to five years?"
      mc "Did you know that squids have a life expectancy of three to five years?{space=-55}"
#     flora annoyed "Did you know that you have a life expectancy of three to five seconds if you keep this up?"
      flora annoyed "Did you know that you have a life expectancy of three to five seconds{space=-30}\nif you keep this up?"
      mc "I'm just saying, don't get too attached."
      $flora.love-=1
      flora cringe "Make that two seconds!"
      mc "Fine. I'm sorry."
      mc "I guess I'm nervous, and when I'm nervous I say stupid shit."
      flora annoyed "Maybe try keeping your mouth shut when that happens?"
      flora annoyed "Besides, why are you even nervous?"
      mc "I don't know. We don't get to spend a lot of alone time together."
      mc "It's just something about being in your presence, I guess."
      flora confused "What about it?"
      mc "Well, I'm just really enjoying it."
      flora annoyed "Try to enjoy it while being less weird, then."
      mc "Whatever you say."
      flora sarcastic "That's a little better already."
    "\"We should do this more often!\"":
      show flora thinking at move_to(.5)
      mc "We should do this more often!"
      flora worried "This has to be a ruse of some sort..."
      mc "What do you mean?"
      flora worried "I've never seen you this happy. Something's up."
      mc "I'm just enjoying myself, okay?"
      mc "The weather is nice. The woman is beautiful. Life is pretty sweet."
      "Much better than my old life."
      flora flirty "Tell me more about the woman."
      mc "Well, she's a bit of a brat..."
      flora laughing "Hey!"
      mc "...but that's part of her charm!"
      mc "And she's pretty damn charming!"
      $flora.lust+=1
      flora blush "Okay, I like where this is going."
      mc "I like where it's going with her, too."
      flora laughing "Hey!"
      mc "What?"
      flora blush "Where exactly do you think it's going?"
      mc "I can think of a few places..."
      flora blush "Okay, that's enough."
  mc "Anyway, I wanted to show you something."
  flora skeptical "It better not be something gross."
  mc "O ye, of little faith."
  flora skeptical "I know you."
  mc "Not well enough, clearly."
  mc "Come on, I'll show you."
  flora skeptical "Okay, but I'm warning you..."
  window hide
  show flora skeptical at disappear_to_left
  pause 0.5
  hide screen interface_hider
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "We follow a bike path into the forest — one that I've taken a million times before."
  "Of course, that was in a different life, and perhaps things aren't the same, but I just have a feeling..."
  flora skeptical "Where are we going, [mc]?"
  $game.location = "school_mushroom_spot"
  mc "It should be... right... here!"
  show black onlayer screens zorder 100
  pause 0.5
  show flora excited at appear_from_right
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  flora excited "Whoa!"
  mc "I thought you'd like it."
  flora excited "It's like a mushroom kingdom!"
  flora excited "How did you know about this spot?"
  "You bike down many odd paths while delivering pizzas..."
  mc "I just stumbled upon it."
  "...while going into the woods to hang myself."
  mc "It's pretty, right?"
  flora excited "It's amazing!"
  mc "I brought this blanket and some—"
  window hide
  show flora excited:
    linear 0.5 zoom 7.5 xoffset -200 yoffset 5000
  with None
  show flora mushroom_kiss at center
  show black onlayer screens zorder 100
  with Dissolve(.5)
  play sound "falling_thud"
  with vpunch
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "The force of her kiss knocks me off my feet."
  "Luckily, the soft moss catches my fall, and I catch her in my arms."
  "She giggles, of course, in her usual mischievous way."
  "It has always been one of my favorite sounds."
  window hide
  show flora mushroom_kiss blush with Dissolve(.5)
  window auto
  "But it's her blush that gets me."
  "It's a side of her that only I know."
  "And I want it to remain that way."
  window hide
  show flora mushroom_kiss with Dissolve(.5)
  window auto
  "Our lips meet again, and this far away from prying eyes, the lust runs wild."
  window hide
  show flora mushroom_kiss blush with Dissolve(.5)
  window auto
  flora mushroom_kiss blush "I want to... try something."
  menu(side="middle"):
    extend ""
    "Follow her lead":
      "[flora] has always been the adventurous one."
      "And it seems like that's the case in the bedroom, too."
      mc "What's that?"
      flora mushroom_kiss blush "I read about this in the Cosmololitan..."
      window hide
      show flora mushroom_anal_sex fingering1
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "In one fluid motion, she gets on her back and spreads her legs."
      "Her fingers dip into her soaked pussy, lewdly toying with the stringy moisture."
      "Coating her fingers until they shine and glisten."
      window hide
      show flora mushroom_anal_sex smearing_asshole with Dissolve(.5)
      window auto
      "She then slides her hand further down, smearing her sticky juices all over her rosebud."
      mc "Err, are you sure about this?"
      flora mushroom_anal_sex smearing_asshole "You're supposed to do it too..."
      mc "Do what?"
      flora mushroom_anal_sex smearing_asshole "Touch me!"
      mc "Jeez. So bossy."
      flora mushroom_anal_sex smearing_asshole "Do you want this or not?"
      mc "I don't even know what this is."
      flora mushroom_anal_sex smearing_asshole "You're such a bad liar!"
      flora mushroom_anal_sex smearing_asshole "Now touch me, and smear it over your... thing."
      mc "As you wish..."
      window hide
      show flora mushroom_anal_sex fingering2 with Dissolve(.5)
      window auto
      flora mushroom_anal_sex fingering2 "Mmmm!"
      "A shudder runs through her as my fingers touch her pussy."
      "It's so sensitive. So warm and soft."
      "Touching her like this feels so wrong, and yet... it's the best feeling ever."
#     "Her folds quiver beneath my touch. Her wetness sticks to my fingers."
      "Her folds quiver beneath my touch. Her wetness sticks to my fingers.{space=-25}"
      window hide
      show flora mushroom_anal_sex smearing_dick with Dissolve(.5)
      window auto
      "And soon, it coats the shaft of my dick as well."
      window hide
      show flora mushroom_anal_sex anticipation with Dissolve(.5)
      window auto
      "She flinches as the tip of my cock settles against her asshole."
      mc "Are you sure about this?"
      flora mushroom_anal_sex anticipation "Y-yes. It's supposed to feel really good."
#     "I'm not sure what article she has read, but I better be super gentle..."
      "I'm not sure what article she has read, but I better be super gentle...{space=-5}"
      window hide
      show flora mushroom_anal_sex pain penetration1 with Dissolve(.5)
      window auto
      "As the tip of my dick settles against her sphincter, she takes a big breath and pushes back."
      show flora mushroom_anal_sex angry penetration1 with dissolve2
      flora mushroom_anal_sex angry penetration1 "Ow, ow, ow!"
      mc "Sorry, I—"
      show flora mushroom_anal_sex pain penetration1 with dissolve2
      flora mushroom_anal_sex pain penetration1 "Keep going! It's supposed to hurt at first!"
      "Damn. She really wants this."
      window hide
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.5)
      window auto
      "As gently as I possibly can, I push myself deeper."
      "She's so tight, it almost hurts my dick."
      show flora mushroom_anal_sex angry penetration2 with dissolve2
      flora mushroom_anal_sex angry penetration2 "Aah! Careful, you idiot!"
      mc "Sorry! I'm trying to be gentle!"
      flora mushroom_anal_sex angry penetration2 "Fuck you! That really hurt!"
      mc "Should I stop?"
      show flora mushroom_anal_sex pain penetration2 with dissolve2
      flora mushroom_anal_sex pain penetration2 "N-no! Keep going!"
      mc "If you say so..."
      window hide
      show flora mushroom_anal_sex pain penetration3 with Dissolve(.5)
      window auto
      "Inch by painful inch, my dick sinks in deeper."
      "The muscles of her ass strains against my intrusion."
      "Her gasps grow shallow, beads of sweat roll on her forehead and my own."
      window hide
      show flora mushroom_anal_sex pain penetration4 with Dissolve(.5)
      window auto
      "She closes her eyes as I enter her fully."
      flora mushroom_anal_sex pain penetration4 "Oh, my god..."
      mc "Does it hurt much?"
      show flora mushroom_anal_sex angry penetration4 with dissolve2
      flora mushroom_anal_sex angry penetration4 "What the hell do you think?!"
      mc "Err, should I pull out?"
      show flora mushroom_anal_sex pain penetration4 with dissolve2
      flora mushroom_anal_sex pain penetration4 "No! Just... let me... adjust..."
      "Well, it's not like I have a problem with that. I could stay in her ass forever."
      "She's squeezing my dick with her anal muscles, and it feels like heaven."
      "And for a few moments, I just stay buried in her, trying not to move."
#     "If you've only watched porn, you can't fully appreciate the fine motor skills required to not hurt someone while putting your dick in their ass."
      "If you've only watched porn, you can't fully appreciate the fine motor skills required to not hurt someone while putting your\ndick in their ass."
      flora mushroom_anal_sex pain penetration4 "Okay... go ahead."
      window hide
      show flora mushroom_anal_sex pain penetration3 with Dissolve(.5)
      window auto
      "Carefully, out..."
      window hide
      show flora mushroom_anal_sex pain penetration4 with Dissolve(.5)
      window auto
      "...then in again."
      show flora mushroom_anal_sex pleasure penetration4 with dissolve2
      flora mushroom_anal_sex pleasure penetration4 "Ohhh!"
      "Oh? That didn't sound like a cry of pain."
      window hide
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.5)
      window auto
      "Out again. A bit more this time."
      window hide
      show flora mushroom_anal_sex pain penetration4 with Dissolve(.5)
      window auto
      flora mushroom_anal_sex pain penetration4 "Uhhh! Ow!"
      mc "Sorry!"
      window hide
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration4 with Dissolve(.25)
      pause 0.25
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration4 with Dissolve(.25)
      pause 0.25
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pain penetration4 with Dissolve(.25)
      window auto show
      show flora mushroom_anal_sex pleasure penetration4 with dissolve2
      flora mushroom_anal_sex pleasure penetration4 "Mmm, yes!"
      mc "Is it good?"
      flora mushroom_anal_sex pleasure penetration4 "Yes! K-keep going!"
      window hide
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
      pause 0.25
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
      pause 0.25
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
      pause 0.25
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
      show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
      window auto
      mc "Let me know if you want me to go faster or slower or harder, okay?"
      flora mushroom_anal_sex pleasure penetration4 "Yes!"
      mc "Yes, what?"
      flora mushroom_anal_sex pleasure penetration4 "Oh, my god!"
      $unlock_replay("flora_experimentation")
      menu(side="middle"):
        extend ""
        "Go faster":
          window hide
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          window auto
          flora mushroom_anal_sex pleasure penetration4 "Oooooooh!"
          "Goddamn! That almost made me cum inside her."
          window hide
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.125)
          window auto show
          $flora.lust+=2
          show flora mushroom_anal_sex ecstasy penetration4 with dissolve2
          flora mushroom_anal_sex ecstasy penetration4 "That... feels... so... good..."
          mc "You... have... no... idea..."
          window hide
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          window auto
          flora mushroom_anal_sex ecstasy penetration4 "Oh, fuck! Oh, fuck!"
          "In a rising crescendo of decadence, our cries of pleasure fill the tiny clearing."
          window hide
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          window auto
          "Nothing has ever felt this good."
          "And never this right... while being so wrong."
          window hide
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          window auto
#         "It's the perfect mix of forbidden and romantic, of loving and depraved."
          "It's the perfect mix of forbidden and romantic, of loving and depraved.{space=-50}"
          "We both need this. We'll be miserable if we can't have this."
          window hide
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex ecstasy penetration4 with Dissolve(.125)
          window auto
          mc "I... fucking... love... you...."
          show flora mushroom_anal_sex orgasm penetration4 with dissolve2
          flora mushroom_anal_sex orgasm penetration4 "Oh, my god..."
          window hide
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          pause 0.125
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration1 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration2 with Dissolve(.125)
          show flora mushroom_anal_sex orgasm penetration4 with Dissolve(.125)
          window auto
          "It's not the words I ever expected to come out of my mouth while having my dick buried in her ass... but it does something to her."
#         "Her stomach flexes, her hips lift and push against me, as the orgasm rips through her."
          "Her stomach flexes, her hips lift and push against me, as the orgasm{space=-20}\nrips through her."
          window hide
          show flora mushroom_anal_sex orgasm cum4 with hpunch
          window auto
          "And I, balls deep inside her ass, explode as well."
          "The world spins... and yet I feel at ease."
        "Go slower":
          "If there's one thing I know about [flora], it's that she always says the opposite of what she means."
          window hide
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with Dissolve(.25)
          window auto
#         "Agonizingly slow, my dick enters her ass, making her feel every inch."
          "Agonizingly slow, my dick enters her ass, making her feel every inch.{space=-15}"
          show flora mushroom_anal_sex disappointed penetration4 with dissolve2
          flora mushroom_anal_sex disappointed penetration4 "I said faster! Go faster!"
          window hide
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration4 with Dissolve(.25)
          pause 0.25
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration4 with Dissolve(.25)
          pause 0.25
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration4 with Dissolve(.25)
          pause 0.25
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex disappointed penetration4 with Dissolve(.25)
          window auto
          mc "I'm... out of breath..."
          mc "And I'm going to—"
          window hide
          show flora mushroom_anal_sex disappointed cum4 with hpunch
          window auto
          mc "F-fuck!"
          flora mushroom_anal_sex disappointed cum4 "Seriously?"
          mc "I'm sorry..."
          show flora mushroom_anal_sex angry cum4 with dissolve2
          flora mushroom_anal_sex angry cum4 "..."
          window hide
          show flora mushroom_anal_sex angry cum3 with Dissolve(.15)
          show flora mushroom_anal_sex angry cum2 with Dissolve(.075)
          show flora mushroom_anal_sex angry cum1 with Dissolve(.075)
          show flora mushroom_anal_sex angry cum0 with Dissolve(.5)
          show flora mushroom_anal_sex pleasure squirt with Dissolve(.5)
          window auto
          "She shoots me an angry look, before starting to rub herself."
          "Her fingers circle her clit with practiced efficiency."
#         "And it doesn't take long before she bucks and explodes in an orgasm."
          "And it doesn't take long before she bucks and explodes in an orgasm.{space=-40}"
          window hide
          show flora mushroom_anal_sex orgasm squirt with hpunch
          window auto
          flora mushroom_anal_sex orgasm squirt "Oooh!"
          "It's beautiful, it's sexy, and it's everything I've ever wanted."
          "And yet... I know I've let her down."
          window hide
          show flora worried
          show black onlayer screens zorder 100
          with Dissolve(3.0)
          pause 0.5
          hide black onlayer screens with Dissolve(.5)
          window auto
          flora worried "You seriously need to work on your stamina."
          mc "I'm sorry..."
          flora laughing "Did you really not do it on purpose?"
          mc "I wanted to enjoy the moment, okay?"
          $flora.love+=1
          $flora.lust+=1
          flora blush "I guess it was a good moment..."
          mc "Did you like it?"
          flora thinking "I mean, it wasn't bad. Just the ending wasn't perfect."
          flora thinking "I guess we have something to work on."
          mc "Right... I'll do better next time."
          window hide
          show flora thinking at disappear_to_right
          pause 0.5
          show black onlayer screens zorder 100 with Dissolve(.5)
          jump quest_flora_walk_park_upon_returning
        "Go harder":
          window hide
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          window auto show
          show flora mushroom_anal_sex pain penetration4 with dissolve2
          flora mushroom_anal_sex pain penetration4 "Owww!"
          mc "Sorry! I was trying to go faster."
          window hide
          show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pain penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pain penetration4 with hpunch
          window auto show
          show flora mushroom_anal_sex angry penetration4 with dissolve2
          flora mushroom_anal_sex angry penetration4 "Fuck you! That's too hard!"
          mc "I'm really sorry!"
          window hide
          show flora mushroom_anal_sex angry penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex angry penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex angry penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex angry penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex angry penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex angry penetration4 with hpunch
          window auto show
          show flora mushroom_anal_sex pleasure penetration4 with dissolve2
          flora mushroom_anal_sex pleasure penetration4 "Fffff... G-god..."
          mc "You feel so good, [flora]!"
          window hide
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex pleasure penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex pleasure penetration4 with hpunch
          window auto show
          show flora mushroom_anal_sex ecstasy penetration4 with dissolve2
          "The harder I go, the more her body responds."
          "Juices ooze out of her pussy, and leak down to her ass, providing me with additional lube."
          window hide
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration4 with hpunch
          pause 0.25
          show flora mushroom_anal_sex ecstasy penetration2 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration1 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration3 with Dissolve(.25)
          show flora mushroom_anal_sex ecstasy penetration4 with hpunch
          window auto show
          show flora mushroom_anal_sex orgasm penetration4 with dissolve2
          flora mushroom_anal_sex orgasm penetration4 "Oooooooh!"
          "Oh, my god. She actually came... from anal."
          "And now, it's my turn!"
          window hide
          show flora mushroom_anal_sex orgasm cum4 with hpunch
          window auto
          mc "F-fuck!"
          "As the orgasm hits me, so does the urge to go balls deep."
#         "It's the most satisfying feeling ever, to bottom out and cum inside a girl's ass."
          "It's the most satisfying feeling ever, to bottom out and cum inside\na girl's ass."
          "It's a million times better than you can imagine."
          "She just gasps as I fill her up."
          "Pulling out carefully to make sure the load stays deposited inside her, something I've always wanted to do."
          window hide
          show flora mushroom_anal_sex orgasm cum3 with Dissolve(.15)
          show flora mushroom_anal_sex orgasm cum2 with Dissolve(.075)
          show flora mushroom_anal_sex orgasm cum1 with Dissolve(.075)
          show flora mushroom_anal_sex orgasm cum0 with Dissolve(.5)
          show flora mushroom_anal_sex disappointed cum0 with Dissolve(.5)
          window auto
          $flora.lust+=3
          $flora.love-=1
          flora mushroom_anal_sex disappointed cum0 "Fuck you..."
          flora mushroom_anal_sex disappointed cum0 "You could at least have pulled out."
          $mc.lust+=2
          mc "Yeah, I could've."
          flora mushroom_anal_sex disappointed cum0 "You're the worst."
          "She's right, but that's also why she likes me."
          window hide
          show black onlayer screens zorder 100 with Dissolve(3.0)
          jump quest_flora_walk_park_upon_returning
    "Take the lead":
      mc "Anything you want, but first..."
      window hide
      play sound "<to 0.75>nylon_rip"
      show flora mushroom_kiss surprised with hpunch
      window auto
      flora mushroom_kiss surprised "[mc]!"
      "Her shirt rips a little as I pull it off."
      "[flora] might be used to my memes, my teasing, and my joker side..."
      "...but she's not used to this."
      window hide
      play sound "fast_whoosh"
      show flora mushroom_kiss topless surprised with hpunch
      window auto
      "She's not used to my growling as I rip her clothes off."
      window hide
      play sound "fast_whoosh"
      show flora mushroom_kiss bottomless surprised with hpunch
      window auto
      "Yanking her skirt and panties down in one fluid motion."
      flora mushroom_kiss bottomless surprised "Oh! Y-you're being kind of rough."
      mc "Just relax and trust me."
      flora mushroom_kiss afraid "What if someone sees us?"
      mc "Nobody comes all the way out here."
      mc "I can't take it anymore. I have been going crazy thinking about you."
      mc "Holding you, kissing you, fucking you."
      show flora mushroom_kiss smile with dissolve2
      "Her eyes widen as I say this and grab a hold of her."
#     "She looks like an innocent doe-eyed deer and I am hungry to devour my prey."
      "She looks like an innocent doe-eyed deer and I am hungry to devour{space=-5}\nmy prey."
      flora mushroom_kiss smile "You have...?"
      mc "Yes. I know you've been thinking about it too."
      mc "Try as we might to deny ourselves, we just can't be kept apart."
      mc "And I'm sick of trying."
      flora mushroom_kiss smile "I am too..."
#     "Her words come out in barely a whisper, but it's all the invitation I need."
      "Her words come out in barely a whisper, but it's all the invitation I need.{space=-70}"
      window hide
      show flora mushroom_sex line_up
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      flora mushroom_sex line_up "Mmmm..."
      "Her slick pussy lips feel like heaven against the tip of my dick..."
      "...and entering her is even better."
      window hide
      show flora mushroom_sex penetration1 with Dissolve(.5)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      window auto
      "She cries out, but I keep pushing until I bottom out deep within her."
      flora mushroom_sex penetration4 "Ohhh! [mc]..."
      "My name leaves her mouth in a soft, breathy exhale and the sound pushes me to greater euphoric heights."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      window auto
      "Time stops for a moment with me inside her, allowing me to enjoy the feel of her squeezing my throbbing cock."
      "My own breathing is shallow, mixing with her little gasps as I look down into her eyes."
      mc "Are you okay?"
      flora mushroom_sex penetration4 "Mmm... mmhmmm..."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration1 with Dissolve(.15)
      pause 0.075
      show flora mushroom_sex penetration2 with Dissolve(.15)
      show flora mushroom_sex penetration3 with Dissolve(.15)
      show flora mushroom_sex penetration4 with Dissolve(.15)
      window auto
      flora mushroom_sex penetration4 "D-don't stop, please!"
      mc "There is no stopping any of this..."
      mc "Not when it feels this good being with you."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with vpunch
      window auto
      "When the words leave me, I know I mean every one."
      "And why fight it? She fits me like a glove."
      "A warm, tight, welcoming glove."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration1 with Dissolve(.125)
      pause 0.0625
      show flora mushroom_sex penetration2 with Dissolve(.125)
      show flora mushroom_sex penetration3 with Dissolve(.125)
      show flora mushroom_sex penetration4 with vpunch
      window auto
      "Fucking [flora] is like a dream come true."
      "It makes her moan and whimper, pushing up into me with her hips."
      flora mushroom_sex penetration4 "Oh, god... P-please..."
      "And the best part is that she wants this as much as I do."
#     "To be filled up. To cork up that emptiness within her when I'm not there."
      "To be filled up. To cork up that emptiness within her when I'm not there.{space=-75}"
      "Willingly, I give her what she needs."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      window auto
      flora mushroom_sex penetration4 "Yeeees!"
      "She likes it a little rough, and I like going hard."
      "Her scream echoes into the sky above us and a few birds fly out of the trees."
      "Her desire and lust fully unleashed with no one around to hear it."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration1 with Dissolve(.1)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.1)
      show flora mushroom_sex penetration3 with Dissolve(.1)
      show flora mushroom_sex penetration4 with vpunch
      window auto
      "God, she drives me crazy in these intimate moments."
      "When she drops all pretense and lets me see the real her."
      "Vulnerable, shy, ready to follow my lead and go down this path with me."
      "If this is what forbidden fruit tastes like, then I never want to leave the garden."
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      window auto
#     "She clenches around me when I drive into her, and the friction burns through me."
      "She clenches around me when I drive into her, and the friction burns{space=-10}\nthrough me."
      "Faster and faster, slamming her ass into the ground beneath us."
      flora mushroom_sex penetration4 "Oh, my god! It feels so good! Don't stop!"
      mc "Don't worry, I won't!"
      window hide
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration1 with Dissolve(.075)
      pause 0.05
      show flora mushroom_sex penetration2 with Dissolve(.075)
      show flora mushroom_sex penetration3 with Dissolve(.075)
      show flora mushroom_sex penetration4 with vpunch
      window auto
      "Sweat drops dot her skin like morning dew as we speed towards the climax."
      "[flora] writhes beneath me, her orgasm mounting."
      "She shudders against me, her hot skin tight against mine."
      window hide
      show flora mushroom_sex clit_rub4 with Dissolve(.5)
      show flora mushroom_sex clit_rub3 with Dissolve(.15)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub1 with Dissolve(.05)
      pause 0.025
      show flora mushroom_sex clit_rub2 with Dissolve(.05)
      show flora mushroom_sex clit_rub3 with Dissolve(.05)
      show flora mushroom_sex clit_rub4 with vpunch
      window auto
      "My hand finds the apex of her pussy, and my fingers her clit."
      "A silent scream escapes her lips as I hit the right spot."
      "And it's more than enough to undo her completely."
      window hide
      show flora mushroom_sex orgasm4
      show flora avatar events mushroom_sex mc_hand5 as mc_hand:
        alpha 1.0 xpos 529 ypos 416
        linear 0.25 alpha 0.0
      with vpunch
      window auto
      flora mushroom_sex orgasm4 "Ohhhhh!"
#     "She spams and moans, my name hissing out of her again and again."
      "She spams and moans, my name hissing out of her again and again.{space=-5}"
      "Her sopping pussy traps me and keeps me buried up to my balls as her orgasm rips through her."
      "Seconds later, I..."
      $unlock_replay("flora_invitation")
      menu(side="middle"):
        extend ""
        "Cum inside":
          "There's no hope of withdrawing from her grip."
          "Even if I wanted to, I wouldn't."
          window hide
          show flora mushroom_sex cum_inside with vpunch
          window auto
          mc "F-fuck!"
          "So, I explode deep within her womb, cumming hard."
          "My hot, sticky semen coats her insides."
          flora mushroom_sex cum_inside "Mmmm... yes..."
          "She is so lost deep within the throes of her lust, that she actually wraps her legs around me."
          "She digs her heels into my back and milks me for every last drop."
          "Every ounce of my love and lust, right into her waiting pussy."
        "Pull out":
          window hide
          show flora mushroom_sex orgasm4 with Dissolve(.15)
          show flora mushroom_sex orgasm3 with Dissolve(.075)
          show flora mushroom_sex orgasm2 with Dissolve(.075)
          show flora mushroom_sex cum_outside with vpunch
          show flora mushroom_sex aftermath_outside with Dissolve(.5)
          window auto
          flora mushroom_sex aftermath_outside "Mmmm..."
          "A fountain of cum explodes all over her heaving stomach."
#         "The ejaculation just keeps on coming, spurt after spurt, until her lower abdomen is covered."
          "The ejaculation just keeps on coming, spurt after spurt, until her lower{space=-60}\nabdomen is covered."
          mc "Goddamn..."
          "She smiles as I collapse next to her."
          flora mushroom_sex aftermath_outside "I'll say..."
  window hide
  show misc autumn_sky
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  $flora.love+=2
  $flora.lust+=3
  "Together we lie side by side, catching our breath, staring up at the clouds that drift across the autumn sky."
  "Holding hands."
  "Enjoying the easy silence that falls between us."
  "There is no regret or shame, just passion and hope."
  "If we both want it enough, we can make it work."
  "And every day I will be grateful for this second chance."
  $quest.flora_walk["autumn_sky"] = True
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  label quest_flora_walk_park_upon_returning:
    if game.hour == 17:
      $game.advance()
    elif game.hour == 16:
      $game.advance(hours=2)
    elif game.hour < 16:
      $game.advance(hours=3)
    $game.location = "school_park"
    pause 1.5
    if quest.flora_walk["autumn_sky"]:
      show flora blush at appear_from_left
    else:
      show flora thinking at appear_from_left
    pause 0.0
    hide misc autumn_sky
    hide black onlayer screens
    with Dissolve(.5)
    pause 0.25
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5
    window auto
    if quest.flora_walk["autumn_sky"]:
      "Hand in hand, we return to the pond."
      "Her still squid splashes happily in the cooling water."
    else:
      "On the way back to the pond, we have a hard time looking at each other."
      "It's our own private walk of shame, but luckily no one is looking."
    flora "Err, I should probably get [cuthbert] out of the pond..."
    mc "That might be a good idea. It's starting to get cold."
    window hide
    hide flora with Dissolve(.5)
    window auto
    "Maybe it's a little awkward, but I think we'll be fine."
    "Normal couples probably feel like this sometimes, too."
    "..."
    "Are we actually a couple?"
    "Not officially, of course, but perhaps in our minds."
    "Our situation isn't exactly normal, but—"
    flora "Eeep!"
    window hide
    play sound "falling_thud"
    play ambient "<from 0.25>audio/sound/paint_splash.ogg" volume 0.75 noloop
    show flora pond flora_pulled
    show black onlayer screens zorder 100
    with vpunch
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    flora pond flora_pulled "Bad [cuthbert]! He pulled me in!"
    mc "Do you need help getting out?"
    show flora pond mc_help with dissolve2
    flora pond mc_help "Y-yeah."
    mc "Okay, hold on to my hand."
    flora pond mc_help "..."
    mc "[flora], what are you—"
    window hide
    play sound "falling_thud"
    play ambient "<from 0.25>audio/sound/paint_splash.ogg" volume 0.75 noloop
    show flora pond mc_pulled with vpunch
    window auto
    flora pond mc_pulled "Hehe!"
    mc "Oh, my god! That's cold!"
    show flora pond smile with dissolve2
    if quest.flora_walk["autumn_sky"]:
      flora pond smile "You looked like you needed some cooling down."
      mc "True. I'm all sorts of hot for you."
      show flora pond embarrassed with dissolve2
      flora pond embarrassed "That's... that's..."
      mc "Don't worry. I won't tell anyone."
      flora pond embarrassed "You better not!"
    else:
      flora pond smile "That's what you get!"
      mc "What did I do?!"
      flora pond smile "You know very well what you did."
      mc "I guess I did deserve this..."
      flora pond smile "Yes, you did!"
    mc "So, how do we explain this to [jo]?"
    show flora pond smile with dissolve2
    flora pond smile "I'll let you come up with something."
    flora pond smile "After all, lying is one of your specialties."
    mc "That's rude."
    flora pond smile "Maybe, but it's also true."
    mc "Brat."
    flora pond smile "Idiot."
    "I guess some things never change..."
    "And I don't think either of us want them to."
    flora pond smile "Come on, [cuthbert], swim into the bucket. We're going back home."
    window hide
    $unlock_replay("flora_cool_down")
    $school_park["cuthbert"] = False
    show black onlayer screens zorder 100 with Dissolve(.5)
    pause 0.5
    show flora smile_bucket at disappear_to_right
    pause 0.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    "Man, how does she have so much energy?"
    "I'm completely spent after what we just did."
    "I can't wait to get home and get some rest..."
    $quest.flora_walk.advance("sleep")
  return

label quest_flora_walk_sleep:
  menu(side="middle"):
    "What time is it?"
    "Sleeping time":
      "My work here is done."
      window hide
      show black onlayer screens zorder 100 with Dissolve(3.0)
      $x = game.day-1 if game.hour < 7 else game.day
      while game.day != x+1:
        $game.advance()
      while game.hour != 9:
        $game.advance()
      if quest.maya_sauce["bedroom_taken_over"]:
        call goto_home_hall
        $home_hall.visit()
      else:
        call goto_home_bedroom
        $home_bedroom.visit()
      hide black onlayer screens with Dissolve(.5)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
      if home_bedroom["alarm"] in ("smashed","smashed_again"):
        "Mmm... I really needed that..."
        "..."
        "Oh, shit! It's already 9!"
        "I knew I shouldn't have smashed that stupid clock!"
      else:
        "Mmm..."
        "Nothing better than waking up fully rested and at a good—"
        "Fuck! How did I sleep through my alarm?!"
        "It's already 9! Shit, shit, shit!"
      $quest.flora_walk.advance("confrontation")
    "Brooding time":
      "Rest is for the dead."
      "And those who are afraid of energy drinks."
  return

label quest_flora_walk_confrontation:
  if isabelle.location == game.location:
    show isabelle cringe with Dissolve(.5)
  else:
    show isabelle cringe at appear_from_right
    pause 0.5
  isabelle cringe "I saw what you did."
  mc "You'll have to be more specific than that."
  isabelle cringe "Don't play dumb with me."
  isabelle cringe "I watched you in the park yesterday through the window."
  mc "..."
  "Fuck. Shit. Fuck."
  "How much did she see?"
  mc "Err, it's not what it seems."
  isabelle cringe "I think it's exactly what it seems."
  "Fuck me. Is this how it all comes crashing down?"
  "I always thought it would be [jo] catching us..."
  "I even thought up like five excuses and appeasements."
  "God. Of course it had to be [isabelle] of all people."
# "It's like a sick twist to receive a second chance, only for this to happen."
  "It's like a sick twist to receive a second chance, only for this to happen.{space=-65}"
  isabelle cringe "This is not okay. And frankly, I'm disgusted."
  show isabelle cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Please, you can't tell anyone!\"":
      show isabelle cringe at move_to(.5)
      mc "Please, you can't tell anyone!"
      isabelle annoyed "Give me one good reason not to."
      mc "I don't care so much about myself... but think about [flora]!"
      $isabelle.love-=1
      isabelle annoyed "She should've thought about that before getting knee-deep in it."
      "Oh, man. This is going to be the end."
    "\"We were just fooling around, okay?\"":
      show isabelle cringe at move_to(.5)
      mc "We were just fooling around, okay?"
      mc "And it sort of just... happened."
      mc "It was honestly an innocent mistake!"
      $isabelle.lust-=1
      isabelle annoyed "It didn't look innocent to me."
      "It really didn't..."
    "\"It was [flora]'s idea!\"":
      show isabelle cringe at move_to(.5)
      mc "It was [flora]'s idea!"
      $isabelle.love-=1
      isabelle annoyed "I don't care whose idea it was. You both should know better."
      mc "But she tricked me! She's a vile witch!"
      $mc.lust+=1
      mc "Her words are dark magic laced with the devil's seduction!"
      isabelle annoyed "She's usually a nice, well-behaved girl."
      mc "That's what she wants you to believe!"
      $flora.love-=1
      mc "Trust me, there's wickedness inside her!"
  isabelle annoyed "I could get you both in so much trouble."
  mc "I know, but please..."
  mc "Surely, there must be something I can do to keep this quiet?"
  isabelle skeptical "Hmm..."
  mc "Pretty please?"
  isabelle skeptical "I'm not usually one to keep things away from the authorities...."
  mc "I'm begging you, [isabelle]."
  mc "I'll take full responsibility, but please don't bring [flora] down."
  mc "Honestly, it was all my idea. She didn't even want to."
  isabelle neutral "That's... kind of sweet of you to protect her like that."
  mc "I'll do anything. Just don't ruin her, please."
  isabelle concerned "She also needs to learn her lesson, though..."
  mc "Please, [isabelle]! Just this one time!"
  isabelle concerned "..."
  isabelle concerned "Fine. I won't tell anyone."
  mc "Thank you! Oh, god, thank you!"
  isabelle skeptical "However, you both still need to learn your lesson."
# isabelle skeptical "Rules exist for a reason. I want to make sure this doesn't happen again."
  isabelle skeptical "Rules exist for a reason. I want to make sure this doesn't happen again.{space=-70}"
  mc "Err, okay. What do you have in mind?"
  isabelle skeptical "Meet me in the forest glade, and bring [flora] with you."
  mc "Absolutely. We'll do anything you say."
  isabelle neutral "Very well, then."
  window hide
  if isabelle.location == game.location:
    hide isabelle with Dissolve(.5)
  else:
    show isabelle neutral at disappear_to_left
    pause 0.5
  window auto
  "Ugh, this means I'll have to tell [flora] that we got caught..."
  "Fuck."
  "And I guess we have no choice but to do what [isabelle] says."
  $quest.flora_walk.advance("caught")
  return

label quest_flora_walk_caught:
  show flora smile with Dissolve(.5)
  flora smile "Are you okay there? You look like you've swallowed a slug."
  flora smile "Which would be karma, by the way."
  mc "[flora], listen to me. We've been caught."
  flora afraid "C-caught?"
  mc "Yes! [isabelle] saw us in the park!"
  flora afraid "Do you mean—"
  mc "Yes!"
  flora embarrassed "Oh, my god."
  flora embarrassed "Please, tell me you're joking..."
  mc "I wish I was..."
  flora crying "This can't be happening. My life is literally over."
  flora crying "What will [jo] say? She will throw us out!"
  mc "I'm really sorry. It's all my fault."
# mc "I should've just stayed in my room and played video games like last time..."
  mc "I should've just stayed in my room and played video games like last time...{space=-120}"
  flora crying "Why is this happening?!"
  flora crying "We were so stupid! And reckless! And impulsive!"
  mc "I really wish there was something I could do to undo it..."
  flora crying "So, what happens now?"
  mc "I've managed to convince [isabelle] to keep quiet for now."
  mc "But she's making us meet her in the forest after school."
  flora crying "What? Why?"
  mc "I don't know..."
  flora crying "What if [jo] finds out?"
  show flora crying at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Then, we kill her.\"":
      show flora crying at move_to(.5)
      mc "Then, we kill her."
      $flora.love-=1
      flora crying "I'm not in the mood for your jokes..."
      mc "Right. I'm sorry."
      mc "Maybe we can come up with a believable story?"
      flora crying "Like what?"
      mc "Maybe you sat down in an anthill and got ants all over you..."
      mc "...and when I tried to help you, they got all over me, as well..."
      mc "...so we had no choice but to take our clothes off?"
      flora crying "No one is going to believe that!"
      mc "Yeah, probably not..."
#   "\"We will get through this together. Trust me.\"":
    "\"We will get through this\ntogether. Trust me.\"":
      show flora crying at move_to(.5)
      mc "We will get through this together. Trust me."
#     flora crying "If this gets out, I'll lose my internship... and all my friends... and [jo]..."
      flora crying "If this gets out, I'll lose my internship... and all my friends... and [jo]...{space=-5}"
      mc "Maybe, but you will never lose me. No matter what."
      flora crying "Really?"
      mc "Really. I can even take the full blame if it comes to that."
      mc "I'll say I forced you."
      $flora.love+=1
      $flora.lust+=1
      flora sad "Would you really do that for me?"
      mc "Absolutely. I'm not destined for great things, but you are."
#     mc "If it helps save your reputation, even a little bit, I'd do it in a heartbeat."
      mc "If it helps save your reputation, even a little bit, I'd do it in a heartbeat.{space=-50}"
      flora sad "Thank you for saying that, but I hope it doesn't come to it."
      mc "Me too..."
      mc "Maybe we can convince [isabelle] that it was an accident?"
#   "\"I once saw a documentary about getting off the grid...\"":
    "\"I once saw a documentary\nabout getting off the grid...\"":
      show flora crying at move_to(.5)
      mc "I once saw a documentary about getting off the grid..."
      flora crying "W-what?"
      mc "I know how to disappear."
      mc "We can go somewhere together where no one will find us."
      mc "Start fresh. Maybe in Argentina."
      flora crying "I don't want to start fresh! I have a life..."
      mc "You might not have a choice, [flora]."
      mc "We knew we might get into trouble when we did this."
      flora crying "I'm going to lose everything, aren't I?"
      mc "You don't know that yet. We need to keep our heads cool."
      mc "Maybe we can convince [isabelle] that it was an accident?"
  flora crying "Oh, my god... I can't believe this is happening..."
  "Damn. I've never seen her so upset before."
  "She has a lot more to lose than me. So, it makes sense."
# "[isabelle] was never the malicious kind, though. Maybe there's hope still?"
  "[isabelle] was never the malicious kind, though. Maybe there's hope still?{space=-70}"
  if quest.kate_stepping.finished:
    "But after what [kate] put her through, maybe she has changed..."
    "...and that's my fault too."
  "Fuck me! Why do I always have to screw things up?"
  "Maybe it doesn't matter how many chances I get."
  mc "I guess we should get going before [isabelle] changes her mind..."
# mc "We have to try to keep our faces straight if we're going to convince her."
  mc "We have to try to keep our faces straight if we're going to convince her.{space=-65}"
  flora crying "Okay... let me just go wash my face..."
  window hide
  show flora crying at (disappear_to_left if game.location == "home_kitchen" else disappear_to_right)
  pause 1.0
  $quest.flora_walk.advance("forest_glade")
  return

label quest_flora_walk_forest_glade_upon_entering:
  "Here we are... on our way to the gallows."
  "Nothing left to do but pray."
  return

label quest_flora_walk_forest_glade_road:
  "Running is unfortunately not the way out of this mess."
  return

label quest_flora_walk_forest_glade:
  show flora worried at Transform(xalign=0.25)
  show isabelle neutral at Transform(xalign=.75)
  with Dissolve(.5)
  flora worried "So, what happens now?"
  isabelle neutral "We clean up this place. I've brought garbage bags."
  flora worried "W-what?"
  "Man, [flora] looks terrified."
  "I hate to see her like this, but [isabelle] doesn't seem to mind at all."
  "She's acting casual as ever."
  isabelle neutral "You two are going to help me clean this place up."
  isabelle neutral "Call it community service."
  flora thinking "..."
  "This feels a little bit suspicious. Is [isabelle] really going to let us off that easily?"
# "It feels almost like she's just using us to get us to do some unrelated thing."
  "It feels almost like she's just using us to get us to do some unrelated{space=-25}\nthing."
  "[flora] has that look on her face, too."
  "What's stopping her from making us do things for her forever?"
  isabelle concerned "You two seem awfully quiet. What's the matter?"
  show flora thinking at move_to("left")
  show isabelle concerned at move_to("right")
  menu(side="middle"):
    extend ""
#   "?quest.kate_stepping.finished@|{image=isabelle_collar}|\"Did [kate] put you up to this?\"":
    "?quest.kate_stepping.finished@|{image=isabelle_collar}|\"Did [kate] put\nyou up to this?\"":
      show flora thinking at move_to(.25)
      show isabelle concerned at move_to(.75)
      mc "Did [kate] put you up to this?"
      isabelle annoyed "What? No. Why would she?"
      isabelle annoyed "I don't think she cares much about nature."
      mc "I meant the blackmail thing."
      isabelle angry "This is not blackmail! This is doing what's right!"
      flora confused "Can I leave, then?"
      isabelle thinking "After you help me clean this place up."
      mc "Sounds like blackmail to me..."
      isabelle cringe "It's the least you can do after what you did in the park!"
#     mc "So, you're telling me [kate] didn't give you permission for any of this?"
      mc "So, you're telling me [kate] didn't give you permission for any of this?{space=-10}"
      show flora cringe
      isabelle afraid "I don't think—"
      mc "If I called her right now, would she tell me this is okay?"
      mc "That you can mess with people without telling her?"
      isabelle subby "I... um..."
      "No witty retort? [kate] sure did a number on her over the fall break..."
      "Maybe we can push this further."
      mc "I didn't think so."
      mc "Okay, here's what's going to happen."
#     mc "You're going to clean up this glade while [flora] and I watch and make sure you do a good job."
      mc "You're going to clean up this glade while [flora] and I watch and make{space=-10}\nsure you do a good job."
      isabelle subby "That's not—"
      flora confused "I mean, I am pretty tired. I wouldn't mind supervising."
      flora sarcastic "I do that all the time with, [mc]."
      isabelle subby "But—"
#     mc "I think if [kate] found out that you're out here trying to blackmail people, she wouldn't be too happy."
      mc "I think if [kate] found out that you're out here trying to blackmail people,{space=-60}\nshe wouldn't be too happy."
      flora sarcastic "I don't think so either."
      isabelle subby "..."
      mc "Come on, [isabelle]. Get started."
      $isabelle.lust+=2
      isabelle subby "Um... okay..."
      window hide
      show flora sarcastic at move_to(.5,1.0)
      hide isabelle with Dissolve(.5)
      window auto
      "Damn. It actually worked."
      flora confused "What happened to her?"
      mc "[kate] has her on a short leash ever since they started dating."
      flora afraid "They're dating?!"
      mc "Apparently."
      flora skeptical "They used to be at each other's throats..."
      flora skeptical "And [isabelle] used to be way more fiery."
      mc "[kate] can be quite persuasive when she wants to."
      flora skeptical "It seems like she has calmed [isabelle] down a bit."
      "Tamed is probably a more fitting word..."
      mc "Yeah, seems that way."
      if quest.isabelle_stolen["lindsey_slaped"]:
        flora cringe "[lindsey] told me that [isabelle] slapped her a while back."
#       flora cringe "So, it's probably a good thing that [kate] is curbing that aggression a bit."
        flora cringe "So, it's probably a good thing that [kate] is curbing that aggression a bit.{space=-70}"
        mc "Right."
      flora confused "So, we're just going to watch her clean up now?"
      mc "Well, since she tried to blackmail us, I think it's only fair."
      flora annoyed "You're right. That was a messed up thing to do."
      flora annoyed "Hey, [isabelle]!" with vpunch
      "I'm not sure if [flora] fully realizes what's going on, but unfortunately for [isabelle], she can be a real slave driver."
      window hide
      show flora annoyed at move_to(.25)
      show isabelle subby at Transform(xalign=.75) with Dissolve(.5)
      window auto
      isabelle subby "Yes?"
      flora annoyed "Let's start with the stream."
      flora annoyed "We need to clean up the oil or whatever is in it."
      isabelle subby "Okay..."
      flora sarcastic "Oh, you should probably get undressed and put in the filters first."
      isabelle afraid "Undressed?!"
      flora sarcastic "Unless you want oil on your clothes?"
      isabelle thinking "Err, I guess not..."
      flora sarcastic "Come on, then! Chop chop!"
      mc "Maybe go easy on her, [flora]?"
      flora eyeroll "Well, it's getting late, and I'm getting cold."
      mc "Good point."
      mc "You heard her, [isabelle], quit burning daylight!"
      isabelle cringe "..."
      window hide
      pause 0.25
      $isabelle.unequip("isabelle_jacket")
      pause 0.75
      window auto
      "And to my surprise, [isabelle] actually does it."
      window hide
      pause 0.25
      $isabelle.unequip("isabelle_tiara")
      $isabelle.unequip("isabelle_top")
      pause 0.75
      window auto
      "She stands there shivering slightly once her top is off."
      window hide
      pause 0.25
      $isabelle.unequip("isabelle_skirt")
      pause 0.75
      window auto
      "But her bottoms quickly follow, anyhow."
      mc "You should probably take it all off, just to be safe."
      show flora annoyed
      isabelle subby "..."
      window hide
      pause 0.25
      $isabelle.unequip("isabelle_green_bra")
      pause 0.75
      window auto
      "She hesitates just a moment, before doing as I suggest."
      "Her large, round breasts spill free."
      "Pink nipples hardening in the chilly autumn air."
      window hide
      pause 0.25
      $isabelle.unequip("isabelle_green_panties")
      pause 0.75
      window auto
      "And after the bra, her panties immediately follow."
      "Leaving her pussy exposed to [flora] and me."
      window hide
      show isabelle stream_cleanup at center
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $quest.flora_walk["uno_reverse_card"] = True
      pause 0.5
      hide flora
      hide black onlayer screens
      with Dissolve(.5)
      window auto
#     "She wades into the oil slicked water with the filters, placing them up and down the length of the stream."
      "She wades into the oil slicked water with the filters, placing them up{space=-5}\nand down the length of the stream."
      flora "That's it! Keep it moving!"
      mc "Yeah! Make sure you get all of the trash when you're finished there!"
      isabelle stream_cleanup "O-okay..."
      "Man, there's something about watching others work."
      "Especially if it's a hot girl that's usually so prim and proper."
      "Usually so clean and pristine."
      "..."
      "I do kind of feel bad for not helping her, though."
      "Especially since it's my fault the stream looks like this..."
      $unlock_replay("isabelle_blackmail")
      menu(side="middle"):
        extend ""
        "Help [isabelle]":
          "It's the right thing to do. Even if I'll probably spend most of the time checking her out."
          "That milky white skin, all filthy and gross..."
          "Sweat on her brow, strain across her face..."
          "She looks so good doing manual labor."
          flora "[mc]? What are you looking at?"
          mc "Nothing! Just, err... trying to find... the dirtiest parts here..."
          "Oh, well. Time to pay my dues to nature."
          jump quest_flora_walk_forest_glade_cleaning_minigame
        "Let her work":
#         "Unfortunately for [isabelle], to see her get down and dirty, in the most literal sense of the word, it does something to me."
          "Unfortunately for [isabelle], to see her get down and dirty, in the most{space=-15}\nliteral sense of the word, it does something to me."
          "And [flora] watches over her like some sort of stern supervisor."
          "I didn't know that she had a domineering side to her, but I guess it makes sense."
          "If you don't push back at her, she'll do whatever she wants."
          "And [isabelle] definitely doesn't push back."
          "She just cleans dutifully. Sweat pours down her brow and back."
          "Shadows of strain fall across her face, but in her eyes there's something else."
          "A smoldering flame."
          $flora.lust+=1
          $isabelle.lust+=1
          $mc.lust+=1
#         "Perhaps a sign of determination, or maybe... something else entirely."
          "Perhaps a sign of determination, or maybe... something else entirely.{space=-10}"
#         "She cleans and cleans until the water starts to run clear and the sun begins to set."
          "She cleans and cleans until the water starts to run clear and the sun{space=-10}\nbegins to set."
          "Naked the whole time as the sludgy oil water runs off of her."
          window hide
          jump quest_flora_walk_forest_glade_upon_cleaning
    "\"This is blackmail. You know that, right?\"":
      show flora thinking at move_to(.25)
      show isabelle concerned at move_to(.75)
      mc "This is blackmail. You know that, right?"
      isabelle eyeroll "I think you're being a bit dramatic now."
      mc "Can you blame me? You're threatening to ruin our lives."
      isabelle eyeroll "Oh, come off it. I'm not going to ruin your lives."
      flora sad "He's right. This isn't funny, okay?"
      flora sad "We're young and made a mistake. It won't happen again."
      isabelle cringe "I sure hope not, but that's not an excuse."
      isabelle cringe "You're both adults, and should know better."
      mc "And how long are you planning to hold this over us?"
      isabelle thinking "I think once this place is cleaned up, that'll make up for it."
      flora sad "And then you'll forget about what you saw?"
      isabelle confident "Sure. I think it's a more than fair agreement."
      flora smile "Yes, it is! Thank you!"
      "[isabelle] is usually a fair judge. I guess we'll just have to trust her."
      mc "Yeah. Thanks for being reasonable."
      $isabelle.love+=1
      isabelle confident "No worries."
    "Let [flora] answer":
      show flora thinking at move_to(.25)
      show isabelle concerned at move_to(.75)
      "I don't make the best decisions under pressure. I better let [flora] take this one."
      flora sad "I don't know what you saw... but you've got the wrong idea."
      isabelle annoyed "I saw what I saw."
      flora sad "Yes, but I think you might be drawing hasty conclusions..."
      isabelle sad "If you don't want to help me clean this place up, that's fine."
      flora embarrassed "We'll help! It's not about that!"
      isabelle cringe "What's it about, then?"
      isabelle cringe "I saw what you two did."
      mc "Maybe you didn't see it right... or interpreted it the wrong way."
      "I'm not sure where [flora] is going with this, but I might as well back her up."
      isabelle thinking "There is no misinterpreting that."
      isabelle cringe "And I will not just stand by and let you two get away with it."
      flora skeptical "But if we help you clean this place up, you'll be okay with it?"
      isabelle confident "I can't be okay with it, but I will let you two carry on."
      "Oh? Carry on, carry on?"
      "That would be cool... if she isn't planning on telling anyone."
      mc "Very well."
      flora smile "Err, all right."
  isabelle confident "Oh, and we should probably get undressed first."
  isabelle confident "You two don't mind doing it in our knickers, do you?"
  mc "I think that's a great idea!"
  flora skeptical "Of course you do."
  isabelle laughing "Come on, then! Let's get started!"
  label quest_flora_walk_forest_glade_cleaning_minigame:
    window hide
    show minigames cleaning background_dirty as minigame_background
    if quest.flora_walk["uno_reverse_card"]:
      show minigames cleaning isabelle_xray_full as minigame_isabelle:
        xpos 700 ypos 50
      show minigames cleaning flora as minigame_flora:
        xpos 1551 ypos 0
    else:
      show minigames cleaning isabelle_xray as minigame_isabelle:
        xpos 700 ypos 50
      if isabelle.equipped_item("isabelle_collar"):
        show minigames cleaning collar as minigame_collar:
          xpos 756 ypos 160
      show minigames cleaning flora_xray as minigame_flora:
        xpos 1111 ypos 200
      show minigames cleaning face_neutral as minigame_face:
        xpos 1197 ypos 287
    show minigames cleaning left_arrow as minigame_left_arrow:
      xpos 450 ypos 800
    show minigames cleaning net_background as minigame_net_background:
      xpos 840 ypos 915
    show minigames cleaning net_foreground as minigame_net_foreground:
      xpos 840 ypos 915
    show minigames cleaning right_arrow as minigame_right_arrow:
      xpos 1400 ypos 800
    show black onlayer screens zorder 100
    with Dissolve(.5)
    $game.ui.hide_hud = True
    pause 0.5
    hide screen interface_hider
    hide black onlayer screens
    with Dissolve(.5)
    $renpy.transition(Dissolve(0.25))
    call start_cleaning_minigame
    if cleaning_minigame.state == "dirty":
      show minigames cleaning background_dirty as minigame_background
    elif cleaning_minigame.state == "dirtyish":
      show minigames cleaning background_dirtyish as minigame_background
    elif cleaning_minigame.state == "cleanish":
      show minigames cleaning background_cleanish as minigame_background
    elif cleaning_minigame.state == "clean":
      show minigames cleaning background_clean as minigame_background
    if not quest.flora_walk["uno_reverse_card"]:
      if cleaning_minigame.oops:
        show minigames cleaning face_angry as minigame_face:
          xpos 1196 ypos 289
      elif cleaning_minigame.oopsnt:
        show minigames cleaning face_laughing as minigame_face:
          xpos 1197 ypos 284
      else:
        show minigames cleaning face_neutral as minigame_face:
          xpos 1197 ypos 287
    if cleaning_minigame.drags[0].x == 40:
      show minigames cleaning net_background as minigame_net_background:
        xpos 460 ypos int(cleaning_minigame.drags[0].y)+910
      show minigames cleaning net_foreground as minigame_net_foreground:
        xpos 460 ypos int(cleaning_minigame.drags[0].y)+910
    elif cleaning_minigame.drags[0].x == 420:
      show minigames cleaning net_background as minigame_net_background:
        xpos 840 ypos int(cleaning_minigame.drags[0].y)+910
      show minigames cleaning net_foreground as minigame_net_foreground:
        xpos 840 ypos int(cleaning_minigame.drags[0].y)+910
    elif cleaning_minigame.drags[0].x == 750:
      show minigames cleaning net_background as minigame_net_background:
        xpos 1170 ypos int(cleaning_minigame.drags[0].y)+910
      show minigames cleaning net_foreground as minigame_net_foreground:
        xpos 1170 ypos int(cleaning_minigame.drags[0].y)+910
    if cleaning_minigame.state != "clean" and cleaning_minigame.round == cleaning_minigame.max_rounds:
      $quest.flora_walk["cleaning_minigame_failed"] = True
      $game.ui.hide_hud = True
      hide screen kitchen_minigame_skip with Dissolve(.25)
      if quest.flora_walk["uno_reverse_card"]:
        window auto
      else:
        window auto show
        show minigames cleaning face_angry as minigame_face with dissolve2:
          xpos 1196 ypos 289
      flora "[mc]! Are you even trying?!"
      flora "Catch the garbage, but not the wildlife!"
      window hide
      show screen kitchen_minigame_skip
      jump quest_flora_walk_forest_glade_cleaning_minigame
    else:
      with None
    show screen interface_hider
    hide screen kitchen_minigame_skip
    label quest_flora_walk_forest_glade_upon_cleaning:
      if quest.flora_walk["uno_reverse_card"]:
        show flora blush at Transform(xalign=0.25)
        show isabelle subby_grimy at Transform(xalign=.75)
      else:
        show flora blush_grimy at Transform(xalign=0.25)
        show isabelle neutral_grimy at Transform(xalign=.75)
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      if not quest.flora_walk["uno_reverse_card"]:
        $flora["outfit_stamp"] = flora.outfit
        $flora.outfit = {"bra":"flora_purple_bra", "panties":"flora_striped_panties"}
      if isabelle.equipped_item("isabelle_green_bra") and isabelle.equipped_item("isabelle_green_panties"):
        $isabelle["outfit_stamp"] = isabelle.outfit
        $isabelle.outfit = {"bra":"isabelle_green_bra", "collar":"isabelle_collar" if quest.kate_stepping.finished else None, "panties":"isabelle_green_panties"}
      $school_forest_glade["pollution"] = 0
      $school_forest_glade["wrappers"] = False
      while game.hour != 19:
        $game.advance()
      pause 0.5
      hide minigame_background
      hide minigame_isabelle
      hide minigame_collar
      hide minigame_flora
      hide minigame_face
      hide minigame_left_arrow
      hide minigame_net_background
      hide minigame_net_foreground
      hide minigame_right_arrow
      hide black onlayer screens
      with Dissolve(.5)
      if "event_player_force_go_home_at_night" in game.events_queue:
        $game.events_queue.remove("event_player_force_go_home_at_night")
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      call screen time_passed
      pause 0.5
      window auto
      if quest.flora_walk["uno_reverse_card"]:
        flora blush "That's looking much better!"
        flora blush "I think we can call it a night. What do you think, [mc]?"
        mc "It's definitely an improvement, anyway."
        mc "I would say that's enough for now."
        "[isabelle] just looks exhausted and relieved."
        "It feels good to have turned the tables on her."
#       "And hopefully she has forgotten about what she saw, or at least will know better than to blackmail us again."
        "And hopefully she has forgotten about what she saw, or at least will{space=-5}\nknow better than to blackmail us again."
        isabelle subby_grimy "Thank you..."
        mc "I'm just glad the forest has finally been cleaned up."
        mc "But next time you try anything funny, I will tell [kate]. Understand?"
        isabelle subby_grimy "I understand."
        isabelle subby_grimy "Thank you for not telling her about this..."
        mc "Good girl. You should get some rest now."
        flora laughing "You should probably go get cleaned up first. You're really dirty."
        isabelle thinking_grimy "You're right. It's getting dark, too."
        isabelle thinking_grimy "I do have something to ask, though."
        flora thinking "What's that?"
        isabelle cringe_grimy "The pond. There's a rare species of frogs living there."
        isabelle cringe_grimy "That no-swimming sign is there for a reason."
#       isabelle cringe_grimy "I know I shouldn't have tried to blackmail you with it, but please don't ever step into it again."
        isabelle cringe_grimy "I know I shouldn't have tried to blackmail you with it, but please don't{space=-30}\never step into it again."
        mc "Wait, what?"
        mc "Us stepping into the pond is what you saw and were upset about?"
        isabelle thinking_grimy "What did you think I meant?"
        flora laughing "Nothing! That's what he thought!"
        mc "Err, yeah! Totally!"
        mc "I just didn't realize the thing about the frogs, that's all."
        isabelle subby_grimy "Very well."
        isabelle subby_grimy "Now, if you'll excuse me..."
        window hide
        show flora laughing:
          ease 1.0 xalign 0.5
        show isabelle subby_grimy behind flora at disappear_to_left
        show flora worried with Dissolve(.5)
        pause 0.5
        window auto
        flora worried "Holy shit."
        mc "That was the misunderstanding of the century..."
        flora worried "Tell me about it..."
#       "Jesus christ. It feels like I've been given another... well, a third chance at life."
        "Jesus christ. It feels like I've been given another... well, a third chance{space=-35}\nat life."
        mc "We really need to be more careful from here on out, [flora]."
        flora sad "..."
        mc "What?"
        flora crying "I don't know if it's a good idea to continue..."
        "Those words hit me like a sledgehammer in the chest."
        "And the worst part is, she's right."
        show teary_eyes1 behind teary_eyes2:
          alpha 0.0 yalign 1.0
          pause 0.4
          easein 0.5 alpha 1.0
        show teary_eyes2 with {"master": close_eyes}:
          ysize 1080
        "If we continue down this path, it's only a matter of time before we get caught for real."
        "We've been deluding ourselves that this could ever work."
        "It breaks my heart to admit it."
        flora sad "You're crying."
        flora sad "I've never seen you cry before."
        mc "It's just... the thought of losing you... it hurts..."
        "And now that I've experienced being with her, it hurts even more."
        flora sad "Do I really mean that much to you?"
        mc "Are you kidding? You mean everything to me."
        flora flirty "Good."
        "Her change in tone sends me reeling."
        show teary_eyes1:
          easeout 0.5 alpha 0.0
        hide teary_eyes2 with {"master": open_eyes}
        mc "Good?"
        flora flirty "I just wanted to hear you say it."
        mc "Err, what?"
        flora blush "If I'm going to risk everything, I need to know what I mean to you."
        mc "But you said—"
        flora laughing "It's definitely not a good idea to continue."
        flora laughing "But I guess seeing you cry made me realize how miserable you'd be without me."
        "God, you have no idea."
        mc "You're unbelievable..."
        flora flirty "No, I'm everything to you."
        mc "You're going to be bringing that up a lot, aren't you?"
        flora blush "Maybe!"
        mc "Ugh, you're such a brat..."
        flora smile "Whatever. I'm hungry. Can we get pizza on the way home?"
      else:
        flora blush_grimy "That's looking much better!"
        flora blush_grimy "I think we can call it a night. What do you think, [mc]?"
        mc "It's definitely an improvement, anyway."
        mc "I would say that's enough for now."
        isabelle excited_grimy "Good job, guys."
        isabelle excited_grimy "The glade looks much healthier now."
        flora laughing_grimy "Yay! We did it!"
        isabelle neutral_grimy "Thank you sincerely for the help."
        mc "I mean, you did force us..."
        isabelle sad_grimy "I did. I'm sorry."
        isabelle annoyed_grimy "I just got angry when I saw you step into the pond."
        flora thinking_grimy "..."
        mc "Wait, what?"
        isabelle sad_grimy "There's a rare species of frogs living there."
        isabelle sad_grimy "That no-swimming sign is there for a reason, you know?"
        mc "Err, that's all you saw?"
        isabelle concerned_grimy "What else did you do?"
        flora laughing_grimy "Nothing! He's just being an idiot!"
        flora laughing_grimy "It makes sense that you wanted this place cleaned up in return."
#       isabelle neutral_grimy "Well, I just really care about nature. I'm sorry to be a hardass about it."
        isabelle neutral_grimy "Well, I just really care about nature. I'm sorry to be a hardass about it.{space=-35}"
        mc "That's okay! It's totally understandable."
        isabelle excited_grimy "Anyway, I need a shower badly now. I'll see you guys tomorrow!"
        window hide
        show flora laughing_grimy:
          ease 1.0 xalign 0.5
        show isabelle excited_grimy behind flora at disappear_to_left
        show flora worried_grimy with Dissolve(.5)
        pause 0.5
        window auto
        flora worried_grimy "Holy shit."
        mc "That was the misunderstanding of the century..."
        flora worried_grimy "Tell me about it..."
#       "Jesus christ. It feels like I've been given another... well, a third chance at life."
        "Jesus christ. It feels like I've been given another... well, a third chance{space=-35}\nat life."
        mc "We really need to be more careful from here on out, [flora]."
        flora sad_grimy "..."
        mc "What?"
        flora crying_grimy "I don't know if it's a good idea to continue..."
        "Those words hit me like a sledgehammer in the chest."
        "And the worst part is, she's right."
        show teary_eyes1 behind teary_eyes2:
          alpha 0.0 yalign 1.0
          pause 0.4
          easein 0.5 alpha 1.0
        show teary_eyes2 with {"master": close_eyes}:
          ysize 1080
        "If we continue down this path, it's only a matter of time before we get caught for real."
        "We've been deluding ourselves that this could ever work."
        "It breaks my heart to admit it."
        flora sad_grimy "You're crying."
        flora sad_grimy "I've never seen you cry before."
        mc "It's just... the thought of losing you... it hurts..."
        "And now that I've experienced being with her, it hurts even more."
        flora sad_grimy "Do I really mean that much to you?"
        mc "Are you kidding? You mean everything to me."
        flora flirty_grimy "Good."
        "Her change in tone sends me reeling."
        show teary_eyes1:
          easeout 0.5 alpha 0.0
        hide teary_eyes2 with {"master": open_eyes}
        mc "Good?"
        flora flirty_grimy "I just wanted to hear you say it."
        mc "Err, what?"
        flora blush_grimy "If I'm going to risk everything, I need to know what I mean to you."
        mc "But you said—"
        flora laughing_grimy "It's definitely not a good idea to continue."
        flora laughing_grimy "But I guess seeing you cry made me realize how miserable you'd be without me."
        "God, you have no idea."
        mc "You're unbelievable..."
        flora flirty_grimy "No, I'm everything to you."
        mc "You're going to be bringing that up a lot, aren't you?"
        flora blush_grimy "Maybe!"
        mc "Ugh, you're such a brat..."
        flora smile_grimy "Whatever. I'm hungry. Can we get pizza on the way home?"
      "I don't think I'll ever fully understand her, but that's part of what makes her so alluring."
      "One moment she's crying, and the next she's bantering."
      "It's almost like bad things don't get to her."
      "Or, at the very least, don't stick with her for very long."
      "I guess that's how she's able to tolerate all of my shit."
      "..."
      "God, I really need to make sure I don't mess it up like last time."
      "That dreaded moment still looms in the distance, and with every passing day of the school year, it gets closer."
      "But for now, I just need to enjoy the moment, and remember that there's more to life than avoiding catastrophes."
      mc "Sure. Let's get pizza."
      $mc["focus"] = ""
      window hide
      if quest.flora_walk["uno_reverse_card"]:
        show flora smile at disappear_to_left
      else:
        show flora smile_grimy at disappear_to_left
      pause 0.5
      if game.quest_guide == "flora_walk":
        $game.quest_guide = ""
      $game.notify_modal("quest","Quest complete",quest.flora_walk.title+"{hr}"+quest.flora_walk._phases[1000].description,5.0)
      pause 0.25
      window auto
      "It's getting late. I can't wait to get home."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
      $game.advance()
      pause 2.0
      hide black onlayer screens with Dissolve(.5)
      play music "home_theme" fadein 0.5
      $quest.flora_walk.finish(silent=True)
      if "event_show_time_passed_screen" in game.events_queue:
        $game.events_queue.remove("event_show_time_passed_screen")
      if quest.flora_walk["uno_reverse_card"]:
        $isabelle.outfit = {"hat": "isabelle_tiara", "collar": "isabelle_collar", "jacket": "isabelle_jacket", "shirt": "isabelle_top", "bra": "isabelle_green_bra", "pants": "isabelle_skirt", "panties": "isabelle_green_panties"}
      else:
        $flora.outfit = flora["outfit_stamp"]
        $isabelle.outfit = isabelle["outfit_stamp"]
  return
