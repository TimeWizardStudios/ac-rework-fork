#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#Quest labels

label quest_flora_bonsai_spaghetti:
  $quest.flora_bonsai.start()
  if nurse.at('school_nurse_room'):
    show nurse smile with Dissolve(.5)
  else:
    show nurse smile at appear_from_right
  $process_event("update_state")
  nurse smile "Hello there, [mc]. How are you feeling today?"
  mc "Other than the creeping feeling of imminent doom, pretty great!"
  nurse concerned "You do know there's counseling available for those kinds of feelings?"
  "Opening up to a therapist would probably leave them traumatized."
  nurse smile "Thanks for watering my bonsai, by the way! She can be a little thirsty sometimes!"
  nurse smile "Anyway, I'm about to—"
  show nurse smile at move_to(.25)
  show flora embarrassed flip at appear_from_right(.75)
  flora embarrassed flip "Ayyeeee! I need a bandaid!"
  nurse surprised "Now, now! I have one right here. What's the matter?"
  flora embarrassed flip "I got something in my eyes!"
  mc "Did you make chili again?"
  nurse concerned "What in god's name?"
  flora angry "No, not chili! Spaghetti bolognese!"
  nurse thinking "Okay, let's wash your eyes off. Was it very hot?"
  flora sad "Yes..."
  nurse thinking "Okay, come over here."
  hide nurse
  hide flora
  with Dissolve(.5)
  $school_nurse_room['curtain_off'] = False
  nurse "Okay, open your eyes. This eye-wash used to wash away chemicals, but it'll work just fine. Now, look in here."
  #SFX: Water flushing
  flora "Ohh!"
  "Typical [flora] to get spaghetti sauce in her eyes. She's always had a rough time eating noodles."
  "One of the few things that I'm better than her at..."
  flora "Thank you. That's much better."
  nurse "Still any pain?"
  flora "Yeah, it feels like dust in my eyes or something. Really uncomfortable."
  nurse "There's a bit of swelling around your tear ducts and eyelids from the heat."
  nurse "There might also be some light scarring on your corneas, but it's nothing serious."
  nurse "How does it feel when you close your eyes?"
  flora "It's mostly fine. It hurts the most when I keep them open."
  nurse "Okay, I'm going to put these pads over your eyes for now."
  nurse "It's just so that you don't irritate your eyes further by rubbing them, okay?"
  flora "How am I supposed to see?"
  nurse "You'll just have to rely on others for help for a week or so."
  $flora.equip("flora_blindfold")
  $school_nurse_room["curtain_off"] = True
  pause(.5)
  show nurse smile at Position(xalign=.25)
  show flora cringe at Position(xalign=.75) #AFTER THIS POINT ALL OF FLORA'S DIALOGUE SPRITES USE THE BANDAID. IT REMAINS ON UNTIL THE END OF THE QUEST OR UNTIL THE LOVE ENDING IN THE LAST PHASE WHEN IT'S SPECIFICALLY TAKEN OFF.
  with Dissolve(.5)
  nurse smile "I'm sure [mc] would be happy to help."
  nurse smile "I'll be right back."
  show nurse smile at disappear_to_right
  show flora cringe at move_to(.5)
  flora cringe "Do not come near me."
  show flora cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I wasn't planning on it.\"":
      show flora cringe at move_to(.5)
      mc "I wasn't planning on it."
      "Fuck. Was totally planning on it."    #could put in funny subs or incel subs. And incest subs
      flora annoyed "You were totally planning on it! I don't need to see you to know you."
      mc "Give me a chance to help you. You know how busy [jo] is."
      mc "All you have to do is hold on to my arm for a while."
      flora cringe "You'll lead me off a cliff."
      mc "I would never!"
      flora annoyed "Maybe not intentionally, but the point is you can't be trusted."
      mc "I think that's a little unfair. Remember when you were sick back in 8th grade?"
      flora angry "Yes! You literally stole my cough drops!"
      mc "I mean, yeah. I did do that."
      mc "But it also turned out the dosage was wrong, so...  basically, I saved you from a potential overdose."
      flora angry "You can't OD on cough drops!"
      mc "I actually read about a guy in Japan who injected cough drops into his—"
      flora annoyed "You know what, fine. You can help me, but I have [jo] on speed dial if you try anything."
    "?flora.lust>=4@[flora.lust]/4|{image= flora contact_icon_bandaid}|{image=stats lust}|\"You look like a cute Matt Murdock!\"":
      show flora cringe at move_to(.5)
      mc "You look like a cute Matt Murdock!"
      flora annoyed "You shut your mouth!"
      flora annoyed "How dare you imply he's not cute?"
      mc "I was implying you're the Daredevil without the \"dare.\""
      flora angry "..."
      flora angry "You know I will make you suffer for that comment."
      mc "Point and case."
      flora annoyed "Come here so I can kick you in the shin."
      mc "Quit being a brat and I'll help you around."
      $flora.lust+=1
      flora annoyed "..."
      flora annoyed "Fine."
    "?flora.love>=4@[flora.love]/4|{image= flora contact_icon_bandaid}|{image=stats love}|\"I don't mind helping you around.\"":
      show flora cringe at move_to(.5)
      mc "I don't mind helping you around."
      flora annoyed "I mind! I mind a lot!"
      flora annoyed "I wouldn't trust you with my least favorite plushie."
      mc "I wouldn't either, but your plushies are evil."
      flora angry "Is that supposed to help your case?"
      mc "Yes, I just implied you're not evil. Even though I've said so once or twice in the past."
      flora angry "Multiple times!"
      mc "In any case, I wouldn't let anything bad happen to you."
      $flora.love+=3
      flora confused "I guess you have been nicer to me lately..."
      mc "Yes, I have."
      flora annoyed "The moment I think you're up to no good, I'm telling [jo]."
  show flora annoyed at move_to(.75)
  show nurse smile at appear_from_right(.25)
  nurse smile "There we are." #assuming smile_right means flip
  nurse smile flip "I made a call to the hospital. Your eyes are fine, [flora], they just need some rest."
  flora neutral "Thank god. How long do I have to keep this on?"
  nurse smile flip "Just for a few days."
  nurse thinking "Let's see. If there's any immediate discomfort or pain, you need to call the hospital."
  nurse thinking "Also, I'm going to a medical conference this week, so I have to close up my office."
  $nurse["none"] = True
  nurse concerned "What an unfortunate timing."
  nurse thinking "Again, thanks for watering my bonsai earlier, [mc]."
  nurse blush "That was very nice of you. Would you mind watering it while I'm gone?"
  flora annoyed "It's not like he has anything better to do with his time."
  mc "..."
  nurse blush "Then it's decided!"
  mc "Great."
  show nurse blush at disappear_to_right
  show flora annoyed at move_to(.5)
  flora annoyed "Take me home."
  "So, I'm basically [flora]'s personal butler for a week. And I need to make sure that little tree doesn't die. Fantastic."
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  #$flora.location=home_kitchen
  jump goto_home_kitchen

label quest_flora_bonsai_help_home:
  show flora confused with Dissolve(.5)
  flora confused "[mc]? Is that you?"
  show flora confused at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Unfortunately.\"":
      show flora confused at move_to(.5)
      mc "Unfortunately."
      flora annoyed "Just my luck."
      mc "Why? What did [jo] say about you getting spaghetti in your eyes?"
      flora "I didn't tell her the specifics..."
      flora "Just that I got scolded at lunch."
      mc "Well, how are you feeling now?"
      flora "Mostly just stupid and helpless. It doesn't really hurt much anymore."
    "\"No, this is a recording of my voice.\"":
      show flora confused at move_to(.5)
      mc "No, this is a recording of my voice."
      flora annoyed "You're so funny."
      flora annoyed "How about I show you a recording of my middle finger?"
      mc "Speaking of recordings. How do you feel about making an amateur movie?"
      flora confident "There's nothing amateurish about me."
      mc "..."
      flora confident "What? Did you run out of tape or something?"
      "Shit. Does she know about the recordings?"
      "Ran out of tape recently on the hidden camera in her room."
      "Need to retrieve it at some point."
    "\"...\"":
      show flora confused at move_to(.5)
      mc "..."
      flora annoyed "I'm blind, not deaf."
      "Oh, man. I love it when she unknowingly quotes video games."
      mc "Were you imprisoned for 10,000 years by a hot elven warden with a fetish for knives?"
      flora angry "What are you even on about?"
      mc "Nothing, just... reminiscing."
  mc "So, do you need help with anything?"
  mc "A drink? A meal? Maybe a shower?"
  flora cringe "Perv!"
  mc "What? Are you not going to shower for an entire week?"
  flora thinking "I'll figure something out..."
  mc "What about a drink or meal then?"
  flora skeptical "I guess I wouldn't mind that... I'm kinda craving something sweet."
  flora skeptical "But if you somehow tamper with it, I'll never forgive you."
  "Hmm... she sounds rather serious, but that never stopped me in the past."
  "Maybe that's part of why she cut all contact with me after we graduated."
  $quest.flora_bonsai.advance('refreshments')
  hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_bed:
  $flora.unequip('flora_shirt')
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  show black with fade
  $home_bedroom["night"] = True
  if game.hour > 6 and game.hour < 23:
    while game.hour != 2:
      $game.advance()
  hide black with fade
  "Man, getting those needed hours of sleep is such a struggle."
  "Everytime you lie down you look at the ceiling, going over the most recent pile of mistakes."
  "And that pile never stops growing..."
  "If only you could shut off your brain with a switch."
  "If only you could—"
  play sound "<from 0.0 to 1.0>door_knock"
  flora "[mc], are you awake?"
  mc "Yeah, come on in."
  $home_bedroom['night'] = False
  show flora sad with dissolve #flora: panties, bra, bandaid
  flora sad "I need cake."
  mc "Seriously?"
  flora sad "Help me bake."
  "Voice shaking, sweat dotting her forehead — clearly, the cake withdrawal is already hitting her in full."
  "Most concerning of all, she came into my room in her underwear."
  mc "Do you know what time it is?"
  flora excited "Cake time!"
  "Watching [flora] bake in her undies does seem like a good time."
  "But if I'm ever going to trust her, she needs to be weeded off her bad habits."
  "If she keeps baking that cake, she'll always have a way of manipulating people."
  "It's not good for her or anyone else."
  "Yes, and I'm a total hypocrite thinking this. Fuck."
# call screen remember_to_save(_with_none=False) with Dissolve(.25)
# with Dissolve(.25)
  show flora excited at move_to(.25)
  menu(side="right"):
    extend ""
#   "{image=stats lust}|Help her bake": #this should maybe have Lust Route icon
    "Help her bake":
      show flora excited at move_to(.5)
      mc "All right, but we'll have to be quiet. If [jo] wakes up, we're both dead."
      flora smile "Probably just you, but whatever."
      flora smile "Let's go!"
      hide flora with Dissolve(.5)
      #$flora.location: home_kitchen, (On chair: bra, panties, bandaid)
      $quest.flora_bonsai.advance('chef')
#   "{image=stats love}|Stop her from baking": #this should maybe have Love Route icon
    "Stop her from baking":
      $unlock_replay("flora_bonsai_bed")
      show flora excited at move_to(.5)
      mc "[flora], we both know you don't really want more cake."
      flora sad "I do, too!"
      mc "Do you really want to turn into a junkie?"
      mc "Imagine baking every morning just to be able to function."
      mc "Your entire world turns into a waiting game for the next slice of cake. The next fix."
      flora cringe "..."
      flora cringe "You're right."
      mc "You don't want to live like that."
      flora sad "I know."
      flora sad "What if I can't stop myself from baking?"
      flora afraid "I don't want to go to rehab."
      mc "I know it's my fault, but I'll take care of you."
      mc "You'll stay in here tonight so I keep an eye on you."
      flora afraid "In here?"
      flora afraid "Where am I going to sleep?"
      mc "Don't be so immature. My bed is big enough."
      flora annoyed "..."
      flora annoyed "I'm not—"
      mc "It's that or the floor, and I haven't vacuumed since Christmas last year."
      flora cringe "Gross."
      mc "Come on, stop being a baby about it."
      flora concerned "Fine, but we'll put the machete between us."
      mc "Whatever you say."
      show flora bed neutral with fadehold
      "It always used to be a fantasy of mine to have [flora] sleeping by my side."
      "But those fantasies were all dirty in nature. This is different."
      "A few minutes ago, she was shaking and sweating, but now she seems more content."
      "Old [flora] would never have agreed to this, but maybe she's changing?"
      "Maybe I'm the one changing?"
      "We always give each other a tough time, but maybe we're starting to trust each other more."
      "At the end of the day, this is what I want."
      "Her soft breath on my neck. The familiar scent of strawberries and sass."
      "I could grow old like this."
      "Why chase other girls when the perfect one's been here all along?"
      "Seeing her like this makes me regret all the bad things I've done to her in the past."
      flora bed neutral "[mc]? Are you awake?"
      mc "Hmm, yeah?"
      flora bed sad "I want cake."
      mc "I know. I was addicted for far too many years. I know the struggle."
      flora bed sad "I didn't fully grasp the magnitude of this cake's power."
      mc "It's okay, I figured you'd be better off knowing. It's a lot of responsibility."
      flora bed smile "You make it sound silly."
      show flora bed content with Dissolve(.5)
      mc "I'm doing my best not to. This cake is evil."
      flora bed sad "I'm burning the recipe tomorrow."
      menu(side="right"):
        extend ""
        "\"You don't have to do that. It really might come in handy someday.\"":
          $mc.intellect+=1
          mc "You don't have to do that. It really might come in handy someday."
          flora bed flirty "That's what you said about the neighbor's old magazines."
          "Oh, they've come in handy a lot. Sometimes multiple times per day."
          show flora bed content with Dissolve(.5)
          mc "Right, but this is different."
          flora bed content "Fine, I'll keep the recipe in case of a cake emergency!"
          mc "Yeah, because it's easier to bake your way to the top than... using your mouth."
          flora bed flirty "As if I need to take shortcuts to be successful."
          show flora bed content with Dissolve(.5)
          mc "I mean... if [jo] got her throat problems from climbing the corporate ladder..."
        "\"That's probably for the best. I'm all about disarming WMDs.\"":
          mc "That's probably for the best. I'm all about disarming WMDs."
          flora bed neutral "Since when do you care about that?"
          mc "Since I saw what that cake did to you."
          mc "I don't want you to get like this again, or anyone else for that matter."
          flora bed content "Aww."
          $mc.strength+=1
          mc "Besides, real fighting should be done using your fists."
        "\"Make sure you also stake it and put salt around the grave.\"":
          $mc.charisma+=1
          mc "Make sure you also stake it and put salt around the grave."
          #incest patch
          #flora bed flirty "Being your [roommate/sister] has taught me everything I need to know about purifying unholy taint."
          flora bed flirty "Being your roommate has taught me everything I need to know about purifying unholy taint."
          show flora bed content with Dissolve(.5)
          mc "Hilarious..."
          flora bed content "You're not all bad. You know that, right?"
          mc "You really are high on cake."
          flora bed neutral "I'm serious!"
      flora bed neutral "You know, you can be a real dick sometimes, and annoying, and a creep, and you rile me up like no one else..."
      mc "Is there a \"but\" coming soon?"
      flora bed smile "But!"
      flora bed content "I know that deep down you care about me."
      flora bed content "I think you've shown that in the last few weeks."
      flora bed content "Give me your hand."
      mc "Why? What are you going to do with it?"
      flora bed flirty "Just shut up and do it!"
      "Her cake addiction might cause her to bite it off..."
      "But you only live twice, right?"
      show flora bed content with Dissolve(.5)
      "Blindly, her fingertips fumble down my arm, searching for something..."
      "Maybe it's approval? Maybe it's comfort."
      "Sweeping across my wrists, her hand finally finds mine and her thin fingers squeeze me tight."
      show flora bed content hand with Dissolve(.5)
      "Luckily, she can't see the surprise and stupid grin on my face."
      "I never thought holding her hand would feel so intimate and pure at the same time."
      "Unlike so many other things between us, no one can judge us for holding hands."
      "There's nothing wrong about it, and she knows it too."
      mc "This feels... right."
      flora bed smile hand "Goodnight, [mc]."
      show flora bed content hand with Dissolve(.5)
      "Who knows what will happen tomorrow?"
      "Maybe we'll go back to our usual ways."
      "But for now, I don't care. I just want to feel her squeezing my hand."
      "I want nothing more than for this moment to last."
      "Both of us pretending to sleep, feeling the soothing heat, the comfort in each other's presence."
      "Both of us wondering what could be? What if things were different?"
      "Maybe we'll never know, but that doesn't matter right now."
      "..."
      "What matters is this... this feeling. Is it happiness? Bliss?"
      "All I know is that I want this."
      "Her hand in mine."
      $set_dialog_mode("default_no_bg")
      show black with fadehold
#     $flora.love+=25
      $flora.love+=5
      while game.hour != 7:
        $game.advance()
      $flora.equip("flora_shirt")
      if quest.flora_handcuffs > "package":
        $flora.equip("flora_skirt")
      else:
        $flora.equip("flora_pants")
      $quest.flora_bonsai["handholding"] = True
      $quest.flora_bonsai.advance("water")
      jump goto_home_bedroom
  return

label quest_flora_bonsai_bake:
  $flora.unequip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  show flora annoyed with Dissolve(.5)
  flora annoyed "What took you so long?"
  mc "Most time went into dragging myself out of bed in the middle of the night and debating why I'm helping you bake."
  flora smile "Well, stop with that."
  flora smile "I'm going to start prepping the oven."
  flora thinking "In the meantime, I need you to bring me a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
  $quest.flora_bonsai['drawers-complete'] = 0
  $quest.flora_bonsai['drawers'] = 1
  $quest.flora_bonsai.advance("bake")
  hide flora with Dissolve(.5)
  return


label quest_flora_bonsai_bake_check:
  if quest.flora_bonsai['drawers'] == 7:
    show flora neutral with Dissolve(.5)
    mc "You're lucky my hands are so big."
    flora sarcastic "They're not that big. Slightly below average, actually."
    mc "Ugh. At least I'm not addicted to cake."
    flora thinking "Yet."
    flora thinking "Anywho, we need sugar. Lots of it!"
    $quest.flora_bonsai.advance("sugar")
    hide flora with Dissolve(.5)
    return
  else:
    if not quest.flora_bonsai['failed']:
      show flora annoyed with Dissolve(.5)
      flora annoyed "Just because I'm blind doesn't mean you can just give me random things. Get it right!"
      flora neutral "I need you to bring me a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
      $quest.flora_bonsai['failed'] = 1
    else:
      if quest.flora_bonsai['failed'] == 1:
        show flora eyeroll with Dissolve(.5)
        flora eyeroll "I know you're hopelessly lazy, but..."
        flora eyeroll "You gotta do the cooking by the book! Start over!"
        flora neutral "I need you to bring me a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
        $quest.flora_bonsai['failed'] = 2
      else:
        show flora thinking with Dissolve(.5)
        flora thinking "Two plus two equals—"
        flora angry "What do you want?"
        mc "Here's your stuff."
        flora angry "No, no, no. Not like that. Do I have to do everything myself?"
        flora neutral "I need you to bring me a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
        $quest.flora_bonsai['failed'] = 1
  $quest.flora_bonsai['drawers-complete'] = 0
  $quest.flora_bonsai['drawers'] = 0
  $quest.flora_bonsai.advance("check")
  hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_bake_confirm:
  show flora neutral with Dissolve(.5)
  flora neutral "Ready?"
  flora neutral "I need you to bring me a fork, a grater, a pan, a measuring beaker, eggs, and flour. In that order."
  $quest.flora_bonsai.advance("bake")
  $quest.flora_bonsai['drawers'] = 1
  hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_sleep:
  show black with fadehold
  while not game.hour == 7:
    $game.advance()
  $quest.flora_bonsai.advance("water")
  return

label quest_flora_bonsai_fetch_sleep:
  show black with fadehold
  while not game.hour == 7:
    $game.advance()
  if quest.flora_bonsai != "fetchover":
    $flora.love-=2
    $flora.lust-=2
  $quest.flora_bonsai.advance("florahelp")
  return

label quest_flora_bonsai_knock_knock:
  "Okay, this should sate the bonsai's thirst for today."
  "I wonder if [flora] needs help with anything. I did promise to—"
  #SFX: Knocking
  isabelle "Hello?"
  "Shit. [isabelle]'s at the door."
  menu(side="middle"):
    extend ""
    "Freeze in panic":
      isabelle "Hello? Anyone in there?"
      #SFX: Knocking
      isabelle "I need pads..."
      isabelle "..."
      lindsey "The [nurse] is away this week, but I always bring extra. Here you go."
      isabelle "Oh, you're so nice! Thank you!"
      isabelle "I also always bring extra, but I think someone took my box when I was in the stall earlier."
      lindsey "It happens. People aren't always paying attention. I should know."
      "..."
      "..."
      "Okay, the coast is clear. God, I really need to get my shit together."
      "If [lindsey] hadn't stopped by, I'd be in serious trouble."
      $quest.flora_bonsai["helped_isabelle"] = False
      $quest.flora_bonsai.advance("fetch")
    "?mc.intellect>=3@[mc.intellect]/3|{image= stats int}|Block the door":
      mc "Sorry, err... you can't come in."
      isabelle "[mc]? What's going on?"
      menu(side="middle"):
        extend ""
        "\"I just threw up. Don't come in.\"":
          mc "I just threw up. Don't come in."
          isabelle "Are you all right?"
          mc "Yeah, just give me a moment."
          isabelle "I ran out of pads, I need to come in."
          "As much as it would be interesting to feed the tree some blood, that seems dangerous."
          "It's already out of control."
          mc "I'll bring them to you. Hold on."
          isabelle "All right."
          $quest.flora_bonsai.advance("pads")
        "?mc.charisma>=5@[mc.charisma]/5|{image= stats cha}|\"I'm naked.\"":
          mc "I'm naked."
          isabelle "...Why?"
          mc "I had to take a quick wank."
          isabelle "What? You're joking?"
          mc "It's a perfectly normal and human thing to do. Everyone does it."
          isabelle "I mean... you're not wrong. But why in the [nurse]'s office?"
          mc "It's the only place with a skeleton."
          isabelle "..."
          mc "I'm joking. Hold on."
          #jump Admin Wing
          call goto_school_ground_floor_west
          show isabelle laughing with Dissolve(.5)
          isabelle laughing "I thought you were serious!"
          mc "Heh, well."
          mc "Anyway, the [nurse] isn't here this week if you were looking for her."
          isabelle smile "Yeah, I was just looking for pads. I think someone took my box by accident when I was in the stall."
          isabelle smile "I was hoping the [nurse] had some."
          "Crap. Think fast."
          mc "I, err... I know where they are. I get them for [flora] all the time 'cause she's a bit of a ditz. Wait here."
          isabelle smile "Thanks! And I'm sure that's not true. [flora] seems like a smart bean."
          hide isabelle with Dissolve(.5)
          $isabelle.love+=1
          call goto_school_nurse_room
          $quest.flora_bonsai.advance("pads")
  return


label quest_flora_bonsai_fetch_sweets_gift(item):
  if item == "doughnut" or item == "lollipop" or item == "lollipop_2":
    $mc.remove_item(item)
    show flora flirty with Dissolve(.5)
    flora flirty "Mmm! Thanks!"
    $flora.love+=1
    mc "Anything else, your highness?"
    flora smile "Not for now, but keep checking your phone."
    "Ugh. Can't I just ghost her instead?"
    $quest.flora_bonsai["sweets_delivered"] = True
    $quest.flora_bonsai['current_complete'] = True
    hide flora with Dissolve(.5)
  else:
    show flora angry with Dissolve(.5)
    flora angry "What did you put in my mouth, [mc]?"
    mc "Just my [item.title_lower]."
    flora angry "I asked for sweets, not stupidity."
    mc "My bad. Would you like the King of Sweets instead?"
    flora cringe "Is that a euphemism for your penis?"
    "Damn, she knows me too well."
    $quest.flora_bonsai.failed_item("sweets",item)
    hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_fetch_drink_gift(item):
  if item == "seven_hp" or item == "cup_of_coffee" or item == "banana_milk" or item == "pepelepsi" or item == "strawberry_juice" or item == "water_bottle" or item == "salted_cola" or item == "milk":
    $quest.flora_bonsai["flora_drink"] = item.title_lower
    $mc.remove_item(item)
    if item == "cup_of_coffee":
      $item.title_lower = "coffee"
      $mc.add_item("coffee_cup")
    else:
      $mc.add_item("empty_bottle")
    show flora excited with Dissolve(.5)
    mc "Here's your [item.title_lower], madam."
    flora excited "Excellent. I like having you as my butler!"
    "Great. This is just great."
    $flora.love+=1
    "Surely, that must be all. Surely."
    $quest.flora_bonsai["drink_delivered"] = True
    $quest.flora_bonsai['current_complete'] = True
    hide flora with Dissolve(.5)
  else:
    show flora angry with Dissolve(.5)
    flora angry "I said I was thirsty. How will your [item.title_lower] help?"
    mc "You could try sucking on it, maybe some liquid will come out."
    flora annoyed "This might come as a shock to you, but most things don't work like penises."
    mc "My goodness, [flora]! Please, get your mind out of the gutter!"
    flora eyeroll "Shut up."
    $quest.flora_bonsai.failed_item("drink",item)
    hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_fetch_bathroom_interact:
  $drink = quest.flora_bonsai["flora_drink"]
  if drink == "cup of coffee":
    $drink = "coffee"
  show flora annoyed with Dissolve(.5)
  flora annoyed "Hurry! I'm about to pee myself!"
  mc "Maybe you shouldn't drink so much [drink]?"
  flora annoyed "Yes, I should! Now help me!"
  show flora annoyed at disappear_to_left
  call goto_home_hall
  show flora skeptical at move_to(.5) with moveinleft
  flora skeptical "Where do you think you're going?"
  mc "You don't need help in there?"
  flora afraid "No! Stay out!"
  hide flora afraid with Dissolve(.5)
  "Well... was worth a shot."
  $home_hall["bathroom_locked_now"] = True
  $flora['hidden_now'] = True
  $flora.lust+=1
  $quest.flora_bonsai["bathroom_delivered"] = True
  $quest.flora_bonsai['current_complete'] = True
  return

label quest_flora_bonsai_fetch_book_gift(item):
  if item == "catch_thirty_four":
    $mc.remove_item(item)
    show flora confident with Dissolve(.5)
    flora confident "This is perfect!"
    mc "Are you going to tell me what you'll use it for?"
    flora excited "Not likely!"
    $flora.lust+=1
    $quest.flora_bonsai["book_delivered"] = True
    $quest.flora_bonsai['current_complete'] = True
    hide flora with Dissolve(.5)
  elif item == "atlas_plugged" or item == "snow_white" or item == "bayonets_etiquettes" or item == "book_of_the_dammed":
    show flora annoyed with Dissolve(.5)
    flora annoyed "I want a different book."
    mc "What's wrong with this one? It tastes perfectly fine."
    flora eyeroll "It's not part of the English syllabus!"
    flora angry "And I'm not going to eat it!"
    $quest.flora_bonsai.failed_item("book",item)
    hide flora with Dissolve(.5)
  else:
    show flora annoyed with Dissolve(.5)
    flora annoyed "Are you trying to be funny? You're not being funny."
    mc "What? Is this not good enough reading material for you?"
    $quest.flora_bonsai.failed_item("book",item)
    hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_fetch_onions_interact:
  show flora neutral with Dissolve(.5)
  mc "I'm here and ready to cut..."
  flora laughing "Oh, great! Please start!"
  #$start.minigame(onion cutting) #play minigame 3 times (doesn't matter if success or not)
  call start_kitchen_minigame
  show flora excited with Dissolve(.5)
  flora excited "More!"
  mc "Seriously? What do you need this much onions for?"
  flora excited "None of your business!"
  #$start.minigame(onion cutting) #play minigame 2 times (doesn't matter if success or not)
  call start_kitchen_minigame
  show flora flirty with Dissolve(.5)
  flora flirty "More!"
  mc "No, I think that's enough."
  flora angry "Fine. I'll do it myself."
  mc "No, you're not. I'm not letting you near this knife in your current state."
  flora laughing "Okay!"
  mc "What's so funny?"
  $flora.love+=1
  flora blush "I was just testing you to see if you'd let me endanger myself."
  mc "So, all these onions were for nothing?"
  flora laughing "Not for nothing! For my entertainment!"
  "God. She's so annoying."
  "I'm actually done with her bullshit requests."
  $quest.flora_bonsai['current_complete'] = True
  $quest.flora_bonsai.advance("fetchover")
  $process_event("update_state")
  hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_vines:
  show flora annoyed with Dissolve(.5)
  flora annoyed "I'm supposed to be the blind one here, why are you taking so long?"
  flora annoyed "I have classes to attend to... and... and..."
  flora confused "[mc]? Hello? Why are you so quiet all of a sudden?"
  mc "Err..."
  flora angry "What the hell? Can you stop touching my leg?"
  mc "I didn't... there's a..."
  mc "[flora], I think you should move..."
  flora smile "Hey, that tickles!"
  flora smile "Stop it or I'm telling [jo]!"
  flora skeptical "Hey!"
  flora skeptical "Stop!"
  $flora.equip('flora_vines')
  show flora embarrassed with Dissolve(.5) #equip until end of dialogue
  flora embarrassed "Stop that! I'm telling [jo]!"
  "Shit, what's happening? Does she need help?"
  flora embarrassed "[mc], that's not okay!"
  mc "It's not me! I'm all the way over here!"
  flora afraid "What...?"
  flora embarrassed "This isn't—"
  show flora afraid at disappear_to_left
  #SFX: door creaking/slamming shut
  flora "Ayeeeee!"
  "Uh, shit shit shit. Not good. Really not good!"
  $quest.flora_bonsai.advance("attack")
  return

label quest_flora_bonsai_attack: #this is the preview that the player sees when clicking on flora in free-roam
  $restrict = quest.flora_bonsai['r_vine_1']+quest.flora_bonsai['r_vine_2']+quest.flora_bonsai['r_vine_3']+quest.flora_bonsai['r_vine_4']+quest.flora_bonsai['r_vine_5']
  $attack = quest.flora_bonsai['a_vine_1']+quest.flora_bonsai['a_vine_2']+quest.flora_bonsai['a_vine_3']+quest.flora_bonsai['a_vine_4']+quest.flora_bonsai['a_vine_5']
  show flora bonsai_rape preview with Dissolve(.5)
  if not quest.flora_bonsai['a_vine_5']:
    flora "[mc]?! Help!!"
  else:
    flora "Mnff!!"
    "I guess she can't give me an earful while her mouth is full."

  if attack >= 2:
    "It seems like I might get to see something interesting if a few more vines headed [flora]'s way."

  if restrict >= 2:
    "If I can free her up a bit more I should be able to get that big vine to put her down."

  hide flora with Dissolve(.5)
  return

 # -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# end scene

label quest_flora_bonsai_ending_lust:
  define tree = Character('Tree', color="#007F00")
  $unlock_replay("flora_bonsai_rape")
  show flora bonsai_rape with Dissolve(.5) #(all assaulting and restricting vines)
  "The writhing mass of vines and roots constrict her helpless limbs."
  "Not sure what's gotten me tripping into this nightmare... meds? [flora]'s cake? Tanner hitting my face with a basketball? That can of pepelepsi last night?"
  "Either way, I should've brought popcorn... err, a shotgun. Yeah, that's what I meant."
  "And a chainsaw. And a can of lube... to err, make sure that the machinery runs smoothly."
  show flora bonsai_rape left with Dissolve(.5)
  show flora bonsai_rape right with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left with Dissolve(.5)
  show flora bonsai_rape right with Dissolve(.5)
  show flora bonsai_rape left with Dissolve(.5)
  "The vines slither around her boobs, milking and squeezing. Suckling on her nipples."
  "Seemingly... in an attempt to understand her anatomy."
  flora "{i}*Gurgle*{/}"
  "Maybe the right thing would be to help her, but what can a man really do against such reckless need?"
  "Even tree-monsters have needs... and as someone who's been deprived his whole life, I understand what he's going through."
  "When no female of your own species wants you, what are you supposed to do?"
  show flora bonsai_rape left with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left with Dissolve(.5)
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape right with Dissolve(.5)
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape ass left right with Dissolve(.5)
  "That vine slid right into her butt and lodged itself in her anal cavity and colon!"
  "If the vines controlling her limbs weren't enough to hold her in place, this certainly will keep her subdued."
  "You can't really blame it for being a pain in the ass either, it's just trying to secure the survival of its species."
  $set_dialog_mode("default_no_bg")
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left right ass with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left right ass with Dissolve(.5)
  $set_dialog_mode("")
  "This is probably what Darwin felt like, observing the life cycles of new species."
  "Surely, no harm is going to come to [flora], and she probably likes it. I've seen those Cthulhu posters on her door."
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape pussy_vine with Dissolve(.5)
  "Oh, I think it has found its target now."
  $set_dialog_mode("default_no_bg")
  show flora bonsai_rape ass with Dissolve(.5)
  show flora bonsai_rape pussy ass with Dissolve(.5)
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape pussy ass with Dissolve(.5)
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape pussy_deep with Dissolve(.5)
  show flora bonsai_rape ass with Dissolve(.5)
  show flora bonsai_rape pussy with Dissolve(.5)
  show flora bonsai_rape ass with Dissolve(.5)
  show flora bonsai_rape pussy with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape right with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left right pussy with Dissolve(.5)
  show flora bonsai_rape left right with Dissolve(.5)
  show flora bonsai_rape left right pussy ass with Dissolve(.5)
  $set_dialog_mode("")
  "Judging from the glistening sweat on [flora]'s skin, she's also getting into it."
  "Maybe reluctantly, but at least her body is responding to the tree's touch."
  "Fascinating to see mating and pollination up close. Truly, a marvel of science!"
  $set_dialog_mode("default_no_bg")
  show flora bonsai_rape left right pussy with Dissolve(.5)
  show flora bonsai_rape left right pussy_deep with Dissolve(.5)
  show flora bonsai_rape left right pussy with Dissolve(.5)
  show flora bonsai_rape right pussy_deep left right ass with Dissolve(.5)
  show flora bonsai_rape left right pussy with Dissolve(.5)
  show flora bonsai_rape left right pussy_deep with Dissolve(.5)
  show flora bonsai_rape left pussy with Dissolve(.5)
  show flora bonsai_rape left right pussy_deep with Dissolve(.5)
  $set_dialog_mode("")
  "Her sounds are all muted by the massive vine corking her throat."
  "Only whimpers and panting escape that massive member."
  "She's opening her legs more — a universal signal amongst all species that she's ready."
  "Here it comes..."
  $set_dialog_mode("default_no_bg")
  show flora bonsai_rape pussy with Dissolve(.5)
  show flora bonsai_rape pussy_deep with Dissolve(.5)
  show flora bonsai_rape right pussy with Dissolve(.5)
  show flora bonsai_rape right pussy_deep left right ass with Dissolve(.5)
  show flora bonsai_rape left pussy_deep ass with Dissolve(.5)
  show flora bonsai_rape right pussy with Dissolve(.5)
  show flora bonsai_rape left right pussy_deep with Dissolve(.5)
  show flora bonsai_rape left right pussy with Dissolve(.5)
  show flora bonsai_rape left right pussy_deep  left right ass with Dissolve(.5)
  $set_dialog_mode("")
  "Oh man, it's pollinating all her holes at once..."
  $set_dialog_mode("default_no_bg")
  show flora bonsai_rape left right mouth_cum with Dissolve(.5)
  show flora bonsai_rape left right mouth with Dissolve(.5)
  show flora bonsai_rape left right mouth_cum with Dissolve(.5)
  show flora bonsai_rape left right mouth with Dissolve(.5)
  show flora bonsai_rape left right mouth_cum with Dissolve(.5)
  show flora bonsai_rape left right mouth with Dissolve(.5)
  show flora bonsai_rape left right mouth_cum with Dissolve(.5)
  $set_dialog_mode("")
  flora "Gulp. Gulp. Gulp. Gulp..."
  $set_dialog_mode("default_no_bg")
  show flora bonsai_rape pussy left right mouth_cum with Dissolve(.5)
  show flora bonsai_rape pussy_deep left right mouth_cum with Dissolve(.5)
  show flora bonsai_rape pussy mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy_deep mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy mouth left ass with Dissolve(.5)
  show flora bonsai_rape with Dissolve(.5)
  show flora bonsai_rape pussy_deep mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy_deep mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy_deep mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy mouth_cum left right ass_cum with Dissolve(.5)
  show flora bonsai_rape pussy_cum mouth_cum left right ass_cum with Dissolve(.5)
  $set_dialog_mode("")
  "Wow. Both her stomach and womb got filled with bonsai seeds!"
  "Hopefully, she's on the pill. Having a bunch of tiny plant-floras running around would be annoying."
  "Hmm... maybe I can still save the situation somehow..."
  mc "Hey, bark brain! Let her go!"
  tree "..." #Tiny Thirsty Tree
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  hide flora with vpunch
  $quest.flora_bonsai["flora_down"] = True
  $school_nurse_room["tree_done"] = True
  $process_event("update_state")
  "Huh. It dropped her on the floor."
  "There's no way. Why didn't I try that to begin with?"
  "Where the hell is it going?"
  "..."
  #Fade in #Nurse's Office (normal)
  $set_dialog_mode("")
  hide black with Dissolve(.5)
  "It climbed out the window and disappeared into the forest..."
  $quest.flora_bonsai["flora_free"] = True
  $process_event("update_state")
  $flora.unequip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  $flora.unequip("flora_panties")
  $flora.unequip("flora_bra")
  $flora.unequip("flora_vines")
  $flora.equip("flora_cum")
  show flora embarrassed with Dissolve(.5) #naked, bandaid, dripping
  flora embarrassed "..."
  mc "Are you okay?"
  flora embarrassed "..."
  "She seems to be in shock... or maybe it's the afterglow of her earth-shattering orgasm."
  "Maybe she just needs some rest."
  "I'll just help her over to the bed. It's probably for the best if I neglect seeing any of this."
  "Honestly, I still feel like I hallucinated it."
  hide flora embarrassed with Dissolve(.5)
  $school_nurse_room["curtain_off"] = False #set back to true somepoint later
  "If she ever mentions it, I'll just pretend she's crazy. Surely, there aren't any trees this thirsty."
  $flora.equip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.equip("flora_skirt")
  else:
    $flora.equip("flora_pants")
  $flora.equip("flora_panties")
  $flora.equip("flora_bra")
  $flora.unequip("flora_blindfold")
  $flora.unequip("flora_cum")
  $flora["hidden_today"] = True
  $school_ground_floor_west["nurse_room_locked_today"] = "flora"
  $set_dialog_mode("")
  $mc["focus"] = ""
  jump goto_school_ground_floor_west

label quest_flora_bonsai_ending_love:
  $quest.flora_bonsai['flora_free'] = True
  $process_event("update_state")
  $flora.unequip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  $flora.unequip("flora_panties")
  $flora.unequip("flora_bra")
  $flora.unequip("flora_vines")
  show flora sad with dissolve #naked, bandaid
  mc "Are you okay?"
  flora sad "I think so... what was that?"
  mc "Just err... some complications."
  flora embarrassed "It's still here, isn't it?"
  mc "Sort of."
  mc "Get out of here, you sap-logged worm-wood!"
  #Fade to black
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  "Huh, it's actually running away, vines between its logs."
  "Can't believe that worked."
  $school_nurse_room["tree_done"] = True
  $process_event("update_state")
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  show flora concerned #naked, bandaid
  hide black with Dissolve(.5)
  $set_dialog_mode("")
  mc "It's gone now."
  flora concerned "What... what  was that?"
  mc "Just a very thirsty tree."
  flora sad "I thought it was going to..."
  mc "Well, I stopped it."
  flora confused "Did you really?"
  mc "Of course. I'm not letting some horny tree get its leaves on you."
  flora sad "I was really scared..."
  mc "Come here."
  show flora hugging_flora_sad_bandaid with Dissolve(.5)
  mc "It's okay. You're safe now."
  "Feels weird hugging her like this, but also... right somehow."
  "We've always been at each other's throats, but this is nice."
  "She's not the only one relieved. Not sure what I would've done if that thing had dragged her off into the forest."
  "Sometimes it's hard to admit, but I do care about her so much."
  show flora hugging_flora_openmouth_bandaid with Dissolve(.5)
  flora "Can you help me take the bandaid off? I don't wanna be blind anymore."
  show flora hugging_flora_sad_bandaid with Dissolve(.5)
  mc "Okay. I think it's been almost a week, right?"
  $flora.unequip("flora_blindfold")
  show flora hugging_flora_admiring with Dissolve(.5)
  "Seeing her looking up at me like this... it feels less real than the tree-monster itself."
  "Somehow, for the first time ever, admiration and relief fill her eyes."
  "Her whole body still trembles as she wraps her arms around me and scoots closer."
  "Guess I always figured that she'd be into this sort of thing, but maybe the reality is very different from the fantasy."
  flora "I'm glad you were around."
  mc "I'm glad too."
  "Knowing her, she'll have forgotten about this by the start of next week, but for once it's nice to be a hero in her eyes and not some worthless loser."
  "And maybe it's wrong to feel this way about her, but if I really tried my best, maybe I could make her happy?"
  show flora hugging_flora_content with Dissolve(.5)
  flora "Just hold me."
  "The urge to troll her is always going to be strong, but this seems like a good moment to just... be there for her."
  "She pulls me tighter and her heart thuds against my chest."
  "Maybe we can be more than this?"
  "Would she be okay with the world despising her? Probably not."
  mc "Whatever happens, I'll be there for you."
  "She just smiles, and that's all that matters."
  "I just need her to smile."
  $quest.flora_bonsai["hug"] = True
  $unlock_replay("flora_bonsai_hug")
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  $flora.equip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.equip("flora_skirt")
  else:
    $flora.equip("flora_pants")
  $flora.equip("flora_panties")
  $flora.equip("flora_bra")
  $process_event("update_state")
  $mc["focus"] = ""
  jump goto_school_ground_floor_west

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# item labels

label quest_flora_bonsai_bonsai_water(item):
  if item == "spray_pepelepsi":
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering =  Pepelepsi
    $school_nurse_room["tree"] = "_pepelepsi"
    "Here you go, little one. Yes, it tastes good, doesn't it? Nuclear reactor waste. Yum!"
    jump quest_flora_bonsai_spaghetti
  elif item == "spray_seven_hp":# Spray Bottle of 7 HP:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = 7 HP
    $school_nurse_room["tree"] = "_7hp"
    "How do you like that jock sweat? Oh, gulping it down? You'll grow up to be a bully, I'm sure."
    jump quest_flora_bonsai_spaghetti
  elif item == "spray_banana_milk":#Spray Bottle of Banana Milk:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Banana Milk
    $school_nurse_room["tree"] = "_milk"
    "Thick and nutritious! Like liquefied kumquat."
    jump quest_flora_bonsai_spaghetti
  elif item == "spray_strawberry_juice": #Spray Bottle of Strawberry Juice:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Strawberry Juice
    $school_nurse_room["tree"] = "_strawberry"
    "Your 1887 Strawberry Juice, matured in oak casks, sir."
    "Only the absolute finest for you."
    jump quest_flora_bonsai_spaghetti
  elif item == "spray_salted_cola": #Spray Bottle of Salted Cola:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Salted Cola
    $school_nurse_room["tree"] = "_saltedcola"
    "33\% salt and almost zero calories. Lots of protein though, for some reason. Oh you, like that?"
    jump quest_flora_bonsai_spaghetti
  elif item == "spray_urine": #Spray Bottle of Urine:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Urine
    $school_nurse_room["tree"] = "_urine"
    "A special brew from yours truly. Tasty? Oh, I knew you'd like it."
    jump quest_flora_bonsai_spaghetti
  elif item == "spray_water": #Spray Bottle of Water:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #Show water puddles on nurse's desk
    $school_nurse_room['puddle_today'] = True
    "Huh, that little bastard spat it all out and made a mess on the table. Clearly, it doesn't want water!"
  else:
    $quest.flora_bonsai.failed_item("bonsai_start",item)
    "If I could somehow liquefy my [item.title_lower], I'd be able to water the bonsai with it, but alas."
  return

label quest_flora_bonsai_gift(item):
  if item == "strawberry_juice":
    if not quest.flora_bonsai['ate_juice']:
      $quest.flora_bonsai['ate_juice'] = True
      $mc.remove_item(item)
      $mc.add_item("empty_bottle")
      show flora excited with Dissolve(.5)
      flora excited "For me?"
      mc "Of course. It's important to stay hydrated."
      flora excited "Thank you!"
      $flora.love+=3
      flora annoyed "I'm still craving something sweet though."
      hide flora with Dissolve(.5)
      return
    else:
      pass
  if item == "lollipop" or item=="lollipop_2":
    if not quest.flora_bonsai['ate_loli']:
      $quest.flora_bonsai['ate_loli'] = True
      $mc.remove_item(item)
      show flora skeptical with Dissolve(.5)
      flora skeptical "Is this a lollipop?"
      mc "Yes. You'll like it. I'll like it. Everyone's happy."
      flora skeptical "Can't say I'm surprised..."
      $flora.lust+=3
      flora annoyed "I'm still craving something sweet though."
      hide flora with Dissolve(.5)
      return
    else:
      pass
  elif item == "cake_slice":
    show flora neutral with dissolve
    "This is a bit cruel perhaps, but she'll never learn if I don't do this to her."
    "And if I'm ever going to get anywhere with her, she needs to understand that feeding someone her devil cake will have consequences."
    mc "Say, \"aaah.\""
    flora confused "You know you don't have to feed me, right?"
    mc "Be nice when people try to help you."
    flora confused "Fine, but if it's something disgusting, I'll scream."
    flora afraid "..."
    $mc.remove_item(item)
    flora skeptical "..."
    flora smile "Yum! It's good!"
    mc "It's your own cake."
    flora embarrassed "..."
    flora embarrassed "How could you?!"
    mc "You mean feeding an unsuspecting victim a highly addictive substance?"
    mc "Feels a bit hypocritical of you to complain about such a thing."
    flora angry "I'm going to kill you!"
    mc "Okay, I'm ready to meet my maker."
    flora angry "Not so fast. Tomorrow, we're baking cake, and I'll need your help."
    mc "It's better to fight the cravings."
    flora angry "I don't remember asking for your advice."
    mc "You can't bake blindfolded, and I'm saying no."
    flora confused "Why would you feed me the cake then?"
    mc "Just so you'll get to experience the withdrawal."
    mc "Maybe you'll reconsider baking this hell cake in the future."
    flora annoyed "You're the worst."
    $flora.love-=1
    mc "Yell if you need anything!"
    $quest.flora_bonsai.advance("nightvisit")
    $process_event("update_state")
    hide flora with Dissolve(.5)
    return
  show flora angry with Dissolve(.5)
  $quest.flora_bonsai.failed_item("refreshments",item)
  flora angry "What do you want me to do with this? Eat it?"
  "She threw it on the ground..."
  hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_sugar(item):
  if item == 'sugar_cube':
    $cubes = mc.owned_item_count('sugar_cube')
    if cubes<4:
      show flora neutral with Dissolve(.5)
      mc "Is this enough sugar?"
      flora sarcastic "Are you serious? That's not nearly enough."
      hide flora with Dissolve(.5)
    if cubes>=4 and cubes<6:
      show flora neutral with Dissolve(.5)
      mc "Surely, this is enough sugar?"
      flora annoyed "No!"
      mc "Well, can't you dip your finger into the dough or something?"
      flora confused "Are you calling me sweet or trying to eat me?"
      mc "Maybe... both?"
      flora annoyed "Gross. More sugar!"
      hide flora with Dissolve(.5)
    if cubes>=6:
      show flora neutral with Dissolve(.5)
      mc "Okay, this has to be enough sugar."
      flora blush "I think it might just be!"
      $mc.remove_item("sugar_cube",cubes)
      "Seeing [flora] dancing around the stove in her undies is almost hypnotizing."
      "Just whip your ass back and forth! Whip your ass back and forth!"
      flora annoyed "Hello? Are you going to help me over to the stove?"
      mc "Yeah, err... of course. Just got lost in my thoughts there for a moment."
      $quest.flora_bonsai["florabaking"] = True
      $process_event("update_state")
      hide flora with Dissolve(.5)
      "She'll probably need more help than this, so I'll just have to stay and watch her work I guess."
      "Nothing weird about that. Just watching her bake. Nothing weird at all."
      $set_dialog_mode("default_no_bg")
      show black with fade
      call event_show_time_passed_screen
      $game.advance()
      hide black with fade
      show flora excited with dissolve
      $set_dialog_mode("")
      $quest.flora_bonsai["florabaking"] = False
      flora excited "It's done!"
      mc "Hey, great. Let's go back to bed."
      flora skeptical "But I want to celebrate."
      mc "Have a slice then, but if you want help getting up the stairs, hurry up!"
      flora annoyed "Can't a girl enjoy a slice of cake in peace?"
      show flora sad with dissolve #trying to simulate chewing
      pause 1.0
      show flora skeptical with dissolve #this needs work, not sure what to do here TBD
      pause 0.5
      show flora blush flip with dissolve
      pause 1.0
      # show flora sad with dissolve
      # show flora skeptical with dissolve
      # show flora sad with dissolve
      # show flora skeptical with dissolve
      flora smile "So good! You should have a bite!"
      mc "Pass."
      flora smile "Suit yourself."
      show flora eyeroll with dissolve
      pause 1.0
      show flora sad with dissolve
      show flora skeptical with dissolve
      pause 1.0
      show flora eyeroll with dissolve
      show flora skeptical with dissolve
      pause 0.5
      show flora blush flip with dissolve
      pause 1.0
      flora smile "Omnomnomnom!"
      mc "All right. Time to go to bed."
      flora excited flip "Okay, fine. My cake cravings have been sated!"
      flora excited flip "Help me up the stairs."
      show flora excited flip at disappear_to_left
      call goto_home_hall
      show flora confident at move_to(.5) with moveinleft
      flora confident "Thank you. I've got it from here."
      mc "Are you sure? I could tuck you in."
      flora concerned "How? You're banned from my room, remember?"
      mc "Thought you could make an exception."
      flora confident "Sorry, not happening. Night!"
      hide flora confident with dissolve
      #SFX: door creak
      "Well, it's probably time for me to hit the sack as well."
      $flora.equip("flora_shirt")
      if quest.flora_handcuffs > "package":
        $flora.equip("flora_skirt")
      else:
        $flora.equip("flora_pants")
      $quest.flora_bonsai.advance("sleep")
  else:
    show flora thinking with Dissolve(.5)
    mc "I can't find the sugar."
    flora thinking "I'm sure it's hiding somewhere around the house."
    $quest.flora_bonsai.failed_item("flora_bonsai",item)
    hide flora with Dissolve(.5)
  return

label quest_flora_bonsai_water2(item):
  if item == "spray_pepelepsi":
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    $school_nurse_room["vine"] = "_pepelepsi"
    "The devil in a can — pour it out in a pentagram to achieve the full nutritional benefits!"
    jump quest_flora_bonsai_knock_knock
  elif item == "spray_seven_hp":# Spray Bottle of 7 HP:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = 7 HP
    $school_nurse_room["vine"] = "_7hp"
    "Sweaty socks and fresh lemon! The ultimate workout combination!"
    jump quest_flora_bonsai_knock_knock
  elif item == "spray_banana_milk":#Spray Bottle of Banana Milk:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Banana Milk
    $school_nurse_room["vine"] = "_milk"
    "Straight from the udders of the Banana Cow. Suckle it, my thirsty tree."
    jump quest_flora_bonsai_knock_knock
  elif item == "spray_strawberry_juice": #Spray Bottle of Strawberry Juice:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Strawberry Juice
    $school_nurse_room["vine"] = "_strawberry"
    "Like fine wine, but actually tasty. Surely, a tree of your stature would appreciate such an acquired taste."
    jump quest_flora_bonsai_knock_knock
  elif item == "spray_salted_cola": #Spray Bottle of Salted Cola:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Salted Cola
    $school_nurse_room["vine"] = "_saltedcola"
    "Filled with minerals. Salt to be exact. Perfect for any growing plant!"
    jump quest_flora_bonsai_knock_knock
  elif item == "spray_urine": #Spray Bottle of Urine:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #first watering = Urine
    $school_nurse_room["vine"] = "_urine"
    "A lovely combination of odor and taste. Few plants and people can say no."
    jump quest_flora_bonsai_knock_knock
  elif item == "spray_water": #Spray Bottle of Water:
    $mc.remove_item(item)
    $mc.add_item("spray_empty_bottle")
    #SFX: Spray bottle sound
    #Show water puddles on nurse's desk
    $school_nurse_room['puddle_today'] = True
    "Huh, that little bastard spat it all out and made a mess on the table. Clearly, it doesn't want water!"
  else:
    $quest.flora_bonsai.failed_item("bonsai",item)
    "If I could somehow liquefy my [item.title_lower], I'd be able to water the bonsai with it, but alas."
  return
