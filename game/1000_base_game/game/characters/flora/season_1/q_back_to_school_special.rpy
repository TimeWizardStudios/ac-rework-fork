label flora_quest_back_to_school_special:
  show flora angry with Dissolve(.5)
  flora angry "Breakfast!"
  "In a way, it's nice to see [flora] so animated. After our fight at the end of senior year, she stopped talking to me."
  "[flora] turning cold and indifferent was always one of my biggest regrets."
  mc "Hi, [flora]! How have you been?" 
  flora annoyed "What? Since last night? Someone made a lot of noise again. What were you even doing up that late?"
  "Things that no woman should ever know about. Least of all, [flora]."
  flora angry "Anyway, where's the breakfast?"
  mc "The breakfast?"
  flora angry "Yes! Did you make the breakfast yet?"
  show flora angry at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Me? I just got down here.\"":
      show flora angry at move_to(.5)
      mc "Me? I just got down here."
      $flora.lust-=1
      flora eyeroll "Typical."
    "\"Make your own breakfast!\"":
      show flora angry at move_to(.5)
      mc "Make your own breakfast!"
      $flora.lust+=1
      flora concerned "Selfish as always. Whatever, fine."
    "\"[jo] said that you should do it from now on.\"":
      show flora angry at move_to(.5)
      $mc.intellect+=1
      $mc.charisma-=1
      mc "[jo] said that you should do it from now on."
      flora skeptical "What? You're lying."
      mc "Ask her."
      flora angry "Typical of her to give me all the responsibility!"
  "She seems entirely unaware. It must just be me, then."
  "Several years have passed for me, but for her it's only a few hours."
  "[flora] actually went off and did something with her life. She probably never felt the need to come home and visit me."
  "Our lives couldn't have turned out any more different. Both [jo] and [flora] were always successful and driven."
  flora annoyed "I'm just going to take this..."
  show flora kitchen_cereals_1 with fadehold
  "When you haven't had a single positive female interaction in years... it's hard not to peep, even though it's wrong."
  "Her frilly little panties always seemed so alluring."
  "How they follow the contours of her butt so perfectly. Hiding and showing everything at the same time."
  "She's so casual in her skimpy ways. Coming down to breakfast in her underwear."
  "Sometimes showering with the door unlocked. Forgetting to bring a towel. Thinking that her naked tip-toeing past my door goes unnoticed."
  "Sucking on a popsicle, half-dressed on her bed. Playing with her hair or legs, without a care in the world."
  "Maybe it's the taboo of it that gets me?"
  flora kitchen_cereals_2 "What are you looking at?"
  "Huh? Crap..."
  menu(side="right"):
    extend ""
    "\"Nothing!\"":
      mc "Nothing!"
      $flora.lust-=1
      flora kitchen_cereals_1 "Ugh, perv."
    "\"Looked like you needed some help reaching...\"":
      $mc.charisma+=1
      mc "Looked like you needed some help reaching..."
      flora kitchen_cereals_1 "Oh, it's okay. I got it."
    "\"Don't touch my cereal!\"":
      mc "Don't touch my cereal!"
      $flora.lust+=1
      flora kitchen_cereals_2 "Try to stop me!"
      mc "..."
      flora kitchen_cereals_1 "Yeah, didn't think so."
  $unlock_replay("flora_cereal")
  show flora confident with Dissolve(.5)
  "It's strange to see her here again, having a somewhat normal conversation."
  "[flora] used to be so annoying back in the day, but I guess I still missed her... but she doesn't need to know that."
  flora sarcastic "Hey, I have to do this questionnaire for the company I'm interning at this semester. You're going to be my first victim."
  "The first day of school and she's already got opportunities lined up... same old [flora]."
  "Although, not quite. She never asked me for help in the past."
  "Might as well humor her."
  flora angry "Are you listening? You better take this seriously, [mc]!"
  mc "Yes, I'm listening!"
  flora neutral "Okay, answer these questions to the best of your ability."
  flora neutral "First question. You're stuck in a room. What's the first thing you do?"
  show flora neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I open the door.\"":
      show flora neutral at move_to(.5)
      $mc.intellect+=1
      mc "I open the door."
      flora thinking "..."
      flora flirty "I didn't think you'd pick up on that! I never actually said the door was locked."
      mc "I figured it would be some trick question bullshit like that."
      flora annoyed "..."
    "\"I break down the door.\"":
      show flora neutral at move_to(.5)
      $mc.strength+=1
      mc "I break down the door."
      flora annoyed "Of course you do."
    "\"Am I alone?\"":
      show flora neutral at move_to(.5)
      $mc.charisma+=1
      $flora.lust+=1
      mc "Am I alone?"
      flora skeptical "What do you mean, \"are you alone?\""
      mc "Like, is there someone else there who can help? Preferably, someone hot and female?"
      mc "Maybe we don't even need to get out?"
      flora annoyed "Oh my god. You're the worst!"
      mc "That's my answer!"
      flora annoyed "Fine. Whatever."
  flora neutral "Okay, next question!"
  flora neutral "For the last few months you've been sacrificing goats in the company server room."
  flora neutral "Your boss is on the way over, shouting, \"What the hell do you think you're doing?\""
  flora neutral "What's your response?"
  show flora neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"With all due respect, ma'am. I'm an expert in my field, and you hired me for a reason. I'm merely doing my job.\"":
      show flora neutral at move_to(.5)
      $mc.charisma+=1
      mc "With all due respect, ma'am. I'm an expert in my field, and you hired me for a reason. I'm merely doing my job."
      flora skeptical "And you think she'd fall for that?"
      mc "Fall for what? The servers are running fine. Better than ever, in fact."
    "\"I'd serve her a glass of wine and a CPU-cooked medium-rare goat sirloin with garlic butter.\"":
      show flora neutral at move_to(.5)
      $mc.charisma+=1
      $mc.love+=1
      mc "I'd serve her a glass of wine and a CPU-cooked medium-rare goat sirloin with garlic butter."
      flora skeptical "I'm sorry... what?"
      mc "Mrs. Shumacher is a lady of refined tastes. She'd appreciate the gesture."
      flora annoyed "Did you just make up a name for your hypothetical boss?"
      $unlock_stat_perk("love1")
    "\"I'd wrestle her down and tie her to the altar. The goat sacrifices haven't improved the speed of the servers. Maybe this will...\"":
      show flora neutral at move_to(.5)
      $mc.strength+=1
      $flora.lust+=1
      mc "I'd wrestle her down and tie her to the altar. The goat sacrifices haven't improved the speed of the servers. Maybe this will..."
      flora afraid "Tell me you're joking!"
      mc "Anything for the company, [flora]. {i}Anything{/} for the company..."
    "\"A wild beast entered the server room and started chewing on the cables, ma'am! I had to put it down, or risk losing our data!\"":
      show flora neutral at move_to(.5)
      mc "A wild beast entered the server room and started chewing on the cables, ma'am! I had to put it down, or risk losing our data!"
      $mc.intellect+=1
      flora cringe "So, you'd lie to your boss?"
      mc "It's for her own good, [flora]. She wouldn't understand the intricacies and sacrifices needed to run a server room."
  flora annoyed "I can't believe we're the same species."
  flora annoyed "All right, next question."
  flora smile "You're walking through the woods with a basket of goodies when you come to a fork."
  flora smile "You know that if you go left, the road will be long and tough, but it's said that beautiful nymphs await the travelers that can reach them."
  flora confident "You also know that if you go right, the road will be short and easy, and you'll make it home safely."
  flora confident "What do you do?"
  show flora confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"What am I? The Little Red Riding Hood?\"":
      show flora confident at move_to(.5)
      mc "What am I? The Little Red Riding Hood?"
      flora annoyed "Can you take this seriously for one minute? I'm already getting fed up with your shit."
    "\"So many options... so many factors to consider...\"":
      show flora confident at move_to(.5)
      mc "So many options... so many factors to consider..."
      flora annoyed "Come on, stop wasting time!"
  mc "Okay! Relax. Can you repeat the question, please?"
  flora annoyed "No. What do you do?"
  show flora annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I go left.\"":
      show flora annoyed at move_to(.5)
      $mc.lust+=1
      mc "I go left."
      "Seems like that's a no brainer. A bunch of beautiful nymphs sounds great."
      "Not exactly sure what a nymph looks like, but my mind goes to naked women bathing in a peaceful pond, tossing me shy smiles over bare shoulders."
      flora sarcastic "I saw that choice coming from a mile away, pervert."
      mc "Hmph..."
      $unlock_stat_perk("lust1")
    "\"I go right.\"":
      show flora annoyed at move_to(.5)
      $mc.intellect+=1
      mc "I go right."
      "The smartest thing to do would probably be to go home..."
      "Besides, aren't nymphs those creatures that drag you underwater to your death? Or wait, are those sirens...?"
      "Either way, with my luck, I probably won't even make it there."
      flora sarcastic "Coward."
      "Can't ever win with her..."
    "\"I pick up the fork and eat the goodies in the basket.\"":
      show flora annoyed at move_to(.5)
      $mc.charisma+=1
      mc "I pick up the fork and eat the goodies in the basket."
      flora afraid "..."
      mc "..."
      flora cringe "..."
      flora cringe "I'm going to pretend you didn't just say that."
  flora thinking "Okay, let's move on."
  flora excited "There's an old pizza box on the floor of your room."
  mc "..."
  flora neutral "..."
  mc "Is there a question?"
  flora confident "No, just a random observation I made earlier."
  mc "Well, if you're interested, it still contains a few pieces of dried-up crust. In case I get hungry."
  flora cringe "Eww! What's wrong with you?"
  flora annoyed "Ugh. Next question..."
  flora annoyed "You're in Antarctica, trying to assemble a team of highly trained butlers. One of them just turned to you and said, \"Noot-noot!\""
  flora annoyed "What do you do?"
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I realize I'm in a Pingu skit and start tearing down the 4th wall to punch whoever wrote this question.\"":
      show flora annoyed at move_to(.5)
      $mc.intellect+=1
      $mc.strength+=1
      mc "I realize I'm in a Pingu skit and start tearing down the 4th wall to punch whoever wrote this question."
      flora afraid "That's... not what I expected from you."
      mc "Pingu is sacred."
    "\"I look him dead in the eye, puffing out my chest, slowly shaping my mouth into a trumpet... 'NOOT-NOOT!'\"":
      show flora annoyed at move_to(.5)
      $mc.strength+=1
      $mc.charisma+=1
      mc "I look him dead in the eye, puffing out my chest, slowly shaping my mouth into a trumpet... \"NOOT-NOOT!\""
      flora laughing "The rock-bottom of stupidity has been reached."
      "Not sure what she expected with a question like that."
    "\"I pull out my penguin hunting rifle and scream, 'Spies! They've infiltrated the butler academy!'\"":
      show flora annoyed at move_to(.5)
      $mc.lust+=1
      $mc.intellect-=1
      mc "I pull out my penguin hunting rifle and scream, \"Spies! They've infiltrated the butler academy!\""
      flora afraid "You'd shoot an innocent penguin?"
      mc "Err... let's just move on."
      $unlock_stat_perk("lust2")
  flora thinking "Okay, final question."
  flora flirty "Who's your ideal lover?"
  show flora flirty at move_to(.25)
  menu(side="right"):
    extend ""
    "\"These questions don't seem related at all. What sort of internship are you doing, exactly?\"":
      show flora flirty at move_to(.5)
      mc "These questions don't seem related at all. What sort of internship are you doing, exactly?"
      flora annoyed "None of your business. Just answer the question!"
    "\"Should you really be asking me this?\"":
      show flora flirty at move_to(.5)
      mc "Should you really be asking me this?"
      flora annoyed "Stop making it weird! You always make it weird."
      flora annoyed "It's the last question. Can we just get this over with?"
  show flora annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Someone hot and driven.\"":
      show flora annoyed at move_to(.5)
      $mc.strength+=1
      $mc.lust+=1
      $lindsey.lust+=1
      mc "Someone hot and driven."
      flora eyeroll "I don't know why I expected anything else."
      $unlock_stat_perk("lust3")
    "\"Someone cute and kind.\"":
      show flora annoyed at move_to(.5)
      $mc.love+=1
      $isabelle.lust+=1
      mc "Someone cute and kind."
      flora sarcastic "How cliche."
      $unlock_stat_perk("love2")
    "\"Someone smart and sophisticated.\"":
      show flora annoyed at move_to(.5)
      $mc.intellect+=1
      $jacklyn.lust+=1
      mc "Someone smart and sophisticated."
      flora sarcastic "I guess you need someone smart to balance out your stupidity."
      mc "Hey..."
    "\"Someone confident.\"":
      show flora annoyed at move_to(.5)
      $mc.charisma+=1
      $kate.lust+=1
      mc "Someone confident."
      flora confused "Really? Not what I expected from you."
      mc "Thanks, I guess?"
    "\"Someone experienced.\"":
      show flora annoyed at move_to(.5)
      $mrsl.lust+=1
      $jo.lust+=1
      mc "Someone experienced."
      flora sarcastic "Yeah, one of you needs to know what they're doing."
      "Did I say that I missed [flora]? I take it back."
    "\"Someone who has a vagina...\"":
      show flora annoyed at move_to(.5)
      $mc.charisma-=1
      $flora.lust+=1
      mc "Someone who has a vagina..."
      flora cringe "Wow. Just... wow."
      mc "Hey, beggars can't be choosers."
  flora neutral "Okay, that's all. My friends will pick me up soon, so I have to go and get ready. Don't bother me."
  show flora neutral at disappear_to_left
  $flora.activity="left"
  "All right. So, this is what it's like being back in high school again. [flora] is clearly unaware that we've both reverted to our younger selves."
  "Seems like I'm the only one who knows."
  "And that means..."
  "Well..."
  "It could mean anything, really."
  "..."
  "Figuring out time travel will just have to take a backseat for now. Got to get to school on time."
  $quest.back_to_school_special.advance("ready_to_leave")
  return
