label flora_showering_interact:
  $flora["outfit_stamp"] = flora.outfit
  $flora.outfit = {}
  show flora afraid with Dissolve(.5)
  flora afraid "Who is that?"
  mc "Sorry, I just have to take a piss."
  flora skeptical "I'm taking a shower!"
  mc "I'll be quick..."
  flora embarrassed "Get out! Get out!"
  flora embarrassed "Aaaaaaaah!"
  window hide None
  play sound "<from 9.2 to 10.0>door_breaking_in"
  $game.location = "home_hall"
  scene location with vpunch
  $flora.outfit = flora["outfit_stamp"]
  $home_hall["bathroom_locked_now"] = True
  return

label flora_talk_back_to_school_special:
  show flora excited with dissolve
  flora "'Sup?"
  "Oh, god. She's doing that thing again..."
  hide flora excited with qdis
  $renpy.pause(0.5)
  return

label flora_flirt_back_to_school_special:
  show flora annoyed with dissolve
  flora "Don't look at me like that. It's creepy."
  hide flora annoyed with qdis
  $renpy.pause(0.5)
  return
