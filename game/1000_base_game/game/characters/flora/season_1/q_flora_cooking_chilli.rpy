label flora_cooking_chilli_drawer_interact:

  if quest.flora_cooking_chilli >= "chilli_recipe_step_eight":

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 4:
      jump flora_quest_flora_cooking_chilli_drawer_check_with_flora

    "You picked up a cooking item. Is it the right one? No one knows."
    $quest.flora_cooking_chilli["drawers_interacted_with"] +=1

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
      $ quest.flora_cooking_chilli["step_eight_first_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      $ quest.flora_cooking_chilli["step_eight_second_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 3:
      $ quest.flora_cooking_chilli["step_eight_third_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 4:
      $ quest.flora_cooking_chilli["step_eight_fourth_drawer"] = quest.flora_cooking_chilli["drawer"]
      $ quest.flora_cooking_chilli.advance("check_with_flora")
    return

  if quest.flora_cooking_chilli >= "chilli_recipe_step_seven":

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 3:
      jump flora_quest_flora_cooking_chilli_drawer_check_with_flora

    "You picked up a cooking item. Is it the right one? No one knows."
    $ quest.flora_cooking_chilli["drawers_interacted_with"] +=1

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
      $ quest.flora_cooking_chilli["step_seven_first_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      $ quest.flora_cooking_chilli["step_seven_second_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 3:
      $ quest.flora_cooking_chilli["step_seven_third_drawer"] = quest.flora_cooking_chilli["drawer"]
      $ quest.flora_cooking_chilli.advance("check_with_flora")
    return

  if quest.flora_cooking_chilli >= "chilli_recipe_step_five":

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      jump flora_quest_flora_cooking_chilli_drawer_check_with_flora

    "You picked up a cooking item. Is it the right one? No one knows."
    $ quest.flora_cooking_chilli["drawers_interacted_with"] +=1

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
      $ quest.flora_cooking_chilli["step_five_first_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      $ quest.flora_cooking_chilli["step_five_second_drawer"] = quest.flora_cooking_chilli["drawer"]
      $ quest.flora_cooking_chilli.advance("check_with_flora")
    return

  if quest.flora_cooking_chilli >= "chilli_recipe_step_four":

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 3:
      jump flora_quest_flora_cooking_chilli_drawer_check_with_flora

    "You picked up a cooking item. Is it the right one? No one knows."
    $ quest.flora_cooking_chilli["drawers_interacted_with"] +=1

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
      $ quest.flora_cooking_chilli["step_four_first_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      $ quest.flora_cooking_chilli["step_four_second_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 3:
      $ quest.flora_cooking_chilli["step_four_third_drawer"] = quest.flora_cooking_chilli["drawer"]
      $ quest.flora_cooking_chilli.advance("check_with_flora")
    return

  if quest.flora_cooking_chilli >= "chilli_recipe_step_one":

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      jump flora_quest_flora_cooking_chilli_drawer_check_with_flora

    "You picked up a cooking item. Is it the right one? No one knows."
    $ quest.flora_cooking_chilli["drawers_interacted_with"] +=1

    if quest.flora_cooking_chilli["drawers_interacted_with"] == 1:
      $ quest.flora_cooking_chilli["step_one_first_drawer"] = quest.flora_cooking_chilli["drawer"]
    if quest.flora_cooking_chilli["drawers_interacted_with"] == 2:
      $ quest.flora_cooking_chilli["step_one_second_drawer"] = quest.flora_cooking_chilli["drawer"]
      $ quest.flora_cooking_chilli.advance("check_with_flora")
    return

label flora_quest_flora_cooking_chilli_start:
  show flora excited with Dissolve(.5)
  flora excited "I've been looking for you!"
  mc "Okay, now I'm officially worried."
  flora concerned "What's that supposed to mean?"
  flora concerned "Whatever. I don't want to know."
  mc "You really don't."
  flora cringe "What did you do?"
  mc "What didn't I do?"
  flora cringe "Gross."
  "I'd love to know what images just popped up in her head."
  "She really does have a low opinion of me. The question is, how low?"
  mc "So, why were you looking for me?"
  flora excited "I've decided to cook for the place I'm interning at this fall."
  flora concerned "I need you to pick up the big pot in the school kitchen and bring it home."
  flora concerned "It's too heavy for me."
  flora confident "And if you don't do it, I'll tell [jo] you broke into the bathroom when I was showering again."
  mc "You wouldn't..."
  flora excited "She said she'd cut off your internet cord if you did it again, remember?"
  mc "I do remember that... like it was yesterday."
  flora angry "It was yesterday!"
  "Ugh, time travel certainly does mess with your memory."
  mc "Fine, I'll do it."
  flora excited "Good!"
  hide flora with Dissolve(.5)
  $quest.flora_cooking_chilli.start()
  $ quest.flora_cooking_chilli.advance("cooking_pot")
  return

label flora_quest_flora_cooking_chilli_give_pot:
  $flora.equip("flora_hat")
  if quest.flora_handcuffs > "package":
    $flora.equip("flora_skirt")
  else:
    $flora.equip("flora_pants")
  show flora annoyed with Dissolve(.5)
  flora "That took some time."
  flora "Did you at least bring the pot?"
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yes, here you go!\"":
      show flora annoyed at move_to(.5)
      mc "Yes, here you go!"
      $mc.remove_item("cooking_pot")
      flora afraid "Just like that?"
      flora afraid "No annoying back and forth? No arguing?"
      mc "I think you have me confused with someone else."
      flora concerned "I think not."
      mc "Well, no funny business this time. I'm trying to get out of my old habits."
      $flora.love+=1
      flora smile "That's good to hear."
    "\"Yes, but you're going to have to pay me in food.\"":
      show flora annoyed at move_to(.5)
      mc "Yes, but you're going to have to pay me in food."
      flora laughing "Oh, I was hoping you'd be my guinea pig!"
      mc "Err... on second thought—"
      flora laughing "Too late! You're going to eat and you're going to smile..."
      flora angry "...or I'm telling [jo] you ruined my cooking. I know you're on thin ice right now."
      mc "Ugh, you're such a freaking child. Fine..."
      flora excited "Sweet."
      $mc.remove_item("cooking_pot")
    "\"Don't talk to me like that.\"":
      show flora annoyed at move_to(.5)
      mc "Don't talk to me like that."
      $flora.lust+=1
      flora confident "What are you going to do about it?"
      mc "I'll release your ant farm again."
      flora embarrassed "You can't! They're not ready to face the real world!"
      flora embarrassed "You'll be in so much trouble!"
      mc "Some things are worth suffering for."
      flora sad "Okay, I'm sorry. Thanks for bringing the pot!"
      mc "You're welcome!"
      $mc.remove_item("cooking_pot")
  show flora smile at move_to(.6)
  $home_kitchen["cooking_pot"] = True
  flora smile "I'm just going to put this here."
  show flora smile at move_to(.5)
  mc "What are you cooking anyway?"
  flora thinking "I'm making chili."
  flora flirty "Extra hot."
  "Damn it, [flora]. Why do you have to do this to me?"
  "It's so hard to think straight when she's always teasing."
  flora neutral "I found the recipe while surfing on your computer."
  mc "What?"
  mc "How did you get on my computer?"
  flora confident "You left it logged in while going to the bathroom. You have a weird web browser, by the way!"
  mc "Ugh. I forgot how annoying you are."
  mc "Wait a minute. Did you use the Tor browser?"
  flora neutral "Yep."
  mc "You found the recipe on the dark web?"
  flora excited "It was cheap, too! Only had to pay five bitcoins."
  mc "Five bitcoins?! What?!"
  flora laughing "I'm kidding! I'm not as clueless as you think."
  flora laughing "You should've seen your face!"
  flora excited "Anywho! I'm going to start cutting up the chili peppers."
  flora "Can you hand me a knife first and then a cutting board, please?"
  $mc["focus"] = "flora_cooking_chilli"
  hide flora with Dissolve(.5)
  $ quest.flora_cooking_chilli["incorrect_count"] = 0
  $ quest.flora_cooking_chilli["chilli_step"] = "one"
  $ quest.flora_cooking_chilli["drawers_interacted_with"] = 0
  $ quest.flora_cooking_chilli["current_items"] = "a knife and a cutting board"
  $ quest.flora_cooking_chilli.advance("chilli_recipe_step_one")
  return

label flora_quest_flora_cooking_chilli_chilli_recipe_step_one:
  $flora.equip("flora_hat")
  if flora["cooking_chilli_step_one"] == "incorrect":
    show flora sad with Dissolve(.5)
    flora sad "[mc]..."
    flora sad "We haven't even made it past the first step."
    flora afraid "I'm not sad. I'm just disappointed."
    flora sad "Please bring me a knife first and then a cutting board."
    flora sad "In that order."
  else:
    show flora neutral with Dissolve(.5)
    mc "So, I brought you the pot. Can I go to my room now?"
    flora angry "Weren't you listening?! I need to start cutting the chili."
    flora angry "Bring me a knife first and then a cutting board."
    flora annoyed "In that order!"
    $quest.flora_cooking_chilli["current_items"] = "a knife and a cutting board"
  hide flora with Dissolve(.5)
  return

label flora_quest_flora_cooking_chilli_drawer_check_with_flora:
  "I already got enough items. I should give them to [flora]."
  $ quest.flora_cooking_chilli.advance("check_with_flora")
  return

label flora_quest_flora_cooking_chilli_step_one_evaluate:

  if quest.flora_cooking_chilli["chilli_step"] == "eight":
    if (quest.flora_cooking_chilli["step_eight_first_drawer"] == 'd2' and quest.flora_cooking_chilli["step_eight_second_drawer"] == 'd3' and quest.flora_cooking_chilli["step_eight_third_drawer"] == 'd3' and quest.flora_cooking_chilli["step_eight_fourth_drawer"] == 'c1'):
      show flora excited with Dissolve(.5)
      flora excited "Finally!"
      flora excited "I mean, you could certainly have done that faster, but I appreciate the help!"
      mc "Gee, thanks."
      flora neutral "I think this is going to be the best chili ever!"
      flora neutral "The people at my internship are going to love it!"
      flora excited "Want to have a taste?"
      show flora excited at move_to(.25)
      menu(side="right"):
        extend ""
        "\"No, thanks. I think I'll pass.\"":
          show flora excited at move_to(.5)
          $mc.intellect+=1
          mc "No, thanks. I think I'll pass."
          $flora.love-=1
          flora angry "Rude."
          flora angry "Fine, more for me!"
          "Probably dodged a bullet there."
        "\"Okay, sure. How bad can it be?\"":
          show flora excited at move_to(.5)
          $mc.intellect-=1
          mc "Okay, sure. How bad can it be?"
          $flora.love+=1
          flora confident "Okay. Choo-choo! Open up, here comes the train!"
          mc "Can you not?"
          flora concerned "Fine."
          "Probably going to regret this later, but here goes..."
          mc "..."
          flora excited "Yummy?"
          mc "{i}*Cough, cough*{/}"
          mc "Yes, lovely..."
          flora laughing "Yay!"
      jump flora_quest_flora_cooking_chilli_end
    else:
      jump flora_quest_flora_cooking_chilli_incorrect

  elif quest.flora_cooking_chilli["chilli_step"] == "seven":
    if (quest.flora_cooking_chilli["step_seven_first_drawer"] == 's1' and quest.flora_cooking_chilli["step_seven_second_drawer"] == 'c1' and quest.flora_cooking_chilli["step_seven_third_drawer"] == 'd1'):
      show flora angry with Dissolve(.5)
      flora angry "Okay, that should do the trick."
      flora angry "Now, I just need to purge these spoons of your unholy taint."
      mc "Make sure you cite the right bible verses..."
      flora confident "Oh, I know them by heart."
      flora confident "While I perform this exorcism, bring me a pan, a shot glass, a measuring beaker, and flour. In that order."
      mc "Whatever you say, chef."
      hide flora with Dissolve(.5)
      $ quest.flora_cooking_chilli["chilli_step"] = "eight"
      $ quest.flora_cooking_chilli["drawers_interacted_with"] = 0
      $ quest.flora_cooking_chilli["current_items"] = "a pan, a shot glass, a measuring beaker, and flour"
      $ quest.flora_cooking_chilli.advance("chilli_recipe_step_eight")
    else:
      jump flora_quest_flora_cooking_chilli_incorrect

  elif quest.flora_cooking_chilli["chilli_step"] == "five":
    if (quest.flora_cooking_chilli["step_five_first_drawer"] == 'd1' and quest.flora_cooking_chilli["step_five_second_drawer"] == 'd1'):
      show flora smile with Dissolve(.5)
      flora smile "Thank you."
      mc "I licked one of them."
      flora cringe "Why?"
      mc "I thought it'd bring some extra flavor."
      flora cringe "Gross."
      flora cringe "I'll have to wash it now."
      mc "That's the price you pay for making me help."
      flora annoyed "Okay, while I clean this off. Put a spoonful of spices in the pot."
      $ mc.add_item("spoon")
      $ quest.flora_cooking_chilli.advance("chilli_recipe_step_six")
      hide flora with Dissolve(.5)
    else:
      jump flora_quest_flora_cooking_chilli_incorrect

  elif quest.flora_cooking_chilli["chilli_step"] == "four":
    if (quest.flora_cooking_chilli["step_four_first_drawer"] == 's1' and quest.flora_cooking_chilli["step_four_second_drawer"] == 'd2' and quest.flora_cooking_chilli["step_four_third_drawer"] == 'c1'):
      show flora afraid with Dissolve(.5)
      flora afraid "Did you get the right spices?"
      mc "You didn't specify so I brought the whole collection..."
      flora eyeroll "Okay, that's better than none."
      $ mc.add_item("spice")
      flora eyeroll "Put on the apron now."
      mc "Me? I thought it was for you."
      flora confident "You're the clumsy one here."
      mc "Ugh. What about the grater?"
      flora thinking "Did I say a grater? Well, I don't need it."
      mc "..."
      hide flora with Dissolve(.5)
      flora laughing "Bring me two spoons instead!"
      $ quest.flora_cooking_chilli["chilli_step"] = "five"
      $ quest.flora_cooking_chilli["drawers_interacted_with"] = 0
      $ quest.flora_cooking_chilli["current_items"] = "two spoons"
      $ quest.flora_cooking_chilli.advance("chilli_recipe_step_five")
    else:
      jump flora_quest_flora_cooking_chilli_incorrect

  else:
    if (quest.flora_cooking_chilli["step_one_first_drawer"] == 'd1' and quest.flora_cooking_chilli["step_one_second_drawer"] == 'd2'):
      show flora excited with Dissolve(.5)
      flora excited "Thank you!"
      mc "So, I'm free to leave?"
      flora concerned "No! I need more help!"
      flora concerned "While I cut the chili peppers, you're going to cut the onion."
      mc "What if I slice off my fingers?"
      flora excited "Then we're adding them to the pot!"
      flora neutral "Don't worry. The knife is really sharp. It won't even hurt."
      mc "Okay, can we stop talking about cutting off my fingers?"
      flora excited "I'll keep it up until you're finished with the onion."
      mc "..."
      flora thinking "I've put the onion on the counter."
      flora thinking "Get to the chopping!"
      flora laughing "Noooow!"
      hide flora with Dissolve(.5)
      $ quest.flora_cooking_chilli.advance("chop_onion")
    else:
      jump flora_quest_flora_cooking_chilli_incorrect
  return

label flora_quest_flora_cooking_chilli_incorrect:
  $s = quest.flora_cooking_chilli["current_items"]
  $quest.flora_cooking_chilli["incorrect_count"] += 1
  show flora neutral with Dissolve(.5)
  $x = random.randint(1, 5)
  if x==1:
    mc "I brought what you asked."
    flora eyeroll "What am I supposed to do with this?"
  if x==2:
    mc "I brought what you asked."
    flora concerned "You're messing with me, aren't you?"
    flora laughing "I will make you suffer later."
  if x==3:
    mc "I brought what you asked."
    flora annoyed "That's all wrong. Start over."
  if x==4:
    mc "I brought what you asked."
    flora cringe "What's this? You think this is a game?"
    flora cringe "My internship rides on this chili! Get it right!"
  if x==5 :
    mc "I brought what you asked."
    flora angry "What are you? What are you?"
    mc "An idiot sandwich?"
    flora angry "Damn it, [mc]! It's fucking raw!"
    "Ugh, I hate her Gordon Ramsay impression..."
  flora laughing "I need [s]. In that order."
  hide flora with Dissolve(.5)
  $ quest.flora_cooking_chilli["drawers_interacted_with"] = 0
  $ flora["cooking_chilli_step_one"] = "incorrect"
  $ quest.flora_cooking_chilli.advance("try_again")
  return

label flora_quest_flora_cooking_try_again:
  show flora smile with Dissolve(.5)
  flora sad "Are you ready to try again?"
  flora afraid "I hope you know what you're retrieveing from the drawers this time."
  if quest.flora_cooking_chilli["chilli_step"] == "eight":
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_eight")
  elif quest.flora_cooking_chilli["chilli_step"] == "seven":
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_seven")
  elif quest.flora_cooking_chilli["chilli_step"] == "five":
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_five")
  elif quest.flora_cooking_chilli["chilli_step"] == "four":
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_four")
  else:
    $ quest.flora_cooking_chilli.advance("chilli_recipe_step_one")
  hide flora with Dissolve(.5)
  return

label flora_quest_flora_cooking_chilli_end:
  $s = quest.flora_cooking_chilli["incorrect_count"]
  if s == 0:
    flora confident "Wow! You did that flawlessly! Not a single mistake! Must be the first time ever."
    $achievement.the_mark_of_shame.unlock()
  else:
    flora laughing "And you only messed up [s] times!"
  flora thinking "I just need you to do one more thing for me now."
  mc "Ugh, what is it?"
  flora flirty "I want to add a special touch, but I forgot my phone in the bathroom and it has the recipe. Can you get it for me?"
  mc "Fine, whatever."
  hide flora with Dissolve(.5)
  $ quest.flora_cooking_chilli.advance("chilli_done")
  return

label flora_quest_flora_cooking_chilli_drawer_end:
  "Wait."
  "What was inside here again?"
  "Beats me."
  return

label flora_quest_flora_cooking_chilli_call_flora:
  #SFX: Ringtone
  ## Ringtone
  play sound "<to 6.0>cell_ringtone" loop
  $ quest.flora_cooking_chilli.advance("flora_called")
  "Hmm... where is that coming from?"
  "..."
  "It's definitely somewhere in this bathroom!"
  "..."
  "It went to voicemail."
  stop sound
  menu(side="middle"):
    extend ""
    "Leave a nice voice message":
      mc "Hey, [flora]! I found your phone. Well, if you're listening to this, you already know that."
      mc "I just wanted to say that it feels like it's been years since we last talked. I missed you more than you know."
      if not quest.flora_cooking_chilli["voicemail_left"]:
        $ flora.love+=1
      mc "Sorry, for not always measuring up, but this time around I'll try to be a better person... and a better friend to you."
    "Leave a vulgar voice message":
      mc "Hey, I found you phone. I also found your browser history on it..."
      if not quest.flora_cooking_chilli["voicemail_left"]:
        $ flora.lust+=1
      mc "Really, [flora]? Dragons fucking cars? Not exactly what I expected from you."
  $quest.flora_cooking_chilli["voicemail_left"] = True
  return

label flora_quest_flora_cooking_return_phone:
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  $flora.unequip("flora_hat")
  show flora embarrassed with Dissolve(.5)
  flora embarrassed "Aaaaah!" with hpunch
  mc "What's wrong, [flora]? Where are your pants?"
  flora embarrassed "I... I... oh my god, it burns!"
  mc "Did you get chili in your eyes?"
  flora afraid "Oh, god! Worse!"
  mc "Where did you get it?"
  flora embarrassed "..."
  mc "Oh, I see."
  show flora embarrassed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Why did you touch yourself while cooking?\"":
      show flora embarrassed at move_to(.5)
      mc "Why did you touch yourself while cooking?"
      mc "I mean, how did this even happen?"
      flora afraid "I... forgot to wash my hands after cutting the chili peppers!"
      flora afraid "Oh my god, it's like fire!"
      mc "Do you think blowing on it would help?"
      $flora.lust+=1
      flora skeptical "Screw..."
      flora skeptical "You..."
      flora embarrassed "Aaaah!"
      show flora embarrassed at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Sounds like you have a bit of a situation on your hands... and in your panties.\"":
          show flora embarrassed at move_to(.5)
          mc "Sounds like you have a bit of a situation on your hands... and in your panties."
          "She's kinda cute jumping up and down, unable to do anything about it."
          $flora.lust+=1
          $flora.love-=1
          flora afraid "You're... the worst..."
          flora afraid "Aaaah! Ah, ah, ah, ah!"
          show flora afraid at disappear_to_left()
          "She ran upstairs, screaming. Probably to wash herself off."
          "I should go check on her."
        "\"Can I help somehow?\"":
          show flora embarrassed at move_to(.5)
          mc "Can I help somehow?"
          $flora.love+=1
          flora afraid "Nooo! I need to—"
          flora afraid "Ah! I'm burning up!"
          show flora afraid at disappear_to_left()
          "She ran to the bathroom, better follow."
      $quest.flora_cooking_chilli.advance("follow_bathroom")
      $process_event("update_state")
    "\"Can I help somehow?\"":
      show flora embarrassed at move_to(.5)
      mc "Can I help somehow?"
      $flora.love+=1
      flora afraid "Nooo! I need to—"
      flora afraid "Ah! I'm burning up!"
      show flora afraid at disappear_to_left()
      "She ran to the bathroom, better follow."
      $quest.flora_cooking_chilli.advance("follow_bathroom")
      $process_event("update_state")
    "\"Seriously, I don't even want to know. I'll look after your food while you clean up.\"":
      show flora embarrassed at move_to(.5)
      mc "Seriously, I don't even want to know. I'll look after your food while you clean up."
      flora afraid "Aaaah! Thanks! I'm sorry you had to see this!"
      mc "Oh, no worries. Here's your phone."
      $mc.remove_item("flora_phone")
      show flora embarrassed at disappear_to_left()
      flora embarrassed "I regret life!"
      "She ran upstairs to the shower. She'll be fine."
      $flora.equip("flora_panties")
      if quest.flora_handcuffs > "package":
        $flora.equip("flora_skirt")
      else:
        $flora.equip("flora_pants")
      $flora.equip("flora_bra")
      $flora.equip("flora_shirt")
      mc "Hey, [flora]! Do you want me to call someone?" with hpunch
      flora "{size=25}I, uhhh! I think I got it!{/}"
      $mc.love +=2
      "She's got it. No need for me to get involved. She's more than capable."
      $unlock_stat_perk("love9")
      $quest.flora_cooking_chilli.finish()
  hide flora with Dissolve(.5)
  return

label flora_quest_flora_cooking_enter_bathroom:
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  $flora.unequip("flora_hat")
  show flora embarrassed with Dissolve(.5)
  flora embarrassed "Oh my god! Oh my god! Oh my god!"
  flora embarrassed "It's getting worse!"
  mc "Just take off your clothes and step in the shower."
  flora afraid "What? Nooo!"
  flora afraid "Not with you here!"
  mc "I've seen you naked before, you know."
  flora embarrassed "Oh my god, it hurts so much! Ow, ow, ow!"
  flora afraid "Uhhhhhhh!"
  mc "Here, I'll help you."
  flora embarrassed "Oh, man! Buuuut! Owwww!"
  flora embarrassed "Fine, but don't look!"
  mc "I can't not look!"
  flora cringe "I'm going to die!"
  $flora.unequip("flora_shirt")
  "Eyes crackling with dark fire, she pulls the shirt over her head, slamming it to the floor."
  $flora.unequip("flora_bra")
  "Her small breasts give a slight bounce as they fall out."
  "Stripping in front of an audience is probably on her list of fantasies."
  "But not this particular audience. Not me. Anyone but me."
  "Despite the blooming flames in her pussy, every move is slow. Reluctant."
  flora embarrassed "Stop looking!"
  "At best, it's a half-hearted attempt at covering her body."
  "In the glistening moisture of her sweat, her freckles twinkle like tiny stars across her skin."
  "The glimpses I've caught of her in the past feel meaningless in comparison."
  "This is different. Intimate."
  "Her ragged breath in my ear as she steadies herself on my shoulder, digging her nails in."
  "Frustration and indignation, but also secret gratitude that I'm here."
  "The heat from her body steams my forehead."
  "Her milky thighs and flat tummy beg for my touch."
  "It would be so easy to reach out and grab her by the waist and pull her close."
  "Push my lips against hers... run my tongue over her skin... explore every curve and taste..."
  "But I can't. I'd burn in hell for that."
  "With one eye on me, her fingers slip inside the waistband of her panties. Cautiously, tugging them down her hip bones."
  $flora.unequip("flora_panties")
  show flora sad with Dissolve(.5)
  "The fires licking her pussy now burn through her body, glowing in embers of embarrassment on her cheeks."
  "The sexual jokes. The teasing. She never meant for it to get real."
  "She can't bring herself to look me in the eyes. This is one of those moments that she'll never forget."
  "Utterly naked."
  "Unable to hide her perky nipples, her inflamed labia, the peach fuzz between her legs."
  "Who does she shave for? Herself? She probably loves to feel sexy in secret."
  "Sliding a hand down her pants when no one's looking. Rubbing the smooth lips of her pussy, coating her fingers in the moisture."
  "They say that girl-next-door-types are the kinkiest. I wonder if she ever puts those fingers in her mouth, tasting herself."
  "Does she ever think of me when touching herself? Probably not."
  "Maybe she will from now on."
  "She bounces on the spot, dizzy with pain, looking to me for guidance and comfort."
  flora crying "I'm going to die!"
  mc "You're not going to die. Just get in the shower and aim the showerhead between your legs."
  flora crying "Aaah! Ah!"
  "She's in too much pain to think straight."
  "[flora] being naked is nothing new to me, but this feels wrong and inappropriate. But she is suffering..."
  show flora crying at move_to(.25)
  menu(side="right"):
    extend ""
    "Stay and help her clean off":
      show flora crying at move_to(.5)
      $mc.lust+=1
      $unlock_stat_perk("lust10")
      show flora FloraShowerPoseA with fadehold
      flora "Aah!"
      "Never seen a real vagina this close before. It's so wrinkly and cute. Like a cupcake with filling."
      "As she spreads her legs, exposing her pussy, strings of sticky natural lubrication cling to inner walls of her vagina."
      "Oozing out and mixing with the sheen of sweat on her swollen outer labia."
      "And the smell is like... hm... there's no good comparison. It's a nice and full smell..."
      "It just smells like pussy! That's it. Nothing like it."
      "They say that every pussy tastes differently."
      "If I stuck out my tongue now, tracing the tip through her folds, would it be sweet?"
      "Would it taste as syrupy and pleasant as I imagine? Or would it be saltier like her personality?"
      "If only she'd let me swirl my tongue over her clit. Dip it into her vagina. Tickle her asshole."
      "I'd take the burning chili any day just to have a taste."
      "Oh, god. These thoughts are so wrong."
      mc "I'll be gentle."
      flora FloraShowerPoseA1 "Oh my god, phrasing!"
      mc "Sorry, I'll be gentle the first time."
      flora "Shut up!"
      flora FloraShowerPoseA1 "Just do it!"
      show flora FloraShowerPoseB with qdis
      flora "Eeeeeee! Cold, cold, cold! Too cold!"
      mc "Sorry, thought you wanted to cool down."
      mc "Is this better?"
      jo "Everything okay up there?"
      "Oh, no! [jo]'s already home."
      mc "Err... Yes! Everything's fine up here, [jo]!"
      jo "[flora]?"
      mc "[flora], answer her!"
      flora "Yes, [jo]! I'm fine!"
      flora "Ugh... I'm so not fine..."
      flora FloraShowerPoseA1 "This isn't working..."
      mc "What should I do?"
      flora "Well, the water isn't working! Get milk or something!"
      mc "Oh, right. That's supposed to work. Be right back!"
      $ quest.flora_cooking_chilli.advance("get_milk")
      jump goto_home_hall
    "Turn on the shower, but let her clean off herself":
      show flora crying at move_to(.5)
      $mc.love+=1
      mc "Here, you're more than capable at handling this yourself."
      mc "I'll wait outside."
      $unlock_stat_perk("love10")
      $flora.love+=1
      flora embarrassed "That's... really... kind..."
      flora embarrassed "Aaah! Please don't tell [jo] about this!"
      mc "Don't worry."
      "That took a lot of willpower, but everyone's going to be better off because of it."
      "Old me would've done some bad things there, but what's the point of another chance if you're just going to spoil it?"
      $mc.remove_item("flora_phone")
      $quest.flora_cooking_chilli["wait_outside"] = True
      jump goto_home_hall
  hide flora with Dissolve(.5)
  return

label flora_quest_flora_cooking_wait_outside:
  $flora.equip("flora_panties")
  if quest.flora_handcuffs > "package":
    $flora.equip("flora_skirt")
  else:
    $flora.equip("flora_pants")
  $flora.equip("flora_bra")
  $flora.equip("flora_shirt")
  $quest.flora_cooking_chilli.finish()
  $quest.flora_cooking_chilli["wait_outside"] = False
  return

label flora_quest_flora_cooking_get_milk:
  "Crap, [jo]'s blocking the mini fridge."
  "Need to get her out of  here somehow, without making it suspicious."
  "She hates it when I take stuff out of the fridge and bring it upstairs."
  "She'll interrogate me into confession..."
  $ quest.flora_cooking_chilli["distraction_count"] = 0
  $ quest.flora_cooking_chilli["distraction_current_item"] = "none"
  $ quest.flora_cooking_chilli["distraction_first_item"] = "none"
  $ quest.flora_cooking_chilli["distraction_second_item"] = "none"
  $ quest.flora_cooking_chilli["distraction_third_item"] = "none"
  return

label flora_cooking_get_milk:
  "Considering the circumstances, she'd probably kill me if I came back empty-handed."
  return

label flora_quest_flora_cooking_chilli_jo:
  show jo smile with Dissolve(.5)
  jo smile "Hey, kiddo! How's your day?"
  jo neutral "Your cheeks are a bit red. You're not coming down with something, are you?"
  mc "I'm fine, [jo]."
  jo neutral "And why are [flora]'s pants on the floor?"
  mc "You know her, [jo]. She's always leaving her stuff everywhere."
  jo thinking "Hmm... when it comes to being messy, she's not the first member of this household that pops into my head."
  mc "Oh, but I think you've become much better at picking up after yourself!"
  "...And after me."
  jo annoyed "Don't test your luck, young man."
  "She's still covering the mini-fridge."
  "Hmm... what would be the best trick to lure her away?"
  "Perhaps using some of the kitchen utensils would work..."
  hide jo with Dissolve(.5)
  return

label flora_quest_flora_cooking_chilli_distraction_add_item:
  if quest.flora_cooking_chilli["distraction_first_item"] == "none":
    $ quest.flora_cooking_chilli["distraction_first_item"] = quest.flora_cooking_chilli["distraction_current_item"]
  elif quest.flora_cooking_chilli["distraction_second_item"] == "none":
    $ quest.flora_cooking_chilli["distraction_second_item"] = quest.flora_cooking_chilli["distraction_current_item"]
  elif quest.flora_cooking_chilli["distraction_third_item"] == "none":
    $ quest.flora_cooking_chilli["distraction_third_item"] = quest.flora_cooking_chilli["distraction_current_item"]

  if quest.flora_cooking_chilli["distraction_count"] == 1:
    $ s1 = quest.flora_cooking_chilli["distraction_first_item"]
    "I got [s1]."
    "I should get a couple more things."

  if quest.flora_cooking_chilli["distraction_count"] == 2:
    $ s2 = quest.flora_cooking_chilli["distraction_second_item"]
    "I got [s2]."
    "With one more item I'll be able to make a distraction."

  if quest.flora_cooking_chilli["distraction_count"] == 3:
    jump flora_quest_flora_cooking_chilli_distraction_check

  return


label flora_quest_flora_cooking_chilli_distraction_check:
  $ s1 = quest.flora_cooking_chilli["distraction_first_item"]
  $ s2 = quest.flora_cooking_chilli["distraction_second_item"]
  $ s3 = quest.flora_cooking_chilli["distraction_third_item"]
  $ items_array = [s1,s2,s3]
  if "a pan" in items_array and "a plastic bag" in items_array and "an apron" in items_array:
    "All right, put on the apron to look presentable."
    "Put the pan on the stove and turn up the heat."
    "A bit of the plastic bag in the pan..."
    $ quest.flora_cooking_chilli["distraction_method"] = "red"
    pause(1)
    show jo concerned with Dissolve(.5)
    jo concerned "What is that smell?"
    mc "Uh, must've been some residue of something left in the pan."
    jo concerned "My goodness! It smells like molten plastic!"
    mc "I'm sorry, [jo]. I should've checked the pan before putting it on the stove."
    jo sarcastic "Don't worry about it."
    jo sarcastic "I'm going to go open the door and let some smoke out."
    show jo sarcastic at disappear_to_left
    "Great! Now's my chance."
    $ quest.flora_cooking_chilli.advance("distracted")
  elif "a lighter" in items_array and "a kitchen towel" in items_array and "a shot glass" in items_array:
    "Okay, use the lighter to \"accidentally\" set the towel on fire."
    $ quest.flora_cooking_chilli["distraction_method"] = "blue"
    mc "Oh, shit!"
    "Then, \"accidentally\" drop shot glass so that it shatters."
    "..."
    "Hmm... that didn't work. Need to use some force."
    #$ SFX: Smashing glass
    ## Glass smash
    play sound "<from 0.3>glass_break"
    pause(.5)
    show jo concerned with Dissolve(.5)
    jo concerned "What's happening?"
    mc "I was trying to light a candle, burned my finger on the lighter."
    mc "I'm sorry I dropped the glass. I know it's one of your favorites."
    jo afraid "Don't worry about the stupid glass. Are you—"
    jo afraid "The towel is on fire! Move!" with hpunch
    show jo afraid at disappear_to_right
    "Great. While she's busy putting out the fire and cleaning up the glass, the milk is mine for the taking."
    $ quest.flora_cooking_chilli.advance("distracted")
  elif "a measuring beaker" in items_array and "spices" in items_array and "flour" in items_array:
    "So, just put the flour in the beaker..."
    "Take a whiff of the spices..."
    "..."
    image white = "#FFF"
    show white
    "Achoo!" with vpunch
    $ quest.flora_cooking_chilli["distraction_method"] = "purple"
    hide white
    show jo concerned
    with Dissolve(.5)
    jo concerned "Oh, my... what happened?"
    mc "I'm so sorry, I sneezed into the flour bag..."
    jo laughing "Don't worry about it!"
    jo laughing "You look adorable covered in flour."
    mc "Thanks, [jo]..."
    jo excited "Go clean up, I'll take care of the kitchen."
    show jo excited at disappear_to_right
    "She's distracted now. The milk shall be mine."
    $ quest.flora_cooking_chilli.advance("distracted")
  elif "a knife" in items_array and "a cutting board" in items_array and "ketchup" in items_array:
    "A little squeeze of ketchup on the finger."
    "Knife and cutting board ready."
    show blood_splatter with hpunch
    mc "Crap!"
    show jo concerned with Dissolve(.5)
    jo concerned "Oh, honey! Are you okay?"
    mc "The knife slipped. It's not that bad."
    jo afraid "Wait here, I'll get go get the first aid kit!"
    show jo afraid at disappear_to_left
    "Okay, here's my chance to grab the milk."
    hide blood_splatter with Dissolve(.5)
    $ quest.flora_cooking_chilli.advance("distracted")
  else:
    "Let's see... I have [s1], [s2], and [s3]."
    "And with this I can make...!"
    "Nothing."
    "Absolutely nothing."
    "Try again."
    $ quest.flora_cooking_chilli["distraction_count"] = 0
    $ quest.flora_cooking_chilli["distraction_current_item"] = "none"
    $ quest.flora_cooking_chilli["distraction_first_item"] = "none"
    $ quest.flora_cooking_chilli["distraction_second_item"] = "none"
    $ quest.flora_cooking_chilli["distraction_third_item"] = "none"
  return

label flora_quest_flora_cooking_got_liquid(item):
  $ quest.flora_cooking_chilli["distraction_method"] = "done"
  $unlock_replay("flora_relief")
  if item.id == "milk":
    show flora FloraShowerPoseA with Dissolve(.5)
    flora "Oh, man! Why won't it stop burning!"
    mc "I brought you the milk."
    mc "Let's try it."
    $mc.remove_item("milk")
    $mc.add_item("empty_bottle")
    show flora FloraShowerPoseC2 with Dissolve(.5)
    flora "Ayeeee! Coooold!"
    mc "Sorry, it's straight from the fridge."
    flora "..."
    flora "It's working! Oh my god, my poor pussy..."
    flora "Thank you..."
    $flora.love+=1
    flora "That's the best feeling ever..."
    "She looks like she's drifting in the clouds right now. Must be quite the release when the heat's finally fading."
    "Beads of sweat dot her forehead and her face is still red, but there's that look of complete relief washing over her face."
    flora "Can you... like... give me a moment, please?"
    show flora FloraMastPose01_1 with Dissolve(.5)
    "She's totally out of it. Her hand is moving between her legs."
    "She didn't even care to check if I left."
    menu(side="far_left"):
      extend ""
      "Stay and watch":
        $mc.lust+=1
        $unlock_stat_perk("lust11")
        show flora FloraMastPose01_2 with Dissolve(.5)
        flora "Mmmmm!"
        "Her hand is moving like it has a will of its own. Down her groin, between her thighs."
        show flora FloraMastPose01_1 with qdis
        "She's hypnotizing me with her hand!"
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        "Slowly circling her clit. A couple of fingers dip inside her. Weaving through her folds."
        "Her hips buck as a shiver of pleasure rolls through her."
        show flora FloraMastPose01_2 with Dissolve(.5)
        "Fingers glistening with her juices, she smears them over her entire pussy."
        "She's so good at it. Years of experience in pleasing herself."
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        "It's better than any porn because this is genuine. She's doing it for herself."
        "She just wants to bring herself to that edge."
        show flora FloraMastPose01_3 with Dissolve(.5)
        "What a marvel to behold! So innocent and naughty at the same time."
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        "Another shiver. The muscles of her stomach and abdomen tensing up."
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        flora "Mmmm..."
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        flora "Ah!"
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        "It finally makes sense why it's called \"playing with your pussy.\""
        "The way she tugs at her labia, teases her clit, sweeps her fingers through her folds. It's all so playful!"
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        "Her breath stops short in her throat. A tiny gasp. A gush that floods her pussy."
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        "She's getting so close. Balancing on that edge of pleasure. Preparing herself for the ultimate dive..."
        show flora FloraMastPose01_1 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        "Here it comes..."
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        show flora FloraMastPose01_2 with Dissolve(.5)
        show flora FloraMastPose01_3 with Dissolve(.5)
        #show flora NEW ORGASM POSE
        flora "Oh god..."
        show flora FloraMastPose01_4 with Dissolve(.5)
        "That final shudder. The sigh of relief."
        "The burst pipe."
        show flora FloraMastPose01_4a with Dissolve(.5)
        "I feel like applauding her, but that would give everything away."
        "She came right over her hand and floor. Not a single care in the world."
        "Her mind is on some other plane of existence right now, but it won't be long before she returns to her senses."
        "Ugh, time to go before she notices me..."
        "I'll just leave her phone here."
        $mc.remove_item("flora_phone")
        "This is one of those things that should probably never be spoken about again."
        "She'd kill me if she knew that I watched her."
        "But damn, that was hot..."
        "Is that a weird thing to feel? Probably."
        "Does anyone care? Probably not."
        "How am I ever going to get these images out of my head?"
        $quest.flora_cooking_chilli["wait_outside"] = True
        jump goto_home_hall
      "Give her some privacy":
        $unlock_stat_perk("love11")
        $mc.love+=1
        "[flora] has been through enough today. She doesn't need me perving on her."
        "That internship is probably putting a lot of stress on her."
        "Hopefully, she'll finally be able to take the edge off and get some much needed release."
        "Leaving her alone is what any decent person would've done."
        "Plus, it's honestly weird to watch [flora] masturbate."
        "I'll just put her phone here..."
        $mc.remove_item("flora_phone")
        $quest.flora_cooking_chilli["wait_outside"] = True
        jump goto_home_hall
  elif item.id == "pepelepsi":
    show flora FloraShowerPoseA with Dissolve(.5)
    flora "Oh god."
    mc "This isn't exactly milk, but I hope it'll help."
    $mc.remove_item("pepelepsi")
    $mc.add_item("empty_bottle")
    show flora FloraShowerPoseC1 with Dissolve(.5)
    flora "..."
    flora "It... It's working!"
    mc "Really?"
    flora "Yes! Can you, um, spread it out?"
    mc "Are you sure?"
    flora "Yes, I still have chili on my fingers!"
    mc "All right..."
    show flora FloraShowerPoseD_1 with Dissolve(.5)
    "It's... so much softer than I ever expected..."
    "Her lips are like hot wet silk in my hand."
    "It's like touching a little bit of heaven. The most forbidden and sinful part of it..."
    "Squishy and smooth and... the best thing ever."
    show flora FloraShowerPoseD_2 with Dissolve(.5)
    "An infinite number of the sweetest juiciest pussies have filled my dreams for years, but nothing could compare to this."
    "Running my fingers through her folds, feeling her twitch under my touch. Her little gasps as my hand brushes over her clit."
    show flora FloraShowerPoseD_3 with Dissolve(.5)
    show flora FloraShowerPoseD_2 with Dissolve(.5)
    flora "Ohhh! It's working!"
    show flora FloraShowerPoseD_1 with Dissolve(.5)
    "She's vibrating against my hand. Buzzing almost!"
    show flora FloraShowerPoseD_2 with Dissolve(.5)
    show flora FloraShowerPoseD_3 with Dissolve(.5)
    show flora FloraShowerPoseD_2 with Dissolve(.5)
    flora "W-what's happening?"
    flora "Oh."
    show flora FloraShowerPoseD_1 with Dissolve(.5)
    show flora FloraShowerPoseD_2 with Dissolve(.5)
    flora "Ohh!"
    show flora FloraShowerPoseE1b with Dissolve(.5)
    mc "..."
    flora "...!"
    show flora FloraShowerPoseE1 with Dissolve(.5)
    show pee_splatter with Dissolve(.5)
    "Her pee is like liquid fire gushing through my fingers, spattering over my arm and face."
    "Always thought girl pee would smell differently, but it's the same musky hint of ammonia."
    "Almost got some in my mouth and eyes..."
    "Glad it missed, although... can't help but be curious about the taste..."
    #Show FloraShowerPoseE1b
    mc "Did you... just pee on me?"
    flora "I'm so sorry!"
    show flora FloraShowerPoseE1b with Dissolve(.5)
    hide pee_splatter with Dissolve(.5)
    flora "I don't know what happened!"
    "My guess is the Pepelepsi combined with the residue of the chili peppers had some kind of chemical reaction..."
    mc "I don't know either, but I'm glad you're not burning up anymore!"
    show flora FloraShowerPoseD_3 with Dissolve(.5)
    flora "Thanks... I guess..."
    "That was probably the most reluctant \"thank you\" I've ever heard, but I'll take it."
    flora FloraShowerPoseA1_concerned_pepelepsi "Why does my pussy smell like Pepelepsi?"
    mc "Err... we were out of milk..."
    flora FloraShowerPoseA1_angry_pepelepsi "So you grabbed the bottle of that radioactive waste and decided to pour it all over my poor pussy?"
    mc "It seemed to work. Would you have rather I let you suffer?"
    flora FloraShowerPoseA1_confident_pepelepsi "Maybe."
    flora "Well, you're lucky my pussy can withstand almost anything."
    "She sure isn't wrong about me being lucky."
    flora concerned "..."
    "Now is probably a good time to make my escape."
    mc "Well, I'm glad you're okay. Peace out!"
    $flora.equip("flora_panties")
    flora angry "Don't you dare tell anyone about this."
    mc "Who am I going to tell?"
    flora confident "I'm glad you don't have any friends."
    flora confident "Then, I'd have to kill you."
    mc "All right, then!"
    if quest.flora_handcuffs > "package":
      $flora.equip("flora_skirt")
    else:
      $flora.equip("flora_pants")
    flora annoyed "Are you going to stare forever or let me dress in peace?"
    mc "Is that a rhetorical question or are you giving me a choice here?"
    flora laughing "I would scream at you, but you smell like pee and that's kinda funny."
    "She has the weirdest humor."
    mc "I'll just take a shower now."
    flora angry "Get out!"
    mc "Ugh, fine."
    $mc.remove_item("flora_phone")
    #SFX: Door creaking
    ## Door creak
    play sound "bedroom_door"
    call goto_home_hall
    "Phew, she looked like she was about to slit my throat with her nails."
    "This incident should probably never be spoken about again."
    $flora.equip("flora_bra")
    $flora.equip("flora_shirt")
    $quest.flora_cooking_chilli.finish()
  else:
    "It would be hot to stick my [item.title_lower] in her pussy, but she'd probably kill me."
    $quest.flora_cooking_chilli.failed_item(item)
  return

