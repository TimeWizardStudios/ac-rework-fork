label flora_quest_flora_jacklyn_introduction_start:
  show flora annoyed with Dissolve(.5)
  flora annoyed "What?"
  mc "Nothing."
  flora eyeroll "What is it, [mc]?"
  mc "You drank all the milk again."
  flora laughing "Damn straight!"
  flora laughing "And it tasted great!"
  mc "Did it really?"
  flora skeptical "What do you mean..."
  mc "Heh."
  flora afraid "What did you do to it?"
  mc "Nothing."
  flora afraid "You put something in it, didn't you?"
  mc "I don't know what you're talking about."
  flora embarrassed "I'm going to be sick!"
  $flora.location="home_bathroom"
  show flora embarrassed at disappear_to_left
  "She ran upstairs to the bathroom, looking like she was going to throw up."
  "Funny thing is, there was nothing but milk in the milk."
  "Getting under her skin has taken me years of hard work, but this time it wasn't even intentional."
  "Better check on her before she tells [jo]."
  $quest.flora_jacklyn_introduction.start()
  $quest.flora_jacklyn_introduction.advance("bathroom")
  return 

label quest_flora_jacklyn_introduction_bathroom:
  show flora sad with Dissolve(.5)
  mc "Are you okay?"
  flora sad "What do you think?"
  show flora sad at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You look like you swallowed a slug.\"":
      show flora sad at move_to(.5)
      mc "You look like you swallowed a slug."
      $ flora.lust+=1
      flora afraid "Is that what you put in the milk?"
      mc "Maybe, who knows?"
      flora embarrassed "What? There's no way!"
      flora embarrassed "[jo]!" with vpunch
      "Ugh, crap."
      show flora embarrassed at move_to(.75)
      show jo confident at appear_from_left(.25)
      jo confident "What's going on in here?"
      mc "[flora]'s being a baby again."
      flora angry "He put a slug in the milk!"
      jo concerned "..."
      mc "I did no such thing."
      jo concerned "I sincerely hope that's true."
      flora concerned "He also chased me up the stairs again."
      jo annoyed "You're both adults now. This is not how adults behave."
      mc "Sorry, but I didn't do anything this time."
      flora concerned "He totally did!"
      jo thinking "Apologize to her, [mc]. You're already on thin ice."
      hide jo with moveoutleft
      show flora concerned at move_to(.5)
      mc "God, you're such a liar."
      flora confident "Heh! And I didn't even drink the milk."
      mc "I can't believe you..."
      flora excited "It's called getting played. Look it up!"
      mc "Well, I'm not apologizing."
      flora neutral "You better, or I'm telling."
      mc "Fine... I'm so so sorry."
      flora concerned "That didn't really sound sincere."
      mc "That's all you're getting."
      flora concerned "Fine."
    "\"I'm sorry. I didn't put anything in the milk.\"":
      show flora sad at move_to(.5)
      mc "I'm sorry. I didn't put anything in the milk."
      flora annoyed "I can't trust anything you say!"
      mc "I know, and I'd like to change that."
      flora afraid "Who are you and what did you do with the real [mc]?"
      flora afraid "Is this another prank?"
      mc "No, it's not."
      mc "It makes me feel bad that you ran up here to throw up."
      flora sarcastic "..."
      mc "What?"
      $flora.love+=1
      flora sarcastic "Well, if we're being honest, I guess I should also come clean."
      flora sarcastic "I didn't really throw up. I didn't even drink the milk."
      mc "Ugh, seriously?"
      flora confident "Sorry, but I know all your tricks."
      mc "..."
      flora neutral "What's wrong? You don't like getting played?"
      mc "Not particularly, no."
      flora excited "Heh! Well, you deserved it."
  flora excited "By the way, have you seen the new art teacher?"
  mc "Yeah, I did."
  flora neutral "Well, what did you think?"
  show flora neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"She's a total babe.\"":
      show flora neutral at move_to(.5)
      $jacklyn.lust+=2
      mc "She's a total babe."
      flora annoyed "A slut, you mean?"
      mc "If you got it, flaunt it."
      flora confused "Why am I not surprised?"
      mc "You shouldn't be."
      flora annoyed "So, you don't think teachers should dress responsibly?"
      mc "I think everyone should have the right to dress however they want."
      flora eyeroll "How progressive of you."
    "\"She's a bit pretentious.\"":
      show flora neutral at move_to(.5)
      $jacklyn.lust-=1
      mc "She's a bit pretentious."
      flora annoyed "Couldn't agree more."
      flora annoyed "The way she carries herself, and the way she talks, and her effing clothes!"
      "Never seen [flora] so angry at a teacher before."
      "She's usually the first one to take their side."
      "Somehow, it's nice seeing her anger directed at someone else for a change."
      "God knows I've invoked it too many times."
    "\"What do {i}you{/} think?\"":
      show flora neutral at move_to(.5)
      mc "What do {i}you{/} think?"
      $flora.love+=1
      flora thinking "Me? Hmm..."
      flora thinking "I like her style, her clothes, her confidence. I think she's pretty nice..."
      flora annoyed "Unless you take into account that she's a total bitch!"
      mc "Whoa, that took a quick turn!"
      "Her mood swings are the stuff of legends, but this seems a bit extreme even for her."
  mc "What's this really about?"
  flora laughing "What do you mean?"
  mc "You seem unusually upset with her."
  flora thinking "She trash-talked my art from last year."
  mc "Hah! Really?"
  flora angry "What's so funny?"
  mc "I mean, you not getting an A is pretty rare."
  flora angry "I'm going to get an A."
  mc "Yeah?"
  flora neutral "Yep. I have a plan."
  mc "Are you going to seduce her?"
  flora annoyed "Can you stop being a pervert?"
  mc "It's a legitimate question!"
  flora sarcastic "I'm going to impress her."
  mc "She's not easily impressed."
  flora annoyed "I said I have a plan!"
  mc "Well, what is it?"
  flora confident "I'm going to befriend her."
  mc "I don't think she'll give you a higher grade. "
  flora concerned "She hasn't tasted my home-made cake yet."
  mc "You promised you wouldn't bake that thing ever again..."
  "That cake is what landed her that internship. It's how she got [jo] to remove my name from her will."
  #"It's also how she in the past has convinced me to... well, pretty much do anything."
  #"It's straight up heroin in cake-form. It's evil."
  flora confident "I don't have a choice. I need an A in art."
  flora excited "And you're going to help me."
  mc "No way. That's immoral. I'm not helping you trick a teacher into addiction."
  flora flirty "I'll let you have a slice..."
  mc "Deal!"
  "Oh, shit. That should probably have taken more convincing..."
  flora laughing "Perfect!"
  flora thinking  "Okay, I need you to lure her out of the school."
  flora thinking "I shouldn't bring the cake inside. That would be reckless."
  mc "And I get a slice? A big one."
  flora flirty "Oh, you'll get a big slice."
  mc "Deal!"
  "Ugh, that was way too eager, but ever since [flora] moved out, I've been like a cake-junkie in withdrawal. It's haunted my dreams for years."
  flora blush "Okay, got to prepare the ingredients."
  show flora blush at disappear_to_left
  pause(1)
  $quest.flora_jacklyn_introduction.advance("jacklyn")
  return

label jacklyn_quest_flora_jacklyn_introduction:
  show jacklyn neutral with Dissolve(.5)
  mc "[jacklyn]?"
  jacklyn neutral "What's up?"
  mc "Can I ask you for a favor?"
  jacklyn thinking "Depends. What's your pitch?"
  show jacklyn thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You really made an impression on [flora], and she'd like to be your friend.\"":
      show jacklyn thinking at move_to(.5)
      $flora.love+=1
      mc "You really made an impression on [flora], and she'd like to be your friend."
      jacklyn excited "Badass. Can you remind me who [flora] is? I've seen too many new faces recently."
      mc "She's about this tall... and has a temper about this tall..."
      jacklyn laughing "I remember her! She too shy to ask herself?"
      mc "Not really, but I'm trying to get back in her good graces. She made you a snack, as well."
      jacklyn laughing "Okay. I'm getting gay vibes from her, is she into me?"
      mc "Not that I know of."
      jacklyn smile "Good, 'cause I don't dabble."
      mc "It's really about her grades. She's a bit of a perfectionist."
      jacklyn smile "Totally. I saw that in her art. It's too stiff. She needs to let her soul wander."
      mc "Right. Would you mind giving her a hand?"
      mc "In a totally straight and platonic way, of course."
      jacklyn laughing "Love to!"
      if jacklyn.at("school_entrance"):
        mc "Okay, let me go get her."
        $ quest.flora_jacklyn_introduction.advance("outside")
        hide jacklyn with Dissolve(.5)
        jump quest_flora_jacklyn_introduction_outside
      else:
        mc "Okay, meet us outside the school."
        $ quest.flora_jacklyn_introduction.advance("outside")
    "\"[flora] has very thick fingers. She needs professional help... with her art.\"":
      show jacklyn thinking at move_to(.5)
      $mc.charisma+=1
      $flora.lust+=1
      mc "[flora] has very thick fingers. She needs professional help... with her art."
      jacklyn laughing "That's comedy!"
      jacklyn laughing "Who is she?"
      jacklyn thinking "I've met a lot of new students this week."
      mc "She's this short brunette, with more sass than her body can handle."      
      mc "She said you guys had met."
      jacklyn smile "Wicked. I remember her."
      jacklyn smile "She's not bad at all. Just stiff as a lightning rod."
      mc "That's a very accurate description of her."
      jacklyn thinking "Why didn't she come herself?"
      mc "Honestly? She probably wants to bang you."
      jacklyn laughing "Dude!"
      jacklyn laughing "That's flattering, but I don't dabble."
      if jacklyn.at("school_entrance"):
        mc "Yeah, I figured. But I think she made you a snack. Would you mind meeting her?"
        jacklyn excited "That's a def."
        jacklyn excited "Never going to say no to a free snack. I'll turn her down myself."
        $ quest.flora_jacklyn_introduction.advance("outside")
        hide jacklyn with Dissolve(.5)
        jump quest_flora_jacklyn_introduction_outside
      else:
        mc "Yeah, I figured. But I think she made you a snack. Would you mind meeting her outside the school?"
        jacklyn excited "That's a def."
        jacklyn excited "Never going to say no to a free snack. I'll turn her down myself."
        $ quest.flora_jacklyn_introduction.advance("outside")
    "\"I think [flora] has a crush on you. Just trying to wingman here.\"":
      show jacklyn thinking at move_to(.5)
      mc "I think [flora] has a crush on you. Just trying to wingman here."
      $ jacklyn.lust+=1
      jacklyn smile "Sorry, but I don't dabble."
      jacklyn smile "Who's she, though?"
      mc "She said you guys had met."
      jacklyn thinking "I've met so many new students. You gotta give me more."
      mc "Short brunette with highlights, bit of hellfire and mischief in her eyes."
      mc "She said you looked at her art."
      jacklyn excited "Oh, I remember her now."
      jacklyn excited "She's a truther. She's also sweet, but alas. No dabblerino."
      if jacklyn.at("school_entrance"):
        mc "Would you mind meeting her, anyway? I think she made you a snack."
        jacklyn laughing "What a cutie pie! If I were even a little gay, I'd eat her right up."
        jacklyn laughing "I won't say no to a regular snack, though!"
        mc "Awesome. I'll go get her."
        $ quest.flora_jacklyn_introduction.advance("outside")
        hide jacklyn with Dissolve(.5)
        jump quest_flora_jacklyn_introduction_outside
      else:
        mc "Would you mind meeting her outside, anyway? I think she made you a snack."
        jacklyn laughing "What a cutie pie! If I were even a little gay, I'd eat her right up."
        jacklyn laughing "I won't say no to a regular snack, though!"
        mc "Awesome. I'll tell her. Come meet us outside the school, maybe you'll change your mind!"
        jacklyn smile "Doubt it."
        $ quest.flora_jacklyn_introduction.advance("outside")
    "\"[flora] really loves your fashion sense, but she's too shy to tell you.\"":
      show jacklyn thinking at move_to(.5)
      mc "[flora] really loves your fashion sense, but she's too shy to tell you."
      $jacklyn.love+=1
      jacklyn laughing "That's adorbs."
      jacklyn laughing "Who's [flora]?"
      mc "She's this tiny brunette with really pointy elbows. She said you looked at her art."
      jacklyn excited "Oh, wicked. I remember her."
      jacklyn excited "Does she dabble?"
      mc "What?"
      jacklyn smile "Does she like the ladies?"
      mc "Hah! I don't know. Well, she did make you a snack, I think."
      jacklyn smile "Damn it. I figured she was into me."
      jacklyn thinking "Too bad I'm straight as an arrow."
      jacklyn thinking "I could probably give her a couple of fashion pointers, though."
      if jacklyn.at("school_entrance"):
        mc "Great! Let me go get her."
        $ quest.flora_jacklyn_introduction.advance("outside")
        hide jacklyn with Dissolve(.5)
        jump quest_flora_jacklyn_introduction_outside
      else:
        mc "Great! Meet us outside the school?"
        jacklyn excited "Def. See you there."
        $ quest.flora_jacklyn_introduction.advance("outside")
  hide jacklyn with Dissolve(.5)
  return

label quest_flora_jacklyn_introduction_outside:
  pause(.5)
  show flora afraid with Dissolve(.5)
  flora afraid "You did it."
  mc "You don't have to sound so surprised."
  flora laughing "I didn't think you had it in you."
  show flora laughing at move_to(.25)
  show jacklyn smile at appear_from_right(.75)
  jacklyn smile "What's up, people!"
  mc "All right, my work here is done."
  flora smile flip "Thanks."
  "Tricking another person into the relentless grip of a cake-addiction isn't exactly good."
  "But on the other hand, more cake for me. It's a vicious circle."
  jacklyn excited "[mc], you can stay if you want."
  flora eyeroll flip "No, he can't. Right, [mc]?"
  "Better not ruin this now."
  $flora.love+=1
  mc "Err... yes, classes and stuff. Gotta run!"
  jacklyn smile "Next time, then. Pocket my good lucks!"
  mc "Thanks. Just waiting for my reward..."
  flora confident flip "Oh, right. Totally forgot about that."
  $mc.add_item("cake_slice")
  $mc.add_item("plastic_fork")
  show flora confident flip at disappear_to_left
  show jacklyn smile at disappear_to_left
  "They're going to the fields behind the school. It's somewhat tempting to spy on them, but eh... it's probably just going to be a platonic picnic."
  $ quest.flora_jacklyn_introduction.finish()
