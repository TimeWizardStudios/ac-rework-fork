label quest_flora_handcuffs_start:
  show flora neutral with Dissolve(.5)
  mc "Hey, [flora]! Do you have a minute?"
  flora neutral "What for?"
  mc "I need your help."
  flora eyeroll "Is it something gross?"
  mc "No!"
  flora confused "Is it something illegal?"
  mc "Listen, I would never—"
  flora annoyed "Four years ago, Christmas morning."
  flora annoyed "[jo] had left her car keys on the dresser."
  "Crap. Her memory is good."
  mc "Okay, fine! I promise you it's nothing illegal!"
  flora cringe "So, it's something perverted?"
  mc "Ugh..."
  flora confident "I knew it!"
  flora concerned "Forget about it."
  mc "Please, it's nothing perverted!"
  flora concerned "I'm leaving."
  mc "I'll get you that skirt you've circled in one of your magazines!"
  mc "The err... Scarlet Starlet's Cutie-Harlot?"
  flora skeptical "..."
  flora skeptical "You can't afford it."
  mc "I can, and I will."
  flora afraid "Also, why were you reading my magazine?"
  show flora afraid at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Err... you left it on the toilet.\"":
      show flora afraid at move_to(.5)
      mc "Err... you left it on the toilet."
      flora annoyed "I absolutely didn't!"
      mc "Huh, that's odd... I guess I must've gone into the wrong room last night.{space=-75}"
      flora cringe "What?!"
      $mc.charisma+=1
      mc "It was dark. Mistakes happen, [flora]."
      flora cringe "I swear to god, [mc]..."
    "\"It's a fine piece of literature!\"":
      show flora afraid at move_to(.5)
      mc "It's a fine piece of literature!"
      flora eyeroll "It's Cosmololitan..."
      mc "Exactly!"
      $mc.intellect+=1
      mc "Perhaps a nod to Nabokov's greatest work, but more importantly, a contemporary zeitgeist."
      mc "Cosmololitan helps young women deal with the psychological traumas{space=-45}\nof growing up without a father figure."
      flora confused "Huh..."
      mc "It's common knowledge, [flora]."
      flora annoyed "Whatever."
    "\"Just happened to see it when\nI was in your room earlier.\"":
      show flora afraid at move_to(.5)
      mc "Just happened to see it when I was in your room earlier."
      flora cringe "You were in my room?!"
      flora cringe "You're not allowed in my room!"
      mc "I was just looking for something to read."
      flora cringe "You were snooping!"
      mc "I wasn't!"
      flora annoyed "You totally were!"
      flora annoyed "What else did you find?"
      mc "Okay, fine. I went through your stuff."
      mc "I found your secret erotica collection."
      $flora.lust+=1
      flora laughing "Idiot!"
      flora worried "Also, gross. I don't have one."
  mc "So, anyway... you'll help me out, right?"
  flora skeptical "..."
  flora skeptical "I'll consider it if you buy me that skirt."
  mc "Perfect!"
  flora skeptical "It also depends on what it is."
  mc "I need help stealing a pair of handcuffs from the school guard..."
  flora laughing "Hahaha!"
  mc "..."
  flora worried "You're serious?"
  mc "Yes!"
  flora cringe "So, it's illegal, perverted, and potentially gross!"
  mc "Stainless steel, [flora]. Nothing gross about it."
  flora eyeroll "Forget about it."
  mc "Oh, come on! That skirt is really cute! Plus, I'll take all the blame if we get caught."
  flora thinking "It is pretty damn cute..."
  mc "Right?"
  flora thinking "Hmm..."
  flora worried "I won't do anything illegal. And if you get caught, I don't know you."
  mc "Deal!"
  flora blush "But first you buy me that skirt."
  mc "Fine..."
  hide flora confident with dissolve2
  "Ugh, buying the Cutie-Harlot for [flora] will ruin my economy for months... but [kate] needs those handcuffs..."
  "The easiest way is probably ordering the skirt online."
  $quest.flora_handcuffs.start()
  return

label quest_flora_handcuffs_order:
  "There used to be a site back in the day that sold all sorts of things at a vast discount..."
  "Odd things like dream catchers and amulets, but also snacks, clothes,{space=-50}\nand tools."
  "The skirt [flora] wants is expensive as hell, but maybe there's a way to get it at a discount?"
  "You need a special browser to reach it... and the government shut it down a few years ago... well, technically, it will get shut down..."
  "..."
  "..."
  "Oh, look, it's up and running!"
  window hide
  window auto
  jump goto_home_computer

label quest_flora_handcuffs_order_cutie_harlot:
  show screen black_market(order_shipped="cutie_harlot")
  "That was anything but cheap, but at least [flora] is going to be happy and agreeable."
  "Now to wait for the delivery..."
  $quest.flora_handcuffs.advance("package")
  return

label quest_flora_handcuffs_package(item):
  if item == "cutie_harlot":
    show flora smile with Dissolve(.5)
    mc "Hey, I have something for you!"
    flora skeptical "..."
    flora skeptical "What is it?"
    mc "Why do you look so suspicious?"
    flora eyeroll "Why do you think?"
    show flora eyeroll at move_to(.75)
    menu(side="left"):
      extend ""
      "\"I've only ever wanted to\nmake you happy.\"":
        show flora eyeroll at move_to(.5)
        mc "I've only ever wanted to make you happy."
        flora laughing "That's the best joke I've heard all day!"
        mc "It's the truth!"
        flora concerned "That's what you said when you lied to [jo] about stealing my underwear.{space=-55}"
        mc "I didn't steal your underwear!"
        mc "When you put them back, it's called borrowing."
        flora angry "I knew it!"
        "Shit. Forgot that I hadn't told her yet..."
        flora angry "I'm telling [jo] that you finally confessed! You're going to creep-prison!{space=-30}"
        "Painful, but fair."
        jump quest_flora_handcuffs_package_making_up
      "\"You're worried for nothing!\"":
        show flora eyeroll at move_to(.5)
        mc "You're worried for nothing!"
        flora concerned "That's what you said right before drenching me with ice water..."
        mc "To be fair, it was for that ice bucket challenge."
        flora angry "You should've poured it over yourself, then!"
        $mc.charisma+=1
        mc "Yeah, but I didn't read the fine print, did I? Plus, it was for a good cause.{space=-85}"
        flora angry "Ugh..."
        mc "This is different, I promise!"
      "\"Well, I think you're going to\nbe surprised.\"":
        show flora eyeroll at move_to(.5)
        mc "Well, I think you're going to be surprised."
        flora concerned "Yeah? Like the time you made a dick-sized hole in a box and made me open it?"
        mc "Ahh, good times... but no, nothing like that."
        flora angry "It was gross and stupid!"
        mc "I'm sorry. I was immature back then."
        flora angry "It was literally this summer!"
        "She's right, but to me it feels like a lifetime ago..."
        show flora angry at move_to(.25)
        menu(side="right"):
          extend ""
          "\"I'm sincerely sorry about that, [flora].\"":
            show flora angry at move_to(.5)
            mc "I'm sincerely sorry about that, [flora]."
            "It's one of those things that always filled me with regret. It wasn't even funny. Just messed up and creepy."
            $flora.love+=1
            flora neutral "That's nice of you. I appreciate it."
          "\"It was just a prank, bro. You need to\nget over it.\"":
            show flora angry at move_to(.5)
            $mc.love-=1
            mc "It was just a prank, bro. You need to get over it."
            $flora.lust-=1
            flora angry "Screw you! Seriously."
            show flora angry with vpunch:
              linear .25 zoom 1.5 yoffset 750
            show flora angry:
              linear .25 zoom 1 yoffset 0
            mc "Ow! That's assault!"
            flora concerned "No, it's just a prank. Get over it."
            "Painful, but fair..."
            jump quest_flora_handcuffs_package_making_up
    flora thinking "Okay, so what do you have for me?"
    mc "Your new skirt came in the mail today!"
    flora flirty "Really?"
    mc "Here you go."
    $mc.remove_item("cutie_harlot")
    flora blush "It's perfect!"
    flora blush "Thank you!"
    mc "You're welcome!"
    flora blush "I'm going to go try it on!"
  else:
    "Although [flora] would still look cute in my [item.title_lower], she'll be able to tell that it's not part of Scarlet Starlet's fashion line."
    $quest.flora_handcuffs.failed_item("cutie_harlot",item)
    return

label quest_flora_handcuffs_package_new_look:
  window hide
  if game.location == "home_kitchen":
    if renpy.showing("flora smile"):
      show flora smile at disappear_to_left
    else:
      show flora blush at disappear_to_left
    pause 0.5
    window auto
    "She practically sprinted up the stairs. I haven't seen her this excited in a long time."
    window hide
    call goto_home_hall
    window auto
  elif game.location == "home_hall":
    if renpy.showing("flora smile"):
      show flora smile at disappear_to_right
    else:
      show flora blush at disappear_to_right
    pause 0.5
    window auto
    "I haven't seen her this excited in a long time..."
  mc "[flora]?"
  flora "Yes?!"
  mc "Can I see it?"
  show flora skeptical at appear_from_right
  flora skeptical "Why?"
  mc "Because I want to see your new look!"
  flora skeptical "Fine, just give me a minute."
  window hide
  show flora skeptical at disappear_to_right
  pause 2.0
  window auto
  "Girls, man. They always take the longest time to change..."
  "I would've been done already at this point."
  "Although, I guess I'm not nearly as cute."
  $flora.unequip("flora_shirt")
  $flora.equip("flora_skirt")
  show flora smile_hands_up at appear_from_right
  flora smile_hands_up "What do you think?"
  mc "Whoa!"
  mc "You really pull off the topless look."
  flora eyeroll "Ugh, seriously?"
  flora annoyed "What about the skirt?"
  mc "I mean, yeah, it looks decent."
  flora confused "Just decent?"
  flora confused "Wait."
  window hide
  show flora confused at disappear_to_right
  pause 0.5
  window auto
  "God, I hope she takes off her bra next."
  "But knowing [flora], probably never in a million years."
  "She's such a tease, I swear..."
  window hide
  $flora.equip("flora_business_shirt")
  show flora blush at appear_from_right
  pause 0.5
  window auto
  flora blush "..."
  mc "..."
  flora worried "What?"
  mc "It's just... you look so... mature?"
  flora laughing "Good! I was going for professional, but I'll take it!"
  flora laughing "I'm going to be spending more time at my internship this fall, so I had to update my wardrobe."
  "Man, it's so weird seeing her dressed like this."
  "[flora] has always been so bratty, and almost tomboyish in her ways..."
  "But this is completely different. Mature... womanly, perhaps... and, god help me... sexy."
  flora flirty "Are you going to close your mouth? I'm tired of looking down your throat."
  mc "Sorry, I spaced out there for a moment..."
  mc "Are you... are you going to wear that to school?"
  flora blush "Maybe!"
  "Ugh, she's going to get a lot more attention from guys now..."
  mc "Are you sure?"
  flora worried "Why not?"
  mc "Err... well, I've seen you eat pasta... odds are you're going to ruin the shirt!"
  flora thinking "Hmm... you're right. It would be silly to risk it."
  flora thinking "One second."
  window hide
  show flora thinking at disappear_to_right
  pause 0.5
  window auto
  "I can't believe that worked!"
  "It feels a bit manipulative, but I don't want other guys to hit on her..."
  "Maybe that's selfish, but whatever."
  $flora.equip("flora_shirt")
  show flora smile_hands_up at appear_from_right
  flora smile_hands_up "Ta-da!"
  mc "I like that."
  "Is it weird to feel this relieved about a shirt?"
  "Getting to hold on to the old [flora] a little while longer... is that so bad?"
  mc "I'm going to miss you..."
  flora skeptical "What are you talking about?"
  mc "Nothing..."
  flora afraid "Why do you look so sad all of sudden?"
  mc "Because at the end of the year you're going to start working full time, move out, and..."
  "...get married to a douchebag with a BMW."
  flora afraid "Well, aren't you?"
  "God, she has no idea what a loser I'll become..."
  mc "Yeah, I guess..."
  flora smile "Still a long time before you have to make that decision."
  flora smile "Anywho..."
  window hide
  show flora smile at disappear_to_right
  pause 0.5
  window auto
  "Not nearly long enough..."
  "Fuck me, I really can't mess it up this time."
  "..."
  "Being passive was what got me to that bad place... now it's time\nto act."
  "If that means helping [kate], then so be it."
  mc "[flora]?"
  $flora.unequip("flora_skirt")
  show flora sarcastic at appear_from_right
  flora sarcastic "What?"
  mc "Now you have to help me steal those handcuffs..."
  flora eyeroll "Just let me enjoy the moment..."
  mc "Enjoy away. I'll call you from the school's entrance hall when I'm ready.{space=-55}"
  flora annoyed "Fine."
  hide flora with Dissolve(.5)
  $quest.flora_handcuffs.advance("phone_call")
  return

label quest_flora_handcuffs_package_making_up:
  mc "Maybe this will change your mind?"
  flora afraid "Is that...?"
  $mc.remove_item("cutie_harlot")
  flora smile "Oh my god!"
  mc "You're welcome."
  jump quest_flora_handcuffs_package_new_look

label quest_flora_handcuffs_phone_call_wrong_location:
  if mc.at("school_*"):
    "Need to call her from the entrance hall so I don't miss the opportunity.{space=-50}"
  else:
    "Need to call her from the school's entrance hall so I don't miss the opportunity."
  return

label quest_flora_handcuffs_phone_call:
  $set_dialog_mode("phone_call_plus_textbox","flora")
  $guard.default_name = "Phone" ## Workaround to use the gray textbox for the "Phone" character
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","flora")
  flora "What?"
  mc "It's time to pay up."
  mc "Come to the school's\nentrance hall."
  flora "Fine. Be right there."
  $set_dialog_mode("")
  pause 0.25
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 2.0
  show flora annoyed at appear_from_right
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora annoyed "So, what now?"
  mc "You distract the guard while I sneak into the booth and grab the handcuffs."
  flora confused "Distract how?"
  show flora confused at move_to(.25)
  label quest_flora_handcuffs_phone_call_distractions:
  menu(side="right"):
    extend ""
    "?flora.lust>=6@[flora.lust]/6|{image=flora contact_icon}|{image=stats lust}|\"Get naked and streak{space=-15}\nacross the hall.\"" if not quest.flora_handcuffs["different_distraction"]:
      if renpy.showing("flora thinking"):
        show flora thinking at move_to(.5)
      else:
        show flora confused at move_to(.5)
      mc "Get naked and streak across the hall."
      flora afraid "W-what?"
      mc "You heard me."
      flora embarrassed "Are you being serious right now?"
      show flora embarrassed at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Absolutely. You owe me.\"":
          show flora embarrassed at move_to(.5)
          mc "Absolutely. You owe me."
          flora embarrassed "There has to be another way!"
          mc "I don't think anything else would wake that sloth of a guard from his slumber."
          flora sad "You're probably right..."
          mc "So, you'll do it?"
          $flora.lust+=3
          $flora.love-=1
          flora skeptical "Do I have a choice?"
          mc "Not really. That skirt was crazy expensive."
          mc "Plus, it's our senior year, so dares and pranks are common. You probably won't even get detention."
          flora eyeroll "Whatever."
          flora eyeroll "I'll do it under one condition."
          "Wait, she's actually considering it? There's no way..."
          mc "Name it!"
          flora annoyed "You won't take [jacklyn] to prom at the end of the year."
          if quest.jacklyn_statement.finished:
            "I knew [flora] was a bit jealous, but I didn't expect this..."
            "She looks serious."
          else:
            "That's the oddest request ever..."
            mc "What do you have against [jacklyn]?"
            flora annoyed "None of your business."
          mc "I don't know, [flora]. [jacklyn] is kind of cute..."
          flora annoyed "..."
          show flora annoyed at move_to(.25)
          menu(side="right"):
            extend ""
            "Promise to not take [jacklyn] to prom":
              $jacklyn["prom_disabled"] = True
              show flora annoyed at move_to(.5)
              mc "Okay, fine. I promise."
              $flora.love+=3
              $flora.lust+=3
              flora excited "Good!"
              mc "..."
              show flora neutral with dissolve2
              flora neutral "..."
              flora concerned "Fine."
              window hide
              $flora.unequip("flora_shirt")
              pause 0.5
              window auto
              "Mmm... I've always fantasized about making [flora] do something\nlike this..."
              "Undressing in public. A sight for sore eyes."
              window hide
              $flora.unequip("flora_skirt")
              pause 0.5
              window auto
              flora concerned "Happy?"
              mc "Not quite..."
              flora angry "Ugh!"
              window hide
              $flora.unequip("flora_bra")
              pause 0.5
              window auto
              mc "Almost there!"
              flora angry "I'm going to murder you later..."
              mc "Then I'll die a happy man."
              window hide
              $flora.unequip("flora_panties")
              pause 0.5
              window auto
              flora angry "Happy now, you goddamn pervert?"
              mc "Very..."
              flora concerned "Gross."
              flora concerned "Okay, I'm doing it now."
              window hide
              show flora concerned at disappear_to_right
              pause 0.5
              window auto
              mc "Wait!"
              show flora annoyed at appear_from_right
              flora annoyed "What?"
              mc "Nothing, I just wanted a good look."
              flora cringe "You're the worst!"
              flora cringe "Don't mess this up because I'll never do it again!"
              window hide
              show flora cringe at disappear_to_right
              pause 0.5
              show black onlayer screens zorder 100
              show flora streaking1 at center
              with Dissolve(.5)
              pause 1.0
              hide black onlayer screens with Dissolve(.5)
              window auto
              "[flora] looks nervous."
              "I mean, probably less nervous than I would be in her place..."
              "I'd be shaking, most likely... maybe even crying."
              "But she's always been braver than I."
              "Always been in charge of her own destiny."
              show flora streaking2 with dissolve2
              "She looks left, right, then just goes for it, boobs bouncing!"
              "Right by the [guard]'s booth!"
              "Oh, shit! He saw her, despite his doughnut-preoccupation..."
              show flora streaking3 with dissolve2
              "Run, [flora], run!"
              "..."
              "God, he's slow. It's like he's not even running."
              "He's like... shuffling forward..."
              "Weak legs are heavy, there's doughnut on his sweater already."
              "Anyway, I should probably—"
              guard "Hey, miss!"
              "Oh, wow! The chase is on! I wish I'd brought popcorn..."
              "One man chasing a naked girl."
              "Illegal if it weren't for his uniform."
              "Strange how clothes make all the difference."
              "Or lack there of, in some cases..."
              "[flora] actually looks worried now, glancing over her shoulder."
              "She's given up on covering herself, and allows herself to be on display for the entire entrance hall."
              "How many times have I not dreamed about having her body close\nto mine..."
              "Her hot skin, burning against my own..."
              "Her nipples hard, her pussy wet..."
              "Shit, I really need to stop thinking of her like that."
              "But... it's hard. It gets me hard. She's so cute and annoying at the same time..."
              "It's her fire that makes her so attractive to me... and perhaps the forbidden nature."
              "I'm a total hypocrite for not wanting other guys to look her way, and then making her streak through the school."
              "But somehow... she's doing it for me, and that makes it okay in a weird way."
              "God, my feelings for her are so twisted..."
              "Everything about it is twisted... and somehow, she's okay with it..."
              "Or at least pretends to be..."
              "..."
              "No, she's not pretending. That can't be."
              "She wouldn't get naked for anyone but me..."
              "It's like an unspoken, forbidden, urge inside us both."
              "It's been there for a while, festering in the back of our minds."
              "She can expose her body to the world, but never her mind."
              "That's what's sad and hot at the same time."
              "The world will always keep us apart, but even now her eye catches mine."
              "And there's a twinkle of that dark passion that only I get to see..."
              "Last time around, I ignored it. Pretended it wasn't there."
              "Conforming to society's expectations."
              "But really, what did society ever do for me?"
              "This time around, I will hold her, love her. Make her mine."
              "The consequences be damned."
              "Strange how I decided that just now, in the middle of a chase..."
              "But it doesn't matter. Nothing matters if I don't take risks."
              "Nothing matters if I don't get to have her—"
              show flora streaking4
              flora streaking4 "Eeep!" with vpunch
              window hide
              play sound "falling_thud"
              show black onlayer screens zorder 100 with vpunch
              pause 1.0
              show black onlayer screens zorder 4
              $set_dialog_mode("default_no_bg")
              "The chase ends abruptly."
              "Of course, the idiot guard had to body slam her..."
              "Probably intentional too, that old pervert."
              "He's probably as touch-starved as me."
              "It's in his eyes. Pitiful and pathetic."
              window hide
              show black onlayer screens zorder 100
              show flora sad at Transform(xalign=0.25)
              show guard angry at Transform(xalign=.75)
              show black onlayer screens zorder 100
              pause 0.5
              $mc["focus"] = "flora_handcuffs"
              hide black onlayer screens with Dissolve(.5)
              window auto
              $set_dialog_mode("")
              guard angry "I'm taking you to the principal's office, young lady."
              window hide
              show guard angry at disappear_to_left
              show flora sad at disappear_to_left
              pause 0.5
              window auto
              "All right, here's my chance!"
              $unlock_replay("flora_distraction")
              $quest.flora_handcuffs.advance("steal")
              return
            "Come up with a different distraction":
              $quest.flora_handcuffs["different_distraction"] = True
              show flora annoyed at move_to(.5)
              mc "Sorry, but I'd like to keep my options open."
              $flora.love-=2
              $flora.lust-=1
              flora angry "It's not like you have a chance with her..."
              mc "You never know, [flora]!"
              flora concerned "Whatever."
        "\"No, not really. I would never make you\ndo something like that.\"":
          $quest.flora_handcuffs["different_distraction"] = True
          show flora embarrassed at move_to(.5)
          mc "No, not really. I would never make you do something like that."
          flora laughing "Thanks! I did not expect that from you."
          $mc.love+=1
          mc "I've been shitty to you for a long time... I'm trying to change that."
          $flora.love+=2
          flora blush "That's nice to hear."
          mc "You're still going to help me, though."
      flora thinking "So, you actually have another plan to distract the guard?"
      show flora thinking at move_to(.25)
      jump quest_flora_handcuffs_phone_call_distractions
    "\"Just tell him you lost the key to your locker.\"{space=-40}" if not quest.flora_handcuffs["spent_too_much"]:
      if renpy.showing("flora thinking"):
        show flora thinking at move_to(.5)
      else:
        show flora confused at move_to(.5)
      mc "Just tell him you lost the key to your locker."
      flora annoyed "Only if you pay for a new lock."
      show flora annoyed at move_to(.75)
      menu(side="left"):
        extend ""
        "?mc.money>=20@[mc.money]/20|{image=ui hud icon_money}|\"Sure, I don't mind.\"":
          show flora annoyed at move_to(.5)
          $mc.money-=20
          mc "Sure, I don't mind."
          flora smile "Okay, wait here. I'll send you a text."
          show flora smile at disappear_to_right
          "If she took my money and left, I'm going to be very upset."
          window hide
          show black onlayer screens zorder 100 with Dissolve(.5)
          pause 1.5
          $mc["focus"] = "flora_handcuffs"
          $mc.add_message_to_history("flora", flora, "The potato has left the couch!\nI repeat, the potato has left the couch!")
          play sound "<from 0.25>phone_vibrate"
          pause 0.5
          $set_dialog_mode("phone_message_plus_textbox","flora")
          hide black onlayer screens with Dissolve(.5)
          pause 1.0
          window auto
          "Okay, time to get myself a pair of handcuffs..."
        "\"Not happening.\"":
          $quest.flora_handcuffs["spent_too_much"] = True
          show flora annoyed at move_to(.5)
          mc "Not happening."
          mc "I already spent too much on your stupid skirt."
          flora blush "You mean my totally cute and gorgeous skirt?"
          mc "Right. That's the one."
          flora thinking "Do you have any other ideas, then?"
          show flora thinking at move_to(.25)
          jump quest_flora_handcuffs_phone_call_distractions
    "\"Tell him to handcuff you, and then ask to borrow them.\"" if not quest.flora_handcuffs["way_too_kinky"]:
      $quest.flora_handcuffs["way_too_kinky"] = True
      if renpy.showing("flora thinking"):
        show flora thinking at move_to(.5)
      else:
        show flora confused at move_to(.5)
      $mc.lust+=1
      mc "Tell him to handcuff you, and then ask to borrow them."
      flora concerned "I told you I wouldn't get involved."
      $flora.lust+=1
      flora confident "Besides, that's way too kinky for me."
      mc "Right..."
      flora laughing "Yep!"
      "[flora] would probably be really into it, but as usual, she's being\nall coy."
      "Imagining her in a pair of handcuffs, though... mmm..."
      "Okay, these thoughts really shouldn't enter my head..."
      flora thinking "What other ideas do you have?"
      show flora thinking at move_to(.25)
      jump quest_flora_handcuffs_phone_call_distractions
    "\"Say that you saw a cat stuck in a tree outside.\"{space=-75}":
      if renpy.showing("flora thinking"):
        show flora thinking at move_to(.5)
      else:
        show flora confused at move_to(.5)
      $mc.love+=1
      mc "Say that you saw a cat stuck in a tree outside."
      flora laughing "That's the most cliché thing I've ever heard!"
      mc "And that's exactly why it'll work."
      flora thinking "Hmm... not what I was expecting from you, but I don't mind. It might{space=-5}\nactually work."
      mc "I think it will. And the cat might've gotten down before he got there."
      flora neutral "All right. I'm going over."
      flora neutral "I'll text you when it's time."
      show flora neutral at disappear_to_right
      "The only risk is that the [guard] is too lazy to help..."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 1.5
      $mc["focus"] = "flora_handcuffs"
      $mc.add_message_to_history("flora", flora, "The sloth has left its... sloth house? I repeat, the sloth is outside!")
      play sound "<from 0.25>phone_vibrate"
      pause 0.5
      $set_dialog_mode("phone_message_plus_textbox","flora")
      hide black onlayer screens with Dissolve(.5)
      pause 1.0
      window auto
      "Okay, the [guard] will be back quickly. I better get a move on."
    "?mc.charisma>=3@[mc.charisma]/3|{image=stats cha}|\"Tell him tentacles are coming out of{space=-65}\nthe toilets in the women's bathroom.\"{space=-75}":
      if renpy.showing("flora thinking"):
        show flora thinking at move_to(.5)
      else:
        show flora confused at move_to(.5)
      mc "Tell him tentacles are coming out of the toilets in the women's bathroom."
      $flora.lust+=3
      flora flirty "There are?"
      flora laughing "I mean, eww! That's gross!"
      mc "..."
      flora blush "..."
      flora blush "He'll never fall for that."
      mc "I don't think he's very smart. Plus, he might be a weeb."
      flora laughing "Those two things tend to go hand in hand!"
      mc "Ugh, shut up..."
      flora worried "Fine, I'll do it."
      flora thinking "Wait here... I'll send you a text."
      window hide
      show flora thinking at disappear_to_right
      pause 0.5
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 1.5
      $mc["focus"] = "flora_handcuffs"
      $mc.add_message_to_history("flora", flora, "The snail has left its shell! I repeat, the snail has left its shell!")
      play sound "<from 0.25>phone_vibrate"
      pause 0.5
      $set_dialog_mode("phone_message_plus_textbox","flora")
      hide black onlayer screens with Dissolve(.5)
      pause 1.0
      window auto
      "I can't believe that worked!"
      "Okay, better hurry before he gets back..."
    "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"Tell him you'll buy him a doughnut{space=-45}\nand coffee from the cafeteria.\"":
      if renpy.showing("flora thinking"):
        show flora thinking at move_to(.5)
      else:
        show flora confused at move_to(.5)
      mc "Tell him you'll buy him a doughnut and coffee from the cafeteria."
      flora annoyed "Under what pretense?"
      mc "You're eighteen. He's... well, above eighteen..."
      flora cringe "Gross!"
      show flora cringe at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Honestly, I don't think you need an excuse.\"{space=-30}":
          show flora cringe at move_to(.5)
          $mc.charisma+=1
          mc "Honestly, I don't think you need an excuse."
          flora laughing "You're probably right! He won't question anything as soon as he hears the word \"doughnut.\""
          mc "That's what I'm banking on."
        "\"Tell him [jo] told you to do it. The school's way of thanking him for his service.\"":
          show flora cringe at move_to(.5)
          $mc.intellect+=1
          mc "Tell him [jo] told you to do it. The school's way of thanking him for his service."
          flora thinking "What if he checks with her?"
          mc "He's way too lazy for that."
          flora laughing "You're absolutely right!"
      flora blush "Wait here. I'll text you when the coast is clear."
      show flora blush at disappear_to_right
      "Surely, a doughnut addict like him will fall for this."
      "And getting attention from a cutie like [flora]... there's no way he'll be able to help himself!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 1.5
      $mc["focus"] = "flora_handcuffs"
      $mc.add_message_to_history("flora", flora, "The bear is in the honeypot! I repeat, the bear is in the honeypot!")
      play sound "<from 0.25>phone_vibrate"
      pause 0.5
      $set_dialog_mode("phone_message_plus_textbox","flora")
      hide black onlayer screens with Dissolve(.5)
      pause 1.0
      window auto
      "The cafeteria is right next to his booth... I need to be quick about this.{space=-35}"
  window hide
  $quest.flora_handcuffs.advance("steal")
  $set_dialog_mode()
  pause 0.5
  window auto
  return

label quest_flora_handcuffs_steal_computer_room_door:
  "I used to think that there's always time for a quick porn surf... but it'll have to be later."
  return

label quest_flora_handcuffs_steal_roof_stairs:
  "As much as I'd like to have a lukewarm beer on the roof right now... I'd not like to have a lukewarm beer right now."
  "A true teeter-totter that one."
  return

label quest_flora_handcuffs_steal_homeroom_door:
  "[mrsl] probably has a set of handcuffs as well, but it's not worth gambling on."
  return

label quest_flora_handcuffs_steal_gf_w_corridor:
  "The [guard] will be back any moment. I need to get those handcuffs."
  return

label quest_flora_handcuffs_steal_stairs:
  "This is a good escape route... but the crime has to be committed first."
  return

label quest_flora_handcuffs_steal_cafeteria_doors:
  "Sadly, no time for strawberry juice. Not during an ongoing heist."
  return

label quest_flora_handcuffs_steal_exit_arrow:
  "This is probably my only chance of getting those handcuffs. I can't leave now."
  return

label quest_flora_handcuffs_steal:
  "As expected, he was too lazy to lock the door..."
  "..."
  "There they are!"
  $mc.add_item("handcuffs")
  "Shiny, stainless steel. Robust and escape-proof. These cuffs will come in handy."
  "I probably shouldn't touch his doughnut stash, though. He'll notice if one's missing."
  $quest.flora_handcuffs.finish()
  return

label quest_flora_handcuffs_streaking_aftermath:
  show flora annoyed with Dissolve(.5)
  show flora annoyed with vpunch:
    linear .25 zoom 1.5 yoffset 750
  show flora annoyed:
    linear .25 zoom 1 yoffset 0
  mc "Ouch!"
  flora annoyed "I got detention for a week!"
  mc "I don't see how that's my fault..."
  flora cringe "It was your stupid idea!"
  mc "Well, it wasn't my idea that you get yourself caught."
  flora cringe "Ugh, I will kill you..."
  mc "That'll get you at least two weeks in detention. We don't want that, do we?"
  $flora.love-=2
  flora annoyed "You're the worst! I'll remember this!"
  show flora annoyed with vpunch:
    linear .25 zoom 1.5 yoffset 750
  show flora annoyed:
    linear .25 zoom 1 yoffset 0
  mc "Ouch!"
  hide flora with dissolve2
  "Well, that could've gone better..."
  "You win some, you lose some, I guess."
  $quest.flora_handcuffs["streaking_aftermath"] = True
  return
