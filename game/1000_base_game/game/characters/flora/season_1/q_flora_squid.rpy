init python:
  renpy.music.register_channel("ambient", "sfx", True, True)

image flora_squid_anal = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-86,-46), Transform("jacklyn jacklyn_anal_insert", size=(288,162))), "ui circle_mask"))
image flora_squid_pegging = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-124,-46), Transform("jacklyn mc_pegged_in", size=(288,162))), "ui circle_mask"))
image flora_squid_pepelepsi = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-115,-83), Transform(LiveComposite((1920,1080), (0,0), "flora FloraShowerPoseE1", (0,0), "pee_splatter"), size=(404,227))), "ui circle_mask"))
image flora_squid_hug = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-74,-62), Transform("flora hugging_flora_content", size=(327,184))), "ui circle_mask"))


label quest_flora_squid_start:
  play sound "<from 1.7 to 3>door_knock"
  pause 1.5
  mc "Hey, [flora]? Have you seen my electric toothbrush?"
  if flora["madeamends"]:
    flora "Yes, but I'm using it right now!"
    mc "What do you mean, {i}you're using it?{/}"
    call quest_flora_squid_happy
  else:
    flora "Go away!"
    "Why is she mad now?"
    "Can't she just be happy all the time like [lindsey]?"
    mc "What's wrong?"
    flora "I just can't deal with you right now!"
    mc "What did I do?"
    flora "You know what you did!"
    mc "I really don't!"
    flora "You made your choice! Don't talk to me!"
    "What the hell is she talking about?"
    "Hmm..."
    mc "Is this about [jacklyn]?"
    flora "..."
    "I'll take that as a yes."
    if school_entrance["bus_jacklyn"] or school_entrance["bus_lust"] or school_entrance["bus_love"]:
      flora "You left with her!"
      mc "You were throwing a fit."
      flora "I was not throwing a fit!"
      mc "..."
      mc "[flora], are you... jealous?"
      flora "N-no! Not even a little bit!"
      menu(side="middle"):
        extend ""
        "?not quest.jacklyn_statement['ending']@|\"Well, I did go with her, but\nnothing happened.\"":
          mc "Well, I did go with her, but nothing happened."
          flora "Really...?"
          mc "Yes! She's just teaching me how to be an artist."
          flora "And you want me to believe that?"
          mc "Ask her."
          flora "I would, but my fingers are wet..."
          mc "..."
          mc "[flora], what are you doing with my toothbrush?"
          call quest_flora_squid_happy
        "?quest.jacklyn_statement['ending'] in ('anal','pegging')@|{image=flora_squid_pegging}|\"Are you sure about that?\"" if quest.jacklyn_statement["ending"] == "pegging":
          jump quest_flora_squid_jealous
        "?quest.jacklyn_statement['ending'] in ('anal','pegging')@|{image=flora_squid_anal}|\"Are you sure about that?\"" if quest.jacklyn_statement["ending"] != "pegging":
          label quest_flora_squid_jealous:
          mc "Are you sure about that?"
          flora "Yes!"
          mc "We had a great time..."
          mc "We looked up at the stars. Pondered life's great meanings. Made a statement on the bus."
          if quest.jacklyn_statement["ending"] == "anal":
            mc "Then, I made a statement in [jacklyn]'s ass."
            $flora.love-=5
            flora "You're horrible!"
            mc "It was quite lovely! Two consenting adults, enjoying each other's bodies.{space=-90}"
            mc "Nothing horrible about that."
            flora "I did not consent!"
            mc "I don't remember you being present."
            flora "Ugh! Go away!"
          elif quest.jacklyn_statement["ending"] == "pegging":
            mc "Then, we—"
            "..."
            "Maybe I shouldn't tell [flora] that [jacklyn] pegged me..."
#           "That's probably not something you want your sister to know." ## Incest patch ##
            "She could get all sorts of bad ideas."
            flora "You, what?"
            mc "Look, it's complicated."
            $flora.love-=1
            flora "You fucked her, didn't you?!"
            mc "I mean... technically, not {i}me...{/}"
            flora "You know what, I don't want to know."
            flora "Leave me alone!"
    else:
      "But now I'm confused... I told [jacklyn] to go after her."
      mc "What happened?"
      flora "Nothing! You're both selfish people!"
      mc "I don't know what [jacklyn] said to you, but I asked her to go after you.{space=-35}"
      mc "I thought that's what you wanted..."
      flora "It's all your fault! You brought her into my life!"
      mc "That's not how I remember it."
      mc "As I recall, you were the one who decided to bake for her."
      flora "That... was for a stupid grade..."
      mc "Well, that's what got you on her radar."
      mc "If you don't want to see her again, you don't have to. Except in class... but that's hard to get around."
      flora "That's not what I meant!"
      mc "What did you mean, then?"
      mc "I was just trying to be helpful..."
      flora "That's the whole problem!"
      mc "..."
      "I'm so lost."
      mc "I still don't get what choice I made..."
      flora "You went on a date with her!"
      mc "Yeah, but then I took a step back when you got upset..."
      flora "But it was too late then!"
      mc "What do you mean? We didn't even shake hands."
      flora "Yes, but... seeing you with her..."
      flora "...it made me realize something."
      mc "And what was that?"
      flora "..."
      mc "Well, what was it?"
      flora "Nothing! Go away!"
      "I really don't get girls sometimes..."
  "Hmm..."
  "I really should give [flora] some privacy, but..."
  "Ever since that incident with the tree, she's been moody and unpredictable. More so than usual."
  "And who can blame her?"
  "She lived through a nightmare that she can't talk about or even process."
  "Who would believe her that a bonsai grew into a sentient tree monster and tried to... seed her?"
  "I can hardly believe it happened, and I saw it with my own eyes!"
  "..."
  "Maybe kindness is the only way to help her..."
  mc "Hey, [flora]?"
  flora "What?"
  mc "Is there anything I can do for you?"
  flora "Shrimps!"
  mc "What?"
  flora "A bucket of shrimps!"
  mc "I'm not getting you shrimps..."
  flora "Then, go away!"
  mc "Fine, fine, fine."
  "I hope she's okay in there... I guess I'll check up on her again tomorrow."
  $quest.flora_squid.start()
  return

label quest_flora_squid_happy:
  flora "I'm using it to make my squid happy!"
  "Squid? Is that a euphemism?"
  mc "Can you unlock the door?"
  flora "No! I'll get the floor wet!"
  "Jesus."
  mc "How much longer are you going to, err... {i}play with your squid?{/}"
  flora "I don't know! It needs a lot of attention!"
  flora "Maybe a few days!"
  mc "Days?!"
  flora "Yeah, I don't want my squid to dry up like [jo]'s did!"
  mc "Wait, what?"
  flora "I'm following her old manual!"
  "That's oddly hot in a way..."
# "Secret techniques, passed down through generations." ## Incest patch ##
# "[jo]'s very own Mama Sutra." ## Incest patch ##
  $quest.flora_squid["squid_happy"] = True
  return

label quest_flora_squid_sleep:
  menu(side="middle"):
    "What time is it?"
    "Sleeping time":
      "I thought girls were typically noisy when they play with their squid, but [flora] has been quiet most of the night..."
      "If she keeps up the secrecy, I'll just have to bring down the battering ram from the attic."
      window hide
      call home_bedroom_bed_interact_force_sleep
      $quest.flora_squid.advance("trail")
      window auto
    "Brooding time":
      "Rest is for the dead."
      "And those who are afraid of energy drinks."
  return

label quest_flora_squid_trail_hall:
  "What the hell is this?"
  "..."
  "Did [flora] finally turn into a snail?"
  "I knew I shouldn't have put that slug in her milk when we were little...{space=-20}"
  $quest.flora_squid["snail_flora"] = True
  return

label quest_flora_squid_trail_kitchen:
  "Hmm... the trail leads out the door..."
  "Did the seeds of that tree actually impregnate her? Did her water just break?"
# "Am I going to be the uncle of [flora]'s little saplings?" ## Incest patch ##
  "That thought is almost too hard to bear..."
  $quest.flora_squid["water_break"] = True
  return

label quest_flora_squid_trail_ground_floor:
  "Well, the trail is gone, but there's this suspicious-looking puddle over there..."
  $quest.flora_squid["suspicious_puddle"] = True
  return

label quest_flora_squid_trail_flora:
  show flora sad_wet with Dissolve(.5)
  "[flora] looks like she peed herself..."
  mc "Is everything okay?"
  flora sad_wet "No! Nothing is okay."
  show flora sad_wet at move_to(.25)
  menu(side="right"):
    extend ""
    "?quest.flora_cooking_chilli.finished and quest.flora_cooking_chilli['distraction_method'] and not ('love11' in game.unlocked_stat_perks or 'lust11' in game.unlocked_stat_perks)@|{image=flora_squid_pepelepsi}|\"Did you have another Pepelepsi-moment?\"":
      show flora sad_wet at move_to(.5)
      mc "Did you have another Pepelepsi-moment?"
      flora embarrassed_wet "I thought we wouldn't talk about that!"
      mc "I mean, I'm just asking."
      mc "I didn't mind it. You know... that you peed on me."
      flora skeptical_wet "Gross."
      mc "It's just bodily fluids, and yours happen to be quite lovely."
      flora skeptical_wet "You're weird. Stop talking about my pee!"
      mc "It's a little hard when it's right there."
      flora afraid_wet "This is just water, you idiot!"
      mc "I mean, that's what all girls say. But it's actually pee, isn't it?"
      $flora.lust+=3
      flora embarrassed_wet "What? No! This is water! I swear!"
      mc "If that's true, why did you pour it all over yourself?"
      flora sad_wet "It wasn't me..."
      mc "Oh, yeah? Who was it?"
      flora sad_wet "My squid."
      mc "Right. Your squid..."
      flora sad_wet "It's true. It's a very naughty squid."
      mc "No kidding! You left a trail."
      flora embarrassed_wet "Can we change the topic, please?"
    "\"Why are you wet?\"" if quest.flora_squid["squid_happy"]:
      show flora sad_wet at move_to(.5)
      mc "Why are you wet?"
      flora sad_wet "I'm not very good at taking care of my squid... It made a mess all the way here."
      flora sad_wet "I think I teased it too much, and now it's really excited."
      mc "That's usually what happens when you tease a squid, [flora]."
      flora embarrassed_wet "I know! But I had so much fun!"
      mc "Are you here to ask the [nurse] for help?"
      flora afraid_wet "What does she know about naughty squids?"
      mc "Oh, I think she knows a thing or two. Her squid is also quite naughty.{space=-15}"
      flora smile_wet "Oh, my god! She also has a squid?"
      mc "[flora], I think you need to pay more attention during biology class..."
      flora skeptical_wet "Whatever."
    "\"Are you turning into a slug?\"":
      show flora sad_wet at move_to(.5)
      mc "Are you turning into a slug?"
      flora skeptical_wet "That's not funny."
      mc "It wasn't meant to be!"
      mc "Do you know how Spider-Man got his powers?"
      mc "I can totally see you sliding down the windows of skyscrapers, leaving wet trails of mucus in your wake."
      $flora.love-=1
      flora sad_wet "I can't take your bullshit right now..."
    "?quest.flora_bonsai.finished and quest.flora_bonsai['hug']@|{image=flora_squid_hug}|\"You look like you need\nanother hug.\"":
      show flora sad_wet at move_to(.5)
      mc "You look like you need another hug."
      flora sad_wet "If this is a trick, I'm not in the mood."
      mc "I would never do something like that."
      flora skeptical_wet "I remember one time when—"
      mc "Okay, I admit. I've done some bad things in the past, but I'm a changed man."
      flora sad_wet "Okay, fine..."
      mc  "Just like that?"
      flora sad_wet "I know you've been trying lately. I guess I trust you."
      mc "Come here..."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.25
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "Wrapping my arms around [flora] fills me with a special kind of happiness."
#     "It's one of the few intimate things that are socially acceptable for us.{space=-15}" ## Incest patch ##
#     "Here I am, just hugging my sister." ## Incest patch ##
#     "No one is going to find that weird." ## Incest patch ##
      "No one needs to know that I'm enjoying the contours of her soft form.{space=-45}"
      "The way her head rests against my chest..."
      "The scent of wild strawberries from her hair..."
      flora "Did you just smell me?"
      mc "What? No!"
      flora "I heard you sniff!"
      mc "It's just my allergies."
      flora "What allergies?"
      show flora skeptical_wet
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      mc "I guess I'm allergic to you being nice."
      mc "Who would've thought, right?"
      $flora.love+=2
      flora smile_wet "I'm always nice, so I think we would've discovered that sooner!"
      mc "I don't know, [flora]. It's not like we know everything about each other.{space=-25}"
      mc "For example, I just recently discovered that your hair smells lovely."
      flora afraid_wet "I knew it!"
  mc "Well, are you going to tell me what really happened here?"
  flora sad_wet "You're not going to believe me, so let's just move on."
  mc "Is there anything I can do to help?"
  flora sad_wet "Would you help me look for my squid?"
  "This sounds like the opening to a weird porno..."
  mc "I mean, I guess. Have you looked in your pants?"
  flora skeptical_wet "If you're not going to take it seriously..."
  mc "Fine! I'll take it seriously. When did you last see it?"
  flora sad_wet "It grew too big for my little fishbowl, so I had to move it to the aquarium here."
  flora sad_wet "I was checking the saline levels... and then it was gone!"
  mc "The squid disappeared?"
  flora embarrassed_wet "Yes! I only looked away for a few moments!"
  mc "Well, it couldn't have gotten far, right?"
  mc "Have you looked under the furniture?"
  flora sad_wet "I've looked everywhere in this corridor..."
  mc "Maybe it slipped into the principal's office?"
  flora sad_wet "I checked it, and I also checked the [nurse]'s office."
  mc "Maybe someone took it?"
  flora afraid_wet "Who would do such a thing?"
  mc "I can think of a few people..."
  flora embarrassed_wet "We have to find it before it's too late!"
  mc "Okay, I guess we should start looking..."
  mc "There are only a few places here that are wet enough for a squid."
  mc "Follow me!"
  $mc["focus"] = "flora_squid"
  $quest.flora_squid.advance("search")
  hide flora with Dissolve(.5)
  return

label quest_flora_squid_search:
  "A detective would call these puddles a lead."
  "I call it a mess."
  "What the hell happened here?"
  $quest.flora_squid["mess_of_puddles"] = True
  return

label quest_flora_squid_search_puddles:
  mc "[flora], take a look at this."
  show flora thinking with Dissolve(.5)
  flora thinking "You're right, this could be the work of [cuthbert]!"
  mc "[cuthbert]?"
  flora blush "Yeah, that's the name of my squid."
  mc "I had no idea girls named their squids."
  mc "I also had no idea a squid could make such a mess... It must've been very stimulated."
  flora annoyed "Can you stop being lewd?"
  mc "Get your mind out of the gutter, [flora]! I'm just talking about your pet.{space=-35}"
  flora eyeroll "Right..."
  mc "Either way, it doesn't appear to be here."
  flora sad "Do you think it could've crawled into the fine arts wing?"
  mc "I don't know. There's no water there..."
  mc "If I were a squid, I'd go toward the nearest body of water."
  flora afraid "The women's bathroom? Okay, hurry!"
  show flora afraid at disappear_to_right
  window hide
  pause 1.0
  window auto
  $quest.flora_squid.advance("choice")
  return

label quest_flora_squid_choice:
  show flora embarrassed with Dissolve(.5)
  flora embarrassed "Come on!"
  mc "I can't go in there. I'll get detention."
  flora afraid "Detention?! This is [cuthbert]'s life we're talking about!"
  show flora afraid at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Sorry, I'm not risking that.\"":
      show flora afraid at move_to(.5)
      mc "Sorry, I'm not risking that."
      flora angry "I thought you were going to help me!"
      mc "Yes, but I can't just walk into the women's bathroom in broad daylight!{space=-50}"
      flora angry "It's a matter of life and death!"
      mc "Stop wasting time, then."
      $flora.love-=2
      flora angry "Ugh, you're the worst..."
      $quest.flora_squid.advance("wait")
      show flora angry at disappear_to_right
      "Well, that certainly could've gone better."
      "I guess I'll just wait here..."
      return
    "\"Fine... let's go.\"":
      show flora afraid at move_to(.5)
      mc "Fine... let's go."
      $flora.love+=2
      flora smile "Thank you!"
      show flora smile at disappear_to_right
      window hide
      pause 0.5
      window auto
      jump goto_school_bathroom

label quest_flora_squid_wait_door_bathroom:
  mc "[flora]? How's it going?"
  "..."
  "No answer."
  "She must be snorkeling a toilet or something..."
  return

label quest_flora_squid_wait_door_gym:
  "Hopefully, no jocks will find me trespassing while I wait for [flora]..."
  return

label quest_flora_squid_wait_back:
  "[flora] is already pissed. Leaving now would make her explode."
  "Actually... hmm..."
  "Nah, I'd probably have to clean up the mess."
  return

label quest_flora_squid_wait_mrsl:
  show mrsl thinking with Dissolve(.5)
  mrsl thinking "..."
  "[mrsl] looks lost in thought..."
  mrsl thinking "{i}*Sniff, sniff*{/}"
  mc "What are you doing?"
  show mrsl surprised with dissolve2
  mrsl surprised "[mc]! Don't sneak up on an old lady like that!" with vpunch
  show mrsl surprised at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You're not that old!\"":
      show mrsl surprised at move_to(.5)
      mc "You're not that old!"
      mrsl flirty "You little charmer..."
      mrsl flirty "You make me miss the old days."
      mc "I don't know, I always found the 90s pretty slow."
      $mrsl.lust+=2
      mrsl laughing "Oh, you're so sweet! I could eat you right up."
      mrsl thinking "{i}*Sniff, sniff*{/}"
      mc "Sorry, but why do you keep sniffing?"
      mrsl surprised "Can you not smell that?"
    "\"Sorry, but have you seen a squid?\"":
      show mrsl surprised at move_to(.5)
      mc "Sorry, but have you seen a squid?"
      mrsl skeptical "What do you mean by that?"
      mc "[flora] has lost her pet squid... I assume you haven't seen it."
      mrsl thinking "I'm afraid not."
      mrsl thinking "Have you looked in the aquarium downstairs?"
      mc "Thanks for the tip..."
      mrsl cringe_hands_up "{i}*Sniff, sniff*{/}"
      mc "Sorry, but why do you keep sniffing?"
      mrsl cringe_hands_up "Can you not smell that?"
  mc "Smell what?"
  mrsl annoyed "Oh, this is not good. Not good at all."
  "What is she talking about?"
  "This corridor smells like it always does — sweat and unbridled testosterone."
  mrsl concerned "Excuse me."
  show mrsl concerned at disappear_to_left
  "[mrsl] looked concerned..."
  "What kind of smell would cause her to run off like that?"
  "..."
  "Might as well have a look around while waiting for [flora]..."
  $quest.flora_squid["smell_concern"] = True
  return

label quest_flora_squid_wait_trophy_case:
  "Hmm... odd. The glass is slightly open."
  "The case containing the school's pride is usually locked up tightly."
  "..."
  "[mrsl] was right, there's a faint smell coming from inside..."
  "Smells vaguely burnt, but not quite..."
  "Like an overheated RC car or drill. Kind of like burning ozone."
  "..."
  "Why would the trophy case smell like that?"
  "Maybe I can find the source of the smell if I—"
  mc "Ouch!" with vpunch
  "What the hell? Glass shards?"
  "..."
  window hide
  show misc broken_snowglobe with Dissolve(.5)
  window auto
  "Why is there a broken snowglobe behind the trophies?"
  "..."
  "Odd. Very odd."
  window hide
  show flora confused
  hide misc broken_snowglobe
  with Dissolve(.5)
  window auto
  flora confused "Are you trying to rob the trophy case?"
  mc "I mean, that's the only way I'll ever get a trophy..."
  flora sarcastic "Ain't that the truth!"
  mc "Whatever, dude. Did you find your squid or not?"
  flora cringe "I've told you not to call me that!"
  flora cringe "But no, still no sign of poor [cuthbert]..."
  show flora cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Have you looked in your pants?\"":
      show flora cringe at move_to(.5)
      mc "Have you looked in your pants?"
      flora annoyed "What's that even supposed to mean?"
      mc "Maybe squid is another word for octopus... with an emphasis on \"pus.\"{space=-55}"
      flora cringe "You're gross. Stop talking."
      mc "If I stop talking, nothing will be said."
      flora confused "That's the whole point..."
      mc "Do you really want to live in silence, [flora]?"
      mc "A home devoid of mirth and the laughter of children?"
      flora eyeroll "Shut up."
      mc "As you wish."
      show flora annoyed with dissolve2
      flora annoyed "..."
      mc "..."
      flora eyeroll "I'm leaving."
      mc "..."
      show flora annoyed with dissolve2
      flora annoyed "..."
      mc "..."
      flora angry "Fine, you can talk! Just don't say stupid things."
      $mc.charisma+=2
      mc "I never do!"
      mc "Except that one time at band camp..."
      mc "And that one time when you asked about cereal and I said they're in the TV..."
      mc "And that one time when you were sick and—"
      flora angry "I'm leaving!"
      mc "But where are you going?"
      flora thinking "I need to find [cuthbert]..."
      mc "Maybe he's outside somewhere?"
    "\"Maybe we should expand our horizons?\"":
      show flora cringe at move_to(.5)
      $mc.intellect+=2
      mc "Maybe we should expand our horizons?"
      flora thinking "That might actually be a good idea..."
      mc "That's usually the case with my ideas."
      flora laughing "Right!"
      mc "I'm serious!"
      mc "You're looking at the inventor of the self-demoisturizing sock."
      flora worried "You're weird."
      mc "I've been called worse things by better people."
      flora cringe "You're not going to start quoting Canadian politicians again! I won't have it!"
      mc "Every single day, we need to choose hope over fear, diversity over division!"
      flora annoyed "Ugh, I'm leaving!"
      mc "Where to?"
      flora thinking "I don't know, but I have to find [cuthbert]..."
    "\"[mrsl] is getting hotter every day, don't you think?\"":
      show flora cringe at move_to(.5)
      mc "[mrsl] is getting hotter every day, don't you think?"
      flora confused "What does that have to do with anything?"
      mc "So, you agree?"
      flora annoyed_texting "..."
      mc "Who are you texting?"
      flora annoyed_texting "Nobody. I'm deleting you from my contacts."
      mc "That's okay... I'll just add myself back later."
      flora annoyed_texting_eyes_up "I've changed my password!"
      mc "Oh, I know."
      mc "Did you really think I wouldn't figure it out? \"Cutethulhu123\" is pretty obvious."
      flora laughing "That's not it at all!"
      mc "Oh, yeah? Then how would I know that you have a topless picture of Dr. Octopus as your background?"
      flora angry "That's not true!"
      mc "It'll be our little secret."
      flora angry "I'm telling [jo] about this..."
      mc "Well, then it's not really a secret anymore, is it?"
      $flora.lust+=2
      flora angry "Ugh, shut up!"
      mc "Maybe [cuthbert] is outside somewhere?"
      flora thinking "Maybe..."

label quest_flora_squid_wait_or_bathroom:
  mc "I feel like we've checked the entire school."
  mc "There's just one more body of water that I know of..."
  flora worried "What? Where?"
  mc "It's in the forest... although, I'm not sure it's a good idea for you to go out there."
  flora worried "Why not?"
  mc "Because that's where the... tree monster... escaped to."
  flora afraid "T-tree monster?"
  mc "Do you not remember getting assaulted by that tree?"
  if quest.flora_bonsai["hug"]:
    mc "If I hadn't been there to stop it, who knows what it would've done to you..."
  else:
    mc "It bound you in its vines and had its way with you..."
    mc "I got there too late..."
  flora embarrassed "T-that actually happened?"
  mc "Yeah..."
  flora sad "Oh, my god."
  flora sad "All this time, I wasn't sure if I'd imagined it or not."
  if quest.flora_bonsai["hug"]:
    flora blush "You saved me."
    mc "I mean, I guess..."
    mc "I don't really think of it like that."
    flora blush "No, you did."
    flora blush "I know you did."
    mc "You would've done the same for me."
    flora worried "So, a tree did that?"
  else:
    flora crying "I thought it was a fever dream or the painkillers..."
    mc "I'm so sorry..."
    flora crying "It's not your fault."
    "Ugh... if only she knew."
    "I'm such a piece of shit..."
    mc "I'm really sorry!"
    flora crying "I know you wouldn't have let it happen if you were there..."
    flora crying "And you did chase it off."
    flora crying "Thank you for that."
    "Fuck me, man..."
    "How will I be able to look myself in the mirror after this?"
    mc "You... would've done the same for me."
    flora sad "So, a tree did that?"
  mc "Yeah, it was the [nurse]'s bonsai..."
  mc "I don't know how it grew so big, but it's still out there somewhere."
  flora worried "We need to find [cuthbert], though..."
  mc "Maybe we should grab some weapons just in case?"
  flora thinking "Yeah, that would probably be a good idea."
  mc "Okay, I'll be back in a bit!"
  $quest.flora_squid.advance("weapon")
  if game.location == "school_bathroom":
    call goto_school_first_hall_east
  else:
    hide flora with dissolve2
  "Something that can cut through vines would be good..."
  if mc.owned_item("machete"):
    "Luckily, no one has confiscated my machete yet."
  $mc["focus"] = ""
  return

label quest_flora_squid_bathroom:
  show flora neutral with Dissolve(.5)
  flora neutral "There are many places to look here. Help me out, okay?"
  mc "If you think I'm going to touch those toilets, you're out of your mind.{space=-10}"
  flora angry "This is a life or death situation!"
  mc "You take the toilets, then. I'll look elsewhere."
  flora angry "..."
  flora angry "Fine!"
  $quest.flora_squid.advance("bathroom")
  hide flora with Dissolve(.5)
  $quest.flora_squid["toilet_diving"] = True
  return

label quest_flora_squid_bathroom_flora1:
  mc "[flora]? What are you doing?"
  flora "I'm looking for [cuthbert]! What the hell does it look like?"
  mc "It looks like you're about to dive into the toilet..."
  flora "That's disgusting! Leave me alone!"
  mc "Whatever, dude."
  flora "Stop calling me that!"
  return

label quest_flora_squid_bathroom_door:
  "Let's be honest, there probably aren't any squids here."
  "Still a couple of places left to look in, though... Might as well be thorough."
  return

label quest_flora_squid_bathroom_water_bottle:
  "Well, it's technically a body of water..."
  "..."
  "Nope, no squid."
  if not quest.flora_squid["water_bottle_searched"]:
    $quest.flora_squid["water_bottle_searched"] = True
    $quest.flora_squid["bathroom_interactables"] += 1
  return

label quest_flora_squid_bathroom_shower1:
  "Who doesn't like when girls get wet?"
  "..."
  "Squids, apparently."
  "None here."
  if not quest.flora_squid["shower_searched"]:
    $quest.flora_squid["shower_searched"] = True
    $quest.flora_squid["bathroom_interactables"] += 1
  return

label quest_flora_squid_bathroom_shower2:
  "Hmm... no sign of [cuthbert] here..."
  "However, it seems like someone's been trying to Scrooge McDuck this bitch up."
  $mc.money+=200
  $quest.flora_squid["scrooge_mcduck"] = True
  if not quest.flora_squid["shower_searched"]:
    $quest.flora_squid["shower_searched"] = True
    $quest.flora_squid["bathroom_interactables"] += 1
  return

label quest_flora_squid_bathroom_urinal:
  mc "[cuthbert], yo-ho!"
  mc "Are you down there?"
  "..."
  "Nope."
  if not quest.flora_squid["urinal_searched"]:
    $quest.flora_squid["urinal_searched"] = True
    $quest.flora_squid["bathroom_interactables"] += 1
  return

label quest_flora_squid_bathroom_flora2:
  show flora sad with Dissolve(.5)
  mc "You know, I don't think the squid is here..."
  flora skeptical "Did you even look anywhere?"
  mc "Yes, I did!"
  flora skeptical "Where did you look?"
  show flora skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"In [lindsey]'s water bottle.\"":
      show flora skeptical at move_to(.5)
      mc "In [lindsey]'s water bottle."
      flora angry "Are you being serious?!"
      mc "What do you mean?"
      flora angry "This is a life or death situation, and you're just dicking around!"
      flora angry "I knew you would be no help!"
      mc "Hey, I also checked everywhere else! I just checked the bottle to be thorough."
      flora sad "Oh... okay..."
      mc "That's it?"
      mc "After yelling at me, even though I'm trying to help you?"
      flora sad "I'm sorry... I'm just really worried."
      mc "It's okay. Don't worry about it."
      $flora.love+=2
      flora sad "Thanks..."
    "\"In the showers.\"":
      show flora skeptical at move_to(.5)
      mc "In the showers."
      flora sad "And he wasn't there?"
      mc "Nope. I didn't even find any squid slime."
      flora embarrassed "Don't call it that!"
      mc "What's it called, then?"
      flora embarrassed "It's moisture!"
      mc "Really, [flora]?"
      flora skeptical "Yes, really!"
      mc "Well, either way, I didn't find him."
    "\"In the urinal.\"":
      show flora skeptical at move_to(.5)
      mc "In the urinal."
      flora cringe "What?"
      mc "I, uh... had to take a piss as well, so I thought it was convenient..."
      flora cringe "You potentially peed on [cuthbert]?"
      mc "No! He wasn't there."
      flora annoyed "That doesn't make it any better!"
      mc "Do you even think he can tell the difference between pee and water?{space=-10}"
      flora annoyed "Maybe..."
      mc "How about we test it out after we find him?"
      flora eyeroll "Why?"
      mc "For science, [flora]!"
      flora annoyed "You just want to pee on my squid, don't you?"
      if quest.flora_cooking_chilli.finished and quest.flora_cooking_chilli["distraction_method"] and not ("love11" in game.unlocked_stat_perks or "lust11" in game.unlocked_stat_perks):
        mc "I mean, you peed on me, so it's only fair."
        flora afraid "That did not happen!"
        mc "What do you mean? I literally poured Pepelepsi on your pu—"
        flora embarrassed "Oh, my god! Be quiet!"
        mc "So, you admit it?"
        flora sad "You're enjoying this, aren't you?"
        mc "Just a little..."
        flora sad "Fine, it happened. Just be quiet."
        mc "Want to do it again some time?"
        $flora.lust+=3
      else:
        mc "Would you like me to?"
        flora afraid "W-what?"
        mc "I'm very open-minded, [flora]."
        mc "If you want me to pee on your squid, I'll happily oblige."
        mc "If that's what makes you excited..."
        $flora.lust+=2
      flora skeptical "You're disgusting."
      mc "That's not a no!"
      flora sad "Let's just find [cuthbert]..."
  flora sad "Where could he possibly be?"
  mc "I don't know..."
  jump quest_flora_squid_wait_or_bathroom

label quest_flora_squid_weapon_fire_axe:
  "This thing is for emergencies. Surely, a thirsty sentient tree qualifies as such?"
  $school_roof_landing["fire_axe_taken"] = True
  $mc.add_item("fire_axe")
  $quest.flora_squid.advance("ready")
  return

label quest_flora_squid_weapon_machete:
  "Yeah, this is perfect for cutting down trees — sentient or otherwise."
  $home_hall["machete_taken"] = True
  $mc.add_item("machete")
  $quest.flora_squid.advance("ready")
  return

label quest_flora_squid_ready:
  show flora cringe with Dissolve(.5)
  flora cringe "What took you so long?"
  mc "I went as fast as I could!"
  flora annoyed "Well, did you find a weapon?"
  mc "Yeah, don't worry. I'll protect you, milady!"
  flora eyeroll "Dork."
  mc "[lindsey] would've liked that..."
  flora annoyed "Do I look like [lindsey] to you?"
  mc "Not really. She's less moody."
  flora annoyed "Ugh... let's go!"
  show flora annoyed at disappear_to_left
  window hide
  pause 0.5
  window auto
  $quest.flora_squid.advance("forest_glade")
  jump goto_school_forest_glade

label quest_flora_squid_forest_glade:
  "Seems like we made it here without getting ambushed by the undergrowth..."
  "At least, not the sentient type."
  show flora worried with Dissolve(.5)
  flora worried "Oh, god! The stream is so polluted!"
  flora thinking "Who would do such a thing to nature?"
  mc "Uh, I don't know... Probably some big corporation..."
  flora sad "Come on! Help me look for [cuthbert]!"
  window hide
  $flora.unequip("flora_shirt")
  pause 0.5
  window auto
  mc "Whoa, there!"
  flora afraid "What? We're not going to find my squid without getting in the water!{space=-15}"
  mc "I mean, sure... but the water isn't exactly clean."
  flora embarrassed "Yeah, so we got to find [cuthbert] before he gets poisoned!"
  window hide
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  pause 0.5
  window auto
  mc "If you say so..."
  $quest.flora_squid["polluted_stream"] = True
  $mc["focus"] = "flora_squid"
  hide flora with Dissolve(.5)
  return

label quest_flora_squid_forest_glade_road_left:
  "[flora] is knee-deep in the river. She might drown if I leave her here..."
  return

label quest_flora_squid_forest_glade_flora:
  flora "Help me look!"
  return

label quest_flora_squid_forest_glade_stream_bottom:
  "[cuthbert] might very well be in these murky waters. It's hard to know for sure."
  menu(side="middle"):
    extend ""
    "Search":
      "All right... might as well jump in."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "Fuck. How am I ever going to find anything here?"
      "I need to comb through the stream."
      "..."
      "Ugh, the chemicals are burning my skin!"
      "..."
      "If it gets into my eyes, I might turn into Matt Murdock or some shit...{space=-5}"
      "..."
      $quest.flora_squid["tree_monster"] = True
      "Okay, the bottom part of the river is clear."
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
    "Watch over [flora]":
      "I better make sure nothing happens to her..."
  return

label quest_flora_squid_forest_glade_stream_middle:
  "Phew! I need to catch my breath..."
  "Man, where's that stupid squid hiding?"
  "..."
  "Time to search the middle section of the river, I guess."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "..."
  "Ew! What is this?"
  "It looks like... a partially dissolved mannequin arm?"
  "Hmm... I could use that to..."
  "Nah, that's too depraved, even for me."
  "..."
  "Come on, [cuthbert]! Where are you hiding?"
  "..."
  flora "Eeep!"
  "Crap."
  $flora.unequip("flora_shirt")
  if quest.flora_handcuffs > "package":
    $flora.unequip("flora_skirt")
  else:
    $flora.unequip("flora_pants")
  show flora afraid_vines with Dissolve(.5)
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "You look away for one fucking minute..."
  flora embarrassed_vines "Help!"
  "Fuck, what do I do?"
  "I can't just chop at [flora]. She'll get hurt."
  show flora embarrassed_vines:
    easeout 2.0 xoffset 125 yoffset 925
  "Oh, god! It's trying to drag her away again!{w=0.5}{nw}"
  "I'll have to cut around her!{w=0.5}{nw}"
  window hide
  show black onlayer screens zorder 100
  hide screen interface_hider
  with Dissolve(.5)
  pause 1.0
  $mc["focus"] = ""
  $game.ui.hide_hud = True
  show flora vine_fight_face3_vine1_vine2_vine3_vine4:
    xoffset 0 yoffset 0
  hide black onlayer screens
  with Dissolve(.5)
  $renpy.transition(Dissolve(0.25))
  call start_vine_attack_minigame(difficulty="easy", headstart=["vine_one","vine_two","vine_three","vine_four"], skip_instructions=False)
  show flora vine_fight_face2
  window auto
  "Ha, take that!"
  "..."
  show flora vine_fight_face3_vine3_vine4 with dissolve2
  "Oh, fuck. They just keep coming!"
  window hide
  call start_vine_attack_minigame(difficulty="medium", headstart=["vine_three","vine_four"])
  show flora vine_fight_face2
  window auto
  "I think I got them all..."
  "..."
  show flora vine_fight_face1_vine1_vine2_vine4 with dissolve2
  flora vine_fight_face1_vine1_vine2_vine4 "Eeep!"
  "Oh, crap."
  window hide
  call start_vine_attack_minigame(difficulty="hard", headstart=["vine_one","vine_two","vine_four"])
  show flora vine_fight_face1_vine1_vine2_vine3_vine4
  window auto
  "What the hell?!"
  "There's too many!"
  mc "[flora]!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  pause 0.25
  play sound "water_splash2"
  pause 1.0
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  mc "Huh?"
  show flora vine_fight_face1_vine1_vine2_vine4_cuthbert1
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  flora vine_fight_face1_vine1_vine2_vine4_cuthbert1 "[cuthbert]!"
  "What the hell?!"
  "Squid out of nowhere!"
  window hide
  play sound "fast_whoosh"
  show flora vine_fight_face2_vine1_vine2_vine4_cuthbert2 with vpunch
  window auto
  "Huh? [cuthbert] is actually fighting the vines?"
  "What is even real anymore?"
  window hide
  play sound "<to 0.75>nylon_rip"
  show flora vine_fight_face1_braless_vine1_vine2_vine3_vine4_cuthbert2 with vpunch
  window auto
  flora vine_fight_face1_braless_vine1_vine2_vine3_vine4_cuthbert2 "Aaaah! [mc]!"
  "Right, I need to help her!"
  "Although... the vines just took her bra off..."
  "Maybe I'm on the wrong side of this fight?"
  "I mean, can you really fault someone for wanting a handful of those?{space=-20}"
  "Those star-studded milkyway-white squeaky toys..."
  "Mmm..."
  "Shit, I really need to stop reading budget erotica."
  "Evil shall not prevail! [cuthbert] isn't standing alone this day!"
  window hide
  call start_vine_attack_minigame(difficulty="medium_squid", headstart=["vine_one","vine_two","vine_three"])
  show flora vine_fight_face2_braless_vine4_cuthbert2
  window auto
  "Aha! Like cutting broccoli!"
  window hide
  play sound "falling_thud"
  show flora vine_fight_face4_braless_vine4_cuthbert3 with hpunch
  window auto
  "Look at the little guy grappling with the vines..."
  "Poseidon would've been proud."
  show flora vine_fight_face3_braless_vine1_vine4_cuthbert3 with dissolve2
  "..."
  "Maybe I should stop thinking about Greek gods and bring the garden shears to Mr. Ivy..."
  window hide
  call start_vine_attack_minigame(difficulty="hard_squid", headstart="vine_one")
  show flora vine_fight_face4_braless_vine4_cuthbert4
  window auto
  "Snip-snip — vasectomy at its finest!"
  $quest.flora_squid["tree_monster"] = False
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 1.0
  $flora.unequip("flora_bra")
  show flora smile at Transform(xalign=0.25)
  show cuthbert neutral at Transform(xalign=.75)
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  flora smile "Oh, my god! [cuthbert], you saved me!"
  show flora smile at move_to("left")
  show cuthbert neutral at move_to("right")
  menu(side="middle"):
    extend ""
    "\"Where can I find a squid that loyal?\"":
      show flora smile at move_to(.25)
      show cuthbert neutral at move_to(.75)
      mc "Where can I find a squid that loyal?"
      flora smile "You can't! [cuthbert] is one of a kind!"
      flora smile "He's a rescue squid from my internship."
      mc "Well, now I'm jealous..."
      flora afraid "No! Don't say that!"
      mc "Say what? Jealous?"
      mc "Jealous. Jealous. Jealous. Jealous."
    "\"Yeah, just ignore my efforts as usual...\"":
      show flora smile at move_to(.25)
      show cuthbert neutral at move_to(.75)
      mc "Yeah, just ignore my efforts as usual..."
      flora smile "Oh, you did fine too. Just not as fine as [cuthbert]!"
      mc "Jeez. That's what I get for saving your life, huh?"
      flora smile "I'll throw parade in your honor later."
      mc "Great."
      flora afraid "Don't pout! [cuthbert] doesn't like pouters."
      mc "Well, I don't like judgy squids that—"
    "\"Are you okay?\"":
      show flora smile at move_to(.25)
      show cuthbert neutral at move_to(.75)
      mc "Are you okay?"
      flora smile "Yeah, I think so."
      mc "I thought you were about to get dragged off again..."
      $flora.love+=1
      flora smile "Thanks for the help!"
      mc "Your squid doesn't look all too happy."
      flora afraid "He's very protective of me, be careful!"
      mc "He doesn't seem all that danger—"
  window hide
  show cuthbert neutral:
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
    pause 0.1
  pause 1.0
  show cuthbert neutral:
    yoffset 0
  play sound "ink_splorts"
  show ink_splorts with vpunch:
    zoom 0.0 alpha 0.0 xpos 1275 ypos 984
    easein 0.175 zoom 1.0 alpha 1.0 xpos 0 ypos 0
  pause 0.25
  window auto
  mc "Aaah! My eyes!!"
  flora eyeroll "Well, that was your own fault."
  mc "[flora], help me! I can't see!"
  $flora.lust+=1
  flora sarcastic "Well, well, well... It seems like the roles have changed."
  mc "This is not funny! Help me, please!"
  flora annoyed "Fine. Take my hand and I'll lead you home."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.advance()
  stop music
  pause 3.0
  show black onlayer screens zorder 4
  jump goto_home_bathroom

label quest_flora_squid_shower:
  pause 0.25
  window auto
  $set_dialog_mode("default_no_bg")
  flora "Watch your step — here come the stairs."
  mc "Thanks for not leaving me in the forest..."
  flora "Oh, please. I would never do something like that!"
  mc "Well, I remember this one time when I climbed onto the roof and you removed the ladder."
  flora "I... have no memory of that."
  mc "Sure you don't."
  flora "Fine, I remember it. I was annoyed about... something."
  mc "I guess the roles have reversed after all..."
  flora "Don't even start with me!"
  flora "..."
  flora "..."
  flora "Here we are."
  mc "I still can't see anything..."
  mc "Can you turn on the shower for me?"
  flora "What? Are you not going to take off your clothes first?"
  mc "Can you leave the room, then?"
  flora "Of course!"
  mc "..."
  mc "You're totally there still, aren't you?"
  flora "Maybe..."
  flora "I think it's only fair since the chili incident."
  mc "Well, your funeral."
  pause 0.25
  play sound "<from 0.2>bedsheet"
  pause 0.5
  flora "Eeep!"
  mc "What's wrong, [flora]? Never seen a dick before?"
  flora "Not covered in ink! I thought you had some kind of disease..."
  mc "Ugh... just turn on the shower, please..."
  flora "Sure thing."
  play ambient "audio/sound/shower_faucet.ogg"  volume 0.5 noloop
  queue ambient "audio/sound/shower_loop.ogg" volume 0.5
  "..."
  "Ah! The flow of water finally washing away the smell of the river!"
  "Hopefully, it'll get rid of the ink too—"
  mc "Hey! Stop pushing me."
  flora "Well, you're in the way..."
  "She slides up in front of me, the warm skin of her back pressing against my stomach."
# "I'm showering with my sister! I can't see anything, but still..." ## Incest patch ##
# "This is all sorts of wrong." ## Incest patch ##
  mc "Couldn't you have waited your turn?"
  flora "Nope!"
  mc "..."
  menu(side="middle"):
    extend ""
    "?flora.love>=15@[flora.love]/15|{image=flora contact_icon}|{image=stats love_3}|Pull her into a hug":
      $quest.flora_squid["shower"] = "hug"
      flora "Oh!" with vpunch
      "For the first time in my life, it feels right."
      "[flora] squirms weakly in my arms, her wet skin sliding beneath my touch."
      "And then she stops moving."
      "It's like we both know this is how it's supposed to be."
#     "Even though it's wrong in the eyes of society, it's right for us." ## Incest patch ##
      flora "Thanks for saving my squid..."
      mc "Anytime."
      "She turns in my arms to face me."
      "Her wet fingers wipe away the ink from my face, from my eyes."
      window hide None
      show flora shower_hug
      hide black onlayer screens
      with Dissolve(1.0)
      window auto
      $set_dialog_mode("")
      "I never thought we'd end up in a situation like this..."
      "Her perky breasts tight against my chest."
      "[flora] is usually a total brat, but there is a soft feminine side to her as well."
      "A caring side."
      mc "I'd like to know you."
      flora shower_hug "What do you mean?"
      mc "The other side of you."
      flora shower_hug "And what side is that?"
      mc "This side... Your gentle side."
      flora shower_hug "Maybe if you treat me well, I'll let you..."
      mc "I'll do my best."
      flora shower_hug "Try it now."
      mc "Okay..."
      window hide
      show flora shower_kiss with Dissolve(1.0)
      window auto
      "Our lips meet for the first time."
      "The kiss is much wetter than in my fantasies."
      "She has a faint sweet taste, nothing more."
      "Not at all a venomous acid taste like her words would sometimes suggest."
      "..."
#     "I never thought I'd know the taste of my sister's lips..." ## Incest patch ##
      "I do like her bratty side in moderation, but this is all new to me."
      "At first, it's like a thought in the back of my mind — a craving of sorts.{space=-50}"
      "Then it grows..."
      "I need to have her."
      "It would be a lie to say it's not a carnal desire, but there's definitely something more..."
      "Something that has been growing in my chest for years."
      "A yearning of the heart."
#     "Most people would say they love their sister, but for me it's a different kind of love." ## Incest patch ##
      "You never really understand how much you love someone until you're close to losing them."
      "Or in my case, until you push them away, and they stop talking to you...{space=-65}"
      "And then you spend the rest of your life thinking what an idiot and coward you were."
      "To never hold her in my arms, her tender form, skin-to-skin... so much regret."
      flora shower_hug "Are you crying?"
      mc "No... it's just the ink..."
      "She doesn't know how it felt to see her in the arms of another man."
      "The pictures of her in a blazing white wedding dress..."
      "God. It hurts to even think about it."
      "..."
      "Don't be a fucking coward, man. Not again."
      "But maybe she doesn't want this? Maybe she just likes the intimacy?{space=-20}"
      "Fuck."
      mc "[flora]..."
      flora shower_hug "What?"
      mc "You need to tell me to stop."
      mc "I'm serious. This is wrong."
      flora shower_hug "Stop what?"
      mc "What I'm about to do..."
      window hide
      play sound "fast_whoosh"
      show flora shower_sex_anticipation with Dissolve(.5): ## This gives the image both the dissolve (transition) and vpunch (transform) effects
        block:
          linear 0.05 xoffset 15
          linear 0.05 xoffset -15
          repeat 3
        linear 0.05 xoffset 0
      hide flora
      show flora shower_sex_anticipation ## This is just to avoid the screen shake if the player is skipping
      window auto
      flora shower_sex_anticipation "Oh!"
      flora shower_sex_anticipation "We can't do this, you know..."
      mc "I know. Tell me to stop."
      flora shower_sex_anticipation "..."
      mc "[flora], please..."
      flora shower_sex_anticipation "Nope!"
      mc "Oh, god."
      "Instead of saying stop, she takes my hand and leans against the wall for support."
      "And it feels right, even though it's not."
#     "Against our better judgment, and the condemnation of society." ## Incest patch ##
      window hide
      show flora shower_sex_penetration1 with Dissolve(.5)
      window auto
      "Our palms meet and our fingers intertwine."
      "We're going to hell together."
      flora shower_sex_penetration1 "Oww, be careful!"
      mc "Sorry!"
      show flora shower_sex_penetration2 with dissolve2
      "[flora] cries out as I part her nether lips and push inside her."
      "She's so soft, so sensitive..."
      "Not at all the tough exterior she puts on."
      "I really do need to be gentle with her."
      show flora shower_sex_penetration3 with dissolve2
      "Her pussy finally relaxes a bit, allowing me to push deeper."
      "[flora] seems nervous, but no longer in pain."
      "Maybe she's regretting it already..."
      mc "Are you okay?"
      flora shower_sex_penetration3 "Y-yeah..."
      mc "You can still tell me to stop..."
      flora shower_sex_penetration3 "..."
      "She says nothing."
      "And maybe she's waiting for me to back out... but I've been waiting years for this."
      "I need to make her mine, even if it's just once."
      window hide
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.15)
      window auto
      "Her thighs and pussy squeeze me hard."
      "Goosebumps explode over my arms and back."
      "The heat of her pussy is like nothing I've ever felt before."
#     "It's a perfect fit — two organs from the same mold." ## Incest patch ##
      window hide
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration3 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.5)
      window auto
      flora shower_sex_penetration2 "Oh, my god!"
      mc "Shhh! [jo] will hear you!"
      show flora shower_sex_penetration6 with dissolve2
      flora shower_sex_penetration6 "Sorry..."
      window hide
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      window auto
      "God, she feels so good!"
#     "My sister has the tightest pussy in all of Newfall." ## Incest patch ##
#     "That's a messed up thing to think... but damn it all..." ## Incest patch ##
      flora shower_sex_penetration7 "Don't stop!"
      mc "I won't... I've waited thirty years for this..."
      show flora shower_sex_penetration3 with dissolve2
      flora shower_sex_penetration3 "W-what?"
      mc "Uh, nothing!"
      window hide
      window hide
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.1)
      show flora shower_sex_penetration4 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      window auto
      "I just need to keep my mouth shut and enjoy the moment..."
      "Enjoy every inch of her pussy around my cock."
      "Enjoy every spasm and moan."
      "Take it all in."
      "Take it all."
      "Take it!"
      window hide
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_flora_cum1 with vpunch
      window auto
      flora shower_sex_flora_cum1 "Oh, fuck!"
      show flora shower_sex_flora_cum2 with dissolve2
      "With a gasp, the orgasm rips through her."
      "Every muscle in her body goes rigid, and then starts spasming."
      "The muscles of her pussy expel me as her eyes roll back."
      "And the blush of pleasure on her freckled cheeks sends me over the edge."
      window hide
      show flora shower_sex_mc_cum1 with Dissolve(.5)
      show flora shower_sex_mc_cum2 with vpunch
      show flora shower_sex_mc_aftermath with Dissolve(.5)
      window auto
      "[flora] slumps against the wall, in a post-orgasmic haze."
      "Her eyes still glazed over from pure bliss."
      "..."
      "Fuck. What have we done?"
      window hide
      show black onlayer screens zorder 100 with Dissolve(3.0)
      stop ambient
      pause 1.0
      $game.advance()
      if quest.flora_handcuffs > "package":
        $flora.unequip("flora_skirt")
      else:
        $flora.unequip("flora_pants")
      $flora.unequip("flora_panties")
      show flora blush
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      flora blush "What's wrong?"
#     "I just fucked my own sister, that's what's wrong..." ## Incest patch ##
      mc "Nothing..."
      flora worried "Do you regret it?"
      mc "I don't know..."
      mc "Do you?"
      flora thinking "I don't know either..."
      $flora.love+=10
      flora thinking "It felt right in the moment."
      mc "Yeah..."
      flora worried "We can't tell anyone about this."
      flora worried "I'm serious, [mc]."
      mc "I know. We won't."
      flora thinking "I have to go..."
      mc "Where?"
      flora thinking "To think."
      mc "Okay..."
      window hide
      pause 0.25
      $flora.equip("flora_panties")
      pause 1.0
      $flora.equip("flora_bra")
      pause 0.75
      show flora thinking at disappear_to_left
      window auto
      "How can we live under the same roof after this?"
      "How are we going to stop it from happening again?"
      "Oh, man... Things just got a whole lot more complicated."
      $unlock_replay("flora_shower")
      $quest.flora_squid.finish()
      pause 0.1
    "?flora.lust>=10@[flora.lust]/10|{image=flora contact_icon}|{image=stats lust_3}|Touch her ass":
      $quest.flora_squid["shower"] = "ass"
      flora "Hey!" with vpunch
      mc "Sorry, blind man here."
      flora "..."
      flora "Your hand is still on my butt..."
      mc "So it seems."
      flora "..."
      mc "..."
      mc "You haven't told me to remove it yet..."
      flora "So it seems."
      mc "You're a brat."
      flora "You're an idiot."
      mc "..."
      flora "..."
      mc "You have a nice butt."
      $flora.lust+=5
      flora "Shut up."
      "God, the soft curve of her ass in my hand..."
      "It feels so wrong to touch her like this... but she's okay with it..."
#     "Fuck me. What's wrong with us?" ## Incest patch ##
      flora "Move back."
      flora "And wipe that grin off your face."
      mc "This is Apollo 11; we've lost contact with the moon! I repeat; we've lost contact with the moon!"
      flora "Just be quiet for once in your life..."
      mc "I'm afraid I can't do—"
      mc "[flora]!!!"
      window hide None
      show flora shower_blowjob
      show flora shower_blowjob_blurred as blurred:
        pause 0.25
        easeout 0.75 alpha 0.0
      hide black onlayer screens with open_eyes
      window auto
      pause 0.25
      $set_dialog_mode("")
      "This is one of those feelings where you just get floored in an instant.{space=-20}"
      "[flora]'s lips wrapped around the tip of my dick..."
#     "My very own sister..." ## Incest patch ##
      "Her mischievous eyes looking up at me..."
      "Fuck me, I can't!"
      mc "[flora]! W-what, hnngh.... are you doing?"
      flora shower_blowjob "You had some ink in this hole..."
      flora shower_blowjob "I have to suck it out like venom."
      mc "Oh, fuck!"
      "The suction of her mouth increases."
      "It's like my entire soul and spirit is about to get sucked out of my dick.{space=-50}"
      "Stars dance before my eyes as the wet heat of her mouth envelops me.{space=-65}"
#     "My sister's mouth..." ## Incest patch ##
      "How did we end up here?"
      "Did I corrupt her? Did she corrupt me?"
      "Or was it always a flaw on both our parts?"
      "Fuck. I can't think straight."
      window hide
      show flora shower_blowjob_blurred as blurred:
        alpha 0.0
        easein 0.75 alpha 1.0
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      show flora shower_blowjob_blurred as blurred:
        pause 0.25
        easeout 0.75 alpha 0.0
      hide black onlayer screens with open_eyes
      pause 0.25
      show flora shower_blowjob_blurred as blurred:
        alpha 0.0
        easein 0.75 alpha 1.0
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      show flora shower_blowjob_blurred as blurred:
        pause 0.25
        easeout 0.75 alpha 0.0
      hide black onlayer screens with open_eyes
      window auto
      flora shower_blowjob "Stay still... I need to get all the ink out."
      mc "[flora], you're killing me..."
      window hide
      show flora shower_blowjob_blurred as blurred:
        alpha 0.0
        easein 0.75 alpha 1.0
      show black onlayer screens zorder 100 with close_eyes
      pause 1.5
      play sound "fast_whoosh"
      hide black onlayer screens
      hide blurred
      show flora shower_sex_anticipation
      with hpunch
      window auto
      flora shower_sex_anticipation "Oh!"
      "In a daze of pleasure and guilt, I pull her up and push her against the shower wall."
      "Her mouth has made me hard. This is her fault."
      "Little devilish vixen."
      "She giggles, acting all innocent... as if she doesn't know what she's doing to me."
      "Years and years of teasing has finally unleashed a beast in me."
      flora shower_sex_anticipation "What are you going to do with that?"
      "Only a grunt of frustration passes over my lips."
      "This is so wrong, but I can't help myself."
      mc "You little succubus..."
      window hide
      show flora shower_sex_penetration1 with vpunch
      window auto
      flora shower_sex_penetration1 "Owie!"
      "In a flash, my fingers catch her nipple."
      "And just like that, my cock split her open."
      show flora shower_sex_penetration6 with dissolve2
      mc "Isn't this what you wanted?"
      "The heat of her vagina almost knocks me out; only sheer will keeps me standing."
      "She doesn't answer, but her hand slips between her legs, her fingers teasing her clitoris."
      "What a fucked up pair we are..."
#     "Brother and sister." ## Incest patch ##
      "To do this in the shower, with only thin walls separating us from [jo]."
      "What would she think if she saw us now?"
#     "Would she sneer in disgust or just want us to be happy?" ## Incest patch ##
      show flora shower_sex_penetration3 with dissolve2
      "Guilt flickers through me, and I can see it in [flora]'s face — she's feeling it too."
      "The sheer wrongness of what's happening."
      "Still I push myself deeper inside her tight pussy."
      "Still she keeps rubbing herself."
      show flora shower_sex_penetration8 with dissolve2
      "She gasps as I bottom out inside her."
      "My fingers keep playing with her nipple."
      "It's so soft, and somehow still firm."
      "Deliciously erotic to the touch."
      window hide
      show flora shower_sex_penetration6 with Dissolve(.25)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.15)
      pause 0.5
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.15)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.5
      show flora shower_sex_penetration2 with Dissolve(.15)
      show flora shower_sex_penetration1 with Dissolve(.1)
      show flora shower_sex_penetration3 with hpunch
      pause 0.15
      show flora shower_sex_penetration2 with Dissolve(.5)
      window auto
      mc "God, you're hot!"
      flora shower_sex_penetration2 "Ohhh!"
      "Her moans are loud enough to cut through the rushing water, but I don't care."
      "I want [flora] to cry out in pleasure."
      window hide
      show flora shower_sex_penetration3 with Dissolve(.1)
      show flora shower_sex_penetration4 with Dissolve(.25)
      show flora shower_sex_penetration3 with Dissolve(.1)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.25
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.25)
      pause 0.1
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      window auto
      "This might be a once in a lifetime thing."
#     "Fucking my sister without any inhibition..." ## Incest patch ##
      "Maybe we'll both pay for it later, but this moment belongs to us alone.{space=-40}"
      "And I'm going to take it all in."
      "Take it all."
      "Take it!"
      window hide
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration8 with Dissolve(.25)
      show flora shower_sex_penetration7 with Dissolve(.1)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.25)
      pause 0.15
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration5 with Dissolve(.15)
      show flora shower_sex_penetration6 with Dissolve(.1)
      show flora shower_sex_penetration7 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration3 with Dissolve(.15)
      show flora shower_sex_penetration2 with Dissolve(.1)
      show flora shower_sex_penetration1 with Dissolve(.15)
      show flora shower_sex_flora_cum1 with vpunch
      window auto
      flora shower_sex_flora_cum1 "Oh, my god!"
      show flora shower_sex_flora_cum2 with dissolve2
      "A shiver runs through her body as she goes rigid."
      "Her pussy tightens around my dick, squeezing it with every wave of her orgasm."
      "She's gripping my dick so tightly, I almost can't pull it out..."
      "But I can't cum inside her!"
      window hide
      show flora shower_sex_mc_cum1 with Dissolve(.5)
      show flora shower_sex_mc_cum2 with vpunch
      show flora shower_sex_mc_aftermath with Dissolve(.5)
      window auto
      "With a plop, my dick dislodges and sprays my load all over her."
      "That was way too fucking close..."
      "Exhausted, [flora] slumps against the wall, her brain seemingly gone with her orgasm."
      "..."
      "Fuck. What have we done?"
      window hide
      show black onlayer screens zorder 100 with Dissolve(3.0)
      stop ambient
      pause 0.25
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "I gather up her limp form in my arms and wrap her in a towel."
      "Whatever the fuck just happened can never be spoken about again."
      "I lost control for a moment, and now..."
      "Well, if [flora] decides this was a mistake, maybe I'll lose her again."
      "But at least I'll have the memory of feeling her tight pussy wrapped around my dick..."
      "It's a better outcome than last time... I hope."
      window hide None
      $game.advance()
      $game.location = "home_hall"
      $unlock_replay("flora_shower")
      $quest.flora_squid.finish()
      hide black onlayer screens
      hide flora
      with Dissolve(.5)
      pause 0.1
      window auto
      $set_dialog_mode("")
    "Do nothing":
      "I'm not sure what to do in a situation like this..."
      "Maybe it's just innocent?"
      "Two people showering together. Nothing strange about that."
      "Just saving a squid..."
      "Saving water as well."
      $mc.love+=10
      "Yeah, this is good. This is safe."
      window hide None
      stop ambient
      $game.advance()
      $game.location = "home_hall"
      $quest.flora_squid.finish()
      hide black onlayer screens with Dissolve(.5)
      pause 0.1
      window auto
      $set_dialog_mode("")
  return
