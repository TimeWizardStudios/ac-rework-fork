init python:
  class Character_spinach(BaseChar):
    notify_level_changed=True

    default_name="Spinach"

    default_stats=[
      ["lust",0,0],
      ["love",0,0],
      ]

    #contact_icon="spinach contact_icon"

    def ai(self):

      ###Forced Locations
      if quest.jo_washed.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if game.season == 1 or quest.lindsey_voluntary == "dream":
        if quest.isabelle_locker == "scent" and quest.isabelle_locker["spinach"]:
          self.location = quest.isabelle_locker["spinach"].replace("2","").replace("3","")
          self.activity = "running"
          return
        elif quest.isabelle_locker >= "bedroom" and not quest.isabelle_locker == "repeat":
          if quest.isabelle_hurricane["darkness"]:
            pass
          elif quest.isabelle_hurricane.in_progress and quest.isabelle_hurricane["backpack"]:
            self.location = None
            self.activity = None
            return
          else:
            self.location = "home_bedroom"
            self.activity = "licking"
            return

      if quest.fall_in_newfall.in_progress:
        self.location = None
        self.activity = None
        self.alternative_locations = {}
        return

      if quest.jacklyn_romance == "hide_and_seek" and not school_clubroom["ball_of_yarn_taken"]:
        self.location = "school_clubroom"
        self.activity = "knitted"
        return
      elif quest.jacklyn_romance == "hide_and_seek" and game.hour in (15,16,17):
        self.location = "school_clubroom"
        self.activity = "licking"
        return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        self.alternative_locations = {}
        return
      ###Forced Locations

      ###ALT Locations
      if not quest.spinach_seek.finished and not spinach["playing_today"]:
        if game.season == 1:
          self.alternative_locations["school_clubroom"] = "hiding"
        else:
          self.alternative_locations["school_clubroom"] = ""
      if spinach["playing_today"]:
        self.alternative_locations["school_clubroom"] = "playing"
      if maxine.at("school_cafeteria","sitting") and not (quest.kate_search_for_nurse["catch_spinach"] == "school_ground_floor" or quest.kate_search_for_nurse == "follow_spinach"):
        self.alternative_locations["school_cafeteria"] = "sitting"
      else:
        self.alternative_locations["school_cafeteria"] = ""
      if quest.kate_search_for_nurse == "give_wrapper":
        if quest.kate_search_for_nurse["spinach_first_int"]:
          if quest.kate_search_for_nurse["catch_spinach"] == "school_first_hall":
            self.alternative_locations["school_first_hall"] = "running"
          elif quest.kate_search_for_nurse["catch_spinach"] == "school_ground_floor":
            self.alternative_locations["school_ground_floor"] = "running"
          elif quest.kate_search_for_nurse["catch_spinach"] == "school_cafeteria":
            self.alternative_locations["school_cafeteria"] = "sitting"
          else:
            self.alternative_locations["school_first_hall_west"] = "running"
        else:
          self.alternative_locations["school_art_class"] = "standing"
      elif quest.kate_search_for_nurse in ("follow_spinach","find_the_nurse"):
        self.alternative_locations["school_entrance"] = "standing"
      ###ALT Locations

      ###Schedule
      if game.dow in (0,1,2,3,4,5,6,7):
        if game.hour in (9,10,11):
          self.location = "school_clubroom"
          self.activity = "licking"
          return
        elif game.hour in (12,13,14):
          self.location = "school_clubroom"
          self.activity = "hiding"
          return
        elif game.hour in (15,16,17):
          self.location = "school_clubroom"
          self.activity = "playing"
          return
        elif game.hour in (7,8,18):
          self.location = "school_clubroom"
          self.activity = "sleeping"
          return
      self.location=None
      self.activity=None
      self.alternative_locations={}
      return
      ###Schedule

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("spinach avatar "+state,True):
          return "spinach avatar "+state
      rv=[]
      if state=="closedeyes":
        rv.append((230,400))
        rv.append(("spinach avatar body",2,2))
        rv.append(("spinach avatar exp_closedeyes",41,60))
      elif state=="drugged":
        rv.append((230,400))
        rv.append(("spinach avatar body",2,2))
        rv.append(("spinach avatar exp_drugged",40,62))
      elif state=="hearteyed":
        rv.append((230,400))
        rv.append(("spinach avatar body",2,2))
        rv.append(("spinach avatar exp_hearteyed",40,60))
      elif state=="neutral":
        rv.append((230,400))
        rv.append(("spinach avatar body",2,2))
        rv.append(("spinach avatar exp_neutral",40,60))
      elif state=="tongueout":
        rv.append((230,400))
        rv.append(("spinach avatar body",2,2))
        rv.append(("spinach avatar exp_tongueout",41,60))
      return rv


  class Interactable_spinach(Interactable):
    def title(cls):
      return spinach.name

    def description(cls):
      if quest.lindsey_voluntary == "dream":
        return random.choice(q_kate_blowjob_dream_investigate_lines)
      elif game.season == 1 and quest.isabelle_locker >= "bedroom" and not quest.isabelle_locker == "repeat" and not quest.isabelle_hurricane["darkness"]:
        return "[maxine]'s beloved pet. Now my beloved pet. That's just how the ball of yarn rolls."
      elif quest.maxine_eggs.ended:
        return "[spinach] is usually a sweetheart, but for some reason, she always hisses at [mrsl]. Maybe it's her perfume?"
      elif quest.maxine_eggs >= "recon":
        return "In our freshman year, [maxine] found [spinach] in a shoebox outside the school.\n\nThe cat hasn't left her side since."
      else:
        if mc.at("school_clubroom"):
          return "Pets aren't allowed in the school, but [spinach] is somehow the exception.\n\nShe's very well-behaved, though, and never leaves this room."
        else:
          return "[maxine]'s beloved pet. She usually never leaves [maxine]'s office on the top floor, but today she's out on an adventure."

    def actions(cls,actions):
      #interact
      if quest.jacklyn_romance == "hide_and_seek" and not school_clubroom["ball_of_yarn_taken"]:
        actions.append("quest_jacklyn_romance_hide_and_seek_school_clubroom_spinach")
        return

      #talk
      if not mc.at("home_*"):
        if mc.at("school_clubroom"):
          if spinach.at("school_clubroom","playing") and (quest.spinach_seek.finished or game.season > 1):
            actions.append(["talk","Talk","?spinach_talk_play"])
          elif spinach.at("school_clubroom","hiding") and (quest.spinach_seek.finished or game.season > 1):
            actions.append(["talk","Talk","?spinach_talk_hide"])
          elif spinach.at("school_clubroom","sleeping") and (quest.spinach_seek.finished or game.season > 1):
            actions.append(["talk","Talk","?spinach_talk_sleep"])
          elif spinach.at("school_clubroom","licking") and (quest.spinach_seek.finished or game.season > 1):
            actions.append(["talk","Talk","?spinach_talk_lick"])
        else:
          actions.append(["talk","Talk","?spinach_talk"])

      #quest
      if mc["focus"]:
        if mc["focus"] == "isabelle_locker":
          if quest.isabelle_locker >= "bedroom" and not quest.isabelle_locker == "repeat" and not quest.isabelle_hurricane["darkness"]:
            if maxine.at("home_bedroom"):
              actions.append("?quest_maxine_hook_wake_home_bedroom_spinach")
            else:
              actions.append("?quest_isabelle_locker_home_bedroom_spinach")
        elif mc["focus"] == "lindsey_voluntary":
          if quest.lindsey_voluntary == "dream":
            actions.append("kate_blowjob_dream_random_interact")
      else:
        #kate search for nurse
        if quest.kate_search_for_nurse.in_progress:
          if quest.kate_search_for_nurse == "give_wrapper" and not mc.at("school_clubroom"):
            if not (mc.at("school_cafeteria") and maxine.at("school_cafeteria","sitting") and quest.kate_search_for_nurse["catch_spinach"] != "school_cafeteria"):
              if not spinach.at("school_art_class","standing"):
                actions.append(["give","Give","select_inventory_item","$quest_item_filter:kate_search_for_nurse,give_wrapper|Give What?","spinach_quest_kate_search_for_nurse_give_wrapper"])
              actions.append(["interact","Pet","spinach_quest_kate_search_for_nurse_interact"])

          elif quest.kate_search_for_nurse == "follow_spinach":
            actions.append("spinach_quest_kate_search_for_nurse_interact_outside")
        #spinach seek
        if game.season == 1:
          if not quest.spinach_seek.started and spinach.at("school_clubroom","hiding") and mc.at("school_clubroom"):
            actions.append(["quest","Quest","?spinach_quest_hide_seek_start"])

          if quest.spinach_seek.in_progress and mc.at("school_clubroom"):
            if mc.owned_item("ball_of_yarn"):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:spinach_seek,spinach_yarn|Give What?","spinach_quest_hide_seek_yarn"])
            else:
              actions.append(["quest","Quest","?spinach_quest_hide_seek_start"])

        if quest.isabelle_locker == "scent":
          if quest.isabelle_locker["spinach"]:
            actions.append(["quest","Quest","quest_isabelle_locker_scent"])
          else:
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:isabelle_locker,isabelle_clothing|Give What?","quest_isabelle_locker_scent_spinach"])

        if quest.isabelle_hurricane == "short_circuit":
          actions.append(["take","Take","quest_isabelle_hurricane_short_circuit_spinach1"])

      #flirt
      if not mc.at("home_*"):
        if spinach.at("school_clubroom","playing") and quest.spinach_seek.finished or not spinach.at("school_clubroom","hiding") and not spinach.at("school_clubroom","sleeping") or not mc.at("school_clubroom"):
          actions.append(["flirt","Flirt","?spinach_flirt"])


label spinach_talk_hide:
  spinach "..."
  mc "What are you hiding from, girl?"
  "Let's be honest, it's probably me."
  return

label spinach_talk_sleep:
  spinach "ZzzZzzzzZz..."
  "[maxine] gets really upset when you wake her cat up."
  "No reason to antagonize either of them."
  return

label spinach_talk_lick:
  show spinach neutral with Dissolve(.5)
  spinach neutral "Meow?"
  show spinach tongueout with Dissolve(.5)
  "She's watching my every move... [maxine], that is."
  show spinach neutral with Dissolve(.5)
  "[spinach] doesn't mind me staring. That's the great thing about animals."
  hide spinach with Dissolve(.5)
  return

label spinach_talk_play:
  show spinach closedeyes with Dissolve(.5)
  spinach closedeyes "Meow! Meow!"
  show spinach neutral with Dissolve(.5)
  "She's really loving her new toy. Making that little furball happy felt good."
  spinach closedeyes "Meow!"
  "It's hard not to envy her life."
  hide spinach with Dissolve(.5)
  return

label spinach_quest_kate_search_for_nurse_give_wrapper(item):
  if item=="wrapper":
    show spinach neutral with Dissolve(.5)
    spinach neutral "{i}*Sniff, sniff*{/}"
    spinach "Meow!"
    show spinach neutral at disappear_to_right()
    if spinach.at("school_first_hall","running"):
      "She ran down the stairs and then straight outside."
      $spinach.alternative_locations["school_first_hall"] = ""
    elif spinach.at("school_ground_floor","running"):
      "She bolted for the doors and ran straight outside."
      $spinach.alternative_locations["school_ground_floor"] = ""
    else:
      "She ran into the entrance hall and then straight outside."
      $spinach.alternative_locations["school_first_hall_west"] = spinach.alternative_locations["school_cafeteria"] = ""
    $quest.kate_search_for_nurse.advance("follow_spinach")
  elif item=="nurse_panties":
    show spinach neutral with Dissolve(.5)
    spinach neutral "{i}*Sniff, sniff*{/}"
    spinach closedeyes "Meow?"
    spinach hearteyed "{i}*Sniff, sniff*{/}"
    spinach tongueout "{i}*Lick, lick*{/}"
    mc "Oh, no!"
    spinach hearteyed "Meow!"
    mc "Well, I guess I can't blame you for having a taste..."
    $achievement.pussy_scent.unlock()
  else:
    "Giving a pussy my [item.title_lower] is an exciting fantasy, but kinda sucky in practice."
    $quest.kate_search_for_nurse.failed_item("give_wrapper",item)
  return

label spinach_quest_kate_search_for_nurse_interact:
  show spinach neutral with Dissolve(.5)
  if not quest.kate_search_for_nurse["spinach_first_int"]:
    $quest.kate_search_for_nurse["spinach_first_int"]=True
    "[spinach] is almost as smart as [maxine] herself. And from the looks of it, the [nurse] put her in the supply closet as a decoy."
    "Perhaps this little furball will lead me to the [nurse]'s real hiding spot. She's like a bloodhound once she gets a whiff of her prey."
    $spinach.alternative_locations["school_art_class"] = "running"
    hide spinach with Dissolve(.5)
    $spinach.alternative_locations["school_art_class"] = ""
  else:
    if spinach.at("school_first_hall_west","running"):
      mc "Heeere, kitty kitty!"
      show spinach neutral at disappear_to_right()
      "Hmm... she ran down the corridor."
      $quest.kate_search_for_nurse["catch_spinach"] = "school_first_hall"
      $spinach.alternative_locations["school_first_hall_west"] = ""
    elif spinach.at("school_first_hall","running"):
      mc "Come here, [spinach]!"
      show spinach neutral at disappear_to_right()
      "She ran downstairs."
      $quest.kate_search_for_nurse["spinach_first_repeat"] = False
      $quest.kate_search_for_nurse["catch_spinach"] = "school_ground_floor"
      $spinach.alternative_locations["school_first_hall"] = ""
    elif spinach.at("school_ground_floor","running"):
      if not quest.kate_search_for_nurse["spinach_first_repeat"]:
        "[spinach] seems a bit lost, but perhaps it's just the smell from the kitchen."
        show spinach neutral at disappear_to_right()
        "She escaped into the cafeteria."
        $quest.kate_search_for_nurse["catch_spinach"] = "school_cafeteria"
        $spinach.alternative_locations["school_ground_floor"] = ""
      else:
        "Well, we're back in the entrance hall, and [spinach] seems lost again."
        show spinach neutral at disappear_to_right()
        "Ugh, she ran back upstairs again."
        $quest.kate_search_for_nurse["catch_spinach"] = "school_first_hall"
        $spinach.alternative_locations["school_ground_floor"] = ""
    elif spinach.at("school_cafeteria","sitting"):
      "[spinach] seems to love it here! That cat is always hungry."
      show spinach neutral at disappear_to_right()
      "I guess not hungry enough... she ran out of the cafeteria."
      $quest.kate_search_for_nurse["spinach_first_repeat"] = True
      $quest.kate_search_for_nurse["catch_spinach"] = "school_ground_floor"
      $spinach.alternative_locations["school_cafeteria"] = ""
    $process_event("update_state")
  return

label spinach_quest_kate_search_for_nurse_interact_outside:
  show spinach neutral with Dissolve(.5)
  spinach neutral "Meww!"
  "The cat is circling the bush here. She's like a bloodhound."
  $quest.kate_search_for_nurse.advance("find_the_nurse")
  show spinach neutral at disappear_to_left()
  pause(.75)
  return

label spinach_talk:
  show spinach closedeyes with Dissolve(.5)
  spinach "Meow?"
  show spinach closedeyes at disappear_to_right()
  pause(.75)
  return

label spinach_flirt:
  show spinach drugged with Dissolve(.5)
  spinach "Meow!"
  show spinach drugged at disappear_to_left()
  pause(.75)
  return
