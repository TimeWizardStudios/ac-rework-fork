init python:
  class Character_guard(BaseChar):
    notify_level_changed=True

    default_name="Guard"

    default_stats=[
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon="guard contact_icon"

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

      if quest.jo_washed.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.isabelle_stolen == "outnuts" and not self["hidden_now"]:
        self.location = "school_cafeteria"
        self.activity = "standing"
        return

      if quest.lindsey_angel in ("off_limits","phone_book","lab_coat") and quest.lindsey_angel["troll_outside_its_cave"]:
        self.location = "school_ground_floor"
        self.activity = "standing"
        return

      if (((quest.maya_spell == "maya" and game.hour in (11,12,13,14,15))
      or guard["at_the_cafeteria_this_scene"])
      and not quest.maya_spell["taking_care_of_it"]):
        self.location = "school_cafeteria"
        self.activity = "standing"
        return

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("guard avatar "+state,True):
          return "guard avatar "+state
      rv=[]

      if state.startswith("angry"):
        rv.append((700,1080))
        rv.append(("guard avatar body1",66,44))
        rv.append(("guard avatar face_angry",229,210))
        if "_flashlight" in state:
          rv.append(("guard avatar b1arm1_flashlight",371,39))
        elif "_phone" in state:
          rv.append(("guard avatar b1arm1_phone",206,388))
        else:
          rv.append(("guard avatar b1arm1",438,389))

      elif state.startswith("neutral"):
        rv.append((700,1080))
        rv.append(("guard avatar body1",66,44))
        if "_phone" in state:
          rv.append(("guard avatar face_neutral_phone",228,208))
        else:
          rv.append(("guard avatar face_neutral",228,208))
        if "_flashlight" in state:
          rv.append(("guard avatar b1arm1_flashlight",371,39))
        elif "_phone" in state:
          rv.append(("guard avatar b1arm1_phone",206,388))
        else:
          rv.append(("guard avatar b1arm1",438,389))

      elif state.startswith("smile"):
        rv.append((700,1080))
        rv.append(("guard avatar body1",66,44))
        rv.append(("guard avatar face_smile",228,208))
        if "_flashlight" in state:
          rv.append(("guard avatar b1arm1_flashlight",371,39))
        else:
          rv.append(("guard avatar b1arm1",438,389))

      elif state.startswith("surprised"):
        rv.append((700,1080))
        rv.append(("guard avatar body1",66,44))
        rv.append(("guard avatar face_surprised",228,205))
        if "_flashlight" in state:
          rv.append(("guard avatar b1arm1_flashlight",371,39))
        elif "_phone" in state:
          rv.append(("guard avatar b1arm1_phone",206,388))
        else:
          rv.append(("guard avatar b1arm1",438,389))

      elif state.startswith("suspicious"):
        rv.append((700,1080))
        rv.append(("guard avatar body1",66,44))
        rv.append(("guard avatar face_suspicious",229,207))
        if "_flashlight" in state:
          rv.append(("guard avatar b1arm1_flashlight",371,39))
        elif "_phone" in state:
          rv.append(("guard avatar b1arm1_phone",206,388))
        else:
          rv.append(("guard avatar b1arm1",438,389))

      elif state.startswith("tcase_perspective"):
        rv.append((1920,1080))
        if state.endswith("_day"):
          rv.append(("guard avatar events tcase_perspective bgday",0,0))
        elif state.endswith("_night"):
          rv.append(("guard avatar events tcase_perspective bgnight",0,0))
          if "_guard" in state:
            rv.append(("guard avatar events tcase_perspective guard",855,297))
            rv.append(("guard avatar events tcase_perspective light",297,177))
            rv.append(("guard avatar events tcase_perspective glow",421,123))

      elif state.startswith("grill_perspective"):
        rv.append((1920,1080))
        if state.endswith("_day"):
          rv.append(("guard avatar events grill_perspective bgday",0,0))
          rv.append(("guard avatar events grill_perspective grillday",0,0))
        elif state.endswith("_night"):
          rv.append(("guard avatar events grill_perspective bgnight",0,0))
          if "_guard" in state:
            rv.append(("guard avatar events grill_perspective guard",844,421))
            rv.append(("guard avatar events grill_perspective light",1040,312))
          rv.append(("guard avatar events grill_perspective grillnight",0,0))

      return rv


  class Interactable_guard(Interactable):
    def title(cls):
      return guard.name

    #def actions(cls,actions):
    #  pass
      #actions
