init python:
  class Character_crab(BaseChar):
    notify_level_changed = True

    default_name = "Dangerous Crab"

    default_stats = [
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon = "crab contact_icon"


init python:
  class Interactable_crab(Interactable):

    def title(cls):
      return crab.name

    def description(cls):
      return "..."

    def actions(cls,actions):
      if crab.love < 3:
        actions.append(["give","Give","select_inventory_item","$quest_item_filter:act_two,dangerous_crab|Give What?","marina_dangerous_crab_give"])
      actions.append("marina_dangerous_crab_interact")


label marina_dangerous_crab_interact:
  menu(side="middle"):
    "Fight":
      window hide
      hide screen interface_hider
      show black
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.25
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      if crab.love < 3:
        mc "N-no... no..."
        mc "Nooooooooooooo!"
        play sound "falling_thud"
        show black onlayer screens zorder 100 with vpunch
        if game.hour == 17:
          $game.advance()
        elif game.hour < 17:
          $game.advance(hours=2)
        $game.location = "school_park"
        $mc["money_stamp"] = mc.money
        $game.disable_notifications = True
        $mc.money-=(50 if mc.money >= 50 else mc.money)
        $game.disable_notifications = False
        pause 3.0
        hide black
        hide black onlayer screens
        show screen interface_hider
        with Dissolve(.5)
        if "event_show_time_passed_screen" in game.events_queue:
          $game.events_queue.remove("event_show_time_passed_screen")
        call screen time_passed
        pause 0.5
        window auto
        $set_dialog_mode("")
        "F-fuck... my ankle feels like it got stabbed a million tiny times..."
        "..."
        "How long was I out for?"
        if mc["money_stamp"]:
          $game.notify("ui icon_money","Lost\n${color=#900}"+str(mc["money_stamp"]-mc.money)+"{/color}",5.0)
          "And where's my wallet?"
      else:
        "We circle each other, like two boxers in the ring."
        "The steel of his blade flickers, but his eyes shine brighter still."
        "Adoration, perhaps? Or maybe just a mutual respect?"
        "They say that love is war. And this is the biggest of them all."
        "We gaze into each other's eyes, and then lower our weapons in understanding."
        "A truce this beautiful is seldom seen on the battlefield."
        show black onlayer screens zorder 100
        pause 0.5
        hide black
        hide black onlayer screens
        show screen interface_hider
        with Dissolve(.5)
        if not achievement.the_truce_of_a_lifetime.is_unlocked():
          $achievement.the_truce_of_a_lifetime.unlock()
          pause 0.25
        show flora excited at appear_from_left
        pause 0.5
        window auto
        $set_dialog_mode("")
        flora excited "Oh, my god! He's adorable!"
        mc "It's my new friend."
        flora excited "I love him! Can we take him home?"
        mc "No, he needs to be free. Captivity isn't for someone like him."
        flora blush "You're right. He's magnificent."
        if not marina["dangerous_crab_befriended"]:
          $marina["dangerous_crab_befriended"] = True
          $flora.love+=5
        flora blush "I'm proud of you for realizing it."
        window hide
        show flora blush at disappear_to_left
        pause 1.0
    "Flight":
      pass
  return

label marina_dangerous_crab_give(item):
  if item == "apple":
    if crab.owned_item("apple"):
      "Feeding it more apples would only make it stronger."
      $quest.act_two.failed_item("dangerous_crab",item)
    else:
      "You look hungry. Maybe you'll enjoy this?"
      window hide
      $crab.love+=1
      $mc.remove_item("apple")
      $crab.add_item("apple")
  elif item == "fish":
    if crab.owned_item("fish"):
      "Feeding it more fish would only make it stronger."
      $quest.act_two.failed_item("dangerous_crab",item)
    else:
      "Here you go, little buddy."
      window hide
      $crab.love+=1
      $mc.remove_item(item)
      $crab.add_item(item)
  elif item in ("strawberry_juice","spray_strawberry_juice"):
    if crab.owned_item(("strawberry_juice","spray_strawberry_juice")):
      "Feeding it more strawberry juice would only make it stronger."
      $quest.act_two.failed_item("dangerous_crab",item)
    else:
      "Here's something to drink, little one."
      window hide
      $crab.love+=1
      $mc.remove_item(item)
      $crab.add_item(item)
      $return_item = item.id.replace("strawberry_juice","empty_bottle")
      $mc.add_item(return_item)
  else:
    "Feeding it my [item.title_lower] would only make it stronger."
    $quest.act_two.failed_item("dangerous_crab",item)
  return
