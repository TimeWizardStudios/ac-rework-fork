init python:
  class Character_lacey(BaseChar):

    notify_level_changed=True

    default_name="Lacey"

    default_stats=[
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon="lacey contact_icon"

    default_outfit_slots=["top","bra","skirt","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("lacey_cheerleader_top")
      self.add_item("lacey_cheerleader_bra")
      self.add_item("lacey_cheerleader_skirt")
      self.add_item("lacey_cheerleader_panties")
      self.equip("lacey_cheerleader_top")
      self.equip("lacey_cheerleader_bra")
      self.equip("lacey_cheerleader_skirt")
      self.equip("lacey_cheerleader_panties")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("lacey avatar "+state,True):
          return "lacey avatar "+state
      rv=[]

      if state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((680,1080))
        rv.append(("lacey avatar body1",135,118))
        if "_cheerleader_panties" in state:
          rv.append(("lacey avatar b1panties",146,733))
        if "_cheerleader_skirt" in state:
          rv.append(("lacey avatar b1skirt",114,722))
        if "_cheerleader_bra" in state:
          rv.append(("lacey avatar b1bra",182,384))
        if "_cheerleader_top" in state:
          rv.append(("lacey avatar b1top",182,378))
        if "_shifty" in state:
          rv.append(("lacey avatar face_shifty",267,222))
          rv.append(("lacey avatar b1arm3_n",73,329))
        else:
          rv.append(("lacey avatar face_neutral",268,222))
          rv.append(("lacey avatar b1arm1_n",63,383))
        if "_cheerleader_top" in state:
          if "_shifty" in state:
            rv.append(("lacey avatar b1arm3_c",71,380))
          else:
            rv.append(("lacey avatar b1arm1_c",62,381))
        if "_paddle" in state:
          rv.append(("lacey avatar b1paddle",281,373))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((680,1080))
        rv.append(("lacey avatar body1",135,118))
        if "_cheerleader_panties" in state:
          rv.append(("lacey avatar b1panties",146,733))
        if "_cheerleader_skirt" in state:
          rv.append(("lacey avatar b1skirt",114,722))
        if "_cheerleader_bra" in state:
          rv.append(("lacey avatar b1bra",182,384))
        if "_cheerleader_top" in state:
          rv.append(("lacey avatar b1top",182,378))
        rv.append(("lacey avatar face_confident",268,229))
        if "_mocking" in state:
          rv.append(("lacey avatar b1arm2_n",116,306))
          if "_cheerleader_top" in state:
            rv.append(("lacey avatar b1arm2_c",117,326))
        else:
          rv.append(("lacey avatar b1arm1_n",63,383))
          if "_cheerleader_top" in state:
            rv.append(("lacey avatar b1arm1_c",62,381))
        if "_paddle" in state:
          rv.append(("lacey avatar b1paddle",281,373))

      return rv


  class Interactable_lacey(Interactable):

    def title(cls):
      return lacey.name

    def actions(cls,actions):
      pass
