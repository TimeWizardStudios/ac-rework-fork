init python:
  class Character_hati(BaseChar):
    notify_level_changed = True

    default_name = "Hati"

    default_stats = [
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon = "hati contact_icon"

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("hati avatar "+state, True):
          return "hati avatar "+state
      rv=[]

      if state.startswith("neutral"):
        rv.append((748,1080))
        if "_collarless" in state:
          rv.append(("hati avatar body_collarless",0,0))
        else:
          rv.append(("hati avatar body",0,0))

      return rv
