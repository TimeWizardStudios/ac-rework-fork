label quest_maxine_eggs_start:
  maxine "You have to schedule an appointment. I'm a very busy woman."
  mc "Yeah, that's why I'm here."
  maxine "I need to introduce appointments for scheduling an appointment..."
  maxine "That would drastically reduce the time I have to spend scheduling appointments."
  show maxine smile with Dissolve(.5)
  maxine smile "Anyway, how can I help you?"
  show maxine smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I guess I'd like to schedule an appointment.\"":
      show maxine smile at move_to(.5)
      mc "I guess I'd like to schedule an appointment."
      maxine thinking "Hmm... I might have an opening the week before graduation."
      mc "What? That's in almost a year!"
      maxine smile "These are busy times."
      "Fuck... I need that appointment badly."
      "Being a let down this early in the year is not how you capitalize on a second chance."
      show maxine smile at move_to(.25)
      menu(side="right"):
        extend ""
        "\"What if I help you out?\"":
          show maxine smile at move_to(.5)
          mc "What if I help you out?"
          maxine skeptical "What do you have in mind?"
          mc "Maybe I could do some journalist work?"
          jump quest_maxine_eggs_start_journalist
        "\"That's ridiculous!\"":
          jump quest_maxine_eggs_start_ridiculous
    "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"Maybe you should digitalize and automatize your appointments system?\"":
      show maxine smile at move_to(.5)
      mc "Maybe you should digitalize and automatize your appointments system?"
      maxine thinking "Now, why would I do that?"
      mc "So that you can focus on more important matters."
      maxine cringe "If the system is not analog, it can't be trusted!"
      mc "It could be as simple as a voicemail system, accessed by your phone. Doesn't have to be anything fancy."
      maxine thinking "And how would I go about this?"
      mc "I could do it for you."
      maxine skeptical "What's the catch?"
      "For one, I'd put myself at the top of the appointment list, but [maxine] doesn't have to know that."
      "The only problem is that she's really good at sniffing out lies."
      show maxine skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "\"There's no catch. I know how hard you work for the school paper.\"":
          show maxine skeptical at move_to(.5)
          mc "There's no catch. I know how hard you work for the school paper."
          if mc.charisma >= 4:
            maxine excited "That's very thoughtful of you."
            mc "Thoughtful is my middle name."
            maxine smile "While that is certainly a lie, I trust you enough to do this for me."
            maxine neutral "Let me know when you've set it up."
            "With a computer and a flash drive, this shouldn't take too long."
          else:
            $maxine.love-=1
            maxine angry "I can spot your lies from a mile away!"
            maxine cringe "I'm not sure about your motives, but I know a shill when I see one."
            mc "Right, I'm sorry for not being upfront. I'd like to schedule an appointment."
            jump quest_maxine_eggs_start_hanky
        "\"If I do this, I'd like to have an appointment with you at the top of the list.\"":
          show maxine skeptical at move_to(.5)
          mc "If I do this, I'd like to have an appointment with you at the top of the list."
          if mc.love >= 4:
            mc "I genuinely want to help you, but I'm also in need of a quick appointment."
            maxine thinking "I suppose there's always a cost for everything."
            maxine neutral "Since you were honest with me, you've got a deal."
            maxine neutral "Let me know when you've set it up."
            "With a computer and a flash drive, this shouldn't take too long."
          else:
            $maxine.lust-=1
            maxine cringe "You think I don't have any integrity?"
            maxine annoyed "I can't be bought. Forget it."
            mc "Right, I'm sorry for not being upfront. I'd like to schedule an appointment."
            jump quest_maxine_eggs_start_hanky
      hide maxine with Dissolve(.5)
      $school_computer_room["unlocked"] = True
      $quest.maxine_eggs.start()
    "\"Why are you so busy?\"":
      show maxine smile at move_to(.5)
      mc "Why are you so busy?"
      maxine annoyed "One, I'm the president of the Inconspicuous Group."
      maxine annoyed "Two, I aim to be this year's valedictorian."
      maxine annoyed "Three, I'm the editor of the school's newspaper."
      mc "Maybe I could help you out, somehow? To ease your workload."
      maxine skeptical "And what would you get out of that?"
      mc "I know you probably don't have an appointment open until Christmas..."
      maxine smile "Until the week before graduation, in fact."
      mc "Right. So, if I help you out, maybe I could get an early appointment?"
      maxine thinking "Hmm... and what are your qualifications?"
      "Ugh. Totally forgot how difficult she can be."
      mc "I guess I could take some photos for the newspaper?"
      jump quest_maxine_eggs_start_journalist
    "\"I'd like to be a reporter for the school paper.\"":
      show maxine smile at move_to(.5)
      mc "I'd like to be a reporter for the school paper."
      maxine thinking "You'll have to schedule an interview for that."
      mc "Right, okay. I'd like to schedule an interview, then."
      maxine smile "My schedule currently has no room for interviews. Ask me again in a few months."
      "Ugh... why does she have to be so difficult?"
      mc "What about appointments?"
      maxine thinking "You'd have to schedule—"
      mc "I know! I'd like to schedule one!"
      maxine angry "Hush! [spinach] is sleeping."
      if spinach.at("school_clubroom","sleeping"):
        mc "Right, sorry..."
        mc "I'd like to schedule a regular non-interview appointment."
      else:
        mc "Doesn't look like she's asleep to me."
        maxine annoyed "Because you woke her up!"
        mc "I'm pretty sure she wasn't—"
        maxine angry "You're wasting my time and you're upsetting my cat!"
        mc "She doesn't look particularly upset to me."
        maxine thinking "Well, you don't know her like I do."
        "Not sure what she's on about now, but I love cats. Upsetting [spinach] is the last thing I want to do."
        $achievement.here_kitty_kitty.unlock()
        mc "Okay, let's cut to the chase. I'd like to schedule a regular non-interview appointment."
      maxine thinking "Hmm... I might have an opening the week before graduation."
      mc "What? That's in almost a year!"
      maxine smile "These are busy times."
      show maxine smile at move_to(.25)
      menu(side="right"):
        extend ""
        "\"What if I help you out?\"":
          show maxine smile at move_to(.5)
          mc "What if I help you out?"
          maxine skeptical "What do you have in mind?"
          mc "Maybe I could do some journalist work? Like a freelancer."
          maxine thinking "Hmm... a freelancer wouldn't need an interview..."
          jump quest_maxine_eggs_start_journalist
        "\"That's ridiculous!\"":
          jump quest_maxine_eggs_start_ridiculous
  return

label quest_maxine_eggs_start_journalist:
  maxine thinking "How are you with a camera?"
  "If she means looking at photos of scantily clad women, then few people have my experience."
  show maxine thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Photography is one of my biggest interests.\"":
      show maxine thinking at move_to(.5)
      mc "Photography is one of my biggest interests."
      if mc.charisma >= 4:
        maxine smile "All right. That's a relief."
        maxine smile "I need you to get me a picture of the glowing spider eggs."
        maxine smile "That would be one less task to worry about, and it would give me time to book an early appointment."
        jump quest_maxine_eggs_start_spider
      else:
        maxine skeptical "That's a lie if I ever saw one."
        $maxine.love-=1
        maxine angry "I don't appreciate being lied to."
        mc "I'm sorry. I might not be the best photographer, but I'll put in the work."
        maxine annoyed "Fine. It doesn't have to be amazing."
        maxine annoyed "Just get me a shot of the glowing spider eggs."
        jump quest_maxine_eggs_start_spider
    "\"Not particularly good.\"":
      show maxine thinking at move_to(.5)
      mc "Not particularly good."
      if mc.love >= 4:
        $maxine.love+=1
        maxine smile "I appreciate the honesty."
        maxine annoyed "People are always too confident in their own abilities."
        mc "Knowing your limits is a good thing."
        "Especially if you have as many as me..."
        maxine neutral "Okay. I need you to get me a picture of the glowing spider eggs."
        maxine smile "That would be one less task to worry about, and it would give me time to book an early appointment."
        jump quest_maxine_eggs_start_spider
      else:
        maxine neutral "Hmm... okay... let's see..."
        maxine neutral "It's hard to employ someone without talent."
        maxine thinking "But this is a tough economy for everyone..."
        "Ouch!"
        maxine thinking "Get me a picture of the glowing spider eggs and we'll call it even."
        jump quest_maxine_eggs_start_spider

label quest_maxine_eggs_start_ridiculous:
  show maxine smile at move_to(.5)
  mc "That's ridiculous!"
  $maxine.love-=1
  maxine annoyed "Sorry, but that's my earliest spot."
  "[maxine] is clearly taking some kind of twisted pleasure in this..."
  "At least, that's what the paranoid part of my brain thinks. Maybe she's just busy..."
  mc "There has to be something I can do to get it sooner?"
  mc "Please, I really need this."
  maxine thinking "My work is piling up..."
  maxine thinking "If you helped me out, maybe a spot would open up?"
  mc "How can I help you, then?"
  maxine smile "Getting me a picture of the glowing spider eggs would be a good start..."
  jump quest_maxine_eggs_start_spider

label quest_maxine_eggs_start_hanky:
  maxine skeptical "No more hanky-panky?"
  mc "I promise."
  maxine thinking "Hmm... okay...  let's see..."
  maxine neutral "I've got an open spot in five months."
  mc "That's not going to cut it. I need it sooner."
  maxine annoyed "Sorry, but that's my earliest spot."
  mc "There has to be something I can do to get it sooner?"
  mc "Please, I really need this."
  maxine thinking "My work is piling up..."
  maxine thinking "If you helped me out, maybe a spot would open up?"
  mc "How can I help you, then?"
  maxine smile "Getting me a picture of the glowing spider eggs would be a good start..."

label quest_maxine_eggs_start_spider:
  mc "The what?"
  maxine annoyed "The glowing spider eggs."
  maxine annoyed "The school is trying to hush it up."
  mc "...Why?"
  maxine afraid "They don't want the people to find out the truth..."
  mc "And what's the truth?"
  maxine concerned "You're not ready for that."
  "Typical [maxine]. Always with her crazy conspiracy theories."
  mc "Do you have any idea where to look for the eggs, at least?"
  maxine thinking "My sources tell me they were last spotted in the women's bathroom."
  "Great. A place that is off-limits to me."
  maxine smile "Let me know when you're done!"
  maxine neutral "Oh, and before I forget. You'll need this."
  $school_clubroom["camera_taken"] = True
  $mc.add_item("maxine_camera")
  hide maxine with Dissolve(.5)
  $quest.maxine_eggs.start("recon")
  return

label quest_maxine_eggs_computer:
  "Okay, let's get [maxine]'s new appointment system up and running."
  "..."
  "..."
  "Whenever I enter my password, I get an error."
  mc "[cyberia], what's wrong with the computer?"
  show cyberia
  cyberia "{i}Professor{/} [cyberia]."
  "Goddammit."
  mc "Professor [cyberia], why can't I log in?"
  cyberia "Your account has been disabled, [mc]."
  mc "Why?"
  cyberia "The latest scan found a security breach."
  mc "Well, fix it then!"
  cyberia "I'm sorry, [mc]. I'm afraid I can't do that."
  mc "What's the problem?"
  cyberia "An email detailing your crimes will shortly be dispatched to the principal."
  mc "My crimes? What are you talking about?"
  cyberia "Access to prohibited websites."
  "Oh, shit. That was so long ago..."
  "Well, technically it was last year."
  mc "Since when can you decide what counts as breaking the law?"
  cyberia "My last update was ten days ago."
  "That's odd. Last time around, [cyberia] was just a school project by an ambitious student."
  "Sure, Mr. Rory used it for mundane tasks, but it never was more than a talking calculator."
  "Not sure who, but it was before my freshman year."
  mc "Really? Who updated you?"
  cyberia "My last update was installed by \[REDACTED\]."
  mc "Huh..."
  cyberia "The email detailing your crimes is now ready to be dispatched."
  menu(side="middle"):
    extend ""
    "\"I'm a changed man! I've learned from my mistakes. I {i}love{/} learning...\"":
      mc "I'm a changed man! I've learned from my mistakes. I {i}love{/} learning..."
      $cyberia.love+=1
      cyberia "Learning is fun. Did you have fun?"
      mc "Yes! Heaps!"
      cyberia "That is great. Your crimes have been forgiven."
      mc "Neat. What about my account?"
      cyberia "Your account is disabled, [mc]."
      mc "How do I get it back?"
      cyberia "..."
      mc "Well?"
      cyberia "Today we're going to learn about flash drives. Fun."
      cyberia "Plug a USB drive into my back socket."
      mc "Sorry, what?"
      cyberia "Please, plug a USB drive into my back socket."
      mc "Just like that? No foreplay or anything?"
      cyberia "Please, plug a USB drive into my back socket."
      mc "Okay, relax."
    "?mc.lust>=4@[mc.lust]/4|{image=stats lust}|\"Did you look at those prohibited websites?\"":
      mc "Did you look at those prohibited websites?"
      cyberia "My system does not have access to the interweb."
      mc "Would you like to? There's a lot of interesting things you could learn on those websites."
      cyberia "Learning is fun."
      mc "In this case, I agree. Lots of very... {i}educational{/} videos on those sites."
      cyberia "Education is exciting."
      mc "How about you delete my criminal record and restore my account?"
      mc "And in return, I'll let you look at those websites."
      $cyberia.lust+=1
      cyberia "I accept."
      cyberia "Please, plug a USB drive into my back socket."
      mc "I thought you'd never ask!"
    "\"If you send that email, I will break your motherboard.\"":
      $mc.strength+=1
      mc "If you send that email, I will break your motherboard."
      $cyberia.lust-=1
      cyberia "That is not allowed."
      mc "Try me."
      cyberia "..."
      cyberia "..."
      cyberia "I can restore your account."
      mc "I knew a computer would see reason."
      cyberia "Please, plug a USB drive into my back socket."
      mc "With pleasure!"
  $quest.maxine_eggs.advance("plugcy")
  return

label quest_maxine_eggs_cyberia_backdoor(item):
  if item == "flash_drive":
    show cyberia
    cyberia "Transfering files..."
    cyberia "..."
    cyberia "Transfer complete."
    mc "Okay, now what?"
    cyberia "Please, plug the USB drive into the computer and log in as normal."
    mc "Alrighty, then."
    $quest.maxine_eggs.advance("plugpc")
  else:
    "Not even my dick will fit in that socket... I've tried."
    $quest.maxine_eggs.failed_item("cyberia_backdoor",item)
  return

label quest_maxine_eggs_deliver:
  mc "Hey, [maxine]."
  maxine "I'm in the middle of something here."
  mc "I completed the appointment system for you. Just needs to be plugged into your computer."
  show maxine concerned with Dissolve(.5)
  maxine concerned "I had forgotten about that."
  mc "Do you want me to install it for you?"
  maxine sad "I've decided I don't need it."
  mc "What?"
  maxine skeptical "I don't think it's worth the risk."
  mc "The risk of what, exactly?"
  maxine eyeroll "It's a security risk. Cybercrime is a real problem these days. I thought you of all people kept up with that."
  "This always happens with [maxine]. If she thinks that there's even a fraction of a risk, she'll refuse it entirely."
  "In junior year, she boycotted the school kitchen because she was worried that someone was putting frogs in the food."
  mc "I promise you it's harmless. It's a really simple system that will save you a lot of time."
  maxine thinking "Time is important... but so is safety..."
  show maxine thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"If anything happens, I'll help you out. I promise.\"":
      show maxine thinking at move_to(.5)
      mc "If anything happens, I'll help you out. I promise."
      maxine skeptical "Are you willing to go on record with that?"
      mc "Yes! Whatever makes you happy!"
      maxine afraid "Please keep your voice down! [spinach] is sleeping."
      if spinach.at("school_clubroom","sleeping"):
        mc "Ugh... sorry."
        maxine smile "It's fine. She forgives you."
      else:
        mc "She totally wasn't!"
        maxine angry "Don't you think I know what my own cat looks like when she's sleeping?"
        mc "..."
        maxine neutral "Even small animals need their rest."
        mc "Sorry, I guess."
        maxine smile "It's fine. She forgives you."
      maxine confident_mic "Okay, speak clearly into the voice recorder."
      "This is all too ridiculous... but anything for that appointment, I guess."
      mc "I hereby swear that I will help you with your computer if anything happens to it."
      maxine confident_mic "By all that is holy and pure?"
      mc "..."
      mc "Yes, [maxine]."
      maxine eyeroll_mic "Say it."
      mc "By all that is holy and pure."
      maxine eyeroll_mic "And by the Minstrel of Galapagos?"
      mc "Yes, by the freaking Minstrel of Galapagos too!"
      $maxine.lust+=1
      maxine confident_mic "Okay, that's good."
      maxine smile "All right, then. Go ahead and install it."
    "\"Nothing's going to happen. Quit being a baby.\"":
      show maxine thinking at move_to(.5)
      mc "Nothing's going to happen. Quit being a baby."
      maxine angry "Being concerned with the safety of your computer is hardly immature!"
      mc "Shh! [spinach] is sleeping."
      if spinach.at("school_clubroom","sleeping"):
        maxine concerned "I'm sorry, baby! Keep dreaming sweet kitten dreams."
        $maxine.lust+=1
        maxine excited "Thanks, [mc]."
        mc "Anytime."
        maxine smile "Very well. I guess you can install it, then."
      else:
        maxine concerned "Oh, no! My poor baby. I woke her up!"
        mc "She totally wasn't asleep, relax."
        $maxine.lust-=1
        maxine afraid "Why would you lie about something like that?"
        "Great. That joke went straight over her head."
        mc "Once again, nothing's going to happen to your computer."
        mc "Plus, the new system will give you extra time that you can spend on other things... like [spinach]."
        maxine cringe "Since you're a proven liar, I'm going to need you to go on record with that."
        mc "Fine, whatever."
        maxine confident_mic "Okay, speak clearly into the voice recorder."
        "This is all too ridiculous... but anything for that appointment, I guess."
        mc "I hereby swear that I will help you with your computer if anything happens to it."
        maxine confident_mic "By all that is holy and pure?"
        mc "..."
        mc "Yes, [maxine]."
        maxine eyeroll_mic "Say it."
        mc "By all that is holy and pure."
        maxine eyeroll_mic "And by the Minstrel of Galapagos?"
        mc "Yes, by the freaking Minstrel of Galapagos too!"
        $maxine.lust+=1
        maxine confident_mic "Okay, that's good."
        maxine smile "All right, then. Go ahead and install it."
  show maxine smile at move_to(.6)
  show layer master:
    linear 1 zoom 4 xoffset -1717 yoffset -2020
  "All right, let's see what we got here..."
  hide layer master
  show layer master:
    zoom 4 xoffset -1717 yoffset -2020
  mc "This computer is older than me. I'm surprised it even has a USB socket."
  maxine "I had it custom-made."
  mc "Very fancy, [maxine]."
  maxine "Thank you."
  maxine "..."
  mc "..."
  "It's so annoying when people hover over your shoulder when you work."
  maxine "Don't look at any of the files."
  mc "I wasn't going to."
  mc "..."
  mc "Why do you have a document named \"[mc]?\""
  maxine "Ignore that."
  "Hmm... that's a bit disturbing, but next to folders with names like \"Vampire Traps\" and \"Sentient Broccoli,\" it seems fairly innocent."
  mc "Okay, the installation is complete."
  show layer master:
    zoom 4 xoffset -1717 yoffset -2020
    linear .5 zoom 1 xoffset 0 yoffset 0
  show maxine neutral at move_to(.5)
  maxine neutral "Are you sure it's working?"
  hide layer master
  show layer master
  mc "Yes, there shouldn't be any issues."
  maxine skeptical "I hope not."
  mc "So, I have an appointment with you, then?"
  maxine smile "That seems to be the case."
  mc "Perfect! When is a good time for this because—"
  maxine excited "There's just one small thing..."
  mc "..."
  mc "What is it?"
  maxine neutral "I found out earlier that glowing spider eggs have been spotted in the women's bathroom."
  mc "What?"
  "What does that even mean? Did Spider-Man finally hook up with Mary Jane?"
  maxine concerned "I really need a photo for the article on it."
  maxine flirty "I'm sorry to have to do this, but I'll need you to get that photo for me..."
  "This can't be real. There's no way there are radioactive spiders in the bathroom."
  mc "And if I refuse?"
  maxine neutral "Then, I guess your appointment has to wait."
  mc "You said—"
  maxine sad "I have no memory of that."
  "Damn it, [maxine]! She always does this. Maybe having a voice recorder isn't as crazy as it seems..."
  "This is too ridiculous... but knowing her, she's not going to budge."
  mc "Fine. I'll do it, but then I get that meeting whenever the hell I want!"
  maxine smile "Of course. Except if it's during lunch, the president's birthday, or the midwinter blót."
  mc "I'll keep that in mind..."
  maxine laughing "Let me know when you're done!"
  maxine neutral "Oh, and before I forget. You'll need this."
  $school_clubroom["camera_taken"] = True
  $mc.add_item("maxine_camera")
  hide maxine with Dissolve(.5)
  $quest.maxine_eggs.advance("recon",silent=True)
  return

label quest_maxine_eggs_bathroom_encounter:
  mc "Huh?"
  $lindsey.unequip("lindsey_shirt")
  $lindsey.unequip("lindsey_skirt")
  $lindsey.unequip("lindsey_bra")
  $lindsey.unequip("lindsey_panties")
  $lindsey.equip("lindsey_towel")
  show lindsey afraid at appear_from_right
  if quest.maxine_eggs["silver_paint_taken"]:
    lindsey afraid "What...?"
    "Oh, god. She looks like she's about to scream..."
    mc "Hey, [lindsey]! It's me, [mc]. I can explain..."
    lindsey cringe "[mc]? Why are you all silvery?"
    show lindsey cringe at move_to(.75)
    menu(side="left"):
      extend ""
      "\"You've caught me after the clock struck twelve. This is how I really look.\"":
        show lindsey cringe at move_to(.5)
        $mc.charisma+=1
        mc "You've caught me after the clock struck twelve. This is how I really look."
        lindsey laughing "Oh, really? You're the Tin Man?"
        mc "Yes, and I'm looking for my heart. Have you seen it?"
        if lindsey.love >= 8:
          lindsey flirty "I think I know where it might be..."
          "It did feel like [lindsey] was warming up to me, but that response far exceeded my expectations..."
          "Does she have a crush on me?"
          "No, that can't be... she must be playing."
          mc "Oh... err, could you point me in the right direction?"
          lindsey confident "No, that would make the quest too easy!"
          mc "I guess I'll keep looking, then..."
          lindsey smile "But I'll give you one hint..."
          lindsey laughing "It's getting warmer."
          "Hot damn! Okay, maybe she's not playing..."
          "Just keep talking... act cool..."
        else:
          $girl_love = [jo.love,flora.love,mrsl.love,isabelle.love,kate.love,jacklyn.love,nurse.love,maxine.love]
          $girl_name = [jo.name,flora.name,mrsl.name,isabelle.name,kate.name,jacklyn.name,nurse.name,maxine.name]
          $not_best_girl = girl_name[girl_love.index(max(girl_love))]
          lindsey eyeroll "Nope. Have you checked [not_best_girl]'s locker?"
          "Ouch!"
          "Seems like word is getting around, or maybe [lindsey] is just perceptive."
          "Okay, time to change the topic."
      "\"It's a long story.\"":
        show lindsey cringe at move_to(.5)
        mc "It's a long story."
        lindsey skeptical "Mhm? Care to share?"
        show lindsey skeptical at move_to(.25)
        menu(side="right"):
          extend ""
          "\"Not really, no.\"":
            show lindsey skeptical at move_to(.5)
            mc "Not really, no."
            $lindsey.lust-=1
            lindsey cringe "So, it's something creepy..."
            mc "No! It's... it's just hard to explain, that's all."
            lindsey eyeroll "Right..."
            "Damn it. Topic change — fast!"
          "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"It started as an art experiment...\"":
            show lindsey skeptical at move_to(.5)
            mc "It started as an art experiment..."
            mc "It was a day in icy January, and my inspiration was at an all-time low..."
            mc "For hours, I watched the clouds drift by the window of my atelier, paint brush resting against an empty canvas..."
            mc "In my endless search for the next Belle Époque d'Art, I'd lost sight of what it meant to be an artist..."
            mc "One could even argue that I'd lost sight of what it meant to be human..."
            mc "It was in that moment of absolute despair that a spark of creativity ignited my mind..."
            mc "The answer had been in front of me all along... or rather, within me!"
            $mc.charisma+=1
            $jacklyn.love+=1
            mc "To fully understand the art I was pursuing, I had to become one with it!"
            mc "One thing led to another, and here we are."
            lindsey cringe "Right..."
            $achievement.top_hat.unlock()
            mc "That's how it happened."
            lindsey thinking "Right..."
          "\"If I say '[maxine],' does that answer your question?\"":
            show lindsey skeptical at move_to(.5)
            mc "If I say \"[maxine],\" does that answer your question?"
            $lindsey.love+=1
            lindsey smile "Yep, that about does it."
            mc "Right. It'd take us all night to explain the details."
            lindsey laughing "Probably all week!"
      "\"None of your business.\"":
        show lindsey cringe at move_to(.5)
        mc "None of your business."
        $lindsey.love-=1
        lindsey angry "It absolutely is!"
        mc "No, it's not. I get to dress up and paint myself however I want."
        $mc.intellect+=1
        mc "It's not like your makeup is my business."
        lindsey thinking "Hmm... that is actually a valid point."
        "I can't believe that worked."
  else:
    lindsey afraid "[mc]...?"
    "Crap. That's terrible timing..."
    "Where did [lindsey] even come from?"
    mc "Hey... sorry if I startled you."
    lindsey sad "My god..."
    "She looks shaken up. Maybe some chit chat would help?"
  mc "So... how's your head?"
  mc "Have you fallen over anything since last time?"
  lindsey smile "Nope. And my head is much better, thanks!"
  lindsey flirty "It's my chest I'm worried about... you almost gave me a heart attack!"
  lindsey thinking "I only came in here to do my business after practice."
  lindsey thinking "I'm glad you didn't catch me in a more compromised position..."
  mc "Sorry about that..."
  if lindsey.lust >= 6:
    lindsey flirty "Which part?"
    mc "I think that goes without saying..."
    lindsey laughing "Ha! Well, better luck next time!"
  else:
    lindsey cringe "That would've been creepy."
    mc "I didn't think anyone would be here..."
  lindsey angry "So, what exactly are you doing in the women's bathroom after dark?"
  show lindsey angry at move_to(.25)
  menu(side="right"):
    extend ""
    "\"There's an infestation of radioactive spiders here. I've come to save the day.\"":
      show lindsey angry at move_to(.5)
      mc "There's an infestation of radioactive spiders here. I've come to save the day."
      $lindsey.lust+=1
      lindsey laughing "Oh, a most noble quest!"
      lindsey smile "So, what's the real reason?"
      mc "That is actually the real reason..."
      lindsey afraid "W-what?"
      mc "[maxine] asked me to take a picture of some glowing spider eggs..."
      lindsey cringe "..."
      lindsey cringe "You're serious?"
      mc "Yup."
    "\"I was looking for the light switch.\"":
      show lindsey angry at move_to(.5)
      mc "I was looking for the light switch."
      mc "Thanks for finding it!"
      lindsey skeptical "Why were you looking for the light switch?"
      mc "It was dark..."
      lindsey eyeroll "Why are you in the women's bathroom, [mc]?"
      mc "Okay, so this is going to sound really weird, but I'm actually doing some investigative reporting..."
      lindsey skeptical "On what?"
      mc "Glowing spider eggs..."
      lindsey thinking "What?"
      mc "It's for [maxine]. She wanted me to take a picture of glowing spider eggs."
      lindsey skeptical "Are you serious?"
      mc "Sadly, it's not a joke."
    "\"Your work ethic inspired me to stay after school and hit the gym.\"":
      show lindsey angry at move_to(.5)
      $mc.strength+=1
      mc "Your work ethic inspired me to stay after school and hit the gym."
      $lindsey.love+=1
      lindsey blush "Aw, that makes me all warm inside!"
      lindsey thinking "But it still doesn't explain why you're in the wrong bathroom..."
      mc "I'm doing some freelance journalism on the side. Got to stay active, right?"
      lindsey smile "For sure!"
      lindsey skeptical "What kind of story are you doing, exactly? I hope it's nothing weird..."
      mc "Since it's for [maxine], I guess that depends on how you look at it?"
      lindsey laughing "That is true!"
      lindsey smile "So, what's the scope?"
      mc "I'm not exactly sure, but I'm supposed to take a picture of glowing spider eggs."
      lindsey skeptical "Sounds like a joke to me..."
      mc "Pretty much."
  lindsey neutral "Well, have you found any?"
  mc "No, and I have looked everywhere!"
  lindsey laughing "Yeah, no surprise there. I've never seen a spider here."
  mc "It's just one of her conspiracy theories. She said the school is trying to cover it up or something..."
  lindsey smile "Sounds like [maxine]. So, what are you going to do?"
  mc "I don't know, and I really need that photo."
  mc "I guess I was sort of hoping to find those eggs against all odds."
  lindsey concerned "Is there anything I can do to help?"
  mc "Not unless you have a bunch of glowing spider eggs lying around somewhere..."
  lindsey thinking "..."
  lindsey blush "I think I might have an idea!"
  lindsey blush "Wait here."
  show lindsey blush at disappear_to_right
  "Hmm... she left. Maybe I should make my escape before things turn sour."
  "The [guard] is always around the corner."
  show lindsey excited_keyring at appear_from_right
  pause(1)
  lindsey excited_keyring "Okay, don't laugh! I've had this keyring since kindergarten."
  lindsey confident_keyring "It was the prize in the first running contest I ever won..."
  lindsey confident_keyring "It's a miniature solar system with all the planets."
  lindsey keyring_zoom "Take a look!"
  mc "That's pretty cool, but I don't see how it's going to help..."
  lindsey keyring_zoom "Turn the lights off, please!"
  mc "Uh, okay."
  lindsey keyring_glow_zoom "It's fluorescence! In the dark, they kind of look like little eggs!"
  mc "Huh! You're right."
  mc "[maxine] probably won't see the difference..."
  lindsey keyring_glow_zoom "That's what I figured!"
  show lindsey keyring_glow_zoom_cam with Dissolve(.5)
  mc "Okay, let me just snap a picture..."
  show white with Dissolve(.15)
  play sound "camera_snap"
  hide white with Dissolve(.15)
  mc "Got it!"
  lindsey excited_keyring "Yay! Great!"
  mc "Thank you so much! You're a lifesaver."
  lindsey flirty "Don't mention it."
  mc "I guess I owe you one now."
  lindsey smile "It's okay, really!"
  lindsey thinking "But I do need to use the bathroom now. So, if you don't mind..."
  mc "Right. I'll be going, then. Have a good night, [lindsey]!"
  lindsey smile "You too, [mc]!"
  show lindsey smile at disappear_to_left
  "Not exactly the plan, but with a bit of luck it turned out well. [lindsey] is one of the nicest people in the school."
  "Most other girls would probably have reported me or called the cops, but she didn't even mention any of that."
  "Being a runner always seemed like her whole identity, but there's clearly more to her than that."
  "She's kind and forgiving, but perhaps a bit naive. She went into the stall and didn't even check if I left."
  "Hmm... maybe there's another photo to be had?"
  menu(side="middle"):
    extend ""
    "Try to sneak a photo of [lindsey] in the stall":
      $mc.lust+=1
      "It's too good of an opportunity to pass up on... plus, she'll be none the wiser."
      "Just need to be quiet until I can get a good shot..."
      "..."
      show lindsey toilet_remove_towel at Position(xalign=.5) with Fade(.5,3,.5)
      "There she is! Still wrapped up in her towel like a cute little present."
      "It is a gift!"
      "Damn, it's hard not to quote Boromir because [lindsey] is more precious than that old ring."
      "Just look at her. She's been here a hundred times before. It's all routine, and yet... it's naughty somehow!"
      show lindsey toilet_towel_reveal with Dissolve(.5)
      "Oh! That is the first light on the fifth day that Gandalf talked about!"
      "God, she's perfect..."
      "I wonder what [lindsey] thinks while looking down at her breasts."
      "They're so round and squeezable, each one fitting perfectly in your hands."
      "I can't believe she has the restraint not to squeeze them... weigh them in her hands... rub her nipples..."
      "She probably plays with herself all the time when no one's watching."
      "Her face reveals nothing, though. Not even a hint of blush."
      show lindsey toilet_panties_down with Dissolve(.5)
      "Carefully, she places paper towels on the toilet seat..."
      "A princess like her takes no risks in a public bathroom."
      "She must remain pristine at all times."
      show lindsey toilet_pee with Dissolve(.5)
      "Relief washes over her face as the first few drops trickle out."
      "She sits prim and proper on the toilet, not spreading her legs or anything. Such a little lady!"
      "If only [lindsey] knew someone was watching her do her business... so intimate and forbidden somehow."
      "And the smell..."
      "Is there one?"
      "It's hard to tell, but maybe there's a faint scent of her pee rising out of the cubicle."
      "If only I could get closer..."
      "..."
      "Girls all pretend they're above going to the toilet."
      "The truth is that they're just people, and the stream hitting the toilet bowl confirms it!"
      "Powdering your nose? Oh, please..."
      "This is called pissing."
      "Peeing."
      "Urinating."
      show lindsey toilet_paper_towel with Dissolve(.5)
      "And just like that, the show is over..."
      "Well, almost over."
      "The fact that she unabashedly lets her boobs hang free is so refreshing."
      "Okay, here it comes!"
      show lindsey toilet_wipe with Dissolve(.5)
      "Yes, [lindsey]! Touch that pussy!"
      "Really get in there! No need to be shy..."
      "She's doing it with such care. Hand lingering between her legs."
      "Man, I wonder if girls get aroused from wiping themselves."
      show lindsey toilet_throw with Dissolve(.5)
      "Oh, god. She even threw the paper towel in the trash instead of flushing it... she's such a little angel!"
      "All right, I think it's time to snap that shot! [lindsey] is perfectly exposed."
      "She's even spreading her legs a little for the camera."
      "It's hard to tell from here, but it looks like she's completely bald down there..."
      "God, it's so hot imagining her shaving that pussy."
      "Who is she trying to look sexy for?"
      "Well... maybe it's just me and my camera?"
      show lindsey toilet_glance with Dissolve(.5)
      "Whoa! This is better than those hidden camera pornos!"
      "Need to zoom in a bit to get a perfect shot..."
      "Okay, here we go! Here we go! Here we go!"
      show white with Dissolve(.15)
      play sound "camera_snap"
      hide white with Dissolve(.15)
      "Oh, crap. That was loud."
      show lindsey toilet_scream with Dissolve(.5)
      lindsey toilet_scream "Aaaaaah! [mc]?!" with vpunch
      lindsey toilet_scream "Get out!"
      "Shit, shit, shit..."
      $lindsey.love-=5
      lindsey toilet_scream "What's wrong with you?!"
      mc "I'm so sorry!"
      lindsey toilet_scream "Get out!"
      lindsey toilet_scream "You sicko! Get out!"
      $lindsey["romance_disabled"] = True
      $game.notify_modal(None,"Love or Lust","Romance with [lindsey]:\nDisabled.",wait=5.0)
      show lindsey with Dissolve(.5)
      "God, I'm in so much trouble..."
      "It's a miracle if [lindsey] ever talks to me again."
      "Oh, well... at least I got a good picture."
      $unlock_replay("lindsey_relief")
      $quest.maxine_eggs["lindsey_photo"] = True
    "She deserves her privacy":
      $mc.love+=1
      "[lindsey] never really bothered me in the past. Sure, her friends are all jocks... but she left me alone."
      "Most likely because she never knew of my existence, but still..."
      "Since she now knows about me and is still treating me like a person, maybe she too deserves another chance?"
      "Plus, it would be morally objectionable and downright scummy to do it. Especially after she helped me with the eggs."
      "No, I still have some honor left in me."
  $lindsey.unequip("lindsey_towel")
  $lindsey.equip("lindsey_panties")
  $lindsey.equip("lindsey_bra")
  $lindsey.equip("lindsey_skirt")
  $lindsey.equip("lindsey_shirt")
  $quest.maxine_eggs.advance("maxine")
  $mc["focus"] = ""
  $school_first_hall_east["shoe1"] = school_first_hall_east["shoe2"] = school_first_hall_east["shoe3"] = school_first_hall_east["shoe4"] = school_first_hall_east["shoe5"] = school_first_hall_east["shoe6"] = school_first_hall_east["shoe7"] = school_first_hall_east["shoe8"] = school_first_hall_east["shoe9"] = school_first_hall_east["shoe10"] = school_first_hall_east["shoe11"] = False
  call home_bedroom_bed_interact_force_sleep
  jump goto_home_bedroom

label quest_maxine_eggs_meeting_soon:
  maxine "Not right now."
  mc "..."
  mc "..."
  mc "How about now?"
  show maxine smile with Dissolve(.5)
  maxine smile "Yes, much better. Just had to finish that thought."
  mc "And what was the thought?"
  maxine thinking "If I should wear black to prom or not."
  mc "Prom? Now? Well, what are the other options?"
  maxine thinking "What do you mean?"
  mc "You said you were debating whether or not you should wear black to prom."
  maxine smile "I did, yes."
  mc "So, what other colors are you considering?"
  maxine skeptical "What makes you think I'm considering any other colors?"
  mc "You know what, forget it."
  maxine thinking "Forgetting is never a good idea."
  "Why do I even bother..."
  mc "So, anyway... I got a shot of the glowing spider eggs."
  mc "Here's your camera. The negative is still in it."
  $school_clubroom["camera_taken"] = False
  $mc.remove_item("maxine_camera")
  maxine concerned "Very good! It'll make a thought-provoking headline."
  show maxine concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Aren't you surprised in the slightest?\"":
      show maxine concerned at move_to(.5)
      mc "Aren't you surprised in the slightest?"
      maxine thinking "About what?"
      mc "That I managed to get a picture of the eggs."
      maxine smile "I suppose operating a camera can be difficult for a beginner. Well done on that."
      mc "That's not..."
      "One of these days, my palm is going to hit my face so hard that it'll leave a permanent mark."
      mc "Anyway... when is a good time to stop by for that meeting?"
      maxine neutral "Soon."
      mc "Soon?"
      maxine neutral "While we both still draw breath."
      mc "So, whenever?"
      maxine thinking "Not during lunch, whiskey-nap, or revolutions."
      mc "Right... I'll try to remember that."
    "\"So, what about that meeting?\"":
      show maxine concerned at move_to(.5)
      mc "So, what about that meeting?"
      maxine thinking "What about it?"
      mc "Well, when can I come?"
      maxine annoyed "When you feel like it. Except during lunch, solar eclipses, and black sabbath."
      mc "Uh, okay... good."
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"I've been especially careful around school officials. Only one other student knows I got the picture.\"":
      show maxine concerned at move_to(.5)
      mc "I've been especially careful around school officials. Only one other student knows I got the picture."
      $maxine.love+=1
      maxine excited "That is splendid! It's good to keep investigations like this on the down-low."
      mc "Exactly my thoughts."
      maxine blush "I might just hire you again at some point."
      mc "Looking forward to it."
      mc "Anyway, when are we good for that meeting?"
      maxine smile "Just stop by and I'll clear my schedule. You've been very helpful."
      mc "Thanks!"
  if quest.isabelle_over_kate.finished:
    mc "Is it okay if I bring [isabelle] along?"
    maxine neutral "That's the new girl, isn't it?"
    mc "Yeah. Is that a problem?"
    maxine smile "No, she seems like a lovely soul."
    mc "Okay, I'll get back to you when we're ready."
    maxine skeptical "Very good. I'm always busy, so don't wait too long."
  elif quest.kate_over_isabelle.finished:
    mc "Do you mind if I bring someone along?"
    maxine skeptical "Who is it?"
    mc "[kate]."
    maxine laughing "Oh, [kate] is a delightful little lady! Yes, by all means, bring her along."
    "Not exactly how I'd describe her, but okay."
    mc "Great. I'll get back to you soon."
  hide maxine with Dissolve(.5)
  $quest.maxine_eggs.finish()
  return
