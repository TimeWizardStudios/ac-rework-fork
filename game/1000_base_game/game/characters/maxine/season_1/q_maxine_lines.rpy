label quest_maxine_lines_start:
  show maxine concerned_binoculars with Dissolve(.5)
  "[maxine] always seems to be investigating something, and it's usually best not to ask any questions..."
  "But what the hell is she looking at now?"
  "The ceiling looks the way it always has."
  maxine concerned_binoculars "Please, be quiet."
  mc "I haven't even said anything..."
  maxine eyeroll "It was an attempt at a pre-emptive measure."
  "Sometimes I think she's just trolling me, but it's impossible to know."
  mc "What were you looking at up there?"
  maxine annoyed "Answers."
  mc "Answers to what?"
  maxine annoyed "Answers to my questions."
  mc "..."
  maxine concerned_binoculars "..."
  mc "So... what's the question?"
  maxine concerned_binoculars "..."
  mc "Hello?"
  maxine skeptical "How many lamps are there in the ceiling?"
  mc "What?"
  maxine skeptical "How many lamps are there in the ceiling?"
  mc "Umm..."
  show maxine skeptical at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Thirteen.\"":
      show maxine skeptical at move_to(.5)
      mc "Thirteen."
      maxine sad "That is factually incorrect."
      maxine sad "Were you just guessing or did you miscount that badly?"
      mc "Err..."
      maxine thinking "Arrogance or imperception? One is worse than the other."
      "Knowing [maxine], it's probably the second..."
      mc "I took a wild guess."
      maxine annoyed "Guesswork is the cradle of misconception."
      maxine annoyed "There are fifteen lamps in total."
    "\"Fourteen.\"":
      show maxine skeptical at move_to(.5)
      mc "Fourteen."
      maxine thinking "You missed the one right above the left basketball hoop, didn't you?"
      mc "..."
      mc "...I did."
      maxine sad "It is a sneaky one, but you should've caught it..."
      $maxine.love-=1
      maxine sad "It's hard to trust you when you make such glaring mistakes."
      maxine angry "Now, do the math in your head once more!"
      maxine angry "Simple addition shouldn't be beyond your capabilities."
      mc "..."
      mc "Fifteen?"
      maxine confident "Fifteen is correct."
    "\"Fifteen.\"":
      show maxine skeptical at move_to(.5)
      mc "Fifteen."
      maxine laughing "You took so long, I wasn't sure you'd get it right!"
      $maxine.love+=1
      maxine smile "I'm pleasantly surprised."
      mc "Thanks, I guess..."
      maxine smile "It's important to stay sharp. Remember that."
      mc "I'll... keep it in mind?"
      maxine skeptical "Will you?"
      mc "Of course!"
      maxine skeptical "So, there are a total of fifteen lamps..."
    "\"You think I pay attention to stuff like that?\"":
      show maxine skeptical at move_to(.5)
      mc "You think I pay attention to stuff like that?"
      $maxine.love-=1
      $maxine.lust-=1
      maxine annoyed "You should."
      mc "Well, I don't."
      maxine annoyed "You should."
      mc "I don't, okay?"
      maxine annoyed "You should."
      "She's not going to back down, is she?"
      mc "Why don't you tell me how many lamps there are, then?"
      maxine eyeroll "Because it's not my duty to babysit you."
      mc "I need babysitting against lamps?"
      maxine sad "You might..."
      mc "So, how many lamps are there?"
      maxine thinking "Fifteen."
  maxine confident "I assume you see the problem."
  mc "The problem?"
  maxine annoyed "There are fifteen lamps in the ceiling."
  mc "Why is that a problem?"
  maxine skeptical "Take a closer look. I'll wait."
  mc "..."
  "[maxine] is crazy, right?"
  "Why would the number of lamps be a problem?"
  "Whenever I listen to her I feel my mind slowly turning into mush..."
  maxine skeptical "Do you see it yet?"
  mc "No, I don't! I have no idea what you're talking about!"
  maxine annoyed "That's a shame..."
  mc "..."
  mc "Are you going to tell me?"
  maxine eyeroll "Yes."
  mc "Well, I'm all ears."
  maxine concerned_binoculars "Do you see those square beams up there?"
  maxine concerned_binoculars "They make up a pattern along with the lamps."
  maxine concerned_binoculars "You say there are fifteen lamps up there, but logically... there should be forty."
  mc "I'm not sure I follow..."
  maxine eyeroll "The basketball court is the center of the room. Logically, there should be twenty lamps on either side of it."
  maxine annoyed "You've counted them yourself. There're only fifteen on the left side and none on the right side."
  mc "Huh..."
  maxine skeptical "This is your problem, [mc]. You're only ever looking at what's in front of you."
  "She's right... I rarely do look at things from a different perspective."
  mc "So, what does this mean? The missing lamps?"
  maxine sad flip "Maybe those that built this place didn't think anyone would pay attention."
  maxine thinking flip "But I'm always watching, and I'm not afraid to break walls to find the answers I seek."
  maxine thinking flip "Start paying attention, [mc]."
  show maxine thinking flip at disappear_to_right
  pause(.5)
  "Damn it, [maxine]! God, I hate it when she does that!"
  "Where the fuck did those last 25 lamps go?!"
  "Maybe I should start paying closer attention to things..."
  $quest.maxine_lines.start()
  return

label maxine_lines_locator:
  show maxine skeptical with Dissolve(.5)
  maxine skeptical "I don't remember inviting you to a meeting, [mc]."
  show maxine skeptical at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Check your calendar.\"":
      show maxine skeptical at move_to(.5)
      mc "Check your calendar."
      maxine sad "Hmm..."
      maxine sad "I only have one entry with your name, and it's for Christmas day."
      mc "I never booked a meeting on Christmas day..."
      maxine thinking "It's not a meeting."
      mc "What is it, then?"
      maxine thinking "It's a check-up."
      mc "I never booked one of those either."
      maxine skeptical "Are you telling the truth?"
      "Did I ever book a Christmas check-up with her? What even is it?"
      mc "What's the check-up for?"
      maxine eyeroll "You, of course. You can't book check-ups for other people."
      mc "Ugh..."
      mc "You know what? Forget it."
      maxine confident "I rarely do. This is going into the ledger."
      mc "..."
      "Okay, just act casual, man."
    "?mc.strength>=4@[mc.strength]/4|{image=stats str}|\"What are you going to do, throw me out?\"":
      show maxine skeptical at move_to(.5)
      mc "What are you going to do, throw me out?"
      maxine thinking "..."
      "I hate it when she looks at me like that..."
      show maxine angry with Dissolve(.5)
      maxine angry "Spinach, kill!" with vpunch
      show maxine angry at move_to(.75,1)
      show spinach neutral at appear_from_left(.25)
      spinach neutral "Meow!"
      maxine sad "Spinach...?"
      show spinach hearteyed at bounce
      spinach hearteyed "Meow! Meow!"
      show spinach closedeyes at disappear_to_left
      show maxine sad at move_to(.5,1)
      pause(1)
      hide spinach
      maxine thinking "You live this time."
      mc "I think she likes me!"
      $maxine.love+=1
      maxine smile "I think so too. She rarely leaves her prey alive."
      maxine neutral "Maybe she's just waiting for the right moment to strike."
    "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"I invited myself in.\"":
      show maxine skeptical at move_to(.5)
      mc "I invited myself in."
      maxine neutral "Very well."
      mc "..."
      maxine neutral "..."
      mc "That's it?"
      maxine laughing "As it happens, I need a guinea pig."
      mc "I don't think—"
      maxine concerned_binoculars "Look up at the ceiling."
      maxine concerned_binoculars "How many lamps do you see?"
      "This shit again?"
      show maxine concerned_binoculars at move_to(.25)
      menu(side="right"):
        extend ""
        "\"One.\"":
          show maxine concerned_binoculars at move_to(.5)
          mc "One."
          maxine concerned_binoculars "Oh, really? Prove it."
          mc "Err... it's right above me."
          maxine concerned_binoculars "I don't see it..."
          mc "It's there. Trust me."
          maxine concerned "How come the room gets dark when the sun goes down, then?"
          mc "The lamp is... err, activated by a light sensor?"
          maxine sad "Interesting... that would explain why the school board refuses to install a lamp in this room."
          mc "I don't think that's the job of the school board..."
          maxine thinking "Whose job is it?"
          mc "I'm guessing it's the janitor's job? Or maybe the guard's?"
          $maxine.lust+=1
          maxine thinking "Very well, I'll trust you on this. I will reach out to them by letter."
        "\"There aren't any lamps.\"":
          show maxine concerned_binoculars at move_to(.5)
          mc "There aren't any lamps."
          maxine concerned_binoculars "I assume you see the problem with that, right?"
          mc "Something-something the shadows indicate that there should be a lamp somewhere?"
          maxine eyeroll "Don't be ridiculous."
          maxine annoyed "There's no lamp here... it gets very dark when the sun goes down. That's the problem."
          "God, she's insufferable!"
          $maxine.love+=1
          maxine confident "Thanks for being my witness. I need to write a lengthy letter to the school board about this."
  mc "Cool. In the meantime, I was curious about the locator thing..."
  maxine excited "The Ley Line Locator, you mean?"
  mc "That's the one, yeah."
  mc "How does it work?"
  maxine concerned "It doesn't."
  "No surprise there."
  maxine concerned "It needs a battery stack."
  mc "A battery stack?"
  mc "Can I borrow it?"
  maxine annoyed "What for?"
  mc "To find the saucer-eyed doll, of course!"
  maxine skeptical "What do you know about that?"
  mc "That... err, it's a doll... and it has saucers for eyes... and it messes with the magnetic field?"
  maxine laughing "Very well."
  mc "So, I can borrow it?"
  maxine neutral "I didn't say that."
  maxine smile "But I can be persuaded if you hit a shot in the gym..."
  mc "That's impossible!"
  maxine smile "Those are my conditions."
  mc "But why?"
  maxine concerned "I need to see if there's something malignant at work there."
  mc "So, it's true that hitting the shot is impossible?"
  maxine flirty "We're going to find out..."
  "Ugh... If only I were as tall and athletic as Chad..."
  $quest.maxine_lines.advance("shot")
  hide maxine neutral with Dissolve(.5)
  return

label maxine_lines_shot_interact:
  show maxine smile_popcorn with Dissolve(.5)
  maxine smile_popcorn "The ball is right there. Hit the hoop."
  mc "Where did you even get popcorn from?"
  maxine laughing_popcorn "Why, from the popcorn dispenser, of course!"
  "There's no popcorn dispenser..."
  hide maxine with Dissolve(.5)
  $maxine["hit_the_hoop"] = True
  return

label maxine_lines_reward_interact_maxine:
  show maxine annoyed with Dissolve(.5)
  mc "How did you get here so fast?"
  maxine eyeroll "I went through the hidden passage."
  mc "Right... the hidden passage. Of course."
  maxine skeptical "After careful consideration, I've decided that you didn't hit the shot."
  mc "What? But you saw it with your own eyes!"
  maxine skeptical "I wasn't looking."
  mc "Seriously?"
  maxine annoyed "I wouldn't be joking about something like this."
  mc "But I hit it!"
  maxine annoyed "I was looking at you the entire time. Your eyes were closed."
  mc "..."
  maxine thinking "Neither of us can say in good faith and with certainty that you hit the shot."
  maxine thinking "So, I've decided that you missed it because all the evidence points to that."
  mc "This is ridiculous! What evidence?!"
  maxine sad "No one has hit it before, and your previous shots have all been miserable."
  mc "You've been watching my previous attempts...?"
  maxine excited "Naturally. I always gather the necessary data before conducting an experiment."
  mc "So, you're not going to let me borrow the locator thingy?"
  maxine concerned "Well, did you hit the shot?"
  show maxine concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"You know the answer to that.\"":
      show maxine concerned at move_to(.5)
      mc "You know the answer to that."
      maxine skeptical "Do you know the answer, though?"
      maxine skeptical "I think you weren't paying attention again."
      mc "Why are you trying to gaslight me?"
      maxine eyeroll "I'm not. The truth is, you're not sure if you hit the shot or not."
      maxine annoyed "You closed your eyes and just assumed it hit."
      mc "Okay, let's try another experiment..."
      maxine confident "Very well."
      mc "Close your eyes."
      maxine laughing "Okay, what now?"
      mc "..."
      $school_clubroom['locator_taken'] = True
      $mc.add_item("leyline_locator")
      mc "Okay, the experiment is complete!"
      maxine neutral "..."
      maxine sad "..."
      maxine thinking "Did you just stuff my Ley Line Locator into your backpack?"
      mc "Did you see me do it?"
      maxine afraid "No, but—"
      mc "Well, after careful consideration, I've decided that I didn't steal it."
      mc "And since you didn't see it, you can't prove otherwise."
      mc "Have a good day!"
      maxine angry "..."
      $maxine.lust+=1
      maxine angry "I'm watching you, [mc]."
    "?lindsey.lust>=3@[lindsey.lust]/3|{image= lindsey contact_icon}|{image=stats lust}|\"What about [lindsey]?\"":
      show maxine concerned at move_to(.5)
      mc "What about [lindsey]?"
      maxine thinking "What about her?"
      mc "If the saucer-eyed doll is making her trip, shouldn't we try to stop it?"
      maxine sad "[lindsey] is of no concern to me."
      mc "Wait, really?"
      mc "Didn't you use to be friends?"
      maxine skeptical "Yes."
      maxine skeptical "That is very observant of you."
      mc "So, why is her well-being of no concern?"
      maxine eyeroll "That is of no concern to you."
      $mc.love+=1
      mc "Well, she matters to me so I'm just going to borrow it."
      maxine annoyed "Bold."
      mc "I've been called worse..."
      mc "..."
      $school_clubroom['locator_taken'] = True
      $mc.add_item("leyline_locator")
      "Hmm... [maxine] didn't even try to stop me."
      "I wonder what happened between them?"
    "\"What about the saucer-eyed doll?\"":
      show maxine concerned at move_to(.5)
      mc "What about the saucer-eyed doll?"
      maxine thinking "What about it?"
      mc "Shouldn't we try to stop it?"
      maxine thinking "That would be ideal."
      mc "So, can I borrow the locator thingy?"
      maxine sad "..."
      maxine thinking "Maybe."
      mc "Seriously?"
      maxine sad "..."
      maxine thinking "Yes."
      "[maxine] follows some kind of logic that I can't even begin to understand."
      mc "Why couldn't you just give it to me when I asked you the first time?"
      maxine neutral "I needed to collect data."
      mc "Data on what?"
      maxine smile "On you."
      maxine smile "I needed to make sure I wasn't giving away my prized artifact to some crazy person."
      mc "But we've talked in the past..."
      maxine eyeroll "Contrary to popular belief, people do in fact change."
      mc "So, I passed your data collection test or whatever?"
      $maxine.love+=1
      maxine confident "For now."
      $quest.maxine_lines['borrowed'] = True
      $school_clubroom['locator_taken'] = True
      $mc.add_item("leyline_locator")
      mc "Thanks..."
      maxine skeptical "Don't get too chipper. There's still a lot of data left to collect about you."
  hide maxine with Dissolve(.5)
  "Well, time to find out why [lindsey] keeps tripping..."
  $school_computer_room["unlocked"] = True
  $quest.maxine_lines.advance("battery")
  return

label quest_maxine_lines_insight:
  show maxine concerned with Dissolve(.5)
  maxine concerned "Have you seen my cat?"
  if spinach.at("school_clubroom"):
    mc "She's right there..."
    maxine flirty "Indeed. I was just checking your eyesight."
  else:
    mc "I saw her earlier... not sure where she's now."
    maxine concerned "She usually never leaves..."
    maxine concerned "I hope she's okay."
    mc "[spinach] is a smart cat! She can take care of herself."
  mc "Anyway, I found something interesting while playing around with your locator device."
  maxine skeptical "Did I say you could borrow it?"
  show maxine skeptical at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You absolutely did!\"" if quest.maxine_lines["borrowed"]: ## If the player chose "What about the saucer-eyed doll?" in phase_4_reward:
      show maxine skeptical at move_to(.5)
      mc "You absolutely did!"
      mc "I suggested we need to stop the saucer-eyed doll, and you said that would be ideal."
      mc "Then I asked if I could borrow it, and you said yes."
      maxine blush "Very good."
      maxine blush "You've scored well on the memory test."
      mc "Thanks?"
      maxine excited "Now for the physical test! Please, undress."
      mc "I think we're done with tests for now..."
      maxine concerned "We've barely started."
      show maxine concerned at move_to(.25)
      menu(side="right"):
        extend ""
        "\"We're done testing.\"":
          show maxine concerned at move_to(.5)
          mc "We're done testing."
          maxine thinking "We will revisit this test at a later stage..."
          mc "Don't count on it."
          maxine thinking "There's no counting involved in this test."
          maxine sad "But I've told you too much already."
          mc "Yeah, it seems like you have."
          mc "You're basically leaking confidential information."
          mc "You're lucky I don't gossip."
          maxine cringe "Gossip is unreliable information!"
          mc "Perhaps, but a lot of people don't care."
        "?maxine.lust>=5@[maxine.lust]/5|{image= maxine contact_icon}|{image= stats lust}|{space=-10}\"Well, I'm not undressing unless you do too.\"":
          show maxine concerned at move_to(.5)
          mc "Well, I'm not undressing unless you do too."
          maxine angry "I'm not the test subject."
          mc "Those are my terms for continuing."
          maxine neutral "Very well."
          $maxine.unequip("maxine_shirt")
          extend " Let's proceed."
          "Whoa! I never thought she'd undress..."
          "[maxine] is the type of girl that seems to have no sex drive, so this is surprising."
          maxine smile "Please, remove your shirt."
          mc "Uh, okay..."
          mc "..."
          mc "There."
          maxine excited "Okay, just as I thought..."
          mc "What?"
          maxine blush "Nothing. The experiment is over."
          mc "And what did you conclude?"
          maxine flirty "That's confidential information."
          mc "Oh, yeah?"
          $maxine.equip("maxine_shirt")
          maxine flirty "Yes."
          mc "Well, for what it's worth, my test also had a positive outcome."
          $maxine.lust+=1
          maxine excited "I'm glad to hear that."
          "It's hard to say if she understood that compliment..."
    "\"Is this another test?\"" if quest.maxine_lines["borrowed"]: ## If the player chose "What about the saucer-eyed doll?" in phase_4_reward:
      show maxine skeptical at move_to(.5)
      $mc.intellect+=1
      mc "Is this another test?"
      maxine skeptical "It might be. What's your answer?"
      mc "I don't consent to being your guinea pig this time."
      maxine thinking "Why not?"
      mc "Because I have better things to do!"
      maxine sad "According to your file, you don't."
      maxine thinking "Are you suggesting your file is incomprehensive?"
      mc "Now, wait just a minute... why do you have a file on me?"
      maxine eyeroll "How else am I supposed to conduct experiments?"
      mc "You're not! I don't like it!"
      maxine annoyed "That's not a very strong argument."
      maxine annoyed "Science takes precedence over your feelings."
      mc "You're not even a scientist!"
      maxine eyeroll "And yet I'm able to conclude that you failed the test."
      mc "Fine. I failed the test, but I didn't steal anything!"
      maxine angry "Why didn't you just say so?"
      mc "..."
      maxine angry "I don't know why you have to be so difficult, [mc]."
      "She must be trolling. There's no way she genuinely thinks I'm the difficult one here."
      mc "You know what—"
      maxine smile "Yes."
      maxine smile "I do know."
      mc "I'm sure you do..."
    "\"You did say that.\"" if not quest.maxine_lines["borrowed"]: ## If the player chose "You know the answer to that." OR "What about [lindsey]?" in phase_4_reward:
      show maxine skeptical at move_to(.5)
      mc "You did say that."
      mc "I believe your exact words were: \"That's a priceless artifact. Hands off.\""
      maxine angry "I have a hard time seeing how that equals permission."
      mc "I took it as an invitation."
      mc "I trust you to do the same if I ever tried to hide something from you."
      $maxine.lust+=1
      maxine smile "I do like that sentiment."
    "\"When a mystery needs uncovering, the rules no longer apply.\"" if not quest.maxine_lines["borrowed"]: ## If the player chose "You know the answer to that." OR "What about [lindsey]?" in phase_4_reward:
      show maxine skeptical at move_to(.5)
      mc "When a mystery needs uncovering, the rules no longer apply."
      $maxine.love+=1
      maxine excited "Very well! That's hard to argue with."
      mc "I knew you'd understand."
    "\"You said you found it in the basement, so it's not like it's really yours anyway...\"" if not quest.maxine_lines["borrowed"]: ## If the player chose "You know the answer to that." OR "What about [lindsey]?" in phase_4_reward:
      show maxine skeptical at move_to(.5)
      mc "You said you found it in the basement, so it's not like it's really yours anyway..."
      maxine angry "I claimed it as my own."
      mc "I'll let you have it back if you tell me where the basement is."
      maxine smile "Under the school."
      mc "Well, I'll just hold on to it until you stop with those deadpan answers..."
      maxine neutral "I don't know what you mean."
  mc "Anyway, would you like to know what I found?"
  maxine thinking "Is it forbidden or dangerous knowledge?"
  mc "I... I'm not entirely sure."
  maxine sad "Okay..."
  mc "Should I tell you?"
  maxine thinking "I believe that would be your best bet."
  mc "My best bet at what, exactly?"
  maxine thinking "Survival."
  mc "Ah, yes. The made up lines and the cheap plastic toy... I'm shaking in my boots."
  maxine concerned "I understand that you're scared."
  maxine concerned "There are things no one should know about. Secrets best left forgotten."
  maxine concerned "Let's hope you didn't stumble upon one of those."
  "[maxine] clearly didn't get the sarcasm..."
  mc "Kind of like my hidden stash of unwashed cum-rags?"
  maxine eyeroll "This best not be taken lightly."
  mc "Okay, so let me tell you."
  maxine confident "Very well."
  mc "I found three circles connected by lines."
  mc "I triangulated those places and then I got an error saying that the signal is too weak."
  maxine thinking "Intriguing."
  mc "Do you have any idea what to do?"
  maxine thinking "Yes."
  mc "Well?"
  maxine sad "..."
  mc "Are you going to tell me what that is?"
  maxine neutral "Yes."
  mc "By all means, don't let me stop you..."
  maxine neutral "By my estimation, it seems like you have discovered a number of ley line nodes."
  mc "Ley line nodes? What does that mean?"
  maxine neutral "There are always four nodes."
  mc "But I only found three circles..."
  maxine smile "If you manage to enhance the signal, the locator device should lead you to the final node."
  mc "And how do I do that?"
  maxine eyeroll "With magnets, of course — you're dealing with a magnetic field."
  "Duh! Why didn't I think of that?"
  "I can't believe she made that sound like it's obvious..."
  "I also can't believe I'm getting dragged into this stupid merry-go-round."
  mc "How do you know all these things?"
  maxine annoyed "It's basic ley line mysticism."
  maxine annoyed "Now, if you'll excuse me, I have articles to write."
  hide maxine with Dissolve(.5)
  "This all seems like a total waste of time, but I might as well finish what I've started..."
  "Where the hell do I find magnets?"
  $quest.maxine_lines.advance("magnetic")
  return

label quest_maxine_lines_unscrew:
  show jacklyn neutral with Dissolve(.5)
  "Jacklyn has the longest and deadliest nails in the school."
  #"She also doesn't care much for rules or the law." ## Removed
  mc "Hey, I need your help with something..."
  jacklyn smile "You sure do, bro, but I can't help you in that department."
  mc "Huh?"
  jacklyn smile_right "Girl problems, yeah? I wouldn't know how to fix them, seeing as I don't dabble."
  mc "Oh, err... actually, this is a different kind of problem."
  mc "I need to borrow your hand..."
  jacklyn laughing "Straight to the point, eh?"
  show jacklyn laughing at move_to(.25)
  menu(side="right"):
    extend ""
    "\"No! That's not what I meant!\"":
      show jacklyn laughing at move_to(.5)
      mc "No! That's not what I meant!"
      jacklyn excited "I'm just pulling your speedo!"
      jacklyn excited "What do you need?"
      mc "This is going to sound weird, but just follow me, okay?"
      jacklyn laughing "A blindfold adventure? Lead the way, chief."
    "?jacklyn.lust>=5@[jacklyn.lust]/5|{image= jacklyn contact_icon}|{image=stats lust_3}|\"I'm game if you are.\"":
      $quest.maxine_lines["hand"] = True
      show jacklyn laughing at move_to(.5)
      mc "I'm game if you are."
      jacklyn excited "All right, shooter. Hands up, pants down!"
      mc "Actually, I have a better idea."
      jacklyn excited "Yeah?"
      mc "Follow me."
      jacklyn laughing "I like the spontaneity. Lead on."
  $quest.maxine_lines.advance("helpinghand")
  hide jacklyn with Dissolve(.5)
  return

label quest_maxine_lines_helpinghand:
  show jacklyn neutral with Dissolve(.5)
  mc "Okay, close your eyes and give me your hand."
  jacklyn smile "I'm a sucker for adventures. Don't let me down."
  window hide
  show black
  hide jacklyn
  with Dissolve(.5)
  $game.location = "school_secret_locker"
  pause 1.0
  hide black with Dissolve(.5)
  window auto
  mc "Okay, this is going to feel a bit weird..."
  jacklyn "Just don't put my hand up your stinky."
  $school_secret_locker["hand"] = 1
  jacklyn "..."
  jacklyn "What exactly are you doing?"
  mc "Just some quick unscrewing."
  jacklyn "Why?"
  if quest.maxine_lines["hand"]: ## If the player chose "I'm game if you are." in phase_10
    mc "I... err, I'm very into construction work in a public setting."
    mc "It really gets me going."
    jacklyn "Well, if that's what inflates your drumstick..."
    mc "It absolutely is. Hold still, please."
    window hide
    show black with Dissolve(.5)
    $school_secret_locker["hand"] = 0
    pause 1.0
    $set_dialog_mode("default_no_bg")
    "There we go! All screws unscrewed."
    "Now for the real party..."
    "..."
    "Never thought I'd unzip in the entrance hall..."
    show jacklyn handjob_base with Dissolve(.5): ## This gives the image both the dissolve (transition) and vpunch (transform) effects
      block:
        linear 0.05 yoffset 15
        linear 0.05 yoffset -15
        repeat 10
      linear 0.05 yoffset 0
    hide black
    hide jacklyn
    show jacklyn handjob_base ## This is just to avoid the screen shake if the player is skipping
    window auto
    $set_dialog_mode("")
    jacklyn handjob_base "Oh, there he is!"
    mc "Hnng!"
    "Fuck! Her grip around my dick is tight!"
    "It's like she's trying to choke me out!"
    "But goddamn if it's not a great feeling..."
    jacklyn handjob_base "Ready for the claw machine?"
    "What the fuck? I don't like the sound of that."
    mc "Please be gentle..."
    jacklyn handjob_base "Stealing the virgin girl's line, eh?"
    jacklyn handjob_base "How's this?"
    show jacklyn handjob_middle with Dissolve(.1) ## 1
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    pause 0.1
    show jacklyn handjob_middle with Dissolve(.1) ## 2
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    pause 0.05
    show jacklyn handjob_middle with Dissolve(.1) ## 3
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    mc "Fuck!"
    jacklyn handjob_base "Not too bad, right?"
    mc "K-keep going!"
    show jacklyn handjob_middle with Dissolve(.15) ## 1
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.05
    show jacklyn handjob_middle with Dissolve(.15) ## 2
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.1
    show jacklyn handjob_middle with Dissolve(.15) ## 3
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.05
    show jacklyn handjob_middle with Dissolve(.15) ## 4
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.15
    show jacklyn handjob_middle with Dissolve(.15) ## 5
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    jacklyn handjob_base "How's that?"
    "Fuck! It feels like she's pulling my dick out an extra couple of inches!"
    "Never thought I'd like a rough hand job, but damn..."
    "[jacklyn]'s definitely got the grip and cruel-looking nails for it."
    show jacklyn handjob_middle with Dissolve(.15) ## 1
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.1
    show jacklyn handjob_middle with Dissolve(.15) ## 2
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.05
    show jacklyn handjob_middle with Dissolve(.15) ## 3
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    pause 0.15
    show jacklyn handjob_middle with Dissolve(.15) ## 4
    show jacklyn handjob_top with Dissolve(.15)
    show jacklyn handjob_middle with Dissolve(.15)
    show jacklyn handjob_base with Dissolve(.15)
    "Uhhh! I can't believe I'm getting a handjob from the hot teacher's assistant in the fucking entrance hall!"
    "Both [jo] and [mrsl] could walk by at any moment! Not to mention all the other students!"
    show jacklyn handjob_middle with Dissolve(.1) ## 1
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    pause 0.15
    show jacklyn handjob_middle with Dissolve(.1) ## 2
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    jacklyn handjob_base "All right, I'm going to really put the squeeze on it now!"
    jacklyn handjob_base "I hope you're ready to fire..."
    show jacklyn handjob_middle with Dissolve(.1) ## 1
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1) ## 2
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1) ## 3
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1) ## 4
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1) ## 5
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_base with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1) ## 6
    show jacklyn handjob_top with Dissolve(.1)
    show jacklyn handjob_middle with Dissolve(.1)
    show jacklyn handjob_cum
    mc "Fuck me!!!" with vpunch
    jacklyn handjob_cum "There it is! The good old pirate cannon..."
    jacklyn handjob_cum "How'd you like it, big boy?"
    mc "Fantastic! Thank you!"
    jacklyn handjob_cum "I knew you were tougher than your average punk bitch."
    jacklyn handjob_cum "Now, if you'll excuse me... job's about to get pissy. Gotta bounce."
    $unlock_replay("jacklyn_helping_hand")
    window hide
    show black with Dissolve(.5)
    pause 1.0
    $quest.maxine_lines["lindsey_just_ran_by"] = True
    call goto_school_ground_floor
    window auto
    "Damn, [lindsey] just ran by! I barely managed to get it back in my pants..."
    $quest.maxine_lines["lindsey_just_ran_by"] = False
    "What a fucking experience."
  else:
    mc "It's for an art thing..."
    jacklyn "Oh, yeah?"
    mc "For... err, a sort of post modern, feminist satire thing?"
    jacklyn "Sounds badass."
    jacklyn "What's your message?"
    mc "{i}\"Women aren't tools!\"{/}"
    jacklyn "That rubs me in the right spot..."
    "Phew!"
    mc "I thought you'd like it."
    mc "Just a couple more shots..."
    window hide
    show black with Dissolve(.5)
    $school_secret_locker["hand"] = 0
    pause 1.0
    call goto_school_ground_floor
    window auto
    show jacklyn neutral with Dissolve(.5)
    mc "Thanks for being my hand model!"
    jacklyn smile "Anything for art, Bobby."
    jacklyn smile "Catch you on the flipside."
    hide jacklyn with Dissolve(.5)
  $school_secret_locker["panel"] = "off"
  $school_secret_locker["eyes"] = True
  $quest.maxine_lines.advance("doll")
  return

label quest_maxine_lines_seemaxine:
  show maxine sad with Dissolve(.5)
  "She appears to be deep in thought."
  "I sometimes wonder what's going on inside her head..."
  "For good or bad, [maxine] does have one of the most unique brains I've ever seen."
  mc "Excuse me?"
  maxine thinking "You're back."
  mc "Yes, and I found something..."
  maxine thinking "What did you discover?"
  mc "This doll..."
  maxine concerned "It's the saucer-eyed doll!"
  mc "You look concerned."
  maxine concerned "I didn't think you would find it..."
  mc "Why not?"
  maxine annoyed "I've been looking for it."
  maxine skeptical "Have you done the necessary precautions?"
  mc "Err... what?"
  maxine afraid "You brought it here without precautions?!"
  maxine afraid "What's wrong with you?!"
  mc "Sorry?"
  maxine cringe "Okay, give it here, quickly!"
  mc "Uh, okay..."
  $mc.remove_item("saucer_eyed_doll")
  maxine cringe "..."
  $school_clubroom["doll"] = True
  maxine thinking "Go find magnets! We need to neutralize its electromagnetic field!"
  if mc.owned_item("magnet",3):
    mc "I actually have these magnets on me..."
    maxine excited "Oh, okay! That's probably what kept you safe..."
    maxine excited "Form a circle around the doll."
  else:
    "Fine, let's keep indulging her delusions..."
    mc "I know where to find them."
    maxine afraid "Hurry! We need to form a circle around it!"
  $quest.maxine_lines.advance("prison")
  hide maxine with Dissolve(.5)
  return
