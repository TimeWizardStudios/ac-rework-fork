label quest_maxine_hook_start:
  show maxine concerned_binoculars with Dissolve(.5)
  mc "Hey, [maxine]! What are you doing?"
  maxine concerned_binoculars "I'm looking for the truth."
  mc "I know that, but..."
  mc "What are you up to right now?"
  maxine thinking "Talking to you, [mc]."
  maxine thinking "You really need to be more perceptive."
  mc "..."
  mc "What were you doing right before you started talking to me?"
  maxine sad "Taking care of business."
  mc "What business?"
  maxine skeptical "You ask an awful lot of questions."
  mc "I'm just curious..."
  maxine eyeroll "If you must know, I was looking for the narrative lens."
  mc "The what?"
  maxine skeptical "It's the window between worlds. Between fiction and reality."
  mc "And what does this lens look like?"
  maxine skeptical "It can be a camera, a canvas with an arrangement of colors, or even a person."
  mc "What? A person?"
  maxine smile "The world is a stage, [mc] — everyone is a player. You know that, right?"
  maxine neutral "But if everyone is on the stage, where is the audience?"
  mc "I..."
  maxine neutral "Now, if you'll excuse me—"
  mc "Wait!"
  maxine concerned "What?"
  mc "I need your help with something."
  maxine excited "Well, what is it?"
  mc "Do you believe in... {i}time travel{/}?"
  maxine thinking "No."
  mc "No?"
  mc "But you believe in all sorts of wacky shit!"
  maxine thinking "Time travel doesn't make any logical sense."
  mc "And a saucer-eyed doll that controls the magnetic field does?!"
  maxine smile "Of course."
  maxine smile "You can travel forward in time by going fast enough — but it's only relative to the people who don't move at that speed."
  maxine neutral "Backwards in time is simply impossible."
  mc "Well, you're wrong, because I did travel back in time."
  maxine laughing "And you're choosing to go to school despite finishing it once already?"
  "Man, she does have a point. What the hell am I doing here?"
  maxine sad "Unless..."
  mc "Unless?"
  maxine thinking "Unless school was a large contributing factor to why your life turned out so poorly."
  maxine thinking "Then you would want a redo."
  mc "How did you know that?"
  maxine skeptical "No offense, but you're not exactly someone with great prospects."
  "Ouch."
  maxine skeptical "Anyway, I have a meeting with a plant. Adieu."
  if maxine.location in (school_first_hall,school_ground_floor_west,school_cafeteria,school_gym):
    show maxine skeptical at disappear_to_right
  else:
    show maxine skeptical at disappear_to_left
  $maxine["at_none_today"] = True
  "I feel like I've made a difference since I returned, but maybe I'm destined for failure regardless of my actions?"
  "Ugh, talking to [maxine] always makes my brain tired. I should probably call it a day already..."
  $quest.maxine_hook.start()
  return

label quest_maxine_hook_wake_home_bedroom_door:
  "I'll just pretend I didn't see a butt and a pair of legs sticking out from under my bed..."
  jump goto_home_hall


label quest_maxine_hook_wake_home_bedroom_spinach:
  show maxine blush at Transform(xalign=.25)
  show spinach neutral at Transform(xalign=.75,yalign=1.0)
  with Dissolve(.5)
  maxine blush "[spinach], how are your captors treating you?"
  spinach closedeyes "Meow?"
  maxine sad flip "You are a hostage. It would be unethical for me to pet you."
  show spinach neutral at Transform(xalign=.75),bounce with Dissolve(.5)
  spinach neutral "Meow!"
  hide spinach
  show spinach neutral at Transform(xalign=.75,yalign=1.0)
  maxine thinking flip "Fine... a single petting, then I'll leave you to your fate."
  show spinach drugged at Transform(xalign=.75),bounce with Dissolve(.5)
  spinach drugged "Roar!"
  hide spinach
  show spinach drugged at Transform(xalign=.75,yalign=1.0)
  maxine skeptical "No, that is highly incorrect."
  maxine skeptical "My sources tell me that [mc] doesn't have any ties to a terrorist organization other than the IRS."
  hide maxine
  hide spinach
  with Dissolve(.5)
  return

label quest_maxine_hook_wake:
  mc "[maxine]...?"
  mc "What are you doing under my bed?"
  maxine "Investigating."
  mc "Investigating what?"
  maxine "Your claims of time travel, of course."
  maxine "But there only seems to be a crusty old sock, two empty batteries, and what appears to be a spider's nest here."
  mc "How did you even get in here?"
  show maxine excited with Dissolve(.5)
  maxine excited "The same way everyone else does."
  mc "I'm... not talking about how you were born..."
  maxine concerned "Neither am I."
  mc "How did you get into my room, [maxine]?"
  maxine thinking "Do not ask questions you don't want the answer to."
  mc "..."
  maxine thinking "..."
  mc "So... did you manage to draw any conclusions about my time traveling yet?"
  maxine skeptical "Yes, my conclusion is that you're lying."
  maxine annoyed "I could've been investigating the ghost of Pond Park. Yet here I am, wasting my time."
  show maxine annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'm not lying!\"":
      show maxine annoyed at move_to(.5)
      mc "I'm not lying!"
      maxine skeptical "Everybody lies. Especially you."
      mc "What's that supposed to mean?"
      maxine skeptical "It means that falsehoods are perpetuated by all humans, but you in particular."
      mc "Why me in particular?"
      maxine eyeroll "How am I supposed to know that?"
      mc "I thought you knew everything."
      maxine confident "I {i}see{/} everything. If I also knew everything, that would make me..."
      mc "A god?"
      maxine skeptical "A serious competition to search engines."
      mc "Great."
      maxine annoyed "It would be for you, since your questions seem to be endless."
      "God, she's annoying."
    "\"The ghost of Pond Park?\"":
      show maxine annoyed at move_to(.5)
      mc "The ghost of Pond Park?"
      maxine skeptical "Indeed. There has been a report of a haunting."
      mc "Who reported it?"
      maxine skeptical "You know very well that my sources—"
      mc "Yeah, I know, but are you sure they're not just messing with you?"
      maxine eyeroll "Why would they do that? A haunting is quite serious."
      mc "Well, what kind of hunting is it?"
      maxine annoyed "I would be able to tell you that already if you had half as many questions."
    "\"What would be the easiest way to get my dick inside you?\"":
      show maxine annoyed at move_to(.5)
      $mc.lust+=1
      mc "What would be the easiest way to get my dick inside you?"
      maxine skeptical "Orally. The other options involve removal of clothing or breaking of skin, which would certainly provide additional difficulty."
      mc "Do you want to try?"
      maxine skeptical "Why? This requires no further inquiry."
      "Oh, well... It was worth a shot."
  maxine thinking "For wasting my time, you now owe me some of yours."
  mc "That's not how it works."
  maxine thinking "That's exactly how it works."
  maxine thinking "I have a membership at the time council. I know their rules."
  mc "That is not a real thing."
  maxine confident_sock "Then what do you call this?"
  mc "It's a regular sock!"
  maxine annoyed_sock "That's what a time thief would think."
  maxine annoyed_sock "This is a time pouch."
  mc "Right..."
  maxine confident_sock "Anyway, see you in the computer classroom."
  mc "But I didn't agree to—"
  show maxine confident_sock at disappear_to_left
  pause 0.4
  $quest.maxine_hook.advance("computer")
  "Aaaand she's gone. Great."
  $school_computer_room["unlocked"] = True
  return

label quest_maxine_hook_computer:
  show maxine smile with Dissolve(.5)
  maxine smile "I knew you'd show up eventually."
  mc "I got bored."
  maxine smile "An idle mind is creativity's playground."
  mc "I'm pretty sure that's not how the saying goes..."
  maxine neutral "What saying?"
  mc "Forget it."
  mc "What are you up to, anyway?"
  maxine excited "I'm working on a project."
  mc "What kind of project?"
  maxine excited "I'm trying to connect to cyberspace."
  mc "Is the router acting up again?"
  maxine concerned "I hope not."
  mc "So, why can't you connect?"
  maxine thinking "I don't have the right tools."
  mc "Have you tried using a computer?"
  maxine afraid "I don't trust computers."
  mc "Well, then it might be hard..."
  maxine smile "Hard, but not impossible."
  mc "What are you even going to connect if not a computer?"
  maxine neutral "Myself, of course."
  mc "Of course."
  maxine neutral "For this to work, we're going to need a laboratory power supply."
  mc "That's it? Can't you get one from the science classroom?"
  maxine skeptical "No, I can't. They're all gone."
  maxine skeptical "Besides, you still owe me some of your time — you'll pay me back by getting me the equipment I need."
  mc "How about no?"
  maxine annoyed "I'll be expecting one from you soon."
  mc "Don't hold your breath."
  maxine confident "I never do. Except while diving."
  show maxine confident at disappear_to_right
  "I have better things to do than playing [maxine]'s silly games."
  if lindsey["romance_disabled"]:
    "Like... err..."
    "Okay, maybe I don't have anything better to do..."
    "But talking to her is exhausting."
    "I could really use a cold glass of strawberry juice and some me-time.{space=-15}"
    $quest.maxine_hook.advance("juice")
  else:
    "Like flirting with [lindsey]!"
    "I wonder what she's up to..."
    $quest.maxine_hook.advance("lindsey")
  return

label quest_maxine_hook_lindsey:
  show lindsey smile with Dissolve(.5)
  lindsey smile "Hey, [mc]!"
  mc "Hello! What are you up to?"
  lindsey laughing "Oh, you know..."
  "She's laughing, but it feels forced."
  show lindsey laughing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You look like a princess today.\"":
      show lindsey laughing at move_to(.5)
      mc "You look like a princess today."
      $lindsey.lust+=1
      lindsey flirty "Aw, thanks! I needed that."
      mc "Are you okay?"
      lindsey smile "It's just so much right now with everything. I feel a bit overwhelmed..."
      mc "Anything I can do to help?"
      lindsey laughing "Thanks, but you already did!"
    "\"Do you know the difference between a car and a jar?\"":
      show lindsey laughing at move_to(.5)
      mc "Do you know the difference between a car and a jar?"
      lindsey smile "I mean, there's a lot of differences..."
      mc "Kind of like you and me."
      lindsey flirty "Oh, yeah? Which one am I, the car or the jar?"
      mc "It depends — do you like to be ridden or filled up?"
      $lindsey.love-=1
      $lindsey.lust-=1
      lindsey skeptical "Excuse me?"
      "Oof, she really didn't like that."
      mc "It was a stupid joke, forget it."
    "\"[maxine] is trying to hook herself up to the internet.\"":
      show lindsey laughing at move_to(.5)
      mc "[maxine] is trying to hook herself up to the internet."
      mc "And before you ask — yes. Not her computer, herself."
      lindsey concerned "What? Why?"
      mc "I thought you might know."
      lindsey concerned "She was never a big fan of electronics..."
      lindsey sad "But it could be any reason at all. She's not the most stable person anymore."
  lindsey thinking "Anyway, would you mind doing me a favor?"
  mc "What kind of favor?"
  lindsey cringe "I forgot my water bottle in the gym, and I'm late for class, and I would really—"
  show lindsey cringe at move_to(.25)
  show maxine angry at appear_from_right(.75)
  maxine angry "[lindsey]! You were supposed to drop off the watermelon in my office an hour ago!"
  lindsey laughing "Oh, right! I forgot about that... I've had things on my mind."
  maxine neutral "I'll need it soon. Please, don't forget it again."
  lindsey smile "Fine, I'll bring it after class!"
  lindsey smile "See you later, [mc]."
  show maxine neutral at move_to(.5)
  hide lindsey smile with Dissolve(.5)
  maxine neutral "I'm also going to need that power supply soon."
  mc "I'm not getting it for you."
  maxine smile "We'll see about that."
  show maxine smile at disappear_to_right
  "Girls are so annoying! They always want something from you, and it's never sex."
  "I could use a moment to myself. Just to stretch out in the grass with the chirp of forest birds around me."
  "Perhaps with a cold drink as well... Mmm, strawberry juice..."
  "Yeah, that's what I need right now."
  $quest.maxine_hook.advance("juice")
  return

label quest_maxine_hook_juice:
  $mc.money-=1
  $mc.add_item(school_cafeteria["drink_choice"])
  "There we go! The red gold."
  "Okay, time to find a quiet place outside to enjoy a few moments of peace..."
  $quest.maxine_hook.advance("glade")
  return

label quest_maxine_hook_glade_upon_entering:
  "Yeah, this is the spot."
  "This is where strawberry juice and chill is the apex of activities."
  return

label quest_maxine_hook_glade(item):
  python:
    x = mc.owned_item(("strawberry_juice","spray_strawberry_juice"))
    y = mc.owned_item(("strawberry_juice","spray_strawberry_juice")).replace("strawberry_juice","")+"empty_bottle"
    mc.remove_item(x)
    mc.add_item(y)
  if mc.at("school_forest_glade"):
    "Oh, hell yes! Mouthgasm!"
    "Man, sometimes you just need to get away from it all."
    "All the favors and nagging. All the pressure and expectations."
    "Not that anyone expects much from me..."
    play sound "twig_snapping"
    pause 1.0
    "Huh? What the hell was that?"
    "The sound came from behind those trees over there..."
    mc "Hello?"
    isabelle "[mc]?"
    show isabelle neutral at appear_from_right
    mc "[isabelle]? What are you doing out here?"
    isabelle excited "Just taking a stroll to clear my head."
    isabelle concerned "What's wrong?"
    mc "I could've sworn I heard something..."
    isabelle neutral "The forest is full of little animals."
    show isabelle neutral at move_to(.75)
    menu(side="left"):
      extend ""
      "\"Yeah, but little animals don't snap twigs...\"":
        show isabelle neutral at move_to(.5)
        mc "Yeah, but little animals don't snap twigs..."
        isabelle confident "Maybe it was Bigfoot?"
        mc "I know you're joking, but who knows what lives within this forest?"
        isabelle laughing "I think [maxine] is rubbing off on you!"
        mc "Ugh, you're right..."
      "\"Are you okay?\"":
        show isabelle neutral at move_to(.5)
        mc "Are you okay?"
        isabelle neutral "For the most part. Wandering through the forest is therapeutic to me.{space=-35}"
        mc "I thought I'd do the same, only I got drunk on strawberry juice and lost my way."
        isabelle laughing "There are worse things to drink, and worse places to get lost!"
        mc "You're cheery today."
        isabelle confident "It's a nice day. The sun is shining, the birds are singing..."
        isabelle confident "It doesn't take much to make me happy."
        "I wish it were that easy for me..."
      "Change the topic":
        show isabelle neutral at move_to(.5)
        mc "I snuck into the school the other night."
        isabelle concerned "Really? How come?"
        mc "I was trying to snap a picture for [maxine]."
        isabelle concerned "And how did that go?"
        mc "I mean, you've seen the front page, right?"
        isabelle confident "Oh, those glowing green things? You took that photo?"
        isabelle laughing "That's neat! What were those things anyway?"
        mc "Just what the headline said — glowing spider eggs."
        isabelle confident "Okay, fine... at least you pranksters are making it an interesting read.{space=-15}"
    play sound "twig_snapping"
    pause 1.0
    mc "Huh? Please tell me you heard that."
    isabelle concerned "I did."
    isabelle concerned_left "Hello? Anyone out there?"
    play sound "twig_snapping"
    pause 1.0
    show maxine sad_map flip at appear_from_left(.25)
    show isabelle concerned_left at move_to(.75,1.0)
    maxine sad_map flip "Two hundred and twenty-eight..."
    show isabelle concerned at move_to(.25,1.0)
    show maxine sad_map flip at move_to(.75,1.0)
    maxine sad_map flip "Two hundred and twenty-nine..."
    show isabelle concerned at move_to(.5,1.0)
    show maxine sad_map flip at disappear_to_right
    mc "[maxine]?"
    if quest.isabelle_locker["time_for_a_change"]:
      isabelle skeptical "Was she spying on us?"
      mc "I don't think so."
      isabelle concerned "It's weird creeping around the woods..."
    else:
      isabelle concerned "I wonder what she's doing..."
    maxine "Two hundred and thirty..."
    mc "She seems lost in her own world."
    show isabelle concerned at move_to(.25,1.0)
    show maxine thinking_map at appear_from_right(.75)
    maxine thinking_map "I'm not lost. In fact, I'm the opposite of lost."
    maxine thinking_map "A map tends to prevent that."
    maxine sad_map "Two hundred and thirty-one..."
    show maxine sad_map at move_to(.25,1.0)
    show isabelle concerned_left at move_to(.75,1.0)
    maxine sad_map "Two hundred and thirty-two..."
    maxine excited "Here we are!"
    mc "Here we are?"
    maxine excited "At the X."
    isabelle excited "Oh, is it like an old treasure map?"
    maxine concerned "No, it's not {i}like{/} an old treasure map. It {i}is{/} one."
    if quest.isabelle_locker["time_for_a_change"]:
      isabelle skeptical "Right, because you correcting me in a really bitchy manner is going to turn out well."
      maxine concerned "It was a perfectly normal correction. You're welcome."
      isabelle skeptical "And you presume to be the authority on normalcy? That strikes me as laughably ironic."
      maxine concerned "There appears to be a disconnect between your emotional response and the topic at hand."
      maxine flirty "Unless, of course, treasure maps excite you in an unusual way."
      maxine flirty "In which case investing in a peg leg and parrot companion might be the cure."
      isabelle skeptical "I have no idea what you're saying, but I don't particularly care either."
      isabelle neutral "Bye, [mc]."
      show maxine flirty at move_to(.5,1.0)
      show isabelle neutral at disappear_to_right
      pause 0.0
      maxine excited "Ahoy, matey! Smooth sailing to yer!"
      "I'm not sure what I just witnessed, but I guess that was [maxine] wrecking [isabelle] in an argument..."
    else:
      isabelle neutral "Sorry, that's what I meant."
      isabelle neutral "Anyway, I should get going. See you around!"
      window hide
      show maxine concerned at move_to(.5,1.0)
      show isabelle neutral at disappear_to_right
      pause 1.0
      window auto
    maxine concerned "Okay, it appears we'll need to dig... I should've brought a shovel."
    if mc.owned_item("locked_box_forest"):
      "Huh, I wonder if that map actually points to the strange box I uncovered with [isabelle]..."
    else:
      "There's no way that map is legit..."
    mc "Where did you find that map?"
    maxine skeptical "In the library."
    mc "But the library is closed."
    maxine skeptical "One door is closed."
    mc "There's only one door..."
    maxine eyeroll "That is an incorrect statement."
    maxine confident "Now, if you'll excuse me, I have a treasure to dig up."
    if mc.owned_item("locked_box_forest"):
      mc "Actually, I may have found your treasure already."
      maxine thinking "How? This map is unique."
      mc "Well, long story short, I found it while burying a chocolate box for [isabelle]."
      "Oh, god... without context, that sounds exactly like something [maxine] would say."
      maxine thinking "What was it?"
      mc "Some old box..."
      mc "Wait, are you saying you don't know what you're looking for?"
      maxine smile "Yes."
      maxine neutral "What was in the box?"
      mc "I haven't actually found a way to open it yet..."
      maxine neutral "Can I have it?"
      show maxine neutral at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I don't think so.\"":
          show maxine neutral at move_to(.5)
          mc "I don't think so."
          maxine angry "But I have the map!"
          mc "And? You weren't fast enough."
          $maxine.lust-=3
          $maxine.love-=3
          maxine thinking "..."
          maxine thinking "Fine."
          mc "Fine? Just like that?"
          maxine sad "By all means — if you want to mess around with dangerous artifacts, I'm not going to stop you."
          "Her ridiculous threats won't faze me."
          "I'm out of here."
        "\"Sure, I guess.\"":
          show maxine neutral at move_to(.5)
          mc "Sure, I guess."
          mc "I'm just curious to find out what's in it..."
          $mc.remove_item("locked_box_forest")
          $maxine.lust+=3
          maxine smile "Thank you. As am I."
          mc "So..."
          maxine smile "Yes?"
          mc "I just gave you a mysterious box..."
          maxine smile "Indeed."
          mc "..."
          mc "Aren't you going to open it?"
          maxine smile "I am."
          mc "..."
          mc "When?"
          maxine thinking "At an undermined time in the future."
          mc "Will you... let me know when that is?"
          maxine sad "Unlikely."
          mc "I demand to know what's in it!"
          maxine thinking "It's an ancient artifact."
          mc "I want to be there when you open it."
          maxine concerned "Are you sure about that?"
          mc "Yes!"
          maxine excited "Very well."
          maxine excited "Now, if you'll excuse me..."
          window hide
          show maxine excited at disappear_to_left
          pause 0.5
          window auto
          mc "That's not the way back to the school!"
          "Whatever. I'm sure she'll realize it sooner or later."
      $quest.maxine_hook.advance("daybefore")
      jump goto_school_entrance
    else:
      mc "How are you going to dig without a shovel?"
      maxine concerned "I'm not. I'll definitely need a shovel."
      mc "Don't look at me..."
      maxine excited "It's too late for that."
      mc "I'm not going to run errands for you."
      maxine flirty "Yes, you will. Bring me that shovel."
      $maxine["at_none_now"] = maxine["at_none_today"] = False
      $mc["focus"] = "maxine_hook"
      $quest.maxine_hook.advance("shovel")
      hide maxine with Dissolve(.5)
  else:
    mc "Ah, the lifeblood courses through my veins once more!"
    $quest.maxine_hook.advance("juice")
  return

label quest_maxine_hook_shovel_home_bedroom_bed:
  menu(side="middle"):
    "What time is it?"
    "Sleeping time":
      "My work here is done."
      window hide
      show black with Dissolve(3.0)
      pause 0.5
      $set_dialog_mode("default_no_bg")
      "..."
      "Why can't I get that stupid shovel out of my head?"
      "How come I can sleep through the thunder of trains, but fail to get this out of my head?"
      "Fuck it."
      hide black with Dissolve(.5)
      $set_dialog_mode("")
      window auto
    "Brooding time":
      "Rest is for the dead."
      "And those who are afraid of energy drinks."
  return

label quest_maxine_hook_shovel:
  show maxine excited with Dissolve(.5)
  maxine excited "Aha! See, I was right!"
  mc "I just got bored. I didn't find anything else to do."
  mc "And I couldn't sleep either..."
  mc "What did you do to me?"
  maxine concerned "Nothing. Maybe you just want to help me?"
  mc "I don't think so, but here's your damn shovel..."
  $mc.remove_item("shovel")
  maxine blush "Perfect! Now dig for me."
  $mc.add_item("shovel")
  mc  "I'm not going to dig for you!"
  maxine flirty "We'll see about that."
  hide maxine with Dissolve(.5)
  $quest.maxine_hook.advance("dig")
  return

label quest_maxine_hook_no_shovel:
  show maxine annoyed with Dissolve(.5)
  maxine annoyed "Still waiting for that shovel."
  maxine annoyed "I can't do earth-surgery with my hands."
  hide maxine with Dissolve(.5)
  return

label quest_maxine_hook_dig_school_forest_glade_road_left:
  call goto_school_forest_glade
  if quest.maxine_hook["forest_glade_exit_count"] == 0:
    "What the hell? I swear I took the road back to the school yard..."
  elif quest.maxine_hook["forest_glade_exit_count"] == 1:
    "No way..."
    "It's like I'm walking in circles..."
  elif quest.maxine_hook["forest_glade_exit_count"] == 2:
    "Fuck!"
    "Am I drunk or something? What the hell was in that soup?!"
    "There is no way I'm this fucking lost..."
  elif quest.maxine_hook["forest_glade_exit_count"] == 3:
    "Okay, I've lost count of the times I've ended up where I started..."
    "Something must be seriously wrong with me."
  $quest.maxine_hook["forest_glade_exit_count"]+=1
  return

label quest_maxine_hook_dig_school_forest_glade_road_left_repeat:
  call goto_school_forest_glade
  if quest.maxine_hook["forest_glade_exit_count"] == 4:
    "At this point, I should've found my way home..."
    "Or at the very least found some forest nymphs..."
    "Yet there's no one here but [maxine]. Fantastic."
    $achievement.the_only_nymph.unlock()
  else:
    "Okay, I've lost count of the times I've ended up where I started..."
    "Something must be seriously wrong with me."
  $quest.maxine_hook["forest_glade_exit_count"]+=1
  return

label quest_maxine_hook_dig:
  show maxine smile with Dissolve(.5)
  maxine smile "Are you lost?"
  show maxine smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"No, I'm not.\"":
      show maxine smile at move_to(.5)
      mc "No, I'm not."
      mc "I just had to turn around, that's all."
    "\"Yes...\"":
      show maxine smile at move_to(.5)
      mc "Yes..."
      maxine laughing "It's a big forest. Lots of paths."
      maxine smile "Luckily, I have a map that I can retrace back to the school."
      maxine smile "I'll help you if you help me."
      mc "Why can't you dig yourself?"
      maxine sad "Carpal tunnel."
      maxine cringe "It's a common ailment among writers and journalists."
      mc "Fine... I guess I'll dig."
  hide maxine with Dissolve(.5)
  return

label quest_maxine_hook_dig_on_advance:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "Great. It's already getting dark..."
  "And to the surprise of absolutely no one, [maxine] doesn't seem to care."
  return

label quest_maxine_hook_dig_use_item(item):
  if item == "shovel":
    show black with Dissolve(.5)
    $set_dialog_mode("default_no_bg")
    "Like a slave in a coal mine."
    "Hacking away at the unending soil."
    "My body and spirit crushed by greed and—"
    play sound "falling_thud"
    $quest.isabelle_buried["hole_state"] = 2
    with vpunch
    "Huh?"
    hide black with Dissolve(.5)
    window hide
    window auto
    $set_dialog_mode("")
    mc "No way! There's actually something buried here..."
    show maxine eyeroll with Dissolve(.5)
    maxine eyeroll "Of course there is. The map wouldn't lie."
    mc "Well, what is it?"
    maxine skeptical "It appears to be a box of some sort."
    mc "Wait, you don't know what we're digging up?"
    maxine skeptical "I will soon."
    mc "What if it's nuclear waste or something?"
    maxine annoyed "It's a parchment map..."
    mc "Fair enough, I guess."
    maxine confident "Thanks for the help."
    mc "Not so fast!"
    show maxine confident at move_to(.25)
    menu(side="right"):
      extend ""
      "\"Don't you need help carrying it?\"":
        show maxine confident at move_to(.5)
        mc "Don't you need help carrying it?"
        maxine excited "I have carpal tunnel, not carpal arms!"
        $maxine.love+=1
        maxine flirty "Thanks for the offer, though."
        mc "All right... show me the way back now."
        maxine flirty "Follow me."
        window hide
        pause 0.25
        $quest.isabelle_buried["hole_state"] = 1
        pause 0.5
        show maxine flirty at disappear_to_left
        pause 0.75
        $mc["focus"] = ""
        show black with Dissolve(.07)
        pause 1.0
        call goto_school_entrance
        window auto
        if 19 > game.hour > 6:
          "Puh... I'm not made for hikes through the woods..."
          "I lost [maxine] somewhere along the way, but at least I managed to return to civilization."
          $quest.maxine_hook.advance("daybefore")
        else:
          "Man, I'm exhausted... I'm not made for nightly escapades in the woods..."
          "[maxine] disappeared somewhere along the way. I lost her in the darkness."
          "Knowing her, she just took a shortcut and forgot to look behind her.{space=-5}"
          $quest.maxine_hook["nightly_escapades"] = True
          $quest.maxine_hook.advance("night")
      "?mc.strength>=7@[mc.strength]/7|{image=stats str}|\"I think I'll just take the box.\"":
        show maxine confident at move_to(.5)
        mc "I think I'll just take the box."
        maxine angry "That's thievery!"
        mc "It's not your box..."
        maxine angry "But I found the way to it."
        mc "Well, you're a tiny girl with sticks for arms — what are you going to do about it?"
        $maxine.love-=3
        $maxine.lust-=3
        maxine neutral "Very well."
        mc "That's it?"
        maxine sad "Nothing I can do to stop you."
        maxine cringe "Hopefully, there's nothing dangerous inside."
        mc "Is that supposed to scare me? There's probably jewels and gold inside."
        $quest.isabelle_buried["hole_state"] = 1
        $mc.add_item("locked_box_forest")
        maxine thinking "..."
        show maxine thinking at disappear_to_left(.5)
        "Crap, she's running away! Better follow her so I don't get lost again!"
        window hide
        $mc["focus"] = ""
        show black with Dissolve(.07)
        pause 1.0
        call goto_school_entrance
        window auto
        if 19 > game.hour > 6:
          "Well, that was interesting..."
          "[maxine] somehow managed to run away. At least I managed to find my way back."
          $quest.maxine_hook.advance("daybefore")
        else:
          "Man, I'm exhausted... I'm not made for nightly escapades in the woods..."
          "[maxine] disappeared somewhere along the way. I lost her in the darkness."
          "Knowing her, she just took a shortcut..."
          $quest.maxine_hook["nightly_escapades"] = True
          $quest.maxine_hook.advance("night")
  else:
    "It's hard to look crazy next to [maxine], but digging with my [item.title_lower] would certainly do it."
    $quest.maxine_hook.failed_item("car_tire",item)
  return

label quest_maxine_hook_night_school_entrance_path:
  "It's too late to explore the forest."
  "You could run into a serial killer, or worse — a rabid beaver."
  return

label quest_maxine_hook_night_school_entrance_right_path:
  "It's too late to explore the park."
  "You could run into a serial killer, or worse — a rabid beaver."
  return

label quest_maxine_hook_night_school_entrance_door:
  "Even though someone left some lights on, this door is locked for the day."
  if not ria["talked"]:
    menu(side="middle"):
      extend ""
      "Knock":
        "Maybe [jo] is working late and can give me a ride home?"
        "Public transportation has always been my least—"
        window hide
        play sound "<from 0.4>bedroom_door"
        pause 0.5
        show ria neutral with Dissolve(.5)
        window auto
        ria neutral "[mc]? What are you doing here so late?"
        "Oh..."
        "I'd forgotten how pretty the janitor is."
        "It's like that crush I had on her has been loaded right back into my active memory."
        ria smile "You do know that nothing good happens after midnight, right?"
        mc "Err... yes?"
        "Unless you count watching porn while everyone is asleep."
        ria smile "Go home and get some sleep. A rested head is a healthy head."
        mc "I will! Thanks! Bye!"
        "God, why am I always so awkward?!"
        ria smile "Goodnight, [mc]."
        hide ria with Dissolve(.5)
        $achievement.an_early_goodnight.unlock()
      "Leave":
        pass
  return

label quest_maxine_hook_day:
  show guard suspicious at appear_from_right
  guard suspicious "All students must go to the cafeteria."
  mc "Why?"
  guard suspicious "Don't ask questions. Just go to the cafeteria."
  show guard suspicious at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You'll have to drag me there.\"":
      show guard suspicious at move_to(.5)
      $mc.strength+=1
      mc "You'll have to drag me there."
      guard angry "That can certainly be arranged."
      window hide
      show black onlayer screens zorder 100 with vpunch
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      mc "Ouch! Careful, that's my masturbation hand!"
      guard "Don't mess with me, kid."
      $game.location = "school_cafeteria"
      $mc["focus"] = "maxine_hook"
      $mc.energy = 100
      hide guard
      show black onlayer screens zorder 100
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      "Son of a bitch! That guy has no chill..."
    "\"Fine. Whatever.\"":
      show guard suspicious at move_to(.5)
      mc "Fine. Whatever."
      guard suspicious "Don't get mouthy with me."
      $mc.intellect+=1
      mc "I didn't plan to."
      $mc.energy = 100
      scene black
      show black onlayer screens zorder 100
      with Dissolve(.07)
      $game.location = "school_cafeteria"
      $mc["focus"] = "maxine_hook"
      $renpy.pause(0.25)
      $renpy.get_registered_image("location").reset_scene()
      scene location
      hide black onlayer screens
      with Dissolve(.5)
  "..."
  "What the hell is going on here?"
  window hide
  show jo thinking flip at appear_from_right(.25)
  pause 1.0
  show jo thinking at move_to(.75,1.0)
  pause 1.0
  show jo thinking flip at move_to(.25,1.0)
  pause 1.0
  show jo thinking at move_to(.5,1.0)
  window auto
  mc "What's going on?"
  jo worried "There's been a break-in."
  jo worried "Some equipment was stolen from the science classroom."
  mc "That's not good, but why are we all in here?"
  jo thinking "I'm afraid we have to interview all the students."
  mc "Isn't that the job of the police?"
  jo worried "We don't want to involve the authorities unnecessarily."
  jo worried "Hopefully, it's just some prank..."
  jo sad "It'll be your turn soon. Just sit tight, okay?"
  jo sad "I'll be outside with the [guard]."
  show jo sad at disappear_to_right
  pause 1.0
  $quest.maxine_hook.advance("interrogation")
  return

label quest_maxine_hook_interrogation_school_cafeteria_exit:
  "With the [guard] blocking the exit, there's zero chance of escaping."
  "Except by running, because he wouldn't be able to keep up."
  "Luckily for him, I'm a law-abiding citizen."
  return

label quest_maxine_hook_interrogation_kate:
  show kate confident with Dissolve(.5)
  kate confident "Admit it, you did this."
  mc "I have no idea what you're talking about."
  kate confident_right "Okay... if you didn't do it, then it was definitely that chick with crooked teeth."
  mc "[isabelle] doesn't have crooked teeth."
  kate laughing "It sure sounds like she does when she talks."
  kate flirty "Let's both say we saw her stealing stuff from the science classroom?"
  mc "That's illegal..."
  kate laughing "You're such a pussy! The cops aren't even involved."
  hide kate with Dissolve(.5)
  return

label quest_maxine_hook_interrogation_isabelle:
  show isabelle concerned with Dissolve(.5)
  isabelle concerned "Can you believe it?"
  if quest.isabelle_buried.finished:
    isabelle concerned "First my box of chocolates. Then my locker. Now expensive lab equipment."
    isabelle skeptical "This is out of control!"
  else:
    isabelle concerned "Someone broke into the science classroom! Why?"
  mc "Lots of expensive things in there..."
  mc "I don't suppose you've seen anything?"
  isabelle concerned_left "I don't think so... I barely know where the science classroom is."
  mc "I'm sure it'll all be resolved."
  isabelle skeptical "Don't be so sure. Things like this tend to be swept under the rug and ignored."
  mc "You really don't trust the authorities, do you?"
  isabelle skeptical "They still haven't given me a reason to."
  hide isabelle with Dissolve(.5)
  return

label quest_maxine_hook_interrogation_jacklyn:
  show jacklyn excited with Dissolve(.5)
  jacklyn excited "Shaking the bars, are we, convict?"
  mc "Not exactly. I'm mostly confused..."
  jacklyn annoyed "Not your type of crime? Mine neither."
  jacklyn laughing "Way too petty."
  mc "Right. Only murder?"
  jacklyn laughing "That's right on the fifty! Murder and occasionally vandalism in the name of art and freedom."
  hide jacklyn with Dissolve(.5)
  return

label quest_maxine_hook_interrogation_maxine:
  show maxine concerned with Dissolve(.5)
  maxine concerned "Do you know who the culprit is?"
  mc "No. Do you?"
  maxine excited "Without a doubt... I see everything."
  mc "Well, who is it?"
  maxine thinking "If I told you, that would put me on the scene of the crime."
  maxine thinking "I need to remain under the radar."
  mc "Right, of course. You know, but won't tell."
  mc "How convenient..."
  maxine eyeroll "I told you yesterday that all the power supply units were missing from the science classroom."
  "[maxine] did tell me that. Maybe she knows something?"
  mc "Are you going to tell them what you know?"
  maxine skeptical "I think I'm going to do some investigating on my own first."
  hide maxine with Dissolve(.5)
  return

label quest_maxine_hook_interrogation:
  show jo afraid at appear_from_right
  jo afraid "[mc], you're up."
  jo afraid "Please, come with me."
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_ground_floor"
  $mc["focus"] = ""
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  show guard suspicious at Transform(xalign=.25)
  show jo skeptical at Transform(xalign=.75)
  with Dissolve(.5)
  guard suspicious "Can you tell me your whereabouts last night?"
  if quest.maxine_hook["nightly_escapades"]:
    mc "I was with [maxine]. She can vouch for me."
    guard suspicious "We will confirm this with her, so you better not be making things up.{space=-5}"
    mc "I'm not stupid..."
  else:
    mc "I was at home. [jo] can vouch for me."
    jo confident "I most certainly can."
  jo smile "I guess that'll be all, won't it?"
  guard surprised "Not so fast!"
  guard suspicious "We're not quite through yet."
  mc "What was even stolen?"
  jo neutral "Five power supply units."
  mc "Really...?"
  guard angry "Why? Do you know something about that?"
  mc "Well, [maxine] said she was looking for one yesterday, and that they were all gone."
  guard neutral "..."
  guard angry "If true, this alters the timetable for the crime!"
  "God, he sounds like he's watched too much CSI..."
  mc "So, I'm off the hook?"
  jo confident "You were never on the hook, honey!"
  jo confident "Let's have a word with [maxine], shall we?"
  guard suspicious "I guess."
  hide guard
  hide jo
  with Dissolve(.5)
  $quest.maxine_hook.advance("thief")
  return

label quest_maxine_hook_thief:
  show maxine laughing with Dissolve(.5)
  maxine laughing "Aha! I knew you'd come through!"
  mc "What are you talking about?"
  maxine smile_power "I'm talking about {i}this.{/}"
  mc "Wait, what? I specifically made a point out of not getting you a power supply."
  maxine laughing_power "It seems like the strings of fate played you like a marionette..."
  mc "Seriously though, how?"
  mc "Did you steal it?"
  maxine smile "I just needed your help to get them off my track."
  maxine smile "They think the break-in took place hours before it did now. It totally ruined their timeline."
  mc "So the power supplies weren't missing when you told me they were?"
  maxine smile "Not even a little bit."
  show maxine smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What if I report you?\"":
      show maxine smile at move_to(.5)
      mc "What if I report you?"
      maxine concerned "It would be your word against mine."
      maxine excited "And who knows, maybe I planted something of yours on the crime scene for insurance?"
      mc "..."
      mc "Fine. I won't say anything."
    "\"You're an evil genius.\"":
      show maxine smile at move_to(.5)
      mc "You're an evil genius."
      $maxine.love+=1
      maxine excited "Thank you."
  maxine flirty "Come on, I now have everything I need to be hooked up to the internet!"
  window hide
  show maxine hooking_up_smile
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $game.location = "school_hook_up_table"
  $mc["focus"] = "maxine_hook"
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  maxine hooking_up_smile "Listen closely. This morning I put a micro-transmitter in my cereal."
  maxine hooking_up_smile "Throughout the day, that transmitter has traveled through my digestive system."
  mc "...You ate electronics for breakfast?"
  maxine hooking_up_angry "Yes! Try to keep up!"
  maxine hooking_up_smile "The transmitter is now situated somewhere in my large intestine."
  maxine hooking_up_smile "And with the help of the equipment here, we're going to hook me up to the internet."
  menu(side="left"):
    extend ""
    "\"This makes absolutely no sense...\"":
      mc "This makes absolutely no sense..."
      $maxine.lust-=2
      maxine hooking_up_angry "I told you to listen! Listen and you will understand."
      mc "I did listen, but it still makes no sense."
      mc "How are you even going to get electricity to the transmitter?"
      maxine hooking_up_angry "Do you really think I haven't thought of that?"
      mc "Honestly, who knows at this point..."
      maxine hooking_up_smile "You underestimate my meticulousness. Everything has been planned in great detail."
    "\"How about we hook up instead?\"":
      mc "How about we hook up instead?"
      $maxine.lust+=2
      maxine hooking_up_flirty "Would you really like to?"
      mc "I mean, sure? Yeah! That's a definitely."
      maxine hooking_up_flirty "I didn't bring a transmitter for you, but maybe I have one somewhere...{space=-50}"
      mc "No, I mean..."
      mc "Forget it."
      maxine hooking_up_afraid "And here I got all excited for nothing..."
      mc "Sorry, I guess."
    "\"Can I have a slice of watermelon?\"":
      mc "Can I have a slice of watermelon?"
      maxine hooking_up_afraid "No! That's out of the question!"
      mc "Why?"
      maxine hooking_up_afraid "It's a vital part of the process."
      mc "I mean, surely you don't need all of it..."
      maxine hooking_up_angry "If I didn't need all of it, then I wouldn't have gotten all of it."
      mc "Fair enough, I guess."
  maxine hooking_up_smile "Let's proceed now."
  maxine hooking_up_thinking "First off, we have this power supply..."
  maxine hooking_up_thinking "I'm lubricating this end for better conductivity."
  maxine hooking_up_thinking "You might have to lubricate it yourself, so look closely."
  mc "I'm not putting a power cable in my mouth."
  maxine hooking_up_excited "That's fine! Any bodily fluid will suffice."
  mc "Uh... what other fluids are there?"
  maxine hooking_up_thinking "You could use pre-ejaculation fluid... or even tears."
  mc "That's a combination I'm all too familiar with..."
  maxine hooking_up_excited "Good! Let's get started!"
  maxine hooking_up_excited "Just follow my instructions closely and no one will get electrocuted!{space=-5}"
  mc "Electrocuted?!"
  maxine hooking_up_flirty_cable_insertion "Y-yes, hnngh... but don't be afraid..."
  mc "Whoa there!"
  "If anyone would just pull their pants down and show their ass for science, it would be [maxine]."
  "The power cable slid right into her ass..."
  mc "What are you doing?"
  maxine hooking_up_orgasm_cable_insertion "I need electricity... for the transmitter..."
  maxine hooking_up_orgasm_cable_insertion "I could have also swallowed the cable... but that's a choking hazard...{space=-20}"
  maxine hooking_up_concerned_cable_insertion "Now... I just need to..."
  maxine hooking_up_concerned_cable_insertion "Get it..."
  maxine hooking_up_concerned_cable_insertion "Right..."
  maxine hooking_up_afraid_cable_insertion "Damn it! I can't get it to attach to the transmitter!"
  mc "I wish I was more surprised..."
  maxine hooking_up_smile_cable_offer "Can you help me out here?"
  mc "Sure, I guess."
  maxine hooking_up_cable_flirty_hands_behind_back "Okay, try not to waste electricity!"
  mc "That's what you're worried about in this scenario...?"
  $school_hook_up_table["expression"] = 1
  $school_hook_up_table["interactive"] = True
  window hide
  hide maxine
  window auto
  $quest.maxine_hook.advance("table")
  return

label quest_maxine_hook_table:
  show maxine hooking_up_concerned_hands_behind_back
  $school_hook_up_table["interactive"] = False
  "Well, everything is in place. I just need to turn on the power now."
  "If [maxine] gets electrocuted in her ass, that's on her..."
  mc "Okay, are you ready?"
  maxine hooking_up_concerned_hands_behind_back "Mmmph!"
  mc "I'll take that as a yes..."
  mc "Here we go."
  window hide
  show white with Dissolve(.25)
  $school_hook_up_table["router_connected"] = True
  $set_dialog_mode("default_no_bg")
  maxine hooking_up_concerned_hands_behind_back "Mmmmmaaah!" with vpunch
  hide white with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Holy shit! That must've hurt..."
  "..."
  mc "Wait... it seems like the router is actually connected to something?"
  mc "I guess the transmitter does have power somehow..."
  maxine hooking_up_concerned_hands_behind_back "Mmmph!"
  "I have no idea what she's saying..."
  menu(side="middle"):
    extend ""
    "Remove the watermelon slice":
      $mc.love+=1
      $school_hook_up_table["watermelon_slice"] = False
      maxine hooking_up_orgasm_hands_behind_back "Oh! That was something else!"
      maxine hooking_up_orgasm_hands_behind_back "I think I saw something!"
      mc "Really? What did you see?"
      maxine hooking_up_smile_hands_behind_back "It was beautiful! It must've been a glimpse of cyberspace!"
      mc "Great."
      maxine hooking_up_flirty_hands_behind_back "Turn the power on again!"
      mc "Seriously?"
      maxine hooking_up_flirty_hands_behind_back "Yes! Higher voltage!"
      mc "If you insist..."
      window hide
      show white with Dissolve(.25)
      $set_dialog_mode("default_no_bg")
      show maxine hooking_up_ahegao_hands_behind_back
      maxine hooking_up_ahegao_hands_behind_back "Aaaaaah!" with vpunch
      hide white with Dissolve(.5)
      window auto
      $set_dialog_mode("")
    "Keep it in":
      $mc.lust+=1
      "I kinda prefer [maxine] gagged, to be honest."
      "She's less annoying that way."
      maxine hooking_up_concerned_hands_behind_back "Mmmph!"
      mc "It's time to increase the voltage!"
      window hide
      show white with Dissolve(.25)
      $set_dialog_mode("default_no_bg")
      maxine hooking_up_concerned_hands_behind_back "Mmmmmmmmmph!" with vpunch
      hide white with Dissolve(.5)
      window auto
      $set_dialog_mode("")
  "Who knew science could be so erotic?"
  "The way her sphincter clenches around the power cable is something deeply perverted."
  "The inner walls of her asshole contract to electrical currents that pulsate through her."
  "I don't know if [maxine] is seeing anything at all in those goggles, but one thing is for sure..."
  "I'm seeing her for who she really is for the first time."
  "Underneath that conspiracy-nutter-exterior is a girl who is deeply dedicated to her work."
  "Someone who will stop at nothing to find the truth, even if it means taking electricity up your ass."
  "That's admirable in some ways, and quite hot."
  "Few people are as driven as [maxine], and perhaps if I spend more time with her it'll rub off on me."
  "...or I'll just become as crazy as her."
  "Either way, I wonder if she'd let me put my dick in her ass if I attached an electrode to it..."
  "That thought alone is worth sticking around for."
  "..."
  "I should probably check if she's okay..."
  $school_hook_up_table["vr_goggles"] = False
  $school_hook_up_table["watermelon_slice"] = False
  maxine hooking_up_out_of_it_hands_behind_back "Uhhhh...."
  mc "Are you okay?"
  maxine hooking_up_out_of_it_hands_behind_back "I... I saw it..."
  mc "What did you see?"
  maxine hooking_up_out_of_it_hands_behind_back "The truth..."
  mc "Yeah? Did you see the light?"
  maxine hooking_up_out_of_it_hands_behind_back "No..."
  maxine hooking_up_out_of_it_hands_behind_back "It was a person..."
  mc "A person?"
  maxine hooking_up_out_of_it_hands_behind_back "Yes... I saw through... the narrative lens..."
  mc "This again?"
  maxine hooking_up_out_of_it_hands_behind_back "It's just a thin screen... separating us... from the truth..."
  mc "What are you talking about?"
  maxine hooking_up_out_of_it_hands_behind_back "We're being watched..."
  mc "You always say that..."
  maxine hooking_up_out_of_it_hands_behind_back "No... I mean really..."
  maxine hooking_up_out_of_it_hands_behind_back "This very moment... there's a person... watching us..."
  mc "I don't understand anything."
  maxine hooking_up_orgasm_hands_behind_back "That's... okay... You never will..."
  maxine hooking_up_orgasm_hands_behind_back "The world... is a stage... and everyone's a player..."
  maxine hooking_up_orgasm_hands_behind_back "But there is... more than one world... and I've seen our audience..."
  maxine hooking_up_orgasm_hands_behind_back "And I now know... what the narrative lens is... or who..."
  "[maxine] seems completely out of it. I guess the electricity fried her brain or something."
  "And what is this narrative lens she keeps talking about?"
  "I should probably take her to the [nurse]'s office..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_ground_floor_west"
  $mc["focus"] = ""
  pause 1.5
  hide maxine
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $unlock_replay("maxine_experiment")
  $maxine["at_none_today"] = True
  $school_ground_floor_west["nurse_room_locked_today"] = "maxine"
  $quest.maxine_hook.finish()
  return
