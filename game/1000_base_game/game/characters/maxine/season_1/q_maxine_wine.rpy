label quest_maxine_wine_start:
  maxine "I already know what you're going to say..."
  mc "Hm?"
  show maxine thinking with Dissolve(.5)
  maxine thinking "You're here to ask me about the glowing spider eggs."
  mc "What glowing spider eggs?"
  maxine eyeroll "See? Our timelines have caught up."
  mc "That's not..."
  mc "You know what, nevermind."
  maxine angry "No, no! That's all wrong. You should always mind!"
  maxine angry "Lest the world slips right past you."
  "[maxine] always was a bit of a weirdo, and that clearly hasn't changed."
  mc "I guess I'll try to be more mindful from now on..."
  maxine afraid "But don't be too mindful! You might not be ready for what hides between the lines..."
  mc "Right. I'll be moderately mindful then."
  maxine smile "Very well. In moderation there is truth."
  mc "I thought it was in wine?"
  maxine concerned "It depends on the brand and on the grapes."
  maxine excited "But I could use a glass of wine..."
  maxine excited "If you want to continue your pursuit of carnal pleasures, I believe the custom is to buy me a drink."
  mc "I wasn't—"
  maxine skeptical "..."
  mc "Okay, fine. I guess I was."
  "Alcohol is not allowed in the school, but there must be a way around it."
  "Who'd be a good person to talk to?"
  hide maxine with Dissolve(.5)
  $quest.maxine_wine.start()
  return

label quest_maxine_wine_jacklyn_deal:
  show jacklyn neutral with Dissolve(.5)
  "[jacklyn] seems like the type who would know how to smuggle alcohol into the school."
  mc "Hey, random question. What are your views on alcohol?"
  jacklyn smile "Party filler. Liver killer. Makes me want to fuck Ben Stiller."
  jacklyn smile "And I wouldn't normally."
  show jacklyn smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What's wrong with Ben?\"":
      show jacklyn smile at move_to(.5)
      mc "What's wrong with Ben?"
      jacklyn smile_right "He's too manly. I like my men more femme."
      mc "Err... right. Why not just date a girl then?"
      jacklyn laughing "Not really a dabbler, that's all!"
      mc "Not sure if I believe you."
      $jacklyn.lust-=1
      jacklyn angry "Can a girl not talk about her sexuality without being questioned?"
      mc "Sorry, but anyway..."
    "\"Why do you wear a skirt that short?\"":
      show jacklyn smile at move_to(.5)
      mc "Why do you wear a skirt that short?"
      jacklyn neutral "Probably the same reason you're not wearing a sombrero."
      mc "And what reason is that?"
      jacklyn excited "Artistic expression!"
      $jacklyn.lust+=1
      jacklyn laughing "And the fact that we both don't mind a little bugger-scotch."
      mc "What...?"
      jacklyn neutral "What?"
      mc "..."
      mc "Anyway..."
  mc "You seem like a person who knows how to smuggle booze into the school."
  jacklyn smile_right "You're far too generous. Not that I would do that or anything."
  mc "Not that I would do that either, but do you have any tips?"
  jacklyn smile_right "Not assuming you would, but I know all secret artforms of getting wasted during school hours."
  jacklyn smile "Of course, nothing's free."
  mc "What do you want?"
  jacklyn neutral "Hmm... I want a new pin."
  mc "A pin?"
  jacklyn smile_right "Right. Like my yellow emoji one."
  mc "Err... okay..."
  "How hard can it be? [flora] used to collect those."
  hide jacklyn with Dissolve(.5)
  $quest.maxine_wine.advance("pins")
  return

label quest_maxine_wine_flora_deal:
  show flora cringe with Dissolve(.5)
  flora cringe "Where did you get that shirt?"
  mc "Stole it."
  flora annoyed "I'm serious."
  mc "Ah, in that case..."
  mc "Stole it."
  flora confused "It's gamer merch, isn't it? It doesn't count as stealing if no one wants it."
  mc "So, I'm off the hook for eating your cereal?"
  flora angry "No!"
  mc "You didn't say you wanted it..."
  flora angry "When I ask [jo] to buy my favorite cereal, it's implied that I want it."
  mc "Well, I didn't know that."
  flora eyeroll "You're not off the hook!"
  show flora eyeroll at move_to(.25)
  menu(side="right"):
    extend ""
    "\"What if I juggle balls?\"":
      show flora eyeroll at move_to(.5)
      mc "What if I juggle balls?"
      flora sarcastic "Isn't that what you do every night in your room?"
      mc "You're damn right it is."
      $mc.charisma+=1
      mc "Joining the circus requires practice."
      flora confused "How about you just apply for the clown position? You've got the experience."
      mc "How about I get [jo] to buy you new cereal?"
      flora annoyed "Don't even touch my cereal again."
    "\"What do you want?\"":
      show flora eyeroll at move_to(.5)
      mc "What do you want?"
      flora flirty "Your heart."
      mc "[flora], I—"
      flora laughing "On a platter!"
      mc "That's just morbid."
      flora annoyed "Whatever ends my constant annoyance with you."
      mc "..."
      $mc.strength+=1
      mc "I've trained my heart through hours in the gym just so it would squirt blood on you if this ever happened."
      flora eyeroll "Whatever."
    "\"Have you seen anything weird since school started?\"":
      show flora eyeroll at move_to(.5)
      $mc.intellect+=1
      mc "Have you seen anything weird since school started?"
      flora blush "Apart from you?"
      mc "How are you always so funny?"
      flora laughing "It's a talent!"
      mc "What about... weird phone calls?"
      flora thinking "Not that I can think of."
      mc "What about new people you haven't seen before?"
      flora confused "I guess it's only [jacklyn] and [isabelle], right?"
      mc "Unless you count [mrsl]'s transformation."
      flora annoyed "You would notice that."
      mc "Obviously. My eyesight is very good!"
  mc "Anyway, you used to collect pins, right?"
  flora smile "Yep."
  mc "I need one."
  flora smile "Nope."
  mc "Seriously, [flora]. You need to give me one."
  flora annoyed "You're not getting one!"
  mc "You're just saying that to make my life difficult."
  flora sarcastic "Obviously."
  flora sarcastic "So, what are you going to offer me today? Money? Power? Glory?"
  mc "Have you become a Lana fan?"
  flora confused "Always have been."
  mc "Well, I'm not going to offer you anything. I think you should just give me one."
  flora concerned "Oh, yeah?"
  flora concerned "And why would I do that?"
  mc "Because I'm struggling, and you don't want to kick a man while he's down."
  flora confident "Don't think you can wiggle out of it."
  flora confident "You want something from me — you're my slave. That's how our relationship has always worked."
  mc "Ugh, fine. What do you want this time?"
  flora thinking "My internship has an open house day and I'm meant to put up posters..."
  flora blush "Now you're going to do it for me."
  mc "..."
  flora flirty "Riiight?"
  mc "Whatever. Fine."
  flora laughing "Perfect! You need to put one up on each bulletin board in the school."
  flora laughing "I'll put the posters in your locker!"
  hide flora with Dissolve(.5)
  $quest.maxine_wine.advance("locker")
  return

label quest_maxine_wine_flora_pin:
  show flora afraid with Dissolve(.5)
  flora afraid "Aaah!" with hpunch
  mc "..."
  flora skeptical "Oh."
  mc "Why so jumpy, [flora]?"
  flora laughing "No reason!"
  mc "What were you doing just now?"
  flora annoyed "None of your business."
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Let me smell your hand, then.\"":
      show flora annoyed at move_to(.5)
      mc "Let me smell your hand, then."
      $flora.lust+=1
      flora cringe "Nope."
      mc "Why not? I hope you're washing your hands, [flora]."
      mc "You know what can happen if you don't."
      flora confused "Whatever."
      mc "So, why can't I smell your hand?"
      flora annoyed "Because it's weird!"
      mc "It's not as weird as being jumpy for no reason."
      mc "How about I pinch my nose and have a lick instead?"
      flora angry "You're not sniffing or licking my hand! Forget about it!"
      mc "Okay, no need to be upset. Just trying to help."
      flora angry "How exactly were you trying to help?"
      mc "By sniffing or licking your hand. I thought we went over this?"
      flora eyeroll "Don't be surprised if you wake up strangled."
      mc "That doesn't make sense."
      flora annoyed "You don't make sense!"
      mc "..."
    "\"I have questions about your internship.\"":
      show flora annoyed at move_to(.5)
      mc "I have questions about your internship."
      flora skeptical "What kind of questions?"
      mc "Remember that long-ass questionnaire you had me do?"
      flora smile "Yep."
      mc "What was that for?"
      flora thinking "Just some basic analysis thing."
      mc "What were they analyzing?"
      flora laughing "I don't know! How much of a dork you are?"
      mc "Who else did you give it to?"
      flora annoyed "What's this about?"
      mc "Just answer the question."
      flora eyeroll "Fine. Only you got it."
      flora eyeroll "It was a questionnaire for young adult males, who are socially challenged."
      mc "Great. Fantastic."
      flora sarcastic "That's what they said too."
      mc "What exactly is it that they do?"
      flora confused "Security, containment, protection. Plus, there's a zoo in the basement."
      flora confused "It's a wildlife conservation sort of thing."
      mc "So, why are they surveying young adult males?"
      flora annoyed "I don't know everything, okay?"
      mc "Have you seen any of the animals?"
      flora smile "I'm still a first-year intern. I'm hoping next year!"
      mc "Hmm..."
    "\"What do you think about [maxine]?\"":
      show flora annoyed at move_to(.5)
      mc "What do you think about [maxine]?"
      flora thinking "[maxine]? What about her?"
      mc "That's what I'm asking you!"
      flora worried "I suppose she's nice. I don't really talk to her much."
      flora laughing "I like her newspaper, though!"
      mc "You're probably the only one that reads it."
      flora annoyed "That's fine. I enjoy it. I don't care if others like it or not."
      mc "I actually find it quite interesting as well."
      flora sarcastic "See? It's not that bad."
      mc "What do you think of the first issue this year?"
      flora confused "What was it again? Question the truth?"
      mc "Right."
      flora confused "Oh, I don't know. Maybe there's something in the picture?"
      flora confused "I didn't have time to really get into the article."
  flora laughing "Anywho! Did you put up the posters yet?"
  mc "Yeah."
  flora flirty "Okay! Well, I brought you this pin."
  $mc.add_item("flora_pin")
  mc "Couldn't you have picked a better one?"
  flora cringe "This one is perfectly fine!"
  mc "I'm just messing with you."
  flora annoyed "..."
  mc "Bye!"
  "I hope it's good enough for [jacklyn]."
  $quest.maxine_wine.advance("jacklynpin")
  hide flora with Dissolve(.5)
  return

label quest_maxine_wine_jacklyn_pin:
  "Where did [jacklyn] come from?"
  "Why is she around this time?"
  "Is it pure chance or by design?"
  "[maxine]'s newspaper said to question the truth. And while listening to her isn't a great idea, it's hard not to wonder."
  "So far, everything's pretty much the same apart from [jacklyn] and [isabelle] showing up."
  "The question is, why are they here now?"
  "Shit. Maybe [maxine] is rubbing off on me."
  if jacklyn.at("school_entrance"):
    show jacklyn neutral at appear_from_right
  else:
    show jacklyn neutral at appear_from_left
  jacklyn neutral "Lost your tongue in the cat?"
  mc "Huh? Not that I know of!"
  jacklyn smile "Do you have a question?"
  jacklyn smile "Perhaps regarding the latest lesson?"
  mc "Not exactly! But I do have another question."
  show jacklyn smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"How did you get this job?\"":
      show jacklyn smile at move_to(.5)
      mc "How did you get this job?"
      jacklyn thinking "One of the teachers here recommended my ass to the school board."
      mc "Mrs. Bloomer?"
      jacklyn cringe "I wouldn't be putting my chips on that number."
      jacklyn cringe "We didn't get along well when I was her student."
      "Hmm... I don't remember seeing [jacklyn] in the corridors before now."
      mc "When did you graduate?"
      jacklyn excited_right "Like, six sun laps ago?"
      mc "Huh. Okay, that was before my time."
      mc "How come you didn't get on with Mrs. Bloomer?"
      jacklyn annoyed "She's impressionist scum."
      mc "That's it?"
      $jacklyn.love+=1
      jacklyn laughing "Do you really need any other reason?"
      mc "I... err, I guess no."
    "\"Do you believe in time travel?\"":
      show jacklyn smile at move_to(.5)
      mc "Do you believe in time travel?"
      jacklyn neutral "Wouldn't hipsters from a distant future be fucking around here then?"
      mc "Maybe they are and you just don't know about it?"
      jacklyn angry "You're a truther, but that would make me old pussy. Slam that in the dumpster!"
      mc "You and your pussy look perfectly ripe to me."
      jacklyn laughing "Careful with your throat there!"
      mc "..."
      "The things she says..."
      "Well, she didn't even flinch when I asked, so she's probably not in on this thing."
      $mc.intellect+=1
      "Whatever this thing is..."
    "\"Who do you think would win in a fight between Picasso and Monet?\"":
      show jacklyn smile at move_to(.5)
      mc "Who do you think would win in a fight between Picasso and Monet?"
      jacklyn excited "Oh, that's easy. Picasso, of course."
      mc "Why?"
      jacklyn laughing "Okay, here's why..."
      jacklyn laughing "Pablo Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y Picasso."
      jacklyn excited_right "Monet would've died of old age before they even finished reading out his opponent's name."
      mc "That makes perfect sense!"
      $jacklyn.lust+=1
      jacklyn laughing "Takes an art major to know these things."
  mc "Anyway, I got you a pin."
  jacklyn neutral "Trying to stick in my ass?"
  mc "Err... no! I still need your help getting that wine."
  jacklyn smile "Right on, AA! Fine, hand over the goodies."
  $mc.remove_item("flora_pin")
  $jacklyn.equip("jacklyn_squidpin")
  jacklyn smile_right "Isn't that a star-dash and a half?"
  mc "Looks good on you."
  jacklyn smile_right "Yeah, like a splash of Oxford on the Cambridge campus."
  mc "So, what about that wine?"
  jacklyn neutral "You know alcohol's illegal in the school, right?"
  mc "..."
  jacklyn laughing "I'm just tugging your bandana!"
  jacklyn laughing "There used to be a hidden stash where the sun does shine..."
  jacklyn laughing "That's all I'll say. Enjoy your drunken stupor."
  "Ugh! Why couldn't [jacklyn] just tell me where it is?"
  "All right, let's play detective..."
  hide jacklyn with Dissolve(.5)
  $quest.maxine_wine.advance("search")
  return

label quest_maxine_wine_make_a_move:
  show maxine skeptical with Dissolve(.5)
  maxine skeptical "Your future depends on this conversation, [mc]."
  mc "No pressure, eh?"
  maxine sad "Have you watered your plants and said your goodbyes?"
  mc "No? But I don't think—"
  maxine afraid "Always! Always water your plants!"
  mc "Fine, I'll keep it in mind."
  maxine thinking "It's not only about planting the seeds, it's also about harvesting the fruits of your labor."
  maxine thinking "Everything in between is just waiting and watering."
  "Why do I get the feeling she's not actually talking about gardening?"
  maxine smile "I'll give you one question because of Uranus."
  "Okay, better make this one count..."
  show maxine smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What about Uranus?\"":
      show maxine smile at move_to(.5)
      mc "What about Uranus?"
      $maxine.love-=1
      maxine eyeroll "That's a bit personal, don't you think?"
      mc "I meant the planet."
      maxine annoyed "So did I."
      mc "..."
      "Knowing [maxine], she probably did mean that."
      maxine annoyed "Never talk to me about Uranus again."
    "\"Would you rather eat one horse-sized dick or five dick-sized horses?\"":
      show maxine smile at move_to(.5)
      mc "Would you rather eat one horse-sized dick or five dick-sized horses?"
      maxine thinking "A very good question."
      $maxine.lust-=1
      maxine thinking "I applaud your ingenuity."
      maxine concerned "Surely, it depends on my appetite."
      maxine flirty "I did see a dick-sized horse once. It looked quite appetizing."
      mc "W-what?"
      maxine concerned "Are you telling me that all these years, you've never seen your own backyard?"
      mc "Is there a... dick-sized horse in my backyard?"
      maxine afraid "It's time we change the subject to a more delicate matter."
    "\"How did you get your own office?\"":
      show maxine smile at move_to(.5)
      mc "How did you get your own office?"
      maxine eyeroll "You do know I run the school newspaper, right?"
      mc "Yes, but do you really need an office for that?"
      maxine confident "I'm glad you asked."
      mc "..."
      maxine confident "..."
      mc "Are you going to tell me?"
      maxine annoyed "That's impossible to know right now."
      maxine annoyed "Also, that's two questions over the limit. I'll have to deduct it from your future pool."
      "Damn it. She's so obnoxious."
      mc "Actually, you gave two answers. That's two over my limit."
      $maxine.lust+=1
      maxine excited "That's going on your permanent record!"
      maxine flirty "One answer should suffice. A visionary approach to curiosity."
    "\"Do you know why I'm here again?\"":
      show maxine smile at move_to(.5)
      mc "Do you know why I'm here again?"
      maxine skeptical "To give romance and sex another chance."
      "..."
      "Does she actually know?"
      "What was it that the mysterious texter said?"
      mc "How do you know about this?"
      maxine thinking "I've been monitoring your excursions."
      mc "Really?"
      maxine sad "You engage in a lot of aimless and sometimes circular walking."
      maxine thinking "It's like you're always looking for something."
      mc "So, did I really get sent back in time?"
      maxine concerned "What makes you say that?"
      mc "Wasn't that what you meant? Why have you been monitoring me then?"
      maxine neutral "You said you were going to pursue me romantically or sexually."
      maxine neutral "Naturally, that gives me cause for tracking your actions."
      maxine laughing "You don't mind that, do you?"
      mc "I guess not..."
      $maxine.love+=1
      maxine excited "Splendid."
  mc "Err... okay. Fine."
  mc "I have brought you wine."
  maxine smile "I'm aware."
  maxine smile "I've been waiting for you to make a move."
  mc "Make a move?"
  maxine eyeroll "It's standard dating procedure."
  maxine eyeroll "First you buy me a drink, then you make a move."
  mc "And what if I don't?"
  maxine thinking "Then your chances of success are statistically lower."
  mc "Maybe I'm not interested in following the playbook..."
  maxine smile "I won't either, then."
  "Why did that sound like a threat?"
  mc "Well, have your wine and we'll see where things go."
  $mc.remove_item("summer_wine")
  maxine neutral "We will, indeed."
  maxine smile "Thanks for the drink."
  mc "You're welcome, I guess..."
  maxine concerned "I might send you a nude later, so be prepared."
  mc "What now?"
  maxine concerned "We're not following standard dating procedure, but you did buy me a drink."
  maxine excited "Normally, you'd make a move now... but since we're both freeballing it, expect the unexpected."
  maxine smile "Anyway... you've way exceeded your question quota for the day, so I shall take my retreat."
  "Not exactly sure what's going on, but you never really know with her."
  $maxine["flirt_unlocked"] = True
  $game.notify_modal(None,"Love or Lust","Flirting with [maxine]:\nUnlocked!",wait=5.0)
  hide maxine with Dissolve(.5)
  $quest.maxine_wine.finish()
  return
