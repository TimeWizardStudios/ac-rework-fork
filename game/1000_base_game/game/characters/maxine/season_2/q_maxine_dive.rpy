label quest_maxine_dive_start:
  "It feels good to get outside and get some fresh air."
  "Just me, myself, and I."
  "With everything that's been going on, it's good to remember to take a moment for myself."
  "I was never good at practicing healthy self-care."
  "It was all porn, games, and obscene amounts of strawberry juice."
  # "Alone in my room, feeding bad habits and my resentment of the world."
  "Alone in my room, feeding bad habits and my resentment of the world.{space=-55}"
  "But this time around, I'm trying to make changes for myself."
  "The air is crisp and awakens my senses, reminding me how good it feels to be alive."
  "This is the best time of year. Not too hot, but not too cold, either."
  "There is a quiet, easy slowness to it."
  "And sometimes that's exactly what is needed to refuel the tank."
  "To just take a moment to appreciate life and the simple pleasures it has to offer."
  # "The fall sure can be dark, but there are places in my soul darker than any season."
  "The fall sure can be dark, but there are places in my soul darker than{space=-25}\nany season."
  # "I never want to take something as simple as autumn for granted again."
  "I never want to take something as simple as autumn for granted again.{space=-50}"
  "Or a quiet afternoon by the pond."
  "No disturbances, no obligations, no—"
  window hide
  show maxine excited at appear_from_right
  pause 0.5
  window auto
  mc "[maxine]."
  maxine excited "[mc]."
  mc "What the hell is that?"
  maxine concerned "Always asking the wrong questions, aren't you?"
  mc "Seriously? It seems like a pretty valid question to me."
  maxine concerned "It's a box, of course."
  maxine concerned "I thought we had discussed you being more perceptive?"
  maxine concerned "Or do I need to commence my examination of your eyes sooner than anticipated?"
  mc "Err, my eyes are fine, thank you."
  mc "What I meant was, what's in the box?"
  maxine thinking "..."
  mc "..."
  maxine sad "..."
  mc "What's in the box, [maxine]...?"
  "Surely, it's too big to be a head, right?"
  "But what if it's lots of heads?"
  "Oh, god."
  maxine thinking "Help me open it and you will see."
  show maxine thinking at move_to(.75)
  menu(side="left"):
    extend ""
    # "\"Why would I put in work when you could just tell me?\"":
    "\"Why would I put in work when\nyou could just tell me?\"":
      show maxine thinking at move_to(.5)
      mc "Why would I put in work when you could just tell me?"
      maxine eyeroll "Why would I tell you when I can show you?"
      mc "Look, we can go in circles all day if you want."
      maxine skeptical "Clockwise or counterclockwise?"
      mc "Does it matter?"
      maxine skeptical "It's only the difference between one multiverse and the next."
      mc "Multiverse? That doesn't exist..."
      maxine confident_hands_down "Wise, [mc]. Unexpectedly wise."
      mc "Err, what is?"
      # maxine confident_hands_down "Believing none of what you hear and only twenty-five percent of what you see."
      maxine confident_hands_down "Believing none of what you hear and only twenty-five percent of what{space=-25}\nyou see."
      mc "Half."
      maxine skeptical "Well, yes, but what if only one of your eyes is open?"
      mc "..."
      mc "Why would that be the case?"
      maxine skeptical "Because one would be wise to sleep with one eye open."
      mc "Right. I guess I hadn't considered that."
      $maxine.lust+=1
      maxine confident_hands_down "Next time you will."
    # "\"Are you saying you need help getting your own box open?\"":
    "\"Are you saying you need help\ngetting your own box open?\"":
      show maxine thinking at move_to(.5)
      mc "Are you saying you need help getting your own box open?"
      maxine eyeroll "It is a very large box."
      mc "I had no idea."
      maxine annoyed "It's right there. Ready to be eased open."
      mc "Do I have permission to touch your box, then?"
      maxine skeptical "Did you suffer a recent fall?"
      maxine skeptical "Your brain seems to be working at a slower capacity than usual."
      "Ouch."
      mc "All right, all right! I'll open your box."
      mc "You really should be more attentive to it yourself, though."
      mc "It's good to explore these things."
      maxine confident_hands_down "Exploration is the ultimate goal here."
      $maxine.love+=1
      maxine confident_hands_down "And I wouldn't allow just anyone to touch my box."
      mc "Don't I feel special..."
    "\"It's something weird, isn't it?\"":
      show maxine thinking at move_to(.5)
      mc "It's something weird, isn't it?"
      maxine confident "Weird is in the thumb of the beholder."
      mc "...the thumb?"
      maxine confident "It is by far the most superior digit."
      mc "I don't know what that has to do with anything."
      maxine skeptical "Everything has to do with everything."
      mc "Ugh! I just wanted to come out here to relax today!"
      mc "And now you're giving me a headache..."
      maxine annoyed "Relaxing? At a time like this?"
      mc "Like what?"
      $maxine.lust-=1
      maxine annoyed "You disappoint me, [mc]."
      mc "You and everyone else in my life..."
  if renpy.showing("maxine confident_hands_down"):
    maxine confident_hands_down "Anyway, just help me open it, will you?"
  elif renpy.showing("maxine annoyed"):
    maxine annoyed "Anyway, just help me open it, will you?"
  "I should really just walk away. Self-care and all that."
  "But damn it if I'm not curious..."
  mc "Ugh! All right, fine! Keep your hat on."
  if renpy.showing("maxine confident_hands_down"):
    maxine confident_hands_down "I always do. It keeps the hair wigs away."
  elif renpy.showing("maxine annoyed"):
    maxine confident "I always do. It keeps the hair wigs away."
  mc "..."
  window hide
  hide screen interface_hider
  show maxine excited
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "And just like that, I'm sucked in."
  "She takes hold of one side of the box and I the other."
  "Together, we prise it open."
  $school_park["diving_bell"] = True
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "Huh. What is it, exactly?"
  maxine excited "It's a diving bell!"
  mc "A what now?"
  maxine concerned "A diving bell."
  mc "But what does it do?"
  maxine concerned "It's in the name, [mc]. A bell used for diving."
  mc "Somehow, that doesn't give me a better idea..."
  maxine excited "Let me explain it to you, then."
  maxine excited "I have been following a map I found and—"
  mc "Is it the map from the forest glade?"
  maxine concerned "Of course not."
  maxine concerned "This is a different map."
  show maxine excited with dissolve2
  extend " I found it in the school basement."
  mc "For the last time, the school has no basement!"
  maxine thinking "Oh, really? Where did I get this map, then?"
  mc "I don't know, [maxine]! Are you sure you didn't draw it yourself?"
  maxine thinking "I am quite certain."
  maxine sad "Or, at least, I have no memory of doing it."
  mc "So, the map you found in the school's imaginary basement led you to this pond?"
  maxine thinking "Correct."
  # maxine thinking "And you are going to dive down there with me to uncover its treasure."
  maxine thinking "And you are going to dive down there with me to uncover its treasure.{space=-40}"
  mc "Err, I am?"
  # maxine sad "Of course. But first you must acquire the necessary armor and weapons."
  maxine sad "Of course. But first you must acquire the necessary armor and weapons.{space=-95}"
  "This is just getting stranger and stranger..."
  mc "You're joking, right?"
  maxine thinking "I never joke about weaponry."
  # "I don't know if I've ever seen her joke at all, despite all the crazy shit she says."
  "I don't know if I've ever seen her joke at all, despite all the crazy shit{space=-10}\nshe says."
  show maxine thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"The sharpest weapon is wit.\"":
      show maxine thinking at move_to(.5)
      mc "The sharpest weapon is wit."
      maxine eyeroll "While that is normally the case, it won't defend you against the crab rocks."
      maxine eyeroll "They're notoriously dull."
      mc "Heh. Dumb as rocks?"
      maxine skeptical "With the temperament of a crab."
      maxine skeptical "It's a very dangerous combination."
      $mc.intellect+=1
      mc "Of course it is."
    # "\"Why do I need armor and weapons for pond diving?\"":
    "\"Why do I need armor and\nweapons for pond diving?\"":
      show maxine thinking at move_to(.5)
      mc "Why do I need armor and weapons for pond diving?"
      maxine annoyed "Because of the pond scum."
      mc "..."
      mc "Did you just make a joke?"
      maxine skeptical "I don't know what you're talking about."
      maxine skeptical "It's a very serious threat down there."
    "\"Neither do I.\"":
      show maxine thinking at move_to(.5)
      mc "Neither do I."
      mc "I keep my weapon hard and ready at all times."
      $maxine.lust+=1
      maxine confident "Is it a sword?"
      mc "The biggest sword you've ever seen."
      maxine confident "Oh? From which age?"
      mc "Err, this one?"
      # maxine skeptical "New age technology doesn't hold up as well as they say, but it will do."
      maxine skeptical "New age technology doesn't hold up as well as they say, but it will do.{space=-35}"
  maxine annoyed "Anyway, you better get started."
  maxine annoyed "Meet me back here once you have your items."
  maxine annoyed "I want to go diving as soon as possible."
  window hide
  $quest.maxine_dive.start(silent=True)
  hide maxine with Dissolve(.5)
  window auto
  "It feels like I'm in some surreal fantasy quest all of a sudden..."
  "Oh, well. I guess this is happening."
  "Maybe I'll get to deep dive that pussy too once this is over?"
  window hide
  $game.notify_modal("quest", "Quest started", quest.maxine_dive.title+"{hr}"+quest.maxine_dive._phases[1].description, 5.0)
  return

label quest_maxine_dive_weapons:
  pause 0.25
  window auto
  "Thankfully, my backpack is full of more or less useful items."
  "And there's probably at least two weapons in there."
  "This is the upside of being a kleptomaniac."
  "Now, I just need to find some armor..."
  $quest.maxine_dive.advance("shield")
  return

label quest_maxine_dive_weapons_fire_axe_upon_entering:
  "Man, climbing all those stairs sure leaves me winded."
  "I wonder if I'll be able to handle a dive into the pond?"
  "But how deep can a little park pond actually be?"
  "..."
  "I can't believe I'm seriously going along with this..."
  $quest.maxine_dive["how_deep"] = True
  return

label quest_maxine_dive_weapons_fire_axe:
  "It can cut through wood, it can cut through bullshit."
  "The perfect weapon for this dive."
  window hide
  $school_roof_landing["fire_axe_taken"] = True
  $mc.add_item("fire_axe")
  return

label quest_maxine_dive_weapons_machete_upon_entering:
  "I need to find a suitable weapon for [maxine]'s crazy pond expedition."
  "Let's see here..."
  $quest.maxine_dive["suitable_weapon"] = True
  return

label quest_maxine_dive_weapons_machete:
  "Here we go! My family's oldest, greatest heirloom."
  "An ancient sword passed down from generation to generation."
  # "A blade which has thirsted upon more blood than the rainforest has rain."
  "A blade which has thirsted upon more blood than the rainforest\nhas rain."
  "..."
  "Or, at least, that would be a way cooler story for it."
  "I'm pretty sure [jo] just bought it online."
  window hide
  $home_hall["machete_taken"] = True
  $mc.add_item("machete")
  return

label quest_maxine_dive_weapons_taken:
  pause 0.25
  window auto
  "Okay! That's the weapons sorted."
  "Now, I just need to figure out what to do for armor..."
  "I really hope there's actually a giant squid or some shit in the pond."
  "Or else I'm going to feel like a total moron doing this."
  "..."
  "Better safe than sorry, I guess."
  $quest.maxine_dive.advance("shield")
  return

label quest_maxine_dive_shield_upon_entering:
  "[maxine] is crazier than usual if she thinks I have access to armor just like that."
  "Not everyone is like her."
  "In fact, no one is."
  "..."
  "Maybe there's something around here I can use, though?"
  $quest.maxine_dive["crazier_than_usual"] = True
  return

label quest_maxine_dive_shield:
  "I can't exactly boil my enemies..."
  "...but this lid could make a nice shield, regardless."
  window hide
  if quest.flora_cooking_chilli.actually_finished:
    $home_kitchen["cooking_pot_taken"] = True
  $quest.maxine_dive.advance("armor")
  $mc.add_item("cooking_pot")
  pause 0.25
  window auto
  "What about the armor, though?"
  "I probably need to play to my strengths."
  "...which is a depressingly short list."
  "I'm only good at gaming, drinking too much strawberry juice, and eating pizza—"
  "Wait, that's it!"
  "I'll order enough pizza to assemble something from the leftover cardboard!"
  "After all, you know what they say about folding paper seven times... It's harder than steel or some shit."
  "This is the perfect excuse to order from my favorite pizza place."
  "{i}Master Splinter's Pizza.{/}"
  $default_char.default_name = "Master Splinter's Pizza"
  $default_char.contact_icon = LiveComposite((128,128),(8,8),Transform("hidden_number contact_icon_alt",size=(112,112)))
  $mc.add_phone_contact("default_char", silent=True)
  return

label quest_maxine_dive_armor:
  if mc.money >= 60:
    $set_dialog_mode("phone_call_plus_textbox","default_char")
    $guard.default_name = "Phone"
    guard "{i}Calling...{/}"
    guard "{i}...{/}"
    guard "{i}Calling...{/}"
    guard "{i}...{/}"
    $guard.default_name = "Guard"
    window hide
    $set_dialog_mode("phone_call_centered","default_char")
    default_char "'Sup, brah?"
    # mc "Hey! I'd like to order one Rocksteady with double sausage, two Shredders without onion, and one Leonardo with extra anchovies."
    mc "Hey! I'd like to order one\nRocksteady with double\nsausage, two Shredders\nwithout onion, and one\nLeonardo with extra\nanchovies."
    mc "For delivery, please."
    # default_char "Ssshya brah, for shizzle!"
    default_char "Ssshya brah,\nfor shizzle!"
    # default_char "That will be, like, sixty bucks."
    default_char "That will be, like,\nsixty bucks."
    $mc.money-=60
    mc "Great, thanks!"
    default_char "Cowabanga, man!"
    play sound "end_call"
    $set_dialog_mode("")
    pause 0.5
    window auto
    "I've always loved the vibes of this place."
    "That's why I became a pizza delivery boy in my old life."
    "That... and the employee discount."
    # "Now, I just need to wait for delivery before I can eat a metric ton of pizza!"
    "Now, I just need to wait for delivery before I can eat a metric ton\nof pizza!"
    $quest.maxine_dive["pizza_ordered"] = True
    $del default_char.contact_icon
    $del default_char.default_name
    $mc.phone_contacts.remove("default_char")
  else:
    $set_dialog_mode("contact_info","default_char")
    "It's hard to pay for pizza without money."
    "I'm sure someone like [jacklyn] could pull it off, but if you're not a hot girl, well..."
    "Sucks to suck, I guess."
    window hide
    $quest.maxine_dive["not_a_hot_girl"] = True
    $set_dialog_mode("")
    pause 0.5
  return

label quest_maxine_dive_armor_advance:
  $game.skip_events(force_go_home_allowed_events)
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's getting late. I can't wait to get home."
  if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
    "I just have to put [maxine]'s priced paper back in the stack first."
    "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    $mc.remove_item("paper")
    pause 0.25
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_kitchen"
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  return

label quest_maxine_dive_armor_pizza_boxes:
  "Time to dig down deep... into my gut. Literally."
  "And run a train on this pizza."
  $home_kitchen["pizza_boxes"]-=1
  "..."
  "Mmm... tastes divine."
  "One pizza goes down with ease."
  $home_kitchen["pizza_boxes"]-=1
  "..."
  "But halfway into the second, I start to feel full."
  mc "Come on, [mc]... you can do it..."
  "It would be a shame to let any go to waste."
  "Just one slice and then another."
  $home_kitchen["pizza_boxes"]-=1
  "..."
  "Mmm... still so... good..."
  window hide
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  pause 0.25
  show black onlayer screens zorder 100 with close_eyes
  pause 0.5
  hide black onlayer screens with open_eyes
  window auto
  "But I'm getting kind of sleepy..."
  "Just a few... more... bites..."
  $home_kitchen["pizza_boxes"]-=1
  "..."
  "Until there's nothing... but crumbs..."
  "Crumbs of pizza... crumbs of my sanity..."
  mc "{i}Buuuurp!{/}" with vpunch
  "Oh, man! That feels much better."
  window hide
  $mc.add_item("cardboard")
  pause 0.25
  window auto
  "Now that that's done, I just need a little nap..."
  window hide
  show black onlayer screens zorder 100 with close_eyes
  if 14 > game.hour > 6:
    while game.hour != 20:
      $game.advance()
  else:
    while game.hour != 7:
      $game.advance()
  pause 2.0
  hide black onlayer screens with open_eyes
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.625
  window auto
  mc "Mmm...?"
  if game.hour == 20:
    "Oh, man. Did I sleep through the day?"
  elif game.hour == 7:
    "Oh, man. Did I sleep through the night?"
  "[maxine] won't be happy about this."
  if game.hour == 20:
    # "First things first tomorrow, I better find a way to put all my cardboard — I mean, armor — pieces together."
    "First things first tomorrow, I better find a way to put all my cardboard{space=-35}\n— I mean, armor — pieces together."
  elif game.hour == 7:
    # "I better find a way to put all my cardboard — I mean, armor — pieces together."
    "I better find a way to put all my cardboard — I mean, armor — pieces{space=-20}\ntogether."
  "Some duct tape should do the trick..."
  return

label quest_maxine_dive_armor_teachers_desk:
  "I knew I could rely on this treasure trove of seemingly useless junk."
  "One man's trash is another man's... armor adhesive."
  window hide
  $mc.add_item("duct_tape")
  pause 0.25
  window auto
  "Now, I just need to assemble my armor."
  $quest.maxine_dive["armor_adhesive"] = True
  return

label quest_maxine_dive_armor_cardboard_duct_tape_combine(item,item2):
  $mc.remove_item(item)
  $mc.remove_item(item2)
  pause 0.25
  "With the most careful craftsmanship of the modern century..."
  "...a true, one of a kind, cardboard-smith..."
  "...carefully, delicately, the two materials fuse together."
  window hide
  $process_event("items_combined", item,item2, items_by_id["armor"])
  $mc.add_item("armor", silent=True)
  pause 0.25
  window auto
  "Silver and brown. Sticky and greasy."
  "And I've read that light armor is far superior to heavy."
  "This is the ultimate protection, both in terms of utility and fashion."
  "..."
  "It will have to do."
  return

label quest_maxine_dive_dive_on_quest_advanced:
  pause 0.25
  window auto

label quest_maxine_dive_dive:
  "Okay, [maxine] said she would meet me back here."
  "But I don't see any signs of her."
  "..."
  "Maybe this was all one big practical joke?"
  "And she does have a twisted sense of humor, after all."
  # "Honestly, I feel like an idiot, standing out here with weapons... wearing a suit of pizza boxes..."
  "Honestly, I feel like an idiot, standing out here with weapons... wearing{space=-45}\na suit of pizza boxes..."
  "Maybe I should just—"
  window hide
  $maxine["outfit_stamp"] = maxine.outfit
  $maxine.outfit = {"glasses":"maxine_glasses", "panties":"maxine_black_panties", "bra":"maxine_black_bra", "shirt":"maxine_armor"}
  show maxine sad_map at appear_from_right
  pause 0.5
  window auto
  "What the hell?!"
  "Look at her! She's literally wearing legit, full scale medieval armor!"
  mc "Where the hell did you get that?"
  maxine thinking_map "I told you. The school basement."
  mc "Not the map, the armor!"
  maxine thinking_map "I don't know where you keep your clothing, [mc], but I get mine from my closet."
  mc "..."
  mc "You know what? Nevermind."
  "I don't understand it or her, but I am weirdly impressed."
  # maxine skeptical "On the subject of proper attire, did I not tell you to wear some armor?"
  maxine skeptical "On the subject of proper attire, did I not tell you to wear some armor?{space=-40}"
  mc "This {i}is{/} my armor."
  maxine skeptical "Well, it's too late now."
  mc "I thought I did a pretty good job..."
  maxine eyeroll "You're lucky it's just the pond scum and not the forest trollitas."
  mc "The what, now?"
  maxine annoyed "Young girl trolls who lure men to their death."
  mc "..."
  maxine confident "Now, are you ready to take the dive?"
  mc "As ready as I'll ever be..."
  maxine confident "To the bell, then!"
  window hide
  show maxine confident at disappear_to_left
  pause 0.5
  window auto
  "Welp. I guess we're doing this."
  window hide
  hide screen interface_hider
  show misc pond_descend as pond_descend:
    ypos 0
  show misc diving_bell as diving_bell:
    xpos 861
  show misc pond_descend as foreground:
    crop (0,0,1920,36) ypos 0
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "A fleeting thought tumbles through my mind, as the diving bell lifts above us."
  "Do I really trust [maxine] enough with my life?"
  mc "Err, how old is this thing? Are you sure it's safe?"
  maxine "It's much more reliable and sturdy than any modern technology."
  mc "If you say so..."
  maxine "I did express it verbally and not telepathically."
  mc "..."
  "Her sanity — or lack of — doesn't do much for the confidence."
  "Oh, well. Here goes nothing..."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Together, we traipse into the icy water."
  "It soaks through, and freezes me to the core."
  "Teeth chattering, we swim into the shadow of the looming bell."
  "The bell hits the water, closing us in."
  window hide
  show misc pond_descend as pond_descend:
    linear 6.0 ypos -1080
  show misc pond_descend as foreground:
    linear 6.0 ypos -1080
  pause 6.0
  hide pond_descend
  show misc pond_descend as pond_descend behind diving_bell:
    ypos -1080
    linear 6.0 ypos -2160
    repeat
  window auto
  "And just like that, it starts to sink, taking us with it, inside a small bubble of air."
  maxine "It's splendid down here, don't you think?"
  mc "It's... a lot deeper than I thought it would be."
  maxine "Indeed. Looks can be deceiving."
  "I know she's literally talking about the pond..."
  "...but it resonates with me."
  "When the whole world judges you from a single first impression."
  "Based on how you look or hold yourself."
  "It's not easy—"
  window hide
  show misc pond_descend as pond_descend:
    ypos renpy.get_image_bounds("pond_descend")[1]
    linear (((2160 + renpy.get_image_bounds("pond_descend")[1]) * 6.0) / 1080) ypos -2160
  pause (((2160 + renpy.get_image_bounds("pond_descend")[1]) * 6.0) / 1080)
  hide pond_descend
  show misc pond_descend as pond_descend behind diving_bell with vpunch:
    ypos -2160
  pause 0.25
  window auto
  mc "[maxine]? What was that?"
  maxine "Hmmm..."
  mc "Oh, god, are we stuck?"
  "I feel my chest tightening. Oxygen depleting."
  "The breath wheezing from my lungs."
  mc "We're going to die, aren't we?"
  maxine "That is true."
  mc "What?!" with vpunch
  maxine "We are all eventually going to die."
  maxine "However, it appears the line of our bell is simply too short."
  maxine "We have to go back up."
  mc "..."
  mc "That was not the way to deliver that news."
  maxine "Would you have preferred an accent? Or perhaps Pig Latin?"
  maxine "Eway avehay otay ogay—"
  mc "Please, [maxine]! Just take us up, will you?"
  maxine "Ervay ellway."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  while game.hour != 19:
    $game.advance()
  $quest.maxine_dive.advance("line")
  pause 2.0
  show maxine sad at appear_from_left
  pause 0.0
  hide pond_descend
  hide diving_bell
  hide foreground
  hide black onlayer screens
  with Dissolve(.5)
  pause 0.25
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "[maxine] and I squelch out of the pond onto the grass."
  "Discarding our bell, inhaling the sweet, dusky air."
  "I honestly thought I was going to die for a moment there..."
  "...and I'm reminded just how grateful I am for this life."
  maxine sad "We need more line."
  mc "Do you think?"
  maxine thinking "It was not conjecture. It's fact."
  maxine thinking "I need you to acquire more line."
  mc "And what are {i}you{/} going to do?"
  maxine confident "Measure the pH of the water."
  mc "If it's not basic enough, try buying it a pumpkin spice latte."
  maxine skeptical "What purpose would that serve?"
  mc "Err, nevermind."
  mc "I'll get more line for us."
  maxine confident_hands_down "Tallyho!"
  window hide
  show maxine confident_hands_down at disappear_to_right
  pause 0.5
  window auto
  "Where can I even get an extra feet of line, anyway?"
  "..."
  if mc.owned_item("ball_of_yarn"):
    "From my backpack, perhaps. That's the advantage of being a kleptomaniac."
    "Regardless, it's getting late. I can't wait to get home."
  else:
    "Oh, well. That's a problem for future [mc]."
    "It's getting late. I can't wait to get home."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  if mc.owned_item("ball_of_yarn"):
    $quest.maxine_dive.advance("dive_2_electric_boogaloo")
  $maxine.outfit = maxine["outfit_stamp"]
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $game.advance()
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  return

label quest_maxine_dive_line:
  show maya neutral with Dissolve(.5)
  mc "[maya]! Can I have some of your yarn?"
  maya annoyed "What the hell are you wearing?"
  maya annoyed "Is this some weird roleplaying thing?"
  maya annoyed "I don't know what you've heard, but this isn't the kind of roleplay I'm into."
  maya smile_hands_in_pockets "Although, I guess I do always go for trash."
  mc "Rude."
  mc "This isn't roleplay, okay? I just really need some yarn."
  maya flirty "Oh! I know what this is!"
  maya flirty "Cat scratch fever."
  # if quest.maya_decoding.finished:
  if False: ## Placeholder ## Decoding Maya ## jotapê ##
    # mc "Heh. Is that what you did to me?"
    # maya kiss "Never! I'm innocent!"
    # mc "Mm-hmm."
    pass
  mc "Thankfully, I've had all my shots."
  maya kiss "So domesticated!"
  mc "Heh. Compared to you, maybe."
  mc "Anyway, I'm serious. I do need your yarn."
  maya thinking "And here I thought you were already yanking my string..."
  mc "Sounds gross."
  mc "So, can I have it?"
  maya thinking "What do you think?"
  show maya thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I think you owe me one.\"":
      show maya thinking at move_to(.5)
      mc "I think you owe me one."
      maya dramatic "I do? Since when?"
      mc "Since you always give me a hard time."
      maya cringe "Me?! I would never!"
      maya sarcastic "I'm the one who likes it hard, don't you know?"
      mc "Long, hard, and slow. I do know it."
      $maya.love+=1
      maya sarcastic "Now we're talking!"
    "\"Err, that I'll owe you one?\"":
      show maya thinking at move_to(.5)
      mc "Err, that I'll owe you one?"
      maya sarcastic "Oh, now you're really creaming my milk!"
      mc "Heh. Cream."
      maya dramatic "It goes down easier than man cream."
      mc "Maybe, but that's not as fun."
      $maya.love-=1
      maya dramatic "Milk is more fun to drink out of a bowl, anyway."
    "\"I think you can't resist this face.\"":
      show maya thinking at move_to(.5)
      mc "I think you can't resist this face."
      maya dramatic "It is a face that inspires panty-dropping pity, I suppose."
      mc "Hey!"
      maya cringe "What?"
      maya cringe "I'm all about hard truths and hard dicks."
      mc "Well, maybe you can give me one and I'll give you the other?"
      $maya.lust+=1
      maya sarcastic "How generous!"
  mc "So, can I have it?"
  maya bored "Have what?"
  mc "Please, [maya]? I don't have much time..."
  maya skeptical "That's because you're a fellow yarn addict."
  maya skeptical "Just looking for a quick fix, aren't you?"
  mc "..."
  maya skeptical "..."
  maya bored "Fine. I'll leave a ball of yarn for you in the homeroom."
  maya bored "But I want a fresh bottle of goat milk left in its place."
  mc "Err, why goat, specifically?"
  maya blush "Because a warm bowl of goat milk is superior to any other!"
  mc "That's weird, but okay..."
  maya confident "If you think that's weird, you should see what I do with some tuna."
  mc "..."
  maya bored "So, deal or no deal?"
  mc "Fine. Deal."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "I should have known that [maya] was a milk drinker..."
  "It doesn't make much sense why it has to be goat milk, though."
  "Milk is milk, isn't it?"
  menu quest_maxine_dive_line_menu(side="middle"):
    extend ""
    "Get some from home":
      "I'm pretty sure [jo] just restocked the fridge with groceries..."
      "That's the simplest, best option."
      if quest.maxine_dive["got_detention"]:
        $quest.maxine_dive["cafeteria_milk"] = False
        $quest.maxine_dive.advance("line")
      $quest.maxine_dive["kitchen_milk"] = True
    "Get some from the cafeteria" if not quest.maxine_dive["got_detention"]:
      "The school is a resource ripe with the fruit of others' labor..."
      "As a student, I have a right to that labor."
      $quest.maxine_dive["cafeteria_milk"] = True
    "Get some online":
      "[maya] is really helping me out by sharing some of her yarn with me..."
      "I better do the right thing and actually get her what she asked for."
      if quest.maxine_dive["got_detention"]:
        $quest.maxine_dive["cafeteria_milk"] = False
        $quest.maxine_dive.advance("line")
      $quest.maxine_dive["black_market_milk"] = True
  $quest.maxine_dive.advance("milk")
  return

label quest_maxine_dive_milk_kitchen_milk_upon_entering:
  "I didn't think turning into the milkman would be on my list of things to do today."
  "Maybe [flora] was right about who my dad really is and I'm following in his footsteps?"
  $quest.maxine_dive["following_in_his_footsteps"] = True
  return

label quest_maxine_dive_milk_kitchen_milk:
  "Ah, here it is!"
  "Thick, white, and creamy..."
  "[maya]'s favorite thing to swallow."
  window hide
  $mc.add_item("milk")
  pause 0.25
  window auto
  "Okay, time to make the drop."
  # "There's no way [maya] will notice this doesn't come from a goat, right?"
  "There's no way [maya] will notice this doesn't come from a goat, right?{space=-30}"
  $quest.maxine_dive.advance("trade")
  return

label quest_maxine_dive_milk_cafeteria_milk:
  "Remember, [mc]. Get in and out."
  "I'm here for the liquid white. No other distractions."
  "Even if my stomach does protest..."
  "I'll just pop back behind the counter, and—"
  window hide
  show guard surprised at appear_from_right
  pause 0.5
  window auto
  guard surprised "Hey! You're not allowed back there!"
  guard angry "Wait, are you stealing from the cafeteria?!"
  mc "Err, I wasn't! It's not—"
  guard suspicious "I don't want to hear the excuses."
  guard suspicious "You better come with me right now."
  "Well, fuck."
  "So much for in and out..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_ground_floor"
  $quest.maxine_dive["got_detention"] = True
  $mc["detention"]+=1
  pause 1.0
  hide guard
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "Welp. That was an epic fail."
  "I guess I better think of a different plan..."
  "If not milk from the cafeteria, where else?"
  jump quest_maxine_dive_line_menu

label quest_maxine_dive_milk_black_market_milk:
  "On the dark web I can buy any kind of milk I want."
  "..."
  "Which is a little disturbing if you think about it too hard."
  "Thankfully, I don't."
  if not home_computer["visited"]:
    "Let's see here... I just need to get that shady site pulled up..."
    "..."
    "..."
    "Ah, perfect! Here we go."
  $quest.maxine_dive["shady_site_pulled_up"] = True
  if home_computer["visited"]:
    return
  else:
    jump goto_home_computer

label quest_maxine_dive_milk_black_market_milk_milk:
  show screen black_market(order_shipped="milk")
  "[maya] doesn't usually ask for a whole lot, and her yarn is precious to her."
  "The least I can do is pay her back with the purest goat milk on the interweb."
  "Now, to wait for the delivery..."
  $quest.maxine_dive.advance("package")
  return

label quest_maxine_dive_milk_black_market_milk_goat:
  show screen black_market(order_shipped="goat")
  "Why settle for just the milk, when you can have the whole goat?"
  "Now, to wait for the delivery..."
  $quest.maxine_dive.advance("package")
  return

label quest_maxine_dive_package:
  if jo.location == "home_kitchen":
    show jo angry with Dissolve(.5)
  else:
    show jo angry at appear_from_left
    pause 0.5
  jo angry "[mc], what is this?"
  show jo angry at move_to(.75)
  menu(side="left"):
    extend ""
    "\"It's for, err... a science project?\"":
      show jo angry at move_to(.5)
      mc "It's for, err... a science project?"
      jo thinking "With live animals?"
      $mc.intellect+=1
      mc "Sure! We're building an ecosystem."
      jo thinking "That sounds messy."
      mc "I'll be sure to clean up after it, okay?"
      jo sad "All right. You better, young man."
    "\"It's a goat.\"":
      show jo angry at move_to(.5)
      mc "It's a goat."
      $jo.love-=1
      jo annoyed "Don't get smart with me."
      mc "Should I get dumb, then?"
      jo annoyed "This is a home, not a farm, young man."
      jo annoyed "Get rid of it."
      mc "Okay, okay! I will!"
      "...at some point."
      jo worried "Good."
    "\"I thought it would be a nice experience...\"":
      show jo angry at move_to(.5)
      mc "I thought it would be a nice experience..."
      jo thinking "A nice experience?"
      mc "Yeah! Learning how to make goat cheese for you, and lotions, too."
      jo thinking "Lotions?"
      mc "A small way to pamper you, since you do so much for us."
      jo eyeroll "I don't know, [mc]..."
      mc "You won't even know it's here, okay?"
      mc "Just let me do this for you."
      jo blush "Well, I suppose it is pretty thoughtful..."
      $jo.love+=1
      jo blush "Oh, all right! Fine!"
  window hide
  if jo.location == "home_kitchen":
    hide jo with Dissolve(.5)
  else:
    if renpy.showing("jo sad"):
      show jo sad at disappear_to_left
    elif renpy.showing("jo worried"):
      show jo worried at disappear_to_left
    elif renpy.showing("jo blush"):
      show jo blush at disappear_to_left
    pause 0.5
  window auto
  "Okay, now that that's taken care of..."
  "I guess I just need to, err... milk this thing."
  $quest.maxine_dive["taken_care_of"] = True
  return

label quest_maxine_dive_trade:
  "As promised, [maya] left the yarn where she said she would."
  "And in return, I leave my humble offering."
  if home_computer["goat_ordered"]:
    "...which was much harder to extract than I thought it would be."
  window hide
  $quest.maxine_dive.advance("dive_2_electric_boogaloo")
  $mc.add_item("ball_of_yarn")
  $mc.remove_item("milk")
  pause 0.25
  window auto
  "Puh, okay! I have the extra line for the bell."
  "Time to go meet [maxine]... again."
  return

label quest_maxine_dive_dive_2_electric_boogaloo:
  $maxine["outfit_stamp"] = maxine.outfit
  $maxine.outfit = {"glasses":"maxine_glasses", "panties":"maxine_black_panties", "bra":"maxine_black_bra", "shirt":"maxine_armor"}
  show maxine eyeroll with Dissolve(.5)
  maxine eyeroll "You're late."
  mc "How did you know to be here already?"
  maxine annoyed "I'm operating on maritime, of course."
  mc "That doesn't make any sense..."
  maxine annoyed "In any case, someone had to keep an eye on the pond."
  mc "Why? I don't think it's going anywhere."
  maxine thinking "Are you unfamiliar with the stages of the water cycle?"
  mc "Err, no?"
  maxine thinking "Then you don't know how moody it can be."
  mc "I don't think we're referring to the same cycle..."
  maxine sad "Anyway, we better dive before it gets angry."
  mc "That sounds very safe."
  maxine thinking "It won't be. Hence the weapons."
  mc "Right."
  mc "Speaking of, here's yours."
  window hide
  $mc.remove_item("fire_axe")
  pause 0.25
  window auto
  mc "And the extra line, as promised."
  window hide
  $mc.remove_item("ball_of_yarn")
  pause 0.25
  window auto show
  show maxine confident with dissolve2
  maxine confident "Thank you. Let's go!"
  window hide
  show maxine confident at disappear_to_left
  pause 0.5
  window auto
  "Here goes nothing... again..."
  "Back once more to the depths."
  window hide
  show misc pond_descend as pond_descend:
    ypos 0
  show misc diving_bell as diving_bell:
    xpos 861
  show misc pond_descend as foreground:
    crop (0,0,1920,36) ypos 0
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 1.0
  hide maxine
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "Oh, god. It's even colder than I remember."
  "I'm eerily reminded of what it's like to be close to death."
  "Lost in the throes of darkness. Cold, alone."
  "Everything you knew slipping away from you."
  "The good and the bad."
  "Left only with fear and regret..."
  maxine "Ready to take off!"
  window hide
  show misc pond_descend as pond_descend:
    linear 6.0 ypos -1080
  show misc pond_descend as foreground:
    linear 6.0 ypos -1080
  pause 6.0
  hide pond_descend
  show misc pond_descend as pond_descend behind diving_bell:
    ypos -1080
    linear 6.0 ypos -2160
    repeat
  window auto
  "...only I'm not alone, am I?"
  "And this {i}is{/} an adventure."
  "Pushing my own boundaries, going out on a limb with [maxine]."
  "Really living my life this time."
  "..."
  # mc "Oh? Have we made it past the point when our line stopped last time?"
  mc "Oh? Have we made it past the point when our line stopped last time?{space=-20}"
  maxine "We have, indeed."
  maxine "We are sinking to new, previously unexplored depths!"
  "We continue to descend in silence for a moment."
  "The anticipation and adrenaline ringing in my ears."
  maxine "Prepare for touchdown!"
  window hide
  show misc pond_descend as pond_descend:
    ypos renpy.get_image_bounds("pond_descend")[1]
    linear (((2160 + renpy.get_image_bounds("pond_descend")[1]) * 6.0) / 1080) ypos -2160
  pause (((2160 + renpy.get_image_bounds("pond_descend")[1]) * 6.0) / 1080)
  hide pond_descend
  show misc pond_descend as pond_descend behind diving_bell:
    ypos -2160
    linear 6.0 ypos -3240
  pause 6.0
  hide pond_descend
  show misc pond_descend as pond_descend behind diving_bell with vpunch:
    ypos -3240
  pause 0.25
  window auto
  "Seconds later, the bell hits the bottom of the pond."
  "It crashes into the sand and seaweed, sending a shock through me."
  maxine "We have impact!"
  mc "Err, now what?"
  maxine "According to my map, the cave is... this way."
  mc "C-cave?"
  maxine "Indeed. It's where the treasure is kept."
  mc "Oh, right. The treasure."
  # "We push on with the bell until [maxine] stops us again."
  # "Eyes focused on her map."
  maxine "Well, this is it."
  maxine "Are you prepared, [mc]?"
  menu(side="middle"):
    extend ""
    "\"I trust you with my life.\"":
      mc "I trust you with my life."
      mc "Lead the way."
      maxine "You should probably be more careful with it."
      maxine "Your life is very breakable."
      "Profound."
      mc "I guess..."
      maxine "Anyway, it's far too late to turn back now."
      mc "The life of a treasure seeker, right?"
      $maxine.lust+=1
      maxine "And truth finder!"
      maxine "Let's go?"
      mc "Right behind you!"
    "\"Maybe we should turn back?\"":
      mc "Maybe we should turn back?"
      maxine "Why would we turn back? There's nothing but water behind us."
      mc "I meant, err... turn up?"
      maxine "We just turned up here."
      mc "Let's go back up to the surface, [maxine]?"
      $maxine.lust-=1
      maxine "Oh, I'm afraid it's far too late for that."
      mc "..."
      mc "Great. Just great."
      maxine "It's time. Let's go."
      mc "Right behind you..."
  window hide
  hide screen notification_side
  hide screen interface_hider
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  play sound "<to 16.0>underwater_movement" loop
  $set_dialog_mode("default_no_bg")
  "We swim out of the diving bell into the dark depths of the pond."
  "The only sign of where I'm going is the turbulence in the water from [maxine]'s kicks."
  "And as the air in my lungs starts to drain, panic creeps into my senses."
  "What if there's no cave?"
  "What if [maxine] was wrong?"
  "What if—"
  $game.location = "school_cave"
  $quest.maxine_dive.advance("cave")
  $quest.maxine_dive["cave_interactables"] = set()
  $mc["focus"] = "maxine_dive"
  show black onlayer screens zorder 100
  stop sound fadeout 0.5
  pause 0.5
  hide pond_descend
  hide diving_bell
  hide foreground
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  pause 0.125
  "Holy shit."
  # "I can't believe this place actually exists... and that [maxine]'s map was accurate."
  "I can't believe this place actually exists... and that [maxine]'s map was{space=-10}\naccurate."
  "It's a whole other world down here!"
  "Full of mystery and wonder!"
  "..."
  "It leaves my stomach fluttering with nervous anticipation..."
  return

label quest_maxine_dive_cave:
  show maxine excited with Dissolve(.5)
  mc "I have to admit, I did not expect to find this cave down here."
  maxine excited "The maps do not lie."
  mc "Heh. I guess they don't."
  mc "I'm impressed."
  maxine excited "Use the impression of your eyes to look around, will you?"
  mc "What are we looking for?"
  maxine concerned "Do you still not know?"
  mc "I do know! It's, err..."
  show maxine concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"...the truth.\"":
      show maxine concerned at move_to(.5)
      mc "...the truth."
      maxine excited "You're starting to see it, and I am starting to understand it."
      maxine excited "These are the first steps!"
      mc "I believe you."
      mc "And I'm looking forward to discovering more."
      maxine blush "In discovery there is banana pudding."
      mc "I don't know what that means, but I like it."
      $maxine.lust+=1
      maxine flirty "It means I'll share one with you once we get back to the surface."
      mc "I can't wait."
    "\"...your panties.\"":
      show maxine concerned at move_to(.5)
      $mc.lust+=1
      mc "...your panties."
      maxine sad "There is a possibility that the wolf lichen carried them away."
      mc "Wait, what?"
      maxine thinking "If you find them, please do return them."
      mc "Uh, okay."
    "\"...sunken treasure.\"":
      show maxine concerned at move_to(.5)
      mc "...sunken treasure."
      maxine concerned "And?"
      mc "Uh, glorious riches?"
      maxine concerned "..."
      $maxine.love-=1
      maxine concerned "Perhaps I overestimated you."
      mc "Well, that's your fault."
  if renpy.showing("maxine flirty"):
    maxine excited "Let's take a look around, shall we?"
    maxine excited "Report your findings back to me."
  else:
    maxine thinking "Let's take a look around, shall we?"
    maxine thinking "Report your findings back to me."
  mc "You got it."
  window hide
  hide maxine with Dissolve(.5)
  window auto
  "It's not every day you get to explore an underwater cave."
  "Let's see what we have here..."
  $quest.maxine_dive["cave_interactables"].add("maxine")
  return

label quest_maxine_dive_cave_upon_exploring:
  show maxine excited with Dissolve(.5)
  maxine excited "That's it! Another box, just as the map promised!"
  mc "The treasure, huh? I wonder what's in it?"
  maxine flirty "The question is not what, but—"
  window hide
  show location with vpunch
  window auto show
  show maxine sad with dissolve2
  "The words die on her lips as the ground beneath our feet trembles."
  mc "Err, what was that?"
  maxine sad "It could be the underground railroad."
  mc "..."
  mc "That is not what that was."
  maxine thinking "Aquatic railway travel has come very far."
  mc "I don't—"
  window hide
  hide screen interface_hider
  show maxine cave_fight earthworm_blur
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.5
  show black onlayer screens zorder 4
  $mc["focus"] = ""
  $set_dialog_mode("default_no_bg")
  "The earth shivers again."
  "A slithering tremor rolling through the rock..."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  $set_dialog_mode("")
  window auto show
  show maxine cave_fight earthworm with {"master": Dissolve(2.0)}
  # $set_dialog_mode("")
  # "...and then, from beneath that rock, bursts the most hideous creature I have ever seen."
  "...and then, from beneath that rock, bursts the most hideous creature{space=-30}\nI have ever seen."
  # "A giant, pale, fat worm wriggles out of the shattered rock and whips its head around."
  "A giant, pale, fat worm wriggles out of the shattered rock and whips{space=-5}\nits head around."
  "Obviously blind. Obviously searching for a meal."
  mc "I can't believe I'm saying this, [maxine], but grab your weapon!"
  window hide
  # show maxine cave_fight first_strike
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  play sound "fire_axe_hit"
  hide black onlayer screens
  show maxine cave_fight first_strike: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  with Dissolve(.5)
  hide maxine
  show maxine cave_fight first_strike ## This is just to avoid the screen shake if the player is skipping
  window auto
  "I hardly need to tell her, though, because she already has her axe in hand, and is hacking away at the beast."
  # "However, it seems to do little more than tickle it and anger it further."
  "However, it seems to do little more than tickle it and anger it further.{space=-15}"
  "Dread and horror shakes me at my core. Do I run? Do I fight?"
  "I can't leave [maxine] behind, even though this was her stupid idea."
  window hide
  pause 0.125
  show maxine cave_fight second_strike_incoming with Dissolve(.5)
  play sound "machete_hit"
  show maxine cave_fight second_strike with vpunch
  pause 0.25
  window auto
  mc "Aaaaaaaah! [maxine]!"
  maxine cave_fight second_strike "Fight or get eaten, [mc]!"
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike with vpunch
  show maxine cave_fight third_strike_aftermath with Dissolve(.5)
  pause 0.25
  window auto
  "Oh, god. It's like cutting through fat."
  "How the fuck did I end up here, battling a giant worm?"
  window hide
  pause 0.125
  play sound "machete_hit"
  show maxine cave_fight fourth_strike with vpunch
  pause 0.25
  window auto
  "It coils around the cavern, hideously slithering along the walls."
  "It bites blindly, threatening to swallow anything that falls into its vile mouth."
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike_aftermath with vpunch
  pause 0.25
  window auto
  "This is it! I'm going to die down here!"
  window hide
  pause 0.125
  play sound "machete_hit"
  show maxine cave_fight fourth_strike with vpunch
  pause 0.25
  window auto
  "I'm ready to run and hide, but somewhere in the chaos, I catch a glimpse of [maxine]'s face."
  "The fearless determination in her eyes invigorates me."
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike_aftermath with vpunch
  pause 0.25
  window auto
  "In a dream-like state, my body moves on its own, dodging, weaving, hacking and slashing."
  "This is not how my second chance at life ends. Not inside the belly of this freak of nature."
  window hide
  pause 0.125
  play sound "machete_hit"
  show maxine cave_fight fourth_strike with vpunch
  pause 0.25
  window auto
  # "Knees weak, arms heavy, as I chop, and chop, and chop, and chop..."
  "Knees weak, arms heavy, as I chop, and chop, and chop, and chop...{space=-5}"
  "...until my face and eyes sting with sweat, tears, and worm juices."
  window hide
  pause 0.125
  play sound "fire_axe_hit"
  show maxine cave_fight third_strike_aftermath with vpunch
  pause 0.25
  window auto
  # "[maxine] is nearby, breathing heavily, crying out as she fights like hell."
  "[maxine] is nearby, breathing heavily, crying out as she fights like hell.{space=-5}"
  "As she fights to the death."
  window hide
  pause 0.125
  show maxine cave_fight fifth_strike_incoming with Dissolve(.5)
  pause 0.25
  hide screen interface_hider
  play sound "fire_axe_hit"
  play audio "machete_hit"
  show maxine afraid_blood
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "In a final chomp that barely misses my head, the worm collapses on the cave floor."
  "Leaking its vile insides onto the rock."
  "Breaths rattle through my mouth, sucking in the fetid air."
  "My hair and skin drenched in worm blood."
  "But I'm alive in glorious victory."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "What... the fuck... was that?!"
  maxine afraid_blood "A giant earthworm."
  mc "But why?!"
  maxine thinking_blood "Are you asking why was it an earthworm?"
  mc "Why was it huge, [maxine]?!"
  maxine sad_blood "Oh. I am not sure."
  maxine sad_blood "I have several hypotheses, but without the necessary control—"
  mc "You know what? Nevermind."
  mc "I'm just glad we're okay. I was worried about you."
  maxine concerned_blood "..."
  "She goes uncharacteristically silent."
  "We stare at each other for a moment."
  "Both of us covered in the ichor of the worm."
  "The adrenaline from the fight still pumping through me."
  show maxine concerned_blood at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Can we get out of here, now?\"":
      show maxine concerned_blood at move_to(.5)
    "?maxine.lust>=12@[maxine.lust]/12|{image=maxine contact_icon}|{image=stats lust_3}|Kiss her":
      show maxine concerned_blood at move_to(.5)
      "This feels like one of those moments in the movies, when the lead character just leans in and kisses the girl."
      window hide
      show maxine cave_kiss
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "It all happens so fast. He pulls her in, and then presses his lips against hers."
      "And the audience breathes a sigh of relief. It's the moment they've all been waiting for."
      "Not the action or fighting scenes. The simple kiss of victory."
      "At first, [maxine] is stiff."
      "But then she melts into me a little more and begins to kiss me back."
      "The adrenaline of the fight and the discovery is probably doing something to her, too."
      "Her salted caramel lips taste like heaven and block out the stench of the dead worm."
      "My shaking hands explore her soft hips and waist."
      "And then she's really giving into the passion of the moment."
      "Her lips push hard against mine. Her tongue darting out to tease."
      "She leans into me, making me support her weight."
      "Pressing her armored chest against me."
      window hide
      hide screen interface_hider
      play sound "falling_thud"
      show maxine cave_sex anticipation
      show black
      show black onlayer screens zorder 100
      with vpunch
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "The force of her kiss overwhelms me in my weakened state, and we tumble onto the floor."
      "Our eyes meet for a moment, and then we're tearing at each other's clothes and armor."
      show black onlayer screens zorder 100
      pause 0.5
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      "My eyes and hands fully take [maxine] in."
      "Her soft body beneath the armor... her smooth legs... her warm crotch..."
      window hide
      pause 0.125
      show maxine cave_sex insertion1 with Dissolve(.5)
      pause 0.25
      window auto
      "She lifts herself up, and slowly impales herself on my dick."
      "Sliding her slick outer lips back and forth on it."
      mc "Ready for our next adventure?"
      show maxine cave_sex insertion1 smile with dissolve2
      "She smiles knowingly down at me..."
      window hide
      pause 0.125
      show maxine cave_sex insertion2 with Dissolve(.25)
      show maxine cave_sex insertion3 with vpunch
      pause 0.25
      window auto
      mc "Oh, f-fuck!"
      # "The weight of her coming down on me feels even better as I fill her up."
      "The weight of her coming down on me feels even better as I fill\nher up."
      "Inch by inch, she adjusts herself around my throbbing shaft, until her ass hits my thighs."
      show maxine cave_sex insertion3 flirty with dissolve2
      "She then sits there for a moment."
      "Her tight pussy squeezing me, clenching and unclenching as it gets accustomed to the invasion."
      # maxine cave_sex insertion3 flirty "The heat of battle really aroused you. I didn't think you had it in you."
      maxine cave_sex insertion3 flirty "The heat of battle really aroused you. I didn't think you had it in you.{space=-10}"
      maxine cave_sex insertion3 flirty "It will make an interesting addition to your fi—"
      mc "Yes, yes, [maxine]!"
      mc "Just ride me, will you?"
      maxine cave_sex insertion3 flirty "A well earned outcome for your valor."
      "I can feel the pressure mounting in my balls..."
      "...driving me insane as she sits there, hugging my dick tightly..."
      window hide
      pause 0.125
      show maxine cave_sex penetration1 with Dissolve(.5)
      pause 0.25
      show maxine cave_sex penetration2 with Dissolve(.175)
      show maxine cave_sex penetration3 with Dissolve(.25)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.175)
      show maxine cave_sex penetration1 with Dissolve(.25)
      pause 0.5
      show maxine cave_sex penetration2 with Dissolve(.175)
      show maxine cave_sex penetration3 with Dissolve(.25)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.175)
      show maxine cave_sex penetration1 with Dissolve(.25)
      pause 0.5
      show maxine cave_sex penetration2 with Dissolve(.175)
      show maxine cave_sex penetration3 with Dissolve(.25)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.175)
      show maxine cave_sex penetration1 with Dissolve(.25)
      pause 0.25
      window auto
      "...but thankfully, she does exactly as I ask."
      "She places her hands on my chest and begins to grind on my dick."
      "Slowly at first, gyrating her hips back and forth."
      mc "Oh, god. That's it! Just like that!"
      window hide
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration3 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration1 with Dissolve(.175)
      pause 0.25
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration3 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration1 with Dissolve(.175)
      pause 0.25
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration3 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration1 with Dissolve(.175)
      pause 0.25
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration3 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration1 with Dissolve(.175)
      pause 0.25
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration3 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration2 with Dissolve(.125)
      show maxine cave_sex penetration1 with Dissolve(.175)
      pause 0.25
      window auto show
      show maxine cave_sex penetration1 aroused with dissolve2
      "Who knew that eccentric, cryptic, strange [maxine] could ride a cock so well?"
      "Like everything she does, she goes about it in a methodical way."
      "Squeezing and releasing me at intervals."
      # "Rocking back and forth while she gains momentum, before suddenly stopping."
      "Rocking back and forth while she gains momentum, before suddenly{space=-15}\nstopping."
      "After she does, she holds me there for a minute."
      window hide
      pause 0.125
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration3 aroused with Dissolve(.175)
      pause 0.5
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration1 aroused with vpunch
      pause 0.5
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration3 aroused with Dissolve(.175)
      pause 0.25
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration1 aroused with vpunch
      pause 0.25
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration3 aroused with Dissolve(.175)
      pause 0.175
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration1 aroused with vpunch
      pause 0.175
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration3 aroused with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration2 aroused with Dissolve(.125)
      show maxine cave_sex penetration1 aroused with vpunch
      pause 0.25
      window auto
      # "Before she starts to bounce again, going up and down on my length."
      "Before she starts to bounce again, going up and down on my length.{space=-15}"
      "Each time she comes down is harder than the last."
      "And she grinds her clit down into me."
      mc "Y-yes! Goddamn, [maxine]!"
      "If this is how the weirdos fuck, the rest of society is definitely missing out."
      window hide
      pause 0.125
      show maxine cave_sex penetration4 with Dissolve(.375)
      pause 0.125
      show maxine cave_sex penetration5 with Dissolve(.125)
      show maxine cave_sex penetration6 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration5 with Dissolve(.125)
      show maxine cave_sex penetration4 with vpunch
      pause 0.125
      show maxine cave_sex penetration5 with Dissolve(.125)
      show maxine cave_sex penetration6 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration5 with Dissolve(.125)
      show maxine cave_sex penetration4 with vpunch
      pause 0.125
      show maxine cave_sex penetration5 with Dissolve(.125)
      show maxine cave_sex penetration6 with Dissolve(.175)
      pause 0.125
      show maxine cave_sex penetration5 with Dissolve(.125)
      show maxine cave_sex penetration4 with vpunch
      pause 0.25
      window auto
      "I grasp her thighs and start to piston up into her sopping wet folds."
      maxine cave_sex penetration4 "N-no, don't!"
      maxine cave_sex penetration4 "I am close to peak arousal, and I know how best to achieve it if you lie still."
      mc "..."
      "It's not exactly sexy talk, but somehow I just seem to stiffen even more within her."
      "My own climax cresting."
      mc "F-fine, but I'm about to—"
      window hide
      pause 0.125
      show maxine cave_sex penetration7 with Dissolve(.375)
      pause 0.125
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration9 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration7 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration9 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration7 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration9 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration7 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration9 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration7 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration9 with Dissolve(.125)
      pause 0.0625
      show maxine cave_sex penetration8 with Dissolve(.1)
      show maxine cave_sex penetration7 with Dissolve(.125)
      pause 0.25
      window auto
      mc "...oooooh!"
      "Before I can even finish speaking, she's back to maneuvering my dick exactly how she wants."
      "Going faster and faster, back and forth on my dick."
      "Sweat rolling down her pale skin."
      "Between her bouncing tits, whose nipples are hard as diamonds."
      show maxine cave_sex penetration7 aroused with dissolve2
      maxine cave_sex penetration7 aroused "..."
      "Suddenly, she stops."
      # "Her legs clenching against my hips, her pussy locking up around me."
      "Her legs clenching against my hips, her pussy locking up around me.{space=-15}"
      window hide
      pause 0.125
      show maxine cave_sex orgasm with vpunch
      pause 0.25
      window auto
      maxine cave_sex orgasm "Mmmmmmmmm!"
      # "She shudders and closes her eyes as the orgasm hits her full force."
      "She shudders and closes her eyes as the orgasm hits her full\nforce."
      "Her mouth hangs open, tremors cascading throughout her entire body."
      "As she sits there, holding me inside of her, I feel my own tidal wave hitting."
      mc "[maxine], I-I'm about to—"
      maxine cave_sex orgasm concerned "Inside! You have to ejaculate inside!"
      menu(side="middle"):
        extend ""
        "Do as she says":
          # "She doesn't have to tell me twice. Even if I wanted to hold back, I couldn't."
          "She doesn't have to tell me twice. There's no better feeling than cumming inside of a warm, wet pussy."
          "Even if I wanted to hold back, I couldn't."
          window hide
          pause 0.125
          show maxine cave_sex penetration10 with Dissolve(.375)
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration12 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration10 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration12 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration10 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration12 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration10 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration12 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration10 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration12 with Dissolve(.125)
          pause 0.0625
          show maxine cave_sex penetration11 with Dissolve(.1)
          show maxine cave_sex penetration10 with Dissolve(.125)
          show maxine cave_sex cum_inside with vpunch
          pause 0.25
          window auto
          "An upward torrent of seed gushes into her waiting womb."
          "Her pussy seems to drink it up, milking me for every last drop."
          $maxine.love+=1
          maxine cave_sex cum_inside flirty "Yes! Good! T-that's very good."
          mc "Mmm... it really is..."
          maxine cave_sex cum_inside flirty "We can't leave any behind, or else the crab rocks will consume it and multiply."
          maxine cave_sex cum_inside flirty "We don't want an infestation, do we?"
          mc "..."
          mc "Err, right. Of course not."
        "Pull out":
          "[maxine] is hot in a crazy kind of way..."
          "...but I am not getting trapped if she gets pregnant or something."
          window hide
          show maxine cave_sex stroke1
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          hide black onlayer screens with Dissolve(.5)
          window auto
          pause 0.125
          show maxine cave_sex stroke2 with Dissolve(.175)
          show maxine cave_sex stroke1 with Dissolve(.25)
          pause 0.03125
          show maxine cave_sex stroke2 with Dissolve(.175)
          show maxine cave_sex stroke1 with Dissolve(.25)
          pause 0.03125
          show maxine cave_sex stroke2 with Dissolve(.175)
          show maxine cave_sex stroke1 with Dissolve(.25)
          pause 0.03125
          show maxine cave_sex stroke2 with Dissolve(.175)
          show maxine cave_sex stroke1 with Dissolve(.25)
          pause 0.03125
          show maxine cave_sex stroke2 with Dissolve(.175)
          show maxine cave_sex cum_outside with hpunch
          show maxine cave_sex cum_outside_aftermath with Dissolve(.5)
          window auto
          "She gasps and looks up at the cave ceiling as my dick pops out of her pussy with a slurping sound."
          "Her eyes glaze over from the orgasm as my seed coats her belly."
          mc "Mmm, yes! Fuck, that felt good!"
          $maxine.love-=1
          show maxine cave_sex cum_outside_aftermath annoyed with dissolve2
          maxine cave_sex cum_outside_aftermath annoyed "Did I not tell you to stay inside?"
          mc "Sorry, that's hot and all, but—"
          show maxine cave_sex cum_outside_aftermath angry with dissolve2
          maxine cave_sex cum_outside_aftermath angry "Now the crab rocks will consume it and multiply!"
          maxine cave_sex cum_outside_aftermath angry "There will be an infestation!"
          mc "Oh. Whoops."
          $quest.maxine_dive["crab_rocks_infestation"] = True
      window hide
      hide screen notification_side
      hide screen interface_hider
      if quest.maxine_dive["crab_rocks_infestation"]:
        show maxine sad_blood
      else:
        show maxine blush_blood
      show black
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "Once we both come down, and the heat of the moment starts to fade, we pull our bloody clothes back on."
      show black onlayer screens zorder 100
      pause 0.5
      hide black
      hide black onlayer screens
      show screen interface_hider
      with Dissolve(.5)
      window auto
      $set_dialog_mode("")
      mc "That was, err... really something, huh?"
      if renpy.showing("maxine sad_blood"):
        maxine thinking_blood "Your sword wielding prowess was better than I anticipated."
      elif renpy.showing("maxine blush_blood"):
        maxine flirty_blood "Your sword wielding prowess was better than I anticipated."
      "I'm not sure if she means my sword or my {i}sword{/}..."
      "Knowing [maxine], it's probably the first one."
      mc "Heh. Thanks."
      $unlock_replay("maxine_adventure")
      $quest.maxine_dive["well_earned_outcome"] = True
  mc "Can we get out of here, now?"
  maxine confident_blood "Indeed."
  show maxine skeptical_blood with dissolve2
  extend " Unless you know how to enter that door?"
  mc "Err, I do not."
  maxine confident_hands_down_blood "Very well. There's research to be done, then."
  window hide
  show maxine confident_hands_down_blood at disappear_to_right
  pause 0.5
  show black onlayer screens zorder 100 with Dissolve(.5)
  while game.hour != 19:
    $game.advance()
  $game.location = "school_park"
  pause 2.0
  show maxine confident_hands_down_blood at appear_from_left
  pause 0.0
  hide black onlayer screens with Dissolve(.5)
  pause 0.25
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "That did not turn out how I expected..."
  "Why the hell was all that stuff down there?"
  "And the giant worm. Yikes."
  "Plus, another one of these boxes..."
  if mc.owned_item("snow_globe"):
    "I probably need to find another key if I want to learn more."
  else:
    "What the hell is in them?"
    "I've got questions on top of questions."
  maxine confident_hands_down_blood "Hand over the box, will you?"
  show maxine confident_hands_down_blood at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Sorry, I don't think so.\"":
      show maxine confident_hands_down_blood at move_to(.5)
      mc "Sorry, I don't think so."
      maxine skeptical_blood "What is there to think about?"
      maxine skeptical_blood "It's a rather simple act."
      mc "I nearly died down there, remember?"
      mc "I will be keeping the box as my payment for the help."
      maxine afraid_blood "But I had the map!"
      mc "So?"
      maxine afraid_blood "Map law states that whomsoever—"
      mc "Well, I'm the law now."
      maxine thinking_blood "That's impossible."
      mc "No, it's not."
      maxine thinking_blood "I will be invoking cartographical justice."
      maxine thinking_blood "Just you wait."
      mc "Err, okay?"
      window hide
      show maxine thinking_blood at disappear_to_right
      pause 0.5
    "\"Here you go.\"":
      show maxine confident_hands_down_blood at move_to(.5)
      mc "Here you go."
      # mc "You're the one who had the map and knew it was down there, after all."
      mc "You're the one who had the map and knew it was down there,\nafter all."
      mc "It's all yours."
      window hide
      $mc.remove_item("locked_box_cave")
      pause 0.25
      window auto show
      show maxine excited_blood with dissolve2
      $maxine.love+=1
      maxine excited_blood "Most excellent! Thank you!"
      maxine excited_blood "And thank you for your assistance, too."
      mc "Happy to help."
      maxine flirty_blood "I will keep it in mind for the next time."
      if quest.maxine_dive["well_earned_outcome"]:
        "After today's ending, I am definitely hoping for a next time..."
      window hide
      show maxine flirty_blood at disappear_to_right
      pause 0.5
  window auto
  "Well, that was quite the adventure."
  "Even my bones feel tired..."
  "I really need to go home and get some sleep."
  "More craziness can wait for another day."
  window hide
  if game.quest_guide == "maxine_dive":
    $game.quest_guide = ""
  $game.notify_modal("quest", "Quest complete", quest.maxine_dive.title+"{hr}"+quest.maxine_dive._phases[1000].description, 5.0)
  pause 0.5
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $game.advance()
  pause 2.0
  hide maxine
  hide black onlayer screens
  with Dissolve(.5)
  play music "home_theme" fadein 0.5
  $quest.maxine_dive.finish(silent=True)
  $maxine.outfit = maxine["outfit_stamp"]
  return
