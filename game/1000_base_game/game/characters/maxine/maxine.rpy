init python:

  class Character_maxine(BaseChar):
    notify_level_changed=True

    default_name="Maxine"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="maxine contact_icon"

    default_outfit_slots=["hat","glasses","necklace","shirt","bra","pants","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("maxine_boat_captain_hat")
      self.add_item("maxine_hat")
      self.add_item("maxine_snorkel")
      self.add_item("maxine_glasses")
      self.add_item("maxine_necklace")
      self.add_item("maxine_hazmat_suit")
      self.add_item("maxine_armor")
      self.add_item("maxine_boat_captain_uniform")
      self.add_item("maxine_diving_suit")
      self.add_item("maxine_sweater")
      self.add_item("maxine_shirt")
      self.add_item("maxine_bra")
      self.add_item("maxine_black_bra")
      self.add_item("maxine_pants")
      self.add_item("maxine_skirt")
      self.add_item("maxine_panties")
      self.add_item("maxine_black_panties")
      self.equip("maxine_hat")
      self.equip("maxine_glasses")
      self.equip("maxine_necklace")
      self.equip("maxine_shirt")
      self.equip("maxine_bra")
      self.equip("maxine_pants")
      self.equip("maxine_panties")

    def ai(self):

      self.location=None
      self.activity=None
      self.alternative_locations={}

      ###ALT Locations
      if quest.kate_stepping == "meeting":
        self.alternative_locations["school_clubroom"] = "sitting"

      if quest.isabelle_dethroning == "meeting":
        self.alternative_locations["school_clubroom"] = "sitting"
      elif (quest.isabelle_dethroning == "dinner" and game.hour == 19) or quest.isabelle_dethroning in ("trap","revenge_done") or quest.isabelle_dethroning["kitchen_shenanigans_replay"]:
        self.alternative_locations["school_cafeteria"] = "newspaper"

      if quest.maya_witch == "meet_up":
        self.alternative_locations["school_clubroom"] = "sitting"

      if maxine["at_forest_glade_now"]:
        self.alternative_locations["school_forest_glade"] = "standing"

      if quest.lindsey_voluntary == "investigation" or maxine["at_science_class_this_scene"]:
        self.alternative_locations["school_science_class"] = "concocting"

      if quest.maxine_dive in ("weapons","shield","armor"):
        self.alternative_locations["school_park"] = "standing"
      elif quest.maxine_dive == "dive_2_electric_boogaloo":
        self.alternative_locations["school_park"] = "armor"
      elif quest.maxine_dive == "cave":
        self.alternative_locations["school_cave"] = "standing"
      ###ALT Locations

      ###Forced Locations
      if quest.jo_washed.in_progress:
        if quest.jo_washed["maxine_location"] == "school_roof":
          self.location = "school_roof"
          self.activity = "standing"
        else:
          self.location = None
          self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if maxine["at_none_now"] or maxine["at_none_today"]:
        self.location = None
        self.alternative_locations={}
        return

      if quest.kate_fate in ("peek","hunt","run","hunt2","deadend","rescue"):
        self.location = None
        self.alternative_locations={}
        return

      if maxine["at_clubroom_now"]:
        self.location = "school_clubroom"
        if game.season == 1 and quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access > "vent" and not quest.nurse_venting.started:
          self.activity = "fishing"
        else:
          self.activity = "sitting"
        return

      if quest.maxine_lines == "attention":
        self.location = None
        self.activity = None
        return

      if quest.maxine_lines in ("locator","reward","insight","seemaxine","prison"):
        self.location = "school_clubroom"
        self.activity = "sitting"
        return

      if quest.maxine_lines == "shot" and not quest.kate_trick.in_progress:
        self.location = "school_gym"
        self.activity = "standing"
        return

      if quest.isabelle_locker in ("interrogate","catnap","repeat"):
        self.location = "school_clubroom"
        self.activity = "sitting"
        return

      if quest.isabelle_locker == "scent":
        if mc.energy > 66:
          self.location = "school_ground_floor_west"
          self.activity = "spying"
        elif 66 >= mc.energy > 33:
          self.location = "school_ground_floor"
          self.activity = "sploot"
        else:
          self.location = "school_first_hall"
          self.activity = "vending"
        return

      if quest.maxine_hook == "wake":
        self.location = "home_bedroom"
        self.activity = "ouija_board"
        return

      if quest.maxine_hook == "computer":
        self.location = "school_computer_room"
        self.activity = "stethoscope"
        return

      if quest.maxine_hook in ("shovel","dig"):
        self.location = "school_forest_glade"
        self.activity = "standing"
        return

      if quest.maxine_hook in ("day","interrogation"):
        self.location = "school_cafeteria"
        self.activity = "newspaper"
        return

      if quest.isabelle_red.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.lindsey_motive["maxine_watching"]:
        self.location = "school_ground_floor_west"
        self.activity = "spying"
        return

      if quest.kate_trick.in_progress and game.hour == 18:
        if quest.isabelle_stolen.finished:
          self.location = "school_ground_floor_west"
          self.activity = "spying"
        else:
          self.location = "school_clubroom"
          self.activity = "sitting"
        return

      if quest.fall_in_newfall.in_progress:
        self.location = None
        self.activity = None
        return

      if (quest.fall_in_newfall.finished and not quest.maya_witch.started) and game.hour == 7:
        self.location = "school_ground_floor"
        self.activity = "sploot"
        return

      if quest.maya_witch == "help":
        if game.hour in (8,16):
          self.location = "school_first_hall"
          self.activity = "vending"
          return
        elif game.hour in (9,10,15):
          self.location = "school_ground_floor"
          self.activity = "sploot"
          return
        elif game.hour == 11:
          self.location = "school_cafeteria"
          self.activity = "sitting"
          return
        elif game.hour in (13,14):
          self.location = "school_ground_floor_west"
          self.activity = "spying"
          return
        elif game.hour == 17:
          self.location = "school_gym"
          self.activity = "standing"
          return

      if quest.jacklyn_romance == "hide_and_seek" and ((game.hour in (8,13,14) and not quest.isabelle_stolen.finished) or game.hour in (9,10,11,15,16,17)) and not school_clubroom["ball_of_yarn_taken"]:
        self.location = "school_gym"
        self.activity = "standing"
        return

      if quest.maya_spell == "maya" and game.hour == 12:
        self.location = "school_ground_floor_west"
        self.activity = "spying"
        return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return

      if quest.nurse_aid in ("fishing","bait") and quest.isabelle_gesture["manic_obsession"] and game.hour in (13,14):
        self.location = "school_ground_floor_west"
        self.activity = "spying"
        return

      ###Forced Locations

      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):

        if game.hour == 7:
          self.location = "school_first_hall"
          self.activity = "vending"
          return

        elif game.hour == 8:
          if quest.isabelle_stolen.finished:
            self.location = "school_ground_floor"
            self.activity = "sploot"
          else:
            self.location = "school_clubroom"
            if game.season == 1 and quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access > "vent" and not quest.nurse_venting.started:
              self.activity = "fishing"
            else:
              self.activity = "sitting"
          return

        elif game.hour in (9,10,11,15,16,17):
          self.location = "school_clubroom"
          if game.season == 1 and quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access > "vent" and not quest.nurse_venting.started:
            self.activity = "fishing"
          else:
            self.activity = "sitting"
          return

        elif game.hour == 12:
          self.location = "school_cafeteria"
          self.activity = "sitting"
          return

        elif game.hour in (13,14):
          if quest.isabelle_gesture["manic_obsession"]:
            self.location = "school_forest_glade"
            self.activity = "standing"
          elif quest.isabelle_stolen.finished:
            self.location = "school_ground_floor_west"
            self.activity = "spying"
          else:
            self.location = "school_clubroom"
            if game.season == 1 and quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access > "vent" and not quest.nurse_venting.started:
              self.activity = "fishing"
            else:
              self.activity = "sitting"
          return

        elif game.hour == 18:
          self.location = "school_gym"
          self.activity = "standing"
          return
      ### Schedule

    def call_label(self):
      if mc["focus"]:
        if mc["focus"] == "isabelle_hurricane":
          if quest.isabelle_hurricane == "call_maxine":
            return "quest_isabelle_hurricane_call_maxine"
        elif mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "costume" and game.location == "school_entrance" and not school_entrance["rope"]:
            return "quest_kate_wicked_costume_exterior_maxine"
      return None

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("maxine avatar "+state,True):
          return "maxine avatar "+state
      rv=[]

      if state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body1",95,175))
        rv.append(("maxine avatar face_afraid",151,260))
        if "_glasses" in state:
          rv.append(("maxine avatar b1glasses",175,311))
        if "_black_bra" in state:
          rv.append(("maxine avatar b1black_bra",122,455))
        elif "_bra" in state:
          rv.append(("maxine avatar b1bra",121,440))
        if "_black_panties" in state:
          rv.append(("maxine avatar b1black_panties",130,819))
        elif "_panties" in state:
          rv.append(("maxine avatar b1panty",132,862))
        if "_shirt" in state:
          rv.append(("maxine avatar b1top",121,451))
        if "_pants" in state:
          rv.append(("maxine avatar b1shorts",105,754))
        elif "_skirt" in state:
          rv.append(("maxine avatar b1skirt",97,732))
        if "_shirt" in state:
          rv.append(("maxine avatar b1blouse",89,466))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1sweater",92,406))
        elif all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b1armor_blood",93,426))
        if "_necklace" in state:
          rv.append(("maxine avatar b1necklace",199,448))
        if "_hat" in state:
          rv.append(("maxine avatar b1hat",128,133))
        rv.append(("maxine avatar b1arm1_n",55,505))
        if "_fishing" in state:
          if "_hat" in state:
            rv.append(("maxine avatar b1rod_c",21,48))
          else:
            rv.append(("maxine avatar b1rod_n",21,48))
        if "_shirt" in state:
          rv.append(("maxine avatar b1arm1_c",55,492))
          if "_fishing" in state:
            if "_hat" in state:
              rv.append(("maxine avatar b1rod_c",21,48))
            else:
              rv.append(("maxine avatar b1rod_n",21,48))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1arm1_sweater",49,492))
        elif all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b1arm1_armor_blood",47,437))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body1",95,175))
        rv.append(("maxine avatar face_cringe",114,261))
        if "_glasses" in state:
          rv.append(("maxine avatar b1glasses",175,311))
        if "_black_bra" in state:
          rv.append(("maxine avatar b1black_bra",122,455))
        elif "_bra" in state:
          rv.append(("maxine avatar b1bra",121,440))
        if "_black_panties" in state:
          rv.append(("maxine avatar b1black_panties",130,819))
        elif "_panties" in state:
          rv.append(("maxine avatar b1panty",132,862))
        if "_shirt" in state:
          rv.append(("maxine avatar b1top",121,451))
        if "_pants" in state:
          rv.append(("maxine avatar b1shorts",105,754))
        elif "_skirt" in state:
          rv.append(("maxine avatar b1skirt",97,732))
        if "_shirt" in state:
          rv.append(("maxine avatar b1blouse",89,466))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1sweater",92,406))
        if "_necklace" in state:
          rv.append(("maxine avatar b1necklace",199,448))
        if "_hat" in state:
          rv.append(("maxine avatar b1hat",128,133))
        if "_phone" in state:
          rv.append(("maxine avatar b1phone_n",95,343))
        else:
          rv.append(("maxine avatar b1arm2_n",90,423))
        if "_shirt" in state:
          if "_phone" in state:
            rv.append(("maxine avatar b1phone_c",88,482))
          else:
            rv.append(("maxine avatar b1arm2_c",83,423))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1arm2_sweater",79,423))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body1",95,175))
        rv.append(("maxine avatar face_sad",180,287))
        if "_glasses" in state:
          rv.append(("maxine avatar b1glasses",175,311))
        if "_black_bra" in state:
          rv.append(("maxine avatar b1black_bra",122,455))
        elif "_bra" in state:
          rv.append(("maxine avatar b1bra",121,440))
        if "_black_panties" in state:
          rv.append(("maxine avatar b1black_panties",130,819))
        elif "_panties" in state:
          rv.append(("maxine avatar b1panty",132,862))
        if "_shirt" in state:
          rv.append(("maxine avatar b1top",121,451))
        if "_pants" in state:
          rv.append(("maxine avatar b1shorts",105,754))
        elif "_skirt" in state:
          rv.append(("maxine avatar b1skirt",97,732))
        if "_shirt" in state:
          rv.append(("maxine avatar b1blouse",89,466))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1sweater",92,406))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b1armor_blood",93,426))
          else:
            rv.append(("maxine avatar b1armor",93,426))
        if "_necklace" in state:
          rv.append(("maxine avatar b1necklace",199,448))
        if "_hat" in state:
          rv.append(("maxine avatar b1hat",128,133))
        if "_map" in state:
          rv.append(("maxine avatar b1map_n",1,494))
        elif "_fishing" in state:
          rv.append(("maxine avatar b1arm1_n",55,505))
          if "_hat" in state:
            rv.append(("maxine avatar b1rod_c",21,48))
          else:
            rv.append(("maxine avatar b1rod_n",21,48))
        elif "_phone" in state:
          rv.append(("maxine avatar b1phone_n",95,343))
        else:
          rv.append(("maxine avatar b1arm2_n",90,423))
        if "_shirt" in state:
          if "_map" in state:
            rv.append(("maxine avatar b1map_c",40,492))
          elif "_fishing" in state:
            rv.append(("maxine avatar b1arm1_c",55,492))
            if "_hat" in state:
              rv.append(("maxine avatar b1rod_c",21,48))
            else:
              rv.append(("maxine avatar b1rod_n",21,48))
          elif "_phone" in state:
            rv.append(("maxine avatar b1phone_c",88,482))
          else:
            rv.append(("maxine avatar b1arm2_c",83,423))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1arm2_sweater",79,423))
        elif "_armor" in state:
          if "_map" in state:
            rv.append(("maxine avatar b1map_armor",0,437))
          elif "_blood" in state:
            rv.append(("maxine avatar b1arm2_armor_blood",78,422))
          else:
            rv.append(("maxine avatar b1arm2_armor",78,422))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body1",95,175))
        rv.append(("maxine avatar face_thinking",181,281))
        if "_glasses" in state:
          rv.append(("maxine avatar b1glasses",175,311))
        if "_black_bra" in state:
          rv.append(("maxine avatar b1black_bra",122,455))
        elif "_bra" in state:
          rv.append(("maxine avatar b1bra",121,440))
        if "_black_panties" in state:
          rv.append(("maxine avatar b1black_panties",130,819))
        elif "_panties" in state:
          rv.append(("maxine avatar b1panty",132,862))
        if "_shirt" in state:
          rv.append(("maxine avatar b1top",121,451))
        if "_pants" in state:
          rv.append(("maxine avatar b1shorts",105,754))
        elif "_skirt" in state:
          rv.append(("maxine avatar b1skirt",97,732))
        if "_shirt" in state:
          rv.append(("maxine avatar b1blouse",89,466))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1sweater",92,406))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b1armor_blood",93,426))
          else:
            rv.append(("maxine avatar b1armor",93,426))
        if "_necklace" in state:
          rv.append(("maxine avatar b1necklace",199,448))
        if "_hat" in state:
          rv.append(("maxine avatar b1hat",128,133))
        if "_map" in state:
          rv.append(("maxine avatar b1map_n",1,494))
        else:
          rv.append(("maxine avatar b1arm1_n",55,505))
          if "_fishing" in state:
            if "_hat" in state:
              rv.append(("maxine avatar b1rod_c",21,48))
            else:
              rv.append(("maxine avatar b1rod_n",21,48))
        if "_shirt" in state:
          if "_map" in state:
            rv.append(("maxine avatar b1map_c",40,492))
          else:
            rv.append(("maxine avatar b1arm1_c",55,492))
            if "_fishing" in state:
              if "_hat" in state:
                rv.append(("maxine avatar b1rod_c",21,48))
              else:
                rv.append(("maxine avatar b1rod_n",21,48))
        elif "_sweater" in state:
          rv.append(("maxine avatar b1arm1_sweater",49,492))
        elif "_armor" in state:
          if "_map" in state:
            rv.append(("maxine avatar b1map_armor",0,437))
          elif "_blood" in state:
            rv.append(("maxine avatar b1arm1_armor_blood",47,437))
          else:
            rv.append(("maxine avatar b1arm1_armor",47,437))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body2",120,171))
        rv.append(("maxine avatar face_annoyed",200,275))
        if "_glasses" in state:
          rv.append(("maxine avatar b2glasses",193,302))
        if "_black_bra" in state:
          rv.append(("maxine avatar b2black_bra",173,451))
        elif "_bra" in state:
          rv.append(("maxine avatar b2bra",175,448))
        if "_black_panties" in state:
          rv.append(("maxine avatar b2black_panties",122,854))
        elif "_panties" in state:
          rv.append(("maxine avatar b2panty",123,877))
        if "_shirt" in state:
          rv.append(("maxine avatar b2top",151,463))
        if "_pants" in state:
          rv.append(("maxine avatar b2shorts",116,745))
        elif "_skirt" in state:
          rv.append(("maxine avatar b2skirt",117,749))
        if "_shirt" in state:
          rv.append(("maxine avatar b2blouse",116,469))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2sweater",117,412))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2boat_captain_uniform",113,420))
        elif "_armor" in state:
          rv.append(("maxine avatar b2armor",114,420))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b2necklace_sweater",231,465))
          else:
            rv.append(("maxine avatar b2necklace",228,452))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b2boat_captain_hat",154,118))
        elif "_hat" in state:
          rv.append(("maxine avatar b2hat",158,111))
        if "_sock" in state:
          rv.append(("maxine avatar b2sock_n",91,420))
        elif "_mc_phone" in state:
          rv.append(("maxine avatar b2mc_phone_n",134,471))
        elif "_isabelle_phone" in state:
          rv.append(("maxine avatar b2isabelle_phone_n",134,471))
        elif "_hose" in state:
          rv.append(("maxine avatar b2hose_n",133,521))
        else:
          rv.append(("maxine avatar b2arm1_n",129,616))
        if "_shirt" in state:
          if "_sock" in state:
            rv.append(("maxine avatar b2sock_c",86,502))
          elif "_mc_phone" in state:
            rv.append(("maxine avatar b2mc_phone_c",142,569))
          elif "_isabelle_phone" in state:
            rv.append(("maxine avatar b2isabelle_phone_c",142,569))
          elif "_hose" in state:
            rv.append(("maxine avatar b2hose_c",133,521))
          else:
            rv.append(("maxine avatar b2arm1_c",133,664))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2arm1_sweater",120,616))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2arm1_boat_captain_uniform",116,616))
        elif "_hazmat_suit" in state:
          rv.append(("maxine avatar b2hazmat_suit",108,153))
          rv.append(("maxine avatar b2arm1_hazmat_suit",124,615))
        elif "_armor" in state:
          rv.append(("maxine avatar b2arm1_armor",121,614))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body2",120,171))
        rv.append(("maxine avatar face_confident",200,275))
        if all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b2blood",198,230))
        if "_glasses" in state:
          rv.append(("maxine avatar b2glasses",193,302))
        if "_black_bra" in state:
          rv.append(("maxine avatar b2black_bra",173,451))
        elif "_bra" in state:
          rv.append(("maxine avatar b2bra",175,448))
        if "_black_panties" in state:
          rv.append(("maxine avatar b2black_panties",122,854))
        elif "_panties" in state:
          rv.append(("maxine avatar b2panty",123,877))
        if "_shirt" in state:
          rv.append(("maxine avatar b2top",151,463))
        if "_pants" in state:
          rv.append(("maxine avatar b2shorts",116,745))
        elif "_skirt" in state:
          rv.append(("maxine avatar b2skirt",117,749))
        if "_shirt" in state:
          rv.append(("maxine avatar b2blouse",116,469))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2sweater",117,412))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2boat_captain_uniform",113,420))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b2armor_blood",115,421))
          else:
            rv.append(("maxine avatar b2armor",114,420))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b2necklace_sweater",231,465))
          else:
            rv.append(("maxine avatar b2necklace",228,452))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b2boat_captain_hat",154,118))
        elif "_hat" in state:
          rv.append(("maxine avatar b2hat",158,111))
        if "_mic" in state:
          rv.append(("maxine avatar b2mic_n",134,515))
        elif "_sock" in state:
          rv.append(("maxine avatar b2sock_n",91,420))
        elif "_fishing" in state:
          rv.append(("maxine avatar b2rod_n",95,6))
        elif "_mc_phone" in state:
          rv.append(("maxine avatar b2mc_phone_n",134,471))
        elif "_isabelle_phone" in state:
          rv.append(("maxine avatar b2isabelle_phone_n",134,471))
        elif "_phone" in state:
          rv.append(("maxine avatar b2phone_n",45,359))
        elif "_hose" in state:
          rv.append(("maxine avatar b2hose_n",133,521))
        elif "_hands_down" in state:
          rv.append(("maxine avatar b2arm1_n",129,616))
        else:
          rv.append(("maxine avatar b2arm2_n",134,451))
        if "_shirt" in state:
          if "_mic" in state:
            rv.append(("maxine avatar b2mic_c",142,569))
          elif "_sock" in state:
            rv.append(("maxine avatar b2sock_c",86,502))
          elif "_fishing" in state:
            rv.append(("maxine avatar b2rod_c",142,508))
          elif "_mc_phone" in state:
            rv.append(("maxine avatar b2mc_phone_c",142,569))
          elif "_isabelle_phone" in state:
            rv.append(("maxine avatar b2isabelle_phone_c",142,569))
          elif "_phone" in state:
            rv.append(("maxine avatar b2phone_c",111,531))
          elif "_hose" in state:
            rv.append(("maxine avatar b2hose_c",133,521))
          elif "_hands_down" in state:
            rv.append(("maxine avatar b2arm1_c",133,664))
          else:
            rv.append(("maxine avatar b2arm2_c",142,568))
        elif "_sweater" in state:
          if "_hands_down" in state:
            rv.append(("maxine avatar b2arm1_sweater",120,616))
          else:
            rv.append(("maxine avatar b2arm2_sweater",117,451))
        elif "_boat_captain_uniform" in state:
          if "_hands_down" in state:
            rv.append(("maxine avatar b2arm1_boat_captain_uniform",116,616))
          else:
            rv.append(("maxine avatar b2arm2_boat_captain_uniform",116,451))
        elif "_hazmat_suit" in state:
          rv.append(("maxine avatar b2hazmat_suit",108,153))
          if "_hands_down" in state:
            rv.append(("maxine avatar b2arm1_hazmat_suit",124,615))
          else:
            rv.append(("maxine avatar b2arm2_hazmat_suit",120,451))
        elif "_armor" in state:
          if "_hands_down" in state:
            if "_blood" in state:
              rv.append(("maxine avatar b2arm1_armor_blood",121,614))
            else:
              rv.append(("maxine avatar b2arm1_armor",121,614))
          else:
            if "_blood" in state:
              rv.append(("maxine avatar b2arm2_armor_blood",119,448))
            else:
              rv.append(("maxine avatar b2arm2_armor",119,448))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body2",120,171))
        rv.append(("maxine avatar face_eyeroll",200,269))
        if "_glasses" in state:
          rv.append(("maxine avatar b2glasses",193,302))
        if "_black_bra" in state:
          rv.append(("maxine avatar b2black_bra",173,451))
        elif "_bra" in state:
          rv.append(("maxine avatar b2bra",175,448))
        if "_black_panties" in state:
          rv.append(("maxine avatar b2black_panties",122,854))
        elif "_panties" in state:
          rv.append(("maxine avatar b2panty",123,877))
        if "_shirt" in state:
          rv.append(("maxine avatar b2top",151,463))
        if "_pants" in state:
          rv.append(("maxine avatar b2shorts",116,745))
        elif "_skirt" in state:
          rv.append(("maxine avatar b2skirt",117,749))
        if "_shirt" in state:
          rv.append(("maxine avatar b2blouse",116,469))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2sweater",117,412))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2boat_captain_uniform",113,420))
        elif "_armor" in state:
          rv.append(("maxine avatar b2armor",114,420))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b2necklace_sweater",231,465))
          else:
            rv.append(("maxine avatar b2necklace",228,452))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b2boat_captain_hat",154,118))
        elif "_hat" in state:
          rv.append(("maxine avatar b2hat",158,111))
        if "_mic" in state:
          rv.append(("maxine avatar b2mic_n",134,515))
        elif "_fishing" in state:
          rv.append(("maxine avatar b2rod_n",95,6))
        else:
          rv.append(("maxine avatar b2arm1_n",129,616))
        if "_shirt" in state:
          if "_mic" in state:
            rv.append(("maxine avatar b2mic_c",142,569))
          elif "_fishing" in state:
            rv.append(("maxine avatar b2rod_c",142,508))
          else:
            rv.append(("maxine avatar b2arm1_c",133,664))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2arm1_sweater",120,616))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2arm1_boat_captain_uniform",116,616))
        elif "_hazmat_suit" in state:
          rv.append(("maxine avatar b2hazmat_suit",108,153))
          rv.append(("maxine avatar b2arm1_hazmat_suit",124,615))
        elif "_armor" in state:
          rv.append(("maxine avatar b2arm1_armor",121,614))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body2",120,171))
        rv.append(("maxine avatar face_skeptical",200,275))
        if all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b2blood",198,230))
        if "_glasses" in state:
          rv.append(("maxine avatar b2glasses",193,302))
        if "_black_bra" in state:
          rv.append(("maxine avatar b2black_bra",173,451))
        elif "_bra" in state:
          rv.append(("maxine avatar b2bra",175,448))
        if "_black_panties" in state:
          rv.append(("maxine avatar b2black_panties",122,854))
        elif "_panties" in state:
          rv.append(("maxine avatar b2panty",123,877))
        if "_shirt" in state:
          rv.append(("maxine avatar b2top",151,463))
        if "_pants" in state:
          rv.append(("maxine avatar b2shorts",116,745))
        elif "_skirt" in state:
          rv.append(("maxine avatar b2skirt",117,749))
        if "_shirt" in state:
          rv.append(("maxine avatar b2blouse",116,469))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2sweater",117,412))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2boat_captain_uniform",113,420))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b2armor_blood",115,421))
          else:
            rv.append(("maxine avatar b2armor",114,420))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b2necklace_sweater",231,465))
          else:
            rv.append(("maxine avatar b2necklace",228,452))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b2boat_captain_hat",154,118))
        elif "_hat" in state:
          rv.append(("maxine avatar b2hat",158,111))
        if "_fishing" in state:
          rv.append(("maxine avatar b2rod_n",95,6))
        elif "_phone" in state:
          rv.append(("maxine avatar b2phone_n",45,359))
        elif "_hazmat_suit" not in state:
          rv.append(("maxine avatar b2magnifying_glass",262,295))
          rv.append(("maxine avatar b2arm2_n",134,451))
        if "_shirt" in state:
          if "_fishing" in state:
            rv.append(("maxine avatar b2rod_c",142,508))
          elif "_phone" in state:
            rv.append(("maxine avatar b2phone_c",111,531))
          else:
            rv.append(("maxine avatar b2arm2_c",142,568))
        elif "_sweater" in state:
          rv.append(("maxine avatar b2arm2_sweater",117,451))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b2arm2_boat_captain_uniform",116,451))
        elif "_hazmat_suit" in state:
          rv.append(("maxine avatar b2hazmat_suit",108,153))
          rv.append(("maxine avatar b2magnifying_glass",262,295))
          rv.append(("maxine avatar b2arm2_n",134,451))
          rv.append(("maxine avatar b2arm2_hazmat_suit",120,451))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b2arm2_armor_blood",119,448))
          else:
            rv.append(("maxine avatar b2arm2_armor",119,448))

      elif state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body3",49,168))
        rv.append(("maxine avatar face_angry",190,283))
        if "_glasses" in state:
          rv.append(("maxine avatar b3glasses",203,308))
        elif "_snorkel" in state:
          rv.append(("maxine avatar b3snorkel",174,261))
        if "_black_bra" in state:
          rv.append(("maxine avatar b3black_bra",169,445))
        elif "_bra" in state:
          rv.append(("maxine avatar b3bra",170,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b3black_panties",143,815))
        elif "_panties" in state:
          rv.append(("maxine avatar b3panty",147,843))
        if "_shirt" in state:
          rv.append(("maxine avatar b3top",170,442))
        if "_pants" in state:
          rv.append(("maxine avatar b3shorts",112,737))
        elif "_skirt" in state:
          rv.append(("maxine avatar b3skirt",44,731))
        if "_popcorn" in state:
          rv.append(("maxine avatar b3popcorn_n",29,541))
        else:
          rv.append(("maxine avatar b3arm2_n",85,384))
          if "_fishing" in state:
            if "_hat" in state:
              rv.append(("maxine avatar b3rod_c",6,8))
            else:
              rv.append(("maxine avatar b3rod_n",6,8))
        if "_shirt" in state:
          rv.append(("maxine avatar b3blouse",133,448))
        elif "_sweater" in state:
          rv.append(("maxine avatar b3sweater",161,399))
        elif "_diving_suit" in state:
          rv.append(("maxine avatar b3diving_suit",48,423))
          rv.append(("maxine avatar b3water_droplets",158,231))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b3necklace_sweater",244,447))
          else:
            rv.append(("maxine avatar b3necklace",244,435))
        if "_hat" in state:
          rv.append(("maxine avatar b3hat",166,137))
        if "_shirt" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_c",29,541))
          else:
            rv.append(("maxine avatar b3arm2_c",67,384))
            if "_fishing" in state:
              if "_hat" in state:
                rv.append(("maxine avatar b3rod_c",6,8))
              else:
                rv.append(("maxine avatar b3rod_n",6,8))
        elif "_sweater" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_sweater",29,531))
          else:
            rv.append(("maxine avatar b3arm2_sweater",76,384))
        elif "_diving_suit" in state:
          rv.append(("maxine avatar b3arm2_diving_suit",84,384))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body3",49,168))
        rv.append(("maxine avatar face_laughing",163,269))
        if "_glasses" in state:
          rv.append(("maxine avatar b3glasses",203,308))
        if "_black_bra" in state:
          rv.append(("maxine avatar b3black_bra",169,445))
        elif "_bra" in state:
          rv.append(("maxine avatar b3bra",170,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b3black_panties",143,815))
        elif "_panties" in state:
          rv.append(("maxine avatar b3panty",147,843))
        if "_shirt" in state:
          rv.append(("maxine avatar b3top",170,442))
        if "_pants" in state:
          rv.append(("maxine avatar b3shorts",112,737))
        elif "_skirt" in state:
          rv.append(("maxine avatar b3skirt",44,731))
        if "_popcorn" in state:
          rv.append(("maxine avatar b3popcorn_n",29,541))
        elif "_power" in state:
          rv.append(("maxine avatar b3power_n",31,463))
        else:
          rv.append(("maxine avatar b3arm2_n",85,384))
          if "_fishing" in state:
            if "_hat" in state:
              rv.append(("maxine avatar b3rod_c",6,8))
            else:
              rv.append(("maxine avatar b3rod_n",6,8))
        if "_shirt" in state:
          rv.append(("maxine avatar b3blouse",133,448))
        elif "_sweater" in state:
          rv.append(("maxine avatar b3sweater",161,399))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b3necklace_sweater",244,447))
          else:
            rv.append(("maxine avatar b3necklace",244,435))
        if "_hat" in state:
          rv.append(("maxine avatar b3hat",166,137))
        if "_shirt" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_c",29,541))
          elif "_power" in state:
            rv.append(("maxine avatar b3power_c",12,462))
          else:
            rv.append(("maxine avatar b3arm2_c",67,384))
            if "_fishing" in state:
              if "_hat" in state:
                rv.append(("maxine avatar b3rod_c",6,8))
              else:
                rv.append(("maxine avatar b3rod_n",6,8))
        elif "_sweater" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_sweater",29,531))
          else:
            rv.append(("maxine avatar b3arm2_sweater",76,384))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body3",49,168))
        rv.append(("maxine avatar face_neutral",190,278))
        if "_glasses" in state:
          rv.append(("maxine avatar b3glasses",203,308))
        elif "_snorkel" in state:
          rv.append(("maxine avatar b3snorkel",174,261))
        if "_black_bra" in state:
          rv.append(("maxine avatar b3black_bra",169,445))
        elif "_bra" in state:
          rv.append(("maxine avatar b3bra",170,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b3black_panties",143,815))
        elif "_panties" in state:
          rv.append(("maxine avatar b3panty",147,843))
        if "_shirt" in state:
          rv.append(("maxine avatar b3top",170,442))
        if "_pants" in state:
          rv.append(("maxine avatar b3shorts",112,737))
        elif "_skirt" in state:
          if "_popcorn" not in state:
            rv.append(("maxine avatar b3arm1_n",114,538))
          rv.append(("maxine avatar b3skirt",44,731))
        if "_popcorn" in state:
          rv.append(("maxine avatar b3popcorn_n",29,541))
        elif "_skirt" not in state:
          rv.append(("maxine avatar b3arm1_n",114,538))
        if "_shirt" in state:
          rv.append(("maxine avatar b3blouse",133,448))
        elif "_sweater" in state:
          rv.append(("maxine avatar b3sweater",161,399))
        elif "_diving_suit" in state:
          rv.append(("maxine avatar b3diving_suit",48,423))
          rv.append(("maxine avatar b3water_droplets",158,231))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b3necklace_sweater",244,447))
          else:
            rv.append(("maxine avatar b3necklace",244,435))
        if "_hat" in state:
          rv.append(("maxine avatar b3hat",166,137))
        if "_shirt" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_c",29,541))
          else:
            rv.append(("maxine avatar b3arm1_c",112,543))
        elif "_sweater" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_sweater",29,531))
          else:
            rv.append(("maxine avatar b3arm1_sweater",110,531))
        elif "_diving_suit" in state:
          rv.append(("maxine avatar b3arm1_diving_suit",114,535))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body3",49,168))
        rv.append(("maxine avatar face_smile",190,278))
        if "_glasses" in state:
          rv.append(("maxine avatar b3glasses",203,308))
        elif "_snorkel" in state:
          rv.append(("maxine avatar b3snorkel",174,261))
        if "_black_bra" in state:
          rv.append(("maxine avatar b3black_bra",169,445))
        elif "_bra" in state:
          rv.append(("maxine avatar b3bra",170,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b3black_panties",143,815))
        elif "_panties" in state:
          rv.append(("maxine avatar b3panty",147,843))
        if "_shirt" in state:
          rv.append(("maxine avatar b3top",170,442))
        if "_pants" in state:
          rv.append(("maxine avatar b3shorts",112,737))
        elif "_skirt" in state:
          if "_popcorn" not in state:
            rv.append(("maxine avatar b3arm1_n",114,538))
          rv.append(("maxine avatar b3skirt",44,731))
        if "_popcorn" in state:
          rv.append(("maxine avatar b3popcorn_n",29,541))
        elif "_power" in state:
          rv.append(("maxine avatar b3power_n",31,463))
        elif "_fishing" in state:
          rv.append(("maxine avatar b3arm2_n",85,384))
          if "_hat" in state:
            rv.append(("maxine avatar b3rod_c",6,8))
          else:
            rv.append(("maxine avatar b3rod_n",6,8))
        elif "_skirt" not in state:
          rv.append(("maxine avatar b3arm1_n",114,538))
        if "_shirt" in state:
          rv.append(("maxine avatar b3blouse",133,448))
        elif "_sweater" in state:
          rv.append(("maxine avatar b3sweater",161,399))
        elif "_diving_suit" in state:
          rv.append(("maxine avatar b3diving_suit",48,423))
          rv.append(("maxine avatar b3water_droplets",158,231))
        if "_necklace" in state:
          if "_sweater" in state:
            rv.append(("maxine avatar b3necklace_sweater",244,447))
          else:
            rv.append(("maxine avatar b3necklace",244,435))
        if "_hat" in state:
          rv.append(("maxine avatar b3hat",166,137))
        if "_shirt" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_c",29,541))
          elif "_power" in state:
            rv.append(("maxine avatar b3power_c",12,462))
          elif "_fishing" in state:
            rv.append(("maxine avatar b3arm2_c",67,384))
            if "_hat" in state:
              rv.append(("maxine avatar b3rod_c",6,8))
            else:
              rv.append(("maxine avatar b3rod_n",6,8))
          else:
            rv.append(("maxine avatar b3arm1_c",112,543))
        elif "_sweater" in state:
          if "_popcorn" in state:
            rv.append(("maxine avatar b3popcorn_sweater",29,531))
          else:
            rv.append(("maxine avatar b3arm1_sweater",110,531))
        elif "_diving_suit" in state:
          rv.append(("maxine avatar b3arm1_diving_suit",114,535))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body4",99,167))
        rv.append(("maxine avatar face_blush",194,270))
        if all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b4blood",219,234))
        if "_glasses" in state:
          rv.append(("maxine avatar b4glasses",216,297))
        if "_black_bra" in state:
          rv.append(("maxine avatar b4black_bra",138,449))
        elif "_bra" in state:
          rv.append(("maxine avatar b4bra",138,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b4black_panties",140,801))
        elif "_panties" in state:
          rv.append(("maxine avatar b4panty",148,832))
        if "_shirt" in state:
          rv.append(("maxine avatar b4top",137,451))
        if "_pants" in state:
          rv.append(("maxine avatar b4shorts",115,741))
        elif "_skirt" in state:
          rv.append(("maxine avatar b4arm1_n",126,495))
          rv.append(("maxine avatar b4skirt",97,729))
        if "_binoculars" in state:
          if "_shirt" in state:
            rv.append(("maxine avatar b4blouse",136,443))
        else:
          if "_skirt" not in state:
            rv.append(("maxine avatar b4arm1_n",126,495))
          if "_shirt" in state:
            rv.append(("maxine avatar b4blouse",136,443))
          elif "_sweater" in state:
            rv.append(("maxine avatar b4sweater",133,409))
          elif "_armor" in state:
            if "_blood" in state:
              rv.append(("maxine avatar b4armor_blood",94,419))
            else:
              rv.append(("maxine avatar b4armor",93,418))
        if "_necklace" in state:
          rv.append(("maxine avatar b4necklace",214,454))
        if "_hat" in state:
          rv.append(("maxine avatar b4hat",146,142))
        if "_binoculars" in state:
          rv.append(("maxine avatar b4binocular_n",28,227))
        if "_shirt" in state:
          if "_binoculars" in state:
            rv.append(("maxine avatar b4binocular_c",25,227))
          else:
            rv.append(("maxine avatar b4arm1_c",114,508))
        elif "_sweater" in state:
          rv.append(("maxine avatar b4arm1_sweater",119,489))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b4arm1_armor_blood",108,486))
          else:
            rv.append(("maxine avatar b4arm1_armor",108,486))

      elif state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body4",99,167))
        rv.append(("maxine avatar face_concerned",234,270))
        if all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b4blood",219,234))
        if "_glasses" in state:
          rv.append(("maxine avatar b4glasses",216,297))
        if "_black_bra" in state:
          rv.append(("maxine avatar b4black_bra",138,449))
        elif "_bra" in state:
          rv.append(("maxine avatar b4bra",138,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b4black_panties",140,801))
        elif "_panties" in state:
          rv.append(("maxine avatar b4panty",148,832))
        if "_shirt" in state:
          rv.append(("maxine avatar b4top",137,451))
        if "_pants" in state:
          rv.append(("maxine avatar b4shorts",115,741))
        elif "_skirt" in state:
          rv.append(("maxine avatar b4skirt",97,729))
        if "_binoculars" not in state:
          rv.append(("maxine avatar b4arm2_n",94,379))
        if "_shirt" in state:
          rv.append(("maxine avatar b4blouse",136,443))
        elif "_sweater" in state:
          rv.append(("maxine avatar b4sweater",133,409))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b4boat_captain_uniform",95,421))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b4armor_blood",94,419))
          else:
            rv.append(("maxine avatar b4armor",93,418))
        if "_necklace" in state:
          rv.append(("maxine avatar b4necklace",214,454))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b4boat_captain_hat",182,122))
        elif "_hat" in state:
          rv.append(("maxine avatar b4hat",146,142))
        if "_binoculars" in state:
          rv.append(("maxine avatar b4binocular_n",28,227))
        if "_shirt" in state:
          if "_binoculars" in state:
            rv.append(("maxine avatar b4binocular_c",25,227))
          else:
            rv.append(("maxine avatar b4arm2_c",87,379))
        elif "_sweater" in state:
          rv.append(("maxine avatar b4arm2_sweater",87,379))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b4arm2_boat_captain_uniform",92,379))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b4arm2_armor_blood",84,378))
          else:
            rv.append(("maxine avatar b4arm2_armor",84,378))
        if "_fishing" in state:
          rv.append(("maxine avatar b4rod",100,-265))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body4",99,167))
        rv.append(("maxine avatar face_excited",207,263))
        if all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b4blood",219,234))
        if "_glasses" in state:
          rv.append(("maxine avatar b4glasses",216,297))
        if "_black_bra" in state:
          rv.append(("maxine avatar b4black_bra",138,449))
        elif "_bra" in state:
          rv.append(("maxine avatar b4bra",138,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b4black_panties",140,801))
        elif "_panties" in state:
          rv.append(("maxine avatar b4panty",148,832))
        if "_shirt" in state:
          rv.append(("maxine avatar b4top",137,451))
        if "_pants" in state:
          rv.append(("maxine avatar b4shorts",115,741))
        elif "_skirt" in state:
          rv.append(("maxine avatar b4skirt",97,729))
        if "_electrodes" in state:
          rv.append(("maxine avatar b4electrodes_n",94,344))
        elif "_binoculars" not in state:
          rv.append(("maxine avatar b4arm2_n",94,379))
        if "_shirt" in state:
          rv.append(("maxine avatar b4blouse",136,443))
        elif "_sweater" in state:
          rv.append(("maxine avatar b4sweater",133,409))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b4boat_captain_uniform",95,421))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b4armor_blood",94,419))
          else:
            rv.append(("maxine avatar b4armor",93,418))
        if "_necklace" in state:
          rv.append(("maxine avatar b4necklace",214,454))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b4boat_captain_hat",182,122))
        elif "_hat" in state:
          rv.append(("maxine avatar b4hat",146,142))
        if "_binoculars" in state:
          rv.append(("maxine avatar b4binocular_n",28,227))
        if "_shirt" in state:
          if "_binoculars" in state:
            rv.append(("maxine avatar b4binocular_c",25,227))
          else:
            rv.append(("maxine avatar b4arm2_c",87,379))
        elif "_sweater" in state:
          if "_electrodes" in state:
            rv.append(("maxine avatar b4electrodes_c",87,344))
          else:
            rv.append(("maxine avatar b4arm2_sweater",87,379))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b4arm2_boat_captain_uniform",92,379))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b4arm2_armor_blood",84,378))
          else:
            rv.append(("maxine avatar b4arm2_armor",84,378))
        if "_fishing" in state:
          rv.append(("maxine avatar b4rod",100,-265))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("maxine avatar body4",99,167))
        rv.append(("maxine avatar face_flirty",200,263))
        if all(suffix in state for suffix in ("_armor","_blood")):
          rv.append(("maxine avatar b4blood",219,234))
        if "_glasses" in state:
          rv.append(("maxine avatar b4glasses",216,297))
        if "_black_bra" in state:
          rv.append(("maxine avatar b4black_bra",138,449))
        elif "_bra" in state:
          rv.append(("maxine avatar b4bra",138,449))
        if "_black_panties" in state:
          rv.append(("maxine avatar b4black_panties",140,801))
        elif "_panties" in state:
          rv.append(("maxine avatar b4panty",148,832))
        if "_shirt" in state:
          rv.append(("maxine avatar b4top",137,451))
        if "_pants" in state:
          rv.append(("maxine avatar b4shorts",115,741))
        elif "_skirt" in state:
          rv.append(("maxine avatar b4arm1_n",126,495))
          rv.append(("maxine avatar b4skirt",97,729))
        if "_binoculars" in state or "_fishing" in state:
          if "_fishing" in state:
            rv.append(("maxine avatar b4arm2_n",94,379))
          if "_shirt" in state:
            rv.append(("maxine avatar b4blouse",136,443))
        else:
          if "_skirt" not in state:
            rv.append(("maxine avatar b4arm1_n",126,495))
          if "_shirt" in state:
            rv.append(("maxine avatar b4blouse",136,443))
          elif "_sweater" in state:
            rv.append(("maxine avatar b4sweater",133,409))
          elif "_boat_captain_uniform" in state:
            rv.append(("maxine avatar b4boat_captain_uniform",95,421))
          elif "_armor" in state:
            if "_blood" in state:
              rv.append(("maxine avatar b4armor_blood",94,419))
            else:
              rv.append(("maxine avatar b4armor",93,418))
        if "_necklace" in state:
          rv.append(("maxine avatar b4necklace",214,454))
        if "_boat_captain_hat" in state:
          rv.append(("maxine avatar b4boat_captain_hat",182,122))
        elif "_hat" in state:
          rv.append(("maxine avatar b4hat",146,142))
        if "_binoculars" in state:
          rv.append(("maxine avatar b4binocular_n",28,227))
        if "_shirt" in state:
          if "_binoculars" in state:
            rv.append(("maxine avatar b4binocular_c",25,227))
          elif "_fishing" in state:
            rv.append(("maxine avatar b4arm2_c",87,379))
          else:
            rv.append(("maxine avatar b4arm1_c",114,508))
        elif "_sweater" in state:
          rv.append(("maxine avatar b4arm1_sweater",119,489))
        elif "_boat_captain_uniform" in state:
          rv.append(("maxine avatar b4arm1_boat_captain_uniform",125,492))
        elif "_armor" in state:
          if "_blood" in state:
            rv.append(("maxine avatar b4arm1_armor_blood",108,486))
          else:
            rv.append(("maxine avatar b4arm1_armor",108,486))
        if "_fishing" in state:
          rv.append(("maxine avatar b4rod",100,-265))

      elif state.startswith("hooking_up"):
        rv.append((1920,1080))
        if "replay" in state:
          rv.append(("maxine avatar events hooking_up bg",0,0))
          if school_hook_up_table["router_connected"]:
            rv.append(("maxine avatar events hooking_up router_connected",514,119))
          rv.append(("maxine avatar events hooking_up power_supply",461,417))
          if not school_hook_up_table["cable"]:
            rv.append(("maxine avatar events hooking_up cable",839,129))
          if not school_hook_up_table["vr_goggles"]:
            rv.append(("maxine avatar events hooking_up vr_goggles",1094,131))
          if school_hook_up_table["watermelon_cut"]:
            rv.append(("maxine avatar events hooking_up watermelon1",1306,264))
          else:
            rv.append(("maxine avatar events hooking_up watermelon2",1306,264))
        if state.endswith(("smile","flirty","angry","afraid","thinking","excited")):
          if state.endswith(("smile","flirty","angry","afraid")):
            rv.append(("maxine avatar events hooking_up power_supply_cable",675,705))
            rv.append(("maxine avatar events hooking_up maxine1_right_arm1_n",995,388))
            rv.append(("maxine avatar events hooking_up maxine1_right_arm1_c",991,378))
          rv.append(("maxine avatar events hooking_up maxine1_body",1005,329))
          rv.append(("maxine avatar events hooking_up maxine1_panties",1037,875))
          rv.append(("maxine avatar events hooking_up maxine1_bra",1054,355))
          rv.append(("maxine avatar events hooking_up maxine1_top",1054,347))
          rv.append(("maxine avatar events hooking_up maxine1_shorts",1005,768))
          rv.append(("maxine avatar events hooking_up maxine1_blouse",1014,354))
          rv.append(("maxine avatar events hooking_up maxine1_necklace",1152,370))
          if "excited" in state:
            rv.append(("maxine avatar events hooking_up maxine1_left_arm1_n",1378,272))
            rv.append(("maxine avatar events hooking_up maxine1_left_arm1_c",1356,272))
          else:
            rv.append(("maxine avatar events hooking_up maxine1_left_arm2_n",1334,455))
            rv.append(("maxine avatar events hooking_up maxine1_left_arm2_c",1334,359))
          if "smile" in state:
            rv.append(("maxine avatar events hooking_up maxine1_head1",953,0))
          elif "flirty" in state:
            rv.append(("maxine avatar events hooking_up maxine1_head2",953,0))
          elif "angry" in state:
            rv.append(("maxine avatar events hooking_up maxine1_head3",953,0))
          elif "afraid" in state:
            rv.append(("maxine avatar events hooking_up maxine1_head4",953,0))
          elif "thinking" in state:
            rv.append(("maxine avatar events hooking_up maxine1_head5",953,0))
          elif "excited" in state:
            rv.append(("maxine avatar events hooking_up maxine1_head6",953,0))
          rv.append(("maxine avatar events hooking_up maxine1_glasses",1077,211))
          rv.append(("maxine avatar events hooking_up maxine1_hat",995,0))
          if "thinking" in state:
            rv.append(("maxine avatar events hooking_up maxine1_right_arm2_n",675,341))
            rv.append(("maxine avatar events hooking_up maxine1_right_arm2_c",675,341))
          elif "excited" in state:
            rv.append(("maxine avatar events hooking_up maxine1_right_arm3_n",675,337))
            rv.append(("maxine avatar events hooking_up maxine1_right_arm3_c",675,337))
        else:
          rv.append(("maxine avatar events hooking_up maxine2_body",929,386))
          rv.append(("maxine avatar events hooking_up maxine2_panties",935,966))
          rv.append(("maxine avatar events hooking_up maxine2_bra",948,429))
          rv.append(("maxine avatar events hooking_up maxine2_top",948,425))
          rv.append(("maxine avatar events hooking_up maxine2_shorts",930,1014))
          if "flirty" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head1",827,205))
          elif "smile" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head2",827,205))
          elif "afraid" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head3",827,205))
          elif "concerned" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head4",827,205))
          elif "orgasm" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head5",827,205))
          elif "ahegao" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head6",827,205))
          elif "out_of_it" in state:
            rv.append(("maxine avatar events hooking_up maxine2_head7",827,205))
          if school_hook_up_table["vr_goggles"]:
            rv.append(("maxine avatar events hooking_up maxine2_vr_goggles",833,233))
          else:
            rv.append(("maxine avatar events hooking_up maxine2_glasses",881,327))
          if school_hook_up_table["watermelon_slice"]:
            rv.append(("maxine avatar events hooking_up maxine2_watermelon1",863,384))
          rv.append(("maxine avatar events hooking_up maxine2_hat",820,152))
          rv.append(("maxine avatar events hooking_up maxine2_necklace",1049,418))
          rv.append(("maxine avatar events hooking_up maxine2_blouse",944,424))
          if state.endswith("cable_insertion"):
            rv.append(("maxine avatar events hooking_up maxine2_arms1_n",675,432))
            rv.append(("maxine avatar events hooking_up maxine2_arms1_c",893,427))
          elif state.endswith("cable_offer"):
            rv.append(("maxine avatar events hooking_up maxine2_arms2_n",675,432))
            rv.append(("maxine avatar events hooking_up maxine2_arms2_c",853,427))
          elif state.endswith("hands_behind_back"):
            rv.append(("maxine avatar events hooking_up maxine2_arms3_n",901,433))
            rv.append(("maxine avatar events hooking_up maxine2_arms3_c",900,425))
            if "cable" in state:
              rv.append(("maxine avatar events hooking_up power_supply_cable",675,705))
            if school_hook_up_table["cable"]:
              rv.append(("maxine avatar events hooking_up maxine2_cable1",1084,535))
            if "grab" in state:
              rv.append(("maxine avatar events hooking_up mc_right_hand1",1071,525))
            if school_hook_up_table["ass_object"] == "watermelon":
              if school_hook_up_table["watermelon_cut"]:
                rv.append(("maxine avatar events hooking_up maxine2_watermelon2",1147,492))
              else:
                rv.append(("maxine avatar events hooking_up maxine2_watermelon3",1146,492))
              rv.append(("maxine avatar events hooking_up mc_right_hand2",1347,568))
            elif school_hook_up_table["ass_object"] == "watermelon_slice":
              rv.append(("maxine avatar events hooking_up maxine2_watermelon4",1097,549))
              rv.append(("maxine avatar events hooking_up mc_right_hand3",1153,559))
            elif school_hook_up_table["ass_object"] == "power_supply_cable":
              rv.append(("maxine avatar events hooking_up maxine2_power_supply_cable",675,634))

      elif state.startswith("surveillance_footage"):
        rv.append((1920,1080))
        if state.endswith("school_ground_floor"):
          rv.append(("maxine avatar events surveillance_footage school ground_floor background",0,0))
          rv.append(("maxine avatar events surveillance_footage school ground_floor lockersback",583,556))
          rv.append(("maxine avatar events surveillance_footage school ground_floor lockersleft",324,551))
          rv.append(("maxine avatar events surveillance_footage school ground_floor lockersfront",552,561))
          rv.append(("maxine avatar events surveillance_footage school ground_floor guard",1306,448))
          rv.append(("maxine avatar events surveillance_footage school ground_floor cafeteria",1140,513))
          rv.append(("maxine avatar events surveillance_footage school ground_floor cafeteria_doors",1171,545))
          rv.append(("maxine avatar events surveillance_footage school ground_floor janitor_door",1562,547))
          rv.append(("maxine avatar events surveillance_footage school ground_floor board",1459,547))
          rv.append(("maxine avatar events surveillance_footage school ground_floor homeroom",483,551))
          if "frame1" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor isabelle",1129,529))
          if "frame2" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor mrsl",1337,581))
          rv.append(("maxine avatar events surveillance_footage school ground_floor couch",695,693))
          if "frame4" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor lindsey",568,573))
          if school_ground_floor["sign_fallen"]:
            rv.append(("maxine avatar events surveillance_footage school ground_floor sign_down",412,730))
          else:
            rv.append(("maxine avatar events surveillance_footage school ground_floor sign",512,680))
          if "frame3" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor maxine",839,654))
          if "frame1" in state or "frame3" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor npc5",1457,265))
          if "frame1" in state or "frame4" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor npc4",347,558))
          if "frame2" in state or "frame4" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor npc3",826,441))
          if "frame3" in state or "frame5" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor npc2",142,460))
          if "frame2" in state or "frame5" in state:
            rv.append(("maxine avatar events surveillance_footage school ground_floor npc1",1566,491))
        elif state.endswith("school_clubroom"):
          rv.append(("maxine avatar events surveillance_footage school_clubroom background",0,0))
          rv.append(("maxine avatar events surveillance_footage school_clubroom trash_can",1352,622))
          rv.append(("maxine avatar events surveillance_footage school_clubroom worldmap",1389,522))
          rv.append(("maxine avatar events surveillance_footage school_clubroom closet",254,76))
          rv.append(("maxine avatar events surveillance_footage school_clubroom blackboard",0,160))
          rv.append(("maxine avatar events surveillance_footage school_clubroom box",0,624))
          rv.append(("maxine avatar events surveillance_footage school_clubroom table",459,601))
          rv.append(("maxine avatar events surveillance_footage school_clubroom books",931,555))
          rv.append(("maxine avatar events surveillance_footage school_clubroom computer",594,542))
          rv.append(("maxine avatar events surveillance_footage school_clubroom typewriter",681,592))
          rv.append(("maxine avatar events surveillance_footage school_clubroom camera",900,663))
          rv.append(("maxine avatar events surveillance_footage school_clubroom bookshelf",1476,16))
          rv.append(("maxine avatar events surveillance_footage school_clubroom chest",1339,718))
          rv.append(("maxine avatar events surveillance_footage school_clubroom newspapers",1582,897))
          if "thinking" in state or "sad" in state:
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine body1",760,175))
            if "thinking" in state:
              rv.append(("maxine avatar events surveillance_footage school_clubroom maxine face_thinking",856,281))
            elif "sad" in state:
              rv.append(("maxine avatar events surveillance_footage school_clubroom maxine face_sad",855,287))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1glasses",850,311))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1bra",796,440))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1panty",808,862))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1top",796,451))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1shorts",780,754))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1blouse",764,466))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1necklace",873,445))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1hat",803,134))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1mask_n",769,419))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b1mask_c",762,487))
          elif "annoyed" in state or "confident" in state or "skeptical" in state:
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine body2",795,171))
            if "annoyed" in state:
              rv.append(("maxine avatar events surveillance_footage school_clubroom maxine face_annoyed",876,275))
            elif "confident" in state:
              rv.append(("maxine avatar events surveillance_footage school_clubroom maxine face_confident",876,278))
            elif "skeptical" in state:
              rv.append(("maxine avatar events surveillance_footage school_clubroom maxine face_skeptical",876,277))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2glasses",868,302))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2bra",850,449))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2panty",798,877))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2top",826,464))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2shorts",791,745))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2blouse",791,470))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2necklace",903,452))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2hat",833,111))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2mask",937,288))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2arm2_n",809,451))
            rv.append(("maxine avatar events surveillance_footage school_clubroom maxine b2arm2_c",817,569))
        rv.append(("maxine avatar events surveillance_footage camera_overlay",0,0))

      elif state.startswith("electrodes_run"):
        rv.append((1920,1080))
        rv.append(("maxine avatar events electrodes_run background",0,0))
        if "lights" in state:
          rv.append(("maxine avatar events electrodes_run generator_lights",101,546))
          if "generator_lights" not in state:
            rv.append(("maxine avatar events electrodes_run computer_lights",181,236))
        rv.append(("maxine avatar events electrodes_run maxine_body",507,277))
        rv.append(("maxine avatar events electrodes_run maxine_underwear",623,321))
        rv.append(("maxine avatar events electrodes_run maxine_clothes",540,274))
        if any(expression in state for expression in ("smile","excited")):
          if "smile" in state:
            rv.append(("maxine avatar events electrodes_run maxine_head1",494,134))
          elif "excited" in state:
            rv.append(("maxine avatar events electrodes_run maxine_head2",494,134))
          rv.append(("maxine avatar events electrodes_run maxine_accessories1",478,93))
        elif any(expression in state for expression in ("neutral","annoyed")):
          if "neutral" in state:
            rv.append(("maxine avatar events electrodes_run maxine_head3",510,113))
          elif "annoyed" in state:
            rv.append(("maxine avatar events electrodes_run maxine_head4",510,113))
          rv.append(("maxine avatar events electrodes_run maxine_accessories2",472,104))
        if "jog" in state:
          rv.append(("maxine avatar events electrodes_run mc_body1",154,0))
        elif " run" in state:
          rv.append(("maxine avatar events electrodes_run mc_body2",154,0))
        elif "faster_run" in state:
          rv.append(("maxine avatar events electrodes_run mc_body3",154,0))
        elif "fastest_run" in state:
          rv.append(("maxine avatar events electrodes_run mc_body4",154,0))
        else:
          rv.append(("maxine avatar events electrodes_run mc_body5",154,0))

      elif state.startswith("electrodes_sex"):
        rv.append((1920,1080))
        rv.append(("maxine avatar events electrodes_sex background",0,0))
        if "lights" in state:
          rv.append(("maxine avatar events electrodes_sex generator_lights",625,0))
          if "generator_lights" not in state:
            rv.append(("maxine avatar events electrodes_sex computer_lights",1192,0))
        if any(expression in state for expression in ("orgasm","aftermath")):
          rv.append(("maxine avatar events electrodes_sex maxine_body1",377,0))
          if "orgasm" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face1",837,0))
          elif "aftermath" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face2",837,0))
          rv.append(("maxine avatar events electrodes_sex maxine_glasses1",851,112))
        else:
          rv.append(("maxine avatar events electrodes_sex maxine_body2",377,0))
          if "surprised" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face3",844,75))
          elif "lip_bite" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face4",844,75))
          elif "concerned" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face5",844,75))
          elif "startled" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face6",844,75))
          elif "eyes_closed" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face7",844,75))
          elif "excited" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face8",844,75))
          elif "pleading" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_face9",844,75))
          if "gripping_table" in state:
            rv.append(("maxine avatar events electrodes_sex maxine_glasses2",860,203))
          else:
            rv.append(("maxine avatar events electrodes_sex maxine_glasses3",861,202))
        if "gripping_table" in state:
          rv.append(("maxine avatar events electrodes_sex maxine_left_arm1",1060,297))
        else:
          rv.append(("maxine avatar events electrodes_sex maxine_left_arm2",1070,297))
        if "juices" in state:
          rv.append(("maxine avatar events electrodes_sex maxine_juices",882,831))
        if "lining_dick_up" in state:
          rv.append(("maxine avatar events electrodes_sex mc_body1",575,59))
        elif "inserting_dick" in state:
          rv.append(("maxine avatar events electrodes_sex mc_body2",575,59))
        elif "full_penetration" in state:
          rv.append(("maxine avatar events electrodes_sex mc_body3",568,59))
        elif "partial_penetration" in state:
          rv.append(("maxine avatar events electrodes_sex mc_body4",574,59))
        elif "no_penetration" in state:
          rv.append(("maxine avatar events electrodes_sex mc_body5",580,59))
        else:
          rv.append(("maxine avatar events electrodes_sex mc_body6",575,59))
        if "cum" in state:
          if "orgasm" in state:
            rv.append(("maxine avatar events electrodes_sex mc_cum1",899,807))
          elif "aftermath" in state:
            rv.append(("maxine avatar events electrodes_sex mc_cum2",862,781))
        if "grabbing_dick" in state:
          rv.append(("maxine avatar events electrodes_sex maxine_right_arm1",727,294))
        elif "lining_dick_up" in state:
          rv.append(("maxine avatar events electrodes_sex maxine_right_arm2",736,294))
        elif "inserting_dick" in state:
          rv.append(("maxine avatar events electrodes_sex maxine_right_arm3",727,294))
        elif "gripping_table" in state:
          rv.append(("maxine avatar events electrodes_sex maxine_right_arm4",553,295))
        else:
          rv.append(("maxine avatar events electrodes_sex maxine_right_arm5",564,295))

      elif state.startswith("chair"):
        rv.append((1920,1080))
        rv.append(("maxine avatar events chair background",0,0))
        if state.endswith("back"):
          rv.append(("maxine avatar events chair maxine_body1",824,152))
          rv.append(("maxine avatar events chair maxine_clothes1",830,103))
          rv.append(("maxine avatar events chair chair1",0,368))
        elif state.endswith("front"):
          rv.append(("maxine avatar events chair chair2",0,382))
          rv.append(("maxine avatar events chair maxine_body2",651,127))
          rv.append(("maxine avatar events chair maxine_underwear",830,436))
          rv.append(("maxine avatar events chair maxine_clothes2",641,73))

      elif state.startswith("cave_fight"):
        rv.append((1920,1080))
        if state.endswith("earthworm_blur"):
          rv.append(("maxine avatar events cave_fight background1",0,0))
          rv.append(("maxine avatar events cave_fight earthworm1",366,0))
        elif state.endswith("earthworm"):
          rv.append(("maxine avatar events cave_fight background2",0,0))
          rv.append(("maxine avatar events cave_fight earthworm2",376,0))
        elif state.endswith(("first_strike","second_strike_incoming","second_strike","third_strike","third_strike_aftermath","fourth_strike","fifth_strike_incoming")):
          rv.append(("maxine avatar events cave_fight background3",0,0))
          if state.endswith(("third_strike_aftermath","fourth_strike","fifth_strike_incoming")):
            rv.append(("maxine avatar events cave_fight earthworm_blood5",1261,690))
          rv.append(("maxine avatar events cave_fight maxine_body",263,331))
          rv.append(("maxine avatar events cave_fight maxine_panties",532,696))
          rv.append(("maxine avatar events cave_fight maxine_bra",614,518))
          rv.append(("maxine avatar events cave_fight maxine_armor",262,499))
          rv.append(("maxine avatar events cave_fight maxine_glasses",675,427))
          if state.endswith(("first_strike","second_strike_incoming","third_strike","third_strike_aftermath")):
            rv.append(("maxine avatar events cave_fight maxine_arms1",595,514))
            rv.append(("maxine avatar events cave_fight maxine_sleeves1",589,503))
            rv.append(("maxine avatar events cave_fight earthworm_blood1",543,428))
          elif state.endswith(("second_strike","fourth_strike","fifth_strike_incoming")):
            rv.append(("maxine avatar events cave_fight maxine_arms2",507,277))
            rv.append(("maxine avatar events cave_fight maxine_sleeves2",501,447))
            rv.append(("maxine avatar events cave_fight earthworm_blood2",496,259))
          if not state.endswith("first_strike"):
            rv.append(("maxine avatar events cave_fight mc_body",1265,152))
            if state.endswith(("second_strike_incoming","third_strike","third_strike_aftermath","fifth_strike_incoming")):
              rv.append(("maxine avatar events cave_fight mc_arms1",1321,0))
              if not state.endswith("second_strike_incoming"):
                rv.append(("maxine avatar events cave_fight earthworm_blood4",971,28))
            elif state.endswith(("second_strike","fourth_strike")):
              rv.append(("maxine avatar events cave_fight mc_arms2",990,327))
              rv.append(("maxine avatar events cave_fight earthworm_blood3",937,347))
          if state.endswith(("third_strike_aftermath","fourth_strike","fifth_strike_incoming")):
            rv.append(("maxine avatar events cave_fight earthworm_blood6",713,631))

      elif state.startswith("cave_kiss"):
        rv.append((1920,1080))
        rv.append(("maxine avatar events cave_kiss background",0,0))
        rv.append(("maxine avatar events cave_kiss mc_body",513,148))
        rv.append(("maxine avatar events cave_kiss maxine_body",349,99))
        rv.append(("maxine avatar events cave_kiss maxine_panties",422,730))
        rv.append(("maxine avatar events cave_kiss maxine_bra",614,393))
        rv.append(("maxine avatar events cave_kiss maxine_armor",342,370))
        rv.append(("maxine avatar events cave_kiss maxine_glasses",795,276))

      elif state.startswith("cave_sex"):
        expression = None if state.endswith(("anticipation","insertion1","insertion2","insertion3","penetration1","penetration2","penetration3","penetration4","penetration5","penetration6","penetration7","penetration8","penetration9","orgasm","penetration10","penetration11","penetration12","cum_inside","stroke1","stroke2","cum_outside","cum_outside_aftermath")) else state.rsplit(" ", 1)[1]
        state = state if state.endswith(("anticipation","insertion1","insertion2","insertion3","penetration1","penetration2","penetration3","penetration4","penetration5","penetration6","penetration7","penetration8","penetration9","orgasm","penetration10","penetration11","penetration12","cum_inside","stroke1","stroke2","cum_outside","cum_outside_aftermath")) else state.rsplit(" ", 1)[0]
        rv.append((1920,1080))
        if state.endswith(("anticipation","insertion1","insertion2","insertion3","penetration1","penetration2","penetration3","penetration4","penetration5","penetration6","penetration7","penetration8","penetration9","orgasm","penetration10","penetration11","penetration12","cum_inside")):
          rv.append(("maxine avatar events cave_sex background1",0,0))
        elif state.endswith(("stroke1","stroke2","cum_outside","cum_outside_aftermath")):
          rv.append(("maxine avatar events cave_sex background2",0,0))
        if state.endswith(("anticipation","insertion1","insertion2","insertion3","penetration1","penetration2","penetration3","penetration7","penetration8","penetration9")):
          rv.append(("maxine avatar events cave_sex mc_body1",410,459))
        if state.endswith(("anticipation","insertion3","penetration1","penetration7")):
          rv.append(("maxine avatar events cave_sex maxine_body1",656,110))
        elif state.endswith(("insertion2","penetration2","penetration8")):
          rv.append(("maxine avatar events cave_sex maxine_body2",656,92))
        elif state.endswith(("insertion1","penetration3","penetration9")):
          rv.append(("maxine avatar events cave_sex maxine_body3",656,75))
        elif state.endswith("penetration4"):
          rv.append(("maxine avatar events cave_sex maxine_body4",408,75))
        elif state.endswith("penetration5"):
          rv.append(("maxine avatar events cave_sex maxine_body5",409,57))
        elif state.endswith("penetration6"):
          rv.append(("maxine avatar events cave_sex maxine_body6",410,44))
        elif state.endswith(("orgasm","cum_inside")) and not expression:
          rv.append(("maxine avatar events cave_sex maxine_body7",408,70))
        elif (state.endswith("orgasm") and expression.endswith("concerned")) or state.endswith("penetration10") or (state.endswith("cum_inside") and expression.endswith("flirty")):
          rv.append(("maxine avatar events cave_sex maxine_body8",408,75))
        elif state.endswith("penetration11"):
          rv.append(("maxine avatar events cave_sex maxine_body9",409,75))
        elif state.endswith("penetration12"):
          rv.append(("maxine avatar events cave_sex maxine_body10",410,75))
        elif state.endswith(("stroke1","stroke2","cum_outside","cum_outside_aftermath")):
          rv.append(("maxine avatar events cave_sex maxine_body11",0,0))
        if state.endswith("anticipation"):
          rv.append(("maxine avatar events cave_sex maxine_face1",858,189))
        elif state.endswith("insertion1") and not expression:
          rv.append(("maxine avatar events cave_sex maxine_face2",860,154))
        elif state.endswith("insertion1") and expression.endswith("smile"):
          rv.append(("maxine avatar events cave_sex maxine_face3",860,154))
        elif state.endswith("insertion2"):
          rv.append(("maxine avatar events cave_sex maxine_face4",859,172))
        elif state.endswith("insertion3") and not expression:
          rv.append(("maxine avatar events cave_sex maxine_face5",858,189))
        elif (state.endswith("insertion3") and expression.endswith("flirty")) or (state.endswith(("penetration1","penetration7")) and not expression):
          rv.append(("maxine avatar events cave_sex maxine_face6",858,189))
        elif state.endswith(("penetration2","penetration8")) and not expression:
          rv.append(("maxine avatar events cave_sex maxine_face7",859,172))
        elif state.endswith(("penetration3","penetration9")) and not expression:
          rv.append(("maxine avatar events cave_sex maxine_face8",860,154))
        elif state.endswith(("penetration1","penetration7")) and expression.endswith("aroused"):
          rv.append(("maxine avatar events cave_sex maxine_face9",858,189))
        elif state.endswith("penetration2") and expression.endswith("aroused"):
          rv.append(("maxine avatar events cave_sex maxine_face10",859,172))
        elif (state.endswith("penetration3") and expression.endswith("aroused")) or state.endswith("penetration4"):
          rv.append(("maxine avatar events cave_sex maxine_face11",860,154))
        elif state.endswith("penetration5"):
          rv.append(("maxine avatar events cave_sex maxine_face12",892,138))
        elif state.endswith("penetration6"):
          rv.append(("maxine avatar events cave_sex maxine_face13",926,126))
        elif state.endswith(("orgasm","cum_inside")) and not expression:
          rv.append(("maxine avatar events cave_sex maxine_face14",854,143))
        elif state.endswith("orgasm") and expression.endswith("concerned"):
          rv.append(("maxine avatar events cave_sex maxine_face15",860,154))
        elif state.endswith(("penetration10","penetration11","penetration12")):
          rv.append(("maxine avatar events cave_sex maxine_face16",860,154))
        elif state.endswith("cum_inside") and expression.endswith("flirty"):
          rv.append(("maxine avatar events cave_sex maxine_face17",860,154))
        elif state.endswith(("stroke1","stroke2","cum_outside","cum_outside_aftermath")) and not expression:
          rv.append(("maxine avatar events cave_sex maxine_face18",1338,153))
        elif state.endswith("cum_outside_aftermath") and expression.endswith("annoyed"):
          rv.append(("maxine avatar events cave_sex maxine_face19",1338,153))
        elif state.endswith("cum_outside_aftermath") and expression.endswith("angry"):
          rv.append(("maxine avatar events cave_sex maxine_face20",1338,153))
        if state.endswith(("anticipation","insertion3","penetration1","penetration7")):
          rv.append(("maxine avatar events cave_sex maxine_glasses1",870,226))
        elif state.endswith(("insertion2","penetration2","penetration8")):
          rv.append(("maxine avatar events cave_sex maxine_glasses2",871,209))
        elif state.endswith(("insertion1","penetration3","penetration4","penetration9","penetration10","penetration11","penetration12")) or (state.endswith("orgasm") and expression and expression.endswith("concerned")) or (state.endswith("cum_inside") and expression and expression.endswith("flirty")):
          rv.append(("maxine avatar events cave_sex maxine_glasses3",872,191))
        elif state.endswith("penetration5"):
          rv.append(("maxine avatar events cave_sex maxine_glasses4",905,173))
        elif state.endswith("penetration6"):
          rv.append(("maxine avatar events cave_sex maxine_glasses5",939,157))
        elif state.endswith(("orgasm","cum_inside")):
          rv.append(("maxine avatar events cave_sex maxine_glasses6",863,193))
        elif state.endswith(("stroke1","stroke2","cum_outside","cum_outside_aftermath")):
          rv.append(("maxine avatar events cave_sex maxine_glasses7",1405,164))
        if state.endswith(("anticipation","insertion3")):
          rv.append(("maxine avatar events cave_sex maxine_arms1",733,349))
        elif state.endswith("insertion2"):
          rv.append(("maxine avatar events cave_sex maxine_arms2",734,332))
        elif state.endswith("insertion1"):
          rv.append(("maxine avatar events cave_sex maxine_arms3",736,314))
        elif state.endswith(("penetration1","penetration7")):
          rv.append(("maxine avatar events cave_sex maxine_arms4",780,354))
        elif state.endswith(("penetration2","penetration8")):
          rv.append(("maxine avatar events cave_sex maxine_arms5",781,337))
        elif state.endswith(("penetration3","penetration9")):
          rv.append(("maxine avatar events cave_sex maxine_arms6",783,319))
        if state.endswith(("stroke1","stroke2","cum_outside","cum_outside_aftermath")):
          rv.append(("maxine avatar events cave_sex mc_body2",0,88))
        if state.endswith(("anticipation","penetration7","penetration8","penetration9","orgasm")):
          rv.append(("maxine avatar events cave_sex mc_left_arm1",695,780))
        elif state.endswith(("insertion3","penetration1")):
          rv.append(("maxine avatar events cave_sex mc_left_arm2",787,779))
        elif state.endswith(("insertion2","penetration2")):
          rv.append(("maxine avatar events cave_sex mc_left_arm3",797,779))
        elif state.endswith(("insertion1","penetration3")):
          rv.append(("maxine avatar events cave_sex mc_left_arm4",807,779))
        elif state.endswith(("penetration10","cum_inside")):
          rv.append(("maxine avatar events cave_sex mc_left_arm5",724,767))
        elif state.endswith("penetration11"):
          rv.append(("maxine avatar events cave_sex mc_left_arm6",724,774))
        elif state.endswith("penetration12"):
          rv.append(("maxine avatar events cave_sex mc_left_arm7",724,780))
        if state.endswith(("stroke1","cum_outside_aftermath")):
          rv.append(("maxine avatar events cave_sex mc_right_arm1",0,646))
        elif state.endswith(("stroke2","cum_outside")):
          rv.append(("maxine avatar events cave_sex mc_right_arm2",0,678))
        if state.endswith(("penetration1","penetration7")):
          rv.append(("maxine avatar events cave_sex maxine_juices1",921,783))
        elif state.endswith(("orgasm","penetration10","cum_inside")):
          rv.append(("maxine avatar events cave_sex maxine_juices2",909,744))
        if state.endswith("anticipation"):
          rv.append(("maxine avatar events cave_sex mc_dick",883,707))
        if state.endswith("cum_inside"):
          rv.append(("maxine avatar events cave_sex mc_cum1",877,704))
        elif state.endswith("cum_outside"):
          rv.append(("maxine avatar events cave_sex mc_cum2",263,583))
        elif state.endswith("cum_outside_aftermath"):
          rv.append(("maxine avatar events cave_sex mc_cum3",189,530))
        if state.endswith(("anticipation","insertion1","insertion2","insertion3","penetration1","penetration2","penetration3","penetration4","penetration5","penetration6","penetration7","penetration8","penetration9","orgasm","penetration10","penetration11","penetration12","cum_inside")):
          rv.append(("maxine avatar events cave_sex foreground",0,0))

      return rv


  class Interactable_maxine(Interactable):

    def title(cls):
      return maxine.name

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
      ##Misc##
      if maxine["at_science_class_this_scene"]:
        return
      #########

      ##Talks##
      if not ((quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or quest.jo_washed.in_progress
      or (quest.lindsey_voluntary == "investigation" and game.location == "school_science_class")
      or (quest.maxine_dive == "dive_2_electric_boogaloo" and game.location == "school_park")
      or quest.maxine_dive == "cave"):
        if maxine["talk_limit_today"] < 2:
          actions.append(["talk","Talk","?maxine_talk_one"])
          actions.append(["talk","Talk","?maxine_talk_two"])
          actions.append(["talk","Talk","?maxine_talk_three"])
          actions.append(["talk","Talk","?maxine_talk_four"])
        else:
          actions.append(["talk","Talk","?maxine_talk_over"])
      #########

      ##Quests##
      if mc["focus"]:
        if mc["focus"] == "maxine_hook":
          if quest.maxine_hook == "shovel":
            if mc.owned_item("shovel"):
              actions.append(["quest","Quest","quest_maxine_hook_shovel"])
            else:
              actions.append(["quest","Quest","?quest_maxine_hook_no_shovel"])
          elif quest.maxine_hook == "dig":
            actions.append(["quest","Quest","?quest_maxine_hook_dig"])
          elif quest.maxine_hook == "interrogation":
            actions.append(["quest","Quest","?quest_maxine_hook_interrogation_maxine"])
        elif mc["focus"] == "kate_stepping":
          if quest.kate_stepping == "meeting" and game.location == "school_clubroom":
            actions.append(["quest","Quest","quest_kate_stepping_meeting_maxine"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19:
            if "maxine" not in quest.isabelle_dethroning["dinner_conversations"]:
              actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_maxine"])
            elif not quest.isabelle_dethroning["dinner_picture"]:
              actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_maxine"])
            else:
              actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_picture"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed == "supplies" and not mc.owned_item("garden_hose"):
            actions.append(["quest","Quest","quest_jo_washed_supplies_garden_hose"])
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
        elif mc["focus"] == "maya_witch":
          if quest.maya_witch == "meet_up" and game.location == "school_clubroom":
            actions.append(["quest","Quest","quest_maya_witch_meet_up"])
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "maxine":
            actions.append(["quest","Quest","quest_kate_moment_maxine"])
        elif mc["focus"] == "maxine_dive":
          if quest.maxine_dive == "cave" and "maxine" not in quest.maxine_dive["cave_interactables"]:
            actions.append(["quest","Quest","quest_maxine_dive_cave"])
      else:
        if game.season == 1:
          if (quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access.finished and not quest.nurse_venting.started
          and maxine.at("school_clubroom","fishing")):
            actions.append(["quest","Quest","?quest_nurse_venting_start"])

        ## Buried Truths
        if quest.isabelle_buried == "buried_maxine":
          actions.append(["quest","Quest","?quest_isabelle_buried_maxine"])

        ## The Ley of the Land
        if quest.maxine_lines == "locator" and not quest.kate_trick.in_progress:
          actions.append(["quest","Quest","maxine_lines_locator"])
        elif quest.maxine_lines == "shot" and game.location == "school_gym":
          actions.append(["quest","Quest","?maxine_lines_shot_interact"])
        elif quest.maxine_lines == "reward":
          actions.append(["quest","Quest","maxine_lines_reward_interact_maxine"])
        elif quest.maxine_lines == "insight" and game.location == "school_clubroom":
          actions.append(["quest","Quest","quest_maxine_lines_insight"])
        elif quest.maxine_lines == "seemaxine":
          actions.append(["quest","Quest","quest_maxine_lines_seemaxine"])

        ## Strawberries, Cherries
        if quest.maxine_wine == "maxinewine" and maxine.at("school_clubroom"):
          actions.append(["quest","Quest","?quest_maxine_wine_make_a_move"])

        ## The Glowing Spider Eggs
        if quest.maxine_eggs == "deliver" and maxine.at("school_clubroom"):
          actions.append(["quest","Quest","quest_maxine_eggs_deliver"])
        elif quest.maxine_eggs == "maxine" and maxine.at("school_clubroom") and not "stuck" > quest.nurse_venting > "supplies":
          actions.append(["quest","Quest","quest_maxine_eggs_meeting_soon"])

        ## Gathering Storm
        if quest.isabelle_locker == "interrogate":
          if quest.isabelle_locker["be_right_back"]:
            actions.append(["quest","Quest","quest_isabelle_locker_interrogate_maxine_brb"])
          else:
            actions.append(["quest","Quest","quest_isabelle_locker_interrogate_maxine"])
        elif quest.isabelle_locker == "catnap":
          if quest.isabelle_locker["be_back"]:
            actions.append(["quest","Quest","quest_isabelle_locker_catnap_maxine_back"])
          else:
            actions.append(["quest","Quest","quest_isabelle_locker_catnap_maxine"])
        elif quest.isabelle_locker == "repeat":
          actions.append(["quest","Quest","quest_isabelle_locker_repeat"])

        ## Hooking Up
        if quest.maxine_hook == "wake":
          actions.append(["quest","Quest","quest_maxine_hook_wake"])
        elif quest.maxine_hook == "computer":
          actions.append(["quest","Quest","quest_maxine_hook_computer"])
        elif quest.maxine_hook == "thief" and maxine.at("school_clubroom"):
          actions.append(["quest","Quest","quest_maxine_hook_thief"])

        ## Venting Frustration
        if quest.nurse_venting == "supplies":
          actions.append(["quest","Quest","quest_nurse_venting_supplies_maxine"])

        ## A Beautiful Motive
        if quest.lindsey_motive == "maxine":
          actions.append(["quest","Quest","quest_lindsey_motive_maxine"])

        ## Jo's Day
        if quest.jo_day == "maxine":
          actions.append(["quest","Quest","quest_jo_day_maxine"])
        elif quest.jo_day == "love_potion":
          if mc.owned_item("queen_rue"):
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:jo_day,love_potion|Give What?","quest_jo_day_love_potion"])
          else:
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:jo_day,bottled_water|Give What?","quest_jo_day_love_potion_water"])

        ## The Dog Trick
        if quest.kate_trick == "maxine":
          actions.append(["quest","Quest","quest_kate_trick_maxine"])

        ## Dethroning the Queen
        if quest.isabelle_dethroning == "meeting" and game.location == "school_clubroom":
          actions.append(["quest","Quest","quest_isabelle_dethroning_meeting"])
        elif quest.isabelle_dethroning == "invitations" and "maxine" not in quest.isabelle_dethroning["invitations"]:
          actions.append(["quest","Quest","quest_isabelle_dethroning_invitations_maxine"])

        ## Big Witch Energy
        if quest.maya_witch == "help":
          actions.append(["quest","Quest","quest_maya_witch_help"])

        ## Voluntary Good
        if quest.lindsey_voluntary == "investigation" and game.location == "school_science_class":
          actions.append(["quest","Quest","quest_lindsey_voluntary_investigation"])

        ## The Dive
        if quest.maxine_dive == "dive_2_electric_boogaloo" and game.location == "school_park":
          actions.append(["quest","Quest","quest_maxine_dive_dive_2_electric_boogaloo"])

        ## Service Aid
        if quest.nurse_aid == "fishing_pole" and (quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished):
          actions.append(["quest","Quest","quest_nurse_aid_fishing_pole_maxine"])

        ##### unstarted quests #######
        if game.season == 1:
          if quest.isabelle_tour.finished and not quest.maxine_wine.started and game.location == "school_clubroom":
            actions.append(["quest","Quest","?quest_maxine_wine_start"])

          if quest.isabelle_tour.finished and not quest.maxine_eggs.started and game.location == "school_clubroom":
            actions.append(["quest","Quest","quest_maxine_eggs_start"])

          if quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.clubroom_access.finished and not quest.maxine_lines.started and game.location == "school_gym":
            actions.append(["quest","Quest","quest_maxine_lines_start"])

          if quest.isabelle_tour.finished and quest.berb_fight.finished and quest.clubroom_access.finished and quest.maxine_eggs.finished and quest.maxine_lines.finished and not quest.maxine_hook.started and not quest.isabelle_buried.in_progress:
            actions.append(["quest","Quest","quest_maxine_hook_start"])
      ##########

      ##Flirts##
      if not ((quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or quest.jo_washed.in_progress
      or (quest.lindsey_voluntary == "investigation" and game.location == "school_science_class")
      or (quest.maxine_dive == "dive_2_electric_boogaloo" and game.location == "school_park")
      or quest.maxine_dive == "cave"):
        if maxine["flirt_unlocked"]:
          if maxine["flirt_limit_today"] < 3:
            actions.append(["flirt","Flirt","?maxine_flirt_one"])
            actions.append(["flirt","Flirt","?maxine_flirt_two"])
            actions.append(["flirt","Flirt","?maxine_flirt_three"])
          else:
            actions.append(["flirt","Flirt","?maxine_flirt_over"])
        else:
          actions.append(["flirt","Flirt","?maxine_flirt_default"])
      ##########


label maxine_talk_one:
  show maxine afraid with Dissolve(.5)
  maxine afraid "Not now, the Vending Corporation is trying to pull the wool over my eyes!"
  $maxine["talk_limit_today"]+=1
  hide maxine with Dissolve(.5)
  return

label maxine_talk_two:
  show maxine skeptical with Dissolve(.5)
  maxine skeptical "The specimen in Area 12 should not be touched, ingested, or worshipped. It is the most dangerous living thing in the school."
  $maxine["talk_limit_today"]+=1
  hide maxine with Dissolve(.5)
  return

label maxine_talk_three:
  show maxine concerned with Dissolve(.5)
  maxine concerned "Things are never as they appear, [mc]. You can never truly see what's behind you."
  $maxine["talk_limit_today"]+=1
  hide maxine with Dissolve(.5)
  return

label maxine_talk_four:
  show maxine laughing_popcorn with Dissolve(.5)
  mc "Hey, [maxine]! What are you up to?"
  maxine smile_popcorn "Just enjoying the show."
  mc "What show?"
  maxine neutral_popcorn "Don't mind me."
  $maxine["talk_limit_today"]+=1
  hide maxine with Dissolve(.5)
  return

label maxine_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label maxine_flirt_default:
  show maxine skeptical with Dissolve(.5)
  maxine skeptical "[mc], I really would suggest the acquiescence of your preferred pimple cream."
  hide maxine with Dissolve(.5)
  return

label maxine_flirt_one:
  show maxine skeptical with Dissolve(.5)
  maxine skeptical "This is a routine safety exercise, [mc]. Please answer the following question."
  maxine skeptical "What is the most dangerous thing in the school?"
  show maxine skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"The specimen in Area 12.\"":
      show maxine skeptical at move_to(.5)
      mc "The specimen in Area 12."
      if not maxine["flirt_bonuses_chosen"]:
        $maxine.love+=1
      maxine smile "I see you've been paying attention."
    "\"Ignorance.\"":
      show maxine skeptical at move_to(.5)
      mc "Ignorance."
      if not maxine["flirt_bonuses_chosen"]:
        $maxine.lust+=1
      maxine flirty "That answer is always correct!"
    "\"[kate]'s wrath.\"":
      show maxine skeptical at move_to(.5)
      mc "[kate]'s wrath."
      maxine excited "Dangerous, sure, but not the most dangerous thing in the school."
      mc "I beg to differ."
    "\"The chemicals in the science classroom.\"":
      show maxine skeptical at move_to(.5)
      mc "The chemicals in the science classroom."
      maxine smile "Oh, you sweet summer child..."
  $maxine["flirt_limit_today"]+=1
  hide maxine with Dissolve(.5)
  return

label maxine_flirt_two:
  show maxine annoyed with Dissolve(.5)
  maxine annoyed "Are you hitting on me again?"
  mc "Me?"
  maxine eyeroll "Yes, you."
  mc "I don't think so?"
  maxine skeptical "That's not what it looked like to me."
  show maxine skeptical at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I was just looking for the popcorn dispenser.\"":
      show maxine skeptical at move_to(.5)
      mc "I was just looking for the popcorn dispenser."
      maxine thinking "What for?"
      mc "Just so I can better enjoy the show."
      if not maxine["flirt_bonuses_chosen"]:
        $maxine.love+=1
      maxine excited "Oh! Well, the dispenser is in the science classroom."
    "\"Okay, fine. I was trying to pick you up.\"":
      show maxine skeptical at move_to(.5)
      mc "Okay, fine. I was trying to pick you up."
      maxine neutral "I don't require an elevated position right now."
      maxine smile "But I'll let you know if I do in the future."
      mc "That's not..."
    "\"What did it look like to you?\"":
      show maxine skeptical at move_to(.5)
      mc "What did it look like to you?"
      maxine smile "I'm glad you asked."
      maxine neutral "To me, it looked like you were trying to put your penis inside me."
      mc "That's... I wasn't!"
      maxine concerned "Oh? Okay... if you want it the other way around, then you should ask [jacklyn] or [kate]."
      mc "What? No! That's not what I meant!"
  $maxine["flirt_limit_today"]+=1
  hide maxine with Dissolve(.5)
  return

label maxine_flirt_three:
  show maxine neutral with Dissolve(.5)
  mc "Hey, [maxine]!"
  maxine smile "Greetings, fellow Newfaller."
  mc "I just wanted to tell you that..."
  show maxine smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You look absolutely delicious today!\"":
      show maxine smile at move_to(.5)
      mc "You look absolutely delicious today!"
      maxine thinking "So does the poison dart frog, yet they're highly poisonous."
      mc "Are you saying you're poisonous or that you're a frog?"
      maxine excited "Indeed."
      mc "..."
    "\"I've been trying to correct my past.\"":
      show maxine smile at move_to(.5)
      if not maxine["flirt_bonuses_chosen"]:
        $mc.intellect+=1
      mc "I've been trying to correct my past."
      maxine eyeroll "That is impossible."
      mc "Never heard of time traveling?"
      maxine skeptical "Logic dictates that once you've traveled back in time, you're correcting the future."
      maxine skeptical "Time is a human invention. The past only exists in your memories. The world is but an accumulation of billions of memories."
      mc "God, you're making my head hurt..."
    "\"I've been working out.\"":
      show maxine smile at move_to(.5)
      if not maxine["flirt_bonuses_chosen"]:
        $mc.strength+=1
      mc "I've been working out."
      maxine concerned "Why?"
      mc "Because I thought you'd like it."
      maxine thinking "Why would I like you tightening and releasing your muscles repeatedly?"
      mc "I meant that you might like me better now."
      maxine smile "The size of your muscle fibers is of no consequence to me."
      "That does make sense... somehow."
    "\"I turned around.\"":
      show maxine smile at move_to(.5)
      mc "I turned around."
      maxine neutral "And what did you see?"
      mc "Are you serious? What do you see other than the room?"
      maxine skeptical "A screen..."
      maxine skeptical "A person."
      mc "A person?"
      maxine concerned "Some kind of primate, at least."
      mc "There's a monkey behind me?!"
      maxine flirty "I'm glad you're finally starting to question your reality, but I've already said too much."
      "What the fuck is she on about?"
  $maxine["flirt_limit_today"]+=1
  $maxine["flirt_bonuses_chosen"] = True
  hide maxine with Dissolve(.5)
  return

label maxine_flirt_over:
  "She must be sick of my poor attempts at flirting by now..."
  return
