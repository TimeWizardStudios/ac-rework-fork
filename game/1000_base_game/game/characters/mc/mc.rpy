init python:
  class Character_mc(BaseChar):
    notify_inventory_changed=True
    notify_outfit_changed=False
    notify_money_changed=True
    notify_phone_app_installed=False
    notify_phone_app_uninstalled=False
    notify_phone_contact_added=True
    notify_xp_granted=False
    notify_level_changed=True

    default_name="" #"Holden"

    default_stats=[
      ["energy",100,0],
      ["intellect",0,0],
      ["strength",0,0],
      ["charisma",0,0],
      ["lust",0,0],
      ["love",0,0],
      ]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.install_phone_app("selector",True)
      self.install_phone_app("hints",True)
      self.install_phone_app("stats",True)
      self.install_phone_app("stat_perks",True)
      self.install_phone_app("messages",True)  #Remove this later if you can confirm it doesnt break saves. 
      self.install_phone_app("call",True)
      self.install_phone_app("contacts",True)
      self.install_phone_app("contact_info",True)
      self.install_phone_app("achievements",True)
      self.install_phone_app("replay",True)
