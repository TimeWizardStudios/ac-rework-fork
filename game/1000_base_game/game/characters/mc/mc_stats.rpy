init python:
  class IntellectStat(BaseStat):
    id="intellect"
    title="Intellect"
    color="#00F"
    icon="stats int"
    notify_xp_granted=False
    notify_level_changed=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]

  class StrengthStat(BaseStat):
    id="strength"
    title="Strength"
    color="#F00"
    icon="stats str"
    notify_xp_granted=False
    notify_level_changed=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]

  class CharismaStat(BaseStat):
    id="charisma"
    title="Charisma"
    color="#0F0"
    icon="stats cha"
    notify_xp_granted=False
    notify_level_changed=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]

  class EnergyStat(BaseStat):
    id="energy"
    title="Energy"
    color="#FFF"
    icon=None
    notify_xp_granted=False
    notify_level_changed=False
    hidden=True
    min_level=0
    max_level=100
    xp_to_next_level=[1 for n in range(max_level-min_level+1)]
