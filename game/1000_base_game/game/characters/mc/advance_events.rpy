init python:
  class Event_player_character_energy(GameEvent):
    priority=10
    def on_advance(event):
      game.pc.energy=game.pc.max_energy
    def on_enter_roaming_mode(event):
      if game.pc and game.pc.energy<=game.pc.min_energy:
        game.advance()
#    def on_stat_level_changed(event,char,stat,old_level,level,silent=False):
#      if char==game.pc and stat=="energy" and level<=char.min_energy:
#        game.advance()

  class Event_player_force_go_home_at_night(GameEvent):
    priority=9
    #def on_advance(event):
      #if not game.location.id.startswith("home_"):
        #if game.hour in (19,20,21,22,23,0,1,2,3,4,5,6):
          #if quest.day1_take2.finished:
            #if not quest.kate_blowjob_dream in ("school","awake") and not quest.isabelle_haggis in ("instructions","puzzle"):
              #if quest.isabelle_buried in ("wait","bury","dig","funeral") and not mc.at("school_forest_glade"):
                #game.events_queue.append("event_player_force_go_home_isabelle_buried")
              #elif quest.jacklyn_statement == ("statement"):
                #return
              #elif quest.isabelle_buried not in ("bury","dig","funeral"):
                #game.events_queue.append("event_player_force_go_home_at_night")
              #pass
      #else:#game location starts with home
        #if game.hour in (20,21,22,23,0,1,2,3,4,5,6):#it's night
          #game.location['night'] = True
          #for id,character in game.characters.items():
            #if character.at(game.location) and not id in (mc.id,mc.name):
              #game.location['night'] = False
        #else:#it's day
          #game.location['night'] = False
    def on_advance(event):
      process_event("update_state")
      if game.hour in (19,20,21,22,23,0,1,2,3,4,5,6):
        if quest.day1_take2.finished:
          if quest.isabelle_buried == "wait" and not (quest.maxine_eggs == "bathroom" or mc["focus"]):
            if mc.at("school_forest_glade"):
              mc["focus"] = "isabelle_buried"
              quest.isabelle_buried.advance("bury")
            else:
              game.events_queue.append("event_player_force_go_home_isabelle_buried")
          elif (quest.isabelle_buried in ("bury","dig","funeral")
          or quest.jacklyn_statement == "statement"
          or quest.maxine_eggs == "bathroom"
          or quest.kate_fate in ("peek","hunt","rescue","run","hunt2","deadend")
          or quest.lindsey_piano == "listen_music_class"
          or quest.jo_day == "picnic"
          or quest.kate_stepping == "party"
          or (quest.kate_wicked == "school" and quest.kate_wicked["party_tonight"])
          or quest.isabelle_dethroning == "dinner"
          or quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape","hospital")
          or quest.kate_moment == "fun"
          or quest.maya_spell in ("midnight","busted")
          or quest.kate_intrigue == "lion_den"
          or quest.isabelle_gesture == "revenge"
          or quest.lindsey_voluntary in ("wait","volunteer","dream")):
            return
          elif quest.maxine_hook == "dig":
            if game.hour == 19:
              game.events_queue.append("quest_maxine_hook_dig_on_advance")
          elif not mc.at("home_*"):
            game.events_queue.append("event_player_force_go_home_at_night")
          elif mc.at("home_*"):
            if game.hour in (20,21,22,23,0,1,2,3,4,5,6):
              game.location["night"] = True
              for id,character in game.characters.items():
                if character.at(game.location) and not id in (mc.id,mc.name):
                  game.location["night"] = False
      else:
        game.location["night"] = False

    def on_location_changed(event,old_location,new_location,silent=False):
      if new_location.id.startswith("home_"):
        if game.hour in (20,21,22,23,0,1,2,3,4,5,6):
          game.location['night'] = True
          for id,character in game.characters.items():
            if character.at(game.location) and not id in (mc.id,mc.name):
              game.location['night'] = False
        else:#it's day
          game.location['night'] = False
      for object_id,object_cls in game.characters.items() + game.locations.items():
        for variable,value in object_cls.flags.items():
          if variable.endswith("_this_scene"):
            del object_cls.flags[variable]

    #def on_enter_roaming_mode(event):
      #if not game.location.id.startswith("home_"):
        #if game.hour in (19,20,21,22,23,0,1,2,3,4,5,6):
          #if quest.day1_take2.finished:
            #if not quest.kate_blowjob_dream in ("school","awake") and not quest.isabelle_haggis in ("instructions","puzzle"):
              #if quest.isabelle_buried in ("wait","bury","dig","funeral") and not mc.at("school_forest_glade"):
                #game.events_queue.append("event_player_force_go_home_isabelle_buried")
              #elif quest.jacklyn_statement == "statement" or quest.maxine_eggs == "bathroom":
                #return
              #elif quest.isabelle_buried not in ("bury","dig","funeral"):
                #game.events_queue.append("event_player_force_go_home_at_night")
              #pass

define force_go_home_allowed_events=[
  "event_player_force_go_home_at_night",
  "event_show_time_passed_screen",
  ]

label event_player_force_go_home_at_night:
  $game.skip_events(force_go_home_allowed_events)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's getting late. I can't wait to get home."
  if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
    "I just have to put [maxine]'s priced paper back in the stack first."
    "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    $mc.remove_item("paper")
    pause 0.25
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  return

label event_player_force_go_home_isabelle_buried:
  $game.skip_events(force_go_home_allowed_events)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's getting late. I promised to meet [isabelle]."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_forest_glade"
  $mc["focus"] = "isabelle_buried"
  $quest.isabelle_buried.advance("bury")
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  return
