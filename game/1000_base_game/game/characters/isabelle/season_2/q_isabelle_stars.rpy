image nurse_aid_not_girlfriend_alt = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-26,0), Transform("nurse aftercare", size=(206,116))), "ui circle_mask"), (6,6), "ui crossed_out_circle1")


label quest_isabelle_stars_start:
  show isabelle concerned_left with Dissolve(.5)
  # "I don't know about [isabelle], but I sure am riding high after everything we did to [kate]."
  "I don't know about [isabelle], but I sure am riding high after everything{space=-25}\nwe did to [kate]."
  "It feels good to be the one on top for once. To have things mostly going my way."
  "Fuck the status quo and fuck bullies and bitches."
  "I learned some hard lessons in my old life, and I sure as hell won't forget them in this one."
  "..."
  "Although, [isabelle] looks less pleased with the new order and more preoccupied than usual."
  "I wonder what's on her mind?"
  mc "Hey, is everything okay?"
  isabelle concerned "Hm?"
  isabelle concerned "Oh, yes. Everything's fine, thanks."
  "There's definitely something going on in that head of hers."
  # isabelle concerned "The overall mood around here feels kind of different, don't you think?"
  isabelle concerned "The overall mood around here feels kind of different, don't you think?{space=-40}"
  mc "Err, I guess?"
  mc "In a good way, though. At least for me."
  mc "Giving [kate] what she deserved still feels pretty good."
  mc "Like we've liberated the people."
  isabelle neutral "Yes, that did feel pretty good."
  mc "Maybe we should plan something for the other cheerleaders next?"
  isabelle concerned "I don't know..."
  isabelle concerned "They were just mindless cheer robots following [kate]'s demands."
  mc "Maybe, but they still think they're in charge."
  isabelle sad "They're a lot less cunning than [kate], though."
  isabelle sad "Besides, there has been too much negativity and sadness around here lately."
  isabelle sad "With the [kate] vacuum of power, and [lindsey] and her... incident."
  isabelle sad "I'm tired of feeding into it all right now."
  # isabelle sad "There was so much of a similar feeling with my sister... towards the end."
  isabelle sad "There was so much of a similar feeling with my sister... towards the end.{space=-85}"
  "Oh, man. I hope she's not feeling guilty."
  mc "Hey, this is nothing like that, okay? I promise."
  isabelle sad "I know..."
  isabelle sad "Maybe the change in season is making me feel kind of down."
  show isabelle sad at move_to(.25)
  menu(side="right"):
    extend ""
    # "\"Aren't you used to this kind of weather where you're from?\"":
    "\"Aren't you used to this kind of\nweather where you're from?\"":
      show isabelle sad at move_to(.5)
      mc "Aren't you used to this kind of weather where you're from?"
      isabelle annoyed "That's hardly the point, is it?"
      isabelle annoyed "You could try to be a little more supportive sometimes."
      mc "Yeesh. I was just joking."
      isabelle sad "..."
      isabelle sad "Sorry. It's all got me in a bit of a mood."
      isabelle sad "I didn't mean to take it out on you."
      mc "It's fine. Don't worry about it."
    # "\"It's okay to be affected by the negativity. It has been a lot.\"":
    "\"It's okay to be affected by the\nnegativity. It has been a lot.\"":
      show isabelle sad at move_to(.5)
      mc "It's okay to be affected by the negativity. It has been a lot."
      isabelle sad "Yeah..."
      isabelle sad "Sometimes it's so easy to get sucked into it all and lose myself."
      isabelle blush "It's a good thing I have you around to help cool me down."
      show isabelle blush at move_to(.75)
      menu(side="left"):
        extend ""
        "\"It's a hard job when you're so hot.\"":
          show isabelle blush at move_to(.5)
          mc "It's a hard job when you're so hot."
          $isabelle.lust+=1
          isabelle laughing "Hah! You do a pretty good job of it."
          isabelle laughing "Aren't I lucky?"
          $mc.charisma+=1
          mc "Heh. Just a bit."
          isabelle confident "Funny, too."
        "\"I don't really do that much...\"":
          show isabelle blush at move_to(.5)
          mc "I don't really do that much..."
          isabelle blush "You can't help but downplay your worth to me, can you?"
          mc "I guess it all still feels a bit like a dream sometimes."
          isabelle blush "Well, I promise it's not."
          $isabelle.love+=1
          isabelle blush "And I truly appreciate you, [mc]."
          mc "Likewise, [isabelle]."
    # "\"Just don't tell me you feel bad about [kate]...\"":
    "\"Just don't tell me you\nfeel bad about [kate]...\"":
      show isabelle sad at move_to(.5)
      mc "Just don't tell me you feel bad about [kate]..."
      mc "...because she deserved what she got, and she's done a lot worse."
      isabelle skeptical "No, I don't feel bad."
      isabelle concerned "I just don't want to keep focusing on it right now."
      isabelle concerned "I want to live our lives without talking about her constantly."
      mc "Fair enough."
      mc "But any time we need to teach her another lesson, just know I've got you, okay?"
      $isabelle.lust+=1
      isabelle neutral "Oh, I know it, all right."
  mc "Anyway, we don't have to think about all that right now."
  mc "In fact, you should take some time to get your mind off things."
  mc "And I know just how to do it."
  isabelle neutral "Oh, do you?"
  isabelle skeptical "This isn't a come on, is it?"
  mc "Heh. No, not this time!"
  mc "...unless you would be into that?"
  isabelle concerned "Sorry, I'm not really in the mood..."
  mc "That's fine! It's really not what I had in mind."
  mc "But also, I can't tell you because it's going to be a surprise. So, you just be ready when I text you later, okay?"
  isabelle neutral "Truly? A surprise?"
  mc "Indeed!"
  isabelle neutral "That's sweet."
  isabelle neutral "All right, I'll be ready."
  isabelle neutral "Talk to you later, [mc]."
  window hide
  hide isabelle with Dissolve(.5)
  window auto
  "There, she's smiling already! That's nice to see."
  "Now, I just need to come up with the surprise."
  "Something sweet, quiet, and romantic..."
  "..."
  "I heard [maxine] talking about a meteor shower tonight."
  "I bet [isabelle] would love that."
  "I just need some things to make it perfect."
  "Starting with something we can use to really see the meteors up close and personal."
  window hide
  $quest.isabelle_stars.start()
  return

label quest_isabelle_stars_telescope:
  # "I never paid much attention to astronomy lessons, but I know telescopes help you see the stars way better."
  "I never paid much attention to astronomy lessons, but I know telescopes{space=-95}\nhelp you see the stars way better."
  "So, I can just borrow the school's and have it back when I'm done."
  window hide
  $school_roof["telescope_taken"] = True
  $mc.add_item("telescope")
  pause 0.25
  window auto
  "Okay, now that the telescope is sorted, I need a little more to make this night extra cozy and romantic."
  "And what's more romantic or cozy than a burning hot fire?"
  if mc.owned_item("fire_axe"):
    "Time to go lumberjack on some wood."
    $quest.isabelle_stars.advance("wood")
  else:
    "..."
    "First things first, I need something to chop up some wood."
    $quest.isabelle_stars.advance("fire_axe")
  return

label quest_isabelle_stars_fire_axe:
  "If this isn't good for chopping wood, I don't know what is."
  window hide
  $school_roof_landing["fire_axe_taken"] = True
  $mc.add_item("fire_axe")
  pause 0.25
  window auto
  "Now, to put it to work like a hoe..."
  $quest.isabelle_stars.advance("wood")
  return

label quest_isabelle_stars_wood_upon_entering:
  "I better make sure I chop up plenty of wood so [isabelle] stays nice and warm tonight."
  return

label quest_isabelle_stars_wood(item):
  if item == "fire_axe":
    $mc.remove_item("fire_axe")
    pause 0.25
    "You're going down, you barky bitch."
    "About to be my kindling!"
    menu(side="middle"):
      "Limp-wrist it":
        pause 0.25
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        "..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Man, this is a lot harder than it looks in the movies..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "It really is... a workout..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Phew! Surely, this is enough wood?"
        window hide
        $mc.add_item("small_pile_of_wood")
      "?mc.strength>=5@[mc.strength]/5|{image=stats str}|Lumberjack that bitch":
        pause 0.25
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        "..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "I'm a regular old lumberjack."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "I bet [isabelle] would be impressed by my manly prowess if she could see me right now."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "The flex of my muscles, the sweat on my brow..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Huff... puff..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Okay, I think that's more than enough."
        window hide
        $mc.add_item("medium_pile_of_wood")
      # "?mc.strength>=10@[mc.strength]/10|{image=stats str}|Channel your inner forest god":
      "?mc.strength>=10@[mc.strength]/10|{image=stats str}|Channel your\ninner forest god":
        pause 0.25
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        "..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "This is actually kind of therapeutic in a way."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "I want to make sure I have plenty of wood for [isabelle]."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "And for the fire, too. Heh."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Really, though. If we're out late, I want her to stay warm and be as comfortable as possible."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Even if... it's a lot of work..."
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "Just! A bit! More!"
        window hide
        pause 0.125
        play sound "fire_axe_impact"
        show location with hpunch
        pause 0.375
        window auto
        "All right! That's plenty of wood for tonight."
        window hide
        $mc.add_item("big_pile_of_wood")
    $mc.add_item("fire_axe")
    pause 0.25
    window auto
    "Now that that's taken care of, what next?"
    "If I'm going to make this a proper date for [isabelle], I'm going to need to prepare something to eat, aren't I?"
    "Hmm..."
    menu(side="middle"):
      "Buy a meal":
        "No amount of money is too much to spend on [isabelle]."
        "She deserves the very best dining experience."
        $quest.isabelle_stars.advance("takeout")
      "Cook something yourself":
        "I might not be the best chef in the world, but they say that it's the thought that counts."
        # "What could be more romantic than a home cooked meal for [isabelle]?"
        "What could be more romantic than a home cooked meal for [isabelle]?{space=-20}"
        $quest.isabelle_stars.advance("homemade")
  else:
    "Just cutting down some trees with the help of my [item.title_lower]. No big deal, right?"
    "Oh, wait. Yes, it is."
    $quest.isabelle_stars.failed_item("lumberjack",item)
  return

label quest_isabelle_stars_takeout_upon_entering:
  "There's no better place around to order some fresh takeout like Newfall's own marina."
  "Let's see here..."
  return

label quest_isabelle_stars_takeout:
  "So many options... so many factors to consider..."
  "What would [isabelle] like best?"
  menu(side="middle"):
    extend ""
    "?mc.money>=40@[mc.money]/40|{image=ui hud icon_money}|Fish and chips":
      "Fish and chips, mate!"
      "That's basically the chosen cuisine of her people, isn't it?"
      "It will make her feel right at home."
      window hide
      $mc.money-=40
      $mc.add_item("fish_and_chips")
    "?mc.money>=100@[mc.money]/100|{image=ui hud icon_money}|Shrimp and lobster":
      "Nothing is too good for [isabelle]."
      "I want her to enjoy this night and forget all the bad stuff for a while."
      "What's better for that than some buttery shrimp and lobster?"
      window hide
      $mc.money-=100
      $mc.add_item("shrimp_and_lobster")
    "?mc.money>=10@[mc.money]/10|{image=ui hud icon_money}|Hotdog":
      "No amount of money may be too much for [isabelle]... but I'm a broke bitch."
      "At least she isn't fussy or stuck up."
      "She will probably be surprised that I even thought to bring food at all, right?"
      window hide
      $mc.money-=10
      $mc.add_item("hotdog")
    "Come back later":
      $quest.isabelle_stars["broke_bitch"] = True
      return
  pause 0.25
  window auto
  "Okay! Now, at least [isabelle] won't starve tonight."
  "I think this is as good as it gets."
  "Time to let her know where to meet me."
  $quest.isabelle_stars.advance("text")
  return

label quest_isabelle_stars_homemade_upon_entering:
  # "I'm not exactly a culinary genius, but I think it will mean a lot to [isabelle] if I do this on my own."
  "I'm not exactly a culinary genius, but I think it will mean a lot to [isabelle]{space=-65}\nif I do this on my own."
  "What to cook, though?"
  menu(side="middle"):
    extend ""
    "Spaghetti":
      "Ciao bello! Perhaps some spaghetti!"
      "You can never go wrong with a little Italian cuisine."
      $quest.isabelle_stars["meal"] = "spaghetti"
    "Sandwich":
      "I need simple picnic food for a night beneath the stars."
      "Sandwich it is."
      $quest.isabelle_stars["meal"] = "sandwich"
    "Fruit":
      "I don't want to ruin a meal, but fruit can be sexy if you do it right."
      $quest.isabelle_stars["meal"] = "fruit"
  return

label quest_isabelle_stars_homemade:
  if quest.isabelle_stars["meal"] == "spaghetti":
    "Spaghetti is simple, yet delicious."
    "..."
    "And we appear to have everything I need! Probably thanks to [flora]."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.advance()
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5625
    $mc.add_item("spaghetti")
    pause 0.25
    window auto
    "Aha! It's beautiful and smells divine."
    "I really hope [isabelle] likes it."
    "Time to let her know where to meet me."
  elif quest.isabelle_stars["meal"] == "sandwich":
    "I better not make it too complicated."
    "[isabelle] isn't one to make a huge fuss, anyway."
    "As long as it's tasty, that's all that matters."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.advance()
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5625
    $mc.add_item("sandwich")
    pause 0.25
    window auto
    "Aha! It's beautiful and smells divine."
    "I really hope [isabelle] likes it."
    "Time to let her know where to meet me."
  elif quest.isabelle_stars["meal"] == "fruit":
    "Good thing [jo] keeps the fridge stocked with fruit."
    "Too bad I never eat it like she tells me to."
    window hide
    $mc.add_item("grapes")
    pause 0.25
    window auto
    "Okay, all that's left to do is tell [isabelle] to meet me."
  $quest.isabelle_stars.advance("text")
  return

label quest_isabelle_stars_text:
  $set_dialog_mode("phone_message_centered","isabelle")
  mc "As promised, your evening out awaits, m'lady."
  mc "Meet me in the park after school?"
  isabelle "The park? Interesting choice of venue this time of the year."
  mc "I promise it won't disappoint!"
  isabelle "Exciting! I'll be there!"
  isabelle "{image=phone emojis relaxed}"
  window hide None
  window auto
  $set_dialog_mode("phone_message_plus_textbox")
  "Man, I'm suddenly kind of nervous."
  "I've never done anything this romantic before."
  "I trust [isabelle] and really like her..."
  "...but there's still that anxious fear of being rejected."
  "Still, I guess you have to take chances for the good things in life."
  "And [isabelle] really is one of the best."
  "..."
  "Okay, I better hurry up. I don't want [isabelle] to beat me there."
  window hide
  $set_dialog_mode("")
  pause 0.75
  $quest.isabelle_stars.advance("stargaze")

label quest_isabelle_stars_stargaze:
  show black onlayer screens zorder 100 with Dissolve(.5)
  while game.hour != 19:
    $game.advance()
  $game.location = "school_park"
  pause 1.0
  stop music fadeout 1.0
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Okay, [mc]. Relax."
  "It's just another night hanging out with [isabelle]."
  "She likes you! And you like her! So, just have fun."
  "..."
  "Hey, that actually kind of worked!"
  "Time to set the scene."
  "The telescope needs to be positioned... just right..."
  window hide
  $school_park["telescope"] = True
  $mc.remove_item("telescope")
  pause 0.25
  window auto
  "The fire, nice... and bright..."
  window hide
  $school_park["blanket"] = True
  if mc.owned_item("small_pile_of_wood"):
    $school_park["small_pile_of_wood"] = True
    $mc.remove_item("small_pile_of_wood")
  elif mc.owned_item("medium_pile_of_wood"):
    $school_park["medium_pile_of_wood"] = True
    $mc.remove_item("medium_pile_of_wood")
  elif mc.owned_item("big_pile_of_wood"):
    $school_park["big_pile_of_wood"] = True
    $mc.remove_item("big_pile_of_wood")
  pause 0.25
  window auto
  "There! That looks pretty good."
  "And just in time, too."
  window hide
  show isabelle laughing at appear_from_left
  pause 0.5
  window auto
  isabelle laughing "Hey, you!"
  isabelle confident "Oh, wow! Did you do all of this on your own?"
  mc "Heh. Believe it or not, I did!"
  mc "I want your mind nice and relaxed tonight."
  mc "Because tonight is all about me, you, and the shooting stars."
  isabelle blush "Sounds romantic."
  mc "Oh, it is."
  mc "May I offer the pretty lady a nice seat next to the glowing fire?"
  isabelle blush "Why yes, you may."
  window hide
  show isabelle stargazing_bonfire smile
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "As soon as she's beside me, all of my nerves seem to steady."
  "And I am left with just a feeling of calm and contentment."
  "[isabelle]'s presence is more warming than the fire."
  # "With that subtle vanilla smell, her beautiful smile, and kind eyes, so full of wonderment that someone would do something like this for her."
  "With that subtle vanilla smell, her beautiful smile, and kind eyes, so full{space=-65}\nof wonderment that someone would do something like this for her."
  "Except it's not even much. I would do this and more."
  mc "I don't get outside as much as I should, but this really is nice."
  mc "And we have some time before the meteor shower gets good."
  mc "..."
  mc "What, err... what do you think of this?"
  isabelle stargazing_bonfire smile "I think it's lovely."
  mc "Were you surprised?"
  isabelle stargazing_bonfire smile "Utterly gobsmacked."
  mc "Smacked by god? Sounds pretty good."
  isabelle stargazing_bonfire laughing "Haha! Close enough."
  show isabelle stargazing_bonfire blush with dissolve2
  isabelle stargazing_bonfire blush "And it is good. Thanks for doing this, [mc]."
  isabelle stargazing_bonfire blush "Nobody has ever done something like this for me before."
  mc "Have you dated much?"
  isabelle stargazing_bonfire thinking "Not really."
  isabelle stargazing_bonfire thinking "It was always kind of difficult, moving around so much."
  isabelle stargazing_bonfire thinking "It was hard to make significant connections."
  isabelle stargazing_bonfire thinking "And when I tried, it seems I had a habit of choosing total wankers."
  mc "Ah."
  "I need to make sure I'm not the next wanker in line..."
  isabelle stargazing_bonfire blush "But luckily, I seem to have finally found a decent guy."
  menu(side="middle"):
    extend ""
    "\"Who is he?! I'll kill him!\"":
      mc "Who is he?! I'll kill him!"
      isabelle stargazing_bonfire laughing "Very funny."
      show isabelle stargazing_bonfire smile with dissolve2
      isabelle stargazing_bonfire smile "I don't think you have to worry about much competition..."
      $mc.charisma+=1
      mc "Excellent. I Stockholmed you first."
      isabelle stargazing_bonfire laughing "Is that what you've done? Brilliant!"
      mc "Heh. Thank you. I try."
    "\"Thanks for saying that. I try to be someone who could deserve you.\"":
      mc "Thanks for saying that. I try to be someone who could deserve you."
      isabelle stargazing_bonfire smile "I don't think you give yourself enough credit, [mc]."
      isabelle stargazing_bonfire smile "You're a good guy, and I'm glad to have met you."
      isabelle stargazing_bonfire smile "Besides, it's not about deserving."
      $isabelle.love+=1
      isabelle stargazing_bonfire smile "I want to be with you, and you want to be with me."
      isabelle stargazing_bonfire smile "And that's all that matters."
      mc "I suppose you're right."
      # mc "I wasn't always the best person, but I realized I need to do some things differently."
      mc "I wasn't always the best person, but I realized I need to do some things{space=-60}\ndifferently."
      mc "I'm glad it's working... and I'm glad to have met you, too."
      isabelle stargazing_bonfire blush "I'm really happy to hear that, [mc]."
      # "Maybe [isabelle] being in my life this time around is positive karma from the universe or something?"
      "Maybe [isabelle] being in my life this time around is positive karma from{space=-45}\nthe universe or something?"
      "A driving force to make me and the world a better place."
      "..."
      "Or at the very least, {i}my{/} world."
      "And that's enough for me."
    "\"It's a good thing you came to Newfall.\"":
      mc "It's a good thing you came to Newfall."
      isabelle stargazing_bonfire smile "And why is that?"
      mc "Superior menfolk."
      # isabelle stargazing_bonfire thinking "I think people in general can be pretty shitty no matter where you go."
      isabelle stargazing_bonfire thinking "I think people in general can be pretty shitty no matter where you go.{space=-20}"
      mc "True..."
      mc "That's why we say fuck the world."
      mc "It's you and me, and that's all that matters."
      isabelle stargazing_bonfire flirty "Is that so?"
      mc "Like Bonnie and Clyde, baby."
      isabelle stargazing_bonfire flirty "You do know they died, right?"
      mc "Well, we're not criminals, yet..."
      mc "...and our passion won't die!"
      $isabelle.lust+=1
      isabelle stargazing_bonfire flirty "Smooth!"
      mc "Heh. Thanks."
  mc "So, anyway, how about some dinner?"
  isabelle stargazing_bonfire smile "Sounds delightful."
  isabelle stargazing_bonfire smile "What are we having?"
  if quest.isabelle_stars["meal"]:
    mc "My specialty."
    if quest.isabelle_stars["meal"] == "spaghetti":
      window hide
      $mc.remove_item("spaghetti", silent=True)
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["spaghetti"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["spaghetti"].title()+"{/}", 0.25)
      show isabelle stargazing_bonfire surprised spaghetti with dissolve2
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["spaghetti"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["spaghetti"].title()+"{/}", 4.75, False)
      pause 0.25
      window auto
      isabelle stargazing_bonfire surprised spaghetti "Oh, wow! It looks tasty!"
      isabelle stargazing_bonfire surprised spaghetti "I am impressed, [mc]! I didn't realize you could cook!"
      mc "Heh. Well, maybe you should try it first?"
      isabelle stargazing_bonfire laughing spaghetti "Fair dos."
      show isabelle stargazing_bonfire thinking spaghetti with dissolve2
      isabelle stargazing_bonfire thinking spaghetti "..."
      isabelle stargazing_bonfire smile spaghetti "Wow."
      mc "Good wow?"
      isabelle stargazing_bonfire flirty spaghetti "Amazing wow!"
      mc "Seriously?"
      isabelle stargazing_bonfire flirty spaghetti "What did you put in this?"
      mc "I actually don't remember. I would need to have a taste myself."
      isabelle stargazing_bonfire flirty spaghetti "You—"
      window hide
      show isabelle stargazing_spaghetti taste
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      pause 0.25
      show isabelle stargazing_spaghetti kiss with Dissolve(.5)
      pause 0.25
      window auto
      "It's a spur of the moment decision, but thankfully, [isabelle] doesn't break our noodly connection."
      "Instead, she goes all in with me and we slurp our way to the middle of the noodle together, until our lips meet."
      "We kiss gently, until she breaks away, laughing."
      window hide
      show isabelle stargazing_bonfire laughing
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      isabelle stargazing_bonfire laughing "Oh, my god! I did not expect that!"
      $mc.love+=1
      # mc "Heh, sorry. I know it's cheesy, but you were just too cute with the noodle hanging out of your mouth like that."
      mc "Heh, sorry. I know it's cheesy, but you were just too cute with the noodle{space=-90}\nhanging out of your mouth like that."
      isabelle stargazing_bonfire blush "You continue to surprise me, [mc]."
      $isabelle.love+=3
      $isabelle.lust+=1
      isabelle stargazing_bonfire blush "I really like it."
    elif quest.isabelle_stars["meal"] == "sandwich":
      window hide
      $mc.remove_item("sandwich", silent=True)
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["sandwich"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["sandwich"].title()+"{/}", 0.25)
      show isabelle stargazing_bonfire laughing sandwich with dissolve2
      $game.notify_modal(None, "Inventory", "{image="+items_by_id["sandwich"].icon+"}{space=50}|Lost\n{color=#900}"+items_by_id["sandwich"].title()+"{/}", 4.75, False)
      pause 0.25
      window auto
      mc "Your dinner, my British crumpet."
      isabelle stargazing_bonfire laughing sandwich "Haha! Thank you!"
      show isabelle stargazing_bonfire smile sandwich with dissolve2
      isabelle stargazing_bonfire smile sandwich "Sandwiches, eh? How very practical."
      mc "Practical is my middle name."
      isabelle stargazing_bonfire thinking sandwich "..."
      isabelle stargazing_bonfire blush sandwich "Oh, my goodness."
      mc "Do you like it?"
      # isabelle stargazing_bonfire blush sandwich "Do I like it? This is the first sandwich I've had since moving here that's actually good!"
      isabelle stargazing_bonfire blush sandwich "Do I like it? This is the first sandwich I've had since moving here that's{space=-50}\nactually good!"
      isabelle stargazing_bonfire blush sandwich "Is that mayonnaise, even?"
      mc "Indeed, it is."
      isabelle stargazing_bonfire blush sandwich "The idea has always disgusted me... but this... this just works..."
      isabelle stargazing_bonfire blush sandwich "Mmm..."
      # mc "Finally, I have shown you and converted you to the tasty Newfall ways!"
      mc "Finally, I have shown you and converted you to the tasty Newfall ways!{space=-55}"
      isabelle stargazing_bonfire laughing sandwich "Hah! Don't get too far ahead of yourself!"
      show isabelle stargazing_bonfire smile sandwich with dissolve2
      isabelle stargazing_bonfire smile sandwich "But this? Keep it up and I'll be sticking around."
      mc "Excellent! My plan is working! In through the stomach!"
      $mc.charisma+=1
      mc "...and maybe later in through somewhere else..."
      $isabelle.lust+=1
      $isabelle.love+=1
      isabelle stargazing_bonfire laughing sandwich "[mc]!"
      window hide
      pause 0.125
      show isabelle stargazing_bonfire blush with Dissolve(.5)
      pause 0.25
      window auto
    elif quest.isabelle_stars["meal"] == "fruit":
      window hide
      $mc.remove_item("grapes")
      pause 0.25
      window auto
      mc "Something light and simple for us before the show begins."
      isabelle stargazing_bonfire smile "Interesting choice."
      $mc.charisma+=1
      mc "I am the interesting choice, baby."
      isabelle stargazing_bonfire laughing "Hah! Very true."
      mc "Besides, I thought I could feed you these grapes?"
      isabelle stargazing_bonfire flirty "Oh, really? Is that what you thought?"
      mc "I promise I'll be gentle..."
      isabelle stargazing_bonfire flirty "Very well. Go on, then."
      "Hell yeah. Sexy, sensual, tasty."
      "I knew this was a good move."
      mc "As you wish."
      window hide
      show isabelle stargazing_grapes feeding
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      # "When I decided on this, I had no idea it could actually be so intimate."
      "When I decided on this, I had no idea it could actually be so intimate.{space=-15}"
      "But the way each round grape slowly goes past her mouth..."
      # "...her eyes on mine while she accepts the fruit from my own fingers..."
      "...her eyes on mine while she accepts the fruit from my own fingers...{space=-20}"
      "...it does something to me."
      window hide
      pause 0.125
      $mc.lust+=1
      show isabelle stargazing_grapes caressing with Dissolve(.5)
      pause 0.25
      window auto
      "As I touch her soft, sensitive lips, a shiver seems to tremble silently through her."
      # "In return, the look she gives me sends a swarm of butterflies fluttering through my chest."
      "In return, the look she gives me sends a swarm of butterflies fluttering{space=-60}\nthrough my chest."
      isabelle stargazing_grapes caressing "Mmm..."
      mc "Good?"
      "I'm surprised my voice sounds so calm to my own ears."
      isabelle stargazing_grapes caressing "Very...."
      window hide
      pause 0.125
      $isabelle.lust+=1
      show isabelle stargazing_grapes licking with Dissolve(.5)
      pause 0.25
      window auto
      "She giggles softly and licks the juice from her lips."
      "I better stop this now before I can't control myself..."
      "She has no idea how easily she undoes me."
      window hide
      show isabelle stargazing_bonfire blush
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
  else:
    mc "I ordered out for us tonight."
    isabelle stargazing_bonfire flirty "You did? Oh, wow!"
    isabelle stargazing_bonfire flirty "Pulling out all the stops, aren't you?"
    mc "Only the best for you."
    if mc.owned_item("fish_and_chips"):
      # mc "And I thought it would be extra nice to bring something to give you a taste of home."
      mc "And I thought it would be extra nice to bring something to give you a{space=-15}\ntaste of home."
      window hide
      $mc.remove_item("fish_and_chips")
      pause 0.25
      window auto show
      show isabelle stargazing_bonfire surprised with dissolve2
      isabelle stargazing_bonfire surprised "Oh! That's so thoughtful of you, [mc]..."
      isabelle stargazing_bonfire surprised "Thank you. Truly."
      mc "My pleasure."
      mc "Plus, it sounded good, too."
      isabelle stargazing_bonfire thinking "..."
      isabelle stargazing_bonfire blush "Mmm! It really is the best."
      $isabelle.love+=1
      isabelle stargazing_bonfire blush "And so are you."
    elif mc.owned_item("shrimp_and_lobster"):
      mc "And I wanted you to have the very best."
      mc "So, please enjoy this shrimp and lobster straight from the sea."
      window hide
      $mc.remove_item("shrimp_and_lobster")
      pause 0.25
      window auto show
      show isabelle stargazing_bonfire surprised with dissolve2
      isabelle stargazing_bonfire surprised "Wow! [mc], you shouldn't have!"
      isabelle stargazing_bonfire surprised "I feel so spoiled right now..."
      mc "Tonight is your night. Away from all the shit and worry."
      mc "So, I want you to extra enjoy it."
      isabelle stargazing_bonfire blush "So far, I definitely am."
      mc "Good. Then, dig in, baby."
      $isabelle.lust+=1
      $isabelle.love+=1
      isabelle stargazing_bonfire blush "Mmm! Delicious!"
    elif mc.owned_item("hotdog"):
      mc "But I still wanted to keep it simple. So, I went with some hotdogs."
      window hide
      $mc.remove_item("hotdog")
      pause 0.25
      window auto show
      show isabelle stargazing_bonfire surprised with dissolve2
      isabelle stargazing_bonfire surprised "Oh."
      "Crap. She looks kind of disappointed."
      "Maybe hotdogs weren't the best choice for a romantic date beneath the stars, after all..."
      isabelle stargazing_bonfire smile "Thank you, anyway."
      mc "Err, of course."
      isabelle stargazing_bonfire smile "And thank you for setting this up."
      mc "No worries..."
  if renpy.showing("isabelle stargazing_bonfire blush"):
    mc "..."
    isabelle stargazing_bonfire blush "..."
  else:
    show isabelle stargazing_bonfire blush with dissolve2
    isabelle stargazing_bonfire blush "..."
    mc "..."
  "We sit together in silence for a little bit as we finish our food and stare up at the vast expanse of black sky."
  "Luckily for us, the moon is waning and the weather is decent."
  "So far, tonight could hardly have gone better."
  "And when I look over at [isabelle] and see the soft smile playing on her lips, it makes me feel like oxygen is filling my lungs for the very first time."
  # isabelle stargazing_bonfire blush "You know, there was a time when I was little when I wanted to travel to space."
  isabelle stargazing_bonfire blush "You know, there was a time when I was little when I wanted to travel{space=-5}\nto space."
  # isabelle stargazing_bonfire blush "There's something so awe-inspiring about the unknown and discovery."
  isabelle stargazing_bonfire blush "There's something so awe-inspiring about the unknown and discovery.{space=-50}"
  # isabelle stargazing_bonfire blush "And I had already been all over the country and Europe by that point."
  isabelle stargazing_bonfire blush "And I had already been all over the country and Europe by that point.{space=-20}"
  isabelle stargazing_bonfire blush "So, I thought, why not make space my last frontier?"
  mc "Heh, that's kind of cute. I had no idea you were a space nerd."
  isabelle stargazing_bonfire laughing "Hah! Well, I wasn't the best at science. Decent, but not the best."
  # isabelle stargazing_bonfire laughing "Plus, I then discovered my love of literature, and kept my nose more firmly in books."
  isabelle stargazing_bonfire laughing "Plus, I then discovered my love of literature, and kept my nose more{space=-10}\nfirmly in books."
  mc "That's adorable."
  mc "What about your sister? What kind of things did she like?"
  isabelle stargazing_bonfire sad "..."
  "Her smile flickers and a stone of guilt pits in my stomach."
  mc "Err, sorry. We don't have to talk about her if you don't want to."
  isabelle stargazing_bonfire sad "No, no. It's okay."
  isabelle stargazing_bonfire sad "It is difficult, but talking about her helps keep her alive."
  isabelle stargazing_bonfire sad "And it's sweet of you to ask."
  mc "I know how important she was to you."
  isabelle stargazing_bonfire sad "Yes, she was."
  isabelle stargazing_bonfire sad "She liked video games, drawing, and music."
  isabelle stargazing_bonfire sad "So many obscure bands I had never heard of..."
  isabelle stargazing_bonfire sad "She was kind of quiet and shy. Basically, the exact opposite of me."
  isabelle stargazing_bonfire sad "But it worked for us."
  isabelle stargazing_bonfire sad "..."
  isabelle stargazing_bonfire blush "She would have loved something like this, too."
  mc "I bet she would."
  mc "But if there is something more after life... well, maybe she's looking over us and enjoying it just the same?"
  isabelle stargazing_bonfire blush "That's a nice thought."
  mc "I'm sure all she would want for you is to be happy."
  isabelle stargazing_bonfire blush "And I'm trying to find a little bit of that every day."
  # isabelle stargazing_bonfire blush "It varies day to day, of course, but if you hold on to the little moments of contentment or joy, they do tend to add up and make life a little better."
  isabelle stargazing_bonfire blush "It varies day to day, of course, but if you hold on to the little moments of{space=-95}\ncontentment or joy, they do tend to add up and make life a little better.{space=-60}"
  mc "That's a nice way of looking at it. Just focusing on the positive."
  isabelle stargazing_bonfire blush "Exactly..."
  isabelle stargazing_bonfire blush "That's why tonight was a much needed reset, I think."
  isabelle stargazing_bonfire blush "And a nice one at that."
  mc "Right. I'm really happy we did this."
  isabelle stargazing_bonfire blush "Me too."
  mc "Should we enjoy the show, then?"
  isabelle stargazing_bonfire blush "Let's do it!"
  window hide
  show isabelle stargazing_embrace
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "With my arm around her, we look into the telescope, ready for a dazzling display."
  # "As it gets later, the night continues to get colder, but with the fire to our backs we manage to stay pretty warm while we look up toward the infinite sky."
  "As it gets later, the night continues to get colder, but with the fire to{space=-5}\nour backs we manage to stay pretty warm while we look up toward the infinite sky."
  "There's also a different kind of warmth."
  # "Not in the crackle and pop of flames jumping in the air, but a quieter, hotter kind building up within me."
  "Not in the crackle and pop of flames jumping in the air, but a quieter,{space=-15}\nhotter kind building up within me."
  # "Just standing next to [isabelle] like this, the two of us out here marveling at the vast expanse of the universe and our small place in it."
  "Just standing next to [isabelle] like this, the two of us out here marveling{space=-75}\nat the vast expanse of the universe and our small place in it."
  # "It's like a comforting embrace heating up through my belly and chest."
  "It's like a comforting embrace heating up through my belly and chest.{space=-30}"
  "Until the mere presence of her makes my cheeks burn."
  "I'm not sure what I did to deserve her in this universe or timeline or whatever this is..."
  "...but instead of feeling small beneath the vast ocean of stars, I feel bigger and more seen than I ever have before."
  "[isabelle] sees the real me and doesn't shrink from it."
  "She stands out in the dark void with me, and helps me pick out the bright spots as they streak across a blackened sky."
  "And it reminds me of what she just said. About finding the good in each day."
  "The streaks of light in the darkness."
  "She's one of those bright, burning lights in my sky."
  isabelle stargazing_embrace "Ooooh!"
  "Every time a meteor streaks by, she gasps and laughs."
  isabelle stargazing_embrace "There's one!"
  mc "I see it!"
  "And each time she does it, I can't help but grin."
  "We both know it's coming, but it still fills her with such wonder."
  isabelle stargazing_embrace "That one was really bright!"
  mc "It was, wasn't it?"
  "Yet nothing can really compare to the brightness of her."
  "She knows exactly who she is, and she's not afraid to burn hot."
  "She doesn't care what anyone thinks if she believes she's right."
  "Looking at her, with her face pressed to the telescope, it feels as if the stars are shooting through my veins."
  # "[isabelle] burns me up like the sun, and I'm struck by how easily I could be consumed by her gravitational pull."
  "[isabelle] burns me up like the sun, and I'm struck by how easily I could{space=-40}\nbe consumed by her gravitational pull."
  $unlock_replay("isabelle_night")
  window hide
  hide screen interface_hider
  show isabelle laughing
  show black
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  isabelle laughing "Hellooo? [mc]?"
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "Huh?"
  isabelle laughing "I said, do you want to take another turn?"
  isabelle laughing "You're not going to boak or something, are you?"
  mc "Err, what?"
  mc "Nevermind. I'm good."
  mc "Great, even."
  isabelle confident "If you say so."
  mc "..."
  isabelle blush "This has been such a lovely night."
  isabelle blush "Thank you for setting this whole thing up. Really."
  isabelle blush "It was incredibly sweet of you."
  mc "Thanks, [isabelle]. I'm happy you liked it."
  isabelle blush "I, um... I actually wanted to ask you something."
  isabelle blush "Would you like to... be my boyfriend?"
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  show isabelle blush at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I would love to!\"" if not nurse["girlfriend"]:
      show isabelle blush at move_to(.5)
      mc "I would love to!"
      isabelle flirty "Truly?"
      # mc "I've actually been meaning to ask you myself for some time now, but I guess I was scared of rejection..."
      mc "I've actually been meaning to ask you myself for some time now, but{space=-10}\nI guess I was scared of rejection..."
      isabelle flirty "You were?"
      mc "How could I not be? You're just so perfect..."
      isabelle flirty "Oh, believe me, I'm not."
      isabelle flirty "But I really like you, [mc]."
      mc "I really like you, too, [isabelle]."
      mc "And I would love to be your boyfriend, and make you my girlfriend."
      $isabelle.love+=3
      isabelle flirty "It's official, then!"
    "?not nurse['girlfriend']@|{image=nurse_aid_not_girlfriend_alt}|\"I would love to!\"" if nurse["girlfriend"]:
      pass
    "\"I thought you would never ask.\"" if not nurse["girlfriend"]:
      show isabelle blush at move_to(.5)
      mc "I thought you would never ask."
      $mc.charisma+=1
      mc "I'm a total catch, after all."
      isabelle flirty "It's true! You are!"
      # mc "Seriously, though. I've actually been meaning to ask you myself for a while. I just wasn't sure how or when."
      mc "Seriously, though. I've actually been meaning to ask you myself for a{space=-10}\nwhile. I just wasn't sure how or when."
      isabelle flirty "Well, now you don't have to worry about it."
      mc "How very progressive of you!"
      $isabelle.love+=1
      $isabelle.lust+=1
      isabelle flirty "What can I say? I take what I want, when I want it."
    # "?not nurse['girlfriend']@|{image=nurse_aid_not_girlfriend_alt}|\"I thought you would never ask.\"" if nurse["girlfriend"]:
    "?not nurse['girlfriend']@|{image=nurse_aid_not_girlfriend_alt}|\"I thought you\nwould never ask.\"" if nurse["girlfriend"]:
      pass
    "\"Not really, sorry...\"":
      show isabelle blush at move_to(.5)
      mc "Not really, sorry..."
      isabelle angry "Seriously?"
      mc "I-It's not you, okay?"
      mc "I just... I don't think I'm ready for that level of commitment yet..."
      $isabelle.love-=5
      isabelle angry "Are you taking the piss right now?"
      isabelle angry "Not ready? After you set all of this up?!"
      mc "I'm really sorry..."
      isabelle annoyed_left "How bloody embarrassing."
      isabelle annoyed_left "You must be mental to go and do this."
      isabelle annoyed_left "I'm going home."
      mc "Look, [isabelle], I just need a little more—"
      show isabelle afraid with dissolve2:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      isabelle afraid "Ah! There's someone in the window!"
      show isabelle afraid:
        yoffset 0
      mc "Err, what?"
      isabelle cringe "In the school! Look!"
      "The last thing I want to do right now is think about school, but she looks genuinely afraid..."
      window hide
      show misc window_silhoutte as window
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide isabelle
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      mc "What the hell?!"
      isabelle "I bloody told you!"
      "Who could that person be?"
      "..."
      "It's so dark in there, I can't really make them out..."
      "If only the school could afford a better telescope."
      window hide
      pause 0.125
      show misc window as window with Dissolve(.5)
      pause 0.25
      window auto
      mc "They left."
      window hide
      show isabelle afraid
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide window
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      isabelle afraid "Who was it?"
      mc "No idea. I could just make out their silhouette."
      isabelle cringe "Okay, that's kind of creepy."
      mc "Right. It's way too late for anyone to still be in there."
      isabelle cringe "Do you think they were watching us?"
      mc "Err, I'm not sure..."
      mc "...but what's even more weird is I'm pretty sure that was the library window..."
      mc "...and the library is still locked."
      isabelle afraid "Oh, my god!"
      isabelle thinking "I have goose pimples."
      isabelle thinking "I'm definitely going home now."
      window hide
      show isabelle thinking at disappear_to_left
      pause 0.5
      window auto
      "Well, fuck."
      "The night was going so well, too."
      "I'm not entirely sure why I said no... other than the fact that my feelings are complicated."
      "Of course I really care about [isabelle], but there's just so much going on."
      "So much that has changed."
      "And she caught me off guard. I wasn't ready for it."
      "..."
      "I wonder if she'll talk to me again after this, or if I totally ruined things between us?"
      window hide
      if game.quest_guide == "isabelle_stars":
        $game.quest_guide = ""
      $game.notify_modal("quest", "Quest complete", quest.isabelle_stars.title+"{hr}"+quest.isabelle_stars._phases[1001].description,5.0)
      pause 0.25
      window auto
      "It's getting late. I can't wait to get home."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
      $game.advance()
      pause 2.0
      hide black onlayer screens with Dissolve(.5)
      play music "home_theme" fadein 0.5
      $quest.isabelle_stars.finish("done_feelings_rejected", silent=True)
      return
  "When she looks up at me, I swear the stars reflect in her eyes."
  "They shine with a soft radiance."
  "Big pools of green that hold genuine feeling for me."
  "I can hardly believe she's the one who just asked me to be official."
  "[isabelle]! My girlfriend!"
  "I'm officially the luckiest guy in school."
  "Hell, in all of Newfall."
  # "The realization of what has just happened hits me like a tsunami, and suddenly I'm drowning in the depths of her eyes."
  "The realization of what has just happened hits me like a tsunami, and{space=-25}\nsuddenly I'm drowning in the depths of her eyes."
  "I'm overcome with a wave of affection for her."
  "I just need to be closer to her. I need to hold her."
  mc "Come here..."
  isabelle flirty "Oh, my!"
  window hide
  # show isabelle stargazing_kiss
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show isabelle stargazing_kiss: ## This gives the image both the dissolve (transition) and hpunch (transform) effects
    block:
      linear 0.05 xoffset 15
      linear 0.05 xoffset -15
      repeat 3
    linear 0.05 xoffset 0
  pause 0.0
  hide black onlayer screens
  with Dissolve(.5)
  hide isabelle
  show isabelle stargazing_kiss ## This is just to avoid the screen shake if the player is skipping
  window auto
  "She nearly falls off balance when I pull her close."
  "Her waist fits perfectly in my hands, and her cloudlike, vanilla lips fit even better on mine."
  "It's a slow and sensual kiss."
  "As I deepen the kiss and tease her tongue with my own, she melts into me a little more."
  # "The taste of her, the feel of her small, yet full frame crushed beneath my hands, elicits a primal growl from deep within my throat."
  "The taste of her, the feel of her small, yet full frame crushed beneath{space=-25}\nmy hands, elicits a primal growl from deep within my throat."
  "It makes [isabelle] gasp into my mouth, and I can feel her heart hitch against my own chest."
  window hide
  hide screen interface_hider
  show isabelle afraid
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "But just as I'm about to slip my hands up under her shirt, she pulls back and gasps again."
  "It sounds less sexy and more frightened, and it momentarily breaks the fog quickly creeping into my brain."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "Sorry! Did I hurt you?"
  isabelle afraid "No, look! There's someone in the window!"
  mc "Err, what?"
  isabelle cringe "In the school! There!"
  mc "All right, hang on!"
  "The last thing I want to do right now is think about school, but she looks genuinely afraid..."
  window hide
  show misc window_silhoutte as window
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide isabelle
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  mc "What the hell?!"
  isabelle "I bloody told you!"
  "There was a part of me that worried [isabelle] was making it up to get out of kissing me."
  "I'm almost glad to see someone up there."
  "..."
  "But I can't quite tell who it is..."
  "If only the school could afford a better telescope."
  window hide
  pause 0.125
  show misc window as window with Dissolve(.5)
  pause 0.25
  window auto
  mc "They left."
  $unlock_replay("isabelle_boyfriend")
  window hide
  show isabelle afraid
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide window
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  isabelle afraid "Who was it?"
  mc "No idea. I could just make out their silhouette."
  isabelle cringe "Okay, that's kind of creepy."
  mc "Right. It's way too late for anyone to still be in there."
  isabelle cringe "Do you think they were watching us?"
  mc "Err, I'm not sure..."
  mc "...but what's even more weird is I'm pretty sure that was the library window..."
  mc "...and the library is still locked."
  isabelle afraid "Oh, my god!"
  isabelle thinking "I have goose pimples."
  isabelle thinking "I don't want to be out here anymore."
  mc "Fair enough."
  mc "How about a rootbeer float at the Prawn Star?"
  isabelle blush "I, um, I would rather just go back to your place..."
  mc "Oh?"
  isabelle sad "I don't want to be alone after that."
  mc "Oh, okay. Don't worry. I got you."
  mc "Come on... girlfriend."
  show isabelle blush with dissolve2
  "That gets a smile from her as she accepts my hand."
  isabelle blush "Lead the way... boyfriend."
  window hide
  show isabelle blush at disappear_to_right
  pause 0.5
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.advance()
  $game.location = "home_kitchen"
  $home_kitchen["night"] = False
  pause 1.0
  show isabelle blush at appear_from_left
  pause 0.0
  hide black onlayer screens with Dissolve(.5)
  pause 0.25
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  call quest_isabelle_stars_sex
  window hide
  $quest.isabelle_stars["girlfriend"] = isabelle["girlfriend"] = True
  $quest.isabelle_stars["finished_this_scene"] = True
  $quest.isabelle_stars.finish()
  return
