image nurse_aid_not_girlfriend = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-26,0), Transform("nurse aftercare", size=(206,116))), "ui circle_mask"), (6,6), "ui crossed_out_circle2")


label quest_isabelle_gesture_start:
  "There are few things worse than knowing the exact moment you irrevocably changed things forever."
  "Things that were great, forever marred by an instant of stupidity."
  "A lapse in judgment, and your own selfish nature."
  "You fucked up by letting your urges get the better of you."
  # "You didn't stop to think about one of the people you care about the most."
  "You didn't stop to think about one of the people you care about the most.{space=-110}"
  "And no matter how much you apologize..."
  "...no matter how often they might say they forgive you..."
  "...you know there is no going back."
  "Not to the exact way things were before."
  "There is simply the time before your mistake, and the time after."
  "And all you can do is hope to regain some of the trust."
  "Some of the magic."
  window hide
  show isabelle sad with Dissolve(.5)
  pause 0.25
  show isabelle sad at move_to(.75,.75)
  pause 1.0
  show isabelle sad flip at move_to(.25,1.0)
  pause 1.25
  show isabelle sad at move_to(.5,.5)
  pause 0.75
  hide isabelle with Dissolve(.5)
  window auto
  # "[isabelle] has hardly looked my way, much less spoken to me since she walked in on me fucking [kate] that night."
  "[isabelle] has hardly looked my way, much less spoken to me since she{space=-35}\nwalked in on me fucking [kate] that night."
  "It doesn't matter how often I try to explain myself."
  "Explain what it was actually about."
  "That carnal, animal need to dominate and finally put the bitch in her place."
  "No desire for [kate] herself. Just the need to conquer her once and for all."
  # "I thought maybe [isabelle] would understand... but it turns out that she really didn't."
  "I thought maybe [isabelle] would understand... but it turns out that she{space=-30}\nreally didn't."
  "What we had really meant something to her."
  "And I ruined it in one brutish, insensitive act."
  "It might be throwing all instincts of survival out the window, but I'm still a man of hope, despite everything..."
  "And sooner or later I'll find the courage to make things right."
  window hide
  $quest.isabelle_gesture.start()
  return

label quest_isabelle_gesture_courage:
  "In my old life, I would never have found the courage to talk to her after doing something like this..."
  "...but I guess that's what second chances are for."
  "Time to take the devil by the horn, and face up to my mistakes."
  window hide
  show isabelle angry with Dissolve(.5)
  window auto
  isabelle angry "What do you want?"
  mc "Are you really going to stay mad at me forever?"
  mc "We were both caught up in the passion of vengeance that night..."
  mc "Surely, you can see it from my perspective?"
  isabelle angry "Oh, like you're trying so hard to see it from mine?"
  show isabelle angry at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I think you're being unreasonable.\"":
      show isabelle angry at move_to(.5)
      mc "I think you're being unreasonable."
      $isabelle.lust-=3
      isabelle angrier "Oh, sod off, mate!"
      isabelle angrier "I'll show you bloody unreasonable!"
      "Shit. I should not have said that."
      "[isabelle] looks like she's about to light a match and set me on fire."
      # "And my inner voice starts counting down, waiting for the explosion..."
      "And my inner voice starts counting down, waiting for the explosion...{space=-20}"
    "\"I feel like I have...\"":
      show isabelle angry at move_to(.5)
      mc "I feel like I have..."
      mc "And I am sorry I made you feel that way."
      mc "I know it's not a nice feeling."
      isabelle angry "Not a nice feeling?"
      $isabelle.love-=3
      isabelle angry "Really? That's it?"
      mc "Err, what else is there?"
      isabelle angrier "..."
      "In an eerie moment, her face goes from boiling anger and disdain to blank."
      "It leaves me confused, jarred into uncertainty."
      # "It'll only be a matter of seconds before the fuse ignites and her head goes boom..."
      "It'll only be a matter of seconds before the fuse ignites and her head{space=-15}\ngoes boom..."
    "\"You're right. I've been an ass.\"":
      show isabelle angry at move_to(.5)
      mc "You're right. I've been an ass."
      isabelle angry "That's a bloody understatement."
      mc "I don't know what else I can say..."
      isabelle annoyed "Good, because I don't want to hear it."
      isabelle annoyed "Words mean very little. It's actions that matter."
      isabelle annoyed "And your actions have been very questionable."
      mc "..."
      "[isabelle] really knows how to guilt trip a guy."
      "I thought after some time she would be more willing to hear me out."
      "To mend this tear in our relationship, complicated as it is..."
  window hide
  if game.location in ("school_ground_floor","school_first_hall","school_forest_glade"):
    if renpy.showing("isabelle angrier"):
      show isabelle angrier at disappear_to_left(.75)
    elif renpy.showing("isabelle annoyed"):
      show isabelle annoyed at disappear_to_left(.75)
  else:
    if renpy.showing("isabelle angrier"):
      show isabelle angrier at disappear_to_right(.75)
    elif renpy.showing("isabelle annoyed"):
      show isabelle annoyed at disappear_to_right(.75)
  $isabelle["at_none_this_scene"] = True
  pause 0.5
  window auto
  "...but instead, she turns away from me and storms off."
  "I'm left standing there on my own, more confused than before."
  "Endlessly digging myself into a deeper and deeper hole."
  "Clearly, I still have a lot left to learn about the emotions of women."
  "..."
  "It's in these moments I wish I had a dad to ask for advice..."
  "But I guess I'll have to settle for the next best thing."
  $quest.isabelle_gesture.advance("advice")
  return

label quest_isabelle_gesture_advice:
  show jo sad with Dissolve(.5)
  jo sad "Hey, kiddo. What's the matter?"
  "She must be able to see the woe on my face."
  "Plain as day."
  mc "I messed up big time..."
  mc "...and I'm not sure what to do now."
  jo worried "What is it? What happened?"
  jo worried "You're not in any kind of legal trouble, are you?"
  mc "What? No! Of course not!"
  mc "It's, err... girl trouble."
  jo thinking "Girl trouble?"
  "Her clear shock at that only adds to the sting of the situation."
  "Apparently, she's more likely to believe I'm in trouble with the law than with a girl."
  "That's just great."
  mc "Yeah..."
  mc "Specifically, with [isabelle]."
  jo sad "Oh. I see."
  jo sad "Well, honey, why don't you tell me what happened, and I'll see if I can help?"
  show jo sad at move_to(.75)
  menu(side="left"):
    extend ""
    "\"It's... complicated.\"":
      show jo sad at move_to(.5)
      mc "It's... complicated."
      jo blush "You know you can share with me, right?"
      $jo.love+=1
      # jo blush "I know you're a young man now with your own stuff going on, but I'll always be there for you."
      jo blush "I know you're a young man now with your own stuff going on, but I'll{space=-10}\nalways be there for you."
      "Huh. I did not expect this sentimental turn from [jo] right now."
      # "She's usually just nagging me about my shortcomings... and, I guess, doing the dishes."
      "She's usually just nagging me about my shortcomings... and, I guess,{space=-30}\ndoing the dishes."
      "But it's nice to still have someone to turn to with my problems."
      mc "Thanks, [jo]. I appreciate that."
      jo blush "It's okay, sweetheart! Just tell me what's bothering you."
      mc "It's [isabelle]..."
      mc "I really like her, and... I messed things up with her pretty badly."
      jo afraid_hands_to_the_side "Oh? I had no idea! The English girl?"
      # "She sounds both surprised and saddened at the same time. A strange combination."
      "She sounds both surprised and saddened at the same time. A strange{space=-40}\ncombination."
      jo sarcastic "[isabelle] is a good girl."
      jo sarcastic "One of our top students. Very intelligent. Very mature."
      "How to really emphasize that I lost someone incredible. Thanks, [jo]."
      jo sarcastic "If you want it, I'm sure you could patch things up with her."
      mc "Do you really think so?"
    "\"She caught me with another girl.\"":
      show jo sad at move_to(.5)
      mc "She caught me with another girl."
      jo embarrassed "What?! As in..."
      "She lowers her voice to a whisper."
      jo embarrassed "...{i}sex{/}?"
      mc "Yes, [jo]. Sex. I'm eighteen."
      jo embarrassed "I know, I know! It's just... you shouldn't be fooling around like that!"
      jo embarrassed "It's inappropriate and unsafe!"
      mc "I didn't ask for a lecture, okay? I just need advice."
      $jo.love-=1
      jo annoyed "I can't believe this, [mc]! Two girls?"
      jo annoyed "..."
      jo thinking "And I didn't know you and [isabelle] were together."
      jo thinking "She's a good girl. She deserves better than that."
      mc "Well, we're kind of together. But we're not together-together."
      mc "That's why she's being so unreasonable, though."
      jo skeptical "Do you like [isabelle]?"
      mc "Yeah, I do."
      jo skeptical "You need to start treating her right, then."
      jo skeptical "You can't yank a girl around like that."
      jo skeptical "I thought I taught you to respect women, mister."
      "Great. The unsolicited extended course. I better grab my notebook."
      mc "You don't even know what actually happened, or why..."
      jo skeptical "But I know I brought you up better than this."
      # jo skeptical "You can't just disregard and disrespect other people's feelings to get your way."
      jo skeptical "You can't just disregard and disrespect other people's feelings to get{space=-25}\nyour way."
      "This is what I get for being honest..."
      mc "Okay, okay. Point taken. I'm sorry."
      jo neutral "I hope you actually are."
    "?isabelle.love>=6@[isabelle.love]/6|{image=isabelle contact_icon}|{image=stats love}|\"I got scared...\"":
      show jo sad at move_to(.5)
      mc "I got scared..."
      jo afraid "Scared? Of what?"
      mc "My feelings for [isabelle]..."
      mc "I think part of me was trying to sabotage it..."
      mc "I don't want to get hurt, so I thought... maybe if I hurt her first..."
      jo blush "Oh, sweetheart."
      jo blush "I had no idea you felt that way about her."
      mc "I guess if no one knows, it will suck less when it all falls apart..."
      jo blush "I know it's scary, but giving your heart to someone can be the most wonderful thing."
      jo blush "Don't let fear hold you back from something great, okay?"
      mc "I don't know..."
      jo flirty_hands_to_the_side "Believe me. If you really care about her, then it's worthwhile."
      # "The way [jo] looks at me, so sure that I deserve love and happiness..."
      "The way [jo] looks at me, so sure that I deserve love and happiness...{space=-5}"
      # "...that I deserve someone as wonderful and giving and kind as [isabelle]..."
      "...that I deserve someone as wonderful and giving and kind as [isabelle]...{space=-75}"
      $isabelle.love+=1
      $mc.love+=1
      "...it fills me with renewed hope."
      mc "You're right. Thank you, [jo]. It means a lot."
      jo flirty_hands_to_the_side "Anytime, darling."
  mc "So, how do you think I can try to fix this with [isabelle]?"
  jo thinking "Well..."
  jo thinking "You can't just try to talk to her if she doesn't want to listen."
  jo thinking "You need to make a grand gesture."
  "Of course. A grand freaking gesture. Why didn't I think of that?"
  "Why do women have to be so complicated?"
  mc "Err, okay. what sort of grand gesture?"
  jo laughing "It doesn't have to be anything crazy!"
  jo laughing "A heartfelt letter, some chocolate, and maybe some flowers would go a long way."
  jo laughing "Women just want to know you really care."
  mc "I guess that does make sense."
  mc "I'll do that, then. Thanks, [jo]!"
  # jo excited "Good luck, honey! And make sure you treat her right after this, okay?"
  jo excited "Good luck, honey! And make sure you treat her right after this, okay?{space=-35}"
  window hide
  hide jo with Dissolve(.5)
  window auto
  "Ugh, she's right..."
  "If this works, I really need to be careful not to screw it up again."
  "..."
  "I guess I'll try my hand at that letter first."
  $quest.isabelle_gesture.advance("letter")
  return

label quest_isabelle_gesture_letter_upon_entering:
  "Writing isn't exactly my forte..."
  # "So, what better place to seek out inspiration than ye olde classroom of English?"
  "So, what better place to seek out inspiration than ye olde classroom{space=-5}\nof English?"
  "Maybe one of these mossy authors that [isabelle] loves so much will come through for me?"
  "Let's see here..."
  $quest.isabelle_gesture["mossy_authors"] = True
  return

label quest_isabelle_gesture_letter:
  "Moby, I've been a dick..."
  "...and now it's time to make a whale of an apology."
  "I've done the crime, and must now endure the punishment."
  "It's been sour grapes and [isabelle]'s wrath for days."
  # "But if I can swallow my pride, maybe I can fix her prejudice against me."
  "But if I can swallow my pride, maybe I can fix her prejudice against me.{space=-60}"
  "After all, it's like Shakespeare said..."
  "...to plea, or not to plea."
  label quest_isabelle_gesture_letter_minigame:
    window hide
    show minigames poem background as minigame_background
    show minigames poem isabelle_autumn_sticker_neutral as minigame_isabelle at sticker_right onlayer screens
    show black onlayer screens zorder 100
    with Dissolve(.5)
    $game.ui.hide_hud = True
    pause 0.5
    hide screen interface_hider
    hide black onlayer screens
    with Dissolve(.5)
    $renpy.transition(Dissolve(0.25))
    call start_letter_minigame
    if happy:
      show isabelle_autumn_sticker_happy as minigame_isabelle at sticker_right onlayer screens
    elif angry:
      show isabelle_autumn_sticker_angry as minigame_isabelle at sticker_right onlayer screens
    else:
      show isabelle_autumn_sticker_neutral as minigame_isabelle at sticker_right onlayer screens
    with None
    show screen interface_hider
    show black onlayer screens zorder 100
    with Dissolve(1.0)
    pause 0.5
    hide minigame_background
    hide minigame_isabelle onlayer screens
    hide black onlayer screens
    with Dissolve(.5)
    if isabelle_words["happy"] > isabelle_words["angry"]:
      $mc.add_item("letter_to_isabelle")
      pause 0.25
      window auto
      "Aha! Finished!"
      "And it's a masterpiece, if I do say so myself."
      "No girl reading this would be able to hate me."
      "Now, I just need to find myself a nice bouquet..."
      $isabelle["at_none_this_scene"] = True
      $quest.isabelle_gesture.advance("bouquet")
      return
    else:
      window auto
      "Yikes. This did not turn out the way I had planned."
      "If I read it to [isabelle], she'll probably strangle me to death..."
      "...and as hot as that would be, I have unfinished business."
      jump quest_isabelle_gesture_letter_minigame

label quest_isabelle_gesture_bouquet_upon_entering:
  "It takes a strong flower to endure the fall..."
  # "...but I have a feeling that's the exact kind of flower [isabelle] would like."
  "...but I have a feeling that's the exact kind of flower [isabelle] would like.{space=-55}"
  "She's a survivor, too."
  "She doesn't bend or blow over for just any sort of frost or rain."
  "Much like some of the stuff blooming right now."
  $quest.isabelle_gesture["strong_flower"] = True
  return

label quest_isabelle_gesture_bouquet:
  window hide
  show maxine sad:
    xanchor 0.075 xpos 1.0 yalign 1.0
    linear 1.5 xanchor 0.925 xpos 0.0
  pause 1.5
  window auto
  "Huh? That's interesting..."
  "I wonder what [maxine] is doing out here?"
  "She's looking a bit frazzled and confused."
  "I should probably avoid enga—"
  window hide
  show maxine confident at appear_from_left
  pause 0.5
  window auto
  maxine confident "Ah! [mc]!"
  "Crap."
  maxine confident "This place feels familiar, doesn't it?"
  mc "I mean, yeah?"
  mc "It's right behind the school... and you've been here before."
  maxine skeptical "Indeed. And yet, there's this mysterious pull."
  maxine skeptical "An unseen force, guiding my steps to this very place."
  # maxine skeptical "Like a magnet, tugging at something buried deep in my subconscious mind."
  maxine skeptical "Like a magnet, tugging at something buried deep in my subconscious{space=-30}\nmind."
  show maxine skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Like a deja vu kind of thing?\"":
      show maxine skeptical at move_to(.5)
      mc "Like a deja vu kind of thing?"
      $maxine.love+=1
      maxine excited "Exactly like that!"
      # maxine excited "Except it's persisting beyond the usual time-window of a regular deja vu experience."
      maxine excited "Except it's persisting beyond the usual time-window of a regular deja{space=-25}\nvu experience."
      mc "Maybe you need some time off?"
      mc "I mean, do you even sleep?"
      maxine concerned "It's right at the tip of my tongue. I can't sleep now."
      mc "I guess getting some fresh air out here would probably help, too."
      # maxine concerned "If only I could... dig a little deeper... I know there's something there..."
      maxine concerned "If only I could... dig a little deeper... I know there's something there...{space=-10}"
    # "\"Maybe your primal urges led you to a potential sexual partner?\"":
    "\"Maybe your primal urges led you\nto a potential sexual partner?\"":
      show maxine skeptical at move_to(.5)
      mc "Maybe your primal urges led you to a potential sexual partner?"
      maxine eyeroll "I've been out here for the last hour."
      maxine eyeroll"I'm sure I would've found that person by now."
      mc "Okay, Miss Savage! I wasn't looking for a third degree burn."
      maxine skeptical "This goes deeper. It's like an itch I can't scratch."
  "[maxine] seems a bit lost, which is unusual for her."
  "She usually walks with purpose, trying to solve the mysteries of the universe."
  "But something about this forest glade clearly has her in some sort of manic obsession."
  mc "Anyway, I just came here to pick some flowers for [isabelle]. I'll be on my way."
  maxine thinking "Oh? I had no idea she was a floral specialist."
  maxine thinking "I, myself, am something of a hobbyist."
  mc "It's for an apology, actually."
  maxine sad "Why? What did she do to the flowers?"
  mc "Nothing. I'm the one apologizing."
  maxine sad "What did {i}you{/} do to the flowers?"
  mc "It's what I did to [isabelle]..."
  maxine thinking "You're not making any sense at all."
  mc "Me?! Who even apologizes to flowers?!"
  maxine eyeroll "It's the polite thing to do when you tread on them."
  maxine eyeroll "They really appreciate it."
  mc "..."
  mc "Is that why you're out here?"
  maxine confident "Yes."
  maxine skeptical "Wait, no. There's something else here, but I can't figure out what..."
  mc "All right, keep your secrets."
  mc "I'm just going to leave..."
  maxine confident_hands_down "Farewell! And remember to be kinder to the flowers."
  maxine confident_hands_down "You never know when they'll take their moment to strike."
  window hide
  $quest.isabelle_gesture["manic_obsession"] = maxine["at_forest_glade_now"] = True
  scene black with Dissolve(.07)
  $game.location = "school_entrance"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location with Dissolve(.5)
  window auto
  "Well, that was weird... as usual."
  "..."
  "Whatever. [isabelle] is my only concern right now."
  # "With flowers and a letter of indulgence in hand, only one thing remains..."
  "With flowers and a letter of indulgence in hand, only one thing remains...{space=-90}"
  "Spending money on expensive chocolate."
  "I sure hope this works. I can't afford to go bankrupt again."
  $quest.isabelle_gesture.advance("chocolate")
  return

label quest_isabelle_gesture_chocolate:
  "No faster way to get ahold of some chocolates than the good old interweb..."
  "Modern convenience at double the cost."
  "Thanks, rainforest!"
  "..."
  "..."
  # "Hmm... here on this fast and easy website I have a variety of choices..."
  "Hmm... here on this fast and easy website I have a variety of choices...{space=-45}"
  menu(side="middle"): #repeatable if player chooses "Not now", start with this menu
    extend ""
    # "?mc.money>=80@[mc.money]/80|{image=ui hud icon_money}|Buy Hearshey Shesay":
    "?mc.money>=80@[mc.money]/80|{image=ui hud icon_money}|Buy Hearshey\nShesay":
      "Word on the street is that this is quality chocolate."
      "Only the best for [isabelle]."
      $mc.money-=80
      $home_computer["hearshey_shesay_ordered"] = home_computer["hearshey_shesay_ordered_today"] = True
      $quest.isabelle_gesture.advance("package")
    "?mc.money>=20@[mc.money]/20|{image=ui hud icon_money}|Buy Tob-le-bone":
      "{i}\"Chocolate so good, she'll want to bone you after!\"{/}"
      "Let's put that slogan to the test."
      $mc.money-=20
      $home_computer["tob_le_bone_ordered"] = home_computer["tob_le_bone_ordered_today"] = True
      $quest.isabelle_gesture.advance("package")
    # "?mc.money>=2@[mc.money]/2|{image=ui hud icon_money}|Buy Dick Wonky's Chocolate Bar":
    "?mc.money>=2@[mc.money]/2|{image=ui hud icon_money}|Buy Dick Wonky's\nChocolate Bar":
      "It's not exactly the most expensive chocolate on the market..."
      "...but it's the thought that counts, right?"
      $mc.money-=2
      $home_computer["dick_wonky_ordered"] = home_computer["dick_wonky_ordered_today"] = True
      $quest.isabelle_gesture.advance("package")
    "Not Now":
      return
  "..."
  "Okay, now for the waiting game..."
  return

label quest_isabelle_gesture_apology_upon_entering:
  show isabelle angry with Dissolve(.5)
  pause 0.25
  show isabelle angry at move_to(.75,.75)
  pause 1.0
  show isabelle angry flip at move_to(.25,1.0)
  pause 1.25
  show isabelle angry at move_to(.5,.5)
  pause 0.75
  hide isabelle with Dissolve(.5)
  "Okay. There she is."
  "Still looking moody."
  "But [jo] is a woman, which means she understands them, right?"
  "Surely, she wouldn't lead me astray and let this whole thing blow up in my face..."
  "..."
  "Time to apologize like I mean it."
  $quest.isabelle_gesture["time_to_apologize"] = True
  return

label quest_isabelle_gesture_apology:
  show isabelle annoyed with Dissolve(.5)
  isabelle annoyed "I'm surprised to see you back for more so soon."
  mc "What can I say? You're worth weathering the moods for."
  isabelle sad "..."
  isabelle sad "I guess I can hold onto a grudge..."
  "That's an understatement."
  "I better tread lightly here, though."
  mc "I understand why you're upset."
  mc "I, err... I got you some flowers."
  window hide
  $mc.remove_item("wildflowers")
  pause 0.25
  window auto show
  show isabelle thinking with dissolve2
  isabelle thinking "Oh?"
  isabelle thinking "That's... sweet."
  "[isabelle] doesn't seem totally sold on the bouquet."
  "Probably because she's not like other girls."
  "She can't just be bought with pretty things and empty words."
  "Damn it! I knew it!"
  "..."
  "Oh, well. Too late to turn back now."
  mc "And some chocolate..."
  window hide
  if mc.owned_item("hearshey_shesay"):
    $mc.remove_item("hearshey_shesay")
    pause 0.25
    window auto show
    show isabelle afraid with dissolve2
    isabelle afraid "Oh, wow!"
    # isabelle afraid "My dad bought me this once when he had to break the news we were moving yet again. It's some of the best there is."
    isabelle afraid "My dad bought me this once when he had to break the news we were{space=-20}\nmoving yet again. It's some of the best there is."
    isabelle flirty "When it melts in your mouth... it's like a taste of heaven."
    $mc.charisma+=1
    mc "So, just like when I kiss you?"
    $isabelle.love+=1
    isabelle blush "..."
    isabelle blush "Is this your way of apologizing?"
  elif mc.owned_item("tob_le_bone"):
    $mc.remove_item("tob_le_bone")
    pause 0.25
    window auto show
    show isabelle cringe with dissolve2
    isabelle cringe "I've seen the marketing campaigns for this chocolate."
    isabelle cringe "Does this mean what I think it means?"
    "Shit. What does she think it means?"
    mc "Err, I don't know..."
    isabelle annoyed "So, is this your way of apologizing?"
  elif mc.owned_item("dick_wonky"):
    $mc.remove_item("dick_wonky")
    pause 0.25
    window auto show
    show isabelle afraid with dissolve2
    isabelle afraid "What's this?"
    mc "Err, chocolate?"
    isabelle cringe "I can see that..."
    $isabelle.love-=1
    isabelle cringe "...and I can see what you think I'm worth."
    "Crap. She doesn't like it."
    "I really shouldn't have cheaped out..."
    isabelle annoyed "So, is this your way of apologizing?"
  mc "Well, partly."
  mc "I also wrote you a letter."
  window hide
  $mc.remove_item("letter_to_isabelle")
  pause 0.25
  window auto show
  show isabelle skeptical with dissolve2
  isabelle skeptical "Thanks, I think?"
  isabelle skeptical_letter "..."
  "She opens it and just stands there for a moment."
  "Her eyes scanning my messy handwriting."
  show isabelle excited_letter with dissolve2
  "She bites her lip, the color rising in her cheeks."
  "My heart flutters with hope..."
  show isabelle concerned_letter with dissolve2
  "...but then her face falls."
  # "Worry creeps into the lines on her face, and sadness pools in her eyes."
  "Worry creeps into the lines on her face, and sadness pools in her eyes.{space=-55}"
  mc "Err, what's wrong?"
  isabelle sad "It's just... how do I know you won't do something like this again?"
  isabelle sad "And do you think you can just pomper me with gifts and all will be forgiven?"
  mc "Look, I don't know what else you want from me, okay?"
  mc "Any other girl would be more than happy with a gesture like this!"
  isabelle angry "Any other girl? Seriously?!"
  isabelle angry "Why don't you just go and have your bloody pick of the lot, then?!"
  mc "..."
  mc "I'm sorry. I didn't mean it like that."
  isabelle angry "Well, it's why we're in this mess in the first place, isn't it?"
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  show isabelle angry at move_to(.75)
  menu(side="left"):
    extend ""
    "?isabelle.love>=10@[isabelle.love]/10|{image=isabelle contact_icon}|{image=stats love}|\"You're the only one that I want.\"":
      show isabelle angry at move_to(.5)
      mc "You're the only one that I want."
      isabelle annoyed "Are you going to break into a song and dance now?"
      mc "No! I mean it."
      show isabelle annoyed at move_to(.25)
      menu(side="right"):
        extend ""
        # "?isabelle.love>=20@[isabelle.love]/20|{image=isabelle contact_icon}|{image=stats love}|\"I want you to be my girlfriend.\"" if not nurse["girlfriend"]:
        "?isabelle.love>=20@[isabelle.love]/20|{image=isabelle contact_icon}|{image=stats love}|\"I want you to be\nmy girlfriend.\"" if not nurse["girlfriend"]:
          show isabelle annoyed at move_to(.5)
          mc "I want you to be my girlfriend."
          mc "There is no other girl I think of."
          mc "Nobody's company I would rather have."
          mc "It's you who are constantly running through my mind."
          mc "When you're not around, it's like an ache in my heart."
          mc "A hole in my stomach."
          isabelle blush "..."
          mc "I hate that I hurt you. And I swear I'll never do it again."
          mc "You deserve everything."
          isabelle blush "I... I don't know what to say..."
          mc "You don't have to say anything."
          mc "And I understand if you can't forgive me."
          mc "But you're the only person that sees me for who I am."
          mc "The only one that gave me a chance when no one else would."
          mc "And the only one that makes me happy."
          isabelle blush "Truly? Do you mean it?"
          mc "More than anything I've ever said in my life."
          isabelle blush "Oh, [mc]! I think that's one of the sweetest things I've ever heard."
          mc "Well, it's true."
          isabelle blush "I believe you. I feel the same way."
          # isabelle blush "Ever since you kept me company on my sister's birthday... I've known that I really like you."
          isabelle blush "Ever since you kept me company on my sister's birthday... I've known{space=-30}\nthat I really like you."
          # isabelle sad "That's why I got so devastated when I saw you with [kate]. I thought we were meant to be."
          isabelle sad "That's why I got so devastated when I saw you with [kate]. I thought we{space=-45}\nwere meant to be."
          mc "We {i}are{/} meant to be together."
          mc "I've been an idiot for not seeing it clearly."
          isabelle blush "Very well. I forgive you."
          $isabelle.love+=5
          isabelle blush "And, um, yes... I will be your girlfriend."
          "Holy shit. I can't believe it."
          "The way [isabelle] is looking at me right now..."
          "Any trace of anger or hurt... gone."
          "Her cheeks flushed with nervous excitement."
          "Her big eyes sparkling."
          "It's so strange to see her looking this shy all of a sudden..."
          "She's one of the most confident, self assured people I know."
          "But the idea of being my girlfriend... {i}mine!{/}"
          "Turns her all sweet and cute."
          "It makes my heart trip over itself with giddy delight."
          mc "Yeah?"
          isabelle blush "Mm-hmm."
          mc "You've just made me the happiest guy in all of Newfall."
          isabelle laughing "Hyperbole!"
          mc "I'm serious!"
          mc "Come on, let's ditch the rest of the day?"
          mc "I just want to spend time with you. Soak up this moment."
          isabelle confident "Oh, my! So eager to get me all alone!"
          mc "Heh. So excited you said yes."
          isabelle confident "Excited-excited?"
          mc "You have no idea."
          isabelle confident "I don't know... It feels wrong."
          mc "How can it, when this is so right?"
          isabelle laughing "Has anyone ever told you that you can be very persistent?"
          "I know what happens if I don't..."
          "If I just let everything pass me by, I know where I'll end up."
          mc "Sometimes, you have to be."
          mc "Especially when it's something — or someone — worth having."
          isabelle confident "Oh, bloody hell! All right!"
          isabelle confident "Let's do it. Let's get out of here."
          mc "Okay, come with me!"
          window hide
          show isabelle confident at disappear_to_right
          pause 0.5
          hide screen interface_hider
          show black
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          show black onlayer screens zorder 4
          $set_dialog_mode("default_no_bg")
          "I can't believe it!"
          "My first official girlfriend... and it's [isabelle]!"
          "Fuck, am I glad things worked out like this."
          "Better than I could have ever hoped."
          while game.hour != 18:
            $game.advance()
          $game.location = "home_kitchen"
          $home_kitchen["exclusive"] = "isabelle"
          show black onlayer screens zorder 100
          pause 0.5
          show isabelle laughing at appear_from_left
          pause 0.0
          hide black
          hide black onlayer screens
          show screen interface_hider
          with Dissolve(.5)
          pause 0.25
          if "event_show_time_passed_screen" in game.events_queue:
            $game.events_queue.remove("event_show_time_passed_screen")
          call screen time_passed
          pause 0.5
          window auto
          $set_dialog_mode("")
          label quest_isabelle_stars_sex:
          "[isabelle] and I burst through the door, giggling and giddy."
          "I have never felt like this before. I never thought I would have the chance."
          "To go from a life like I was living before — cold, lonely, and pathetic — to this?"
          "I just want to cherish every moment with her from here on out."
          "I feel an endless warmth trickling through me, filling my brain with an elated buzz."
          "Drunk on happiness."
          if quest.isabelle_gesture == "apology":
            "Triumphant in our reconciliation."
          elif quest.isabelle_stars == "stargaze":
            "Deeply in love."
          isabelle blush "[mc]?"
          mc "Hmm?"
          isabelle blush "Where did you go on me?"
          mc "Err, nowhere! I just can't believe this is actually happening..."
          mc "It feels a bit like I'm dreaming."
          isabelle blush "Well, you better believe it."
          isabelle blush "Because there's no taking it back now."
          mc "Heh. As if I would want to."
          mc "I'm the luckiest guy ever."
          isabelle blush "Keep buttering me up like that and see what happens..."
          mc "You'll turn into an English breakfast?"
          isabelle laughing "Stop!"
          mc "..."
          show isabelle confident with dissolve2
          isabelle confident "..."
          "We laugh for a moment, and then our eyes meet."
          "The laughter fades and lapses into an easy, comfortable silence."
          "She's willingly mine."
          "And I guess she sees the hunger in my eyes."
          "The pink flush of shyness and excitement colors her cheeks again."
          "And then I'm crossing the room towards her in a single step."
          window hide
          show isabelle kitchen_kiss eager
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          hide black onlayer screens with Dissolve(.5)
          window auto
          if quest.isabelle_gesture == "apology":
            "Our lips meet in an explosion of relief, excitement, and tender newfound hope."
          elif quest.isabelle_stars == "stargaze":
            "Our lips meet again in a star storm of eager excitement."
          "It's a fevered, bruising kiss that scorches my insides and pulsates through my skin."
          "Her lips consume me."
          "The world disappears and time stops."
          "There's only me and [isabelle], in our own little bubble of happiness."
          "My hand meets the silky skin of her neck."
          "My fingers dig into her flesh, desperate to never let go."
          show isabelle kitchen_kiss forceful
          isabelle kitchen_kiss forceful "Ow!" with vpunch
          mc "Sorry! Did I hurt you?"
          isabelle kitchen_kiss forceful "No, no! It's fine! Don't stop."
          window hide
          pause 0.125
          show isabelle kitchen_kiss embrace with Dissolve(.5)
          pause 0.25
          window auto
          "Her words come out in a hurried, desperate breath."
          "Her fingers slide up the back of my neck, combing through my hair."
          # "With her body pressed against mine, and our hearts beating in eager synchronization, I feel like I can't get close enough."
          "With her body pressed against mine, and our hearts beating in eager{space=-15}\nsynchronization, I feel like I can't get close enough."
          "I can't get enough of the taste of her vanilla lips."
          window hide
          # show isabelle kitchen_sex lifted_up
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          show isabelle kitchen_sex lifted_up: ## This gives the image both the dissolve (transition) and hpunch (transform) effects
            block:
              linear 0.05 xoffset 15
              linear 0.05 xoffset -15
              repeat 3
            linear 0.05 xoffset 0
          pause 0.0
          hide black onlayer screens
          with Dissolve(.5)
          hide isabelle
          show isabelle kitchen_sex lifted_up ## This is just to avoid the screen shake if the player is skipping
          window auto
          "We move through the kitchen in a dream."
          "Until her back hits the counter behind her and she lets out a little gasp."
          "The sudden halt gives me the momentum I need to lift her right up on the counter top."
          isabelle kitchen_sex lifted_up "Goodness, [mc]..."
          window hide
          pause 0.125
          show isabelle kitchen_sex neck_kiss with Dissolve(.5)
          pause 0.25
          window auto
          "My lips find the soft skin of her neck."
          "The sweet smell of cinnamon leaves me intoxicated and reeling."
          "She moans softly, her breathing strained in my ear."
          isabelle kitchen_sex neck_kiss "Go on, baby..."
          "It feels so good when she calls me that."
          "I am officially hers and she is officially mine."
          "The mere thought of it drives me wild."
          "Kissing and touching her feels different, even."
          "Better."
          window hide
          pause 0.125
          show isabelle kitchen_sex anticipation with Dissolve(.5)
          pause 0.25
          window auto
          "My anticipation only mounts as my dick fully hardens."
          "[isabelle] is so beautiful with her skirt up and her panties to the side."
          "She shivers at my gentle touch, unable to take it any longer."
          window hide
          pause 0.125
          show isabelle kitchen_sex insertion with Dissolve(.5)
          pause 0.25
          show isabelle kitchen_sex penetration1 with Dissolve(.075)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with hpunch
          pause 0.25
          window auto
          isabelle kitchen_sex penetration3 "Ohhhh!"
          "In one fast, hard thrust, my dick perfectly reaches the insides of her pussy."
          "It's like they were made for each other."
          "And somehow, it feels even better than before."
          "Knowing we are exclusive."
          "Knowing hers is the only pussy I want hugging my dick."
          "Knowing my dick is the only one she'll take again and again."
          "Squeezing and pulsing around it as she whispers my name."
          "Her juices leaking out onto the very counter where [flora] eats."
          "Fuck, that's so hot."
          window hide
          pause 0.125
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration1 with Dissolve(.15)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with Dissolve(.15)
          pause 0.125
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration1 with Dissolve(.15)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with Dissolve(.15)
          pause 0.1
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration1 with Dissolve(.15)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with Dissolve(.15)
          pause 0.125
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration1 with Dissolve(.15)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with Dissolve(.15)
          pause 0.125
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration1 with Dissolve(.15)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with Dissolve(.15)
          pause 0.1
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration1 with Dissolve(.15)
          show isabelle kitchen_sex penetration2 with Dissolve(.15)
          show isabelle kitchen_sex penetration3 with Dissolve(.15)
          pause 0.25
          window auto
          isabelle kitchen_sex penetration3 "Mmm, yes! Just like that!"
          # "As I piston in and out of her, she wraps her legs around me, bringing us even closer."
          "As I piston in and out of her, she wraps her legs around me, bringing{space=-15}\nus even closer."
          "Making it impossible to pull all the way out of her."
          "Not that I would even want to."
          window hide
          pause 0.125
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.1
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.1
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.075
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.1
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.1
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.075
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          pause 0.075
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration1 with Dissolve(.125)
          show isabelle kitchen_sex penetration2 with Dissolve(.125)
          show isabelle kitchen_sex penetration3 with Dissolve(.125)
          # pause 0.25
          show isabelle kitchen_sex penetration6 with Dissolve(.25)
          window auto
          "Instead, I bury myself as deeply as possible."
          # "My face inches from hers, my breathing coming in ragged pants as I work my cock within her."
          "My face inches from hers, my breathing coming in ragged pants as\nI work my cock within her."
          "Impossibly close."
          "As close as two people can be, in every sense."
          window hide
          pause 0.125
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.075
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.05
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.05
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.075
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.075
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.05
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.25
          window auto
          isabelle kitchen_sex penetration6 "T-that's it, [mc]! Don't stop!"
          "She tilts her head back and grasps the edge of the counter as she gyrates her hips into me."
          "I hold onto her for dear life, our orgasms building together."
          mc "Oh, god! Oh, fuck!"
          window hide
          pause 0.125
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.05
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.05
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.025
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.025
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.1)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration6 with Dissolve(.1)
          pause 0.25
          window auto
          isabelle kitchen_sex penetration6 "Mmm, yes... cum with me..."
          isabelle kitchen_sex penetration6 "I'm sooo close!"
          "Her words are enough to undo me completely."
          window hide
          pause 0.125
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex penetration4 with Dissolve(.075)
          show isabelle kitchen_sex penetration5 with Dissolve(.1)
          show isabelle kitchen_sex cum with hpunch
          pause 0.25
          window auto
          isabelle kitchen_sex cum "Oooooh!"
          mc "[isabelle]!"
          "With one final, desperate thrust, I bottom out deep within her."
          "Her orgasm hits immediately and mine comes seconds after."
          "She squeezes down hard, trapping me as I explode with her name on my lips."
          "My arms wrapped around her, holding on for dear life as we ride wave after wave."
          mc "God... damn..."
          "The words leave me in a shaky, disbelieving breath as I stand there, tremoring with the aftershocks."
          "Unleashing final spurts of cum into her quivering pussy."
          window hide
          pause 0.125
          show isabelle kitchen_sex aftermath with Dissolve(.5)
          pause 0.25
          window auto
          isabelle kitchen_sex aftermath "Oh, my god..."
          # "She trembles in my arms as she finishes milking me for every last drop."
          "She trembles in my arms as she finishes milking me for every\nlast drop."
          "Our bodies slick with sweat, my face pressed into her neck."
          "It was fast... but so fucking good."
          "Each of us just too desperate and hot for the other."
          mc "God... I think that was the most amazing time yet..."
          isabelle kitchen_sex aftermath "Truly..."
          isabelle kitchen_sex aftermath "Who knew being yours could feel so good?"
          mc "Heh. Say it again?"
          isabelle kitchen_sex aftermath "What? That I'm yours?"
          mc "I don't think I'll ever get tired of hearing that..."
          "She beams at me in silence."
          "And I can't help but soak in what may very well be the best day of my life."
          window hide
          pause 0.125
          show isabelle kitchen_sex aftermath_kiss with Dissolve(.5)
          pause 0.25
          window auto
          "If I could freeze this moment for just a little longer, I would."
          "Her lips on mine, radiating with satisfaction."
          "But we have today and every day that follows to be together."
          "And I can't wait."
          $unlock_replay("isabelle_forgiveness")
          window hide
          show isabelle flirty
          show black onlayer screens zorder 100
          with Dissolve(3.0)
          pause 0.5
          hide black onlayer screens with Dissolve(.5)
          window auto
          if quest.isabelle_gesture == "apology":
            mc "[jo] and [flora] will be home soon..."
          isabelle flirty "We got a bit carried away there, didn't we?"
          mc "And I would do it all over again."
          isabelle flirty "Easy there, tiger!"
          isabelle thinking "I should probably get home..."
          mc "If you must."
          isabelle blush "I'll talk to you later, okay?"
          mc "Okay... girlfriend."
          isabelle blush "You're cute."
          window hide
          show isabelle blush at disappear_to_left
          if quest.isabelle_gesture == "apology":
            $isabelle["romance_disabled"] = False
            $game.notify_modal(None,"Love or Lust","Romance with [isabelle]:\nEnabled.",wait=5.0)
            pause 0.25
          elif quest.isabelle_stars == "stargaze":
            pause 0.5
          window auto
          "Man, what a day..."
          if quest.isabelle_gesture == "apology":
            "Good thing I took [jo]'s advice and everything worked out in the end."
            "[isabelle] and I are stronger than ever now."
            # "And I'm going to keep proving to her that I can be what she deserves."
            "And I'm going to keep proving to her that I can be what she deserves.{space=-30}"
            "Because she really does deserve the best."
          elif quest.isabelle_stars == "stargaze":
            "I feel like I nailed the date and everything thereafter."
            # "I'm such a lucky guy to receive a second chance, and to meet someone like [isabelle]."
            "I'm such a lucky guy to receive a second chance, and to meet someone{space=-65}\nlike [isabelle]."
            "It's like fate bent over backward to make up for all the shit in my old life."
            "Do I really deserve someone like her?"
            "I don't know... but I'm not complaining about hitting the jackpot."
            return
          window hide
          $home_kitchen["exclusive"] = False
          $quest.isabelle_gesture["girlfriend"] = isabelle["girlfriend"] = True
          if quest.kate_moment == "coming_soon":
            $game.quest_guide = "kate_moment"
          $quest.isabelle_gesture.finish()
          if quest.kate_moment == "coming_soon":
            pause 0.25
            window auto
            "..."
            "Crap. I totally forgot to tell [isabelle] about [kate]'s weekly doctor's appointment, didn't I?"
            "I better send her a text before I end up forgetting again."
            $quest.kate_moment.advance("text")
        # "?not nurse['girlfriend']@[isabelle.love]/20|{image=isabelle contact_icon}|{image=stats love_3}||+||{image=nurse_aid_not_girlfriend}|\"I want you to be my girlfriend.\"" if nurse["girlfriend"]:
        "?not nurse['girlfriend']@[isabelle.love]/20|{image=isabelle contact_icon}|{image=stats love_3}||+||{image=nurse_aid_not_girlfriend}|\"I want you{space=-50}\nto be my{space=-20}\ngirlfriend.\"" if nurse["girlfriend"]:
          pass
        # "\"And I'm going to spend as long as it takes proving it to you.\"":
        "\"And I'm going to spend as long\nas it takes proving it to you.\"":
          show isabelle annoyed at move_to(.5)
          mc "And I'm going to spend as long as it takes proving it to you."
          isabelle concerned "It was rather sweet of you to go through all this trouble..."
          mc "Only because I was an idiot for hurting you in the first place."
          isabelle excited "True."
          isabelle excited "Hopefully, the idiot streak is broken."
          mc "Heh. So, we're okay?"
          isabelle neutral "Yes, we're okay."
          isabelle skeptical "But I don't do that three strikes nonsense."
          mc "Not a fan of baseball?"
          isabelle skeptical "Not a fan of being slagged off."
          mc "Fair enough."
          $isabelle.love+=1
          isabelle neutral "Anyway, I do appreciate the gesture."
          isabelle neutral "What's done is done."
          mc "Thank you, [isabelle]."
          window hide
          if game.location in ("school_ground_floor","school_first_hall","school_forest_glade"):
            show isabelle neutral at disappear_to_left
          else:
            show isabelle neutral at disappear_to_right
          $isabelle["at_none_this_scene"] = True
          $isabelle["romance_disabled"] = False
          $game.notify_modal(None,"Love or Lust","Romance with [isabelle]:\nEnabled.",wait=5.0)
          pause 0.25
          window auto
          "With that, she gives me a small, reassuring smile, and then leaves."
          "At least she's not angry with me anymore..."
          # "...and from here on out, I will show her just how important she really is to me."
          "...and from here on out, I will show her just how important she really{space=-10}\nis to me."
          window hide
          if quest.kate_moment == "coming_soon":
            $game.quest_guide = "kate_moment"
          $quest.isabelle_gesture.finish()
          if quest.kate_moment == "coming_soon":
            pause 0.25
            window auto
            "..."
            "Crap. I totally forgot to tell [isabelle] about [kate]'s weekly doctor's appointment, didn't I?"
            "I better send her a text before I end up forgetting again."
            $quest.kate_moment.advance("text")
    # "\"You act like I cheated on you, but we're not a couple.\"":
    "\"You act like I cheated on you,\nbut we're not a couple.\"":
      show isabelle angry at move_to(.5)
      mc "You act like I cheated on you, but we're not a couple."
      isabelle cringe "Oh, bloody mature, are we?"
      mc "No, just factual."
      mc "I do like you, but we never agreed to be exclusive."
      isabelle cringe "Brilliant. Just brilliant."
      isabelle cringe "So, you wouldn't mind if I shagged... bloody... Chad, then?"
      mc "..."
      "That name sends a jolt through me."
      "A wave of nausea crashing through my stomach."
      isabelle cringe "Since we're not exclusive, you wouldn't mind it, would you?"
      mc "The thought makes me sick..."
      isabelle cringe "Well, now you know how I feel."
      isabelle thinking "But I don't think I could get through letting Chad touch me without boaking."
      isabelle thinking "Maybe [kate], though."
      mc "Wait, what?"
      # "As her words register, I feel an onslaught of confusing emotions and sensations."
      "As her words register, I feel an onslaught of confusing emotions and{space=-15}\nsensations."
      "On the one hand, the thought of her and [kate] together... mmm..."
      "The image automatically makes my dick twitch."
      "But on the other hand... what if [isabelle] actually likes it?"
      "And likes it more than she does with me?"
      "I don't think I could bear that."
      mc "That's very funny..."
      isabelle cringe "I don't see anyone laughing. Do you?"
      mc "..."
      "She's being serious, isn't she?"
      mc "Err, I guess if that's what it takes for you to forgive me..."
      isabelle cringe "Don't you think it's only fair?"
      mc "I mean, how are you even going to convince her to do it?"
      isabelle annoyed_left "Oh, don't worry about that. I have a good plan already."
      mc "...have you been thinking about fucking her?"
      isabelle annoyed "Maybe I have. Maybe I'll choose to be with her instead."
      isabelle annoyed "It's not like we're exclusive anyway, right?"
      mc "Okay, now you're just trying to antagonize me."
      isabelle sad "Oh? So, you admit that it's antagonizing now?"
      isabelle sad "And perhaps even hurtful?"
      "Ugh..."
      mc "Look, I'm just trying to make things right, okay?"
      isabelle annoyed "I don't see what the problem is."
      isabelle annoyed "If you get to fuck her, then I get to fuck her, too."
      isabelle angry "A true eye for an eye!"
      mc "It was only once!"
      isabelle angry "I'll only fuck her once, then. No big deal."
      mc "Fine. Whatever."
      mc "Let me know when you're done, I guess."
      isabelle cringe "Oh, no. I'm not just going to do it..."
      isabelle cringe "I'm going to do it, and you're going to watch us."
      isabelle cringe "I want you to really know how it feels."
      "Damn. This is some twisted shit."
      mc "Seriously?"
      isabelle cringe "Seriously."
      isabelle cringe "That's the only way I'll forgive you."
      mc "All right, fine. I'll come watch."
      # mc "I still don't understand how you're going to get [kate] to go along with it, though..."
      mc "I still don't understand how you're going to get [kate] to go along with{space=-20}\nit, though..."
      isabelle annoyed "Don't you worry about that."
      isabelle annoyed "Just show up when I text you."
      mc "Err, okay..."
      if game.location == "school_english_class":
        window hide
        $isabelle["at_none_now"] = True
        $mc["focus"] = mc["focus"] if mc["focus"] else "isabelle_gesture"
        $renpy.transition(Dissolve(.25),layer="screens")
        play sound "<from 9.2 to 10.0>door_breaking_in"
        $game.location = "school_first_hall_west"
        scene location with vpunch
        pause 0.125
        window auto
        "And just like that, she kicks me out."
      else:
        $isabelle["at_none_now"] = True
        $mc["focus"] = mc["focus"] if mc["focus"] else "isabelle_gesture"
        window hide
        if game.location in ("school_ground_floor","school_first_hall","school_forest_glade"):
          show isabelle annoyed at disappear_to_left(.75)
        else:
          show isabelle annoyed at disappear_to_right(.75)
        pause 0.5
        window auto
        "And just like that, she storms off."
      "I still can't tell if she's being serious. I guess I'll just have to wait and see."
      "I don't know how we ended up on this road..."
      "...but it doesn't feel like there's any turning back now."
      $quest.isabelle_gesture["revenge"] = True
      $quest.isabelle_gesture.advance("wait")
  return

label quest_isabelle_gesture_wait:
  pause 0.3
  window hide
  play sound "<from 0.25 to 1.0>phone_vibrate"
  pause 0.5
  if not mc.check_phone_contact("isabelle"):
    $mc.add_phone_contact("isabelle")
  $set_dialog_mode("phone_message_centered","isabelle")
  isabelle "This is what you wanted."
  isabelle "Come to the English classroom and see how you like it."
  $set_dialog_mode("phone_message_plus_textbox")
  window auto
  "Somehow, I doubt she's talking about her research paper..."
  "Is this really happening right now, or is she messing with me?"
  "..."
  "I guess there's only one way to find out."
  $quest.isabelle_gesture["only_one_way"] = True
  window hide
  $set_dialog_mode("")
  pause 0.5
  return

label quest_isabelle_gesture_revenge_early:
  "Oh? Is that [isabelle]'s voice?"
  "I better wait for her to text me before I barge in."
  return

label quest_isabelle_gesture_revenge:
  "Let's see what [isabelle] is actually getting up to..."
  "I'm sure she's just messing with me, right?"
  "There's no way she actually tries to fuck [kate]."
  "She's just angry and needs a way to release her frustration."
  "And maybe I can help with that."
  "Some passionate hate s—"
  kate "What do you want, Poppins?"
  isabelle "Oh, wow! That's so clever! You know a single British character."
  if quest.kate_moment == "coming_soon":
    kate "I'm here for five minutes to pick up some things, and you manage to ambush me in that time?"
    isabelle "Coincidental."
  "Damn. [kate] is actually in there with her."
  "Maybe I'll just take a quick peek and see what they're doing..."
  window hide
  show isabelle door_peek confrontation
  show black onlayer screens zorder 100
  with Dissolve(.5)
  if mc["focus"] == "isabelle_gesture":
    $mc["focus"] = ""
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  kate "Whatever. I'm busy. I don't have time to properly roast you."
  "There's a hint of [kate]'s old fire crackling in her voice."
  "No matter what happens next, it doesn't seem like she's going to just roll over for [isabelle]."
  isabelle door_peek confrontation "Well, so am I."
  isabelle door_peek confrontation "I'm doing a little ghost hunting."
  kate "..."
  "That seems to deflate [kate] some."
  "She's quiet, the air heavy with a breathless tension."
  kate "Why would you say that?"
  isabelle door_peek confrontation "I heard the school is haunted. I just thought I would see for myself."
  kate "I don't need this..."
  window hide
  pause 0.125
  show isabelle door_peek step_around1 with Dissolve(.5)
  pause 0.25
  show isabelle door_peek step_around2 with Dissolve(.5)
  pause 0.25
  window auto
  "[kate] tries to step around [isabelle], but [isabelle] sidesteps in front of her, blocking her way to the door."
  kate "What are you doing, freak? Let me out!"
  isabelle door_peek step_around2 "I wanted to ask you something first."
  kate "Well, too bad."
  isabelle door_peek step_around2 "Why are you always such a twat?"
  kate "Ugh! Just leave me alone!"
  "The old [kate] probably would have had an actual comeback to that..."
  "...but the new one clearly just wants to get away."
  window hide
  pause 0.125
  show isabelle door_peek other_direction1 with Dissolve(.5)
  pause 0.25
  show isabelle door_peek other_direction2 with Dissolve(.5)
  pause 0.25
  window auto
  "She attempts a maneuver around [isabelle] in the other direction, but is blocked again."
  isabelle door_peek other_direction2 "Is it because that's the only way you feel anything?"
  isabelle door_peek other_direction2 "Or are you just a sociopath?"
  "These words seem to reignite [kate]'s temper."
  window hide
  pause 0.125
  show isabelle door_peek arm_swing1 with Dissolve(.5)
  pause 0.25
  window auto
  "She straightens up and cocks her arm back, only to swing it back around, aiming her hand at [isabelle]'s face."
  window hide
  pause 0.125
  show isabelle door_peek arm_swing2 with hpunch
  pause 0.25
  window auto
  "However, [isabelle] is quick to react."
  "She probably saw the fire raging in [kate]'s eyes."
  "She catches [kate]'s wrist before she can make contact, then holds it tight for a moment as they stare each other down."
  window hide
  pause 0.125
  show isabelle door_peek kiss1 with Dissolve(.5)
  pause 0.25
  window auto
  "And then, to [kate]'s surprise and mine, [isabelle] presses her lips to [kate]'s, disarming her for a moment."
  if isabelle.love >= 10:
    "And it's as if a crack opens in my heart."
    "Tears roll down my cheeks."
    "How did I ruin it with [isabelle] so badly?"
  # "It's hard to tell if it's in revenge for the attempted slap, or something else entirely..."
  "It's hard to tell if it's in revenge for the attempted slap, or something{space=-15}\nelse entirely..."
  "If it was an accident of passion or intentional."
  if isabelle.love >= 10:
    "A part of me hopes that this is all for show."
    "And even then, it hurts to watch."
  window hide
  pause 0.125
  show isabelle door_peek kiss2 with Dissolve(.5)
  pause 0.25
  window auto
  "With her one hand still clasped in [isabelle]'s grip, [kate] shoves at her weakly with the other, breaking the kiss."
  "[isabelle] doesn't seem fazed, though."
  "Only an unwavering determination fills her every move."
  window hide
  pause 0.125
  show isabelle door_peek kiss3 with Dissolve(.5)
  pause 0.25
  window auto
  kate "Owww!"
  "Instead, she uses her other hand to reach up and grab [kate] roughly by the hair."
  "She yanks [kate]'s head back, keeping a tight hold on the blonde tresses, before kissing her again."
  "Harder this time, locking [kate] in her embrace."
  window hide
  pause 0.125
  show isabelle door_peek grope1 with hpunch
  pause 0.25
  window auto
  # "Then, she relentlessly pushes forward until [kate]'s back hits the wall."
  "Then, she relentlessly pushes forward until [kate]'s back hits the wall.{space=-15}"
  "Wrist pinned, lips touching, she just holds her there."
  "And that seems to undo something in [kate]."
  "Her posture slackens and she starts to kiss [isabelle] back."
  if isabelle.love >= 10:
    "And that's when I can't take it anymore."
    "This is the worst day of my life."
    "Losing what I had with [isabelle] thanks to my own stupidity and selfishness..."
  else:
    "Hesitant at first, then with more force."
    "I can hardly believe what I'm seeing..."
    "[isabelle], not taking any of [kate]'s crap and woman-handling her into submission like that..."
    "Their soft, full lips going at each other, filling the room with the luscious wet sound..."
    "..."
    "My dick takes on a mind of its own, getting harder and harder at the sight."
    "The kissing... the roughness of it..."
    "Fuck me."
  window hide
  pause 0.125
  show isabelle door_peek grope2 with Dissolve(.5)
  pause 0.25
  window auto
  "[isabelle]'s hand travels down [kate]'s body, stopping at her heaving chest."
  "Feeling her up as they kiss each other with fast, desperate lips."
  "As if both are fighting to come out on top."
  if isabelle.love >= 10:
    "And maybe they both come out on top."
    "Maybe I'm the only loser in all of this..."
  "Low moans escape from [kate]'s mouth as [isabelle] works her tongue inside."
  "And [kate] just lets it happen, growing weaker and weaker against the unrelenting force that is [isabelle]."
  window hide
  pause 0.125
  show isabelle door_peek grope3 with Dissolve(.5)
  pause 0.25
  window auto
  "Even as [isabelle] trails her hand lower, down from [kate]'s chest to her skirt."
  "Their lips never break apart."
  if isabelle.love >= 10:
    "It's the most crushing thing I've seen."
    jump quest_isabelle_gesture_revenge_just_watch
  else:
    "It's the most confusing and hot thing I've ever seen."
  menu(side="middle"):
    extend ""
    "Masturbate":
      "I can't stand it anymore."
      "If I don't get some release, I might actually explode."
      window hide
      pause 0.125
      $isabelle.lust+=1
      $mc.lust+=3
      show isabelle door_peek stroke1 grope3 with Dissolve(.5)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.2
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.15
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.25
      window auto
      "Mmm... watching the two girls continue to go at it..."
      "The way [isabelle] takes control, holding [kate] in place with her fingers up her skirt..."
      "Fuuuuck."
      window hide
      pause 0.125
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.15
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.15
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.1
      show isabelle door_peek stroke1 grope3 with Dissolve(.25)
      show isabelle door_peek stroke2 grope3 with Dissolve(.25)
      pause 0.25
      window auto
      "Watching these two together..."
      "Bitter enemies, engaging in a kissing match..."
      "And [kate] somehow losing..."
      "It's like nothing I've ever seen before."
      $quest.isabelle_gesture["masturbated"] = True
    "Just watch":
      label quest_isabelle_gesture_revenge_just_watch:
        if isabelle.love >= 10:
          "As much as I want to scream for them to stop... I can do nothing but watch it unfold."
          "Part of me probably knows that I deserve to suffer for what I did."
          "And the cracks spread across my heart."
          "Breaking it apart with every moment that passes."
        else:
          "But as hard as my dick is... I can only stand there and stare."
          "And as hot as the fantasy is, I do find it difficult to watch [isabelle] attempting to fuck someone else."
        $isabelle.love+=1
        $mc.love+=3
        if isabelle.love >= 10:
          "And it makes me realize how much I really like [isabelle]."
        else:
          "And it makes me realize how much I really like her."
        "How stupid I would be to ruin things with her."
        "Especially over someone like [kate]."
  window hide
  pause 0.125
  if quest.isabelle_gesture["masturbated"]:
    show isabelle door_peek stroke2 grope4 with Dissolve(.5)
  else:
    show isabelle door_peek grope4 with Dissolve(.5)
  pause 0.25
  window auto
  "Despite [isabelle]'s grip and hungry passion, [kate] finally comes to her senses and shoves her away."
  if isabelle.love >= 10:
    "Thank god..."
  "Both of them pant for air, confusion lingering in the silence."
  window hide
  pause 0.125
  if quest.isabelle_gesture["masturbated"]:
    show isabelle door_peek stroke2 push_away with hpunch
  else:
    show isabelle door_peek push_away with hpunch
  pause 0.25
  if quest.isabelle_gesture["masturbated"]:
    show isabelle door_peek stroke2 run_away with Dissolve(.5)
  else:
    show isabelle door_peek run_away with Dissolve(.5)
  pause 0.25
  window auto
  "Then, [kate] seems to snap back to reality."
  "Quickly, she pushes past [isabelle] and runs for the door."
  "[isabelle] just lets it happen, standing there in stunned silence."
  "Clearly, not even sure herself of what just happened."
  if quest.isabelle_gesture["masturbated"]:
    "Crap. I better get out of here before [kate] sees—"
    mc "Fuck, fuck, fuuuuuck! It's too late to stop now!"
    window hide
    pause 0.125
    show isabelle door_peek stroke1 run_away with Dissolve(.25)
    show isabelle door_peek stroke2 run_away with Dissolve(.25)
    pause 0.1
    show isabelle door_peek stroke1 run_away with Dissolve(.25)
    show isabelle door_peek cum run_away with hpunch
    pause 0.25
  $unlock_replay("isabelle_revenge")
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 0.5
  hide isabelle
  hide black onlayer screens
  with Dissolve(.5)
  if quest.isabelle_gesture["masturbated"]:
    show kate sad at appear_from_left
    pause 0.5
    window auto show
    show kate surprised with dissolve2
    kate surprised "What the fuck?"
    kate surprised "What are you doing, you creep?!"
    mc "I, err... I..."
    kate embarrassed "Oh, my god!"
    window hide
    show kate embarrassed at disappear_to_right(.5)
    pause 0.5
    window auto
    "Seemingly too flustered to react further, she runs for it."
    "And I'm left standing there, shaking as I come down."
    "Feeling guilty and even more confused than before."
  else:
    show kate sad:
      xanchor 1.0 xpos 0.0 yalign 1.0
      linear 1.0 xanchor 0.0 xpos 1.0
    pause 1.0
    window auto
  "I can't believe what I just witnessed..."
  window hide
  show isabelle annoyed at appear_from_left(delay=1.5)
  pause 0.5
  window auto
  isabelle annoyed "Do you understand how it feels now?"
  if isabelle.love >= 10:
    "It hurts so bad..."
  else:
    "It was hot as hell, honestly..."
  mc "Err, yes... I get it now."
  isabelle sad "I'm sorry I had to do that to you."
  mc "I guess I deserved it."
  isabelle sad "Yes, you did. But still, I don't like who I was in that classroom."
  mc "I'm sorry for putting you through it, and for putting us through it."
  isabelle sad "Thank you. I appreciate the apology."
  "There's genuine hurt in her eyes."
  if isabelle.love >= 10:
    "And I guess she can see it in mine, too."
    "This was so stupid..."
  isabelle sad "I guess we'll have to try to move past it now."
  isabelle sad "It might take time."
  mc "I'm willing to try if you are."
  isabelle subby "That's all we can ever do."
  window hide
  show isabelle subby at disappear_to_right
  $isabelle["romance_disabled"] = False
  $game.notify_modal(None,"Love or Lust","Romance with [isabelle]:\nEnabled.",wait=5.0)
  pause 0.25
  window auto
  "I'm really sorry, [isabelle]..."
  if isabelle.love >= 10:
    "...more than you'll ever know."
  window hide
  if quest.kate_moment == "coming_soon":
    $game.quest_guide = "kate_moment"
  $quest.isabelle_gesture.finish()
  if game.hour == 19:
    pause 0.25
    window auto
    "It's getting late. I can't wait to get home."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
  if quest.kate_moment == "coming_soon":
    pause 0.25
    window auto
    "..."
    "Crap. I totally forgot to tell [isabelle] about [kate]'s weekly doctor's appointment, didn't I?"
    "I better send her a text before I end up forgetting again."
    $quest.kate_moment.advance("text")
  return
