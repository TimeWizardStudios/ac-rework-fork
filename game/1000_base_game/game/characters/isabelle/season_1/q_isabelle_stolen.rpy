label isabelle_quest_stolen_start:
  show isabelle angry with Dissolve (.5)
  isabelle angry "Can you bloody believe it?!"
  isabelle angry "Someone broke into my locker!"
  show isabelle angry at move_to(.25)
  menu(side="right"):
    extend ""
    "?quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed@|{image=kate contact_icon}|\"Was anything stolen?\"":
      show isabelle angry at move_to(.5)
      mc "Was anything stolen?"
      isabelle annoyed "Well, yes!"
      mc "What was it?"
      isabelle concerned_left "That's unimportant."
      isabelle skeptical "What matters is finding and bringing the culprit to justice!"
      "That's an odd stance to take... you'd think that finding the stolen item would be her number one priority."
      mc "So, what are you going to do about it?"
      isabelle thinking "First we need suspects."
      mc "We?"
      isabelle cringe "An injustice has been committed. Are you just planning to watch?"
      mc "I mean, that was the first thought that popped into my head..."
      isabelle thinking "Well, I'm going to do some investigating at least."
      mc "Do you even know where to start?"
      isabelle cringe "Wouldn't surprise me if [kate]'s behind it."
      mc "Really? When was the thing stolen?"
      isabelle thinking "Hmm... I reckon it was this morning during my time in the gym."
      mc "I was with [kate] then, so it definitely wasn't her."
      "It feels a bit wrong to lie, but her throwing baseless accusations around is just as bad."
      isabelle annoyed "Damn, that's annoying... it totally felt like something she'd do."
      mc "I don't know. [kate] is a lot of things... but a thief? Doesn't really sound like her."
      isabelle annoyed "What do you mean?"
      mc "She's pretty loaded. What would she get out of stealing?"
      isabelle eyeroll "Are you serious? It's obviously just a bullying tactic."
      mc "Hey, no need to be rude."
      isabelle annoyed "If you're being willfully ignorant of the motives, it's your own fault."
      isabelle annoyed_left "I'm going to have a word with her."
      hide isabelle with Dissolve(.5)
      "Ugh, this again? Why can't she just leave things be?"
      "[kate] would probably appreciate it if I gave her a heads up."
      $quest.isabelle_stolen.start("warnkate")
    "\"Have you talked to the [guard]?\"":
      show isabelle angry at move_to(.5)
      mc "Have you talked to the [guard]?"
      isabelle eyeroll "He told me he couldn't do anything about it."
      mc "That sounds odd... how come?"
      "Knowing him, probably laziness."
      isabelle annoyed "Apparently, if the locker itself isn't broken, you have to take it up with the police."
      isabelle annoyed_left "He doesn't do investigations."
      mc "If the lock wasn't broken, are you sure you didn't just misplace it?"
      isabelle angry "That's exactly what the [guard] said! And the answer is no. I wouldn't lose something like this."
      mc "Well, someone must've seen something..."
      show isabelle angry at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Maybe we can ask [maxine]? She's got this school under a microscope.\"":
          show isabelle angry at move_to(.5)
          mc "Maybe we can ask [maxine]? She's got this school under a microscope."
          jump isabelle_quest_stolen_start_maxine
        "\"I'll do some snooping. You stay put.\"":
          show isabelle angry at move_to(.5)
          mc "I'll do some snooping. You stay put."
          isabelle thinking "I mean, that's nice and all, but I'd like to get involved..."
          isabelle thinking "I'm the victim of this theft."
          mc "That's the point — you asking around might not work."
          isabelle cringe "So, I'm supposed to just sit here?"
          show isabelle cringe at move_to(.25)
          menu(side="right"):
            extend ""
            "\"Yes, sit pretty like a lady for once and let me do the work.\"":
              show isabelle cringe at move_to(.5)
              mc "Yes, sit pretty like a lady for once and let me do the work."
              $isabelle.love-=1
              $isabelle.lust-=1
              isabelle angry "That's the most repulsive thing I've heard all week!"
              isabelle angry "I don't need your help with this. Good day."
              hide isabelle with Dissolve(.5)
              "Welp, what a bitch. Although, I am a little curious about the theft now..."
              "Maybe I'll do some investigating on my own."
              "The logical first step is obviously talking to the [guard]."
              "It doesn't matter if [isabelle] already interrogated him in her preliminary investigation."
              "After that, you start from scratch. Treat everyone as a liar."
              $quest.isabelle_stolen.start("noluck")
            "\"No one ever notices me... I'm the perfect spy.\"":
              show isabelle cringe at move_to(.5)
              mc "No one ever notices me... I'm the perfect spy."
              isabelle afraid "Don't say that!"
              mc "Well, it's true."
              isabelle cringe "It's not! Else I wouldn't be talking to you now."
              mc "Well, you're different."
              isabelle thinking "Different?"
              mc "In a good way... you stand out from the crowd."
              $isabelle.love+=1
              isabelle flirty "Thanks, I think?"
              "That felt clumsy, but [isabelle] seemed to like it nonetheless."
              isabelle flirty "I guess I can let you do some snooping for me..."
              mc "Okay, starting by asking [maxine] is our best bet. She even calls her cat \"Detective [spinach].\""
              jump isabelle_quest_stolen_start_maxine
    "?quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed@|{image=isabelle contact_icon}|\"Do you think this is retaliation?\"":
      show isabelle angry at move_to(.5)
      mc "Do you think this is retaliation?"
      isabelle skeptical "From [kate]?"
      mc "Wouldn't surprise me..."
      isabelle skeptical "Would she really stoop that low?"
      mc "You have no idea."
      isabelle concerned "You're right, I don't."
      isabelle concerned_left "...but she does seem like a good suspect."
      isabelle concerned_left "Someone must've seen her fiddle with my locker."
      isabelle concerned "It's not like she goes unnoticed... the spotlight always seems to fall on her."
      show isabelle concerned at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Maybe we can ask [maxine]? She's got this school under a microscope.\"":
          show isabelle concerned at move_to(.5)
          mc "Maybe we can ask [maxine]? She's got this school under a microscope."
          jump isabelle_quest_stolen_start_maxine
        "?mc.charisma>=3@[mc.charisma]/3|{image=stats cha}|\"Someone once told me you need to nip bad behaviors in the bud and confront your enemies.\"":
          show isabelle concerned at move_to(.5)
          mc "Someone once told me you need to nip bad behaviors in the bud and confront your enemies."
          $isabelle.lust+=1
          isabelle flirty "That someone seems like quite the character."
          mc "She's all sorts of fierce."
          isabelle blush "Oh, man..."
          "[isabelle]'s cheeks bloom into a garden of bashful roses."
          "Most people find my compliments creepy, but she seems to take them well."
          "Maybe it's the clean slate with her that does the trick?"
          "Either way, it's addicting."
          mc "I do think we'll have to be smart about this, though."
          mc "If we want to really stick it to her, we need to find proof."
          isabelle laughing "You're probably right. I would've just marched up to her and—"
          mc "Right, but with some finesse, we can really stick it to her this time."
          isabelle confident "Okay, okay... what do we do then?"
          mc "If [kate]'s behind this, odds are she didn't do it herself."
          isabelle thinking "What do you mean?"
          mc "I mean that it's more likely that one of her cronies did it."
          isabelle cringe "She seems fairly hands-on to me..."
          mc "She can be, if there's any fun involved for her. Stealing something is different."
          isabelle angry "I can't believe she'd do that!"
          mc "You need to believe it. She's going to take you down if you're not careful."
          isabelle sad "You're right, but where do we even start?"
          mc "Hmm..."
          mc "I think we'd cover more ground if we split up and did an investigation of our own."
          mc "Then, we can meet up and compare our results."
          isabelle blush "Sounds like a plan!"
          "Hmm... I'm still going to start by eavesdropping outside the locker room."
          "[kate]'s cronies are mostly cheerleaders and jocks, so that's the logical first step."
          "[isabelle] doesn't need to know about my crooked cop tactics, though."
          if not quest.isabelle_stolen["privacy_seen"]:
           call isabelle_quest_stolen_start_isa
          $quest.isabelle_stolen.start("eavesdrop")
  return

label isabelle_quest_stolen_start_maxine:
  isabelle skeptical "Not really sure what I think about her and her methods..."
  mc "What's the worst thing that could happen?"
  isabelle confident "Oh, I don't know... a zombie apocalypse?"
  mc "That is certainly in the cards when dealing with [maxine]."
  isabelle laughing "Fine, let's try that!"
  isabelle eyeroll "Anything beats getting the police involved."
  mc "You don't like cops?"
  isabelle angry "Hate them! They're minions of the ruling class. Law and order is a lie... there's no justice for the rich."
  mc "Hm... okay. You sound a bit like a Reddit incarnation..."
  isabelle skeptical "What's \"Reddit?\""
  mc "Uh, nothing."
  mc "Anyway, now that I think of it, asking [maxine] might not be so easy right now."
  isabelle concerned "What do you mean?"
  mc "I just remembered that she's super busy this time of the year. At least, that's what she says..."
  isabelle concerned "Well, how do we get a hold of her?"
  mc "Probably need to write a letter and put it in her personal mailbox."
  isabelle excited "She has a personal mailbox?"
  mc "Well... it's not exactly a mailbox."
  mc "It's more of a plant."
  isabelle concerned "What?"
  mc "It's complicated. Let me take care of it."
  "For some reason, [maxine] started keeping that plant under close observation during our junior year."
  "A letter there will reach her faster than [maya] rejects boys."
  isabelle excited "Okay! You know everyone's quirks here better than I do."
  isabelle neutral "Here's a pen to write the letter with, but I don't have any paper on me..."
  $mc.add_item("pen")
  "Okay, I need to find a piece of paper to write on and then put the letter in the admin wing plant. Shouldn't be too hard..."
  if not quest.isabelle_stolen["privacy_seen"]:
    call isabelle_quest_stolen_start_isa
  if not quest.isabelle_stolen.in_progress:
    $quest.isabelle_stolen.start("askmaxine")
  else:
    $quest.isabelle_stolen.advance("askmaxine")
  return

label isabelle_quest_stolen_start_isa:
  $quest.isabelle_stolen["privacy_seen"] = True
  mc "What was stolen, anyway?"
  isabelle concerned_left "I'd rather not say."
  mc "Mhm..."
  isabelle concerned "What?"
  mc "Nothing... I guess I'll respect your privacy just this once."
  isabelle excited "Thank you."
  hide isabelle excited with Dissolve(.5)
  return

label isabelle_quest_stolen_guard:
  mc "Excuse me..."
  if quest.isabelle_stolen["guard_first_talk"]:
    guard "You again?"
  else:
    guard "Can't you see I'm on my break right now?"
    mc "Actually, your break ended twelve minutes ago."
    mc "I memorized your schedule from the board in the admin wing."
    guard "Well... I..."
    guard "Fine. What do you want, kid?"
    mc "Did you happen to see anything suspicious going on by the lockers earlier today?"
    guard "Ah, I see what this is."
  guard "Like I told the young lady earlier, I would've noticed a break-in... now scram!"
  "Ugh, that guy is impossible to deal with. What an asshole!"
  "If the law won't help you, what other choice do you have than breaking it?"
  "If I could somehow access the surveillance footage from the entrance hall..."
  "Need to somehow get him out of that booth for that, though."
  "Luckily, there's one thing that always gets him drooling — doughnuts."
  $quest.isabelle_stolen.advance("donuts")
  return

label isabelle_quest_stolen_noluck:
  mc "Hello?"
  guard "Can't you see I'm on my break now? What does it look like I'm eating?"
  mc "I thought that was a regular doughnut and not a break-doughnut... my bad."
  guard "Are you trying to be a smartass with me?"
  mc "Of course not! Everyone knows there's a significant difference between doughnuts."
  guard "Hmm..."
  $mc.charisma+=1
  mc "Glazed. Sprinkled. Filled. Eaten as an appetizer. Eaten as a desert. Eaten as a meal. Break-doughnut, non-break-doughnut, half-break-doughnut."
  mc "The only thing they have in common is the shape!"
  guard "Fine, you know your doughnuts. What do you want?"
  mc "A lady that I know had something stolen from her locker..."
  guard "Was it the brunette in glasses? I already told her that she needs to go to the cops."
  mc "But—"
  guard "That's all. Now scram!"
  "This lazy old fuck..."
  "Well, it seems [isabelle] was right about the [guard]."
  "Damn it. I probably need to make up with her..."
  $quest.isabelle_stolen["guard_first_talk"] = True
  $quest.isabelle_stolen.advance("sorry")
  return

label isabelle_quest_stolen_sorry:
  show isabelle annoyed_left with Dissolve(.5)
  isabelle annoyed_left "I'm not talking to you."
  mc "I'm sorry for being a dick earlier..."
  mc "I fell back into my old habits."
  isabelle annoyed "You have a habit of being sexist and rude?"
  mc "Look, the point is I'm trying to change."
  isabelle annoyed "I'm starting to get really fed up with this place..."
  isabelle annoyed_left "Is common decency too much to ask for?"
  mc "It's not. I'm sorry... let me help you find the thief."
  isabelle annoyed "Fine, but don't be a dick."
  mc "I won't, I promise!"
  isabelle sad "Thanks... I've been struggling to come up with a plan."
  mc "Hmm... same. I thought talking to the [guard] would work, but he's insufferable."
  mc "Maybe we should check if [maxine] could point us in the right direction?"
  jump isabelle_quest_stolen_start_maxine

label isabelle_quest_stolen_cafeteria_donut:
  "This is where the [guard] goes to stock his doughnut supply."
  "He makes this short trip every hour or so..."
  "The problem is that his booth is right there, so he's never gone for more than a minute."
  "There's currently ten doughnuts available, which will last him the rest of the day."
  "However... if the cafeteria were to run out..."
  menu(side="middle"):
    extend ""
    "?mc.money>=20@[mc.money]/20|{image=ui hud icon_money}|Buy the doughnuts":
      $mc.money-=20
      $quest.isabelle_stolen.advance("nonuts")
      $mc.add_item("doughnut",10)
      "All mine. Let's starve the [guard]."
    "?mc.strength>=7@[mc.strength]/7|{image=stats str}|Steal the doughnuts":
      "The lunch ladies are all busy in the back."
      "So climbing over the counter and stuffing my backpack should be easy enough."
      $quest.isabelle_stolen.advance("nonuts")
      $mc.add_item("doughnut",10)
      if jacklyn.at("school_cafeteria") and quest.jacklyn_sweets < "oldpromises" and not quest.isabelle_stolen["help_with_the_project"]:
        "Let's just hope—"
        label quest_jacklyn_sweets_doughnuts_jacklyn:
        $quest.isabelle_stolen["hold_your_horse_dick"] = True
        show jacklyn angry at (appear_from_left if jacklyn.at("school_cafeteria") else appear_from_right)
        jacklyn angry "Hold your horse dick! What's going on here?"
        "Shit."
        show jacklyn angry at move_to((.25 if jacklyn.at("school_cafeteria") else .75))
        menu(side=("right" if jacklyn.at("school_cafeteria") else "left")):
          extend ""
          "\"It's not what it looks like...\"":
            $quest.isabelle_stolen["not_what_it_looks_like"] = True
            show jacklyn angry at move_to(.5)
            mc "It's not what it looks like..."
            jacklyn thinking "Looks like one punk is robbing the bakery."
            mc "Err... do you want one?"
            jacklyn smile "Trying to bribe an official?"
            jacklyn smile "Besides, you don't get this figure by dumpstering your diet."
            mc "[flora] told me you eat chips for breakfast."
            jacklyn excited "She did, did she?"
            jacklyn laughing "Did she also tell you what I use as a plate?"
            mc "..."
            jacklyn smile "Anyhow, I'm confiscating half your supply."
            $mc.remove_item("doughnut",5)
            jacklyn neutral "Oh, and I have to give you detention."
            $mc["detention"]+=1
            jacklyn angry "You're lucky my battery is dead, else I'd set the bulldog on your ass.{space=-5}"
            hide jacklyn with Dissolve(.5)
          "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"I'm doing an art project!\"":
            show jacklyn angry at move_to(.5)
            mc "I'm doing an art project!"
            jacklyn neutral "An art project?"
            mc "Yes, I'm doing a food nouveau photography thing for a local magazine!"
            jacklyn thinking "Truly?"
            mc "I don't joke about art."
            mc "But as many struggling artists before me, I couldn't really afford to pay for the supplies..."
            jacklyn laughing "Well, art's above all else... and that includes the law!"
            jacklyn excited "I'll pay for the doughnuts."
            mc "Hey, thanks! That's so nice."
            jacklyn excited "I got you, bro."
            show jacklyn excited at move_to((.75 if jacklyn.at("school_cafeteria") else .25))
            menu(side=("left" if jacklyn.at("school_cafeteria") else "right")):
              extend ""
              "\"So, I'm free to go?\"":
                show jacklyn excited at move_to(.5)
                mc "So, I'm free to go?"
                jacklyn laughing "Go! Be free, little bird!"
                jacklyn laughing "Let the wings of inspiration carry you to new artistic heights!"
                mc "Err... thanks... I'll do my best."
                hide jacklyn laughing with Dissolve (.5)
              "?jacklyn.lust>=3@[jacklyn.lust]/3|{image= jacklyn contact_icon}|{image=stats lust}|\"I was hoping you could help me with the project...\"":
                $quest.isabelle_stolen["help_with_the_project"] = True
                show jacklyn excited at move_to(.5)
                mc "I was hoping you could help me with the project..."
                jacklyn laughing "Yeah?"
                mc "I need a model, and you'd be perfect for it."
                jacklyn laughing "Well, stop by my classroom and let's give those pins a strike!"
                hide jacklyn laughing with Dissolve (.5)
                if quest.jacklyn_sweets == "doughnuts":
                  "All right! Got away with theft and got [jacklyn] into my fictional art project."
                  "Well done me!"
                else:
                  "All right! Got away with theft... screwed the [guard] over... and got [jacklyn] into my fictional art project."
                  "Well done me!"
                  $quest.jacklyn_sweets.start()
                  $quest.jacklyn_sweets.advance("oldpromises")
                  ## $game.notify_modal(None,"Coming soon","{image= jacklyn contact_icon}{space=25}|"+"King of Sweets is currently\nbeing worked on!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image= items monkey_wrench}",5.0)
      elif jo.at("school_cafeteria"):
        "Let's just hope—"
        label quest_jacklyn_sweets_doughnuts_jo:
        show jo embarrassed at appear_from_right
        jo embarrassed "What do you think you're doing?!"
        mc "Err..."
        jo annoyed "This is not the young man I know."
        jo annoyed "Stealing from the school kitchen?"
        if quest.jacklyn_sweets == "doughnuts":
          mc "You told me to get the guard something from the cafeteria!"
          jo concerned "But I didn't tell you to steal it, did I?"
          mc "I don't have a job! How am I supposed to afford overpriced doughnuts?{space=-70}"
          jo skeptical "You're a smart kid. You should be able to figure it out."
        else:
          mc "Look, I wasn't stealing... I just didn't have any money and the [guard] looked really hungry..."
          jo thinking "The [guard]? Come on now, who needs ten doughnuts?"
          mc "You don't know his appetite!"
          jo annoyed "..."
        mc "Fine, I'll put them back..."
        jo angry "No, you've already touched them! I'll just have to pay for them."
        jo annoyed "This means detention. I hope you understand that."
        $mc["detention"]+=1
        hide jo annoyed with Dissolve (.5)
        "Well, that could certainly have gone smoother..."
        "Detention again... that's just great."
      else:
        "Okay, got them all."
        "Now I just need to wait for the [guard] to realize."
    "Come back later":
      return
  if nurse["strike_book_activated"]:
    return

label isabelle_quest_stolen_cafeteria_donut_nurse:
  if nurse.at("school_cafeteria"):
    show nurse concerned at appear_from_left
  else:
    show nurse concerned at appear_from_right
  nurse concerned "Excuse me... I saw you bought all the doughnuts..."
  nurse concerned "I'm really craving something sweet right now."
  nurse blush "Would you mind selling me one?"
  show nurse blush at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Sorry, but I need these.\"":
      show nurse blush at move_to(.5)
      mc "Sorry, but I need these."
      if quest.isabelle_stolen["not_what_it_looks_like"]:
        nurse thinking "All five of them?"
      else:
        nurse thinking "All ten of them?"
      mc "Yes. I'm making a doughnut castle."
      nurse sad "Okay..."
      $nurse.love-=1
      nurse sad "Be careful not to overeat."
    "\"Sure, I don't need all of these!\"":
      show nurse blush at move_to (.5)
      mc "Sure, I don't need all of these!"
      nurse smile "Oh, great! Here's two dollars..."
      show nurse smile at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Thanks, I hope you like it!\"":
          show nurse smile at move_to(.5)
          $mc.money+=2
          $mc.remove_item("doughnut")
          mc "Thanks, I hope you like it!"
          $nurse.love+=1
          nurse smile "I think I will! It's probably not good for my figure, but sometimes you just have to indulge a little..."
          mc "I think your figure is fantastic. No worries there!"
          nurse blush "That is very kind of you! Thank you, [mc]."
        "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"Oh, they're more expensive than that...\"":
          show nurse smile at move_to(.5)
          mc "Oh, they're more expensive than that..."
          nurse annoyed "What do you mean?"
          mc "I mean that you're not getting them at retail price anymore."
          nurse neutral "Oh..."
          nurse neutral "How much are they now?"
          show nurse neutral at move_to(.25)
          menu(side="right"):
            extend ""
            "\"They're $10 each now.\"":
              show nurse neutral at move_to(.5)
              mc "They're $10 each now."
              nurse annoyed "That's a bit much, isn't it?"
              mc "I think it's a fair price for someone in your position."
              nurse surprised "M-my position?"
              mc "You know very well what I'm talking about... I still have that photo."
              $nurse.lust+=1
              nurse afraid "Okay, here's a ten!"
              $mc.money+=10
              $mc.remove_item("doughnut")
              mc "I think you should have a couple more!"
              nurse annoyed "I... I suppose I could..."
              nurse annoyed "Here you go."
              $mc.money+=20
              $mc.remove_item("doughnut",2)
              mc "Pleasure doing business with you!"
              nurse concerned "Likewise..."
            "?nurse.lust>=2@[nurse.lust]/2|{image= nurse contact_icon}|{image=stats lust}|\"I'm not looking for money.\"":
              show nurse neutral at move_to(.5)
              mc "I'm not looking for money."
              if nurse["strike_book"]:
                nurse annoyed "..."
                mc "You can trade the doughnut for a strike in your little book."
                nurse annoyed "Err... just keep the doughnut..."
                mc "Are you sure?"
                nurse sad "I'd really like that doughnut..."
                mc "Let's add one strike then."
                nurse blush "Okay. That's okay."
                mc "Here you go."
                $mc.remove_item("doughnut")
                $nurse["strike_book"]+=1
              else:
                nurse annoyed "What... err... what are you looking for?"
                mc "I think it's due time we start a tally."
                nurse concerned "A tally?"
                mc "Yeah, it'll be a little game between the two of us."
                mc "For example, this doughnut will add one strike to the tally."
                nurse thinking "What is the tally for?"
                mc "Basically, it's just so that we won't forget what you owe me."
                mc "We'll keep a notebook on your desk where we record the strikes."
                mc "Strikes can be traded in for... other things later on."
                nurse afraid "What sort of things...?"
                mc "That'd be up to me to decide, but I promise I'll be fair about it."
                nurse annoyed "I don't know about this..."
                mc "But I do."
                mc "So, unless you want that photo of yours landing in the principal's mailbox..."
                nurse surprised "Fine! I'll do it!"
                mc "Good. And the next time I come into your office, that notebook better be on the table for inspection."
                nurse afraid "It will!"
                mc "Enjoy your doughnut now."
                $mc.remove_item("doughnut")
                nurse annoyed "Thanks..."
                $nurse["strike_book_activated"] = True
                $nurse["strike_book"] = 1
  if nurse.at("school_cafeteria"):
    hide nurse with Dissolve(.5)
  else:
    hide nurse with moveoutright
  if quest.jacklyn_sweets == "doughnuts":
    $quest.jacklyn_sweets.advance("feed")
  return

label isabelle_quest_stolen_entrance_guard_nonuts:
  "There he goes... licking the sugar residue from his lips. It won't be long now."
  "The sweetness hits his tongue, lighting up the receptors in his brain."
  "Then comes the craving...  the need for more."
  "At first, he tries to suppress it. Surely, he can go a little longer?"
  "Then, the need hits him in full. It's like he's scripted like the Lady in Red from Matrix."
  "He clears his throat and gets up, straightening his uniform."
  "And just like that, he's heading for the cafeteria pastries."
  $quest.isabelle_stolen.advance("outnuts")
  return

label isabelle_quest_stolen_cafeteria_outnuts:
  #show guard (free roam sprite) by the pastries (he's angry af)
  "The [guard]'s hand is shaking, his face flushed red."
  "The sudden lack of doughnuts has sent his nervous system into overdrive."
  "Surely, a blood pressure that high is nothing to joke about."
  "The veins in his neck and forehead bulge like fire hoses, struggling to put out the flames in his eyes."
  guard "This fucking place..."
  "His only joy in life, stolen."
  menu(side="middle"):
    extend ""
    "Eat one of the doughnuts to savor his misery":
      $mc.remove_item("doughnut")
      $mc.lust+=1
      "Mmmm... these taste even better knowing he can't have any."
      "The glazed topping mixed with the strawberry jam. Heaven."
      "Oh, shit. Did his nose just twitch? Is he sniffing out my doughnut?"
      "Fevered desperation flickers through his eyes as he scans the room."
      "Nothing could've prepared him for my smiling face, mouth full of doughnut."
      "Maybe it's sorrow washing over him, maybe it's suppressed rage."
      "The tears in his eyes have never bothered me before, but this is heartbreaking."
      "It's the same kind of suffering I feel when watching girls in the gym, boobs bouncing and locks flowing in the wind."
      "To know such suffering is to..."
    "Gloating is how you end up in a bodybag":
      "Sweat sloshes down the furrows of the [guard]'s forehead."
      "This is his worst day since the cancellation of Baywatch..."
      "The worst day since he lost his job at the video game store and was forced to watch noisy high schoolers for a living."
      "The worst day since Vietnam."
      "Anger and soul-crushing pain — distilled disappointment."
      $mc.love+=1
      "I give the poor man a commiserating look. The only thing appropriate for the downfall of man."
      "There are few men who can withstand such an overload of emotions, but the world keeps turning and there's a doughnut shop across the street."
      "With the embers of hatred still burning in his eyes, he drags his feet and disappears into the wilderness."
  $guard["hidden_now"] = True
  $process_event("update_state")
  "Well, he left. Okay, time to put my plan into action. {i}Operation Sweet Tooth [guard]'s Booth.{/}"
  "Also, I should probably stop watching Matrix reruns."
  $quest.isabelle_stolen.advance("breakin")
  return

label isabelle_quest_stolen_dilemma:
  show isabelle neutral with Dissolve (.5)
  isabelle neutral "Hey, how's the investigation going? Any new leads?"
  "While I know that [lindsey] is the thief, I feel like there might be more to the story."
  "It's hard to believe that someone like her would do this..."
  "On the other hand, the surveillance footage doesn't lie."
  show isabelle neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I've finished my investigation.\"":
      show isabelle neutral at move_to(.5)
      mc "I've finished my investigation."
      isabelle skeptical "Really?"
      isabelle skeptical "Go on then, tell me who the thief is!"
      "This is pretty tough... [lindsey]'s whole running career could potentially be ruined, but she did steal that thing, in spite of everything."
      "Saying it was [kate] could probably work without evidence, but what would the consequences of that be?"
      "[isabelle]'s going to go after someone hard. She has that fire in her eyes..."
      show isabelle skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "\"[kate] did it.\"":
          show isabelle skeptical at move_to(.5)
          mc "[kate] did it."
          isabelle angry "I bloody well knew it!"
          isabelle angry "I'm going to kill her."
          "That didn't take much convincing..."
          mc "Calm down. We need a plan."
          isabelle angry "Don't tell me to calm down!"
          mc "One simply doesn't attack [kate] without a plan... there's always jocks around who will kick your ass if you get into a fight."
          mc "Be smart about this."
          isabelle skeptical "Whose side are you on?"
          mc "Listen to yourself for a moment — you're ready to fight the most popular girl in the school."
          mc "What did she even steal from you?"
          isabelle concerned_left "It doesn't matter..."
          mc "I think it does. You're usually the most sensible person in the room, but this thing has got you fired up like nothing else."
          isabelle concerned "Okay, fine... it's a box of chocolate hearts."
          mc "What now?"
          isabelle sad "It's complicated..."
          mc "We've got time."
          isabelle sad "They're my sister's favorite."
          mc "I didn't know you have a sister."
          isabelle sad "It's... {i}had{/}."
          mc "What?"
          isabelle sad "I {i}had{/} a sister."
          mc "Oh, I'm so sorry..."
          isabelle sad "It's her birthday tomorrow, and I always used to give her a box of chocolate hearts..."
          isabelle sad "We used to eat them together."
          mc "Damn..."
          "That explains [isabelle]'s reaction. I can't imagine what that must feel like."
          isabelle annoyed "It's... let's just talk about how we destroy [kate]."
          mc "I think we're going to have to do a lot of planning."
          mc "Let's get a nice cold bottle of ice tea in the cafeteria and think this through."
          isabelle concerned "You're probably right..."
          isabelle neutral "Thanks for calming me down."
          hide isabelle with Dissolve(.5)
          $quest.isabelle_stolen["accused"] = "kate"
          $quest.isabelle_stolen.finish()
        "\"[lindsey] did it.\"":
          show isabelle skeptical at move_to(.5)
          mc "[lindsey] did it."
          isabelle concerned_left "[lindsey]...? Are you certain?"
          mc "Yeah, I watched the surveillance footage..."
          mc "I saved the recording of it. I'll send it over."
          isabelle thinking "..."
          isabelle thinking "..."
          isabelle afraid "I can't believe it!"
          mc "It's weird, but there you have it..."
          isabelle annoyed_left "That little bitch."
          mc "Are you going to the police?"
          isabelle annoyed "Absolutely not. This is personal."
          isabelle angry "I'm going to find her."
          show isabelle angry at disappear_to_left
          "Oh, shit... [isabelle] had murder in her eyes. Better go after her to find [lindsey]!"
          $quest.isabelle_stolen["accused"] = "lindsey"
          $quest.isabelle_stolen.advance("slap")
        "?quest.isabelle_stolen['lindsey_talk']@|{image=lindsey contact_icon}|\"It's complicated...\"":
          show isabelle skeptical at move_to(.5)
          mc "It's complicated..."
          isabelle concerned_left "Complicated how?"
          mc "See, I have evidence of [lindsey] stealing your chocolate hearts..."
          mc "But I spoke to her, and she claims to not remember doing it."
          mc "And somehow, in spite of all the evidence—"
          isabelle annoyed "You believe her, don't you?"
          isabelle annoyed "This is how bullies and cheaters get away with it!"
          isabelle annoyed "They lie and steal and exploit the goodwill of nice people!"
          isabelle annoyed_left "And in spite of the bloody evidence, those same nice people make excuses for them!"
          "[isabelle] does have a point..."
          "This is exactly what someone like [kate] would do... but is [lindsey] really like her?"
          isabelle angry "Well, it ends now."
          show isabelle angry at disappear_to_left
          "She stomped off, probably in search of [lindsey]."
          "Better follow her so things don't get out of hand."
          $quest.isabelle_stolen["accused"] = "lindsey"
          $quest.isabelle_stolen.advance("slap")
    "\"I'm still investigating...\"":
      show isabelle neutral at move_to(.5)
      mc "I'm still investigating..."
      isabelle concerned "Man, I hope we can figure this out soon."
      hide isabelle with Dissolve (.5)
  return

label isabelle_quest_stolen_lindsey_dilemma:
  show lindsey thinking at appear_from_right(1.0)
  show lindsey thinking at move_to(0.0)
  pause(1)
  show lindsey thinking flip at move_to(1.0)
  pause(1)
  show lindsey thinking at move_to(0.0)
  pause(1)
  show lindsey thinking flip at move_to(1.0)
  pause(1)
  show lindsey thinking at move_to(.5)
  pause(1)
  $quest.isabelle_stolen["lindsey_talk"] = True
  "She's looking nervous. Perhaps a guilty conscience eating away at her?"
  mc "Hi, thief."
  lindsey skeptical "W-what?"
  mc "The surveillance footage doesn't lie."
  lindsey laughing "You have a weird sense of humor, [mc]!"
  mc "Listen, I saw you breaking into [isabelle]'s locker."
  lindsey skeptical "What are you talking about?"
  if lindsey['romance_disabled']:
    $lindsey.love-=3
    lindsey angry "Why do you keep harassing me?"
    lindsey angry "I've never done or said anything bad to you!"
    "Of course, she's going to make this about her. Play the victim."
    "Typical entitled bitch behavior..."
    "She's had it so easy her entire life. As soon as someone even questions her, the tears start flowing."
    "It's pathetic, really."
    mc "You might have everyone else fooled, but I know that nice-girl exterior is only a facade."
    mc "You walk around the school without a care in the world. You think the world is yours."
    mc "But I've got you this time."
  mc "You can deny it all you want."
  lindsey cringe "I genuinely don't know what you're talking about..."
  mc "So, what can you tell me about the clip I have here on my phone?"
  lindsey thinking "..."
  lindsey thinking "What is this?"
  mc "Keep watching."
  lindsey thinking "..."
  lindsey afraid "What the hell?!"
  lindsey afraid "Is this a joke?!"
  mc "You tell me."
  "Either [lindsey] is a really good actress, or she's as surprised as I was seeing her break into that locker."
  lindsey cringe "This is so weird! I have no memory of doing that..."
  mc "So, if we look  through your bag, there won't be any stolen goods in it?"
  lindsey afraid "Oh my god!"
  mc "What?"
  lindsey afraid "It all makes sense now!"
  lindsey sad "So, I discovered this box of chocolate hearts in my bag the other day..."
  lindsey sad "I had no idea how they got there, but I figured it was a gift from a fan or something. People sometimes leave flowers and stuff next to your bag during races."
  mc "How convenient."
  lindsey cringe "I swear I have no memory of even going to her locker!"
  show lindsey cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"That's very hard to believe...\"":
      show lindsey cringe at move_to(.5)
      mc "That's very hard to believe..."
      $lindsey.love-=1
      lindsey afraid "It's the truth!"
      mc "There's actual video evidence saying otherwise."
      lindsey cringe "Fans leave stuff by my bag sometimes... I thought this was a gift..."
      lindsey cringe "I shared the chocolate hearts with my teammates..."
      mc "Good luck explaining that."
      "It's hard to tell if she's lying. She's got tears in her eyes and seems upset about this."
      "But let's be honest, the camera doesn't lie."
      "Might as well report back to [isabelle]... [lindsey]'s probably just making stuff up at this point."
    "?not lindsey['romance_disabled']@|{image=stats love_2}|{image=lindsey contact_icon}|\"If you give the hearts back to [isabelle], she might forgive you.\"":
      show lindsey cringe at move_to(.5)
      mc "If you give the hearts back to [isabelle], she might forgive you."
      mc "She's a nice person."
      lindsey sad "I wish it were that easy..."
      mc "Hmm?"
      lindsey eyeroll "Well, since I thought it was from a fan, I gave a heart to each person on the sprint team..."
      lindsey sad "Oh my god... I can't believe this!"
      lindsey sad "This is horrible!"
      "[lindsey] does seem upset, but there's no real explanation for the security footage."
      "Fact is, she broke into [isabelle]'s locker."
      "Not really sure what to make of it..."
      "The only thing I can think of is some kind of screw up in the timelines."
      "I did travel back in time... who knows what's been impacted by that?"
      "The simplest answer is that she's lying, but it's all so bizarre."
      mc "I think you need to come clean to [isabelle]."
      lindsey cringe "What do I even say to her?"
      mc "I don't know... that's pretty rough considering you ate them all."
      mc "Maybe I can talk to her?"
      lindsey blush "You would do that for me?"
      mc "Well, no promises it'll help..."
      if not quest.isabelle_stolen["lindsey_forgive_option"]:
        $lindsey.lust+=1
        $lindsey.love+=1
        $quest.isabelle_stolen["lindsey_forgive_option"] = True
      lindsey blush "Thank you so much!"
  hide lindsey with Dissolve(.5)
  return

label isabelle_quest_stolen_slap:
  show lindsey thinking at Position(xalign=.25)
  show isabelle skeptical at Position(xalign=.75)
  with Dissolve(.5)
  isabelle skeptical "There you are... you rotten thief."
  lindsey thinking flip "Huh?"
  show isabelle skeptical at move_to(.4)
  #SFX: Slap sound  TBD
  pause(.4)
  show white with Dissolve(.1)
  show lindsey afraid_slap with hpunch
  show isabelle skeptical at move_to(.5)
  hide white with Dissolve(.1)
  "Whoa."
  "The sound of that slap rang out into the entire school."
  "It feels as if time has stopped completely..."
  "Who knew [isabelle] could move with such speed?"
  "That was really hard too..."
  "Under that mild-mannered exterior hides some true viciousness."
  "Never in a million years did I think she'd actually slap her."
  "Poor [lindsey]... that'll leave a red handprint on her cheek."
  show isabelle skeptical at move_to(.75)
  lindsey afraid_slap "Owww!"
  isabelle "That's what you get for stealing!"
  if quest.isabelle_stolen["lindsey_talk"]:
    lindsey sad_slap flip "I'm really sorry!"
    isabelle annoyed_left "You should be, you twat!"
    isabelle annoyed_left "Selfish, pitiful slag..."
    "[lindsey] looks destroyed..."
    "I should probably have tried to talk some sense into [isabelle]... never thought she'd get physical."
    "[lindsey] probably feels like she deserves it on some level, but there's still confusion lingering in her eyes."
    "It's one of those things that is so hard to fake."
    "She's definitely feeling guilty for distributing the chocolate hearts, but if you're to believe her story, she couldn't have known any better."
    lindsey concerned_slap flip "I swear I don't remember the stuff on that video happening!"
    lindsey concerned_slap flip "Please, let me buy you a new box!"
    isabelle annoyed "It was imported from Switzerland... a new box won't get here in time. Forget it."
    lindsey sad_slap flip "I'm just going to go... I'm really sorry this happened..."
    isabelle angry "I don't care if you're sorry. Just piss off already."
  else:
    lindsey skeptical_slap flip "Whaat?"
    isabelle skeptical "Don't try to act innocent."
    lindsey skeptical_slap flip "W-why did you hit me?"
    isabelle annoyed_left "Because you stole my box of chocolate hearts, you pitiful slag!"
    lindsey sad_slap flip "I didn't steal anything..."
    isabelle annoyed_left "Yes, you did! I saw the surveillance footage!"
    lindsey sad_slap flip "I'm so confused..."
    lindsey concerned_slap flip "I mean, I came across a box... but I certainly didn't steal it... I don't know what evidence—"
    isabelle angry "Oh, bollocks! Is this not you right here in this clip?"
    lindsey concerned_slap flip "..."
    lindsey afraid_slap flip "What?! How?! That... I don't have any memory of that!"
    isabelle angry "I don't want to hear it. Just bugger off."
    lindsey sad_slap flip "I'm sorry... I'm so confused..."
  show lindsey sad_slap flip at disappear_to_left
  show isabelle angry at move_to(.5,1)
  pause(1)
  mc "Those chocolate hearts must've been more than just candy, huh?"
  isabelle sad "They're my sister's favorite."
  mc "I didn't know you have a sister."
  isabelle sad "It's... {i}had{/}."
  mc "What?"
  isabelle sad "I {i}had a sister{/}."
  mc "Oh, I'm so sorry..."
  isabelle sad "It's her birthday tomorrow, and I always used to give her a box of chocolate hearts..."
  isabelle sad "We used to eat them together."
  mc "Damn..."
  "That explains [isabelle]'s reaction. I can't imagine what that must feel like."
  "Part of me wants to know how she died, but it's probably not a good time."
  "It's probably best to leave it be for now."
  "This is not how I expected this to turn out..."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_stolen["lindsey_slaped"] = True
  $quest.isabelle_stolen.finish()
  return

label isabelle_quest_stolen_warnkate:
  show kate eyeroll with Dissolve(.5)
  kate eyeroll "You can't seem to get enough of me, can you?"
  kate skeptical "What is it this time?"
  show kate skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You look lovely today, ma'am!\"":
      show kate skeptical at move_to(.5)
      mc "You look lovely today, ma'am!"
      $kate.lust+=1
      kate excited "I look lovely every day... what else?"
      mc "Your hair is like a river of honey!"
      kate laughing "You're funny. Keep going."
      mc "Your eyes sparkle like blood diamonds!"
      kate neutral "Okay, enough with the flattery."
      kate neutral "I know you want something."
      jump isabelle_quest_stolen_warnkate_warn
    "?mc.random_consumable()@|{image=items random_consumables_1}|{image=ui random}|{image=items random_consumables_2}|\"I bring tributes.\"":
      $ritem = items_by_id[mc.random_consumable()]
      show kate skeptical at move_to(.5)
      mc "I bring tributes."
      kate excited "As you should."
      mc "Please enjoy this humble tribute — my favorite [ritem.title_lower]."
      $game.notify_modal(None,"Inventory","{image="+find_item(ritem).icon+"}{space=50}|"+"Lost\n{color=#900}"+str(find_item(ritem))+"{/color}",5.0)
      kate confident "Oh, that's perfect!"
      kate confident "Here's what we'll do with that..."
      show expression find_item(ritem).icon at toss_items(1.0) behind kate as itemone
      pause(1)
      $mc.remove_item(ritem,silent=True)
      "Ugh..."
      kate laughing "Fun, right?"
      show kate laughing at move_to(.75)
      menu(side="left"):
        extend ""
        "?mc.random_consumable()@|{image=items random_consumables_1}|{image=ui random}|{image=items random_consumables_2}|\"Would you like something else?\"":
          $ritem = items_by_id[mc.random_consumable()]
          show kate laughing at move_to(.5)
          mc "Would you like something else?"
          kate smile "You know what? I think I do."
          mc "Okay..."
          mc "This is [flora]'s [ritem.title_lower]. She sat on it once during dinner... it still smells like her."
          $game.notify_modal(None,"Inventory","{image="+find_item(ritem).icon+"}{space=50}|"+"Lost\n{color=#900}"+str(find_item(ritem))+"{/color}",5.0)
          kate laughing "Seriously?"
          mc "I hope you enjoy it."
          kate confident "That's great. I'll just put it right here."
          show expression find_item(ritem).icon at toss_items_at_floor_roll(.95) behind kate as itemtwo
          pause(1)
          $mc.remove_item(ritem,silent=True)
          "Damn... I really treasured that [ritem.title_lower]."
          show kate confident at move_to(.25)
          menu(side="right"):
            extend ""
            "?mc.random_consumable()@|{image=items random_consumables_1}|{image=ui random}|{image=items random_consumables_2}|\"More?\"":
              $ritem = items_by_id[mc.random_consumable()]
              show kate confident at move_to(.5)
              mc "More?"
              kate smile "Yes!"
              mc "This [ritem.title_lower] is the only thing my dad left behind..."
              mc "Here you go."
              $game.notify_modal(None,"Inventory","{image="+find_item(ritem).icon+"}{space=50}|"+"Lost\n{color=#900}"+str(find_item(ritem))+"{/color}",5.0)
              kate laughing "Poof!"
              show expression find_item(ritem).icon at toss_items_at_wall(.9) behind kate as itemthree
              pause(1)
              $mc.remove_item(ritem,silent=True)
              "My life savings..."
              show kate laughing at move_to(.75)
              menu(side="left"):
                extend ""
                "?mc.random_consumable()@|{image=items random_consumables_1}|{image=ui random}|{image=items random_consumables_2}|\"More?\"":
                  $ritem = items_by_id[mc.random_consumable()]
                  show kate laughing at move_to(.5)
                  mc "More?"
                  kate smile "Yep!"
                  mc "My dead grandmother's finest [ritem.title_lower]."
                  $game.notify_modal(None,"Inventory","{image="+find_item(ritem).icon+"}{space=50}|"+"Lost\n{color=#900}"+str(find_item(ritem))+"{/color}",5.0)
                  kate laughing "Gone!"
                  show expression find_item(ritem).icon at toss_items(.85) behind kate as itemfour
                  pause(1)
                  $mc.remove_item(ritem,silent=True)
                  "Never going to see that again..."
                  show kate laughing at move_to(.25)
                  menu(side="right"):
                    extend ""
                    "?mc.random_consumable()@|{image=items random_consumables_1}|{image=ui random}|{image=items random_consumables_2}|\"More?\"":
                      $ritem = items_by_id[mc.random_consumable()]
                      show kate laughing at move_to(.5)
                      mc "More?"
                      kate smile "Def!"
                      mc "A family heirloom, passed down through generations. This [ritem.title_lower] is my entire legacy."
                      "My last possession with any sentimental value..."
                      $game.notify_modal(None,"Inventory","{image="+find_item(ritem).icon+"}{space=50}|"+"Lost\n{color=#900}"+str(find_item(ritem))+"{/color}",5.0)
                      $kate.lust+=1
                      kate flirty "That has been very enjoyable. You've entertained me well."
                      mc "I'm glad."
                      kate confident "Are you ready?"
                      mc "Yes, ma'am."
                      kate laughing "Whoosh!"
                      show expression find_item(ritem).icon at toss_items_at_wall(.8) behind kate as itemfive
                      pause(1)
                      $mc.remove_item(ritem,silent=True)
                      jump isabelle_quest_stolen_warnkate_joentrance
                    "\"I'm glad you enjoyed it.\"":
                      show kate laughing at move_to(.5)
                      mc "I'm glad you enjoyed it."
                      jump isabelle_quest_stolen_warnkate_nomore
                "\"I'm glad you enjoyed it.\"":
                  show kate laughing at move_to(.5)
                  mc "I'm glad you enjoyed it."
                  jump isabelle_quest_stolen_warnkate_nomore
            "\"I'm glad you enjoyed it.\"":
              show kate confident at move_to(.5)
              mc "I'm glad you enjoyed it."
              jump isabelle_quest_stolen_warnkate_nomore
        "\"I'm glad you enjoyed it.\"":
          show kate laughing at move_to(.5)
          mc "I'm glad you enjoyed it."
          jump isabelle_quest_stolen_warnkate_nomore
    "\"If you're civil about it, I might provide you with some information.\"":
      show kate skeptical at move_to(.5)
      mc "If you're civil about it, I might provide you with some information."
      kate confident "I'm nothing if not civil."
      $kate.lust-=1
      kate neutral "Now, stop wasting my time."
      show kate neutral at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Not good enough.\"":
          show kate neutral at move_to(.5)
          mc "Not good enough."
          "Going to [kate] was clearly a mistake."
          "She'll never learn to respect me if I keep bending to her will."
          mc "It really is a shame that you can't treat people well..."
          mc "Your loss."
          kate laughing "As if I need your help with anything."
          show kate laughing at disappear_to_right
          $mc.love+=1
          "I guess I'm on my own. At least I stood up for myself this time..."
          "Maybe I should just try to find proof of whoever did this and then sell it to the highest bidder?"
          "The best place to start is probably with the [guard]."
          "I really should start acting more in my own interest... it feels good to tell her to shove it."
          $quest.isabelle_stolen.advance("guard")
        "\"Be nice, or I'll slap you.\"":
          show kate neutral at move_to(.5)
          mc "Be nice, or I'll slap you."
          $kate.lust-=1
          kate gushing "With those little t-rex arms?"
          show kate gushing at bounce
          kate gushing "Rawr! Rawr!"
          kate laughing "You're the worst combination of ridiculous and pathetic."
          mc "Ugh..."
          "I really need to stop making empty threats. That just leads to mockery."
          kate confident "You really think I need your information?"
          kate confident "Out of my way, freak."
          show kate confident at disappear_to_right
          "Bitch."
          "Well, now I kinda want her to be the thief..."
          "I'm going to do some investigating on my own. Let's start by asking the [guard]."
          $quest.isabelle_stolen.advance("guard")
        "\"All right, listen closely.\"":
          show kate neutral at move_to(.5)
          mc "All right, listen closely."
          jump isabelle_quest_stolen_warnkate_warn
  return

label isabelle_quest_stolen_warnkate_warn:
  mc "[isabelle] thinks you stole something from her locker."
  kate laughing "What could I possibly want from her?"
  mc "That's what I told her, but she wouldn't listen."
  kate confident_right "She's proving to be quite the troublemaker..."
  kate confident "It's good that you're keeping tabs on her for me."
  mc "Of course, ma'am."
  kate thinking "Now, what to do about this?"
  kate thinking "While I'm obviously innocent in all of this, I don't like her poking around or getting her hands on surveillance footage."
  mc "How do we stop her?"
  kate skeptical "Do an investigation of your own. Destroy any evidence that might put me in a bad light."
  kate skeptical "My social image is important, so don't screw this up."
  mc "Where do I start?"
  kate eyeroll "I don't know. You figure it out."
  mc "Okay..."
  kate excited "Maybe there'll be a reward for you if you do well. Maybe not. I haven't decided yet."
  "Okay, a reward from [kate] is worth everything!"
  "My dick is getting hard just thinking about it..."
  mc "I'll do my best, ma'am!"
  kate skeptical "Yes, you will."
  hide itemone
  hide itemtwo
  hide itemthree
  hide itemfour
  hide itemfive
  with Dissolve(.5)
  show kate skeptical at disappear_to_right
  "All right, the best place to start is probably with the [guard]."
  $quest.isabelle_stolen.advance("guard")
  $quest.isabelle_stolen["warned_kate"] = True
  return

label isabelle_quest_stolen_warnkate_nomore:
  kate neutral "Did you run out of stuff to give me?"
  mc "Err... yes, I did. I'm sorry..."
  kate neutral "Next time, bring more."
  mc "I will! I promise!"
  kate confident "Well, then. You've held me up long enough."
  mc "..."
  kate neutral "What?"
  mc  "There was one more thing..."
  kate neutral "Tell me."
  jump isabelle_quest_stolen_warnkate_warn

label isabelle_quest_stolen_warnkate_joentrance:
  show kate laughing at move_to(.75)
  show jo concerned at appear_from_left(.25)
  jo concerned "What's going on here?"
  show kate surprised with Dissolve(.5)
  mc "Err..."
  jo concerned "Are those your things, [mc]?"
  show jo concerned at move_to("left")
  show kate surprised at move_to("right")
  menu(side="middle"):
    extend ""
    "\"No, [kate]'s littering.\"":
      show jo concerned at move_to(.25)
      show kate surprised at move_to(.75)
      mc "No, [kate]'s littering."
      $kate.lust-=3
      kate angry "What did you just say?!"
      jo thinking "[kate]?"
      kate sad "That is not what happened, Mrs. Principal..."
      jo annoyed "Are you lying to me, [mc]?"
      show jo annoyed at move_to("left")
      show kate sad at move_to(.5)
      show isabelle concerned_left at appear_from_right("right")
      isabelle concerned_left "Actually, that's exactly what happened."
      isabelle concerned_left "She was tossing stuff all over the place."
      jo thinking "Oh, dear... I'm sorry, [kate], but we have trash cans for a reason."
      jo thinking "I'm afraid I'll have to give you detention for this..."
      kate sad "Detention? I never get detention."
      jo sad "I know, but what example would that set for our new student here?"
      jo sad "Please, report to [mrsl] at the end of the day."
      jo sad "Oh, and pick up those things..."
      show jo sad at disappear_to_left
      show kate sad at move_to(.25,1)
      show isabelle concerned_left at move_to(.75,1)
      pause(1)
      kate angry "You little snake..."
      mc "Sorry, but you get what you give."
      $isabelle.lust+1
      isabelle excited "He's right, you know? Play stupid games, win stupid prizes."
      kate smile_right "You seem like an expert in that field."
      isabelle annoyed_left "At least I'm not a thief."
      "Whoops... I was supposed to warn [kate] about that."
      "Oh, well. She can probably fend for herself..."
      kate laughing "I don't know what crazy ideas are bouncing around in your little head, but that's just sad."
      isabelle annoyed_left "I know what you did. And you're probably used to getting away with stuff like that... but I'm going to put an end to that."
      kate eyeroll "Did what, exactly?"
      kate excited "Wait, did someone dog-ear the pages in your favorite smut novel?"
      isabelle angry "Argh!"
      show kate excited at move_to(.5,1)
      show isabelle angry at disappear_to_right
      pause(1)
      kate skeptical "What's up with her?"
    "\"Yeah, it's my stuff... [kate] was just telling me to pick up after myself.\"":
      show jo concerned at move_to(.25)
      show kate surprised at move_to(.75)
      mc "Yeah, it's my stuff... [kate] was just telling me to pick up after myself."
      $kate.lust+=1
      kate sad "Uh, right."
      jo sad "[mc]? This is a school, not a—"
      show jo sad at move_to("left")
      show kate sad at move_to(.5)
      show isabelle concerned_left at appear_from_right("right")
      isabelle concerned_left "Sorry for barging in."
      isabelle concerned_left "But that's not what happened."
      jo thinking "[isabelle]?"
      menu(side="middle"):
        extend ""
        "\"You just got here, [isabelle]. It really is my fault...\"":
          mc "You just got here, [isabelle]. It really is my fault..."
          $kate.lust+=1
          kate smile_right "Everyone knows I'm a model student. You must've been mistaken, [isabelle]."
          jo sad "Very well, [mc]. Report to [mrsl] for detention. You should know better..."
          $mc["detention"]+=1
          show jo sad at disappear_to_left
          show kate smile_right at move_to(.25,1)
          show isabelle concerned_left at move_to(.75,1)
          pause(1)
          $isabelle.lust-=1
          isabelle angry "Why did you do that, [mc]?!"
          mc "Do what?"
          isabelle annoyed_left "Seriously? I'm not bloody stupid. Why'd you take the fall for her?"
          kate flirty "Because he knows my time is valuable. He also knows he'll be rewarded later."
          isabelle annoyed_left "You're the worst."
          isabelle annoyed "And you... you're enabling her."
          kate neutral "Why don't you mind your own business, hm?"
          "These two are worse than repelling magnets... sparks seem to fly whenever they get near each other."
          isabelle annoyed_left "Says the girl who literally got her hands into mine."
          kate smile "Only in your lezzie fantasies, sweetheart."
          isabelle annoyed_left "I know what you did. And you're probably used to getting away with stuff like that... but I'm going to put an end to that."
          kate eyeroll "Did what, exactly?"
          kate excited "Wait, did someone dog-ear the pages in your favorite smut novel?"
          isabelle angry "Argh!"
          show kate excited at move_to(.5,1)
          show isabelle angry at disappear_to_right
          pause(1)
          kate skeptical "What's up with her?"
        "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"It's true... I was trying to cover for [isabelle]. She was actually the one littering.\"":
          mc "It's true... I was trying to cover for [isabelle]. She was actually the one littering."
          $isabelle.lust-=3
          isabelle afraid "What the hell?!"
          $kate.lust+=1
          kate confident "[mc] is right. [isabelle] probably doesn't know that we use trash cans here..."
          kate confident_right "She's from a different culture, so it's understandable."
          jo worried "I'm sorry, [isabelle], but rules are rules... you're new here, so just treat this as a learning experience."
          jo worried "Behavior like this won't be tolerated here. Please, report to [mrsl] for detention."
          show jo worried at disappear_to_left
          show kate confident_right at move_to(.25,1)
          show isabelle afraid at move_to(.75,1)
          pause(1)
          isabelle cringe "I can't believe you just did that, [mc]! What a truly disgusting thing to do..."
          isabelle cringe "Don't bother talking to me again."
          $isabelle["romance_disabled"] = True
          show kate confident_right at move_to(.5,1)
          show isabelle cringe at disappear_to_right
          $game.notify_modal(None,"Love or Lust","Romance with [isabelle]:\nDisabled.",wait=5.0)
          kate laughing "Now, you've done it."
          "[isabelle] seemed genuinely upset, which is fair..."
          "It was a dick-move, but that's what happens when you go up against [kate]."
          "I might still be able to patch things up with her by finding the thief... but do I really care?"
          mc "She definitely didn't like that."
          kate confident "But I did! I loved the look of surprise on her face."
          kate confident "You also got me out of trouble, and that's what matters."
          kate excited "I guess you're smarter than you look..."
          #$ kate.love 1/3 "Moment of Glory" unlocked
          mc "Thank you!"
          "A compliment from [kate]? I must be dreaming again."
          kate skeptical "What's up with her anyway?"
  jump isabelle_quest_stolen_warnkate_warn

#maxine
label isabelle_quest_stolen_askmaxine_lindsey:
  show lindsey smile with Dissolve(.5)
  mc "Hey, you don't happen to have a sheet of paper on you, do you?"
  lindsey smile "Sorry, I left my bag at the tracks..."
  lindsey flirty "But I actually saw [flora] write something down in the art classroom!"
  hide lindsey with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_isabelle:
  show isabelle neutral with Dissolve(.5)
  mc "I know you gave me the pen already, but are you sure you don't have any paper on you?"
  isabelle concerned_left "No, but there should be papers around here somewhere..."
  isabelle concerned "Maybe check the English classroom or ask someone?"
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_kate:
  show kate eyeroll with Dissolve(.5)
  kate eyeroll "Why do you always drag your feet?"
  mc "It's just how I walk..."
  mc "Do you happen to have anything I can write on? A paper of some sort?"
  kate excited "Of course I do."
  mc "..."
  kate excited "..."
  kate cringe "What? You think I'll just give it to you? Get lost."
  hide kate with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_flora:
  show flora laughing with Dissolve(.5)
  mc "What are you laughing at?"
  flora blush "You running around the school, looking lost..."
  mc "Damn it, [flora]. Stop following me!"
  flora blush "Make me!"
  mc "Ugh. Do you have a paper I can borrow?"
  flora thinking "I had one like thirty seconds ago..."
  mc "Where is it now?"
  flora laughing "I ate it."
  mc "But why?"
  flora worried "I saw you coming over, and I panicked..."
  mc "You're so funny..."
  flora laughing "I know! Try checking for paper in the gym."
  hide flora with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_nurse:
  show nurse sad with Dissolve(.5)
  mc "Hey, do you happen to have any paper I can write on?"
  nurse thinking "I'm afraid not, but there might be some in my office."
  nurse concerned "Or you can ask [kate]... she always seems good at keeping track of things."
  hide nurse with Dissolve (.5)
  return

label isabelle_quest_stolen_askmaxine_mrsl:
  show mrsl neutral with Dissolve(.5)
  mrsl neutral "[mc], what can I do for you?"
  "If only you knew the real answer to that question..."
  mc "Err... any idea where I can find a piece of paper?"
  mrsl thinking "Paper? What do you need that for?"
  mc "To write a letter."
  mrsl surprised "What about email and text messages?"
  mc "The person I'm trying to reach doesn't believe in those things..."
  mrsl surprised "That's unusual."
  mrsl thinking "Well, I just ran out of paper... try asking [jacklyn]."
  hide mrsl with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_jacklyn:
  show jacklyn neutral with Dissolve(.5)
  if mc.at("school_cafeteria"):
    mc "Sorry to bother you during lunch, but do have anything I can write a letter on?"
  else:
    mc "Sorry to bother you after class, but do have anything I can write a letter on?"
  jacklyn smile "No bother, Hank."
  mc "It's [mc]."
  if mc.name.lower() == "hank":
    jacklyn thinking "That's what I said."
    mc "Oh, I thought you called me Hank!"
    jacklyn thinking "I did..."
    mc "But my name is [mc]."
    jacklyn excited "Oh! It's with a French spelling, isn't it?"
    jacklyn annoyed "Like \"Honk\" or something? Honk, honk, honk? Wait, no... that's the Canadian spelling."
    $achievement.goose_goes_hank.unlock()
  else:
    jacklyn thinking "So it is. I'm still trying to memorize the names here."
  jacklyn excited "Anyway... a paper, huh? I don't have any on me, but check with me later during class."
  jacklyn laughing "[mrsl] might also have some if you're desperate!"
  hide jacklyn with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_jacklyn_art:
  show jacklyn smile with Dissolve(.5)
  mc "Papers! I need papers!"
  jacklyn smile "Sure looks like it."
  jacklyn smile "Well, let me get one for you."
  show jacklyn smile at disappear_to_right
  mc "..."
  mc "..."
  mc "..."
  "Why is she taking so long?"
  mc "..."
  mc "..."
  mc "..."
  show jacklyn annoyed at appear_from_right
  jacklyn annoyed "It seems like we've run out..."
  jacklyn annoyed "Got to speak to the principal about this."
  mc "Seriously?"
  jacklyn cringe "If you're desperate, ask her yourself. The upper management is probably hoarding the paper."
  hide jacklyn with Dissolve(.5)
  return

label isabelle_quest_stolen_askmaxine_jo:
  show jo thinking with Dissolve(.5)
  if mc.at("home_*"):
    jo thinking "Shouldn't you be in class?"
  else:
    jo thinking "Shouldn't you be cleaning your room?"
  mc "I, err... I want to practice my handwriting."
  jo annoyed "That's not what I asked."
  mc "I, err... feel like handwriting is important in all stages of life!"
  jo confident "You're not wrong about that."
  mc "Do you know where I can find any paper to write on? I feel like I've looked everywhere."
  jo skeptical "We've actually run out of paper at school... the manufacturer said something about beavers chewing up the trees."
  if mc.at("home_*"):
    jo neutral "Anyway, we should probably have some here."
  else:
    jo neutral "Anyway, we should probably have some at home."
  jo smile "Worst case you could use the back of one of those horrendous posters in your room!"
  "How dare [jo] insult art and culture like that?"
  hide jo with Dissolve(.5)
  return

label isabelle_quest_stolen_eavesdrop:
  "Okay, let's see what the latest gossip is..."
  "..."
  kate "...not that I know of, but anyway. Great work today, girls!"
  kate "And if we can get [lacey] to stop drooling over Brad during practice, we'll be set for the season."
  lacey "He kept giving me flirty looks!"
  stacy "He totally wants to eat your peach."
  lacey "[stacy]!"
  stacy "It's true, babe! You're busting his balls with your teasing!"
  lacey "Maybe if he stops playing, I'll be nicer to his balls..."
  casey "Speaking of busting balls, what's the sitch with the new chick, Katie?"
  stacy "Yeah, is she still being a total hoe-bag?"
  kate "Still working on it, but I'm planning on replacing the stick up her ass with... this thing!"
  lacey "Queen!"
  casey "What the frick is that?"
  stacy "Check out little miss innocent!"
  casey "It's not my fault I have a boyfriend and don't spend all day looking up weird things..."
  stacy "I don't settle for losers, babe."
  casey "Are you calling Tanner a loser?"
  stacy "If you compare him to Chad!"
  casey "As if you could have Chad..."
  kate "Ladies!"
  kate "Allow me to demonstrate. You use these straps... to fasten it around your hips... like so. And then you just... thrust."
  lacey "Whoa! That thing is probably bigger than Chad!"
  kate "If you must know, it was molded after him."
  stacy "Oh, shit... I knew he was big, but damn, girl!"
  casey "Maybe I should aim for Chad too..."
  lacey "Gonna need a bucket of lube to get that thing into her dry English cunt."
  kate "Like I said, it's going in her ass."
  stacy "Hah, you're wicked!"
  casey "The bitch deserves it."
  lacey "She won't be able to sit for the rest of the year!"
  kate "She won't have to... once I'm done with her, she'll crawl around like a little puppy."
  lacey "Queen!"
  kate "All right, let's hit the showers, girls!"
  "Well, that's not exactly what I'd hoped for..."
  "Although, the thought of [kate] fucking [isabelle] in the ass with a massive strap-on is definitely a turn on."
  "Mmm..."
  "Anyway, nothing about the theft... you'd think that'd be a topic of conversation if [kate] were behind it."
  "Hmm..."
  "Best report back to [isabelle] anyway."
  $quest.isabelle_stolen.advance("tellisabelle")
  return

label isabelle_quest_stolen_tellisabelle:
  show isabelle neutral with Dissolve(.5)
  mc "So, I got some new information, but it's not really what we're looking for..."
  isabelle concerned "What did you learn?"
  "It probably wouldn't help to tell her about the strap-on."
  "Honestly, that would probably give her anxiety."
  show isabelle concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"That [kate] is evil.\"":
      show isabelle concerned at move_to(.5)
      mc "That [kate] is evil."
      isabelle thinking "When I was younger I didn't believe in good and evil."
      isabelle thinking "I used to tell myself that people were just unlucky with their situations, causing them to do evil things."
      isabelle cringe "But there are exceptions to that..."
      isabelle cringe "I've come to realize that there are people who are indeed evil... and [kate] may very well be one of them."
      mc "I didn't think you were this cynical, but I couldn't agree more."
      mc "It's unusual coming from an intellectual like you."
      isabelle sad "Intellectuals are usually the first ones to be killed when a dictator takes control of a country..."
      $isabelle.love+=1
      isabelle blush "Thanks for calling me that, though!"
    "\"[kate] said she's planning to replace the stick up your butt...\"":
      show isabelle concerned at move_to(.5)
      mc "[kate] said she's planning to replace the stick up your butt..."
      isabelle cringe "...With what?"
      mc "Well, she had this... strap-on, if you know what that is..."
      isabelle afraid "She's joking!"
      mc "To be honest, probably... she was probably just showing off to her sycophants."
      isabelle cringe "She's absolutely bonkers!"
      $isabelle.lust+=1
      isabelle blush "Thanks for telling me, though."
      mc "I'll make sure it doesn't happen."
  mc "Anyway, I'm sorry to say it, but I didn't learn anything useful so far."
  isabelle sad "That's too bad. What should we do now?"
  mc "I think you should keep on investigating on your own. I have another idea."
  isabelle neutral "What's that?"
  mc "I think if anyone's seen something it's [maxine]."
  jump isabelle_quest_stolen_start_maxine
  return
