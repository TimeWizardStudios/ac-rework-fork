image sugar_cube_small = Transform("items sugar_cube",zoom=.5)
image isabelle_locker_art_teacher = AlphaMask(LiveComposite((116,116),(-90,-119),Transform(LiveComposite((1920,1080),(0,0),"school entrance school",(10,0),"flora confident flip",(202,0),"jacklyn smile"),size=(768,432))),"ui circle_mask")
image isabelle_locker_work_out = AlphaMask(LiveComposite((116,116),(-83,-79),Transform("misc computer_screen_ad",size=(480,270))),"ui circle_mask")
image isabelle_locker_outside_lately = AlphaMask(LiveComposite((116,116),(-107,-188),Transform(LiveComposite((1920,1080),(0,0),"school entrance school",(371,779),"school entrance spinach",(337,0),"nurse blush"),size=(653,368))),"ui circle_mask")

label quest_isabelle_locker_start:
  show isabelle concerned with Dissolve(.5)
  mc "Are you okay?"
  isabelle skeptical "What do you think?"
  mc "Okay..."
  isabelle sad "Sorry, I didn't mean to snap at you."
  mc "Don't worry about it."
  isabelle blush "Really, you've been nothing but good to me throughout this!"
  isabelle sad "Taking it out on you is simply unfair."
  mc "Hey, it's fine! Did you manage to get new books?"
  isabelle neutral "Yeah, I did. So, at least my grades won't suffer from this..."
  mc "We'll get to the bottom of this. I promise."
  isabelle concerned "Thank you, [mc]."
  isabelle concerned "Do you have a plan?"
  show isabelle concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Nothing besides questioning as many people as possible.\"":
      show isabelle concerned at move_to(.5)
      mc "Nothing besides questioning as many people as possible."
      isabelle confident "It's as good a strategy as any."
      mc "Yeah, someone must know something..."
      isabelle confident "I really appreciate you helping me out, [mc]."
      mc "You would've done the same for me."
      isabelle laughing "Absolutely!"
    "?mc.charisma>=6@[mc.charisma]/6|{image=stats cha}|\"Leave this in the hands of Sherlock!\"":
      show isabelle concerned at move_to(.5)
      mc "Leave this in the hands of Sherlock!"
      isabelle confident "And that's supposed to be you?"
      mc "This will be elementary, my dear [isabelle]."
      isabelle confident "You forgot to bring your pipe and deerstalker cap."
      mc "I also didn't bring my heroin. I'm Sherlock 2.0."
      $isabelle.love+=1
      isabelle laughing "You've actually read it!"
      mc "Naturally."
      isabelle flirty "Okay, who do you think did this, Mr. Holmes?"
      mc "It's a capital mistake to theorize before one has data..."
      isabelle flirty "A direct quote? Color me impressed!"
      mc "You'll be even more impressed when I solve this case for you."
      mc "I'll let you know when I have a lead."
      isabelle blush "Thanks so much for helping me out. I do appreciate it."
      hide isabelle with Dissolve(.5)
      "Let's see here. What would Sherlock have done?"
      "Gathered data... interrogations... yes, let's interrogate people."
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"I might have an idea...\"":
      show isabelle concerned at move_to(.5)
      mc "I might have an idea..."
      isabelle neutral "Yeah?"
      mc "If you think about it, there's only one person in the school who deals in the bizarre."
      mc "And a missing locker certainly classifies as bizarre."
      isabelle concerned_left "You think [maxine] did this?"
      mc "Even if she didn't, she'll probably be interested in the story."
      mc "Her newspaper is all about this sort of thing..."
      isabelle excited "That's smart! Let's go!"
      mc "Wait. If she's the culprit, it might be best if I talk to her alone."
      mc "Your presence might give it away."
      isabelle laughing "I'm lucky to have made such a clever friend!"
      mc "Just trying to do my part. No need to get sentimental."
      isabelle confident "Fine. Okay, I'll let you handle it."
      $quest.isabelle_locker["might_have_an_idea"] = True
  if renpy.showing("isabelle"):
    hide isabelle with Dissolve(.5)
  $quest.isabelle_locker.start()
  return

label quest_isabelle_locker_interrogate_flora:
  show flora annoyed with Dissolve(.5)
  flora annoyed "Why are you looking at me like I ripped your favorite anime poster?"
  mc "Uh, that's not the look I was going for..."
  mc "What about this?"
  mc "..."
  flora annoyed "..."
  flora confused "Now you're looking at me like I stole the last slice of your dried up pizza..."
  mc "Close enough, I guess."
  mc "Did you see that someone took [isabelle]'s entire locker?"
  flora laughing "I did see that, yeah."
  flora blush "Was it you?"
  mc "Me?! Why would you even think that?"
  flora thinking "Stealing a locker is weird. You're weird. Seems like a good suspect to me..."
  mc "I'm not weird! Well... not stealing-a-locker-weird, at least."
  flora annoyed_texting "..."
  mc "Who are you texting?"
  flora annoyed_texting "None of your business. Leave me alone."
  mc "Fine."
  hide flora with Dissolve(.5)
  "Well, that didn't help much..."
  $quest.isabelle_locker["flora_interrogated"] = True
  return

label quest_isabelle_locker_interrogate_jo:
  show jo confident with Dissolve(.5)
  jo confident "Hey, kiddo. I was looking for you earlier."
  mc "Me? Why me?"
  jo neutral "Because I wanted to ask you about the mess in the basement at home."
  mc "I haven't been in the basement for a long time!"
  jo smile "I know, I know! But since you're all grown up now, I figured I should start asking you for more help at home."
  jo smile "The basement needs a good cleaning."
  show jo smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Can't [flora] do it?\"":
      show jo smile at move_to(.5)
      mc "Can't [flora] do it?"
      $jo.love-=1
      jo eyeroll "I asked you, [mc]..."
      mc "Well, I'm busy."
    "\"Sure thing, [jo]! I'll take care of it.\"":
      show jo smile at move_to(.5)
      mc "Sure thing, [jo]! I'll take care of it."
      "At some point..."
      $jo.love+=1
      jo flirty "Thank you, [mc]! You're getting more mature every time I see you, it feels like."
  mc "By the way, do you know anything about that missing locker?"
  jo afraid "You're the third person to ask me this..."
  jo thinking "The permit is valid. I had it checked."
  jo worried "I'm sure the locker company will supply a new one and repair the floor."
  jo worried "Anyway, I have a meeting to prepare for. See you at home tonight, okay?"
  hide jo with Dissolve(.5)
  $quest.isabelle_locker["jo_interrogated"] = True
  return

label quest_isabelle_locker_interrogate_mrsl:
  show mrsl confident with Dissolve(.5)
  mc "Hey, [mrsl]!"
  mrsl confident "Hello there, [mc]!"
  mrsl flirty "What can I do for you?"
  mc "I was just wondering if you know anything about the missing locker?"
  mrsl thinking "Missing locker?"
  mc "Yeah, someone stole [isabelle]'s entire locker."
  mc "I take it you didn't know?"
  mrsl surprised "I did not!"
  mrsl surprised "Where is it now?"
  mc "Well, that's what I'm trying to figure out..."
  mrsl afraid "Let me know if you find out who's behind it."
  mc "Same, I guess."
  hide mrsl with Dissolve(.5)
  "[mrsl] did seem awfully bothered by it, but not exactly guilty."
  "Hmm..."
  $quest.isabelle_locker["mrsl_interrogated"] = True
  return

label quest_isabelle_locker_interrogate_lindsey:
  show lindsey smile with Dissolve(.5)
  mc "Hey, [lindsey]!"
  lindsey flirty "Hello!"
  mc "Did you hear about [isabelle]'s locker?"
  lindsey smile "Her locker?"
  mc "Apparently, someone stole it."
  lindsey laughing "What? Her entire locker? You're joking!"
  mc "Yeah, and a chunk of the floor."
  lindsey skeptical "Why?"
  mc "No idea."
  lindsey thinking "That's so weird..."
  mc "So, it wasn't you?"
  lindsey cringe "What? Of course not!"
  lindsey cringe "I have to go."
  show lindsey cringe at disappear_to_left
  "[lindsey] did seem a little guilty, but it's probably just from the chocolates..."
  hide lindsey
  $quest.isabelle_locker["lindsey_interrogated"] = True
  return

label quest_isabelle_locker_interrogate_kate:
  show kate skeptical with Dissolve(.5)
  mc "Did you steal [isabelle]'s locker?"
  kate eyeroll "If I did, I sure wouldn't tell you."
  kate excited "It's hilarious that her bad deeds finally caught up with her, though!"
  mc "What bad deeds?"
  kate laughing "Trying to mess with the natural order of the universe, for one."
  kate confident "Buh-bye."
  show kate confident at disappear_to_right
  pause 1.0
  hide kate
  $quest.isabelle_locker["kate_interrogated"] = True
  return

label quest_isabelle_locker_interrogate_maxine:
  show maxine annoyed with Dissolve(.5)
  maxine annoyed "What?"
  mc "What?"
  maxine eyeroll "What?"
  mc "My turn?"
  maxine thinking "You're playing a dangerous game."
  mc "And so are you."
  maxine cringe "Who told you about my plans...?"
  mc "It was, err..."
  show maxine cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Nobody. What plans are you talking about?\"":
      show maxine cringe at move_to(.5)
      mc "Nobody. What plans are you talking about?"
      maxine skeptical "Are you telling me that you're... {i}unaware?{/}"
      mc "Unaware of what?"
      maxine skeptical "My role. Your role. The great game."
      mc "My role? What's my role?"
      maxine annoyed "That is entirely up to you."
      mc "Gee, that's helpful. Thanks so much, [maxine]."
      maxine confident "You're welcome."
      "I don't think she has a very firm understanding of sarcasm..."
      mc "What's your role?"
      maxine confident "I can't trust you with that information."
      mc "Great."
    "\"[spinach].\"":
      show maxine cringe at move_to(.5)
      mc "[spinach]."
      maxine annoyed "Are you saying that my own cat betrayed me?"
      mc "You better believe it."
      maxine skeptical "Despite the fresh tuna?"
      mc "Yup."
      maxine skeptical "Excuse me for a moment."
      show maxine angry with Dissolve(.5)
      maxine angry "Spinach! Come here, girl!" with vpunch
      show maxine angry at move_to(.75,1)
      show spinach neutral at appear_from_left(.25)
      spinach neutral "Meow?"
      show maxine angry at Transform(xalign=.75),bounce
      maxine angry "Meow, meow! Hiss, hiss!"
      hide maxine
      show maxine angry at Transform(xalign=.75)
      show spinach drugged at Transform(xalign=.25),bounce with Dissolve(.5)
      spinach drugged "Meeew!"
      hide spinach
      show spinach drugged at Transform(xalign=.25,yalign=1.0)
      show maxine neutral at Transform(xalign=.75),bounce with Dissolve(.5)
      maxine neutral "Meowoewouw?"
      hide maxine
      show maxine neutral at Transform(xalign=.75)
      show spinach tongueout at Transform(xalign=.25),bounce with Dissolve(.5)
      spinach tongueout "Mew! Mew! Mew!"
      hide spinach
      show spinach tongueout at Transform(xalign=.25,yalign=1.0)
      "There's no way she's actually talking to the cat..."
      show maxine smile at Transform(xalign=.75),bounce with Dissolve(.5)
      maxine smile "Muw! Muw! Purr! Blep!"
      hide maxine
      show maxine smile at Transform(xalign=.75)
      show spinach closedeyes at Transform(xalign=.25),bounce with Dissolve(.5)
      spinach closedeyes "Meep!"
      hide spinach
      show spinach closedeyes at Transform(xalign=.25,yalign=1.0)
      maxine laughing "Good one, [spinach]!"
      show maxine laughing at move_to(.5,1)
      show spinach closedeyes at disappear_to_left
      pause 0.0
      $maxine.love-=1
      maxine angry "I knew you were lying, [mc]."
      hide spinach
      mc "You're joking... right?"
      maxine neutral "I would never joke about treason."
      mc "..."
    "\"The Inconspicuous Group.\"":
      show maxine cringe at move_to(.5)
      mc "The Inconspicuous Group."
      maxine excited "That's funny!"
      mc "What is?"
      maxine excited "That you think there's a group like that."
      maxine concerned "There's no group like that."
      mc "Well, I just saw a permit that said otherwise."
      maxine sad "You must be mistaken."
      mc "I saw it with my own two eyes!"
      maxine thinking "But can your eyes be trusted?"
      "Hmm... she does have a point..."
      mc "Is this another dream?"
      maxine sad "Life is a dream..."
      maxine sad "A dream about the past. A dream about the future."
      maxine sad "The present is an illusion."
      mc "What?"
      maxine thinking "You're made up of memories. That's all a person is. A memory container."
      mc "Right..."
  mc "Did you steal [isabelle]'s locker?"
  maxine annoyed "Steal? No."
  mc "But you took it?"
  maxine skeptical "Why do you want to know?"
  mc "Because [isabelle] is upset."
  maxine skeptical "I don't see how the two are connected."
  mc "Look, I'm just trying to help her feel better."
  mc "I need to know why you took it."
  maxine annoyed "But I didn't take it."
  mc "Well, the Inconspicuous Group did. Are you telling me you're not part of it?"
  label quest_isabelle_locker_interrogate_maxine_brb:
  show maxine smile with Dissolve(.5)
  maxine smile "If I were part of a secret esoteric group, do you think I would tell you?"
  show maxine smile at move_to(.75)
  $item_count = mc.owned_item_count("sugar_cube")
  menu(side="left"):
    extend ""
    "?mc.owned_item('sugar_cube')@[item_count]/1|{image= sugar_cube_small}|\"All information has a price. I'm willing to pay.\"" if mc.owned_item("sugar_cube"):
      jump quest_isabelle_locker_interrogate_maxine_pay
    "?mc.money>=50@[mc.money]/50|{image=ui hud icon_money}|\"All information has a price. I'm willing to pay.\"" if not mc.owned_item("sugar_cube"):
      label quest_isabelle_locker_interrogate_maxine_pay:
      show maxine smile at move_to(.5)
      mc "All information has a price. I'm willing to pay."
      maxine neutral "My information isn't cheap."
      mc "How much?"
      maxine thinking "One... sugar cube."
      mc "What?"
      maxine thinking "You heard me."
      mc "Why don't you just get one from the cafeteria?"
      maxine thinking "There aren't any in the cafeteria."
      mc "What do you mean? Of course there are!"
      maxine skeptical "You're not paying very close attention, are you?"
      maxine skeptical "That might very well be your downfall."
      if mc.owned_item("sugar_cube"):
        mc "If there aren't any sugar cubes there, how come I have this one?"
        maxine annoyed "Because you didn't find it there."
        mc "Fine. But I'm willing to trade it for information."
        maxine excited "You have a deal. Hand it over!"
        $mc.remove_item("sugar_cube")
      else:
        mc "Do you really think I carry around sugar cubes?"
        mc "I'll give you a dollar instead."
        maxine thinking "Fifty."
        mc "Fifty?!"
        maxine thinking "It's a small price for a woman's affection."
        mc "Affection? What?"
        maxine concerned "Are you not doing this in an attempt to engage [isabelle] in coitus...?"
        mc "No! I'm just trying to help her!"
        maxine excited "That is very altruistic of you, [mc]."
        maxine concerned "It'll still cost you fifty dollars."
        mc "Fine..."
        $mc.money-=50
        mc "There you go. Spill the beans!"
      maxine excited "Thanks for your kind donation. We appreciate the funding of our cause."
      mc "We?"
      maxine thinking "Did I say that?"
      mc "Yes."
      maxine sad "Okay."
      mc "Why did you say that?"
      maxine thinking "Additional information will cost you twenty-two sugar cubes."
      mc "Ugh... just tell me about the locker..."
      maxine thinking "The locker you seek is no longer available."
    "?mc.intellect>=8@[mc.intellect]/8|{image=stats int}|\"I'm also part of the Inconspicuous Group.\"":
      show maxine smile at move_to(.5)
      mc "I'm also part of the Inconspicuous Group."
      maxine angry "No, you're not."
      mc "Oh, yeah? How would you know? Are you saying you're part of it?"
      maxine sad "..."
      "Checkmate, bitch."
      maxine thinking "Part of what?"
      mc "Oh, I see what you're doing."
      maxine concerned "I may have information about that locker."
      maxine concerned "But you need to stop talking about the group."
      mc "What group?"
      maxine flirty "Okay, very good."
      maxine concerned "The locker you seek is no longer available."
    "\"Maybe. I'll be right back...\"":
      show maxine smile at move_to(.5)
      mc "Maybe. I'll be right back..."
      hide maxine with Dissolve(.5)
      $quest.isabelle_locker["be_right_back"] = True
      return
  mc "What? Why?"
  maxine skeptical "It's been... repurposed."
  mc "To what?"
  maxine skeptical "A gateway to the underworld."
  mc "Right. Of course it has."
  mc "Where is it now?"
  maxine annoyed "In a place beyond your reach."
  mc "So, like... the women's bathroom?"
  maxine laughing "Don't be ridiculous!"
  maxine neutral "Now, if you'll excuse me, I have important matters to attend to."
  $quest.isabelle_locker.advance("talk")
  hide maxine neutral with Dissolve(.5)
  "Great."
  "[isabelle] won't be happy about this..."
  return

label quest_isabelle_locker_talk:
  show isabelle laughing with Dissolve(.5)
  isabelle laughing "Hey!"
  mc "You look a bit happier."
  isabelle confident "Do I?"
  mc "Yeah! Did something happen?"
  isabelle confident "Not really. I was just thinking about how sweet you were earlier."
  mc "You'd have to be more specific. The amount of sweetness I provide the world is quite extensive."
  isabelle sad "I meant how you handled my little outburst... It was very mature."
  mc "Oh, I thought you meant the kiss."
  isabelle blush "That was also sweet. And the way you helped me bury the box."
  isabelle blush "I like you, [mc]."
  mc "Then you're going to like me even more now..."
  isabelle excited "Is that right?"
  mc "I found out who stole your locker."
  if quest.isabelle_stolen["lindsey_slaped"]:
    isabelle skeptical "It was [lindsey] again, wasn't it?"
    mc "Nope."
    isabelle skeptical "[kate]?"
    mc "It was [maxine]."
  else:
    isabelle skeptical "It was [kate] again, wasn't it?"
    mc "Nope. It was [maxine]."
  isabelle concerned "[maxine]...?"
  isabelle concerned "Well, where's the locker now?"
  mc "No idea. She wouldn't tell me."
  isabelle annoyed "Why?"
  mc "Sorry, I don't know. She's always so cryptic..."
  isabelle annoyed_left "Miserable slag! What did I ever do to her?!"
  mc "I don't think—"
  isabelle angry "I'm going to..."
  mc "Hey! Don't you think it would be smart to mull this over?"
  if quest.isabelle_stolen["lindsey_slaped"]:
    isabelle sad "Do you think they're all in on this?"
    mc "They?"
    isabelle sad "[kate]... [lindsey]... [maxine]..."
    mc "[lindsey] and [maxine] used to be best friends."
    mc "[kate] and [lindsey] are both jocks."
    mc "It's a possibility..."
  else:
    isabelle sad "Do you think they're both in on this?"
    mc "They?"
    isabelle sad "[kate] and [maxine]."
    mc "Feels unlikely, but it's not impossible..."
    mc "Maybe [kate] has some dirt on [maxine]?"
  isabelle annoyed "Bullying isn't always coordinated."
  isabelle annoyed "Sometimes a bully picks on someone, and other people just follow suit."
  mc "People are shit."
  isabelle sad "Unfortunately, that's often true."
  isabelle sad "I learned it too late."
  mc "I'm sorry."
  isabelle concerned "Sometimes the only thing you can do is hit back."
  isabelle concerned_left "I wish I'd been there to do it for my sister..."
  mc "It's not her bullies this time, but I'm sure she's up there watching."
  isabelle neutral "Thank you. I hope so."
  mc "Let's start with [maxine] — how are we going to get back at her?"
  isabelle skeptical "You know her best..."
  isabelle skeptical "What's something she cares about?"
  mc "Hmm... she's not the easiest person to rattle..."
  mc "She only really cares about her mysteries and conspiracy theories."
  isabelle excited "What about her cat?"
  mc "Oh, you're right! She does care about [spinach]!"
  mc "What are you thinking?"
  isabelle thinking "Well, when someone steals from you, it gives you the right to steal from them."
  mc "I don't think that's how the law works..."
  isabelle cringe "That's how the world works."
  isabelle cringe "An eye for an eye because the world is already blind."
  mc "Well, [maxine] is far from blind — she has eyes everywhere. Stealing [spinach] won't be easy."
  isabelle thinking "But it's possible, right?"
  mc "Maybe if we can lure the cat out of her office..."
  mc "I actually might have an idea."
  isabelle flirty "What is it?"
  mc "Well, [maxine] calls her cat Detective [spinach]."
  isabelle flirty "Does it solve crimes?"
  mc "Not really, but it has a really good sense of smell. Like a bloodhound."
  isabelle thinking "That's fascinating. Not sure how it'll help us steal it, though..."
  mc "I do."
  show isabelle thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "?isabelle.lust>=10@[isabelle.lust]/10|{image= isabelle contact_icon}|{image=stats lust_3}|\"Give me your panties.\"":
      $quest.isabelle_locker["personal_sacrifice"] = True
      show isabelle thinking at move_to(.5)
      mc "Give me your panties."
      isabelle afraid "What?! Why?"
      mc "Because, uh... it's the only way [spinach] will search for you?"
      isabelle laughing "Oh..."
      mc "Are you willing to make a personal sacrifice in the quest for revenge?"
      isabelle confident "Be right back."
      show isabelle confident at disappear_to_left
      "Wait, seriously?"
      isabelle "..."
      isabelle "Just give me a minute!"
      "Huh, I guess so..."
      $isabelle.unequip("isabelle_panties")
      show isabelle flirty_holdingpanties at appear_from_left
      isabelle flirty_holdingpanties "Here you go."
      show isabelle flirty
      $mc.add_item("isabelle_panties_inv")
      mc "Thanks!"
      isabelle laughing "Don't lose them!"
      mc "Don't worry. They're my most treasured possession."
      isabelle smile "I want them back when we're done."
      mc "We'll see about that..."
      isabelle eyeroll "[mc]..."
      mc "Relax, I'm joking!"
    "\"I need an article of your clothing.\"":
      show isabelle thinking at move_to(.5)
      mc "I need an article of your clothing."
      isabelle laughing "Oh, I get it — you'll let her sniff it and then she'll search for me. Clever!"
      mc "Exactly."
      isabelle confident "Okay, do you think my glasses will work?"
      mc "Probably..."
      isabelle confident "All right, here."
      $isabelle.unequip("isabelle_glasses")
      $mc.add_item("isabelle_glasses_inv")
      isabelle laughing "Don't lose them!"
      mc "I'll take good care of them."
  mc "Now, go wait outside and I'll make the cat come to you."
  $quest.isabelle_locker.advance("catnap")
  hide isabelle with Dissolve(.5)
  return

label quest_isabelle_locker_catnap:
  "Damn it — [maxine] is here."
  "If I'm going to lure the cat outside, I'll need to get rid of her somehow..."
  $quest.isabelle_locker["get_rid_of_her"] = True
  return

label quest_isabelle_locker_catnap_maxine:
  show maxine concerned_binoculars with Dissolve(.5)
  maxine concerned_binoculars "Do not disturb me. This is an important process."
  mc "What is?"
  maxine concerned_binoculars "Observing the extraction of milk from bananas."
  mc "Right, of course. What else would you be looking at up there?"
  maxine concerned_binoculars "What did I just tell you? Go away."
  label quest_isabelle_locker_catnap_maxine_back:
  if not renpy.showing("maxine"):
    show maxine concerned_binoculars with Dissolve(.5)
  "Hmm... I need to come up with something that catches her interest..."
  show maxine concerned_binoculars at move_to(.25)
  menu(side="right"):
    extend ""
    "?quest.flora_jacklyn_introduction.finished@|{image=isabelle_locker_art_teacher}|\"What do you think about the new art teacher?\"":
      show maxine concerned_binoculars at move_to(.5)
      mc "What do you think about the new art teacher?"
      maxine concerned_binoculars "A colorful personality. Very fertile."
      mc "...Fertile?"
      maxine concerned_binoculars "That's when a female is able to produce offspring."
      mc "I know that!"
      mc "Anyway, did you know that she doesn't dabble?"
      maxine concerned_binoculars "Everyone knows that."
      mc "Did you know that she might actually be dabbling?"
      maxine skeptical "I'm listening."
      mc "Stop listening. Go investigate."
      maxine skeptical "Are your sources solid?"
      mc "Naturally! Do you think I would bring you false information?"
      maxine thinking "Would you?"
      mc "No!"
      maxine thinking "..."
      maxine thinking "Excuse me."
      show maxine thinking at disappear_to_left
      "Perfect. That'll keep her busy for a while..."
    "?quest.mrsl_HOT.finished@|{image=isabelle_locker_work_out}|\"Do you ever work out?\"":
      show maxine concerned_binoculars at move_to(.5)
      mc "Do you ever work out?"
      maxine concerned_binoculars "I don't need to."
      mc "Why not?"
      maxine concerned_binoculars "I'm planning to install hydraulics."
      mc "...In your body?"
      maxine concerned_binoculars "Only in my arms and legs."
      mc "..."
      mc "Did you know that [mrsl] used to sell workout videos?"
      maxine concerned "I did not."
      mc "Well, you might want to invest in that conspiracy."
      maxine excited "I might indeed. Excuse me."
      show maxine excited at disappear_to_left
      "Easy! Now for the cat..."
    "?quest.kate_search_for_nurse.finished and 'love20' in game.unlocked_stat_perks@|{image=isabelle_locker_outside_lately}|\"Have you been outside lately?\"":
      show maxine concerned_binoculars at move_to(.5)
      mc "Have you been outside lately?"
      maxine concerned_binoculars "Yes, whenever I hunt goblins in the woods."
      mc "..."
      mc "Did you know that two very rare herbs grow on the school grounds?"
      maxine concerned "You have my attention."
      mc "They're called Nymphoria Heartus and Venus Vagenis."
      maxine excited "That is very interesting! Excuse me."
      show maxine excited at disappear_to_left
      "Well, that was easy..."
    "\"I'll be back.\"":
      show maxine concerned_binoculars at move_to(.5)
      mc "I'll be back."
      hide maxine with Dissolve(.5)
      $quest.isabelle_locker["be_back"] = True
      return
  $quest.isabelle_locker.advance("scent")
  return

label quest_isabelle_locker_scent_spinach(item):
  if item == "isabelle_panties_inv":
    if not quest.isabelle_locker["first_sniff"]:
      $quest.isabelle_locker["first_sniff"] = True
      mc "Here, kitty kitty..."
      show spinach neutral with Dissolve(.5)
      spinach neutral "Meow?"
      mc "Can you help me find [isabelle]?"
      mc "Sniff these — I promise they smell lovely."
      $mc.remove_item(item)
      spinach neutral "..."
      spinach hearteyed "Growl!"
      show spinach hearteyed at disappear_to_left(.75)
      "There she goes! Right out of the window."
    else:
      show spinach neutral with Dissolve(.5)
      mc "Come on, [spinach]... I know you want another sniff."
      $mc.remove_item(item)
      spinach hearteyed "Mewhehehehew!"
      show spinach hearteyed at disappear_to_left(.75)
      "Damn, she's flying!"
    $spinach.alternative_locations["school_clubroom"] = ""
    $quest.isabelle_locker["spinach"] = "school_homeroom"
    $mc.add_item(item)
  elif item == "isabelle_glasses_inv":
    if not quest.isabelle_locker["first_sniff"]:
      $quest.isabelle_locker["first_sniff"] = True
      mc "[spinach], I need your help!"
      show spinach neutral with Dissolve(.5)
      spinach neutral "Meeew?"
      mc "I can't find [isabelle], and she's lost her glasses."
      mc "Here, have a whiff..."
      $mc.remove_item(item)
      spinach closedeyes "Meow!"
      show spinach closedeyes at disappear_to_left(.75)
      mc "Oh, I think she's caught a trail!"
    else:
      show spinach neutral with Dissolve(.5)
      mc "Here, [spinach]! Find [isabelle] for me."
      $mc.remove_item(item)
      spinach closedeyes "Meow!"
      show spinach closedeyes at disappear_to_left(.75)
      "There she goes!"
    $spinach.alternative_locations["school_clubroom"] = ""
    $quest.isabelle_locker["spinach"] = "school_homeroom"
    $mc.add_item(item)
  else:
    "If I let [spinach] sniff my [item.title_lower], she might lead me to [item.title_lower]-land. I don't have time to visit exotic places right now."
    $quest.isabelle_locker.failed_item("isabelle_clothing",item)
  return

label quest_isabelle_locker_scent:
  $process_event("update_state")
  if spinach.location == maxine.location:
    jump quest_isabelle_locker_scent_busted
  if spinach.at("school_homeroom"):
    show spinach neutral with Dissolve(.5)
    mc "Come on, little one! Lead the way!"
    spinach closedeyes "Meow! Meow!"
    show spinach closedeyes at disappear_to_right(.75)
    pause 0.75
    $quest.isabelle_locker["spinach"] = "school_ground_floor"
  elif spinach.at("school_ground_floor"):
    if "3" in quest.isabelle_locker["spinach"]:
      show spinach hearteyed with Dissolve(.5)
      spinach hearteyed "Meeeeeow!"
      show spinach hearteyed at disappear_to_right(.75)
      "Finally! She ran outside!"
      $quest.isabelle_locker["spinach"] = "school_entrance"
    elif "2" in quest.isabelle_locker["spinach"]:
      show spinach drugged with Dissolve(.5)
      spinach drugged "Mew! Meoeoeoew!"
      show spinach drugged at disappear_to_left(.75)
      "Ugh, not upstairs! Bad [spinach]!"
      $quest.isabelle_locker["spinach"] = "school_first_hall"
    else:
      show spinach drugged with Dissolve(.5)
      spinach drugged "Meep!"
      show spinach drugged at disappear_to_left(.75)
      "Hmm... she went into the admin wing..."
      $quest.isabelle_locker["spinach"] = "school_ground_floor_west"
  elif spinach.at("school_ground_floor_west"):
    show spinach tongueout with Dissolve(.5)
    spinach tongueout "Meow!"
    show spinach tongueout at disappear_to_right(.75)
    "Okay, good — she made a one-eighty back to the entrance hall!"
    $quest.isabelle_locker["spinach"] = "school_ground_floor2"
  elif spinach.at("school_first_hall"):
    show spinach neutral with Dissolve(.5)
    mc "Did you lose the trail, [spinach]?"
    spinach closedeyes "Meow?"
    show spinach closedeyes at disappear_to_right(.75)
    "I guess not..."
    "She went back downstairs again."
    $quest.isabelle_locker["spinach"] = "school_ground_floor3"
  return

label quest_isabelle_locker_scent_busted:
  show spinach tongueout with Dissolve(.5)
  pause 0.0
  show spinach tongueout at move_to(.25,1)
  show maxine concerned at appear_from_right(.75)
  maxine concerned "[spinach]! What are you doing down here?"
  spinach neutral "Meow!"
  maxine blush "Come on, let's go back to my office."
  show maxine blush at disappear_to_left
  pause 0.5
  show spinach neutral at disappear_to_left(.5)
  pause 0.5
  $quest.isabelle_locker["busted"]+=1
  $quest.isabelle_locker["spinach"] = False
  $quest.isabelle_locker.advance("repeat")
  return

label quest_isabelle_locker_repeat:
  show maxine neutral with Dissolve(.5)
  "Okay, I got to get [maxine] out of here again..."
  if quest.isabelle_locker["busted"] == 1:
    mc "In case you're interested, there was a case of spontaneous combustion in the gym earlier."
    maxine concerned "Let me grab my asbestos suit."
    show maxine concerned at disappear_to_left
    "It was really just [kate] going off on a girl in her squad for having a wrinkly skirt..."
    "Sorry, Barry."
  elif quest.isabelle_locker["busted"] == 2:
    mc "Did you know there's a gathering of snails in the fine arts wing?"
    mc "They seemed drawn to douchey impressionist art."
    maxine concerned "This requires further investigation."
    show maxine concerned at disappear_to_left
    "Obviously just some jocks trying to impress [jacklyn], but [maxine] doesn't need to know that..."
  elif quest.isabelle_locker["busted"] == 3:
    mc "Someone managed to get in [maya]'s pants!"
    maxine thinking "Don't lie to me."
    mc "Sorry..."
    "Crap. That was obviously too unbelievable."
    mc "What I meant was, uh... there's an evil AI on the loose... trying to take over the school?"
    show maxine afraid with Dissolve(.5)
    maxine afraid "I knew this would happen sooner or later!" with vpunch
    maxine afraid "Where did I put my battering-RAM?!" with vpunch
    show maxine afraid at disappear_to_left
    "Hope she doesn't break all the school computers. I still have a folder of... {i}important stuff{/} on one of them."
  else:
    mc "I hear beaver-gnawing from inside the walls sometimes."
    mc "It's like a termite infestation. Night and day."
    maxine concerned "Where?"
    mc "At home... at school... in my dreams..."
    mc "I rarely sleep anymore..."
    maxine concerned "Let me go grab my haruspex gear. We might be dealing with beaver ghosts."
    show maxine concerned at disappear_to_left
  "[spinach] probably lost her trail too — I better allow her another sniff."
  $quest.isabelle_locker.advance("scent")
  return

label quest_isabelle_locker_scent_isabelle:
  show isabelle excited_spinach with Dissolve(.5)
  isabelle excited_spinach "Look what I found!"
  spinach "Meow!"
  mc "So, what do we do now?"
  isabelle neutral_spinach "I figured we'd take her to your place."
  mc "Why not yours?"
  isabelle concerned_left_spinach "We're actually still living in a hotel, and they don't allow pets..."
  mc "I'm not sure [jo] will allow it either."
  isabelle neutral_spinach "It'll only be for a short while! We'll trade her in for my locker."
  mc "Alright. It's probably for the best, anyway — since the stolen locker is yours, you'd be the number one suspect."
  isabelle excited_spinach "Good thinking!"
  mc "Okay, let's go."
  isabelle neutral_spinach "Just one thing..."
  mc "Yeah?"
  if mc.owned_item("isabelle_panties_inv"):
    isabelle neutral_spinach "I'd like my panties back now."
    mc "You seem to have your hands full right now. I don't mind holding on to them a little longer."
    isabelle skeptical_spinach "[mc]..."
    mc "I'm totally kidding! Here."
    $mc.remove_item("isabelle_panties_inv")
    isabelle excited_holdingpanties_spinach "Thanks!"
    isabelle neutral_holdingpanties_spinach "Okay, I'm ready."
    mc "Aren't you going to put them on?"
    isabelle concerned_left_holdingpanties_spinach "My hands are full... It'll have to wait."
    mc "That's just so hot."
    isabelle excited_holdingpanties_spinach "Oh, stop it!"
  elif mc.owned_item("isabelle_glasses_inv"):
    isabelle neutral_spinach "I need my glasses back."
    mc "Oh, sure! Here you go."
    $mc.remove_item("isabelle_glasses_inv")
    $isabelle.equip("isabelle_glasses")
    isabelle excited_spinach "Okay, I'm ready."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $isabelle.equip("isabelle_panties")
  $quest.isabelle_locker.advance("bedroom")
  $mc["focus"] = "isabelle_locker"
  $game.location = "home_bedroom"
  $game.advance()
  hide isabelle
  pause 1.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  return

label quest_isabelle_locker_bedroom:
  if home_bedroom["clean"]:
    show isabelle excited with Dissolve(.5)
    isabelle excited "Your room is really nice!"
    isabelle neutral "Do you always keep it this clean?"
    mc "Yeah, I'm something of a germophobic."
    isabelle concerned "Are you really?"
    mc "It's not a problem or anything... I don't obsess over it."
    $isabelle.love+=1
    isabelle excited "Oh, okay. Then clean is good!"
  else:
    show isabelle afraid with Dissolve(.5)
    isabelle afraid "What happened here?!"
    mc "What do you mean?"
    isabelle cringe "The place is a mess!"
    mc "Sorry, I didn't have time to clean..."
    $isabelle.love-=1
    isabelle thinking "And what's with all those lewd posters?"
  mc "Let's just figure out what our next step is."
  isabelle neutral "You're right. We need to start negotiating with [maxine]."
  mc "But how?"
  isabelle concerned_left "We could get a burner phone and a voice changer..."
  isabelle skeptical "{i}[mc], I want to play a game.{/}"
  mc "Great impression, but I have a better idea."
  mc "Let's just make a new email account and send her a message."
  isabelle laughing "That would be a lot easier!"
  mc "Okay, I'll set it up and you can start thinking about how to phrase it."
  isabelle confident "Deal."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_locker.advance("email")
  return

label quest_isabelle_locker_home_bedroom_spinach:
  show spinach drugged with Dissolve(.5)
  spinach drugged "Meow!"
  "Oh, god. I hope she doesn't uncover something embarrassing..."
  hide spinach with Dissolve(.5)
  return

label quest_isabelle_locker_home_bedroom_door:
  "I can't leave [isabelle] alone here with [spinach]."
  "Who knows what dirty secrets they'll dig up?"
  return

label quest_isabelle_locker_email:
  "All right, let's create a new email account..."
  "What should be the handle?"
  menu(side="middle"):
    extend ""
    "BeachBodSunKing01":
      $mc.strength+=1
      "Might as well flex on this skinny nerd."
    "NietzschesBitches666":
      $mc.intellect+=1
      "Just to let [maxine] know she's dealing with real intellectuals."
    "AtomicSexBomb1945":
      $mc.charisma+=1
      "If it doesn't work out with [isabelle], it's smart to have a plan B."
  mc "The account is ready to go."
  show isabelle laughing with Dissolve(.5)
  isabelle laughing "That was quick!"
  mc "I'm a man of utmost swiftness."
  isabelle confident "Let's get started."
  show isabelle confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'll compose it.\"":
      show isabelle confident at move_to(.5)
      mc "I'll compose it."
      isabelle skeptical "And why is that?"
      mc "I know [maxine] best, that's all."
      mc "Besides, I've got longer grievances with her than you do."
      isabelle concerned "What did she do?"
      mc "I was placed in the same group as [lindsey] and [maxine] for an art project once."
      mc "They kicked me out and then proceeded to win the art contest that year."
      isabelle skeptical "That's messed up!"
      mc "There were other things too..."
      $isabelle.lust+=1
      isabelle neutral "I'm so sorry. You definitely deserve to release some of that rage."
      mc "Thanks! I've been looking forward to this."
      window hide
      hide isabelle
      show black
      with Dissolve(.5)
      pause 1.0
      hide black with Dissolve(.5)
      window auto
      mc "All right, I'm done!"
      show isabelle excited with Dissolve(.5)
      isabelle excited "Okay, that looks good! Let's hit send."
      mc "..."
      mc "Sent!"
    "\"I think you should write it.\"":
      show isabelle confident at move_to(.5)
      mc "I think you should write it."
      mc "You're the best person I know when it comes to writing and English."
      $isabelle.love+=1
      isabelle blush "Aw, thanks!"
      isabelle blush "Okay, give me a minute and I'll type this up."
      window hide
      show black with Dissolve(.5)
      pause 1.0
      hide black
      show isabelle neutral
      with Dissolve(.5)
      window auto
      isabelle neutral "All done!"
      mc "Send it!"
      isabelle concerned "Don't you want to read it first?"
      mc "No need. You've got this under control."
      isabelle excited "Thank you."
      mc "It's the truth! You're the most reliable person I know."
      isabelle neutral "You're not so bad yourself..."
      isabelle neutral "Okay, sent!"
  isabelle neutral "I suppose we wait now."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_locker.advance("wait")
  return

label quest_isabelle_locker_wait:
  pause 0.5
  play sound "notification"
  $quest.isabelle_locker["got_a_response"] = True
  pause 0.5
  show isabelle excited with Dissolve(.5)
  isabelle excited "Oh, we've got a response!"
  mc "What does it say?"
  isabelle excited "..."
  isabelle concerned "It just says... no."
  mc "Even though [maxine] knows we have her cat?"
  isabelle afraid "Yes! I don't get it!"
  isabelle afraid "Doesn't she love her cat?"
  mc "She does."
  isabelle cringe "What kind of crazy person would care more about a stupid locker than her favorite pet?"
  isabelle cringe "She's trying to test me."
  isabelle cringe "That's what bullies do! Always trying to see how close to the edge they can push you!"
  "If she's right, then [kate]'s got the whole school on her side."
  "I always thought [maxine] was the neutral kind..."
  isabelle annoyed "I'm not going to let her do this."
  isabelle annoyed "I'm not going to let them get away with it."
  if quest.isabelle_stolen["lindsey_slaped"]:
    isabelle annoyed "[maxine], [lindsey], and [kate] — they'll all get what's coming for them."
  else:
    isabelle annoyed "I don't care if [maxine] and [kate] are in on this together — I'm going to destroy them both."
  isabelle angry "Forget about nipping things in the bud!"
  isabelle angry "I'm going to rip up the entire rotten root!"
  "Damn, [isabelle] is really losing it..."
  "I can't imagine the pain of losing her sister to bullying, only to get bullied herself."
  isabelle annoyed_left "Miserable fucking twats... It was never about the locker..."
  isabelle annoyed "It was about the bloody inability to be decent!"
  isabelle angry "If they want to keep playing games, I'm going to break the board for them!"
  isabelle angry "Let's see if [maxine] still says no when I send her the cat's bloody head..."
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  show isabelle angry at move_to(.75)
  menu(side="left"):
    extend ""
    "{image=stats love}|\"That's enough, [isabelle].\"":
      show isabelle angry at move_to(.5)
      mc "That's enough, [isabelle]."
      isabelle skeptical "What?"
      mc "I said enough."
      isabelle skeptical "What the hell do you mean?"
      mc "Listen to yourself!" with vpunch
      isabelle concerned "..."
      mc "You're talking about killing an innocent animal for revenge!"
      mc "That's not the [isabelle] I know."
      isabelle concerned_left "..."
      mc "Stealing [spinach] was far enough. We need to find a better way."
      mc "If you throw away your decency, you're no better than the bullies."
      isabelle sad "You're right..."
      isabelle sad "It's just so hard to deal with these emotions... I'm so angry all the time..."
      mc "You've been through hell — it's normal to feel this way."
      mc "But you can't let the anger and quest for revenge define you."
      mc "You can't let it cloud your judgment."
      isabelle sad "God, I'm such an idiot..."
      mc "Oh, stop it! You're one of the smartest people I know."
      mc "You're kind, strong, and independent."
      mc "And more importantly, you stand for what is right."
      mc "A lot of people just talk the talk. You walk the walk!"
      mc "And you're not afraid to make enemies in the process."
      mc "You just need to know when to stop... I'm here to help you with that."
      $isabelle.love+=3
      isabelle blush "You always get me in such a good mood, [mc]..."
      isabelle blush "I feel like no one else really understands me."
      mc "I usually struggle when it comes to being social, but you make it easy."
      mc "You never judge me, and I think that's a big part of it."
      isabelle laughing "I think everyone deserves a fair chance!"
      isabelle confident "What about you? Do you often judge people?"
      mc "I try not to."
      isabelle confident "Well, would you judge me if I did... {i}this?{/i}"
      mc "Did what?"
      window hide
      show isabelle confident:
        linear 0.5 zoom 7.5 yoffset 2500
      $mc["focus"] = ""
      show black with Dissolve(.5)
      play sound "<from 0.2>bedsheet"
      with vpunch
      pause 0.5
      $set_dialog_mode("default_no_bg")
      isabelle "Did this..."
      hide black
      hide isabelle
      show isabelle straddling_gaze
      with Dissolve(1.0)
      window auto
      $set_dialog_mode("")
      isabelle straddling_gaze "Hi."
      mc "H-hello..."
      "A moment ago [isabelle] was about to blow up. How did we end up here?"
      "She sits on my lap like it's the most normal thing ever."
      "Like we've done this a million times before."
      "Her weight and the warmth of her thighs..."
      "It feels... right, somehow."
      "And then she leans in closer. So close that the tip of her nose touches mine."
      "She smiles, and it's the most reassuring smile anyone has ever given me."
      "Like all the problems in the world no longer exist."
      "Like it's just the two of us and nothing else matters."
      isabelle straddling_gaze "Are you okay?"
      mc "Yes!"
      mc "I mean, sure. I'm chilling."
      "Fuck... that was not smooth."
      isabelle straddling_gaze "Chilling? Are you sure?"
      "Oh, god. She's so close. The scent of her peppermint lip gloss sharpens my senses."
      "A breath of fresh air. That's what she is, and—"
      show isabelle straddling_kiss with Dissolve(.5)
      "..."
      isabelle straddling_kiss "..."
      isabelle straddling_gaze "How about now, [mc]? Are we still chilling?"
      mc "Uhh... I think I need more data..."
      isabelle straddling_gaze "Heh! Okay!"
      show isabelle straddling_kiss with Dissolve(.5)
      "As our lips meet, tiny stars explode across my skin."
      "Tingles down the back of my neck. Goosebumps on my arms."
      "There's confidence in her every move, but also gentleness."
      show isabelle straddling_gaze with Dissolve(.5)
      mc "Fuck me..."
      isabelle straddling_gaze "Yeah?"
      mc "Sorry, it's an American figure of speech..."
      isabelle straddling_gaze "Are you certain it wasn't a Freudian slip?"
      mc "Uh..."
      isabelle straddling_gaze "Don't worry, I'm just taking the piss."
      mc "Not on the bed!"
      isabelle straddling_gaze "Ha!"
      mc "I know something we can do on the bed, though..."
      isabelle straddling_gaze "And what's that?"
      mc "Well, it's a bit scientifically complex, but it would require you to remove your shirt..."
      isabelle straddling_gaze "A curious proposal."
      isabelle straddling_gaze "Okay, I'll bite."
      isabelle straddling_remove_top "Like this?"
      mc "Exactly like that..."
      "Damn, this is like a dream come true!"
      "[isabelle] undressing on my bed! And it's not even at gunpoint!"
      isabelle straddling_remove_bra "I presume you want me to remove this as well..."
      mc "Strictly scientifically speaking, fuck yes!"
      isabelle straddling_remove_bra "Okay, then..."
      show isabelle straddling_boob_hide with Dissolve(.5)
      "Never thought I'd have her in this position..."
      "Topless. Hands bound to her chest by the invisible shackles of decency."
      "And she likes it."
      "That subtle lip-bite tells me everything. It's like a poker tell of passion and I'm about to check."
      mc "Remove your hands."
      isabelle straddling_boob_hide "Whoa! Bossy!"
      mc "...Please?"
      isabelle straddling_boob_reveal "Hah, if you insist!"
      "She releases her breasts and they land with a tiny bounce."
      "Pure natural perfection."
      "Her nipples stand, perky with excitement."
      "They call to me. A deep primal urge..."
      "It's not a matter of choice. I just have to touch them."
      show isabelle straddling_boob_play with Dissolve(.5)
      "Heat radiates through her chest into my hands. A perfect fit."
      "It's like they're made to grab and squeeze and play with."
      "And the softness is simply divine..."
      "Most girls would never have allowed me to touch them like this, but [isabelle] isn't most girls."
      "Instead, she moans with pleasure and delight."
      isabelle straddling_boob_play "Oh, god... don't stop..."
      mc "As if."
      "As I gently twist and play with her nipples, a shudder of pleasure rolls through her body."
      isabelle straddling_boob_play "I can't... take it..."
      "And just like that, she slides back and slips out of her pants."
      show isabelle straddling_remove_panties with Dissolve(.5)
      "She takes her time with her panties, pulling the frilly waistband down her thighs."
      "Not a twinkle of doubt in her face."
      show isabelle straddling_panty_drop with Dissolve(.5)
      "This is happening."
      "The thought dawns on me as her silky panties sail to the bed. This is actually it."
      "Already, a sheen of wetness sparkles like morning dew in her peach fuzz."
      "She's excited. Her swollen labia speaks the truth."
      "Of course, she's not as excited as I am..."
      isabelle straddling_dick_reveal "Oh, man..."
      "Like cherry blossoms carried by the wind, a soft blush sweeps across her cheeks."
      "Surely, this isn't the first time she has seen one of these..."
      show isabelle straddling_mutual_fap1 with Dissolve(.5)
      "Slowly, her hand snakes down her stomach and settles on her pussy."
      "Her fingers dance across her nether lips."
      "[isabelle] is clearly no novice to treating herself."
      "The thought of her getting off stirs my own juices."
      window hide
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap3 with Dissolve(.25)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap1 with Dissolve(.25)
      pause 0.2
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap3 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap3 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      pause 0.15
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap3 with Dissolve(.25)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap3 with Dissolve(.25)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap3 with Dissolve(.25)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap3 with Dissolve(.25)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap3 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap3 with Dissolve(.25)
      show isabelle straddling_mutual_fap2 with Dissolve(.25)
      show isabelle straddling_mutual_fap1 with Dissolve(.25)
      window auto
      "There's this silent mutual agreement between us."
      "If this is a race, we're running it hand in hand. We'll only cross the finish line together."
      window hide
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap3 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap3 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap3 with Dissolve(.2)
      show isabelle straddling_mutual_fap2 with Dissolve(.2)
      show isabelle straddling_mutual_fap1 with Dissolve(.2)
      pause 0.15
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap3 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap3 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap3 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap3 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap1 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap3 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap1 with Dissolve(.15)
      show isabelle straddling_mutual_fap2 with Dissolve(.15)
      show isabelle straddling_mutual_fap3 with Dissolve(.15)
      show isabelle straddling_getting_ready with Dissolve(.5)
      window auto
      isabelle straddling_getting_ready "I need you inside me! Right this moment."
      "She doesn't wait for my response. Just lifts herself up..."
      "Rubs the tip of my dick between her pussy lips, letting me feel how absolutely soaked she is..."
      show isabelle straddling_insertion with Dissolve(.5)
      isabelle straddling_insertion "Ohhh!" with vpunch
      "Then she slides me inside her, and the heat of her pussy almost makes me blow my load."
      "The tightness, the pull of her muscles..."
      "Her hot wetness squeezes the tip of my dick."
      isabelle straddling_insertion "Oh, my god!"
      mc "Fuck!"
      "Inch by inch, [isabelle] slides down the shaft of my dick, filling herself up.{space=-5}"
      show isabelle straddling_penetration1 with Dissolve(.5)
      "Carefully, she bottoms herself out, letting my dick rest against her cervix."
      isabelle straddling_penetration1 "Oh, my god!"
      "She cries out in a mixture of pain and deep-seated pleasure."
      "The squeeze of her vaginal muscles sends waves of pleasure through my lower abdomen."
      window hide
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration3 with Dissolve(.25)
      show isabelle straddling_penetration2 with Dissolve(.4)
      pause 0.1
      show isabelle straddling_penetration3 with Dissolve(.4)
      show isabelle straddling_penetration2 with Dissolve(.4)
      pause 0.1
      show isabelle straddling_penetration3 with Dissolve(.4)
      show isabelle straddling_penetration2 with Dissolve(.4)
      pause 0.1
      show isabelle straddling_penetration3 with Dissolve(.4)
      show isabelle straddling_penetration1 with Dissolve(.4)
      pause 0.25
      show isabelle straddling_penetration2 with Dissolve(.4)
      show isabelle straddling_penetration1 with Dissolve(.4)
      show isabelle straddling_penetration2 with Dissolve(.4)
      show isabelle straddling_penetration1 with Dissolve(.4)
      show isabelle straddling_penetration2 with Dissolve(.4)
      show isabelle straddling_penetration1 with Dissolve(.4)
      pause 0.15
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration3 with Dissolve(.25)
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration1 with Dissolve(.25)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      pause 0.1
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration1 with Dissolve(.25)
      window auto
      "Her breathing quickens. Gasps of pleasure fill my usually austere bedroom."
      "She moans quietly, very unlike the faked screams of a pornstar."
      window hide
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      pause 0.2
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration3 with Dissolve(.25)
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration1 with Dissolve(.25)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration1 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.2)
      show isabelle straddling_penetration3 with Dissolve(.2)
      show isabelle straddling_penetration2 with Dissolve(.25)
      show isabelle straddling_penetration1 with Dissolve(.25)
      pause 0.25
      show isabelle straddling_penetration2 with Dissolve(.4)
      show isabelle straddling_penetration1 with Dissolve(.4)
      show isabelle straddling_penetration2 with Dissolve(.4)
      show isabelle straddling_penetration1 with Dissolve(.4)
      show isabelle straddling_penetration2 with Dissolve(.4)
      window auto
      "We move in unison."
      "A sigh of relief follows each rise."
      window hide
      show isabelle straddling_penetration4 with Dissolve(.4)
      pause 0.15
      show isabelle straddling_penetration5 with Dissolve(.25)
      show isabelle straddling_penetration6 with Dissolve(.25)
      show isabelle straddling_penetration5 with Dissolve(.25)
      show isabelle straddling_penetration4 with Dissolve(.25)
      show isabelle straddling_penetration5 with Dissolve(.25)
      show isabelle straddling_penetration6 with Dissolve(.25)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      pause 0.25
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      pause 0.2
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.25)
      show isabelle straddling_penetration4 with Dissolve(.25)
      pause 0.1
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration6 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration4 with Dissolve(.15)
      pause 0.25
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration6 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration4 with Dissolve(.15)
      pause 0.25
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration6 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration4 with Dissolve(.15)
      window auto
      "A cry of pleasure follows each fall."
      window hide
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      show isabelle straddling_penetration5 with Dissolve(.4)
      show isabelle straddling_penetration4 with Dissolve(.4)
      pause 0.15
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.4)
      window auto
      "She bottoms herself out gently."
      "It teases me, making me crave harder thrusts..."
      "But she's on top now, controlling the pace."
      window hide
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration6 with Dissolve(.2)
      show isabelle straddling_penetration5 with Dissolve(.2)
      show isabelle straddling_penetration4 with Dissolve(.2)
      pause 0.1
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration6 with Dissolve(.25)
      show isabelle straddling_penetration5 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration9 with Dissolve(.25)
      show isabelle straddling_penetration8 with Dissolve(.1)
      show isabelle straddling_penetration7 with Dissolve(.15)
      show isabelle straddling_penetration4 with Dissolve(.5)
      window auto
      isabelle straddling_penetration4 "Oh, my god! Yes!"
      "[isabelle] rides my dick the way she likes it, just like everything she touches in life."
      "She knows what she wants, how she likes it."
      "Her confidence intoxicates me."
      "Rubs off on me."
      "Gives rise to a boldness that I didn't know I had."
      mc "[isabelle]...?"
      isabelle straddling_change_position "Yes?"
      show isabelle lying_down_surprise with Dissolve(.5)
      "With as much gentleness as my overexcited muscles can muster, I push her over."
      "Taking control of her."
      "A squeal of surprise and excitement escapes her lips."
      isabelle lying_down_surprise "Oh! [mc]!"
      mc "I want you so bad..."
      isabelle lying_down_handholding "Then, take me..."
      "Our chests meet for the first time. Skin and sweat becoming one."
      "[isabelle] is so soft... so huggable..."
      "Her nipples rub against me as I lean in."
      show isabelle lying_down_kiss with Dissolve(.5)
      "For a moment, our panting mouths meet in a wet kiss."
      "My entire being tells me to stick it inside her again, but I want to savor this moment."
      "I want to feel everything that I've been deprived of for so long."
      "I want to explore her body completely. Map out every ridge and valley with my tongue."
      "But there's just so much time..."
      show isabelle lying_down_leg_raise with Dissolve(.5)
      "I'll split her legs and conquer her like fucking Columbus."
      isabelle lying_down_leg_raise "Oh!"
      mc "Are you ready?"
      "She's too consumed by lust to answer, but her pleading eyes say it all.{space=-50}"
      show isabelle lying_down_clit_rub1 with Dissolve(.5)
      "Her needy lips try to draw me in..."
      window hide
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub3 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub1 with Dissolve(.2)
      pause 0.25
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub3 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub1 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub3 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub1 with Dissolve(.15)
      pause 0.15
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub3 with Dissolve(.25)
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub3 with Dissolve(.25)
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub3 with Dissolve(.25)
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub3 with Dissolve(.25)
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub1 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub3 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub1 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub3 with Dissolve(.25)
      show isabelle lying_down_clit_rub2 with Dissolve(.25)
      show isabelle lying_down_clit_rub1 with Dissolve(.25)
      window auto
      "She whines as my fingers touch her clit."
      "She wants me inside of her again, but her trembling body is just too exquisite not to tease."
      isabelle lying_down_clit_rub4 "[mc]..."
      mc "Yes?"
      isabelle lying_down_clit_rub4 "Take me..."
      mc "Oh, I'm planning on it."
      isabelle lying_down_clit_rub4 "Take me now!"
      window hide
      show isabelle lying_down_clit_rub1 with Dissolve(.5)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub3 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub1 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub3 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub1 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub3 with Dissolve(.15)
      show isabelle lying_down_clit_rub2 with Dissolve(.15)
      show isabelle lying_down_clit_rub1 with Dissolve(.15)
      pause 0.2
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub3 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub1 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub3 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub1 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub3 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub1 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.1)
      show isabelle lying_down_clit_rub3 with Dissolve(.1)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub3 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub3 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      show isabelle lying_down_clit_rub3 with Dissolve(.2)
      show isabelle lying_down_clit_rub2 with Dissolve(.2)
      window auto
      "I ignore her and keep playing with her clit."
      "When I stick it inside [isabelle] again, I want her to come like never before."
      "She looks up at me, eyes watery. The muscles in her abdomen shake and shiver..."
      show isabelle lying_down_insertion with Dissolve(.5)
      isabelle lying_down_insertion "Oh, my god!" with hpunch
      "She gasps as my dick once again penetrates her."
      "The desire burning in my eyes is making her nervous, but also excited."
      "It's finally time."
      window hide
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration3 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration3 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration3 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration3 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration3 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.5)
      window auto
      isabelle lying_down_penetration1 "Take me!"
      "Each stroke goes deep and hard."
      "Her vaginal walls push against me, trying to stop me from going too deep."
      window hide
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      pause 0.15
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      pause 0.2
      show isabelle lying_down_penetration2 with Dissolve(.25)
      show isabelle lying_down_penetration1 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      pause 0.1
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      show isabelle lying_down_penetration2 with Dissolve(.4)
      show isabelle lying_down_penetration3 with Dissolve(.4)
      window auto
      "But I don't want to hurt her... just hit her deep enough to take her over the edge."
      window hide
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration1 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.2)
      show isabelle lying_down_penetration3 with Dissolve(.2)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration1 with Dissolve(.15)
      show isabelle lying_down_penetration2 with Dissolve(.1)
      show isabelle lying_down_penetration3 with Dissolve(.25)
      pause 0.25
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.1)
      show isabelle lying_down_penetration3 with hpunch
      pause 0.4
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.1)
      show isabelle lying_down_penetration3 with hpunch
      pause 0.4
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.1)
      show isabelle lying_down_penetration3 with hpunch
      pause 0.4
      show isabelle lying_down_penetration2 with Dissolve(.15)
      show isabelle lying_down_penetration1 with Dissolve(.1)
      show isabelle lying_down_penetration3 with hpunch
      window auto
      isabelle lying_down_penetration3 "Bloody hell!"
      "With a scream of pleasure, her pussy spasms around my dick."
      "Her eyes glaze over and then roll back into her head."
      "Nothing but the bliss of her orgasm fills her senses."
      mc "Fuck!"
      "I can't hold it back any longer..."
      menu(side="middle"):
        extend ""
        "Cum inside":
          show isabelle lying_down_cum_inside with hpunch
          $quest.isabelle_locker["creampie"] = True
          # $isabelle["creampied"]+=1
          "My dick spasms inside her, shooting my load deep into her pussy."
          "Filling her up with hot semen."
          "[isabelle] moans — too far gone to notice or care."
          show isabelle lying_down_embrace1 with Dissolve(.5)
        "Pull out":
          show isabelle lying_down_cum_outside1 with vpunch
          "Just as the unstoppable wave of pleasure rips through me, I pull out.{space=-20}"
          show isabelle lying_down_cum_outside2 with Dissolve(.5)
          "My load splashes against her stomach and lower abdomen."
          "Marking her as mine for all to see."
          show isabelle lying_down_embrace2 with Dissolve(.5)
      "And then we just lie there in each other's arms as the sea of pleasure ebbs out..."
      "The afterglow of the orgasm leaves us speechless."
      "But words aren't needed anymore."
      "All I need is her arms around me. Our bodies close together."
      "A smile of satisfaction touches her lips."
      "Gently, she runs her fingers through my hair."
      "And it's such a tiny gesture, but it brings a lump to my throat and a tear to my eye."
      "I never thought I'd be able to please a woman — much less someone like [isabelle]..."
      "But that touch on my head is filled with love and approval."
      "It means the world to me."
      $unlock_replay("isabelle_judgment")
      window hide
      $quest.isabelle_locker["sleep"] = True
      call home_bedroom_bed_interact_force_sleep
      call screen time_passed
      pause 0.5
      window auto
      "Last night was crazy..."
      "I wish we could've cuddled for longer, but [isabelle] had to go celebrate her dad's birthday."
      "However, something tells me this wasn't a one-night stand. [isabelle] isn't that kind of girl."
      "So, where does this leave us? Is she my girlfriend now?"
      "I guess we'll talk it out like adults when the moment presents itself..."
      "Until then, I have this cat to take care of."
      mc "[spinach], are you hungry?"
      show spinach closedeyes with Dissolve(.5)
      spinach closedeyes "Meow?"
      mc "I'm sure we can find you some delicious squid somewhere around the house..."
    "{image=stats lust}|\"You're right — negotiations are over.\nIt's time for justice.\"":
      show isabelle angry at move_to(.5)
      mc "You're right — negotiations are over. It's time for justice."
      $isabelle.lust+=3
      isabelle skeptical "I'm going to stomp on their little idyll."
      mc "With iron boots."
      isabelle skeptical "Only scorched earth from here on out."
      isabelle concerned_left "Can you keep the cat in your room while I get things ready?"
      mc "What are you planning to do?"
      isabelle skeptical "We're just going to give them what they've been asking for..."
      show isabelle skeptical at disappear_to_left(.75)
      pause 0.75
      play sound "<from 9.2 to 10>door_breaking_in"
      with hpunch
      "Damn, she's like a hurricane!"
      "A cute storm with lightning eyes, ready to crush everything in her path..."
      "It's about time things changed around here."
      mc "What do you think, [spinach]? Is it time for a change?"
      show spinach neutral with Dissolve(.5)
      spinach neutral "Meow!"
      $quest.isabelle_locker["time_for_a_change"] = True
      $quest.isabelle_locker["finished_today"] = True
  hide spinach with Dissolve(.5)
  $quest.isabelle_locker.finish()
  return
