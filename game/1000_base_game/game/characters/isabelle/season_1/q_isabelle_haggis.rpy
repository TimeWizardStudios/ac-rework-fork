label isabelle_quest_haggis_deliver(item):
  if item=="haggis_lunchbox":
    show isabelle excited with Dissolve(.5)
    $mc.remove_item("haggis_lunchbox")
    isabelle excited "What is this?"
    mc "Have a taste."
    isabelle skeptical "Okay..."
    isabelle skeptical "..."
    isabelle afraid "Dear god! What in the blooming hell is that?!"
    show isabelle afraid at move_to(.75)
    menu(side="left"):
      extend ""
      "\"What, you don't like it?\"":
        show isabelle afraid at move_to(.5)
        mc "What, you don't like it?"
        isabelle cringe "It's awful."
        mc "Are you sure?"
        isabelle afraid "Yes, I'm bloody sure!"
        mc "I thought you loved everything Scottish?"
        isabelle thinking "What is this? Some kind of gotcha-moment?"
        mc "Gotcha!"
        isabelle cringe "Seriously? What's wrong with you?"
        mc "Just wanted to prove that not everything Scottish is great."
        isabelle annoyed "That's messed up. Obviously, there'll be things I don't like that are Scottish."
        isabelle annoyed "You don't have to prove that."
        $isabelle.love-=1
        isabelle sad "Can't I be allowed to like something without you trying to spoil it?"
        show isabelle sad at move_to(.75)
      "\"Haggis — the Scottish national dish.\"":
        show isabelle afraid at move_to(.5)
        mc "Haggis — the Scottish national dish."
        isabelle flirty "Oh, huh!"
        isabelle flirty "Maybe it's an acquired taste... I'll give it another shot."
        mc "Go ahead."
        isabelle laughing "Later! I'll save it!"
        isabelle laughing "Thanks for bringing it!"
        show isabelle laughing at move_to(.25)
        menu(side="right"):
          extend ""
          "\"I think you should have another taste now.\"":
            show isabelle laughing at move_to(.5)
            $mc.lust+=1
            mc "I think you should have another taste now."
            mc "Just to be sure you like it, I mean."
            isabelle flirty "Okay, fine... how bad can it be?"
            isabelle thinking "..."
            isabelle cringe "Good! It's... very good."
            mc "Are you sure? You look a little nauseous."
            isabelle cringe "I'm... fine..."
            show isabelle cringe at move_to(.75)
          "\"You're welcome!\"":
            show isabelle laughing at move_to(.5)
            mc "You're welcome!"
            $isabelle.love+=1
            isabelle confident "I appreciate the gesture."
            mc "You're a good sport, [isabelle]."
            isabelle laughing "I will eat it later. Maybe."
            mc "Don't worry! I won't be checking the trash cans if you change your mind."
            isabelle laughing "It's actually one of the most repulsive things I've tasted."
            mc "Hah! I knew it."
            show isabelle laughing at move_to(.75)
    show flora smile at appear_from_left(.25)
    flora smile "What's that smell?"
    isabelle annoyed_left  "It's this box of death."
    flora afraid "I think it smells nice!"
    "Always trust [flora] to be the contrarian."
    show flora afraid at move_to("left")
    show isabelle annoyed_left at move_to("right")
    menu(side="middle"):
      extend ""
      "\"Why don't you have a taste, then?\"":
        show flora afraid at move_to(.25)
        show isabelle annoyed_left at move_to(.75)
        mc "Why don't you have a taste, then?"
        flora flirty "I think I just might."
        mc "Okay, here you go..."
        flora thinking "..."
        flora blush "It's amazing!"
        isabelle afraid "Really?!"
        mc "[flora]'s always had some messed up taste buds."
        mc "She used to put ketchup on ice cream when she was little."
        flora laughing "It's not as bad as it sounds!"
        show flora laughing at move_to("left")
        show isabelle afraid at move_to("right")
        menu(side="middle"):
          extend ""
          "\"There's definitely something wrong with her.\"":
            show flora laughing at move_to(.25)
            show isabelle afraid at move_to(.75)
            mc "There's definitely something wrong with her."
            $isabelle.love-=1
            show flora annoyed
            isabelle annoyed "That's not very nice, [mc]."
            $flora.love-=1
            $flora.lust+=1
            flora annoyed "Yeah! You should take a look in the mirror some time!"
            show flora annoyed at move_to(.5)
            show isabelle annoyed at move_to("right")
          "\"I think it's cool that she's able to appreciate things others might not.\"":
            show flora laughing at move_to(.25)
            show isabelle afraid at move_to(.75)
            mc "I think it's cool that she's able to appreciate things others might not."
            $isabelle.love+=1
            show flora blush
            isabelle blush "You guys are adorable!"
            $flora.love+=1
            flora blush "I did not expect you to be mature about it!"
            "Earning [flora]'s respect is like the twelve Herculean labors combined. Where's my reward?"
            show flora blush at move_to(.5)
            show isabelle blush at move_to("right")
      "\"[flora], not now.\"":
        $quest.isabelle_haggis["chose_not_now"] = True  #tbd check if this bugs out
        show flora afraid at move_to(.25)
        show isabelle annoyed_left at move_to(.75)
        mc "[flora], not now."
        flora annoyed "What are you going to do about it? Steal my panties again?"
        mc "[flora]!"
        isabelle thinking "What is she talking about?"
        mc "Err, nothing! She's just trying to embarrass me!"
        flora confused "As if you need any help with that."
        flora sarcastic "He used to take them out of the laundry basket."
        mc "[flora], you're killing me..."
        isabelle cringe "That's highly inappropriate."
        mc "She's lying!"
        flora laughing "Yeah, that was a joke about the laundry basket."
        mc "Thank go—"
        $flora.lust+=1
        flora flirty "He usually tries to pull them off while I'm still wearing them!"
        $isabelle.lust-=1
        isabelle afraid "!!!"
        mc "Ugh, that's not—"
        show flora flirty at move_to(.5)
        show isabelle afraid at move_to("right")
      "\"[isabelle], put that lunch box in the trash, please.\"":
        show flora afraid at move_to(.25)
        show isabelle annoyed_left at move_to(.75)
        mc "[isabelle], put that lunch box in the trash, please."
        flora angry "No! I want a taste."
        mc "You're joking."
        show isabelle afraid
        flora confident "I'm one hundred percent serious!"
        mc "Well, it's your funeral."
        flora confident "Would you visit my grave?"
        show flora confident at move_to("left")
        show isabelle afraid at move_to("right")
        menu(side="middle"):
          extend ""
          "\"Of course! You actually mean a lot to me.\"":
            show flora confident at move_to(.25)
            show isabelle afraid at move_to(.75)
            mc "Of course! You actually mean a lot to me."
            $isabelle.love+=1
            show flora flirty
            isabelle flirty "Aww!"
            $flora.love+=1
            flora flirty "That's not what I expected from you!"
            flora laughing "I was sure you'd make it weird somehow!"
            mc "I'm full of surprises."
            show flora laughing at move_to(.5)
            show isabelle flirty at move_to("right")
          "\"Yeah... and I'd stake you, just to be sure.\"":
            $quest.isabelle_haggis["chose_stake"] = True
            show flora confident at move_to(.25)
            show isabelle afraid at move_to(.75)
            mc "Yeah... and I'd stake you, just to be sure."
            show isabelle cringe
            $flora.lust+=1
            $isabelle.love-=1
            flora concerned "Typical."
            mc "You wouldn't want to be a zombie-vampire, would you?"
            flora angry "Maybe I would! Stop making assumptions!"
            isabelle thinking "I agree with [flora]. Don't, err... stake without consent."
            mc "Can you two stop ganging up on me for trying to keep the world safe?"
            show flora angry at move_to(.5)
            show isabelle thinking at move_to("right")
    show mrsl surprised at appear_from_left("left")
    mrsl surprised "What's going on here?"
    show flora confused
    show isabelle concerned_left
    mrsl thinking "What's that smell?"
    menu(side="middle"):
      extend ""
      "\"It's [flora].\"":
        mc "It's [flora]."
        $flora.lust+=1
        if quest.isabelle_haggis["chose_stake"]:
          mc "Apparently, she's into graveyard fragrances."
        elif quest.isabelle_haggis["chose_not_now"]:
          mc "And her stinking lies."
        else:
          mc "She didn't shower this morning."
          flora annoyed "I'm going to poison your next meal..."
        show flora annoyed
        isabelle cringe "It's actually this lunch box."
      "\"[isabelle]'s favorite food.\"":
        mc "[isabelle]'s favorite food."
        $isabelle.love-=1
        isabelle cringe "It really isn't..."
        mrsl surprised "It does smell from that lunch box of yours."
        isabelle thinking "It's not mine! And I meant that it's not my favorite food."
      "\"I only smell your perfume... and it's heavenly.\"":
        $mc.charisma+=1
        mc "I only smell your perfume... and it's heavenly."
        $mrsl.lust+=1
        mrsl laughing "Thank you, [mc]! You're quite the charmer."
        $flora.lust-=1
        flora eyeroll "Quite the kiss-ass, more like."
        mrsl confident "Now, where does that smell come from?"
        isabelle cringe "It's this stupid lunch box."
    mrsl surprised "Don't you know that eating cooked meals outside the cafeteria is against the rules?"
    isabelle afraid "I didn't!"
    mrsl skeptical "Very well... you're new here, [isabelle]. But you two should know better."
    mc "I didn't eat anything!"
    show isabelle sad
    flora sarcastic "But he brought it."
    mc "Thanks a lot, [flora]..."
    mrsl thinking "I think you've all broken the rules to some extent, but I'm hesitant to give you detention..."
    mrsl neutral "Perhaps a lesson in communication and cooperation would be more suitable since you all seem to be in disagreement."
    mrsl neutral "Meet me in the homeroom."
    window hide
    show mrsl neutral at disappear_to_right(1.25) behind flora,isabelle
    show flora sarcastic at move_to(.25,1.25)
    show isabelle sad at move_to(.75,1.25)
    pause 0.5
    window auto show
    show flora confused with dissolve2
    flora confused "Well done, [mc]."
    window hide
    show flora confused at disappear_to_right behind isabelle
    show isabelle sad at move_to(.5,1.0)
    pause 0.5
    window auto show
    show isabelle annoyed with dissolve2
    isabelle annoyed "Yeah, this one's on you."
    window hide
    show isabelle annoyed at disappear_to_right
    pause 0.5
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.location = "school_homeroom"
    $school_homeroom["exclusive"] = ("flora","isabelle")
    $quest.isabelle_haggis.advance("trouble")
    $mc["focus"] = "isabelle_haggis"
    hide mrsl
    hide flora
    hide isabelle
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
  else:
    "Not sure why [isabelle] would want my [item.title_lower]."
    $quest.isabelle_haggis.failed_item("start",item)
    return

label isabelle_quest_haggis_mrsl_detention:
  show mrsl confident at Position(xalign=0.0)
  show flora concerned
  show isabelle concerned_left at Position(xalign=1.0)
  with Dissolve(.5)
  window auto
  mrsl confident "Here we are! Cooperation and teamwork are important skills to master."
  mrsl laughing "Hopefully, by the end of this exercise, you'll all have a better understanding of it."
  mrsl flirty "I'll leave you to it. Instructions are on the blackboard!"
  show mrsl flirty at disappear_to_left(.5)
  show flora concerned at move_to(.25)
  show isabelle concerned_left at move_to(.75)
  mc "Locked in with two cute girls. Is this really a punishment?" #(flora eyeroll) (isabelle eyeroll)
  show isabelle skeptical
  flora angry "It is for us."
  mc "This is almost like in your questionnaire!"
  flora angry "I've already repressed your answers."
  mc "That's probably for the best."
  $flora["hidden_now"] = flora["hidden_today"] = isabelle["at_none_now"] = isabelle["at_none_today"] = mrsl["at_none_now"] = mrsl["at_none_today"] = False
  hide flora
  hide isabelle
  with Dissolve(.5)
  $quest.isabelle_haggis.advance("instructions")
  $process_event("update_state") #Updates the AI
  return

label isabelle_quest_haggis_isabelle_cafeteria:
  show isabelle thinking with Dissolve(.5)
  mc "Hey, [isabelle]!"
  isabelle thinking "Hold on, [mc]. I'm mid-chew."
  hide isabelle with Dissolve(.5)
  "I should probably talk to her somewhere else..."
  return

label isabelle_quest_haggis_detention_start:
  show flora annoyed flip at Position(xalign=.25)
  show isabelle smile at Position(xalign=.75)
  with Dissolve(.5)
  mc "So, what now?"
  isabelle smile_left "I think we should start by reading the instructions."
  flora sarcastic flip "I'm not sure [mc] actually knows how to read."
  isabelle laughing "I'm sure that's not true!"
  flora annoyed flip "It is! That's why he's reading comic books! Lots of pictures."
  "Ugh... what did that quote in the English classroom say again?"
  "Oh, right!"
  mc "A picture is worth a thousand words!"
  flora sarcastic flip "And you're worth one word — \"clown.\""
  isabelle confident "Hey, maybe we should get this done?"
  mc "Yeah! Stop wasting time, [flora]."
  flora eyeroll flip "Whatever."
  hide flora
  hide isabelle
  with Dissolve(.5)
  return

label isabelle_quest_haggis_isabelle_detention:
  show isabelle neutral with Dissolve(.5)
  mc "So, who do you think should get the reward?"
  isabelle excited "I've read about this type of exercise! It's not about who gets the reward, it's about how you solve a problem together."
  mc "But if you had to pick, who would it be?"
  isabelle concerned_left "Well, I'm really the victim in all of this. You brought the stinking lunch box, and then you both started arguing..."
  isabelle concerned "I think it's only fair I get the reward."
  isabelle concerned "What do you think?"
  show isabelle concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"[flora] should get the reward.\"":
      show isabelle concerned at move_to(.5)
      mc "[flora] should get the reward."
      if quest.isabelle_haggis["isabelle_endorse"]=="flora":
        isabelle excited "All right, I agree."
        isabelle excited "[flora] is a nice girl, she deserves it. But more importantly, I'm happy we could solve it!"
        isabelle neutral "I reckon we must wait for [mrsl] to return now."
        mc "I guess so."
        show black with fadehold
        $game.advance()
        call isabelle_quest_haggis_isabelle_winner("flora")
        return
      else:
        mc "I haven't always been the best, so I'd like to do something nice for her..."
        isabelle excited "That's kind of you, but you can't buy someone's forgiveness."
    "\"You should get the reward.\"":
      show isabelle concerned at move_to(.5)
      mc "You should get the reward."
      if quest.isabelle_haggis["flora_endorse"]=="isabelle":
        mc "...and I've managed to convince [flora]."
        mc "It was my fault that this thing even happened, and her fault we were fighting."
        show isabelle concerned at move_to(.25)
        show flora annoyed at appear_from_right(.75)
        flora annoyed "Seriously? It's always your fault, [mc]!" #(isabelle afraid)
        mc "Well, anyway. Moving on!"
        show isabelle neutral
        flora eyeroll "Gross, but I guess the deed is done..."
        flora sarcastic "The only positive thing is that [isabelle] gets the reward, and you don't."
        show black with fadehold
        $game.advance()
        call isabelle_quest_haggis_isabelle_winner("isabelle")
        return
      else:
        mc "...but I doubt [flora] will agree. She's pretty greedy."
        isabelle laughing "I'm sure she's all right! Most people would say they're the most deserving."
    "\"I should get the reward!\"" if not quest.isabelle_haggis["mc_lost"]:
      show isabelle concerned at move_to(.5)
      mc "I should get the reward!"
      if quest.isabelle_haggis["flora_endorse"]=="mc" and quest.isabelle_haggis["isabelle_endorse"]=="mc":
        isabelle excited "I guess that concludes the challenge..."
        isabelle excited "If I'm going to be honest, I didn't think you'd get the reward."
        mc "Me neither. I probably don't deserve it."
        isabelle laughing "Oh, stop it! You're as deserving as anyone else."
        isabelle confident "Let's wait for [mrsl] now."
        show black with fadehold
        $game.advance()
        call isabelle_quest_haggis_isabelle_winner("mc")
        return
      else:
        mc "I brought you a meal. I was trying to be nice."
        isabelle cringe "You said yourself that it was a gotcha-moment..."
        mc "Well, if you hadn't gushed so hard over Scotland, I wouldn't have brought you the haggis..."
        isabelle thinking "That's not a very compelling argument. Nobody forced you to bring me the haggis."
    "This requires more pondering":
      pass
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_haggis_flora_detention:
  show flora blush with Dissolve(.5)
  flora blush "I already know what you're going to ask, and the answer is simple..."
  flora laughing "Either I get the reward or we're stuck here forever!"
  mc "Do you think you deserve it?"
  flora laughing "Without a doubt!"
  mc "What about [isabelle]?"
  flora thinking "What about her?"
  mc "Ugh."
  show flora thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You should get the reward.\"":
      show flora thinking at move_to(.5)
      mc "You should get the reward."
      if quest.isabelle_haggis["isabelle_endorse"]=="flora":
        flora laughing "I'm glad everyone found their senses!"
        flora excited "I can't wait to get that reward! Another one for my trophy shelf!"
        mc "Can you be any more insufferable?"
        flora angry "What? Are you going to rain on my parade now?"
        flora angry "That's what sore losers do!"
        mc "Okay, fine... just stop being so annoying. Else I might change my mind."
        flora confident "Fine, I'll save my gloating for later..."
        flora neutral "Let's wait for [mrsl] now."
        show black with fadehold
        $game.advance()
        call isabelle_quest_haggis_isabelle_winner("flora")
        return
      else:
        flora afraid "Really?"
        mc "I know how hard you work, and it's my fault we're here."
        flora smile "Finally, something we agree on!"
        flora smile "How do we convince [isabelle]?"
        mc "I have a plan."
        flora skeptical "Those words always worry me..."
    "\"[isabelle] should get the reward.\"":
      show flora thinking at move_to(.5)
      mc "[isabelle] should get the reward."
      if quest.isabelle_haggis["flora_endorse"]=="isabelle":
        flora confused "All right... let's get out of here."
        show isabelle smile at appear_from_left(.25)
        show flora confused at move_to(.75)
        isabelle smile "Thanks, guys! I think you're both equally as deserving, but I appreciate it."
        flora sarcastic "You're the nicest out of us three. You're especially nicer than [mc]!"
        mc "Hey..."
        isabelle laughing "You're sweet, [flora]!"
        mc "Ugh, let's just wait for [mrsl]."
        show black with fadehold
        $game.advance()
        call isabelle_quest_haggis_isabelle_winner("isabelle")
        return
      else:
        mc "She's suffered through enough of our kibble... she should get something out of this."
        flora annoyed "So what? I have to suffer your bullshit around the clock!"
        "[flora] actually has a point there..."
    "\"I should get the reward!\"":
      show flora thinking at move_to(.5)
      mc "I should get the reward!"
      if quest.isabelle_haggis["flora_endorse"]=="mc" and quest.isabelle_haggis["isabelle_endorse"]=="mc":
        if school_homeroom["cupgame_played"]:
          flora eyeroll "Typical of you to rub it in."
          mc "Well, I think it's the first time I've ever won something..."
          flora annoyed "Winning doesn't suit you very well."
          show isabelle smile at appear_from_left(.25)
          show flora annoyed at move_to(.75)
          isabelle smile "Hey, I don't think it's about winning. We solved the challenge together."
          isabelle laughing "We should all be happy!"
          flora sarcastic  "You're right. The biggest prize is getting out of here and away from [mc]!"
          mc "..."
        else:
          flora confused "Yeah, whatever... I'm tired of being in this classroom."
        show black with fadehold
        $game.advance()
        call isabelle_quest_haggis_isabelle_winner("mc")
        return
      else:
        mc "Please? I've never won anything..."
        flora eyeroll "Of course you'd say that."
        flora eyeroll "You always seem to think that the world owes you something."
    "I need some more time to ruminate":
      pass
  hide flora with Dissolve(.5)
  return

label isabelle_quest_haggis_isabelle_winner(winner):
  show isabelle neutral at Position(xalign=0.0)
  show mrsl neutral at Position(xalign=.5)
  show flora neutral at Position(xalign=1.0)
  hide black
  with Dissolve(.5)
  mrsl neutral "Okay... so you've come to a conclusion, I hope?"
  mc "Yeah, we did."
  mrsl excited "Very well! Who did you decide on for the reward?"
  if winner == "flora":
    flora excited "I won!"
    mrsl excited "Indeed?"
    mrsl concerned "It's not really about winning, though. It's about the teamwork that went into the decision."
    mc "See, what did I tell you?"
    isabelle concerned "Don't start now, else she might make us start over!"
    mrsl thinking "That's a good point, [isabelle]. Moving forward, you should all keep this lesson in mind..."
    mrsl excited "Now, [flora], here's the reward. It's nothing big, just a brooch that shows you're a problem solver. I hope you like it..."
    $flora["brooch"] = True
    #Permanently add Lichtenstein's Brooch to Flora's top.
    flora smile "Whoa! I love it!"
    flora smile "Thanks so much, [mrsl]!"
    mrsl neutral "You should thank your classmates first and foremost."
    flora laughing "Thanks, [isabelle]!"
    isabelle laughing "No worries!"
    flora concerned "[mc], I'll maybe thank you later."
    mc "Typical..."
    mrsl excited "Okay, you're all dismissed!"
  elif winner == "isabelle":
    mc "We decided to give the reward to [isabelle]."
    isabelle blush "They're too nice!"
    mrsl excited "A good choice. How did you decide that?"
    flora laughing "Through careful deliberation and teamwork."
    mc "I guess, yeah."
    mrsl neutral "Very well! I'm glad you managed to work it out."
    mrsl neutral "The reward isn't that big, just this little brooch that shows you're a problem solver..."
    $isabelle["brooch"] = True
    #Permanently add Lichtenstein's Brooch to Isabelle's top
    isabelle excited "Does it look good?"
    mc "I think it fits you, [isabelle]!"
    flora blush "Yeah! I'm a little jelly!"
    mrsl excited "Well, the real prize is that you all came out better people by the end of the challenge."
    mrsl excited "Okay, you're all dismissed!"
  elif winner == "mc":
    isabelle smile "We decided to give [mc] the reward."
    mrsl surprised "Oh?"
    mc "Is that a problem?"
    mrsl surprised "No, no! I'm just a little surprised, that's all..."
    flora sarcastic "I think everyone's surprised. He hasn't won anything before."
    mc "Thanks a lot, [flora]..." #(isabelle laughing)
    mrsl excited "It's not about winning, it's about teamwork."
    mrsl excited "And if you've come to an agreement, you're all winners!" #(isabelle smile)
    mrsl neutral "Now, the reward isn't very big, but hopefully, you'll like it."
    $mc.add_item("mrsl_brooch")
    isabelle confident "Looks good on you, [mc]!"
    flora annoyed "It doesn't look terrible..."
    mrsl excited "Okay, you're all dismissed!"
  hide isabelle
  hide mrsl
  hide flora
  with Dissolve(.5)
  $quest.isabelle_haggis.finish()
  return

label isabelle_quest_haggis_cup_game:
  menu(side="middle"):
    "Call [flora] over":
      $school_homeroom["cupgame_played"] = True
      mc "Hey, [flora]! I want to play a game..."
      show flora annoyed at appear_from_right
      flora annoyed "Very funny."
      flora eyeroll "What now?"
      mc "If you can guess which cup the eraser is under, I'll give up my claim on the reward."
      mc "If you can't guess, you'll have to give up your claim on the reward."
      flora sarcastic "Okay, should be easy enough."
      mc "Great. Let's begin!"
      "Okay, under which cup should the eraser go?"
      show flora sarcastic at move_to(.75)
      menu(side="left"):
        extend ""
        "Left":
          show flora sarcastic at move_to(.5)
          window hide
          call isabelle_quest_haggis_cup_game_result("left")
          return
        "Middle":
          show flora sarcastic at move_to(.5)
          window hide
          call isabelle_quest_haggis_cup_game_result("middle")
          return
        "Right":
          show flora sarcastic at move_to(.5)
          window hide
          call isabelle_quest_haggis_cup_game_result("right")
          return
        "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|Rig the game":
          show flora sarcastic at move_to(.5)
          window hide
          $mc.add_item("eraser")
          window auto
          "Hopefully, she didn't see me dropping the eraser into my pocket..."
          window hide
          $quest.isabelle_haggis["table_cup"] = 0
          $quest.isabelle_haggis["table_eraser"] = False
          show school homeroom cup_left as cup1 at Position(xpos=1112,ypos=724,xanchor=0,yanchor=0)
          show school homeroom cup_right as cup2 at Position(xpos=1229,ypos=721,xanchor=0,yanchor=0)
          show school homeroom cup_middle as cup3 at Position(xpos=1174,ypos=733,xanchor=0,yanchor=0)
          play sound "cup_on_table"
          show layer master:
            linear 1 zoom 4 xoffset -3880 yoffset -2440
          pause 1.0
          show school homeroom cup_left as cup1:
            linear 0.5 xpos 1229 ypos 721
          show school homeroom cup_right as cup2:
            linear 0.5 xpos 1112 ypos 724
          pause 0.5
          show school homeroom cup_left as cup1 zorder 1:
            linear 0.5 xpos 1174 ypos 733
          show school homeroom cup_middle as cup3:
            linear 0.5 xpos 1229 ypos 721
          pause 0.5
          show school homeroom cup_right as cup2:
            linear 0.5 xpos 1229 ypos 721
          show school homeroom cup_middle as cup3:
            linear 0.5 xpos 1112 ypos 724
          pause 0.5
          show school homeroom cup_left as cup1 zorder 0:
            linear 0.5 xpos 1112 ypos 724
          show school homeroom cup_middle as cup3 zorder 1:
            linear 0.5 xpos 1174 ypos 733
          pause 0.5
          show school homeroom cup_right as cup2 zorder 1:
            linear 0.5 xpos 1174 ypos 733
          show school homeroom cup_middle as cup3 zorder 0:
            linear 0.5 xpos 1229 ypos 721
          pause 0.5
          hide cup1
          hide cup2
          hide cup3
          show school homeroom cup_left as cup1 at Position(xpos=1112,ypos=724,xanchor=0,yanchor=0) behind flora
          show school homeroom cup_middle as cup3 at Position(xpos=1229,ypos=721,xanchor=0,yanchor=0) behind flora
          show school homeroom cup_right as cup2 at Position(xpos=1174,ypos=733,xanchor=0,yanchor=0) behind flora
          window auto
          mc "Well, which one is it?"
          flora sarcastic "Easy! Right one."
          show school homeroom cup_middle as cup3:
            linear .5 yanchor 0.5
          mc "Nope."
          show school homeroom cup_middle as cup3:
            linear .5 yanchor 0.0
          show flora cringe
          flora cringe "What the hell? I could've sworn you put it there!"
          show layer master:
            zoom 4 xoffset -3880 yoffset -2440
            linear .5 zoom 1 xoffset 0 yoffset 0
          mc "Well, that's too bad."
          jump isabelle_quest_haggis_cup_game_again
    "Not now":
      return
  return

label isabelle_quest_haggis_cup_game_result(choice):
  pause 0.25
  $quest.isabelle_haggis["table_cup"] = 0
  $quest.isabelle_haggis["table_eraser"] = False
  show school homeroom cup_left as cup1 at Position(xpos=1112,ypos=724,xanchor=0,yanchor=0)
  show school homeroom cup_right as cup2 at Position(xpos=1229,ypos=721,xanchor=0,yanchor=0)
  show school homeroom cup_middle as cup3 at Position(xpos=1174,ypos=733,xanchor=0,yanchor=0)
  play sound "cup_on_table"
  show layer master:
    linear 1 zoom 4 xoffset -3880 yoffset -2440
  pause 1.0
  show school homeroom cup_left as cup1:
    linear 0.5 xpos 1229 ypos 721
  show school homeroom cup_right as cup2:
    linear 0.5 xpos 1112 ypos 724
  pause 0.5
  show school homeroom cup_left as cup1 zorder 1:
    linear 0.5 xpos 1174 ypos 733
  show school homeroom cup_middle as cup3:
    linear 0.5 xpos 1229 ypos 721
  pause 0.5
  show school homeroom cup_right as cup2:
    linear 0.5 xpos 1229 ypos 721
  show school homeroom cup_middle as cup3:
    linear 0.5 xpos 1112 ypos 724
  pause 0.5
  show school homeroom cup_left as cup1 zorder 0:
    linear 0.5 xpos 1112 ypos 724
  show school homeroom cup_middle as cup3 zorder 1:
    linear 0.5 xpos 1174 ypos 733
  pause 0.5
  show school homeroom cup_right as cup2 zorder 1:
    linear 0.5 xpos 1174 ypos 733
  show school homeroom cup_middle as cup3 zorder 0:
    linear 0.5 xpos 1229 ypos 721
  pause 0.5
  hide cup1
  hide cup2
  hide cup3
  show school homeroom cup_left as cup1 at Position(xpos=1112,ypos=724,xanchor=0,yanchor=0) behind flora
  show school homeroom cup_middle as cup3 at Position(xpos=1229,ypos=721,xanchor=0,yanchor=0) behind flora
  show school homeroom cup_right as cup2 at Position(xpos=1174,ypos=733,xanchor=0,yanchor=0) behind flora
  window auto
  "Okay, let's see if she got it right."
  mc "Where's the eraser?"
  if choice=="middle":
    $quest.isabelle_haggis["table_eraser"] = True
    flora thinking "Middle one!"
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.5
    mc "Crap. You got it."
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.0
    show flora laughing
    flora laughing "Yes!"
    show layer master:
      zoom 4 xoffset -3880 yoffset -2440
      linear .5 zoom 1 xoffset 0 yoffset 0
    "Oh, well... the reward probably sucks anyway."
    $quest.isabelle_haggis["table_cup"] = 3
    $quest.isabelle_haggis["mc_lost"] = True
    $quest.isabelle_haggis["flora_endorse"]="isabelle"
    window hide
    hide cup1
    hide cup2
    hide cup3
    hide flora
    with Dissolve(.5)
    return
  else:
    flora "Middle one!"
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.5
    mc "Wrong!"
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.0
    show flora cringe
    flora cringe "What the hell? I could've sworn you put it there!"
    show layer master:
      zoom 4 xoffset -3880 yoffset -2440
      linear .5 zoom 1 xoffset 0 yoffset 0
    mc "Well, that's too bad."
    jump isabelle_quest_haggis_cup_game_again

label isabelle_quest_haggis_cup_game_again:
  flora annoyed "Bullshit. Let's play again. Best of three!"
  show flora annoyed at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Sure, but we up the stakes.\"":
      show flora annoyed at move_to(.5)
      mc "Sure, but we up the stakes."
      flora confused "What do you mean?"
      mc "If you lose again, you'll have to accept a dare."
      flora annoyed "Well, I'm not getting naked if that's what you're thinking."
      "Crap."
      mc "Err, okay... I wasn't thinking that!"
      flora eyeroll "You're gross."
      mc "I said I wasn't thinking that!"
      flora annoyed "Let's just play."
      mc "Okay, okay!"
      if mc.owned_item("eraser"):
        $mc.remove_item("eraser")
      "Under which cup should the eraser go this time?"
      show flora annoyed at move_to(.75)
      menu(side="left"):
        extend ""
        "Left":
          call isabelle_quest_haggis_cup_game_round_2("left")
          return
        "Middle":
          call isabelle_quest_haggis_cup_game_round_2("middle")
          return
        "Right":
          call isabelle_quest_haggis_cup_game_round_2("right")
          return
    "\"You lost. Get over it.\"":
      show flora annoyed at move_to(.5)
      mc "You lost. Get over it."
      flora eyeroll "Whatever. I don't really care anyway."
      show flora eyeroll at move_to(.75)
      call isabelle_quest_haggis_flora_endorse("eyeroll")
  return

label isabelle_quest_haggis_cup_game_round_2(choice):
  show flora annoyed at move_to(.5)
  window hide
  pause 0.25
  hide cup1
  hide cup2
  hide cup3
  show school homeroom cup_left as cup1 at Position(xpos=1112,ypos=724,xanchor=0,yanchor=0)
  show school homeroom cup_right as cup2 at Position(xpos=1229,ypos=721,xanchor=0,yanchor=0)
  show school homeroom cup_middle as cup3 at Position(xpos=1174,ypos=733,xanchor=0,yanchor=0)
  play sound "cup_on_table"
  show layer master:
    linear 1 zoom 4 xoffset -3880 yoffset -2440
  pause 1.0
  show school homeroom cup_left as cup1:
    linear 0.5 xpos 1229 ypos 721
  show school homeroom cup_right as cup2:
    linear 0.5 xpos 1112 ypos 724
  pause 0.5
  show school homeroom cup_left as cup1 zorder 1:
    linear 0.5 xpos 1174 ypos 733
  show school homeroom cup_middle as cup3:
    linear 0.5 xpos 1229 ypos 721
  pause 0.5
  show school homeroom cup_right as cup2:
    linear 0.5 xpos 1229 ypos 721
  show school homeroom cup_middle as cup3:
    linear 0.5 xpos 1112 ypos 724
  pause 0.5
  show school homeroom cup_left as cup1 zorder 0:
    linear 0.5 xpos 1112 ypos 724
  show school homeroom cup_middle as cup3 zorder 1:
    linear 0.5 xpos 1174 ypos 733
  pause 0.5
  show school homeroom cup_right as cup2 zorder 1:
    linear 0.5 xpos 1174 ypos 733
  show school homeroom cup_middle as cup3 zorder 0:
    linear 0.5 xpos 1229 ypos 721
  pause 0.5
  hide cup1
  hide cup2
  hide cup3
  show school homeroom cup_left as cup1 at Position(xpos=1112,ypos=724,xanchor=0,yanchor=0) behind flora
  show school homeroom cup_middle as cup3 at Position(xpos=1229,ypos=721,xanchor=0,yanchor=0) behind flora
  show school homeroom cup_right as cup2 at Position(xpos=1174,ypos=733,xanchor=0,yanchor=0) behind flora
  window auto
  mc "Which one, [flora]?"
  if choice == "middle":
    $quest.isabelle_haggis["table_eraser"] = True
    flora thinking "Hmm... middle one!"
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.5
    mc "Damn it, [flora]!"
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.0
    show flora laughing
    flora laughing "Woo! Easy!"
    show layer master:
      zoom 4 xoffset -3880 yoffset -2440
      linear .5 zoom 1 xoffset 0 yoffset 0
    mc "Congratulations, I guess."
    $quest.isabelle_haggis["table_cup"] = 3
    $quest.isabelle_haggis["mc_lost"] = True
    $quest.isabelle_haggis["flora_endorse"] = "isabelle"
    window hide
    hide cup1
    hide cup2
    hide cup3
    hide flora
    with Dissolve(.5)
    return
  else:
    flora "Hmm... middle one!"
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.5
    mc "Sorry, [flora]. Better luck next time."
    show school homeroom cup_right as cup2:
      linear .5 yanchor 0.0
    flora "You cheated!"
    show layer master:
      zoom 4 xoffset -3880 yoffset -2440
      linear .5 zoom 1 xoffset 0 yoffset 0
    mc "I would never cheat. I take all games very seriously."
    flora annoyed "I can't believe this."
    mc "I guess it's just my lucky day. Now for the dare..."
    flora cringe "I'm not eating anything gross, and I'm not getting naked."
    mc "Your dare is to kiss [isabelle]."
    flora cringe "No way, you perv!"
    mc "On the mouth."
    flora eyeroll "Not happening."
    show flora eyeroll at move_to(.25)
    menu(side="right"):
      extend ""
      "?mc.charisma>=3@[mc.charisma]/3|{image=stats cha}|\"She's smart, kind, and a total bookworm. You'd make a perfect couple!\"":
        show flora eyeroll at move_to(.5)
        mc "She's smart, kind, and a total bookworm. You'd make a perfect couple!"
        flora sarcastic "In other words, she's your polar opposite?"
        mc "Well, I wouldn't go that far—"
        flora sarcastic "You're right, she's also pretty. Wait, that's still your opposite!"
        mc "..."
        mc "I don't know what the big deal is. It's just a dare."
        flora annoyed "And you're just a pervert."
        mc "I beat you fair and square. And it could've been worse."
        flora annoyed "How?"
        mc "I could've dared you to kiss anyone, but I picked a girl that you think is cute."
        mc "Just pay your dues."
        flora eyeroll "..."
      "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"Oh? Does that mean I can back out of the slug deal?\"":
        show flora eyeroll at move_to(.5)
        mc "Oh? Does that mean I can back out of the slug deal?"
        flora afraid "What?! No!"
        mc "Well, then you know what to do..."
        flora sad "Have I ever told you that I hate you?"
        mc "No, this is the first time. And I'm deeply wounded."
        mc "Only a kiss will heal me. Meaning, you kissing [isabelle]."
        flora skeptical "Whatever, perv."
      "\"I'll let you buy your way out of it.\"":
        show flora eyeroll at move_to(.5)
        mc "I'll let you buy your way out of it."
        flora confused "How much?"
        mc "Hmm... ten bucks."
        "She's not going to do it anyway. This way, I at least get something out of it."
        flora annoyed "All right, fine."
        $mc.money+=10
        show flora annoyed at move_to(.75)
        call isabelle_quest_haggis_flora_endorse("annoyed")
        return
  $unlock_replay("floras_dare")
  mc "Come on, get to it."
  flora angry "Ugh, fine..."
  flora confident "[isabelle], can you come here for a moment?"
  show isabelle excited at appear_from_right(.75) behind flora
  show flora confident at move_to(.25)
  isabelle excited "What's up?"
  show flora confident at move_to(.35)
  flora confident "..."
  isabelle concerned_left "..."
  show flora confident at move_to(.55)
  pause(.5)
  show flora FloraIsabelleKiss with Dissolve(.5)
  "It's probably wrong to be turned on by what many courts would consider an assault, but you only live once, right?"
  "[isabelle]'s surprise adds extra spice... a hint of non-con, but it's still rather playful."
  "[flora]'s usually never one to back out of a dare, but she seems to be enjoying it."
  "There are things about her that even I don't know."
  "Two straight girls kissing, though..."
  "And the way their boobs squish together..."
  "Hot!"
  show isabelle afraid
  show flora afraid at Position(xalign=.25)
  with Dissolve(.5)
  isabelle afraid "What the hell?!"
  flora embarrassed "Oh, I'm so sorry! I tripped!"
  flora embarrassed "I didn't mean for that to happen! God, I'm so embarrassed!"
  isabelle laughing "Aw, it's okay! Don't worry about it."
  isabelle smile_left "Seriously, it's no big deal! Don't beat yourself up."
  show flora embarrassed at move_to(.5)
  hide isabelle with Dissolve(.5)
  flora skeptical "I hope you're happy."
  mc "You have no idea!"
  flora skeptical "Gross."
  $quest.isabelle_haggis["flora_kissed"] = True
  show flora skeptical at move_to(.75)
  call isabelle_quest_haggis_flora_endorse("skeptical")
  return

label isabelle_quest_haggis_flora_endorse(flora_expression):
  menu(side="left"):
    extend ""
    "\"I also want you to endorse me in getting the reward.\"":
      if flora_expression == "annoyed":
        show flora annoyed at move_to(.5)
      elif flora_expression == "eyeroll":
        show flora eyeroll at move_to(.5)
      elif flora_expression == "skeptical":
        show flora skeptical at move_to(.5)
      mc "I also want you to endorse me in getting the reward."
      flora annoyed "Yeah, that's fine. Just get me out of here."
      $quest.isabelle_haggis["flora_endorse"] = "mc"
    "\"I also want you to endorse [isabelle] in getting the reward.\"":
      if flora_expression == "annoyed":
        show flora annoyed at move_to(.5)
      elif flora_expression == "eyeroll":
        show flora eyeroll at move_to(.5)
      elif flora_expression == "skeptical":
        show flora skeptical at move_to(.5)
      mc "I also want you to endorse [isabelle] in getting the reward."
      flora sarcastic "Better her than you."
      $quest.isabelle_haggis["flora_endorse"] = "isabelle"
  $quest.isabelle_haggis["table_cup"] = 3
  $quest.isabelle_haggis["table_eraser"] = True
  window hide
  hide cup1
  hide cup2
  hide cup3
  hide flora
  with Dissolve(.5)
  return

label isabelle_quest_haggis_give_flora(item):
  if item == "haggis_lunchbox" and quest.isabelle_haggis >= "puzzle":
    $mc.remove_item(item)
    show flora excited with Dissolve(.5)
    flora excited "Oh, good! I'm freaking starving!"
    mc "You're probably the only person in the school that likes this food..."
    flora excited "It's an acquired taste!"
    if not quest.isabelle_haggis["haggis_bracelet"]:
      flora confident "I'd like you to have this as thanks for saving me from starvation!"
      $mc.add_item("flora_bracelet")
      $flora.love+=1
      $flora.lust+=1
      mc "Can I also have the reward?"
      $quest.isabelle_haggis["haggis_bracelet"] = True
    else:
      mc "Can I have the reward?"
    flora annoyed "Yeah, whatever. Take it. I'm tired of being in this classroom..."
    flora sarcastic "But seriously, though. Have a taste of this! It's fantastic!"
    mc "Pass."
    $quest.isabelle_haggis["flora_endorse"] = "mc"
  elif item == "greasy_bolt":
    show flora neutral with Dissolve(.5)
    mc "[flora], look! Someone drew an octopus on the blackboard!"
    flora excited "Really?!"
    "..."
    $mc.remove_item(item)
    $mc.add_item("bolt")
    flora afraid "What the hell?"
    "Crap."
    flora angry "Did you just wipe off the grease from that bolt on my shirt...?"
    mc "Maybe..."
    $flora.love-=1
    flora "What's wrong with you?"
    mc "Sorry!"
  else:
    "Some people have sticky fingers."
    "[flora] has radioactive fingers."
    "I'm not giving her my [item.title_lower]."
    $quest.isabelle_haggis.failed_item("puzzle_flora",item)
  hide flora with Dissolve(.5)
  return

label isabelle_quest_haggis_give_seconds_isabelle(item):
  if item == "haggis_lunchbox":
    show isabelle sad with Dissolve(.5)
    mc "Here's another lunch box of haggis."
    $mc.remove_item(item)
    isabelle sad "All right..."
    show isabelle sad at move_to(.25)
    menu(side="right"):
      extend ""
      "\"I see that you're already regretful. Don't eat it.\"":
        show isabelle sad at move_to(.5)
        mc "I see that you're already regretful. Don't eat it."
        isabelle sad "But I'd hate to be disrespectful..."
        mc "Come on, it's just a dish. There's no need to hold yourself to medieval traditions."
        isabelle sad "Do you really think so?"
        mc "Yeah! I know you're a great person. No need to prove yourself."
        isabelle blush "Thank you, [mc]."
        mc "Give me the lunch box and I'll throw it away."
        $mc.add_item(item)
        isabelle blush "This was really nice and mature of you! I think you should have the reward."
        if not quest.isabelle_haggis["haggis_regretful"]:
          $isabelle.love+=1
          $quest.isabelle_haggis["haggis_regretful"] =  True
        $quest.isabelle_haggis["isabelle_endorse"] = "mc"
      "\"Bon appetit!\"":
        show isabelle sad at move_to(.5)
        mc "Bon appetit!"
        $isabelle.love-=1
        isabelle thinking "..."
        isabelle cringe "Very... ugh.... tasty..."
        "[isabelle] looks like she's about to throw up."
        isabelle thinking "..."
        isabelle cringe "That's... cough... all of it..."
        isabelle cringe "Do you have anything to wash my mouth out with?"
        show isabelle cringe at move_to(.75)
        $item_count = mc.owned_item_count("water_bottle")
        menu(side="left"):
          extend ""
          "\"No, sorry.\"":
            show isabelle cringe at move_to(.5)
            mc "No, sorry."
            isabelle afraid "Excuse me, [flora]... do you have anything I can drink?"
            show isabelle afraid at move_to(.25)
            show flora excited flip at appear_from_right(.75)
            flora excited flip "Sure thing! I always bring water with me. Here, have a sip."
            isabelle laughing flip "Thank you!"
            flora laughing flip "No problem, friend!"
            isabelle smile_left flip "[flora], I think you should have the reward for being so nice!"
            flora flirty flip "Thanks! I do my best to weigh up my [mc]'s cruelties."
            $quest.isabelle_haggis["isabelle_endorse"]="flora"
            hide flora
          "?mc.owned_item('water_bottle')@[item_count]/1|{image=items bottle water_bottle}|\"Yeah, I have this bottle of water!\"":
            show isabelle cringe at move_to(.5)
            mc "Yeah, I have this bottle of water!"
            $mc.remove_item("water_bottle")
            $mc.add_item("empty_bottle")
            isabelle flirty "Thank god! It was like a graveyard in my mouth."
            isabelle flirty "Thank you so much!"
            mc "No worries. I'll forgive you for your graveyard comment."
            isabelle cringe "Oh shit, you're right! I'm sorry..."
            isabelle laughing "That haggis was great!"
            isabelle confident "I think you should have the reward. That water saved my life."
            $quest.isabelle_haggis["isabelle_endorse"]="mc"
    hide isabelle
    with Dissolve(.5)
  else:
    "[isabelle]'s way too sophisticated to appreciate my [item.title_lower]."
    "Then again, very few people would appreciate my [item.title_lower]..."
    $quest.isabelle_haggis.failed_item("puzzle",item)
  return

label isabelle_quest_haggis_use_coffee_cup_school_bench(item):
  if quest.isabelle_haggis["table_cup"] == 0:
    $quest.isabelle_haggis["table_cup"] = 1
    $mc.remove_item("coffee_cup")
    "Two girls..."
    "One cup..."
    "..."
    "Oh no!"
  elif quest.isabelle_haggis["table_cup"] == 1:
    $quest.isabelle_haggis["table_cup"] = 2
    $mc.remove_item("coffee_cup")
    "A modern art piece if I ever saw one. Minimalism at its finest."
  elif quest.isabelle_haggis["table_cup"] == 2:
    $quest.isabelle_haggis["table_cup"] = 3
    $mc.remove_item("coffee_cup")
    "Three of Cups. Reminds me of the tarot reading I got as a kid. "
    "Great fortune was supposed to be waiting for me right around the corner..."
    "Must've taken the wrong turn because the only thing that awaited there was great failures."
  else:
    "It's never a good idea to put everything on the table. Especially not my [item.title_lower]."
    $quest.isabelle_haggis.failed_item("homeroom_desk",item)
  return

label isabelle_quest_haggis_use_eraser_school_bench(item):
  "May this offering to the table gods erase the insults carved into the counter."
  $mc.remove_item(item)
  $quest.isabelle_haggis["table_eraser"]=True
  return

label isabelle_quest_haggis_give_bayonets_etiquettes_isabelle(item):
  if item=="bayonets_etiquettes":
    if mc.owned_item("monkey_wrench"):
      show isabelle neutral with Dissolve(.5)
      mc "Hey, [isabelle]! Did you know that spitting out your haggis is considered extremely rude in Scotland?"
      mc "In medieval times, it was cause for exile."
      isabelle concerned "Well, I didn't spit it out. I just didn't like it..."
      mc "That's actually worse!"
      mc "Hating haggis was considered worse than treason."
      isabelle skeptical "I don't believe you."
      mc "It says right here in this book on etiquette."
      mc "You can read it if you like."
      $mc.remove_item(item)
      isabelle concerned "Hm, all right. I'll save it for later... I'm sorry if I offended you with my dislike for haggis."
      show isabelle concerned at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I'm not Scottish, so no offense taken.\"":
          show isabelle concerned at move_to(.5)
          mc "I'm not Scottish, so no offense taken."
          $isabelle.love+=1
          isabelle blush "Thank god. I always try to be respectful of other cultures."
          isabelle sad "Scotland is my favorite country, though. I should've known this haggis fact."
        "\"This is quite the slight on Scotland.\"":
          show isabelle concerned at move_to(.5)
          mc "This is quite the slight on Scotland."
          isabelle sad "I'm really sorry!"
          mc "Well, you know the scots. They don't like being offended."
          isabelle sad "I know..."
          mc "I'm not Scottish, but I felt offended on Scotland's behalf."
          $isabelle.lust+=1
          isabelle blush "Thanks for sticking up for Scotland. They've always been the underdog."
          mc "No worries. I'd do the same for any small country or minority."
      isabelle sad "I'm pretty sad I messed up so badly."
      isabelle sad "I think I need to eat a plate of haggis to atone."
      mc "I mean, I guess that's an option. I think the scots would accept such an apology."
      $quest.isabelle_haggis["isabelle_regret"] = True
      hide isabelle with Dissolve(.5)
    else:
      "I should at least skim through this before suggesting it. The French are always up to no good."
  else:
    "I got [isabelle] into this mess."
    "I doubt my [item.title_lower] will lighten the mood."
    "My dick, on the other hand..."
    $quest.isabelle_haggis.failed_item("puzzle",item)
  return

label isabelle_quest_haggis_mrsl_caught:
  show mrsl surprised with Dissolve(.5)
  mrsl surprised "[mc], what are you doing here? You're supposed to be in the teamwork class!"
  "Shit... better come up with a good excuse fast."
  show mrsl surprised at move_to(.25)
  menu(side="right"):
    extend ""
    "\"[flora] got really hungry, so I had to go get her something...\"":
      show mrsl surprised at move_to(.5)
      mc "[flora] got really hungry, so I had to go get her something..."
      mrsl excited "That's very kind of you!"
      mrsl concerned "But that means you failed the teamwork challenge..."
      mrsl concerned "You'll have to pick [flora] or [isabelle] as the winner."
      mc "That doesn't seem fair..."
      mrsl afraid "Life isn't fair. And failing to work together amplifies that sometimes."
      mrsl excited "Since you're no longer part of the challenge, you can be the judge of it instead!"
      show mrsl excited at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I pick [flora].\"":
          show mrsl excited at move_to(.5)
          mc "I pick [flora]."
          mrsl surprised "Are you sure she didn't trick you into leaving the room in order to disqualify you?"
          mc "If she did, she deserves it, right?"
          mrsl thinking "You're right... I never specified the rules."
          mrsl blush "Very well! [flora] will have the reward."
          mrsl blush "It's just this little brooch. I hope she'll like it..."
          mc "She loves to hoard rewards."
          mrsl smile "She's a talented young woman and deserves the recognition!"
          mc "I guess..."
          $flora["brooch"] = True
        "\"I pick [isabelle].\"":
          show mrsl excited at move_to(.5)
          mc "I pick [isabelle]."
          mc "I feel like it's a nice gesture to give it to the new girl..."
          mrsl confident "What about [flora]?"
          mc "What about her?"
          mrsl laughing "Well, she practically tricked you into forfeiting your claim to the reward."
          mc "Maybe she did, but I'm not giving her the satisfaction of winning."
          mc "She has enough rewards as it is."
          mrsl flirty "I was hoping for a more impartial choice, but that's fine!"
          mc "Well, what's the reward anyway?"
          mrsl smile "It's just this little brooch..."
          mrsl smile "I'm sure she'll like it."
          mc "I think so too!"
          $isabelle["brooch"] = True
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"We actually completed the teamwork challenge a while ago!\"":
      show mrsl surprised at move_to(.5)
      mc "We actually completed the teamwork challenge a while ago!"
      mrsl thinking "Indeed?"
      mrsl skeptical "And who unlocked the door?"
      mc "Err... the janitor?"
      mrsl angry "That wasn't really part of the plan..."
      "Huh, anger? That's unusual for [mrsl]..."
      mrsl sad "Very well. I'm glad you completed the challenge."
      mrsl blush "It's not a big reward, but I'm glad you solved it without too much trouble. Just this little brooch..."
      mrsl blush "Who did you decide on for the reward?"
      show mrsl blush at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Me!\"":
          show mrsl blush at move_to(.5)
          mc "Me!"
          mrsl laughing "Really, now?"
          mc "Yes, it was a unanimous vote."
          mrsl confident "Are you telling the truth, [mc]?"
          mc "Yes, we had ballots and everything. Very democratic!"
          mrsl flirty "You're not a very good liar, but I'll give you the reward anyway."
          mrsl laughing "Sometimes winning isn't about teamwork, just about outsmarting the competition!"
          mrsl confident "I think intelligence is worth rewarding, as well..."
          mrsl flirty "Wear it with pride."
          $mc.add_item("mrsl_brooch")
        "\"[flora].\"":
          show mrsl blush at move_to(.5)
          mc "[flora]."
          mrsl smile "I know you're not always on the best terms with [flora], so that's a bit surprising..."
          mrsl smile "I'm happy that this exercise has solved some of your issues!"
          mc "I'm glad too! I've been trying to be nicer to her. I hope she sees that."
          mrsl blush "I'm sure she will."
          mc "Want me to bring it to her?"
          mrsl sad "No, that's all right. I'll do it myself..."
          $flora["brooch"] = True
        "\"[isabelle].\"":
          show mrsl blush at move_to(.5)
          mc "[isabelle]."
          mrsl smile "Okay, then. [isabelle] is a good pick! Nice of you to pick the new student."
          mrsl smile "Hopefully, she'll like the brooch..."
          mc "Want me to bring it to her?"
          mrsl sad "No, that's all right. I'll do it myself..."
          $isabelle["brooch"] = True
  hide mrsl with Dissolve(.5)
  $quest.isabelle_haggis.finish()
  return
