image isabelle_locker_panties = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-289,-119), Transform(LiveComposite((1920,1080), (-74,-287), "school english_class english_classroom", (558,144), "school english_class blackboard", (496,346), "school english_class teacher_desk", (863,391), "school english_class cabinet", (675,0), "isabelle flirty_holdingpanties"), size=(768,432))), "ui circle_mask"))


label quest_isabelle_hurricane_start:
  show isabelle concerned with Dissolve(.5)
  mc "Hey, how are you holding up?"
  isabelle skeptical "It's like a dark cloud blocking the sunlight..."
  isabelle skeptical "...and the cloud has a name."
  mc "[maxine]?"
  isabelle skeptical "That's right."
  mc "Well, do you have a revenge plan yet?"
  isabelle concerned "It's justice I'm after, not revenge."
  mc "What's the difference?"
  isabelle concerned "One is to settle a score, the other is to uphold the law."
  mc "And you're the law?"
  isabelle laughing "Someone has to be in this place."
  mc "Fair enough."
  isabelle confident "And yes, I do in fact have a plan."
  mc "We're not really killing [spinach], are we?"
  isabelle sad "No, not really. I was angry."
  mc "And you're not angry now?"
  isabelle sad "I'm not exactly happy, but I currently don't see red."
  show isabelle sad at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What color do you see now?\"":
      show isabelle sad at move_to(.5)
      mc "What color do you see now?"
      isabelle confident "I see a lighter shade of pink."
      mc "The color of strawberry juice?"
      $isabelle.love+=1
      isabelle laughing "That's pretty cute."
    "?quest.isabelle_locker['personal_sacrifice']@|{image=isabelle_locker_panties}|\"I also have a plan...\"":
      show isabelle sad at move_to(.5)
      mc "I also have a plan..."
      isabelle confident "Surely, it has nothing to do with my knickers this time, right?"
      mc "Well, then I'm out of ideas."
      isabelle laughing "You're bad!"
      mc "At literally everything I do. It's a talent."
      isabelle confident "I know a few things you're good at."
      mc "Do tell."
      isabelle confident "Making me laugh is one."
      mc "And the others?"
      $isabelle.lust+=2
      $isabelle.love+=1
      isabelle blush "Maybe I'll tell you later..."
      "Why am I getting hard now? Are compliments a turn-on for me?"
      "I better hurry things along so we get to the fabled {i}later!{/}"
    "\"I'm looking forward to\npunishing [maxine].\"":
      show isabelle sad at move_to(.5)
      $mc.lust+=1
      mc "I'm looking forward to punishing [maxine]."
      $isabelle.lust+=2
      isabelle blush "Same. We're going to teach her a proper lesson."
  mc "All right, so what's the plan?"
  isabelle annoyed_left "We first have to locate my locker."
  isabelle annoyed_left "It's not proper justice if the stolen belongings aren't returned to their rightful owners."
  mc "Then what?"
  isabelle angry "Well, she clearly doesn't care about things that are supposedly precious to her..."
  isabelle angry "So, retaliation has to come in a different form."
  mc "I thought you said this was about justice?"
  isabelle sad "That's what I meant."
  "[isabelle] is clearly still on edge, and her anger could boil over at any moment..."
  mc "Maybe it would be best if you let me take care of this?"
  isabelle concerned "What makes you say that?"
  mc "Having a cool head is pretty important when exacting one's revenge.{space=-10}"
  mc "How about you just chill and leave this to me?"
  isabelle concerned_left "That might be a good idea."
  isabelle skeptical "But I want to know all the details after."
  mc "You can come over when I'm done and I'll tell you about it."
  mc "Maybe we can watch some NutFlex and order takeout?"
  isabelle neutral "Sounds good. I've been meaning to watch Quid Game forever."
  isabelle neutral "It was my sister's favorite survival show. Winner gets a million quid."
  mc "Sounds interesting! It's a date!"
  isabelle excited "Okay!"
  hide isabelle with dissolve2
  "All right, locating [isabelle]'s locker isn't a bad first step..."
  "But where could [maxine] possibly be keeping it?"
  $quest.isabelle_hurricane.start("maxine_office")
  return

label quest_isabelle_hurricane_maxine_office:
  "Well, the locker sure as hell ain't here."
  "And I haven't seen it in any of the classrooms."
  "Where does one even store a stolen locker?"
  "..."
  if maxine.location == "school_clubroom":
    show maxine skeptical with Dissolve(.5)
  else:
    show maxine skeptical at appear_from_left
  maxine skeptical "Why are you snooping around in my office?"
  show maxine skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'm looking for a stolen locker.\"":
      show maxine skeptical at move_to(.5)
      mc "I'm looking for a stolen locker."
      maxine concerned "That's curious."
      maxine concerned "A stolen locker? Why would anyone steal a locker?"
      mc "..."
      mc "You're joking, right?"
      maxine thinking "I would never joke about larceny."
      mc "But you stole it!"
      maxine thinking "Do you have any proof of that?"
      mc "Well... you said it's been repurposed. So, you know... things."
      maxine skeptical "The last time I read through the statutory codes, knowledge was not listed as a crime."
      mc "But obstruction of justice is! And you're obstructing a lot here!"
      maxine annoyed "You can take me to court, then. Here's my lawyer's number."
      if mc.check_phone_contact("maxine"):
        mc "..."
        mc "This is your number..."
        maxine concerned "Is it?"
        mc "Yes!"
        maxine concerned "You must be mistaken."
        mc "Oh, yeah?"
      else:
        $maxine.default_name = "Maxine's\nLawyer"
        $maxine.contact_icon = "hidden_number contact_icon"
        $mc.add_phone_contact("maxine")
        $mc.phone_contacts.remove("maxine")
        mc "..."
        "She probably gave me a fake number..."
      window hide
      $guard.default_name = "Phone"
      $set_dialog_mode("phone_call_plus_textbox","maxine")
      pause 0.5
      window auto
      guard "{i}Calling...{/}"
      guard "{i}...{/}"
      guard "{i}Calling...{/}"
      guard "{i}...{/}"
      $guard.default_name = "Guard"
      $maxine.contact_icon = "maxine contact_icon"
      window hide
      $set_dialog_mode("phone_call_centered","maxine")
      show maxine sad_phone with dissolve2
      maxine "Max & Max Legal,\nhow can I help you?"
      if mc.check_phone_contact("maxine"):
        mc "..."
        maxine "Hello? Anyone there?"
        window auto
        $set_dialog_mode("phone_call_plus_textbox","maxine")
        "Is she for real?"
        window hide
        $set_dialog_mode("phone_call_centered","maxine")
        maxine "Hello?"
      else:
        mc "Are you being\nserious right now?"
        maxine "I assure you that our\nservices are both serious\nand affordable."
        maxine "Are you looking for\ncourt representation?"
        mc "Screw you."
      play sound "end_call"
      $set_dialog_mode("")
      $maxine.default_name = "Maxine"
      pause 0.5
      show maxine cringe_phone with Dissolve(.5)
      window auto
      if mc.check_phone_contact("maxine"):
        maxine cringe_phone "Sorry about that. Someone must've gotten the wrong number."
        mc "You think?"
      else:
        maxine cringe_phone "Sorry, someone was being very rude over the phone."
        maxine cringe_phone "People say the darndest things when they're not face to face."
        mc "..."
        $mc.add_phone_contact("maxine", silent=True)
    "\"That's none of your business.\"":
      show maxine skeptical at move_to(.5)
      mc "That's none of your business."
      maxine eyeroll "You're in my office..."
      mc "Last time I checked, this was a classroom."
      maxine annoyed "When was that?"
      mc "I don't know! It's a figure of speech!"
      maxine concerned "I thought I'd eradicated those. Where did you see them?"
      mc "What?"
      maxine concerned "The Figures of Schpeetch — a secret cult that used to occupy the school basement."
      mc "The school has no basement..."
      maxine sad "I've said too much."
    "\"Why are {i}you{/} snooping around\nin your office?\"":
      show maxine skeptical at move_to(.5)
      mc "Why are {i}you{/} snooping around in your office?"
      maxine excited "A fair question."
      "Was it?"
      maxine excited "I'm looking for dormant energy from the sixth plane."
      mc "Right, I should've guessed that."
      maxine concerned "Are you familiar with the sixth plane?"
      mc "Can't say I am."
      maxine concerned "Perhaps you are, but you don't know it yet..."
      mc "What's that supposed to mean?"
      maxine concerned "Perhaps you are familiar with it without knowing."
      mc "..."
      maxine excited "I can also repeat it in French, as well as in nine dialects of 12th century Germanic if you prefer."
      mc "No, that's fine..."
  maxine thinking "Anyhow, I need to get back to work."
  maxine thinking "Don't touch anything except the floor."
  if maxine.location == "school_clubroom":
    hide maxine with dissolve2
  else:
    window hide
    show maxine thinking at disappear_to_left
    pause 0.5
    window auto
  "Well, that didn't really give me any new information about the missing locker."
  "Maybe I should actually confirm that [maxine] did in fact steal it..."
  "Only one way to do that — getting the [guard] out of his booth."
  "A task much easier said than done, considering his schedule of doughnut siestas."
  $quest.isabelle_hurricane.advance("guard_booth")
  return

label quest_isabelle_hurricane_guard_booth:
  guard "ZzzzZzzzz..."
  "Sleeping like a sugar addict, complete with drool and everything..."
  "Accessing the surveillance tape will be difficult this time. Luring him out with the promise of doughnuts probably won't work twice."
  "..."
  "Or maybe it would. Who knows how deep his hunger goes?"
  "Maybe I could even—"
  show kate excited at appear_from_left
  kate excited "Why do you look so shifty, hm?"
  "Ugh, am I really that easy to read?"
  show kate excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"What do you want?\"":
      show kate excited at move_to(.5)
      mc "What do you want?"
      kate skeptical "Hey, don't be rude now."
      kate skeptical "Apologize."
      show kate skeptical at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I'm sorry.\"":
          show kate skeptical at move_to(.5)
          mc "I'm sorry."
          $kate.lust+=1
          kate confident "That's better. Know your manners."
          kate confident "Now, tell me why you look so shifty."
          mc "I need to get into the [guard]'s booth..."
          kate laughing "Why? Did he confiscate your porn collection or something?"
          mc "That's, err... yes. Yes, he did."
          kate smile "Are you being serious?"
          mc "It's really embarrassing, but I need it urgently."
          kate smile "You're disgusting. Your horny little dick should be locked up."
          mc "Can you help me?"
          kate flirty "With what? Burglary or locking you up?"
          mc "Either..."
          $kate.lust+=1
          kate laughing "You're pathetic. Seriously."
          if quest.kate_desire["balls_busted"]:
            kate confident "Maybe we should repeat your forfeit..."
            kate confident "You know what they do with horny dogs, right?"
            kate confident "Castration."
          else:
            kate confident "You deserve to be stepped on."
            kate confident "And I have a new pair of stilettos I'd like to try out."
          mc "I... don't think we have to go that far..."
          kate neutral "Your loss."
          mc "What about the [guard]?"
          kate smile_right "He's pathetic too, but too old for my taste."
          mc "I meant if you could help me distract him..."
          kate smile "Now, why would I do that?"
          show kate smile at move_to(.25)
          menu(side="right"):
            extend ""
            "\"Because I asked nicely?\"":
              show kate smile at move_to(.5)
              mc "Because I asked nicely?"
              kate laughing "Dream on, sucker."
              window hide
              show kate laughing at disappear_to_left
              pause 0.5
              window auto
              "Well, shit. I guess I'm on my own..."
            "?mc.lust>=4@[mc.lust]/4|{image=stats lust}|\"Because I know you lost a bet with Chad last year.\"":
              show kate smile at move_to(.5)
              mc "Because I know you lost a bet with Chad last year."
              kate skeptical "How did you know that?"
              mc "I heard the guys brag in the locker room."
              mc "I know you're supposed to suck someone's dick before graduation."
              kate annoyed "The bet was rigged!"
              mc "Well... maybe I can help you rig the forfeit, then."
              kate eyeroll "If you think I'll suck your dick, you're living in fantasy land."
              mc "Not exactly..."
              mc "The truth is, I've been having these thoughts lately."
              mc "I'd like to try giving a blowjob. And who's got a better dick than Chad?{space=-45}"
              kate thinking "You're joking, right?"
              mc "No, I'm serious."
              kate blush "I can tell!"
              kate blush "I always had a feeling you were gay."
              mc "I'm not actually gay. Just curious."
              kate blush "Of course..."
              mc "Well, do we have a deal or not?"
              mc "I know the [guard] will do anything you tell him."
              kate confident "Hmm... yeah, I guess we could make it work."
              kate neutral "But this stays between you and me."
              kate neutral "If you ever tell anyone about this deal, you're done."
              "She looks serious..."
              mc "Okay, I promise."
              kate confident "Very well. Meet me in the sports wing."
              window hide
              show kate confident at disappear_to_right
              pause 0.5
              window auto
              "I can't believe this is happening..."
              "Maybe I am gay? Damn, I guess we'll find out..."
              $quest.isabelle_hurricane.advance("blowjob")
              return
        "\"Screw you.\"":
          show kate skeptical at move_to(.5)
          mc "Screw you."
          $kate.lust-=2
          kate eyeroll "Don't make me slap you."
          mc "You will get what's coming for you. Mark my words."
          show kate sad_hands_up at bounce with Dissolve(.5)
          kate sad_hands_up "Oh, no! Please! I'm so scared!"
          hide kate
          show kate sad_hands_up
          "Ugh..."
          show kate sad_hands_up at bounce
          kate sad_hands_up "No, really! What are you going to do? Spank me for being a bad girl?{space=-10}"
          hide kate
          show kate sad_hands_up
          "I'd be lying if I said I hadn't had that fantasy once or twice..."
          kate surprised "Oh, no! You've been thinking about it, haven't you?"
          "..."
          "I really need to stop wearing my face on my sleeve or whatever."
          kate embarrassed "Let me tell you something straight here."
          kate embarrassed "You're a perverted loser who will never touch a woman."
          kate embarrassed "Get it through your head."
          window hide
          show kate embarrassed at disappear_to_left
          pause 0.5
          window auto
          "Damn, I always feel deflated after talking to [kate]..."
          "And the [guard] definitely wouldn't fall for the doughnut trick twice."
          "I need to stop deluding myself and find a real way to get a hold of the surveillance tapes..."
        "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"You're as radiant as the sunset\nover the Newfall Harbor.\"":
          show kate skeptical at move_to(.5)
          mc "You're as radiant as the sunset over the Newfall Harbor."
          kate flirty "That's cute."
          kate flirty "But it's still not an apology."
          mc "Oh, that was just me practicing for a poetry reading."
          kate laughing "Right, right. And I didn't make the [nurse] swallow eight dicks this morning."
          mc "Why wasn't I invited?"
          kate excited "I didn't know you liked sucking dick, but I guess I can see it..."
          mc "Nothing to be ashamed of, really. I'm sure you enjoy it too."
          kate eyeroll "Get lost."
          $mc.lust+=2
          mc "What? Don't tell me you're too proud to gobble walnuts?"
          $kate.lust-=1
          kate cringe "You're disgusting."
          window hide
          show kate cringe at disappear_to_left
          pause 0.5
          window auto
          "And that's how you get rid of [kate]! Easy as a lubricated asshole."
          "All right, the [guard] is next..."
          "Let's see what's left in the old play book."
          guard "ZzzzZz—"
          mc "Hey, dumbo! Wake up!" with vpunch
          guard "Huh?! What?!"
          mc "There's a doughnut-sale in the cafeteria!"
          guard "..."
          guard "Are you trying to be funny or something?"
          mc "No, I'm trying to be helpful."
          guard "Well, I just checked and there's no sale."
          guard "Quit playing games with me!"
          "Well, shit. Of course he would check that..."
          "I need to find another way."
    "\"Leave me alone!\"":
      show kate excited at move_to(.5)
      mc "Leave me alone!" with vpunch
      $kate.lust-=2
      kate eyeroll "You're getting more boring each day."
      show kate eyeroll at disappear_to_left
      "Sometimes, you just got to get loud. It's the only way [kate] will leave me alone."
      "After all, she doesn't want to be seen talking to me."
      "Hmm... I still need to come up with a way to get inside the [guard]'s booth."
  $quest.isabelle_hurricane.advance("short_circuit")
  return

label quest_isabelle_hurricane_short_circuit_fuse_box1:
  "Well, one thing that would certainly force the [guard] to leave his booth{space=-50}\nwould be to short circuit the fuse box."
  "But I would need to find a distraction for the eye in the sky..."
  $quest.isabelle_hurricane["distraction"] = True
  return

label quest_isabelle_hurricane_short_circuit_spinach1:
  show spinach neutral with Dissolve(.5)
  "Maybe [spinach] could help me distract the [guard]?"
  mc "Wanna go into my backpack for a bit, girl?"
  spinach closedeyes "Meow!"
  "She seems excited."
  $quest.isabelle_hurricane["backpack"] = True
  hide spinach with dissolve2
  $mc.add_item("spinach")
  return

label quest_isabelle_hurricane_short_circuit_door(item):
  if item == "spinach":
    show spinach neutral with dissolve2
    $mc.remove_item("spinach")
    mc "Here you go, [spinach] — have a scratch at this door!"
    spinach tongueout "Meow!"
    window hide
    $quest.isabelle_hurricane["scratching"] = True
    hide spinach with Dissolve(.5)
    window auto
    "Look at that! Big brother is already interested in her."
    "Now, I just have to stand very still..."
  else:
    "If my [item.title_lower] moved at all, it would be the perfect distraction. Alas."
    $quest.isabelle_hurricane.failed_item("distraction",item)
  return

label quest_isabelle_hurricane_short_circuit_fuse_box2:
  "Now all that remains is... darkness."
  window hide
  show black onlayer screens with vpunch
  $set_dialog_mode("default_no_bg")
  window auto
  "Well, shit."
  "How am I supposed to find my way to the [guard]'s booth now?"
  $quest.isabelle_hurricane["darkness"] = True
  return

label quest_isabelle_hurricane_short_circuit_phone:
  window hide None
  "Thank god we're living in the future... I have a phone flashlight and I'm not afraid to use it."
  "..."
  "Now, I just have to hurry to the [guard]'s booth before the lights get fixed."
  "Down the stairs... One step, two steps... Now is not the time to break{space=-15}\na neck..."
  "..."
  $game.location = "school_ground_floor"
  "..."
  "Here we are... the doughnut fortress... the domain of the eternal sleeper..."
  "Wow, he even left it unlocked in the hurry!"
  "..."
  "It seems like the coast is clear."
  "I'll just download the surveillance footage real quick..."
  $mc.remove_item("flash_drive")
  "..."
  "..."
  "..."
  "Got it!"
  $mc.add_item("flash_drive")
  $quest.isabelle_hurricane.advance("surveillance_footage")
  hide black onlayer screens with Dissolve(.5)
  $set_dialog_mode("")
  window auto
  "Phew! That was close!"
  "Better get out of here before the [guard] comes back."
  return

label quest_isabelle_hurricane_short_circuit_spinach2:
  "Huh?"
  mc "[spinach]? Where are you, girl?"
  "..."
  "She must've made her way back to [maxine]'s office after the blackout..."
  $quest.isabelle_hurricane["spinach_gone"] = True
  return

label quest_isabelle_hurricane_blowjob_kate:
  show kate confident with Dissolve(.5)
  kate confident "There's my little cocksucker."
  "Great. She's going to rub it in as well..."
  kate confident "I've put a blindfold on Chad, and he's waiting in the women's bathroom."
  mc "..."
  kate neutral "You better not flake out now."
  mc "I won't..."
  kate blush "Good! I have brought a couple props for you."
  kate blush_blindfold_wig "I found these in the theater supply closet."
  mc "Is that a wig?"
  kate blush_blindfold_wig "Yeah! I've sprayed it with my perfume."
  kate blush_blindfold_wig "And this is a blindfold."
  mc "Why do I need that?"
  kate thinking_blindfold_wig "Well, girls sometimes get intimidated when they see Chad's dick for the first time..."
  kate embarrassed_blindfold_wig "I don't want you backing out after seeing it."
  mc "Fine, I guess."
  kate blush_blindfold_wig "Okay, let me put them on you."
  window hide
  show kate blush_blindfold_wig:
    linear 0.25 zoom 2.5 yoffset 1250
  play sound "<from 0.2>bedsheet"
  pause 0.5
  show kate blush_blindfold:
    linear 0.25 zoom 1.0 yoffset 0
  pause 0.25
  window auto
  kate blush_blindfold "There's the wig! Don't you look pretty?"
  mc "..."
  kate blush_blindfold "And here comes the blindfold..."
  window hide
  show kate blush_blindfold:
    linear 0.25 zoom 2.5 yoffset 1250
  play sound "<to 1.5>tying_knot"
  pause 0.5
  show black with easeintop
  hide kate
  pause 0.5
  $set_dialog_mode("default_no_bg")
  kate "All right, come with me."
  "[kate] takes me by the arm and leads me into the women's bathroom."
  if school_bathroom["visited"] == 0:
    $school_bathroom["utter_disappointment"] = True
  $game.location = "school_bathroom"
  "A dozen different scents assault my senses."
  "Strawberry perfume. Lemon soap. Chlorine. Urine. Maybe a hint of sweat and pussy."
  "The list goes on as she leads me over to one of the stalls."
  "Her hand leaves my arm and settles on my shoulder, pushing down."
  "I sink to my knees on the dirty bathroom floor."
  play sound "zipper"
  "The sound of an opening zipper, followed by the rustle of pants..."
  "This is it."
  "Suddenly, the warm tip of a dick pokes at my lips."
  "Chad" "Ohhh!"
  "It has a certain stench to it."
  "It smells familiar somehow, but I guess all dicks smell the same."
  "Old semen mixed with sweat and precum."
  "Even Chad must be excited to get a blowjob from [kate]."
  "She guides my head forward, forcing me to open my mouth."
  "The dick slips past my lips and rests for a moment on my tongue."
  "Chad" "Oooh..."
  "I'm surprised it even fit inside my mouth, considering Chad's reputation..."
  "But on the other hand, [flora] tells me I have the biggest mouth in all of Newfall."
  "Perhaps it's a match made in hell?"
  "It doesn't taste all that bad either... just this salty, musky tang."
  "Chad lets out a sigh of pleasure as his dick slides deeper and my lips close around it."
  "The tip pokes at the back of my mouth, trying to find the entrance to my throat."
  "Will I be able to deepthroat Chad? That's a skill very few girls possess."
  "There's no medal for it, but it definitely comes with bragging rights."
  "[kate]'s hand leaves my head, allowing me to pick the tempo."
  "I bob my head... suck a bit... lick a bit... try to breathe..."
  "Chad" "Mmmm..."
  play sound "<to 2.2>high_heels"
  "Chad doesn't seem to notice the clicking of [kate]'s heels as she backs out of the stall to give us some space."
  "Instead, his hand grips the back of my head, slowly guiding the rhythm, and finally pushing his dick down my throat."
  "At first, my body convulses, gagging."
  "But then his dick goes deep enough... and the feeling disappears."
  "He holds me there for a moment, enjoying the feeling of my throat tightly wrapped around his dick."
  "Then he lets up for a breath, but it doesn't take long before he pushes me forward again."
  "His dick slides in and out of my throat, and I try to breathe in between.{space=-50}"
  "I never realized a blowjob was so much work before now..."
  "A shiver rolls through Chad just as he pushes further down my throat than ever before."
  "Chad" "Hnnng!" with vpunch
  "His dick spasms and unloads into my stomach."
  "His hot seed fills me up completely."
  play sound "<to 2.2>high_heels"
  "Finally, he pulls out as I hear [kate]'s heels behind me."
  "She takes me by the arm and helps me up."
  kate "There you go, sweetie."
  "Chad is too knocked out to even answer..."
  "With his semen still warm in my belly, [kate] walks me out of the bathroom."
  if school_bathroom["visited"] == 1:
    $school_bathroom["visited"]-=1
    $school_bathroom["visited_now"]-=1
    $school_bathroom["visited_today"]-=1
    $del school_bathroom.flags["utter_disappointment"]
  $game.location = "school_first_hall_east"
  "Without a word, she wipes my mouth with a napkin and then pulls off the blindfold and wig."
  show kate confident behind black
  pause 0.25
  hide black with Dissolve(.5)
  $set_dialog_mode("")
  window auto
  kate confident "How was it?"
  mc "W-weird..."
  kate laughing "That's a more common reaction than you think!"
  kate smile_right "Anyway, I managed to get the [guard] out of his booth."
  kate smile_right "So, if you're quick, you can get whatever it was that you wanted."
  mc "Thanks..."
  kate confident "Pleasure doing business with you."
  $quest.isabelle_hurricane["cocksucker"] = True
  hide kate with Dissolve(.5)
  return

label quest_isabelle_hurricane_blowjob_guard_booth:
  "[kate] was right. The [guard] is gone, and he seems to have forgotten to lock up."
  "I wonder why he was in such a hurry?"
  "Well, then..."
  "No one seems to be looking."
  "Just a quick in and out to download the surveillance footage to my flash drive."
  $mc.remove_item("flash_drive")
  "..."
  "..."
  "..."
  "There we go!"
  $mc.add_item("flash_drive")
  "And we're gone—"
  show guard smile at appear_from_left
  guard smile "Good day to you."
  "What? Why does he seem so happy?"
  "In all my years here, he has never greeted me..."
  mc "Um, good day, sir."
  guard smile "Beautiful weather today, eh? Perfect for a walk in the park."
  guard smile "I think I'll take one myself."
  window hide
  show guard smile at disappear_to_right
  pause 0.5
  window auto
  "What the hell is going on? He's even whistling..."
  "..."
  "Whatever. Let's get back home and watch the surveillance footage."
  $quest.isabelle_hurricane.advance("surveillance_footage")
  return

image surveillance_footage_static_screen:
  "maxine avatar events surveillance_footage static_screen1"
  0.1
  "maxine avatar events surveillance_footage static_screen2"
  0.1
  "maxine avatar events surveillance_footage static_screen3"
  0.1
  repeat

label quest_isabelle_hurricane_surveillance_footage(item):
  if item == "flash_drive":
    "Front or back port, darling? What do you feel like tonight?"
    "Back port, you say? I can fuck with that."
    $mc.remove_item("flash_drive")
    "Okay, let's see here..."
    "If my memory serves me right, the theft happened the day after we buried the box in the forest..."
    "That should be about... here."
    window hide
    show maxine surveillance_footage_frame1_school_ground_floor:
      xsize 1328 ysize 747 xpos 322 ypos 60
    if 20 > game.hour > 6:
      show home bedroom black_market computer_day as computer
    else:
      show home bedroom black_market computer_night as computer
    with Dissolve(.5)
    window auto
    "Everyone is milling about. Picking up their stuff for classes."
    show maxine surveillance_footage_frame2_school_ground_floor with dissolve2
    "Nothing seems out of the ordinary..."
    show maxine surveillance_footage_frame3_school_ground_floor with dissolve2
    "There's [maxine]..."
    show maxine surveillance_footage_frame4_school_ground_floor with dissolve2
    "[lindsey] running... going way too fast as usual..."
    show maxine surveillance_footage_frame5_school_ground_floor with dissolve2
    "Okay... so the theft should be somewhere around here—"
    play sound "walkie_talkie2" loop
    show maxine surveillance_footage_confident_school_clubroom
    show surveillance_footage_static_screen behind computer:
      xsize 1328 ysize 747 xpos 322 ypos 60
    window hide
    pause 1.0
    stop sound
    hide surveillance_footage_static_screen
    pause 0.5
    window auto
    maxine surveillance_footage_confident_school_clubroom "Greetings, fellow truth seeker."
    "You've got to be kidding me..."
    maxine surveillance_footage_annoyed_school_clubroom "Right now, you probably think I'm kidding you..."
    maxine surveillance_footage_annoyed_school_clubroom "But as a member of the Inconspicuous Group, I assure you that this message is of the utmost gravity."
    "Seriously, [maxine]?"
    maxine surveillance_footage_skeptical_school_clubroom "While we applaud your efforts in seeking the truth, you should know that certain knowledge can be dangerous."
    maxine surveillance_footage_skeptical_school_clubroom "We want you to understand that you're dealing with powers far beyond your control."
    maxine surveillance_footage_skeptical_school_clubroom "And it would be safest to stop looking for the artifact now."
    "Artifact? She's probably talking about the stupid locker..."
    maxine surveillance_footage_thinking_school_clubroom "If you still want to pursue this arcane knowledge, you do so at your own peril."
    maxine surveillance_footage_thinking_school_clubroom "I'll wait a moment for you to make up your mind."
    maxine surveillance_footage_sad_school_clubroom "..."
    "As if this babble would stop me now."
    maxine surveillance_footage_skeptical_school_clubroom "If you're still watching, you are indeed a seeker of the truth."
    maxine surveillance_footage_skeptical_school_clubroom "The artifact has been removed from its original location for the purpose of safekeeping."
    maxine surveillance_footage_skeptical_school_clubroom "It will remain in its new location for the time being."
    maxine surveillance_footage_annoyed_school_clubroom "This is to prevent easy access to the underworld."
    maxine surveillance_footage_annoyed_school_clubroom "The decision to relocate the artifact was made after several malicious attempts to access the nether realm."
    maxine surveillance_footage_annoyed_school_clubroom "The stability of this plane is at the very core of the Inconspicuous Group's constitution."
    maxine surveillance_footage_sad_school_clubroom "Now, if you've come this far, little will be able to stop you from getting to the bottom of this."
    maxine surveillance_footage_sad_school_clubroom "So, to spare you from further trouble..."
    "Yeah, right. As if she would ever spare anyone any trouble."
    maxine surveillance_footage_thinking_school_clubroom "The artifact is now located outside of harm's reach."
    maxine surveillance_footage_thinking_school_clubroom "In a place lost to time and civilization."
    "Of course it is..."
    maxine surveillance_footage_thinking_school_clubroom "Find the truth before it finds you."
    play sound "walkie_talkie2" loop
    show surveillance_footage_static_screen behind computer:
      xsize 1328 ysize 747 xpos 322 ypos 60
    window hide
    pause 0.5
    window auto
    "So, [maxine] definitely stole the locker. I think that's the takeaway from this."
    "And it's in a place lost to time and civilization..."
    "..."
    "Maybe [isabelle] would know?"
    window hide
    stop sound
    hide surveillance_footage_static_screen
    hide computer
    hide maxine
    with dissolve2
    $mc.add_item("flash_drive")
    $quest.isabelle_hurricane.advance("forest")
    window auto
  else:
    "If only I could plug my [item.title_lower] into the computer, all my dreams would come through."
    "Unfortunately, technology hasn't advanced that far."
    $quest.isabelle_hurricane.failed_item("surveillance_footage",item)
  return

label quest_isabelle_hurricane_forest_isabelle:
  show isabelle neutral with Dissolve(.5)
  isabelle neutral "Hey, how's it going?"
  mc "I've made some discoveries..."
  isabelle neutral "Truly? What did you learn?"
  mc "I've done some digging and your locker is... well..."
  mc "It's apparently in a place lost to time and civilization."
  mc "Any idea where that might be?"
  isabelle concerned_left "I mean, it must be outside the school somewhere, right?"
  mc "If my sources are to be believed."
  isabelle concerned "Maybe it's in the forest somewhere?"
  mc "Hmm...  you might be right..."
  if mc.check_phone_contact("isabelle"):
    mc "I guess I'll do some hiking. I'll call you if I find it."
    isabelle excited "Perfect!"
  else:
    mc "I guess I'll do some hiking."
    isabelle excited "Perfect! Let me put my number in your phone and you can call me if you find it?"
    mc "Sounds good. Here you go."
    $mc.add_phone_contact("isabelle")
    isabelle excited "Okay, done!"
  hide isabelle with Dissolve(.5)
  $quest.isabelle_hurricane["hiking"] = True
  return

label quest_isabelle_hurricane_forest_creepy_hut:
  "Hmm... this old shack is abandoned, so it's definitely lost to time..."
  "..."
  "It's pretty dark inside... I can't see anything."
  "Maybe I can get a better look from the other side?"
  "There are some holes in the wall near the roof."
  "They're pretty far up, but luckily I can climb up on this—"
  "..."
  "Son of a bitch."
  "It's [isabelle]'s locker. It's just lying here in the grass."
  "..."
  "She'll be happy to learn that I found it, I guess."
  $quest.isabelle_hurricane.advance("call_isabelle")
  $mc["focus"] = "isabelle_hurricane"
  return

label quest_isabelle_hurricane_call_isabelle:
  $set_dialog_mode("phone_call_plus_textbox","isabelle")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","isabelle")
  isabelle "Hi!"
  mc "Hey, it's me. I've\nfound your locker."
  isabelle "You're joking!"
  mc "I wish I was..."
  isabelle "Well, where is it?"
  mc "It's in the forest glade."
  isabelle "Someone dragged it\nthere? Bloody hell..."
  mc "Seems that way."
  isabelle "Okay, I'll be\nthere in a bit."
  play sound "end_call"
  $set_dialog_mode("")
  pause 0.25
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 2.0
  show isabelle thinking at appear_from_left
  hide black onlayer screens with Dissolve(.5)
  window auto
  isabelle thinking "Didn't you say the locker was here?"
  mc "It is. Right behind that old hut."
  mc "And I've also confirmed that it was [maxine] who took it."
  isabelle afraid "And that twat just dumped it here?"
  isabelle cringe "So, it was entirely malicious!"
  isabelle cringe "Miserable slag..."
  mc "I guess so..."
  "[isabelle] is getting really worked up again."
  "Perhaps I shouldn't have called her here? I haven't even started to think of a revenge plan yet..."
  isabelle cringe "Help me drag it out, please."
  "Damn. She's so polite even when furious."
  mc "Sure thing."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 1.0
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Man, this thing is heavy..."
  "..."
  "..."
  $quest.isabelle_hurricane["locker"] = True
  mc "T-there we go."
  show isabelle concerned_left
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "What now? How are we going to get it back to the school?"
  isabelle skeptical "Screw that. We're settling this right here, right now."
  mc "What do you mean?"
  isabelle annoyed "I mean, ring that wanker. Get her here."
  isabelle annoyed "I have a brilliant fucking plan."
  if not mc.check_phone_contact("maxine"):
    mc "I don't actually have [maxine]'s number..."
    isabelle annoyed "Luckily, I didn't delete her from my contacts just yet."
    isabelle annoyed "Here you go."
    $mc.add_phone_contact("maxine")
    mc "..."
  "I'm not sure about this. Someone might get seriously hurt."
  isabelle angry "[mc], would you please ring her up? I'm too pissed off to talk\nright now."
  show isabelle angry at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I don't think this is a good idea.\"":
      show isabelle angry at move_to(.5)
      $mc.love+=1
      mc "I don't think this is a good idea."
      $isabelle.lust-=2
      isabelle afraid "What the hell, mate? Have you lost your spine now?"
      isabelle afraid "This is how the bullies win!"
      mc "I just don't want you to do something you might regret."
      mc "You were talking about decapitating her cat earlier..."
      isabelle cringe "And she would've bloody well deserved it!"
      mc "But the cat wouldn't!"
      isabelle sad "I'm not going to hurt the cat, okay?"
      isabelle sad "I'm just trying to get some justice, that's all..."
      isabelle sad "Not just for me, but for other victims as well."
      isabelle sad "I swear no one will get seriously injured."
      mc "All right, fine. I'll call her."
      mc "But who knows if she'll even come..."
    "\"Absolutely. She's had it coming\nfor some time.\"":
      show isabelle angry at move_to(.5)
      $mc.lust+=1
      mc "Absolutely. She's had it coming for some time."
      $isabelle.lust+=2
      isabelle blush "That's the bloody spirit!"
      isabelle blush "I reckon we'll show that arsemonger this and that today."
      isabelle blush "Go on then, ring her up."
  $quest.isabelle_hurricane.advance("call_maxine")
  hide isabelle with Dissolve(.5)
  return

label quest_isabelle_hurricane_call_isabelle_road:
  "Yeah, no. I'm not letting this locker out of my sight again."
  return

label quest_isabelle_hurricane_call_maxine:
  $set_dialog_mode("phone_call_plus_textbox","maxine")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","maxine")
  maxine "Who is this?"
  mc "It's [mc]."
  maxine "Why are you calling me\non the secure line?"
  mc "I didn't realize there\nwas an unsecure line..."
  maxine "There isn't. That\nwould be silly."
  mc "..."
  mc "Anyway, I need you to\ncome to the forest glade\nbehind the school."
  maxine "Nope. Busy."
  $set_dialog_mode("phone_call","maxine")
  menu(side="middle"):
    extend ""
    "\"I've discovered a new species\nof beaver.\"":
      $set_dialog_mode("phone_call_centered","maxine")
      $mc.intellect+=1
      mc "I've discovered a new\nspecies of beaver."
      maxine "I'll bring my rifle."
      mc  "That... won't be necessary."
      maxine "Are you a beaver\nsympathizer?"
      mc "N-no! I've... uh,\nalready subdued it."
      maxine "Great. I'll bring my\nskinning knife, then."
      mc "No need! Already\ntaken care of it!"
      maxine "So why did you call me?"
      mc "I thought you'd be excited\nthat it's a new species..."
      maxine "Are you excited whenever\nthere's a new strain of the flu?"
      mc "...no."
      maxine "Didn't think so."
      mc "Can I at least give you the\nhead to keep as a trophy?"
      maxine "Are you trying\nto court me?"
      mc "No! I'm just\nbeing friendly."
      maxine "That's too bad."
      mc "So, you're not coming?"
      maxine "I {i}might{/} come."
      play sound "end_call"
      $set_dialog_mode("")
      pause 0.25
      window auto
      "I guess that's the best I'll get from her..."
    "\"[isabelle] wants to fight you.\"":
      $set_dialog_mode("phone_call_centered","maxine")
      $mc.strength+=1
      mc "[isabelle] wants to fight you."
      maxine "Okay."
      mc "Okay? So, you're coming?"
      maxine "I've been meaning to test out\nmy homunculus for a while..."
      maxine "I just haven't found\na worthy opponent."
      mc "Whatever you say.\nShe's waiting."
      maxine "I'll be right there."
      maxine "Alpha-Bravo, prepare\nfor launch—"
      play sound "end_call"
      $set_dialog_mode("")
      pause 0.25
      window auto
      "If [maxine] starts a nuclear war before dinner, I swear to god..."
    "\"I accidentally opened a portal\nto the underworld.\"":
      $set_dialog_mode("phone_call_centered","maxine")
      $mc.charisma+=1
      mc "I accidentally opened a\nportal to the underworld."
      maxine "How did you manage that?"
      mc "Err... I just had some force\ncrystals... and some herbs...\nand then I rubbed it on the\nlocker... I mean, artifact..."
      maxine "Well, what did you think would\nhappen when you performed\nthe Ritual of Baal?"
      mc "The what?"
      maxine "I'll be right there."
      play sound "end_call"
      $set_dialog_mode("")
      pause 0.25
      window auto
      "No idea how that worked, but okay..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  show isabelle annoyed_left
  $mc["focus"] = ""
  pause 2.0
  show maxine confident_hands_down at appear_from_left(.25) behind isabelle
  show isabelle annoyed_left at move_to(.75,1.0)
  hide black onlayer screens with Dissolve(.5)
  window auto
  isabelle annoyed_left "There you are, you daft minger."
  maxine confident_hands_down "Greetings."
  isabelle annoyed_left "I'll give you one chance to explain yourself."
  maxine skeptical "I only have time for a short explanation. What would you like to have explained?"
  isabelle annoyed_left "What do you think?"
  maxine skeptical "I think you would benefit the most from basic mathematics."
  isabelle annoyed_left "Oh, yeah? I think you would benefit the most from a whacking."
  maxine annoyed "What do you have in mind?"
  isabelle annoyed_left "Something like this..."
  window hide
  show isabelle annoyed_left at move_to(.375)
  pause 0.25
  show maxine cringe
  show white onlayer screens zorder 100
  with Dissolve(.1)
  play sound "slap"
  $renpy.transition(vpunch, layer="screens")
  pause 0.25
  show isabelle annoyed_left at move_to(.75)
  hide white onlayer screens with Dissolve(.1)
  window auto
  "Ouch."
  maxine cringe "That was uncalled for."
  isabelle angry "Uncalled for?! Are you bloody serious?"
  isabelle angry "You think you can just mess with people and that's okay?"
  maxine thinking "I don't remember messing with anyone."
  isabelle annoyed_left "Oh, really? How about this thing right here? Remember this locker?"
  maxine thinking "That's not a locker, that's a gate—"
  isabelle angry "Shut up! Shut the hell up!"
  isabelle angry "She really is a git, isn't she?"
  "Crap. [isabelle] is about to blow up big time."
  "There's a storm raging behind her eyes."
  "She might actually strangle [maxine]..."
  isabelle annoyed_left "Well, I have a surprise for you, you miserable twat."
  window hide
  play sound "fast_whoosh"
  hide maxine
  show isabelle locker_push
  with hpunch
  window auto
  "Whoa! [maxine] sure didn't see that coming."
  "[isabelle] is fast when she wants to be... and strong..."
  "But maybe that's just her adrenaline and hatred fueling her."
  window hide
  play sound "LockerBump"
  show isabelle locker_lie with vpunch
  window auto
  isabelle locker_lie "Not so fun now, is it?"
  isabelle locker_lie "Are you bloody laughing now?"
  maxine "I would never laugh about the misuse of a priceless—"
  isabelle locker_lie "Oh, piss off!"
  "Perhaps I should step in and try to put a stop to this?"
  "But then [isabelle] would probably never forgive me..."
  "And [maxine] did steal her locker..."
  window hide
  play audio "lock_click"
  show isabelle locker_lock with Dissolve(.5)
  window auto
  isabelle locker_lock "Now, this is what poetic justice looks like!"
  isabelle locker_lock "The bully trapped in their locker."
  maxine "This is highly dangerous..."
  isabelle locker_lock "Oh, don't be so wet. There's plenty of holes to breathe through."
  "It's kind of hot seeing her so fired up. So... dominant."
  "Very few people would ever try to go up against [maxine]."
  "She's simply too much to handle."
  "But [isabelle] has her exactly where she wants her."
  "She's completely in her control."
  window hide
  show isabelle locker_sit with Dissolve(.5)
  window auto
  isabelle locker_sit "Maybe I'll let you out tomorrow. Maybe I'll let you rot in there for\na week."
  maxine "May I remind you—"
  isabelle locker_sit "No, you bloody well may not!"
  isabelle locker_sit "You may be quiet and think over your actions!"
  mc "..."
  isabelle locker_sit "What?"
  mc "You look absolutely stunning right now."
  mc "You're the very incarnation of Lady Justice."
  isabelle locker_sit "Thank you! I couldn't have done it without you."
  isabelle locker_sit "Finally, some things set right in this place..."
  mc "You were incredible. That's one bully locked up where she belongs."
  mc "I could kiss you right now."
  isabelle locker_sit "Go on, then."
  mc "Or maybe I'll do more than just kiss you..."
  isabelle locker_sit "I said go on, then!"
  window hide
  show black onlayer screens zorder 100
  show isabelle locker_invitation
  with Dissolve(.5)
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "We roll in the grass, tearing each other's clothes off."
  "Her nails dig into my skin, leaving trails of passion down my back."
  "It's the kind of unbridled lust they show in movies."
  "Raw. Skin to skin. Sweat. Hunger. Victory."
  "We both wrestle for control, both wanting to devour the other whole.{space=-15}"
  "I lift her up against the wayward locker."
  "Her mouth paints my neck with lipstick marks. Love bites with a hint of teeth."
  "Her sweet taste fills my mouth, my veins... and it burns through me like the fiery streams down an erupting volcano."
  "Her words come out breathless. Feverish."
  show black onlayer screens zorder 100
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  isabelle locker_invitation "Go on, shag me..."
  "It's one of those things you've always dreamt about hearing."
  "It's more than just a request — it's validation that you're not unlovable. Not unfuckable."
  "[isabelle] wants me. She wants me bad."
  show isabelle locker_anticipation with dissolve2
  "She wraps her legs around my waist, pulling me closer."
  "Her labia opens like a rose in bloom, kissed by the morning dew."
  "Wet, ready, and absolutely gorgeous."
  "Our eyes meet for a moment. Desire and triumph burn in her gaze."
  "This is our victory lap."
  window hide
  show isabelle locker_penetration1 with Dissolve(.5)
  pause 0.2
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.3)
  pause 0.4
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.2)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration3 with Dissolve(.2)
  show isabelle locker_penetration2 with Dissolve(.2)
  show isabelle locker_penetration1 with Dissolve(.2)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  window auto
  isabelle locker_moan_penetration4 "Let her hear it! Let her feel it!"
  "[isabelle] gasps as I slam into her with force, every last inch of me pinning her against the locker."
  "I hold her there for a moment."
  "So deep inside her. Deeper than I ever thought possible."
  "The heat of her almost sends me over the edge. Her pulsating muscles squeeze me hard."
  window hide
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.1
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.1
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration4 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  pause 0.4
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  pause 0.4
  show isabelle locker_penetration3 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_moan_penetration4 with hpunch
  window auto
  maxine "Be careful. This is a priceless artifact."
  isabelle locker_moan_penetration4 "Oooh... S-shut up..."
  "Oh, god... The fact that we're doing it right on top of [maxine]..."
  "Only a thin layer of metal separating us..."
  "She probably feels our heat. She's probably sweating inside."
  "Our little bully prisoner."
  window hide
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.25)
  pause 0.15
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  pause 0.1
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  pause 0.1
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.25)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration2 with Dissolve(.15)
  show isabelle locker_penetration1 with Dissolve(.15)
  show isabelle locker_penetration2 with Dissolve(.1)
  show isabelle locker_penetration3 with Dissolve(.1)
  show isabelle locker_penetration4 with Dissolve(.25)
  window auto
  mc "F-fuck! I'm going to—"
  menu(side="middle"):
    extend ""
    "Come inside":
#     "God, I've been waiting so long for this..."
      show isabelle locker_cum_inside
      isabelle locker_cum_inside "Oh, my god!" with vpunch
      $quest.isabelle_hurricane["creampie"] = True
      # $isabelle["creampied"]+=1
      "Her pussy grips me hard as we come in unison."
      "The tight muscles milk me for every drop I'm worth."
      "It's the end of a perfect storm."
#     "She's my hurricane and we'll—"
    "Pull out":
      show isabelle locker_cum_outside2 with dissolve2
      "There's a special place this load going..."
      window hide
      show isabelle locker_cum_outside1 with vpunch
      show isabelle locker_cum_outside2 with Dissolve(.25)
      window auto
      maxine "That's wet..."
      mc "Does it taste good?"
      isabelle locker_cum_outside2 "Oh, my god! I can't believe you did that!"
      mc "She deserves it."
      isabelle locker_cum_outside2 "You're absolutely right."
      menu(side="middle"):
        extend ""
        "\"We should let her soak in it\nfor a few hours.\"":
          mc "We should let her soak in it for a few hours."
          isabelle locker_cum_outside2 "I'm inclined to keep her in there over the weekend, at least!"
          mc "As long as I get to fuck you on top of her again..."
#         isabelle locker_cum_outside2 "Oh, we're going to have a lot of fun with—"
        "?isabelle.lust>=6@[isabelle.lust]/6|{image= isabelle contact_icon}|{image=stats lust_3}|\"Your turn?\"":
          mc "Your turn?"
          isabelle locker_cum_outside2 "I have another idea..."
          mc "Oh, yeah?"
          isabelle locker_cum_outside2 "But maybe you should look away."
          mc "Why would I do that?"
          mc "As far as I'm concerned, she deserves it all."
          isabelle locker_cum_outside2 "Bloody right, she does!"
          isabelle locker_cum_outside2 "It's because of people like you that my sister is dead."
          isabelle locker_pee1 "I've been wanting to do this for years..."
          window hide
          show isabelle locker_pee2 with Dissolve(.5)
          window auto
          maxine "Pfft!"
          isabelle locker_pee2 "Yeah? Drink it up!"
          "Oh, wow! I can't believe [isabelle] did that!"
          "Is this really the girl that stands for justice and what's right?"
          "This has gone way further than I ever thought it would..."
          "I can't say I hate it... but I'm also not sure I like it."
#         isabelle locker_pee2 "Marinate in it, you pitiful slag."
#         isabelle locker_pee2 "Let me hear you beg to get out! The bullies never let my sister—"
  window hide
  show black onlayer screens zorder 100
  show isabelle locker_slide1
  with Dissolve(3.0)
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Huh?"
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  pause 0.25
  show isabelle locker_slide2 with Dissolve(.25)
  pause 0.25
  show isabelle locker_slide3 with Dissolve(.25)
  pause 0.1
  show isabelle locker_slide4
  pause 0.1
  show isabelle locker_slide5
  pause 0.05
  show isabelle locker_slide6
  pause 0.05
  show isabelle locker_slide7
  pause 0.1
  play sound "<from 0.25>water_splash2"
  show isabelle cringe at center
  show black onlayer screens zorder 100
  with vpunch
  $unlock_replay("isabelle_justice")
  $isabelle.unequip("isabelle_glasses")
  $isabelle.unequip("isabelle_shirt")
  $isabelle.unequip("isabelle_bra")
  $isabelle.unequip("isabelle_pants")
  $isabelle.unequip("isabelle_panties")
  $quest.isabelle_hurricane["locker"] = False
  pause 1.0
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Time seems to move in slow motion."
  "A moment of panic ripples through the glade."
  "What started out as a quest for justice can no longer be recognized...{space=-30}"
  "Everything can change in the blink of an eye."
  "Karma has a tendency to catch up with everyone sooner or later — and in our case, it's definitely sooner."
  "With a wet slurp, the stream swallows up the locker."
  "We rush into the water, trying to pull it up."
  "But it slips from us and sinks to the bottom."
  "We lift and struggle to get it up, but the only thing that surfaces is the crushing realization..."
  "It's too heavy."
  show black onlayer screens zorder 100
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  isabelle cringe "What have we done...?"
  mc "..."
  "The thought itself is abhorrent. It's one you never want inside\nyour head."
  "{i}We{/} caused this."
  "This has nothing to do with justice, and we caused this."
  "It's over..."
  "Everything is over..."
  "Everything is—"
  window hide
  show isabelle cringe at move_to(.25,1.0)
  show maxine neutral at appear_from_right(.75)
  pause 0.5
  show isabelle afraid with Dissolve(.5)
  window auto
  isabelle afraid "W-what?"
  mc "[maxine]? How?"
  maxine neutral "How what?"
  isabelle afraid "Y-you were in that locker!"
  isabelle afraid "I couldn't get the lock open..."
  isabelle afraid "You sank to the bottom..."
  maxine angry "I did no such thing!"
  mc "What do you mean?"
  maxine angry "I mean what I said. I did not sink to any bottom."
  maxine neutral "Now, where is my homunculus?"
  mc "...your what?"
  maxine annoyed "I've built a homunculus. Where is it?"
  isabelle skeptical "Those are not real..."
  mc "You know what it is?"
  isabelle concerned "Yeah, it was first popularized in 16th-century alchemy, and later\nin fiction."
  isabelle concerned "Basically, it's the creation of a small artificial person."
  maxine confident "Very nice! You know things."
  mc "So, you built a thing... a homunculus... that looks exactly like you?"
  maxine skeptical "Right. Have you seen it?"
  mc "Well..."
  isabelle cringe "This is weird as hell. I'm leaving."
  window hide
  pause 0.25
  $isabelle.equip("isabelle_panties")
  pause 1.0
  $isabelle.equip("isabelle_bra")
  pause 0.75
  show isabelle cringe at disappear_to_left
  pause 0.25
  show maxine skeptical at move_to(.5,1.0)
  window auto
  mc "..."
  mc "Can it breathe underwater?"
  maxine skeptical "I'm not sure. I haven't tried."
  mc "Uh, I have to go as well..."
  maxine annoyed "You're not going to help me look for it?"
  mc "Nope, I'm out of here!"
  window hide
  $school_forest_glade["exclusive"] = True
  call goto_school_entrance
  window auto
  "What the hell is actually going on?"
  "This has to be some kind of trick."
  "I feel nauseous..."
  $quest.isabelle_hurricane.finish()
  return

label quest_isabelle_hurricane_call_maxine_isabelle:
  show isabelle annoyed with Dissolve(.5)
  isabelle annoyed "Have you read \"The Lies of Locke Lamora?\""
  isabelle annoyed_left "I don't have access to horse piss, but I reckon we'll figure something out."
  hide isabelle with Dissolve(.5)
  return

label quest_isabelle_hurricane_call_maxine_locker:
  "How did [maxine] even get this here?"
  "Nothing makes sense anymore..."
  return

label quest_isabelle_hurricane_call_maxine_road:
  "[isabelle] did spare [spinach], but I doubt I'll be as lucky if I run off now.{space=-20}"
  return
