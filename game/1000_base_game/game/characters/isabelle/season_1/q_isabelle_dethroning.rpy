image isabelle_haggis_kiss = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-59,0),Transform("flora FloraIsabelleKiss",size=(206,116))),"ui circle_mask"))
image isabelle_locker_sex = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-37,0),Transform("isabelle lying_down_leg_raise",size=(206,116))),"ui circle_mask"))
image isabelle_hurricane_sex = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-41,0),Transform(LiveComposite((1920,1080),(0,0),"isabelle locker_anticipation",(764,376),"isabelle avatar events locker sex isabelle_left_arm1"),size=(206,116))),"ui circle_mask"))


label quest_isabelle_dethroning_start:
  show isabelle neutral with Dissolve(.5)
  mc "Hi, [isabelle]! I managed to score a meeting with [maxine]."
  mc "I've also found all the things you asked for."
  isabelle excited "That's terrific!"
  mc "..."
  isabelle concerned "Isn't it?"
  mc "Well, [maxine] isn't the easiest person to deal with."
  isabelle neutral "I'm sure we can handle her together."
  isabelle neutral "We just have to do a little bit of theater first."
  mc "What do you mean?"
  isabelle skeptical "[maxine] believes in a lot of strange things, right?"
  mc "She sure does."
  isabelle skeptical "Do you think she'll buy that there's a ghost pig roaming around\nthe school?"
  mc "Probably, yeah."
  isabelle excited "Great! Put on the pig mask, please, and I'll take a blurry photo\nto bring along as evidence."
  show isabelle excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"As long as I don't have to oink...\"":
      $quest.isabelle_dethroning["no_oinking"] = True
      show isabelle excited at move_to(.5)
      mc "As long as I don't have to oink..."
      isabelle confident "If it makes it more authentic, I don't see why you shouldn't?"
      mc "Don't push it."
      isabelle laughing "Sorry, sorry!"
      mc "If you think this will help take down [kate], then I'm all for it."
      $isabelle.lust+=2
      isabelle blush "That's such a good mindset!"
      isabelle blush "I always admire selflessness."
      mc "Yeah, yeah. Just take the damn photo, will you?"
      isabelle laughing "Fine, you old grump!"
      isabelle confident_phone "..."
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      window auto
      isabelle confident_phone "Want to see it?"
      mc "Sure."
      window hide
      show expression "isabelle avatar events ghost_pig isabelle_phone" as photo
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide isabelle
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      isabelle "It looks creepy, doesn't it?"
      mc "Thanks a lot, mate..."
      isabelle laughing "Hah! That's a proper English impression!"
      mc "I guess it does look scary. [maxine] will like it."
      window hide
      show isabelle blush
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide photo
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      isabelle blush "Perfect! Let's get going, then."
    "\"I think you should wear it instead.\"":
      show isabelle excited at move_to(.5)
      mc "I think you should wear it instead."
      isabelle concerned "Why?"
      show isabelle concerned at move_to(.25)
      menu(side="right"):
        extend ""
        "\"It just suits you better.\"":
          show isabelle concerned at move_to(.5)
          mc "It just suits you better."
          isabelle skeptical "What's that supposed to mean?"
          show isabelle skeptical at move_to(.75)
          menu(side="left"):
            extend ""
            "\"It, err... matches your hair?\"":
              show isabelle skeptical at move_to(.5)
              mc "It, err... matches your hair?"
              isabelle eyeroll "Is that right?"
              mc "Trust me, I dress myself every day. I know how to work\nan ugly face."
              isabelle annoyed "..."
              $mc.charisma+=1
              mc "I'm talking about the mask, of course."
              isabelle laughing "Hah!"
              isabelle confident "Fine, I'll wear it."
              mc "Great!"
              "There's something oddly hot about a girl in a mask... at least\nthat's what—"
              window hide
              show isabelle confident_pig with Dissolve(.5)
              window auto
              "Holy crap."
              isabelle confident_pig "Make sure you get my good side!"
              mc "Heh, okay."
              window hide
              pause 0.25
              show white onlayer screens zorder 100 with Dissolve(.125)
              play sound "camera_snap"
              hide white onlayer screens with Dissolve(.125)
              pause 0.25
              window auto
              mc "Got it!"
              isabelle confident_pig "Let me see!"
              window hide
              show expression "isabelle avatar events ghost_pig mc_phone" as photo
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide isabelle
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle "Damn, that looks creepy!"
              mc "Right? It's perfect."
              window hide
              show isabelle laughing
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide photo
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle laughing "Okay, let's go talk to [maxine]!"
            "\"It looks scarier with your long hair.\"":
              show isabelle skeptical at move_to(.5)
              $mc.intellect+=1
              mc "It looks scarier with your long hair."
              isabelle confident "Hmm... you're probably right."
              mc "You can put it on, I'll get the camera ready."
              isabelle laughing "One spooky piglet coming right up!"
              window hide
              show isabelle laughing_pig with Dissolve(.5)
              window auto
              mc "That looks really wacky."
              isabelle laughing_pig "Go on, take the photo!"
              window hide
              pause 0.25
              show white onlayer screens zorder 100 with Dissolve(.125)
              play sound "camera_snap"
              hide white onlayer screens with Dissolve(.125)
              pause 0.25
              window auto
              mc "Got it!"
              show isabelle confident_pig with dissolve2
              isabelle confident_pig "Let me see!"
              window hide
              show expression "isabelle avatar events ghost_pig mc_phone" as photo
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide isabelle
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle "Oh, that's creepy!"
              mc "Right? Well done!"
              isabelle "Thanks! You too!"
              window hide
              show isabelle laughing
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide photo
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle laughing "Okay, let's go meet up with [maxine]!"
            "\"I meant that there's no real difference.\"":
              show isabelle skeptical at move_to(.5)
              mc "I meant that there's no real difference."
              isabelle annoyed "Excuse me?"
              $mc.lust+=1
              mc "Same snout and bacon cheeks."
              isabelle annoyed "That's not even a bit funny."
              mc "It wasn't supposed to be."
              $isabelle.love-=2
              isabelle angry "Piss off, will you?"
              mc "Just put it on already..."
              isabelle angry "..."
              window hide
              show isabelle angry_pig with Dissolve(.5)
              window auto
              mc "Say \"Oink!\""
              window hide
              pause 0.25
              show white onlayer screens zorder 100 with Dissolve(.125)
              play sound "camera_snap"
              hide white onlayer screens with Dissolve(.125)
              pause 0.25
              window auto
              isabelle angry_pig "You're a real wanker."
              mc "Oh, come on! You look adorable!"
              isabelle angry_pig "Really, now?"
              mc "Have a look yourself."
              window hide
              show expression "isabelle avatar events ghost_pig mc_phone" as photo
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide isabelle
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle "I wouldn't exactly call that adorable."
              mc "Different strokes for different folks, I guess!"
              window hide
              show isabelle annoyed
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide photo
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle annoyed "Let's just go talk to [maxine]..."
        "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"I've taken photos for [maxine]\nin the past... I know what\nshe's looking for.\"":
          show isabelle concerned at move_to(.5)
          mc "I've taken photos for [maxine] in the past... I know what she's\nlooking for."
          isabelle confident "Oh, I didn't think of that!"
          mc "Don't worry, I'm sure you'll look stunning even in this mask."
          $isabelle.love+=2
          isabelle blush "Oh, stop it!"
          mc "Get this thing on and we'll see if I'm right."
          isabelle laughing "Hah! Fine."
          window hide
          show isabelle laughing_pig with Dissolve(.5)
          window auto
          mc "See? Stunning!"
          mc "In a... creepy and weird kind of way... but still..."
          isabelle laughing_pig "Just take the photo, will you?"
          window hide
          pause 0.25
          show white onlayer screens zorder 100 with Dissolve(.125)
          play sound "camera_snap"
          hide white onlayer screens with Dissolve(.125)
          pause 0.25
          window auto show
          show isabelle confident_pig with dissolve2
          isabelle confident_pig "All done?"
          mc "Yeah, I think I've captured true evil..."
          isabelle confident_pig "Let me see!"
          window hide
          show expression "isabelle avatar events ghost_pig mc_phone" as photo
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          hide isabelle
          hide black onlayer screens
          with Dissolve(.5)
          window auto
          isabelle "Oh, yeah, that's messed up!"
          isabelle "You're great with that camera. Do you think you could film me sometime when I'm reciting poetry?"
          mc "Sure, I don't mind."
          isabelle "Thank you!"
          window hide
          show isabelle blush
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          hide photo
          hide black onlayer screens
          with Dissolve(.5)
          window auto
          isabelle blush "Anyway, this photo is perfect. Let's go talk to [maxine]!"
        "\"Because I don't want to wear it.\"":
          show isabelle concerned at move_to(.5)
          mc "Because I don't want to wear it."
          isabelle concerned "Why not?"
          mc "It's degrading."
          isabelle skeptical "Why should {i}I{/} wear it, then?"
          show isabelle skeptical at move_to(.75)
          menu(side="left"):
            extend ""
            "\"You don't have to.\"":
              show isabelle skeptical at move_to(.5)
              mc "You don't have to."
              isabelle annoyed "So, you'll wear it, then?"
              mc "I didn't say that."
              $mc.intellect+=1
              mc "But this was {i}your{/} plan, remember?"
              isabelle sad "You're right..."
              isabelle sad "Fine. Just take the damn photo."
              window hide
              show isabelle sad_pig with Dissolve(.5)
              window auto
              mc "Okay, smile!"
              isabelle sad_pig "Hilarious..."
              window hide
              pause 0.25
              show white onlayer screens zorder 100 with Dissolve(.125)
              play sound "camera_snap"
              hide white onlayer screens with Dissolve(.125)
              pause 0.25
              window auto
              mc "Got it!"
              isabelle sad_pig "Thank god."
              window hide
              show isabelle laughing with Dissolve(.5)
              window auto
              isabelle laughing "I'm happy we've put that behind us."
              isabelle confident "How did the photo turn out?"
              mc "Have a look."
              window hide
              show expression "isabelle avatar events ghost_pig mc_phone" as photo
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide isabelle
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              mc "Pretty creepy, right? It's perfect for [maxine]."
              isabelle "You're right, it does look scary!"
              window hide
              show isabelle confident
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide photo
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle confident "Let's go talk to her, shall we?"
            "\"It's less degrading for you.\"":
              show isabelle skeptical at move_to(.5)
              mc "It's less degrading for you."
              isabelle eyeroll "And why is that?"
              mc "It just is."
              $isabelle.lust-=1
              isabelle annoyed "No reason, yeah?"
              isabelle annoyed "Well, it certainly is less degrading for {i}you.{/}"
              isabelle angry "You take the damn photo, then!"
              window hide
              show isabelle angry_pig with Dissolve(.5)
              window auto
              isabelle angry_pig "Go on!"
              window hide
              pause 0.25
              show white onlayer screens zorder 100 with Dissolve(.125)
              play sound "camera_snap"
              hide white onlayer screens with Dissolve(.125)
              pause 0.25
              window auto
              isabelle angry_pig "I hope you got what you wanted. I'm done with this."
              window hide
              show isabelle annoyed with Dissolve(.5)
              window auto
              mc "I guess this works..."
              isabelle annoyed "Brilliant. Let's go."
            "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"People might recognize me.\"":
              show isabelle skeptical at move_to(.5)
              mc "People might recognize me."
              isabelle thinking "How so?"
              mc "Mask, no mask — hardly makes a difference in my case."
              isabelle afraid "That's not true at all! Besides, looks aren't everything."
              mc "I guess I can wear it if you want, then..."
              $isabelle.love+=2
              $isabelle.lust+=1
              isabelle laughing "Don't worry about it! I'll wear it."
              isabelle confident "Just make sure you get a good shot, okay?"
              window hide
              show isabelle confident_pig with Dissolve(.5)
              window auto
              isabelle confident_pig "How do I look?"
              mc "Fabulous. Are you ready?"
              isabelle confident_pig "Go for it!"
              window hide
              pause 0.25
              show white onlayer screens zorder 100 with Dissolve(.125)
              play sound "camera_snap"
              hide white onlayer screens with Dissolve(.125)
              pause 0.25
              window auto
              mc "It's a masterpiece..."
              isabelle confident_pig "Let me see!"
              window hide
              show expression "isabelle avatar events ghost_pig mc_phone" as photo
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide isabelle
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle "That's perfect! Creepy... but perfect."
              mc "Do you think [maxine] will buy it?"
              isabelle "I'm the ghost pig and even I'm spooked!"
              window hide
              show isabelle laughing
              show black onlayer screens zorder 100
              with Dissolve(.5)
              pause 0.5
              hide photo
              hide black onlayer screens
              with Dissolve(.5)
              window auto
              isabelle laughing "Let's go talk to her, shall we?"
  hide isabelle with Dissolve(.5)
  if quest.isabelle_over_kate.in_progress:
    if game.quest_guide == "isabelle_over_kate":
      $game.quest_guide = "isabelle_dethroning"
    $quest.isabelle_over_kate.finish(silent=True)
    $quest.isabelle_dethroning.start(silent=True)
  else:
    $quest.isabelle_dethroning.start()
  return

label quest_isabelle_dethroning_meeting:
  show maxine sad at Transform(xalign=.25)
  show isabelle neutral at Transform(xalign=.75)
  with Dissolve(.5)
  mc "Excuse us."
  maxine sad "..."
  mc "[maxine]?"
  maxine sad "..."
  isabelle concerned "What's the matter with her?"
  mc "I'm not sure..."
  mc "[maxine]!"
  maxine sad "Be quiet. They're listening."
  isabelle skeptical "What?"
  mc "Who's listening, [maxine]?"
  maxine thinking "The souls of the perished."
  isabelle concerned "The souls of the perished?"
  maxine sad "The souls of the perished."
  isabelle eyeroll "I reckon they'll stick around, but I have a class in thirty, so I'd really appreciate your attention."
  maxine eyeroll "What do you want? I'm a busy woman."
  mc "What are you even busy with? Listening to ghosts?"
  maxine skeptical "{i}They're{/} listening. Weren't you listening?"
  mc "Honestly? Not really."
  mc "But you do owe us a meeting."
  show maxine afraid with Dissolve(.5)
  maxine afraid "Out of the question!" with vpunch
  isabelle laughing "Great. Now the souls of the perished definitely heard us."
  maxine cringe "Are you sure?"
  isabelle confident "Love, do you have time for us or not?"
  maxine sad "I suppose I did promise you both a meeting..."
  maxine thinking "Fine. Where do you want to meet?"
  mc "How about here?"
  show maxine afraid with Dissolve(.5)
  maxine afraid "Absolutely out of the question!" with vpunch
  mc "Where, then?"
  maxine thinking "Where the sun shines, but the raindrops never reach."
  window hide
  show maxine thinking at disappear_to_left
  pause 0.25
  show isabelle confident at move_to(.5,1.0)
  pause 0.25
  $maxine["at_none_today"] = True
  window auto show
  show isabelle thinking with dissolve2
  isabelle thinking "What's that supposed to mean?"
  mc "She already left... but it seems to be some kind of riddle."
  mc "I guess I can try to figure it out if you have class."
  isabelle flirty "I don't really have class. I just wanted her to get a move on."
  isabelle flirty "We can find her together."
  mc "Deal!"
  show isabelle flirty at disappear_to_left
  "Where the sun shines, but the raindrops never reach..."
  "Where could that be?"
  $quest.isabelle_dethroning.advance("riddle")
  $mc["focus"] = "isabelle_dethroning"
  return

label quest_isabelle_dethroning_climb_down:
  show isabelle confident with Dissolve(.5)
  isabelle confident "Thanks for helping me down."
  isabelle laughing "I can't believe we have to climb that bloody rope every time!"
  mc "I think it's to keep people out."
  isabelle confident "Surely, it must be against the school regulations?"
  mc "Well, [maxine] isn't exactly the type to care about regulations."
  isabelle laughing "Fair enough!"
  show isabelle laughing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I thought you didn't like rules?\"":
      show isabelle laughing at move_to(.5)
      mc "I thought you didn't like rules?"
      isabelle neutral "Obviously, some rules are required for society to work."
      isabelle neutral "Nothing is black and white."
      $mc.charisma+=2
      mc "Except a chessboard."
      isabelle excited "Fair enough!"
      mc "Come on, let's find [maxine]."
      isabelle excited "Lead the way!"
    "\"Do you have any book recommendations?\"":
      show isabelle laughing at move_to(.5)
      mc "Do you have any book recommendations?"
      isabelle excited "You better believe it!"
      isabelle excited "Anything specific you have in mind?"
      show isabelle excited at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Something literary.\"":
          show isabelle excited at move_to(.5)
          mc "Something literary."
          $isabelle.love+=2
          isabelle blush "That's... very exciting!"
          isabelle blush "I had no idea you were into finer literature!"
          mc "Well, what can I say? If it ain't avant-garde, it's a bit watered-down, isn't it?"
          isabelle blush "You really know how to speak to my heart. I'll text you some recs!"
          "[isabelle] looks really excited..."
          "If only I'd known this sooner, I would've taken her to the library\nday one."
          mc "You look flustered."
          isabelle laughing "I'm okay!"
        "\"Anything by Lovecraft.\"":
          show isabelle excited at move_to(.5)
          $flora.lust+=2
          mc "Anything by Lovecraft."
          isabelle laughing "I didn't know you were into eldritch horror."
          mc "Well, not really. I'm just trying to understand [flora] better."
          isabelle blush "Aw, that's sweet! Okay, I'll text you some recs."
          "If only she knew my motives aren't exactly pure..."
        "\"Do you know the movie 'Mean Girls?'\"":
          show isabelle excited at move_to(.5)
          mc "Do you know the movie \"Mean Girls?\""
          $kate.lust+=2
          mc "I think I have a crush on Regina George."
          isabelle eyeroll "Seriously?"
          mc "The heart wants what the heart wants."
          isabelle laughing "I guess I'll text you some recs."
          mc "Much appreciated!"
        "\"Do you know what shibari is?\"":
          show isabelle excited at move_to(.5)
          $nurse.lust+=2
          mc "Do you know what shibari is?"
          isabelle laughing "Sure! I'll text you some recs."
          mc "You're not weirded out?"
          isabelle confident "As long as everyone involved are consenting adults, it's none of\nmy business."
          "Wow, [isabelle] might be the most mature person I've met..."
          mc "Cool, thanks!"
      isabelle confident "Let's find [maxine], shall we?"
    "?quest.isabelle_locker.finished and (not quest.isabelle_locker['time_for_a_change'] or quest.isabelle_hurricane.finished)@|{image=isabelle_hurricane_sex}|\"What do you think\nabout [flora]?\"" if quest.isabelle_locker.finished and quest.isabelle_hurricane.finished:
      jump quest_isabelle_dethroning_homeroom_sex
    "?quest.isabelle_locker.finished and (not quest.isabelle_locker['time_for_a_change'] or quest.isabelle_hurricane.finished)@|{image=isabelle_locker_sex}|\"What do you think\nabout [flora]?\"" if (quest.isabelle_locker.finished and not quest.isabelle_locker["time_for_a_change"]) or not quest.isabelle_locker.finished:
      label quest_isabelle_dethroning_homeroom_sex:
      show isabelle laughing at move_to(.5)
      mc "What do you think about [flora]?"
      isabelle neutral "She's wild."
      mc "Too wild for you?"
      isabelle concerned "What do you mean?"
      if quest.isabelle_haggis['flora_kissed']:
        mc "I totally saw you kiss her earlier."
        isabelle skeptical "{i}She{/} kissed me!"
        mc "Did you like it?"
        isabelle laughing "Clearly, not as much as you!"
        mc "What an outrageous accusation!"
        isabelle confident "Is it?"
        isabelle confident "I wasn't born yesterday. I know many guys are into that."
        mc "Honestly, it was kind of hot, yeah."
        isabelle laughing "I bloody well knew it!"
        mc "Heh, what can I say? Two cute girls making out."
        isabelle afraid "It was barely a peck!"
        mc "I think it was more than that..."
        isabelle flirty "Maybe it could be. Do you have her number?"
        mc "Seriously?"
        isabelle laughing "No, silly! I'm just taking the piss."
        mc "Damn, you totally had me."
        isabelle confident "I'm actually interested in someone else..."
      else:
        mc "She's pretty wild is all I'm saying..."
        isabelle laughing "Did she put you up to this?"
        mc "No, I was just curious."
        isabelle confident "Well, I'm actually interested in someone else..."
      mc "Oh, who's the lucky girl?"
      isabelle blush "It's not a girl."
      mc "The [guard], then?"
      isabelle blush "Yes! There's something about a balding doughnut addict in his forties that just makes me so..."
      mc "..."
      isabelle laughing "Oh, come off it! You know exactly who I'm talking about!"
      mc "I can wait all day for you to say my name."
      isabelle confident "And I can wait all day for you to make me."
      show isabelle confident at move_to(.25)
      menu(side="right"):
        extend ""
        "\"I see how it is. Lock the door.\"":
          show isabelle confident at move_to(.5)
          mc "I see how it is. Lock the door."
          isabelle blush "You lost a word."
          mc "Look at me. I'm the captain now."
          isabelle sad "That's too bad..."
          isabelle sad "I'm not looking for a captain. I'm looking for a gentleman."
          "Ugh, [isabelle] is too much of a normie to get the memes..."
          mc "Would you {i}please{/} mind locking the door, love?"
          isabelle laughing "Is that your English impression?"
          mc "Everyone knows gentlemen are English."
          isabelle laughing "Oh, that's true!"
          window hide
          show isabelle laughing at disappear_to_right
          pause 1.0
          play sound "lock_click"
          pause 0.5
          show isabelle confident at appear_from_right
          pause 0.5
          window auto
          isabelle confident "All locked."
          mc "Then, come over here!"
          isabelle blush "Aye, aye, captain!"
          mc "I thought you weren't looking for a captain?"
          isabelle blush "Maybe I'll make an exception just this once..."
          window hide
          show isabelle desk_sex_lying_down
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 0.5
          hide black onlayer screens with Dissolve(.5)
          window auto
          mc "I do like exceptions."
          isabelle desk_sex_lying_down "Oh, yeah? What else do you like?"
          mc "Whatever is under that shirt of yours."
          window hide
          show isabelle desk_sex_lifting_top with Dissolve(.5)
          window auto
          isabelle desk_sex_lifting_top "These?"
          mc "Those are the ones."
          mc "Although..."
          isabelle desk_sex_lifting_top "Hmm?"
          mc "There's something blocking the view, that's all."
          window hide
          show isabelle desk_sex_lowering_bra with Dissolve(.5)
          window auto
          isabelle desk_sex_lowering_bra "How about now?"
          mc "Damn..."
          mc "How lucky am I?"
          isabelle desk_sex_lowering_bra "Let's find out?"
          window hide
          show isabelle desk_sex_spreading_legs with Dissolve(.5)
          window auto
          mc "I'm listening..."
          isabelle desk_sex_spreading_legs "And watching?"
          mc "Definitely watching."
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            isabelle desk_sex_spreading_legs "And what do you see?"
            mc "The hottest girl in the entire school."
            isabelle desk_sex_spreading_legs "Just the school?"
          else:
            window hide
            show isabelle desk_sex_removing_pants with Dissolve(.5)
            window auto
            isabelle desk_sex_removing_pants "And what do you see?"
            mc "The hottest girl in the entire school."
            isabelle desk_sex_removing_pants "Just the school?"
          window hide
          show isabelle desk_sex_pulling_panties with Dissolve(.5)
          window auto
          isabelle desk_sex_pulling_panties "How about now?"
          mc "God... sorry, I meant the hottest girl in Newfall!"
          mc "Although, I might have to verify it."
          isabelle desk_sex_pulling_panties "With a thermometer?"
          mc "Something like that..."
          window hide
          show isabelle desk_sex_licking1 with Dissolve(.5)
          window auto
          "Her pussy opens like a rose for me."
          "So juicy... so full of flavor..."
          isabelle desk_sex_licking1 "Mmmmh!"
          "That little whimper is one of the cutest things ever."
          "It does something to me."
          "Sends me into a frenzy of sexual lust."
          window hide
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.1
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.1
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          window auto
          "Men that don't like eating pussy are crazy. It's one of the tastiest appetizers there is."
          "And a good appetizer always enhances the meal."
          window hide
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          window auto
          isabelle desk_sex_licking1 "Oh, my god..."
          mc "You can say my name whenever you're ready."
          window hide
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking4 with Dissolve(.075)
          show isabelle desk_sex_licking3 with Dissolve(.075)
          show isabelle desk_sex_licking2 with Dissolve(.075)
          show isabelle desk_sex_licking1 with Dissolve(.075)
          window auto
          isabelle desk_sex_licking1 "Mhmmmm..."
          isabelle desk_sex_licking1 "F-fuck me, please..."
          mc "Not until you say my name."
          isabelle desk_sex_licking1 "You—"
          show isabelle desk_sex_fingering1
          isabelle desk_sex_fingering1 "Ooooh!" with vpunch
          "With two fingers, I press into her soft wet folds."
          "Touching a pussy is something I always wanted to do in my old life."
          window hide
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          window auto
          "The texture is like silk. Hot wet silk."
          "And the scent... my god... makes me shiver."
          window hide
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.05
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering1 with Dissolve(.075)
          show isabelle desk_sex_fingering2 with Dissolve(.075)
          show isabelle desk_sex_fingering3 with Dissolve(.075)
          show isabelle desk_sex_fingering4 with Dissolve(.075)
          window auto
          isabelle desk_sex_fingering4 "Ohhh! God!"
          mc "Say my name!"
          window hide
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering1 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering4 with Dissolve(.05)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering1 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering4 with Dissolve(.05)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering1 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering4 with Dissolve(.05)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering1 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering4 with Dissolve(.05)
          pause 0.0
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering1 with Dissolve(.05)
          show isabelle desk_sex_fingering2 with Dissolve(.05)
          show isabelle desk_sex_fingering3 with Dissolve(.05)
          show isabelle desk_sex_fingering4 with Dissolve(.05)
          window auto
          isabelle desk_sex_fingering4 "[mc]!"
          "Her pussy trembles around my fingers as I massage her G-spot."
          "Her body bucks under my hand, trying to get my fingers deeper."
          "It's one of the most beautiful things I've seen."
          "Light gasps, a sheen of sparkling perspiration on her skin, her muscles clenching."
          "God, it's so invigorating seeing her like this..."
          window hide
          show isabelle desk_sex_dick_reveal with Dissolve(.5)
          window auto
          isabelle desk_sex_dick_reveal "Oh, my..."
          mc "Grab my dick."
          isabelle desk_sex_dick_reveal "No please this time?"
          mc "None."
          isabelle desk_sex_dick_reveal "Such savagery..."
          window hide
          show isabelle desk_sex_grabbing_dick with Dissolve(.5)
          window auto
          "She won't let anyone order her around, but this is foreplay."
          "Sex can be an exception to one's principles."
          "That's something I've learned from her. She doesn't mind a bit of roleplay if it makes the sex hotter."
          mc "That's right. Good girl."
          "Female hands are so delicate, almost made to handle a dick."
          "[isabelle] would never allow this outside the bedroom, but now she bites her lip in anticipation."
          mc "Get it well and ready, because..."
          menu(side="middle"):
            extend ""
            "\"...I'm going to take you,\nright here on this table.\"":
              mc "...I'm going to take you, right here on this table."
              isabelle desk_sex_grabbing_dick "Oh, my goodness..."
              window hide
              show isabelle desk_sex_anticipation with Dissolve(.5)
              window auto
              "[isabelle] just spreads her legs wide, inviting me to ravish her."
              show isabelle desk_sex_vaginal1 with dissolve2
              "She's so wet and ready, sliding inside her is no hard task."
              "And she feels so good around the tip of my dick."
              "Her pussy, so soft and yet tight."
              "It's the best feeling in the world."
              "Pussies are the reason humanity has come this far, there's no doubt about that."
              window hide
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              pause 0.1
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              pause 0.1
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              pause 0.1
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              window auto
              isabelle desk_sex_vaginal4 "Mhmmmm..."
              "I go slow and steady, taking my time."
              "Enjoying every inch of her pussy to the fullest."
              "Letting my dick explore her insides."
              "It's so warm and gooey."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              window auto
              "Picking up the pace, her breath becomes ragged."
              "Her eyes water, glaze over, the pleasure getting to her head."
              "She gasps in pleasure as I slide in and out of her."
              "The muscles in her lower abdomen tremble with excitement."
              "That's when I start to go hard."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with vpunch
              pause 0.1
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with vpunch
              window auto
              "Our eyes meet as I slam into her."
              "There's something extra intense about fucking someone and looking into their eyes."
              "Hers are pleading and sparkling with tears of bliss."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              window auto
              "She wants me to take her to the edge."
              "Dance her next to the abyss."
              "Push her over down into the deepest depths of pleasure."
              "But I have other plans... and I'm in charge now."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal1 with Dissolve(.125)
              show isabelle desk_sex_vaginal2 with Dissolve(.125)
              show isabelle desk_sex_vaginal3 with Dissolve(.125)
              show isabelle desk_sex_vaginal4 with Dissolve(.125)
              window auto
              "I'm going to dangle her over the edge."
              "Tease her with the promise of pleasure."
              "Go agonizingly slow."
              "Extract every bit of sexual frustration."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              window auto
              "Sweat dots her forehead, blush raging across her cheeks."
              "She's frustrated, excited, mad, and devoured by the pleasure."
              "Her lips form silent words."
              "Pleading words that echo through her mind, but never reach\nmy ears."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              pause 0.15
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal1 with Dissolve(.15)
              show isabelle desk_sex_vaginal2 with Dissolve(.15)
              show isabelle desk_sex_vaginal3 with Dissolve(.15)
              show isabelle desk_sex_vaginal4 with Dissolve(.15)
              window auto
              "There's nothing quite like a girl's pussy, squeezing your dick in desperation."
              "She whines because the pleasure has gone to her head, and she needs that release."
              "That's when I pick up the pace again."
              "Giving her a taste of what's to come, while still staying completely in control."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal1 with Dissolve(.1)
              show isabelle desk_sex_vaginal2 with Dissolve(.1)
              show isabelle desk_sex_vaginal3 with Dissolve(.1)
              show isabelle desk_sex_vaginal4 with Dissolve(.1)
              window auto
              isabelle desk_sex_vaginal4 "P-please..."
              mc "Call me captain."
              isabelle desk_sex_vaginal4 "Please, captain!"
              mc "That's better."
              window hide
              show isabelle desk_sex_vaginal3 with Dissolve(.075)
              show isabelle desk_sex_vaginal2 with Dissolve(.075)
              show isabelle desk_sex_vaginal1 with Dissolve(.075)
              show isabelle desk_sex_vaginal2 with Dissolve(.075)
              show isabelle desk_sex_vaginal3 with Dissolve(.075)
              show isabelle desk_sex_vaginal4 with Dissolve(.075)
              pause 0.05
              show isabelle desk_sex_vaginal3 with Dissolve(.075)
              show isabelle desk_sex_vaginal2 with Dissolve(.075)
              show isabelle desk_sex_vaginal1 with Dissolve(.075)
              show isabelle desk_sex_vaginal2 with Dissolve(.075)
              show isabelle desk_sex_vaginal3 with Dissolve(.075)
              show isabelle desk_sex_vaginal4 with Dissolve(.075)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal1 with Dissolve(.05)
              show isabelle desk_sex_vaginal2 with Dissolve(.05)
              show isabelle desk_sex_vaginal3 with Dissolve(.05)
              show isabelle desk_sex_vaginal4 with Dissolve(.05)
              window auto show None
              show isabelle desk_sex_vaginal4_orgasm with dissolve2
              isabelle desk_sex_vaginal4_orgasm "Ohhhh!" with vpunch
              "My hard thrust sends an electric shock through her body."
              "Releases adrenaline, and floods her brain with pleasure chemicals."
              "Her eyes roll back as her pussy starts to spasm."
              "It didn't take much to bring on her cataclysmic orgasm."
              window hide
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.075)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.075)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.075)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.075)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.075)
              window auto
              "I hold her down and fuck her through tremors."
              "Her pussy squeezes me like never before."
              "Urges me to fill her up to the brim."
              "But that's just a natural reflex on her part. Her conscious mind has already ascended to Nirvana."
              "Leaving a shivering husk of pleasure behind, as her brain floats through space."
              window hide
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              window auto
              "And still I keep going, but I'm finally getting close myself."
              "Seeing the pleasure warping her pretty face is what does it for me this time."
              "Bringing another person to orgasm is sexy in and of itself."
              "But with [isabelle] it's extra special."
              "With her it's like erasing the modern woman, and bringing out something primal."
              "She's so strong and independent, which makes it extra hot to turn her into a pleasure-fueled mess."
              window hide
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal1_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal2_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal3_orgasm with Dissolve(.05)
              show isabelle desk_sex_vaginal4_orgasm with Dissolve(.05)
              window auto
              mc "Fuck, here it comes!"
              menu(side="middle"):
                extend ""
                "Fill her up":
                  window hide
                  show isabelle desk_sex_vaginal_cum_inside with vpunch
                  window auto
                  isabelle desk_sex_vaginal_cum_inside "Ohhhhhhh!"
                  "My hot semen floods her vagina, fills her up completely and squirts out around my dick."
                  "Our fluids mix in the ultimate lubricant."
                  window hide
                  show isabelle desk_sex_aftermath_inside with Dissolve(.5)
                  $quest.isabelle_dethroning["creampie"] = True
                  # $isabelle["creampied"]+=1
                  $isabelle.lust+=1
                  window auto
                  "A brief smile flickers across [isabelle]'s lips, in spite of her\nabsent mind."
                  "And that's what it's all about."
                  "Erasure of past doubts. Instilling a new and growing sense of\nself-worth."
                  "Nothing is quite like the feeling of orgasming together with another person."
                  "Knowing that you've made them feel better than anything else."
                  "It's the pride and joy of sex."
                "Pull out":
                  window hide
                  show isabelle desk_sex_cum_outside_orgasm with vpunch
                  show isabelle desk_sex_aftermath_outside with Dissolve(.5)
                  window auto
                  "My load shoots far, splashing across her stomach and tits."
                  "Still lost in her own cosmic orgasm, she licks her lips."
                  "Tastes my hot cum and smiles."
                  "It's the perfect ending to the perfect sex."
                  "A subconscious confirmation of enjoyment."
                  "Nothing more powerful than that."
            "?isabelle.lust>=6@[isabelle.lust]/6|{image= isabelle contact_icon}|{image=stats lust_3}|\"...I'm fucking\nyour ass today.\"":
              mc "...I'm fucking your ass today."
              isabelle desk_sex_grabbing_dick "Oh, my goodness..."
              "She blinks a couple times, wets her lips as if to say something."
              "Our eyes meet, and she tilts her head slightly."
              "I'm expecting outrage... or at the very least, reluctance."
              window hide
              show isabelle desk_sex_anticipation with Dissolve(.5)
              window auto
              "But she just looks at me, blush rising in her cheeks. Slowly, she shyly spreads her legs."
              "And when my dick settles against her wrinkly star, she doesn't move away."
              show isabelle desk_sex_anal1 with dissolve2
              "Instead, she pushes against my tip, taking me inside her ass."
              "The faintest of gasps escapes her lips as her blush deepens."
              "Clearly, the symbolism of taking a dick in your ass isn't lost on someone like [isabelle]."
              "To willingly accept pain to give someone else pleasure, that speaks volumes about a person."
              "And it's hard to think that a headstrong girl like her would be okay with such an unfavorable trade."
              "Yet here she is, submissively offering it to me."
              window hide
              show isabelle desk_sex_anal2 with Dissolve(.15)
              show isabelle desk_sex_anal3 with Dissolve(.15)
              show isabelle desk_sex_anal4 with Dissolve(.15)
              pause 0.2
              show isabelle desk_sex_anal3 with Dissolve(.15)
              show isabelle desk_sex_anal2 with Dissolve(.15)
              show isabelle desk_sex_anal1 with Dissolve(.15)
              show isabelle desk_sex_anal2 with Dissolve(.15)
              show isabelle desk_sex_anal3 with Dissolve(.15)
              show isabelle desk_sex_anal4 with Dissolve(.15)
              pause 0.2
              show isabelle desk_sex_anal3 with Dissolve(.15)
              show isabelle desk_sex_anal2 with Dissolve(.15)
              show isabelle desk_sex_anal1 with Dissolve(.15)
              show isabelle desk_sex_anal2 with Dissolve(.15)
              show isabelle desk_sex_anal3 with Dissolve(.15)
              show isabelle desk_sex_anal4 with Dissolve(.15)
              window auto
              "I give it to her slowly, steadily."
              "She muffles a moan with her hand."
              "Her ass clenches around my dick, but with every stroke, she takes me deeper."
              window hide
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal1 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal4 with Dissolve(.125)
              pause 0.15
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal1 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal4 with Dissolve(.125)
              pause 0.15
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal1 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal4 with Dissolve(.125)
              pause 0.15
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal1 with Dissolve(.125)
              show isabelle desk_sex_anal2 with Dissolve(.125)
              show isabelle desk_sex_anal3 with Dissolve(.125)
              show isabelle desk_sex_anal4 with Dissolve(.125)
              window auto
              "She might not even orgasm from anal, and that somehow makes it even hotter."
              "While I derive all the pleasure from her hot ass, she just gets pain and not much else."
              "It's an unfair trade that most girls would never make."
              "But [isabelle] isn't most girls."
              window hide
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal1 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal4 with Dissolve(.1)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal1 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal4 with Dissolve(.1)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal1 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal4 with Dissolve(.1)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal1 with Dissolve(.1)
              show isabelle desk_sex_anal2 with Dissolve(.1)
              show isabelle desk_sex_anal3 with Dissolve(.1)
              show isabelle desk_sex_anal4 with Dissolve(.1)
              window auto
              "She will take it for me."
              "If that's not romantic and selfless, I don't know what is!"
              window hide
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              window auto
              isabelle desk_sex_anal4 "Oh, my god! Careful!"
              "It's hard to contain the urge to just ram it home. Very hard."
              "But hurting her too much would just be cruel when she's offering me something so priceless."
              window hide
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              pause 0.1
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal1 with Dissolve(.075)
              show isabelle desk_sex_anal2 with Dissolve(.075)
              show isabelle desk_sex_anal3 with Dissolve(.075)
              show isabelle desk_sex_anal4 with Dissolve(.075)
              window auto
              isabelle desk_sex_anal4 "Ow! Ow! Ow!"
              "God, that feels so good."
              "The heat from her ass is radiating through my dick, into my pelvis, making me sweat bullets."
              "The way her anal muscles try to push me out is driving me crazy."
              window hide
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.05
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              pause 0.0
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal1 with Dissolve(.05)
              show isabelle desk_sex_anal2 with Dissolve(.05)
              show isabelle desk_sex_anal3 with Dissolve(.05)
              show isabelle desk_sex_anal4 with Dissolve(.05)
              window auto
              isabelle desk_sex_anal4 "Oh, god!"
              mc "Say my name!"
              isabelle desk_sex_anal4 "[mc]!"
              isabelle desk_sex_anal4 "Ahhh! [mc]!"
              "Fuck, I'm about to explode!"
              menu(side="middle"):
                extend ""
                "Pull out":
                  window hide
                  show isabelle desk_sex_cum_outside with vpunch
                  show isabelle desk_sex_aftermath_outside with Dissolve(.5)
                  window auto
                  "With a roar of satisfaction, I pull out and blast my seed all over her."
                  isabelle desk_sex_aftermath_outside "Oh, wow..."
                  mc "Took the words right out of my mouth. Hot damn."
                  mc "Are you okay?"
                "Anal creampie":
                  window hide
                  show isabelle desk_sex_anal_cum_inside with vpunch
                  window auto
                  "In a final deep thrust, I push myself all the way in, emptying my balls inside her bowels."
                  isabelle desk_sex_anal_cum_inside "Did you just...?"
                  show isabelle desk_sex_aftermath_inside with dissolve2
                  mc "Sorry, I didn't have time..."
                  $isabelle.lust+=2
                  isabelle desk_sex_aftermath_inside "It's okay! Don't worry about it."
                  "Knowing that she'll have to walk around with my cum in her ass the rest of the day..."
                  "...is just so incredibly hot."
                  mc "Are {i}you{/} okay, though?"
              isabelle "I... think so."
              mc "I really went hard in the end."
              isabelle "I noticed!"
              mc "We should definitely do this again another time."
              isabelle "..."
              mc "What? Was it that bad?"
              isabelle "Well, I... would've liked to have an orgasm."
              menu(side="middle"):
                extend ""
                "\"Next time, I'll take care of it first.\"":
                  mc "Next time, I'll take care of it first."
                  isabelle "I think that will be a must if you ever want to fuck my ass again..."
                  mc "Hehe, deal!"
                "\"I think it's hot that you didn't.\"":
                  mc "I think it's hot that you didn't."
                  isabelle "That's..."
                  isabelle "...I don't know, a little twisted?"
                  mc "It is."
                  isabelle "I think I get it, though."
                  mc "You do?"
                  isabelle "Sure. It's about putting someone else above your own gratification."
                  isabelle "I definitely see the appeal."
                  $isabelle.lust+=1
                  isabelle "If it's something that really turns you on, we can play that game sometimes."
                  mc "God, you're perfect."
          $unlock_replay("isabelle_exception")
          $school_homeroom["isabelle_sex"]+=1
          window hide
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            show isabelle laughing
          else:
            $isabelle.unequip("isabelle_pants")
            show isabelle laughing_untucked
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause 1.5
          hide black onlayer screens with Dissolve(.5)
          window auto
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            isabelle laughing "Let's find [maxine] now, shall we?"
          else:
            isabelle laughing_untucked "Let's find [maxine] now, shall we?"
            window hide
            $isabelle.equip("isabelle_pants")
            show isabelle laughing with Dissolve(.5)
            pause 0.25
            window auto
        "\"Perhaps later. We need to find [maxine].\"":
          show isabelle confident at move_to(.5)
          mc "Perhaps later. We need to find [maxine]."
          isabelle excited "You're right! Let's go!"
  hide isabelle with Dissolve(.5)
  $quest.isabelle_dethroning["school_regulations"] = True
  return

label quest_isabelle_dethroning_riddle:
  show isabelle smile at appear_from_left(.25)
  show maxine afraid at appear_from_right(.75)
  pause 0.5
  $maxine["at_none_today"] = False
  $mc["focus"] = ""
  maxine afraid "What took you so long?"
  isabelle eyeroll "Oh, I don't know, running around the school trying to figure out\na damn riddle?"
  maxine thinking "Why would you waste your time like that? You should've come here immediately."
  isabelle annoyed "Don't you try putting this on us!"
  mc "Ladies! Let's not get too heated. We have bigger fish to fry."
  isabelle blush "You're right."
  mc "[maxine], let's have that meeting right now. No more games."
  maxine thinking "Games?"
  mc "Look, can we have the meeting or not?"
  maxine sad "I suppose we could..."
  mc "So, we have a story you might be interested in."
  isabelle blush "Newspaper headlines."
  maxine thinking "I'm listening."
  mc "Have you heard of the devil pig?"
  maxine confident "Of course! Who hasn't?"
  mc "Well, we caught a photo of it inside the school."
  maxine skeptical "The devil-pig hasn't been spotted outside of Papua New Guinea since 1906."
  isabelle confident "...alive."
  maxine annoyed "What?"
  isabelle laughing "It hasn't been spotted {i}alive{/} since 1906. The one we're talking about has attained a more... incorporeal form."
  mc "Basically, it's a ghost pig, and we have the proof right here."
  mc "Take a look at this photo."
  window hide
  if quest.isabelle_dethroning["no_oinking"]:
    show maxine annoyed_isabelle_phone with Dissolve(.5)
  else:
    show maxine annoyed_mc_phone with Dissolve(.5)
  pause 0.5
  window auto show
  if quest.isabelle_dethroning["no_oinking"]:
    show maxine confident_isabelle_phone with dissolve2
    maxine confident_isabelle_phone "Hmm... very intriguing..."
  else:
    show maxine confident_mc_phone with dissolve2
    maxine confident_mc_phone "Hmm... very intriguing..."
  isabelle concerned "It attacked us out of nowhere. We're lucky to be alive."
  mc "The fact that we caught it with the camera is a small miracle."
  if quest.isabelle_dethroning["no_oinking"]:
    maxine confident_isabelle_phone "The photo does seem to be unmanipulated..."
  else:
    maxine confident_mc_phone "The photo does seem to be unmanipulated..."
  isabelle skeptical "We showed it to the [guard], but he ignored it."
  mc "People have a right to know they might be in danger, don't you think?{space=-30}"
  if quest.isabelle_dethroning["no_oinking"]:
    maxine annoyed_isabelle_phone "This is true..."
  else:
    maxine annoyed_mc_phone "This is true..."
  maxine annoyed "Okay, fax me the picture."
  mc "Fax?!"
  isabelle excited "It's fine, I'll handle it!"
  isabelle neutral "My dad is a lawyer, and he has an old fax machine in the garage."
  maxine confident "Perfect. Now, if you'll excuse me, I have inverted flects to tend to."
  mc "Have fun, I guess."
  maxine thinking "That's like saying \"have fun\" to a soldier going to war."
  maxine thinking "War is never fun."
  window hide
  show maxine thinking behind isabelle at disappear_to_left
  pause 0.25
  show isabelle neutral at move_to(.5,1.0)
  pause 0.25
  window auto show
  show isabelle laughing with dissolve2
  isabelle laughing "That didn't take a lot of convincing."
  mc "Yeah, well. Fortunately for us, [maxine] will believe in anything."
  mc "So, what's next?"
  isabelle confident "Everything is ready. Now, we just need to make sure [kate] sees\nthe article."
  isabelle confident "Then, during the Independence Day dinner, just before the cheerleader tryouts..."
  isabelle confident "...that's when we'll strike."
  mc "But what are we going to do with the paint and stuff?"
  isabelle flirty "We'll scare her witless with the mask."
  isabelle flirty "She'll run, trip, splash — tryouts missed."
  mc "All right, then. Are you sure about this?"
  mc "Being a cheerleader is basically her entire identity. She'll be furious."
  isabelle cringe "I don't think anything else will teach her a lesson."
  mc "You're probably right..."
  mc "Okay, I'll make sure the newspaper about the ghost pig crosses\nher path."
  isabelle flirty "Lovely! I'll see you later, then!"
  show isabelle flirty at disappear_to_left
  "It'll probably take a day or two before [maxine] has printed up a new issue of the school paper..."
  "I'll just have to keep checking the newspaper stand."
  $quest.isabelle_dethroning.advance("sleep")
  return

label quest_isabelle_dethroning_newspaper_kate:
  show kate eyeroll with Dissolve(.5)
  kate eyeroll "What exactly do you think you're doing this close to me?"
  mc "Err..."
  "Crap. If I just give her the newspaper, she'll get suspicious."
  kate skeptical "Go grunt somewhere else. You're disturbing the peace."
  hide kate with dissolve2
  "Hmm... I probably need to plant it somewhere she'll find it..."
  $quest.isabelle_dethroning["disturbing_the_peace"] = True
  return

label quest_isabelle_dethroning_newspaper:
  "[kate] always sits down to take her shoes off... which means the trophy case is right in her field of view."
  "If I could somehow get the newspaper inside the glass, that would be perfect."
  "Unfortunately, it's locked, and only [jo] has the key."
  $quest.isabelle_dethroning.advance("key")
  return

label quest_isabelle_dethroning_key:
  show jo smile with Dissolve(.5)
  jo smile "What's going on, kiddo?"
  mc "I'm in a bit of a pickle."
  jo skeptical "That doesn't sound good."
  show jo skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.strength>=5@[mc.strength]/5|{image=stats str}|\"I lost a bet with the\ncheerleading squad.\"":
      show jo skeptical at move_to(.5)
      mc "I lost a bet with the cheerleading squad."
      jo cringe "Gambling, [mc]? Really?"
      mc "It wasn't really gambling... just a silly bet."
      jo cringe "Well, how much do you owe them?"
      mc "Nothing! I mean, I don't owe them money."
      jo displeased "What, then?"
      mc "I'm supposed to polish some of the trophies in the sports wing."
      jo laughing "Okay, that's not as bad as I thought!"
      jo excited "I'm glad you're making friends this year. Even if I don't approve of gambling."
      mc "It wasn't gambling! I just lost a weightlifting competition!"
      jo thinking "...against the cheerleading squad?"
      mc "They're stronger than they look!"
      jo worried "I don't know if I believe you."
      mc "Fine! I let them win, okay?"
      mc "I had fun, though."
      jo blush "And that's what matters!"
      jo blush "You used to be so competitive, but now you're okay with losing to make others happy."
      "Since when? It's like she doesn't know that I've been losing at everything all my life."
      "Must be a blindspot of some sort..."
      $jo.lust+=2
      jo blush "I'm proud of you."
      mc "Well, could you unlock the trophy case for me, then?"
      jo flirty "Sure! I'll unlock it on my way to the gym. I happen to have a meeting{space=-5}\nin there anyway."
    "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|\"I think the janitor forgot\nto clean the trophy case.\"":
      show jo skeptical at move_to(.5)
      mc "I think the janitor forgot to clean the trophy case."
      jo afraid "That doesn't sound like her. She's a very thorough lady."
      mc "Well, there's grease and fingerprints on the inside..."
      mc "I don't mind cleaning it up. The school's pride is important, after all."
      jo eyeroll "I don't recall you ever caring about that."
      mc "I, err... I'm trying to make an effort this last year?"
      $jo.love+=2
      jo blush "This gladdens me."
      jo blush "All right, I'll unlock it on my way to the gym. I happen to have\na meeting in there anyway."
    "\"I can't tell you right now,\nbut pray for me.\"":
      show jo skeptical at move_to(.5)
      mc "I can't tell you right now, but pray for me."
      jo worried "What did you just say?"
      mc "I have to go."
      jo embarrassed "Now, wait just one minute, young man!"
      window hide
      play sound "fast_whoosh"
      show black onlayer screens zorder 100 with hpunch
      $game.location = "school_entrance"
      pause 1.0
      hide jo
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "Phew! Made it out alive!"
      "Too bad I have to face her again soon if I ever want that trophy case opened..."
      return
  $jo["at_none_now"] = True
  window hide
  if renpy.showing("jo flirty"):
    show jo flirty at disappear_to_right
  elif renpy.showing("jo blush"):
    show jo blush at disappear_to_right
  pause 1.0
  $quest.isabelle_dethroning.advance("trophy_case")
  window auto
  return

label quest_isabelle_dethroning_trophy_case(item):
  if item == "newspaper":
    "At least [jo] didn't forget to unlock it..."
    window hide
    $school_first_hall_east["newspaper"] = True
    $mc.remove_item("newspaper")
    window auto
    "There we go."
    "I don't need to stick around for this. [kate] won't miss it."
    "Now, I just have to wait for Independence Day..."
    $quest.isabelle_dethroning.advance("independence_day")
  else:
    "As much as I'd like to show [kate] my [item.title_lower], she's not ready for that. Not yet."
    $quest.isabelle_dethroning.failed_item("trophy_case",item)
  return

label quest_isabelle_dethroning_announcement_cafeteria_doors:
  "Oh, [isabelle] is just about to make the dinner announcement!"
  "Look at her getting up on a chair and everything..."
  "I probably should wait until she's done."
  isabelle "Hi, everyone! I was hoping I could take a few moments out of\nyour lunch today to make an announcement!"
  isabelle "I'm new here and don't really know most of you, but I'd like to change that!"
  isabelle "That's why I'd like to be the host for the Independence Day\nstudent dinner this year!"
  isabelle "I would be very happy if you all showed up!"
  "It takes some serious balls to just stand up like that and make\nan announcement to the entire cafeteria."
  "It's hard not to be impressed... and a little turned on."
  isabelle "I'll be sending out invitations today! Thank you!"
  "..."
  "Well, I don't hear any booing, so that's good."
  jump goto_school_cafeteria

label quest_isabelle_dethroning_announcement:
  show isabelle blush with Dissolve(.5)
  mc "That was very brave of you."
  isabelle blush "Thank you!"
  isabelle sad "I've been so caught up in the feud with [kate] that I've neglected\nthe rest of our classmates."
  isabelle blush "I'd like to get to know them a bit better."
  show isabelle blush at move_to(.25,1.0)
  show kate laughing at appear_from_right(.75)
  kate laughing "[mc], did you tell her yet?"
  isabelle annoyed "Tell me what?"
  kate smile "That students aren't allowed to host parties at the school."
  "Right, [jo] specifically prohibited it after a series of out-of-control birthday parties during our sophomore year..."
  "Thanks a lot, jocks."
  show isabelle annoyed at move_to("left")
  show kate smile at move_to("right")
  menu(side="middle"):
    extend ""
    "\"I'm sorry, but [kate]'s right...\"":
      show isabelle annoyed at move_to(.25)
      show kate smile at move_to(.75)
      mc "I'm sorry, but [kate]'s right..."
      $kate.lust+=1
      kate gushing "And don't you forget it!"
      isabelle thinking "I guess we'll just do it as a picnic outside, then..."
      kate gushing "Also not allowed."
      isabelle cringe "You're lying."
      if school_entrance["crop"] or school_entrance["crop_today"]:
        kate gushing "Someone recently burnt strange messages into the front lawn."
        kate gushing "It's been banned ever since."
        mc "It was aliens!"
        kate laughing "You're a meme."
      else:
        kate gushing "Someone once scorched the front lawn with one of those\nNot-a-Flamethrowers."
        kate gushing "It's been prohibited ever since."
        isabelle annoyed "Seriously?"
        kate laughing "Quite the rager, that party."
        "So, that's what all the burnt grass was about. [jo] was really angry."
      isabelle annoyed "What's your problem, [kate]?"
      kate laughing "I didn't make the rules."
      isabelle annoyed "But you sure like rubbing it in my face."
      kate smile "It's my duty as the class president to inform you of them."
      kate smile "And you've just been informed. You're welcome!"
    "\"That's the rule, but I could\ntry talking to the principal.\"":
      show isabelle annoyed at move_to(.25)
      show kate smile at move_to(.75)
      mc "That's the rule, but I could try talking to the principal."
      $isabelle.love+=1
      isabelle blush "Aw, thanks! That would be great."
      kate laughing "You'd have more luck talking the [guard] into a doughnut-free diet."
      isabelle annoyed "Stay out of this, [kate]."
      kate smile "It's my duty as the class president to inform you of the rules."
      kate smile "And you've just been informed. You're welcome!"
    "\"Get lost, [kate]! You're not invited!\"":
      show isabelle annoyed at move_to(.25)
      show kate smile at move_to(.75)
      mc "Get lost, [kate]! You're not invited!"
      $isabelle.love-=1
      $isabelle.lust+=2
      isabelle sad "Everyone's invited. Even [kate]."
      $kate.lust-=1
      kate laughing "A dinner in the school cafeteria? Thanks, but no thanks."
      kate laughing "Besides, there won't be one. The rules prohibit it."
    "?mc.intellect>=3@[mc.intellect]/3|{image=stats int}|\"It's true, but I have an idea.\"":
      show isabelle annoyed at move_to(.25)
      show kate smile at move_to(.75)
      mc "It's true, but I have an idea."
      kate laughing "Watch him try to trick you into a two-man party behind the bleachers!{space=-35}"
      isabelle concerned "[mc] is not like that."
      kate eyeroll "Seriously? He's as thirsty as they come."
      isabelle skeptical "I'll make up my own mind, thank you very much."
      kate excited "Your funeral... and I mean that literally."
      kate excited "He probably already has a folder with your name on his computer."
      window hide
      $quest.isabelle_dethroning.advance("principal")
      if kate.at("school_cafeteria"):
        hide kate with Dissolve(.5)
      else:
        show kate excited at disappear_to_right
        pause 0.25
      show isabelle skeptical at move_to(.5,1.0)
      pause 0.25
      window auto show
      show isabelle angry with dissolve2
      isabelle angry "Gah! [kate] makes my blood boil!"
      isabelle angry "I can't wait to set her straight! She so needs a proper lesson!"
      mc "She's always been like this."
      isabelle sad "We're going to change that soon. Mark my words."
      isabelle blush "Anyway, what's your idea?"
      mc "I, err... I don't really have an idea other than talking to the principal."
      mc "I just figured [kate] would leave if I said I had a solution..."
      isabelle laughing "Smart! She does seem to feed off of frustration."
      isabelle confident "If you could try to talk to [jo], that would be superb! I promise\nit'll just be dinner and nothing crazy."
      mc "I'll do my best."
      isabelle confident "Thank you, [mc]."
      if isabelle.at("school_cafeteria"):
        hide isabelle with dissolve2
      else:
        show isabelle confident at disappear_to_right
      "This seems really important to [isabelle]. I better not screw it up."
      return
  kate "Later, nerds."
  window hide
  $quest.isabelle_dethroning.advance("principal")
  if kate.at("school_cafeteria"):
    hide kate with Dissolve(.5)
  else:
    if renpy.showing("kate smile"):
      show kate smile at disappear_to_right
    elif renpy.showing("kate laughing"):
      show kate laughing at disappear_to_right
    pause 0.25
  if renpy.showing("isabelle annoyed"):
    show isabelle annoyed at move_to(.5,1.0)
  elif renpy.showing("isabelle sad"):
    show isabelle sad at move_to(.5,1.0)
  pause 0.25
  window auto show
  show isabelle angry with dissolve2
  isabelle angry "Gah! [kate] makes my blood boil!"
  isabelle angry "I can't wait to set her straight! She so needs a proper lesson!"
  mc "She's always been like this."
  isabelle sad "We're going to change that soon. Mark my words."
  isabelle blush "Anyway, if you could try to talk to [jo], that would be superb!\nI promise it'll just be dinner and nothing crazy."
  mc "I'll do my best."
  isabelle blush "Thank you, [mc]."
  if isabelle.at("school_cafeteria"):
    hide isabelle with dissolve2
  else:
    show isabelle blush at disappear_to_right
  "This seems really important to [isabelle]. I better not screw it up."
  return

label quest_isabelle_dethroning_principal:
  show jo flirty with Dissolve(.5)
  jo flirty "Hi, honey! How's your day so far?"
  mc "Passable, I guess."
  jo eyeroll "Well, as long as it's not bad."
  mc "I was wondering if you could make an exception to one of\nthe school rules?"
  jo worried "Sorry, hon. I don't think that's possible."
  jo worried "It would reflect very poorly on me if I started to make exceptions for you..."
  mc "It's not for me, though."
  mc "There's a new girl in my class and she'd like to host an Independence{space=-30}\nDay dinner to get to know everyone better."
  jo blush "Aw, that's a good cause!"
  jo sad "Still, rules are rules, I'm afraid."
  show jo sad at move_to(.75)
  menu(side="left"):
    extend ""
#   "\"[jo], please! This might be your\nonly chance at grandchildren!\"": ## Incest patch ##
#     show jo sad at move_to(.5)
#     mc "[jo], please! This might be your only chance at grandchildren!"
#     ...
    "\"What if I do exceptionally well\nin a subject of your choice?\"":
      show jo sad at move_to(.5)
      mc "What if I do exceptionally well in a subject of your choice?"
      jo concerned "Honey, your grades have been dropping across the board..."
      mc "I know, but with the new program, I feel like I can reach my\nfull potential!"
      mc "I can't do everything at once, though."
      jo smile "That's great news, and I suppose you're right."
      jo smile "I'll tell you what — once you complete an assignment with a good grade, I'll sign off on the dinner."
      jo neutral "You're still going to need adult supervision, and it's going to be\na dinner, not a party."
      mc "That works for me! Thanks, [jo]!"
      hide jo with dissolve2
      "Let's see... the English poetry assignment is probably the simplest one to complete."
      "As long as no one dies while I'm working on it, everything should\nbe fine."
      "Can't be too careful around this stuff."
      $quest.isabelle_dethroning.advance("poem")
      return
    "\"[flora] got a new makeup mirror...\"":
      show jo sad at move_to(.5)
      mc "[flora] got a new makeup mirror..."
      jo concerned "The lights of her old one broke! It's an early Christmas present."
      mc "Can't this be an early Christmas present for me, then?"
      jo neutral "That's... actually very selfless of you."
      $jo.love+=1
      jo smile "I see that you really want to help the new girl."
      jo smile "I'll tell you what — if you get adult supervision, I'll allow it."
      mc "We're all adults..."
      jo skeptical "Well, I want a member of the staff present."
      mc "I guess that's fine. Thanks, [jo]! You're the best!"
      jo laughing "Aw, honey! You really know how to make this old lady happy."
#     jo laughing "Aw, honey! You really know how to make your old mom happy." ## Incest patch ##
      jo excited "I'd like to chat more, but I need to get back to work now."
      hide jo with Dissolve(.5)
      $quest.isabelle_dethroning.advance("good_news")
      return
  return

label quest_isabelle_dethroning_poem:
  "Okay, let's check out the blackboard for this week's assignment..."
  $quest.isabelle_dethroning["weekly_assignment"] = True
  return

label quest_isabelle_dethroning_poem_blackboard:
  "{i}\"This week's assignment is to write a poem about love.\"{/}"
  menu(side="middle"):
    extend ""
    "Write the poem":
      "Hopefully, that documentary on Shakespeare's sex life will help..."
      window hide
      show minigames poem background as minigame_background
      show minigames poem flora_sticker as minigame_flora at sticker_leftmost onlayer screens
      show minigames poem kate_sticker as minigame_kate at sticker_mid onlayer screens
      show minigames poem maxine_sticker as minigame_maxine at sticker_rightmost onlayer screens
      show minigames poem isabelle_sticker as minigame_isabelle at sticker_left onlayer screens
      show minigames poem lindsey_sticker as minigame_lindsey at sticker_right onlayer screens
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $game.ui.hide_hud = True
      pause 0.5
      hide screen interface_hider
      hide black onlayer screens
      with Dissolve(.5)
      $renpy.transition(Dissolve(0.25))
      call start_poem_minigame
      if hop == flora:
        show flora_sticker hop as minigame_flora at sticker_leftmost onlayer screens
        show isabelle_sticker as minigame_isabelle at sticker_left onlayer screens
        show kate_sticker as minigame_kate at sticker_mid onlayer screens
        show lindsey_sticker as minigame_lindsey at sticker_right onlayer screens
        show maxine_sticker as minigame_maxine at sticker_rightmost onlayer screens
      elif hop == kate:
        show flora_sticker as minigame_flora at sticker_leftmost onlayer screens
        show isabelle_sticker as minigame_isabelle at sticker_left onlayer screens
        show kate_sticker hop as minigame_kate at sticker_mid onlayer screens
        show lindsey_sticker as minigame_lindsey at sticker_right onlayer screens
        show maxine_sticker as minigame_maxine at sticker_rightmost onlayer screens
      elif hop == maxine:
        show flora_sticker as minigame_flora at sticker_leftmost onlayer screens
        show isabelle_sticker as minigame_isabelle at sticker_left onlayer screens
        show kate_sticker as minigame_kate at sticker_mid onlayer screens
        show lindsey_sticker as minigame_lindsey at sticker_right onlayer screens
        show maxine_sticker hop as minigame_maxine at sticker_rightmost onlayer screens
      elif hop == isabelle:
        show flora_sticker as minigame_flora at sticker_leftmost onlayer screens
        show isabelle_sticker hop as minigame_isabelle at sticker_left onlayer screens
        show kate_sticker as minigame_kate at sticker_mid onlayer screens
        show lindsey_sticker as minigame_lindsey at sticker_right onlayer screens
        show maxine_sticker as minigame_maxine at sticker_rightmost onlayer screens
      elif hop == lindsey:
        show flora_sticker as minigame_flora at sticker_leftmost onlayer screens
        show isabelle_sticker as minigame_isabelle at sticker_left onlayer screens
        show kate_sticker as minigame_kate at sticker_mid onlayer screens
        show lindsey_sticker hop as minigame_lindsey at sticker_right onlayer screens
        show maxine_sticker as minigame_maxine at sticker_rightmost onlayer screens
      else:
        show flora_sticker as minigame_flora at sticker_leftmost onlayer screens
        show isabelle_sticker as minigame_isabelle at sticker_left onlayer screens
        show kate_sticker as minigame_kate at sticker_mid onlayer screens
        show lindsey_sticker as minigame_lindsey at sticker_right onlayer screens
        show maxine_sticker as minigame_maxine at sticker_rightmost onlayer screens
      with None
      show screen interface_hider
      show black onlayer screens zorder 100
      with Dissolve(1.0)
      pause 0.5
      hide minigame_background
      hide minigame_flora onlayer screens
      hide minigame_kate onlayer screens
      hide minigame_maxine onlayer screens
      hide minigame_isabelle onlayer screens
      hide minigame_lindsey onlayer screens
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      if progress > 5:
        if game.characters[max(winning_words,key=winning_words.get)] == kate:
          $quest.isabelle_dethroning["kate_love_points"] = True
        else:
          $game.characters[max(winning_words,key=winning_words.get)].love+=3
      "That should do it! Now I can scoff at the uncultured masses."
      "Time to tell [jo] and get her to sign off on [isabelle]'s party."
      $quest.isabelle_dethroning.advance("poem_done")
    "Not today. Maybe never.":
      pass
  return

label quest_isabelle_dethroning_poem_done:
  show jo neutral with Dissolve(.5)
  mc "Guess who's a certified poet now? I got my first ever B\nin an English assignment!"
  jo confident "That's great to hear, sweetie!"
  jo confident "I'm happy you're finally making some strides in your education."
  mc "You promised to sign off on [isabelle]'s dinner, remember?"
  jo laughing "I did, didn't I?"
  jo excited "Very well. You can tell her the dinner is happening."
  jo excited "But remember, you need staff supervision!"
  mc "Thanks, [jo]!"
  jo excited "You're very welcome."
  hide jo with Dissolve(.5)
  $quest.isabelle_dethroning.advance("good_news")
  return

label quest_isabelle_dethroning_good_news:
  show isabelle neutral with Dissolve(.5)
  mc "Hey! I managed to convince [jo] to allow that dinner of yours."
  isabelle excited "That's fantastic! How?"
  mc "A magician doesn't reveal his tricks..."
  mc "...but we do need staff supervision."
  isabelle laughing "That's fine! I won't ask, then."
  isabelle confident "Since you basically saved my little dinner, would you mind helping me set it up?"
  mc "Sure, I don't mind that."
  isabelle excited "Lovely! Would you like to be in charge of invitations?"
  isabelle excited "I know I already announced it, but people love to get a personal invitation as well."
  "Hmm... me asking could potentially scare people off... but on the other hand, isn't this what a second chance is all about?"
  show isabelle excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'm not sure I'm the right\nperson for that.\"":
      show isabelle excited at move_to(.5)
      mc "I'm not sure I'm the right person for that."
      isabelle concerned "Why not?"
      mc "I don't know. People don't really like me very much."
      isabelle laughing "Oh, bollocks!"
      isabelle confident "Come on, you managed to save the dinner. Just show them your wit.{space=-10}"
      mc "I'd... much rather help out with other things..."
      isabelle sad "I know this is really hard for you."
      isabelle sad "My sister was the exact same."
      isabelle blush "But one day, I got her to help me sell magazines."
      isabelle blush "And it turns out she was really good at it. Better than me!"
      isabelle laughing "She sold so many that she could afford a new xCube that summer!"
      mc "She played xCube?"
      isabelle confident "Oh, yeah! Every day."
      isabelle confident "Bottom line is, she would never have put herself in that situation\nif I hadn't pushed her."
      mc "I see what you're saying..."
      mc "Fine, I'll give it a go."
      $isabelle.love+=2
      isabelle blush "Awesome! Let me know when you have all the invitations!"
    "\"I'll handle it, no problem!\"":
      show isabelle excited at move_to(.5)
      mc "I'll handle it, no problem!"
      $isabelle.lust+=2
      isabelle blush "So confident!"
      mc "Well, I never really put myself out there in the past. It's time to change that."
      isabelle blush "That's great to hear."
      isabelle blush "Let me know when you have all the invitations!"
  hide isabelle with Dissolve(.5)
  $quest.isabelle_dethroning.advance("invitations")
  return

label quest_isabelle_dethroning_invitations_flora:
  show flora annoyed with Dissolve(.5)
  mc "Hey, [flora]!"
  flora annoyed "What do you want?"
  mc "Whoa! What did I do now?"
  flora cringe "There were no clean plates this morning. I had to go and retrieve one from your dirty stack."
  flora cringe "Gross as hell."
  mc "Err, sorry about that..."
  flora annoyed "Well, what do you want?"
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'm here to invite you\nto [isabelle]'s dinner.\"":
      show flora annoyed at move_to(.5)
      mc "I'm here to invite you to [isabelle]'s dinner."
      flora thinking "Why? I thought everyone was invited."
      mc "Yeah, but I wanted to make sure you got it."
      flora worried "..."
      mc "What?"
      flora worried "That doesn't seem like you at all."
      mc "Well, I'm trying to be a better person. More considerate."
      $flora.love+=1
      flora blush "Okay, I appreciate that. I'm looking forward to it."
      window hide
      hide flora with Dissolve(.5)
    "\"I'm here to tell you that you're\nnot invited to [isabelle]'s dinner.\"":
      show flora annoyed at move_to(.5)
      mc "I'm here to tell you that you're not invited to [isabelle]'s dinner."
      flora angry  "What?! How dare you?"
      show flora angry at move_to(.25)
      menu(side="right"):
        extend ""
        "\"It's nothing personal.\"":
          show flora angry at move_to(.5)
          mc "It's nothing personal."
          mc "You happened to be in the cafeteria during the announcement,\nso I came to clear it up."
          $flora.love-=1
          flora concerned "I'm coming anyway. Screw you."
          hide flora with dissolve2
          "Oh, well. It was worth a shot."
        "\"Don't worry, I'll save you\nsome sausages.\"":
          show flora angry at move_to(.5)
          mc "Don't worry, I'll save you some sausages."
          flora concerned "What the hell? Why?"
          mc "You seem a bit angry and frustrated right now."
          mc "Maybe they could help you relax?"
          flora cringe "..."
          $flora.love-=2
          $flora.lust+=2
          flora cringe "Gross."
          flora annoyed_texting "And anyway, you can't stop me from coming. I'm texting [isabelle]."
          "Well, shit. Gotta run!"
          window hide
          hide flora with Dissolve(.5)
  $quest.isabelle_dethroning["invitations"].add("flora")
  window auto
  return

label quest_isabelle_dethroning_invitations_kate:
  show kate eyeroll with Dissolve(.5)
  kate eyeroll "Don't make me bring out the measuring tape."
  mc "What?"
  kate skeptical "You're in my space, weirdo."
  mc "I guess I can take a step back..."
  kate skeptical "How about five steps back?"
  mc "Err... fine."
  kate eyeroll "Aren't you going to apologize?"
  mc "I'm actually here on [isabelle]'s behalf, so you'll have to take that up with her."
  kate excited "Interesting. So, she's your legal guardian now?"
  mc "..."
  show kate excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I just came to invite you to her dinner.\"":
      show kate excited at move_to(.5)
      mc "I just came to invite you to her dinner."
      kate neutral "And why would I want to come?"
      kate neutral "I'm planning my own party, and it's going to blow her little dinner out of the water."
      mc "Maybe once you get to know her, you might get along?"
      kate laughing "The only way we're getting along is when she's on her knees, begging for forgiveness."
      kate smile "Honestly, it's quite funny that you even decided to ask me."
      mc "Ugh, whatever."
      kate neutral "Don't give me attitude."
      mc "..."
      kate confident "You know what? Maybe I'll stop by just to see what a joke it is."
      kate confident "Call it morbid curiosity."
    "\"I just came to tell you that you're not invited to her dinner, obviously.\"":
      show kate excited at move_to(.5)
      mc "I just came to tell you that you're not invited to her dinner, obviously.{space=-20}"
      kate laughing "You think you have that power?"
      kate laughing "I'm the queen of the school, and I'll show up if I please."
      kate smile "I might also throw my own party just to show everyone how it's done.{space=-35}"
      mc "..."
      kate confident "Actually, count me in. It might spice things up."
      mc "Err... okay, I guess."
      "That did not go as planned. Hopefully, [kate] doesn't ruin the dinner."
      kate neutral "Why are you still standing there?"
      kate neutral "Wipe that drool and run along."
  window hide
  hide kate with Dissolve(.5)
  $quest.isabelle_dethroning["invitations"].add("kate")
  window auto
  return

label quest_isabelle_dethroning_invitations_lindsey:
  show lindsey smile with Dissolve(.5)
  mc "[lindsey]? Do you have a moment?"
  lindsey laughing "Sure thing!"
  mc "So, [isabelle] put me in charge of inviting people to the student dinner.{space=-20}"
  lindsey laughing "Oh, okay."
  mc "..."
  "Crap. Why is it so hard to talk to cute girls? It always gets awkward."
  show lindsey laughing at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Err... I guess this is me\nofficially inviting you?\"":
      show lindsey laughing at move_to(.5)
      mc "Err... I guess this is me officially inviting you?"
      $lindsey.love+=1
      lindsey blush "Yay! I'm excited! Thanks for the personal invitation!"
      window hide
      hide lindsey with Dissolve(.5)
    "\"You're not invited!\"":
      show lindsey laughing at move_to(.5)
      mc "You're not invited!"
      $lindsey.lust-=1
      $lindsey.love-=1
      lindsey annoyed "You came here just to tell me that in person?"
      lindsey annoyed "I don't really know [isabelle] that well, but that's uncool."
      "Crap. She didn't realize it was a joke."
      mc "It was a joke! Of course you're invited!"
      lindsey eyeroll "Whatever. I'm kinda busy, anyway. I'll see if I show up."
      hide lindsey with dissolve2
      "Sometimes I say the dumbest things..."
  $quest.isabelle_dethroning["invitations"].add("lindsey")
  window auto
  return

label quest_isabelle_dethroning_invitations_maxine:
  show maxine sad with Dissolve(.5)
  mc "[maxine]?"
  maxine sad "..."
  mc "Hello?"
  maxine thinking "Yes? What is it?"
  mc "Err... what were you doing?"
  maxine thinking "Pondering the moon-gravitational pull."
  mc "The what?"
  maxine thinking "The ebb and flow."
  mc "...so, anyway. [isabelle] put me in charge of inviting people to the student dinner."
  maxine thinking "That is a fascinating but ultimately useless tidbit of information."
  mc "Fascinating? How?"
  maxine skeptical "Due to your social track record, that is objectively a bad move\nfor any party."
  maxine skeptical "It's out of character for [isabelle] to entrust you with this responsibility."
  show maxine skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Well, in any case, you're invited.\"":
      show maxine skeptical at move_to(.5)
      mc "Well, in any case, you're invited."
      maxine excited "Thank you."
      mc "Are you... going to come?"
      maxine excited "If time allows."
      maxine excited "There might be valuable information to obtain."
      mc "Such as?"
      maxine concerned "Can I trust you to keep a secret?"
      show maxine concerned at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Absolutely!\"":
          show maxine concerned at move_to(.5)
          mc "Absolutely!"
          $maxine.lust+=1
          maxine thinking "No, I can't."
          mc "Okay..."
          maxine thinking "I might collect food samples for later investigation, and maybe\ndo some general profiling."
          maxine thinking "But don't tell anyone. I need to observe them in their natural state."
          mc "Sounds fascinating."
          maxine sad "Fascinating and necessary."
          maxine sad "..."
          maxine thinking "Leave me."
          mc "Okay, then..."
        "\"You don't have to disclose\nany vital information.\"":
          show maxine concerned at move_to(.5)
          mc "You don't have to disclose any vital information."
          mc "I trust you to do the right thing."
          $maxine.love+=1
          maxine smile "I see. This is very beneficial."
          maxine smile "I did not realize you had this quality in you."
          mc "I guess I'm full of surprises."
          maxine neutral "No, you are full of blood and bones. And shit."
          mc "Excuse me?"
          maxine skeptical "Food passes through the stomach and small intestine at a rate of six to eight hours."
          maxine skeptical "After that, it enters the final digestion stage in the large intestine. Excrement can remain for up to twenty-four hours."
          mc "What are you talking about?"
          maxine skeptical "Go to the toilet, [mc]."
          mc "..."
          mc "See you at the party, I guess."
    "\"You're probably right...\nbecause you're not invited.\"":
      show maxine skeptical at move_to(.5)
      mc "You're probably right... because you're not invited."
      maxine smile "A wise choice."
      mc "How so?"
      maxine smile "I prefer to go unnoticed."
      mc "So, you're coming, anyway?"
      maxine neutral "I am not. In fact, you'll be the one coming."
      mc "What?"
      maxine neutral "I'll be there, and you'll be coming."
      mc "This makes absolutely no sense."
      maxine concerned "I wish I could tell you more, but I'm afraid you can't be trusted."
      mc "Oh, well. A great loss."
      maxine concerned "A loss so insignificant it barely registered as a ripple on the lake\nof time."
      mc "..."
      mc "Goodbye, I guess."
      maxine flirty "And a farewell to you."
  window hide
  hide maxine with Dissolve(.5)
  $quest.isabelle_dethroning["invitations"].add("maxine")
  window auto
  return

label quest_isabelle_dethroning_invitations:
  "Okay, that should be everyone. Time to tell [isabelle]."
  $quest.isabelle_dethroning.advance("invitations_done")
  return

label quest_isabelle_dethroning_invitations_done:
  show isabelle laughing with Dissolve(.5)
  isabelle laughing "Hey! How did the invitations go?"
  mc "I'm done with them, but I'm not sure everyone's coming..."
  isabelle confident "Well, that's just how it is."
  isabelle confident "Is [kate] coming?"
  mc "Possibly. She'll be in the school, at any rate."
  mc "The cheer team tryouts are at the same time as the dinner."
  isabelle flirty "That's perfect."
  isabelle flirty "We'll sneak away from the dinner to set our trap without anyone noticing."
  mc "If you say so..."
  isabelle thinking "You don't sound so convinced."
  mc "Well, you never know with [kate]. She might be one step ahead of us."
  isabelle laughing "I don't think so. Not this time!"
  mc "I hope you're right... for both of our sakes."
  isabelle confident "Thanks for the help, [mc]. I'll see you tonight."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_dethroning.advance("dinner")
  return

label quest_isabelle_dethroning_dinner:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location != "school_cafeteria":
    "It's almost dinner time. I better get to the cafeteria."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    call goto_school_cafeteria
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
  $isabelle["outfit_stamp"] = isabelle.outfit
  $isabelle.outfit = {"bra": "isabelle_bra", "hat": None, "panties": "isabelle_panties", "pants": None, "shirt": "isabelle_party_dress"}
  show isabelle excited with Dissolve(.5)
  window auto
  isabelle excited "I'm excited so many people showed up!"
  isabelle concerned "I'm less excited about being the only one who dressed up."
  mc "It's fine. You're the host, after all."
  isabelle neutral "You're right. No worries."
  mc "I've never been a fan of big gatherings, but this doesn't look\nso bad."
  isabelle neutral "I'm glad to hear that. This is the first time I've organized something like this, so I'm a bit nervous."
  show isabelle neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Really? You look like someone\nwho snorts coke on Tuesdays.\"":
      show isabelle neutral at move_to(.5)
      $mc.charisma+=1
      mc "Really? You look like someone who snorts coke on Tuesdays."
      $isabelle.love-=1
      isabelle eyeroll "For your sake, I hope that's an expression and not an actual comment on my looks."
      mc "Err... yeah, look it up! It's listed under \"Failed Humor.\""
      isabelle eyeroll "I wonder if it's also listed under \"Inappropriate Comments?\""
      isabelle laughing "Oh, there it is!"
      mc "Sorry, I'll keep my mouth shut from now on..."
      isabelle confident "You don't have to do that. Just don't always say the first thing that pops into your head."
      mc "I'll keep that in mind."
      isabelle confident "Okay, let's forget about this and have a good time."
    "\"I've only ever been to\n[flora]'s birthday parties.\"":
      show isabelle neutral at move_to(.5)
      mc "I've only ever been to [flora]'s birthday parties."
      isabelle laughing "Seems like we'll both have to support each other, then."
      isabelle confident "A dinner party is a great place to start practicing your social skills."
      mc "You don't seem like the type to have issues with that."
      isabelle confident "I used to be really shy, but I worked through it. I started taking risks and talking to people."
      isabelle confident "The discomfort never really goes away, but I've definitely learned\nto deal with it."
      mc "Huh, that sounds nice... I guess I'm still in the process of trying\nnot to panic."
      isabelle laughing "Keep practicing on me, and you'll get it eventually!"
      mc "I do find it easier to talk to you for some reason."
      $isabelle.love+=2
      isabelle blush "That's great to hear! I'm happy."
  mc "I think this will be a great dinner party."
  isabelle blush "I hope so too. Let's join in?"
  $mc["focus"] = "isabelle_dethroning"
  hide isabelle with Dissolve(.5)
  return

label quest_isabelle_dethroning_dinner_exit:
  "Is it weird to be spending the whole time at a party wondering\nwhen it's appropriate to leave?"
  return

label quest_isabelle_dethroning_dinner_flora:
  show flora eyeroll with Dissolve(.5)
  flora eyeroll "Can't you leave me alone to enjoy the dinner in peace?"
  mc "I didn't even do anything..."
  flora annoyed "Yet."
  mc "Well, I..."
  "I guess she does have a point."
  hide flora with Dissolve(.5)
  $quest.isabelle_dethroning["dinner_conversations"].add("flora")
  return

label quest_isabelle_dethroning_dinner_isabelle:
  show isabelle confident with Dissolve(.5)
  isabelle confident "I guess there's not much else to do than enjoy the food."
  mc "And the company."
  isabelle laughing "Smooth!"
  mc "Thanks!"
  isabelle laughing "..."
  mc "..."
  isabelle laughing "..."
  "I need to come up with something to say before it gets awkward..."
  show isabelle laughing at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You look radiant tonight.\"":
      show isabelle laughing at move_to(.5)
      mc "You look radiant tonight."
      isabelle blush "Aw, thank you!"
      mc "You're welcome!"
      mc "..."
      mc "What, err... how was your day?"
      isabelle excited "Pretty good. Read a poem I really liked."
      isabelle excited "How was yours?"
      mc "Good. Good."
      mc "..."
      "This isn't going well at all. Why is socializing so hard?"
      "What do you even ask a girl?"
      mc "What's your favorite... shoes?"
      isabelle laughing "I like boots a lot. They're both comfy and stylish."
      isabelle laughing "Why do you ask?"
      mc "No reason..."
      mc "Do you miss London?"
      isabelle thinking "Not really. It's pretty dreary and the air is bad."
      isabelle thinking "I find it way nicer here."
      mc "That's good."
      mc "..."
      "This feels like a damn interrogation. [isabelle] must really hate it."
      "What are you supposed to do to keep a conversation going?"
      "..."
      "Maybe questions aren't even the right way?"
      show isabelle thinking at move_to(.75)
      menu(side="left"):
        extend ""
        "Ask about her favorite book":
          show isabelle thinking at move_to(.5)
          mc "I don't remember if I've asked you before, but what's your favorite book?"
          $isabelle.love+=2
          isabelle blush "Oh! I've always loved the classics!"
          isabelle blush "There's so many good books! Jane Eyre, Mockingbird, Lolita..."
          mc "Those are old!"
          isabelle laughing "Timeless! If I ever learn to write half as good as Nabokov, I'd die happy!"
          mc "You have time! A lot, I hope."
          isabelle confident "You never know when the government tries to kill literature... or\nthe ones writing it."
          mc "I think you should be more worried about the Twitter mob."
          isabelle confident "What's that?"
          mc "Oh, you don't have to worry about that for a few years more..."
          isabelle laughing "Thank god for that!"
        "Tell her a joke":
          show isabelle thinking at move_to(.5)
          mc "Do you know which state has the most pirates?"
          isabelle confident "Which one?"
          mc "Arrrrkansas!"
          isabelle laughing "Heh!"
          "That was cringe. God damn it."
        "Let the silence consume you":
          show isabelle thinking at move_to(.5)
          mc "..."
          isabelle laughing  "..."
          mc "..."
          isabelle neutral "..."
          mc "..."
          isabelle blush "I love this. Sometimes, there's no need for words."
          "Wow. I never thought she'd enjoy the awkward silence."
          "That just goes to show that sometimes it's all in my head..."
          mc "I agree. I enjoy your company and that's enough for me."
          $isabelle.love+=3
          isabelle blush "Aww!"
    "\"Did I ever tell you about my pet rock?\"":
      show isabelle laughing at move_to(.5)
      mc "Did I ever tell you about my pet rock?"
      isabelle concerned "Your pet rock?"
      mc "Yeah, funny story..."
      mc "I put it on a string and walked it around the block, but it got lost somewhere along the way."
      isabelle concerned "That's... very interesting."
      mc "I searched for hours!"
      mc "Then, when I got back, tired and distraught, I realized that I'd never tied it to the string."
      mc "It was sitting right there on the porch, all gravelly and cute."
      isabelle neutral "Must've been one tiny rock if you didn't notice it not dragging behind you."
      mc "It was average sized. I measured it."
      isabelle neutral "Of course it was."
    "?mc.charisma>=7@[mc.charisma]/7|{image=stats cha}|\"Did you poison my meal?\"":
      show isabelle laughing at move_to(.5)
      mc "Did you poison my meal?"
      isabelle afraid "What do you mean?!"
      mc "My heart started to beat really fast all of a sudden... but maybe that's just you."
      isabelle flirty "That's cheesy!"
      mc "That's the line my grandfather used on my grandmother."
      isabelle flirty "Truly?"
      mc "It's been passed down through generations!"
      isabelle laughing "No, it has not!"
      mc "Oh, it absolutely has, and it will continue to be passed down now."
      $isabelle.lust+=3
      isabelle laughing "You're bad!"
      mc "Funny, that's exactly what my granny said to my grandpa."
      isabelle laughing "Hah!"
  hide isabelle with Dissolve(.5)
  $quest.isabelle_dethroning["dinner_conversations"].add("isabelle")
  return

label quest_isabelle_dethroning_dinner_kate:
  show kate eyeroll with Dissolve(.5)
  mc "So, you showed up after all."
  kate eyeroll "What a complete snooze."
  kate eyeroll "My grandmother throws wilder parties at the retirement home."
  mc "It's a dinner, not a party..."
  kate laughing "I love that you think that matters!"
  kate laughing "Calling a sloth a cheetah doesn't make it run faster."
  mc "..."
  hide kate with Dissolve(.5)
  $quest.isabelle_dethroning["dinner_conversations"].add("kate")
  return

label quest_isabelle_dethroning_dinner_lindsey:
  show lindsey laughing with Dissolve(.5)
  lindsey laughing "This is quite nice, don't you agree?"
  lindsey smile "[isabelle] is a lovely host."
  hide lindsey with Dissolve(.5)
  $quest.isabelle_dethroning["dinner_conversations"].add("lindsey")
  return

label quest_isabelle_dethroning_dinner_maxine:
  show maxine skeptical with Dissolve(.5)
  mc "I noticed that you're not really talking to anyone, so I thought\nI'd check on you."
  maxine skeptical "I'm observing."
  mc "Err... okay. What exactly are you observing?"
  maxine skeptical "The rise and fall of spirits."
  mc "So, ghosts, eh?"
  maxine eyeroll "Don't be ridiculous."
  maxine eyeroll "In a wingding like this one, mood swings come in abundance."
  mc "Why don't you just call it a dinner?"
  maxine concerned "That would be highly inappropriate... and potentially dangerous."
  mc "..."
  $girl_love = [flora.love,isabelle.love,kate.love,lindsey.love,nurse.love]
  $girl_name = [flora.name,isabelle.name,kate.name,lindsey.name,nurse.name]
  $most_love = girl_name[girl_love.index(max(girl_love))]
  maxine excited "Anyway, watch how [most_love]'s pupils dilate whenever she looks\nover here."
  maxine excited "A soft shade of pink kissing her cheeks."
  maxine excited "According to my studies, a reaction like this is due to a temporary hormonal imbalance."
  maxine concerned "Or being a Lakers fan."
  mc "What?"
  maxine sad "Of course, there's still research left to be done in this field of study."
  maxine sad "And that's why I'm here now."
  mc "Right..."
  maxine thinking "Now, for your part in this endeavor."
  show maxine thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"My part? I never agreed to take part\nin your experiments.\"":
      show maxine thinking at move_to(.5)
      mc "My part? I never agreed to take part in your experiments."
      maxine skeptical "I could've sworn you agreed..."
      $maxine.love-=1
      maxine skeptical "Very well. Some people just aren't meant to carry the burden\nof greater insight."
      "She doesn't look too disappointed. That's good."
      $mc.intellect+=1
      "If there's something I've learned in the past, it's to stay clear\nof [maxine]'s experiments."
      maxine skeptical "Let's see here... an unwillingness to cooperate... slight perspiration...{space=-10}\nconfusion and suppressed anger..."
      "Crap. She's profiling me now. Let's not give her any more data."
      window hide
    "\"This sounds important. Are you sure\nyou can entrust me with this?\"":
      show maxine thinking at move_to(.5)
      mc "This sounds important. Are you sure you can entrust me with this?"
      maxine thinking "We're going to find out. You're also part of the study."
      mc "What?"
      maxine sad "That may or may not have been me slipping you inflammatory pieces of information."
      maxine thinking "Of course, as a test subject, you can't know if that was a mistake on my part, or if it's a component of the experiment."
      mc "..."
      show maxine thinking at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Okay, fine. I'll do it. I trust you.\"":
          show maxine thinking at move_to(.5)
          mc "Okay, fine. I'll do it. I trust you."
          $maxine.love+=2
          maxine confident "Excellent. Let's get started."
        "\"Sorry, but I'm not a guinea pig.\"":
          show maxine thinking at move_to(.5)
          mc "Sorry, but I'm not a guinea pig."
          $maxine.love-=1
          maxine concerned "That is a shame, and entirely unexpected."
          maxine concerned "This disproves six hypotheses."
          "She looks disappointed, but such is life."
          $mc.intellect+=1
          "If there's something I've learned in the past, it's to stay clear\nof [maxine]'s experiments."
          window hide
          hide maxine with Dissolve(.5)
          $quest.isabelle_dethroning["dinner_conversations"].add("maxine")
          return
        "\"Just tell me what to do.\"":
          show maxine thinking at move_to(.5)
          mc "Just tell me what to do."
          maxine excited "Your eagerness is duly noted."
          maxine flirty "..."
          mc "What are you doing?"
          maxine flirty "Just updating your profile."
          mc "My profile?"
          maxine excited "For secret research in the future."
          mc "All right, I'll make sure no one knows about your research. I'll be very discreet."
          $maxine.lust+=1
          maxine excited "Thank you. That means a lot."
      maxine confident "First of all, take this."
      $mc.add_item("maxine_camera")
      maxine confident_hands_down "Now, I'd like you to bring me a photograph that captures the spirit of this wingding."
      maxine confident_hands_down "I'm going to use it for the front page of the school's newspaper. Come talk to me when you're done."
      mc "What about the ghost pig?"
      maxine skeptical "What about it? I'm still waiting for more pictures."
  hide maxine with Dissolve(.5)
  $quest.isabelle_dethroning["dinner_conversations"].add("maxine")
  return

label quest_isabelle_dethroning_dinner_nurse:
  show nurse concerned with Dissolve(.5)
  mc "Hey, are you able to enjoy yourself despite chaperoning us?"
  nurse thinking "Y-yes. I just hope everything goes well..."
  "The [nurse] looks terrified. Probably not the best choice of supervision.{space=-40}"
  hide nurse with Dissolve(.5)
  $quest.isabelle_dethroning["dinner_conversations"].add("nurse")
  return

label quest_isabelle_dethroning_dinner_picture_flora:
  menu(side="middle"):
    "Take a picture of [flora]":
      show flora skeptical with Dissolve(.5)
      mc "Hey, [flora]!"
      flora skeptical "What?"
      mc "Smile!"
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      show flora afraid with Dissolve(.5)
      window auto
      flora afraid "What the hell?!"
      mc "It's for the school paper!"
      flora embarrassed "I did not give you permission for that!"
      flora embarrassed "Besides, I wasn't ready!"
      mc "Don't worry, you look adorable."
      $flora.lust+=2
      flora embarrassed "Ugh!"
      hide flora with Dissolve(.5)
      $quest.isabelle_dethroning["dinner_picture"] = "flora"
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture_isabelle:
  menu(side="middle"):
    "Take a picture of [isabelle]":
      show isabelle confident with Dissolve(.5)
      mc "[maxine] asked me to take a picture for her article about this dinner."
      mc "I figured the hostess would be the best option."
      "And the cutest option."
      $isabelle.love+=2
      isabelle blush "Aw, that's sweet!"
      isabelle blush "Go on, then!"
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      hide isabelle with Dissolve(.5)
      $quest.isabelle_dethroning["dinner_picture"] = "isabelle"
      window auto
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture_kate:
  menu(side="middle"):
    "Take a picture of [kate]":
      show kate skeptical with Dissolve(.5)
      mc "Would you mind if I take a picture of you?"
      kate eyeroll "For what? Your personal collection?"
      kate eyeroll "I knew you were a creep, but come on."
      mc "It's for the front page of the school paper..."
      $kate.lust+=2
      kate blush "And you picked me over the hostess of the dinner? That's brilliant!"
      mc "Yes!"
      kate blush "Make sure you get the lighting right."
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      window auto
      mc "Oh, that's hot."
      kate blush "You know it."
      hide kate with Dissolve(.5)
      $quest.isabelle_dethroning["dinner_picture"] = "kate"
    "Wait for a better shot":
      "[kate] always looks hot, but she's so often on the cover of the school newspaper..."
      "I need to find something fresh."
      window hide
      $quest.isabelle_dethroning["kitchen_shenanigans"] = True
      window auto
  return

label quest_isabelle_dethroning_dinner_picture_kitchen:
  kate "Quit stalling. I don't have all day."
  "Huh?"
  "It sounds like [kate] is in the kitchen, being her usual domineering self.{space=-30}"
  "I wonder who the unfortunate soul is..."
  menu(side="middle"):
    extend ""
    "Stay and take a picture":
      nurse "B-but, Miss [kate]! They're right outside!"
      kate "Yes, that's perfect for you. Someone could walk in here at any moment.{space=-65}"
      nurse "Mhm..."
      kate "Make the round, slut. I will not repeat myself again."
      nurse "Yes, Miss [kate]..."
      window hide
      show nurse kitchen_crawling
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      kate "Come on, crawl."
      kate "If you're not quick, I'll make you eat that carrot afterward."
      "Holy shit! The [nurse] is crawling after [kate] in broad daylight...\nright at the party..."
      if quest.kate_desire["stakes"] == "release":
        "This is really hot..."
        "But [kate] broke her promise."
        mc "What are you doing, [kate]?"
      nurse kitchen_crawling "Oh my goodness!"
      kate "Look, we have company! It's my least favorite nerd!"
      if quest.kate_desire["stakes"] != "release":
        mc "Err... I was just looking for ice cream..."
      nurse kitchen_crawling "Oh, no... Oh, god..."
      kate "Please, I know you love this. And it looks like [mc] here brought\na camera!"
      if quest.kate_desire["stakes"] == "release":
        mc "You broke your promise to leave her alone."
        kate "I didn't, actually. She asked me to do this."
        kate "Our deal was only to stop blackmailing her."
        mc "I won't allow this, [kate]."
        kate "Don't get your panties in a twist. This is the last time, okay?"
        kate "She just wanted to say goodbye... the right way."
        mc "Is this true, [nurse]?"
        nurse kitchen_crawling "Y-yes..."
        kate "See?"
      kate "Come on, slut! Pose for the camera!"
      nurse kitchen_crawling "My life is over..."
      kate "Don't be such a drama queen."
      kate "This is only for the school paper. No one reads it."
      window hide
      show nurse kitchen_posing
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      nurse kitchen_posing "Oh my god..."
      kate "Take the picture, [mc]."
      mc "I guess it'll look great on the cover..."
      mc "All right, smile!"
      $nurse.lust+=3
      nurse kitchen_posing "Oh my god..."
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      window auto
      "Despite looking spooked, the [nurse] seems to have enjoyed that. She's practically gushing."
      kate "Keep crawling, slut. We're not done yet."
      nurse kitchen_posing "Yes, Miss [kate]..."
      kate "I'm starting to think you do want to eat that carrot."
      kate "Maybe I should make you eat it at the table?"
      nurse kitchen_posing "Oh! Please, no!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 0.5
      hide nurse
      hide black onlayer screens
      with Dissolve(.5)
      $unlock_replay("nurse_pose")
      $quest.isabelle_dethroning["dinner_picture"] = "nurse"
      window auto
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture_lindsey:
  menu(side="middle"):
    "Take a picture of [lindsey]":
      show lindsey confident with Dissolve(.5)
      lindsey confident "Hey, [mc]!"
      mc "So, err... I've been tasked to take a photo for the front page\nof the next issue of the school paper."
      lindsey excited "Oh! Exciting! Do you need any help?"
      show lindsey excited at move_to(.75)
      menu(side="left"):
        extend ""
        "\"You're the prettiest girl here. Would\nyou mind being on the cover?\"":
          show lindsey excited at move_to(.5)
          mc "You're the prettiest girl here. Would you mind being on the cover?"
          lindsey laughing "Oh my god!"
          mc "It's true!"
          lindsey flirty "..."
          mc "It's fine, right?"
          lindsey flirty "I guess so..."
          mc "Okay, I know you're fast, but I'd like you to stay still for the photo!"
          $lindsey.love+=2
          lindsey laughing "[mc]! Stop it, you're making me blush!"
          mc "Heh! Okay, smile!"
        "\"Normally, I'd try to shield you from\nthe paparazzi, but today I'm one\nof them. Would you mind?\"":
          show lindsey excited at move_to(.5)
          mc "Normally, I'd try to shield you from the paparazzi, but today I'm\none of them. Would you mind?"
          $lindsey.lust+=2
          lindsey blush "That's so nice of you!"
          lindsey blush "Okay, let's do it!"
          mc "Smile!"
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      hide lindsey with Dissolve(.5)
      $quest.isabelle_dethroning["dinner_picture"] = "lindsey"
      window auto
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture_maxine:
  menu(side="middle"):
    "Take a picture of [maxine]":
      show maxine skeptical with Dissolve(.5)
      maxine skeptical "What exactly do you think you're doing?"
      mc "I'm conducting an experiment."
      maxine annoyed "That goes against the guidelines of the newspaper board."
      mc "Interesting..."
      mc "Defensive behavior... slight annoyance... possibly extra terrestrial tendencies..."
      maxine thinking "I'm not annoyed."
      mc "Interesting..."
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      window auto
      mc "I think I've caught a cryptid."
      maxine sad "Where?"
      mc "You'll see later."
      $maxine.lust+=2
      hide maxine with Dissolve(.5)
      $quest.isabelle_dethroning["dinner_picture"] = "maxine"
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture_soda_fountain:
  menu(side="middle"):
    "Take a picture of the soda fountain":
      "Ah, my one true love!"
      $mc.love+=1
      "Finally, the strawberry juice is getting its well-deserved time\nin the spotlight."
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      window auto
      $quest.isabelle_dethroning["dinner_picture"] = "soda_fountain"
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture_vending_machine:
  menu(side="middle"):
    "Take a picture of the vending machine":
      "Ugh... I needed a moment away from all the noise..."
      "Parties exhaust me like nothing else."
      "..."
      "Well, this party does. The sample size is quite small."
      "All right, I better take that picture and get back to [maxine]."
      play sound "doll_laughter" volume 0.25
      window hide
      pause 1.25
      window auto
      "What was that?"
      "..."
      $mc.lust+=1
      "Eh, probably nothing..."
      window hide
      pause 0.25
      show white onlayer screens zorder 100 with Dissolve(.125)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.125)
      pause 0.25
      window auto
      $quest.isabelle_dethroning["dinner_picture"] = "vending_machine"
    "Wait for a better shot":
      pass
  return

label quest_isabelle_dethroning_dinner_picture:
  show maxine excited with Dissolve(.5)
  maxine excited "I have foreseen this moment."
  mc "Oh, yeah?"
  maxine excited "You have come to deliver me something."
  mc "Right, your camera. I took a photo for you."
  maxine concerned "Oh... I guess the foresight was wrong, then."
  mc "..."
  mc "Do I even want to know?"
  maxine skeptical "That is a question only you can answer."
  mc "Right..."
  mc "Anyway, here's your camera back. I hope you like the photo."
  $mc.remove_item("maxine_camera")
  $maxine.love+=1
  $maxine.lust+=1
  maxine confident "Thank you for participating in the experiment."
  mc "No problem, I guess."
  window hide
  $quest.isabelle_dethroning["kitchen_shenanigans"] = False
  hide maxine with Dissolve(.5)
  return

label quest_isabelle_dethroning_dinner_kate_departure:
  show kate gushing with Dissolve(.5)
  window auto
  kate gushing "This was a fun party! Very fun!"
  "[kate]'s voice is dripping with sarcasm."
  kate gushing "Anyway, I have to go. I've got bigger and better things going on tonight."
  show kate gushing at disappear_to_right
  show expression LiveComposite((340,329),(192,0),"school cafeteria isabelle_dinner",(66,235),Crop((0,0,274,94),"school cafeteria chair_b1"),(0,259),Crop((0,0,251,70),"school cafeteria chair_b2")) as isabelle behind kate:
    xpos 1173 ypos 348 alpha 1.0
    pause 0.5 alpha 0.0
  "She probably means the cheer tryouts. I better get [isabelle]. This is our chance."
  "..."
  "Where is [isabelle]?"
  "..."
  "I guess she followed [kate] out."
  $quest.isabelle_dethroning.advance("trap")
  return

label quest_isabelle_dethroning_trap:
  show isabelle laughing with Dissolve(.5)
  isabelle laughing "There you are! I almost thought you'd bailed on me."
  mc "Never. Not now."
  isabelle confident "That's the attitude!"
  isabelle confident "Are you ready?"
  mc "As ready as I'll ever be."
  "If this fails, we're both dead meat..."
  isabelle neutral "Perfect. Do you have all the things with you?"
  mc "I think so."
  isabelle neutral "All the cheerleaders are already upstairs getting ready for the tryouts.{space=-40}"
  mc "I guess we better hurry, then."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_dethroning["better_hurry"] = True
  return

label quest_isabelle_dethroning_trap_leave_school:
  "This would be a good opportunity to escape into the night..."
  "Run as far as my legs will take me..."
  "Set up a camp near an old volcano..."
  "Forge a bunch of rings..."
  "Distribute them amongst my enemies..."
  "Lose the one I made for myself like a fool..."
  "Then spend the rest of my days in a tower, watching some dude and his gardener going on a hike..."
  "..."
  "Eh, maybe tomorrow."
  return

label quest_isabelle_dethroning_trap_cafeteria_doors:
  "As much as I'd like a snack right now, there are places to be and traps to set."
  return

label quest_isabelle_dethroning_trap_locked:
  "Locked."
  return

label quest_isabelle_dethroning_trap_first_hall:
  show isabelle thinking with Dissolve(.5)
  isabelle thinking "Shh! Wait!"
  isabelle thinking "They're getting ready in the women's bathroom."
  isabelle thinking "While you set up the trap, I'll try to sneak in there."
  mc "Um... okay."
  isabelle flirty "You got this! I believe in you."
  mc "That makes one of us."
  isabelle laughing "Oh, stop being so wet and hand over the pig mask!"
  mc "..."
  isabelle confident "It means stop whining! Let's do this!"
  mc "Right."
  $mc.remove_item("pig_mask")
  mc "Okay, I'll build the trap."
  isabelle confident "Hurry!"
  window hide
  show isabelle confident at disappear_to_right
  pause 1.0
  $quest.isabelle_dethroning["lets_do_this"] = True
  window auto
  return

label quest_isabelle_dethroning_trap_leave_first_hall:
  "This is not the time to play hide and seek."
  "I have a trap to build."
  return

label quest_isabelle_dethroning_trap_vending_machine(item):
  if item == "red_paint":
    $school_first_hall["red_paint"] = True
    $mc.remove_item("red_paint")
    "All right. That's step one."
  else:
    "Having my [item.title_lower] fall on [kate]'s head would be funny, but ultimately ineffective."
    $quest.isabelle_dethroning.failed_item("vending_machine",item)
  return

label quest_isabelle_dethroning_trap_red_paint(item):
  if item == "fishing_hook":
    $school_first_hall["fishing_hook"] = True
    $mc.remove_item("fishing_hook")
    "There we go. Now I can attach the tripwire."
  elif item == "fishing_line":
    if school_first_hall["fishing_hook"]:
      $school_first_hall["fishing_line"] = True
      $mc.remove_item("fishing_line")
      "And just like that, the trap is set!"
      "Now, I'll just have to wait for—"
      kate "Aeeeyeeeee!"
      window hide
      show kate chase
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Holy shit, [isabelle] is scary in that thing!"
      isabelle "Ooooooooink, oink, oink, oink!"
      kate chase "Aaaaaaaah! Help!"
      show kate trip
      kate trip "Eeep!" with vpunch
      window hide
      play sound "falling_thud"
      play ambient "<from 0.25>audio/sound/paint_splash.ogg" volume 0.75 noloop
      show isabelle annoyed_left_pig
      show black onlayer screens zorder 100
      with vpunch
      $unlock_replay("kate_scare")
      $school_first_hall["red_paint"] = school_first_hall["fishing_hook"] = school_first_hall["fishing_line"] = False
      $school_first_hall["paint_splash"] = True
      pause 1.5
      hide kate
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      isabelle annoyed_left_pig "Ugh, I can't see anything wearing this! Where'd she go?"
      mc "She ran down the hall, bumped into the piano, and then locked herself{space=-45}\ninside the art classroom."
      isabelle annoyed_left_pig "Do you have the key?"
      show isabelle annoyed_left_pig at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Do you really want to chase her further?\"":
          show isabelle annoyed_left_pig at move_to(.5)
          mc "Do you really want to chase her further?"
          isabelle blush_pigless "Yes! I think we can scare her more."
          mc "I think that's probably how we get caught..."
          $isabelle.lust-=1
          isabelle annoyed_pigless "Who cares? I'm not happy until she's peed herself!"
        "\"We should probably clean up and get rid of the mask before anyone comes...\"":
          show isabelle annoyed_left_pig at move_to(.5)
          mc "We should probably clean up and get rid of the mask before anyone comes..."
          $isabelle.lust-=1
          isabelle annoyed_pigless "No way! We're hunting her down!"
        "\"Maybe, but I want a turn with the mask.\"":
          show isabelle annoyed_left_pig at move_to(.5)
          mc "Maybe, but I want a turn with the mask."
          mc "I saw the horror in her eyes. I want to do that to her."
          $isabelle.lust+=1
          isabelle laughing_pigless "Fair, fair!"
          isabelle laughing_pigless "It did feel amazing to chase her down. I think I made her cry, heh!"
          isabelle confident_pigless "All right, you go get her."
          show isabelle confident with dissolve2
          $mc.add_item("pig_mask")
          mc "What about you?"
          isabelle confident "I'll just clean up this mess."
          isabelle confident "I can see that you're really longing to get revenge."
          "More than she knows..."
          mc "Thanks, it means a lot!"
          isabelle blush "Go on, then! Go get her!"
          "[isabelle] really is the perfect girl. No two ways about it."
          "Staying behind to clean up, while I get my revenge? There's something{space=-55}\nincredibly hot about that."
          "Maybe when this is all over, I'll ask her to be my girlfriend..."
          hide isabelle with Dissolve(.5)
          $quest.isabelle_dethroning.advance("revenge")
          return
      mc "Her dad basically owns the school board. If we get caught, we'll both{space=-15}\nget expelled."
      mc "We might even face criminal charges."
      isabelle annoyed_pigless "And then my dad will wreck them in court."
      show isabelle annoyed_pigless at move_to(.75)
      menu(side="left"):
        extend ""
        "\"We already messed up her cheer\ntryout and scared her shitless...\"":
          show isabelle annoyed_pigless at move_to(.5)
          mc "We already messed up her cheer tryout and scared her shitless..."
          mc "We got our revenge."
          isabelle angry_pigless "Seriously?"
          isabelle angry_pigless "After years of abuse, you're happy to just scare her into missing some stupid cheerleader tryout?"
          $mc.intellect+=1
          $mc.love+=1
          mc "You have to understand that [kate] is like the house in gambling.\nIf you keep playing, you'll end up losing."
          mc "You have to take the victories you can get."
          mc "Besides, we'll still have the rest of the year for more revenge if\nwe don't get caught."
          $isabelle.lust-=2
          isabelle eyeroll_pigless "Bloody hell..."
          isabelle eyeroll_hand_on_hip "Fine. Let's not push our luck."
          mc "Thank you, [isabelle]."
          isabelle sad "Sorry, I always get a bit carried away when it comes to revenge..."
          mc "Don't worry about it."
          $isabelle.love+=5
          isabelle blush "Thanks for talking me down. This is the smarter choice."
          mc "Don't mention it. I've known [kate] a lot longer than you have."
          isabelle blush "I do need to trust you more on these things."
          isabelle blush "Let's get back to the cafeteria before we're missed?"
          window hide
          show isabelle blush at disappear_to_right
          pause 0.25
          show black onlayer screens zorder 100 with Dissolve(.5)
          $quest.isabelle_dethroning.advance("revenge_done")
          call goto_school_cafeteria
          show isabelle
          pause 1.5
          show isabelle flirty at appear_from_right
          hide black onlayer screens with Dissolve(.5)
          window auto
        "\"Fine, let's get her!\"":
          show isabelle annoyed_pigless at move_to(.5)
          mc "Fine, let's get her!"
          isabelle laughing_pigless "That's the spirit!"
          mc "But let's be smart about it."
          isabelle eyeroll_pigless "Fine."
          mc "You go get the key to the art classroom. It's probably in the\n[guard]'s booth."
          mc "I'll watch the door, so she doesn't escape."
          isabelle confident_pigless "Okay, and how do I get into the [guard]'s booth?"
          mc "Maybe the [nurse] has the key? Else you could try breaking into the principal's office."
          isabelle confident_pigless "All right, be right back!"
          window hide
          show isabelle confident_pigless at disappear_to_right
          pause 1.0
          $quest.isabelle_dethroning["be_right_back"] = True
          $quest.isabelle_dethroning.advance("revenge")
          window auto
    else:
      "Hook, line, sinker — without order, the world descends into chaos."
  else:
    "Setting the trap with my [item.title_lower] could potentially kill [kate]. That's not the goal today."
    $quest.isabelle_dethroning.failed_item("red_paint",item)
  return

label quest_isabelle_dethroning_revenge:
  if quest.isabelle_dethroning["be_right_back"]:
    "[isabelle] will probably be gone for a while..."
    "I'm not sure if she'll even get into the [guard]'s booth without breaking the glass..."
    "Might as well make myself useful."
  else:
    "All right, here's my chance for revenge..."
  $quest.isabelle_dethroning["chance_for_revenge"] = True
  return

label quest_isabelle_dethroning_revenge_art_door:
  "The door is locked, but I once saw the jocks open it with a paperclip.{space=-15}"
  "If those pea-brains can pick the lock, it shouldn't be too hard..."
  $quest.isabelle_dethroning["pea_brains"] = True
  return

label quest_isabelle_dethroning_revenge_art_door_lockpicking(item):
  if item in ("high_tech_lockpick","safety_pin"):
    if item == "high_tech_lockpick":
      "Luckily, I never leave home without my trusty lock-pick."
    elif item == "safety_pin":
      "If Chad can do it, so can I!"
      "...said no one ever."
    window hide
    show minigames lock_picking_lesson background as minigame_background onlayer screens:
      xpos 436 ypos 270
    show minigames lock_picking_lesson pin1 as minigame_pin1 onlayer screens:
      xpos 819 ypos 469
    show minigames lock_picking_lesson pin2 as minigame_pin2 onlayer screens:
      xpos 943 ypos 469
    show minigames lock_picking_lesson pin1 as minigame_pin3 onlayer screens:
      xpos 1079 ypos 469
    show minigames lock_picking_lesson pin3 as minigame_pin4 onlayer screens:
      xpos 1205 ypos 469
    show minigames lock_picking_lesson safety_pin as minigame_safety_pin onlayer screens:
      xpos 105 ypos 645
    show expression LiveComposite((106,106),(0,0),Crop((904,921,106,106),"school first_hall_west 1fwcorridor"),(0,0),Crop((904,921,106,106),"school first_hall_west night_overlay")) as minigame_workaround onlayer screens:
      xpos 904 ypos 921
    show black onlayer screens zorder 100
    with Dissolve(.5)
    $game.ui.hide_hud = True
    pause 0.5
    hide screen interface_hider
    hide black onlayer screens
    with Dissolve(.5)
    $renpy.transition(Dissolve(0.25))
    call start_lock_picking_lesson_minigame(item)
    show screen interface_hider
    show black onlayer screens zorder 100
    with Dissolve(.5)
    if lock_picking_minigame.win:
      call goto_school_art_class
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
  else:
    "Picking the lock with my [item.title_lower] would only work in a fantasy novel."
    $quest.isabelle_dethroning.failed_item("lockpicking",item)
  return

label quest_isabelle_dethroning_revenge_art_class:
  "Oh, [kate]..."
  play sound "interact_popup6"
  $school_art_class["lights_on"] = True
  extend " where are you?"
  return

label quest_isabelle_dethroning_revenge_art_class_door:
  "Hmm... maybe chasing her down is taking things too far?"
  menu(side="middle"):
    extend ""
    "Oh, what am I even\nthinking? This is [kate]!":
      "Carpe [kate]."
      "That's what life is currently about."
    "Leave":
      "Sometimes you have to take the high road. Even with someone\nlike [kate]."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $quest.isabelle_dethroning.advance("revenge_done")
      call goto_school_cafeteria
      pause 1.5
      hide black onlayer screens with Dissolve(.5)
      window auto
  return

label quest_isabelle_dethroning_revenge_art_class_supply_closet:
  "Unfortunately for [kate], the hiding places are very limited in this classroom."
  "Hmm... she has managed to lock it from the inside."
  if quest.kate_search_for_nurse > "open_closet":
    "Poetic justice that she's now the one hiding."
  $lockpicking = mc.owned_item(("high_tech_lockpick","safety_pin")).replace("pick","") or "safety_pin"
  menu(side="middle"):
    extend ""
    "?mc.owned_item(('high_tech_lockpick','safety_pin'))@|{image=items [lockpicking]}|Pick the lock":
      $item = mc.owned_item(("high_tech_lockpick","safety_pin"))
      $mc.remove_item(item)
      play sound "lock_click"
      "..."
      $mc.add_item(item,silent=True)
      kate "Aaaaaaah! Go away!"
      "So much for trying to keep a low profile..."
      "Although, I guess that's never been her forte."
    "?mc.strength>=3@[mc.strength]/3|{image=stats str}|Break down the door":
      "Let's see how much those hours in the gym are worth..."
      "One..."
      "Two..."
      play sound "<from 9.2 to 10.2>door_breaking_in"
      "Three!" with vpunch
      kate "Aaaaaaaaaah! Nooooo!"
    "Enjoy the moment":
      "Mmm... nothing like cornering your prey."
      return
  window hide
  show kate cowering
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Wow, look at her... so small and fearful."
  "That's not something you see every day... or ever, really."
  "Her face is completely covered, her eyes glued shut by the\ndrying paint."
  if mc.owned_item("pig_mask"):
    "The pig mask is pretty useless in her current state."
  else:
    "Maybe I should call [isabelle]... but that would give away my voice."
  "The most important thing right now is to not speak."
  "If I talk, [kate] will know it's me."
  "..."
  "But perhaps I could oink to scare her?"
  "Or maybe..."
  menu(side="middle"):
    extend ""
    "Oink":
      pass
    "Grunt":
      mc "Uh...?"
      kate cowering "Chad, is that you?!"
      mc "Uh-huh."
      kate cowering "Oh, thank god! You won't believe what I just saw!"
      "Chad must really be a man of few words. I can't believe she thinks I'm him."
      kate cowering "I swear I just saw a ghost pig!"
      kate cowering "I was freaking out until you got here!"
      mc "Mhm..."
      kate cowering "Haha, I'm being serious!"
      mc "Uh-huh."
      kate cowering "Hihihi, why are you always teasing me?"
      "It's odd to see this side of [kate] up close."
      "She's always such a bitch to me, it feels totally out of character..."
      kate cowering "Can you help me get this stuff out of my eyes?"
      mc "Nuh-uh."
      kate cowering "What? You think I look cute like this?"
      mc "Uh-huh."
      kate cowering "My god, you're such a charmer!"
      "Ugh, it's disgusting how she fawns over him."
      "She's always such an ice queen, but with Chad, she's so... girly."
      "Life's so unfair sometimes..."
      "But maybe I can use this to my advantage?"
      menu(side="middle"):
        extend ""
        "Grab her":
          window hide
          show black onlayer screens zorder 100 with Dissolve(.5)
          pause 0.5
          play sound "fast_whoosh"
          show kate hate_sex_bent_over:
            block:
              linear 0.05 xoffset 25
              linear 0.05 xoffset -25
              repeat 2
            linear 0.05 xoffset 0
          hide black onlayer screens
          with Dissolve(.5)
          hide kate
          show kate hate_sex_bent_over
          window auto
          kate hate_sex_bent_over "Oh, Chad! You're in a mood today!"
          "You have no idea, bitch."
          mc "Uh-huh."
          kate hate_sex_bent_over "What about [stacy]?"
          mc "Uh-uh."
          kate hate_sex_bent_over "Oh? You like me more now?"
          mc "Mmmm."
          kate hate_sex_bent_over "Oh, you! You can date us both if you want to!"
          mc "Mhm..."
          kate hate_sex_bent_over "This stuff is really burning my eyes, though..."
          mc "Huh."
          kate hate_sex_bent_over "What is it? I can't see anything!"
          mc "Mhm."
          kate hate_sex_bent_over "You're kinky! My god!"
          "As if [kate] really thinks that. God, she's so fake."
          window hide
          show kate hate_sex_smile_spanking1 with Dissolve(.5)
          show kate hate_sex_smile_spanking2 with Dissolve(.075)
          play sound "slap"
          $quest.isabelle_dethroning["kate_spanking"]+=1
          $renpy.transition(vpunch, layer="master")
          show kate hate_sex_smile_spanking3 with Dissolve(.05)
          show kate hate_sex_spanking4 with Dissolve(.05)
          show kate hate_sex_spanking1 with Dissolve(.125)
          window auto
          kate hate_sex_spanking1 "Ohhh!"
          mc "Mhm."
          window hide
          show kate hate_sex_spanking2 with Dissolve(.075)
          play sound "slap"
          $quest.isabelle_dethroning["kate_spanking"]+=1
          $renpy.transition(vpunch, layer="master")
          show kate hate_sex_spanking3 with Dissolve(.05)
          show kate hate_sex_spanking4 with Dissolve(.05)
          show kate hate_sex_spanking1 with Dissolve(.125)
          window auto show
          show kate hate_sex_smile_spanking1 with dissolve2
          kate hate_sex_smile_spanking1 "Ow! Hehe! What am I getting punished for?"
          "For being a total and eternal bitch."
          window hide
          show kate hate_sex_smile_spanking2 with Dissolve(.075)
          play sound "slap"
          $quest.isabelle_dethroning["kate_spanking"]+=1
          $renpy.transition(vpunch, layer="master")
          show kate hate_sex_smile_spanking3 with Dissolve(.05)
          show kate hate_sex_spanking4 with Dissolve(.05)
          show kate hate_sex_spanking1 with Dissolve(.125)
          window auto
          kate hate_sex_spanking1 "Owww! Be careful!"
          kate hate_sex_spanking1 "You're really strong, you know?"
          menu(side="middle"):
            extend ""
            "Spank her harder":
              window hide
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              pause 0.1
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              window auto
              kate hate_sex_spanking1 "Owwwww! That really hurt!"
              mc "Mhm!"
              kate hate_sex_spanking1 "I'm all for fooling around and being kinky, but..."
              window hide
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              pause 0.05
              show kate hate_sex_spanking2 with Dissolve(.075)
              play sound "slap"
              $quest.isabelle_dethroning["kate_spanking"]+=1
              $renpy.transition(vpunch, layer="master")
              show kate hate_sex_spanking3 with Dissolve(.05)
              show kate hate_sex_spanking4 with Dissolve(.05)
              show kate hate_sex_spanking1 with Dissolve(.125)
              window auto show
              show kate hate_sex_tearing_up_spanking1 with dissolve2
              kate hate_sex_tearing_up_spanking1 "Owwww! Stop! That's way too hard!"
              menu(side="middle"):
                extend ""
                "Spank her harder":
                  window hide
                  show kate hate_sex_tearing_up_spanking2 with Dissolve(.075)
                  play sound "slap"
                  $quest.isabelle_dethroning["kate_spanking"]+=1
                  $renpy.transition(vpunch, layer="master")
                  show kate hate_sex_tearing_up_spanking3 with Dissolve(.05)
                  show kate hate_sex_spanking4 with Dissolve(.05)
                  show kate hate_sex_spanking1 with Dissolve(.125)
                  pause 0.0
                  show kate hate_sex_spanking2 with Dissolve(.075)
                  play sound "slap"
                  $quest.isabelle_dethroning["kate_spanking"]+=1
                  $renpy.transition(vpunch, layer="master")
                  show kate hate_sex_spanking3 with Dissolve(.05)
                  show kate hate_sex_spanking4 with Dissolve(.05)
                  show kate hate_sex_spanking1 with Dissolve(.125)
                  window auto show
                  show kate hate_sex_tearing_up_spanking1 with dissolve2
                  kate hate_sex_tearing_up_spanking1 "Noooo! Stop!"
                  kate hate_sex_tearing_up_spanking1 "This hurts too much!"
                  "So? Never seen you care about that before."
                  window hide
                  show kate hate_sex_tearing_up_spanking2 with Dissolve(.075)
                  play sound "slap"
                  $quest.isabelle_dethroning["kate_spanking"]+=1
                  $renpy.transition(vpunch, layer="master")
                  show kate hate_sex_tearing_up_spanking3 with Dissolve(.05)
                  show kate hate_sex_spanking4 with Dissolve(.05)
                  show kate hate_sex_spanking1 with Dissolve(.125)
                  window auto show
                  show kate hate_sex_crying_spanking1 with dissolve2
                  kate hate_sex_crying_spanking1 "Chaaaaaaaad! Pleaseeeeee!"
                  mc "Mhm."
                  kate hate_sex_crying_spanking1 "You're hurting me, babe..."
                  "Duh. That's the whole point."
                  mc "Uh-huh."
                  kate hate_sex_crying_spanking1 "Why? What did I do?"
                  "God, she's such a whiner..."
                  "You torment people for fun, but can't take a little spanking? Pathetic.{space=-20}"
                  menu(side="middle"):
                    extend ""
                    "Spank her harder":
                      window hide
                      show kate hate_sex_crying_spanking2 with Dissolve(.075)
                      play sound "slap"
                      $quest.isabelle_dethroning["kate_spanking"]+=1
                      $renpy.transition(vpunch, layer="master")
                      show kate hate_sex_crying_spanking3 with Dissolve(.05)
                      show kate hate_sex_spanking4 with Dissolve(.05)
                      show kate hate_sex_spanking1 with Dissolve(.125)
                      pause 0.0
                      show kate hate_sex_spanking2 with Dissolve(.075)
                      play sound "slap"
                      $quest.isabelle_dethroning["kate_spanking"]+=1
                      $renpy.transition(vpunch, layer="master")
                      show kate hate_sex_spanking3 with Dissolve(.05)
                      show kate hate_sex_spanking4 with Dissolve(.05)
                      show kate hate_sex_spanking1 with Dissolve(.125)
                      pause 0.0
                      show kate hate_sex_spanking2 with Dissolve(.075)
                      play sound "slap"
                      $quest.isabelle_dethroning["kate_spanking"]+=1
                      $renpy.transition(vpunch, layer="master")
                      show kate hate_sex_spanking3 with Dissolve(.05)
                      show kate hate_sex_spanking4 with Dissolve(.05)
                      show kate hate_sex_spanking1 with Dissolve(.125)
                      window auto show
                      show kate hate_sex_orgasm_spanking1 with dissolve2
                      kate hate_sex_orgasm_spanking1 "Mmmhmmm..."
                      "Huh? She liked that?"
                      "That sounded almost like an orgasm..."
                      "..."
                      "There's no way [kate] is secretly a masochist, right?"
                      "Maybe she complained so much because she knew she would\nget off on it?"
                      "Or maybe she just fainted. It's hard to say with the paint covering her face."
                      $achievement.hand_of_fate.unlock()
                      "Either way, now it's time to claim my prize."
                      menu(side="middle"):
                        extend ""
                        "Fuck her pussy":
                          $quest.isabelle_dethroning["kate_sex"] = "pussy"
                        "Fuck her ass":
                          $quest.isabelle_dethroning["kate_sex"] = "ass"
                    "Fuck her pussy":
                      $quest.isabelle_dethroning["kate_sex"] = "pussy"
                    "Fuck her ass":
                      $quest.isabelle_dethroning["kate_sex"] = "ass"
                "Fuck her pussy":
                  $quest.isabelle_dethroning["kate_sex"] = "pussy"
                "Fuck her ass":
                  $quest.isabelle_dethroning["kate_sex"] = "ass"
            "Fuck her pussy":
              $quest.isabelle_dethroning["kate_sex"] = "pussy"
            "Fuck her ass":
              $quest.isabelle_dethroning["kate_sex"] = "ass"
          window hide
          if renpy.showing("kate hate_sex_spanking1"):
            show kate hate_sex_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_pulling_panties2 with Dissolve(.5)
          elif renpy.showing("kate hate_sex_tearing_up_spanking1"):
            show kate hate_sex_tearing_up_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_tearing_up_pulling_panties2 with Dissolve(.5)
          elif renpy.showing("kate hate_sex_crying_spanking1"):
            show kate hate_sex_crying_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_crying_pulling_panties2 with Dissolve(.5)
          elif renpy.showing("kate hate_sex_orgasm_spanking1"):
            show kate hate_sex_orgasm_pulling_panties1 with Dissolve(.5)
            show kate hate_sex_orgasm_pulling_panties2 with Dissolve(.5)
          window auto
          if quest.isabelle_dethroning["kate_sex"] == "pussy":
            "Wow, look at that moist pussy."
            "I can't believe [kate] is this wet already."
            if quest.isabelle_dethroning["kate_spanking"] == 13:
              "I wonder if it's from the spanking?"
            "Probably just the fact that she thinks I'm Chad."
            "It's seriously weird how some women work."
            "Let's see if she can tell the difference between our cocks..."
            window hide
            show kate hate_sex_dick_reveal with Dissolve(.5)
            window auto
            kate hate_sex_dick_reveal "Chad, come on! Stop teasing me!"
            "So eager. So thirsty for popular dudes. Typical cheerleader behavior.{space=-10}"
            mc "Ugh..."
            kate hate_sex_dick_reveal "What's wrong? Don't you like what you see?"
            mc "Nuh-uh."
            kate hate_sex_dick_reveal "What do you mean? Is it the stuff on my face?"
            mc "Mhm."
            kate hate_sex_dick_reveal "Well, I asked you to remove it, but you—"
            window hide
            show kate hate_sex_vaginal4 with hpunch
            window auto
            kate hate_sex_vaginal4 "Ohhhhhh!"
            "God, she's so hot."
            "Even drenched in paint, she has the perfect body. The perfect pussy."
            "It fits the exact measurements of my dick."
            "It's almost as if we were genetically made for each other..."
            "Of course, she'll never know that."
            kate hate_sex_vaginal4 "Hmm? What's wrong?"
            "What is she on about now?"
            kate hate_sex_vaginal4 "Am I not pleasing to you?"
            "God, she's pathetic when it comes to alpha dudes."
            mc "Uh..."
            kate hate_sex_vaginal4 "Don't I usually get you a lot harder?"
            "Ah, great. I see what this is about."
            "Chad clearly has a much bigger dick than me, so [kate] thinks\nhe's not fully hard yet."
            mc "Nuh-uh."
            kate hate_sex_vaginal4 "Oh my god... that's so mean..."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            window auto
            kate hate_sex_vaginal4 "Ohhh! Cha-aad!"
            kate hate_sex_vaginal4 "You're making me feel soooo good!"
            "She might be faking it, but she sounds almost relieved that\nit's not Chad's thunder-cock banging her pussy..."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            window auto
            kate hate_sex_vaginal4 "Oh, fuck yes! Keep going, you stud!"
            "It does feel nice to be able to pleasure someone like [kate]."
            "She probably has the highest standards in the whole school."
            "Who would've thought me and my loser-dick could pull that off?"
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal1 with Dissolve(.075)
            show kate hate_sex_vaginal2 with Dissolve(.075)
            show kate hate_sex_vaginal3 with Dissolve(.075)
            show kate hate_sex_vaginal4 with Dissolve(.075)
            window auto
            kate hate_sex_vaginal4 "God, you're such an animal!"
            kate hate_sex_vaginal4 "Fuck me! Fuck me harder!"
            mc "Mmhh! Mmhh!"
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Ohhhh! If you want to... dump [stacy]..."
            kate hate_sex_vaginal4 "I'm... down for a fling..."
            "No, bitch. All I want is a one night stand."
            "You'd make a horrible girlfriend."
            mc "Nuh-uh."
            kate hate_sex_vaginal4 "Ohhh... okay!"
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Oh, my god! You fuck me so good!"
            kate hate_sex_vaginal4 "Are you... sure? We could be..."
            kate hate_sex_vaginal4 "The new... ohhh... power couple!"
            mc "Nuh-uh."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Ohhh! Screw you... Chad... Haha!"
            kate hate_sex_vaginal4 "You know... we'd be great..."
            mc "Uh-uh."
            window hide
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal1 with Dissolve(.05)
            show kate hate_sex_vaginal2 with Dissolve(.05)
            show kate hate_sex_vaginal3 with Dissolve(.05)
            show kate hate_sex_vaginal4 with Dissolve(.05)
            window auto
            kate hate_sex_vaginal4 "Oh, god! I'm going to—"
            show kate hate_sex_squirt
            kate hate_sex_squirt "Ahhhhhhhh!" with vpunch
            "Her pussy squeezes my dick the tightest I've ever felt."
            "In an instant, she pushes me out and starts to shake in a\npowerful orgasm."
            "Damn, I even made her squirt!"
            "Guess I'll just finish myself off..."
            window hide
            show kate hate_sex_jerking_off4 with Dissolve(.5)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off1 with Dissolve(.05)
            show kate hate_sex_jerking_off2 with Dissolve(.05)
            show kate hate_sex_jerking_off3 with Dissolve(.05)
            show kate hate_sex_jerking_off4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_jerking_off3 with Dissolve(.025)
            show kate hate_sex_jerking_off2 with Dissolve(.025)
            show kate hate_sex_jerking_off1 with Dissolve(.025)
            show kate hate_sex_cum_outside with vpunch
            $quest.isabelle_dethroning["kate_cum"] = "outside"
            show kate hate_sex_aftermath with Dissolve(.5)
            window auto
            "Ahhhh.... there it is..."
            "Bitch just got marked."
            "I still can't believe I fucked her to orgasm."
            "That in itself crosses out most of the nasty things she's said\nabout me in the past."
            "I bet she didn't think a loser like me could make her orgasm,\nmuch less squirt!"
          elif quest.isabelle_dethroning["kate_sex"] == "ass":
            "There it is... the holy grail of assholes."
            if quest.isabelle_dethroning["kate_spanking"] == 13:
              "Her ass is really glowing from the spanking..."
              "And her pussy is dripping..."
            "She's perfect and ripe for the taking."
            window hide
            show kate hate_sex_reluctance with Dissolve(.5)
            window auto
            "As I move closer, the tip of my dick grazes [kate]'s asshole."
            "It twitches, she twitches, and lets out a tiny gasp."
            kate hate_sex_reluctance "No, you know that won't work! You're way too big!"
            mc "Uh-uh."
            kate hate_sex_reluctance "Come on, it won't fit! You know this!"
            mc "Uh-uh."
            kate hate_sex_reluctance "Just put it in my pussy instead, okay? It's much—"
            window hide
            show kate hate_sex_anal1 with hpunch
            window auto
            kate hate_sex_anal1 "Oww! I told you—"
            mc "Mmmm."
            "She gasps for air, her breathing suddenly shallow."
            "My dick isn't nearly as big as Chad's, but it probably hurts to\ntake it in the butt regardless."
            "Her fingers curl into fists, and she bites her lip to ease the pain."
            "She's probably trying to come to terms with the fact that Chad\ngets what Chad wants."
            "And right now... Chad wants anal."
            window hide
            show kate hate_sex_anal2 with hpunch
            window auto
            kate hate_sex_anal2 "Owww! Wait! It really hurts!"
            "That's the point, bitch."
            "How does it feel to get sodomized?"
            "For me, it's heaven."
            "She's so tight and squirmy, but I'm not letting my dick out of\nher ass now."
            "I've almost got her exactly where I want her."
            "Just a few more inches..."
            window hide
            show kate hate_sex_anal3 with hpunch
            window auto
            kate hate_sex_anal3 "Oh, my god! That's sooooo deep!"
            "[kate] is probably proud of herself for taking Chad in the ass."
            "Girls take pride in the weirdest things."
            "Although, to be fair, no one else in the school would probably survive Chad's cock up the ass..."
            window hide
            show kate hate_sex_anal4 with hpunch
            window auto
            kate hate_sex_anal4 "Ooooooh!"
            "God damn! That's as deep as it goes."
            "It feels good to finally put that bitch in her place."
            "And to do it with my cock up her ass, that's daydream stuff."
            window hide
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            window auto
            kate hate_sex_anal4 "Oh, god! Your dick in my ass feels so good, Chad!"
            "Wow, it sounds like [kate] is enjoying almost as much as I am."
            "If only she knew..."
            window hide
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal1 with Dissolve(.075)
            show kate hate_sex_anal2 with Dissolve(.075)
            show kate hate_sex_anal3 with Dissolve(.075)
            show kate hate_sex_anal4 with Dissolve(.075)
            window auto
            kate hate_sex_anal4 "Ohhhhhh! I can feel you in my tummy!"
            "Slamming into her again and again..."
            "Burying myself in her ass..."
            "That's what every guy dreams of."
            window hide
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            window auto
            "To thaw the fucking ice queen..."
            "Make her a docile kitten with my dick..."
            "Make her take the whole length..."
            "Make her enjoy an ass pounding..."
            "That's a life achievement!"
            window hide
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.0
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal1 with Dissolve(.05)
            show kate hate_sex_anal2 with Dissolve(.05)
            show kate hate_sex_anal3 with Dissolve(.05)
            show kate hate_sex_anal4 with Dissolve(.05)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            pause 0.05
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal1 with Dissolve(.025)
            show kate hate_sex_anal2 with Dissolve(.025)
            show kate hate_sex_anal3 with Dissolve(.025)
            show kate hate_sex_anal4 with Dissolve(.025)
            window auto
            kate hate_sex_anal4 "Oh, my god!"
            mc "Mmmm!"
            "God, going all the way in, pulling out, then bottoming out again..."
            "Fuck! It's too good!"
            "I'm going to come!"
            menu(side="middle"):
              extend ""
              "Pull out":
                window hide
                show kate hate_sex_pain_cum_outside with vpunch
                $quest.isabelle_dethroning["kate_cum"] = "outside"
                show kate hate_sex_aftermath with Dissolve(.5)
                window auto
                "That's a pretty picture..."
                "[kate]'s ass coated in my cum."
                kate hate_sex_aftermath "You wipe that off, buddy!"
                mc "Nuh-uh."
                kate hate_sex_aftermath "Haha! You're so bad!"
                kate hate_sex_aftermath "Fine, I'll clean myself up..."
              "Fill her ass":
                window hide
                show kate hate_sex_cum_inside with hpunch
                window auto
                kate hate_sex_cum_inside "Ooooh!"
                kate hate_sex_cum_inside "You naughty boy... you came inside me!"
                mc "Uh-uh."
                window hide
                $quest.isabelle_dethroning["kate_cum"] = "inside"
                show kate hate_sex_aftermath with Dissolve(.5)
                window auto
                kate hate_sex_aftermath "I'll keep it inside me for the rest of the day."
                kate hate_sex_aftermath "Would you like that?"
                mc "Mmmm..."
                kate hate_sex_aftermath "Okay, then!"
            kate "Can you help me get this stuff off my face now?"
            mc "Nuh-uh."
            kate "Please!"
            "No, you keep it on."
            "Show everyone what you are."
            "Nothing but a dethroned queen."
            "The laughing stock of the undesirables."
          $unlock_replay("kate_terror")
          if quest.isabelle_dethroning["be_right_back"]:
            "..."
            "I really should take a selfie in case no one believes me."
            "I hardly believe it my—"
            window hide
            play sound "<from 0.1>open_door" volume 0.66
            show kate hate_sex_isabelle_surprised_aftermath with Dissolve(.5)
            window auto
            isabelle "Bloody hell..."
            show kate hate_sex_surprised_caught with dissolve2
            "Shit."
            kate hate_sex_surprised_caught "What? Have you never seen two people fucking before?"
            kate hate_sex_surprised_caught "I knew you were a dumb bitch, but I didn't think you were a virgin."
            "At first, [isabelle] just stares in shock."
            if isabelle.love > isabelle.lust:
              $isabelle.love-=5
              $quest.isabelle_dethroning["isabelle_caught"] = "sad"
              show kate hate_sex_sad_caught with dissolve2
              "Then, she looks down as sadness sweeps across her face."
              "A single tear sparkles and descends down her cheek."
              "All that triumph of sticking it to [kate] snuffed out in an instant."
              "My cheeks flush as the realization creeps in."
              "[isabelle] really liked me... and of course I had to go and fuck it up."
              window hide
              show kate hate_sex_caught with Dissolve(.5)
              window auto
              "She turns slowly, and just walks off."
            elif isabelle.lust >= isabelle.love:
              $isabelle.lust-=5
              $quest.isabelle_dethroning["isabelle_caught"] = "disgusted"
              show kate hate_sex_disgusted_caught with dissolve2
              "Then, slowly her face twists into disgust."
              "The ultimate betrayal — having sex with her rival."
              "Her eyes darken like thunder clouds."
              window hide
              show kate hate_sex_caught with Dissolve(.5)
              window auto
              "Without another word, she turns on her heel, and storms off."
              "Maybe fucking [kate] wasn't the smartest idea with [isabelle]\nright around the corner..."
            "Man, I'm like a kid who found a secret ice cream stash in the\nfreezer and just gorged myself without thinking."
            "Maybe it's not the best idea to just fuck everything that moves\njust because I can."
            "This is my second chance, and there sure are many ways to\nmess it up again."
            "Ways that I didn't consider before..."
            "Fuck, I better go check on her."
            if quest.isabelle_dethroning["isabelle_caught"] == "sad":
              "She looked devastated."
            elif quest.isabelle_dethroning["isabelle_caught"] == "disgusted":
              "She looked like she was about to drown a kitten."
            "I'll just leave [kate] here. I'm done with that bitch, anyway."
            window hide
            show black onlayer screens zorder 100 with Dissolve(.5)
            $quest.isabelle_dethroning.advance("revenge_done")
            call goto_school_cafeteria
            pause 1.5
            hide black onlayer screens with Dissolve(.5)
            window auto
            return
          else:
            "And now it's time to..."
            menu(side="middle"):
              extend ""
              "...give her a heart attack.":
                $mc.lust+=3
                mc "Oink."
                kate hate_sex_scare "W-what?"
                mc "..."
                kate hate_sex_scare "Chad?"
                "She probably can't believe her ears."
                kate hate_sex_scare "Chad?!"
                "I wonder what thoughts rush through her bewildered mind..."
                "Does she think Chad abandoned her?"
                "Or that Chad is the ghost pig?"
                "Did the ghost pig just fuck her?"
                "In the end, it doesn't matter."
                "Only her trauma matters."
                mc "Oink! Oink! Oink!"
                kate hate_sex_scare "Aaaaaaayee!"
                window hide
                show kate hate_sex_kateless with Dissolve(.5)
                pause 0.25
                show expression LiveComposite((1920,1080),(0,0),"kate avatar events hate_sex background",(873,76),"kate avatar events hate_sex door_closed") as kate
              "...make my escape.":
                $mc.love+=3
                "I've pushed my luck twice today, and it's time to dip."
                if quest.isabelle_dethroning["kate_sex"] == "pussy":
                  "I came for [kate]'s fear, and got her pussy as well."
                elif quest.isabelle_dethroning["kate_sex"] == "ass":
                  "I came for [kate]'s fear, and got her ass as well."
                "She can crawl around like a blind maggot for a while. That's\nmore than deserved."
                "Now, I have a dinner to get back to."
                window hide
                show black onlayer screens zorder 100 with Dissolve(.5)
                $quest.isabelle_dethroning.advance("revenge_done")
                call goto_school_cafeteria
                pause 1.5
                hide black onlayer screens with Dissolve(.5)
                window auto
                return
        "Oink":
          pass
  if renpy.showing("kate cowering"):
    mc "Oink!"
    kate cowering "N-noooo! Go away!"
    mc "Ooooink, oink, oink, oink!"
    kate cowering "Aaaaaaaaaah! Help!"
    "Her face twisted in fear is all I've ever dreamed of."
    "To take back some of the power."
    "To show her that she's not the queen of the world."
    "She's just a trembling girl, scared out of her mind."
    window hide
    show kate cowering_kateless with Dissolve(.5)
    pause 0.25
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "She crawls across the floor in abject horror, hitting her head on chairs and easels."
  "I follow her as she cries and pleads, oinking in her ear."
  "Tears flow down her cheeks. Streams of piss down her legs."
  "She screams in fear."
  "Calls out for her dad."
  "Screams again."
  "Finally, she reaches the corner of the room, and just cowers."
  "Her body shakes in complete mind-numbing terror."
  "And that's how I leave her."
  "A whimpering mess on the floor."
  "Justice served, bitch."
  show black onlayer screens zorder 100
  $quest.isabelle_dethroning["kate_pee"] = True
  $quest.isabelle_dethroning.advance("revenge_done")
  call goto_school_cafeteria
  pause 1.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  return

label quest_isabelle_dethroning_revenge_done:
  if quest.isabelle_dethroning["isabelle_caught"] == "sad":
    show isabelle sad with Dissolve(.5)
    mc "Hey, are you okay?"
    isabelle sad "What do you think?"
    mc "Probably not okay..."
    isabelle sad "A proper good guess."
    if quest.isabelle_buried.finished:
      isabelle sad "Did our kiss mean nothing to you?"
    if quest.isabelle_locker.finished and (not quest.isabelle_locker['time_for_a_change'] or quest.isabelle_hurricane.finished):
      isabelle sad "What about the sex?"
    isabelle sad "You just threw everything we had away for a quick shag with the bitch queen?"
    show isabelle sad at move_to(.75)
    menu(side="left"):
      extend ""
      "\"I see that I hurt you.\"":
        show isabelle sad at move_to(.5)
        mc "I see that I hurt you."
        isabelle annoyed "You think?"
        mc "There's nothing I can say that can take back what I did..."
        mc "But please know that it wasn't anything other than carnal."
        isabelle annoyed "And that makes it okay?"
        mc "No, it doesn't."
        mc "I'm not trying to make excuses for what I just did."
        mc "I'm simply hoping you can find it in your heart to forgive my mistake.{space=-15}"
        mc "It was a hate-fuck. That's all it was."
        isabelle sad "Well, it still hurts."
        isabelle sad "I thought we had something special, and now—"
      "\"It's not that simple.\"":
        show isabelle sad at move_to(.5)
        mc "It's not that simple."
        isabelle sad "Explain to me why you did it, then."
        mc "I saw an opportunity to get back at her."
        mc "There was a sort of poetic justice in fucking her, after she'd fucked me over for so long."
        isabelle sad "I guess I can see that... but it still hurts..."
        mc "I'm sorry. It was really stupid of me."
        mc "All I thought about was revenge and then—"
  elif quest.isabelle_dethroning["isabelle_caught"] == "disgusted":
    show isabelle annoyed_left with Dissolve(.5)
    "Great. She looks pissed."
    mc "Hey..."
    isabelle annoyed_left "Don't even talk to me. I don't want to hear it."
    show isabelle annoyed_left at move_to(.25)
    menu(side="right"):
      extend ""
      "\"What's the problem?\"":
        show isabelle annoyed_left at move_to(.5)
        mc "What's the problem?"
        isabelle angry "What's the problem?!"
        isabelle angry "The problem is you shagging [kate], you daft cunt!"
        mc "And why is that a problem, exactly?"
        isabelle angry "Because... she's the enemy!"
        mc "Did it look like I was making love to her?"
        mc "I conquered her with my dick, that's all I did."
        mc "Sex isn't always about love. Sometimes it's about dominance."
        mc "I merely put her in her place. No feelings were involved."
        mc "Besides, what's it to you? It's not like we're dating or anything."
        isabelle sad "What's it to me?"
        isabelle sad "I thought we had—"
      "\"I'm really sorry. Let me explain.\"":
        show isabelle annoyed_left at move_to(.5)
        mc "I'm really sorry. Let me explain."
        isabelle angry "Mate, you were knee deep in her twat."
        isabelle angry "What's there to explain?"
        mc "I wasn't thinking..."
        isabelle angry "Ain't that the truth."
        mc "I don't have feelings for her or anything, okay?"
        mc "I just—"
        isabelle angry "You just saw a good shag? Is that it?"
        mc "No, it's part of a bigger revenge plan!"
        isabelle angry "Brilliant."
        mc "I'm serious! She hates me, and she thought—"
  else:
    if renpy.showing("isabelle"):
      isabelle flirty "All in all, this was a big success!"
    else:
      show isabelle flirty with Dissolve(.5)
      isabelle flirty "How did it go?"
      mc "Oh, it went better than expected..."
      if quest.isabelle_dethroning["kate_sex"]:
        mc "I got my revenge in more than one way today."
      mc "She'll probably need therapy after what I put her through."
      if quest.isabelle_dethroning["kate_pee"]:
        mc "And, yes, she did in fact pee herself."
      isabelle flirty "Brilliant! So, it was a big success!"
    isabelle flirty "I still can't get over how freaked out she got!"
    mc "Yeah, we definitely took her down a few notches!"
    isabelle laughing "Yeah, we did! And we won't get caught either, thanks to you keeping your head cool."
    $school_first_hall["paint_splash"] = False
    mc "Thanks to you too for cleaning up that mess."
    isabelle confident "We make a pretty good team, don't you think?"
    mc "We sure do, and maybe we can—"
  play sound "doll_laughter"
  window hide
  pause 1.0
  show isabelle cringe with Dissolve(.5)
  window auto
  isabelle cringe "What was that?"
  mc "I'm not sure..."
  isabelle afraid "Look!"
  window hide
  show misc doll_scare
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  stop music
  play sound "light_switch"
  hide isabelle
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $lindsey.default_name = "{size=24}Eyeless\n{space=25}Doll{/}"
  lindsey "Hehehe! Run! Hehehe! Run for your life!"
  "God, that's freaky."
  "I've never seen the cafeteria this quiet before..."
  "It's like everyone froze in fear."
  if quest.maxine_lines > "doll" and not quest.maxine_lines.failed:
    "..."
    "Isn't that the doll I found in the compartment behind that locker?"
    "How is it talking?"
  lindsey "Hehehe! Fly! Hehehe! Fly to the moon!"
  $lindsey.default_name = "Lindsey"
  "What the fuck..."
  $quest.isabelle_dethroning["fly_to_the_moon"] = True
  window hide
  show lindsey
  show isabelle thinking
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide misc doll_scare
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  isabelle thinking "Nobody panic! This is clearly a bad joke."
  show isabelle thinking at move_to(.75)
  show flora afraid at Transform(xalign=.25) with dissolve2
  flora afraid "Where is [lindsey]?"
  mc "Huh? I swear I saw her a minute ago..."
  flora embarrassed "[lindsey]!"
  flora embarrassed "She's gone!"
  mc "I'm sure she's around here somewhere..."
  mc "[lindsey]!"
  show flora embarrassed at move_to(.5)
  show isabelle thinking at move_to(.9)
  show nurse concerned at Transform(xalign=.1) with dissolve2
  nurse concerned "I, um... as the chaperon... I... I think we should leave..."
  isabelle afraid "We can't just leave without [lindsey]!"
  isabelle cringe "I know this is just someone trying to mess with us, but... just in case...{space=-45}"
  mc "[isabelle], maybe you can help the [nurse] get everyone out, and I'll try to find [lindsey]?"
  isabelle afraid "Are you sure?"
  mc "Yeah! As you said, it's nothing to worry about."
  isabelle afraid "Okay, good luck!"
  isabelle afraid "Everyone, follow me!"
  window hide
  show maxine
  show flora embarrassed at disappear_to_right behind isabelle
  pause 0.25
  show isabelle afraid at disappear_to_right(.75)
  pause 0.15
  show nurse concerned at move_to(0.5,1.0)
  window auto show
  show nurse thinking with dissolve2
  nurse thinking "Oh, right... follow [isabelle], everyone!"
  show nurse thinking at disappear_to_right
  "Hmm... where could've [lindsey] gone?"
  "If she ran outside, then the others will find her..."
  "So, I guess I'll search the school..."
  jump goto_school_ground_floor

label quest_isabelle_dethroning_revenge_done_entrance_hall:
  "Well, the good thing is that she doesn't seem to be near the stairs. That could get dangerous."
  "But where do I even start?"
  $quest.isabelle_dethroning["where_do_i_start"] = True
  return

label quest_isabelle_dethroning_revenge_done_leave_school:
  "Murder dolls don't scare me. This place filled with students is the real horror."
  "Luckily, it looks like everyone went outside."
  return

label quest_isabelle_dethroning_revenge_done_cafeteria_doors:
  "[lindsey] is not in the cafeteria. That's for sure."
  return

label quest_isabelle_dethroning_revenge_done_admin_wing:
  "Well, she doesn't seem to be here... unless she managed to squeeze in behind the bookshelf."
  "But I think only [maya] could pull that off."
  $quest.isabelle_dethroning["only_maya"] = True
  return

label quest_isabelle_dethroning_revenge_done_admin_wing_bookshelf:
  "[lindsey]?"
  "..."
  "Nope, not behind there."
  return

label quest_isabelle_dethroning_revenge_done_admin_wing_nurse_door:
  "The [nurse] has locked up for the night. There's no way [lindsey] managed to get in there."
  "Unless she's a ghost now... and then it's too late anyway."
  return

label quest_isabelle_dethroning_revenge_done_admin_wing_principal_door:
  "Locked."
  "Well, at least I checked. Better be thorough than... sorrow."
  "Yeah, not my best work."
  return

label quest_isabelle_dethroning_revenge_done_first_hall:
  "Hmm... no [lindsey] here. Where could she have gone?"
  $quest.isabelle_dethroning["no_lindsey_here"] = True
  return

label quest_isabelle_dethroning_revenge_done_arts_wing:
  mc "[lindsey], where are you?!"
  "..."
  "What the hell is that?"
  $quest.isabelle_dethroning["where_are_you"] = True
  return

label quest_isabelle_dethroning_revenge_done_arts_wing_exit:
  show mrsl surprised_blowtorch at appear_from_left
  mrsl surprised_blowtorch "[mc]? What are you doing here?"
  show mrsl surprised_blowtorch at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I could ask you the same...\"":
      show mrsl surprised_blowtorch at move_to(.5)
      mc "I could ask you the same..."
      mrsl neutral_blowtorch "I'm just making sure things are as they should."
      mc "With a... {i}blow torch?{/}"
      mrsl thinking_blowtorch "Oh, this? Someone had left it in the bathroom."
      mrsl thinking_blowtorch "It seemed too dangerous to be lying around."
      mc "Okay..."
      mrsl neutral_blowtorch "Anyway, I'll see you in class."
    "\"I'm looking for [lindsey]!\nHave you seen her?\"":
      show mrsl surprised_blowtorch at move_to(.5)
      mc "I'm looking for [lindsey]! Have you seen her?"
      mrsl thinking_blowtorch "[lindsey]? Can't say I have."
      mrsl thinking_blowtorch "Why are you both running around in the school after dark?"
      mc "She disappeared during the Independence Day dinner, and I'm trying to find her..."
      mrsl neutral_blowtorch "Well, good luck with that. I'll see you in class."
  $school_first_hall_west["scorch_marks"] = True
  show mrsl neutral_blowtorch at disappear_to_right
  "That was pretty odd, but whatever, I guess..."
  "I'm really starting to get worried about [lindsey]."
  return

label quest_isabelle_dethroning_revenge_done_sports_wing:
  "This is [lindsey]'s domain. Hopefully, she's around here somewhere...{space=-5}"
  $quest.isabelle_dethroning["lindsey_domain"] = True
  return

label quest_isabelle_dethroning_revenge_done_sports_wing_bathroom_door:
  "[lindsey]? Psst! Are you in there?"
  "..."
  "Hmm... doesn't seem so."
  return

label quest_isabelle_dethroning_revenge_done_sports_wing_gym_door:
  "Locked..."
  "Damn it! This was my best guess."
  return

label quest_isabelle_dethroning_revenge_done_sports_wing_pool_door:
  "Locked."
  "The pool doesn't open until later, anyway. There's no way [lindsey] could've gotten in there."
  return

label quest_isabelle_dethroning_revenge_done_roof_landing:
  "What the hell...?"
  $quest.isabelle_dethroning["what_the_hell"] = True
  return

label quest_isabelle_dethroning_revenge_done_roof_landing_door:
  "It's locked, as expected..."
  if quest.isabelle_dethroning["gravity_of_the_situation"]:
    "Hopefully, [jo] steps on the gas."
  elif quest.isabelle_dethroning["call_jo"]:
    "I need to call [jo]. Besides the [guard], she's the only one with a key to the roof."
  else:
    "But then how did [lindsey] get through?"
    "Damn it, I need to get the key from the [guard]'s booth quickly!"
  $quest.isabelle_dethroning.advance("panik")
  return

label quest_isabelle_dethroning_panik_leave_school:
  "No time to fuck around. [lindsey] is in danger."
  return

label quest_isabelle_dethroning_panik_entrance_hall_guard_booth:
  "The key to the roof... is gone."
  "Usually, it hangs right there on the wall. [lindsey] must've somehow got a hold of the key..."
  "..."
  "I need to call [jo]. Besides the [guard], she's the only one with a key to the roof."
  $quest.isabelle_dethroning["call_jo"] = True
  return

label quest_isabelle_dethroning_panik_jo:
  $set_dialog_mode("phone_call_plus_textbox","jo")
  $guard.default_name = "Phone"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  window hide
  $set_dialog_mode("phone_call_centered","jo")
  jo "Sweetheart! How's\nthe dinner going?"
  mc "Hey, [jo]..."
  jo "What's wrong, [mc]?"
  mc "This is going to sound really\nweird, but I think something\nis wrong with [lindsey]..."
  mc "She has gotten onto the roof,\nand I'm worried. I need the key."
  jo "Dear lord. I'm at home, but\nI'll get into the car right away."
  jo "Meet me outside\nthe school, okay?"
  play sound "end_call"
  $set_dialog_mode("")
  pause 0.5
  window auto
  "Well, at least she understands the gravity of the situation..."
  "Hopefully, she steps on the gas."
  $quest.isabelle_dethroning["gravity_of_the_situation"] = True
  return

label quest_isabelle_dethroning_panik_outside:
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_entrance"
  $renpy.pause(0.25)
  if quest.kate_stepping.started:
    $mc["focus"] = ""
    pause 1.25
    stop music
  $renpy.get_registered_image("location").reset_scene()
  scene location
  show lindsey roof_ground_still
  hide black onlayer screens
  with Dissolve(.5)
  "Oh, wow. Half the school is still here."
  if quest.kate_stepping.started:
     "Must've been one hell of a party..."
  "But what are they all looking at? The fireworks?"
  window hide
  show lindsey roof_ground_still:
    ease 3.0 yalign 0.0
  pause 3.0
  hide lindsey
  show lindsey roof_ground_still:
    yalign 0.0
  window auto
  "Holy shit."
  mc "[lindsey]!!"
  mc "Get away from the edge!!"
  "There are too many people shouting..."
  "Some are even chanting for her to jump..."
  "Luckily, she probably can't hear a thing up there."
  if quest.isabelle_dethroning.in_progress:
    "Where is [jo]?"
  "..."
  "[lindsey] looks completely out of it."
  "A gust of wind could knock her right off the roof."
  "..."
  if quest.isabelle_dethroning.in_progress:
    "Where the hell is [jo]?!"
    window hide
    $set_dialog_mode("phone_call_plus_textbox","jo")
    pause 0.5
    $guard.default_name = "Phone"
    window auto
    guard "{i}Calling...{/}"
    guard "{i}...{/}"
    $guard.default_name = "Guard"
    window hide
    $set_dialog_mode("phone_call_centered","jo")
    jo "Hello?"
    mc "[jo], how long until\nyou're here?"
    jo "It'll be another fifteen\nminutes or so, honey."
    jo "I just got into the car."
    mc "That's too long..."
    jo "I'll be there as\nfast as I can—"
    play sound "end_call"
    $set_dialog_mode("")
    pause 0.5
    window auto
  menu(side="middle"):
    "?quest.lindsey_motive.finished@|{image=lindsey contact_icon}|{image=stats love}|\"Fuck it, I'm not waiting{space=-50}\nfor [lindsey] to fall down!\"{space=-75}":
      "Fuck it, I'm not waiting for [lindsey] to fall down!"
      "I'm going up there even if I have to break down the door!"
      if quest.isabelle_dethroning.in_progress:
        $quest.isabelle_dethroning.advance("fly_to_the_moon")
      scene black
      show black onlayer screens zorder 100
      with Dissolve(.07)
      $game.location = "school_ground_floor"
      $mc["focus"] = "isabelle_dethroning" if quest.isabelle_dethroning.in_progress else "kate_stepping"
      $renpy.pause(0.25)
      $renpy.get_registered_image("location").reset_scene()
      scene location
      hide black onlayer screens
      with Dissolve(.5)
      return
    "?not quest.lindsey_motive.finished@|\"Oh, well. I've done what I could.\"":
      "Oh, well. I've done what I could."
      "She'll probably snap out of it and come back down, right?"
      "I'm not sure why I'm all that worried."
      "Sure, she's been a bit clumsy lately... but surely she's not going to fall off the roof."
      "It's weird that she's up there, but what can you do?"
      "It's not like anyone else is lifting a finger, anyway."
      if quest.isabelle_dethroning.in_progress:
        "At least I called [jo] and she'll be here in a few minutes."
      window hide
      show lindsey roof_ground_wings with Dissolve(.5)
      window auto
      "Oh, good! She's finally moving!"
      "..."
      "Wait a minute, what is she doing?"
      "No, no, no, no—"
      "[lindsey], what are you—"
      window hide
      show lindsey roof_ground_fall with Dissolve(.5)
      window auto
      "And then it just happens."
      "As if time stops, she hangs suspended in the air for a moment."
      "And in that moment — that fraction of a second — you feel regret."
      "Regret that you didn't try harder to help her."
      "Regret that you didn't get to know her better."
      "Regret for things said, and unsaid."
      "Things that could have been different."
      "It's all too easy to get caught up in life. Even when you have another chance."
      "Because sometimes the bigger picture gets clouded by impulses and desires."
      "Sometimes, reality itself is blinding."
      "Your mind fills with glimpses of past conversations."
      "Visions of could-have-beens and unfulfilled dreams."
      "Then the moment passes..."
      "...gravity takes hold..."
      play sound "falling_thud"
      show lindsey roof_ground_lindseyless with vpunch2
      "...and she falls."
      jump quest_isabelle_dethroning_ending

label quest_isabelle_dethroning_fly_to_the_moon_computer_room_door:
  "Googling how to be a hero is probably not the way to be a hero..."
  return

label quest_isabelle_dethroning_fly_to_the_moon_homeroom_door:
  "Unfortunately, the climbing rope only goes up to the second floor."
  return

label quest_isabelle_dethroning_fly_to_the_moon_corridor:
  "I'm about to have a heart attack from all this running..."
  "Luckily, the [nurse]'s office is right there if needed."
  return

label quest_isabelle_dethroning_fly_to_the_moon_stairs:
  "I'm sure there's a secret passage to the roof somewhere in\nthe school..."
  "But I don't have the time to find it now."
  return

label quest_isabelle_dethroning_fly_to_the_moon_leave_school:
  "Not the time for knock-knock jokes."
  return

label quest_isabelle_dethroning_fly_to_the_moon_cafeteria_doors:
  "If only I were a stronger climber, perhaps I could break a window in the cafeteria and scale the waterspout. Alas."
  return

label quest_isabelle_dethroning_fly_to_the_moon_fire_axe:
  "I don't care if I get detention for life, I'm taking this door down."
  $school_roof_landing["fire_axe_taken"] = True
  $mc.add_item("fire_axe")
  return

label quest_isabelle_dethroning_fly_to_the_moon_roof_door(item):
  if item == "fire_axe":
    $mc.remove_item("fire_axe")
    "The hours at the gym have surely prepared me for this..."
    window hide
    play sound "fire_axe_impact"
    $school_roof_landing["axe_hole"]+=1
    show location with vpunch
    pause 0.5
    window auto
    "Fucking hell, this door is sturdy!"
    window hide
    play sound "fire_axe_impact"
    $school_roof_landing["axe_hole"]+=1
    show location with vpunch
    pause 0.5
    window auto
    "Come on! Come on!"
    window hide
    play sound "fire_axe_impact"
    $school_roof_landing["axe_hole"]+=1
    show location with vpunch
    $mc.add_item("fire_axe",silent=True)
    pause 0.5
    window auto
    "I managed to make a hole, but damn... it's too small to get through..."
    "At least I can talk to [lindsey] now."
    window hide
    show lindsey roof_door_still
    show black onlayer screens zorder 100
    with Dissolve(.5)
    $mc["focus"] = ""
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    "Oh, my god..."
    "She seems to be in a complete trance. She probably doesn't realize how close she is to the edge of the roof."
    mc "[lindsey]!!"
    lindsey roof_door_still "Run! Run!"
    mc "What are you saying?! Why are you up here?!"
    "She's totally out of it..."
    mc "[lindsey], get away from the edge!!"
    "..."
    "No reaction. She doesn't seem to hear me."
    "Maybe the wind is to blame, or the shouting below..."
    window hide
    show lindsey roof_door_wings with Dissolve(.5)
    window auto
    "What the hell is she doing?"
    mc "[lindsey]!! Listen to me!!"
    "For a moment, she looks back at me, but her gaze is empty."
    "It's like she's not there at all."
    "Then she turns away."
    lindsey roof_door_wings "Fly!"
    mc "No!! Stop!!"
    lindsey roof_door_wings "Fly to the moon!"
    window hide
    show lindsey roof_door_step with Dissolve(.5)
    window auto
    "And just like that, your heart stops as she steps over the edge."
    "For a moment, she hangs in the balance."
    "Like an angel about to take flight."
    "And in that split second you think that maybe if you pray hard enough..."
    "Maybe if you believe it with all your might..."
    "Maybe if you wish for it with every penny and birthday candle\nyou have..."
    "Then just maybe, maybe wings of blazing white will unfold on\nher back."
    "Wings that catch the updraft and send her soaring into the sky."
    window hide
    show expression LiveComposite((1920,1080),(0,0),"lindsey LindseyPose02c",(0,0),"misc flashback") onlayer screens zorder 4 as flashback
    show white onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    hide lindsey
    hide white onlayer screens
    with Dissolve(.5)
    window auto
    "With each wingbeat, memories flood your mind..."
    window hide
    show expression LiveComposite((1920,1080),(0,0),"nurse clinic_nurse_confused",(0,0),"lindsey clinic_lindsey_laughing",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    $set_dialog_mode("default_no_bg")
    ""
    show expression LiveComposite((1920,1080),(0,0),"lindsey lindseycatch_smile",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    ""
    show expression LiveComposite((1920,1080),(0,0),"lindsey soaked_flirty",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    ""
    show expression LiveComposite((1920,1080),(0,0),"lindsey hugging_mcshirt",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    ""
    show expression LiveComposite((1920,1080),(0,0),"lindsey dancing_with_the_mc_background",(0,0),"lindsey dancing_with_the_mc_admiring",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    ""
    show expression LiveComposite((1920,1080),(0,0),"lindsey kiss_kitchen",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    ""
    show expression LiveComposite((1920,1080),(0,0),"lindsey painting_date_laughing",(0,0),"misc flashback") onlayer screens zorder 4 as flashback with Dissolve(.5)
    window auto
    $set_dialog_mode("")
    "You could've had it all..."
    window hide
    show lindsey roof_door_fall
    show white onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    hide flashback onlayer screens
    hide white onlayer screens
    with Dissolve(.5)
    window auto
    "But then reality creeps back in."
    "And in the back of your mind you know... she isn't really an angel."
    "No amount of wishing will save her from the pull of gravity."
    "She's just a girl."
    "Just [lindsey]."
    play sound "falling_thud"
    show lindsey roof_door_lindseyless with vpunch2
    "And then she falls."
  else:
    "Normally, I'd give my [item.title_lower] a chance... but not when [lindsey]'s life is in danger."
    $quest.isabelle_dethroning.failed_item("fire_axe",item)
    return

label quest_isabelle_dethroning_ending:
  $unlock_replay("lindsey_flight")
  window hide
  show misc fireworks onlayer screens zorder 4
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  $mc["focus"] = ""
  pause 5.0
  hide lindsey
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "The Newfall Independence Day — everyone's favorite celebration."
  "Office slaves stream out of their cubicles to watch the fireworks and have an unhealthy meal."
  "Single mothers rock their babies with one hand and sip wine with the other."
  "Girls put on short skirts and complain about unwanted attention."
  "Guys get shitfaced and make mistakes."
  "Blue-eyed kids gather in the streets and backyards, looking up at the stars, realizing their insignificance for the first time."
  "Nothing in this town ever changes."
  "..."
  "At least, that used to be the case..."
  window hide
  show misc ambulance onlayer screens zorder 4
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Who would've thought that going back in time would anger destiny itself?"
  "Thinking about it now, perhaps that was a given."
  "Naturally, the dominos will fall differently when you start tipping new ones."
  "Maybe it was only a matter of time before someone got hurt?"
  "Did my mere presence cause this, or was it something I did along the way?"
  if quest.isabelle_dethroning.in_progress:
    "Maybe breaking the status quo with [isabelle] wasn't such a good idea..."
    "Taking down [kate] is bound to have caused ripples."
  elif quest.kate_stepping.started:
    "Maybe my blind lust for [kate] and the utter destruction of [isabelle]\nis what caused this..."
    "Perhaps karma has the longest whip."
  "But maybe it was out of my hands entirely, and someone else is strumming the strings of fate."
  "Perhaps there's a bigger reason I'm here?"
  "It's absurd to think that time and space bent itself just to get\nme laid."
  "Then again... maybe I'm just here to suffer a second time."
  if quest.isabelle_dethroning.in_progress:
    $quest.isabelle_dethroning.finish(silent=True)
  elif quest.kate_stepping.started:
    $quest.kate_stepping["second_time"] = True
    $quest.kate_stepping.finish(silent=True)
# window hide
# show black onlayer screens zorder 100 with Dissolve(.5)
  #
# show expression Text("To Be Continued...", font="fonts/Fresca-Regular.ttf", color="#fefefe", size=69) onlayer screens zorder 100 as to_be_continued:
#   xalign 0.5 yalign 0.5 alpha 0.0
#   pause 0.5
#   easein 3.0 alpha 1.0
# return
  #
# pause 1.5
# hide misc ambulance onlayer screens
# hide black onlayer screens
# hide to_be_continued onlayer screens
  jump quest_jo_washed_not_started_bedroom
  #
