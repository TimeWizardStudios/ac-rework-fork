image mysterious_medallion_small = Transform("items mysterious_medallion", zoom=0.75)
image bonsai_small = Transform("school nurse_room bonsai", zoom=0.6)
image isabelle_buried_slap =  LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-257,-132), Transform(LiveComposite((1920,1080), (-12,-119), "school ground_floor background", (319,0), "lindsey afraid_slap", (540,0), "isabelle skeptical"), size=(845,476))), "ui circle_mask"))


label isabelle_quest_buried_start:
  show isabelle neutral with Dissolve(.5)
  isabelle neutral "Thanks for the ice tea idea. It always helps me cool down."
  "[isabelle] doesn't show it, but those chocolate hearts really messed her up."
  "The rage still simmers below the surface. And at any given moment..."
  isabelle concerned_left "..."
  mc "Are you okay?"
  isabelle concerned "Yeah... I think."
  mc "Wanna talk about it?"
  isabelle concerned "About what?"
  mc "Maybe your sister? What was she like?"
  isabelle neutral "She was... a lot like you, actually."
  show isabelle neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Short?\"":
      show isabelle neutral at move_to(.5)
      mc "Short?"
      if not quest.isabelle_buried.started:
        $isabelle.lust-=1
      isabelle concerned_left "She wasn't very tall, but I meant lonely."
      isabelle concerned_left "Height isn't something I think of when it comes to people."
      isabelle laughing "To me, personality is the only important factor... and she had a lot of that!"
      isabelle confident "If you can stand up for yourself and others, you're the tallest person in my mind."
      mc "You're right. I'm sorry for your loss."
    "\"Ugly?\"":
      show isabelle neutral at move_to(.5)
      mc "Ugly?"
      isabelle cringe "What?!"
      if not quest.isabelle_buried.started:
        $isabelle.love-=1
      isabelle cringe "Why would you say that?"
      mc "I guess it's the first trait that comes to mind when I think of myself..."
      isabelle thinking "Well, it's a bloody inappropriate thing to say."
      mc "My bad. It just slipped out."
      mc "I'm sorry about your sister... good job keeping it together."
    "\"Lonely?\"":
      show isabelle neutral at move_to(.5)
      mc "Lonely?"
      isabelle sad "Yes. She was very lonely."
      mc "It's a terrible feeling sitting down alone at lunch, watching people laughing and joking a few tables away."
      isabelle sad "I knew she was sad sometimes and complained about her classmates, but I never realized how she really felt."
      mc "It's hard to put into words. Shame, guilt, anxiety... you start second-guessing yourself."
      mc "Do you just sit down with them and have them think you're a parasite?"
      mc "Do you ask if you can join, only to get a few seconds of silence?"
      mc "It's easier to sit by yourself and pretend to be on your phone or study."
      if not quest.isabelle_buried.started:
        $isabelle.love+=2
      isabelle blush "Thanks for sharing this with me. I'm still trying to understand how she felt and this helps a lot."
      isabelle sad "I'm sorry you have to deal with those types of feelings in your everyday life."
      mc "I'm sorry about your sister."
    "\"Funny, handsome, and extremely humble?\"":
      show isabelle neutral at move_to(.5)
      mc "Funny, handsome, and extremely humble?"
      if not quest.isabelle_buried.started:
        $mc.charisma+=1
        $isabelle.lust+=1
      isabelle laughing "Hah! Thanks for lightening the mood."
      isabelle laughing "I tend to get very down and emotional when it comes to her."
      mc "I get that. Can't be easy carrying that around every day."
      isabelle confident "Thank you, [mc]. You're a good guy."
      mc "I'm sorry about your sister... good job keeping it together."
  isabelle sad "Thanks... I do my best to keep it from consuming me, but certain things just rile me."
  if quest.isabelle_stolen["accused"] == "kate":
    isabelle angry "[kate] is one of those things!"
    isabelle angry "I just want to strangle her."
    "It feels weird lying to her about who did it, but honestly, [kate] deserves it."
  else:
    isabelle thinking "I just can't believe [lindsey] would do something like that."
    mc "Yeah, I'm surprised as well."
    mc "But then, how well do you really know people?"
  isabelle cringe "I can't believe she kept the box too. I wish she would at least give it back."
  mc "Is the box valuable?"
  isabelle thinking "Not really, it's just... I like to bury it every year on my sister's birthday."
  isabelle sad "It helps me process the pain and stops it from building."
  isabelle sad "I'd like her to know that no matter where I am in the world, I'll remember her."
  mc "That's really nice of you."
  "This box really means a lot to [isabelle]. it's not just about the chocolates."
  "She'd probably appreciate it if I could somehow find the box. Maybe [lindsey] still has it?"
  hide isabelle with Dissolve(.5)
  $quest.isabelle_buried.start()
  return

label isabelle_quest_buried_start_lindsey:
  if quest.isabelle_stolen["accused"] == "lindsey":
    show lindsey thinking with Dissolve(.5)
    mc "Hey, [lindsey]. How's your cheek?"
    lindsey thinking "It's doing fine, thanks. I'm still so confused about that video of me..."
    mc "Why?"
    lindsey sad "'Cause like I said, I don't remember breaking into her locker!"
    lindsey sad "I swear on my life."
    mc "I don't think it matters at this point."
    mc "[isabelle] is still furious even if she's trying to hide it."
    mc "It might help if you gave her the box back, though."
    jump isabelle_quest_buried_lindsey_box
  elif quest.isabelle_stolen["accused"] == "kate":
    if quest.isabelle_stolen["lindsey_talk"]:
      show lindsey neutral with Dissolve(.5)
      lindsey neutral "[mc]! I almost ran into you again!"
      mc "Don't worry, I'm ready for it now."
      lindsey laughing "That's good, 'cause I keep tripping over things."
      mc "So, about the chocolates... I didn't tell [isabelle] you took them."
      lindsey cringe "I... didn't do it. Someone must've messed with the tape."
      mc "I assume you didn't confess it either?"
      lindsey sad "What am I supposed to tell her?"
      mc "Look, it doesn't really matter at this point, but if you care about [isabelle], she'd really like the box back."
      jump isabelle_quest_buried_lindsey_box
    else:
      jump isabelle_quest_buried_lindsey_missed_scene
  return

label isabelle_quest_buried_lindsey_box:
  lindsey concerned "Oh, um. I don't have it anymore."
  mc "What do you mean?"
  lindsey sad "I gave it to [jacklyn]."
  mc "Why?"
  lindsey thinking "Well, she was watching the female track team when I handed out the chocolates."
  lindsey thinking "So, I gave her the last heart and as well as the box. She got really excited about it for some reason."
  mc "That's a bit odd. Does she always watch the track teams?"
  lindsey laughing "I think only the female team. The guys always end their practice before us, but she usually arrives after us."
  lindsey laughing "She's really nice. Always cheers for us."
  mc "Okay, well I should probably go find that box."
  hide lindsey with Dissolve(.5)
  $quest.isabelle_buried.advance("buried_jacklyn")
  return

label isabelle_quest_buried_lindsey_missed_scene:
  show lindsey thinking at appear_from_right(1.0)
  show lindsey thinking at move_to(0.0)
  pause(1)
  show lindsey thinking flip at move_to(1.0)
  pause(1)
  show lindsey thinking at move_to(0.0)
  pause(1)
  show lindsey thinking flip at move_to(1.0)
  pause(1)
  show lindsey thinking at move_to(.5)
  pause(1)
  $quest.isabelle_stolen["lindsey_talk"] = True
  "She's looking nervous. Perhaps a guilty conscience eating away at her?"
  mc "Hi, thief."
  lindsey skeptical "W-what?"
  mc "The surveillance footage doesn't lie."
  lindsey laughing "You have a weird sense of humor, [mc]."
  mc "Listen, I saw you breaking into [isabelle]'s locker."
  lindsey skeptical "What are you talking about?"
  if lindsey['romance_disabled']:
    lindsey angry "Why do you keep harassing me?"
    lindsey angry "I've never done or said anything bad to you!"
    "Of course, she's going to make this about her. Play the victim."
    "Typical entitled bitch behavior."
    "She's had it so easy her entire life. As soon as someone even questions her, the tears start flowing."
    "It's pathetic really."
    mc "You might have everyone else fooled, but I know that nice-girl exterior is only a facade."
    mc "You walk around the school without a care in the world. You think the world is yours."
    mc "But I've got you this time."
    $lindsey.love-=3
  mc "You can deny it all you want."
  lindsey cringe "I genuinely don't know what you're talking about."
  mc "So, what can you tell me about the clip I have here on my phone?"
  lindsey thinking "..."
  lindsey thinking "What is this?"
  mc "Keep watching."
  lindsey thinking "..."
  lindsey afraid "What the hell?!"
  lindsey afraid "Is this a joke?!"
  mc "You tell me."
  "Either she's a really good actress, or she's as surprised as I was seeing her break into that locker."
  lindsey cringe "This is so weird, I have no memory of doing that..."
  mc "So, if we look  through your bag, there won't be any stolen goods in it?"
  lindsey afraid "Oh my god!"
  mc "What?"
  lindsey afraid "It all makes sense now!"
  lindsey sad "So, I discovered this box of chocolate hearts in my bag the other day."
  lindsey sad "I had no idea how they got there, but I figured it was a gift from a fan or something."
  lindsey sad "People sometimes leave flowers and stuff next to your bag during races."
  mc "How convenient."
  lindsey cringe "I swear I have no memory of even going to her locker!"
  show lindsey cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"That's very hard to believe.\"":
      show lindsey cringe at move_to(.5)
      mc "That's very hard to believe."
      $lindsey.love-=1
      lindsey sad "It's the truth!"
      mc "There's actual video evidence saying otherwise."
      lindsey cringe "Fans leave stuff by my bag sometimes. I thought this was a gift."
      lindsey cringe "I shared the chocolate hearts with my teammates..."
      "It's hard to tell if [lindsey] is lying. She's got tears in her eyes and seems upset about this."
      "But let's be honest, the camera doesn't lie."
      mc "Right now, [isabelle] thinks that [kate] stole the box. She hasn't seen the footage."
      lindsey thinking "What are you saying?"
      show lindsey thinking at move_to(.25)
      menu(side="right"):
        extend ""
        "?lindsey['romance_disabled']@|{image=lindsey contact_icon_evil}|\"I'm saying you should probably start sucking my dick right about now.\"":
          show lindsey thinking at move_to(.5)
          mc "I'm saying you should probably start sucking my dick right about now."
          $lindsey.love-=2
          $lindsey.lust-=2
          lindsey cringe "You're disgusting."
          mc "Come on, it was a joke!"
          "With an ass like hers... deciding where to put my dick is easy."
          "Just slap her to the floor, spread her cheeks, and plunge hard."
          "That's all a whore is good for anyway."
          mc "Honestly, I just want to find that box."
          jump isabelle_quest_buried_lindsey_box
        "\"I'm saying that [isabelle] doesn't have to know the details. Just give me that box.\"":
          show lindsey thinking at move_to(.5)
          mc "I'm saying that [isabelle] doesn't have to know the details. Just give me that box."
          jump isabelle_quest_buried_lindsey_box
        "\"I wasn't insinuating anything, just stating the facts.\"":
          show lindsey thinking at move_to(.5)
          mc "I wasn't insinuating anything, just stating the facts."
          lindsey thinking "I should probably come clean..."
          mc "You do as you want,  but I need that box."
          jump isabelle_quest_buried_lindsey_box
    "?not lindsey['romance_disabled']@|{image=stats love_2}|{image=lindsey contact_icon}|\"If you give the hearts back to [isabelle], she might forgive you.\"":
      show lindsey cringe at move_to(.5)
      mc "If you give the hearts back to [isabelle], she might forgive you."
      mc "She's a nice person."
      lindsey sad "I wish it were that easy."
      mc "Hmm?"
      lindsey eyeroll "Well, since I thought it was from a fan, I gave a heart to each person on the sprint team."
      lindsey eyeroll "Oh my god. I can't believe this."
      lindsey sad "This is horrible!"
      "She does seem upset, but there's no real explanation for the security footage."
      "Fact is, she broke into [isabelle]'s locker."
      "Not really sure what to make of it."
      "The only thing I can think of is some kind of screw up in the timelines."
      "I did travel back in time. Who knows what's been impacted by that?"
      "The simplest answer is that she's lying, but it's all so bizarre."
      mc "I think you need to come clean to [isabelle]."
      lindsey cringe "What do I even say to her?"
      mc "I don't know. That's pretty rough considering you ate them all."
      lindsey sad "What do I do?"
      mc "I don't think you really can do anything, unless you want to come clean to her."
      lindsey sad "I don't even know what happened..."
      mc "Well, [isabelle] thinks [kate] stole it and they already hate each other. So, maybe you could just keep your head down."
      mc "If you give the box back, it might spare [isabelle] some grief."
      mc "It has some sentimental value."
      jump isabelle_quest_buried_lindsey_box
  return

label isabelle_quest_buried_jacklyn:
  show jacklyn smile_right with Dissolve(.5)
  jacklyn smile_right "Purple shadow with purple! Understand? The paper has to talk to you!"
  jacklyn smile "Oh, [mc]. How does the ball swing?"
  mc "Left and right, I suppose?"
  jacklyn thinking "What does that even mean?"
  "Ugh. I should ask her that."
  jacklyn excited "So, how's the art coming along?"
  show jacklyn excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I've been inspired by Japanese animation lately.\"":
      show jacklyn excited at move_to(.5)
      mc "I've been inspired by Japanese animation lately."
      $jacklyn.lust-=1
      jacklyn cringe "Anime? Ew, no."
      mc "I thought you said all art is equal."
      jacklyn annoyed "I also said you need to be a banana in order to replicate its essence."
      jacklyn annoyed "Do you want to be a banana, [mc]?"
    "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"I've been getting into photography lately.\"":
      show jacklyn excited at move_to(.5)
      mc "I've been getting into photography lately."
      $jacklyn.lust+=1
      jacklyn smile "Wicked. That's the right side of the flip."
      jacklyn smile "What kind of photography?"
      mc "Err... nude?"
      jacklyn excited "I get that. Nothing better than a free roll and carte blanche."
      jacklyn laughing "Just remember that taking the cap off the lens is only recommended."
    "\"My new biggest hobby is painting.\"":
      show jacklyn excited at move_to(.5)
      mc "My new biggest hobby is painting."
      jacklyn neutral "Flex. What sort? If you say pastel, I'll cut a bitch."
      show jacklyn neutral at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Pastel.\"":
          show jacklyn neutral at move_to(.5)
          mc "Pastel."
          jacklyn angry "That's a D, bro."
          mc "Not so fast!"
          mc "I challenge you to a paint-off. Pastel only."
          jacklyn annoyed "I'd rather put a box of crayons up my butt."
          mc "Is that a challenge?"
          jacklyn laughing "Yeah, but you go first."
          mc "I... don't think so."
          $jacklyn.lust+=2
          jacklyn laughing "Well, then. No pastels and no crayons! My art class just got improved twofold."
        "?quest.spinach_seek.finished@|{image=items bottle spray_cap}|\"Spray.\"" if not quest.jacklyn_statement.finished:
          show jacklyn neutral at move_to(.5)
          mc "Spray."
          jacklyn excited "Hot shit, dude. Tell me it's not on a canvas."
          mc "The city is my canvas."
          $jacklyn.love+=2
          jacklyn excited_right "Okay, now you're just rubbing my clit!"
          #if mc has jacklyn's number but not played the quest The Statement:
          #jacklyn excited_right "You have my number. Let's go out and make a statement!"
          #"Not sure what that means, but it sounds illegal."
          #"Totally my cup of tea."
          #else:
          jacklyn excited_right "Give me your number and we'll make a statement together."
          $mc.add_phone_contact(jacklyn,silent=False)
          "Not sure what that means, but it sounds illegal."
          "Totally my cup of tea."
        "\"Aquarelle.\"":
          show jacklyn neutral at move_to(.5)
          $mc.intellect+=1
          mc "Aquarelle."
          jacklyn thinking "Really, now?"
          mc "What's wrong with that?"
          jacklyn thinking "You don't strike me as the patient type."
          mc "I'm all sorts of patient!"
          jacklyn annoyed "Well, don't cry in the water color, it'll ruin your wash."
          mc "What's that supposed to mean?"
          jacklyn laughing "Oh, nothing!"
  jacklyn thinking "Anything else you wanted?"
  mc "Actually, yes. [lindsey] said she gave you a box of chocolate hearts. Do you still have it?"
  jacklyn neutral "She did, that little sugar snap. Too bad I'm diabetic."
  mc "What?"
  jacklyn smile "It means I don't dabble."
  mc "Oh. I knew that."
  mc "What about the box?"
  jacklyn neutral "Don't have it. Threw it in the trash outside my classroom."
  "Crap. Better pray the janitor hasn't gotten to it yet."
  mc "Okay, gotta run!"
  hide jacklyn with Dissolve(.5)
  $quest.isabelle_buried.advance("boxhunt")
  return

label quest_isabelle_buried_maxine:
  show maxine thinking with Dissolve(.5)
  maxine thinking "Yes. The answer is most assuredly yes."
  mc "Err... what's the question?"
  maxine neutral "What?"
  mc "I'm confused."
  maxine annoyed "Why did you seek me out if you don't even know the question?"
  mc "I do know the question..."
  maxine annoyed "I'm on my thirty-eight seconds break now, and you're wasting my follicle cells."
  mc "Right. Sorry about that. Do you know who took the box that [jacklyn] put in the trash outside the art classroom?"
  maxine eyeroll "I already told you the answer to that question. You really aren't keeping up."
  mc "Well... who was it?"
  maxine annoyed "See, now I'm not sure if I'm inclined to help you anymore."
  "Oh, shit. She's about to walk off. Think fast... [maxine] loves mysteries."
  show maxine annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.owned_item('mysterious_medallion')@|{image=mysterious_medallion_small}|\"I found a strange medallion.\"":
      show maxine annoyed at move_to(.5)
      mc "I found a strange medallion."
      maxine skeptical "Of course you did."
      mc "What's that supposed to mean?"
      maxine neutral "It could mean many things."
      mc "Are you going to tell me?"
      maxine thinking "Probably not."
      "God. She's so annoying."
      mc "Does that mean you know?"
      maxine smile "It means I might know at some point, but probably won't tell you."
      mc "..."
      mc "Why?"
      maxine thinking "Speculating on future occurrences is dangerous, especially here."
      mc "Right..."
    "?quest.mrsl_HOT >= 'eavesdrop'@|{image=mrsl contact_icon}|\"I overheard an odd conversation.\"":
      show maxine annoyed at move_to(.5)
      mc "I overheard an odd conversation."
      maxine skeptical "Specific details are the currency of the mind."
      mc "[mrsl] was being accused by another woman in the locker room."
      mc "They were talking about a deal of some sort."
      maxine excited "Interesting! What else?"
      mc "The other lady accused [mrsl] of breaking the deal or something."
      maxine laughing "Hahaha!"
      mc "..."
      mc "What's so funny?"
      maxine neutral "A lot of things are funny — stand up comedians, clumsy kittens, the fact that you think you can engage me in coitus."
      mc "What? Err... I never—"
      maxine skeptical "..."
      mc "Fine. You're pretty hot in your own weird way."
      maxine thinking "Incorrect. I'm of average temperature."
      "It's impossible to know if she liked that or not, or why she found the accusation so funny."
    "{image=bonsai_small}|\"There was a tree.\"" if quest.flora_bonsai.finished:
      show maxine annoyed at move_to(.5)
      mc "There was a tree."
      maxine thinking "So, it was."
      mc "I've been struggling to make sense of it."
      mc "The tree growing to an insane size, attacking [flora], escaping through the window."
      mc "Was that all real?"
      maxine "As real as you and me."
      mc "What about, err... specimen 12?"
      if school_ground_floor_west["lifted_glass"]:
        maxine thinking "Yes. You ignored my direct orders."
        mc "You don't seem too upset about it."
        maxine neutral "Why would I be?"
        mc "Because... I ignored you?"
        $maxine.lust+=1
        maxine smile "You let curiosity get the better of you. It's commendable."
        mc "So, you were just trolling?"
        maxine thinking "No, you've most definitely doomed us all."
        mc "..."
      else:
        maxine smile "It's safe and sound."
        maxine smile "When things go from bad to words, we can rely on it."
        mc "You mean \"worse,\" right? When things go from bad to worse."
        $maxine.love+=1
        maxine thinking "I mean what I said."
    "\"I travelled back in time.\"":
      show maxine annoyed at move_to(.5)
      mc "I travelled back in time."
      maxine skeptical "Indeed?"
      mc "Yeah, been here already before."
      maxine skeptical "It's unfortunate, you're an unreliable witness."
      mc "What do you mean?"
      maxine thinking "You've been meddled with."
      mc "Meddled with? Please, if you really know something I need answers!"
      maxine "You only think you do."
      "She's probably lying. That's the problem with [maxine]."
      "Most of the time she's just living in her own little world, making shit up as she goes."
  mc "So... are you going to tell me who took the box?"
  maxine thinking "No, but I'll give you a riddle."
  mc "Seriously?"
  maxine smile "Of course!"
  mc "Great..."
  maxine excited "When stuck in tar, caught in a jar, or chased by a maniac with a crowbar, what do you do?"
  mc "..."
  maxine smile "You've held me up long enough. Adieu."
  hide maxine smile with Dissolve(.5)
  "Hmm... I better write it down before I forget."
  "When stuck in tar, caught in a jar, or chased by a maniac with a crowbar, what do you do?"
  $quest.isabelle_buried.advance("callforhelp")
  return

label quest_isabelle_buried_callforhelp_hidden_number:
  #SFX: (phone) calling...
  "Calling..."
  "Beep!"
  "Shit, it went to straight voicemail."
  mc "Hello? You said you wanted to help me earlier, so I've come to cash in."
  mc "You seem to know what's going on and maybe this is silly to ask, but do you know who took the box of chocolates from the trash?"
  mc "You know how to reach me."
  "Hell of a hail mary, but oh well."
  $quest.isabelle_buried.advance("text")
  return

label quest_isabelle_buried_callforhelp_flora:
  #SFX: (phone) calling...
  "Calling..."
  flora "Hello?"
  mc "Did you pick up an empty box of chocolates from the trash outside the art classroom?"
  flora "Gross."
  mc "So, is that a no?"
  flora "Unlike you, I don't dumpster dive as a hobby."
  mc "It's more of a job. Do you know how much I've made searching the belly of filth and neglect?"
  flora "Bye."
  "She hung up."
  return

label quest_isabelle_buried_callforhelp_jacklyn:
  #SFX: (phone) calling...
  "Calling..."
  jacklyn "What up, sex-fiend?"
  mc "Err...?"
  jacklyn "Whoops! Thought it was my granny!"
  jacklyn "How's it dangling, [mc]?"
  mc "It's... slack, I guess?"
  jacklyn "Gold star! And, no — not that kind."
  mc "Right. Did you pick up a box..."
  mc "Wait, I forgot that you were the one who threw it away. My bad..."
  jacklyn "Don't sweat it, hot-shot."
  "She hung up."
  "Crap. That was embarrassing."
  return

label quest_isabelle_buried_callforhelp_isabelle:
  #SFX: (phone) calling...
  "Calling..."
  "..."
  "Calling..."
  "..."
  "No answer."
  "She would've told me if she'd found it already."
  return

label quest_isabelle_buried_callforhelp_lindsey:
  #SFX: (phone) calling...
  "Calling..."
  "..."
  "Calling..."
  "..."
  "No answer."
  "Not sure why I'm calling her for help, though. Considering she's the one who stole it in the first place."
  return

label quest_isabelle_buried_callforhelp_nurse:
  #SFX: (phone) calling...
  "Calling..."
  if mc.owned_item('compromising_photo'):
    nurse "Hello?"
    mc "Hey.."
    nurse "Goodness, [mc]. I'm really busy, I... I don't think I have time for any... you know."
    mc "Well, this is not about that. However, don't expect any leniency from me — that's not how we do things. Understood?"
    nurse "Yes, yes, understood!"
    mc "Very well."
  else:
    nurse "Hi, [mc]! How are you feeling today?"
    mc "I feel fine."
    nurse "Have you taken your vitamins and had enough to drink?"
    mc "Yes, thanks for looking after me."
    nurse "It's entirely my pleasure!"
  mc "I was wondering if you happened to pick up an empty box of chocolate on the second floor?"
  nurse "I... I'm trying to cut down on sweets. I swear I didn't have any chocolate!"
  mc "Was just wonder if you'd found it."
  nurse "Goodness, no. Sorry, I misunderstood."
  mc "No worries."
  "Hmm... I figured she could've found it and picked it up just to smell it, but I guess not."
  return

label quest_isabelle_buried_callforhelp_kate:
  #SFX: (phone) calling...
  "Calling..."
  kate "Don't make me block your number, creep."
  mc "..."
  "She hung up."
  return

label quest_isabelle_buried_callforhelp_jo:
  #SFX: (phone) calling...
  "Calling..."
  jo "Sweetheart, I have to get to a meeting. What is it?"
  mc "Was just wondering if you'd seen anyone pick up a box from the trash?"
  jo "Oh, no. I don't think so, I've been quite busy. What's going on?"
  mc "I'm just trying to figure something out."
  jo "Well, remember to wash your hands when you're done."
  mc "Thanks, [jo]..."
  return

label quest_isabelle_buried_callforhelp_text:
  #SFX: New text message
  "Huh?"
  $set_dialog_mode("phone_message","hidden_number")
  hidden_number "It is a blessing seeing your name on my screen! At long last, you have come to your senses."
  hidden_number "I have followed your first steps and I have what you seek."
  mc "You have the chocolate box?"
  hidden_number "Indeed."
  mc "Can I have it?"
  hidden_number "You can. I will leave it in your locker."
  hidden_number "However, you should know that I did not find it in the trash."
  mc "What do you mean? Where did you find it?"
  hidden_number "Hidden."
  mc "Hidden? Why?"
  mc "Hello?"
  $set_dialog_mode()
  "Typical. Why do they have to be so cryptic?"
  "And why would [jacklyn] lie about throwing the box in the trash?"
  "Something fishy is definitely happening behind the scenes..."
  "Not sure how [jacklyn] and the box are related to all of it. Honestly, that person could just be lying."
  "Let's see if they at least held their word about putting the box in my locker."
  $quest.isabelle_buried.advance("getbox")
  return

label isabelle_quest_buried_isabelle:
  show isabelle sad with Dissolve(.5)
  isabelle sad "Sorry, but I'd like to be alone."
  mc "Today's the day, isn't it?"
  isabelle sad "Yes..."
  isabelle sad "I feel like I've failed her."
  show isabelle sad at move_to(.75)
  menu(side="left"):
    extend ""
    "?quest.isabelle_stolen['lindsey_slaped']@|{image=isabelle_buried_slap}|\"I think your sister would've been proud of you.\"":
      show isabelle sad at move_to(.5)
      mc "I think your sister would've been proud of you."
      isabelle concerned "Thanks... I probably shouldn't have slapped [lindsey], though."
      mc "If she's anything like me, I'm sure she wouldn't have minded."
      mc "I imagine it feels great letting some of that righteous anger out."
      $isabelle.lust+=1
      isabelle smile "I suppose it did."
      isabelle smile "I still don't get why she stole from me. I didn't peg her as the type."
      mc "I didn't either, but sometimes you don't know the people around you as well as you think."
    "\"I've felt like you for most of my adult life.\"":
      show isabelle sad at move_to(.5)
      mc "I've felt like you for most of my adult life."
      $mc.charisma+=1
      isabelle thinking "What do you mean? Didn't you just turn eighteen?"
      mc "Didn't I tell you I traveled back in time? I'm ancient."
      isabelle laughing "Hah, right!"
      isabelle laughing "Thanks for making me laugh. I needed that."
      mc "Anytime. And honestly, losing [flora] is one of my worst nightmares. Despite not always getting along."
      isabelle smile "You never know what you have until you lose it."
      mc "That's true."
    "\"Remembering her is the important thing here.\"":
      show isabelle sad at move_to(.5)
      mc "Remembering her is the important thing here."
      isabelle sad "I rarely go a day without thinking of her. I'd like to think she's watching me from up there."
      mc "The world is horrible, and I'm glad you're trying to make it a better place for her and for everyone else."
      isabelle smile "Thanks. Sometimes it feels like I'm the only one trying to make a difference."
      mc "People are so caught up in their everyday problems..."
      mc "So focused on getting that grade, job, or girlfriend. Trying to find happiness."
      mc "It's never until tragedy strikes that people realize how little certain things matter."
      $isabelle.love+=1
      isabelle confident "You're absolutely right."
    "\"Was your sister hot?\"":
      show isabelle sad at move_to(.5)
      $mc.lust+=1
      mc "Was your sister hot?"
      $isabelle.love-=1
      $isabelle.lust-=1
      isabelle angry "Are you serious right now?!"
      "Shit, did I say that out loud?"
      mc "Err, I meant hot as in hot-headed! Standing up for herself and such."
      mc "It's American slang!"
      isabelle skeptical "I'm sure it is..."
  mc "Anyway, I have a surprise for you."
  isabelle excited "What is it?"
  mc "I stumbled upon this little box..."
  $mc.remove_item("isabelle_box")
  $isabelle.love+=1
  $isabelle.lust+=1
  isabelle afraid "What?! I can't believe it!"
  isabelle flirty "How did you find it?"
  mc "I have my ways."
  isabelle blush "Aw, thank you so much. You have no idea how much this means to me!"
  mc "Too bad all the chocolates are gone, but at least you can bury it now."
  isabelle blush "You're one of the good ones, [mc]! My sister would've liked you."
  "Back in my old life, if I'd been asked what true happiness is, my answer would probably have been something crude."
  "Losing my virginity. Getting rich. Getting a girlfriend. Getting a blowjob. Anal."
  "While those things are still high on the list, I would never have said that this right here is one of those things..."
  "Making a girl blush with happiness and relief. Helping her deal with a great loss."
  "It's odd that no one ever says that helping others is one of the truest forms of happiness."
  "And it comes so easily!"
  "Finding the box wasn't a journey to Mt. Doom; it was just an ordinary favor, and it meant everything to her."
  isabelle sad "Would you help me bury it?"
  mc "Are you sure?"
  isabelle sad "You've been so kind to me and I think my sister would've liked it."
  mc "What about you?"
  isabelle blush "I'd like it too."
  mc "All right. Have you picked out a spot?"
  isabelle smile "I did! After you showed me around, I took a walk through the woods."
  isabelle smile "There's a nice little glade, which I think would be perfect."
  isabelle excited "I'll show you after school."
  if mc.owned_item("shovel"):
    mc "I think I know the place. I also have a shovel if needed."
    isabelle laughing "Fantastic! I totally forgot about that."
    isabelle smile "Okay, meet me there later!"
    $quest.isabelle_buried.advance("wait")
  else:
    mc  "What about a shovel?"
    isabelle thinking "You're right. I forgot about that."
    isabelle thinking "Do you know where we can find one?"
    mc "Well, the schoolyard is kept by a maintenance company, so actually finding a shovel might be hard."
    mc "But... perhaps we could make one. The forest is full of sticks, just need something spade-like to put at the end of it."
    mc "Wait here, I'll see if I can put something together."
    isabelle smile "All right."
    "All right. Need to find something made of metal. The forest should be full of sticks."
    $quest.isabelle_buried.advance("shovel")
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_buried_isabelle_shovel:
  if mc.owned_item("shovel"):
    show isabelle sad with Dissolve(.5)
    mc "It took me all day to build this shovel. Are you ready?"
    isabelle sad "Thank you. Let's go."
    window hide
    show isabelle sad at disappear_to_right
    pause 0.5
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.location = "school_forest_glade"
    $mc["focus"] = "isabelle_buried"
    $quest.isabelle_buried.advance("bury")
    while game.hour < 19:
      $game.advance()
    hide isabelle
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
  else:
    "Promised to make [isabelle] a shovel. No rest until the deed is done."
  return

label quest_isabelle_buried_entered_glade:
  hide black with Dissolve(.5)
  "Damn it. I was hoping that she'd found another glade than the polluted and beaver-infested one."
  "Just got to keep my mouth shut about the fact that I caused the pollution..."
  $school_forest_glade["exclusive"] = "isabelle"
  $process_event("update_state")
  return

label isabelle_quest_buried_isabelle_bury:
  show isabelle concerned with Dissolve(.5)
  isabelle concerned "I can't believe it. The state of this stream is horrible."
  isabelle concerned "I don't remember it being this bad."
  mc "Maybe we can find another place?"
  isabelle sad "I really like this spot..."
  isabelle sad "The chirping of the birds, the breeze rustling through the leaves, the soft murmur of the stream..."
  isabelle sad "I think I'll try to clean up the stream later."
  mc "Okay, where should I dig?"
  if quest.maxine_hook.finished:
    isabelle thinking "Maybe by that patch of dirt? The ground might be softer there..."
  else:
    isabelle thinking "Maybe by that old car tire? We could throw it away while we're at it...{space=-10}"
  mc "Good idea."
  $quest.isabelle_buried.advance("dig")
  return

label isabelle_buried_car_tire_use(item):
  if item == "shovel":
    show black with Dissolve(.5)
    $set_dialog_mode("default_no_bg")
    "Man, digging is a lot tougher than it seems."
    "Grass, hard-packed dirt, rocks, roots..."
    "[isabelle] will be happy, though. After everything she's been through, she deserves that."
    "..."
    if quest.maxine_hook.finished:
      $quest.isabelle_buried["hole_state"] = 1
      hide black with Dissolve(.5)
      window hide
      window auto
      $set_dialog_mode("")
      show isabelle concerned with Dissolve(.5)
      mc "Is that deep enough?"
      isabelle concerned "Yeah, I think that should do it."
    else:
      "Just a few a feet more and—"
      play sound "falling_thud"
      $quest.isabelle_buried["hole_state"] = 2
      with vpunch
      mc "Huh?"
      hide black with Dissolve(.5)
      window hide
      window auto
      $set_dialog_mode("")
      "What the hell is this?"
      show isabelle concerned_left with Dissolve(.5)
      isabelle concerned_left "What's that?"
      mc "A box of some sort..."
      isabelle concerned_left "It looks ancient... I wonder what's inside."
      mc "Might be bones."
      isabelle excited "It's too small to be a coffin, and it's wrapped in chains!"
      mc "Might be [maya]'s pants."
      isabelle concerned "What do you mean?"
      mc "Err... nothing..."
      "Just that it seems about as hard to get into!"
      isabelle concerned_left "Well, I don't think we'll be able to open it now."
      mc "Probably not. I'll put it in my backpack."
      $quest.isabelle_buried["hole_state"] = 1
      $mc.add_item("locked_box_forest")
    $quest.isabelle_buried.advance("funeral")
  else:
    "If I started digging with my [item.title_lower], [isabelle] would either laugh or think I'm a moron."
    $quest.isabelle_buried.failed_item("car_tire",item)
    return

label isabelle_quest_buried_isabelle_funeral:
  isabelle concerned "Thanks for all the help, [mc]."
  mc "Honestly, happy to. I see how much this means to you."
  isabelle sad "Yeah, it's a pretty big deal..."
  mc "So, what do you usually do now?"
  isabelle sad "I eat the chocolate hearts, and with each one, I think of a good memory with my sister."
  isabelle sad "But... the box is empty now."
  show isabelle sad at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Maybe you can tell me about some of those memories instead?\"":
      show isabelle sad at move_to(.5)
      mc "Maybe you can tell me about some of those memories instead?"
      isabelle sad "Are you sure?"
      mc "Yeah, I think it could be therapeutic."
      isabelle sad "You might be right... I've just never shared these memories with anyone before."
      mc "I think that it's nice you keep them so close to your heart."
      isabelle blush "Aw, all right."
      isabelle blush "My sister was several years younger than me, but we still had to share a small bedroom."
      isabelle neutral "She always woke up before me, took my lipstick, and drew faces on the makeup mirror."
      isabelle neutral "If she was sad, she'd draw a frown. If she was happy, she'd draw a smile."
      isabelle excited "One time she drew hearts when she had a crush on some stupid boy."
      isabelle excited "She always let me know exactly how she was feeling every day."
      mc "I imagine there were a lot of sad faces in the end."
      isabelle sad "I... made her stop doing it. I didn't want her wasting my lipstick."
      isabelle sad "In hindsight, it's one of those things I really regret."
      isabelle sad "Such a silly thing. Who cares about some lipstick, right?"
      mc "I've told [flora] not to touch my things a million times. I don't think you should beat yourself up over this."
      isabelle smile "It's hard not to overanalyze everything you could've done differently."
      mc "Oh, trust me, I know exactly what that's like. Lying awake at night, thinking of all those things you'd do differently if you could."
      mc "I don't think it helps."
      $isabelle.love+=1
      isabelle confident "You're probably right."
    "\"Even though the box is empty, your heart seems fuller than ever.\"":
      show isabelle sad at move_to(.5)
      mc "Even though the box is empty, your heart seems fuller than ever."
      isabelle sad "It doesn't feel that way..."
      mc "Well, I can see how much your sister meant to you. There's no denying that."
      mc "The box is just a symbol, you have all your love for your sister in your heart. No one can steal that away."
      isabelle blush "You're right."
      mc "Sure am."
      isabelle blush "Sometimes you say things I'd never expect. It's like you're wise beyond your years."
      mc "I guess I might be."
      $mc.intellect+=1
      "Didn't travel back in time for nothing..."
      isabelle smile "I appreciate it."
    "\"We'll make sure everyone gets what they deserve.\"":
      show isabelle sad at move_to(.5)
      mc "We'll make sure everyone gets what they deserve."
      isabelle annoyed_left "Yeah, this isn't over."
      $mc.lust+=1
      mc "I imagine it'll be an endless fight to rid the world of bullies, but I'll be with you along the way."
      $isabelle.lust+=1
      isabelle blush "Thank you. I'd like that."
      mc "It's the only thing that we can do."
      isabelle confident "I like that sentiment."
      isabelle confident "But first, let's do what we came here to do."
    "Say nothing":
      show isabelle sad at move_to(.5)
      isabelle sad "Nothing to do about it, I reckon."
      isabelle neutral "Life's never perfect. I know that."
      isabelle skeptical "What I can't get over is the wickedness."
      isabelle skeptical "This is one of those things that she'd come home crying about."
      isabelle angry "Those wankers would take her things."
      isabelle angry "I get how it feels now."
      isabelle sad "I'm just sad it took me so long to understand her pain."
      isabelle sad "..."
      isabelle sad "Sorry, I'm rambling."
      mc "Hey, it's okay. I totally get it."
      $mc.love+=1
      isabelle blush "You're such a good listener. Thank you, [mc]."
  mc "So, how do you usually do this?"
  isabelle sad "It's complicated."
  mc "Complicated, how?"
  isabelle sad "I always feel like she's watching."
  mc "Maybe she is."
  isabelle neutral "I think so."
  isabelle neutral "..."
  isabelle neutral "My first memory of her is from a Christmas day when we were little."
  isabelle smile "She was sitting on my lap and she caught my cheek between her chubby little fingers, pinching it hard."
  isabelle smile "She could definitely be a brat when she wanted to, but she was a really good person."
  mc "Sounds like she had quite the personality."
  isabelle laughing "You could say that!"
  isabelle smile "One time she found a bird with a broken wing."
  isabelle smile "Dad said there was no point, but she was stubborn."
  isabelle confident "It took her a couple of months, but she nursed the bird back to health."
  isabelle confident "I'll never forget her smile when that bird spread its wings and flew into the summer sky."
  mc "I'm really sorry."
  isabelle sad "So am I... every single day."
  "What do you even say to someone who's dealing with this kind of pain?"
  "These kinds of wounds probably never heal."
  "One day they're there, and the next... your world is never the same."
  "You're left clutching for memories and struggling with what ifs."
  "To everyone else, it's just an empty box, but to [isabelle] it's all the good things she shared with her sister."
  "Every year she buries it to get some semblance of closure. That's all she has left."
  isabelle sad "I think it's time."
  window hide
  show black with Dissolve(.5)
  $set_dialog_mode("default_no_bg")
  "..."
  "What goes through her mind when she kneels next to the hole and puts the box into the ground?"
  "What sort of dust and ashes tumble through her mind as she fills the hole?"
  "I've known thoughts so painful they rip a chunk out of your soul, but they're made of self-pity and jealousy."
  $quest.isabelle_buried["hole_state"] = 3
  "Her pain must be different. A broken heart that won't stop bleeding."
  hide black with Dissolve(.5)
  pause 0.5
  $quest.isabelle_buried["hole_state"] = 4
  pause 1.0
  $quest.isabelle_buried["hole_state"] = 5
  pause 1.0
  window auto
  $set_dialog_mode("")
  isabelle sad "Will you stay with me a little while?"
  mc "Of course. Take as long as you need."
  isabelle sad "Do you think there's a place where good people get to go when they die?"
  mc "Like a heaven?"
  isabelle concerned_left "Maybe... I just find it so unfair that she's gone."
  mc "It is unfair. I don't really believe in heaven, but I hope she's found peace."
  isabelle concerned "Sometimes when I wake up, I see her in the morning light as it passes through the window."
  isabelle concerned "Sometimes I see her in the reflection of a puddle or amongst the drifting clouds."
  isabelle excited "She's always smiling at me, like the day she released that bird."
  mc "Maybe she's just as free to spread her wings now."
  isabelle neutral "I'd like to believe that."
  isabelle neutral "..."
  isabelle sad "After what happened, I wasn't sure if I'd be able to do this on my own."
  mc "I'm sure you could've."
  isabelle blush "Thanks, but I'm glad I got to share this with you."
  isabelle blush "I think you're quite special."
  mc "Special?"
  "Only ever heard that in a negative connotation before, but [isabelle]'s seems genuine."
  "I didn't really do much of anything either, but maybe just being there for her is enough?"
  isabelle blush "Is it okay if I hug you?"
  mc "Yes! I mean, I guess. Sure."
  isabelle laughing "Smooth!"
  show isabelle isabelle_hug with Dissolve(.5)
  "They never tell you what a hug of true gratitude feels like."
  "They never share what a girl's arms wrapped around your neck feels like."
  "How she rests her head on your shoulder."
  "All those lovely scents of her hair and skin."
  "How she leans her full weight against you."
  "How she breathes into you, and you into her."
  "They never tell you!"
  "And there you are, worrying about ever having sex. Worrying about love and lust."
  "And in reality, the thing you're really missing is intimacy."
  "Who knows, maybe that's the first step? Platonic intimacy."
  "Maybe I'm just drunk on happy brain chemicals, but there's definitely a difference between hugs."
  "I've seen girls hug the jocks and each other — they do it every morning, like a ritual — but they don't hug like this."
  "They don't hug like [isabelle] is hugging me now."
  "This is another type of hug."
  "They never tell you about this type of hug."
  "They never tell you—"
  show isabelle isabelle_kiss with Dissolve(.5)
  "................................................................."
  "And then it just happens."
  "Just like that, she gets on her tippy toes and puts her lips to yours."
  "After you've put it on a pedestal for thirty years, it's... almost too natural."
  "Like it was inevitable somehow."
  "[isabelle] is headstrong, confident, proud, and kind, but she also has her flaws and demons."
  "But who doesn't?"
  "There's no such thing as the perfect girl, but her lips..."
  "Maybe her lips are flawless."
  "Warm and soft and everything I've never dreamed of, because they don't tell you about it!"
  "The movies show it, but they never tell you what it feels like to kiss in the sunset."
  "And maybe that's for the best."
  "Let everyone experience it for themselves."
  "Or maybe they can't tell you..."
  "Maybe that first kiss would be tainted with flowery metaphors and overblown imagery if they tried."
  "Winding sentences about entwined lovers, tender touches, sparkling eyes."
  "Diluted with rushing blood and hammering heartbeats. Dilated pupils and rosy cheeks."
  "..."
  "It's good that they don't tell you, because there's no point."
  "It's just the first kiss..."
  "No need to romanticize it."
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  $game.location = home_bedroom
  $mc["focus"] = ""
  while game.hour != 7:
    $game.advance()
  pause 1.0
  hide isabelle
  hide black onlayer screens
  with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "It's hard to believe [isabelle] kissed me last night."
  "A girl like her and a guy like me. Must've been a dream."
  if quest.maxine_hook.finished:
    "But then... I can still feel her arms around me..."
    "..."
    "God damn, I actually kissed [isabelle]!"
  else:
    "Then again... I have that box we dug up. What the hell could be in it?{space=-10}"
    "And god damn, I actually kissed [isabelle]!"
  "Can still feel the tingles on my lips..."
  "I wonder how she feels about it today?"
  "For the first time in forever, I'm eager to get to school."
  $unlock_replay("isabelle_hug")
  $quest.isabelle_buried.advance("missinglocker")
  $school_forest_glade["exclusive"] = 0
  return

label school_ground_floor_missing_locker_interact_isabelle_bury:
  "What the hell happened here?"
  "This might just be the most bizarre thing I've seen."
  "The entire locker and large chunk of the floor is just gone..."
  "[isabelle] is going to—"
  show isabelle angry at appear_from_right(.5)
  isabelle angry "This is bloody bonkers. First my box of chocolates and now my entire locker!"
  isabelle angry "How dare they steal my locker?!"
  mc "I'm sure there's some kind of misunderstanding..."
  isabelle eyeroll "There really isn't. Just read the note."
  mc "The note?"
  isabelle angry "Yeah, someone even left a bogus permit."
  isabelle angry "Can you imagine?!"
  show misc lockerremoval with Dissolve(.5)
  pause
  hide misc lockerremoval with Dissolve(.5)
  "Uh, this doesn't look good at all. [isabelle]'s about to explode."
  show isabelle angry at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Have you heard of that group before?\"":
      show isabelle angry at move_to(.5)
      mc "Have you heard of that group before?"
      isabelle skeptical "Seems made up. There's no way anyone steals a locker and a good chunk of the floor."
      isabelle skeptical "I talked to the [guard] earlier. Really useful bloke that one..."
      mc "What did he say?"
      isabelle annoyed_left "He said someone messed up the security feed so there was no footage."
      isabelle annoyed "He also said the permit looks legit, which is complete bollocks."
      mc "Is he a suspect then?"
      isabelle cringe "To be honest, he just seems lazy and uncooperative."
      isabelle cringe "I'm going to get an ice tea and then I'm off to the city for new books. Bloody fantastic day."
    "\"This is pretty crazy.\"":
      show isabelle angry at move_to(.5)
      mc "This is pretty crazy."
      isabelle skeptical "Just amazing, really."
      isabelle skeptical "First my chocolate box, now my locker. Unreal."
      mc "Think it's related?"
      isabelle annoyed "How can it not be?"
      mc "Hmm... in that case, someone's got it out for you..."
      mc "Never heard of that group, but it's worth looking into."
      isabelle cringe "I'm going to get an ice tea and then off to the city for new books. Bloody fantastic day."
    "?isabelle.love>=3@[isabelle.love]/3|{image= isabelle contact_icon}|{image=stats love}|\"We need to talk about the kiss.\"":
      show isabelle angry at move_to(.5)
      mc "We need to talk about the kiss."
      isabelle flirty "You want to talk about it or do it again?"
      mc "Err... I guess the latter?"
      isabelle blush "Help me figure this out and we'll see!"
      mc "Have I ever told you that I'm a direct descendant of Sherlock Holmes?"
      isabelle laughing "Thanks for always lightening the mood, [mc]. It's a real talent you have!"
      isabelle confident "Anyway, I need to go and buy new school books. See you around, detective."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_buried.finish()
