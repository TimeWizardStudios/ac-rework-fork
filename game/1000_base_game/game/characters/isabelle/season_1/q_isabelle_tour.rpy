label isabelle_quest_isabelle_tour:
  show isabelle laughing with Dissolve(.5)
  isabelle laughing "I just wanted to say that I appreciate you showing me around."
  mc "It's fine! This school is confusing sometimes, even for me."
  "It's been years since graduation... my memory is a bit fuzzy on the layout."
  mc "So, I guess we should start by signing up for our focus classes. That's what [mrsl] said, right?"
  isabelle smile "I think so!"
  mc "What classes are you interested in?"
  isabelle smile_left "Hmm... I haven't really had the time to think about it yet, but English is a given for me... we could start there."
  if not quest.the_key.finished:
    mc "Sure, just got to pick up my locker key. Meet you upstairs, okay?"
    isabelle laughing "All right! See you there."
  else:
    mc "Sure, let's head upstairs, then."
    isabelle laughing "Great!"
  $quest.isabelle_tour.advance("go_upstairs")
  $process_event("update_state")
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_isabelle_tour_first_hall:
  if not isabelle["volunteered"]:
    show isabelle annoyed_left with Dissolve(.5)
    isabelle annoyed_left "I saw [kate] on the way up."
    isabelle annoyed_left "She gave me this smug look... as if she took some kind of twisted pleasure in ditching me earlier."
    show isabelle annoyed_left at move_to(.75)
    menu(side="left"):
      extend ""
      "\"Best you can do is keep your head down.\"":
        show isabelle annoyed_left at move_to(.5)
        $mc.intellect+=1
        mc "Best you can do is keep your head down."
        $isabelle.lust-=1
        isabelle annoyed "I don't think I will. She needs to learn some manners..."
        mc "Trust me, you don't want to push her."
        isabelle angry "Bullies make me sick! I'm going to have a word with her... are you coming or not?"
        jump isabelle_tour_first_hall_ending
      "\"She probably doesn't understand what a great opportunity it is.\"":
        show isabelle annoyed_left at move_to(.5)
        mc "She probably doesn't understand what a great opportunity it is."
        isabelle annoyed "A great opportunity for what?"
        $mc.charisma+=1
        mc "To meet a new person and exchange experiences!"
        $isabelle.love+=1
        isabelle blush "Aw, I like that mindset!"
        "Playing Super Seducer is finally paying off! Hell yeah."
      "\"I'll go talk to her and make sure she apologizes.\"":
        show isabelle annoyed_left at move_to(.5)
        $mc.intellect-=1
        mc "I'll go talk to her and make sure she apologizes."
        $isabelle.lust+=1
        isabelle blush "That's so nice!"
        "And probably so stupid... sometimes the need to impress flushes my wits down the toilet."
        isabelle blush "I didn't think you would do something like that..."
        isabelle sad "I'm sorry for misjudging your character. You're more than meets the eye."
        "The old me was too much of a loser to even look at [kate]... has anything really changed?"
        show isabelle sad at move_to(.25)
        menu(side="right"):
          extend ""
          "\"On second thought, that's a terrible idea...\"":
            show isabelle sad at move_to(.5)
            mc "On second thought, that's a terrible idea..."
            $isabelle.lust-=1
            isabelle annoyed "Really? Did your spine turn to rubber all of a sudden?"
            mc "I mean... it's not like that. It's just..."
            isabelle angry "Bullies make me sick! I'm going to have a word with her... are you coming or not?"
            jump isabelle_tour_first_hall_ending
          "\"I've always wanted to set her straight. It's about damn time!\"":
            show isabelle sad at move_to(.5)
            mc "I've always wanted to set her straight. It's about damn time!"
            $isabelle.lust+=1
            $isabelle.love+=1
            isabelle blush "Definitely! Let's go!"
            "Things are different now. At least, on the paper... hopefully?"
            "Shit... this might just be the worst idea ever."
            $quest.isabelle_tour.advance("confrontation_side_isabelle")
            hide isabelle with Dissolve(.5)
            return
  else:
    show isabelle annoyed_left with Dissolve(.5)
    isabelle annoyed_left "Who is the bored-looking blonde over there?"
    mc "That's [kate]. She's the head cheerleader, mean-bean extraordinaire, and future prom queen."
    isabelle annoyed "On the way up the stairs, I overheard her calling you all sorts of nasty things for volunteering."
    mc "That's... let's just ignore that..."
    isabelle annoyed "Ignore it? Why?"
    "Crap. How do you tell someone that you're everyone's punching bag?"
    show isabelle annoyed at move_to(.75)
    menu(side="left"):
      extend ""
      "\"You're new here... some things just aren't worth it.\"":
        show isabelle annoyed at move_to(.5)
        $mc.charisma+=1
        mc "You're new here... some things just aren't worth it."
        isabelle angry "And some things are absolutely worth it!"
        isabelle angry "I've seen this all before, and it makes everyone miserable... even her."
        mc "I think you're wrong. [kate] takes great pleasure in this."
        isabelle annoyed_left "..."
        isabelle angry "I'm going to have a word with her. Are you coming or not?"
        jump isabelle_tour_first_hall_ending
      "\"I'm used to it.\"":
        show isabelle annoyed at move_to(.5)
        mc "I'm used to it."
        $isabelle.lust-=1
        isabelle sad "That's one of the saddest things I've heard. You need to stand up for yourself..."
        "Right... because that's such an easy thing to do."
        isabelle angry "Bullies make me sick! I'm going to have a word with her... are you coming or not?"
        jump isabelle_tour_first_hall_ending
      "\"She's probably just jealous that I got to hang out with the cute new girl first.\"":
        show isabelle annoyed at move_to(.5)
        $mc.charisma+=1
        mc "She's probably just jealous that I got to hang out with the cute new girl first."
        $isabelle.love+=1
        isabelle blush "Aw, that's so sweet!"
  isabelle annoyed_left "But her behavior is unacceptable! I'm going to have a word with her."
  show isabelle annoyed_left at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I've always wanted to set her straight... it's about damn time.\"":
      show isabelle annoyed_left at move_to(.5)
      mc "I've always wanted to set her straight... it's about damn time."
      $isabelle.lust+=1
      $isabelle.love+=1
      isabelle blush "Definitely! Let's go!"
      "Things are different now. At least, on paper... hopefully?"
      "Shit... this might just be the worst idea ever."
      $quest.isabelle_tour.advance("confrontation_side_isabelle")
      hide isabelle with Dissolve(.5)
      return
    "\"That's not a good idea.\"":
      show isabelle annoyed_left at move_to(.5)
      mc "That's not a good idea."
      isabelle angry "I think it's a perfect idea! We can't allow this type of behavior."
      mc "This is how you end up in a dumpster. Don't try to break the status quo."
      isabelle angry "Sorry, but that's what I'm all about."
      isabelle angry "I'm going to have a word with her... are you coming or not?"
    "\"Is making enemies really the best way to start off the year?\"":
      show isabelle annoyed_left at move_to(.5)
      mc "Is making enemies really the best way to start off the year?"
      isabelle sad "I suppose it's not..."
      isabelle angry "But with people like her, you have to nip that attitude in the bud! Otherwise, they'll just keep doing it..."
      isabelle angry "I'm going to have a word with her. Are you coming or not?"

label isabelle_tour_first_hall_ending:
  show isabelle angry at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Ugh, I guess...\"":
      show isabelle angry at move_to(.5)
      mc "Ugh, I guess..."
      isabelle angry "Let's go!"
    "\"No, I've made that mistake before...\"":
      show isabelle angry at move_to(.5)
      mc "No, I've made that mistake before..."
      isabelle sad "Why did I expect more?"
      $isabelle.lust-=1
      $isabelle.love-=1
      isabelle sad "I'll go alone."
  $quest.isabelle_tour.advance("confrontation_side_kate")
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_isabelle_tour_english_class:
  show isabelle annoyed_left with Dissolve(.5)
  isabelle annoyed_left "I can't believe people like [kate] are allowed to walk around the school..."
  isabelle annoyed_left "It's stark raving bonkers!"
  mc "She's always been like that, and she never gets detention or any other form of discipline."
  isabelle annoyed "How come?"
  mc "Her dad knows the people on the board... at least, that's what I've heard."
  isabelle angry "A corrupt system! That's always how bad people get away with things."
  isabelle sad "If we can't trust the authorities to do their job, we'll have to take justice into our own hands..."
  show isabelle sad at move_to(.25)
  menu(side="right"):
    extend ""
    "\"What are you planning to do?\"":
      show isabelle sad at move_to(.5)
      mc "What are you planning to do?"
      isabelle concerned_left "I don't know yet, but I'm sure I'll come up with something..."
      isabelle skeptical "Someone needs to show her that her behavior isn't acceptable."
      mc "And I guess that someone is you?"
      isabelle neutral "And you, I hope? You saw how she acted..."
      mc "I mean, it's a bit risky, but I've got your back."
      isabelle excited "Thanks! I knew you weren't all bad."
      "That's funny... might be the first time I've heard those words within these walls."
      isabelle excited "I've signed up for English. Can you take me to the art classroom next?"
      mc "Sure thing, it's right next door."
    "\"I think we should start by signing up for our focus classes. Have you decided on yours yet?\"":
      show isabelle sad at move_to(.5)
      mc "I think we should start by signing up for our focus classes. Have you decided on yours yet?"
      isabelle skeptical "You're right... revenge and malice can wait."
      isabelle excited "So, I've signed up for English now. I think art would be fun as well!"
      isabelle neutral "I'll have to think a bit more on my third pick."
      mc "Okay, let's meet in the art classroom? It's right next door."
      isabelle excited "Sure. Meet you there!"
  $quest.isabelle_tour.advance("art_class")
  $process_event("update_state")
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_isabelle_tour_art_class:
  show isabelle excited with Dissolve(.5)
  isabelle excited "[jacklyn] seems nice! I'm really looking forward to taking her classes."
  mc "Hmm..."
  isabelle concerned "What? You don't think so?"
  mc "Hmm..."
  isabelle concerned "You seem distracted..."
  mc "I have a lot on my mind right now."
  "And a hot teacher's assistant is just one of them."
  "The whole [kate] drama is making me nervous. [isabelle] is acting like nothing happened... does she often run into trouble like that?"
  "Also, going back in time still has me off balance."
  "Those texts... I really need to find out who sent them... they might be the only one with answers."
  isabelle neutral "I think I need to balance out my schedule with something physical."
  isabelle excited "I'm going to take gym classes as my last focus!"
  mc "Why?"
  isabelle concerned "Why what?"
  mc "Why gym? There are all sorts of other classes upstairs."
  isabelle excited "I just told you why... I need something physical."
  mc "...Can't you just ride a bike to school?"
  isabelle skeptical "What's wrong, [mc]? Why the shifty eyes?"
  "If only she knew what a toll gym class took on me... the pushy teachers, the nasty classmates, crowded locker rooms and showers..."
  mc "Bad things always happen in the east wing..."
  isabelle skeptical "What kind of bad things?"
  mc "I... I always end up getting hurt..."
  "Physically, but also psychologically. That place is cursed."
  isabelle confident "It's all right... I'll look after you."
  "Ugh, she's too nice for this place."
  isabelle laughing "Come on, let's get it over with!"
  $quest.isabelle_tour.advance("gym")
  $process_event("update_state")
  hide isabelle with Dissolve(.5)
  return

label isabelle_quest_isabelle_tour_gym:
  show isabelle smile with Dissolve(.5)
  isabelle smile "Thanks for showing me around, [mc]!"
  isabelle laughing "Apart from the run-in with [kate], this was quite the pleasant tour."
  mc "No problem!"
  if quest.kate_over_isabelle.in_progress:
    "The story with [kate] is, however, far from over..."
    "[isabelle] has no idea what's coming. It feels a little bad, but such is life."
    "The first part of [kate]'s request was finding out [isabelle]'s focus classes... with that out of the way, the next thing is her favorite drink."
    mc "Hey, would you like me to show you the cafeteria as well?"
    isabelle confident "Oh, sure! If you have time..."
    mc "Yeah, no worries. The food isn't amazing, but the strawberry juice is the best!"
    isabelle confident "Cool, let's meet up there."
    $quest.kate_over_isabelle.advance("spy_drink")
  elif quest.isabelle_over_kate.in_progress:
    "The story with [kate] is, however, far from over..."
    "[isabelle] has no idea what's coming. I need to make sure she's safe."
    mc "Have you thought about the [kate] problem?"
    isabelle eyeroll "Yeah, we need to do something about her..."
    isabelle eyeroll "Meet me in the cafeteria and we'll come up with a plan."
    mc "Sounds good."
    $quest.isabelle_over_kate.advance("meeting")
  isabelle flirty "By the way, have you—"
  isabelle afraid "Look out!"
  $quest.isabelle_tour.advance("crash")
  play sound "falling_thud"
  hide isabelle
  show black
  with hpunch
  pause(1)
  hide black
  show lindsey lindseypose01
  with Dissolve(.5)
  lindsey lindseypose01 "Oww..."
  "Damn, that took the wind out of me! [lindsey]'s always been fast, but luckily for me, she's tiny."
  "That's the second time she's crashed today. She never seemed like the clumsy kind... maybe her eye-sight is getting bad?"
  "In any case, mine isn't, and that's a relief — else I wouldn't be able to feast my eyes on her."
  "It does feel a little bad, but everyone else is staring too... who can blame us when she spreads her legs like that?"
  "It's like her dazed state has shattered her inhibitions. Her body is calling out for someone to... just give it to her."
  "Like she's longing to wrap her legs around your waist as you slam into her. Maybe she has a thing for slamming into stuff?"
  "Maybe she likes it rough and saw the opportunity to get physical with me?"
  "Oh, who am I kidding? That's just me projecting my fantasies — push her down, grab her by the throat, thrust into her repeatedly until her eyes roll back."
  "Might be a trick of the light, but isn't there a wet spot... just in the right spot?"
  "The contour of her labia against her panties... oh, man... "
  $mc["turn_off_location_transitions"] = True
  $unlock_replay("lindsey_collision")
  $quest.lindsey_nurse.start()
  lindsey lindseypose01 "Who put a wall there...?"
  show isabelle thinking at right with Dissolve(.5)
  isabelle thinking "Hey, are you okay?"
  lindsey lindseypose01 "M-my head..."
  isabelle afraid "[mc], she looks really hurt!"
  menu(side="far_left"):
    extend ""
    "\"Wait here, I'll go get the [nurse]!\"":
      mc "Wait here, I'll go get the [nurse]!"
      $isabelle.love+=1
      isabelle afraid "Okay, but hurry!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $quest.lindsey_nurse.advance("fetch_the_nurse")
      $mc["focus"] = "lindsey_nurse"
      $school_nurse_room["curtain_off"] = False
      if game.quest_guide == "isabelle_tour":
        $game.quest_guide = "lindsey_nurse"
        $quest.lindsey_nurse["revert_quest_guide"] = True
      pause 0.5
      hide lindsey
      hide isabelle
      hide black onlayer screens
      with Dissolve(.5)
    "\"Help me get her to the [nurse]'s office!\"":
      mc "Help me get her to the [nurse]'s office!"
      "[lindsey] probably never cared about my existence until it hit her in the face... but this is what second chances are about."
      isabelle concerned_left "Hey, what's your name?"
      lindsey lindseypose01 "[lindsey]..."
      isabelle concerned_left "Okay, [lindsey], do you think you can stand if we help you?"
      lindsey lindseypose01 "M-maybe..."
      isabelle neutral "Okay, put one arm around each of our necks... we'll take you to the [nurse]."
      hide lindsey
      hide isabelle
      show black
      with Dissolve(.5)
      $quest.lindsey_nurse.advance("carry_to_nurse")
      $lindsey["at_school_nurse_room_now"] = True
      $isabelle["at_school_nurse_room_now"] = True
      pause(1)
      jump goto_school_nurse_room
    "\"She should've paid more attention.\"":
      mc "She should've paid more attention."
      $isabelle.love-=1
      isabelle angry "What's wrong with you?!"
      mc "Why should I care about someone who has never given two shits about me?"
      isabelle annoyed_left "Because she's hurt! Don't you have any compassion?"
      mc "She would probably have laughed if I ran into a beefy football player..."
      isabelle angry "Here's your chance to set things right, then! Be the bigger person!"
      menu(side="far_left"):
        extend ""
        "Help":
          mc "Fine... okay. Help me get her up."
          isabelle sad "Thank god."
          hide lindsey
          hide isabelle
          show black
          with Dissolve(.5)
          $quest.lindsey_nurse.advance("carry_to_nurse")
          $lindsey["at_school_nurse_room_now"] = True
          $isabelle["at_school_nurse_room_now"] = True
          pause(1)
          jump goto_school_nurse_room
        "Pass":
          mc "Nope, not my problem."
          $isabelle.love-=1
          $isabelle.lust-=1
          $lindsey.love-=1
          isabelle annoyed "How can you be so callous?"
          "When no one gives a shit about you, it's not that hard to turn off your emotions..."
          "[lindsey] never cared about my existence until it hit her in the face."
          lindsey lindseypose01 "Stars... so many stars..."
          isabelle concerned_left "Hey, what's your name?"
          lindsey lindseypose01 "[lindsey]..."
          isabelle concerned_left "Okay, [lindsey], do you think you can stand up?"
          lindsey lindseypose01 "M-maybe..."
          isabelle neutral "Okay, put your arm around my neck... I'll take you to the [nurse]."
          window hide
          $lindsey["at_none_now"] = True
          $lindsey["at_school_nurse_room_now"] = True
          $isabelle["at_school_nurse_room_now"] = True
          hide lindsey
          hide isabelle
          show black onlayer screens zorder 100
          with Dissolve(.5)
          pause(1)
          hide black onlayer screens with Dissolve(.5)
          $quest.isabelle_tour.advance("cafeteria")
          $quest.lindsey_nurse.fail()
          $mc["turn_off_location_transitions"] = False
          $lindsey["romance_disabled"] = True
          $game.notify_modal(None,"Love or Lust","Romance with [lindsey]:\nDisabled.",wait=5.0)
          window auto
    "?mc.strength>=3@[mc.strength]/3|{image=stats str}|Carry [lindsey]":
      hide isabelle
      show lindsey LindseyPose02a
      with fadehold
      call goto_school_first_hall_east
      lindsey LindseyPose02a "W-what happened...?"
      mc "You had an accident. I'm taking you to the [nurse]."
      "This is probably the closest I've ever been to a girl, and it's really not what I expected..."
      "I just want to hold her, breathe in the scent of her hair, the flowery shampoo and natural oils."
      "All those dirty thoughts feel wrong now."
      "She's so light in my arms... so soft and fragile... like porcelain."
      "It's a good feeling — a pure and selfless one — to care for someone in need."
      "She's trusting me with her vulnerability... that's the best part."
      $lindsey.love+=1
      $lindsey.lust+=1
      "Makes me want to kiss her on the forehead and comfort her. Be a gentleman and a hero!"
      "Perhaps, that's what this feeling is — a taste of heroism."
      lindsey LindseyPose02b "Everything's spinning..."
      menu(side="right"):
        extend ""
        "\"Don't worry, I've got you.\"":
          mc "Don't worry, I've got you."
          $lindsey.lust+=1
          lindsey LindseyPose02a "T-thank you... I think I hit my head..."
          mc "You did."
          mc "Smacked right into my chest!"
          lindsey LindseyPose02b "Oh... did I... did I hurt you?"
          mc "Nah, you're good. Sorry I couldn't soften the blow."
          lindsey LindseyPose02a "It's good that you're strong enough to carry me..."
          lindsey LindseyPose02a "I don't think I could walk right now..."
          mc "No worries! I'll carry you right back to the gym as soon as the [nurse] clears you!"
          lindsey LindseyPose02c "I bet you would..."
        "\"Never seen anyone run so fast!\"":
          mc "Never seen anyone run so fast!"
          $lindsey.love+=1
          lindsey LindseyPose02a "Chivalry and flattery...? What's next...?"
          mc "Hopefully, the [nurse] saying that it's a mild concussion and you'll be okay in a couple of days."
          mc "You have a sprinting competition to win this year!"
          lindsey LindseyPose02a "T-thanks for taking me there... that's so nice of you..."
          "I've never seen a girl look at me like this before..."
          "Who knew admiration would feel so fulfilling?"
          "[lindsey] must've hit her head really hard."
      show lindsey LindseyPose02nobg with Dissolve(.5)
      call goto_school_first_hall_east
      pause(1)
      call goto_school_first_hall
      pause(1)
      call goto_school_ground_floor
      pause(1)
      call goto_school_ground_floor_west
      pause(1)
      hide lindsey
      show black
      with Dissolve(.5)
      $quest.lindsey_nurse.advance("carry_to_nurse")
      $lindsey["at_school_nurse_room_now"] = True
      pause(1)
      jump goto_school_nurse_room
  return

label isabelle_quest_isabelle_tour_fetch_nurse:
  $mc["turn_off_location_transitions"] = False
  $nurse.talking=True
  nurse "Mr. Brown!"
  nurse "That's not how you use a medical massager!"
  nurse "Oh! Oh, my... {i}Mr. Brown!{/}"
  nurse "You can't let anyone know! My reputation! My job!"
  label quest_nurse_photogenic_second_chance:
  menu(side="left"):
    extend ""
    "Pull back the curtain":
      $unlock_replay("nurse_compromise")
      $unlock_stat_perk("lust12")
      $school_nurse_room["curtain_off"]= True
      $nurse.equip("nurse_masturbating")
      show nurse neutral with Dissolve(.5)
      show nurse surprised with dissolve2
      nurse surprised "[mc]?! What in god's name?!" with vpunch
      "Look at her all embarrassed! The heat of her lust still clinging to her cheeks!"
      $mc.lust+=1
      "To catch someone in such a compromising position has always been a fantasy of mine."
      "To see the terror and desire in her eyes... her fingers still glistening with her juices..."
      "Beads of sweat on her forehead, so close to reaching her climax — but unable now."
      "Denied."
      "And then the repercussions start filling her head. Shame... regret... fear... but also excitement."
      "It still lingers as her body disagrees with her mind."
      "Her blush deepens."
      "How will she get out of this? Will I let her?"
      "The loss of power... the exposure... we're both feeling it."
      nurse afraid "This is not what it looks like!"
      show nurse afraid at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Huh! It looks exactly like you were\nhaving a very wet daydream about\nthe English teacher!\"":
          show nurse afraid at move_to(.5)
          mc "Huh! It looks exactly like you were having a very wet daydream about the English teacher!"
          nurse annoyed "I... I..."
          mc "Don't worry, I won't tell anyone."
          nurse neutral  "Really?"
          mc "Of course not! It'll be our little secret."
          nurse smile "Thank you... I can't lose this job..."
          mc "You're welcome!"
          show nurse smile at move_to(.75)
          menu(side="left"):
            extend ""
            "\"Just one for the yearbook, okay?\"":
              show nurse smile at move_to(.5)
              mc "Just one for the yearbook, okay?"
              show white with Dissolve(.15)
              play sound "camera_snap"
              hide white with Dissolve(.15)
              $mc.add_item("compromising_photo")
              $nurse.lust+=1
              $nurse.love-=1
              nurse afraid "W-what?"
              mc "Your job and reputation mean everything to you, right?"
              nurse surprised "Please, this can't get out!"
              mc  "If you play your cards right, nobody will see this."
              nurse surprised "This is blackmail!"
              mc "Yes, and I've got nothing to lose... you do."
              mc "Do you know how easy it is to print up a few of these?"
              mc "Or just report you to [jo]?"
              nurse afraid "Please! I'll be ruined!"
              mc "You'll be all right... just don't do anything stupid."
              nurse annoyed "Like what?"
              mc "Like snitching. No snitching!"
              nurse afraid "I won't say anything!"
              nurse afraid "Just please don't show this to anyone!"
              mc "You have my word."
              "This could all come crashing down on me, but I never took risks in the past."
              "Here's my chance to get something back from the place that took everything from me..."
              "Just got to be smart about this."
            "\"It's okay, I know how hard you work.\"":
              show nurse smile at move_to(.5)
              mc "It's okay, I know how hard you work."
              $nurse.lust-=1
              $nurse.love+=1
              nurse smile "I appreciate that. It won't happen again!"
              mc "And if it does, just make sure you lock the door first."
              nurse smile "Okay, I promise."
              "The [nurse] seems relieved, but there's something else in her eyes."
              "Disappointment...? Nah, that can't be — must be my imagination."
        "\"I'm so sorry! I should've knocked!\"":
          show nurse afraid at move_to(.5)
          mc "I'm so sorry! I should've knocked!"
          nurse afraid "Oh my lord! This can't be happening..."
          mc "Hey, it could've been worse... at least you look great."
          nurse smile "Do you really think so?"
          nurse annoyed "Err, I mean... this is widely inappropriate!"
          mc "Maybe what you were doing. I'm just complimenting you."
          nurse neutral "This is the most embarrassing moment of my life..."
          mc "Don't beat yourself up. It's really not a big deal."
          nurse neutral "Don't tell anyone, okay?"
          mc "My lips are sealed!"
        "\"Smile for the camera!\"":
          show nurse afraid at move_to(.5)
          mc "Smile for the camera!"
          show white with Dissolve(.15)
          play sound "camera_snap"
          hide white with Dissolve(.15)
          $mc.add_item("compromising_photo")
          $nurse.lust+=1
          nurse surprised "D-did you just...?"
          mc "You have a lot to lose, don't you?"
          nurse afraid "Oh, my... this... this can't get out!"
          mc  "If you follow my instructions, it won't."
          nurse afraid "Y-you're going to blackmail me?!"
          mc "We'll see, I guess..."
          mc "Maybe I'll just print up a bunch of these and post them all over the school."
          mc "Maybe I'll give the photo anonymously to the principal."
          nurse surprised "Oh, please! Please, you can't! My life will be over!"
          mc "Maybe I won't... it all depends on your actions."
          nurse annoyed "What do you mean?"
          mc "If you're quiet about it, no one will know. But if you decide to tell people..."
          nurse afraid "I'll keep quiet! I swear!"
          nurse afraid "Please, don't ruin me..."
          mc "Okay, that's good. We'll see what happens, then... for now, you're safe."
          mc "But if you do decide to tell people, just know that my life isn't worth anything, and the only loser in that scenario will be you."
          nurse afraid "I won't! I promise!"
          "This could all come crashing down on me, but I never took risks in the past."
          "Here's my chance to get something back from the place that took everything from me..."
          "Just got to be smart about this."
        "\"Relax... there's no shame in this,\nand I won't tell anyone.\"":
          show nurse afraid at move_to(.5)
          mc "Relax... there's no shame in this, and I won't tell anyone."
          nurse neutral "Really?"
          mc "Yes, really."
          "Getting caught masturbating is a very relatable fear..."
          "If it ever happened to me, I'd like it to be by someone kind and understanding."
          mc "Everyone needs to take the edge off... just lock the door next time."
          $nurse.lust-=1
          $nurse.love+=1
          nurse smile "I'm such an idiot... I'm so sorry you had to see it. I've been so stressed out lately..."
          mc "Don't worry about it!"
          "The [nurse] seems relieved, but there's something else in her eyes, as well..."
          "Disappointment...? Nah, that can't be — must be my imagination."
      if quest.nurse_photogenic == "eavesdrop":
        return
      jump isabelle_quest_isabelle_tour_nurse_blackmail
    "Make your presence known":
      $unlock_stat_perk("love12")
      $mc.love+=1
      mc "...Hello?"
      nurse "..."
      nurse "Just a moment!"
      nurse "..."
      $school_nurse_room["curtain_off"] = True
      $nurse.unequip("nurse_pantys")
      show nurse blush with Dissolve(.5)
      nurse "How can I help?"
      show nurse blush at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Where is Mr. Brown?\"":
          show nurse blush at move_to(.5)
          mc "Where is Mr. Brown?"
          nurse afraid "W-who?"
          mc "The English teacher. I thought I heard you say his name?"
          nurse annoyed "I, err... I think you must've misheard..."
          mc "Were you masturbating?"
          nurse surprised "Absolutely not! What an outrageous thing to say!"
          "Getting caught masturbating is a very relatable fear..."
          "If it ever happened to me, I'd like it to be by someone kind and understanding."
          mc "Everyone needs to take the edge off... just lock the door next time."
          $nurse.love+=1
          nurse smile "I... I didn't, but thanks... that's very mature of you..."
          mc "Don't worry about it!"
          if quest.nurse_photogenic == "eavesdrop":
            return
          mc "Right, I almost forgot. [lindsey] had an accident in the gym."
          nurse surprised "My goodness! Let's go immediately!"
          window hide
          show nurse surprised at disappear_to_right
          pause 0.75
          window auto
          "Looks like she forgot to hide the evidence... these might come in handy."
          $mc.add_item("nurse_panties")
          window hide
        "\"What were you doing behind there?\"":
          show nurse blush at move_to(.5)
          mc "What were you doing behind there?"
          nurse annoyed "N-nothing! Just preparing for the day."
          mc  "Sounded like very in-depth preparation..."
          nurse afraid  "Thanks! I... I like to—"
          mc "Oh, it did sound like you were enjoying yourself!"
          if quest.nurse_photogenic == "eavesdrop":
            return
          nurse annoyed "Err... was there anything you wanted?"
          mc "Yes, [lindsey] had an accident in the gym."
          nurse surprised "My goodness! Let's go immediately!"
          window hide
          show nurse surprised at disappear_to_right
          pause 0.75
          window auto
          "Looks like she forgot to hide the evidence... these might come in handy."
          $mc.add_item("nurse_panties")
          window hide
        "\"Sorry to intrude, but [lindsey] had an accident in the gym...\"" if not quest.nurse_photogenic == "eavesdrop":
          show nurse blush at move_to(.5)
          mc "Sorry to intrude, but [lindsey] had an accident in the gym..."
          nurse surprised "Oh, no! What happened?"
          mc "She ran into me. Hit her head quite hard."
          nurse neutral "Okay, let's hurry then!"
          nurse neutral "Gosh, I hope she's okay..."
          window hide
          show nurse neutral at disappear_to_right
          pause 0.75
          window auto
          "Hmm... the room smells a bit funny..."
          "Oh, well. Better go and make sure [lindsey]'s okay."
          window hide
      $quest.lindsey_nurse.advance("return_to_gym")
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 1.0
      jump isabelle_quest_isabelle_tour_nurse_lets_go_immediately
  return

label isabelle_quest_isabelle_tour_nurse_blackmail:
  mc "Right, I almost forgot. [lindsey] had an accident in the gym."
  nurse neutral "My goodness! Let me just..."
  $school_nurse_room["curtain_off"] = False
  hide nurse with Dissolve(.5)
  nurse "..."
  nurse "..."
  $school_nurse_room["curtain_off"] = True
  $nurse.equip("nurse_outfit")
  show nurse neutral with Dissolve(.5)
  mc "We should probably hurry, she hit her head quite hard..."
  nurse neutral "Gosh, I hope she's okay."
  window hide
  show nurse neutral at disappear_to_right
  pause 0.5
  show black onlayer screens zorder 100 with Dissolve(.5)
  $quest.lindsey_nurse.advance("return_to_gym")
  pause 1.0
  label isabelle_quest_isabelle_tour_nurse_lets_go_immediately:
  $game.location = school_gym
  hide black onlayer screens
  show nurse concerned at left
  show lindsey lindseypose01 behind nurse
  show isabelle concerned at right
  with Dissolve(.5)
  window auto
  nurse concerned "Goodness gracious, [lindsey]!"
  lindsey lindseypose01 "It's not the knee this time, I think..."
  isabelle concerned_left "No, it's way worse! She's been talking all sorts of nonsense while you were gone."
  isabelle concerned_left "It's like the collision made her drunk... is that common?"
  nurse thinking "I don't know... I don't think it is..."
  nurse concerned "Can you help me get her up?"
  isabelle neutral "Of course!"
  show lindsey skeptical with fadehold
  lindsey skeptical "Why is the floor moving...?"
  nurse thinking "I think you have a concussion, but I'll need to take you to my office for more tests."
  lindsey laughing "Do I get a lollipop?"
  nurse blush "I suppose that could be arranged..."
  nurse blush "Thank you, [mc]. I've got it from here."
  $item_count = mc.owned_item_count("compromising_photo")
  menu(side="middle"):
    extend ""
    "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"I'll be in touch.\"":
      mc "I'll be in touch."
      nurse afraid "W-what?"
      mc "About... you know."
      nurse annoyed "But I thought—"
      mc "We can discuss it now if you want! I don't think anyone would mind."
      $nurse.lust+=1
      nurse afraid "No! That's okay!"
      mc "Okay, then. Don't worry too much about it now... just tend to [lindsey]."
      nurse neutral "Right... fine..."
      nurse smile "Okay, [lindsey], come with me."
      $lindsey["at_none_now"] = True
      window hide
      show nurse smile at disappear_to_left
      show lindsey laughing at disappear_to_left
      pause 0.5
      show isabelle neutral at move_to(.5,1.0)
      pause 0.5
      show isabelle concerned_left with Dissolve(.5)
      window auto
    "\"I've always wanted to help out those\nin need. Glad I got the chance!\"":
      mc "I've always wanted to help out those in need. Glad I got the chance!"
      $isabelle.lust+=1
      isabelle excited "That's a great goal to have!"
      $lindsey.love+=1
      lindsey smile "Thanks again! Sorry for running into you!"
      lindsey flirty "I hope it didn't hurt too much..."
      mc "I'm fine! Never been better."
      lindsey laughing "That's nice... I'll see you around, [mc]!"
      nurse blush "Okay, let's go."
      $lindsey["at_none_now"] = True
      window hide
      show nurse blush at disappear_to_left
      show lindsey laughing at disappear_to_left
      pause 0.5
      show isabelle excited at move_to(.5,1)
      pause 0.5
      window auto
      "Somehow, being positive feels good."
      "Even though it's mostly forced and for show, everyone else seems to appreciate it..."
      "Maybe that's worth it?"
    "\"I demand a kiss as payment!\nThat's what all heroes get!\"":
      mc "I demand a kiss as payment! That's what all heroes get!"
      $nurse.lust+=1
      $lindsey.love-=1
      $isabelle.love-=1
      show nurse concerned
      show lindsey skeptical
      isabelle skeptical "Running to get the [nurse] hardly qualifies as heroic..."
      isabelle skeptical "Besides, heroes don't demand payment."
      lindsey skeptical "Thanks for getting me help, but that's really not how it works..."
      "Ugh, girls are the worst... so entitled to everything, yet never willing to pay up!"
      "Ridiculous, when you think about it. The nicer you are, the less they like you..."
      nurse concerned "Okay, [lindsey], come with me."
      $lindsey["at_none_now"] = True
      window hide
      show nurse concerned at disappear_to_left
      show lindsey skeptical at disappear_to_left
      pause 0.5
      show isabelle skeptical at move_to(.5,1)
      pause 0.5
      show isabelle concerned_left with Dissolve(.5)
      window auto
    "\"It's my fault she's hurt... fetching you\nwas the least I could do.\"":
      mc "It's my fault she's hurt... fetching you was the least I could do."
      $nurse.love+=1
      nurse blush "That's a good attitude to have."
      $isabelle.love+=1
      isabelle excited "Yes, I like that! If everyone was more like you, the world would be a better place."
      "That's probably a stretch and a half... I can hardly stand my reflection."
      "Feels kinda nice that they noticed it, though! Trying to be a decent human being might not be so bad after all."
      lindsey skeptical "I think I might pass out..."
      nurse concerned "It's okay, there's a bed in my office... lean on me."
      $lindsey["at_none_now"] = True
      window hide
      show nurse concerned at disappear_to_left
      show lindsey skeptical at disappear_to_left
      pause 0.5
      show isabelle excited at move_to(.5,1)
      pause 0.5
      show isabelle concerned_left with Dissolve(.5)
      window auto
  isabelle concerned_left "I hope she'll be okay..."
  mc "I'm sure she'll be fine."
  isabelle excited "Oh, man! This tour turned out way more eventful than I expected!"
  isabelle excited "I think I need to sit down for a bit... let's go to the cafeteria."
  mc "All right."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_tour.advance("cafeteria")
  $quest.lindsey_nurse.finish()
  return

label isabelle_quest_isabelle_tour_carry_to_nurse:
  $mc["turn_off_location_transitions"] = False
  $quest.lindsey_nurse["carried"] = True
  hide black
  show nurse concerned at Position(xalign=.25)
  show isabelle sad at Position(xalign=.75)
  show lindsey skeptical at Position(xalign=.95)
  with Dissolve(.5)
  nurse concerned "My, oh my! Please, put her on the bed..."
  $school_nurse_room["curtain_off"] = True
  pause(1)
  $lindsey.unequip("lindsey_shirt")
  pause(1)
  hide isabelle
  show nurse clinic_nurse_angry
  show lindsey clinic_lindsey_neutral
  with Dissolve(.5)
  nurse "How are you feeling, [lindsey]?"
  lindsey clinic_lindsey_annoyed "D-dizzy... my head hurts..."
  nurse clinic_nurse_angry "How did this happen?"
  menu(side="far_right"):
    extend ""
    "\"She tried to mow me down, but\ndidn't realize I'm made of steel.\"":
      $mc.strength+=1
      mc "She tried to mow me down, but didn't realize I'm made of steel."
      $lindsey.lust+=1
      lindsey clinic_lindsey_laughing "Not true! I just didn't see you!"
      $nurse.love-=1
      nurse clinic_nurse_confused "I get the feeling this wasn't a car accident..."
      mc "No, but the result would probably have been the same."
      $isabelle.lust-=1
      show isabelle eyeroll at appear_from_right("right")
      isabelle "Right. Since you're all muscle, there aren't any brain cells to lose."
      lindsey clinic_lindsey_angry "Nothing wrong with staying fit!"
      show isabelle eyeroll at disappear_to_right
    "\"Frontal collision at about\neighty miles an hour.\"":
      $mc.charisma+=1
      mc "Frontal collision at about eighty miles an hour."
      $nurse.love-=1
      nurse clinic_nurse_confused "I get the feeling this wasn't a car accident..."
      mc "No, but she was going way over the speed limit."
      $lindsey.love+=1
      lindsey clinic_lindsey_blush "Awww, I never knew you were so charming before!"
      lindsey clinic_lindsey_blush "In fact, we've never really talked much at all..."
    "\"She was doing laps in the gym and accidentally ran into me... she hit\nher head quite hard.\"":
      mc "She was doing laps in the gym and accidentally ran into me... she hit her head quite hard."
      nurse clinic_nurse_confused "Accidents happen. Good job bringing her here so fast."
      menu(side="far_right"):
        extend ""
        "\"It's my duty to keep the people safe!\"":
          mc "It's my duty to keep the people safe!"
          show isabelle blush at appear_from_right("right")
          $isabelle.lust+=1
          isabelle blush "That's an admirable sentiment."
          show isabelle blush at disappear_to_right
        "\"It was my fault... and she looked like\nshe needed my help getting here.\"":
          mc "It was my fault... and she looked like she needed my help getting here."
          lindsey clinic_lindsey_smile "It was both our faults!"
          $lindsey.lust+=1
          lindsey clinic_lindsey_blush "But, yes, I could hardly walk... thank you."
        "Let [isabelle] explain":
          show isabelle sad at appear_from_right("right")
          $lindsey.lust-=1
          isabelle sad "They ran into each other... [lindsey] got the worst of it."
          nurse clinic_nurse_confused "Very well. Good that you got her here so fast..."
          $isabelle.love+=1
          isabelle blush "That's the least we could do! [mc] seemed more than happy to help out."
          nurse clinic_nurse_confused "[lindsey] is lucky to have such considerate classmates..."
          nurse clinic_nurse_confused "I'm sure she's thankful even though she's a bit out of it right now."
          show isabelle blush at disappear_to_right
  nurse clinic_nurse_confused "Okay, [lindsey]. I'm going to check you for a concussion... does anywhere else hurt?"
  lindsey clinic_lindsey_smile "My head and my chest..."
  nurse clinic_nurse_confused "All right, you guys can wait outside. Thanks for your help!"
  show isabelle neutral at appear_from_right("right")
  isabelle neutral "No problem! [mc], meet me at the cafeteria."
  $lindsey["at_none_now"] = True
  $school_nurse_room["curtain_off"] = False
  hide isabelle
  hide nurse
  hide lindsey
  with Dissolve(.5)
  if game.quest_guide == "isabelle_tour":
    $game.quest_guide = "lindsey_nurse"
    $quest.lindsey_nurse["revert_quest_guide"] = True
  $quest.lindsey_nurse.advance("nurse_room")#this phase is used for in-room peeking scene, finish this quest when leaving thru door
  $quest.isabelle_tour.advance("cafeteria")
  return

label isabelle_quest_isabelle_tour_peak_lindsey:
  nurse "It looks like you have a concussion, [lindsey], but it's not severe."
  lindsey  "Oh, no! How long 'til I can run again?"
  nurse "It can take a while, you need to be careful..."
  lindsey  "What about the District Marathon?"
  nurse "If you rest well, you'll be back on your feet long before that."
  nurse "I'd like to look at your chest now. So, if you could take off your top, please..."
  "This could be my only chance to see a pair of real-life boobs!"
  "But it might ruin my chances with [lindsey] for good... and I'd likely get detention..."
  menu(side="left"):
    extend ""
    "Pull back the curtain":
      $unlock_replay("lindsey_exam")
      $unlock_stat_perk("lust13")
      show nurse clinic_nurse_confused
      show lindsey clinic_lindsey_neutral
      with Dissolve(.5)
      nurse "Okay, let's get that bra off so I can listen to your heart..."
      lindsey clinic_lindsey_neutral_topless "Okay."
      nurse clinic_nurse_surprised "Huh?"
      lindsey clinic_lindsey_scared_topless "Aaah!"
      "Some prophets may call this a heavy risk, but there it finally is — the prize."
      "People would kill to see her tits. There's a big chance that no man has ever seen her naked..."
      "That's what makes this so special."
      "Judging by the tan lines, even the sun's been deprived of the soft flesh of her chest."
      "The unblemished snow-white skin — a sharp but enticing contrast to the soft pink of her areolas."
      "Some girls sunbathe to get an even tan, but [lindsey] clearly doesn't care about that."
      "As long as her sports bra keeps her boobs from bouncing!"
      "If only she'd allow me to reach out, weigh them in my hands, squeeze them gently..."
      "Or roughly... whatever, she prefers! Twist her nipples... or give them kind and gentle kisses..."
      $mc.lust+=1
      "There are so many possibilities! Play with them! Nibble them! Suck them!"
      "So soft and supple — so forbidden — joy-sized perfection!"
      nurse clinic_nurse_surprised "What in god's name?!"
      menu(side="far_right"):
        extend ""
        "\"Just claiming my reward for helping you.\"":
          mc "Just claiming my reward for helping you."
          $lindsey.love-=1
          $lindsey.lust-=1
          lindsey clinic_lindsey_angry_topless "That's sick! Go away!"
          $nurse.love-=1
          $nurse.lust-=1
          nurse clinic_nurse_angry "That is outrageous, [mc]!"
          nurse "Wait until [jo] hears about this..."
          $mc["detention"]+=1
          $lindsey["romance_disabled"] = True
          $game.notify_modal(None,"Love or Lust","Romance with [lindsey]:\nDisabled.",wait=5.0)
        "\"I'm sorry, but I figured I'd only have one chance at seeing an angel the way god made her...\"":
          mc "I'm sorry, but I figured I'd only have one chance at seeing an angel the way god made her..."
          $lindsey.love+=1
          lindsey clinic_lindsey_blush_topless  "That's oddly nice..."
          $nurse.love-=1
          nurse clinic_nurse_angry "No, it's inappropriate, and I'm reporting it!"
          nurse clinic_nurse_angry "Get out of here, [mc]."
          $mc["detention"]+=1
        "?mc.charisma>=3@[mc.charisma]/3|{image=stats cha}|\"I was going to see them\nsooner or later, anyway.\"":
          mc "I was going to see them sooner or later, anyway."
          $lindsey.lust+=1
          lindsey clinic_lindsey_laughing_topless "Oh my god!"
          mc "I agree... two-fold."
          lindsey clinic_lindsey_blush_topless "Oh my god!"
          $nurse.lust+=1
          nurse clinic_nurse_confused "Young love, huh? Okay, pull back the curtain now."
          mc "Who said anything about love?"
          lindsey clinic_lindsey_annoyed_topless "Oh my god!"
      hide nurse
      hide lindsey
      with Dissolve(.5)
    "Don't be a creep":
      $unlock_stat_perk("love13")
      $mc.love+=2
      "Old me would've jumped at the opportunity, but this is my chance to be better. To learn from my mistakes."
      "Someone once told me that being decent goes a long way..."
      "It always seemed like good advice."
      lindsey "Don't tell him, but [mc] made me feel really cared for..."
      nurse "He did look concerned when he brought you in..."
      nurse "Okay, take a deep breath."
      lindsey "..."
      nurse "Your chest looks fine. You do have an elevated heart rate, but I don't think it's related..."
      lindsey "Heh... probably not!"
  if not school_nurse_room["curtain_off"]:
    if not quest.lindsey_nurse=="fetch_the_nurse":
      $school_nurse_room["curtain_off"] = True
  $quest.lindsey_nurse.finish()
  return

label isabelle_quest_isabelle_tour_cafeteria:
  show isabelle excited with Dissolve(.5)
  isabelle excited "All right, so I've got my three focus classes down... that's a relief."
  isabelle neutral "I think I'm going to get something to drink."
  mc "The drinks here aren't really conventional, apart from the strawberry juice."
  isabelle concerned_left "Yes... I can see that. Salted cola? Pepelepsi?"
  mc "Yes, they're a bit off-brand, but the strawberry juice is a godsend."
  isabelle excited "What about ice tea? That's my favorite!"
  if quest.kate_over_isabelle.in_progress:
    "Great. That was easier than expected..."
    "English, Art, and Gym are her focus classes, and ice tea is her favorite drink."
    "Hopefully, [kate] will have a reward ready for me!"
    $achievement.kates_spy.unlock()
    $quest.kate_over_isabelle.advance("report")
  else:
    "[isabelle] seems way too relaxed regarding [kate]. She could strike at any moment..."
    mc  "What are you going to do about [kate]?"
    isabelle concerned "You mean \"we,\" right?"
    mc "Fine... what are we going to do about [kate]?"
    isabelle concerned_left "We need a plan first..."
    isabelle concerned_left "What does she care about the most?"
    mc "Status, I guess, and her looks."
    isabelle excited "That makes things a lot easier."
    mc "What do you have in mind?"
    isabelle skeptical "We're going to teach her a lesson, once and for all."
    isabelle neutral "Do you trust me?"
    show isabelle neutral at move_to(.75)
    menu(side="left"):
      extend ""
      "\"Wholeheartedly!\"":
        show isabelle neutral at move_to(.5)
        mc "Wholeheartedly!"
        $isabelle.love+=1
        isabelle excited "Great! Do you know of Carrie? The book, I mean."
        mc "I don't read much, to be honest..."
        isabelle neutral "That's okay!"
        isabelle neutral "Here's what we'll need: a bucket of red paint, a fishing line, a hook, a pig mask, and whoever is in charge of the school's newspaper."
        mc "That would be [maxine]."
        isabelle excited "Okay, good! Do you think you can get the items and arrange a meeting with her?"
        mc "I'll do my best... I've always wanted to see [kate] fall."
        isabelle skeptical "People like her have to learn the hard way."
        mc "I think you're right."
      "\"Maybe, I don't know...\"":
        show isabelle neutral at move_to(.5)
        mc "Maybe, I don't know..."
        isabelle neutral "We've only just met, so I get that hesitation."
        isabelle concerned_left "[kate] seems to be quite popular, so going after her doesn't come without risks."
        mc "Right. It might not be the smartest thing..."
        isabelle concerned "Sometimes you have to face your fears to make changes."
        isabelle neutral "But in this case, you don't have to face them alone... so what do you say?"
        "Suffering through a whole year of bullying is something I've done before."
        "Going through it a second time — fuck that... but I know I'll survive."
        "Trying to stop [kate] might make it worse, though."
        mc "I'm done being afraid."
        $isabelle.lust+=1
        isabelle excited "That's the spirit!"
        "Living with regrets sucks, and this is one of those things that I wish I'd had the courage to do last time around."
        mc "Right. So, what's the plan?"
        isabelle concerned "Do you know of Carrie? The book, I mean."
        mc "I don't read much, to be honest..."
        isabelle neutral "That's okay!"
        isabelle neutral "Here's what we'll need: a bucket of red paint, a fishing line, a hook, a pig mask, and whoever is in charge of the school's newspaper."
        mc "That would be [maxine]."
        isabelle excited "Okay, good! Do you think you can get the items and arrange a meeting with her?"
        mc "I'll do my best."
        isabelle excited "Great! I'll fill you in on the details once you've got the items."
        mc "That works for me."
      "\"Not really, no.\"":
        show isabelle neutral at move_to(.5)
        mc "Not really, no."
        $isabelle.love-=1
        isabelle concerned_left "I see..."
        isabelle concerned "Do you think we can work together anyway?"
        mc "I guess. We both want to see a bully fall."
        $isabelle.lust+=1
        isabelle excited "You're right! Allies of convenience, then."
        mc "Right... so, what's the plan?"
        isabelle concerned "Do you know of Carrie? The book, I mean."
        mc "I don't read much, to be honest..."
        isabelle neutral "That's okay!"
        isabelle neutral "Here's what we'll need: a bucket of red paint, a fishing line, a hook, a pig mask, and whoever is in charge of the school's newspaper."
        mc "That would be [maxine]."
        isabelle excited "Okay, good! Do you think you can get the items and arrange a meeting with her?"
        mc "I'll do my best."
        isabelle excited "Great! I'll fill you in on the details once you've got the items."
        mc "That works for me."
    $quest.isabelle_over_kate.advance("search_and_schedule")
#   $quest.isabelle_over_kate.finish(silent=True)
#   $game.notify_modal(None,"Coming soon","{image=isabelle contact_icon}{space=25}|"+"You have reached the end of\nDethroning the Queen\nfor now!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image= items monkey_wrench}",5.0)
    mc "Anyway, where were we?"
    isabelle neutral "Ice tea?"
    mc "Right."
  mc "They only have regular tea, and you'll have to ask for hot water in the kitchen."
  isabelle concerned_left "Damn it... I was craving something cold. I guess I'll have strawberry juice, then."
  mc "A very fine choice!"
  isabelle excited "Why do you talk like that sometimes?"
  show isabelle excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I must've been a butler in a previous life.\"":
      show isabelle excited at move_to(.5)
      $mc.charisma+=1
      mc "I must've been a butler in a previous life."
      isabelle flirty "Did you forget your tuxedo and white gloves back home?"
      mc "I said \"previous life.\" You don't keep your clothes... unless you were buried with them."
      isabelle afraid "Don't tell me you were buried, err..."
      mc "Nude as the day I was born!"
      isabelle cringe "Oh god, how did we get here?"
      isabelle cringe "I never thought I'd think of naked dead butlers..."
      mc "You're welcome."
      isabelle thinking "Ugh, let's change the topic."
    "\"Just trying to make you feel at home.\"":
      show isabelle excited at move_to(.5)
      mc "Just trying to make you feel at home."
      isabelle excited "Because all English people have a personal butler?"
      show isabelle excited at move_to(.75)
      menu(side="left"):
        extend ""
        "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"I thought you were Scottish?\"":
          show isabelle excited at move_to(.5)
          mc "I thought you were Scottish?"
          isabelle blush "I wish..."
          mc "Same! There's just something special about a hot-blooded Scottish lass..."
          isabelle annoyed_left "Pfft!"
          isabelle annoyed_left "The English aren't so bad..."
          mc "Imagine if William Wallace could hear you now..."
          $isabelle.lust+=1
          $isabelle.love+=1
          isabelle blush "Shh! Don't tell him!"
        "\"Only the posh ones.\"":
          show isabelle excited at move_to(.5)
          mc "Only the posh ones."
          mc "The commoners have to settle for American students."
          isabelle confident "I might have a butler back home, you don't know that!"
          mc "Maybe you do, but he's not here right now... nobody has to know."
          isabelle laughing "Sorry, but I would never cheat on him!"
          mc "You're loyal... I like that in a girl."
          isabelle confident "Is that right?"
          mc "When you find the one, you just know. Truly good service is worth waiting for."
          $isabelle.love+=1
          isabelle laughing "Right? That's what I think too!"
        "\"I've seen the movies!\"":
          show isabelle excited at move_to(.5)
          mc "I've seen the movies!"
          isabelle confident "Is that right?"
          mc "Yes, and when they lift the cover there's a hole in the platter..."
          mc "And they say, \"Would the lady care for some spotted dick?\""
          $isabelle.love-=1
          $isabelle.lust+=1
          isabelle laughing "Ew! Those weren't the movies I had in mind!"
    "\"Like what?\"":
      show isabelle excited at move_to(.5)
      mc "Like what?"
      isabelle neutral "Overly polite and old-fashioned."
      show isabelle neutral at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I thought you liked polite...\"":
          show isabelle neutral at move_to(.5)
          mc "I thought you liked polite..."
          isabelle smile "You're right! I do like that."
          isabelle eyeroll "I'm just used to Americans being loud and rude..."
          mc "The loud ones get noticed. They're bad PR for the rest of us."
          isabelle smile "That's probably true for a lot of things."
          isabelle smile_left "The ones screaming the loudest don't necessarily represent the majority."
          mc "I don't mind screamers. I don't mind criers either."
          isabelle laughing "I feel like this conversation just took a whole new direction..."
          $mc.charisma+=1
          mc "A better direction!"
          isabelle laughing "I don't know about that!"
        "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"'Tis but how I prattle!\"":
          show isabelle neutral at move_to(.5)
          mc "'Tis but how I prattle!"
          isabelle laughing "You're weird, you know that?"
          "Somehow, my joke landed with her... that's what's weird here."
          "[isabelle] is so easy to talk to. Maybe she's more forgiving than other girls?"
          "Or perhaps my social skills have improved?"
          mc "Maybe I'm the normal one? Have you thought about that?"
          $isabelle.love+=1
          isabelle confident "Nope, definitely weird! But not in a bad way."
          isabelle confident "What would life be like if everyone was normal?"
          mc "Monotone, I guess..."
          isabelle laughing "Boring, that's for sure!"
        "\"Remind me to be ruder.\"":
          show isabelle neutral at move_to(.5)
          mc "Remind me to be ruder."
          isabelle laughing "That's not what I meant!"
          mc "Are you sure? Some girls like it rude."
          isabelle eyeroll "Really, now?"
          mc "I meant \"rough.\" Some girls like it rough."
          $isabelle.lust-=1
          isabelle eyeroll "Way to ruin a joke."
          mc "I thought you didn't get it..."
          isabelle confident "You're just digging yourself deeper, mate."
          "Damn it... why is it so hard to flirt with girls?"
    "\"Why do you talk like you do all the time?\"":
      show isabelle excited at move_to(.5)
      mc "Why do you talk like you do all the time?"
      isabelle afraid "Whoa! I wasn't being judgemental or anything."
      isabelle cringe "Way to make something out of nothing..."
      $isabelle.love-=1
      isabelle thinking "And for the record, my accent is the original English accent."
      "That was perhaps a bit snappy..."
      "But when you're used to people poking fun at every little thing you do..."
      show isabelle thinking at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Sorry... I overreacted...\"":
          show isabelle thinking at move_to(.5)
          mc "Sorry... I overreacted..."
          isabelle eyeroll "You think so?"
          mc "Sometimes I have a hard time reading the situation right... especially with new people."
          isabelle smile "Ah, okay! Don't worry about it. No harm done."
          "[isabelle] is so nice. How is she so nice? Why do I always screw things up?"
          mc "Thanks... I'm not sure what to say... I'm sorry for snapping."
          $isabelle.love+=1
          isabelle laughing "You don't have to say anything, but I appreciate the apology!"
        "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"That's actually incorrect!\"":
          show isabelle thinking at move_to(.5)
          mc "That's actually incorrect!"
          mc "During the Industrial Revolution, many lower-class British people found themselves with a great deal of money, but a voice that instantly marked them as a commoner..."
          mc "In order to distinguish themselves from their lowly roots, they developed their own posh way of speaking."
          mc "Eventually, their way of talking caught on with the entire country."
          mc "The original English accent sounded a lot more like today's American."
          $isabelle.love+=1
          isabelle flirty "I did not know that!"
          isabelle flirty "That's pretty cool, and it makes sense since the pilgrims left to colonize America before the revolution."
        "\"Don't sass me, woman. I could snap you like a twig.\"":
          show isabelle thinking at move_to(.5)
          $mc.strength+=1
          mc "Don't sass me, woman. I could snap you like a twig."
          $isabelle.love-=1
          $isabelle.lust-=1
          isabelle afraid "Seriously?!"
          isabelle cringe "Sorry if I hurt your feelings... I just found your way of talking funny there."
          mc "You can't hurt me! My feelings are as firm as my abs."
          isabelle thinking "I'm sure they are, Popeye..."
  isabelle neutral "All right, I think I'm going to check out the surrounding area outside now... I appreciate the tour."
  mc "There's another floor as well that you haven't seen yet."
  isabelle concerned_left "I'll have a look later. I just need some fresh air right now..."
  "It feels like [isabelle] is trying to get rid of me, which isn't all that strange. By tomorrow, she'll have moved on to more interesting people."
  "Staying relevant might be the play here, but nobody likes a clinger..."
  show isabelle concerned_left at move_to(.25)
  menu(side="right"):
    extend ""
    "Play it cool":
      show isabelle concerned_left at move_to(.5)
      mc "All right, I guess I'll see you around."
      if isabelle.lust>=4:
        isabelle smile "Don't look so disappointed! I'm not leaving for good."
        "Crap. So much for playing it cool..."
        mc "I didn't think that..."
        isabelle laughing "Come on, I'm just taking the piss."
        mc "You're doing what, now?"
        isabelle confident "It means I'm messing with you."
        isabelle confident "Here, let me put my number in your phone and we can hang out sometime?"
        "The what now? Was that...  did she just... wait, what's happening?"
        "Not losing face... that's the goal. Already took one risk, might as well take another..."
        mc "Sorry, my contact list is full."
        isabelle eyeroll "Really?"
        mc "No, I'm just taking the piss."
        isabelle laughing "I probably deserved that..."
        isabelle confident "Okay, hand it over!"
        "Even though it means very little, watching her fingers tapping away at the screen is a dream come true..."
        "It doesn't mean that we'll sleep together or that she has a crush on me, but it does mean that I'm not repulsive to her."
        "That's a victory in my book."
        $mc.add_phone_contact(isabelle)
        isabelle neutral "Okay, done!"
        isabelle excited "See you around, [mc]!"
        show isabelle excited at disappear_to_right
      else:
        isabelle excited "Okay, see you around, [mc]!"
        show isabelle excited at disappear_to_right
        "Not sure if that worked. Maybe she's just gone now..."
        "On the plus side, no rejection."
    "Don't let her slip away":
      show isabelle concerned_left at move_to(.5)
      if isabelle.love>=4:
        mc "H-hey, would you like to hang out sometime...?"
        isabelle excited "Sure! I just need to clear my head right now... this [kate] thing has me all riled up."
        mc "Yeah, I get that..."
        isabelle neutral "If you want, I can put my number in your phone?"
        "Her number... in my phone?!"
        "Is there a replay function in this version of the Matrix because this doesn't seem like real life..."
        mc "W-what?"
        isabelle blush "You know, if you want to hang out?"
        "It's a hundred percent platonic. No reason to freak out."
        "No need to throw your hand up in triumph or run a victory lap around the school."
        mc "R-right! Sounds good! Here you go..."
        "Even though it means very little, watching her fingers tapping away at the screen is a dream come true..."
        "It doesn't mean that we'll sleep together or that she has a crush on me, but it does mean that I'm not repulsive to her."
        "That's a victory in my book."
        $mc.add_phone_contact(isabelle)
        isabelle neutral "Okay, done!"
        isabelle excited "See you around, [mc]!"
        show isabelle excited at disappear_to_right
      else:
        mc "I could show you the top floor too, and the computer room!"
        mc "The computers are program-locked so you can't install anything, but I know a way around it..."
        isabelle neutral "Thanks, but I think I can find my way around on my own now."
        isabelle excited "See you around, [mc]!"
        show isabelle excited at disappear_to_right
        "God, that was stupid... she's way out of my league, and now she thinks I'm desperate..."
  $quest.isabelle_tour.finish()
  $isabelle["at_none_now"] = True
  return
