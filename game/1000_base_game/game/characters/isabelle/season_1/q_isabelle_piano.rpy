screen music_notes:
  default note_visible = True
  default note_color = renpy.random.choice(["red_note","green_note","orange_note","blue_note","yellow_note"])
  default note_position = renpy.random.choice([328,408,511,612,689,777,881,955,1057,1130,1236,1305,1413,1492])

  fixed:
    if note_visible:
      at transform:
        parallel:
          alpha 0.0
          easein 0.25 alpha 1.0
          delay 0.25
          easeout 0.25 alpha 0.0
        parallel:
          xpos note_position ypos 879
          easein 0.5 yoffset -50
      add "misc piano_play " + note_color
      timer 0.5 action SetScreenVariable("note_visible",False)
    else:
      timer 0.25 action Show("music_notes")

  fixed:
    if not preferences.get_volume("music"):
      at transform:
        on show:
          xalign 0.5 yalign 0.5
          easein 0.15 zoom 1.1
          easeout 0.1 zoom 1.0
      frame style "notification_modal_title_frame" xalign 0.5 yalign 0.5:
        text "Your music volume is OFF" style "notification_modal_title"


label quest_isabelle_piano_start:
  stop music
  window hide
  show misc piano_play bg with Dissolve(.5)
  pause 1.0
  play music "sweet_and_romantic"
  show screen music_notes
  pause 33.0
  play music "school_theme" fadein 0.5
  hide screen music_notes
  hide misc piano_play bg
  show isabelle excited
  with Dissolve(.5)
  window auto
  isabelle excited "I had no idea you play the piano!"
  show isabelle excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I started recently.\"":
      show isabelle excited at move_to(.5)
      mc "I started recently."
      isabelle excited "You're definitely getting good!"
      mc "Thank you! I feel like I'm finally starting to get the hang of it..."
      isabelle neutral "Maybe this is your true calling?"
      mc "I don't know about that, but I do enjoy it."
      mc "What is your favorite classical piece?"
      isabelle concerned_left "It's hard to name just one... but I think the nocturnes are magical."
      mc "Didn't take you for a Chopin-chick."
      isabelle skeptical "What did you think?"
      mc "Maybe a Beethoven-broad?"
      isabelle confident "And what do you call a girl who is into Mozart?"
      mc "Don't be ridiculous. Girls don't like Mozart. "
      $isabelle.love+=1
      isabelle laughing "Hah! You're funny!"
      mc "Thanks! You're not so bad yourself."
    "\"It comes naturally to me... I'm very good with my fingers.\"":
      show isabelle excited at move_to(.5)
      $mc.charisma+=1
      mc "It comes naturally to me... I'm very good with my fingers."
      isabelle neutral "Is that right?"
      mc "If you're lucky, I might show you sometime."
      isabelle concerned "I thought that's what you just did?"
      mc "You haven't seen half of it yet..."
      mc "I'm very experienced at pushing the right buttons and keys."
      isabelle skeptical "Relax, Mozart. Women are nothing like keyboards."
      mc "Women? I don't know what you're inferring."
      isabelle skeptical "Sure, you don't..."
    "\"I had no idea you like music!\"":
      show isabelle excited at move_to(.5)
      mc "I had no idea you like music!"
      isabelle skeptical "That's a bit stand-offish, don't you think?"
      isabelle skeptical "Who doesn't like music?"
      show isabelle skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "\"Rap fans.\"":
          show isabelle skeptical at move_to(.5)
          $mc.intellect+=1
          $mc.strength-=1
          mc "Rap fans."
          mc "Talking to a beat isn't music."
          isabelle confident "What would you call it, then?"
          mc "Rappers are basically poets with a bad attitude."
          isabelle laughing "That is somehow accurate!"
          mc "I always imagine Shakespeare with a ton of bling, rapping about quids, tobacco, and Lady Macbeth's booty."
          isabelle confident "I'd love to see that!"
        "\"Radio pop fans.\"":
          show isabelle skeptical at move_to(.5)
          mc "Radio pop fans."
          $isabelle.lust+=1
          isabelle blush "I suppose that's fair!"
          isabelle annoyed "It represents everything I'm against."
          mc "Which is?"
          isabelle angry "The corporate music industry. The sexualization of women. The death of creativity."
          isabelle angry "Art shouldn't be factory-made."
          mc "I agree. At least about the last part."
          isabelle annoyed "So, you think the sexualization of women is fine?"
          mc "I plead the 5th."
        "\"Bagpipe fans.\"":
          show isabelle skeptical at move_to(.5)
          $mc.charisma+=1
          mc "Bagpipe fans."
          $isabelle.lust-=1
          isabelle angry "How very dare you?!"
          mc "Yeah, I said it!"
          isabelle angry "That's... ugh!"
          isabelle sad "What is that that you don't like about it?"
          mc "It's loud and obnoxious."
          isabelle laughing "Shouldn't Americans love it, then?"
          isabelle laughing "It's the sound of the Scottish highland! The sound of freedom!"
          mc "Why do you like Scotland so much? Doesn't the average English person dislike Scotland?"
          isabelle confident "I'm no stereotype... but also, I like what Scotland represents."
          mc "I guess I can respect that."
  isabelle neutral "So, what are your thoughts on classical music in general?"
  mc "I guess it's okay. I'm starting to appreciate it more and more."
  isabelle excited "I've always been a fan! Nothing wrong with modern music, but there's something timeless about the classical pieces..."
  isabelle excited "Would you mind playing another song for me?"
  if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
    "This is going a bit off script, but perhaps this could be a good opportunity to earn her trust?"
    "[kate] did ask me to get closer to [isabelle], after all."
    "If only for the wrong reasons..."
  mc "Sure, why not?"
  mc "What would you like to hear?"
  isabelle blush "Something by Chopin!"
  mc "Hmm... I'd have to learn one..."
  isabelle blush "Would you do that for me?"
  mc "I guess? It might take some time, though."
  isabelle confident "That's fine!"
  isabelle confident "If you do learn one, please let me know. I'd love to hear it."
  mc "Sure thing."
  isabelle laughing "I can't wait! Thanks for the tune, [mc]! Talk to you later!"
  show isabelle laughing at disappear_to_right
  "Chopin pieces aren't the easiest. Most of them are way above music class difficulty, so there probably aren't any scores in the classroom..."
  "Luckily, there's always the internet and printers."
  if quest.isabelle_piano.in_progress:
    $quest.isabelle_piano.advance("computer", silent=True)
  else:
    $quest.isabelle_piano.start("computer")
  return

label quest_isabelle_piano_computer_wrong:
  "It's all about connections these days..."
  "Getting work. Getting laid. Getting stuff printed."
  return

label quest_isabelle_piano_computer:
  $school_computer_room["screen1"] = True
  "{i}Chopin... Chopin...{/}"
  "..."
  "Hmm..."
  "Nocturne, but which one?"
  "..."
  "All right, this one should do."
  "Now to print it out..."
  $school_computer_room["screen1"] = False
  $quest.isabelle_piano.advance("printer")
  return

label quest_isabelle_piano_printer_interact:
  "Huh?"
  "Looks like this thing isn't plugged in..."
  "Hmm... the power cable is missing."
  $quest.isabelle_piano["missing_cable"] = True
  return

label quest_isabelle_piano_printer_cable_box_take:
  "These cables all look old and broken. It's impossible to know if they'll work."
  "But hey, at least they're color-coded..."
  menu(side="middle"):
    extend ""
    "Take the {color=#C86055}red{/} cable" if not school_computer_room["red_cable_taken"]:
      $school_computer_room["red_cable_taken"] = True
      $mc.add_item("power_cable_red")
    "Take the {color=#70B3F8}blue{/} cable" if not school_computer_room["blue_cable_taken"]:
      $school_computer_room["blue_cable_taken"] = True
      $mc.add_item("power_cable_blue")
    "Take the {color=#F2C878}yellow{/} cable" if not school_computer_room["yellow_cable_taken"]:
      $school_computer_room["yellow_cable_taken"] = True
      $mc.add_item("power_cable_yellow")
    "Take the candle" if not school_computer_room["candle_taken"]:
      "The most interesting thing here is this fat candle. Imagine that."
      "Might as well keep it..."
      $school_computer_room["candle_taken"] = True
      $mc.add_item("candle")
  return

label quest_isabelle_piano_printer_cable_box_interact:
  "This box of useless cables. Nothing of value here."
  return

label quest_isabelle_piano_printer_use_item(item):
  if item == "power_cable_red":
    "It fits... but the printer apparently hates the color."
    $quest.isabelle_piano["red_cable_refused"] = True
    $quest.isabelle_piano.failed_item("printer",item)
  elif item == "power_cable_blue":
    $mc.remove_item("power_cable_blue")
    "Okay, great! Now it's working."
    "It even has ink. Fantastic!"
    "..."
    "..."
    $mc.add_item("chopin_music_score")
    if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
      "There we go. [isabelle] will be impressed."
      "And impressing someone is a great way to get them to lower\ntheir guard..."
    else:
      "Impressing girls is something new to me. It's a thrill. Especially with someone as sophisticated as [isabelle]."
      "She'll be excited... I just need to tell her about it without sounding too proud of myself."
    $quest.isabelle_piano.advance("isabelle")
  elif item == "power_cable_yellow":
    "It fits... but the printer apparently hates the color."
    $quest.isabelle_piano["yellow_cable_refused"] = True
    $quest.isabelle_piano.failed_item("printer",item)
  else:
    "Not the best fit for my [item.title_lower], but I know another good hole to plug..."
    $quest.isabelle_piano.failed_item("printer",item)
  return

label quest_isabelle_piano_isabelle:
  show isabelle neutral with Dissolve(.5)
  mc "Hey! Are you busy right now?"
  isabelle neutral "Not really. Why?"
  mc "I was just wondering..."
  show isabelle neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.charisma>=5@[mc.charisma]/5|{image= stats cha}|\"Is your name Elise? Because I have a song für you.\"":
      show isabelle neutral at move_to(.5)
      mc "Is your name Elise? Because I have a song für you."
      $isabelle.love+=1
      $isabelle.lust+=1
      isabelle confident "Smooth."
      mc "You should see my Bagatelle in E-flat major..."
      isabelle laughing "What does that even mean?"
      mc "Uh, I was hoping you wouldn't ask."
      mc "Anyway! Are you ready for Chopin?"
      isabelle blush "Yes! Lead the way!"
    "\"Are you ready to hear some Chopin?\"":
      show isabelle neutral at move_to(.5)
      mc "Are you ready to hear some Chopin?"
      isabelle blush "Definitely! I'm excited!"
      mc "I hope I picked the right nocturne..."
      isabelle blush "Oh, they're all great!"
      mc "They better be if he spent that many years of his life composing them.{space=-45}"
      isabelle laughing "Have you not heard them before?"
      mc "I'm sure I have, I just can't remember their names."
      isabelle confident "Understandable. Lead the way!"
    "\"Have you ever heard the Nocturnes played on bagpipes?\"":
      show isabelle neutral at move_to(.5)
      mc "Have you ever heard the Nocturnes played on bagpipes?"
      $isabelle.lust+=1
      isabelle blush "That would be the dream..."
      mc "I'll keep that in mind."
      mc "For now, the piano will have to do. Are you ready?"
      isabelle blush "Yes! I can't wait!"
  "[isabelle] seems genuinely excited. Who knew something like this would be a turn on for a woman?"
  if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
    "If only she knew that the noose is tightening around her pretty neck..."
  else:
    "So far, learning the piano has been one of the best choices in my life. The best part is that it's enjoyable, as well."
  mc "Okay, let's go!"
  isabelle confident "Are you going to tell me which one you picked?"
  if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
    mc "You'll just have to wait and see."
  else:
    mc "You'll just have to wait and see..."
  isabelle laughing "Fine. It'll be a nice surprise!"
  window hide
  show isabelle laughing at (disappear_to_left if game.location in ("school_forest_glade","school_ground_floor","school_first_hall") else disappear_to_right)
  pause 1.0
  window auto
  $quest.isabelle_piano.advance("concert")
  return

label quest_isabelle_piano_concert_music_class:
  "Maybe bringing [isabelle] here was a bad idea... but there's no time to practice now."
  return

label quest_isabelle_piano_concert_english_class:
  "One of [isabelle]'s favorite subjects! But she's here for the music now.{space=-20}"
  return

label quest_isabelle_piano_concert_art_class:
  "[isabelle] is waiting excitedly by the piano. Ditching her would be highly inappropriate."
  return

label quest_isabelle_piano_concert_exit_arrow:
  "Escape is no longer an option. [isabelle] is watching my every move."
  return

label quest_isabelle_piano_concert:
  show isabelle neutral with Dissolve(.5)
  isabelle neutral "I told a few people on the way here that there's a mini-concert happening! I hope you don't mind."
  isabelle neutral "They should be coming soon..."
  mc "Um...  I guess that's okay..."
  isabelle excited "Lovely! You should get ready!"
  hide isabelle with Dissolve(.5)
  "I thought I would just play for [isabelle]..."
  "..."
  "Fuck. Why am I sweating all of a sudden?"
  if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
    "I guess there's a lot more at stake here than just music."
    "Trust. Betrayal. Espionage."
    "..."
  "Hopefully it's just [lindsey] and [flora]..."
  "Okay, better put the score in its place and sit down before my knees give out."
  $quest.isabelle_piano.advance("score")
  return

label quest_isabelle_piano_score_interact:
  "I still need to put the score on the stand..."
  "...or do I? Maybe [isabelle] will appreciate the spontaneity of a scoreless jam?"
  menu(side="middle"):
    extend ""
    "Play without the score":
      window hide
      show misc piano_play bg with Dissolve(.5)
      $school_first_hall_west["exclusive"] = "isabelle"
      window auto
      "Jazz musicians do it all the time. It's no big deal."
      show misc piano_play crowd as crowd with Dissolve(.5):
        xpos 711 ypos 0
      "Shit. Why are all these people interested in a piano tune?"
      "It must be because [isabelle] is cute, so they just tagged along..."
      "That's another perk of being attractive — people's default is to trust you, whereas an ugly person is met with suspicion."
      "It's a sick world created by pop culture."
      "Villains are ugly. Heroes are beautiful. It's absolutely disgusting!"
      "..."
      "I should probably focus..."
      if mc.intellect >= 5:
        stop music
        window hide
        pause 1.0
        play music "spontaneous"
        show screen music_notes
        pause 33.0
        stop music
        hide screen music_notes
        pause 1.0
        window auto
        "No applause? Damn..."
        window hide
        hide misc piano_play bg
        hide crowd
        show isabelle afraid
        with Dissolve(.5)
        play music "school_theme" fadein 0.5
        window auto
        isabelle afraid "..."
        mc "What?"
        isabelle flirty "What was that?!"
        mc "What do you mean?"
        isabelle laughing "Don't try to act all innocent!"
        isabelle confident "We both know that wasn't Chopin..."
        show isabelle confident at move_to(.25)
        menu(side="right"):
          extend ""
          "?mc.intellect>=5@[mc.intellect]/5|{image= stats int}|\"I forgot to bring the sheet music... I had to improvise.\"":
            show isabelle confident at move_to(.5)
            mc "I forgot to bring the sheet music... I had to improvise."
            mc "I didn't want to let you and the crowd down, so I just winged it."
            $isabelle.love+=3
            isabelle blush "It was amazing! I can't believe you improvised that."
            isabelle blush "That's true talent!"
          "\"Did you like it, though?\"":
            show isabelle confident at move_to(.5)
            mc "Did you like it, though?"
            $isabelle.love+=1
            isabelle blush "Are you kidding me? I loved it!"
            mc "Really?"
            isabelle blush "Yes! It was genuinely good!"
            mc "I feel like I messed up a few notes..."
            isabelle laughing "Technically you messed up all of them, since this wasn't Chopin."
            isabelle confident "Come on, stop overthinking it! That was great."
          "?mc.charisma>=5@[mc.charisma]/5|{image= stats cha}|\"You must've missed\nhis latest release.\"":
            show isabelle confident at move_to(.5)
            mc "You must've missed his latest release."
            isabelle laughing "Right... Chopin's latest posthumous release."
            mc "Hey, music is a tricky business! Sometimes, you have to wait years for agents, producers, and record labels."
            isabelle laughing "Poor Chopin..."
            mc "Anyway, what did you think?"
            $isabelle.lust+=3
            isabelle blush "I actually loved it!"
            mc "Thank you! I'm glad."
        if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
          "[isabelle]'s eyes glow in adoration. She has no idea that she's walking right into a trap."
          "She has no idea I'm not who she thinks I am."
          isabelle blush "I feel like praise isn't enough, but I don't really have anything\napart from this..."
          isabelle blush "It's my favorite ice tea. I hope you like it."
          $mc.add_item("ice_tea")
          isabelle blush "Talk to you later, [mc]!"
          show isabelle blush at disappear_to_left
          "What do you call it when a girl buys you a drink?"
          "Uncharted territory."
          $school_first_hall_west["exclusive"] = False
          $quest.isabelle_piano.finish()
          return
        else:
          window hide
          if renpy.showing("isabelle blush"):
            show isabelle blush at move_to(.9,1.5)
          elif renpy.showing("isabelle confident"):
            show isabelle confident at move_to(.9,1.5)
          pause 0.0
          $set_dialog_mode("default_no_bg")
          $isabelle.default_name = None
          isabelle blush "{nw}"
          pause 0.5
          window auto
          $set_dialog_mode("")
          $isabelle.default_name = "Isabelle"
          "[isabelle] leans against the side of the piano, eyes sparkling with admiration."
          "She's not easy to impress, which makes this all the more special."
          isabelle blush "Do you think you could teach me the song?"
          mc "Like a private lesson?"
          isabelle blush "Would you do that for me?"
          mc "I would do all sorts of things for you."
          isabelle blush "Oh, man..."
      else:
        stop music fadeout 0.5
        window hide
        pause 1.0
        window auto
        "..."
        "Maybe this wasn't the best idea..."
        "Everyone is staring..."
        "Fuck. Where do I even start?"
        "..."
        "B-flat minor, r-right?"
        "The crickets are already chirping..."
        "Choking... choking..."
        "Damn it. Fuck. Shit."
        window hide
        show black onlayer screens zorder 100
        show isabelle concerned
        with Dissolve(.5)
        pause 2.0
        hide misc piano_play bg
        hide crowd
        hide black onlayer screens
        with Dissolve(.5)
        play music "school_theme" fadein 0.5
        window auto
        "They all laughed and left. Mom's fucking spaghetti..."
        isabelle concerned "What happened?"
        mc "I couldn't do it..."
        isabelle sad "I'm sorry... It was the crowd, wasn't it?"
        show isabelle sad at move_to(.25)
        menu(side="right"):
          extend ""
          "\"No, I just thought I could play\nwithout sheet music...\"":
            show isabelle sad at move_to(.5)
            mc "No, I just thought I could play without sheet music..."
            isabelle concerned_left "It's okay. It probably had something to do with the crowd, as well."
            isabelle concerned "In hindsight, that was a stupid idea... but I appreciate you not wanting to blame me."
            $mc.love+=1
            mc "It's not your fault at all."
            isabelle sad "Thanks, but I still feel bad..."
          "\"Yeah, I got nervous...\"":
            show isabelle sad at move_to(.5)
            mc "Yeah, I got nervous..."
            isabelle sad "I'm really sorry. I didn't think it would be a big deal."
            $mc.lust+=1
            mc "Next time, you should probably ask first..."
            isabelle sad "You're right. I apologize."
            "She's sad for real. Now it feels a bit shitty to put all the blame on her...{space=-50}"
        if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
          isabelle sad "It's not much, but please take this as a token of my regret..."
          $mc.add_item("ice_tea")
          isabelle sad "It's my favorite. I hope you like it."
          show isabelle sad at disappear_to_left
          "There she goes, hanging her head. That sadness is an all too familiar feeling."
          "Regret. Failure. Letting someone else down."
          "[isabelle] probably isn't as used to it as me."
          "..."
          "I really need to get better at distancing myself from these feelings."
          "If I can't, what the hell am I doing on [kate]'s team?"
          $school_first_hall_west["exclusive"] = False
          $quest.isabelle_piano.finish()
          return
        else:
          window hide
          pause 0.5
          show isabelle blush with Dissolve(.5)
          window auto
          "[isabelle] looks at me with pity for a moment, but then a spark of excitement flashes across her eyes."
          isabelle blush "Do you think you could teach me the song?"
          mc "I'm not sure... I don't exactly feel qualified right now..."
          isabelle blush "Please? It'll be just the two of us this time!"
          isabelle blush "Like a private lesson..."
          mc "I guess, if you really want..."
          isabelle "I do! Thank you, [mc]."
      jump quest_isabelle_piano_sex
    "Try to find the score":
      pass
  return

label quest_isabelle_piano_score_use_item(item):
  if item == "chopin_music_score":
    $school_first_hall_west["music_score"] = "chopin"
    $mc.remove_item("chopin_music_score")
    "All right... everything's set and I'm only mildly nervous."
    window hide
    show misc piano_play bg
    show misc piano_play music_score as music_score:
      xpos 748 ypos 701
    with Dissolve(.5)
    $school_first_hall_west["exclusive"] = "isabelle"
    window auto
    "Oh, who am I kidding? I'm shaking like a leaf in a shit storm."
    show misc piano_play crowd as crowd with Dissolve(.5):
      xpos 711 ypos 0
    "Shit. Why are all these people interested in a piano tune?"
    "It must be because [isabelle] is cute, so they just tagged along..."
    "That's another perk of being attractive — people's default is to trust you, whereas an ugly person is met with suspicion."
    "It's a sick world created by pop culture."
    "Villains are ugly. Heroes are beautiful. It's absolutely disgusting!"
    "..."
    "I should probably focus..."
    show isabelle excited at appear_from_left
    isabelle excited "Everyone's here now!"
    isabelle concerned "I was a bit worried you'd mind. Are you sure you're okay?"
    mc "Yes..."
    isabelle neutral "You're a good pianist, and people should get the pleasure of hearing it.{space=-55}"
    mc "Thanks, I guess."
    show isabelle neutral at disappear_to_left
    "Fuck. I'm sweating already."
    "Here goes nothing..."
    stop music
    window hide
    pause 1.0
    play music "chopin"
    show screen music_notes
    pause 33.0
    play music "school_theme" fadein 0.5
    hide isabelle
    hide screen music_notes
    hide misc piano_play bg
    hide music_score
    hide crowd
    show isabelle blush
    with Dissolve(.5)
    $isabelle.love+=2
    $isabelle.lust+=2
    window auto
    isabelle blush "Whoa! I'm speechless!"
    isabelle blush "That was fantastic! I'm so impressed!"
    show isabelle blush at move_to(.25)
    menu(side="right"):
      extend ""
      "\"Nobody else seemed to like it...\"":
        show isabelle blush at move_to(.5)
        mc "Nobody else seemed to like it..."
        isabelle blush "I think they did! And if not, who cares?"
        if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
          isabelle blush "Those who didn't enjoy it clearly have no taste. They probably don't understand how difficult that piece is."
        else:
          isabelle annoyed_left "Those who didn't enjoy it clearly have no taste. They probably don't understand how difficult that piece is."
      "\"I feel free doing this.\"":
        show isabelle blush at move_to(.5)
        mc "I feel free doing this."
        mc "It's weird... Whenever I sit down in front of the piano, it's like a huge burden is lifted from my shoulders."
        mc "I'm happy I could share that feeling today! Setting people free through music is fantastic."
        $isabelle.lust+=1
        isabelle blush "Oh, man. That should be a quote!"
      "\"Thank you! You're too kind.\"":
        show isabelle blush at move_to(.5)
        mc "Thank you! You're too kind."
        $isabelle.love+=1
        isabelle laughing "You're not only a great pianist, but you're also humble! I like that."
    if quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed:
      "[isabelle]'s eyes glow in adoration. She has no idea that she's walking right into a trap."
      "She has no idea I'm not who she thinks I am."
      isabelle blush "Thanks for the concert. I meant it!"
      isabelle blush "Here's a small token of appreciation. It's my favorite flavor."
      $mc.add_item("ice_tea")
      isabelle blush "I should get going. Have a lovely day!"
      show isabelle blush at disappear_to_left
      "What do you call it when a girl buys you a drink?"
      "Uncharted territory."
      $school_first_hall_west["exclusive"] = False
      $quest.isabelle_piano.finish()
      return
    else:
      window hide
      if renpy.showing("isabelle annoyed_left"):
        show isabelle annoyed_left at move_to(.9,1.5)
      elif renpy.showing("isabelle blush"):
        show isabelle blush at move_to(.9,1.5)
      elif renpy.showing("isabelle laughing"):
        show isabelle laughing at move_to(.9,1.5)
      pause 0.0
      $set_dialog_mode("default_no_bg")
      $isabelle.default_name = None
      isabelle blush "{nw}"
      pause 0.5
      window auto
      $set_dialog_mode("")
      $isabelle.default_name = "Isabelle"
      "[isabelle] leans against the side of the piano, eyes sparkling with admiration."
      "She's not easy to impress, which makes this all the more special."
      "It's not every day you find a girl classy enough to enjoy a piece of classical music."
      isabelle blush "Do you think you could teach me the song?"
      mc "Like a private lesson?"
      isabelle blush "Would you do that for me?"
      mc "I would do all sorts of things for you."
      isabelle blush "Oh, man..."
      jump quest_isabelle_piano_sex
  else:
    "Making a sonata in honor of my [item.title_lower] would certainly count as modern art... Unfortunately, [isabelle] wants classical music."
    $quest.isabelle_piano.failed_item("score",item)
  return

label quest_isabelle_piano_sex:
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $mc["focus"] = ""
  pause 1.0
  show isabelle piano_sex_bend_over
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  isabelle piano_sex_bend_over "I love it when you play the piano... It helps me control the fire inside me.{space=-75}"
  isabelle piano_sex_bend_over "Your fingers are so nimble, so skilled and sure on the keys..."
  show isabelle piano_sex_kiss with Dissolve(.5)
  isabelle piano_sex_kiss "Mmmm..."
  "It feels like we've grown much closer lately."
  "[isabelle] doesn't shy away from my touch, which is something I've always been used to with girls."
  "Her ass is so soft, it's hard to keep my hands off..."
  mc "My fingers are skilled at many things..."
  isabelle piano_sex_kiss "Oh? What sort of things?"
  mc "Take off your clothes and I'll show you."
  isabelle piano_sex_kiss "Right here? What if someone comes?"
  mc "Sometimes you have to risk it for the... what's that thing you have with tea?"
  isabelle piano_sex_kiss "A scone?"
  mc "Yes... sometimes you have to risk it for the scone."
  isabelle piano_sex_strip1 "You're so right..."
  "I can't believe the effect this is having on her."
  "That she's slowly undressing for me right now."
  "The admiration in her eyes has turned from a spark to a smoldering ember of desire."
  isabelle piano_sex_strip1 "I'd let you show me all sorts of things, [mc]."
  isabelle piano_sex_strip1 "I have some things I could show you, too."
  show isabelle piano_sex_strip2 with dissolve2
  isabelle piano_sex_strip2 "How's that for starters?"
  "Her snow white skin prickles in the slight draught of the corridor."
  "She bites her lip as her nipples harden."
  mc "God damn..."
  "Even through the fabric of her clothes, the smell of her arousal reaches my nose."
  "The scent of her pussy is like an aphrodisiac to me, making me hard in an instant."
  "Maybe it'll land us in detention, but who cares?"
  show isabelle piano_sex_strip3 with dissolve2
  "I need to have a taste of her."
  show isabelle piano_sex_strip4 with dissolve2
  "My fingers slip into the waist band of her panties, pulling down, revealing her perfect ass and pussy."
  show isabelle piano_sex_pussy_lick with Dissolve(.5)
  isabelle piano_sex_pussy_lick "Ohh!"
  "The taste of her juices flood my senses."
  "A perfect combination of salty and sweet."
  isabelle piano_sex_pussy_lick "Oh, god... don't stop..."
  "Her words come out in gasps as my tongue slides between her nether lips."
  "She's so wet. So ready."
  show isabelle piano_sex_anticipation with Dissolve(.5)
  if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
    "With her skirt pushed up around her waist and her panties down around her ankles, [isabelle] looks back at me expectantly."
  else:
    "With her pants and panties down around her ankles, [isabelle] looks back at me expectantly."
  "Sex in a school corridor in broad daylight is a thrill in and of itself."
  "But getting caught would be a mood killer. We have to be careful."
  isabelle piano_sex_anticipation "P-please..."
  "I know what she needs."
  "I want to be the one who gives it to her."
  show isabelle piano_sex_penetration1 with Dissolve(.5)
  isabelle piano_sex_penetration1 "Oooh..."
  "I line myself up with her pussy lips and go slow."
  "Penetrate her with just the tip of my dick."
  window hide
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.3)
  pause 0.4
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.3)
  pause 0.4
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.3)
  pause 0.4
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.3)
  window auto
  "She moans, louder than before."
  "I tease her, withdraw from her."
  isabelle piano_sex_penetration1 "Please!"
  "I like to hear how badly she needs it. Needs me."
  window hide
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  pause 0.15
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  pause 0.15
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.2)
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration1 with Dissolve(.2)
  pause 0.2
  show isabelle piano_sex_penetration2 with Dissolve(.2)
  show isabelle piano_sex_penetration3 with Dissolve(.3)
  window auto
  "So, I ease into her again, farther this time."
  "Her muscles clamp down on me, her whole body reacting to the desperation she feels."
  "And she feels so good around me. So tight and hot and wet."
  "It's enough to make me groan."
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.5)
  "Once more, I pull out and she tries to look back at me, but my fist is tightly wound in her hair and I keep her looking forward."
  window hide
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration4 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  pause 0.1
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration4 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  pause 0.1
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration4 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration4 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration4 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration4 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  pause 0.15
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with hpunch
  pause 0.4
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with hpunch
  pause 0.4
  show isabelle piano_sex_penetration3 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with hpunch
  window auto
  "Then I thrust into her hard, giving her all of me now."
  isabelle piano_sex_penetration4 "[mc]!"
  "I ride her into the piano, and we're making our own sort of music now.{space=-40}"
  "Our pants and groans and grunts fill the room."
  window hide
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.25)
  pause 0.15
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  pause 0.1
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  pause 0.1
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.25)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration2 with Dissolve(.15)
  show isabelle piano_sex_penetration1 with Dissolve(.15)
  show isabelle piano_sex_penetration2 with Dissolve(.1)
  show isabelle piano_sex_penetration3 with Dissolve(.1)
  show isabelle piano_sex_penetration4 with Dissolve(.25)
  window auto
  "I go faster, nearing the edge as she clamps around me and starts to spasm."
  "And that's what finally makes me lose control."
  menu(side="middle"):
    extend ""
    "Pull out":
      window hide
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration2 with Dissolve(.15)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration3 with Dissolve(.25)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration4 with Dissolve(.25)
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration2 with Dissolve(.15)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration4 with Dissolve(.25)
      pause 0.1
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration2 with Dissolve(.15)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_anticipation with Dissolve(.5)
      pause 0.15
      show isabelle piano_sex_cum_outside with vpunch
      window auto
      "Just in time, I slide out and paint [isabelle]'s ass white."
      "She looks... surprised? Did she expect me to shoot my load inside her?{space=-65}"
      "Or did I get some on her shirt?"
      "Maybe she'll have to walk around with a cum stain on her black shirt for the rest of the day..."
      "There's something hot about that."
      "I wonder if anyone would notice."
    "Fill her up":
      window hide
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration2 with Dissolve(.15)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration3 with Dissolve(.25)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration4 with Dissolve(.25)
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration2 with Dissolve(.15)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      show isabelle piano_sex_penetration2 with Dissolve(.1)
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration4 with Dissolve(.25)
      pause 0.1
      show isabelle piano_sex_penetration3 with Dissolve(.1)
      show isabelle piano_sex_penetration2 with Dissolve(.15)
      show isabelle piano_sex_penetration1 with Dissolve(.15)
      pause 0.15
      show isabelle piano_sex_cum_inside with hpunch
      window auto
      "Hot jets of semen fill [isabelle] up to the brim."
      "At first, surprise and worry fill her eyes, but then she smiles."
      "Surely, she's on the pill. She's responsible like that."
      "But still, a twisted thought lingers in the back of my mind..."
      "What if she isn't?"
      "What would happen?"
      $quest.isabelle_piano["creampie"] = True
      # $isabelle["creampied"]+=1
  $unlock_replay("isabelle_lesson")
  window hide
  hide screen interface_hider
  show black onlayer screens zorder 4
  with Dissolve(.5)
  $isabelle.unequip("isabelle_pants")
  $isabelle.unequip("isabelle_panties")
  pause 1.0
  show isabelle laughing_untucked at center
  show screen interface_hider
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  if quest.isabelle_piano.finished:
    return
  isabelle laughing_untucked "I can't believe we didn't get caught!"
  mc "Heh, well. There's always the next time..."
  $isabelle.equip("isabelle_panties")
  show isabelle confident_untucked with dissolve2
  isabelle confident_untucked "Is that right?"
  mc "Of course! It's my duty to give my students the education they deserve!{space=-75}"
  mc "I suspect you'll need at least five more private lessons. Or ten."
  $isabelle.equip("isabelle_pants")
  show isabelle confident with dissolve2
  isabelle confident "All right, teacher... You just let me know when the next one is."
  $game.notify_modal(None, "Love or Lust", "You have unlocked\na repeatable scene\nwith [isabelle]!", wait=5.0)
  isabelle excited "Oh, I almost forgot!"
  isabelle excited "It's not much, but please take this as a token of my appreciation for the concert earlier."
  $mc.add_item("ice_tea")
  isabelle neutral "It's my favorite ice tea... I hope you like it."
  isabelle neutral "Talk to you later, [mc]!"
  show isabelle neutral at disappear_to_left
  "What do you call it when a girl buys you a drink?"
  "Uncharted territory."
  $school_first_hall_west["exclusive"] = False
  $quest.isabelle_piano["private_lesson_today"] = True
  $quest.isabelle_piano.finish()
  return

label quest_isabelle_piano_sex_repeatable:
  if quest.isabelle_piano["private_lesson_today"]:
    "I've already given [isabelle] a private lesson today."
    "Giving her another one so soon would be asking for us to get caught.{space=-30}"
  else:
    stop music
    window hide
    show misc piano_play bg with Dissolve(.5)
    $school_first_hall_west["exclusive"] = "isabelle"
    pause 1.0
    play music "sweet_and_romantic"
    show screen music_notes
    pause 33.0
    play music "school_theme" fadein 0.5
    hide screen music_notes
    hide misc piano_play bg
    show isabelle blush
    with Dissolve(.5)
    window auto
    isabelle blush "I just can't get enough of this tune..."
    mc "Would you like another private lesson?"
    isabelle blush "How could I possibly refuse...?"
    call quest_isabelle_piano_sex
    isabelle laughing_untucked "I can't believe we didn't get caught yet again!"
    $isabelle.equip("isabelle_panties")
    show isabelle confident_untucked with dissolve2
    isabelle confident_untucked "I feel like I'm starting to get the hang of the piano now..."
    $isabelle.equip("isabelle_pants")
    show isabelle confident with dissolve2
    isabelle confident "See you later, teacher."
    show isabelle confident at disappear_to_left
    "Who knew that music could be so exciting?"
    $school_first_hall_west["exclusive"] = False
    $quest.isabelle_piano["private_lesson_today"] = True
  return
