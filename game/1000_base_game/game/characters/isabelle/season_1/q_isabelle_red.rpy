label quest_isabelle_red_start:
  show jacklyn neutral with Dissolve(.5)
  mc "Hi, do you know where I could find a bucket of red paint?"
  jacklyn neutral "What's rustling Santa's jimmies?"
  "Err... what? She must mean what it's for."
  show jacklyn neutral at move_to(.25)
  menu(side="right"):
    extend ""
    "\"It's for a personal art project.\"":
      show jacklyn neutral at move_to(.5)
      mc "It's for a personal art project."
      $jacklyn.lust+=1
      jacklyn smile "Right on!"
      jacklyn smile "What's the Mona Lisa?"
      mc "I'd like to keep it a secret for now..."
      jacklyn laughing "Jokes. I get that."
      jacklyn laughing "Just like Misaratti — he kept the zipper on until his great fresco was completed and displayed in its full glory."
      jacklyn excited "Art has its own voice. Words can't describe the stroke of a brush or the curve of a smile."
      jacklyn excited "It's an extension of the human mind."
      jacklyn excited "Thoughts and ideas visualized on the canvas. An object of beauty out of thin air."
      mc "Right."
    "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"It's for a play about the Battle of Edwin's Bridge.\"":
      show jacklyn neutral at move_to(.5)
      mc "It's for a play about the Battle of Edwin's Bridge."
      jacklyn smile "Red paint for a victimless battle?"
      "Oh, snap. How the hell did she know about that?"
      "There's no information on the internet about it..."
      "The only reason I know about this event is because of [jo]'s great great uncle's love letters during the time."
      mc "Then you must also know the color of their shields..."
      jacklyn thinking "Shields? You're tickling my funny bone."
      jacklyn thinking "The combatants were all peasants and barely had access to weapons, much less shields."
      "How does she know this?!"
      mc "The banners—"
      jacklyn laughing "Blue."
      mc "The sunset—"
      jacklyn laughing "Midnight battle."
      mc "The rickety windmill on hill by the—"
      jacklyn laughing "Swept away by a storm the day before the battle."
      "Shit. I'm digging my own grave."
      mc "The... err, the red lipstick that my great great uncle's lover kissed her letters with?"
      jacklyn annoyed "That's... I didn't know about that."
      "Ha! Got her."
      jacklyn excited "Was your great great uncle's name Enoch, by any chance?"
      mc "Huh! How did you know that?"
      jacklyn laughing "My great great aunt's name was Lora."
      jacklyn laughing "Wicked coincidence, my dude!"
      "This is crazy..."
      mc "It's such a romantic tale! Two lovers stuck on different sides of the river during the war."
      mc "Exchanging letters with the help of carrier pigeons. Longing for each other's arms."
      $jacklyn.love+=1
      jacklyn excited "Love on crack."
    "\"I'm here to investigate the pigment quality on behalf of the school paper.\"":
      show jacklyn neutral at move_to(.5)
      mc "I'm here to investigate the pigment quality on behalf of the school paper.{space=-90}"
      jacklyn thinking "That's sus, bro."
      mc "What is?"
      jacklyn thinking "You're second in line for that."
      mc "Huh?"
      jacklyn annoyed "The chick with the glasses. I always forget her name."
      jacklyn annoyed "Pigment quality investigation, yeah."
      mc "And you just gave her the paint?"
      jacklyn excited "Pigment quality is top prio."
      mc "Did she tell you when she'd give it back?"
      jacklyn laughing "When the doves turn black."
      mc "What does that mean?"
      jacklyn laughing "Beats me, but those were her words."
      hide jacklyn with dissolve2
      "Well, shit."
      "Wrestling something out of [maxine]'s hands is almost impossible..."
      "I need to come up with a plan."
      $quest.isabelle_red.start()
      return
  mc "Anyway, what about that bucket of red paint?"
  jacklyn neutral "I'd let you have it, but another student already snagged it."
  mc "Do you only have one bucket?"
  jacklyn neutral "Budget cuts whipped our asses."
  mc "Well, do you know who borrowed it?"
  jacklyn thinking "That chick with the glasses. I always forget her name."
  "Ugh, [maxine]..."
  "Getting stuff from her is like pulling a sword from a stone — you basically need to be the king of England."
  jacklyn smile "Smooth winds, sailor."
  hide jacklyn with Dissolve(.5)
  $quest.isabelle_red.start()
  return

label quest_isabelle_red_note:
  if quest.isabelle_red == "note":
    "I totally forgot about her annoying habit of leaving clues about where she's gone."
    "Apparently, it's in case a highly intelligent civilization stops by her door during her coffee break."
    "Aliens from outer space would surely enjoy her hide and seek games.{space=-30}"
    "Let's see what it says..."
    "{i}\"From my office, feet so light, I've made my way this day most bright. Hidden in a world of greens — petals, pistils, color-schemes.\"{/}{space=-20}"
    "Ugh, a riddle?"
    "Well, if I can't figure it out on my own, maybe [isabelle] can help..."
    $quest.isabelle_red.advance("greenhouse")
  else:
    "{i}\"From my office, feet so light, I've made my way this day most bright. Hidden in a world of greens — petals, pistils, color-schemes.\"{/}{space=-20}"
  return

label quest_isabelle_red_greenhouse_isabelle:
  show isabelle neutral with Dissolve(.5)
  mc "Hey! Do you have a moment?"
  isabelle neutral "Sure thing!"
  mc "So, I need help solving this riddle..."
  isabelle excited "I love riddles!"
  mc "{i}\"From my office, feet so light, I've made my way this day most bright. Hidden in a world of greens — petals, pistils, color-schemes.\"{/}{space=-20}"
  mc "I think it's somewhere in the school."
  isabelle concerned "Sounds like a place with flowers and sunlight, right?"
  mc "Hmm..."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_red["greenhouse_riddle_help"] = True
  return

label quest_isabelle_red_greenhouse:
  if quest.isabelle_red == "greenhouse":
    "Well, no sign of [maxine], but here's another note..."
    "{i}\"Thought you had me, did you not? But I've already left this spot. Down the stairway, through the hall. In a classroom, on a wall.\"{/}"
    $quest.isabelle_red.advance("black_note")
  else:
    "{i}\"Thought you had me, did you not? But I've already left this spot. Down the stairway, through the hall. In a classroom, on a wall.\"{/}"
  return

label quest_isabelle_red_black_note_isabelle:
  show isabelle neutral with Dissolve(.5)
  mc "Hey, where in the school do you think this riddle is?"
  mc "{i}\"Thought you had me, did you not? But I've already left this spot. Down the stairway, through the hall. In a classroom, on a wall.\"{/}"
  isabelle concerned_left "Hmm... one of the classrooms. Could it be a painting?"
  mc "Good thinking."
  hide isabelle with Dissolve(.5)
  $quest.isabelle_red["black_note_riddle_help"] = True
  return

label quest_isabelle_red_black_note_english_door:
  "Huh? There are voices coming from inside..."
  "Crap. It sounds like [kate] and her cheerleader posse."
  lacey "But I'm not... you know..."
  kate "A lesbian? Well, neither am I."
  kate "But a bet is a bet."
  lacey "Do I really have to? It's..."
  kate "It's what?"
  lacey "..."
  lacey "Nothing."
  kate "Just do it. We have practice soon."
  "What's going on in there?"
  "Maybe I can sneak a peek?"
  play audio "open_door"
  "..."
  "Damn, they locked the door."
  menu(side="middle"):
    extend ""
    "?mc.owned_item('high_tech_lockpick')@|{image=items high_tech_lock}|\"Locks can't stop me!\"":
      "Locks can't stop me!"
      $mc.remove_item("high_tech_lockpick")
      play audio "lock_click"
      "..."
      $mc.add_item("high_tech_lockpick")
      "Please don't notice me..."
      window hide
      show black onlayer screens zorder 100
      show kate lacey_bet_anticipation
      with Dissolve(.5)
      $game.location = "school_english_class"
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "No way..."
      lacey "Can't we just do something else?"
      kate lacey_bet_anticipation "Quit being such a baby."
      kate lacey_bet_anticipation "Just imagine you're eating a papaya."
      lacey "A papaya?"
      "[kate] seems way more into it than [lacey]."
      "There's a certain anxiety to [lacey] that I haven't seen before..."
      "She's always so energetic."
      "Perhaps I should offer to take her place?"
      "Considering [kate] is pretty much perfect in every way, her pussy probably is too."
      "Although, she would probably kill me before letting me anywhere near her pussy."
      kate lacey_bet_anticipation "Come on, just give it a lick. It won't kill you."
      lacey "Okay..."
      window hide
      show kate lacey_bet_licking_panties with Dissolve(.5)
      window auto
      kate lacey_bet_licking_panties "See? That wasn't so bad, was it?"
      lacey "No..."
      "It's weird to see [kate] so... nurturing."
      "She's this stone cold bully with me, but with [lacey]... she's almost motherly in a way."
      "Maybe there are more sides to her than I thought."
      kate lacey_bet_licking_panties "Now, take my panties off and try again."
      window hide
      show kate lacey_bet_removing_kate_skirt with Dissolve(.5)
      pause 0.5
      show kate lacey_bet_removing_kate_panties with Dissolve(.5)
      pause 0.5
      show kate lacey_bet_licking_pussy1 with Dissolve(.5)
      window auto
      "The tip of [lacey]'s tongue grazes [kate]'s rosy folds."
      "As expected, it is a perfect pussy — completely shaven clean, glistening and swollen."
      "[lacey] sweeps her tongue up and down, slowly, gently... maybe nervously."
      "Her bubbly smiley self, replaced by a more concentrated countenance.{space=-50}"
      lacey "Doesn't taste like papaya..."
      "[kate] stifles a gasp of pleasure, a single bead of sweat sparkling down the side of her face."
      kate lacey_bet_licking_pussy1 "W-what does it taste like?"
      lacey "I don't know..."
      "God, [kate] has completely mesmerized her with her pussy."
      "How can someone so rotten on the inside, be so beautiful on the outside?"
      "It's not fair."
      "Pretty privilege is absolutely real, and [kate] is the ruling class."
      kate lacey_bet_licking_pussy1 "Touch me like you would touch yourself, [lacey]."
      window hide
      show kate lacey_bet_removing_lacey_top with Dissolve(.5)
      pause 0.5
      show kate lacey_bet_fingering_pussy with Dissolve(.5)
      window auto
      kate lacey_bet_fingering_pussy "Ohhh! Yes! Exactly like that!"
      lacey "Mmmhmm..."
      lacey "If you have an orgasm, that means you're gay, right?"
      kate lacey_bet_fingering_pussy "Ahh! No, it doesn't!"
      kate lacey_bet_fingering_pussy "Keep going! I love it when you talk into my pussy!"
      lacey "Mmmm... what?"
      kate lacey_bet_fingering_pussy "Nothing! Tell me about your day."
      window hide
      show kate lacey_bet_removing_kate_top with Dissolve(.5)
      pause 0.5
      show kate lacey_bet_removing_kate_and_lacey_bra with Dissolve(.5)
      window auto
      "Whoa! [kate] is really getting into it..."
      "She slips out of her top and helps [lacey] out of hers."
      lacey "Mmm... after school... mmm.... I'm going to my sister's beauty pageant...{space=-75}"
      kate lacey_bet_removing_kate_and_lacey_bra "Ohhh!"
      lacey "Mmm... then I'm going to go shopping... mmm... then maybe a mani-pedi...{space=-110}"
      kate lacey_bet_removing_kate_and_lacey_bra "Y-yes!"
      lacey "Mmm... okay... definitely a mani-pedi, then..."
      "The vibrations from [lacey]'s voice are driving [kate] closer and closer to the edge."
      "I wonder what bet it was that put them in this position, but I'm sure [kate] rigged it."
      "Getting her friends to eat her out over a bet is typical [kate] behavior.{space=-5}"
      lacey "Mmm... then I'm going to see Tanner..."
      kate lacey_bet_removing_kate_and_lacey_bra "Touch me like you touch yourself when you think of Tanner!"
      lacey "Mmm... okay..."
      window hide
      show kate lacey_bet_fingering_ass1 with Dissolve(.5)
      window auto
      kate lacey_bet_fingering_ass1 "Oooh!"
      "Holy shit! [lacey] just put two fingers up [kate]'s ass..."
      "I knew the cheerleaders were slutty bimbos, but god damn!"
      kate lacey_bet_fingering_ass1 "That's... hnnngh!"
      "[kate]'s eyes roll back as [lacey] drives her finger in and out, forcing the orgasm out."
      kate lacey_bet_fingering_ass1 "Aaaaaaaah! My god!" with vpunch
      "Suddenly, [kate]'s entire body bucks, and a deep orgasmic spasm runs through her abdomen."
      "Then the climax hits in full, turning her limbs into jelly."
      "For a moment, she just lies there while [lacey], clearly unaware of the climax, keeps licking and fingering."
      show kate lacey_bet_fingering_ass2 with dissolve2
      kate lacey_bet_fingering_ass2 "[lacey]-baby?"
      lacey "Mmmhmm?"
      kate lacey_bet_fingering_ass2 "Would you do something for me?"
      show kate lacey_bet_fingering_ass3 with dissolve2
      lacey "Yes, what?"
      window hide
      show kate lacey_bet_spreading_ass with Dissolve(.5)
      window auto
      "[kate] suddenly flips over, presenting her ass."
      "And it is a fine ass by any standard."
      "Round and firm, like a semi-ripe peach."
      kate lacey_bet_spreading_ass "Lick me."
      lacey "There...?"
      kate lacey_bet_spreading_ass "Yes, I'm very clean. You might even like it."
      lacey "Hmm... okay. If you say so..."
      kate lacey_bet_spreading_ass "I do say so."
      "[kate] does bossy just right. Man, I wish she would boss me like this."
      "I know I wouldn't mind eating her ass..."
      "Ugh, what am I thinking?"
      show kate lacey_bet_licking_ass1 with dissolve2
      "Carefully, [lacey] pokes her tongue out."
      "She circles [kate]'s rosebud."
      kate lacey_bet_licking_ass1 "Ahhh!"
      "A million nerve endings meeting a million nerve endings."
      "The tip of her tongue, swirling over the tiny dark brown wrinkles."
      "Poking through the sphincter, tasting, exploring deeper."
      "[kate] moans in pleasure — it's a soft feminine sound, very unlike her usual harsh tone."
      show kate lacey_bet_licking_ass2 with dissolve2
      "[lacey] again slides her fingers into [kate]'s pussy, fucking her with both tongue and digit."
      kate lacey_bet_licking_ass2 "Oh my god..."
      kate lacey_bet_licking_ass2 "Yes... eat my ass, [isabelle]..."
      "[isabelle]?"
      show kate lacey_bet_licking_ass3 with dissolve2
      lacey "[isabelle]?"
      show kate lacey_bet_spreading_ass with dissolve2
      kate lacey_bet_spreading_ass "Uh... sorry, my mind was somewhere else..."
      kate lacey_bet_spreading_ass "Continue."
      lacey "Okay..."
      show kate lacey_bet_licking_ass2 with dissolve2
      lacey "Mmmhmm..."
      kate lacey_bet_licking_ass2 "I was just daydreaming about when I'll have her eating my ass on every recess..."
      kate lacey_bet_licking_ass2 "I'm so looking forward to making her my docile bitch-pet..."
      kate lacey_bet_licking_ass2 "I'll have her eat your ass too, [lacey]."
      lacey "Mmmm..."
      kate lacey_bet_licking_ass2 "And everyone else... on the squad..."
      kate lacey_bet_licking_ass2 "Hnnngh! Yes!"
      kate lacey_bet_licking_ass2 "God, you're good at this, [lacey]!"
      kate lacey_bet_licking_ass2 "Lick my pussy again!"
      show kate lacey_bet_licking_pussy with dissolve2
      lacey "Mmmhmmm..."
      kate lacey_bet_licking_pussy "Just like that!"
      "With her tongue inside that pussy, and her nose buried in [kate]'s ass, [lacey] is really showing her true self."
      "She has always been such a brown-nosed kiss-ass."
      "Always the yes-man... or well, yes-queen in this case."
      "Her tongue whips across [kate]'s fold, almost like a machine."
      "She might be a complete airhead, but at least she knows how to eat pussy and ass."
      "Maybe it comes natural to girls like her?"
      kate lacey_bet_licking_pussy "Oh, my god!"
      "[kate] seems to be having the time of her life."
      "I wish I was able to please someone as popular and pretty as her..."
      "If only she would give me a chance..."
      show kate lacey_bet_squirting1 with dissolve2
      kate lacey_bet_squirting1 "Oh, fuck! Yes!" with vpunch
      "God damn! She came again and squirted all over [lacey]!"
      show kate lacey_bet_squirting2 with dissolve2
      lacey "Hey..."
      "[lacey] narrows her eyes in annoyance, but [kate] just squirts again, this time on her face."
      "Eyes half-closed, she peers around the room for a tissue of some sort.{space=-60}"
      "Holy shit, I can't believe that just happened!"
      "If only I'd had the presence of mind to record it..."
      "Damn, that could've been my ticket out of the gutter."
      "Feels a bit like I won the lottery and then lost the ticket."
      "I need to step up my game this time around. No way I'm letting an opportunity like that slip through my fingers again."
      "..."
      "Crap. I really need to go before they notice me."
      "And I really need to find the nearest bathroom and jerk off."
      $unlock_replay("kate_papaya")
      $quest.isabelle_red["sneaked_a_peek"] = True
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $game.location = "school_first_hall_east"
      pause 1.0
      $game.advance()
      pause 1.0
      hide kate lacey_bet_squirting
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Ah, that was one of the better wanks this week!"
      "I really should jerk off more often in a bathroom stall. It's just more thrilling."
      "Hopefully, [kate] and [lacey] are done in the English classroom now, and I can go find [maxine]'s next clue..."
    "\"Just my luck...\"":
      "Just my luck..."
      "I guess I'll just have to wait for them to finish."
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 1.0
      $game.advance()
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Okay, I think they left..."
  $quest.isabelle_red["english_class_empty"] = True
  return

label quest_isabelle_red_black_note:
  if quest.isabelle_red == "black_note":
    "Seriously? Black paper?"
    "Fucking [maxine]..."
    "Let's see what it says, I guess."
    "{i}\"Clever is your middle name, your eye for detail is no shame. My name is a tricky matter, hit me hard and ears may shatter.\"{/}"
    $quest.isabelle_red.advance("triangle")
  else:
    "{i}\"Clever is your middle name, your eye for detail is no shame. My name is a tricky matter, hit me hard and ears may shatter.\"{/}"
  return

label quest_isabelle_red_triangle_isabelle:
  show isabelle neutral with Dissolve(.5)
  mc "Hey, [isabelle]! Could you help me with another riddle?"
  isabelle excited "Sure!"
  mc "What do you think about this one?"
  mc "{i}\"Clever is your middle name, your eye for detail is no shame. My name is a tricky matter, hit me hard and ears may shatter.\"{/}"
  isabelle skeptical "Hmm... that's a tough one."
  isabelle skeptical "Tricky matter? Ears shattering might have something to do with sound?{space=-85}"
  mc "I think I have an idea now, thanks!"
  $quest.isabelle_red["triangle_riddle_help"] = True
  if quest.isabelle_red["greenhouse_riddle_help"] == quest.isabelle_red["black_note_riddle_help"] == quest.isabelle_red["triangle_riddle_help"] == True:
    $isabelle.love+=2
    isabelle excited "Thanks for sharing these riddles with me! I'm having a ton of fun."
  else:
    isabelle neutral "You're welcome!"
  hide isabelle with Dissolve(.5)
  return

label quest_isabelle_red_triangle:
  "I can never resist the urge..."
  window hide
  pause 0.25
  play sound "<from 4.0>triangle"
  pause 1.25
  show maxine thinking with Dissolve(.5)
  window auto
  mc "Huh? Where the hell did you come from?"
  if quest.isabelle_red["greenhouse_riddle_help"] == quest.isabelle_red["black_note_riddle_help"] == quest.isabelle_red["triangle_riddle_help"] == False:
    $achievement.riddle_me_this.unlock()
  maxine thinking "Keep it down."
  hide maxine thinking with Dissolve(.5)
  "What the fuck? She climbed into a cabinet drawer!"
  $quest.isabelle_red.advance("cabinet")
  return

label quest_isabelle_red_cabinet:
  "Huh? How did she fit inside?"
  maxine "...Bloody Mary, Bloody Mary..."
  mc "Hello?"
  maxine "What?"
  mc "What are you doing in there?"
  show maxine annoyed with Dissolve(.5)
  maxine "What does it look like I'm doing?"
  mc "Why were you in the cabinet?"
  maxine skeptical "Acquiring supplies. What else would one do in a supply cabinet?"
  mc "..."
  maxine smile "Now that I'm done here..."
  mc "Wait!"
  maxine neutral "Yes?"
  mc "What about the riddles?"
  maxine neutral "What about them?"
  mc "Why not just write that you're in the music classroom?"
  maxine laughing "Don't be ridiculous!"
  maxine smile "Now, if you'll excuse me..."
  mc "Wait!"
  maxine concerned "Yes?"
  mc "I need that bucket of red paint you borrowed from the art classroom...{space=-45}"
  maxine excited "Here you go."
  $mc.add_item("red_paint")
  mc "Just like that?"
  mc "No tricks or wild goose chases? No impossible demands?"
  maxine thinking "That doesn't sound like something I would do."
  mc "What? You're always difficult about everything!"
  maxine sad "I don't think that's the case. Maybe you have me confused with someone else?"
  "As if confusing [maxine] with someone else would even be possible."
  "She's the strangest and most unique person on this side of the equator.{space=-75}"
  mc "I doubt it."
  maxine thinking "That's peculiar."
  maxine thinking "Most peculiar."
  maxine thinking "Now, if there's nothing else..."
  show maxine thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Why did you borrow the paint\nin the first place?\"":
      show maxine thinking at move_to(.5)
      mc "Why did you borrow the paint in the first place?"
      maxine confident "Testing."
      mc "Testing what?"
      maxine confident "Pigment quality, of course."
      "Hmm... that sounds like a lie..."
      mc "May I ask you why?"
      maxine confident "Yes, you may."
      mc "..."
      mc "Why?"
      maxine annoyed "Don't tell anyone, but I believe the quality of the pigment has been in decline for a time, especially when it comes to the red paint."
      mc "Any idea why that might be?"
      maxine skeptical "I have my suspicions..."
      maxine skeptical "Let's just say there are rabbits in the hat."
      mc "What?"
      maxine skeptical "There are owls in the cellar."
      mc "Owls? What cellar?"
      maxine skeptical "Exactly."
      mc "..."
      maxine skeptical "You've kept me long enough."
      show maxine skeptical at disappear_to_right
    "\"What's peculiar?\"":
      show maxine thinking at move_to(.5)
      mc "What's peculiar?"
      maxine confident "Lots of things! Crolytes, bearlings, paper machetes..."
      mc "What's a bearling?"
      maxine eyeroll "Faking innocence will get you nowhere."
      mc "..."
      mc "An extra small bear?"
      maxine laughing "Don't be ridiculous!"
      mc "Anything I can do to help?"
      maxine neutral "Just stay ever vigilant."
      mc "Uh, okay..."
      maxine smile "Anyway, you've kept me long enough."
      show maxine smile at disappear_to_right
    "\"What were you really doing\nin the supply cabinet?\"":
      show maxine thinking at move_to(.5)
      mc "What were you really doing in the supply cabinet?"
      maxine skeptical "If you must know, there's a rumor going around..."
      "Why am I not surprised?"
      maxine skeptical "Mary was last seen in the women's bathroom."
      mc "Mary?"
      maxine smile "Yes, the bloody one."
      mc "The urban legend?"
      maxine neutral "If that's what you want to call her."
      mc "That still doesn't explain what you were doing in the cabinet..."
      maxine thinking "The drain behind the cabinet connects to the sewer system."
      mc "And you think she's crawling through the pipes?"
      mc "You know what, I don't want to know."
      maxine thinking "Well, you've kept me long enough."
      show maxine thinking at disappear_to_right
  $quest.isabelle_red.finish()
  return
