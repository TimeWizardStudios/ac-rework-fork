init python:
  class Character_isabelle(BaseChar):
    notify_level_changed=True

    default_name="Isabelle"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="isabelle contact_icon"

    default_outfit_slots=["hat","collar","jacket","shirt","bra","pants","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("isabelle_panties")
      self.add_item("isabelle_green_panties")
      self.add_item("isabelle_pants")
      self.add_item("isabelle_skirt")
      self.add_item("isabelle_bra")
      self.add_item("isabelle_green_bra")
      self.add_item("isabelle_shirt")
      self.add_item("isabelle_party_dress")
      self.add_item("isabelle_top")
      self.add_item("isabelle_jacket")
      self.add_item("isabelle_collar")
      self.add_item("isabelle_glasses")
      self.add_item("isabelle_tiara")
      self.equip("isabelle_panties")
      self.equip("isabelle_pants")
      self.equip("isabelle_bra")
      self.equip("isabelle_shirt")
      self.equip("isabelle_glasses")

    def ai(self):

      self.location=None
      self.activity=None
      self.alternative_locations={}
      ###Forced Locations

      if quest.jo_washed.in_progress:
        self.location = None
        self.activity = None
        return

      ###Kate Blowjob
      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return
      ###Kate Blowjob

      if isabelle["at_none_this_scene"] or isabelle["at_none_now"] or isabelle["at_none_today"]:
        self.location = None
        self.alternative_locations = {}
        return

      if quest.kate_fate in ("peek","hunt","hunt2","deadend","rescue"):
        self.location = "school_first_hall"
        self.activity = "standing"
        return
      if quest.kate_desire == "advice":
        self.location = "school_ground_floor_west"
        self.activity = "standing"
        self.alternative_locations={}
        return
      ###Nurse Room
      #if isabelle["at_school_nurse_room_now"]:
      #  self.location="school_nurse_room"
      #  return
      ###Nurse Room

      ###Stolen Hearts & Loser to the Rescue
      if quest.isabelle_stolen == "slap" or quest.lindsey_nurse in ("fetch_the_nurse","return_to_gym"):
        self.location = None
        self.activity = None
        return
      ###Stolen Hearts & Loser to the Rescue

      ###Buried Truths
      if quest.isabelle_buried in ("bury","dig","funeral"):
        self.location = "school_forest_glade"
        self.activity = "standing"
        return
      ###Buried Truths

      ###Haggis Quest
      if quest.isabelle_haggis>="trouble" and quest.isabelle_haggis.in_progress:
        self.location="school_homeroom"
        self.activity="sitting"
        return
      ###Haggis Quest

      ###Isabelle Tour####
      if quest.isabelle_tour.in_progress:
        if quest.isabelle_tour == "cafeteria":
          self.location = "school_cafeteria"
          self.activity = "sitting"
          return

        if quest.isabelle_tour in ("gym","crash"):
          self.location = "school_gym"
          self.activity = "standing"
          return

        if quest.isabelle_tour == "art_class":
          self.location = "school_art_class"
          self.activity = "standing"
          return

        if quest.isabelle_tour == "english_class":
          self.location = "school_english_class"
          self.activity = "sitting"
          return

        if quest.isabelle_tour >= "go_upstairs" and quest.isabelle_tour < "english_class":
          self.location = "school_first_hall"
          self.activity = "standing"
          return

        if quest.isabelle_tour == "beginning":
          self.location = "school_ground_floor"
          self.activity = "standing"
          return

      if not quest.isabelle_tour.started:
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      ###Isabelle Tour####

      ###Gathering Storm
      if quest.isabelle_locker in ("catnap","scent","repeat") and not quest.kate_desire == "bathroomhelp":
        self.location = None
        self.activity = None
        return

      if quest.isabelle_locker in ("bedroom","email","wait") and not quest.isabelle_locker["sleep"]:
        self.location = "home_bedroom"
        self.activity = "standing"
        return
      ###Gathering Storm

      ###Hooking Up
      if quest.maxine_hook in ("day","interrogation"):
        self.location = "school_cafeteria"
        self.activity = "sitting"
        return
      ###Hooking Up

      ###Chops and Nocs
      if quest.isabelle_piano in ("concert","score"):
        self.location = "school_first_hall_west"
        self.activity = "standing"
        return
      ###Chops and Nocs

      ###Paint It Red
      if quest.isabelle_red == "black_note" and game.hour in (10,11):
        self.location = "school_first_hall"
        self.activity = "standing"
        return
      ###Paint It Red

      ###Hurricane Isabelle
      if quest.isabelle_hurricane == "call_maxine":
        self.location = "school_forest_glade"
        self.activity = "standing"
        return
      ###Hurricane Isabelle

      ###Dethroning the Queen
      if quest.isabelle_dethroning == "announcement" and game.hour in (7,8):
        self.location = "school_cafeteria"
        self.activity = "sitting"
        return
      ###Dethroning the Queen

      ###Fall in Newfall
      if quest.fall_in_newfall.in_progress and quest.fall_in_newfall < "stick_around":
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      ###Fall in Newfall

      ###Fall in Newfall
      if (quest.fall_in_newfall.finished and not quest.maya_witch.started) and game.hour in (15,16):
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      ###Fall in Newfall

      ###A Single Moment
      if quest.kate_moment == "fun" and game.hour == 19 and quest.maya_sauce["bedroom_taken_over"]:
        self.location = "home_hall"
        self.activity = "standing"
        return
      elif quest.kate_moment == "fun" and game.hour == 19:
        self.location = "home_bedroom"
        self.activity = "standing"
        return
      ###A Single Moment

      ###Spell You Later
      if quest.maya_spell == "sweat" and game.hour in (13,14) and quest.jacklyn_statement["ending"] in ("anal","pegging"):
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      elif quest.maya_spell == "frogs" and game.hour == 18:
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      elif quest.maya_spell == "maya" and game.hour == 12:
        self.location = "school_gym"
        self.activity = "standing"
        return
      ###Spell You Later

      ###A Walk in the Park
      if quest.flora_walk in ("caught","forest_glade"):
        self.location = "school_forest_glade"
        self.activity = "standing"
        return
      ###A Walk in the Park

      ###Vicious Intrigue
      if quest.kate_intrigue == "eavesdrop":
        if "school_cafeteria" not in quest.kate_intrigue["eavesdropped_locations"]:
          self.location = "school_cafeteria"
          self.activity = "sitting"
          return
        elif "school_first_hall" not in quest.kate_intrigue["eavesdropped_locations"]:
          self.location = "school_first_hall"
          self.activity = "standing"
          return
        elif "school_gym" not in quest.kate_intrigue["eavesdropped_locations"]:
          self.location = "school_gym"
          self.activity = "standing"
          return
        elif "school_ground_floor" not in quest.kate_intrigue["eavesdropped_locations"]:
          self.location = "school_ground_floor"
          self.activity = "standing"
          return
        else:
          self.location = "school_english_class"
          self.activity = "sitting"
          return
      elif quest.kate_intrigue == "spy":
        self.location = "school_english_class"
        self.activity = "sitting"
        return
      elif quest.kate_intrigue == "report" and game.hour == 12:
        self.location = "school_english_class"
        self.activity = "sitting"
        return
      ###Vicious Intrigue

      ###The Grand Gesture
      if quest.isabelle_gesture == "letter" and game.hour in (10,11):
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      elif quest.isabelle_gesture == "revenge":
        self.location = "school_english_class"
        return
      ###The Grand Gesture

      ###Hot My Bot
      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return
      ###Hot My Bot

      ###Service Aid
      if quest.nurse_aid in ("fishing","bait") and game.hour == 18:
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      ###Service Aid

      ###Forced Locations

      ###ALT Locations
      if quest.isabelle_tour == "cafeteria":
        self.alternative_locations["school_cafeteria"] = "sitting"

      if quest.isabelle_tour.finished and (quest.kate_over_isabelle.started and not quest.kate_over_isabelle.failed) and sum([mc.owned_item("ice_tea"),mc.owned_item("sleeping_pills"),mc.owned_item("handcuffs")]) >= 2 and not quest.kate_trick.started:
        self.alternative_locations["school_first_hall"] = "standing"

      if quest.kate_stepping in ("isabelle","phone_call"):
        self.alternative_locations["school_english_class"] = "sitting"

      if quest.isabelle_dethroning == "announcement":
        self.alternative_locations["school_cafeteria"] = "sitting"
      elif (quest.isabelle_dethroning == "dinner" and game.hour == 19) or quest.isabelle_dethroning == "revenge_done" or quest.isabelle_dethroning["kitchen_shenanigans_replay"]:
        self.alternative_locations["school_cafeteria"] = "dinner"

      if isabelle["reading_this_scene"]:
        self.alternative_locations["school_english_class"] = "sitting"

      if quest.maya_sauce == "classes":
        self.alternative_locations["school_homeroom"] = "sitting"
      ###ALT Locations

      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):

        if game.hour in (7,8):
          self.location = "school_ground_floor"
          self.activity = "standing"
          return

        elif game.hour == (9):
          self.location = "school_gym"
          self.activity = "standing"
          return

        elif game.hour in (10,11):
          self.location = "school_english_class"
          self.activity = "sitting"
          return

        elif game.hour == 12:
          self.location = "school_cafeteria"
          self.activity = "sitting"
          return

        elif game.hour in (13,14):
          self.location = "school_art_class"
          self.activity = "standing"
          return

        elif game.hour in (15,16):
          self.location = "school_first_hall"
          self.activity = "standing"
          return

        elif game.hour == 17:
          self.location = "school_homeroom"
          self.activity = "sitting"
          return

        elif game.hour == 18:
          if game.season == 1 and isabelle["roof"]:
            self.location = "school_roof"
            self.activity = "standing"
          elif school_forest_glade["unlocked"]:
            self.location = "school_forest_glade"
            self.activity = "standing"
          else:
            self.location = "school_homeroom"
            self.activity = "sitting"
          return

        #else:
        #  self.location=None
        #  self.activity=None
        #  self.alternative_locations={}
      ### Schedule

    def call_label(self):
      if mc["focus"]:
        if mc["focus"] == "isabelle_hurricane":
          if quest.isabelle_hurricane == "call_isabelle":
            return "quest_isabelle_hurricane_call_isabelle"
      else:
        if quest.isabelle_buried == "callforhelp":
          return "quest_isabelle_buried_callforhelp_isabelle"
      return None

    def message_label(self):
      if mc["focus"]:
        if mc["focus"] == "kate_moment":
          if quest.kate_moment == "text":
            return "quest_kate_moment_text"
      else:
        if quest.isabelle_stars == "text":
          return "quest_isabelle_stars_text"
      return None

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("isabelle avatar "+state,True):
          return "isabelle avatar "+state
      rv=[]

      if state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body1",42,134))
        if "_glasses" in state:
          rv.append(("isabelle avatar b1glasses",159,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b1tiara",175,137))
        if "_left" in state:
          rv.append(("isabelle avatar face_concerned_left",192,229))
        elif "_letter" in state:
          rv.append(("isabelle avatar face_concerned_letter",187,236))
        else:
          rv.append(("isabelle avatar face_concerned",192,228))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b1green_panties",152,778))
        elif "_panties" in state:
          rv.append(("isabelle avatar b1panty",166,715))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b1green_bra",134,428))
        elif "_bra" in state:
          rv.append(("isabelle avatar b1bra",134,526))
        if "_grimy" in state:
          rv.append(("isabelle avatar body1_grimy",42,438))
        if "_shirt" in state:
          rv.append(("isabelle avatar b1tucked",89,410))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b1party_dress",38,391))
        if "_spinach" in state:
          if "_holdingpanties" in state:
            rv.append(("isabelle avatar b1arm3_panties_n",7,366))
            rv.append(("isabelle avatar b1arm3_spinach_n",33,515))
          else:
            rv.append(("isabelle avatar b1arm3_n",87,407))
        elif "_grimy" in state:
          rv.append(("isabelle avatar b1arm2_n_grimy",88,452))
        elif "_phone" in state:
          rv.append(("isabelle avatar b1phone_n",71,295))
        elif "_letter" in state:
          rv.append(("isabelle avatar b1letter_n",81,388))
        else:
          rv.append(("isabelle avatar b1arm2_n",88,452))
        if "_pants" in state:
          rv.append(("isabelle avatar b1pants",42,735))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b1skirt",83,736))
        if "_shirt" in state:
          if "_spinach" in state:
            if "_holdingpanties" in state:
              rv.append(("isabelle avatar b1arm3_panties_c",7,366))
              rv.append(("isabelle avatar b1arm3_spinach_c",33,515))
            else:
              rv.append(("isabelle avatar b1arm3_c",87,407))
          else:
            rv.append(("isabelle avatar b1arm2_c",88,453))
        elif "_top" in state:
          rv.append(("isabelle avatar b1top",132,409))
        if "_jacket" in state:
          rv.append(("isabelle avatar b1jacket",88,408))
          if "_phone" in state:
            rv.append(("isabelle avatar b1phone_jacket",68,295))
          elif "_letter" in state:
            rv.append(("isabelle avatar b1letter_jacket",76,388))
          else:
            rv.append(("isabelle avatar b1arm2_jacket",85,452))
        if "_collar" in state:
          rv.append(("isabelle avatar b1collar",252,389))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body1",42,134))
        if "_glasses" in state:
          rv.append(("isabelle avatar b1glasses",159,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b1tiara",175,137))
        if "_letter" in state:
          rv.append(("isabelle avatar face_blush_letter",186,227))
        else:
          rv.append(("isabelle avatar face_excited",192,223))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b1green_panties",152,778))
        elif "_panties" in state:
          rv.append(("isabelle avatar b1panty",166,715))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b1green_bra",134,428))
        elif "_bra" in state:
          rv.append(("isabelle avatar b1bra",134,526))
        if "_grimy" in state:
          rv.append(("isabelle avatar body1_grimy",42,438))
        if "_shirt" in state:
          rv.append(("isabelle avatar b1tucked",89,410))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b1party_dress",38,391))
        if "_spinach" in state:
          if "_holdingpanties" in state:
            rv.append(("isabelle avatar b1arm3_panties_n",7,366))
            rv.append(("isabelle avatar b1arm3_spinach_n",33,515))
          else:
            rv.append(("isabelle avatar b1arm3_n",87,407))
        elif "_grimy" in state:
          rv.append(("isabelle avatar b1arm1_n_grimy",88,650))
        elif "_letter" in state:
          rv.append(("isabelle avatar b1letter_n",81,388))
        else:
          rv.append(("isabelle avatar b1arm1_n",88,650))
        if "_pants" in state:
          rv.append(("isabelle avatar b1pants",42,735))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b1skirt",83,736))
        if "_shirt" in state:
          if "_spinach" in state:
            if "_holdingpanties" in state:
              rv.append(("isabelle avatar b1arm3_panties_c",7,366))
              rv.append(("isabelle avatar b1arm3_spinach_c",33,515))
            else:
              rv.append(("isabelle avatar b1arm3_c",87,407))
          else:
            rv.append(("isabelle avatar b1arm1_c",88,650))
        elif "_top" in state:
          rv.append(("isabelle avatar b1top",132,409))
        if "_jacket" in state:
          rv.append(("isabelle avatar b1jacket",88,408))
          if "_letter" in state:
            rv.append(("isabelle avatar b1letter_jacket",76,388))
          else:
            rv.append(("isabelle avatar b1arm1_jacket",82,630))
        if "_collar" in state:
          rv.append(("isabelle avatar b1collar",252,389))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body1",42,134))
        if "_glasses" in state:
          rv.append(("isabelle avatar b1glasses",159,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b1tiara",175,137))
        rv.append(("isabelle avatar face_neutral",192,225))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b1green_panties",152,778))
        elif "_panties" in state:
          rv.append(("isabelle avatar b1panty",166,715))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b1green_bra",134,428))
        elif "_bra" in state:
          rv.append(("isabelle avatar b1bra",134,526))
        if "_grimy" in state:
          rv.append(("isabelle avatar body1_grimy",42,438))
        if "_shirt" in state:
          rv.append(("isabelle avatar b1tucked",89,410))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b1party_dress",38,391))
        if "_spinach" in state:
          if "_holdingpanties" in state:
            rv.append(("isabelle avatar b1arm3_panties_n",7,366))
            rv.append(("isabelle avatar b1arm3_spinach_n",33,515))
          else:
            rv.append(("isabelle avatar b1arm3_n",87,407))
        elif "_grimy" in state:
          rv.append(("isabelle avatar b1arm1_n_grimy",88,650))
        else:
          rv.append(("isabelle avatar b1arm1_n",88,650))
        if "_pants" in state:
          rv.append(("isabelle avatar b1pants",42,735))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b1skirt",83,736))
        if "_shirt" in state:
          if "_spinach" in state:
            if "_holdingpanties" in state:
              rv.append(("isabelle avatar b1arm3_panties_c",7,366))
              rv.append(("isabelle avatar b1arm3_spinach_c",33,515))
            else:
              rv.append(("isabelle avatar b1arm3_c",87,407))
          else:
            rv.append(("isabelle avatar b1arm1_c",88,650))
        elif "_top" in state:
          rv.append(("isabelle avatar b1top",132,409))
        if "_jacket" in state:
          rv.append(("isabelle avatar b1jacket",88,408))
          rv.append(("isabelle avatar b1arm1_jacket",82,630))
        if "_collar" in state:
          rv.append(("isabelle avatar b1collar",252,389))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body1",42,134))
        if "_glasses" in state:
          rv.append(("isabelle avatar b1glasses",159,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b1tiara",175,137))
        if "_left" in state:
          rv.append(("isabelle avatar face_skeptical_left",187,229))
        elif "_letter" in state:
          rv.append(("isabelle avatar face_skeptical_letter",192,229))
        else:
          rv.append(("isabelle avatar face_skeptical",187,229))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b1green_panties",152,778))
        elif "_panties" in state:
          rv.append(("isabelle avatar b1panty",166,715))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b1green_bra",134,428))
        elif "_bra" in state:
          rv.append(("isabelle avatar b1bra",134,526))
        if "_shirt" in state:
          rv.append(("isabelle avatar b1tucked",89,410))
        if "_spinach" in state:
          rv.append(("isabelle avatar b1arm3_n",87,407))
        elif "_letter" in state:
          rv.append(("isabelle avatar b1letter_n",81,388))
        else:
          rv.append(("isabelle avatar b1arm2_n",88,452))
        if "_pants" in state:
          rv.append(("isabelle avatar b1pants",42,735))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b1skirt",83,736))
        if "_shirt" in state:
          if "_spinach" in state:
            rv.append(("isabelle avatar b1arm3_c",87,407))
          else:
            rv.append(("isabelle avatar b1arm2_c",88,453))
        elif "_top" in state:
          rv.append(("isabelle avatar b1top",132,409))
        if "_jacket" in state:
          rv.append(("isabelle avatar b1jacket",88,408))
          if "_letter" in state:
            rv.append(("isabelle avatar b1letter_jacket",76,388))
          else:
            rv.append(("isabelle avatar b1arm2_jacket",85,452))
        if "_collar" in state:
          rv.append(("isabelle avatar b1collar",252,389))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        if "_phone" in state:
          rv.append(("isabelle avatar body2_armless",103,130))
        else:
          rv.append(("isabelle avatar body2",103,130))
        if "_glasses" in state and "_pig" not in state:
          rv.append(("isabelle avatar b2glasses",136,149))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b2tiara",169,149))
        rv.append(("isabelle avatar face_confident",193,216))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b2green_panties",120,780))
        elif "_panties" in state:
          rv.append(("isabelle avatar b2panty",132,742))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b2green_bra",164,432))
        elif "_bra" in state:
          rv.append(("isabelle avatar b2bra",165,531))
        if "_party_dress" not in state and "_phone" not in state:
          rv.append(("isabelle avatar b2arm_n",65,584))
        if "_shirt" in state:
          if "_untucked" in state:
            rv.append(("isabelle avatar b2untucked",145,398))
          elif "_phone" in state:
            rv.append(("isabelle avatar b2tucked_armless",146,398))
          else:
            rv.append(("isabelle avatar b2tucked",145,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b2party_dress",87,394))
          rv.append(("isabelle avatar b2arm_party_dress",67,537))
        if "_phone" in state:
          rv.append(("isabelle avatar b2arm_phone_n",147,403))
        if "_pants" in state:
          rv.append(("isabelle avatar b2pants",104,713))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b2skirt",95,717))
        if "_shirt" in state:
          if "_phone" in state:
            rv.append(("isabelle avatar b2arm_phone_c",147,507))
          else:
            rv.append(("isabelle avatar b2arm_c",65,584))
        elif "_top" in state:
          rv.append(("isabelle avatar b2top",165,398))
        if "_jacket" in state:
          rv.append(("isabelle avatar b2jacket",140,398))
          rv.append(("isabelle avatar b2arm_jacket",58,574))
        if "_collar" in state:
          rv.append(("isabelle avatar b2collar",282,382))
        if "_pig" in state:
          if "less" in state:
            rv.append(("isabelle avatar b2blanket2",12,126))
            rv.append(("isabelle avatar b2pig_mask2",128,102))
          else:
            rv.append(("isabelle avatar b2blanket1",6,128))
            rv.append(("isabelle avatar b2pig_mask1",160,167))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body2",103,130))
        if "_glasses" in state:
          rv.append(("isabelle avatar b2glasses",136,149))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b2tiara",169,149))
        rv.append(("isabelle avatar face_eyeroll",204,220))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b2green_panties",120,780))
        elif "_panties" in state:
          rv.append(("isabelle avatar b2panty",132,742))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b2green_bra",164,432))
        elif "_bra" in state:
          rv.append(("isabelle avatar b2bra",165,531))
        if "_shirt" in state:
          rv.append(("isabelle avatar b2tucked",145,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b2party_dress",87,394))
          if "_pigless" in state or "_hand_on_hip" in state:
            rv.append(("isabelle avatar b2arm_party_dress",67,537))
        if "_pants" in state:
          rv.append(("isabelle avatar b2pants",104,713))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b2skirt",95,717))
        if "_top" in state:
          rv.append(("isabelle avatar b2top",165,398))
        if "_jacket" in state:
          rv.append(("isabelle avatar b2jacket",140,398))
        if "_collar" in state:
          rv.append(("isabelle avatar b2collar",282,382))
        if "_pigless" in state:
          rv.append(("isabelle avatar b2blanket2",12,126))
          rv.append(("isabelle avatar b2pig_mask2",128,102))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body2",103,130))
        if "_glasses" in state and "_pig" not in state:
          rv.append(("isabelle avatar b2glasses",136,149))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b2tiara",169,149))
        rv.append(("isabelle avatar face_laughing",197,232))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b2green_panties",120,780))
        elif "_panties" in state:
          rv.append(("isabelle avatar b2panty",132,742))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b2green_bra",164,432))
        elif "_bra" in state:
          rv.append(("isabelle avatar b2bra",165,531))
        if "_party_dress" not in state:
          rv.append(("isabelle avatar b2arm_n",65,584))
        if "_shirt" in state:
          if "_untucked" in state:
            rv.append(("isabelle avatar b2untucked",145,398))
          else:
            rv.append(("isabelle avatar b2tucked",145,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b2party_dress",87,394))
          rv.append(("isabelle avatar b2arm_party_dress",67,537))
        if "_pants" in state:
          rv.append(("isabelle avatar b2pants",104,713))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b2skirt",95,717))
        if "_shirt" in state:
          rv.append(("isabelle avatar b2arm_c",65,584))
        elif "_top" in state:
          rv.append(("isabelle avatar b2top",165,398))
        if "_jacket" in state:
          rv.append(("isabelle avatar b2jacket",140,398))
          rv.append(("isabelle avatar b2arm_jacket",58,574))
        if "_collar" in state:
          rv.append(("isabelle avatar b2collar",282,382))
        if "_pig" in state:
          if "less" in state:
            rv.append(("isabelle avatar b2blanket2",12,126))
            rv.append(("isabelle avatar b2pig_mask2",128,102))
          else:
            rv.append(("isabelle avatar b2blanket1",6,128))
            rv.append(("isabelle avatar b2pig_mask1",160,167))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body2",103,130))
        if "_glasses" in state:
          rv.append(("isabelle avatar b2glasses",136,149))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b2tiara",169,149))
        if "_left" in state:
          rv.append(("isabelle avatar face_smile_left",204,223))
        else:
          rv.append(("isabelle avatar face_smile",204,222))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b2green_panties",120,780))
        elif "_panties" in state:
          rv.append(("isabelle avatar b2panty",132,742))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b2green_bra",164,432))
        elif "_bra" in state:
          rv.append(("isabelle avatar b2bra",165,531))
        if "_shirt" in state:
          rv.append(("isabelle avatar b2tucked",145,398))
        if "_pants" in state:
          rv.append(("isabelle avatar b2pants",104,713))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b2skirt",95,717))
        if "_top" in state:
          rv.append(("isabelle avatar b2top",165,398))
        if "_jacket" in state:
          rv.append(("isabelle avatar b2jacket",140,398))
        if "_collar" in state:
          rv.append(("isabelle avatar b2collar",282,382))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body3",90,135))
        if "_glasses" in state:
          rv.append(("isabelle avatar b3glasses",173,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b3tiara",193,143))
        if "_left" in state:
          rv.append(("isabelle avatar face_afraid_left",209,238))
        else:
          rv.append(("isabelle avatar face_afraid",209,238))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b3green_panties",158,774))
        elif "_panties" in state:
          rv.append(("isabelle avatar b3panty",179,718))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b3green_bra",153,429))
        elif "_bra" in state:
          rv.append(("isabelle avatar b3bra",153,489))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3tucked",101,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3party_dress",86,398))
        rv.append(("isabelle avatar b3arm1_n",98,547))
        if "_pants" in state:
          rv.append(("isabelle avatar b3pants",90,725))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b3skirt",104,738))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3arm1_c",97,547))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3arm1_party_dress",179,623))
        elif "_top" in state:
          rv.append(("isabelle avatar b3top",153,403))
        if "_collar" in state:
          rv.append(("isabelle avatar b3collar",259,395))
        if "_jacket" in state:
          rv.append(("isabelle avatar b3jacket",100,401))
          rv.append(("isabelle avatar b3arm1_jacket",92,547))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body3",90,135))
        if "_glasses" in state:
          rv.append(("isabelle avatar b3glasses",173,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b3tiara",193,143))
        if "_left" in state:
          rv.append(("isabelle avatar face_cringe_left",209,242))
        else:
          rv.append(("isabelle avatar face_cringe",209,241))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b3green_panties",158,774))
        elif "_panties" in state:
          rv.append(("isabelle avatar b3panty",179,718))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b3green_bra",153,429))
        elif "_bra" in state:
          rv.append(("isabelle avatar b3bra",153,489))
        if "_grimy" in state:
          rv.append(("isabelle avatar body3_grimy_n",90,422))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3tucked",101,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3party_dress",86,398))
        if "_grimy" in state:
          rv.append(("isabelle avatar b3arm1_n_grimy",98,541))
        else:
          rv.append(("isabelle avatar b3arm1_n",98,547))
        if "_pants" in state:
          rv.append(("isabelle avatar b3pants",90,725))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b3skirt",104,738))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3arm1_c",97,547))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3arm1_party_dress",179,623))
        elif "_top" in state:
          rv.append(("isabelle avatar b3top",153,403))
        if "_collar" in state:
          rv.append(("isabelle avatar b3collar",259,395))
        if "_jacket" in state:
          rv.append(("isabelle avatar b3jacket",100,401))
          rv.append(("isabelle avatar b3arm1_jacket",92,547))
        elif "_top" in state:
          rv.append(("isabelle avatar b3arm1_n",98,547))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body3",90,135))
        if "_glasses" in state:
          rv.append(("isabelle avatar b3glasses",173,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b3tiara",193,143))
        rv.append(("isabelle avatar face_flirty",202,241))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b3green_panties",158,774))
        elif "_panties" in state:
          rv.append(("isabelle avatar b3panty",179,718))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b3green_bra",153,429))
        elif "_bra" in state:
          rv.append(("isabelle avatar b3bra",153,489))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3tucked",101,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3party_dress",86,398))
        if "_holdingpanties" in state:
          rv.append(("isabelle avatar b3arm2_panties_n",24,342))
        else:
          rv.append(("isabelle avatar b3arm2_n",83,359))
        if "_pants" in state:
          rv.append(("isabelle avatar b3pants",90,725))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b3skirt",104,738))
        if "_shirt" in state:
          if "_holdingpanties" in state:
            rv.append(("isabelle avatar b3arm2_panties_c",24,342))
          else:
            rv.append(("isabelle avatar b3arm2_c",82,359))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3arm2_party_dress",178,621))
        elif "_top" in state:
          rv.append(("isabelle avatar b3top",153,403))
        if "_collar" in state:
          rv.append(("isabelle avatar b3collar",259,395))
        if "_jacket" in state:
          rv.append(("isabelle avatar b3jacket",100,401))
          rv.append(("isabelle avatar b3arm2_jacket",80,359))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body3",90,135))
        if "_glasses" in state:
          rv.append(("isabelle avatar b3glasses",173,155))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b3tiara",193,143))
        rv.append(("isabelle avatar face_thinking",209,244))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b3green_panties",158,774))
        elif "_panties" in state:
          rv.append(("isabelle avatar b3panty",179,718))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b3green_bra",153,429))
        elif "_bra" in state:
          rv.append(("isabelle avatar b3bra",153,489))
        if "_grimy" in state:
          rv.append(("isabelle avatar body3_grimy_n",90,422))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3tucked",101,398))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3party_dress",86,398))
        if "_grimy" in state:
          rv.append(("isabelle avatar b3arm2_n_grimy",82,359))
        else:
          rv.append(("isabelle avatar b3arm2_n",83,359))
        if "_pants" in state:
          rv.append(("isabelle avatar b3pants",90,725))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b3skirt",104,738))
        if "_shirt" in state:
          rv.append(("isabelle avatar b3arm2_c",82,359))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b3arm2_party_dress",178,621))
        elif "_top" in state:
          rv.append(("isabelle avatar b3top",153,403))
        if "_collar" in state:
          rv.append(("isabelle avatar b3collar",259,395))
        if "_jacket" in state:
          rv.append(("isabelle avatar b3jacket",100,401))
          rv.append(("isabelle avatar b3arm2_jacket",80,359))

      elif state.startswith(("angry","angrier")):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body4",91,136))
        if "_glasses" in state and "_pig" not in state:
          rv.append(("isabelle avatar b4glasses",205,170))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b4tiara",218,143))
        if state.startswith("angrier"):
          rv.append(("isabelle avatar face_angrier",237,247))
        elif "_left" in state:
          rv.append(("isabelle avatar face_angry_left",237,242))
        else:
          rv.append(("isabelle avatar face_angry",237,240))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b4green_panties",165,771))
        elif "_panties" in state:
          rv.append(("isabelle avatar b4panty",215,739))
        if "_bra" in state:
          rv.append(("isabelle avatar b4arm2_n",135,527))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b4green_bra",161,422))
        elif "_bra" in state:
          rv.append(("isabelle avatar b4bra",160,542))
        if "_shirt" in state:
          rv.append(("isabelle avatar b4tucked",115,397))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4party_dress",86,384))
        if "_holdingpanties" in state:
          rv.append(("isabelle avatar b4arm2_panties_n",92,531))
        elif "_bra" not in state:
          rv.append(("isabelle avatar b4arm2_n",135,527))
        if "_pants" in state:
          rv.append(("isabelle avatar b4pants",91,704))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b4skirt",108,727))
        if "_shirt" in state:
          if "_holdingpanties" in state:
            rv.append(("isabelle avatar b4arm2_panties_c",92,531))
          else:
            rv.append(("isabelle avatar b4arm2_c",135,522))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4arm2_n_fix",135,647))
          rv.append(("isabelle avatar b4arm2_party_dress",246,685))
        elif "_top" in state:
          rv.append(("isabelle avatar b4top",124,400))
        if "_jacket" in state:
          rv.append(("isabelle avatar b4jacket",110,398))
          rv.append(("isabelle avatar b4arm2_jacket",135,512))
        if "_collar" in state:
          rv.append(("isabelle avatar b4collar",234,389))
        if "_pig" in state:
          if "less" in state:
            rv.append(("isabelle avatar b4blanket2a",4,126))
            rv.append(("isabelle avatar b4pig_mask2",204,102))
          else:
            rv.append(("isabelle avatar b4blanket1",6,131))
            rv.append(("isabelle avatar b4pig_mask1",199,177))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body4",91,136))
        if "_glasses" in state:
          rv.append(("isabelle avatar b4glasses",205,170))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b4tiara",218,143))
        if "_left" in state:
          rv.append(("isabelle avatar face_annoyed_left",250,242))
        else:
          rv.append(("isabelle avatar face_annoyed",250,242))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b4green_panties",165,771))
        elif "_panties" in state:
          rv.append(("isabelle avatar b4panty",215,739))
        if "_grimy" in state:
          rv.append(("isabelle avatar body4_grimy",88,408))
          rv.append(("isabelle avatar b4arm2_n_grimy",133,522))
          if "_green_bra" in state:
            rv.append(("isabelle avatar b4green_bra_grimy",161,422))
        else:
          if "_bra" in state:
            rv.append(("isabelle avatar b4arm2_n",135,527))
          if "_green_bra" in state:
            rv.append(("isabelle avatar b4green_bra",161,422))
          elif "_bra" in state:
            rv.append(("isabelle avatar b4bra",160,542))
        if "_shirt" in state:
          rv.append(("isabelle avatar b4tucked",115,397))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4party_dress",86,384))
        if "_holdingpanties" in state:
          rv.append(("isabelle avatar b4arm2_panties_n",92,531))
        elif "_phone" in state:
          rv.append(("isabelle avatar b4phone_n",360,270))
        elif "_bra" not in state:
          rv.append(("isabelle avatar b4arm2_n",135,527))
        if "_pants" in state:
          rv.append(("isabelle avatar b4pants",91,704))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b4skirt",108,727))
        if "_shirt" in state:
          if "_holdingpanties" in state:
            rv.append(("isabelle avatar b4arm2_panties_c",92,531))
          else:
            rv.append(("isabelle avatar b4arm2_c",135,522))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4arm2_n_fix",135,647))
          rv.append(("isabelle avatar b4arm2_party_dress",246,685))
        elif "_top" in state:
          rv.append(("isabelle avatar b4top",124,400))
        if "_jacket" in state:
          rv.append(("isabelle avatar b4jacket",110,398))
          if "_phone" in state:
            rv.append(("isabelle avatar b4phone_jacket",360,270))
          else:
            rv.append(("isabelle avatar b4arm2_jacket",135,512))
        if "_collar" in state:
          rv.append(("isabelle avatar b4collar",234,389))
        if "_pig" in state:
          if "less" in state:
            rv.append(("isabelle avatar b4blanket2a",4,126))
            rv.append(("isabelle avatar b4pig_mask2",204,102))
          else:
            rv.append(("isabelle avatar b4blanket1",6,131))
            rv.append(("isabelle avatar b4pig_mask1",199,177))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body4",91,136))
        if "_glasses" in state:
          rv.append(("isabelle avatar b4glasses",205,170))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b4tiara",218,143))
        rv.append(("isabelle avatar face_blush",224,239))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b4green_panties",165,771))
        elif "_panties" in state:
          rv.append(("isabelle avatar b4panty",215,739))
        if "_bra" in state:
          rv.append(("isabelle avatar b4arm1_n",266,511))
        if "_green_bra" in state:
          rv.append(("isabelle avatar b4green_bra",161,422))
        elif "_bra" in state:
          rv.append(("isabelle avatar b4bra",160,542))
        if "_shirt" in state:
          rv.append(("isabelle avatar b4tucked",115,397))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4party_dress",86,384))
        if "_bra" not in state:
          rv.append(("isabelle avatar b4arm1_n",266,511))
        if "_pants" in state:
          rv.append(("isabelle avatar b4pants",91,704))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b4skirt",108,727))
        if "_shirt" in state:
          rv.append(("isabelle avatar b4arm1_c",266,553))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4arm1_n_fix",266,629))
          rv.append(("isabelle avatar b4arm1_party_dress",320,899))
        elif "_top" in state:
          rv.append(("isabelle avatar b4top",124,400))
        if "_jacket" in state:
          rv.append(("isabelle avatar b4jacket",110,398))
          rv.append(("isabelle avatar b4arm1_jacket",266,549))
        if "_collar" in state:
          rv.append(("isabelle avatar b4collar",234,389))
        if "_pigless" in state:
          rv.append(("isabelle avatar b4blanket2b",4,126))
          rv.append(("isabelle avatar b4pig_mask2",204,102))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body4",91,136))
        if "_glasses" in state and "_pig" not in state:
          rv.append(("isabelle avatar b4glasses",205,170))
        elif "_tiara" in state:
          rv.append(("isabelle avatar b4tiara",218,143))
        rv.append(("isabelle avatar face_sad",227,239))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b4green_panties",165,771))
        elif "_panties" in state:
          rv.append(("isabelle avatar b4panty",215,739))
        if "_grimy" in state:
          rv.append(("isabelle avatar body4_grimy",88,408))
          rv.append(("isabelle avatar b4arm1_n_grimy",265,539))
          if "_green_bra" in state:
            rv.append(("isabelle avatar b4green_bra_grimy",161,422))
        else:
          if "_phone" in state:
            rv.append(("isabelle avatar b4phone_n",360,270))
          elif "_bra" in state:
            rv.append(("isabelle avatar b4arm1_n",266,511))
          if "_green_bra" in state:
            rv.append(("isabelle avatar b4green_bra",161,422))
          elif "_bra" in state:
            rv.append(("isabelle avatar b4bra",160,542))
        if "_shirt" in state:
          rv.append(("isabelle avatar b4tucked",115,397))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4party_dress",86,384))
        if "_bra" not in state:
          rv.append(("isabelle avatar b4arm1_n",266,511))
        if "_pants" in state:
          rv.append(("isabelle avatar b4pants",91,704))
        elif "_skirt" in state:
          rv.append(("isabelle avatar b4skirt",108,727))
        if "_shirt" in state:
          rv.append(("isabelle avatar b4arm1_c",266,553))
        elif "_party_dress" in state:
          rv.append(("isabelle avatar b4arm1_n_fix",266,629))
          rv.append(("isabelle avatar b4arm1_party_dress",320,899))
        elif "_top" in state:
          rv.append(("isabelle avatar b4top",124,400))
        if "_jacket" in state:
          rv.append(("isabelle avatar b4jacket",110,398))
          if "_phone" in state:
            rv.append(("isabelle avatar b4phone_jacket",360,270))
          else:
            rv.append(("isabelle avatar b4arm1_jacket",266,549))
        if "_collar" in state:
          rv.append(("isabelle avatar b4collar",234,389))
        if "_pig" in state:
          rv.append(("isabelle avatar b4blanket1",6,131))
          rv.append(("isabelle avatar b4pig_mask1",199,177))

      elif state.startswith("subby"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("isabelle avatar body4",91,136))
        if "_tiara" in state:
          rv.append(("isabelle avatar b4tiara",218,143))
        rv.append(("isabelle avatar face_subby",227,239))
        if "_green_panties" in state:
          rv.append(("isabelle avatar b4green_panties",165,771))
        if "_grimy" in state:
          rv.append(("isabelle avatar body4_grimy_n",88,408))
          rv.append(("isabelle avatar b4arm1_n_grimy",265,539))
          if "_green_bra" in state:
            rv.append(("isabelle avatar b4green_bra_grimy",161,422))
        else:
          if "_phone" in state:
            rv.append(("isabelle avatar b4phone_n",360,270))
          else:
            rv.append(("isabelle avatar b4arm1_n",266,511))
          if "_green_bra" in state:
            rv.append(("isabelle avatar b4green_bra",161,422))
        if "_skirt" in state:
          rv.append(("isabelle avatar b4skirt",108,727))
        if "_top" in state:
          rv.append(("isabelle avatar b4top",124,400))
        if "_jacket" in state:
          rv.append(("isabelle avatar b4jacket",110,398))
          if "_phone" in state:
            rv.append(("isabelle avatar b4phone_jacket",360,270))
          else:
            rv.append(("isabelle avatar b4arm1_jacket",266,549))
        if "_collar" in state:
          rv.append(("isabelle avatar b4collar",234,389))

      elif state=="isabelle_hug":
        rv.append((1920,1080))
        rv.append(("isabelle avatar events isahugkiss hugkiss_bg_hug",0,0))
        rv.append(("isabelle avatar events isahugkiss hugkiss_mc",210,0))
        rv.append(("isabelle avatar events isahugkiss hugkiss_isa_n",246,35))
        rv.append(("isabelle avatar events isahugkiss hugkiss_isa_c",244,495))
        rv.append(("isabelle avatar events isahugkiss hugkiss_tears",828,476))
      elif state=="isabelle_kiss":
        rv.append((1920,1080))
        rv.append(("isabelle avatar events isahugkiss hugkiss_bg_kiss",0,0))
        rv.append(("isabelle avatar events isahugkiss hugkiss_isa_n_kiss",789,76))
        rv.append(("isabelle avatar events isahugkiss hugkiss_isa_c_kiss",849,865))
        rv.append(("isabelle avatar events isahugkiss hugkiss_tears_kiss",913,338))
        rv.append(("isabelle avatar events isahugkiss hugkiss_mc_kiss",210,0))
        rv.append(("isabelle avatar events isahugkiss hugkiss_hand",830,280))
        rv.append(("isabelle avatar events isahugkiss hugkiss_hand_c",849,654))

      elif state.startswith("straddling"):
        rv.append((1920,1080))
        if state.endswith(("gaze","kiss")):
          if home_bedroom["clean"]:
            rv.append(("isabelle avatar events straddling kiss bg_clean",0,0))
          else:
            rv.append(("isabelle avatar events straddling kiss bg_dirty",0,0))
          rv.append(("isabelle avatar events straddling kiss mc_body",468,555))
          if "gaze" in state:
            rv.append(("isabelle avatar events straddling kiss mc_head1",571,260))
          elif "kiss" in state:
            rv.append(("isabelle avatar events straddling kiss mc_head2",543,287))
          rv.append(("isabelle avatar events straddling kiss isa_legs_n",864,606))
          rv.append(("isabelle avatar events straddling kiss isa_legs_u",906,682))
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            pass
          else:
            rv.append(("isabelle avatar events straddling kiss isa_legs_c",864,603))
          rv.append(("isabelle avatar events straddling kiss mc_right_arm",931,924))
          if "gaze" in state:
            rv.append(("isabelle avatar events straddling kiss isa_body1_n",555,129))
            rv.append(("isabelle avatar events straddling kiss isa_body1_u",922,549))
            rv.append(("isabelle avatar events straddling kiss isa_body1_c",789,184))
          elif "kiss" in state:
            rv.append(("isabelle avatar events straddling kiss isa_body2_n",555,200))
            rv.append(("isabelle avatar events straddling kiss isa_body2_u",878,608))
            rv.append(("isabelle avatar events straddling kiss isa_body2_c",816,258))
        else:
          if home_bedroom["clean"]:
            rv.append(("isabelle avatar events straddling sex bg_clean",0,0))
          else:
            rv.append(("isabelle avatar events straddling sex bg_dirty",0,0))
          rv.append(("isabelle avatar events straddling sex mc_body_n",751,713))
          if state.endswith(("remove_top","remove_bra","boob_hide","boob_reveal","boob_play","remove_panties","panty_drop")):
            rv.append(("isabelle avatar events straddling sex mc_body1_c",758,710))
          elif state.endswith("dick_reveal"):
            rv.append(("isabelle avatar events straddling sex mc_body2_c",759,710))
          if state.endswith(("remove_top","remove_bra","boob_hide","boob_reveal","boob_play","dick_reveal")):
            rv.append(("isabelle avatar events straddling sex isa_legs_n",570,510))
            if not state.endswith("dick_reveal"):
              rv.append(("isabelle avatar events straddling sex isa_legs_u",827,756))
              if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
                rv.append(("isabelle avatar events straddling sex isa_legs1_c",757,638))
              else:
                rv.append(("isabelle avatar events straddling sex isa_legs2_c",570,681))
            if state.endswith("remove_top"):
              rv.append(("isabelle avatar events straddling sex isa_arms1_c",759,87))
            elif state.endswith("remove_bra"):
              rv.append(("isabelle avatar events straddling sex isa_arms2_u",649,141))
            elif state.endswith("boob_hide"):
              rv.append(("isabelle avatar events straddling sex isa_arms3_n",763,87))
            elif state.endswith("boob_reveal"):
              rv.append(("isabelle avatar events straddling sex isa_arms4_n",625,87))
            elif state.endswith("boob_play"):
              rv.append(("isabelle avatar events straddling sex isa_arms5_n",766,94))
              rv.append(("isabelle avatar events straddling sex mc_hands1",713,487))
            elif state.endswith("dick_reveal"):
              rv.append(("isabelle avatar events straddling sex isa_arms6_n",771,99))
          elif state.endswith("remove_panties"):
            rv.append(("isabelle avatar events straddling sex isa_body1_u",623,69))
          elif state.endswith("panty_drop"):
            rv.append(("isabelle avatar events straddling sex isa_body2_u",577,50))
          elif state.endswith(("mutual_fap1","mutual_fap2","mutual_fap3")):
            rv.append(("isabelle avatar events straddling sex isa_body3_n",576,72))
            if state.endswith("mutual_fap1"):
              rv.append(("isabelle avatar events straddling sex isa_arms7_n",789,315))
            elif state.endswith("mutual_fap2"):
              rv.append(("isabelle avatar events straddling sex isa_arms8_n",789,315))
            elif state.endswith("mutual_fap3"):
              rv.append(("isabelle avatar events straddling sex isa_arms9_n",791,315))
          elif state.endswith("getting_ready"):
            rv.append(("isabelle avatar events straddling sex isa_body4_n",622,83))
            rv.append(("isabelle avatar events straddling sex wetness1",997,751))
          if state.endswith(("dick_reveal","mutual_fap1","mutual_fap2","mutual_fap3","getting_ready","insertion","penetration1","penetration2","penetration3","penetration4","penetration5","penetration6","penetration7","penetration8","penetration9","change_position")):
            rv.append(("isabelle avatar events straddling sex mc_dick",926,799))
            rv.append(("isabelle avatar events straddling sex panties",282,867))
            if state.endswith("mutual_fap1"):
              rv.append(("isabelle avatar events straddling sex mc_hands2",935,880))
            elif state.endswith("mutual_fap2"):
              rv.append(("isabelle avatar events straddling sex mc_hands3",935,849))
            elif state.endswith("mutual_fap3"):
              rv.append(("isabelle avatar events straddling sex mc_hands4",929,809))
          if state.endswith("insertion"):
            rv.append(("isabelle avatar events straddling sex isa_body5_n",583,133))
          elif state.endswith(("penetration1","penetration2","penetration3","penetration4","penetration5","penetration6","penetration7","penetration8","penetration9","change_position")):
            rv.append(("isabelle avatar events straddling sex wetness2",947,800))
            if state.endswith("penetration1"):
              rv.append(("isabelle avatar events straddling sex isa_body6_n",543,165))
            elif state.endswith("penetration2"):
              rv.append(("isabelle avatar events straddling sex isa_body7_n",541,165))
            elif state.endswith("penetration3"):
              rv.append(("isabelle avatar events straddling sex isa_body8_n",541,124))
            elif state.endswith("penetration4"):
              rv.append(("isabelle avatar events straddling sex isa_body9_n",544,209))
            elif state.endswith("penetration5"):
              rv.append(("isabelle avatar events straddling sex isa_body10_n",541,190))
            elif state.endswith("penetration6"):
              rv.append(("isabelle avatar events straddling sex isa_body11_n",538,163))
            elif state.endswith("penetration7"):
              rv.append(("isabelle avatar events straddling sex isa_body12_n",544,209))
            elif state.endswith("penetration8"):
              rv.append(("isabelle avatar events straddling sex isa_body13_n",541,190))
            elif state.endswith("penetration9"):
              rv.append(("isabelle avatar events straddling sex isa_body14_n",538,163))
            elif state.endswith("change_position"):
              rv.append(("isabelle avatar events straddling sex isa_body15_n",546,211))

      elif state.startswith("lying_down"):
        rv.append((1920,1080))
        if home_bedroom["clean"]:
          rv.append(("isabelle avatar events lying_down bg_clean",0,0))
        else:
          rv.append(("isabelle avatar events lying_down bg_dirty",0,0))
        rv.append(("isabelle avatar events lying_down mc_feet",1326,836))
        if state.endswith(("surprise","handholding","kiss","leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside1","cum_outside2","embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down isa_body1",358,326))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down isa_body2",358,326))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down isa_body3",342,317))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down isa_body4",324,265))
        if state.endswith(("surprise","handholding","leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","insertion","penetration1","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down isa_head1",131,93))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down isa_head2",142,68))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down isa_head3",124,85))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down isa_head4",116,74))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down isa_head5",124,110))
        if state.endswith("surprise"):
          rv.append(("isabelle avatar events lying_down isa_face1",322,148))
        elif state.endswith("handholding"):
          rv.append(("isabelle avatar events lying_down isa_face2",319,147))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down isa_face3",328,133))
        elif state.endswith(("leg_raise","cum_outside2")):
          rv.append(("isabelle avatar events lying_down isa_face4",329,148))
        elif state.endswith(("clit_rub1","clit_rub2","clit_rub3")):
          rv.append(("isabelle avatar events lying_down isa_face5",305,139))
        elif state.endswith("clit_rub4"):
          rv.append(("isabelle avatar events lying_down isa_face6",332,148))
        elif state.endswith("insertion"):
          rv.append(("isabelle avatar events lying_down isa_face7",298,139))
        elif state.endswith(("penetration1","cum_outside1")):
          rv.append(("isabelle avatar events lying_down isa_face8",332,139))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down isa_face9",319,132))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events lying_down isa_face10",305,121))
        elif state.endswith("cum_inside"):
          rv.append(("isabelle avatar events lying_down isa_face11",304,121))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down isa_face12",315,158))
        if state.endswith(("surprise","handholding")):
          rv.append(("isabelle avatar events lying_down isa_boobs1",461,419))
        elif state.endswith(("kiss","leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside1","cum_outside2","embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down isa_boobs2",461,377))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down isa_boobs3",461,377))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down isa_boobs4",446,371))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down isa_boobs5",424,360))
        if state.endswith(("surprise","handholding")):
          rv.append(("isabelle avatar events lying_down isa_left_arm1",511,294))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down isa_left_arm2",526,0))
        elif state.endswith(("leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down isa_left_arm3",632,0))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down isa_left_arm4",316,6))
        if state.endswith("surprise"):
          rv.append(("isabelle avatar events lying_down isa_right_arm1",144,540))
        elif state.endswith(("handholding","kiss")):
          rv.append(("isabelle avatar events lying_down isa_right_arm2",286,428))
        elif state.endswith(("leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside2")):
          rv.append(("isabelle avatar events lying_down isa_right_arm3",324,287))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down isa_right_arm4",45,20))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down isa_right_arm5",33,0))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down isa_right_arm6",20,0))
        elif state.endswith(("embrace1","embrace2","cum_outside1")):
          rv.append(("isabelle avatar events lying_down isa_right_arm7",49,446))
        if state.endswith(("surprise","handholding","kiss")):
          rv.append(("isabelle avatar events lying_down isa_leg1",967,452))
        elif state.endswith(("leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down isa_leg2",658,0))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down isa_leg3",665,0))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down isa_leg4",619,0))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down isa_leg5",572,0))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down isa_leg6",632,0))
        if state.endswith(("surprise","handholding")):
          rv.append(("isabelle avatar events lying_down mc_body1",822,0))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down mc_body2",794,0))
        elif state.endswith(("leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down mc_body3",981,0))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down mc_body4",729,0))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down mc_body5",741,0))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down mc_body6",852,0))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down mc_body7",661,0))
        if state.endswith(("surprise","handholding")):
          rv.append(("isabelle avatar events lying_down mc_head1",491,0))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down mc_head2",331,0))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down mc_head3",137,1))
        if state.endswith(("surprise","handholding")):
          rv.append(("isabelle avatar events lying_down mc_leg1",1120,296))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down mc_leg2",1120,296))
        elif state.endswith(("leg_raise","clit_rub1","clit_rub2","clit_rub3","clit_rub4","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down mc_leg3",1119,284))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down mc_leg4",1119,370))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down mc_leg5",1119,450))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down mc_leg6",1113,388))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down mc_leg7",1120,296))
        if state.endswith(("surprise","handholding","kiss")):
          rv.append(("isabelle avatar events lying_down mc_dick1",1065,514))
        elif state.endswith(("leg_raise","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down mc_dick2",1084,530))
          if state.endswith("cum_outside1"):
            rv.append(("isabelle avatar events lying_down mc_cum2",705,527))
          elif state.endswith("cum_outside2"):
            rv.append(("isabelle avatar events lying_down mc_cum3",694,596))
        elif state.endswith(("clit_rub1","clit_rub2","clit_rub3","clit_rub4")):
          rv.append(("isabelle avatar events lying_down mc_dick3",1056,536))
        elif state.endswith("insertion"):
          rv.append(("isabelle avatar events lying_down mc_dick4",1179,578))
        elif state.endswith("penetration1"):
          rv.append(("isabelle avatar events lying_down mc_dick5",1179,578))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down mc_dick6",1165,613))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down mc_dick7",1148,608))
          if state.endswith("cum_inside"):
            rv.append(("isabelle avatar events lying_down mc_cum1",1041,550))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down mc_dick8",1039,535))
          if state.endswith("embrace2"):
            rv.append(("isabelle avatar events lying_down mc_cum3",694,596))
        if state.endswith("surprise"):
          rv.append(("isabelle avatar events lying_down mc_arm1",661,0))
        elif state.endswith("handholding"):
          rv.append(("isabelle avatar events lying_down mc_arm2",280,0))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events lying_down mc_arm3",280,0))
        elif state.endswith(("leg_raise","cum_outside1","cum_outside2")):
          rv.append(("isabelle avatar events lying_down mc_arm4",796,0))
          rv.append(("isabelle avatar events lying_down mc_arm5",1138,0))
        elif state.endswith(("clit_rub1","clit_rub2","clit_rub3","clit_rub4")):
          rv.append(("isabelle avatar events lying_down mc_arm6",1185,0))
          if state.endswith(("clit_rub1","clit_rub4")):
            rv.append(("isabelle avatar events lying_down mc_thumb1",1149,655))
          elif state.endswith("clit_rub2"):
            rv.append(("isabelle avatar events lying_down mc_thumb2",1144,655))
          elif state.endswith("clit_rub3"):
            rv.append(("isabelle avatar events lying_down mc_thumb3",1141,654))
        elif state.endswith(("insertion","penetration1")):
          rv.append(("isabelle avatar events lying_down mc_arm7",681,0))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events lying_down mc_arm8",639,0))
        elif state.endswith(("penetration3","cum_inside")):
          rv.append(("isabelle avatar events lying_down mc_arm9",588,0))
        elif state.endswith(("embrace1","embrace2")):
          rv.append(("isabelle avatar events lying_down mc_arm10",568,30))
          rv.append(("isabelle avatar events lying_down isa_head5_fix",537,356))
          rv.append(("isabelle avatar events lying_down isa_left_arm4_fix",316,6))

      elif state.startswith("piano_sex"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events piano_sex bg",0,0))
        if state.endswith("bend_over"):
          rv.append(("isabelle avatar events piano_sex mc_body1",12,0))
          rv.append(("isabelle avatar events piano_sex mc_arms1",4,0))
        if state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex isabelle_legs1_n",268,476))
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            rv.append(("isabelle avatar events piano_sex isabelle_legs1_c1",250,517))
          else:
           rv.append(("isabelle avatar events piano_sex isabelle_legs1_c2",250,959))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex isabelle_legs2_n",268,463))
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            rv.append(("isabelle avatar events piano_sex isabelle_legs2_c1",250,506))
          else:
            rv.append(("isabelle avatar events piano_sex isabelle_legs2_c2",250,963))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex isabelle_legs3_n",268,454))
          if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
            rv.append(("isabelle avatar events piano_sex isabelle_legs3_c1",250,503))
          else:
            rv.append(("isabelle avatar events piano_sex isabelle_legs3_c2",250,968))
        else:
          rv.append(("isabelle avatar events piano_sex isabelle_legs4_n",268,481))
          if state.endswith(("bend_over","kiss","strip1","strip2","strip3")):
            rv.append(("isabelle avatar events piano_sex isabelle_legs4_u",534,482))
          if state.endswith(("bend_over","kiss","strip1","strip2")):
            if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
              rv.append(("isabelle avatar events piano_sex isabelle_legs4_c1",250,477))
            else:
              rv.append(("isabelle avatar events piano_sex isabelle_legs4_c2",250,476))
          else:
            if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
              rv.append(("isabelle avatar events piano_sex isabelle_legs5_c1",250,518))
            else:
              rv.append(("isabelle avatar events piano_sex isabelle_legs5_c2",250,951))
        if state.endswith("bend_over"):
          rv.append(("isabelle avatar events piano_sex isabelle_body1_n",864,356))
          rv.append(("isabelle avatar events piano_sex isabelle_body1_u",972,521))
        elif state.endswith("kiss") or "strip" in state:
          rv.append(("isabelle avatar events piano_sex isabelle_body2_n",830,306))
          if state.endswith(("kiss","strip1")):
            rv.append(("isabelle avatar events piano_sex isabelle_body2_u",961,640))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex isabelle_body3_n",827,372))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex isabelle_body4_n",777,384))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex isabelle_body5_n",734,392))
        else:
          rv.append(("isabelle avatar events piano_sex isabelle_body6_n",864,358))
        if state.endswith(("penetration1","cum_inside")):
          rv.append(("isabelle avatar events piano_sex mc_arms5a",702,635))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex mc_arms6a",670,614))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex mc_arms7a",629,590))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex mc_arms8a",602,574))
        if state.endswith("bend_over"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms1_n",1073,401))
        elif state.endswith("kiss") or "strip" in state:
          pass
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms2_n",818,372))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms3_n",813,385))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms4_n",802,399))
        else:
          rv.append(("isabelle avatar events piano_sex isabelle_arms5_n",840,359))
        if state.endswith("bend_over"):
          rv.append(("isabelle avatar events piano_sex isabelle_body1_c",864,394))
        elif state.endswith("kiss") or "strip" in state:
          rv.append(("isabelle avatar events piano_sex isabelle_body2_c1",830,337))
          if state.endswith("kiss"):
            rv.append(("isabelle avatar events piano_sex isabelle_body2_c2",958,510))
          else:
            rv.append(("isabelle avatar events piano_sex isabelle_body2_c3",1042,456))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex isabelle_body3_c",1037,365))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex isabelle_body4_c",996,378))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex isabelle_body5_c",961,386))
        else:
          rv.append(("isabelle avatar events piano_sex isabelle_body6_c",1069,354))
        if state.endswith("bend_over"):
           rv.append(("isabelle avatar events piano_sex isabelle_arms1_c",1074,476))
        elif state.endswith("kiss") or "strip" in state:
          pass
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms2_c",812,368))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms3_c",811,380))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms4_c",801,393))
        else:
          rv.append(("isabelle avatar events piano_sex isabelle_arms5_c",836,362))
        if state.endswith("bend_over"):
          rv.append(("isabelle avatar events piano_sex isabelle_head1",1087,63))
          rv.append(("isabelle avatar events piano_sex isabelle_face1",1327,289))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses1",1294,82))
        elif state.endswith("kiss") or "strip" in state:
          rv.append(("isabelle avatar events piano_sex isabelle_head2",1081,8))
          if state.endswith("kiss"):
            rv.append(("isabelle avatar events piano_sex isabelle_face2",1235,188))
          elif state.endswith(("strip1","strip2")):
            rv.append(("isabelle avatar events piano_sex isabelle_face3",1236,182))
          else:
            rv.append(("isabelle avatar events piano_sex isabelle_face4",1237,189))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses2",1251,0))
        elif state.endswith("pussy_lick"):
          rv.append(("isabelle avatar events piano_sex isabelle_head3",1234,97))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses3",1399,98))
        elif state.endswith("penetration1"):
          rv.append(("isabelle avatar events piano_sex isabelle_head4",1255,93))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses4",1487,171))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex isabelle_head5",1232,85))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses5",1434,136))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex isabelle_head6",1134,83))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses6",1324,104))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex isabelle_head7",1075,77))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses7",1306,111))
        else:
          rv.append(("isabelle avatar events piano_sex isabelle_head8",1161,116))
          if state.endswith("cum_inside"):
            rv.append(("isabelle avatar events piano_sex isabelle_face9",1391,365))
          else:
            rv.append(("isabelle avatar events piano_sex isabelle_face10",1391,364))
          rv.append(("isabelle avatar events piano_sex isabelle_glasses8",1344,166))
        if state.endswith("kiss"):
          rv.append(("isabelle avatar events piano_sex isabelle_arms6_n",1221,406))
          rv.append(("isabelle avatar events piano_sex isabelle_arms6_c",1281,538))
        elif "strip" in state:
          rv.append(("isabelle avatar events piano_sex isabelle_arms7_n",1261,453))
          rv.append(("isabelle avatar events piano_sex isabelle_arms7_c",1282,585))
        if state.endswith("bend_over"):
          pass
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events piano_sex mc_body2",166,0))
          rv.append(("isabelle avatar events piano_sex mc_arms2",161,137))
        elif "strip" in state:
          rv.append(("isabelle avatar events piano_sex mc_body3",47,0))
          rv.append(("isabelle avatar events piano_sex mc_arms3",156,0))
        elif state.endswith("pussy_lick"):
          rv.append(("isabelle avatar events piano_sex mc_body4",84,308))
          rv.append(("isabelle avatar events piano_sex mc_arms4",117,524))
        elif state.endswith(("penetration1","cum_inside")):
          rv.append(("isabelle avatar events piano_sex mc_body5",0,0))
          if state.endswith("cum_inside"):
            rv.append(("isabelle avatar events piano_sex mc_cum1",453,543))
          rv.append(("isabelle avatar events piano_sex mc_arms5b",278,0))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events piano_sex mc_body6",0,0))
          rv.append(("isabelle avatar events piano_sex mc_arms6b",273,0))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events piano_sex mc_body7",0,0))
          rv.append(("isabelle avatar events piano_sex mc_arms7b",251,0))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events piano_sex mc_body8",0,0))
          rv.append(("isabelle avatar events piano_sex mc_arms8b",220,0))
        else:
          rv.append(("isabelle avatar events piano_sex mc_body9",0,0))
          rv.append(("isabelle avatar events piano_sex mc_arms9",42,0))
          if state.endswith("cum_outside"):
            rv.append(("isabelle avatar events piano_sex mc_cum2",623,392))

      elif state.startswith("locker"):
        rv.append((1920,1080))
        if state.endswith(("_push","_lie","_lock","_sit")):
          rv.append(("isabelle avatar events locker push background",0,0))
          if state.endswith(("_push","_lie")):
            rv.append(("isabelle avatar events locker push locker_door1",470,0))
          else:
            rv.append(("isabelle avatar events locker push locker_door2",62,0))
          if state.endswith("_push"):
            rv.append(("isabelle avatar events locker push shadow1",318,199))
            rv.append(("isabelle avatar events locker push maxine_body1",735,98))
            rv.append(("isabelle avatar events locker push maxine_underwear1",801,353))
            rv.append(("isabelle avatar events locker push maxine_clothes1",727,1))
          elif state.endswith("_lie"):
            rv.append(("isabelle avatar events locker push shadow2",230,261))
            rv.append(("isabelle avatar events locker push maxine_body2",310,70))
            rv.append(("isabelle avatar events locker push maxine_underwear2",529,268))
            rv.append(("isabelle avatar events locker push maxine_clothes2",293,124))
          else:
            rv.append(("isabelle avatar events locker push maxine_body3",235,52))
            rv.append(("isabelle avatar events locker push maxine_clothes3",363,155))
          if state.endswith("_push"):
            rv.append(("isabelle avatar events locker push isabelle_body1",1165,225))
            rv.append(("isabelle avatar events locker push isabelle_underwear1",1616,719))
            rv.append(("isabelle avatar events locker push isabelle_clothes1",1245,226))
            if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
              rv.append(("isabelle avatar events locker push isabelle_skirt1",1726,894))
            else:
              rv.append(("isabelle avatar events locker push isabelle_pants1",1725,893))
          elif state.endswith("_lie"):
            rv.append(("isabelle avatar events locker push isabelle_body2",1202,162))
            rv.append(("isabelle avatar events locker push isabelle_underwear2",1502,524))
            rv.append(("isabelle avatar events locker push isabelle_clothes2",1201,172))
            if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
              rv.append(("isabelle avatar events locker push isabelle_skirt2",1584,701))
            else:
              rv.append(("isabelle avatar events locker push isabelle_pants2",1303,701))
          elif state.endswith("_lock"):
            rv.append(("isabelle avatar events locker push shadow3",753,261))
            rv.append(("isabelle avatar events locker push isabelle_body3",753,64))
            rv.append(("isabelle avatar events locker push isabelle_underwear3",1248,337))
            rv.append(("isabelle avatar events locker push isabelle_clothes3",875,92))
            if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
              rv.append(("isabelle avatar events locker push isabelle_skirt3",1404,456))
            else:
              rv.append(("isabelle avatar events locker push isabelle_pants3",1403,456))
          else:
            rv.append(("isabelle avatar events locker push shadow4",399,255))
            rv.append(("isabelle avatar events locker push isabelle_body4",560,68))
            rv.append(("isabelle avatar events locker push isabelle_underwear4",796,366))
            rv.append(("isabelle avatar events locker push isabelle_clothes4",542,86))
            if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
              rv.append(("isabelle avatar events locker push isabelle_skirt4",951,463))
            else:
              rv.append(("isabelle avatar events locker push isabelle_pants4",951,463))
        elif state.endswith(("_invitation","_anticipation","_penetration1","_penetration2","_penetration3","_penetration4","_cum_inside","_cum_outside1","_cum_outside2","_pee1","_pee2")):
          rv.append(("isabelle avatar events locker sex background",0,0))
          if state.endswith(("_invitation","_anticipation","_cum_outside1","_cum_outside2","_pee1","_pee2")):
            rv.append(("isabelle avatar events locker sex isabelle_shadow1",0,271))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex isabelle_shadow2",75,283))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex isabelle_shadow3",75,275))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex isabelle_shadow4",75,276))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex isabelle_shadow5",75,257))
          if state.endswith(("_invitation","_anticipation","_cum_outside1","_cum_outside2","_pee1","_pee2")):
            rv.append(("isabelle avatar events locker sex isabelle_body1",0,0))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex isabelle_body2",0,0))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex isabelle_body3",0,0))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex isabelle_body4",0,0))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex isabelle_body5",0,0))
          if state.endswith(("_invitation","_anticipation")):
            rv.append(("isabelle avatar events locker sex isabelle_right_arm1",1356,0))
          elif state.endswith(("_cum_outside1","_cum_outside2","_pee1","_pee2")):
            rv.append(("isabelle avatar events locker sex isabelle_right_arm2",1053,208))
          if state.endswith(("_invitation","_pee1","_pee2")):
            rv.append(("isabelle avatar events locker sex isabelle_left_arm1",764,376))
            if state.endswith("_pee2"):
              rv.append(("isabelle avatar events locker sex isabelle_pee",625,513))
          elif state.endswith(("_anticipation","_cum_outside1","_cum_outside2")):
            rv.append(("isabelle avatar events locker sex isabelle_left_arm2",1404,476))
          if state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex isabelle_arms1",1139,272))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex isabelle_arms2",1175,250))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex isabelle_arms3",1193,248))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex isabelle_arms4",1206,247))
          if state.endswith("_invitation"):
            rv.append(("isabelle avatar events locker sex isabelle_head1",1452,100))
          elif state.endswith("_anticipation"):
            rv.append(("isabelle avatar events locker sex isabelle_head2",1452,100))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex isabelle_head3",1222,274))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex isabelle_head4",1260,272))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex isabelle_head5",1246,264))
          elif state.endswith("_penetration4"):
            if "_moan" in state:
              rv.append(("isabelle avatar events locker sex isabelle_head7",1287,274))
            else:
              rv.append(("isabelle avatar events locker sex isabelle_head6",1244,274))
          elif state.endswith("_cum_inside"):
            rv.append(("isabelle avatar events locker sex isabelle_head7",1287,274))
          elif state.endswith(("_cum_outside1","_cum_outside2")):
            rv.append(("isabelle avatar events locker sex isabelle_head8",1446,43))
          elif state.endswith(("_pee1","_pee2")):
            rv.append(("isabelle avatar events locker sex isabelle_head9",1446,43))
          if state.endswith("_anticipation"):
            rv.append(("isabelle avatar events locker sex mc_left_leg1",0,522))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex mc_left_leg2",0,494))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex mc_left_leg3",0,479))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex mc_left_leg4",0,499))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex mc_left_leg5",0,449))
          if state.endswith("_anticipation"):
            rv.append(("isabelle avatar events locker sex mc_left_arm1",802,0))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex mc_left_arm2",689,0))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex mc_left_arm3",711,0))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex mc_left_arm4",764,0))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex mc_left_arm5",782,0))
          if state.endswith("_anticipation"):
            rv.append(("isabelle avatar events locker sex mc_body1",58,0))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex mc_body2",0,0))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex mc_body3",0,0))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex mc_body4",27,0))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex mc_body5",115,0))
          elif state.endswith("_cum_outside1"):
            rv.append(("isabelle avatar events locker sex mc_body6",0,0))
          elif state.endswith(("_cum_outside2","_pee1","_pee2")):
            rv.append(("isabelle avatar events locker sex mc_body7",0,0))
          if state.endswith("_anticipation"):
            rv.append(("isabelle avatar events locker sex mc_penis1",370,492))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex mc_penis2",232,522))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex mc_penis3",307,510))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex mc_penis4",335,511))
          elif state.endswith("_penetration4"):
            rv.append(("isabelle avatar events locker sex mc_penis5",431,499))
          elif state.endswith("_cum_inside"):
            rv.append(("isabelle avatar events locker sex mc_penis6",431,499))
          if state.endswith("_anticipation"):
            rv.append(("isabelle avatar events locker sex mc_right_arm1",418,0))
          elif state.endswith("_penetration1"):
            rv.append(("isabelle avatar events locker sex mc_right_arm2",0,0))
          elif state.endswith("_penetration2"):
            rv.append(("isabelle avatar events locker sex mc_right_arm3",0,0))
          elif state.endswith("_penetration3"):
            rv.append(("isabelle avatar events locker sex mc_right_arm4",0,0))
          elif state.endswith(("_penetration4","_cum_inside")):
            rv.append(("isabelle avatar events locker sex mc_right_arm5",87,0))
        elif "_slide" in state:
          rv.append(("isabelle avatar events locker slide background",0,0))
          if state.endswith("1"):
            rv.append(("isabelle avatar events locker slide locker1",565,0))
          elif state.endswith("2"):
            rv.append(("isabelle avatar events locker slide locker2",560,0))
          elif state.endswith("3"):
            rv.append(("isabelle avatar events locker slide locker3",565,0))
          elif state.endswith("4"):
            rv.append(("isabelle avatar events locker slide locker4",516,0))
          elif state.endswith("5"):
            rv.append(("isabelle avatar events locker slide locker5",432,0))
          elif state.endswith("6"):
            rv.append(("isabelle avatar events locker slide locker6",427,241))
          elif state.endswith("7"):
            rv.append(("isabelle avatar events locker slide locker7",449,535))
            rv.append(("isabelle avatar events locker slide water_splash",223,776))

      elif state.startswith("roof_sunset"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events roof_sunset background",0,0))
        if "_hug" in state:
          rv.append(("isabelle avatar events roof_sunset mc_body1",912,75))
          rv.append(("isabelle avatar events roof_sunset isabelle_body1",540,396))
          rv.append(("isabelle avatar events roof_sunset isabelle_underwear1",570,849))
          rv.append(("isabelle avatar events roof_sunset isabelle_clothes1",540,450))
          if state.endswith("blush"):
            rv.append(("isabelle avatar events roof_sunset isabelle_head1",473,178))
          elif state.endswith("concerned"):
            rv.append(("isabelle avatar events roof_sunset isabelle_head2",473,178))
          elif state.endswith("smile"):
            rv.append(("isabelle avatar events roof_sunset isabelle_head3",473,178))
          elif state.endswith("skeptical"):
            rv.append(("isabelle avatar events roof_sunset isabelle_head4",473,178))
          rv.append(("isabelle avatar events roof_sunset isabelle_glasses1",630,162))
        elif "_kiss" in state:
          rv.append(("isabelle avatar events roof_sunset isabelle_body2",540,389))
          rv.append(("isabelle avatar events roof_sunset isabelle_underwear2",570,849))
          rv.append(("isabelle avatar events roof_sunset isabelle_clothes2",540,425))
          if state.endswith("_surprised"):
            rv.append(("isabelle avatar events roof_sunset isabelle_head5",493,185))
          elif state.endswith("_eyes_closed"):
            rv.append(("isabelle avatar events roof_sunset isabelle_head6",493,185))
          rv.append(("isabelle avatar events roof_sunset isabelle_glasses2",601,171))
          rv.append(("isabelle avatar events roof_sunset mc_body2",792,77))
          rv.append(("isabelle avatar events roof_sunset isabelle_body3",572,387))
          rv.append(("isabelle avatar events roof_sunset isabelle_clothes3",566,404))

      elif state.startswith("desk_sex_"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events desk_sex background",0,0))
        if not state.endswith(("_lying_down","_lifting_top","_lowering_bra","_spreading_legs")) and not False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
          rv.append(("isabelle avatar events desk_sex isabelle_pants3",393,190))
        rv.append(("isabelle avatar events desk_sex isabelle_torso",772,193))
        if state.endswith("_pulling_panties"):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm3a",725,267))
        if not state.endswith("_lying_down"):
          rv.append(("isabelle avatar events desk_sex isabelle_top2a",833,253))
        if not state.endswith(("_lying_down","_lifting_top")):
          rv.append(("isabelle avatar events desk_sex isabelle_bra2",833,505))
        if state.endswith(("_vaginal2","_vaginal2_orgasm","_anal2")):
          rv.append(("isabelle avatar events desk_sex isabelle_boobs2",824,381))
        elif state.endswith(("_vaginal3","_vaginal3_orgasm","_anal3")):
          rv.append(("isabelle avatar events desk_sex isabelle_boobs3",822,382))
        elif state.endswith(("_vaginal4","_vaginal4_orgasm","_anal4","_cum_inside","_cum_outside","_cum_outside_orgasm")):
          rv.append(("isabelle avatar events desk_sex isabelle_boobs4",820,381))
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_boobs1",832,381))
        if state.endswith(("_lying_down","_lifting_top")):
          rv.append(("isabelle avatar events desk_sex isabelle_bra1",832,456))
        if state.endswith("_lying_down"):
          rv.append(("isabelle avatar events desk_sex isabelle_top1",830,253))
        elif state.endswith(("_lifting_top","_lowering_bra")):
          rv.append(("isabelle avatar events desk_sex isabelle_top2b",836,298))
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_top2c",844,377))
        if state.endswith(("_cum_inside","_aftermath_inside","_aftermath_outside")):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm1",449,267))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve1",449,266))
        if state.endswith(("_lying_down","_lifting_top","_lowering_bra","_spreading_legs","_removing_pants","_pulling_panties","_dick_reveal","_grabbing_dick","_anticipation")):
          rv.append(("isabelle avatar events desk_sex isabelle_head1",832,3))
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_head2",855,2))
        if state.endswith("_lying_down"):
          rv.append(("isabelle avatar events desk_sex isabelle_face1",880,125))
        elif state.endswith(("_pulling_panties","_dick_reveal","_grabbing_dick")):
          rv.append(("isabelle avatar events desk_sex isabelle_face2",880,126))
        elif state.endswith(("_lifting_top","_lowering_bra","_spreading_legs","_removing_pants","_anticipation")):
          rv.append(("isabelle avatar events desk_sex isabelle_face3",880,126))
        elif state.endswith(("_licking1","_licking2","_licking3","_licking4","_aftermath_inside","_aftermath_outside")) and not state.endswith("_vaginal_cum_inside"):
          rv.append(("isabelle avatar events desk_sex isabelle_face7",909,152))
        elif state.endswith(("_fingering1","_fingering2","_fingering3","_fingering4")):
          rv.append(("isabelle avatar events desk_sex isabelle_face6",909,152))
        elif state.endswith(("_vaginal1","_vaginal2","_vaginal3","_vaginal4")):
          rv.append(("isabelle avatar events desk_sex isabelle_face8",909,147))
        elif state.endswith(("_anal1","_anal2","_anal3","_anal4","_cum_outside","_anal_cum_inside")):
          rv.append(("isabelle avatar events desk_sex isabelle_face9",909,147))
        elif state.endswith(("_orgasm","_vaginal_cum_inside")):
          rv.append(("isabelle avatar events desk_sex isabelle_face10",909,151))
        if state.endswith(("_lying_down","_lifting_top","_lowering_bra","_spreading_legs","_removing_pants","_pulling_panties","_dick_reveal","_grabbing_dick","_anticipation")):
          rv.append(("isabelle avatar events desk_sex isabelle_glasses1",816,5))
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_glasses2",859,24))
        if state.endswith(("_lying_down","_lifting_top","_lowering_bra")):
          rv.append(("isabelle avatar events desk_sex isabelle_legs1",736,759))
        elif state.endswith(("_anticipation","_vaginal1","_vaginal1_orgasm","_anal1","_aftermath_inside","_aftermath_outside")):
          rv.append(("isabelle avatar events desk_sex isabelle_legs3",0,424))
        elif state.endswith(("_vaginal2","_vaginal2_orgasm","_anal2")):
          rv.append(("isabelle avatar events desk_sex isabelle_legs4",0,429))
        elif state.endswith(("_vaginal3","_vaginal3_orgasm","_anal3")):
          rv.append(("isabelle avatar events desk_sex isabelle_legs5",0,436))
        elif state.endswith(("_vaginal4","_vaginal4_orgasm","_anal4","_cum_inside","_cum_outside","_cum_outside_orgasm")):
          rv.append(("isabelle avatar events desk_sex isabelle_legs6",0,444))
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_legs2",635,759))
        if state.endswith("_licking1"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy1",900,795))
        elif state.endswith("_licking2"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy2",900,795))
        elif state.endswith("_licking3"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy3",900,795))
        elif state.endswith("_licking4"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy4",900,795))
        elif state.endswith("_fingering1"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy5",900,795))
        elif state.endswith("_fingering2"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy6",900,795))
        elif state.endswith("_fingering3"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy7",900,795))
        elif state.endswith("_fingering4"):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy8",900,795))
        elif state.endswith(("_vaginal1","_vaginal1_orgasm")):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy9",904,829))
        elif state.endswith(("_vaginal2","_vaginal2_orgasm")):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy10",904,825))
        elif state.endswith(("_vaginal3","_vaginal3_orgasm")):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy11",906,818))
        elif state.endswith(("_vaginal4","_vaginal4_orgasm","_vaginal_cum_inside")):
          rv.append(("isabelle avatar events desk_sex isabelle_pussy12",896,812))
        if state.endswith(("_lying_down","_lifting_top","_lowering_bra")):
          rv.append(("isabelle avatar events desk_sex isabelle_panties1",762,741))
        elif state.endswith(("_spreading_legs","_removing_pants")):
          rv.append(("isabelle avatar events desk_sex isabelle_panties2",762,741))
        elif state.endswith("_pulling_panties"):
          rv.append(("isabelle avatar events desk_sex isabelle_panties3",762,741))
        elif state.endswith(("_anticipation","_vaginal1","_vaginal1_orgasm","_anal1","_aftermath_inside","_aftermath_outside")):
          rv.append(("isabelle avatar events desk_sex isabelle_panties5",863,742))
        elif state.endswith(("_vaginal2","_vaginal2_orgasm","_anal2")):
          rv.append(("isabelle avatar events desk_sex isabelle_panties6",853,737))
        elif state.endswith(("_vaginal3","_vaginal3_orgasm","_anal3")):
          rv.append(("isabelle avatar events desk_sex isabelle_panties7",844,731))
        elif state.endswith(("_vaginal4","_vaginal4_orgasm","_anal4","_cum_inside","_cum_outside","_cum_outside_orgasm")):
          rv.append(("isabelle avatar events desk_sex isabelle_panties8",833,724))
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_panties4",762,741))
        if state.endswith(("_lying_down","_lifting_top","_lowering_bra")) and not False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
          rv.append(("isabelle avatar events desk_sex isabelle_pants1",733,658))
        elif state.endswith("_spreading_legs") and not False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
          rv.append(("isabelle avatar events desk_sex isabelle_pants2",632,658))
        if state.endswith("_licking1"):
          rv.append(("isabelle avatar events desk_sex mc_tongue1",887,915))
        elif state.endswith("_licking2"):
          rv.append(("isabelle avatar events desk_sex mc_tongue2",891,902))
        elif state.endswith("_licking3"):
          rv.append(("isabelle avatar events desk_sex mc_tongue3",896,870))
        elif state.endswith("_licking4"):
          rv.append(("isabelle avatar events desk_sex mc_tongue4",898,865))
        elif state.endswith("_fingering1"):
          rv.append(("isabelle avatar events desk_sex mc_fingers1",863,896))
        elif state.endswith("_fingering2"):
          rv.append(("isabelle avatar events desk_sex mc_fingers2",849,895))
        elif state.endswith("_fingering3"):
          rv.append(("isabelle avatar events desk_sex mc_fingers3",825,896))
        elif state.endswith("_fingering4"):
          rv.append(("isabelle avatar events desk_sex mc_fingers4",807,894))
        elif state.endswith(("_vaginal1","_vaginal1_orgasm")):
          rv.append(("isabelle avatar events desk_sex mc_dick2",899,903))
        elif state.endswith(("_vaginal2","_vaginal2_orgasm")):
          rv.append(("isabelle avatar events desk_sex mc_dick3",899,899))
        elif state.endswith(("_vaginal3","_vaginal3_orgasm")):
          rv.append(("isabelle avatar events desk_sex mc_dick4",899,893))
        elif state.endswith(("_vaginal4","_vaginal4_orgasm")):
          rv.append(("isabelle avatar events desk_sex mc_dick5",899,886))
        elif state.endswith("_vaginal_cum_inside"):
          rv.append(("isabelle avatar events desk_sex mc_dick6",899,883))
        elif state.endswith("_anal1"):
          rv.append(("isabelle avatar events desk_sex mc_dick7",905,945))
        elif state.endswith("_anal2"):
          rv.append(("isabelle avatar events desk_sex mc_dick8",905,942))
        elif state.endswith("_anal3"):
          rv.append(("isabelle avatar events desk_sex mc_dick9",899,935))
        elif state.endswith("_anal4"):
          rv.append(("isabelle avatar events desk_sex mc_dick10",898,928))
        elif state.endswith("_anal_cum_inside"):
          rv.append(("isabelle avatar events desk_sex mc_dick11",898,928))
        if False: ## Placeholder ## Isabelle is wearing the plaid miniskirt ##
          if state.endswith(("_lying_down","_lifting_top","_lowering_bra")):
            rv.append(("isabelle avatar events desk_sex isabelle_skirt1",707,658))
          elif state.endswith(("_anticipation","_vaginal1","_vaginal1_orgasm","_anal1","_aftermath_inside","_aftermath_outside")):
            rv.append(("isabelle avatar events desk_sex isabelle_skirt3",770,613))
          elif state.endswith(("_vaginal2","_vaginal2_orgasm","_anal2")):
            rv.append(("isabelle avatar events desk_sex isabelle_skirt4",765,623))
          elif state.endswith(("_vaginal3","_vaginal3_orgasm","_anal3")):
            rv.append(("isabelle avatar events desk_sex isabelle_skirt5",757,623))
          elif state.endswith(("_vaginal4","_vaginal4_orgasm","_anal4","_cum_inside","_cum_outside")):
            rv.append(("isabelle avatar events desk_sex isabelle_skirt6",748,624))
          else:
            rv.append(("isabelle avatar events desk_sex isabelle_skirt2",675,658))
        if state.endswith(("_dick_reveal","_grabbing_dick","_anticipation")):
          rv.append(("isabelle avatar events desk_sex mc_dick1",903,852))
        elif state.endswith("_aftermath_inside"):
          rv.append(("isabelle avatar events desk_sex mc_dick12",899,809))
        elif state.endswith(("_cum_outside","_cum_outside_orgasm")):
          rv.append(("isabelle avatar events desk_sex mc_dick13",869,513))
        elif state.endswith("_aftermath_outside"):
          rv.append(("isabelle avatar events desk_sex mc_dick14",854,565))
        if state.endswith(("_lifting_top","_lowering_bra")):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm2",751,266))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve2",749,266))
        elif state.endswith("_pulling_panties"):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm3b",774,741))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve3",723,266))
        elif state.endswith("_dick_reveal"):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm4",751,92))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve4",750,92))
        elif state.endswith(("_grabbing_dick","_anticipation")):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm5",791,268))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve5",754,266))
        elif state.endswith(("_vaginal1","_vaginal2","_vaginal3","_vaginal4","_orgasm","_anal1","_anal2","_anal3","_anal4","_cum_outside")):
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm6",593,219))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve6",590,219))
        elif state.endswith(("_cum_inside","_aftermath_inside","_aftermath_outside")):
          pass
        else:
          rv.append(("isabelle avatar events desk_sex isabelle_right_arm1",449,267))
          rv.append(("isabelle avatar events desk_sex isabelle_right_sleeve1",449,266))

      elif state.startswith("bed_kiss"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events bed_kiss background",0,0))
        rv.append(("isabelle avatar events bed_kiss isabelle_body",700,269))
        rv.append(("isabelle avatar events bed_kiss isabelle_underwear",701,269))
        rv.append(("isabelle avatar events bed_kiss isabelle_clothes",692,268))
        if "startled" in state or "eyes_closed" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_head1",0,373))
        elif "worried" in state or "excited" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_head2",0,400))
        if "startled" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_face1",270,404))
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm1_n",594,587))
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm1_c",709,610))
        elif "worried" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_face2",250,426))
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm2_n",682,128))
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm2_c",710,369))
        elif "excited" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_face3",253,426))
        elif "eyes_closed" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_face4",269,403))
        if "startled" in state or "eyes_closed" in state:
          rv.append(("isabelle avatar events bed_kiss mc_body1",0,0))
        elif "worried" in state or "excited" in state:
          rv.append(("isabelle avatar events bed_kiss mc_body2",0,0))
        if "excited" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm3_n",414,0))
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm3_c",406,0))
        elif "eyes_closed" in state:
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm4_n",518,0))
          rv.append(("isabelle avatar events bed_kiss isabelle_right_arm4_c",513,0))

      elif state.startswith("bed_sex"):
        rv.append((1920,1080))
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(("isabelle avatar events bed_sex background_alt",0,0))
        else:
          rv.append(("isabelle avatar events bed_sex background",0,0))
        if "exposed" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body1",0,99))
        elif "pulled_closer" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body2",108,0))
        if "licking" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body3",122,0))
        elif "sucking" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body4",122,0))
        elif "anticipation" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body5",135,0))
        elif "penetration1" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body6",105,0))
        elif "penetration2" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body7",62,0))
        elif "penetration3" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body8",24,0))
        elif "penetration4" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body9",0,0))
        elif "orgasm" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body10",24,0))
        elif "aftermath" in state:
          rv.append(("isabelle avatar events bed_sex isabelle_body11",36,0))
        if "pulled_closer" in state:
          rv.append(("isabelle avatar events bed_sex mc_body1",1227,0))
        elif "licking" in state:
          rv.append(("isabelle avatar events bed_sex mc_body2",1629,18))
        elif "sucking" in state:
          rv.append(("isabelle avatar events bed_sex mc_body3",1629,6))
        elif "anticipation" in state:
          rv.append(("isabelle avatar events bed_sex mc_body4",1657,0))
        elif "penetration1" in state:
          rv.append(("isabelle avatar events bed_sex mc_body5",825,1))
        elif "penetration2" in state:
          rv.append(("isabelle avatar events bed_sex mc_body6",790,0))
        elif "penetration3" in state:
          rv.append(("isabelle avatar events bed_sex mc_body7",777,0))
        elif "penetration4" in state:
          rv.append(("isabelle avatar events bed_sex mc_body8",759,0))
        elif "orgasm" in state:
          rv.append(("isabelle avatar events bed_sex mc_body9",777,0))
        elif "aftermath" in state:
          rv.append(("isabelle avatar events bed_sex mc_body10",390,865))

      elif state.startswith("bed_after"):
        rv.append((1920,1080))
        if quest.maya_sauce["bedroom_taken_over"]:
          rv.append(("isabelle avatar events bed_after background_alt",0,0))
        else:
          rv.append(("isabelle avatar events bed_after background",0,0))
        rv.append(("isabelle avatar events bed_after isabelle_body",0,88))
        if "blush" in state:
          rv.append(("isabelle avatar events bed_after isabelle_face1",421,339))
        elif "worried" in state:
          rv.append(("isabelle avatar events bed_after isabelle_face2",444,413))
        elif "smile" in state:
          rv.append(("isabelle avatar events bed_after isabelle_face3",439,413))
        elif "thinking" in state:
          rv.append(("isabelle avatar events bed_after isabelle_face4",435,413))
        elif "excited" in state:
          rv.append(("isabelle avatar events bed_after isabelle_face5",439,413))
        elif "eyes_closed" in state:
          rv.append(("isabelle avatar events bed_after isabelle_face6",439,410))
        rv.append(("isabelle avatar events bed_after mc_body",51,59))

      elif state.startswith("stream_cleanup"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events stream_cleanup background",0,0))
        rv.append(("isabelle avatar events stream_cleanup isabelle_body",867,72))
        rv.append(("isabelle avatar events stream_cleanup flora_body",326,171))
        rv.append(("isabelle avatar events stream_cleanup flora_underwear",491,303))
        rv.append(("isabelle avatar events stream_cleanup flora_clothes",438,282))

      elif state.startswith("kitchen_kiss"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events kitchen_kiss background",0,0))
        rv.append(("isabelle avatar events kitchen_kiss isabelle_body",617,541))
        rv.append(("isabelle avatar events kitchen_kiss isabelle_underwear",632,557))
        rv.append(("isabelle avatar events kitchen_kiss isabelle_clothes",607,535))
        if state.endswith(("eager","embrace")):
          rv.append(("isabelle avatar events kitchen_kiss isabelle_head1",69,327))
          if state.endswith("eager"):
            rv.append(("isabelle avatar events kitchen_kiss isabelle_face1",389,394))
          elif state.endswith("embrace"):
            rv.append(("isabelle avatar events kitchen_kiss isabelle_face2",380,393))
        elif state.endswith("forceful"):
          rv.append(("isabelle avatar events kitchen_kiss isabelle_head2",80,304))
        rv.append(("isabelle avatar events kitchen_kiss mc_body",798,0))
        if state.endswith("eager"):
          rv.append(("isabelle avatar events kitchen_kiss mc_arms1",260,448))
          rv.append(("isabelle avatar events kitchen_kiss mc_head1",236,0))
          rv.append(("isabelle avatar events kitchen_kiss isabelle_arms1",711,433))
          rv.append(("isabelle avatar events kitchen_kiss isabelle_sleeves1",966,1033))
        elif state.endswith("forceful"):
          rv.append(("isabelle avatar events kitchen_kiss mc_arms2",374,377))
          rv.append(("isabelle avatar events kitchen_kiss mc_head2",270,0))
          rv.append(("isabelle avatar events kitchen_kiss isabelle_arms2",776,467))
          rv.append(("isabelle avatar events kitchen_kiss isabelle_sleeves2",858,1006))
        elif state.endswith("embrace"):
          rv.append(("isabelle avatar events kitchen_kiss mc_arms3",1026,518))
          rv.append(("isabelle avatar events kitchen_kiss mc_head1",236,0))
          rv.append(("isabelle avatar events kitchen_kiss isabelle_arms3",447,0))
          rv.append(("isabelle avatar events kitchen_kiss isabelle_sleeves3",614,11))

      elif state.startswith("kitchen_sex"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events kitchen_sex background",0,0))
        if state.endswith(("lifted_up","neck_kiss","anticipation","insertion","aftermath","aftermath_kiss")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_body1",358,267))
          rv.append(("isabelle avatar events kitchen_sex isabelle_bra1",428,312))
          rv.append(("isabelle avatar events kitchen_sex isabelle_tops1",355,312))
        elif state.endswith(("penetration1","penetration4")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_body2",323,316))
          rv.append(("isabelle avatar events kitchen_sex isabelle_bra2",391,331))
          rv.append(("isabelle avatar events kitchen_sex isabelle_tops2",332,340))
        elif state.endswith(("penetration2","penetration5","cum")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_body3",306,366))
          rv.append(("isabelle avatar events kitchen_sex isabelle_bra3",383,362))
          rv.append(("isabelle avatar events kitchen_sex isabelle_tops3",307,361))
        elif state.endswith(("penetration3","penetration6")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_body4",238,350))
          rv.append(("isabelle avatar events kitchen_sex isabelle_bra4",309,346))
          rv.append(("isabelle avatar events kitchen_sex isabelle_tops4",236,343))
        if state.endswith(("lifted_up","aftermath_kiss")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head1",41,208))
          if state.endswith("lifted_up"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face1",197,196))
          elif state.endswith("aftermath_kiss"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face8",197,196))
          rv.append(("isabelle avatar events kitchen_sex mc_head1",219,0))
        elif state.endswith(("neck_kiss","aftermath")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head2",52,173))
          if state.endswith("neck_kiss"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face2",196,210))
          elif state.endswith("aftermath"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face7",196,212))
          rv.append(("isabelle avatar events kitchen_sex mc_head2",309,107))
        elif state.endswith(("anticipation","insertion")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head3",81,129))
          if state.endswith("anticipation"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face3",299,227))
            rv.append(("isabelle avatar events kitchen_sex isabelle_left_arm",501,237))
          elif state.endswith("insertion"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face4",299,227))
        elif state.endswith("penetration1"):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head4",57,120))
        elif state.endswith("penetration2"):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head5",68,103))
        elif state.endswith("penetration3"):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head6",18,79))
        elif state.endswith("penetration4"):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head7",32,165))
        elif state.endswith(("penetration5","cum")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head8",45,139))
          if state.endswith("penetration5"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face5",207,198))
          elif state.endswith("cum"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_face6",206,198))
        elif state.endswith("penetration6"):
          rv.append(("isabelle avatar events kitchen_sex isabelle_head9",0,108))
        if state.endswith(("lifted_up","neck_kiss")):
          rv.append(("isabelle avatar events kitchen_sex mc_body1",370,0))
        if state.endswith(("lifted_up","neck_kiss")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_legs1",969,0))
          rv.append(("isabelle avatar events kitchen_sex isabelle_panties1",955,399))
          rv.append(("isabelle avatar events kitchen_sex isabelle_skirt1",954,277))
          rv.append(("isabelle avatar events kitchen_sex mc_left_arm1",319,0))
        elif state.endswith(("anticipation","insertion","aftermath","aftermath_kiss")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_legs2",871,0))
          if state.endswith("anticipation"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_pussy1",1151,394))
          elif state.endswith("insertion"):
            rv.append(("isabelle avatar events kitchen_sex isabelle_pussy2",1151,394))
          elif state.endswith(("aftermath","aftermath_kiss")):
            rv.append(("isabelle avatar events kitchen_sex isabelle_pussy6",1151,394))
          rv.append(("isabelle avatar events kitchen_sex isabelle_panties2",958,341))
          rv.append(("isabelle avatar events kitchen_sex isabelle_skirt2",937,139))
        elif state.endswith(("penetration1","penetration4")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_legs3",803,0))
          rv.append(("isabelle avatar events kitchen_sex isabelle_pussy3",1108,422))
          rv.append(("isabelle avatar events kitchen_sex isabelle_panties3",897,369))
          rv.append(("isabelle avatar events kitchen_sex isabelle_skirt3",877,168))
        elif state.endswith(("penetration2","penetration5","cum")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_legs4",888,0))
          rv.append(("isabelle avatar events kitchen_sex isabelle_pussy4",1121,484))
          rv.append(("isabelle avatar events kitchen_sex isabelle_panties4",915,423))
          rv.append(("isabelle avatar events kitchen_sex isabelle_skirt4",912,199))
        elif state.endswith(("penetration3","penetration6")):
          rv.append(("isabelle avatar events kitchen_sex isabelle_legs5",843,0))
          rv.append(("isabelle avatar events kitchen_sex isabelle_pussy5",1019,523))
          rv.append(("isabelle avatar events kitchen_sex isabelle_panties5",840,441))
          rv.append(("isabelle avatar events kitchen_sex isabelle_skirt5",837,223))
        if state.endswith("anticipation"):
          rv.append(("isabelle avatar events kitchen_sex mc_body2",904,0))
          rv.append(("isabelle avatar events kitchen_sex mc_dick1",1279,79))
        elif state.endswith("insertion"):
          rv.append(("isabelle avatar events kitchen_sex mc_body3",824,0))
          rv.append(("isabelle avatar events kitchen_sex mc_dick2",1257,130))
        elif state.endswith(("penetration1","penetration4")):
          rv.append(("isabelle avatar events kitchen_sex mc_body4",374,0))
          rv.append(("isabelle avatar events kitchen_sex mc_dick3",1211,312))
        elif state.endswith(("penetration2","penetration5","cum")):
          rv.append(("isabelle avatar events kitchen_sex mc_body5",468,0))
          if state.endswith(("penetration2","penetration5")):
            rv.append(("isabelle avatar events kitchen_sex mc_dick4",1221,426))
          elif state.endswith("cum"):
            rv.append(("isabelle avatar events kitchen_sex mc_dick6",1161,426))
        elif state.endswith(("penetration3","penetration6")):
          rv.append(("isabelle avatar events kitchen_sex mc_body6",546,0))
          rv.append(("isabelle avatar events kitchen_sex mc_dick5",1124,431))
        elif state.endswith(("aftermath","aftermath_kiss")):
          rv.append(("isabelle avatar events kitchen_sex mc_body7",370,0))
          rv.append(("isabelle avatar events kitchen_sex mc_dick7",1360,387))
          rv.append(("isabelle avatar events kitchen_sex mc_left_arm2",409,44))

      elif state.startswith("door_peek"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events door_peek background",490,0))
        rv.append(("isabelle avatar events door_peek foreground",0,0))
        if state.endswith(("confrontation","other_direction1","other_direction2")):
          rv.append(("isabelle avatar events door_peek kate_body1",816,162))
          rv.append(("isabelle avatar events door_peek kate_underwear1",877,345))
          rv.append(("isabelle avatar events door_peek kate_clothes1",805,282))
        elif state.endswith(("step_around1","step_around2")):
          rv.append(("isabelle avatar events door_peek kate_body2",807,155))
          rv.append(("isabelle avatar events door_peek kate_underwear2",867,308))
          rv.append(("isabelle avatar events door_peek kate_clothes2",807,263))
        elif state.endswith(("arm_swing1","arm_swing2")):
          rv.append(("isabelle avatar events door_peek kate_body3",808,109))
          rv.append(("isabelle avatar events door_peek kate_underwear3",876,332))
          rv.append(("isabelle avatar events door_peek kate_clothes3",823,195))
        elif state.endswith(("kiss1","kiss2","kiss3")):
          rv.append(("isabelle avatar events door_peek kate_body4",821,205))
          if state.endswith(("kiss1","kiss2")):
            rv.append(("isabelle avatar events door_peek kate_hair1",1124,232))
            if state.endswith("kiss1"):
              rv.append(("isabelle avatar events door_peek kate_face1",1099,231))
            elif state.endswith("kiss2"):
              rv.append(("isabelle avatar events door_peek kate_face2",1088,230))
          elif state.endswith("kiss3"):
            rv.append(("isabelle avatar events door_peek kate_hair2",1165,232))
            rv.append(("isabelle avatar events door_peek kate_face3",1093,231))
          rv.append(("isabelle avatar events door_peek kate_underwear4",945,352))
          rv.append(("isabelle avatar events door_peek kate_clothes4",817,257))
          if state.endswith("kiss1"):
            rv.append(("isabelle avatar events door_peek kate_left_arm1",1117,357))
            rv.append(("isabelle avatar events door_peek kate_left_sleeve1",1114,389))
          elif state.endswith(("kiss2","kiss3")):
            rv.append(("isabelle avatar events door_peek kate_left_arm2",1056,357))
            rv.append(("isabelle avatar events door_peek kate_left_sleeve2",1055,367))
        elif state.endswith(("grope1","grope2","grope3","grope4")):
          rv.append(("isabelle avatar events door_peek kate_body5",992,308))
          rv.append(("isabelle avatar events door_peek kate_underwear5",1164,400))
          rv.append(("isabelle avatar events door_peek kate_clothes5",986,345))
          if state.endswith(("grope1","grope2","grope3")):
            rv.append(("isabelle avatar events door_peek kate_head1",1218,252))
          elif state.endswith("grope4"):
            rv.append(("isabelle avatar events door_peek kate_head2",1217,241))
        elif state.endswith("push_away"):
          rv.append(("isabelle avatar events door_peek kate_body6",997,260))
          rv.append(("isabelle avatar events door_peek kate_underwear6",1164,400))
          rv.append(("isabelle avatar events door_peek kate_clothes6",986,335))
        elif state.endswith("run_away"):
          rv.append(("isabelle avatar events door_peek kate_body7",892,146))
          rv.append(("isabelle avatar events door_peek kate_underwear7",1039,422))
          rv.append(("isabelle avatar events door_peek kate_clothes7",935,326))
        if state.endswith(("confrontation","step_around1","other_direction2","arm_swing1")):
          rv.append(("isabelle avatar events door_peek isabelle_body1",720,147))
          rv.append(("isabelle avatar events door_peek isabelle_underwear1",757,276))
          rv.append(("isabelle avatar events door_peek isabelle_clothes1",717,271))
        elif state.endswith(("step_around2","other_direction1")):
          rv.append(("isabelle avatar events door_peek isabelle_body2",719,192))
          rv.append(("isabelle avatar events door_peek isabelle_underwear2",767,284))
          rv.append(("isabelle avatar events door_peek isabelle_clothes2",717,266))
        elif state.endswith("arm_swing2"):
          rv.append(("isabelle avatar events door_peek isabelle_body3",720,157))
          rv.append(("isabelle avatar events door_peek isabelle_underwear3",749,276))
          rv.append(("isabelle avatar events door_peek isabelle_clothes3",714,267))
        elif state.endswith(("kiss1","kiss2","kiss3")):
          rv.append(("isabelle avatar events door_peek isabelle_body4",753,287))
          rv.append(("isabelle avatar events door_peek isabelle_underwear4",875,312))
          if state.endswith(("kiss1","kiss3")):
            rv.append(("isabelle avatar events door_peek isabelle_head1",975,213))
          elif state.endswith("kiss2"):
            rv.append(("isabelle avatar events door_peek isabelle_head2",979,203))
          rv.append(("isabelle avatar events door_peek isabelle_clothes4",752,305))
          if state.endswith(("kiss1","kiss2")):
            rv.append(("isabelle avatar events door_peek isabelle_right_arm1",1058,326))
            rv.append(("isabelle avatar events door_peek isabelle_right_sleeve1",1053,326))
          elif state.endswith("kiss3"):
            rv.append(("isabelle avatar events door_peek isabelle_right_arm2",1055,286))
            rv.append(("isabelle avatar events door_peek isabelle_right_sleeve2",1050,313))
        elif state.endswith(("grope1","grope2","grope3","grope4")):
          rv.append(("isabelle avatar events door_peek isabelle_body5",920,355))
          rv.append(("isabelle avatar events door_peek isabelle_underwear5",1062,360))
          if state.endswith(("grope1","grope2","grope3")):
            rv.append(("isabelle avatar events door_peek isabelle_head3",1140,264))
          elif state.endswith("grope4"):
            rv.append(("isabelle avatar events door_peek isabelle_head4",1138,255))
          rv.append(("isabelle avatar events door_peek isabelle_clothes5",942,352))
          if state.endswith("grope1"):
            rv.append(("isabelle avatar events door_peek isabelle_right_arm3",1215,265))
            rv.append(("isabelle avatar events door_peek isabelle_right_sleeve3",1211,338))
          elif state.endswith("grope2"):
            rv.append(("isabelle avatar events door_peek isabelle_right_arm4",1167,372))
            rv.append(("isabelle avatar events door_peek isabelle_right_sleeve4",1165,371))
          elif state.endswith(("grope3","grope4")):
            rv.append(("isabelle avatar events door_peek isabelle_right_arm5",1164,373))
            rv.append(("isabelle avatar events door_peek isabelle_right_sleeve5",1163,369))
        elif state.endswith("push_away"):
          rv.append(("isabelle avatar events door_peek isabelle_body6",651,270))
          rv.append(("isabelle avatar events door_peek isabelle_underwear6",652,377))
          rv.append(("isabelle avatar events door_peek isabelle_clothes6",648,372))
        elif state.endswith("run_away"):
          rv.append(("isabelle avatar events door_peek isabelle_body7",617,269))
          rv.append(("isabelle avatar events door_peek isabelle_underwear7",633,379))
          rv.append(("isabelle avatar events door_peek isabelle_clothes7",607,363))
        if any(action in state for action in ("stroke1","stroke2","cum")):
          rv.append(("isabelle avatar events door_peek mc_body1",0,0))
          if "stroke1" in state:
            rv.append(("isabelle avatar events door_peek mc_right_arm1",0,0))
          elif any(action in state for action in ("stroke2","cum")):
            rv.append(("isabelle avatar events door_peek mc_right_arm2",0,0))
            if "cum" in state:
              rv.append(("isabelle avatar events door_peek mc_cum",657,371))
        else:
          rv.append(("isabelle avatar events door_peek mc_body2",0,0))

      elif state.startswith("stargazing_bonfire"):
        meal = None if state.endswith(("smile","laughing","blush","thinking","flirty","surprised","sad")) else state.rsplit(" ", 1)[1]
        state = state if state.endswith(("smile","laughing","blush","thinking","flirty","surprised","sad")) else state.rsplit(" ", 1)[0]
        rv.append((1920,1080))
        rv.append(("isabelle avatar events stargazing_bonfire background",0,0))
        rv.append(("isabelle avatar events stargazing_bonfire isabelle_body",203,74))
        rv.append(("isabelle avatar events stargazing_bonfire isabelle_underwear",481,447))
        rv.append(("isabelle avatar events stargazing_bonfire isabelle_clothes",286,92))
        if meal and meal.endswith("spaghetti"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_arms1",593,386))
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_sleeves1",551,386))
        elif meal and meal.endswith("sandwich"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_arms2",579,255))
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_sleeves2",536,255))
        else:
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_arms3",561,574))
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_sleeves3",540,572))
        if state.endswith("smile"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face1",395,168))
        elif state.endswith("laughing"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face2",395,168))
        elif state.endswith("blush"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face3",395,167))
        elif state.endswith("thinking"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face4",403,168))
        elif state.endswith("flirty"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face5",395,167))
        elif state.endswith("surprised"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face6",401,168))
        elif state.endswith("sad"):
          rv.append(("isabelle avatar events stargazing_bonfire isabelle_face7",395,168))

      elif state.startswith("stargazing_spaghetti"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events stargazing_spaghetti background",0,0))
        if state.endswith("taste"):
          rv.append(("isabelle avatar events stargazing_spaghetti mc_body1",947,71))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_body1",162,662))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_underwear1",162,748))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_clothes1",153,696))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_head1",254,98))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_tiara1",613,133))
        elif state.endswith("kiss"):
          rv.append(("isabelle avatar events stargazing_spaghetti mc_body2",892,86))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_body2",161,654))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_underwear2",153,738))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_clothes2",151,680))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_head2",282,95))
          rv.append(("isabelle avatar events stargazing_spaghetti isabelle_tiara2",663,140))

      elif state.startswith("stargazing_grapes"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events stargazing_grapes background",0,0))
        rv.append(("isabelle avatar events stargazing_grapes isabelle_body",159,63))
        if state.endswith("feeding"):
          rv.append(("isabelle avatar events stargazing_grapes isabelle_face1",463,255))
        elif state.endswith("caressing"):
          rv.append(("isabelle avatar events stargazing_grapes isabelle_face2",468,255))
        elif state.endswith("licking"):
          rv.append(("isabelle avatar events stargazing_grapes isabelle_face3",468,255))
        rv.append(("isabelle avatar events stargazing_grapes isabelle_underwear",623,389))
        rv.append(("isabelle avatar events stargazing_grapes isabelle_clothes",219,125))
        if state.endswith("feeding"):
          rv.append(("isabelle avatar events stargazing_grapes mc_hand1",619,573))
        elif state.endswith("caressing"):
          rv.append(("isabelle avatar events stargazing_grapes mc_hand2",734,559))

      elif state.startswith("stargazing_embrace"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events stargazing_embrace background",0,0))
        rv.append(("isabelle avatar events stargazing_embrace mc_body",0,375))
        rv.append(("isabelle avatar events stargazing_embrace isabelle_body",279,378))
        rv.append(("isabelle avatar events stargazing_embrace isabelle_underwear",297,754))
        rv.append(("isabelle avatar events stargazing_embrace isabelle_clothes",276,411))

      elif state.startswith("stargazing_kiss"):
        rv.append((1920,1080))
        rv.append(("isabelle avatar events stargazing_kiss background",0,0))
        rv.append(("isabelle avatar events stargazing_kiss isabelle_body",197,48))
        rv.append(("isabelle avatar events stargazing_kiss isabelle_underwear",856,445))
        rv.append(("isabelle avatar events stargazing_kiss isabelle_clothes",621,133))

      return rv


  class Interactable_isabelle(Interactable):

    def title(cls):
      return isabelle.name

    def actions(cls,actions):
      if not (quest.isabelle_haggis in ("instructions","puzzle","escape")
      or quest.isabelle_buried in ("bury","dig","funeral")
      or quest.kate_fate in ("peek","hunt","hunt2","deadend","rescue")
      or quest.isabelle_piano in ("concert","score")
      or quest.isabelle_hurricane == "call_maxine"
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.fall_in_newfall.in_progress
      or quest.flora_walk == "forest_glade"
      or quest.isabelle_gesture in ("courage","apology")):
        if quest.kate_stepping.finished:
          actions.append(["talk","Talk","?isabelle_talk_broken"])
        elif isabelle["romance_disabled"]:
          actions.append(["talk","Talk","?isabelle_talk_romance_disabled"])
        else:
          if isabelle["talk_limit_today"]<3:
            actions.append(["talk","Talk","?isabelle_talk_one"])
            actions.append(["talk","Talk","?isabelle_talk_two"])
            actions.append(["talk","Talk","?isabelle_talk_three"])
            actions.append(["talk","Talk","?isabelle_talk_four"])
            actions.append(["talk","Talk","?isabelle_talk_five"])
          else:
            actions.append(["talk","Talk","?isabelle_talk_over"])
      #####Interactions that can happen at any time#######
      if mc["focus"]:
        if mc["focus"] == "isabelle_haggis":
          if quest.isabelle_haggis == "instructions":
            actions.append(["quest","Quest","?isabelle_quest_haggis_detention_start"])
          elif quest.isabelle_haggis >= "puzzle":
            actions.append(["quest","Quest","isabelle_quest_haggis_isabelle_detention"])
            if quest.isabelle_haggis["isabelle_regret"] and mc.owned_item("haggis_lunchbox"):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:isabelle_haggis,puzzle|Give What?","isabelle_quest_haggis_give_seconds_isabelle"])
            if mc.owned_item("bayonets_etiquettes"):
              actions.append(["give","Give","select_inventory_item","$quest_item_filter:isabelle_haggis,puzzle|Give What?","isabelle_quest_haggis_give_bayonets_etiquettes_isabelle"])
        elif mc["focus"] == "isabelle_buried":
          if quest.isabelle_buried in ("bury","dig"):
            actions.append(["quest","Quest","?isabelle_quest_buried_isabelle_bury"])
        elif mc["focus"] == "kate_fate":
          if quest.kate_fate == "rescue":
            actions.append(["quest","Quest","?quest_kate_fate_rescue"])
        elif mc["focus"] == "isabelle_locker":
          if quest.isabelle_locker == "bedroom":
            actions.append(["quest","Quest","quest_isabelle_locker_bedroom"])
        elif mc["focus"] == "kate_desire":
          if quest.kate_desire == "bathroomhelp":
            actions.append(["quest","Quest","?quest_kate_desire_bathroomhelp_isabelle"])
        elif mc["focus"] == "maxine_hook":
          if quest.maxine_hook == "interrogation":
            actions.append(["quest","Quest","?quest_maxine_hook_interrogation_isabelle"])
        elif mc["focus"] == "isabelle_piano":
          if quest.isabelle_piano == "concert":
            actions.append(["quest","Quest","quest_isabelle_piano_concert"])
        elif mc["focus"] == "isabelle_hurricane":
          if quest.isabelle_hurricane == "call_maxine":
            actions.append(["quest","Quest","?quest_isabelle_hurricane_call_maxine_isabelle"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19:
            if "isabelle" not in quest.isabelle_dethroning["dinner_conversations"]:
              actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_isabelle"])
            if "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
              actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_isabelle"])
        elif mc["focus"] == "kate_moment":
          if quest.isabelle_gesture == "courage":
            actions.append(["quest","Quest","quest_isabelle_gesture_courage"])
          elif quest.isabelle_gesture == "apology":
            actions.append(["quest","Quest","quest_isabelle_gesture_apology"])
          if quest.kate_moment == "fun" and game.hour == 19:
            actions.append(["quest","Quest","quest_kate_moment_fun_isabelle"])
        elif mc["focus"] == "flora_walk":
          if quest.flora_walk == "forest_glade":
            actions.append(["quest","Quest","quest_flora_walk_forest_glade"])
      else:
        ##########isabelle_haggis##########
        if quest.isabelle_haggis == "start" and not mc["focus"]:
          if game.location == "school_cafeteria":
            actions.append(["quest","Quest","?isabelle_quest_haggis_isabelle_cafeteria"])
          else:
            actions.append(["give","Give","select_inventory_item","$quest_item_filter:isabelle_haggis,start|Give What?","isabelle_quest_haggis_deliver"])

        ##########kate_search_for_nurse##############
        if quest.kate_search_for_nurse.in_progress and not quest.kate_search_for_nurse["isabelle_seen"]:
          actions.append(["quest","Quest","?kate_quest_search_for_nurse_isabelle_talk"])

        ##########isabelle_tour##############
        if quest.isabelle_tour.in_progress:

          if quest.isabelle_tour=="beginning":
            actions.append(["quest","Quest","?isabelle_quest_isabelle_tour"])

          elif quest.isabelle_tour=="first_hall_meetup":
            actions.append(["quest","Quest","?isabelle_quest_isabelle_tour_first_hall"])

          elif quest.isabelle_tour=="english_class":
            if game.location == "school_english_class":
              actions.append(["quest","Quest","?isabelle_quest_isabelle_tour_english_class"])

          elif quest.isabelle_tour=="art_class":
            if game.location == "school_art_class":
              actions.append(["quest","Quest","?isabelle_quest_isabelle_tour_art_class"])

          elif quest.isabelle_tour=="gym":
            if game.location == "school_gym":
              actions.append(["quest","Quest","?isabelle_quest_isabelle_tour_gym"])

          elif quest.isabelle_tour=="cafeteria":
            if game.location == "school_cafeteria":
              actions.append(["quest","Quest","?isabelle_quest_isabelle_tour_cafeteria"])

        #########kate_desire##################
        if quest.kate_desire == "advice":
          actions.append(["quest","Quest","?quest_kate_desire_isabelle_advice"])

        ##########isabelle_buried##############
        if quest.isabelle_buried == "isabelle":
          actions.append(["quest","Quest","?isabelle_quest_buried_isabelle"])
        elif quest.isabelle_buried == "shovel":
          actions.append(["quest","Quest","?isabelle_quest_buried_isabelle_shovel"])

        ##########isabelle_stolen##############
        if quest.isabelle_stolen.in_progress:
          if quest.isabelle_haggis>="trouble" and quest.isabelle_haggis.in_progress:
            pass
          else:
            if quest.isabelle_stolen == "sorry":
              actions.append(["quest","Quest","?isabelle_quest_stolen_sorry"])
            elif quest.isabelle_stolen == "tellisabelle":
              actions.append(["quest","Quest","?isabelle_quest_stolen_tellisabelle"])
            elif quest.isabelle_stolen == "askmaxine":
              actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_isabelle"])
            elif quest.isabelle_stolen == "dilemma":
              actions.append(["quest","Quest","?isabelle_quest_stolen_dilemma"])

        ##########poolside_story##############
        if quest.poolside_story == "petition":
          actions.append(["quest","Quest","?poolside_story_isabelle_petition"])
        elif quest.poolside_story == "isadec":
          actions.append(["quest","Quest","?poolside_story_isabelle_isadec"])

        ##########isabelle_locker##############
        if quest.isabelle_locker == "talk":
          actions.append(["quest","Quest","quest_isabelle_locker_talk"])

        ###########isabelle_piano##############
        if quest.isabelle_piano == "isabelle":
          actions.append(["quest","Quest","quest_isabelle_piano_isabelle"])

        ############isabelle_red###############
        if quest.isabelle_red == "greenhouse" and not quest.isabelle_red["greenhouse_riddle_help"]:
          actions.append(["quest","Quest","quest_isabelle_red_greenhouse_isabelle"])
        elif quest.isabelle_red == "black_note" and not quest.isabelle_red["black_note_riddle_help"]:
          actions.append(["quest","Quest","quest_isabelle_red_black_note_isabelle"])
        elif quest.isabelle_red == "triangle" and not quest.isabelle_red["triangle_riddle_help"]:
          actions.append(["quest","Quest","quest_isabelle_red_triangle_isabelle"])

        #########isabelle_hurricane############
        if quest.isabelle_hurricane == "forest" and not quest.isabelle_hurricane["hiking"]:
          actions.append(["quest","Quest","quest_isabelle_hurricane_forest_isabelle"])

        ###########lindsey_motive##############
        if quest.lindsey_motive == "isabelle":
          actions.append(["quest","Quest","quest_lindsey_motive_isabelle"])

        ###########kate_stepping##############
        if quest.kate_stepping == "isabelle":
          actions.append(["give","Give","select_inventory_item","$quest_item_filter:kate_stepping,spiked_drink|Give What?","quest_kate_stepping_isabelle"])

        ##########isabelle_dethroning##########
        if quest.isabelle_dethroning == "good_news":
          actions.append(["quest","Quest","quest_isabelle_dethroning_good_news"])
        elif quest.isabelle_dethroning == "invitations_done":
          actions.append(["quest","Quest","quest_isabelle_dethroning_invitations_done"])

        ############fall_in_newfall############
        if quest.fall_in_newfall == "late" and "isabelle" not in quest.fall_in_newfall["assembly_conversations"]:
          actions.append(["quest","Quest","quest_fall_in_newfall_late_school_homeroom_isabelle"])

        ##############maya_spell###############
        if quest.maya_spell == "hair" and quest.kate_stepping.finished:
          actions.append(["quest","Quest","quest_maya_spell_hair_isabelle"])

        #############kate_intrigue#############
        if quest.kate_intrigue == "private_chat":
          if game.location == "school_english_class":
            actions.append(["quest","Quest","quest_kate_intrigue_private_chat"])
          else:
            actions.append(["quest","Quest","?quest_kate_intrigue_private_chat_wrong_location"])
        elif quest.kate_intrigue == "eavesdrop":
          if "school_cafeteria" not in quest.kate_intrigue["eavesdropped_locations"]:
            actions.append(["quest","Quest","quest_kate_intrigue_eavesdrop_school_cafeteria"])
          if "school_first_hall" not in quest.kate_intrigue["eavesdropped_locations"]:
            actions.append(["quest","Quest","quest_kate_intrigue_eavesdrop_school_first_hall"])
          if "school_gym" not in quest.kate_intrigue["eavesdropped_locations"]:
            actions.append(["quest","Quest","quest_kate_intrigue_eavesdrop_school_gym"])
          if "school_ground_floor" not in quest.kate_intrigue["eavesdropped_locations"]:
            actions.append(["quest","Quest","quest_kate_intrigue_eavesdrop_school_ground_floor"])
          else:
            actions.append(["quest","Quest","quest_kate_intrigue_eavesdrop_school_english_class"])
        elif quest.kate_intrigue == "spy":
          actions.append(["quest","Quest","quest_kate_intrigue_spy"])

        ###########isabelle_gesture############
        if quest.isabelle_gesture == "courage":
          actions.append(["quest","Quest","quest_isabelle_gesture_courage"])
        elif quest.isabelle_gesture == "apology":
          actions.append(["quest","Quest","quest_isabelle_gesture_apology"])

        ########## quest interactions for picking up new quests go down here ############
        ########## so they don't pop up while another quest is in_progress ##############

        if game.season == 1:
          if quest.isabelle_tour.finished and not quest.isabelle_stolen.started and not quest.jacklyn_sweets.in_progress and not quest.isabelle_haggis.in_progress:
            actions.append(["quest","Quest","?isabelle_quest_stolen_start"])

          if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.berb_fight.finished and not quest.isabelle_buried.started and not quest.maxine_hook.in_progress and not isabelle["romance_disabled"]:
            actions.append(["quest","Quest","?isabelle_quest_buried_start"])

          if quest.kate_search_for_nurse.finished and quest.spinach_seek.finished: ## All other quests featuring Spinach must have been already finished, since Gathering Storm puts her in the MC's room indefinitely
            if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and quest.clubroom_access.finished and not quest.isabelle_locker.started and not quest.isabelle_buried["done_today"] and not isabelle["romance_disabled"]:
              actions.append(["quest","Quest","quest_isabelle_locker_start"])

          if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and quest.isabelle_locker["time_for_a_change"]) and not quest.isabelle_locker["finished_today"] and not quest.isabelle_hurricane.started and not isabelle["romance_disabled"]:
            actions.append(["quest","Quest","?quest_isabelle_hurricane_start"])

          if quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.maxine_eggs.finished and quest.isabelle_red.finished and quest.nurse_venting.finished and (quest.kate_wicked.finished and mc.owned_item("pig_mask")) and not quest.isabelle_dethroning.started:
            actions.append(["quest","Quest","?quest_isabelle_dethroning_start"])

        elif game.season == 2:
          if quest.kate_stepping.finished and quest.fall_in_newfall.finished and game.location != "school_english_class" and not quest.kate_intrigue.started:
            actions.append(["quest","Quest","quest_kate_intrigue_start"])

          # if (quest.isabelle_locker.actually_finished and not quest.isabelle_locker["time_for_a_change"]) and (quest.isabelle_dethroning.finished and not quest.isabelle_dethroning["kate_sex"]) and quest.fall_in_newfall.finished and not quest.isabelle_stars.started:
          if (quest.isabelle_locker.actually_finished and not quest.isabelle_locker["time_for_a_change"]) and (quest.isabelle_dethroning.finished and not quest.isabelle_dethroning["isabelle_caught"]) and quest.fall_in_newfall.finished and not quest.isabelle_stars.started:
            actions.append(["quest","Quest","quest_isabelle_stars_start"])

      ########## flirt##############
      if not (quest.isabelle_haggis in ("instructions","puzzle","escape")
      or quest.isabelle_buried in ("bury","dig","funeral")
      or quest.kate_fate in ("peek","hunt","hunt2","deadend","rescue")
      or quest.isabelle_piano in ("concert","score")
      or quest.isabelle_hurricane == "call_maxine"
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.fall_in_newfall.in_progress
      or quest.flora_walk == "forest_glade"
      or quest.isabelle_gesture in ("courage","apology")):
        if quest.kate_stepping.finished:
          actions.append(["flirt","Flirt","?isabelle_flirt_broken"])
          return
        if isabelle["romance_disabled"]:
          return
        if game.season == 1:
          if quest.isabelle_tour.finished and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and (quest.isabelle_locker.finished and (not quest.isabelle_locker["time_for_a_change"] or quest.isabelle_hurricane.finished)):
            if isabelle["roof"]:
              if isabelle.at("school_roof"):
                actions.append(["flirt","Flirt","isabelle_flirt_sunset"])
            else:
              actions.append(["flirt","Flirt","isabelle_flirt_tips"])
        if (isabelle["flirt_limit_today"]<3):
          actions.append(["flirt","Flirt","?isabelle_flirt_one"])
          actions.append(["flirt","Flirt","?isabelle_flirt_two"])
          actions.append(["flirt","Flirt","?isabelle_flirt_three"])
          actions.append(["flirt","Flirt","?isabelle_flirt_four"])
          actions.append(["flirt","Flirt","?isabelle_flirt_five"])
          actions.append(["flirt","Flirt","?isabelle_flirt_six"])
          actions.append(["flirt","Flirt","?isabelle_flirt_seven"])
          actions.append(["flirt","Flirt","?isabelle_flirt_eight"])
          actions.append(["flirt","Flirt","?isabelle_flirt_nine"])
        else:
          actions.append(["flirt","Flirt","?isabelle_flirt_over"])


label isabelle_talk_romance_disabled:
  "I don't think [isabelle] wants to talk to me after what I did to her..."
  return

label isabelle_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label isabelle_flirt_over:
  "She must be sick of my poor attempts at flirting by now..."
  return

label isabelle_talk_one:
  show isabelle smile with Dissolve(.5)
  isabelle smile "When I was little, Dad used to take me on vacation to Scotland."
  isabelle confident "I always liked the bagpipes and kilts, but most of all I enjoyed the strolls along the gray waters of Loch Ness, hoping to catch a glimpse of Nessie."
  $isabelle["talk_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_talk_two:
  show isabelle excited with Dissolve(.5)
  isabelle excited "I once had a short story published in the London Times."
  isabelle neutral "It was about a squirrel mother fighting off a pack of dogs and saving her children."
  isabelle confident "Kind of like Orwell, but less dystopian! I've always dreamed of becoming an author, but for now, it's just a hobby of mine."
  $isabelle["talk_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_talk_three:
  show isabelle smile with Dissolve(.5)
  isabelle smile "I've always felt like I was destined to do something out of the ordinary."
  isabelle laughing "Something that people will remember me for when I'm gone."
  isabelle smile "Maybe a great novel or poem. It doesn't have to be anything big, just something with a positive impact on the world."
  $isabelle["talk_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_talk_four:
  show isabelle smile with Dissolve(.5)
  isabelle smile "Thanks to my dad's job, I've had the chance to experience the rich cultures of the world."
  isabelle confident "I've lived in twelve different countries and visited another sixteen!"
  isabelle excited "One thing I've learned is that people are the same wherever you go."
  $isabelle["talk_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_talk_five:
  show isabelle annoyed with Dissolve(.5)
  isabelle annoyed "The thing that pisses me off the most is when people are loud and obnoxious."
  isabelle angry "Especially when they have nothing good to say!"
  isabelle angry "Like when they're droning on about themselves as if they're the most interesting thing that has happened since the Jurassic era."
  isabelle thinking "I also really dislike gym bunnies, but don't tell anyone..."
  $isabelle["talk_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_one:
  show isabelle smile with Dissolve(.5)
  isabelle smile "What part of my childhood vacations to Scotland did I enjoy the most?"
  show isabelle smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"The bagpipes! You always had a thing for blowing instruments.\"":
      show isabelle smile at move_to(.5)
      mc "The bagpipes! You always had a thing for blowing instruments."
      isabelle thinking "Why do you have to make everything sexual?"
    "\"The kilts! You like men who are a bit feminine.\"":
      show isabelle smile at move_to(.5)
      mc "The kilts! You like men who are a bit feminine."
      isabelle annoyed "Kilts are very manly, but, no, that's not it."
    "\"Loch Ness! You have a thing for camera-shy beasts... and that's why you like me.\"":
      show isabelle smile at move_to(.5)
      mc "Loch Ness! You have a thing for camera-shy beasts... and that's why you like me."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle laughing "Correct! The first part, at least. You're not that bad, you know."
    "\"Braveheart! You like people who carve their own path and aren't afraid to stand up to the English.\"":
      show isabelle smile at move_to(.5)
      mc "Braveheart! You like people who carve their own path and aren't afraid to stand up to the English."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle blush "Wrong answer... so wrong..."
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_two:
  show isabelle excited with Dissolve(.5)
  isabelle excited "My hobbies are really important to me. Can you name one?"
  show isabelle excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You have a crush on George Orwell and collect Animal Farm posters.\"":
      show isabelle excited at move_to(.5)
      mc "You have a crush on George Orwell and collect Animal Farm posters."
      isabelle thinking "I admire his writing, but I definitely don't have a crush on him."
    "\"You write short stories. Some of them include small animals in dire situations.\"":
      show isabelle excited at move_to(.5)
      mc "You write short stories. Some of them include small animals in dire situations."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle flirty "Yes! Writing is one of my passions!"
    "\"You're really into dystopian video games. Especially 'The Fall of London Bridge' and 'Queen Elizabeth: the Second Coming.'\"":
      show isabelle excited at move_to(.5)
      mc "You're really into dystopian video games. Especially \"The Fall of London Bridge\" and \"Queen Elizabeth: the Second Coming.\""
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle blush "What?! Who told you about that?"
    "\"Trick question! You don't have any hobbies.\"":
      show isabelle excited at move_to(.5)
      mc "Trick question! You don't have any hobbies."
      isabelle annoyed "I have plenty of hobbies, thank you very much."
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_three:
  show isabelle concerned with Dissolve(.5)
  isabelle concerned "Do you believe in destiny?"
  isabelle concerned_left "I've always wanted to leave something behind when I go... do you know what?"
  show isabelle concerned_left at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You just want to leave the world a better place than when you came. Starting with me!\"":
      show isabelle concerned_left at move_to(.5)
      mc "You just want to leave the world a better place than when you came. Starting with me!"
      isabelle smile "Correct! I think that's worth striving for."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle laughing "I'm not sure about the last part, though!"
    "\"A poem about George Orwell, where you confess your undying love.\"":
      show isabelle concerned_left at move_to(.5)
      mc "A poem about George Orwell, where you confess your undying love."
      isabelle angry "What?! Where are you even getting these ideas?"
    "\"You want to leave behind a legacy of freedom. Destiny is what we make for ourselves. Every man dies, not every man truly lives!\"":
      show isabelle concerned_left at move_to(.5)
      mc "You want to leave behind a legacy of freedom. Destiny is what we make for ourselves. Every man dies, not every man truly lives!"
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle blush "Is that a Braveheart quote?!"
    "\"You want to solve the Riemann Hypothesis. Because, let's be honest, who doesn't?\"":
      show isabelle concerned_left at move_to(.5)
      mc "You want to solve the Riemann Hypothesis. Because, let's be honest, who doesn't?"
      isabelle skeptical "Sounds like you're just trying to show off. Do you even know what that is?"
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_four:
  show isabelle smile_left with Dissolve(.5)
  isabelle smile_left "When I was younger, I didn't like to move every few months."
  isabelle laughing "But as I've grown older, experiencing new cultures and meeting new people is something I truly love!"
  isabelle confident "Do you know how many countries I've been to?"
  show isabelle confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You've lived in twelve countries and visited another fifteen.\"":
      show isabelle confident at move_to(.5)
      mc "You've lived in twelve countries and visited another fifteen."
      isabelle afraid "That's so close! I've actually visited sixteen."
      mc "Was Finland one of them?"
      isabelle laughing "Yep!"
      mc "My point exactly. Fifteen. Finland isn't a real place."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle confident "Actually, it's seventeen if you count Finland. You think I don't know memes?"
    "\"You've been to Scotland twelve times. Everything else is irrelevant.\"":
      show isabelle confident at move_to(.5)
      mc "You've been to Scotland twelve times. Everything else is irrelevant."
      isabelle thinking "I do love Scotland... and the people there..."
      mc "You mean the kilts, the tartan hats, and the men who wear them?"
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle flirty "I did not say that!"
    "\"You've long since stopped counting. People are the same brand of terrible everywhere.\"":
      show isabelle confident at move_to(.5)
      mc "You've long since stopped counting. People are the same brand of terrible everywhere."
      isabelle afraid "That's not true at all! Everyone is struggling in their own way, and we all share this planet."
      isabelle thinking "Sure, there are bad people in every country, but the vast majority are just like you and me."
    "\"You've been to like thirty countries or something. Honestly, who's keeping track?\"":
      show isabelle confident at move_to(.5)
      mc "You've been to like thirty countries or something. Honestly, who's keeping track?"
      isabelle eyeroll "Well, clearly not you."
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_five:
  show isabelle angry with Dissolve(.5)
  isabelle angry "Not a lot of things get on my nerves, but there are a few things that really make my blood boil! Do you know what?"
  show isabelle angry at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Bunnies — you hate those. It's their long creepy ears and the way they jump around. Monte Python got it right...\"":
      show isabelle angry at move_to(.5)
      mc "Bunnies — you hate those. It's their long creepy ears and the way they jump around. Monte Python got it right..."
      isabelle eyeroll "Very funny."
    "\"Selfish people and attention whores. You're fine with regular prostitutes.\"":
      show isabelle angry at move_to(.5)
      mc "Selfish people and attention whores. You're fine with regular prostitutes."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust-=1
        $isabelle.love+=1
      isabelle concerned_left "I mean, you're not wrong. But I'm not sure why you brought that up."
    "\"Stupid people. Fuck those guys.\"":
      show isabelle angry at move_to(.5)
      mc "Stupid people. Fuck those guys."
      isabelle skeptical "Wrong. As long as someone is nice and do their best to help others, I don't care."
    "\"Don't focus on what you hate. Focus on what you like — warrior poets!\"":
      show isabelle angry at move_to(.5)
      mc "Don't focus on what you hate. Focus on what you like — warrior poets!"
      isabelle blush "How come you always know the right thing to say?"
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle blush "Mmm, warrior poets..."
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_six:
  show isabelle flirty with Dissolve(.5)
  isabelle flirty "If you were a warrior, what kind would it be?"
  show isabelle flirty at move_to(.25)
  menu(side="right"):
    extend ""
    "\"A freedom fighter! I'd fight for the people and what is right — a warrior poet!\"":
      show isabelle flirty at move_to(.5)
      mc "A freedom fighter! I'd fight for the people and what is right — a warrior poet!"
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle blush "Oh, my..."
    "\"A banker! Steel may win battles, but gold wins wars.\"":
      show isabelle flirty at move_to(.5)
      mc "A banker! Steel may win battles, but gold wins wars."
      isabelle cringe "So, you'd hire others to fight for you?"
    "\"The strongest motherfucker on the battlefield, that's for sure.\"":
      show isabelle flirty at move_to(.5)
      $mc.strength+=1
      mc "The strongest motherfucker on the battlefield, that's for sure."
      isabelle eyeroll "The one with the biggest mouth, at least."
    "\"A diplomat! The greatest wars aren't won on the battlefield.\"":
      show isabelle flirty at move_to(.5)
      mc "A diplomat! The greatest wars aren't won on the battlefield."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle smile "That's true! Saving lives is nobler than taking them, even for a just cause."
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_seven:
  show isabelle smile with Dissolve(.5)
  isabelle smile "I think life goals are important. Do you have any?"
  show isabelle smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Not really. I just hope to find love one day. That would be enough for me.\"":
      show isabelle smile at move_to(.5)
      mc "Not really. I just hope to find love one day. That would be enough for me."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle laughing "That's a great goal to have!"
    "\"Life goals are for people with opportunities. Everyone else just has to make due.\"":
      show isabelle smile at move_to(.5)
      mc "Life goals are for people with opportunities. Everyone else just has to make due."
      isabelle afraid "That's not true!"
      isabelle thinking "Even those with the worst luck in life's lottery can strive for something."
      isabelle flirty "Being selfless and generous will make the world a better place."
      mc "Right, but it won't get me laid. Trust me, I've tried."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust-=1
      isabelle cringe "It's the opposite of selfless then, isn't it? Besides, nobody likes a fake."
    "\"I'm too lazy to have goals. Unless doing whatever I feel like counts...\"":
      show isabelle smile at move_to(.5)
      mc "I'm too lazy to have goals. Unless doing whatever I feel like counts..."
      isabelle laughing "I'd say that counts. Complete independence is something worth striving for!"
      mc "That's not what I meant."
      isabelle confident "Yeah, I know. I just put a positive spin on it."
    "\"Right now, I'm thinking of picking up poetry. Scottish medieval poetry, to be exact.\"":
      show isabelle smile at move_to(.5)
      mc "Right now, I'm thinking of picking up poetry. Scottish medieval poetry, to be exact."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle blush "Oh! You have to read one to me at some point!"
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_eight:
  show isabelle excited with Dissolve(.5)
  isabelle excited "You know I love books, but I also appreciate movies. What kind of movies are you into?"
  show isabelle excited at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Science fiction!\"":
      show isabelle excited at move_to(.5)
      mc "Science fiction!"
      mc "Nothing better than lightsabers and spaceships."
      isabelle thinking "Okay, I guess we should avoid going to the cinema together..."
    "\"Romance!\"":
      show isabelle excited at move_to(.5)
      mc "Romance!"
      mc "Moulin Rouge... The Notebook... anything that hits you right in the heart."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle laughing "Couldn't agree more!"
    "\"Action and drama!\"":
      show isabelle excited at move_to(.5)
      mc "Action and drama!"
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust+=1
      isabelle afraid "Whoa! Please tell me you've seen Braveheart!"
    "\"Anything anime!\"":
      show isabelle excited at move_to(.5)
      mc "Anything anime!"
      isabelle concerned "So, you don't care as long as it's a cartoon?"
      mc "I do have a preference... but I can't tell you what it is..."
      isabelle skeptical "Okay...?"
      mc "Starts with an \"H\" and rhymes with \"tie.\""
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love-=1
      isabelle cringe "You're being way too cryptic... and knowing you, that means it's something dirty."
  $isabelle["flirt_limit_today"]+=1
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_nine:
  show isabelle flirty with Dissolve(.5)
  isabelle flirty "Who do you think is the prettiest girl in school? You can't say me, by the way, because that would be a conflict of interest!"
  show isabelle flirty at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Apart from you? I don't really pay much attention to them.\"":
      show isabelle flirty at move_to(.5)
      mc "Apart from you? I don't really pay much attention to them."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love+=1
      isabelle blush "That's... oh, man."
    "\"As much as I hate to say it, nobody can really compete with [kate] in that regard.\"":
      show isabelle flirty at move_to(.5)
      if not isabelle["flirt_bonuses_chosen"]:
        $kate.lust+=1
      mc "As much as I hate to say it, nobody can really compete with [kate] in that regard."
      isabelle annoyed_left "I overheard her calling you a cockroach the other day."
      mc "Sounds about right."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.love-=1
      isabelle angry "She doesn't deserve your admiration!"
    "\"Definitely [mrsl]. What a woman!\"":
      show isabelle flirty at move_to(.5)
      if not isabelle["flirt_bonuses_chosen"]:
        $mrsl.lust+=1
      mc "Definitely [mrsl]. What a woman!"
      isabelle thinking "To each their own, I guess..."
    "\"I think there's something special about athletic girls, so I'm going with [lindsey].\"":
      show isabelle flirty at move_to(.5)
      if not isabelle["flirt_bonuses_chosen"]:
        $lindsey.love+=1
      mc "I think there's something special about athletic girls, so I'm going with [lindsey]."
      isabelle annoyed "What's so special about them?"
      mc "Toned stomachs and legs, grace, and they know sports. I love sports."
      if not isabelle["flirt_bonuses_chosen"]:
        $isabelle.lust-=1
      isabelle angry "Since when?!"
  $isabelle["flirt_limit_today"]+=1
  $isabelle["flirt_bonuses_chosen"] = True
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_tips:
  show isabelle confident with Dissolve(.5)
  isabelle confident "Hey, you!"
  mc "Who? Me?"
  isabelle laughing "Yeah, you silly goose!"
  "It's weird to think that everything can change so fast..."
  "If it weren't for the chocolate thief, [isabelle] never would've\nkissed me."
  "And if she hadn't kissed me, we probably wouldn't have had sex."
  "That's crazy to think about."
  mc "I was wondering if you had any tips for making out?"
  isabelle neutral "Making out?"
  mc "Yeah! Maybe you've read a manual or something?"
  isabelle neutral "I may know a thing or two, why?"
  mc "There's this girl I'm trying to impress..."
  isabelle skeptical "Truly?"
  mc "Yeah, she sometimes gets a bit feisty, but in reality she's really kind.{space=-5}"
  mc "And she has an adorable accent."
  isabelle blush "Oh."
  isabelle blush "She does sound like a proper catch."
  mc "So, you'll help me?"
  isabelle flirty "I think I might!"
  isabelle flirty "But good kisses are all about the setting!"
  isabelle flirty "I'm free last period. Meet me on the roof?"
  mc "You got it!"
  hide isabelle with Dissolve(.5)
  $isabelle["roof"] = True
  return

label isabelle_flirt_sunset:
  if school_roof["isabelle_kiss"]:
    show isabelle confident with Dissolve(.5)
    isabelle confident "Hey, you!"
    mc "Have I ever told you that you're absolutely beautiful?"
    isabelle laughing "I don't know, have you?"
    mc "Maybe once or twice!"
    isabelle confident "I suppose my memory isn't the best."
    isabelle confident "What did you say again?"
    mc "That you're beautiful."
    window hide
    show black onlayer screens zorder 100
    show isabelle roof_sunset_hug_blush
    with Dissolve(.5)
    pause 1.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    "[isabelle] leans in closer, touching my neck seductively."
    isabelle roof_sunset_hug_blush "Truly?"
    mc "Yes, and that you're one of a kind."
    isabelle roof_sunset_hug_blush "Thank you."
    window hide
    show isabelle roof_sunset_kiss_eyes_closed with Dissolve(.5)
    window auto
    "Our lips meet again. Hers, soft and delicate."
    "That, now familiar, hint of vanilla frosting makes my mouth water."
    "She pulls back for a moment, perhaps due to my extra saliva...."
#   "But perhaps due to something else..." ## Placeholder ## Repeatable Sex Scene ##
    show isabelle roof_sunset_hug_concerned with Dissolve(.5)
    mc "I'm sorry. You taste so sweet, I'm literally drooling."
    isabelle roof_sunset_hug_smile "You like my lip gloss?"
    mc "You know, that stuff should be illegal..."
    mc "It's like I'm kissing a birthday cake."
    isabelle roof_sunset_hug_smile "That doesn't sound awful."
    mc "It's not, I'm just—"
    window hide
    show isabelle roof_sunset_kiss_eyes_closed with Dissolve(.5)
    window auto
    "Mmm..."
    "This kiss is more playful... or at least it started that way."
    "Somehow, her grin melts away with the passion."
    "Her tongue, my tongue, bumping into each other."
    "Sparks crackle through my veins. The hairs on my arms stand up."
    "Our hearts race together. I can feel hers against my chest."
    "The fire of want and need roars through me, but the flame of being wanted burns brighter still."
    "It's one of those feelings that you can only experience through the warm fullness if it."
    "It's the opposite of the hollow cold that is loneliness."
    "And once you've felt it, you need it again. And again. And again."
    isabelle roof_sunset_hug_blush "Whoa..."
    mc "Yeah? Blown away?"
    isabelle roof_sunset_hug_skeptical "Don't get cocky, mate."
    mc "Why not? I just kissed the cutest girl in school."
    isabelle roof_sunset_hug_blush "Aw! Okay, you can be a little cocky."
    mc "I'll come up with a good response for next time."
    isabelle roof_sunset_hug_smile "Next time! Okay, I see what you did there."
    mc "You make me confident. What can I say?"
    isabelle roof_sunset_hug_smile "I think you said it all tonight."
    isabelle roof_sunset_hug_smile "And yes, there will be a next time."
    isabelle roof_sunset_hug_smile "Goodnight, [mc]."
    mc "Goodnight, [isabelle]."
  else:
    show isabelle blush with Dissolve(.5)
    isabelle blush "Heya! Look at the sunset!"
    isabelle blush "Beautiful, isn't it?"
    mc "I guess."
    isabelle laughing "You don't sound particularly excited."
    mc "The sun sets every night."
    isabelle confident "It sure does..."
    isabelle confident "But every sunset is unique, and so is every kiss."
    mc "If you put it like that..."
    isabelle confident "I am putting it like that."
    window hide
    show black onlayer screens zorder 100
    show isabelle roof_sunset_hug_blush
    with Dissolve(.5)
    pause 1.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    "[isabelle] steps in close. As close and intimate you can be without doing anything."
    "It feels special in a way."
    "Her hair and eyes glowing in the setting sun."
    mc "You know, I once swore off women for good..."
    mc "But that was a long time ago, and I think I'm warming up to your kind.{space=-35}"
    mc "Or, at least, to you."
    isabelle roof_sunset_hug_concerned "Oh, yeah? Did [kate] steal your crayons in kindergarten?"
    mc "No, it was... a different life."
    "[isabelle] will never understand loneliness like that."
    "Very few people can."
    "She deserves to be happy, more than anyone I know."
    isabelle roof_sunset_hug_smile "Well, I'm glad you've changed your mind."
    mc "You know you could get someone much better than me, right?"
    isabelle roof_sunset_hug_smile "Better? What's better?"
    mc "You know... more attractive, successful, popular..."
    isabelle roof_sunset_hug_smile "Love isn't a scale. It's a jigsaw puzzle."
    isabelle roof_sunset_hug_smile "You either match with someone or you don't."
    mc "Love?"
    isabelle roof_sunset_hug_skeptical "I was making a point!"
    mc "Can I make a point, too?"
    isabelle roof_sunset_hug_smile "Certainly, I'm all—"
    window hide
    show isabelle roof_sunset_kiss_surprised with Dissolve(.5)
    window auto
    isabelle roof_sunset_kiss_surprised "!!!"
    "When our lips meet, her eyelids flutter in surprise."
    show isabelle roof_sunset_kiss_eyes_closed with dissolve2
    "But it lasts for only a moment, and then she leans into the kiss."
    "Deeper than ever before."
    "Our tongues meeting is like an electric shock."
    "It's a rush of breathtaking excitement, kind of like a sudden tickle."
    "A hint of vanilla frosting from her lip gloss spreads into my mouth, mixing with our saliva."
    "Just like everything else she does in life, [isabelle] kisses with practiced skill."
    "With gentle confidence, and feminine sensitivity."
    "She knows what she wants, but she's more than happy to give in to my desires as well."
    "It's a perfect balance."
#   "As the sun sets over Newfall, I lay her down gently." ## Placeholder ## Repeatable Sex Scene ##
    "Like a tightrope dancer, she leads me out over passion's abyss."
    "And as I look down, the vertigo hits in full."
    "In my mind, through the dark clouds of neglect and depression,\na tiny glimmer of light breaks."
    "Perhaps, this can work?"
    "Maybe, maybe, maybe we can get together..."
    "Go on dates..."
    "Be one of those happy couples, picnicking in the park..."
    "Maybe even more?"
    "She would look so good in a white dress..."
    "God, I'm getting ahead of myself."
    "What if she doesn't like the kiss?"
    "And then it's over. A present moment turned memory."
    isabelle roof_sunset_hug_blush "Mmm..."
    mc "Was it okay?"
    isabelle roof_sunset_hug_blush "It was good!"
    mc "Good enough to do again?"
    isabelle roof_sunset_hug_blush "I'll think about it."
    mc "..."
    isabelle roof_sunset_hug_skeptical "Come on, I was taking the piss!"
    mc "I know. I'm just nervous."
    isabelle roof_sunset_hug_smile "Don't be! Life's too short to worry."
    mc "You're right..."
    mc "Goodnight, [isabelle]."
    isabelle roof_sunset_hug_smile "Goodnight, [mc]."
  window hide
  if not school_roof["isabelle_kiss"]:
    $game.notify_modal(None, "Love or Lust", "You have unlocked\na repeatable scene\nwith [isabelle]!", wait=5.0)
    pause 0.25
  show black onlayer screens zorder 100 with Dissolve(.5)
  $unlock_replay("isabelle_kissing_tips")
  $school_roof["isabelle_kiss"]+=1
  $school_roof["isabelle_kiss_today"] = True
  $game.advance()
  pause 1.0
  hide isabelle
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  return

label isabelle_talk_broken:
  show isabelle sad with Dissolve(.5)
  isabelle sad "Sorry, I have to go."
  window hide
  hide isabelle with Dissolve(.5)
  return

label isabelle_flirt_broken:
  show isabelle sad with Dissolve(.5)
  isabelle sad "Sorry, I have to go."
  window hide
  hide isabelle with Dissolve(.5)
  return
