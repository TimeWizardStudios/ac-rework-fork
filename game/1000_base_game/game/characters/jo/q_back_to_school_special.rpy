label jo_quest_back_to_school_special:
  $jo.unequip("jo_bra")
  show jo smile with dissolve
  "Whoa... even [jo] looks different. Younger, happier, and less resentful toward me."
  jo embarrassed "What's wrong? You look like you've seen a ghost!"
  jo smile "Well if you're down here now, you'll make it to the bus. Tell [flora] I said goodbye, okay?"
  jo flirty "Come here. Let me give you a kiss."
  show jo JoMorningKiss with Dissolve(.5)
  "[jo] showing affection? That's new."
  "Well, not really new."
  "All those years of slowly losing her glow. Man, it's good to see her so perky again."
  "And to feel those forbidden sensations that only the most deprived and sexually starved person can ever experience."# from their mother."
  "The sweet familiar scent of her mango-pine perfume, with just a hint of sweat."
  "The skirt hugging the full curves of her mature figure, straining to contain her hips."
  "The way she sometimes rocks her blazer without a bra, just for the thrill of it. Just to feel young and hot again!"
  "The wet tingle on my forehead after the kiss."
  "This time around, being good should probably be one of my priorities."
  mc "Mmm... thanks, [jo]! I needed this."
  jo smile "Have a good day, now! Don't be late!"
  $jo.activity="left"
  $unlock_replay("jo_kiss")
  show jo smile at disappear_to_right
  "Nice to see [jo] so happy again."
  "Did she say that [flora] is here? It's been years since we last spoke."
  $jo.equip("jo_bra")
  $flora.unequip("flora_pants")
  show flora confused at appear_from_left(.5)
  flora "Morning."
  show flora confused at disappear_to_right()
  pause(.5)
  $quest.back_to_school_special.advance("talk_to_flora")
  return

label jo_talk_back_to_school_special:
  show jo smile with dissolve
  jo "Good morning! I hope you're ready for the new school year."
  hide jo smile with qdis
  $renpy.pause(0.5)
  return

label jo_flirt_back_to_school_special:
  show jo flirty with dissolve
  jo flirty "My handsome young man!"
  hide jo flirty with qdis
  $renpy.pause(0.5)
  return
