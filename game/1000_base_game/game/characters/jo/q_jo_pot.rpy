label quest_jo_pot_start:
  show jo confident with Dissolve(.5)
  jo "How's it going, honey?"
  mc "Not too shabby. I've been on a—"
  #SFX: phone ringing
  jo thinking_phone "Sorry, I have to take this."
  "Disruption always comes at the perfect timing when I'm about to tell her about something."
  "Well, guess she won't know about the glade or its satanic cult of beavers."
  "Or how I single-handedly defeated their leader..."
  jo thinking_phone "It's the doctor."
  jo thinking_phone "Tell me later, okay?"
  show jo thinking_phone at disappear_to_left
  "That's just typical."
  show flora confident flip at appear_from_right
  flora confident flip "You look like someone punched you in the face."
  mc "Thanks for noticing."
  flora excited  "I was more interested in how one would go about doing that."
  mc "..."
  flora neutral flip "So, who is [jo] talking to?"
  show flora neutral flip at move_to(.75)
  menu(side="left"):
    extend ""
    "\"The adoption services. You're getting a new home!\"":
      show flora neutral flip at move_to(.5)
      mc "The adoption services. You're getting a new home!"
      flora angry flip "I'm eighteen, idiot."
      mc "Could've fooled me."
      flora sarcastic flip "To be fair, most things do."
      mc "..."
      mc "I'm serious. Sometimes I forget you're an adult now. And that's not meant to be an insult."
      flora confused flip "What is it meant to be?"
      mc "I don't know. I guess I should start treating you as a grown-up."
      flora confused flip "Are you feeling okay?"
      mc "Yes! Learn to take a compliment."
      $flora.love+=1
      flora thinking "Was that a compliment?"
      mc "Yes... sort of."
    "\"An Antarctica researcher.\"":
      show flora neutral flip at move_to(.5)
      mc "An Antarctica researcher."
      mc "They've found an ancient civilization buried in the ice."
      flora flirty "What kind of civilization?"
      mc "I don't know, some elder gods or something. Wasn't really paying attention."
      $flora.lust+=1
      flora concerned "Oh em gee, stop lying!"
    "\"Your internship. They want me instead.\"":
      show flora neutral flip at move_to(.5)
      mc "Your internship. They want me instead."
      flora laughing "That's actually funny!"
      flora laughing "Too bad I don't intern at the comedy club."
      mc "I don't really have a comeback for that."
      if quest.flora_cooking_chilli.finished:
        mc "Only that I'm surprised that there's anyone left alive there after that chili."
        flora angry "My chili was great!"
        mc "I know at least one pair of lips that would disagree..."
        flora thinking "..."
        flora cringe "Gross!"
        mc "I don't think they're that gross."
        $flora.lust+=1
        flora angry "Can you stop talking about my pussy?"
        mc "Fine. We can revisit the topic later."
      else:
        mc "Only that successful comedians earn a lot of money."
        flora sarcastic flip "Who said anything about successful?"
        mc "Ugh..."
  mc "Anyway, if you must know, [jo] said it was the doctor calling."
  flora sad "Is she okay?"
  mc "I don't know. She doesn't tell me these things."
  flora sad "Maybe it's her throat again?"
  "Right, I totally forgot about that."
  "When [jo] worked her way to the top of the school board, the stress gave her some sort of chronic trachea ache."
  "Last time around, [flora] helped cure it somehow."
  "Not sure how, but she put [flora]'s name alone on her will after that."
  show flora sad at move_to(.75)
  show jo confident at appear_from_left(.25)
  jo confident "What are you two whispering about?"
  flora neutral flip "Just wondering how you're feeling. Is it your throat again?"
  jo thinking "Yes, it's been starting to act up again. Thanks for remembering, sweetie."
  flora sad "Wish I could do something to help."
  jo laughing flip "Aw, sweetheart. You really make these old bones hurt a little less."
  "Hmm... this is going down the same path as last time..."
  "Need to do something about it. [flora] is already her favorite, but this feels like one of those crossroad moments."
  "Surely, there must be some information about it on the internet."
  hide jo
  hide flora
  with Dissolve(.5)
  $quest.jo_potted.start()
  return

label quest_jo_pot_getnurse:
  show nurse concerned with Dissolve(.5)
  nurse concerned "[mc], are you drinking enough water? You look a little parched."
  mc "You're absolutely right. I'm parched in ways you can't even begin to imagine."
  mc "That's part of why I'm here."
  nurse annoyed "That's... disappointing to hear. Have a bottle of water first."
  if not quest.jo_potted["nurese_gave_bottle"]:
    $mc.add_item("water_bottle")
    $quest.jo_potted["nurese_gave_bottle"] = True
  nurse annoyed "Try to stay away from the sodas in the cafeteria if you can."
  mc "Thanks, but my thirst is a bit different."
  mc "Anyway, I've been getting into agriculture lately, and I was hoping you could give me a hand."
  nurse concerned "I don't really know anything about that..."
  mc "Neither do the oxen. They just follow the farmer's directions."
  nurse thinking "I suppose that's true, but I'm very short on time and have to get back to work..."
  show nurse thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"You'll make some time.\"":
      show nurse thinking at move_to(.5)
      mc "You'll make some time."
      mc "Because if you don't, you know what happens."
      nurse annoyed "I... didn't think the blackmail would mean flipping dirt on a field..."
      mc "Ah, you're worried it won't be interesting enough?"
      mc "Trust me, it'll be very interesting."
      nurse afraid "No, that's not what I meant!"
      mc "Let's get going."
      nurse concerned "Okay..."
      $quest.jo_potted.advance("plowcow")
      $nurse["plowcow_now"] = True
      $school_forest_glade["exclusive"] = "nurse"
      $process_event("update_state")
      show nurse concerned at disappear_to_right
      pause(1)
    "\"No worries, I'll find another way.\"":
      show nurse thinking at move_to(.5)
      mc "No worries, I'll find another way."
      mc "You would've looked hot plowing that field for me, but I understand."
      if nurse.love>=3:
        nurse blush "I admire you thinking about organic produce."
        nurse blush "I have a break now... I could probably help you out."
        show nurse blush at move_to(.75)
        menu(side="left"):
          extend ""
          "\"No, I've changed my mind. You're too nice.\"":
            show nurse blush at move_to(.5)
            mc "No, I've changed my mind. You're too nice."
            if not quest.jo_potted["nurese_gave_love"]:
              $nurse.love+=2
              $quest.jo_potted["nurese_gave_love"] = True
            nurse concerned "Oh, okay then."
            nurse concerned "I... I hope you have a good day."
            nurse blush "I'm just going to go..."
            "The [nurse] looks so embarrassed, her face is like a tomato."
            "She probably would've enjoyed plowing the fields, but maybe she's not ready for that kind of humiliation."
            "It's better to be nice to her for now."
            hide nurse blush with Dissolve(.5)
          "\"All right, follow me.\"":
            show nurse blush at move_to(.5)
            mc "All right, follow me."
            nurse smile "Where are we going?"
            mc "To my field, of course."
            mc "I told you. You're going to plow it for me."
            nurse annoyed "O-okay..."
            $quest.jo_potted.advance("plowcow")
            $nurse["plowcow_now"] = True
            $school_forest_glade["exclusive"] = "nurse"
            $process_event("update_state")
            show nurse annoyed at disappear_to_right
            pause(1)
      else:
        nurse blush "Okay..."
        nurse blush "I'm just going to go..."
        "The [nurse] looks so embarrassed, her face is like a tomato."
        "She probably would've enjoyed plowing the fields, but maybe she's not ready for that kind of humiliation."
        "It's better to be nice to her for now."
        hide nurse blush with Dissolve(.5)
  return

label quest_jo_pot_nurse_plow:
  $unlock_replay("nurse_plow")
  show nurse thinking with Dissolve(.5)
  #nurse annoyed "What is this place?"
  #mc "It's my little farm... sort of."
  #nurse thinking "It doesn't look like a farm to me."
  #mc "Yeah, it still requires a bit of work, but that's why I brought you here."
  nurse thinking "I still don't know what you have in mind..."
  mc "See that patch of grass over there? That needs plowing."
  nurse smile "How are you going to do that?"
  mc "I got this plow and now I got the cattle to pull it."
  nurse annoyed "I don't see any cattle..."
  mc "Well, I took a picture of her earlier. Have a look."
  nurse neutral "..."
  nurse surprised "That's me!"
  mc "Right! You're going to be my little plow-cow today!"
  nurse afraid "You can't be serious..."
  mc "Well... it's up to you, really. Do you want to keep your job and reputation?"
  nurse sad "I do, but—"
  mc "There's no two ways about it. And you're already here, so let's get you naked and ready for the plow."
  nurse surprised "N-naked?!"
  mc "I mean, unless you want your scrubs and coat to get all dirty..."
  nurse annoyed "..."
  mc "Come on, we're wasting precious daylight."
  mc "Nothing to be embarrassed about out here anyway. Just forest critters."
  nurse annoyed "I... I suppose..."
  #$nurse.unequip("nurse_outfit")
  mc "It's fine, you can give me your clothes. I'll make sure they're safe."
  $nurse.unequip("nurse_shirt")
  nurse annoyed "Now what?"
  mc "Are you giving me attitude?"
  nurse afraid "No! I wasn't!"
  mc "Well, then... what are you waiting for?"
  nurse thinking "Waiting for?"
  mc "Take off the rest. Now."
  nurse afraid "..."
  $nurse.unequip("nurse_bra")
  mc "That wasn't so hard, was it?"
  $nurse.unequip("nurse_pantys")
  nurse annoyed "I... I guess not..."
  mc "Well then, Daisy. Let's get you harnessed up for the plow."
  #Fade to black (blackscreen until fade in)
  show black with fadehold
  $set_dialog_mode("default_no_bg")
  mc "..."
  #SFX: rustle of chains, creaking of leather
  mc "Okay, there we go."
  mc "Are you ready?"
  nurse "Y-yes..."
  mc "Pull!"
  show nurse plowcow
  hide black with Dissolve(.5)
  $set_dialog_mode()
  mc "Pull, my little moo-cow!"
  nurse "..."
  "Watching her muscles strain, her thighs flexing against the weight of the plow, lights something dark within me."
  "It's in the way her eyes crackle with determination, while her cheeks burn in utter shame."
  "I definitely struck gold that day in the [nurse]'s office. Whenever that photo enters my mind, it fuels a primal sadism."
  "And it's a perfect match because deep down, she wants this too. I can see the submissiveness in her downcast eyes."
  "She has urges that might be darker than mine, which is so refreshing when it comes to women."
  "Vanilla is the lamest form of sexual desires. It's like eating a bowl of pure rice. Sure, it satisfies the needs, but there's no flavor."
  "If only I had a larger field to plow, I'd let her work in the sun for hours. Driving her trembling legs on with the harsh crack of my whip."
  "Seeing the sweat drip down her back, following her spine before seeping into her asscrack."
  "Smelling the trailing scent of her pussy as it weeps in humiliation."
  "There's nothing quite like it."
  $nurse.lust+=3
  #Fade to black
  $game.advance()
  #Fade in
  #show Agricultured Meadow with Dissolve(.5)
  $school_forest_glade["farm"] = "farm"
  $quest.jo_potted.advance("plant")
  $process_event("update_state")
  show nurse sad with Dissolve(.5)
  mc "All done! I'm so proud of you!"
  nurse blush "Thank you..."
  nurse blush "Can I go now?"
  mc "Oh, you can leave whenever you want. I'm not keeping you."
  nurse sad "Okay... just don't tell anyone about this."
  mc "Why would I do that? You did everything I asked. That's the deal, remember?"
  nurse sad "Right."
  mc "All right, then. Off you go."
  if school_forest_glade['pollution']:
    mc "You probably don't want to wash off your feet in the stream here, it's a bit polluted."
  nurse concerned "Okay... I'll just get my clothes and..."
  $nurse.equip("nurse_shirt")
  $nurse.equip("nurse_bra")
  $nurse.equip("nurse_pantys")
  pause(.5)
  if nurse.at("school_forest_glade"):
    hide nurse with Dissolve(.5)
  else:
    show nurse concerned at disappear_to_left
  "I think she enjoys the humiliation, but it's probably exhausting as well."
  "No need to keep her longer than necessary. And there's always a next time."
  "That's the beauty of a good blackmailing scheme."
  "..."
  "Now, let's have a look at this patch of farmland."
  "My very own place on earth, where I can grow my seeds and enjoy nature while my plow cow sweats in the sun."
  "It ain't much, but it's honest work."
  if school_forest_glade["exclusive"]:
    $school_forest_glade["exclusive"] = 0
  return

label quest_jo_pot_tea_school:
  show jo confident with Dissolve(.5)
  jo confident "What's with the smile, kiddo?"
  mc "I've... learned to work the lands."
  jo smile  "I'm glad you have!"
  jo smile "Is that a metaphor for something? I'm struggling to keep up with the slang."
  jo eyeroll "Last night [flora] told me that her new hair dryer blows her skirt up."
  jo sarcastic "But apparently that just means it's really good and she's excited about it."
  mc "Right... I'm sure that's what it means. [jacklyn]'s a bad influence on her."
  mc "Anyway, I meant I've gotten into agriculture."
  jo smile "That is great news! I'm glad you're finally interested in something other than your computer and cartoons."
  mc "I actually grew some plants and I've made tea from their leaves. It's supposed to help with throat pain!"
  jo concerned "You did all that for me?"
  mc "Yes, is that so hard to believe?"
  jo confident "Of course not. I'm really proud of you. Empathy is a sign of maturity."
  mc "Would you like a cup?"
  jo smile "I most definitely do, but it'll have to wait until we get home."
  jo smile "The truly good teas often have a strong smell and I have a board meeting later."
  $quest.jo_potted["school_talk"] = True
  return

label quest_jo_pot_tea_home:
  if quest.jo_potted["school_talk"]:
    show jo flirty with Dissolve(.5)
    jo flirty "That smells nice! Is that the tea you made me?"
    mc  "Yeah, are you interested in a cup?"
  else:
    show jo confident with Dissolve(.5)
    jo confident "What's with the smile, kiddo?"
    mc "I've... learned to work the lands."
    jo smile "I'm glad you have!"
    jo smile "Is that a metaphor for something? I'm struggling to keep up with the slang."
    jo eyeroll "Last night [flora] told me that her new hair dryer blows her skirt up."
    jo sarcastic "But apparently that just means it's really good and she's excited about it."
    mc "Right... I'm sure that's what it means. [jacklyn]'s a bad influence on her."
    mc "Anyway, I meant I've gotten into agriculture."
    jo smile "That is great news! I'm glad you're finally interested in something other than your computer and cartoons."
    mc "I actually grew some plants and I've made tea from their leaves. It's supposed to help with throat pain!"
    jo concerned "You did all that for me?"
    mc "Yes, is that so hard to believe?"
    jo confident "Of course not. I'm really proud of you. Empathy is a sign of maturity."
    mc "Would you like a cup?"
  jo excited "Certainly."
  jo excited "I sure hope it helps with the pain."
  mc "It's supposed to have all sorts of anti-inflammatory qualities."
  mc "Also helps you relax. I know stress is what's causing the throat ache."
  $unlock_replay("jo_pot")
  $mc.remove_item("gigglypuff_tea")
  jo smile_cup "I appreciate it, honey."
  if flora.at("home_kitchen"):
    show jo smile_cup at move_to(.75)
    show flora annoyed flip at appear_from_left(.25)
    flora annoyed flip "Gross. What's that smell?"
    jo neutral_cup "Don't say that, [flora]."
    jo smile_cup "[mc] has been kind enough to brew me a cup of tea for my throat."
    flora skeptical "I would check the table of contents if I were you..."
    mc "It's entirely organic. I grew it myself."
    mc "Would you like a cup as well?"
    flora angry "No, thanks. I'll be in my room."
    show flora angry at disappear_to_left
    show jo smile_cup at move_to(.5,1)
    $flora.location="home_bedroom"
    "She stomped off all upset. She'd probably made plans to cure [jo]'s throat pain herself."
    "Sucks to suck, [flora]."
  jo smile_cup "Okay, let's give it a taste."
  jo confident_cup "..."
  jo concerned_cup "What's in this?"
  show jo concerned_cup at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Just err... tea leaves.\"":
      show jo concerned_cup at move_to(.5)
      mc "Just err... tea leaves."
      jo concerned_cup "It's very potent!"
      mc "Yeah, the plant is exotic. Nothing quite like it in the wild."
      jo confident_cup "..."
      jo smile_cup "I appreciate it, [mc]."
      mc "Anything to help you."
      $jo.love+=1
      jo smile_cup "Aw, sweetie!"
    "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|\"The plants are a distant relative to the Cannabis sativa plant.\"":
      show jo concerned_cup at move_to(.5)
      mc "The plants are a distant relative to the Cannabis sativa plant."
      jo neutral_cup "The drug?"
      mc "They don't seem related at all, kind of like me and [flora]."
      jo smile_cup "I'm not upset with you! I think it's a nice gesture."
      jo smile_cup "Don't tell [flora], but I used to smoke a good bit of pot when I first started as a school administrator..."
      jo smile_cup "It always helped with the stress and took the burn and aches out of my throat."
      jo confident_cup "..."
      $jo.lust+=1
      jo smile_cup "I used to be very sore down there after those late nights at the office."
    "?mc.strength>=4@[mc.strength]/4|{image=stats str}|\"Hard work, for the most part.\"":
      show jo concerned_cup at move_to(.5)
      mc "Hard work, for the most part."
      jo smile_cup "I've noticed you getting into shape lately."
      jo smile_cup "And I've been meaning to reward you."
      $mc.money+=25
      jo confident_cup "..."
      mc "Thanks, [jo]."
      jo smile_cup "I know it's not much, but if you keep this up, you can expect more of it in the future."
  jo smile_cup "And the tea is very effective! My throat feels better already."
  mc "Happy it works."
  jo skeptical flip "Gosh, I'm already getting sleepy... it's been such a long day..."
  jo skeptical flip "Thanks for the tea, honey. I think I'm going to head upstairs before I pass out on the floor. Have a good night!"
  show jo skeptical flip at disappear_to_right()
  "Not sure what's going on, but that's not the way to her bedroom."
  "..."
  mc "[jo]? Are you all right?"
  $jo.unequip("jo_blazer")
  $jo.unequip("jo_bra")
  #$jo.unequip("jo_panties")
  show jo thinking at appear_from_right()
  jo thinking "I'm really tired, honey. I'm going to bed now."
  mc "..."
  show black with Dissolve(.5)
  show jo drugged with Dissolve(.5)
  hide black with hpunch
  jo "Have a good night now, I think I'm—"
  jo "ZzZz.... zzZz..."
  "Seriously? That tea was a lot stronger than expected..."
  jo "ZzzzZzz... [flora]... ZzZ... I love you, [flora]... zZzz..."
  "Ugh, even in her sleep."
  mc "[jo]? This is the kitchen."
  "She's completely knocked out."
  "..."
  "It would be wrong, but..."
  "Maybe this is my only chance to see what's underneath that skirt..."
  "No one would know."
  "..."
  "But on the other hand, this is [jo]."
  "It wouldn't just be illegal, it'd be morally unjust."
  "But it's a once in a lifetime chance..."
  "Maybe just a quick peek. No one would be the wiser."
  "I'm going to hell for this..."
  show jo drugged_sleepy with Dissolve(.5)
  jo "zZz... zZz..."
  jo "[mc]... ZzZ... please take better care of yourself.... zZz..."
  "Fuck. Now I feel bad."
  "Maybe I should carry her upstairs? She needs to sleep it off."
  "Her throat is better so my job here is technically done."
  jo "zZzz... itchy... zZz... it's itchy..."
  menu(side="left"):
    extend ""
    "Carry her upstairs":
      mc "All right, [jo]. Let's get you to bed."
      show black with fadehold
      call goto_home_hall
      hide black with Dissolve(.5)
      #fade out
      #Jump Home Hall
      #Fade in
      "All right, she's fast asleep in her bed now."
      "I've done some shitty things in the past, but molesting someone who's asleep is where I draw the line."
      $jo.love+=2
      "[jo] might not remember, but doing the right thing shouldn't depend on if other people know."
      $mc.love+=2
      "As long as I know, it's a step in the right direction."
      "Crap. I forgot to leave the key to her room with her."
      "Oh, well, I'll just give it to her when she wakes up."
      $mc.add_item("key_jo_room")
      $quest.jo_potted.finish()
    "Scratch her itch":
      mc "Where... err.... where's the itch?"
      jo "zZz... it's so itchy..."
      "She's never complained or mentioned an itch before, and she's big on sharing these types of details with [flora]."
      "So, if my eavesdropping skills don't deceive me, maybe the itch is some kind of reaction to the tea."
      jo "zZz... uhhhh..."
      "She's getting really fidgety."
      jo "Oww..."
      "The old cramps stop her from reaching back."
      "She'd kill me if she found out, but..."
      "My boxers shouldn't feel this cramped, but..."
      "There's something about her squirming like this, helplessly trying to rip her pantyhose open."
      "Maybe it wouldn't hurt just to... help her?"
      "She would've done the same for me, right?"
      "That one time when I spilled ice cream on my dick and she had to suck it clean?"
      "Okay, that never happened, but still."
      "She's the helpful kind. Maybe it's time for me to... be a better person?"
      "Be better. That's what she always tells me."
      "Yeah..."
      #SFX: ripping nylon
      show jo drugged_ripped_hand with hpunch#Dissolve(.5)
      "A wave of musky sweat and strong perfume rolls over me."
      "Humid air, released from its nylon prison."
      "The smell of a mature woman's most intimate place."
      mc "There you go."
      show jo drugged_ripped with Dissolve(.5)
      "Squirming back and forth, her ass wiggles as the itch grows."
      jo "ZzZz... aaah... zZz... so itchy!"
      "She can't deal with it. It's out of her reach."
      "She used to be really flexible, but times change I guess."
      jo "ZzzZ... please..."
      "Since I caused the itch, maybe it's up to me to fix it?"
      "She's basically asking me to do it..."
      "Would it count as sexual assault to touch her there?"
      mc "[jo]! Wake up!"
      jo "ZzzzzzZzz..."
      "Should I call for [flora]?"
      "Err... not sure how I'd explain the ripped pantyhose though..."
      "Probably too late for that."
      jo "Aaaah! ZzzzZz... uhhh...."
      "Fuck it. I'm going in."
      "Maybe it's wrong, but it's also an emergency... sort of."
      "I've been raised to help people in need..."
      show jo drugged_scratch0 with Dissolve(.5)
      jo "Ahh! Yes! Just like that!"
      jo "ZzZzz..."
      "Shit, I almost thought she woke up."
      "Fuck me. Her juicy pussy lips kissing my fingers through the fabric of her panties!"
      "It's like touching a squishy fire of silk and jiggly flesh."
      "Man, the feeling of her warm ass in my hand."
      "So soft and loose. Not at all the firm muscular ass of a younger woman."
      "A perfectly ripened peach. Juicy and sweet. Certainly a forbidden fruit."
      jo "More... ZzZ..."
      "Well, she always tells me to listen to her."
      "And I don't feel like getting grounded."
      show jo drugged_scratch15 with Dissolve(.5)
      show jo drugged_scratch0 with Dissolve(.5)
      show jo drugged_scratch15 with Dissolve(.5)
      show jo drugged_scratch0 with Dissolve(.5)
      jo "That's the stuff... zZz...."
      show jo drugged_scratch15 with Dissolve(.5)
      show jo drugged_scratch0 with Dissolve(.5)
      show jo drugged_scratch15 with Dissolve(.5)
      show jo drugged_scratch0 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      "Her pussy juices leak through the fibres of her panties, wetting my fingers."
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      jo "Don't stop... zZz...."
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      show jo drugged_scratch3 with Dissolve(.5)
      jo "Deeper."
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch3 with Dissolve(.5)
      show jo drugged_scratch1 with Dissolve(.5)
      show jo drugged_scratch2 with Dissolve(.5)
      show jo drugged_scratch3 with Dissolve(.5)
      "So sticky. What does it taste like?"
      "I imagine younger girls taste all sweet and shit, but what about someone [jo]'s age?"
      show jo drugged_scratch4 with Dissolve(.5)
      mc "..."
      "Tangy... salty... very intense..."
      "Man..."
      "That's better than I ever imagined it."
      "Maybe... maybe I can get a taste straight from the source..."
      "The problem is if she wakes up... it's so risky."
      "But I never took risks in the past..."
      "..."
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      "Mmmm..."
      "The squishy flesh of her taint against my nose."
      "The earthy smell of her asshole mixing with the odour of the cream oozing out of her pussy."
      "The taste of heaven's most depraved garden."
      "She wiggles against the touch of my tongue. Sharp gasps escaping her lips."
      "Her pussy opens like a flower, inviting me deeper."
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      jo "Ohh!"
      "Damn it! I can't help myself."
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      "It's like licking perfectly seasoned honey right out of the pot. Only it tastes better than honey."
      "My tongue sweeping across the wet folds of her pussy..."
      "Exploring the warm wet ridges and valleys..."
      "Sucking in the juices..."
      jo "Oh my... oh my..."
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      show jo drugged_tongue1 with Dissolve(.5)
      show jo drugged_tongue2 with Dissolve(.5)
      jo "Ohh! Ohh!"
      "She's balancing on the edge of orgasm. A few more whips of my tongue could send her crashing... but I want more..."
      "I want to taste it all..."
      "Just a tiny taste of hell, that's all. She won't remember any of this..."
      "Always curious about eating ass..."
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      jo "Mmmmmmmmm!"
      "Fuck. The taste is harsh. Sweat, passion, and all sorts of darker flavors."
      "It's not horrible, but it's not exactly strawberries and cream either."
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      "Mmm... yeah."
      "The feeling of her wrinkled asshole against my tongue is like nothing I've ever experienced before."
      "Shudders of pleasure grips her mature body and prickles her skin."
      "She breathes faster, her heart racing. Her dreams plunging into the darkest pits of decadence."
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      jo "Hnnnnnnnnnnnnngh!"
      "Her asshole flirts with the tip of my tongue. Winking at it. Trying to suck me inside."
      "She's getting really close now, and stopping now would probably risk her waking up, more so than the other option..."
      "I'm going to make her come right here on the kitchen table with my tongue in her asshole."
      "[flora]'s going to have cereal here tomorrow, right in the spot where I tongue-fucked [jo]."
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      "Right where she came..."
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_tongueass1 with Dissolve(.5)
      show jo drugged_tongueass2 with Dissolve(.5)
      show jo drugged_orgasm
      jo "Fucking... cunt... whore... Ahh!" with vpunch
      if mc["moments_of_corruption"] < 3:
        $mc["moments_of_corruption"]+=1
        $game.notify_modal(None,"Guts & Glory","Moments of Corruption:\n"+str(mc["moments_of_corruption"])+"/3 Unlocked!",wait=5.0)
      "She came so hard... the contractions of her orgasm ripping through her muscles."
      jo "Fuck... ZzZ..."
      "And somehow, she didn't wake up. Somehow she's still lost in neverland, her dreams drifting toward the ceiling."
      "I wonder who she thinks about in her most depraved moments, when she's at the height of an orgasm."
      "Maybe I'll never know, but I'm definitely inclined to find out."
      if quest.jo_potted["corrupt"]:
        show jo drugged_stillitchy with Dissolve(.5)
        jo "zZz... itchy... uhhh..."
        "What the hell? Still?"
        "She's flexing her asshole at me this time. Maybe that's where the itch was all along?"
        jo "ZzZ... uhhh... huh?"
        "Damn it! Is she waking up? Fuck, fuck, fuck."
        "She's getting really squirmy. I've never seen anyone wiggle their ass like that."
        "Fuck, my tongue clearly didn't work."
        "Maybe she needs a legit scratching..."
        "There's nothing really good within reach..."
        "..."
        "Wait, I got it!"
        show jo drugged_brush1 with Dissolve(.5)
        jo "Hmmm?"
        "If anything can give a good scratch it's the rough bristles of a dish brush."
        "Just a slight pressure scratch... nothing too serious..."
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        jo "Mmmmmm!"
        "Damn, I didn't expect her to like it that much. She's pushing herself against the bristles."
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        jo "ZzZz... yes... more..."
        "If you say so..."
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        show jo drugged_brush2 with Dissolve(.5)
        show jo drugged_brush1 with Dissolve(.5)
        jo "Ah! Yesss!"
        "Man, she keeps pushing against it. It's almost as if she wants me to..."
        "Fine! Your funeral, lady."
        show jo drugged_insert1 with Dissolve(.5)
        jo "Uhhh!"
        "If she wakes up my life is over, but somehow she seems to like it."
        "That's that weird expression of satisfaction on her face. As if I've just found the right spot."
        jo "Zzz... just like that..."
        "The tea must've numbed her pain receptors... or she's more of an anal acrobat than many porn stars..."
        "Let's try a little deeper then... who am I to deprive her of relief?"
        show jo drugged_insert2 with Dissolve(.5)
        jo "Yes! Ahh! Scratch me!"
        "As you wish."
        show jo drugged_insert1 with Dissolve(.5)
        show jo drugged_insert2 with Dissolve(.5)
        show jo drugged_insert1 with Dissolve(.5)
        show jo drugged_insert2 with Dissolve(.5)
        show jo drugged_insert1 with Dissolve(.5)
        show jo drugged_insert2 with Dissolve(.5)
        jo "Mmmmm..."
        mc "More?"
        jo "Zzz... yes..."
        show jo drugged_insert1 with Dissolve(.5)
        show jo drugged_insert2 with Dissolve(.5)
        show jo drugged_insert1 with Dissolve(.5)
        show jo drugged_insert2 with Dissolve(.5)
        show jo drugged_insert1 with Dissolve(.5)
        show jo drugged_insert2 with Dissolve(.5)
        jo "Ahhh..."
        "A grin of bliss and satisfaction settles on her face. The insides of her asshole has been thoroughly scrubbed and scratched."
        "It would be funny to leave it in and let her wake up with a dish brush up her ass... but...  that wouldn't end well for me."
        "I need to hide this brush and move her over to the living room couch."
        show jo drugged_stillitchy with Dissolve(.5)
        $mc.add_item("dish_brush")
        $jo["at_none_now"] = True
        $process_event("update_state")
        hide jo with Fade(.5,3,.5)
        "There we go. She's fast asleep."
        "Hopefully, she won't remember any of this when she wakes up. That would be problematic."
      else:
        "Can't believe I just did that with [jo]. My mouth still tastes of her ass and pussy."
        "Well, I should probably put her on the living room couch so she can sleep it off."
        #Fade to black
        $jo["at_none_now"] = True
        $process_event("update_state")
        hide jo with Fade(.5,3,.5)
        "Not sure what she'll think of the ripped panties, but that's a problem for another day."
        "Hopefully, when she wakes up, she'll remember that I fixed her aching throat and forget all about her itchy pussy."
  $quest.jo_potted.finish()
  $jo.equip("jo_blazer")
  $jo.equip("jo_bra")
  return

















# =-=-=-=-=-=-=-=-=-=-=-=-ITEMS=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


label quest_jo_pot_house(item):
  if item == "umbrella":
    "Okay, let's poke around in there and see what comes out. Hopefully, a rake or shovel."
    "Just really something I can plow the field with."
    "..."
    "What the fuck is this?"
    $mc.add_item("power_plower")
    $school_forest_glade["got_plow"] = True
  else:
    "I'm not opposed to sticking my dick into new and exciting holes, but sticking my [item.title_lower] in there is too kinky even for me."
    $quest.jo_potted.failed_item("house",item)
  return

label quest_jo_pot_meadow(item):
  if item == "gigglypuff_seeds":
    "This looks like the perfect place to plant my seeds, but the ground needs to be plowed first. Need to find some equipment."
  elif item == "shovel":
    "A farmer thinks about plowing every seven seconds on average."
    "Some dream and study the art of plowing for years without ever getting to experience it firsthand."
    "As an avid student and plowing enthusiast, the mistake here is obvious."
    "To plow a field of such extraordinary beauty, one would need a tool more powerful than a mere shovel."
  elif item == "power_plower":
    "This seems like a lot of work. Maybe I could get someone else to do it for me?"
    menu(side="middle"):
      extend ""
      "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|Find someone else":
        "I know at least one person I can blackmail into plowing this field for me..."
        if quest.jo_potted != "plowcow":
          $quest.jo_potted.advance("getnurse")
      "?mc.strength>=5@[mc.strength]/5|{image=stats str}|Plow it yourself":
        "Alright, time to sweat and bleed."
        show black with fadehold
        $game.advance()
        $school_forest_glade["farm"] = "farm"
        $quest.jo_potted.advance("plant")
        hide black with Dissolve(.5)
        "That looks perfect for growing illegal substances."
      "Come back later":
        return
  else:
    "Even the birds will think I'm a crazy forest hermit if I start plowing the field with my [item.title_lower]."
    $quest.jo_potted.failed_item("meadow",item)
  return

label quest_jo_pot_earth_plant(item):
  if item == "gigglypuff_seeds":
    $mc.remove_item("gigglypuff_seeds")
    "There. Planted firmly in the soft earth!"
    "Now we just need sunlight, water, and time."
    $quest.jo_potted.advance("seeds")

  else:
    "Even though a garden of [item.title_lower] trees would be neat, planting this [item.title_lower] could jeopardize the whole ecosystem."
    "Nothing's more important than caring for Mother Earth!"
    $quest.jo_potted.failed_item("plant",item)

  return

label quest_jo_pot_earth(item):
  if not school_forest_glade["canal"] and item == "shovel":
    "Careful... very careful..."
    "You know what happens when one digs too greedily and too deep..."
    "You may awake something in the darkness..."
    "Shadow and flame..."
    "..."
    "Oh, look. It's done!"
    $school_forest_glade["canal"] = True
    $school_forest_glade["farm"] = "wet"
  elif item == "water_bottle":
    if school_forest_glade["canal"]:
      "No need to manually water these babies anymore. Just let the stream take care of it."
    elif school_forest_glade["farm"] == "wet":
      "Like trying to water seagrass. Not only ineffective, absolutely ridiculous."
      "I've watered it enough for today."
      "Now I've just got to wait."
      pass
    else:
      if quest.jo_potted == "seeds":
        if not quest.jo_potted["water"]:
          "Drink up, little seeds. Grow strong. Become the plants you were meant to be!"
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]=1
          "Well, one measly bottle of water makes no farm. Need more."
        elif quest.jo_potted["water"] == 1:
          "Pour it on."
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]+=1
          "Like hatchlings, they scream for more water. They're too young to understand the problems of greed."
        elif quest.jo_potted["water"] == 2:
          "There you go, gulp it down. Fill your little throats."
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]+=1
          $school_forest_glade["farm"] = "wet"
          $process_event("update_state")
          "And there we have it! Their thirst has been quenched. At least for the time being."
      elif quest.jo_potted == "small":
        if not quest.jo_potted["water"]:
          "That's refreshing, isn't it? Crystal clear, straight from a splashing mountain stream. Or at the very least a local tap."
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]=1
        elif quest.jo_potted["water"] == 1:
          "Drink! Drink! Grow strong, my saplings!"
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]+=1
        elif quest.jo_potted["water"] == 2:
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]+=1
          $school_forest_glade["farm"] = "wet"
          "That's it! The water quota has been met!"
          "Now we wait for the planets to align!"
          "That is to say, the sun and Earth aligning."
      elif quest.jo_potted == "medium":
        if not quest.jo_potted["water"]:
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]=1
          "They slurped that down like a parched Bedouin after a week in the desert."
        elif quest.jo_potted["water"] == 1:
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]+=1
          "They chugged that down like a normie at a pub after months in quarantine."
        elif quest.jo_potted["water"] == 2:
          "They sucked that down like a group of weebs at the hentai booth at comic-con."
          "Man, I miss comic-con."
          $mc.remove_item("water_bottle")
          $mc.add_item("empty_bottle")
          $quest.jo_potted["water"]+=1
          $school_forest_glade["farm"] = "wet"
          "Well, their thirst has been sated for now."

  else:
    if quest.jo_potted["water"] >=1:
      "These baby plants hunger for my [item.title_lower], but the truth is they need water to grow."
      "Maybe I'll grind down the [item.title_lower] and sprinkle the dust into the water to make them happy."
      "Anything for my babies. {i}Anything.{/}"
    else:
      "Plants need water, but I guess they can suck on my [item.title_lower] for a bit. That's it, little ones, extract all the nutrients."
    $quest.jo_potted.failed_item("earth",item)
  return

  # =-=-=-=-=-=-=-=-=-=-=-=-ITEMS=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


label quest_jo_pot_kettle_stove_boil(item):
  if item == "lamp":
    "Unscrew the lightbulb."
    "Plug it in the cord."
    "And just dip it... into the kettle..."
    $mc.remove_item("lamp")
    $home_kitchen["kettle"] = "lamp"
    #SFX: electricity  noises
    #SFX: flashing screen
    #show kettle_smoke with Dissolve(.5)
    "Now {i}this{/} is pot-racing!"
    $mc.add_item("gigglypuff_tea") #tbd tea item image? does there even need to be an item?
    $quest.jo_potted.advance("jodrink")
  else:
    "Hold up! Sticking my [item.title_lower] into that pot of water could be extremely dangerous."
    "Need to come up with something bright and clever. If only a lightbulb would pop up."
    $quest.jo_potted.failed_item("kettle_stove_boil",item)
  return

label quest_jo_pot_kettle_stove(item):
  if item == "water_bottle":
    if quest.jo_potted['water_added']:
      "It's already soaking wet" #tbd line for double water
    elif quest.jo_potted['leaves_added']:
      "Okay, added leaves and water. Time to turn up the heat."
      $mc.remove_item(item)
      $mc.add_item("empty_bottle")
      $quest.jo_potted.advance("boil")
    else:
      "That's water. Now for the leaves."
      $quest.jo_potted['water_added'] = True
      $mc.remove_item(item)
      $mc.add_item("empty_bottle")
  elif item == "gigglypuff_leaves" or item == "gigglypuff_leaves_corrupted":
    if item == "gigglypuff_leaves_corrupted":
      $quest.jo_potted['corrupt_tea'] = True
    $mc.remove_item(item)
    if quest.jo_potted['water_added']:
      "Okay, added leaves and water. Time to turn up the heat."
      $quest.jo_potted.advance("boil")
    else:
      "That's leaves. Now for the water."
      $quest.jo_potted['leaves_added'] = True
  else:
    "Boiling my [item.title_lower] would probably add some interesting flavors. But there's always a next time."
    $quest.jo_potted.failed_item("kettle_stove",item)
  return
