label quest_jo_day_start:
  show jo displeased_cup with Dissolve(.5)
  jo displeased_cup "Ugh... who uses bold text in a professional email? Who does she think she's talking to—"
  jo excited_cup "Oh! Hi, honey! I didn't see you there."
  "Whoa, she's stirring her tea way too hard. Her grip on that cup is tighter than the pussy of a sex-starved succubus."
  mc "Hey, [jo]. Are you okay? You look a little upset."
  "Hopefully, that didn't come off as sarcastic..."
  jo worried_cup "It's nothing you need to worry about, sweetheart."
  jo annoyed_cup "A colleague of mine had the audacity to criticize my work. Me!"
  jo annoyed_cup "{i}\"If you have any questions, don't hesitate to contact me.\"{/}"
  jo annoyed_cup "Oh, don't worry. I won't just be contacting you... I'll end you."
  jo annoyed_cup "{i}\"Kindest regards.\"{/}"
  window hide
  show jo annoyed_cupless with Dissolve(.5)
  play sound "cup_break"
  with vpunch
  show jo embarrassed with Dissolve(.5)
  window auto
  jo embarrassed "Oh, no! My mug!"
  jo embarrassed "It's broken!"
  window hide
  show black onlayer screens zorder 100
  show jo broken_cup_sad
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  jo broken_cup_sad "What else could make my day any worse?"
  "Even though she's on her knees, this is too sad to be hot..."
  "I think that was one of her favorite cups. [flora] gave it to her after winning the middle school science fair. She looks devastated."
  mc "[jo], are you really this upset over an email?"
  jo broken_cup_cringe "An email? {i}Just{/} an email? No."
  "Crap. I probably should've escaped when I had the chance."
  jo broken_cup_angry "I'm upset that you never lift a finger to help!"
  jo broken_cup_angry "[flora] and I do everything to keep a good home, and you..."
  if not home_bedroom["clean"]:
    jo broken_cup_angry "...you can't even clean your room!"
  jo broken_cup_angry "All you do is make excuses! You can't do anything for yourself!"
  jo broken_cup_angry "You're the most ungrateful... spoiled...!"
  "Ouch."
  jo broken_cup_sad "Oh... oh, no. I—"
  jo broken_cup_sad "I'll get the janitor."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 1.0
  $jo["at_none_now"] = True
  hide jo
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "How did talking about an email turn me into the bad guy?"
  "..."
  "She's not wrong, though."
  "I mean, really. When was the last time I even washed the dishes?"
  "Maybe today's the day to turn things around..."
  "I need to get back into [jo]'s good graces, or else she might kick me out like the bum I am."
  "First step is finding [flora]. She's fluent in female which is a language{space=-5}\nfar more complicated and nuanced than I'm equipped to handle."
  "Maybe she can tell me how to make this better?"
  $quest.jo_day.start()
  return

label quest_jo_day_flora:
  show flora sarcastic with Dissolve(.5)
  flora sarcastic "Hey, why do you look like that?"
  mc "Like what?"
  flora sarcastic "Guilty. Like you did something."
  mc "No reason. I always look this way."
  mc "I have a condition. It's called RGF."
  flora confused "What's RGF?"
  mc "Resting Guilt Face."
  flora eyeroll "That's the stupidest thing I've heard all day, and I've been talking\nto [maxine]."
  mc "Talking about what?"
  flora annoyed "None of your business!"
  mc "Aha-aha."
  flora eyeroll "Stop smirking."
  mc "I'm not smirking."
  flora annoyed "You're totally smirking!"
  mc "Fine, but I need your help."
  flora sarcastic "I knew it! You only want my help when you're in trouble!"
  flora sarcastic "So, what did you do now?"
  show flora sarcastic at move_to(.2)
  menu(side="right"):
    extend ""
    "\"Nothing! You're acting crazy.\"":
      show flora sarcastic at move_to(.5)
      mc "Nothing! You're acting crazy."
      flora concerned "I'm acting perfectly reasonable in the face of a cold-blooded liar."
      mc "I didn't do anything wrong!"
      mc "Stop reading into every twitch in my facial muscles, and listen."
      mc "[jo] may or may not be pissed at me for no reason in particular..."
      mc "So, unless you help me out, I'm going to make it your problem, too."
      $flora.love-=1
      flora angry "Ugh, I knew you did something!"
      mc "Maybe, but I'm going to make it all better."
      mc "I'm planning a surprise for her."
    "\"Something happened.\"":
      show flora sarcastic at move_to(.5)
      mc "Something happened."
      mc "[jo] is mad at me... more than usual this time."
      mc "Remember that mug you won at the science fair? Her favorite one?"
      flora worried "What did you do to her mug?"
      mc "Well, she said something about an email, and then she knocked\nit over."
      mc "I just asked what the email was about, and all of a sudden, she started ripping me a new one!"
      flora thinking "Hmm... that doesn't seem like [jo]. You must've done something else."
      mc "No idea, but I'm trying to do the right thing. I'm trying to make it up to her."
      $flora.love+=1
      mc "So, can you help me out? I'm planning a surprise for her."
  mc "Let's be honest, when was the last time she was celebrated?"
  flora confident "What do you mean? Every Mother's Day. I celebrate with her every year.{space=-70}"
  flora confident "In fact, we've celebrated every single Mother's Day as far back as I remember."
  mc "Oh... right."
  mc "Well, I'm trying to put together my own version of Mother's Day to win [jo] back."
  mc "Can you help me with ideas?"
  if home_bedroom["clean"]:
    flora thinking "I can think of a few things that would make [jo] tolerate you better..."
    flora worried "How about you take her out somewhere?"
  else:
    flora confident "I can think of a few things that would make [jo] tolerate you better..."
    flora concerned "For one, you never, ever clean."
    flora concerned "Everywhere you go, you leave a mess behind."
    flora concerned "You're like an animal of some kind. So, start with your bedroom."
    mc "I mean... we're technically all animals..."
    flora concerned "My ears are off. Don't even bother."
    flora neutral "How about you take her out somewhere?"
  mc "Where? There's nowhere she'd rather be except going to work and coming home."
  mc "You know how she is. Type-A workaholic."
  flora excited "Then don't leave the school grounds. She likes nature."
  flora excited "Have a picnic! You can bring sandwiches and a charcuterie board!"
  mc "Shark coochie board? I don't even know what that is."
  flora concerned "You know what? That's not surprising."
  mc "I don't really want to put in all that effort..."
  mc "Setting up a picnic? I've never done that before."
  flora eyeroll "That's just typical. You're too selfish for this."
  flora annoyed "It's not even a lot of effort."
  flora annoyed "[jo] would be ecstatic if you just {i}tried{/} doing something for her."
  flora annoyed "I wish I was that lucky. I have to bust my ass to stay her favorite."
  flora annoyed "Meanwhile, you could fold one single piece of laundry and you've gone above and beyond."
  flora sarcastic "Besides, the more repulsed you are by something, the more [jo] would like it."
  mc "I guess you're right..."
  flora sarcastic "You know I am. Always."
  mc "Except that one time when you said you could chug an entire glass of milk."
  flora cringe "You put a slug in it!"
  mc "But technically, you were wrong."
  flora cringe "And technically, you're a sicko!"
  mc "One man's sicko is another man's genius."
  flora annoyed "Ugh..."
  mc "Anyway, I have to go. There's a lot I have to get done now."
  if home_bedroom["clean"]:
    flora sarcastic "Good luck. I'll see you later."
    flora sarcastic "Hopefully, [jo] doesn't kick you out on the spot."
    mc "Pray for me..."
    hide flora with Dissolve(.5)
    $quest.jo_day.advance("supplies")
  else:
    mc "Unless... you want to help me?"
    show flora annoyed at move_to (.75)
    menu(side="left"):
      extend ""
      "?flora.love>=10@[flora.love]/10|{image=flora contact_icon}|{image=stats love_3}|\"You want to\nhelp me, right?\"":
        show flora annoyed at move_to (.5)
        mc "You want to help me, right?"
        flora skeptical "Not really..."
        mc "That doesn't sound like an outright \"no.\""
        flora skeptical "I don't want [jo] to be upset is all."
        flora skeptical "I wouldn't be helping you. I would be helping her."
        flora sad "She's been really stressed out lately... I hate to see her like that."
        flora sad "And if I leave it all to you, you're gonna mess something up."
        flora skeptical "You'll wash the whites with the darks, knowing you."
        mc "Huh?"
        flora smile "Exactly."
        flora smile "I'll take care of your bedroom. You gather the picnic supplies."
        $flora["hidden_now"] = True
        window hide
        show flora smile at disappear_to_right
        pause 1.0
        $quest.jo_day["flora_help"] = True
        $home_bedroom["clean"] = True
        $quest.jo_day.advance("supplies")
        window auto
      "\"I mean, you're basically a master at this.\"":
        show flora annoyed at move_to (.5)
        mc "I mean, you're basically a master at this."
        flora concerned "Flattery will get you nowhere."
        mc "What would?"
        flora confident "You could convince me, I'm sure."
        mc "How?"
        flora confident "The same way you'd \"convince\" anyone else."
        flora confident "What makes the world go round?"
        mc "Seriously? You're extorting me?"
        flora excited "It's not extortion, it's commerce! The trade of goods and services."
        mc "How much?"
        flora neutral "Two-hundred bucks. That's my final offer."
        show flora neutral at move_to(.2)
        menu(side="right"):
          extend ""
          "?mc.money>=200@[mc.money]/200|{image=ui hud icon_money}|\"That's outrageous!\nHere you go.\"":
            show flora neutral at move_to(.5)
            $mc.money-=200
            mc "That's outrageous! Here you go."
            flora laughing "Pleasure doing business with you!"
            flora flirty "I'll take care of your bedroom. You gather the picnic supplies."
            $flora["hidden_now"] = True
            window hide
            show flora flirty at disappear_to_right
            pause 1.0
            $quest.jo_day["flora_help"] = True
            $home_bedroom["clean"] = True
            $quest.jo_day.advance("supplies")
            window auto
          "\"You're out of your mind.\"":
            show flora neutral at move_to(.5)
            mc "You're out of your mind."
            flora laughing "Am I? I guess you'll just have to do your own grunt work, then."
            flora laughing "Hopefully, [jo] doesn't kick you out on the spot."
            mc "Pray for me..."
            hide flora with Dissolve(.5)
            $quest.jo_day.advance("cleaning")
  return

label quest_jo_day_cleaning_vacuum_cleaner:
  "Better late than never, I guess..."
  $quest.jo_day["vacuum_cleaner_taken"] = True
  $mc.add_item("vacuum_cleaner")
  return

label quest_jo_day_cleaning:
  "This place is a dump. No wonder [jo] lashed out at me earlier."
  "I really need to give it a once-over..."
  if mc.owned_item("vacuum_cleaner"):
    "Best get to work."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $home_bedroom["clean"] = True
    $game.advance(hours=2)
    pause 3.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    "Okay, much better! Almost looks presentable now."
    "Now for the picnic supplies..."
    $quest.jo_day.advance("supplies")
  else:
    "The vacuum cleaner should be in the hallway closet outside."
  return

label quest_jo_day_cleaning_flora:
  "Wow, I should get [flora] to do this more often. She's a natural!"
  "Hopefully, I didn't leave out anything that would make her think less of me..."
  "..."
  "What am I saying? This whole room is a NEET den."
  "A weeb's wet dream."
  "At least it's clean for once."
  $quest.jo_day["flora_helped"] = True
  return

label quest_jo_day_supplies_hole_interact:
  "A long time ago, [flora] and [jo] used to go on regular hikes."
  "I was never eager to go even though they always invited me."
  "The old blanket they used should be up here somewhere... I just have to find it."
  return

label quest_jo_day_supplies_hole_take:
  if ("quest_jo_day_supplies_hole_interact",()) not in game.seen_actions:
    call quest_jo_day_supplies_hole_interact
  "..."
  "..."
  "Aha! Found it!"
  $quest.jo_day["picnic_blanket_taken"] = True
  $mc.add_item("picnic_blanket")
  return

label quest_jo_day_supplies_mini_fridge_interact:
  "Huh? There's a compartment behind the fridge."
  "I've never gone this deep before..."
  return

label quest_jo_day_supplies_mini_fridge_take:
  if ("quest_jo_day_supplies_mini_fridge_interact",()) not in game.seen_actions:
    call quest_jo_day_supplies_mini_fridge_interact
  "Oh, what's this?"
  $quest.jo_day["jo_secret_wine_taken"] = True
  $mc.add_item("jo_secret_wine")
  return

label quest_jo_day_supplies_teacher_desk_mrsl:
  if not quest.jo_day["steal_attempt"]:
    show mrsl concerned with Dissolve(.5)
    mrsl concerned "You're looking awfully captivated by my mug today. Does someone need coffee?"
    mrsl concerned "Unfortunately, it's against health and safety protocols for me to give you a sip..."
    mrsl excited "But you can purchase some in the cafeteria!"
  elif quest.jo_day["steal_attempt"] == 1:
    show mrsl skeptical with Dissolve(.5)
    mrsl skeptical "Should I be concerned? You haven't taken your eyes off of my mug."
    mrsl surprised "You haven't even blinked!"
    mrsl surprised "You should probably go see the [nurse] if you're not feeling well."
  elif quest.jo_day["steal_attempt"] == 2:
    show mrsl cringe with Dissolve(.5)
    mrsl cringe "Oh, I see now. We have a little kleptomaniac on our hands."
    mrsl cringe "Well, I'm not going to just let you have it. I'm keeping a close eye on you, [mc]."
  hide mrsl with Dissolve(.5)
  $quest.jo_day["steal_attempt"]+=1
  return

label quest_jo_day_supplies_teacher_desk_mrsl_repeat:
  show mrsl annoyed with Dissolve(.5)
  mrsl annoyed "You're on thin ice, mister."
  hide mrsl with Dissolve(.5)
  return

label quest_jo_day_supplies_teacher_desk_take:
  "Thankfully, [jo] isn't the only caffeine-addicted MILF in the school..."
  $quest.jo_day["mug_taken"] = True
  $mc.add_item("mug")
  "Yoink! Mine now."
  return

label quest_jo_day_supplies_plant_one:
  "[jo] has a spidey sense about moving things in the kitchen. There's no way I'd get away with taking this."
  return

label quest_jo_day_supplies_plant_two:
  "Touching this plant is a death sentence. [maxine] will reign down nukes on me if I even breathe on this."
  return

label quest_jo_day_supplies_plant_three:
  "The [nurse]'s prized bonsai. I should probably find a less expensive plant."
  return

label quest_jo_day_supplies_plant_four:
  "This plant is glued to the floor."
  "Too many people tripped over it while trying to get downstairs."
  return

label quest_jo_day_supplies_plant_five:
  "The school will probably sue for grand larceny if I take this home with me..."
  return

label quest_jo_day_supplies_cactus_interact:
  "This isn't quite a bouquet of roses, but it'll have to do on such short notice."
  return

label quest_jo_day_supplies_cactus_take:
  if ("quest_jo_day_supplies_cactus_interact",()) not in game.seen_actions:
    call quest_jo_day_supplies_cactus_interact
  $school_english_class["cactus_taken"] = True
  $mc.add_item("cactus")
  return

label quest_jo_day_supplies_paint_shelf_interact:
  "One of these old paint jars should do..."
  "Hopefully, [jo] will think it's something I made."
  return

label quest_jo_day_supplies_paint_shelf_take:
  if ("quest_jo_day_supplies_paint_shelf_interact",()) not in game.seen_actions:
    call quest_jo_day_supplies_paint_shelf_interact
  $quest.jo_day["paint_jar_taken"] = True
  $mc.add_item("paint_jar")
  return

label quest_jo_day_supplies_picnic(item):
  if item == "picnic_blanket":
    $school_forest_glade["picnic_blanket"] = True
    $mc.remove_item("picnic_blanket")
    "Feeling the blades of grass touch my fingers as I smooth out this blanket is the most peace I've felt in a while."
  elif item == "jo_secret_wine":
    $school_forest_glade["jo_secret_wine"] = True
    $mc.remove_item("jo_secret_wine")
    "Time to crack open a cold one with the boys... I mean, [jo]."
  elif item == "cactus_in_a_pot":
    $school_forest_glade["cactus_in_a_pot"] = True
    $mc.remove_item("cactus_in_a_pot")
    "[jo] would probably expect a boring old bouquet, but she'll be pleasantly surprised when she sees this."
  elif item == "love_potion_droplet":
    $school_forest_glade["love_potion_droplet"] = True
    $mc.remove_item("love_potion_droplet")
    "Everyone knows drugging a girl is the best way to earn her trust and respect."
    if 18 > game.hour > 6:
      "All right, that should be all of it..."
      "Time to clear the air with [jo] before she ends up hating me forever."
    else:
      "Puh! This whole setting up a picnic ordeal has taken its toll on me..."
      "I should probably get some sleep before attempting to clear the air with [jo]."
    $quest.jo_day.advance("amends")
  else:
    "While I do like living on the wild side, this [item.title_lower] wouldn't protect me from a coordinated ant attack."
    $quest.jo_day.failed_item("picnic",item)
  return

label quest_jo_day_supplies_maxine:
  "There we go! It's nothing special, but hopefully she'll forgive me."
  "Although... saying the wrong thing might just set her off again..."
  "I need some kind of guarantee that the picnic will go smoothly."
  "Maybe [maxine] has some brain-zapper gun that could help me soothe{space=-15}\n[jo]'s erratic temper?"
  $quest.jo_day.advance("maxine")
  return

label quest_jo_day_maxine:
  show maxine annoyed with Dissolve(.5)
  mc "Before you say anything, hear me out."
  mc "I have a mind-blowing idea for you."
  maxine annoyed "I'd rather my mind stay intact, actually."
  mc "What if there was a way to instantly make someone more... suggestible, if you know what I mean?"
  maxine skeptical "You could do anything with a tool like that in your arsenal. Dreams are the limit."
  mc "So, do you have anything like that?"
  maxine sad "I could probably... but it would take..."
  maxine sad "I would just need to distill..."
  maxine sad "Split the nine... carry the two..."
  maxine sad "Then I would compound those...."
  maxine thinking "Find the [nurse] and tell her that I need Queen's Rue."
  maxine thinking "She'll know what it is."
  mc "Queen's Rue? Sounds exotic."
  mc "If I get that for you, will you make me something to help with my girl problems?"
  maxine laughing "Sorry, you can't be trusted not to give girls problems."
  maxine smile "But I'll spare you a drop from my mixture."
  mc "Just a drop? Can't you give me, like, a bottle?"
  maxine smile "No, just a drop will do."
  hide maxine with Dissolve(.5)
  $quest.jo_day.advance("nurse")
  return

label quest_jo_day_nurse:
  show nurse smile with Dissolve(.5)
  "The [nurse] always looks so happy when she's strolling through the woods."
  "I wonder if she was a dainty little woodland critter in a past life..."
  "I wouldn't put it past her."
  mc "What's up, doc?"
  nurse afraid "Oh, golly! You scared me!"
  mc "Sorry about that."
  nurse neutral "That's all right. I'm just out here foraging for some wild seeds."
  nurse smile "Organic, locally sourced grains are an important part of a balanced diet.{space=-80}"
  mc "Your diet?"
  nurse annoyed "I'm trying to shed a few pounds here and there..."
  "She really doesn't need to. Look at those love handles. A little extra never hurt anyone."
  if mc.owned_item(("compromising_photo","damning_document")):
    mc "I like you the way you are. No more dieting."
    nurse blush "Aw, thank you!"
    mc "That was an order, not a compliment."
    $nurse.lust+=2
    nurse concerned "O-oh..."
  else:
    mc "I think you look great."
    $nurse.love+=1
    nurse blush "Thank you, [mc]."
  mc "Anyway, [maxine] said you would know about something called \"Queen's Rue?\""
  nurse thinking "Oh, sure! It's a native flora quite common in Newfall."
  nurse thinking "You should be able to find it here."
  if mc.owned_item(("compromising_photo","damning_document")):
    mc "Actually, you're going to find it for me."
    nurse neutral "Oh, um..."
    mc "Is that a problem?"
    nurse afraid "No, no! Of course not!"
    mc "It shouldn't be too hard, if it's as common as you say."
    nurse annoyed "Yes, I'll... I'll find a few bulbs for you..."
    mc "Good girl."
  else:
    mc "I was actually hoping you could help me..."
    mc "I'm awful at botany. I can't tell a dandelion apart from a regular lion."
    nurse smile "Oh, that's no bother! I'll see if I can find a few bulbs for you while I'm out here."
    mc "Thanks, I appreciate it!"
  $nurse["at_none_now"] = True
  window hide
  if mc.owned_item(("compromising_photo","damning_document")):
    show nurse annoyed at disappear_to_right
  else:
    show nurse smile at disappear_to_right
  pause 1.0
  $quest.jo_day["nurse_harvest"] = True
  window auto
  return

label quest_jo_day_nurse_harvest:
  show nurse smile with Dissolve(.5)
  mc "How did that go? Were you able to find the Queen's-whatever?"
  nurse smile "I only found a few, but I hope it's enough..."
  $mc.add_item("queen_rue")
  if mc.owned_item(("compromising_photo","damning_document")) and nurse["strike_book_activated"]:
    mc "We'll just add another strike if it's not."
    nurse afraid "I can go out again if you need more!"
    nurse annoyed "Is there anything else I can help you with?"
  else:
    mc "I'm sure this is fine. Thank you!"
    nurse smile "Don't mention it!"
    nurse smile "Is there anything else I can help you with?"
  "If people would take the time to get to know the [nurse], they would see how polite and sweet she is."
  "It's a shame everyone takes advantage of her generosity..."
  "...including me."
  mc "No, thanks. That's all for now."
  nurse thinking "Did [maxine] give you anything for me?"
  mc "Uh... no, I don't think so..."
  nurse sad "Strange. She usually has something for me."
  mc "What kind of thing?"
  nurse smile "She makes keto-friendly energy bars to keep me going throughout the day."
  nurse smile "I had to go a really long way to get all of those. Quite a workout!"
  show nurse smile at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.money>=5@[mc.money]/5|{image=ui hud icon_money}|\"How about I'll buy\nyou a snack instead?\"":
      show nurse smile at move_to(.5)
      mc "How about I'll buy you a snack instead?"
      nurse concerned "Really? You don't have to do that."
      $mc.money-=5
      mc "Here, have a couple of dollars."
      $nurse.love+=1
      nurse blush "How sweet of you."
      nurse blush "Try not to land yourself in my office any time soon!"
      mc "A difficult task considering my wild lifestyle."
      nurse blush "Oh, you..."
      hide nurse with Dissolve(.5)
    "\"Yeah, well... that's too bad.\"":
      show nurse smile at move_to(.5)
      mc "Yeah, well... that's too bad."
      mc "Take it up with [maxine]. I gotta go."
      $nurse.love-=1
      hide nurse with dissolve2
      "Shifting the blame onto someone else always works."
  $quest.jo_day.advance("love_potion")
  return

label quest_jo_day_love_potion(item):
  if item == "queen_rue":
    show maxine thinking with Dissolve(.5)
    mc "Here you go. The [nurse] went through a lot of trouble to get these for you."
    $mc.remove_item("queen_rue")
    mc "She also said something about you owing her an energy bar?"
    mc "And it has to be keto-friendly."
    maxine sad "I've never claimed to be a dietician, but it technically causes ketosis.{space=-10}"
    mc "Is it organic and cruelty-free?"
    maxine thinking "No."
    mc "Anyway, I gave you the fruits of her labor. What about that potion?"
    maxine skeptical "They aren't fruits. They're highly reactive, conscience-altering narcotics."
    mc "Uh, are you sure they're safe, then?"
    mc "I mean, should a high-schooler really be handling this?"
    maxine annoyed "I've done extensive studies on this very substance."
    maxine confident "However, I need pure water to complete the mixture. Bottled water should be fine."
    maxine confident "Get me some, and we'll have a top-shelf love potion on our hands."
    if mc.owned_item(("water_bottle","spray_water")):
      mc "Oh, I actually have a water bottle on me already."
      call quest_jo_day_love_potion_water(mc.owned_item(("water_bottle","spray_water")))
    else:
      hide maxine with Dissolve(.5)
  else:
    "Although my [item.title_lower] sounds just as exotic, I don't need another lecture from [maxine] about how valuable her time is."
    $quest.jo_day.failed_item("love_potion",item)
  return

label quest_jo_day_love_potion_water(item):
  if item in ("water_bottle","spray_water"):
    if not renpy.showing("maxine"):
      show maxine confident with Dissolve(.5)
    mc "Here's the final piece to our puzzle."
    $mc.remove_item(item)
    maxine eyeroll "Puzzle to you, maybe."
    maxine eyeroll "It's perfectly straightforward for the rest of us."
    "Who exactly falls into the \"us\" category?"
    "She's probably the leader of some secret society..."
    maxine confident "Follow me. This mixture can only be brewed in the girls' bathroom."
    mc "I can't go in there."
    maxine confident "Exactly. You're catching on."
    window hide
    show maxine confident at disappear_to_right
    pause 0.5
    show black onlayer screens zorder 100 with Dissolve(.5)
    call goto_school_first_hall_east
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    "[jo] will never be able to call me lazy again."
    "Look at all the effort I went through just to spike her drink!"
    "She's going to be more affectionate towards me than ever before."
    "I'm totally gonna rub [flora]'s nose in it, too."
    "..."
    "..."
    mc "Are you done yet?"
    maxine "Perfection takes time!"
    window hide
    play sound "bubbles" volume 0.5
    pause 2.0
    window auto
    maxine "The perfect viscosity has been reached."
    window hide
    show maxine excited at appear_from_right
    pause 0.5
    window auto
    maxine excited "Here you go."
    $mc.add_item("love_potion_droplet")
    if item == "water_bottle":
      $mc.add_item("empty_bottle")
    elif item == "spray_water":
      $mc.add_item("spray_empty_bottle")
    mc "Seriously? You're really only giving me a drop?"
    mc "I thought that was just a joke!"
    maxine concerned "I would never joke about top government secrets."
    mc "I gave you an entire bottle of water and enough of those flowers for a whole cauldron full!"
    maxine angry "And I gave you the exact amount I said I would."
    mc "What a rip off."
    maxine smile "Trust me, a drop is all you need."
    maxine laughing "Besides, someone like you can't really be trusted with any more than that."
    "That's actually a fair point."
    mc "I think you're being a little presumptuous..."
    maxine thinking "Hardly."
    maxine thinking "You're on your own now. Adieu."
    show maxine thinking at disappear_to_left
    "Oh, well. At least I got what I came for."
    $quest.jo_day.advance("droplet")
  else:
    "If only I could somehow liquify my [item.title_lower], maybe [maxine] would consider adding it to her mixture. Alas."
    $quest.jo_day.failed_item("bottled_water",item)
  return

label quest_jo_day_amends_home:
  "Approaching [jo] before she has had a cup of tea is risky."
  "I should probably wait until she has made her way to the cafeteria."
  return

label quest_jo_day_amends:
  show jo sad with Dissolve(.5)
  mc "Hey, [jo]."
  jo sad "[mc]..."
  mc "I just wanted to say sorry. Sorry for being such a worthless leech over the years."
# mc "I just wanted to say sorry. Sorry for being such a worthless son over the years." ## Incest patch ##
  mc "I know I've made things unnecessarily hard for you by being lazy and selfish."
  mc "I didn't think of all the ways my bad habits would affect you or [flora].{space=-15}"
  jo worried "Oh, honey... I should be the one apologizing."
  jo worried "I must've hurt your feelings so badly when I said those horrible things to you."
  jo worried "I lost my temper. I had a bad day and it made me lash out."
  mc "Well, you weren't exactly wrong."
  mc "I do need to act more like an adult. More like you."
  mc "Here, I got you something."
  $mc.remove_item("mug")
  jo laughing_cup "Oh! How lovely!"
  jo excited_cup "It looks so... familiar. Like I've seen it somewhere before."
  mc "Anyway! I also put together a picnic."
  mc "I wanted to celebrate a day that I've never taken into consideration before..."
  mc "A day that you deserve more than anyone else."
  mc "Mother's Day!"
  mc "Because, let's be honest, you're the mother of every student in Newfall High."
  jo blush "Aw, sweetheart!"
  jo blush "A picnic? How elaborate!"
  jo blush "Did you make a charcuterie board?"
  mc "Uhhh..."
  jo flirty_hands_to_the_side "Nevermind, that might be asking too much. I'm sure it's perfect the way it is!"
  mc "Could you meet me in the forest glade behind the school at sunset?"
  jo sarcastic "At sunset? Meaning we'll get to watch it together?"
  jo sarcastic "Very well. I'll be there."
  hide jo with dissolve2
  "Okay, that went a lot better than expected!"
  "Now for the waiting game..."
  $quest.jo_day.advance("picnic")
  return

label quest_jo_day_picnic:
  call screen time_passed
  if game.location == "school_forest_glade":
    pause 0.5
  else:
    "It's almost sunset. I promised to meet [jo] in the forest glade."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    call goto_school_forest_glade
    pause 1.5
    hide black onlayer screens with Dissolve(.5)
  show jo blush at appear_from_left
  window auto
  jo blush "There's my handsome boy!"
  jo blush "And look at all of this! A wonderful view of the greenery right before dark."
  jo sarcastic "Oh, and what a lovely little cactus. That's... different."
  jo sarcastic "I'm sure I have a place for it at home. Maybe in a corner somewhere.{space=-5}"
  jo sarcastic "What did you pack for us to eat?"
  mc "Wine..."
  jo concerned "Wine? Just wine?"
  mc "Well, I'm not much of a chef, so I didn't want to poison us."
  mc "But I did get your favorite!"
  jo smile "Goodness, aren't you thoughtful?"
  mc "Sometimes I forget to try, but I just want you to know how much I appreciate you..."
  jo worried "All right, who are you and what have you done with [mc]?"
  mc "Heh, it's true!"
  jo thinking "I don't know, I really shouldn't be drinking on school grounds."
  jo thinking "But..."
  jo flirty "As long as all the others have gone home, and it's just you and me...{space=-10}"
  jo flirty "I suppose I can bend the rules just this once for my thoughtful, special boy."
  window hide
  show black onlayer screens zorder 100
  show jo picnic_smile_holding_wine
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "I cleaned my room. Everything's sparkling, so you don't have to lift a finger."
  jo picnic_smile_holding_wine "Wow! Color me impressed. I can't remember the last time you did that.{space=-50}"
  mc "Probably never. I've always taken everything for granted."
  mc "But not anymore. I'll try to do better. I want to turn things around."
  "She doesn't know that I've already seen the future that awaits me\nif I don't..."
  jo picnic_smile_holding_wine "I'm glad you're taking up more responsibility. It's an important part of your life as an adult."
  jo picnic_blush_smile_holding_wine "You've grown up so fast. Faster than I ever wanted."
  jo picnic_blush_smile_holding_wine "I'm proud to see the man you've become."
# jo picnic_blush_smile_holding_wine "Your father would be proud to see the man you've become." ## Incest patch ##
  "[jo] is never proud of me. I guess I really must've impressed her\nthis time."
# "[jo] never talks about Dad. I guess I really must've impressed her\nthis time." ## Incest patch ##
  jo picnic_flirty_holding_wine "Now that you're a grown-up, maybe it's time to start treating you like one..."
  mc "I'd like that. How's the wine?"
  jo picnic_blush_flirty_holding_wine "It's delicious. It goes down so... smoothly, too."
  jo picnic_blush_flirty_holding_wine "Mmm, so delectable..."
  jo picnic_blush_flirty_offering_wine "Did you try some?"
  mc "I'm okay, thanks. It's all yours, you deserve it."
  mc "You work so hard, and do so much for me and [flora]."
  jo picnic_smile_holding_wine"You really are something today, [mc]! I'm not sure what's gotten into you."
  jo picnic_smile_holding_wine "Not that I'm complaining!"
  mc "Just trying to be better..."
  window hide
  show jo picnic_surprised_spilling_wine with Dissolve(.5)
  window auto
  jo picnic_surprised_spilling_wine "Oop!"
  jo picnic_surprised_looking_down_spilling_wine "Oh, no, I've spilled some on myself! Clumsy me..."
  jo picnic_blush_smile_spilling_wine "I suppose I should take this thing off, keep it from staining too badly.{space=-20}"
  window hide
  show jo picnic_blush_smile_unbuttoning_shirt with Dissolve(.5)
  pause 0.25
  show jo picnic_blush_smile_removing_shirt with Dissolve(.5)
  window auto
  jo picnic_blush_smile_removing_shirt "There, that's better already."
  jo picnic_blush_smile_removing_shirt "Don't you think?"
  mc "Much better..."
  jo picnic_blush_flirty_removing_shirt "I should probably slow down!"
  "God, please don't slow down..."
  "Wait, what am I thinking? It doesn't matter, I can't look away..."
  window hide
  show jo picnic_blush_smile_holding_wine_shirtless with Dissolve(.5)
  window auto
  jo picnic_blush_smile_holding_wine_shirtless "Oooh, this wine has me feeling woozy..."
  jo picnic_blush_smile_holding_wine_shirtless "It was so good that I drank it a little too quickly."
  jo picnic_blush_smile_holding_wine_shirtless "Just a little more won't hurt, though..."
  mc "It's okay to have some more. I'm here for you."
  jo picnic_flirty_holding_wine_shirtless "You're going to take responsibility, right?"
  mc "Of course!"
  jo picnic_blush_flirty_holding_wine_shirtless "Such a mature, grown man you're becoming..."
  jo picnic_blush_flirty_holding_wine_shirtless "I'm so proud of you... taking care of me like this..."
  jo picnic_blush_flirty_holding_wine_shirtless "How did I get soo lucky?"
  "Damn, she might actually be drunk now."
  window hide
  show jo picnic_flirty_pouring_wine with Dissolve(.5)
  window auto
  jo picnic_flirty_pouring_wine "Oh!"
  jo picnic_blush_flirty_pouring_wine "Would you look at that...? I've gone and spilled some more all over my bra..."
  jo picnic_blush_flirty_pouring_wine "What a clumsy thing I am today."
  jo picnic_blush_flirty_pouring_wine "Thank goodness I have you here, looking after me..."
  window hide
  show jo picnic_blush_flirty_removing_bra with Dissolve(.5)
  window auto
  jo picnic_blush_flirty_removing_bra "I should probably take this off though, don't you think?"
  menu(side="middle"):
    extend ""
    "\"Err, yes... just in case!\"":
      mc "Err, yes... just in case!"
      $quest.jo_day["just_in_case"] = True
    # "\"Actually, [jo], I don't think you would feel good about it later.\"":
    "\"Actually, [jo], I don't think you\nwould feel good about it later.\"":
      mc "Actually, [jo], I don't think you would feel good about it later."
      jo picnic_surprised_removing_bra "Ah, yes. You're right. That would be inappropriate."
      mc "It's okay. You'll survive with a stain on it for just a bit."
      window hide
      show jo picnic_neutral_looking_down_resting_hands_bra_on with Dissolve(.5)
      window auto
      jo picnic_neutral_looking_down_resting_hands_bra_on "Sorry, I've really had too much to drink..."
      jo picnic_blush_smile_resting_hands_bra_on "I was just so happy that you put this thing together for me."
      jo picnic_blush_smile_resting_hands_bra_on "You're such a sweet... sweet boy..."
      "Despite her drunken state, it does feel good hearing [jo] say these things."
      mc "I'm trying to do better this time. To be a better person."
      jo picnic_blush_smile_resting_hands_bra_on "This time?"
      show teary_eyes1 behind teary_eyes2:
        alpha 0.0 yalign 1.0
        pause 0.4
        easein 0.5 alpha 1.0
      show teary_eyes2 with {"master": close_eyes}:
        ysize 1080
      mc "Yeah... it's really rare to get second chances like this..."
      jo picnic_surprised_resting_hands_bra_on "My darling boy, are you crying?"
      "Damn. I actually am."
      mc "Sorry... it just brings me so much joy seeing you happy..."
      mc "When [flora] left... and I didn't do anything with my life..."
      mc "I think it really crushed you..."
      jo picnic_surprised_resting_hands_bra_on "When [flora] left? You have me all sorts of confused, [mc]."
      jo picnic_surprised_resting_hands_bra_on "Maybe it's the wine..."
      "[jo] turned into a gray shell of her former self, only ever lighting up during the holidays when [flora] visited."
      "And it was all my fault for being such a lazy and selfish piece of shit."
      "No one is perfect, but I still feel like it was my fault that she became depressed."
      show teary_eyes1:
        easeout 0.5 alpha 0.0
      hide teary_eyes2 with {"master": open_eyes}
      mc "Err, yes. It must be the wine."
      mc "I'm really glad we did this, [jo]."
      jo picnic_smile_resting_hands_bra_on "Aww! Me too, me too."
      jo picnic_smile_resting_hands_bra_on "The school year has barely started and I already felt drained before this..."
      mc "I'll try to make things easier for you this year, I promise."
      mc "I know I've been difficult in the past."
      jo picnic_flirty_resting_hands_bra_on "[mc]!"
      jo picnic_flirty_resting_hands_bra_on "Such a big heart on my big man..."
      mc "Anyway, I think we should head home now. It's already getting dark."
      jo picnic_smile_resting_hands_bra_on "Oh? I guess you're right."
      jo picnic_blush_smile_resting_hands_bra_on "There's only one problem... the ground is a bit unstable... or maybe that's just me..."
      mc "Don't worry. I've got you."
      window hide
      show black onlayer screens zorder 100
      show jo sunset_walk
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      "There is a certain feeling that you can only get by being selfless."
      # "It starts as a lightness in your chest, and then spreads to your shoulder and face, relaxing your posture and tickling your lips into a smile."
      "It starts as a lightness in your chest, and then spreads to your shoulder{space=-75}\nand face, relaxing your posture and tickling your lips into a smile."
      "This feeling is almost like a drug that lifts your spirits without any of the bad side effects."
      "It sparks a hope inside you that maybe, just maybe, you're really a bad person."
      "There was some goodness all along, and all you had to do was act on it."
      "And you wonder why you didn't act on it sooner."
      "I've never believed in karma, but maybe I just didn't understand what it was until now?"
      "It's not some cosmic force of justice."
      "It's just you feeling better about yourself, and in turn the world around you feels a little more positive, and you pay more attention to when good things happen to you."
      "..."
      "Or maybe I'm just high on positivity right now, and I'll crash and burn before I know it..."
      "But at least [jo] is happy and proud of me, and maybe that's enough."
      jump quest_jo_day_picnic_aftermath
  jo picnic_flirty_removing_bra "Mmm, exactly... just in case..."
  window hide
  show jo picnic_blush_flirty_squeezing_boobs with Dissolve(.5)
  window auto
  jo picnic_blush_flirty_squeezing_boobs "God, that's muuuch better. So... freeing."
  "Wow! Her breasts are right there, ready for the taking!"
  "..."
  "Why am I looking at [jo] this way? Maybe I'm buzzed, too."
  mc "I don't know if it's the way the sun's hitting you this late in the day, but you look astonishing."
  jo picnic_blush_smile_resting_hands "Aww! You're making me blush!"
  mc "I can't imagine how anyone could be mean to you."
  mc "You're just so beautiful. How could they?"
  mc "I bet the person who wrote that email feels terrible about it now."
  jo picnic_blush_smile_resting_hands "Oh, I doubt it... but I don't want to think about that right now..."
  jo picnic_blush_smile_resting_hands "I just want to enjoy this relaxing moment."
  mc "I know, but I'm kind of glad that she got you so fired up. If she hadn't, we wouldn't be enjoying this moment right now."
  jo picnic_flirty_resting_hands "It does feel good to just slow down and enjoy this time with you..."
  mc "It does. And you worked so hard to get to where you are now."
  mc "I hope that all of your ambition rubbed off on me."
  mc "[flora] and I really don't know how lucky we are to have someone like you taking care of us."
  jo picnic_blush_flirty_resting_hands "Sweetie, you're being too generous. An old lady like me?"
  mc "You're so vibrant and full of life. You're hardly old."
  mc "I mean, just look at your breasts! They're so perky compared to other women your age."
  jo picnic_surprised_squeezing_boobs "[mc]... that's... that's so inappropriate!"
  mc "But it's true!"
  jo picnic_surprised_squeezing_boobs "That's not the point! You shouldn't even have eyes wandering down there!"
  mc "How could I not? They're huge. When they knock against each other,{space=-10}\nI can't help it..."
  mc "Besides, if you didn't want me to look, you wouldn't have them out."
  mc "Yet here they are. I'm staring right at them, and you're not stopping me.{space=-70}"
  jo picnic_neutral_looking_down_squeezing_boobs "It's just that they've been constricted all day... and then I spilled that wine on, on accident..."
  mc "On accident, of course."
  jo picnic_blush_smile_squeezing_boobs "Yesss, of course. Accidents happen and that's... that's okay."
  mc "I know one thing..."
  mc "...if there is a God, the way he created you definitely wasn't an accident."
  jo picnic_smile_resting_hands "Well, isn't that a thing to say!"
  jo picnic_smile_resting_hands "How very sweet of you, [mc]."
  jo picnic_smile_resting_hands "I think I raised you right, after all."
  mc "You certainly did."
  jo picnic_blush_smile_resting_hands "Goodness, but that wine is so strong..."
  jo picnic_blush_smile_resting_hands "And on an empty stomach. It's really gotten to my head, I think."
  jo picnic_blush_smile_resting_hands "How irresponsible!"
  mc "It's okay to just let loose sometimes."
  jo picnic_blush_flirty_resting_hands "So loose..."
  mc "Remember, you deserve it."
  mc "You should lie back and relax. Let the evening breeze wash over your skin."
  jo picnic_blush_flirty_resting_hands "Oh, but that does sound so wonderful..."
  jo picnic_blush_flirty_resting_hands "Maybe just for a moment..."
  window hide
  show black onlayer screens zorder 100
  show jo boobjob_smile_lying_down
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  jo boobjob_smile_lying_down "Mmmm... that does feel good..."
  jo boobjob_smile_lying_down "That wine has really rushed to my head... makes it hard to th-think too clearly."
  "She keeps saying that, but I can see how much she's enjoying this."
  mc "Don't think too much, just keep relaxing."
  jo boobjob_flirty_lifting_boob "Ohhh... that feels good, and really helps me relax. Sometimes at night, all on my own, well..."
  mc "Yeah?"
  jo boobjob_laughing_lifting_boob "Oh! Never mind, you naughty boy."
  mc "I could help relax you some more, you know."
  jo boobjob_flirty_lifting_boob "Mmmm, yeah? Please..."
  window hide
  show jo boobjob_surprised_looking_down_dick_reveal with vpunch
  window auto
  jo boobjob_surprised_looking_down_dick_reveal "Ohh!"
  jo boobjob_flirty_looking_down_grabbing_dick "It's been so long since someone showed me a cock like yours..."
  jo boobjob_flirty_looking_down_grabbing_dick "..."
  jo boobjob_concerned_grabbing_dick "Wait, did I just say that out loud?"
  mc "What are you gonna do about it?"
  jo boobjob_smile_grabbing_dick "Well... there was one thing my husband loved to do..."
# jo boobjob_smile_grabbing_dick "Well... there was one thing your father loved to do..." ## Incest patch ##
  jo boobjob_flirty_looking_down_grabbing_dick "He liked to stick it right between..."
  jo boobjob_flirty_looking_down_dick_top "Yes... just like that..."
  window hide
  #1
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #2
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #3
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #4
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  #5
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.2)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.2)
  window auto
  jo boobjob_flirty_looking_down_dick_top "Oh, my! What a big man you've grown into..."
  jo boobjob_flirty_looking_down_dick_top "And I do mean big!"
  window hide
  #1
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #2
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #3
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #4
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #5
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #6
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  #7
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.175)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.175)
  window auto
  jo boobjob_flirty_looking_down_dick_top "Shove it right between them and really make them bounce!"
  jo boobjob_flirty_looking_down_dick_top "Up and down! Up and down!"
  window hide
  #1
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #2
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #3
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #4
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  #5
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_bottom with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_middle with Dissolve(.15)
  show jo boobjob_flirty_looking_down_dick_top with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.5)
  window auto
  jo boobjob_drunk_dick_top "Keep going! You're doing great!"
  jo boobjob_drunk_dick_top "Don't you dare stop!"
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #4
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #5
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  #6
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_bottom with Dissolve(.15)
  show jo boobjob_drunk_dick_middle with Dissolve(.15)
  show jo boobjob_drunk_dick_top with Dissolve(.15)
  window auto
  "Her boobs feel so perfect, like they were made just for me..."
  "There's no way I could stop now, even if I wanted to!"
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #4
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #5
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #6
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  #7
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_bottom with Dissolve(.125)
  show jo boobjob_drunk_dick_middle with Dissolve(.125)
  show jo boobjob_drunk_dick_top with Dissolve(.125)
  window auto
  jo boobjob_drunk_dick_top "Come on, sweetheart! You can do it!"
  jo boobjob_drunk_dick_top "Show me how it's done!"
# jo boobjob_drunk_dick_top "Show momma how it's done!" ## Incest patch ##
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #4
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #5
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #6
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #7
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #8
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  window auto
  "Fuck! She's squeezing them against me with everything she's got!"
  "I can't believe I'm gonna blow my load right on [jo]'s face!"
  window hide
  #1
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #2
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  #3
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_bottom with Dissolve(.1)
  show jo boobjob_drunk_dick_middle with Dissolve(.1)
  show jo boobjob_drunk_dick_top with Dissolve(.1)
  show jo boobjob_drunk_cum with vpunch
  pause 0.25
  show jo boobjob_flirty_aftermath with Dissolve(.5)
  window auto
  jo boobjob_flirty_aftermath "Mmmm, yes! Just like that!"
  mc "Goddamn... you really drained it all out of me..."
  jo boobjob_smile_aftermath "That's what a woman with experience is supposed to do, honey."
  label quest_jo_day_picnic_aftermath:
    window hide
    show black onlayer screens zorder 100 with Dissolve(3.0)
    hide jo
    while game.hour != 7:
      $game.advance()
    $game.location = "home_kitchen"
    pause 1.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
    if "event_show_time_passed_screen" in game.events_queue:
      $game.events_queue.remove("event_show_time_passed_screen")
    call screen time_passed
    pause 0.5
    show jo neutral with Dissolve(.5)
    window auto
    mc "Hey, [jo]! what did you think about your belated Mother's Day?"
    jo concerned "Um, well.... my memory is actually a little foggy."
    jo confident "I remember you brought a bottle of wine to our picnic, naughty boy."
    # jo neutral "I must've drank too much, because I don't remember anything after that."
    jo neutral "I must've drank too much, because I don't remember anything after that.{space=-90}"
    if quest.jo_day["just_in_case"]:
      jo skeptical "And for some reason, my chest feels a little swollen..."
      mc "Uh, I can't think of why that would be."
      "Now that she mentioned it, her boobs do look a little bit bigger."
      "Does she really not remember the night we had?"
      jo annoyed_crossed_arms "Eyes up here, young man."
      mc "What? You said they were swollen! I was just making sure."
      jo annoyed_crossed_arms "Uh-huh."
      "Last night, I had to wipe off her face and take her home after we had our fun."
      # "She kept trying to make out with me and called me by Dad's name..." ## Incest patch ##
      # "She kept trying to make out with me and called me by her husband's name..."
      "She kept trying to make out with me and called me by her husband's{space=-15}\nname..."
      "Maybe she thought she was reliving her glory days?"
      # "Hopefully, [flora] didn't notice us busting through the door after dark."
      "Hopefully, [flora] didn't notice us busting through the door after dark.{space=-15}"
      jo worried "Did we get back home before your curfew?"
    else:
      jo skeptical "Did we get back home before your curfew?"
    # mc "Do I look like the kind of guy that would break curfew on Mother's Day?"
    mc "Do I look like the kind of guy that would break curfew on Mother's Day?{space=-70}"
    if quest.jo_day["just_in_case"]:
      "I do look like that guy, but way worse."
      jo thinking "Good. I hope I wasn't too much trouble."
    else:
      jo smile "Good. I hope I wasn't too much trouble."
    mc "You were just the right amount of trouble for me to handle."
    jo blush "Oh, you..."
    mc "I'll handle all the trouble you have for me until you're old and gray."
    jo sarcastic "Hey! You better watch it, mister."
    mc "Love you, [jo]!"
    jo flirty "Mhm... I love you too, sweetie."
    window hide
    hide jo with Dissolve(.5)
    $unlock_replay("jo_picnic")
    $quest.jo_day.finish()
  return
