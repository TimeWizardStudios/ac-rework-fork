init python:
  quest_jo_washed_interact_lines = [
  "quest_jo_washed_interact_line_one",
  "quest_jo_washed_interact_line_two",
  "quest_jo_washed_interact_line_three"
  ]

  quest_jo_washed_investigate_lines = [
  "{#daydream}It feels like yesterday.",
  "{#daydream}Sometimes it's hard to remember what was and what wasn't.",
  "{#daydream}We're just meat, burned by the frying pan of the past."
  ]


label quest_jo_washed_not_started_bedroom:
  scene black
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  python:
    game.location = "home_bedroom"
    while game.hour != 23:
      game.advance()
    if "event_show_time_passed_screen" in game.events_queue:
      game.events_queue.remove("event_show_time_passed_screen")
    for quest_title,quest_id in get_available_quest_guides():
      if quest_id not in ("","act_one"):
        game.quests[quest_id].finish(silent=True)
        game.timed_out_quests.add(quest_id)
    if game.quest_guide in ("kate_stepping","isabelle_dethroning"):
      game.quest_guide = ""
    home_bedroom["night"] = True
  pause 1.5
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide misc ambulance onlayer screens
  hide black onlayer screens
  hide to_be_continued onlayer screens
  with Dissolve(.5)
  call screen time_passed
  pause 0.5

label quest_jo_washed_start:
  "Sometimes when life gets rough, it's easier to just escape."
  "Video games, TV shows, and masturbation used to be my go-to choices, but also daydreams."
  "Back in my old life of pizza deliveries and existential dread, I used to zone out and think back to the good times."
  "There weren't many, of course, as most of my school years were a trainwreck and distilled misery..."
  "...but a few golden nuggets here and there could occasionally keep my mind occupied for a while."
  "One of my favorite daydreams started something like this..."
  window hide
  play sound "dream"
  show white onlayer screens zorder 100
  show misc flashback zorder 100
  with Dissolve(.5)
  python:
    a = game.day
    b = game.hour
    c = game.location
    d = mc.money
    e = mc.inv
    game.day = 1
    game.hour = 11
    game.location = "school_ground_floor"
    mc.money = 0
    mc.inv = []
    quest.jo_washed.start(silent=True)
  pause 0.5
  hide white onlayer screens with Dissolve(.5)
  window auto
  "It was a warm summer day when I entered the school."
  "Sweltering heat pressed in through the windows, making the air itself a choking hazard."
  "As usual, this didn't bother [lindsey], who, unlike me, was going somewhere, fast."
  window hide
  play sound "<from 0.4>poof"
  show expression LiveComposite((56,213),(0,0),"school ground_floor lindsey",(0,0),AlphaMask(Crop((568,573,56,213),"misc flashback"),"school ground_floor lindsey")):
    alpha 0.0 xanchor 0.5 xpos 596 yanchor 0.5 ypos 680 zoom 0.0
    easein_bounce 0.25 alpha 1.0 zoom 1.0
  pause 0.5
  $quest.jo_washed["lindsey_location"] = "school_ground_floor"
  window auto
  "And me, I was just minding my own business when..."
  window hide
  $flora["outfit_stamp"] = flora.outfit
  $flora.outfit = {"bra": "flora_bra", "panties": "flora_panties", "pants": "flora_pants", "shirt": "flora_shirt"}
  show flora worried at appear_from_left(.25)
  show jo concerned at appear_from_right(.75)
  pause 0.5
  window auto
  jo concerned "I know you have your internship."
  flora worried "And studies."
  jo concerned "And your studies, of course."
  flora flirty "And admiring [mc]'s big muscles."
  jo confident "I know, I know. They are very big and admirable."
  jo confident "And don't get me started on his dick."
  flora flirty "Yummy!"
  "Okay, they probably didn't say those things."
  "But hey, sometimes you have to embellish a bit to feel good about yourself."
  jo skeptical "I know you have a lot of things on your plate, but I really need\nyour help."
  flora blush "All right, fine!"
  jo smile "Thank you, sweetie!"
  window hide
  show flora blush at disappear_to_right
  show jo smile at move_to(.5,1.0)
  pause 0.5
  $quest.jo_washed["flora_location"] = "school_entrance"
  window auto
  "And then [jo] turned and noticed me, grabbed her left breast, moaned, and gave me a seductive look."
  jo smile "[mc]! Perfect timing!"
  "Ah, well. Almost."
  mc "Perfect timing for what? Did the school board cut the funding for the school dance?"
  jo eyeroll "No, the school board cut—"
  jo afraid "Yes! How did you know that?"
  mc "Just a good guess."
  "Of course, the dance will involve [flora] in a skimpy dress, [jo] showing{space=-20}\ntoo much cleavage..."
  "...two cheerleaders kissing, [lindsey] getting wasted and inappropriate,{space=-40}\nand an all-you-can-eat doughnut buffet."
  mc "This dance needs to happen."
  jo afraid "Unfortunately, it might not. It's pretty expensive to host one."
  mc "How about a car wash fundraiser?"
  jo sarcastic "That's... a great idea!"
  jo sarcastic "I was struggling to come up with ways to earn money, but you're all sorts of crafty and intelligent!"
  "It feels good to have all the answers. I don't even remember if it was my idea back then, but I'll gladly take credit now."
  jo flirty "Not to mention handsome! My handsome boy!"
  "Okay, it definitely wasn't my idea. Probably [flora]'s."
  "And then something else happened, I don't really remember what... but the car wash got pushed forward."
  "[jo] got all sorts of nervous, because..."
  "...because..."
  "...oh, right! People were already queuing to get their glimpse of half-naked girls covered in soap."
  window hide
  play sound "honking1" volume 0.33
  play ambient "audio/sound/honking2.ogg" noloop
  pause 1.0
  window auto show
  show jo embarrassed with dissolve2
  jo embarrassed "Oh, my god, [mc]! They're already here!"
  jo embarrassed "I've been in a meeting all morning, and didn't have time to prepare!"
  jo embarrassed "The car wash is supposed to start already, but we still haven't gotten all the things we need..."
  mc "Where's [flora] off to?"
  jo embarrassed "She's pulling out the buckets and towels. Oh, we really are in a hurry now!"
  mc "I guess I could lend a hand..."
  "If this was for anything other than seeing girls in bikinis, I definitely wouldn't have agreed to help."
  "I used to be such an ass."
  jo thinking "Really?"
  "The look of surprise on her face that day haunted me for a long time.{space=-25}"
  "As if it were the strangest thing in the world that I offered to help."
  jo laughing "Thank god! You're a lifesaver!"
  mc "Just tell me what you need, and I'll make it happen."
  jo excited "All right! We need a hose to spray down the cars. You can borrow the one from the greenhouse."
  mc "Okay, that should be—"
  jo excited "[kate] has been working on the signs already. Just get them from her.{space=-5}"
  mc "Uh, I don't—"
  jo excited "The janitor should have some soap in her closet."
  mc "But doesn't she—"
  jo excited "Finally, we need sponges to scrub the cars. Sorry, hon, I'm not sure where those might be."
  "Right, I remember this part. Having to run around the school like Bateman on crack."
  jo laughing "I have to go now, but I'll be back to help out later on."
  jo laughing "If you need a reminder, [flora] will be right next to the bus stop."
  jo laughing "This is important, okay?"
  mc "Hell yeah, it is."
  jo laughing "I believe in you, honey! Good luck!"
  window hide
  show jo laughing at disappear_to_left
  pause 0.5
  window auto
  "Gone like the overworked wind she is..."
  "..."
  "The cars are already here, so I should probably get started."
  "Bikinis will be worn today, they will get wet, and the dance will be saved!"
  "At least, if I remember what to look for..."
  window hide
  $quest.jo_washed.started = False
  $quest.jo_washed.start()
  return

label quest_jo_washed_interact_line_one:
  "Some things never change unless you get another chance."
  return

label quest_jo_washed_interact_line_two:
  "Is the past even real, or is it just a collection of memories?"
  return

label quest_jo_washed_interact_line_three:
  "My life felt so worthless back then. I really am one of the luckiest sons of bitches."
  return

label quest_jo_washed_supplies_reminder:
  show flora afraid with Dissolve(.5)
  "Oh, yeah. And then I went and talked to [flora] in the middle of all the honking."
  "She didn't seem pleased at all. A bit like that time when I put my dick in her birthday cake."
  "Ah, good times."
  flora afraid "[mc]? What the hell?!"
  mc "What?"
  flora embarrassed "Can't you see the cars?! Why aren't you helping?!"
  flora embarrassed "[jo] texted me that you would help! Where are all the things?!"
  mc "I haven't found them yet..."
  flora embarrassed "Well, hurry up and get them! They're getting impatient!"
  mc "So, about that..."
  flora skeptical "..."
  flora skeptical "You forgot what she asked for, didn't you?"
  mc "Maybe..."
  flora annoyed "Ugh, you're hopeless!"
  flora sarcastic "But I still love you. Like, a lot. Especially your dick."
  "Ah, if only..."
  "The real [flora] never compliments my dick. Very rude of her."
  mc "Thank you, [flora]. I love you too."
  flora annoyed "Listen closely this time, okay?"
  flora annoyed "We need the garden hose from the greenhouse, the signs [kate] has been working on, the soap from the janitor's closet, and sponges."
  flora annoyed "Come back here as soon as you get everything."
  "She used to be so bossy back then. Well, I guess she still is."
  "..."
  "It was really hard to focus on anything while thinking about her\nin a bikini..."
  "Mmm... wet and soapy, rubbing all over the cars..."
  mc "Wait, could you repeat that?"
  flora eyeroll "[mc]..."
  mc "I'm kidding! I'm definitely kidding..."
  mc "I'll be back before you can say \"hose me down!\""
  flora concerned "Hose me down."
  "She actually did say that. One of my all time favorite lines of hers. Very hot."
  mc "All right, but you'll have to wait until I get back!"
  flora angry "Ugh!"
  window hide
  hide flora with Dissolve(.5)
  return

label quest_jo_washed_supplies_entrance_hall:
  "And then I went to the entrance hall."
  "Everything was quiet except for the muted honking of cars."
  return

label quest_jo_washed_supplies_homeroom:
  "And then I went to the homeroom."
  "And I remember thinking, \"If only [mrsl] would get out of her cardigan and help with the car wash...\""
  return

label quest_jo_washed_supplies_first_hall:
  "And then I went and looked out the window for a moment, imagining{space=-10}\nwhat freedom would feel like."
  return

label quest_jo_washed_supplies_roof:
  "Then, finally, I made it to the roof."
  "A relaxing breeze tugged playfully at my hair, and soothed the scorching heat."
  "Oh, and on the roof was [maxine], of course. Nearly forgot that part."
  window hide
  play sound "<from 0.4>poof"
  show expression LiveComposite((71,363),(0,0),"school roof maxine",(0,0),AlphaMask(Crop((1505,482,85,429),"misc flashback"),"school roof maxine")) as maxine_dream onlayer screens:
    alpha 0.0 xanchor 0.5 xpos 1541 yanchor 0.5 ypos 664 zoom 0.0
    easein_bounce 0.25 alpha 1.0 zoom 1.0
    pause 0.5 alpha 0.0
  show screen location
  pause 0.25
  $quest.jo_washed["maxine_location"] = "school_roof"
  return

label quest_jo_washed_supplies_garden_hose:
  hide maxine_dream onlayer screens
  show maxine thinking
  with Dissolve(.5)
  maxine thinking "Have you also come to water the plants?"
  "She was a bit less weird back then, but still weird."
  "I didn't interact with her much, so I can't say for sure."
  mc "Uh, hello to you, too."
  maxine thinking "No one comes up here at this hour unless it's important."
  mc "Well, it sort of is."
  mc "I need the garden hose for the car wash."
  maxine sad "No. I'm using it."
  "Here we go again..."
  mc "I know. You're going to get rid of the plant zombies."
  maxine excited "Aha! And [lindsey] said they weren't real!"
  maxine flirty "I must've misjudged you, [mc]."
  maxine flirty "Perhaps you'll be the one to vanquish this threat."
  mc "All I'm interested in vanquishing is a future without a bikini car wash.{space=-15}"
  maxine concerned "So, you journeyed here for nothing?"
  mc "Like I said, I need the hose."
  maxine annoyed "But there's an incursion of zombie plants!"
  mc "Are you talking about the plants in the greenhouse?"
  maxine skeptical "Indeed."
  mc "I'm pretty sure they're harmless."
  maxine eyeroll "Quite the opposite, actually."
  maxine eyeroll "Danger is oftentimes masked under a veil of harmlessness."
  mc "And the zombies have masked themselves under a veil of... broccoli and cabbage?"
  maxine skeptical "Affirmative. The veil is lifting, and what will you do then?"
  mc "If they still look like vegetables, I'll probably just eat them."
  maxine angry "Fool! You'd sully yourself with the blight of undeath!"
  mc "Well, I could always drench them in ranch dressing."
  maxine angry "They lure you here, only for you to make jokes?"
  maxine angry "It would be simple to subdue them, but we must use the hose now."
  mc "Actually, I need to get it down to the fundraiser right away."
  mc "Can't you just use water bottles or something to drown the zombies?{space=-30}"
  mc "Maybe wait for it to rain?"
  maxine neutral "Do you happen to have thousands of water bottles on you?"
  mc "No..."
  maxine neutral "Then, we require the garden hose if we are to stop this blight from spreading."
# mc "I guess the carrots would be up next."
  show maxine neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"The blight can wait. I need the hose.\"":
      show maxine neutral at move_to(.5)
      mc "The blight can wait. I need the hose."
      maxine thinking "This mistake will be costly."
      mc "Yeah, well, the fundraiser falling through would be even more costly.{space=-10}"
      mc "I'm sure you'll figure something out."
      "That's not exactly how it went down... but I'd rather fast forward to the good parts."
    "\"Fine. Let's water the demons.\"":
      show maxine neutral at move_to(.5)
      mc "Fine. Let's water the demons."
      maxine confident "Let's do it!"
      $school_roof["garden_hose_taken"] = True
      maxine confident_hose "Just hold the door open while I rain down upon them, okay?"
      "Things I didn't expect to hear from [maxine] back then..."
      maxine confident_hose "Ready?"
      mc "As I'll ever be."
      maxine confident_hose "Open the door!"
      mc "Go! Go! Go!"
      window hide
      show maxine confident_hose at disappear_to_right
      pause 1.0
      play ambient "audio/sound/watering1.ogg" noloop volume 0.66
      pause 0.25
      play sound "shrieking1"
      pause 0.5
      window auto
      "She totally made that noise herself, didn't she?"
      "I always thought she did, at least... but maybe not."
      maxine "It's proving effective!"
      mc "Watch out! Behind you!"
      "There was nothing there, of course..."
      "...well, I couldn't really see as she was blocking the entrance."
      "Perhaps the greenhouse needs to be investigated in this timeline, too.{space=-40}"
      window hide
      play ambient "audio/sound/watering2.ogg" noloop volume 0.66
      pause 0.5
      play sound "shrieking2"
      pause 2.5
      window auto
      maxine "Target eliminated."
      show maxine confident_hose at appear_from_right
      mc "Nice work, soldier!"
      mc "Are you sure they won't come back?"
      maxine confident_hose "I'll continue to monitor their growth, but they do seem to be subdued{space=-25}\nfor now."
      mc "You really showed them who's boss."
      "Back then, most interactions with girls were a pain, but this one felt good."
      "Perhaps pretending to play a squirt 'em up game with [maxine] helped{space=-10}\nget me out of my shell."
      "It's a fond memory, either way."
      maxine confident_hose "Your aid was invaluable, [mc]."
      mc "Hopefully, the history books will be kind to me."
      maxine annoyed_hose "No one is going to write about this."
      mc "No one?"
      maxine confident_hose "Well, I just might add your name to the report."
      mc "Thanks! Let it be known that I helped stop a zombie invasion!"
      mc "That should count for some good karma, right?"
      maxine confident_hose "Every good deed counts."
      $quest.jo_washed["car_wash_volunteers"].add("maxine")
  window hide
  hide maxine with dissolve2
  $school_roof["garden_hose_taken"] = True
  $mc.add_item("garden_hose")
  if sum([mc.owned_item("garden_hose"),mc.owned_item("car_wash_sign"),mc.owned_item("bottle_of_soap"),mc.owned_item("sponges")]) == 4:
    window auto
    "So, when I finally collected all the items, the plan was to head outside."
    $quest.jo_washed.advance("car_wash")
  return

label quest_jo_washed_supplies_gym:
  "The gym was quite the experience, too. I'm still not sure how I feel about it."
  "I remember how relieved I was that the cheerleaders weren't around when I stepped into the gym."
  "Only [kate] working on the signs."
  window hide
  play sound "<from 0.4>poof"
  show expression LiveComposite((492,180),(0,0),"school gym car_wash_signs1",(0,0),AlphaMask(Crop((524,814,492,180),"misc flashback"),"school gym car_wash_signs1"),(0,0),AlphaMask(Crop((524,814,492,180),"school gym overlay"),"school gym car_wash_signs1")) as kate_dream onlayer screens:
    alpha 0.0 xanchor 0.5 xpos 770 yanchor 0.5 ypos 904 zoom 0.0
    easein_bounce 0.25 alpha 1.0 zoom 1.0
    pause 0.5 alpha 0.0
  show expression LiveComposite((164,457),(0,0),"school gym kate_car_wash_signs",(0,0),AlphaMask(Crop((536,488,164,457),"misc flashback"),"school gym kate_car_wash_signs"),(0,0),AlphaMask(Crop((536,488,164,457),"school gym overlay"),"school gym kate_car_wash_signs")) as signs_dream onlayer screens:
    alpha 0.0 xanchor 0.5 xpos 618 yanchor 0.5 ypos 717 zoom 0.0
    easein_bounce 0.25 alpha 1.0 zoom 1.0
    pause 0.5 alpha 0.0
  show screen location
  pause 0.25
  $quest.jo_washed["kate_location"] = "school_gym"
  return

label quest_jo_washed_supplies_car_wash_signs:
  hide kate_dream onlayer screens
  hide signs_dream onlayer screens
  show kate skeptical
  with Dissolve(.5)
  kate skeptical "..."
  kate skeptical "Weird. I thought this was a bum-free zone."
  mc "Ehh... you've had better."
  kate excited "I guess my attention is on other things. More important things."
  mc "Like the fundraiser?"
  kate cringe "Gross. Don't tell me you're going to be there."
  mc "I'm afraid so."
  mc "[jo] asked me to gather everything we need to get it started."
  kate excited "Well, aren't you a good little delivery boy?"
  kate excited "I bet [jo] asked you to get these signs too, right?"
  mc "Man, how have I never realized how smart you are, [kate]?"
  kate skeptical "Are you giving me attitude? Interesting strategy."
  "She wasn't wrong about that. Things might've ended differently if I hadn't provoked her."
  kate skeptical "I guess you don't really need the signs..."
  mc "No, I do."
  mc "[jo] is counting on me."
  kate excited "Oh, is she now?"
  kate excited "See, that's what I thought."
  kate excited "And I bet you'd do just about anything to get them from me."
  "And she was right, of course. I really did need them."
  "Probably would've been grounded for weeks if I didn't."
  "And my next words probably didn't help appeasing [kate] either."
  "I was really dumb back then. Trying to act tougher than I was."
  mc "Just hand them over!"
  mc "The faster you do, the sooner we can go our separate ways!"
  kate neutral "Not good enough."
  kate neutral "You don't really sound as desperate as I know you are."
  kate confident "Try again."
  mc "..."
  mc "[kate]..."
  kate confident "Yes, [mc]?"
  mc "I {i}really{/} need those signs."
  mc "Would you {i}please{/} hand them over, so I can get them to the fundraiser?{space=-60}"
  kate neutral "..."
  mc "...with a cherry on top?"
  kate neutral "Still not good enough."
  mc "Look, I don't have time for—"
  kate confident "I want to hear you beg."
  kate confident "Get on your knees and beg me for them."
  "She looked so dominant. So in control of everything."
  "And me, I just melted under her stern gaze."
  "A true moment of shame."
  kate confident "So, what's it going to be?"
  show kate confident at move_to(.75)
  menu(side="left"):
    extend ""
    "Beg":
      show kate confident at move_to(.5)
      "Just thinking about the next part makes my cheek burn in shame."
      "The two of us, alone in the gym."
      window hide
      show layer master:
        xanchor 0.5 xpos 960 yanchor 1.0 ypos 1080
        parallel:
          ease_quart 2.0 zoom 2.0
        parallel:
          pause 0.4
          ease_quad 0.4 xoffset 10
          ease_quad 0.4 xoffset -30
          ease_quad 0.4 xoffset 20
          ease_quad 0.4 xoffset 0
      pause 2.0
      window auto show
      show kate blush with dissolve2
      kate blush "That's more like it."
      hide layer master
      show layer master:
        xanchor 0.5 xpos 960 yanchor 1.0 ypos 1080 zoom 2.0
      kate blush "Now, did you have something you wanted to ask?"
      mc "Please, I'm begging you, [kate]..."
      mc "...please, give me the signs for the fundraiser."
      mc "Please, please, please."
      show kate thinking with dissolve2
      kate thinking "..."
      "She really dragged the moment out, too."
      "Enjoying the shame burning me up from within."
      kate thinking "Yeah, that's still not quite right."
      kate thinking "Something else is missing."
      mc "[kate], I need to get—"
      kate embarrassed "Stay down there until I say otherwise."
      mc "Yes, ma'am."
      kate embarrassed "Good."
      kate gushing "Wait, that's it! I know what you're missing!"
      kate gushing "You've been such an obedient little bitch today, and I think everyone should know."
      kate gushing "Stay where you are."
      kate thinking "Hmm... where did I leave the markers?"
      window hide
      pause 0.25
      show kate thinking at move_to(.35,.5)
      pause 0.75
      show kate thinking flip at move_to(.65,1.0)
      pause 1.25
      show kate thinking at move_to(.5,.5)
      pause 0.5
      window auto
      mc "[kate], I really have to—"
      show kate blush_marker at center with dissolve2:
        easein 0.175 yoffset -10
        easeout 0.175 yoffset 0
        easein 0.175 yoffset -4
        easeout 0.175 yoffset 0
      kate blush_marker "Ah, there they are!"
      show kate blush_marker:
        yoffset 0
      mc "What do you need a marker for?"
      "I had no idea what she had in store for me, but I was super worried at that point."
      "Worried about the fundraiser... and my pride."
      kate blush_marker "Shut up and close your eyes."
      mc "But—"
      show kate embarrassed_marker with dissolve2
      kate embarrassed_marker "Now."
      "..."
      window hide
      show black onlayer screens zorder 100 with close_eyes
      pause 0.5
      show black onlayer screens zorder 4
      $set_dialog_mode("default_no_bg")
      "I probably shouldn't have let her walk all over me like this, but I\nwas scared."
      "And maybe I liked it in some twisted way..."
      show kate blush_marker with dissolve2
      kate blush_marker "Stay perfectly still."
      "And then I felt the wet tip of the marker against my forehead."
      play sound "<to 1.9>marker"
      kate blush_marker "{i}\"Property...\"{/}"
      play sound "<from 2.4 to 3>marker"
      kate blush_marker "{i}\"...of...\"{/}"
      play sound "<from 3.8>marker"
      kate blush_marker "{i}\"...[kate].\"{/}"
      kate blush_marker "There! All done."
      kate blush_marker "Open your eyes."
      show black onlayer screens zorder 100
      pause 0.5
      hide black onlayer screens
      show layer master
      show kate laughing
      with open_eyes
      window auto
      $set_dialog_mode("")
      kate laughing "Now everyone will know who you belong to!"
      mc "..."
      mc "Can I take the signs now?"
      kate smile "I'll allow you to have one."
      kate smile "Take it and get out of my sight before I change my mind."
      window hide
      $school_gym["car_wash_sign_taken"] = True
      $mc.add_item("car_wash_sign")
      window auto
      mc "Just one? What's the point of making so many, then?"
      kate confident "So I can deny worthless little dogs like you."
      mc "..."
      kate neutral "Now, piss off."
      window hide
      play sound "falling_thud"
      scene black
      show black onlayer screens zorder 100
      with vpunch
      $game.location = "school_first_hall_east"
      $renpy.pause(0.07)
      $renpy.get_registered_image("location").reset_scene()
      scene location
      show misc flashback zorder 100
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "Ouch! That kick was a good one..."
      "Those cheerleader legs really hurt. I remember being sore for a few days after."
      $quest.jo_washed["car_wash_volunteers"].add("kate")
    "Snatch and run":
      show kate confident at move_to(.5)
      "What's the point of daydreams if you can't change a few things here and there?"
      "And this is probably what I should have done, anyway."
      mc "Fine, I'll beg."
      mc "But not until you tell that guy behind you to leave."
      kate surprised "Huh? What guy?"
      kate surprised "There's no guy—"
      window hide
      play sound "fast_whoosh"
      show kate surprised at move_to(.05) with hpunch
      window auto
      mc "Yoink!"
      window hide
      $school_gym["car_wash_sign_taken"] = True
      $mc.add_item("car_wash_sign")
      show kate surprised at move_to(.5,1.0)
      window auto show
      show kate angry with dissolve2
      kate angry "You little worm!"
      kate angry "Give that back! You haven't earned it!"
      mc "Sorry, [kate]! No time to stay and chat!"
      call goto_school_first_hall_east
      kate "{i}You'll pay for that, [mc]!{/}"
      "If only I had more guts back then, this is how it would've turned out.{space=-5}"
      "But I guess outsmarting [kate] is easier in theory than practice..."
  "Anyway, one sign was firmly in my possession after that little incident."
  if sum([mc.owned_item("garden_hose"),mc.owned_item("car_wash_sign"),mc.owned_item("bottle_of_soap"),mc.owned_item("sponges")]) == 4:
    "So, when I finally collected all the items, the plan was to head outside."
    $quest.jo_washed.advance("car_wash")
  return

label quest_jo_washed_supplies_janitor_closet:
  "Oh, man. The next part is a bit hard to think about."
  "It was one of those moments that probably didn't mean much to anyone but me."
  "I was pushing against the door with all my might..."
  "Veins bulging, door unbudging..."
  "That's when she showed up."
  window hide
  show lindsey smile with Dissolve(.5)
  window auto
  lindsey smile "[mc], are you okay?"
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   "She had that stupid innocent smile on her lips as usual."
#   "Back then, I didn't know how much of a cunt she was."
#   "If I did, I'd probably have used her head as a battering ram to get the door open."
#   "Just like this..."
#   window hide
#   pause 0.25
#   show lindsey smile at disappear_to_right
#   window auto show
#   show lindsey afraid with dissolve2
#   lindsey afraid "Eeeep!{w=0.5}{nw}"
#   window hide None
#   play sound "<from 6.4 to 7.4>battering_ram"
#   show black onlayer screens zorder 100 with hpunch
#   pause 1.0
#   hide black onlayer screens with Dissolve(.5)
#   window auto
#   "Like cracking eggs."
    pass
  else:
    "That was such a happy moment. She knew my damn name!"
    "I'd never thought a girl like [lindsey] would ever pay me any attention...{space=-40}"
    "And she even smiled at me!"
    lindsey smile "What are you doing with that door?"
    mc "You didn't hear? I'm the new janitor."
    lindsey laughing "Really, now?"
    "I actually made her laugh. One of my biggest achievements in life."
    mc "I just couldn't contain my passion for mopping any longer."
    mc "Something about seeing that spotless bathroom stall..."
    lindsey laughing "You're messing with me! I know it!"
    mc "Heh, maybe a little bit. But I do need to get into this closet."
    mc "Have you heard about the fundraiser?"
    lindsey smile "Yep, [flora] came flying past me muttering something about a car wash.{space=-50}"
    mc "I need to find some soap, and I'm guessing it's in there."
    mc "Speaking of, are you coming?"
    lindsey thinking "I wish I could, but I have a race to prepare for this afternoon."
    lindsey thinking "There'll be talent scouts and everything."
    "Oh, man. Thinking about this really hurts."
    "[lindsey] had such a bright future ahead of her..."
    lindsey blush "But maybe I can help if it's locked?"
    show lindsey blush at move_to(.75)
    menu(side="left"):
      extend ""
      "\"It's fine! I got this.\"":
        show lindsey blush at move_to(.5)
        mc "It's fine! I got this."
        "...is what I wish I had said."
        lindsey laughing "Oh, okay!"
        "And then I wish I would've rolled up my sleeves..."
        lindsey flirty "Whoa!"
        "She would've swooned as I took a few steps back, and then charged{space=-10}\nthe door!"
        window hide
        play sound "<from 6.4 to 7.4>battering_ram"
        show location with hpunch
        pause 0.25
        window auto show
        show lindsey blush with dissolve2:
          easein 0.175 yoffset -10
          easeout 0.175 yoffset 0
          easein 0.175 yoffset -4
          easeout 0.175 yoffset 0
        lindsey blush "Oh, [mc]! That was so amazing!"
        show lindsey blush:
          yoffset 0
        lindsey blush "Please impregnate me right now!"
        mc "Sorry, darling. I have a fundraiser to save."
        mc "Being a hero is no walk in the park."
        lindsey blush "You're so brave! I will save myself for you, [mc], and you alone!"
        "Ah, if only. If only..."
        window hide
        pause 0.25
        play sound "<from 0.4>poof"
        show lindsey blush:
          xanchor 0.5 xpos 960 yanchor 0.5 ypos 540
          easeout_bounce 0.25 alpha 0.0 zoom 0.0
        pause 0.5
        window auto
        "But [lindsey] did help me get into the janitor's closet... just not\nlike this."
      "\"Thanks! I'd really appreciate it.\"":
        show lindsey blush at move_to(.5)
        mc "Thanks! I'd really appreciate it."
        lindsey laughing "Okay, how about you try this?"
        "She then pulled a hairpin out of her strawberry locks."
        "And the scent of her hair accompanied it. "
        "And me, like the idiot I was back then... brought it right up to my nose, inhaling the sweet cotton candy perfume."
        lindsey skeptical "Did you just smell my hairpin?"
        mc "What? No! Of course not!"
        mc "I was, err... merely inspecting it."
        lindsey flirty "Oh! Do you think it'll work?"
        mc "We're about to find out."
        "As I bent down and started fiddling with the lock, she got up close to me and put her hand on my arm."
        "I guess it was an act of support, but it felt like so much more."
        "A girl like [lindsey], willingly touching me... total daydream material."
        play sound "lock_click"
        "And somehow, maybe by a miracle, her hairpin hit home, and the lock clicked open."
        show lindsey blush with dissolve2:
          easein 0.175 yoffset -10
          easeout 0.175 yoffset 0
          easein 0.175 yoffset -4
          easeout 0.175 yoffset 0
        lindsey blush "Whoa! That was so cool!"
        show lindsey blush:
          yoffset 0
        "I couldn't believe my luck."
        "She genuinely looked so impressed, I could hardly keep the smile off my face."
        mc "Thanks for the help, [lindsey]!"
        mc "I think I messed up your hairpin, though."
        lindsey laughing "Oh, I have plenty of those! Don't worry about it!"
        lindsey flirty "I'm just glad I could help. I hope the fundraiser goes well!"
        window hide
        show lindsey flirty at disappear_to_left
        pause 0.5
        window auto
        "I really can't get enough of her cheery innocent nature..."
  window hide
  $mc.add_item("bottle_of_soap")
  $quest.jo_washed["lindsey_location"] = None
  if sum([mc.owned_item("garden_hose"),mc.owned_item("car_wash_sign"),mc.owned_item("bottle_of_soap"),mc.owned_item("sponges")]) == 4:
    window auto
    "So, when I finally collected all the items, the plan was to head outside."
    $quest.jo_washed.advance("car_wash")
  return

label quest_jo_washed_supplies_art_class:
  "So, then my thought process was something like this..."
  "If sponges were going to be anywhere, it had to be the art classroom.{space=-30}"
  "People paint with sponges, right?"
  "The art classroom felt so different back then..."
  "[jacklyn] sure was a big improvement."
  "If she were a part of that timeline, the conversation would probably have gone something like this..."
  window hide
  play sound "<from 0.4>poof"
  show expression LiveComposite((104,358),(0,0),"school art_class jacklyn",(0,0),AlphaMask(Crop((361,389,104,358),"misc flashback"),"school art_class jacklyn")) as jacklyn_dream onlayer screens:
    alpha 0.0 xanchor 0.5 xpos 413 yanchor 0.5 ypos 568 zoom 0.0
    easein_bounce 0.25 alpha 1.0 zoom 1.0
    pause 0.5 alpha 0.0
  show screen location
  pause 0.25
  $quest.jo_washed["jacklyn_location"] = "school_art_class"
  return

label quest_jo_washed_supplies_jacklyn:
  hide jacklyn_dream onlayer screens
  show jacklyn smile
  with Dissolve(.5)
  mc "Hey, [jacklyn]."
  jacklyn smile "Moneybags."
  mc "Do you know where I can find any sponges?"
  mc "We need them for the car wash fundraiser that's going down today."
  "Or rather, this very second..."
  jacklyn thinking "Sponges, huh?"
  jacklyn thinking "Sure, if the available selection hits your noggin."
  mc "And where might this nogging-hitting selection be?"
  jacklyn smile_hands_down "Over in the noggin-hitting cabinet, of course."
  jacklyn smile_hands_down "I'll grab them for you. Just a sec."
  show jacklyn smile_hands_down at disappear_to_right
  "Man, it's so hard to get her voice right in my head..."
  show jacklyn excited_sponges at appear_from_right
  jacklyn excited_sponges "These tit your tat?"
  "Uh, there's way more than I thought there would be."
  "I probably don't need all of these..."
  mc "Those are perfect, but I really only need a couple of them."
  jacklyn laughing_sponges "Right-o. You've got a keen eye, tiger."
  jacklyn annoyed_sponges "Some of these sponges are way too abrasive for a car."
  jacklyn annoyed_sponges "You don't want to rub one out with those."
  mc "Then why even bring those out?"
  jacklyn laughing_sponges "An artist needs to see all the options before him. Even the bad ones.{space=-15}"
  jacklyn laughing_sponges "Be a baby and give them a spin on me, will you?"
  mc "Give them a spin... on you?"
  jacklyn excited_sponges "Yeah, rub one out."
  jacklyn excited_sponges "All of them, actually."
  "The thing about [jacklyn] is that she probably would've said something{space=-35}\nlike that."
  "She's so effortlessly sexy."
  mc "You want me to scrub you with the sponges?"
  mc "Can't I just take the two softest ones?"
  jacklyn annoyed_sponges "Do you want to know the truth, or do you want to feel it?"
  show jacklyn annoyed_sponges at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Know the truth.\"":
      show jacklyn annoyed_sponges at move_to(.5)
      mc "Know the truth."
      mc "Sorry, [jacklyn], but I really don't have time to waste."
      jacklyn angry "One man's waste is another woman's treasure."
      jacklyn angry "Take your soft sponges and soak them up."
      window hide
      $mc.add_item("sponges")
      window auto
      jacklyn angry "You've left me all dry."
      "Why did I make [jacklyn] annoyed with me in a daydream?"
      "I got some issues to figure out..."
      "In fact, why don't I have her naked in my mind? Jesus."
      window hide
      pause 0.25
      play sound "<from 0.4>poof"
      show jacklyn angry_clothesless
      show jacklyn angry_jacklynless as jacklyn_clothes:
        xanchor 0.5 xpos 960 yanchor 0.5 ypos 540
        easeout_bounce 0.25 alpha 0.0 zoom 0.0
      show jacklyn avatar b1hand1_n as jacklyn_hand:
        xpos 1022 ypos 684
      pause 0.5
      window auto
      "Much better."
      "She really does have a great body."
      "Makes you want to do all sorts of bad things."
      mc "Thanks, [jacklyn]."
      window hide
      $jacklyn.outfit = {"bra": None, "panties": None, "pants": None, "pin": None, "shirt": None}
      hide jacklyn
      hide jacklyn_clothes
      hide jacklyn_hand
      with Dissolve(.5)
    "\"Feel the truth.\"":
      show jacklyn annoyed_sponges at move_to(.5)
      mc "Feel the truth."
      mc "...whatever that means."
      jacklyn excited_sponges "Bingo, buster."
      jacklyn excited_sponges "Give this one a shot."
      jacklyn excited_sponges "Take my hand, and give my arm a good rub."
      "Hmm... she probably wouldn't have said that."
      "It would've been something more clever, like... deep-tissue scrub-rub.{space=-30}"
      mc "All right... here goes..."
      window hide
      play sound "<to 0.25>sponge_rub"
      show jacklyn excited_sponges with vpunch
      window auto show
      show jacklyn cringe_sponges with dissolve2
      jacklyn cringe_sponges "This one is a bust. Too soft."
      mc "That's good, right?"
      mc "We can use it for the car wash."
      jacklyn cringe_sponges "Small potatoes, grab the next one."
      mc "On it."
      "This is such a weird fantasy. Rubbing [jacklyn] with sponges."
      "Although...  I wonder how she would react to a rougher one?"
      "Probably something like this..."
      window hide
      play sound "<from 0.4 to 0.75>sponge_rub"
      show jacklyn cringe_sponges with vpunch
      window auto show
      show jacklyn annoyed_sponges with dissolve2
      jacklyn annoyed_sponges "Whoa..."
      mc "Good whoa? Bad whoa?"
      jacklyn annoyed_sponges "Next one whoa."
      jacklyn annoyed_sponges "Too soft."
      mc "If you say so."
      "An even rougher one would probably go something like this..."
      window hide
      play sound "<from 0.75 to 1.0>sponge_rub"
      show jacklyn annoyed_sponges with vpunch
      window auto show
      show jacklyn excited_sponges with dissolve2
      jacklyn excited_sponges "This one is a banger!"
      mc "No kidding. It feels like a pumice stone."
      mc "There's no way I could use this on the cars."
      jacklyn excited_sponges "Rub harder, baby."
      mc "Excuse me?"
      "Pretending to be surprised in my own fantasy. Classic."
      jacklyn excited_sponges "More!"
      "Of course, no one would actually get off to getting rubbed with a sponge, but..."
      "I guess my fantasy version of [jacklyn] has more of a pain fetish than the real one. Hehe."
      window hide
      play sound "<from 1.05 to 1.4>sponge_rub"
      show jacklyn excited_sponges with vpunch
      window auto show
      show jacklyn laughing_sponges with dissolve2
      jacklyn laughing_sponges "Oooh..."
      jacklyn laughing_sponges "Rub me raw, [mc]!"
      window hide
      play sound "<to 0.25>sponge_rub"
      show jacklyn laughing_sponges with vpunch
      window auto
      jacklyn laughing_sponges "Mmm! It hurts so good!"
      "Her hand grips me tighter and tighter."
      window hide
      play sound "<from 0.75 to 1.0>sponge_rub"
      show jacklyn laughing_sponges with vpunch
      window auto
      "What kind of freak enjoys this kind of pain?"
      "My kind of freak, I guess."
      window hide
      play sound "<from 1.05 to 1.4>sponge_rub"
      show jacklyn laughing_sponges with vpunch
      window auto
      mc "[jacklyn]..."
      jacklyn excited_sponges "Oh?"
      jacklyn excited_sponges "Nice fat roll of quarters."
      "My dick must've grown, like, six extra inches..."
      mc "I can't help it when I see you get so turned on."
      jacklyn laughing_sponges "My passion infects the soul, baby."
      "She probably wouldn't have said that, but I'm getting too turned on to care."
      mc "Consider me infected."
      jacklyn excited_sponges "But I'd say your ducks are all in a row."
      jacklyn excited_sponges "Now we both feel the truth."
      "Yeah, a lot more truth than I was expecting..."
      jacklyn excited_sponges "My whistle's been wet, so thank you."
      mc "Can't we keep going?"
      jacklyn laughing_sponges "Would you like to see how many sponges I can fit in my ass?"
      "Okay, now it's turning into a wet dream rather than a daydream."
      "Time to skip forward a bit."
      window hide
      hide jacklyn with dissolve2
      $mc.add_item("sponges")
  if sum([mc.owned_item("garden_hose"),mc.owned_item("car_wash_sign"),mc.owned_item("bottle_of_soap"),mc.owned_item("sponges")]) == 4:
    window auto
    "So, when I finally collected all the items, the plan was to head outside."
    $quest.jo_washed.advance("car_wash")
  return

label quest_jo_washed_car_wash_other_locations:
  $current_location = game.location.title.lower()
  if quest.jo_washed["before_bringing_the_items"]:
    "Then, I went to the [current_location]..."
  else:
    "But before bringing the items to the car wash, I went to the [current_location]..."
    $quest.jo_washed["before_bringing_the_items"] = True
  return

label quest_jo_washed_car_wash_outside:
  "Finally, I made it outside."
  "Just on time, of course."
  "The line of cars was already stretching all the way down to Bonnie's Bagels and Flytraps."
  return

label quest_jo_washed_car_wash:
  show flora annoyed with Dissolve(.5)
  mc "Hey! I got the goods."
  flora annoyed "You're late, [mc]!"
  flora annoyed "Though I'm surprised you even pulled through."
  mc "What do you mean? I'm very reliable."
  flora eyeroll "Yeah, yeah, just help me set everything up, okay?"
  flora eyeroll "The donors are getting impatient."
  flora annoyed "If they drive off, I swear I'll strangle you with the hose."
  mc "Huh! I didn't know you were into breath-play."
  flora cringe "Shut up."
  window hide
  $mc.remove_item("bottle_of_soap")
  $mc.remove_item("garden_hose")
  $mc.remove_item("sponges")
  $mc.remove_item("car_wash_sign")
  window auto
  mc "Why don't you take off your shirt or something to keep them satisfied?{space=-60}"
  flora annoyed "Why don't you?"
  mc "Well, we want to keep the donors here, don't we? Not drive them away.{space=-55}"
  flora annoyed "Whatever."
  mc "..."
  mc "So, no bikinis?"
  flora cringe "Ugh! Fine!"
  flora cringe "I'll go change before I drown myself in one of the buckets."
  window hide
  show flora car_wash flora excited_right
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 1.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  flora car_wash flora excited_right "Thanks for waiting, everyone!"
  flora car_wash flora excited_right "Welcome to the Newfall High car wash fundraiser!"
  flora car_wash flora excited_right "You'll get a scrub and a rinse!"
  flora car_wash flora excited_right "All donations will go to our school dance at the end of the year!"
  "The funny thing was, it was just the two of us there, and a line of cars longer than [kate]'s strap on."
  if "maxine" in quest.jo_washed["car_wash_volunteers"]:
    "But then a miracle happened..."
    maxine "That's a lot of cars. I didn't know car washes were so in demand."
    window hide
    show flora car_wash flora excited_right maxine neutral with Dissolve(.5)
    window auto
    mc "[maxine]?"
    "I truly didn't expect her to show up."
    "Especially not in... {i}that.{/}"
    show flora car_wash flora excited_right maxine surprised with Dissolve(.5)
    maxine "Has no one taught you to expect the unexpected?"
    "Not when a zombie hunting weirdo comes dressed in a hot bikini."
    flora car_wash flora excited_right maxine surprised "[maxine]! We could really use a hand! Thanks for coming!"
    "I always wished she'd get that excited when seeing me. Alas."
    $guard.default_name = "Driver"
    guard "Make sure to get the windshield real good!"
    guard "Lots of dead bugs splattered on there."
    show flora car_wash flora excited_right maxine smile with Dissolve(.5)
    maxine "Their bodies will be exhumed, and your windshield will shine again."
    guard "Uh... okay."
    $guard.default_name = "Guard"
    "That guy looked completely shocked, but I was already enjoying myself... and the view."
  if "kate" in quest.jo_washed["car_wash_volunteers"]:
    kate "I thought there would be nicer cars..."
    "And then, of course, [kate] showed up."
    window hide
    show flora car_wash flora excited_right maxine smile kate annoyed hands_on_hips with Dissolve(.5)
    window auto
    "But it wasn't all bad. [kate] in a bikini is a treasured memory."
    kate "I'm just here to get some sun and watch others work."
    show flora car_wash flora excited_right maxine smile kate smile hands_on_hips with Dissolve(.5)
    kate "Oh, and to make sure you don't wash off that marker, [mc]."
    mc "..."
    flora car_wash flora surprised maxine smile kate smile hands_on_hips "I wasn't going to say anything, but..."
    flora car_wash flora surprised maxine smile kate smile hands_on_hips "That's a bit weird, [mc]."
    mc "It's not like I wanted this, okay?"
    flora car_wash flora smile maxine smile kate smile hands_on_hips "So, since you're [kate]'s property, does that mean you won't be coming home?"
    kate "I do have an empty dog house."
    mc "No way! I'm definitely coming home!"
    mc "If I don't, call the cops and say I've been kidnapped."
    show flora car_wash flora smile maxine smile kate neutral with Dissolve(.5)
    kate "Don't flatter yourself."
    flora car_wash flora smile maxine smile kate neutral "You'd probably have to pay the kidnappers for that to happen, [mc].{space=-5}"
    mc "Ugh..."
    "If the fundraiser had taken place in the new timeline, I bet [isabelle] would've been there to back me up."
  else:
    "If the fundraiser had taken place in the new timeline, I bet [isabelle] would've been there as well."
  "It would probably have gone down like this..."
  isabelle "The cavalry is here!"
  window hide
  if "kate" in quest.jo_washed["car_wash_volunteers"]:
    show flora car_wash flora smile maxine smile kate neutral isabelle neutral with Dissolve(.5)
  else:
    show flora car_wash flora excited_right maxine smile kate neutral isabelle neutral with Dissolve(.5)
  window auto
  isabelle "I heard you needed some help."
  "She'd be showing off that perfect milky white skin, in an appropriately{space=-40}\nrevealing bikini."
  flora car_wash flora excited_right maxine smile kate neutral isabelle neutral "Thanks for showing up, [isabelle]! It's a huge help."
  show flora car_wash flora excited_right maxine smile kate neutral isabelle smile with Dissolve(.5)
  isabelle "Of course, love! Where should I start?"
  flora car_wash flora smile maxine smile kate neutral isabelle smile "You can start by sucking [mc]'s dick."
  isabelle "Perfect! Let me just get my lips wet..."
  "Okay, maybe that's a bit over the top."
  "Although, what if [jacklyn] showed up as well?"
  "That would've been something."
  jacklyn "Wicked wheels over here."
  window hide
  show flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn annoyed with Dissolve(.5)
  window auto
  jacklyn "They could use some artistic twang, though."
  "She'd be cool and confident as ever, probably only wearing a bikini to stop others from getting flustered."
  "Although, that clearly wouldn't have worked on [flora]."
  flora car_wash flora surprised_left maxine smile kate neutral isabelle neutral jacklyn annoyed "J-[jacklyn]?!"
  "She would have blushed so hard seeing [jacklyn] like this."
  show flora car_wash flora surprised_left maxine smile kate neutral isabelle neutral jacklyn smile_left with Dissolve(.5)
  jacklyn "What's swinging, sister?"
  jacklyn "Let's take this car to school, yeah?"
  flora car_wash flora smile_left maxine smile kate neutral isabelle neutral jacklyn smile_left "Y-yeah, let's do it!"
  flora car_wash flora smile_left maxine smile kate neutral isabelle neutral jacklyn smile_left "T-thanks for helping us out, [jacklyn]."
  "You should be thanking me for bringing this fantasy to life."
  "...or, well, maybe I should thank myself."
  "But back to the real memory for a bit..."
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile "We need some water over here, [mc]!"
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile "Can you man the hose?"
  "The air was so hot that day, she probably couldn't wait to get wet."
  window hide
  show flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_anticipation with Dissolve(.5)
  window auto
  mc "All right, here it goes!"
  "Ready..."
  "...aim..."
  show flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_spraying
  "...fire!" with vpunch
  show flora car_wash flora embarrassed maxine surprised kate neutral isabelle surprised jacklyn smile hose_everyhwere wet
  flora car_wash flora embarrassed maxine surprised kate neutral isabelle surprised jacklyn smile hose_everyhwere wet "Ahhh!" with hpunch
  mc "That's right, soak it up!"
  "No matter what happens, I'll always have the mental image of her dripping in front of me."
  "And it's one I really like to take my time with and relive."
  flora car_wash flora annoyed maxine surprised kate neutral isabelle surprised jacklyn smile hose_dripping wet "[mc], come on!"
  flora car_wash flora annoyed maxine surprised kate neutral isabelle surprised jacklyn smile hose_dripping wet "At least try to hit the car!"
  mc "How about I try to hit your open mouth instead?"
  flora car_wash flora cringe maxine surprised kate neutral isabelle surprised jacklyn smile hose_dripping wet "Don't you even—"
  show flora car_wash flora embarrassed_down maxine smile kate neutral isabelle neutral jacklyn smile hose_flora_mouth wet
  flora car_wash flora embarrassed_down maxine smile kate neutral isabelle neutral jacklyn smile hose_flora_mouth wet "Eeeek!" with vpunch
  "Watching [flora] running around, trying to escape the cold spray of the water, did something to me."
  "Her youthful laughter and looks that could kill."
  "Man, I really lost everything when high school ended."
  flora car_wash flora annoyed maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "[mc]! Stop it!"
  "I could think of her running around in that soaked bikini all day..."
  "But there was one thing missing."
  mc "..."
  mc "Where's [jo]?"
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "I'm not sure..."
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "She said she'd be here, but I think we can manage."
  flora car_wash flora smile maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "[jo] taking a breather is fine. She deserves it."
  "Well, I deserve to see her in a bikini."
  show flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet with Dissolve(.5)
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "It is a little strange, though."
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "She was supposed to be here."
  mc "I'll go look for her."
  "I remember being a bit grumpy about having to leave the girls..."
  "...but the day didn't feel complete without [jo]."
  mc "Any idea where she might've gone?"
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "Sorry, no clue."
  flora car_wash flora excited_right maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "But! I'll be happy to take over the hose until you get back!"
  "She said, mischievously."
  mc "Fine, but treat her like you would treat my—"
  flora car_wash flora annoyed maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet "Why do you have to make everything weird?"
  $quest.jo_washed.advance("insecure")
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 0.5
  hide flora
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_jo_washed_insecure_entrance_hall:
  "So, I left the land of wet boobs and honey, and I went back inside to look for [jo]."
  "Truly a heroic sacrifice on my part."
  return

label quest_jo_washed_insecure_other_locations:
  $current_location = game.location.title.lower()
  "I looked in the [current_location]..."
  return

label quest_jo_washed_insecure_sports_wing:
  "I looked in the sports wing..."
  "And that's when I heard it."
  "It came from inside the women's bathroom."
  "A sniffing, almost sobbing sound."
  "And in the case of emotional turmoil, everyone knows the rules of gender-restricted areas are suspended."
  "...at least, that's what I hoped as I entered."
  return

label quest_jo_washed_insecure_bathroom:
  "..."
  "Someone was definitely crying in one of the stalls."
  "It was a familiar sound."
  "I've always been a big enough disappointment to recognize them anywhere."
  "So, I steeled myself, hoping against the odds that this wasn't my doing."
  mc "[jo], is that you?"
  jo "..."
  "And the sniffing instantly stopped, as if I'd caught her masturbating or something."
  "Strange how sadness is associated with shame."
  $jo["outfit_stamp"] = jo.outfit
  $jo.outfit = {"glasses": "jo_glasses", "bra": "jo_bikini_top", "panties": "jo_bikini_bottom"}
  show jo embarrassed at appear_from_left
  jo embarrassed "[mc]?! What are you doing in here?!"
  jo embarrassed "You're not allowed in this bathroom!"
  "As she came out of the stall, I remembered thinking..."
  "What on god's beautiful, voluptuous green earth am I seeing\nright now?"
  "What did I just walk in on?"
  "And why did I wait so long?!"
  mc "I came!"
  jo embarrassed "What?!"
  mc "Looking for you!"
  mc "I came looking for you!"
  jo embarrassed "..."
  mc "Err, we've been working on the fundraiser for a while and got worried{space=-25}\nwhen you never showed up."
  jo thinking "Oh... I see..."
  jo annoyed "That's still no excuse to go barging into the women's bathroom."
  jo annoyed "I believe I raised you better than that."
  mc "Sorry, [jo]. [flora] and I were just afraid something might've happened.{space=-5}"
  "I remember her glasses were fogged up and her mascara slightly smudged."
  "I'd heard her crying at night sometimes throughout my childhood, but never before in the school."
  jo sad "I appreciate your concern, honey, but nothing happened, okay?"
  mc "If nothing happened, then why were you hiding in here?"
  jo annoyed "Who says I'm hiding?"
  jo annoyed "Can't a woman simply change without someone barging in?"
  mc "I heard you crying..."
  mc "Just wanted to make sure you're okay."
  jo sad "..."
  "That's when her face dropped, and fresh tears started pouring down her cheeks."
  jo sad "Fine..."
  jo sad "I was getting changed to join the fundraiser..."
  jo sad "I guess it's just been so long since I wore this..."
  jo sad "I didn't realize how much weight I've gained..."
  jo embarrassed "It barely fits me anymore... I can't show up wearing it..."
  jo embarrassed "Certainly not in front of the other students... that would be humiliating!"
  mc "What are you talking about?"
  mc "Weight? Are you crazy?"
  jo thinking "[mc], you don't have to do that..."
  jo thinking "I feel the strings and fabric stretching more than they're\nsupposed to."
  jo thinking "It used to fit me so much better..."
  "She seemed really torn up about this."
  "Like the years were somehow starting to catch up with her."
  "And maybe it was my mind filling up with dopamine, but..."
  show jo thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You look incredible, [jo].\"":
      show jo thinking at move_to(.5)
      mc "You look incredible, [jo]."
      mc "I don't know how that bikini used to fit..."
      "...I could only imagine..."
      mc "...but there's no way it looked better than it does right now."
      jo blush "[mc]..."
      jo blush "I appreciate your support, but you don't need to lie to make me\nfeel better."
      jo blush "The thing barely covers what it needs to!"
      "But that was the point, wasn't it?"
      "If I sneezed too hard, it'd probably blow off her top."
      mc "That's the current trend, though!"
      mc "According to Cosmololitan, you're a league above the other girls out there."
      jo afraid "That's very flattering, [mc], but I'm not sure it's true."
      mc "It is. [flora] would tell you the same."
      mc "Besides, [jo], it's a car wash!"
      mc "Isn't the whole point to get wet in a swimsuit and soak in the sun?"
      jo eyeroll "No, the whole point is to make money."
      mc "And don't you think we'll make more money if you go out\nwearing that?"
      jo sarcastic "..."
      "Checkmate."
      jo sarcastic "You do have a point..."
      mc "And you have a fundraiser to fundraise."
#     jo blush "You remind me so much of your father right now."
#     jo blush "He always made me feel good about myself."
#     mc "Hmm... thank you, I guess."
#     "It was one of the few times she mentioned him, and it somehow felt like a good compliment."
      jo sarcastic "Yes, I suppose I do."
      mc "So, let's get out there and make some money!"
      jo flirty "All right! Let's do it!"
      window hide
      show jo flirty at disappear_to_right
      pause 0.5
      window auto
      "I could barely believe that worked..."
      "She used to be so stubborn back then, and I very rarely got my way."
      "I guess that's part of why this is such a treasured memory."
      "It's an embarrassing thing to admit, but I wasn't actually this mature{space=-10}\nback then."
      "I had this need to feel superior. To get one up on [jo]."
      "In reality, I probably made her feel way worse about her bikini situation..."
    "\"It is a bit much...\"":
      show jo thinking at move_to(.5)
      mc "It is a bit much..."
      jo annoyed "You don't have to tell me."
      jo annoyed "I'm very well aware."
      mc "It doesn't look bad, but I'm not sure how legal it would be to wear that in public..."
      "She didn't look bad, of course, but it felt good to tell [jo] she wasn't perfect."
      "It was probably my insecurities taking over, or perhaps the need\nfor revenge."
      jo embarrassed "I know that, [mc]!"
      jo embarrassed "I would've been outside already otherwise, okay?"
      mc "I'm sorry, [jo]."
      jo sad "I know, I know. You didn't do or say anything wrong."
      jo sad "This is just an old gal's insecurities. It's nothing you need to be worried about."
      jo blush "I really appreciate your help with the fundraiser. And thank you for checking up on me."
      jo blush "Go ahead and get back to the fundraiser now, all right?"
      mc "The thing is, we really need your help..."
      mc "There's way too many cars. We'll be out there all night."
      jo eyeroll "Well, what am I supposed to do?"
      mc "I think you should just come anyway. It will be a bit embarrassing, but you'll survive."
      mc "People will forget it in no time, and the dance will be saved."
      jo thinking "I don't know..."
      mc "Trust me, the perverts in the cars are here to look at the girls.\nThey won't pay you much attention."
      "That was such a cruel thing to say. And it wasn't even true."
      "[jo] had a great body, and I'm sure she was the main gossip for months afterward."
      jo worried "Err... okay. You're right."
      jo worried "The dance has been a thing for as long as the school has existed."
      jo worried "What kind of principal would I be if I let my own issues get in the way of that?"
      mc "That's the spirit! Let's go!"
      window hide
      show jo worried at disappear_to_right
      pause 0.5
      window auto
      "I knew I had hurt her feelings, but I didn't care much about that back then."
      "Just my own selfish gratification..."
  $quest.jo_washed.advance("fundraiser")
  return

label quest_jo_washed_fundraiser:
  "When we got outside, the line of cars had only grown."
  "It seemed like all of Newfall, including tourists, had decided to show up."
  "And much to [jo]'s embarrassment, it would be a day to remember."
  window hide
  show flora car_wash flora hose maxine smile kate neutral isabelle neutral jacklyn smile wet new_cars
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "Give me the hose back!"
  flora car_wash flora hose maxine smile kate neutral isabelle neutral jacklyn smile wet new_cars "Saying please wouldn't hurt, you know?"
  mc "Pretty please with melons on top."
  flora car_wash flora hose maxine smile kate neutral isabelle neutral jacklyn smile wet new_cars "Ugh! Just shut up and spray..."
  window hide
  show flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet new_cars with Dissolve(.5)
  window auto
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet new_cars "What took you so long, anyway?"
  mc "I had to search the school for [jo]."
  flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile hose_dripping wet new_cars "Was something wrong?"
  mc "Well..."
  jo "Everything is fine, sweetie!"
  window hide
  show flora car_wash flora neutral maxine smile kate neutral isabelle neutral jacklyn smile jo smile hose_dripping wet new_cars with Dissolve(.5)
  window auto
  jo "Just, err... some administrative stuff I had to take care of!"
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo smile hose_dripping wet new_cars "What the hell are—"
  mc "...we doing just standing around?!"
  mc "Let's get this show on the road!"
  show flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo excited hose_dripping wet new_cars with Dissolve(.5)
  jo "Yes!"
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo excited hose_dripping wet new_cars "..."
  flora car_wash flora surprised maxine smile kate neutral isabelle neutral jacklyn smile jo excited hose_dripping wet new_cars "Okay, then..."
  "Mission accomplished."
  "And [jo] fit right in."
  "All she needed was to get a little more wet..."
  "Luckily, I was in the perfect position to help her out."
  window hide
  show jo car_wash jo smile hose_dripping
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide flora
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  mc "I see some dirt over to the right, [jo]!"
  jo car_wash jo surprised hose_dripping "Huh? Where?"
  jo car_wash jo smile bending_over hose_dripping "Oh, I see it! Thank you, honey!"
  "For a moment, I thought that her ass would snap that bikini\nbottom off."
  "I mean, who designed that thing anyway?"
  "Must've been a true genius in string theory."
  "Even when she bent over slightly, I could see her labia peeking out."
  "Those perfect folds of heavenly flesh..."
  "Ripples set by the gods themselves..."
  "The ultimate forbidden fruit."
  "And then I got one of my best ideas ever."
  "It was a little mean, but..."
  "Sometimes, you just have to be mean!"
  "I had all of this power in my hands, what kind of person would I have been if I squandered it?"
  mc "You're doing great, [jo]!"
  mc "Here's a bit more water!"
  show jo car_wash jo surprised bending_over hose_spraying wet
  jo car_wash jo surprised bending_over hose_spraying wet "Oh!" with vpunch
  jo car_wash jo laughing bending_over hose_dripping wet "The water feels great! Thank you, [mc]!"
  mc "You're very welcome!"
  "There wasn't quite enough water yet, but I was making progress."
  "And I thought, maybe a direct hit would do the trick..."
  show jo car_wash jo afraid bending_over hose_jo_panties wet
  jo car_wash jo afraid bending_over hose_jo_panties wet "Ah!" with vpunch
  jo car_wash jo afraid exposed hose_jo_pussy wet "Oh, no!"
  "Oh, yes!"
  "The bikini bottom went straight off. I felt like Chris Kyle."
  "Not that it was covering much... but it sure made a difference in\nher posture."
  jo car_wash jo embarrassed covering_up hose_jo_pussy wet "[mc], please!"
  mc "This hose is out of control!"
  mc "It has a mind of its own!"
  "I could see the goosebumps exploding on her skin."
  "The droplets of water rolling down her ass and falling from her pussy..."
  "The streaks wrapping around her legs..."
  "That look of desperation..."
  "Visual poetry."
  jo car_wash jo embarrassed covering_up hose_jo_pussy wet "Mmmm!"
  "And the best part, [jo] was getting turned on."
  "The water pressure must've been high enough on her vulva."
  jo car_wash jo blush covering_up hose_jo_pussy wet "I... I can't..."
  "She was just about to come, right there in front of the entire town!"
  $guard.default_name = "Driver"
  guard "Hell yeah, lady! Keep it going!"
  guard "I'll give ya everything I got, just don't stop!"
  $guard.default_name = "Guard"
  jo car_wash jo blush covering_up hose_jo_pussy wet "Oooh..."
  "[jo] shifted slightly toward the beam."
  "It was a glorious thing to witness."
  "Getting a woman off from ten feet away... surely, I was breaking some sort of obscure record?"
  "Either way, if it went on for much longer, my dick was going to rip through my pants."
  "Then I'd have two hoses, and no standing army could stop me."
  jo car_wash jo embarrassed covering_up hose_jo_pussy wet "I can't! It's too much!"
  "She was cumming!"
  show jo car_wash jo orgasm covering_up hose_jo_pussy wet
  jo car_wash jo orgasm covering_up hose_jo_pussy wet "Oh, god! Ohhh!" with vpunch
  "I was right there with her."
  "The hose water's falling down her back and mixing with her cum..."
  "It must've felt amazing."
  jo car_wash jo orgasm covering_up hose_dripping wet "Huff... puff..."
  "Her knees were wobbling. She was barely able to keep herself up after that orgasm."
  mc "Are you okay, [jo]?"
  jo car_wash jo aftermath covering_up hose_dripping wet "I... I think so..."
  jo car_wash jo aftermath covering_up hose_dripping wet "This is... very inappropriate..."
  mc "Here, let's get you inside and wrapped up in a towel, all right?"
  jo car_wash jo aftermath covering_up hose_dripping wet "T-thank you... sweetie..."
  jo car_wash jo aftermath covering_up hose_dripping wet "But... there are still... more cars..."
  mc "Don't worry about them. We can manage from here."
  jo car_wash jo aftermath covering_up hose_dripping wet "Okay... good..."
  "More cars, and more time to soak girls in bikinis."
  "Even though I never got a cent's worth out of the fundraiser, I felt like a very rich man."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $unlock_replay("jo_embarrassment")
  $game.hour = 19
  $school_roof["garden_hose_taken"] = False
  $quest.jo_washed.advance("fundraiser_over")
  $quest.jo_washed["flora_location"] = "school_entrance"
  pause 1.5
  hide jo
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "It took all day, but we made it through the entire line of cars."
  "And I made a woman orgasm for the first time in my life."
  "What a beautiful day..."
  window hide
  $flora.outfit = {"bra": "flora_bra", "panties": "flora_panties", "pants": "flora_pants", "shirt": "flora_shirt"}
  show flora excited with Dissolve(.5)
  window auto
  flora excited "We did it!"
  "She was so happy, and still completely soaked."
  mc "Pretty successful fundraiser, eh?"
  flora neutral "Pretty successful, even if it did get a little weird."
  "Weird was one way to describe it."
  "Hot and unforgettable would be another."
  mc "How much did we end up making?"
  flora neutral "You'll have to ask [jo]. I just handed her the bucket of money."
  flora neutral "But it was a very full bucket."
  mc "So, you're saying if there's ever another car wash, I'm the first person you're going to call?"
  flora confident "You did handle the hose with skill..."
  mc "Is that a yes?"
  flora concerned "Some would say a suspicious amount of skill..."
  mc "Is that a crime?"
  flora concerned "It should be."
  mc "Practice is the only way you get better at something, [flora]."
  mc "Maybe I could teach you some day?"
  flora angry "Maybe stick to washing cars."
  show flora angry at disappear_to_right
  "Of course, the day wasn't complete without making [flora] throw up in her mouth."
  $jo.outfit = jo["outfit_stamp"]
  show jo sarcastic at appear_from_left
  jo sarcastic "[mc]! I'm happy to report that the fundraiser was a success."
  jo sarcastic "We covered all the expenses of the dance, with plenty to spare."
  jo blush "Sweetheart, I really appreciate your help. We couldn't have done it without you."
  mc "Oh, it was no trouble."
  "Well, it was actually a lot of trouble."
  "But the reward more than made up for it."
  jo eyeroll "If you hadn't come and found me, I probably would've sat crying in that bathroom all afternoon."
  mc "It was nothing, really..."
  jo flirty "Still, you're my handsome young man."
  jo flirty "Never forget that I love you."
  show jo flirty at disappear_to_right
  "That almost made me melt. It was one of the few times she'd told me that."
  "It's one of those things you'll always remember when the goings get hard."
  "At least one person loves you."
  "That's why it'll always be a fond memory of mine."
  window hide
  play sound "dream"
  show white onlayer screens zorder 100 with Dissolve(.5)
  python:
    game.day = a
    game.hour = b
    game.location = c
    mc.money = d
    mc.inv = e
    quest.jo_washed.finish(silent=True)
  pause 0.5
  hide misc flashback
  hide white onlayer screens
  with Dissolve(.5)
  window auto
  "Unfortunately, you can't live in the past forever..."
  "And in the end, you're always flung back to the harsh reality."
  window hide
  hide location
  hide screen hud
  hide screen interface_hider
  show black
  with Dissolve(3.0)
  #
# show expression Text("To Be Continued...", font="fonts/Fresca-Regular.ttf", color="#fefefe", size=69) onlayer screens zorder 100 as to_be_continued:
#   xalign 0.5 yalign 0.5 alpha 0.0
#   pause 0.5
#   easein 3.0 alpha 1.0
# return
  #
  if not quest.lindsey_motive.actually_finished:
    show expression Text("Two Weeks Later", font="fonts/Fresca-Regular.ttf", color="#fefefe", size=69) onlayer screens zorder 100 as two_weeks_later:
      xalign 0.5 yalign 0.5 alpha 0.0
      pause 1.0
      easein 3.0 alpha 1.0
  pause 5.0
  jump quest_fall_in_newfall_start
  #
