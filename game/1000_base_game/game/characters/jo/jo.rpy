style say_name_frame_jo is say_name_frame_jo:
  xminimum 200
  ## normally you may want to change namebox background, padding or position
  #yoffset -300

#style say_name_jo is say_name:
#  color "#FFF"
#  outlines [(3,"#000",1,1)]

init python:

  class Character_jo(BaseChar):
    notify_level_changed=True

    default_name="Jo" # "Johanna", "Jocasta"

    default_outfit_slots=["glasses","coat","shirt","bra","pants","panties"]

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    contact_icon="jo contact_icon"

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("jo_glasses")
      self.add_item("jo_coat")
      self.add_item("jo_blazer")
      self.add_item("jo_shirt")
      self.add_item("jo_skirt")
      self.add_item("jo_pants")
      self.add_item("jo_bra")
      self.add_item("jo_bikini_top")
      self.add_item("jo_white_bra")
      self.add_item("jo_panties")
      self.add_item("jo_bikini_bottom")
      self.add_item("jo_white_panties")
      self.equip("jo_glasses")
      self.equip("jo_blazer")
      self.equip("jo_bra")
      self.equip("jo_skirt")
      self.equip("jo_panties")

    def ai(self):

      self.location=None
      self.activity=None
      self.alternative_locations={}

      ###Forced Locations
      if quest.jo_washed.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if jo["at_none_this_scene"] or jo["at_none_now"] or jo["at_none_today"] or jo["hidden_today"]:
        self.location = None
        self.activity = None
        return

      if quest.back_to_school_special.in_progress:
        if quest.back_to_school_special <"talk_to_flora":
          self.location="home_kitchen"
          self.activity="reading"
          if not jo.talking:
            self.unequip("jo_bra")
          return
        else:
          self.location = None
        return

      if quest.flora_cooking_chilli >= "bring_pot" and quest.flora_cooking_chilli.in_progress:
        self.location = "school_cafeteria"
        self.activity = "coffee"
        return

      if quest.flora_cooking_chilli == "get_milk":
        self.location="home_kitchen"
        self.activity="standing"
        if not jo.talking:
          self.unequip("jo_bra")
        return

      if quest.flora_cooking_chilli == "distracted":
        self.location= None
        self.activity= None
        return

      if quest.isabelle_stolen == "askmaxine":
        self.activity="standing"
        self.location="home_kitchen"
        if not jo.talking:
          self.equip("jo_bra")
        return

      if quest.mrsl_table == "letter":
        if game.hour in (18,19):
          self.location = "home_kitchen"
          self.activity = "standing"
        else:
          self.location = None
          self.activity = None
        return

      if quest.mrsl_HOT in ("text_flora","jo_talk_laundry_done"):
        self.activity="standing"
        self.location="home_kitchen"
        if not jo.talking:
          self.equip("jo_bra")
        return

      if quest.maxine_hook == "day":
        self.location = "school_cafeteria"
        self.activity = "coffee"
        return

      if quest.fall_in_newfall.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.maya_quixote in ("attic","board_game"):
        self.location = None
        self.activity = None
        return

      if quest.lindsey_voluntary == "dream":
        self.location = None
        self.activity = None
        return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return

      if quest.maya_sauce == "dinner":
        self.location = None
        self.activity = None
        return
      ###Forced Locations

      ###ALT Locations
      ###ALT Locations

      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):
        if game.hour in (7,8):
          self.location="home_kitchen"
          self.activity="reading"
          if not jo.talking:
            self.unequip("jo_bra")
          return

        if game.hour in (9,10,11,12,13,14,15,16,17):
          self.location="school_cafeteria"
          self.activity="coffee"
          if not jo.talking:
            self.equip("jo_bra")
          return

        if game.hour in (18,19):
          self.location="home_kitchen"
          self.activity="standing"
          return

        if game.hour > 20:
          self.location=None
          self.activity=None
          self.alternative_locations={}
          return

    def call_label(self):
      if mc["focus"]:
        if mc["focus"] == "kate_wicked":
          if quest.kate_wicked == "phone_call":
            return "quest_kate_wicked_phone_call"
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "panik" and quest.isabelle_dethroning["call_jo"]:
            return "quest_isabelle_dethroning_panik_jo"
        elif mc["focus"] == "maya_witch":
          if quest.maya_witch == "phone_call":
            return "quest_maya_witch_phone_call"
      else:
        if quest.isabelle_buried == "callforhelp":
          return "quest_isabelle_buried_callforhelp_jo"
      return None

    def message_label(self):
      if mc["focus"]:
        if mc["focus"] == "maya_quixote":
          if quest.maya_quixote == "text":
            return "quest_maya_quixote_text"
      return None

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("jo avatar "+state,True):
          return "jo avatar "+state
      rv=[]

      if state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((602,1080))
        rv.append(("jo avatar body1",110,124))
        rv.append(("jo avatar face_concerned",226,241))
        if "_glasses" in state:
          rv.append(("jo avatar b1glasses",224,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b1white_panties",189,761))
        elif "_panties" in state:
          rv.append(("jo avatar b1panty",188,747))
        if "_white_bra" in state:
          rv.append(("jo avatar b1white_bra",109,408))
        elif "_bra" in state:
          rv.append(("jo avatar b1bra",110,401))
        if "_skirt" in state:
          rv.append(("jo avatar b1stocking",188,703))
          rv.append(("jo avatar b1skirt",181,734))
        elif "_pants" in state and "_hands_down" not in state:
          rv.append(("jo avatar b1pants",186,714))
        if "_cup" in state:
          rv.append(("jo avatar b1tea1_n",43,247))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1tea1_c",37,247))
        else:
          if "_hands_down" in state:
            rv.append(("jo avatar b1arm1_n",16,410))
            rv.append(("jo avatar b1pants",186,714))
          elif "_tie" in state:
            rv.append(("jo avatar b1tie_n",18,285))
          else:
            rv.append(("jo avatar b1arm2_n",37,245))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1arm2_c",34,245))
          elif "_shirt" in state:
            rv.append(("jo avatar b1shirt",107,406))
          if "_coat" in state:
            rv.append(("jo avatar b1coat",124,383))
            if "_hands_down" in state:
              rv.append(("jo avatar b1arm1_coat",9,459))
            elif "_tie" in state:
              rv.append(("jo avatar b1tie_coat",18,285))
            else:
              rv.append(("jo avatar b1arm2_coat",33,245))
          if "_bag" in state:
            rv.append(("jo avatar b1bag",269,646))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((602,1080))
        rv.append(("jo avatar body1",110,124))
        rv.append(("jo avatar face_confident",226,239))
        if "_glasses" in state:
          rv.append(("jo avatar b1glasses",224,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b1white_panties",189,761))
        elif "_panties" in state:
          rv.append(("jo avatar b1panty",188,747))
        if "_white_bra" in state:
          rv.append(("jo avatar b1white_bra",109,408))
        elif "_bra" in state:
          rv.append(("jo avatar b1bra",110,401))
        if "_skirt" in state:
          rv.append(("jo avatar b1stocking",188,703))
          rv.append(("jo avatar b1skirt",181,734))
        elif "_pants" in state:
          rv.append(("jo avatar b1pants",186,714))
        if "_cup" in state:
          rv.append(("jo avatar b1tea2_n",48,257))
          rv.append(("jo avatar b1blazer",109,319))
          rv.append(("jo avatar b1tea2_c",39,257))
        else:
          rv.append(("jo avatar b1arm2_n",37,245))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1arm2_c",34,245))
          elif "_shirt" in state:
            rv.append(("jo avatar b1shirt",107,406))
          if "_coat" in state:
            rv.append(("jo avatar b1coat",124,383))
            rv.append(("jo avatar b1arm2_coat",33,245))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((602,1080))
        rv.append(("jo avatar body1",110,124))
        rv.append(("jo avatar face_neutral",226,241))
        if "_glasses" in state:
          rv.append(("jo avatar b1glasses",224,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b1white_panties",189,761))
        elif "_panties" in state:
          rv.append(("jo avatar b1panty",188,747))
        if "_white_bra" in state:
          rv.append(("jo avatar b1white_bra",109,408))
        elif "_bra" in state:
          rv.append(("jo avatar b1bra",110,401))
        if "_skirt" in state:
          rv.append(("jo avatar b1stocking",188,703))
          rv.append(("jo avatar b1skirt",181,734))
        elif "_pants" in state:
          rv.append(("jo avatar b1arm1_n",16,410))
          rv.append(("jo avatar b1pants",186,714))
        if "_cup" in state:
          rv.append(("jo avatar b1tea1_n",43,247))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1tea1_c",37,247))
        else:
          if "_pants" not in state:
            if "_bag" in state:
              rv.append(("jo avatar b1arm3_n",16,413))
            else:
              rv.append(("jo avatar b1arm1_n",16,410))
              if "_skirt" in state:
                rv.append(("jo avatar b1skirt_fix",181,783))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1arm1_c",11,410))
          elif "_shirt" in state:
            rv.append(("jo avatar b1shirt",107,406))
          if "_coat" in state:
            rv.append(("jo avatar b1coat",124,383))
            if "_bag" in state:
              rv.append(("jo avatar b1arm3_coat",9,437))
            else:
              rv.append(("jo avatar b1arm1_coat",9,459))
          if "_bag" in state:
            rv.append(("jo avatar b1bag",269,646))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((602,1080))
        rv.append(("jo avatar body1",110,124))
        rv.append(("jo avatar face_skeptical",226,241))
        if "_glasses" in state:
          rv.append(("jo avatar b1glasses",224,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b1white_panties",189,761))
        elif "_panties" in state:
          rv.append(("jo avatar b1panty",188,747))
        if "_white_bra" in state:
          rv.append(("jo avatar b1white_bra",109,408))
        elif "_bra" in state:
          rv.append(("jo avatar b1bra",110,401))
        if "_skirt" in state:
          rv.append(("jo avatar b1stocking",188,703))
          rv.append(("jo avatar b1skirt",181,734))
        elif "_pants" in state:
          rv.append(("jo avatar b1pants",186,714))
        if "_tie" in state:
          rv.append(("jo avatar b1tie_n",18,285))
        else:
          rv.append(("jo avatar b1arm2_n",37,245))
        if "_blazer" in state:
          rv.append(("jo avatar b1blazer",109,319))
          rv.append(("jo avatar b1arm2_c",34,245))
        elif "_shirt" in state:
          rv.append(("jo avatar b1shirt",107,406))
        if "_coat" in state:
          rv.append(("jo avatar b1coat",124,383))
          if "_tie" in state:
            rv.append(("jo avatar b1tie_coat",18,285))
          else:
            rv.append(("jo avatar b1arm2_coat",33,245))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((602,1080))
        rv.append(("jo avatar body1",110,124))
        rv.append(("jo avatar face_smile",226,241))
        if "_glasses" in state:
          rv.append(("jo avatar b1glasses",224,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b1white_panties",189,761))
        elif "_panties" in state:
          rv.append(("jo avatar b1panty",188,747))
        if "_white_bra" in state:
          rv.append(("jo avatar b1white_bra",109,408))
        elif "_bra" in state:
          rv.append(("jo avatar b1bra",110,401))
        if "_skirt" in state:
          rv.append(("jo avatar b1stocking",188,703))
          rv.append(("jo avatar b1skirt",181,734))
        elif "_pants" in state:
          if "_tie" in state:
            rv.append(("jo avatar b1tie_n",18,285))
          else:
            rv.append(("jo avatar b1arm1_n",16,410))
          rv.append(("jo avatar b1pants",186,714))
        if "_cup" in state:
          rv.append(("jo avatar b1tea1_n",43,247))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1tea1_c",37,247))
        else:
          if "_pants" not in state:
            rv.append(("jo avatar b1arm1_n",16,410))
            if "_skirt" in state:
              rv.append(("jo avatar b1skirt_fix",181,783))
          if "_blazer" in state:
            rv.append(("jo avatar b1blazer",109,319))
            rv.append(("jo avatar b1arm1_c",11,410))
          elif "_shirt" in state:
            rv.append(("jo avatar b1shirt",107,406))
          if "_coat" in state:
            rv.append(("jo avatar b1coat",124,383))
            if "_tie" in state:
              rv.append(("jo avatar b1tie_coat",18,285))
            else:
              rv.append(("jo avatar b1arm1_coat",9,459))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body2",89,129))
        rv.append(("jo avatar face_afraid",191,234))
        if "_glasses" in state:
          rv.append(("jo avatar b2glasses",192,272))
        if "_white_panties" in state:
          rv.append(("jo avatar b2white_panties",119,804))
        elif "_panties" in state:
          rv.append(("jo avatar b2panty",121,779))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b2bikini_bottom",116,814))
        if "_white_bra" in state:
          rv.append(("jo avatar b2white_bra",112,406))
        elif "_bra" in state:
          rv.append(("jo avatar b2bra",113,401))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b2bikini_top",120,405))
        if "_skirt" in state:
          rv.append(("jo avatar b2stocking",105,742))
          rv.append(("jo avatar b2skirt",101,766))
        elif "_pants" in state:
          rv.append(("jo avatar b2pants",100,740))
        if "_hands_to_the_side" in state:
          rv.append(("jo avatar b2arm1_n",359,385))
        else:
          rv.append(("jo avatar b2arm2_n",288,312))
        if "_blazer" in state:
          rv.append(("jo avatar b2blazer",84,386))
          rv.append(("jo avatar b2arm2_c",288,311))
        elif "_shirt" in state:
          rv.append(("jo avatar b2shirt",112,403))
        if "_coat" in state:
          rv.append(("jo avatar b2coat",79,389))
          if "_hands_to_the_side" in state:
            rv.append(("jo avatar b2arm1_coat",346,385))
          else:
            rv.append(("jo avatar b2arm2_coat",288,312))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body2",89,129))
        rv.append(("jo avatar face_blush",186,234))
        if "_glasses" in state:
          rv.append(("jo avatar b2glasses",192,272))
        if "_white_panties" in state:
          rv.append(("jo avatar b2white_panties",119,804))
        elif "_panties" in state:
          rv.append(("jo avatar b2panty",121,779))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b2bikini_bottom",116,814))
        if "_white_bra" in state:
          rv.append(("jo avatar b2white_bra",112,406))
        elif "_bra" in state:
          rv.append(("jo avatar b2bra",113,401))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b2bikini_top",120,405))
        if "_skirt" in state:
          rv.append(("jo avatar b2stocking",105,742))
          rv.append(("jo avatar b2skirt",101,766))
        elif "_pants" in state:
          rv.append(("jo avatar b2pants",100,740))
        rv.append(("jo avatar b2arm2_n",288,312))
        if "_blazer" in state:
          rv.append(("jo avatar b2blazer",84,386))
          rv.append(("jo avatar b2arm2_c",288,311))
        elif "_shirt" in state:
          rv.append(("jo avatar b2shirt",112,403))
        if "_coat" in state:
          rv.append(("jo avatar b2coat",79,389))
          rv.append(("jo avatar b2arm2_coat",288,312))
        if "_bag" in state:
          rv.append(("jo avatar b2bag",260,732))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body2",89,129))
        rv.append(("jo avatar face_eyeroll",185,236))
        if "_glasses" in state:
          rv.append(("jo avatar b2glasses",192,272))
        if "_white_panties" in state:
          rv.append(("jo avatar b2white_panties",119,804))
        elif "_panties" in state:
          rv.append(("jo avatar b2panty",121,779))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b2bikini_bottom",116,814))
        if "_white_bra" in state:
          rv.append(("jo avatar b2white_bra",112,406))
        elif "_bra" in state:
          rv.append(("jo avatar b2bra",113,401))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b2bikini_top",120,405))
        if "_skirt" in state:
          rv.append(("jo avatar b2stocking",105,742))
          rv.append(("jo avatar b2skirt",101,766))
        elif "_pants" in state:
          rv.append(("jo avatar b2pants",100,740))
        rv.append(("jo avatar b2arm1_n",359,385))
        if "_blazer" in state:
          rv.append(("jo avatar b2blazer",84,386))
          rv.append(("jo avatar b2arm1_c",349,385))
        elif "_shirt" in state:
          rv.append(("jo avatar b2shirt",112,403))
        if "_coat" in state:
          rv.append(("jo avatar b2coat",79,389))
          rv.append(("jo avatar b2arm1_coat",346,385))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body2",89,129))
        rv.append(("jo avatar face_flirty",189,235))
        if "_glasses" in state:
          rv.append(("jo avatar b2glasses",192,272))
        if "_white_panties" in state:
          rv.append(("jo avatar b2white_panties",119,804))
        elif "_panties" in state:
          rv.append(("jo avatar b2panty",121,779))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b2bikini_bottom",116,814))
        if "_white_bra" in state:
          rv.append(("jo avatar b2white_bra",112,406))
        elif "_bra" in state:
          rv.append(("jo avatar b2bra",113,401))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b2bikini_top",120,405))
        if "_skirt" in state:
          rv.append(("jo avatar b2stocking",105,742))
          rv.append(("jo avatar b2skirt",101,766))
        elif "_pants" in state:
          rv.append(("jo avatar b2pants",100,740))
        if "_hands_to_the_side" in state:
          rv.append(("jo avatar b2arm1_n",359,385))
        else:
          rv.append(("jo avatar b2arm2_n",288,312))
        if "_blazer" in state:
          rv.append(("jo avatar b2blazer",84,386))
          if "_hands_to_the_side" in state:
            rv.append(("jo avatar b2arm1_c",349,385))
          else:
            rv.append(("jo avatar b2arm2_c",288,311))
        elif "_shirt" in state:
          rv.append(("jo avatar b2shirt",112,403))
        if "_coat" in state:
          rv.append(("jo avatar b2coat",79,389))
          if "_hands_to_the_side" in state:
            rv.append(("jo avatar b2arm1_coat",346,385))
          else:
            rv.append(("jo avatar b2arm2_coat",288,312))
        if "_bag" in state:
          rv.append(("jo avatar b2bag",260,732))

      elif state.startswith("sarcastic"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body2",89,129))
        rv.append(("jo avatar face_sarcastic",189,234))
        if "_glasses" in state:
          rv.append(("jo avatar b2glasses",192,272))
        if "_white_panties" in state:
          rv.append(("jo avatar b2white_panties",119,804))
        elif "_panties" in state:
          rv.append(("jo avatar b2panty",121,779))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b2bikini_bottom",116,814))
        if "_white_bra" in state:
          rv.append(("jo avatar b2white_bra",112,406))
        elif "_bra" in state:
          rv.append(("jo avatar b2bra",113,401))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b2bikini_top",120,405))
        if "_skirt" in state:
          rv.append(("jo avatar b2stocking",105,742))
          rv.append(("jo avatar b2skirt",101,766))
        elif "_pants" in state:
          rv.append(("jo avatar b2pants",100,740))
        if "_hands_to_the_side" in state or "_bag" in state:
          rv.append(("jo avatar b2arm1_n",359,385))
        else:
          rv.append(("jo avatar b2arm2_n",288,312))
        if "_blazer" in state:
          rv.append(("jo avatar b2blazer",84,386))
          if "_hands_to_the_side" in state:
            rv.append(("jo avatar b2arm1_c",349,385))
          else:
            rv.append(("jo avatar b2arm2_c",288,311))
        elif "_shirt" in state:
          rv.append(("jo avatar b2shirt",112,403))
        if "_coat" in state:
          rv.append(("jo avatar b2coat",79,389))
          if "_bag" in state:
            rv.append(("jo avatar b2arm1_coat",346,385))
          else:
            rv.append(("jo avatar b2arm2_coat",288,312))
        if "_bag" in state:
          rv.append(("jo avatar b2bag",260,732))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body3",35,119))
        rv.append(("jo avatar face_cringe",251,215))
        if "_glasses" in state:
          rv.append(("jo avatar b3glasses",232,245))
        if "_white_panties" in state:
          rv.append(("jo avatar b3white_panties",205,743))
        elif "_panties" in state:
          rv.append(("jo avatar b3panty",204,728))
        if "_white_bra" in state:
          rv.append(("jo avatar b3white_bra",108,394))
        elif "_bra" in state:
          rv.append(("jo avatar b3bra",111,393))
        if "_skirt" in state:
          rv.append(("jo avatar b3stocking",186,726))
          rv.append(("jo avatar b3skirt",181,745))
        elif "_pants" in state:
          rv.append(("jo avatar b3pants",184,696))
        rv.append(("jo avatar b3arm2",127,133))
        if "_blazer" in state:
          rv.append(("jo avatar b3blazer",35,249))
        elif "_shirt" in state:
          rv.append(("jo avatar b3shirt",106,391))
        if "_coat" in state:
          rv.append(("jo avatar b3coat",29,247))

      elif state.startswith("displeased"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body3",35,119))
        rv.append(("jo avatar face_displeased",251,215))
        if "_glasses" in state:
          rv.append(("jo avatar b3glasses",232,245))
        if "_white_panties" in state:
          rv.append(("jo avatar b3white_panties",205,743))
        elif "_panties" in state:
          rv.append(("jo avatar b3panty",204,728))
        if "_white_bra" in state:
          rv.append(("jo avatar b3white_bra",108,394))
        elif "_bra" in state:
          rv.append(("jo avatar b3bra",111,393))
        if "_skirt" in state:
          rv.append(("jo avatar b3stocking",186,726))
          rv.append(("jo avatar b3skirt",181,745))
        elif "_pants" in state:
          rv.append(("jo avatar b3pants",184,696))
        if "_cup" in state:
          rv.append(("jo avatar b3cup",49,120))
        else:
          rv.append(("jo avatar b3arm2",127,133))
        if "_blazer" in state:
          rv.append(("jo avatar b3blazer",35,249))
        elif "_shirt" in state:
          rv.append(("jo avatar b3shirt",106,391))
        if "_coat" in state:
          rv.append(("jo avatar b3coat",29,247))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body3",35,119))
        rv.append(("jo avatar face_excited",248,215))
        if "_glasses" in state:
          rv.append(("jo avatar b3glasses",232,245))
        if "_white_panties" in state:
          rv.append(("jo avatar b3white_panties",205,743))
        elif "_panties" in state:
          rv.append(("jo avatar b3panty",204,728))
        if "_white_bra" in state:
          rv.append(("jo avatar b3white_bra",108,394))
        elif "_bra" in state:
          rv.append(("jo avatar b3bra",111,393))
        if "_skirt" in state:
          rv.append(("jo avatar b3stocking",186,726))
          rv.append(("jo avatar b3skirt",181,745))
        elif "_pants" in state:
          rv.append(("jo avatar b3pants",184,696))
        if "_cup" in state:
          rv.append(("jo avatar b3cup",49,120))
        else:
          rv.append(("jo avatar b3arm1",140,132))
        if "_blazer" in state:
          rv.append(("jo avatar b3blazer",35,249))
        elif "_shirt" in state:
          rv.append(("jo avatar b3shirt",106,391))
        if "_coat" in state:
          rv.append(("jo avatar b3coat",29,247))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body3",35,119))
        rv.append(("jo avatar face_laughing",244,215))
        if "_glasses" in state:
          rv.append(("jo avatar b3glasses",232,245))
        if "_white_panties" in state:
          rv.append(("jo avatar b3white_panties",205,743))
        elif "_panties" in state:
          rv.append(("jo avatar b3panty",204,728))
        if "_white_bra" in state:
          rv.append(("jo avatar b3white_bra",108,394))
        elif "_bra" in state:
          rv.append(("jo avatar b3bra",111,393))
        if "_skirt" in state:
          rv.append(("jo avatar b3stocking",186,726))
          rv.append(("jo avatar b3skirt",181,745))
        elif "_pants" in state:
          rv.append(("jo avatar b3pants",184,696))
        if "_cup" in state:
          rv.append(("jo avatar b3cup",49,120))
        else:
          rv.append(("jo avatar b3arm1",140,132))
        if "_blazer" in state:
          rv.append(("jo avatar b3blazer",35,249))
        elif "_shirt" in state:
          rv.append(("jo avatar b3shirt",106,391))
        if "_coat" in state:
          rv.append(("jo avatar b3coat",29,247))

      elif state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body4",101,123))
        rv.append(("jo avatar face_angry",257,239))
        if "_glasses" in state:
          rv.append(("jo avatar b4glasses",247,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b4white_panties",131,764))
        elif "_panties" in state:
          rv.append(("jo avatar b4panty",130,747))
        if "_white_bra" in state:
          rv.append(("jo avatar b4white_bra",101,401))
        elif "_bra" in state:
          rv.append(("jo avatar b4bra",101,400))
        if "_skirt" in state:
          rv.append(("jo avatar b4stocking",105,734))
          rv.append(("jo avatar b4skirt",105,766))
        elif "_pants" in state:
          rv.append(("jo avatar b4pants",105,709))
        rv.append(("jo avatar b4arm1_n",88,395))
        if "_blazer" in state:
          rv.append(("jo avatar b4blazer",101,380))
          rv.append(("jo avatar b4arm1_c",88,394))
        elif "_shirt" in state:
          rv.append(("jo avatar b4shirt",100,394))
        if "_coat" in state:
          rv.append(("jo avatar b4coat",72,387))
          rv.append(("jo avatar b4arm1_coat",87,496))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body4",101,123))
        rv.append(("jo avatar face_annoyed",252,245))
        if "_glasses" in state:
          rv.append(("jo avatar b4glasses",247,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b4white_panties",131,764))
        elif "_panties" in state:
          rv.append(("jo avatar b4panty",130,747))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b4bikini_bottom",127,792))
        if "_white_bra" in state:
          rv.append(("jo avatar b4white_bra",101,401))
        elif "_bra" in state:
          rv.append(("jo avatar b4bra",101,400))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b4bikini_top",101,401))
        if "_skirt" in state:
          rv.append(("jo avatar b4stocking",105,734))
          rv.append(("jo avatar b4skirt",105,766))
        elif "_pants" in state:
          rv.append(("jo avatar b4pants",105,709))
        if "_cupless" in state:
          rv.append(("jo avatar b4cupless_n",23,377))
        elif "_cup" in state:
          rv.append(("jo avatar b4cup_n",23,366))
        elif "_crossed_arms" in state:
          rv.append(("jo avatar b4arm1_n",88,395))
        else:
          rv.append(("jo avatar b4arm2_n",23,319))
        if "_blazer" in state:
          rv.append(("jo avatar b4blazer",101,380))
          if "_cupless" in state:
            rv.append(("jo avatar b4cupless_c",23,377))
          elif "_cup" in state:
            rv.append(("jo avatar b4cup_c",23,366))
          elif "_crossed_arms" in state:
            rv.append(("jo avatar b4arm1_c",88,394))
          else:
            rv.append(("jo avatar b4arm2_c",23,319))
        elif "_shirt" in state:
          rv.append(("jo avatar b4shirt",100,394))
        if "_coat" in state:
          rv.append(("jo avatar b4coat",72,387))
          rv.append(("jo avatar b4arm2_coat",23,319))

      elif state.startswith("embarrassed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body4",101,123))
        rv.append(("jo avatar face_embarrassed",223,206))
        if "_glasses" in state:
          rv.append(("jo avatar b4glasses",247,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b4white_panties",131,764))
        elif "_panties" in state:
          rv.append(("jo avatar b4panty",130,747))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b4bikini_bottom",127,792))
        if "_white_bra" in state:
          rv.append(("jo avatar b4white_bra",101,401))
        elif "_bra" in state:
          rv.append(("jo avatar b4bra",101,400))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b4bikini_top",101,401))
        if "_skirt" in state:
          rv.append(("jo avatar b4stocking",105,734))
          rv.append(("jo avatar b4skirt",105,766))
        elif "_pants" in state:
          rv.append(("jo avatar b4pants",105,709))
        rv.append(("jo avatar b4arm2_n",23,319))
        if "_blazer" in state:
          rv.append(("jo avatar b4blazer",101,380))
          rv.append(("jo avatar b4arm2_c",23,319))
        elif "_shirt" in state:
          rv.append(("jo avatar b4shirt",100,394))
        if "_coat" in state:
          rv.append(("jo avatar b4coat",72,387))
          rv.append(("jo avatar b4arm2_coat",23,319))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body4",101,123))
        rv.append(("jo avatar face_sad",252,246))
        if "_glasses" in state:
          rv.append(("jo avatar b4glasses",247,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b4white_panties",131,764))
        elif "_panties" in state:
          rv.append(("jo avatar b4panty",130,747))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b4bikini_bottom",127,792))
        if "_white_bra" in state:
          rv.append(("jo avatar b4white_bra",101,401))
        elif "_bra" in state:
          rv.append(("jo avatar b4bra",101,400))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b4bikini_top",101,401))
        if "_skirt" in state:
          rv.append(("jo avatar b4stocking",105,734))
          rv.append(("jo avatar b4skirt",105,766))
        elif "_pants" in state:
          rv.append(("jo avatar b4pants",105,709))
        rv.append(("jo avatar b4arm1_n",88,395))
        if "_blazer" in state:
          rv.append(("jo avatar b4blazer",101,380))
          rv.append(("jo avatar b4arm1_c",88,394))
        elif "_shirt" in state:
          rv.append(("jo avatar b4shirt",100,394))
        if "_coat" in state:
          rv.append(("jo avatar b4coat",72,387))
          rv.append(("jo avatar b4arm1_coat",87,496))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body4",101,123))
        rv.append(("jo avatar face_thinking",252,239))
        if "_glasses" in state:
          rv.append(("jo avatar b4glasses",247,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b4white_panties",131,764))
        elif "_panties" in state:
          rv.append(("jo avatar b4panty",130,747))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b4bikini_bottom",127,792))
        if "_white_bra" in state:
          rv.append(("jo avatar b4white_bra",101,401))
        elif "_bra" in state:
          rv.append(("jo avatar b4bra",101,400))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b4bikini_top",101,401))
        if "_skirt" in state:
          rv.append(("jo avatar b4stocking",105,734))
          rv.append(("jo avatar b4skirt",105,766))
        elif "_pants" in state:
          rv.append(("jo avatar b4pants",105,709))
        if "_phone" in state:
          rv.append(("jo avatar b4arm2_n_phone",23,319))
        else:
          rv.append(("jo avatar b4arm2_n",23,319))
        if "_blazer" in state:
          rv.append(("jo avatar b4blazer",101,380))
        if "_phone" in state:
          rv.append(("jo avatar b4arm2_c_phone",23,319))
        elif "_blazer" in state:
          rv.append(("jo avatar b4arm2_c",23,319))
        elif "_shirt" in state:
          rv.append(("jo avatar b4shirt",100,394))
        if "_coat" in state:
          rv.append(("jo avatar b4coat",72,387))
          rv.append(("jo avatar b4arm2_coat",23,319))
        if "_bag" in state:
          rv.append(("jo avatar b4bag",-36,638))

      elif state.startswith("worried"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jo avatar body4",101,123))
        rv.append(("jo avatar face_worried",252,241))
        if "_glasses" in state:
          rv.append(("jo avatar b4glasses",247,277))
        if "_white_panties" in state:
          rv.append(("jo avatar b4white_panties",131,764))
        elif "_panties" in state:
          rv.append(("jo avatar b4panty",130,747))
        elif "_bikini_bottom" in state:
          rv.append(("jo avatar b4bikini_bottom",127,792))
        if "_white_bra" in state:
          rv.append(("jo avatar b4white_bra",101,401))
        elif "_bra" in state:
          rv.append(("jo avatar b4bra",101,400))
        elif "_bikini_top" in state:
          rv.append(("jo avatar b4bikini_top",101,401))
        if "_skirt" in state:
          rv.append(("jo avatar b4stocking",105,734))
          rv.append(("jo avatar b4skirt",105,766))
        elif "_pants" in state:
          rv.append(("jo avatar b4pants",105,709))
        if "_cup" in state:
          rv.append(("jo avatar b4cup_n",23,366))
        else:
          rv.append(("jo avatar b4arm2_n",23,319))
        if "_blazer" in state:
          rv.append(("jo avatar b4blazer",101,380))
          if "_cup" in state:
            rv.append(("jo avatar b4cup_c",23,366))
          else:
            rv.append(("jo avatar b4arm2_c",23,319))
        elif "_shirt" in state:
          rv.append(("jo avatar b4shirt",100,394))
        if "_coat" in state:
          rv.append(("jo avatar b4coat",72,387))
          rv.append(("jo avatar b4arm2_coat",23,319))
        if "_bag" in state:
          rv.append(("jo avatar b4bag",-36,638))

      elif state.startswith("JoMorningKiss"):
        rv.append((1920,1080))
        if "_repeat" in state:
          rv.append(("jo avatar events morningkiss JoMorningKiss_BG_alt",0,0))
        else:
          rv.append(("jo avatar events morningkiss JoMorningKiss_BG",0,0))
        rv.append(("jo avatar events morningkiss JoMorningKiss_Body",728,77))
        rv.append(("jo avatar events morningkiss JoMorningKiss_Panty",755,797))
        if "_repeat" in state and jo.equipped_item("jo_bra"):
          rv.append(("jo avatar events morningkiss JoMorningKiss_Bra",758,383))
        rv.append(("jo avatar events morningkiss JoMorningKiss_c",727,361))
        rv.append(("jo avatar events morningkiss JoMorningKiss_Glasses",782,267))

      elif state=="drugged":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty1",771,578))
        rv.append(("jo avatar events drugged JoDrugged_Sleepy",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_sleepy",701,578))
        rv.append(("jo avatar events drugged JoDrugged_Skirt",699,577))
      elif state=="drugged_sleepy":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty1",771,578))
        rv.append(("jo avatar events drugged JoDrugged_Sleepy",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_sleepy",701,578))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_sleepy",705,444))
      elif state=="drugged_ripped":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty1",771,578))
        rv.append(("jo avatar events drugged JoDrugged_Sleepy",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
      elif state=="drugged_ripped_hand":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty1",771,578))
        rv.append(("jo avatar events drugged JoDrugged_Sleepy",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_rippedhand",699,486))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
      elif state=="drugged_scratch0":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty1",771,578))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch1",603,697))
      elif state=="drugged_scratch1":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty2",771,578))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch1",603,697))
      elif state=="drugged_scratch15":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty1",771,578))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch2",599,744))
      elif state=="drugged_scratch2":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty2",771,578))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch2",599,744))
      elif state=="drugged_scratch3":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty2",771,578))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch3",599,744))
      elif state=="drugged_scratch4":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty2",771,578))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch4",599,744))
      elif state=="drugged_pantypull":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace1",746,63))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Pantypull",1144,780))
      elif state=="drugged_tongue1":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace2",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Tongue1",1127,875))
      elif state=="drugged_tongue2":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace2",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Tongue2",1109,899))
      elif state=="drugged_tongue3":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_ScratchFace2",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Tongue3",1113,913))
      elif state=="drugged_tongueass1":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_TongueFace",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Tongueass1",1115,689))
      elif state=="drugged_tongueass2":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_TongueFace",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Tongueass2",1096,711))
      elif state=="drugged_tongueass3":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_TongueFace",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Tongueass3",1101,726))
      elif state=="drugged_orgasm":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Orgasm",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Scratch4",599,744))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
      elif state=="drugged_stillitchy":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_StillItchy",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
      elif state=="drugged_brush1":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Brush1Face",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
        rv.append(("jo avatar events drugged JoDrugged_Brush1",1121,664))
      elif state=="drugged_brush2":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Brush1Face",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
        rv.append(("jo avatar events drugged JoDrugged_Brush2",1125,709))
      elif state=="drugged_brush3":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Brush1Face",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
        rv.append(("jo avatar events drugged JoDrugged_Brush3",1111,663))
      elif state=="drugged_insert1":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Insert",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
        rv.append(("jo avatar events drugged JoDrugged_Brushinsert1",895,642))
      elif state=="drugged_insert2":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Insert",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
        rv.append(("jo avatar events drugged JoDrugged_Brushinsert2",888,613))
      elif state=="drugged_insert3":
        rv.append((1920,1080))
        rv.append(("jo avatar events drugged JoDrugged_BG",0,0))
        rv.append(("jo avatar events drugged JoDrugged_Body",394,0))
        rv.append(("jo avatar events drugged JoDrugged_Panty3",1027,581))
        rv.append(("jo avatar events drugged JoDrugged_Insert",711,32))
        rv.append(("jo avatar events drugged JoDrugged_Glasses",781,113))
        rv.append(("jo avatar events drugged JoDrugged_Pantyhose_ripped",701,582))
        rv.append(("jo avatar events drugged JoDrugged_Skirt_ripped",705,572))
        rv.append(("jo avatar events drugged JoDrugged_Juice",1042,693))
        rv.append(("jo avatar events drugged JoDrugged_Brushinsert3",858,663))

      elif state.startswith("under_desk"):
        rv.append((1920,1080))
        rv.append(("jo avatar events under_desk bg",0,0))
        if "_closed_legs" in state or "_half_open_legs" in state or "open_legs" in state:
          rv.append(("jo avatar events under_desk body1_n",574,146))
          rv.append(("jo avatar events under_desk b1bra",783,160))
          rv.append(("jo avatar events under_desk body1_c",670,149))
          if "_closed_legs" in state:
            rv.append(("jo avatar events under_desk b1leg1_n",172,266))
            rv.append(("jo avatar events under_desk b1leg1_c",149,267))
          elif "_half_open_legs" in state:
            rv.append(("jo avatar events under_desk b1leg2_n",580,403))
            rv.append(("jo avatar events under_desk b1panties",883,452))
            rv.append(("jo avatar events under_desk b1leg2_c",596,397))
          elif "open_legs" in state:
            rv.append(("jo avatar events under_desk b1leg3_n",422,394))
            rv.append(("jo avatar events under_desk b1panties2",866,453))
            rv.append(("jo avatar events under_desk b1leg3_c",388,347))
        elif "_look" in state:
          rv.append(("jo avatar events under_desk body3_n",718,109))
          rv.append(("jo avatar events under_desk b3bra",964,171))
          rv.append(("jo avatar events under_desk body3_c",718,109))
          rv.append(("jo avatar events under_desk b3leg_n",648,394))
          if "_ripped_panties" in state:
            rv.append(("jo avatar events under_desk b3panties2",952,480))
            rv.append(("jo avatar events under_desk b3leg2_c",648,390))
          else:
            rv.append(("jo avatar events under_desk b3panties1",941,479))
            rv.append(("jo avatar events under_desk b3leg1_c",648,390))
        elif "_grab_stockings" in state or "_ripped_stockings" in state or "_grab_panties" in state or "_ripped_panties" in state or "_grab_pussy" in state:
          rv.append(("jo avatar events under_desk body2_n",575,156))
          rv.append(("jo avatar events under_desk b2bra",783,174))
          rv.append(("jo avatar events under_desk body2_c",670,169))
          rv.append(("jo avatar events under_desk b2leg1_n",458,308))
          if "_grab_stockings" in state:
            rv.append(("jo avatar events under_desk b2panties1",887,444))
            rv.append(("jo avatar events under_desk b2leg1_c1",458,252))
          elif "_ripped_stockings" in state:
            rv.append(("jo avatar events under_desk b2panties1",887,444))
            rv.append(("jo avatar events under_desk b2leg1_c2",458,308))
          elif "_grab_panties" in state:
            rv.append(("jo avatar events under_desk b2panties1",887,444))
            rv.append(("jo avatar events under_desk b2leg1_c3",458,252))
          elif "_ripped_panties" in state:
            rv.append(("jo avatar events under_desk b2panties2",912,425))
            rv.append(("jo avatar events under_desk b2leg1_c3",458,252))
            rv.append(("jo avatar events under_desk hand2",1010,529))
          elif "_grab_pussy" in state:
            rv.append(("jo avatar events under_desk b2panties3",912,425))
            rv.append(("jo avatar events under_desk b2leg1_c3",458,252))
          rv.append(("jo avatar events under_desk hand1",612,497))
        elif "_tongue" in state or "_orgasm" in state:
          rv.append(("jo avatar events under_desk body2_n",575,156))
          rv.append(("jo avatar events under_desk b2bra",783,174))
          rv.append(("jo avatar events under_desk body2_c",670,169))
          if "_tongue" in state:
            rv.append(("jo avatar events under_desk b2leg2_n",458,307))
            rv.append(("jo avatar events under_desk b2panties4",912,427))
            rv.append(("jo avatar events under_desk b2leg2_c",407,307))
            rv.append(("jo avatar events under_desk hand3",627,484))
            if "_tongue1" in state:
              rv.append(("jo avatar events under_desk tongue1",951,576))
            elif "_tongue2" in state:
              rv.append(("jo avatar events under_desk tongue2",935,551))
            elif "_tongue3" in state:
              rv.append(("jo avatar events under_desk tongue3",969,498))
            elif "_tongue4" in state:
              rv.append(("jo avatar events under_desk tongue4",976,470))
            elif "_tongue5" in state:
              rv.append(("jo avatar events under_desk tongue5",976,465))
          elif "_orgasm" in state:
            rv.append(("jo avatar events under_desk b2leg3_n",216,369))
            rv.append(("jo avatar events under_desk b2panties5",912,419))
            rv.append(("jo avatar events under_desk b2leg3_c",216,368))
            if not "_orgasm2" in state:
              rv.append(("jo avatar events under_desk hand4",523,541))
              rv.append(("jo avatar events under_desk tongue5",976,465))
        rv.append(("jo avatar events under_desk table",0,0))
        if "_maya" in state:
          rv.append(("jo avatar events under_desk bg_dark",0,0))
          rv.append(("jo avatar events under_desk maya",510,103))

      elif state.startswith("broken_cup"):
        rv.append((1920,1080))
        rv.append(("jo avatar events broken_cup background",0,0))
        rv.append(("jo avatar events broken_cup jo_body",681,296))
        rv.append(("jo avatar events broken_cup jo_underwear",908,361))
        rv.append(("jo avatar events broken_cup jo_bottoms",681,607))
        rv.append(("jo avatar events broken_cup jo_arms",798,372))
        rv.append(("jo avatar events broken_cup jo_blazer",832,342))
        if state.endswith("sad"):
          rv.append(("jo avatar events broken_cup jo_head1",894,118))
        elif state.endswith("cringe"):
          rv.append(("jo avatar events broken_cup jo_head2",894,118))
        elif state.endswith("angry"):
          rv.append(("jo avatar events broken_cup jo_head3",894,118))
        rv.append(("jo avatar events broken_cup jo_glasses",910,265))
        rv.append(("jo avatar events broken_cup coffee",552,928))

      elif state.startswith("picnic"):
        rv.append((1920,1080))
        rv.append(("jo avatar events picnic background",0,0))
        rv.append(("jo avatar events picnic jo_body",327,272))
        if state.endswith(("unbuttoning_shirt","removing_shirt")):
          rv.append(("jo avatar events picnic glass1",533,682))
          rv.append(("jo avatar events picnic jo_boobs1",869,414))
        elif state.endswith(("removing_bra","resting_hands","squeezing_boobs","resting_hands_bra_on")):
          rv.append(("jo avatar events picnic glass2",533,682))
          if state.endswith("squeezing_boobs"):
            rv.append(("jo avatar events picnic jo_boobs2",876,414))
        if "neutral_looking_down" in state:
          rv.append(("jo avatar events picnic jo_head1",889,26))
        elif "surprised_looking_down" in state:
          rv.append(("jo avatar events picnic jo_head2",889,26))
        elif "surprised" in state:
          rv.append(("jo avatar events picnic jo_head3",889,26))
        elif "blush_smile" in state:
          rv.append(("jo avatar events picnic jo_head4",889,26))
        elif "smile" in state:
          rv.append(("jo avatar events picnic jo_head5",889,26))
        elif "blush_flirty" in state:
          rv.append(("jo avatar events picnic jo_head6",889,26))
        elif "flirty" in state:
          rv.append(("jo avatar events picnic jo_head7",889,26))
        rv.append(("jo avatar events picnic jo_glasses",912,171))
        rv.append(("jo avatar events picnic jo_panties",818,680))
        rv.append(("jo avatar events picnic jo_bottoms",311,673))
        if state.endswith(("unbuttoning_shirt","removing_shirt")):
          rv.append(("jo avatar events picnic jo_bra1",869,322))
          if state.endswith("unbuttoning_shirt"):
            rv.append(("jo avatar events picnic jo_blazer1",802,303))
          elif state.endswith("removing_shirt"):
            rv.append(("jo avatar events picnic jo_blazer2",802,316))
        if state.endswith(("holding_wine","holding_wine_shirtless")):
          rv.append(("jo avatar events picnic jo_arms1",683,335))
        elif state.endswith("offering_wine"):
          rv.append(("jo avatar events picnic jo_arms2",717,335))
        elif state.endswith("spilling_wine"):
          rv.append(("jo avatar events picnic jo_arms3",741,335))
        elif state.endswith("unbuttoning_shirt"):
          rv.append(("jo avatar events picnic jo_arms4",744,335))
        elif state.endswith("removing_shirt"):
          rv.append(("jo avatar events picnic jo_arms5",744,335))
        elif state.endswith("pouring_wine"):
          rv.append(("jo avatar events picnic jo_arms6",666,27))
        elif state.endswith("removing_bra"):
          rv.append(("jo avatar events picnic jo_arms7",679,335))
        elif state.endswith(("resting_hands","resting_hands_bra_on")):
          rv.append(("jo avatar events picnic jo_arms8",679,335))
        elif state.endswith("squeezing_boobs"):
          rv.append(("jo avatar events picnic jo_arms9",748,335))
        if state.endswith(("holding_wine","offering_wine","spilling_wine","holding_wine_shirtless","pouring_wine","removing_bra","resting_hands","resting_hands_bra_on")):
          rv.append(("jo avatar events picnic jo_boobs1",869,414))
        if state.endswith(("holding_wine","offering_wine","spilling_wine","holding_wine_shirtless")):
          rv.append(("jo avatar events picnic jo_bra1",869,322))
        elif state.endswith(("pouring_wine","resting_hands_bra_on")):
          rv.append(("jo avatar events picnic jo_bra2",869,322))
        elif state.endswith("removing_bra"):
          rv.append(("jo avatar events picnic jo_bra3",869,324))
        if state.endswith(("holding_wine","offering_wine")):
          rv.append(("jo avatar events picnic jo_blazer3",802,303))
        elif state.endswith("spilling_wine"):
          rv.append(("jo avatar events picnic jo_blazer4",802,303))
        if state.endswith("holding_wine"):
          rv.append(("jo avatar events picnic jo_sleeves1",722,334))
        elif state.endswith("offering_wine"):
          rv.append(("jo avatar events picnic jo_sleeves2",763,334))
        elif state.endswith("spilling_wine"):
          rv.append(("jo avatar events picnic jo_sleeves3",741,334))
        elif state.endswith("unbuttoning_shirt"):
          rv.append(("jo avatar events picnic jo_sleeves4",743,332))
        elif state.endswith("removing_shirt"):
          rv.append(("jo avatar events picnic jo_sleeves5",743,345))
        if state.endswith(("holding_wine","holding_wine_shirtless")):
          rv.append(("jo avatar events picnic glass3",715,323))
        elif state.endswith("offering_wine"):
          rv.append(("jo avatar events picnic glass4",724,373))
        elif state.endswith("spilling_wine"):
          rv.append(("jo avatar events picnic glass5",723,340))

      elif state.startswith("boobjob"):
        rv.append((1920,1080))
        rv.append(("jo avatar events boobjob background",0,0))
        if state.endswith(("lying_down","aftermath")):
          rv.append(("jo avatar events boobjob jo_body1",683,347))
        elif state.endswith(("lifting_boob","dick_reveal","grabbing_dick")):
          rv.append(("jo avatar events boobjob jo_body2",683,347))
        elif state.endswith("dick_bottom"):
          rv.append(("jo avatar events boobjob jo_body3",683,347))
        elif state.endswith("dick_middle"):
          rv.append(("jo avatar events boobjob jo_body4",683,347))
        elif state.endswith(("dick_top","cum")):
          rv.append(("jo avatar events boobjob jo_body5",683,347))
        if "surprised_looking_down" in state:
          rv.append(("jo avatar events boobjob jo_head1",759,42))
        elif "flirty_looking_down" in state:
          rv.append(("jo avatar events boobjob jo_head2",759,42))
        elif "flirty" in state:
          rv.append(("jo avatar events boobjob jo_head3",759,42))
        elif "smile" in state:
          rv.append(("jo avatar events boobjob jo_head4",759,42))
        elif "laughing" in state:
          rv.append(("jo avatar events boobjob jo_head5",759,42))
        elif "sad" in state:
          rv.append(("jo avatar events boobjob jo_head6",759,42))
        elif "concerned" in state:
          rv.append(("jo avatar events boobjob jo_head7",759,42))
        elif "drunk" in state:
          rv.append(("jo avatar events boobjob jo_head8",759,42))
        rv.append(("jo avatar events boobjob jo_glasses",789,214))
        if state.endswith(("dick_reveal","grabbing_dick")):
          rv.append(("jo avatar events boobjob mc_dick1",386,700))
        elif state.endswith("dick_bottom"):
          rv.append(("jo avatar events boobjob mc_dick2",893,741))
        elif state.endswith("dick_middle"):
          rv.append(("jo avatar events boobjob mc_dick3",893,678))
        elif state.endswith(("dick_top","cum")):
          if state.endswith("cum"):
            rv.append(("jo avatar events boobjob cum1",848,177))
          rv.append(("jo avatar events boobjob mc_dick4",892,568))
        elif state.endswith("aftermath"):
          rv.append(("jo avatar events boobjob cum2",848,177))
          rv.append(("jo avatar events boobjob mc_dick5",851,641))
        if state.endswith("lying_down"):
          rv.append(("jo avatar events boobjob jo_arms1",469,489))
        elif state.endswith(("lifting_boob","dick_reveal")):
          rv.append(("jo avatar events boobjob jo_arms2",469,489))
        elif state.endswith("grabbing_dick"):
          rv.append(("jo avatar events boobjob jo_arms3",589,489))
        elif state.endswith(("dick_bottom","dick_middle","dick_top","cum")):
          rv.append(("jo avatar events boobjob jo_arms4",516,487))
        elif state.endswith("aftermath"):
          rv.append(("jo avatar events boobjob jo_arms5",563,489))

      elif state.startswith("car_wash"):
        rv.append((1920,1080))
        rv.append(("jo avatar events car_wash background",0,0))
        if "wet" in state:
          rv.append(("jo avatar events car_wash water_puddles",219,89))
        if "bending_over" in state or "exposed" in state or "covering_up" in state:
          if "wet" in state:
            rv.append(("jo avatar events car_wash jo_body4",827,244))
          else:
            rv.append(("jo avatar events car_wash jo_body3",827,244))
          if "covering_up" in state:
            rv.append(("jo avatar events car_wash jo_arms2",672,264))
          else:
            rv.append(("jo avatar events car_wash jo_arms1",626,264))
          if "bending_over" in state:
            rv.append(("jo avatar events car_wash jo_bikini2",904,259))
            if "jo smile" in state:
              rv.append(("jo avatar events car_wash jo_face2",804,202))
            elif "jo surprised" in state:
              rv.append(("jo avatar events car_wash jo_face3",806,202))
            elif "jo laughing" in state:
              rv.append(("jo avatar events car_wash jo_face4",806,202))
            elif "jo afraid" in state:
              rv.append(("jo avatar events car_wash jo_face5",806,202))
            rv.append(("jo avatar events car_wash jo_hair1",795,145))
            rv.append(("jo avatar events car_wash jo_glasses2",809,234))
          elif "exposed" in state or "covering_up" in state:
            rv.append(("jo avatar events car_wash jo_bikini3a",904,259))
            rv.append(("jo avatar events car_wash jo_bikini3b",963,899))
            rv.append(("jo avatar events car_wash jo_hair2",789,136))
            if "jo afraid" in state:
              rv.append(("jo avatar events car_wash jo_face6",804,194))
            elif "jo embarrassed" in state:
              rv.append(("jo avatar events car_wash jo_face7",804,194))
            elif "jo blush" in state:
              rv.append(("jo avatar events car_wash jo_face8",804,194))
            elif "jo orgasm" in state:
              rv.append(("jo avatar events car_wash jo_face9",804,194))
            elif "jo aftermath" in state:
              rv.append(("jo avatar events car_wash jo_face10",804,194))
            rv.append(("jo avatar events car_wash jo_glasses3",801,219))
        else:
          rv.append(("jo avatar events car_wash jo_body1",715,98))
          rv.append(("jo avatar events car_wash jo_bikini1",907,224))
          if "jo surprised" in state:
            rv.append(("jo avatar events car_wash jo_face1",837,183))
          rv.append(("jo avatar events car_wash jo_glasses1",845,200))
        if "hose_dripping" in state:
          rv.append(("jo avatar events car_wash mc_hand6",1173,824))
        elif "hose_spraying" in state:
          rv.append(("jo avatar events car_wash mc_hand2",805,0))
        elif "hose_jo_panties" in state:
          rv.append(("jo avatar events car_wash mc_hand4",1056,460))
        elif "hose_jo_pussy" in state:
          rv.append(("jo avatar events car_wash mc_hand5",1037,362))

      elif state.startswith("sunset_walk"):
        rv.append((1920,1080))
        rv.append(("jo avatar events sunset_walk background",0,0))
        rv.append(("jo avatar events sunset_walk jo_body",945,237))
        rv.append(("jo avatar events sunset_walk jo_underwear",955,349))
        rv.append(("jo avatar events sunset_walk jo_clothes",944,335))
        rv.append(("jo avatar events sunset_walk mc_body",942,208))
        rv.append(("jo avatar events sunset_walk foreground",0,0))

      return rv


  class Interactable_jo(Interactable):

    def title(cls):
      return jo.name

    def actions(cls,actions):

      #############talks###############
      if quest.jacklyn_town in ("ready","marina"):
        pass
      elif quest.back_to_school_special.in_progress:
        actions.append(["talk","Talk","?jo_talk_back_to_school_special"])
      elif jo["talk_limit_today"]<3:
        actions.append(["talk","Talk","?jo_talk_one"])
        actions.append(["talk","Talk","?jo_talk_two"])
        actions.append(["talk","Talk","?jo_talk_three"])
        actions.append(["talk","Talk","?jo_talk_four"])
        actions.append(["talk","Talk","?jo_talk_five"])
        actions.append(["talk","Talk","?jo_talk_six"])
      else:
        actions.append(["talk","Talk","?jo_talk_over"])

     #################Quests#############
      if mc["focus"]:
        if mc["focus"] == "flora_cooking_chilli":
          if quest.flora_cooking_chilli == "get_milk":
            actions.append(["quest","Quest","?flora_quest_flora_cooking_chilli_jo"])
        elif mc["focus"] == "kate_moment":
          if quest.kate_moment == "jo":
            actions.append(["quest","Quest","?quest_kate_moment_jo"])
          if quest.isabelle_gesture == "advice":
            actions.append(["quest","Quest","quest_isabelle_gesture_advice"])
        elif mc["focus"] == "jacklyn_town":
          if quest.jacklyn_town == "ready":
            actions.append(["quest","Quest","quest_jacklyn_town_ready"])
      else:
        if quest.back_to_school_special == "talk_to_jo":
          actions.append(["quest","Quest","?jo_quest_back_to_school_special"])

        if quest.isabelle_stolen == "askmaxine":
          actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_jo"])

        if quest.mrsl_HOT.in_progress:
          if quest.mrsl_HOT == "jo_talk":#and jo.at("home_kitchen"):
            actions.append(["quest","Quest","?mrsl_quest_HOT_jo_talk"])

          elif quest.mrsl_HOT == "jo_talk_laundry_done":
            actions.append(["quest","Quest","?mrsl_quest_HOT_jo_talk_laundry_done"])

          elif quest.mrsl_HOT == "delivery" and game.day > quest.mrsl_HOT["current_day"]:
            actions.append(["quest","Quest","?mrsl_quest_HOT_delivery_arrive_talk_jo"])
          elif quest.mrsl_HOT == "items" and game.location == "home_kitchen":
            actions.append(["quest","Quest","?mrsl_quest_HOT_jo_talk_capture_card"])

        if quest.jo_potted.in_progress:
          if quest.jo_potted == "jodrink":
            if jo.at('school_*'):
              actions.append(["quest","Quest","?quest_jo_pot_tea_school"])
            else:
              actions.append(["quest","Quest","quest_jo_pot_tea_home"])

        if quest.jacklyn_statement.in_progress:
          if quest.jacklyn_statement == "remove" and not quest.jacklyn_statement["jo_removed"]:
            actions.append(["quest","Quest","?jacklyn_statement_remove_jo"])

        if quest.mrsl_table == "letter":
          actions.append(["quest","Quest","quest_mrsl_table_letter"])

        if quest.isabelle_locker == "interrogate" and jo.at("school_*") and not quest.isabelle_locker["jo_interrogated"] and not quest.isabelle_locker["might_have_an_idea"]:
          actions.append(["quest","Quest","quest_isabelle_locker_interrogate_jo"])

        if quest.lindsey_piano == "jo_talk":
          actions.append(["quest","Quest","quest_lindsey_piano_jo_talk"])
        elif (quest.lindsey_piano == "album_day" and game.day > quest.lindsey_piano["jo_talk"]
          or quest.lindsey_piano == "album_night") and jo.at("home_kitchen"):
            actions.append(["quest","Quest","quest_lindsey_piano_album"])

        if quest.lindsey_motive == "keys" and jo.at("school_*"):
          actions.append(["quest","Quest","quest_lindsey_motive_keys_jo"])

        if quest.jo_day == "amends":
          if game.location == "school_cafeteria":
            actions.append(["quest","Quest","quest_jo_day_amends"])
          else:
            actions.append(["quest","Quest","?quest_jo_day_amends_home"])

        if quest.kate_trick == "jo":
          actions.append(["quest","Quest","quest_kate_trick_jo"])
        elif quest.kate_trick == "report":
          if game.location == "school_cafeteria":
            actions.append(["quest","Quest","quest_kate_trick_report"])
          else:
            actions.append(["quest","Quest","quest_kate_trick_report_home"])

        if quest.isabelle_dethroning == "key" and game.location.id.startswith("school_"):
          actions.append(["quest","Quest","quest_isabelle_dethroning_key"])
        elif quest.isabelle_dethroning == "principal":
          actions.append(["quest","Quest","quest_isabelle_dethroning_principal"])
        elif quest.isabelle_dethroning == "poem_done":
          actions.append(["quest","Quest","quest_isabelle_dethroning_poem_done"])

        if quest.isabelle_gesture == "advice":
          actions.append(["quest","Quest","quest_isabelle_gesture_advice"])

        if quest.lindsey_voluntary == "locker_file":
          actions.append(["quest","Quest","quest_lindsey_voluntary_locker_file"])

        ##### unstarted quests #######
        if game.season == 1:
          if quest.isabelle_tour.finished and not quest.jo_potted.started and quest.berb_fight > "corner":
            actions.append(["quest","Quest","quest_jo_pot_start"])

          if (quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.berb_fight.finished and quest.jo_potted.finished and not quest.jo_day.started
          and game.location == "school_cafeteria"):
            actions.append(["quest","Quest","quest_jo_day_start"])

      ###################flirts#################
      if not quest.jacklyn_town in ("ready","marina"):
        actions.append(["flirt","Flirt","?jo_flirt_back_to_school_special"])



label jo_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label jo_talk_one:
  show jo neutral with Dissolve(.5)
  $jo["talk_limit_today"]+=1
  jo neutral "Remember your 7 p.m. curfew."
  jo neutral "I don't want you out causing any more trouble than you already have."
  jo confident "Besides, I want to cook more dinners for you and [flora]."
  hide jo with Dissolve(.5)
  return

label jo_talk_two:
  show jo thinking with Dissolve(.5)
  $jo["talk_limit_today"]+=1
  jo thinking "Learning the important dates of the year is part of growing up."
  jo thinking "Birthdays especially. I don't want to hear any excuses this year."
  hide jo with Dissolve(.5)
  return

label jo_talk_three:
  show jo smile with Dissolve(.5)
  $jo["talk_limit_today"]+=1
  jo smile "There's nothing more important than your grades."
  jo smile "Your future rests on those marks. I know you've been struggling in the past, but if [flora] can do it, so can you!"
  hide jo with Dissolve(.5)
  return

label jo_talk_four:
  show jo excited with Dissolve(.5)
  $jo["talk_limit_today"]+=1
  jo excited "I think the new curriculum is going to fit you perfectly."
  jo laughing "You're always a bit slow to get started in the mornings."
  jo excited "Besides, I'll be able to keep a close track of how you're doing."
  hide jo with Dissolve(.5)
  return

label jo_talk_five:
  show jo eyeroll with Dissolve(.5)
  $jo["talk_limit_today"]+=1
  jo eyeroll "Now is not the time to talk about those things."
  jo eyeroll "He's gone and that's all you need to know."
  jo flirty "Besides, we're doing just fine, aren't we?"
  hide jo with Dissolve(.5)
  return

label jo_talk_six:
  show jo smile with Dissolve(.5)
  $jo["talk_limit_today"]+=1
  jo smile "Do you still like swings, honey?"
  jo confident "Going to the park with you and [flora] used to be the highlight of my week."
  jo confident "How time flies!"
  hide jo with Dissolve(.5)
  return
