init python:
  class Character_ria(BaseChar):
    notify_level_changed = True

    default_name = "Ria"

    default_stats = [
      ["lust",0,0],
      ["love",0,0],
      ]

    contact_icon = "ria contact_icon"

    default_outfit_slots = ["hat","necklace","shirt","bra","pants","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("ria_necklace")
      self.add_item("ria_coverall")
      self.add_item("ria_bra")
      self.add_item("ria_panties")
      self.equip("ria_necklace")
      self.equip("ria_coverall")
      self.equip("ria_bra")
      self.equip("ria_panties")

    def ai(self):
      self.location = None
      self.activity = None
      self.alternative_locations = {}

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("ria avatar "+state,True):
          return "ria avatar "+state
      rv=[]

      if state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((885,1080))
        rv.append(("ria avatar body1",269,148))
        rv.append(("ria avatar face_neutral",381,243))
        if "_bra" in state:
          rv.append(("ria avatar b1bra",295,404))
        if "_panties" in state:
          rv.append(("ria avatar b1panties",294,796))
        rv.append(("ria avatar b1arm1_n",179,204))
        if "_necklace" in state:
          rv.append(("ria avatar b1necklace",445,394))
        if "_coverall" in state:
          rv.append(("ria avatar b1coverall",267,381))
          rv.append(("ria avatar b1arm1_c",179,204))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((885,1080))
        rv.append(("ria avatar body1",269,148))
        rv.append(("ria avatar face_smile",401,244))
        if "_bra" in state:
          rv.append(("ria avatar b1bra",295,404))
        if "_panties" in state:
          rv.append(("ria avatar b1panties",294,796))
        rv.append(("ria avatar b1arm1_n",179,204))
        if "_necklace" in state:
          rv.append(("ria avatar b1necklace",445,394))
        if "_coverall" in state:
          rv.append(("ria avatar b1coverall",267,381))
          rv.append(("ria avatar b1arm1_c",179,204))

      return rv
