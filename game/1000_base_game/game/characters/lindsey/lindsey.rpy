init python:

  class Character_lindsey(BaseChar):
    notify_level_changed=True

    default_name="Lindsey"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ["hate",1,0],
      ]

    name_color="#1594ff"

    contact_icon="lindsey contact_icon"

    default_outfit_slots=["hat","shirt","bra","pants","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("lindsey_hat")
      self.add_item("lindsey_towel")
      self.add_item("lindsey_mummy_costume")
      self.add_item("lindsey_shirt")
      self.add_item("lindsey_tshirt")
      self.add_item("lindsey_mcshirt")
      self.add_item("lindsey_cropped_hoodie")
      self.add_item("lindsey_bra")
      self.add_item("lindsey_skirt")
      self.add_item("lindsey_pants")
      self.add_item("lindsey_sweatpants")
      self.add_item("lindsey_panties")
      self.equip("lindsey_shirt")
      self.equip("lindsey_bra")
      self.equip("lindsey_skirt")
      self.equip("lindsey_panties")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

      #Forced Locations
      if quest.jo_washed.in_progress:
        if quest.jo_washed["lindsey_location"] == "school_ground_floor":
          self.location = "school_ground_floor"
          self.activity = "running"
        else:
          self.location = None
          self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if lindsey["at_school_nurse_room_now"]:
        self.location="school_nurse_room"
        return

      if lindsey["at_none_now"]:
        self.location = None
        self.alternative_locations={}
        return

      if quest.kate_fate in ("peek","hunt","run","hunt2","deadend","rescue"):
        self.location = None
        self.alternative_locations={}
        return

      if quest.lindsey_nurse in ("fetch_the_nurse","return_to_gym"):
        self.location = None
        self.activity = None
        return

      if quest.lindsey_wrong == "follow":
        self.location = "school_entrance"
        self.activity = "sitting"
        return
      elif quest.lindsey_wrong == "fall" or quest.maxine_lines == "attention":
        if quest.lindsey_wrong["fall"]:
          self.location = "school_ground_floor"
          self.activity = "stairs"
        else:
          self.location = None
        return
      elif quest.lindsey_wrong in ("drinks","fountain","mop","doughnuts","clean","lindseybottle"):
        self.location = "school_gym"
        self.activity = "running"
        return
      elif quest.lindsey_wrong == "lindseyart":
        self.location = "school_art_class"
        self.activity = "naked"
        return
      elif quest.lindsey_wrong in ("florahelp", "floralocker", "lindseyclothes"):
        self.location = "school_art_class"
        self.activity = "mcshirt"
        return

      if quest.maxine_lines == "helpinghand" and quest.maxine_lines["lindsey_just_ran_by"]:
        self.location = "school_ground_floor"
        self.activity = "running"
        return

      if quest.lindsey_piano == "listen_music_class":
        if quest.lindsey_piano["lindsey_arrived"]:
          self.location = "school_music_class"
          self.activity = "standing"
        else:
          self.location = "school_gym"
          self.activity = "running"
        return
      elif quest.lindsey_piano == "pick_up":
        self.location = "home_kitchen"
        self.activity = "standing"
        return
      elif quest.lindsey_piano == "listen_bedroom" and quest.lindsey_piano["lindsey_arrived"]:
        self.location = "home_bedroom"
        self.activity = "sitting"
        return

      if quest.lindsey_motive == "steal":
        self.location = "school_bathroom"
        return

      if game.season == 2:
        self.location = "hospital"
        self.activity = "recovering"
        return
      #Forced Locations

      #Alt Locations
      if quest.isabelle_tour == "gym":
        self.alternative_locations["school_gym"] = "running"

      if quest.isabelle_tour in ("beginning","go_upstairs") and quest.day1_take2.in_progress:
        self.alternative_locations["school_ground_floor"] = "running"

      if ((quest.isabelle_dethroning == "dinner" and game.hour == 19) or quest.isabelle_dethroning in ("trap","revenge_done")) or quest.isabelle_dethroning["kitchen_shenanigans_replay"]:
        self.alternative_locations["school_cafeteria"] = "walking"
      #Alt Locations

      #Schedule
      if game.dow in (0,1,2,3,4,5,6,7):
        if game.hour in (7,9,11):
          self.location="school_gym"
          self.activity="running"
          self.alternative_locations["school_ground_floor"]="running"
          return
        elif game.hour in (8,10):
          self.location="school_english_class"
          self.activity="standing"
          return
        elif game.hour == (12):
          self.location="school_cafeteria"
          self.activity="walking"
          return
        elif game.hour in (13,14):
          self.location="school_first_hall_east"
          self.activity="stretching"
          return
        elif game.hour in (15,16):
          self.location="school_art_class"
          self.activity="standing"
          return
        elif game.hour in (17,18):
          self.location="school_entrance"
          self.activity="sitting"
          return
        #else:
        #  self.location=None
        #  self.activity=None
        #  self.alternative_locations={}
        #Schedule

    def call_label(self):
      if quest.isabelle_buried == "callforhelp":
        return "quest_isabelle_buried_callforhelp_lindsey"
      elif quest.lindsey_piano == "phone_call" and  20 > game.hour > 6:
        return "quest_lindsey_piano_phone_call_same_room" if mc.location == lindsey.location else "quest_lindsey_piano_phone_call"
      else:
        return None

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("lindsey avatar "+state,True):
          return "lindsey avatar "+state
      rv=[]

      if state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body1",126,157))
        rv.append(("lindsey avatar face_angry",196,278))
        if "_bra" in state:
          rv.append(("lindsey avatar b1bra",167,448))
        if "_panties" in state:
          rv.append(("lindsey avatar b1panty",158,786))
        if "_skirt" in state:
          rv.append(("lindsey avatar b1skirt",123,787))
        rv.append(("lindsey avatar b1arm1_n",38,468))
        if "_shirt" in state:
          rv.append(("lindsey avatar b1jacket",143,407))
          rv.append(("lindsey avatar b1arm1_c",33,468))
        elif "_towel" in state:
          rv.append(("lindsey avatar b1towel1",112,566))

      elif state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body1",126,157))
        rv.append(("lindsey avatar face_confident",196,274))
        if "_bra" in state:
          rv.append(("lindsey avatar b1bra",167,448))
        if "_panties" in state:
          rv.append(("lindsey avatar b1panty",158,786))
        if "_skirt" in state:
          rv.append(("lindsey avatar b1skirt",123,787))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b1sweatpants",119,727))
        rv.append(("lindsey avatar b1arm2_n",16,394))
        if "_keyring" in state:
          if "_glow" in state:
            rv.append(("lindsey avatar b1keyring_glow",60,438))
          else:
            rv.append(("lindsey avatar b1keyring",91,438))
        if "_album" in state:
          rv.append(("lindsey avatar b1album",13,307))
        if "_shirt" in state:
          rv.append(("lindsey avatar b1jacket",143,407))
          rv.append(("lindsey avatar b1arm2_c",16,394))
          if "_album" in state:
            rv.append(("lindsey avatar b1album",13,307))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b1cropped_hoodie",49,410))
        elif "_towel" in state:
          rv.append(("lindsey avatar b1towel2",112,566))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body1",126,157))
        rv.append(("lindsey avatar face_excited",196,273))
        if "_bra" in state:
          rv.append(("lindsey avatar b1bra",167,448))
        if "_panties" in state:
          rv.append(("lindsey avatar b1panty",158,786))
        if "_skirt" in state:
          rv.append(("lindsey avatar b1skirt",123,787))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b1sweatpants",119,727))
        rv.append(("lindsey avatar b1arm2_n",16,394))
        if "_keyring" in state:
          if "_glow" in state:
            rv.append(("lindsey avatar b1keyring_glow",60,438))
          else:
            rv.append(("lindsey avatar b1keyring",91,438))
        if "_album" in state:
          rv.append(("lindsey avatar b1album",13,307))
        if "_shirt" in state:
          rv.append(("lindsey avatar b1jacket",143,407))
          rv.append(("lindsey avatar b1arm2_c",16,394))
          if "_album" in state:
            rv.append(("lindsey avatar b1album",13,307))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b1cropped_hoodie",49,410))
        elif "_towel" in state:
          rv.append(("lindsey avatar b1towel2",112,566))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body1",126,157))
        rv.append(("lindsey avatar face_neutral",196,273))
        if "_bra" in state:
          rv.append(("lindsey avatar b1bra",167,448))
        if "_panties" in state:
          rv.append(("lindsey avatar b1panty",158,786))
        if "_skirt" in state:
          rv.append(("lindsey avatar b1skirt",123,787))
        rv.append(("lindsey avatar b1arm1_n",38,468))
        if "_shirt" in state:
          rv.append(("lindsey avatar b1jacket",143,407))
          rv.append(("lindsey avatar b1arm1_c",33,468))
        elif "_towel" in state:
          rv.append(("lindsey avatar b1towel1",112,566))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body2",91,145))
        rv.append(("lindsey avatar face_flirty",193,258))
        if "_bra" in state:
          rv.append(("lindsey avatar b2bra",183,445))
        if "_panties" in state:
          rv.append(("lindsey avatar b2panty",196,803))
        if "_skirt" in state:
          rv.append(("lindsey avatar b2skirt",106,796))
        elif "_pants" in state:
          rv.append(("lindsey avatar b2pants",89,765))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b2sweatpants",86,728))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2mummy_costume",110,216))
        rv.append(("lindsey avatar b2arm1_n",64,467))
        if "_mcshirt" in state:
          rv.append(("lindsey avatar b2ranglan",138,443))
          rv.append(("lindsey avatar b2ranglanarm1",64,463))
        elif "_tshirt" in state:
          rv.append(("lindsey avatar b2shirt",182,466))
          rv.append(("lindsey avatar b2shirtarm1",64,467))
        elif "_shirt" in state:
          rv.append(("lindsey avatar b2jacket",183,403))
          rv.append(("lindsey avatar b2arm1_c",61,469))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b2cropped_hoodie",178,368))
          rv.append(("lindsey avatar b2arm1_cropped_hoodie",64,464))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2arm1_mummy_costume",97,549))
        elif "_towel" in state:
          rv.append(("lindsey avatar b2towel1",138,570))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body2",91,145))
        rv.append(("lindsey avatar face_laughing",193,261))
        if "_bra" in state:
          rv.append(("lindsey avatar b2bra",183,445))
        if "_panties" in state:
          rv.append(("lindsey avatar b2panty",196,803))
        if "_skirt" in state:
          rv.append(("lindsey avatar b2skirt",106,796))
        elif "_pants" in state:
          rv.append(("lindsey avatar b2pants",89,765))
        elif "_sweatpants" in state:
          if "_cropped_hoodie" in state:
            rv.append(("lindsey avatar b2arm2_n",91,325))
          rv.append(("lindsey avatar b2sweatpants",86,728))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2mummy_costume",110,216))
        if "_cropped_hoodie" not in state:
          rv.append(("lindsey avatar b2arm2_n",91,325))
        if "_mcshirt" in state:
          rv.append(("lindsey avatar b2ranglan",138,443))
          rv.append(("lindsey avatar b2ranglanarm2",90,325))
        elif "_tshirt" in state:
          rv.append(("lindsey avatar b2shirt",182,466))
          rv.append(("lindsey avatar b2shirtarm2",91,325))
        elif "_shirt" in state:
          rv.append(("lindsey avatar b2jacket",183,403))
          rv.append(("lindsey avatar b2arm2_c",91,325))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b2cropped_hoodie",178,368))
          rv.append(("lindsey avatar b2arm2_cropped_hoodie",83,325))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2arm2_mummy_costume",104,560))
        elif "_towel" in state:
          rv.append(("lindsey avatar b2towel2",136,570))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body2",91,145))
        if "_phone" in state:
          rv.append(("lindsey avatar face_skeptical_phone",193,259))
        else:
          rv.append(("lindsey avatar face_skeptical",193,259))
        if "_slap" in state:
          rv.append(("lindsey avatar face_skeptical_slap",272,284))
        if "_bra" in state:
          rv.append(("lindsey avatar b2bra",183,445))
        if "_panties" in state:
          rv.append(("lindsey avatar b2panty",196,803))
        if "_skirt" in state:
          rv.append(("lindsey avatar b2skirt",106,796))
        elif "_pants" in state:
          rv.append(("lindsey avatar b2pants",89,765))
        elif "_sweatpants" in state:
          if "_cropped_hoodie" in state:
            rv.append(("lindsey avatar b2arm2_n",91,325))
          rv.append(("lindsey avatar b2sweatpants",86,728))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2mummy_costume",110,216))
        if "_cropped_hoodie" not in state:
          if "_phone" in state:
            rv.append(("lindsey avatar b2phone_n",120,438))
          else:
            rv.append(("lindsey avatar b2arm2_n",91,325))
        if "_mcshirt" in state:
          rv.append(("lindsey avatar b2ranglan",138,443))
          rv.append(("lindsey avatar b2ranglanarm2",90,325))
        elif "_tshirt" in state:
          rv.append(("lindsey avatar b2shirt",182,466))
          rv.append(("lindsey avatar b2shirtarm2",91,325))
        elif "_shirt" in state:
          rv.append(("lindsey avatar b2jacket",183,403))
          if "_phone" in state:
            rv.append(("lindsey avatar b2phone_c",115,438))
          else:
            rv.append(("lindsey avatar b2arm2_c",91,325))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b2cropped_hoodie",178,368))
          rv.append(("lindsey avatar b2arm2_cropped_hoodie",83,325))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2arm2_mummy_costume",104,560))
        elif "_towel" in state:
          rv.append(("lindsey avatar b2towel2",136,570))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body2",91,145))
        rv.append(("lindsey avatar face_smile",192,261))
        if "_bra" in state:
          rv.append(("lindsey avatar b2bra",183,445))
        if "_panties" in state:
          rv.append(("lindsey avatar b2panty",196,803))
        if "_skirt" in state:
          rv.append(("lindsey avatar b2skirt",106,796))
        elif "_pants" in state:
          rv.append(("lindsey avatar b2pants",89,765))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b2sweatpants",86,728))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2mummy_costume",110,216))
        rv.append(("lindsey avatar b2arm1_n",64,467))
        if "_mcshirt" in state:
          rv.append(("lindsey avatar b2ranglan",138,443))
          rv.append(("lindsey avatar b2ranglanarm1",64,463))
        elif "_tshirt" in state:
          rv.append(("lindsey avatar b2shirt",182,466))
          rv.append(("lindsey avatar b2shirtarm1",64,467))
        elif "_shirt" in state:
          rv.append(("lindsey avatar b2jacket",183,403))
          rv.append(("lindsey avatar b2arm1_c",61,469))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b2cropped_hoodie",178,368))
          rv.append(("lindsey avatar b2arm1_cropped_hoodie",64,464))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b2arm1_mummy_costume",97,549))
        elif "_towel" in state:
          rv.append(("lindsey avatar b2towel1",138,570))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((642,1080))
        rv.append(("lindsey avatar body3",193,155))
        rv.append(("lindsey avatar face_afraid",245,265))
        if "_slap" in state:
          rv.append(("lindsey avatar face_afraid_slap",299,317))
        if "_bra" in state:
          rv.append(("lindsey avatar b3bra",205,437))
        if "_panties" in state:
          rv.append(("lindsey avatar b3panty",216,810))
        if "_skirt" in state:
          rv.append(("lindsey avatar b3skirt",190,800))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b3sweatpants",185,734))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3mummy_costume",194,206))
        rv.append(("lindsey avatar b3arm2_n",139,347))
        if "_shirt" in state:
          rv.append(("lindsey avatar b3jacket",202,407))
          rv.append(("lindsey avatar b3arm2_c",132,346))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b3cropped_hoodie",201,364))
          rv.append(("lindsey avatar b3arm2_cropped_hoodie",129,347))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3arm2_mummy_costume",139,545))
        elif "_towel" in state:
          rv.append(("lindsey avatar b3towel2",192,562))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((642,1080))
        rv.append(("lindsey avatar body3",193,155))
        rv.append(("lindsey avatar face_blush",243,260))
        if "_bra" in state:
          rv.append(("lindsey avatar b3bra",205,437))
        if "_panties" in state:
          rv.append(("lindsey avatar b3panty",216,810))
        if "_skirt" in state:
          rv.append(("lindsey avatar b3skirt",190,800))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b3sweatpants",185,734))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3mummy_costume",194,206))
        rv.append(("lindsey avatar b3arm2_n",139,347))
        if "_shirt" in state:
          rv.append(("lindsey avatar b3jacket",202,407))
          rv.append(("lindsey avatar b3arm2_c",132,346))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b3cropped_hoodie",201,364))
          rv.append(("lindsey avatar b3arm2_cropped_hoodie",129,347))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3arm2_mummy_costume",139,545))
        elif "_towel" in state:
          rv.append(("lindsey avatar b3towel2",192,562))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((642,1080))
        rv.append(("lindsey avatar body3",193,155))
        rv.append(("lindsey avatar face_cringe",245,265))
        if "_bra" in state:
          rv.append(("lindsey avatar b3bra",205,437))
        if "_panties" in state:
          rv.append(("lindsey avatar b3panty",216,810))
        if "_skirt" in state:
          rv.append(("lindsey avatar b3skirt",190,800))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b3sweatpants",185,734))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3mummy_costume",194,206))
        rv.append(("lindsey avatar b3arm1_n",192,459))
        if "_shirt" in state:
          rv.append(("lindsey avatar b3jacket",202,407))
          rv.append(("lindsey avatar b3arm1_c",192,458))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b3cropped_hoodie",201,364))
          rv.append(("lindsey avatar b3arm1_cropped_hoodie",191,446))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3arm1_mummy_costume",505,545))
        elif "_towel" in state:
          rv.append(("lindsey avatar b3towel1",192,562))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((642,1080))
        rv.append(("lindsey avatar body3",193,155))
        if "_drinking" in state:
          rv.append(("lindsey avatar face_thinking_bottle",245,258))
        else:
          rv.append(("lindsey avatar face_thinking",245,258))
        if "_bra" in state:
          rv.append(("lindsey avatar b3bra",205,437))
        if "_panties" in state:
          rv.append(("lindsey avatar b3panty",216,810))
        if "_skirt" in state:
          rv.append(("lindsey avatar b3skirt",190,800))
        elif "_sweatpants" in state:
          rv.append(("lindsey avatar b3sweatpants",185,734))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3mummy_costume",194,206))
        if "_drinking" in state:
          rv.append(("lindsey avatar b3armbottle_n",13,322))
        else:
          rv.append(("lindsey avatar b3arm1_n",192,459))
        if "_shirt" in state:
          rv.append(("lindsey avatar b3jacket",202,407))
          if "_drinking" in state:
            rv.append(("lindsey avatar b3armbottle_c",12,322))
          else:
            rv.append(("lindsey avatar b3arm1_c",192,458))
        elif "_cropped_hoodie" in state:
          rv.append(("lindsey avatar b3cropped_hoodie",201,364))
          rv.append(("lindsey avatar b3arm1_cropped_hoodie",191,446))
        elif "_mummy_costume" in state:
          rv.append(("lindsey avatar b3arm1_mummy_costume",505,545))
        elif "_towel" in state:
          rv.append(("lindsey avatar b3towel1",192,562))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body4",79,157))
        rv.append(("lindsey avatar face_annoyed",186,272))
        if "_bra" in state:
          rv.append(("lindsey avatar b4bra",136,455))
        if "_panties" in state:
          rv.append(("lindsey avatar b4panty",156,789))
        if "_skirt" in state:
          rv.append(("lindsey avatar b4skirt",89,799))
        rv.append(("lindsey avatar b4arm1_n",98,456))
        if "_shirt" in state:
          rv.append(("lindsey avatar b4jacket",137,424))
          rv.append(("lindsey avatar b4arm1_c",91,456))
        elif "_towel" in state:
          rv.append(("lindsey avatar b4towel1",97,564))

      elif state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body4",79,157))
        rv.append(("lindsey avatar face_concerned",186,267))
        if "_slap" in state:
          rv.append(("lindsey avatar face_sad_slap",251,309))
        if "_bra" in state:
          rv.append(("lindsey avatar b4bra",136,455))
        if "_panties" in state:
          rv.append(("lindsey avatar b4panty",156,789))
        if "_skirt" in state:
          rv.append(("lindsey avatar b4skirt",89,799))
        rv.append(("lindsey avatar b4arm2_n",68,441))
        if "_shirt" in state:
          rv.append(("lindsey avatar b4jacket",137,424))
          rv.append(("lindsey avatar b4arm2_c",68,441))
        elif "_towel" in state:
          rv.append(("lindsey avatar b4towel2",97,564))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body4",79,157))
        rv.append(("lindsey avatar face_eyeroll",190,267))
        if "_bra" in state:
          rv.append(("lindsey avatar b4bra",136,455))
        if "_panties" in state:
          rv.append(("lindsey avatar b4panty",156,789))
        if "_skirt" in state:
          rv.append(("lindsey avatar b4skirt",89,799))
        rv.append(("lindsey avatar b4arm2_n",68,441))
        if "_shirt" in state:
          rv.append(("lindsey avatar b4jacket",137,424))
          rv.append(("lindsey avatar b4arm2_c",68,441))
        elif "_towel" in state:
          rv.append(("lindsey avatar b4towel2",97,564))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("lindsey avatar body4",79,157))
        rv.append(("lindsey avatar face_sad",191,270))
        if "_slap" in state:
          rv.append(("lindsey avatar face_sad_slap",251,309))
        if "_bra" in state:
          rv.append(("lindsey avatar b4bra",136,455))
        if "_panties" in state:
          rv.append(("lindsey avatar b4panty",156,789))
        if "_skirt" in state:
          rv.append(("lindsey avatar b4skirt",89,799))
        rv.append(("lindsey avatar b4arm1_n",98,456))
        if "_shirt" in state:
          rv.append(("lindsey avatar b4jacket",137,424))
          rv.append(("lindsey avatar b4arm1_c",91,456))
        elif "_towel" in state:
          rv.append(("lindsey avatar b4towel1",97,564))

      elif state.startswith("lindseypose01"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events gymfall LindseyGround_Body",0,0))
        rv.append(("lindsey avatar events gymfall LindseyGround_Panty",896,619))
        rv.append(("lindsey avatar events gymfall LindseyGround_Bra",835,232))
        rv.append(("lindsey avatar events gymfall LindseyGround_c",488,25))
      elif state.startswith("LindseyPose02a"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events carry CarryLindsey_BG",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Body",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Panty",1286,735))
        rv.append(("lindsey avatar events carry CarryLindsey_Bra",369,522))
        rv.append(("lindsey avatar events carry CarryLindsey_c",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_default",388,266))
      elif state.startswith("LindseyPose02b"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events carry CarryLindsey_BG",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Body",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Panty",1286,735))
        rv.append(("lindsey avatar events carry CarryLindsey_Bra",369,522))
        rv.append(("lindsey avatar events carry CarryLindsey_c",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_concerned",393,239))
      elif state.startswith("LindseyPose02c"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events carry CarryLindsey_BG",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Body",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Panty",1286,735))
        rv.append(("lindsey avatar events carry CarryLindsey_Bra",369,522))
        rv.append(("lindsey avatar events carry CarryLindsey_c",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_teasing",387,262))
      elif state.startswith("LindseyPose02nobg"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events carry CarryLindsey_Body",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_Panty",1286,735))
        rv.append(("lindsey avatar events carry CarryLindsey_Bra",369,522))
        rv.append(("lindsey avatar events carry CarryLindsey_c",0,0))
        rv.append(("lindsey avatar events carry CarryLindsey_concerned",393,239))

      elif state.startswith("clinic_lindsey"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events clinic LindseyClinic_Lindsey_Body",179,64))
        rv.append(("lindsey avatar events clinic LindseyClinic_Jacket",361,906))
        if "_topless" not in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_Lindsey_Bra",932,369))
        rv.append(("lindsey avatar events clinic LindseyClinic_Lindsey_Panty",1046,752))
        rv.append(("lindsey avatar events clinic LindseyClinic_Skirt",950,692))
        if "_annoyed" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_annoyed",874,195))
        if "_neutral" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_neutral",874,192))
        if "_angry" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_angry",874,199))
        if "_laughing" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_laughing",874,192))
        if "_blush" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_blush",874,201))
        if "_smile" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_smile",874,192))
        if "_scared" in state:
          rv.append(("lindsey avatar events clinic LindseyClinic_scared",880,203))

      if state.startswith("LindseyPose00b"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events trip LindseyTrip_BG",0,0))
        rv.append(("lindsey avatar events trip LindseyTrip_Head2",1192,43))
        rv.append(("lindsey avatar events trip LindseyTrip_Body",246,213))
        rv.append(("lindsey avatar events trip LindseyTrip_undies",789,233))
        rv.append(("lindsey avatar events trip LindseyTrip_c",661,182))
        rv.append(("lindsey avatar events trip LindseyTrip_Shoes",240,681))
        rv.append(("lindsey avatar events trip LindseyTrip_Ponytail",1164,56))
      elif state.startswith("LindseyPose00"): #lindsey avatar events trip
        rv.append((1920,1080))
        rv.append(("lindsey avatar events trip LindseyTrip_BG",0,0))
        rv.append(("lindsey avatar events trip LindseyTrip_Head1",1210,41))
        rv.append(("lindsey avatar events trip LindseyTrip_Body",246,213))
        rv.append(("lindsey avatar events trip LindseyTrip_undies",789,233))
        rv.append(("lindsey avatar events trip LindseyTrip_c",661,182))
        rv.append(("lindsey avatar events trip LindseyTrip_Shoes",240,681))
        rv.append(("lindsey avatar events trip LindseyTrip_Ponytail",1164,56))

      if state.startswith("soaked_gushed"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket2",158,206))
        rv.append(("lindsey avatar events wet lindsey_soaked_gushedbase",742,118))
        rv.append(("lindsey avatar events wet lindsey_soaked_gushedbra",941,430))
        rv.append(("lindsey avatar events wet lindsey_soaked_gushedclothes",776,128))
        rv.append(("lindsey avatar events wet lindsey_soaked_splash",474,0))
      if state.startswith("soaked_bg"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        if "_jacket1" in state:
          rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket1",158,132))
        if "_jacket2" in state:
          rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket2",158,206))
        if "_splash" in state:
          rv.append(("lindsey avatar events wet lindsey_soaked_splash",474,0))
      elif state.startswith("soaked_annoyed"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket1",158,132))
        rv.append(("lindsey avatar events wet lindsey_soaked_base",914,38))
        rv.append(("lindsey avatar events wet lindsey_soaked_bra",1002,438))
        rv.append(("lindsey avatar events wet lindsey_soaked_jacket",899,359))
        rv.append(("lindsey avatar events wet lindsey_soaked_annoyed",1150,192))
      elif state.startswith("soaked_flirty"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket1",158,132))
        rv.append(("lindsey avatar events wet lindsey_soaked_base",914,38))
        rv.append(("lindsey avatar events wet lindsey_soaked_bra",1002,438))
        rv.append(("lindsey avatar events wet lindsey_soaked_jacket",899,359))
        rv.append(("lindsey avatar events wet lindsey_soaked_flirty",1114,192))
      elif state.startswith("soaked_blush"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket1",158,132))
        rv.append(("lindsey avatar events wet lindsey_soaked_base",914,38))
        rv.append(("lindsey avatar events wet lindsey_soaked_bra",1002,438))
        rv.append(("lindsey avatar events wet lindsey_soaked_jacket",899,359))
        rv.append(("lindsey avatar events wet lindsey_soaked_blush",1121,192))
      elif state.startswith("soaked_skeptical"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket1",158,132))
        rv.append(("lindsey avatar events wet lindsey_soaked_base",914,38))
        rv.append(("lindsey avatar events wet lindsey_soaked_bra",1002,438))
        rv.append(("lindsey avatar events wet lindsey_soaked_jacket",899,359))
        rv.append(("lindsey avatar events wet lindsey_soaked_skeptical",1150,192))
      elif state.startswith("soaked_sad"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wet lindsey_soaked_bg",0,0))
        rv.append(("lindsey avatar events wet lindsey_soaked_puddle",736,780))
        rv.append(("lindsey avatar events wet lindsey_soaked_fountain",177,143))
        rv.append(("lindsey avatar events wet lindsey_soaked_mcjacket1",158,132))
        rv.append(("lindsey avatar events wet lindsey_soaked_base",914,38))
        rv.append(("lindsey avatar events wet lindsey_soaked_bra",1002,438))
        rv.append(("lindsey avatar events wet lindsey_soaked_jacket",899,359))
        rv.append(("lindsey avatar events wet lindsey_soaked_sad",1150,192))

      if state.startswith("hugging_mcshirt"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events wethug lindsey_hugging_bg",0,0))
        rv.append(("lindsey avatar events wethug lindsey_hugging",426,0))
        rv.append(("lindsey avatar events wethug lindsey_hugging_shirt",552,147))

      if state=="lindseyfall":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall falling lindseyfall_bg",0,0))
        rv.append(("lindsey avatar events fall falling lindseyfall_shadow",804,478))
        rv.append(("lindsey avatar events fall falling lindseyfall_n",635,65))
        rv.append(("lindsey avatar events fall falling lindseyfall_undies",979,270))
        rv.append(("lindsey avatar events fall falling lindseyfall_c",763,242))
      if state.startswith("lindseycrashed"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall crashed lindseycrashed_bg",0,0))
        rv.append(("lindsey avatar events fall crashed lindseycrashed_shadow",376,128))
        rv.append(("lindsey avatar events fall crashed lindseycrashed_n",392,116))
        rv.append(("lindsey avatar events fall crashed lindseycrashed_undies",677,460))
        rv.append(("lindsey avatar events fall crashed lindseycrashed_c",385,108))
        if "_waking" in state:
          rv.append(("lindsey avatar events fall crashed lindseycrashed_face_wakingup",1049,725))
        elif "_angry" in state:
          rv.append(("lindsey avatar events fall crashed lindseycrashed_face_angry",1057,726))
        else:
          rv.append(("lindsey avatar events fall crashed lindseycrashed_face_closedeyes",1049,725))
        if "_rec" in state:
          rv.append(("lindsey avatar events fall crashed lindseycrashed_camera",0,0))
      if state=="lindseycatch":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall catch lindseycatch_bg",0,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body1n",3,387))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchead1",1171,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body1u",646,383))
        rv.append(("lindsey avatar events fall catch lindseycatch_body1c",0,355))
        rv.append(("lindsey avatar events fall catch lindseycatch_head1",852,90))
        rv.append(("lindsey avatar events fall catch lindseycatch_face1",1168,206))
        rv.append(("lindsey avatar events fall catch lindseycatch_mcbody",456,284))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm1n",988,252))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm1c",975,318))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchand2",747,492))
      elif state=="lindseycatch_look":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall catch lindseycatch_bg",0,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body2n",476,371))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchead2",1183,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body2u",555,371))
        rv.append(("lindsey avatar events fall catch lindseycatch_body2c",447,332))
        rv.append(("lindsey avatar events fall catch lindseycatch_head2",783,55))
        rv.append(("lindsey avatar events fall catch lindseycatch_face2",969,172))
        rv.append(("lindsey avatar events fall catch lindseycatch_mcbody",456,284))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm2n",876,186))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm2c",858,334))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchand1",763,597))
      elif state=="lindseycatch_smile":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall catch lindseycatch_bg",0,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body2n",476,371))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchead2",1183,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body2u",555,371))
        rv.append(("lindsey avatar events fall catch lindseycatch_body2c",447,332))
        rv.append(("lindsey avatar events fall catch lindseycatch_head2",783,55))
        rv.append(("lindsey avatar events fall catch lindseycatch_face3",981,168))
        rv.append(("lindsey avatar events fall catch lindseycatch_mcbody",456,284))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm2n",876,186))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm2c",858,334))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchand1",763,597))
      elif state=="lindseycatch_kiss":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall catch lindseycatch_bg",0,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body3n",486,383))
        rv.append(("lindsey avatar events fall catch lindseycatch_body3u",601,379))
        rv.append(("lindsey avatar events fall catch lindseycatch_body3c",500,340))
        rv.append(("lindsey avatar events fall catch lindseycatch_head3",873,46))
        rv.append(("lindsey avatar events fall catch lindseycatch_face4",1055,148))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchead1",1171,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_mcbody",456,284))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm3n",953,195))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm3c",944,334))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchand1",763,597))
      elif state=="lindseycatch_hug":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events fall catch lindseycatch_bg",0,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body4n",492,404))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchead2",1183,0))
        rv.append(("lindsey avatar events fall catch lindseycatch_body4u",654,405))
        rv.append(("lindsey avatar events fall catch lindseycatch_body4c",519,130))
        rv.append(("lindsey avatar events fall catch lindseycatch_head4",990,96))
        rv.append(("lindsey avatar events fall catch lindseycatch_face5",1156,219))
        rv.append(("lindsey avatar events fall catch lindseycatch_mcbody",456,284))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm4n",1005,401))
        rv.append(("lindsey avatar events fall catch lindseycatch_arm4c",994,397))
        rv.append(("lindsey avatar events fall catch lindseycatch_mchand2",747,492))

      if state.startswith("keyring"):
        if "_glow" in state:
          if "_cam" in state:
            rv.append(("lindsey avatar events spidereggs bgnightcamera",0,0))
            rv.append(("lindsey avatar events spidereggs cameraoverlay",0,0))
          else:
            rv.append(("lindsey avatar events spidereggs bgnight",0,0))
        else:
          rv.append(("lindsey avatar events spidereggs bgday",0,0))

      if state == "toilet_remove_towel":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet lindsey_remove_towel",1028,207))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_towel_reveal":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet lindsey_towel_reveal",791,207))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_panties_down":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet tissue",742,596))
        rv.append(("lindsey avatar events toilet lindsey_panties_down",895,359))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet towel",655,577))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_pee":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet tissue",742,596))
        rv.append(("lindsey avatar events toilet lindsey_pee",758,233))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet towel",655,577))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_paper_towel":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet tissue",742,596))
        rv.append(("lindsey avatar events toilet lindsey_paper_towel",758,267))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet towel",655,577))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_wipe":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet tissue",742,596))
        rv.append(("lindsey avatar events toilet lindsey_wipe",722,258))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet towel",655,577))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_throw":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet tissue",742,596))
        rv.append(("lindsey avatar events toilet lindsey_throw",536,259))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet towel",655,577))
        rv.append(("lindsey avatar events toilet mc_hand1",225,824))
      elif state == "toilet_glance":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg_glance",0,0))
        rv.append(("lindsey avatar events toilet eggs_glow",687,660))
        rv.append(("lindsey avatar events toilet lindsey_glance",536,259))
        rv.append(("lindsey avatar events toilet door_glance",0,0))
        rv.append(("lindsey avatar events toilet camera",0,0))
      elif state == "toilet_scream":
        rv.append((1920,1080))
        rv.append(("lindsey avatar events toilet bg",0,0))
        rv.append(("lindsey avatar events toilet tissue",742,596))
        rv.append(("lindsey avatar events toilet lindsey_scream",758,246))
        rv.append(("lindsey avatar events toilet door",0,468))
        rv.append(("lindsey avatar events toilet wall",0,0))
        rv.append(("lindsey avatar events toilet towel",655,577))
        rv.append(("lindsey avatar events toilet mc_hand2",231,754))

      if state.startswith("kiss"):
        rv.append((1920,1080))
        if state.endswith("_kitchen"):
          rv.append(("lindsey avatar events kiss bg_kitchen",0,0))
        elif state.endswith("_bedroom"):
          rv.append(("lindsey avatar events kiss bg_bedroom",0,0))
        rv.append(("lindsey avatar events kiss mc",189,68))
        rv.append(("lindsey avatar events kiss lindsey_n",410,112))
        rv.append(("lindsey avatar events kiss lindsey_u",479,524))
        rv.append(("lindsey avatar events kiss lindsey_c",471,490))

      if state.startswith("dancing"):
        rv.append((1920,1080))
        if "_with_the_mc" in state:
          if state.endswith("_background"):
            if home_bedroom["clean"]:
              rv.append(("lindsey avatar events dancing with_the_mc bg_clean",0,0))
            else:
              rv.append(("lindsey avatar events dancing with_the_mc bg_dirty",0,0))
          else:
            rv.append(("lindsey avatar events dancing with_the_mc shadow",267+100,791))
            rv.append(("lindsey avatar events dancing with_the_mc mc_body",116+100,477))
            rv.append(("lindsey avatar events dancing with_the_mc lindsey_body",142+100,0))
            if state.endswith("smile"):
              rv.append(("lindsey avatar events dancing with_the_mc lindsey_face1",1087+100,266))
            elif state.endswith("blush"):
              rv.append(("lindsey avatar events dancing with_the_mc lindsey_face2",1054+100,266))
            elif state.endswith("admiring"):
              rv.append(("lindsey avatar events dancing with_the_mc lindsey_face3",1089+100,266))
            elif state.endswith("blush_closed"):
              rv.append(("lindsey avatar events dancing with_the_mc lindsey_face4",1054+100,266))
            rv.append(("lindsey avatar events dancing with_the_mc hair_fix",1188+100,-19))
            rv.append(("lindsey avatar events dancing with_the_mc lindsey_bra",783+100,352))
            rv.append(("lindsey avatar events dancing with_the_mc lindsey_panties",688+100,647))
            rv.append(("lindsey avatar events dancing with_the_mc lindsey_cropped_hoodie",344+100,255))
            rv.append(("lindsey avatar events dancing with_the_mc lindsey_sweatpants",626+100,647))
        elif "_on_her_own" in state:
          rv.append(("lindsey avatar events dancing on_her_own bg",0,0))
          if state.endswith("1"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body1",1064,265))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties1",1191,678))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra1",1263,455))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants1",1048,648))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie1",1090,428))
          elif state.endswith("2"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body2",896,166))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties2",1098,718))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra2",1138,448))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants2",902,642))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie2",1095,223))
          elif state.endswith("3"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body3",847,244))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties3",964,663))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra3",1043,421))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants3",841,634))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie3",888,395))
          elif state.endswith("4"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body4",835,47))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties4",925,706))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra4",928,408))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants4",829,603))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie4",847,174))
          elif state.endswith("5"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body5",493,188))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties5",807,635))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra5",912,386))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants5",495,597))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie5",837,338))
          elif state.endswith("6"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body6",657,11))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties6",686,651))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra6",801,368))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants6",645,596))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie6",737,85))
          elif state.endswith("7"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body7",409,56))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties7",624,641))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra7",690,354))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants7",405,560))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie7",516,196))
          elif state.endswith("8"):
            rv.append(("lindsey avatar events dancing on_her_own lindsey_body8",396,102))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_panties8",534,593))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_bra8",588,298))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_sweatpants8",392,510))
            rv.append(("lindsey avatar events dancing on_her_own lindsey_cropped_hoodie8",487,117))

      if state.startswith("painting_date"):
        rv.append((1920,1080))
        if state.endswith("float"):
          rv.append(("lindsey avatar events painting_date float",0,0))
        elif state.endswith("sink"):
          rv.append(("lindsey avatar events painting_date sink",0,0))
        else:
          rv.append(("lindsey avatar events painting_date background",0,0))
          if state.endswith(("_laughing","_smile","_blush","_pouting","_blank")):
            if "_armless" in state:
              pass
            elif "_wallet" in state:
              rv.append(("lindsey avatar events painting_date lindsey_right_arm1_n",866,554))
              rv.append(("lindsey avatar events painting_date lindsey_right_arm1_c",1017,612))
            else:
              rv.append(("lindsey avatar events painting_date lindsey_right_arm2_n",846,509))
              rv.append(("lindsey avatar events painting_date lindsey_right_arm2_c",987,609))
            rv.append(("lindsey avatar events painting_date lindsey_body1",1121,416))
            if lindsey.equipped_item("lindsey_panties"):
              rv.append(("lindsey avatar events painting_date lindsey_panties",1206,882))
            if lindsey.equipped_item("lindsey_skirt"):
              if "_wet" in state:
                rv.append(("lindsey avatar events painting_date lindsey_skirt1",1149,821))
              else:
                rv.append(("lindsey avatar events painting_date lindsey_skirt2",1149,821))
            if "_boobs" in state:
              if "_wet" in state:
                rv.append(("lindsey avatar events painting_date lindsey_bra1",1161,423))
              else:
                rv.append(("lindsey avatar events painting_date lindsey_bra2",1161,423))
            else:
              rv.append(("lindsey avatar events painting_date lindsey_bra3",1122,423))
            rv.append(("lindsey avatar events painting_date lindsey_hoodie",1154,401))
            if "_wet" in state:
              rv.append(("lindsey avatar events painting_date water_drops",1155,501))
            if state.endswith("_blank"):
              rv.append(("lindsey avatar events painting_date lindsey_left_arm1_n",1334,805))
              rv.append(("lindsey avatar events painting_date lindsey_left_arm1_c",1322,803))
            else:
              rv.append(("lindsey avatar events painting_date lindsey_left_arm2_n",1205,805))
              rv.append(("lindsey avatar events painting_date lindsey_left_arm2_c",1253,803))
            if state.endswith("_smile"):
              rv.append(("lindsey avatar events painting_date lindsey_head1",1151,150))
            elif state.endswith("_laughing"):
              rv.append(("lindsey avatar events painting_date lindsey_head2",1151,150))
            elif state.endswith("_blush"):
              rv.append(("lindsey avatar events painting_date lindsey_head3",1151,150))
            elif state.endswith("_pouting"):
              rv.append(("lindsey avatar events painting_date lindsey_head4",1151,150))
            elif state.endswith("_blank"):
              if "_wet" in state:
                rv.append(("lindsey avatar events painting_date lindsey_head5",1105,158))
              else:
                rv.append(("lindsey avatar events painting_date lindsey_head6",1105,158))
          elif state.endswith("_water"):
            rv.append(("lindsey avatar events painting_date lindsey_body2",1042,410))

      if state.startswith("making_out"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events making_out background",0,0))
        if state.endswith(("_limp","_dazed")):
          rv.append(("lindsey avatar events making_out lindsey_body1",0,123))
          if state.endswith("_limp"):
            rv.append(("lindsey avatar events making_out lindsey_face1",1499,338))
          elif state.endswith("_dazed"):
            rv.append(("lindsey avatar events making_out lindsey_face2",1498,332))
        elif state.endswith(("_surprised","_happy","_adore")):
          rv.append(("lindsey avatar events making_out lindsey_body2",0,123))
          if state.endswith("_surprised"):
            rv.append(("lindsey avatar events making_out lindsey_face3",1533,336))
          elif state.endswith("_happy"):
            rv.append(("lindsey avatar events making_out lindsey_face4",1537,336))
          elif state.endswith("_adore"):
            rv.append(("lindsey avatar events making_out lindsey_face5",1541,336))
        elif state.endswith(("_kiss","_surprised2","_blush")):
          rv.append(("lindsey avatar events making_out lindsey_body3",0,0))
          if state.endswith("_kiss"):
            rv.append(("lindsey avatar events making_out lindsey_face6",1450,152))
          elif state.endswith("_surprised2"):
            rv.append(("lindsey avatar events making_out lindsey_face7",1450,144))
          elif state.endswith("_blush"):
            rv.append(("lindsey avatar events making_out lindsey_face8",1450,144))
        elif state.endswith(("_adore2","_nervous")):
          rv.append(("lindsey avatar events making_out lindsey_body4",0,0))
          if state.endswith("_adore2"):
            rv.append(("lindsey avatar events making_out lindsey_face9",1494,239))
          elif state.endswith("_nervous"):
            rv.append(("lindsey avatar events making_out lindsey_face10",1480,239))
        if state.endswith(("_limp","_dazed","_surprised","_happy","_adore")):
          if "_workaround" in state:
            rv.append(("lindsey avatar events making_out mc_body2",0,0))
          else:
            rv.append(("lindsey avatar events making_out mc_body1",0,0))
          if "_boob" in state:
            rv.append(("lindsey avatar events making_out mc_arm1",282,0))
          else:
            if "_workaround" not in state:
              rv.append(("lindsey avatar events making_out mc_arm2",166,0))
          if "_hand" in state:
            rv.append(("lindsey avatar events making_out lindsey_arm1",909,430))
            if "_workaround" in state:
              rv.append(("lindsey avatar events making_out mc_arm5",964,0))
          else:
            rv.append(("lindsey avatar events making_out lindsey_arm2",836,617))
        elif state.endswith(("_kiss","_surprised2","_blush","_adore2","_nervous")):
          rv.append(("lindsey avatar events making_out mc_body2",0,0))
          if state.endswith("_nervous"):
            rv.append(("lindsey avatar events making_out mc_arm3",509,0))
          else:
            if state.endswith("_adore2") or "_boob" in state:
              rv.append(("lindsey avatar events making_out mc_arm4",547,0))
          if state.endswith("_adore2"):
            rv.append(("lindsey avatar events making_out lindsey_arm3",871,284))
          elif state.endswith("_nervous"):
            rv.append(("lindsey avatar events making_out lindsey_arm4",608,460))
          else:
            if "_hand" in state:
              rv.append(("lindsey avatar events making_out lindsey_arm5",876,301))
            else:
              rv.append(("lindsey avatar events making_out lindsey_arm6",1200,0))
              if "_boob" not in state:
                rv.append(("lindsey avatar events making_out mc_arm5",964,0))
        rv.append(("lindsey avatar events making_out grass",735,560))

      if state.startswith("roof_ground_"):
        rv.append((1920,2160))
        rv.append(("lindsey avatar events roof ground background",0,0))
        rv.append(("lindsey avatar events roof ground crowd",0,1392))
        if state.endswith("_still"):
          rv.append(("lindsey avatar events roof ground lindsey_body1",941,385))
          rv.append(("lindsey avatar events roof ground lindsey_underwear1",950,442))
          rv.append(("lindsey avatar events roof ground lindsey_bottoms1",945,505))
        elif state.endswith("_wings"):
          rv.append(("lindsey avatar events roof ground lindsey_body2",828,364))
          rv.append(("lindsey avatar events roof ground lindsey_underwear1",950,442))
          rv.append(("lindsey avatar events roof ground lindsey_bottoms2",945,505))
        elif state.endswith("_fall"):
          rv.append(("lindsey avatar events roof ground lindsey_body3",693,417))
          rv.append(("lindsey avatar events roof ground lindsey_underwear2",842,544))
          rv.append(("lindsey avatar events roof ground lindsey_bottoms3",841,612))

      if state.startswith("roof_door"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events roof door background",0,0))
        if state.endswith("still"):
          rv.append(("lindsey avatar events roof door lindsey_shadow1",0,603))
          rv.append(("lindsey avatar events roof door lindsey_body1a",665,214))
          rv.append(("lindsey avatar events roof door lindsey_underwear1",676,276))
          rv.append(("lindsey avatar events roof door lindsey_bottoms1",661,353))
          rv.append(("lindsey avatar events roof door lindsey_body1b",737,287))
        elif state.endswith("wings"):
          rv.append(("lindsey avatar events roof door lindsey_shadow1",0,603))
          rv.append(("lindsey avatar events roof door lindsey_body2",602,206))
          rv.append(("lindsey avatar events roof door lindsey_underwear1",676,276))
          rv.append(("lindsey avatar events roof door lindsey_bottoms1",661,353))
        elif state.endswith("step"):
          rv.append(("lindsey avatar events roof door lindsey_shadow2",0,603))
          rv.append(("lindsey avatar events roof door lindsey_body3",622,222))
          rv.append(("lindsey avatar events roof door lindsey_underwear2",681,285))
          rv.append(("lindsey avatar events roof door lindsey_bottoms2",660,357))
        elif state.endswith("fall"):
          rv.append(("lindsey avatar events roof door lindsey_body4",716,412))
          rv.append(("lindsey avatar events roof door lindsey_underwear3",781,469))
          rv.append(("lindsey avatar events roof door lindsey_bottoms3",716,492))
        if "doorless" not in state:
          rv.append(("lindsey avatar events roof door door",0,0))

      if state.startswith("hospital_room"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events hospital room background",0,0))
        rv.append(("lindsey avatar events hospital room lindsey_body",241,146))
        rv.append(("lindsey avatar events hospital room lindsey_underwear",1247,403))
        rv.append(("lindsey avatar events hospital room lindsey_hospital_gown",885,374))
        if state.endswith(("asleep","drowsy","sad2")):
          rv.append(("lindsey avatar events hospital room lindsey_casts_neckbraceless",243,179))
        else:
          rv.append(("lindsey avatar events hospital room lindsey_casts",243,179))
        if state.endswith("neutral"):
          rv.append(("lindsey avatar events hospital room lindsey_face1",1282,275))
        elif state.endswith("surprised"):
          rv.append(("lindsey avatar events hospital room lindsey_face2",1282,273))
        elif state.endswith(("sad","sad2")):
          rv.append(("lindsey avatar events hospital room lindsey_face3",1282,277))
        elif state.endswith("skeptical"):
          rv.append(("lindsey avatar events hospital room lindsey_face4",1282,277))
        elif state.endswith("afraid"):
          rv.append(("lindsey avatar events hospital room lindsey_face5",1282,272))
        elif state.endswith("crying"):
          rv.append(("lindsey avatar events hospital room lindsey_face6",1286,281))
        elif state.endswith("asleep"):
          rv.append(("lindsey avatar events hospital room lindsey_face7",1285,275))
        elif state.endswith("drowsy"):
          rv.append(("lindsey avatar events hospital room lindsey_face8",1284,275))

      if state.startswith("hospital_bed_angel"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events hospital bed_angel background",0,0))
        if any(expression in state for expression in ("puddle","pleased","napkin_dab","embarrassed")):
          rv.append(("lindsey avatar events hospital bed_angel puddle",0,352))
        rv.append(("lindsey avatar events hospital bed_angel mc_right_arm",1129,196))
        if any(action in state for action in ("hand_under_panties","fully_covered_pussy","sliding_hand_further","sliding_finger_inside","squirt_into_hand","napkin_dab")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_body1",0,0))
        else:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_body2",0,141))
        rv.append(("lindsey avatar events hospital bed_angel lindsey_bra",904,310))
        if any(action in state for action in ("hand_under_panties","fully_covered_pussy","sliding_hand_further","sliding_finger_inside","squirt_into_hand","napkin_dab")):
          pass
        else:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_panties1",529,293))
        if any(action in state for action in ("hand_under_panties","fully_covered_pussy","sliding_hand_further","sliding_finger_inside","squirt_into_hand","napkin_dab")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_hospital_gown1",477,281))
        else:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_hospital_gown2",53,143))
        if "fingertips_against_cheek" in state:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_arm1",717,0))
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_sleeve1",933,119))
        elif any(action in state for action in ("cupping_breast","hand_under_panties","squirt_into_hand","pleased","blush fully_covered_pussy")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_arm2",1194,0))
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_sleeve2",1131,18))
        elif any(action in state for action in ("fully_covered_pussy","sliding_hand_further","sliding_finger_inside","napkin_dab")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_arm3",954,232))
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_sleeve3",956,228))
        else:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_arm4",519,212))
          rv.append(("lindsey avatar events hospital bed_angel lindsey_left_sleeve4",933,211))
        if any(expression in state for expression in ("crying","surprised","blush","pleading")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_head1",1085,118))
          if "crying" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face1",1214,236))
          elif "surprised" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face2",1266,236))
          elif "blush" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face3",1272,236))
          elif "pleading" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face4",1256,236))
        elif any(expression in state for expression in ("sad","closed_eyes","sleeping","sleepy","smile","aroused","embarrassed")) and "closed_eyes_once_more" not in state:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_head2",1081,110))
          if "sad" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face5",1253,234))
          elif "closed_eyes" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face6",1205,234))
          elif "sleeping" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face7",1256,234))
          elif "sleepy" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face8",1257,234))
          elif "smile" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face9",1271,234))
          elif any(expression in state for expression in ("aroused","embarrassed")):
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face10",1260,234))
        elif any(expression in state for expression in ("admiring","kiss","closed_eyes_once_more","ahegao","squirt","puddle","pleased")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_head3",1087,122))
          if "admiring" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face11",1288,234))
          elif "kiss" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face12",1248,234))
          elif "ahegao" in state:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face13",1242,234))
          elif any(expression in state for expression in ("squirt ","puddle")):
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face14",1271,234))
          elif any(expression in state for expression in ("closed_eyes_once_more","pleased")):
            rv.append(("lindsey avatar events hospital bed_angel lindsey_face15",1288,234))
        elif "embraced" in state:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_head4",1085,118))
          rv.append(("lindsey avatar events hospital bed_angel lindsey_face16",1262,236))
        if "hand_against_chest" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm1",462,347))
        elif "fingertips_against_cheek" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm2",462,347))
        elif "cupping_breast" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm3",714,297))
        elif "hand_under_panties" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm4",557,338))
        elif "fully_covered_pussy" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm5",337,434))
        elif "sliding_hand_further" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm6",303,423))
        elif "sliding_finger_inside" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm7",292,421))
        elif "squirt_into_hand" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm8",203,472))
        if "hand_under_panties" in state:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_panties2",529,293))
        elif "fully_covered_pussy" in state:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_panties3",501,294))
        elif "sliding_hand_further" in state:
          rv.append(("lindsey avatar events hospital bed_angel lindsey_panties4",504,294))
        elif "sliding_finger_inside" in state:
          if any(expression in state for expression in ("squirt","puddle","pleased")):
            rv.append(("lindsey avatar events hospital bed_angel lindsey_panties5",497,294))
            if "squirt" in state:
              rv.append(("lindsey avatar events hospital bed_angel lindsey_squirt",0,206))
          else:
            rv.append(("lindsey avatar events hospital bed_angel lindsey_panties6",497,294))
        elif any(action in state for action in ("squirt_into_hand","napkin_dab")):
          rv.append(("lindsey avatar events hospital bed_angel lindsey_panties7",520,294))
        if "napkin_dab" in state:
          rv.append(("lindsey avatar events hospital bed_angel mc_left_arm9",350,393))

      elif state.startswith("roof_floor"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events roof floor background",0,0))
        rv.append(("lindsey avatar events roof floor lindsey_body",197,0))
        rv.append(("lindsey avatar events roof floor lindsey_underwear",619,329))
        rv.append(("lindsey avatar events roof floor lindsey_skirt",840,695))
        if state.endswith("scared"):
          rv.append(("lindsey avatar events roof floor lindsey_face1",376,175))
        elif state.endswith("crying"):
          rv.append(("lindsey avatar events roof floor lindsey_face2",376,175))
        elif state.endswith("admiring"):
          rv.append(("lindsey avatar events roof floor lindsey_face3",376,175))
        rv.append(("lindsey avatar events roof floor mc_body",561,536))

      elif state.startswith("hospital_bed_voluntary"):
        rv.append((1920,1080))
        rv.append(("lindsey avatar events hospital bed_voluntary background",0,0))
        rv.append(("lindsey avatar events hospital bed_voluntary lindsey_body",0,40))
        rv.append(("lindsey avatar events hospital bed_voluntary lindsey_underwear",253,286))
        rv.append(("lindsey avatar events hospital bed_voluntary lindsey_hospital_gown",17,199))
        if state.endswith("awake"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_face1",1346,230))
        elif state.endswith("mc_chest"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_face2",1355,230))
        elif state.endswith("lindsey_chest"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_face3",1353,230))
        elif state.endswith("vitals_check"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_face4",1353,230))
        elif state.endswith("forehead_kiss"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_face5",1353,228))
        rv.append(("lindsey avatar events hospital bed_voluntary mc_body",0,400))
        if state.endswith(("awake","mc_chest","lindsey_chest","vitals_check")):
          rv.append(("lindsey avatar events hospital bed_voluntary mc_head1",1571,395))
        elif state.endswith("forehead_kiss"):
          rv.append(("lindsey avatar events hospital bed_voluntary mc_head2",1555,237))
        if state.endswith("lindsey_chest"):
          rv.append(("lindsey avatar events hospital bed_voluntary mc_left_arm1",692,324))
        elif state.endswith("vitals_check"):
          rv.append(("lindsey avatar events hospital bed_voluntary mc_left_arm2",692,406))
        if state.endswith(("awake","vitals_check","forehead_kiss")):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_right_arm1",6,178))
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_right_sleeve1",704,176))
        elif state.endswith("mc_chest"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_right_arm2",612,178))
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_right_sleeve2",702,175))
        elif state.endswith("lindsey_chest"):
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_right_arm3",467,170))
          rv.append(("lindsey avatar events hospital bed_voluntary lindsey_right_sleeve3",627,168))

      return rv


  class Interactable_lindsey(Interactable):

    def title(cls):
      return lindsey.name

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
    ###############talks####################
      if not (quest.lindsey_wrong in ("lindseyart","florahelp","floralocker","lindseyclothes")
      or quest.lindsey_piano in ("pick_up","listen_bedroom")
      or (quest.lindsey_piano == "listen_music_class" and quest.lindsey_piano["lindsey_arrived"])
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or quest.jo_washed.in_progress):
        if lindsey["romance_disabled"] and (quest.lindsey_book.failed or quest.maxine_eggs["lindsey_photo"] or quest.lindsey_piano.failed):
          actions.append(["talk","Talk","?lindsey_talk_romance_disabled"])
        else:
          if lindsey["talk_limit_today"]<3:
            actions.append(["talk","Talk","?lindsey_talk_one"])
            actions.append(["talk","Talk","?lindsey_talk_two"])
            actions.append(["talk","Talk","?lindsey_talk_three"])
            actions.append(["talk","Talk","?lindsey_talk_four"])
            actions.append(["talk","Talk","?lindsey_talk_five"])
          else:
            actions.append(["talk","Talk","?lindsey_talk_over"])
      #######################################

      ##############Quests####################
      if mc["focus"]:
        if mc["focus"] == "lindsey_wrong":
          if quest.lindsey_wrong == "lindseyart" and mc.at("school_art_class"):
            actions.append(["quest","Quest","?quest_lindsey_wrong_lindseyart"])
          elif quest.lindsey_wrong in ("florahelp","floralocker") and mc.at("school_art_class"):
            actions.append(["quest","Quest","?quest_lindsey_wrong_hurry_up"])
          elif quest.lindsey_wrong == "lindseyclothes" and mc.at("school_art_class"):
            actions.append(["quest","Quest","?quest_lindsey_wrong_lindseyclothes"])
        elif mc["focus"] == "lindsey_piano":
          if quest.lindsey_piano == "listen_music_class" and quest.lindsey_piano["lindsey_arrived"]:
            if quest.lindsey_piano["stereo_system"]:
              actions.append(["quest","Quest","quest_lindsey_piano_listen_music_class_lindsey"])
            else:
              actions.append(["quest","Quest","?quest_lindsey_piano_listen_music_class_lindsey_ready"])
          elif quest.lindsey_piano == "pick_up":
            actions.append(["quest","Quest","quest_lindsey_piano_pick_up_lindsey"])
          elif quest.lindsey_piano == "listen_bedroom":
            actions.append(["quest","Quest","quest_lindsey_piano_listen_bedroom_lindsey"])
        elif mc["focus"] == "isabelle_dethroning":
          if quest.isabelle_dethroning == "dinner" and game.hour == 19:
            if "lindsey" not in quest.isabelle_dethroning["dinner_conversations"]:
              actions.append(["quest","Quest","quest_isabelle_dethroning_dinner_lindsey"])
            if "maxine" in quest.isabelle_dethroning["dinner_conversations"] and not quest.isabelle_dethroning["dinner_picture"]:
              actions.append(["camera","Take a picture","quest_isabelle_dethroning_dinner_picture_lindsey"])
        elif mc["focus"] == "jo_washed":
          if quest.jo_washed.in_progress:
            actions.append(random.choice(quest_jo_washed_interact_lines))
      else:
        #quest lindsey_book interactions we need to possibly simplify this.
          #if quest.isabelle_tour.finished:
          if not quest.lindsey_book.in_progress:
            if not quest.lindsey_book.failed:
              if not quest.lindsey_making_up.started:
                if not quest.lindsey_book.finished:
                  actions.append(["quest","Quest","quest_lindsey_book_start"])
              else:
                if quest.lindsey_making_up.in_progress:
                  if quest.lindsey_making_up=="start":
                    actions.append(["give","Give","select_inventory_item","$quest_item_filter:lindsey_making_up,start|Give What?","quest_lindsey_book_apology_gift"])
                  elif quest.lindsey_making_up=="second_item":
                    actions.append(["give","Give","select_inventory_item","$quest_item_filter:lindsey_making_up,second_item|Give What?","quest_lindsey_book_apology_gift_two"])
            else:
              pass# tbd redemption option for those that previously failed or just do nothing?
          else:#quest already in progress

            if quest.lindsey_book=="suggest":
              actions.append(["give","Give", "select_inventory_item","$quest_item_filter:lindsey_book,suggest|Give What?","quest_lindsey_book_suggest"])

            elif quest.lindsey_book == "feedback":
              actions.append(["quest","Quest","quest_lindsey_book_feedback"])

            elif quest.lindsey_book == "easel_created" and game.location == "school_entrance":
              actions.append(["give","Give", "select_inventory_item","$quest_item_filter:lindsey_book,easel_created|Give What?","quest_lindsey_book_easel"])

          if quest.isabelle_stolen.in_progress:
            if quest.isabelle_stolen == "askmaxine":
              actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_lindsey"])
            elif quest.isabelle_stolen == "slap":
              actions.append(["quest","Quest","?isabelle_quest_stolen_slap"])
            elif quest.isabelle_stolen == "dilemma" and not quest.isabelle_stolen["lindsey_talk"]:
              actions.append(["quest","Quest","?isabelle_quest_stolen_lindsey_dilemma"])

          if quest.mrsl_HOT.in_progress and quest.mrsl_HOT == "lindsey_talk":
            actions.append(["quest","Quest","?mrsl_quest_lindsey_talk"])

          if quest.isabelle_buried.in_progress:
            if quest.isabelle_buried == "lindsey":
              actions.append(["quest","Quest","?isabelle_quest_buried_start_lindsey"])

          if quest.lindsey_wrong.in_progress:
            if quest.lindsey_wrong == "follow":
              actions.append(["quest","Quest","?quest_lindsey_wrong_follow"])
            if quest.lindsey_wrong == "drinks" and mc.at("school_gym"):
              actions.append(["quest","Quest","?quest_lindsey_wrong_gym"])

          if quest.isabelle_locker == "interrogate" and not quest.isabelle_locker["lindsey_interrogated"] and not quest.isabelle_locker["might_have_an_idea"]:
            actions.append(["quest","Quest","quest_isabelle_locker_interrogate_lindsey"])

          if quest.maxine_hook == "lindsey":
            actions.append(["quest","Quest","quest_maxine_hook_lindsey"])

          if quest.lindsey_piano == "nurse" and quest.lindsey_piano["follow_me"]:
            actions.append(["quest","Quest","quest_lindsey_piano_nurse_follow"])
          elif quest.lindsey_piano == "lindsey_talk":
            actions.append(["quest","Quest","quest_lindsey_piano_lindsey_talk"])

          if quest.lindsey_motive == "lindsey":
            actions.append(["quest","Quest","quest_lindsey_motive_lindsey"])

          if quest.isabelle_dethroning == "invitations" and "lindsey" not in quest.isabelle_dethroning["invitations"]:
            actions.append(["quest","Quest","quest_isabelle_dethroning_invitations_lindsey"])
          ############################################

          if game.season == 1:
            if quest.isabelle_tour.finished and quest.lindsey_book.ended and not quest.lindsey_wrong.started:
              actions.append(["quest","Quest","?quest_lindsey_wrong_start"])

            if quest.isabelle_tour.finished and quest.lindsey_book.ended and quest.lindsey_wrong.finished and not lindsey["romance_disabled"] and not quest.lindsey_piano.started:
              actions.append(["quest","Quest","?quest_lindsey_piano_start"])

            if (quest.isabelle_tour.finished and quest.lindsey_book.finished and quest.lindsey_wrong.finished and quest.lindsey_piano.finished and quest.jacklyn_broken_fuse.finished and quest.isabelle_stolen.finished and quest.isabelle_buried.finished and quest.maxine_eggs.finished and not quest.lindsey_motive.started
            and game.location == "school_gym"):
              actions.append(["quest","Quest","?quest_lindsey_motive_start"])
      ################################################

      ##############flirts##############################
      if not (quest.lindsey_wrong in ("lindseyart","florahelp","floralocker","lindseyclothes")
      or quest.lindsey_piano in ("pick_up","listen_bedroom")
      or (quest.lindsey_piano == "listen_music_class" and quest.lindsey_piano["lindsey_arrived"])
      or (quest.isabelle_dethroning == "dinner" and game.hour == 19)
      or quest.isabelle_dethroning == "trap"
      or quest.jo_washed.in_progress):
        if lindsey["romance_disabled"] and (quest.lindsey_book.failed or quest.maxine_eggs["lindsey_photo"] or quest.lindsey_piano.failed):
          pass
        else:
          if (lindsey["flirt_limit_today"]<3):
            actions.append(["flirt","Flirt","?lindsey_flirt_one"])
            actions.append(["flirt","Flirt","?lindsey_flirt_two"])
            actions.append(["flirt","Flirt","?lindsey_flirt_three"])
            actions.append(["flirt","Flirt","?lindsey_flirt_four"])
            actions.append(["flirt","Flirt","?lindsey_flirt_five"])
          else:
            actions.append(["flirt","Flirt","?lindsey_flirt_over"])
      ################################################

    def circle(cls,actions,x,y):
      rv =super().circle(actions,x,y)
      if game.location == "school_english_class":
        pass
        rv.start_angle=90
        #rv.angle_per_icon= 30
        rv.center=(-50,70)
        #rv.radius = 150
      return rv


label lindsey_talk_romance_disabled:
  "I don't think [lindsey] wants to talk to me after what I did to her..."
  return

label lindsey_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label lindsey_flirt_over:
  "She must be sick of my poor attempts at flirting by now..."
  return

label lindsey_talk_one:
  show lindsey thinking with Dissolve(.5)
  lindsey thinking "Maybe I'm a bit old-fashioned, but I think saving yourself for marriage is romantic."
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_talk_two:
  show lindsey smile with Dissolve(.5)
  lindsey smile "Mom always says that racing is in my blood, but sometimes I wish I could stop for a moment and catch my breath."
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_talk_three:
  show lindsey sad with Dissolve(.5)
  lindsey sad "My family has always been very competitive. One time, I placed third in a junior race."
  lindsey "When we got home, Dad spent an hour in the woodshed splitting logs and Mom cried more than when granny died. I was four years old."
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_talk_four:
  show lindsey excited with Dissolve(.5)
  lindsey excited "I went to a jousting tournament once with real knights!"
  lindsey blush "The champion picked me out from the crowd and took me on a ride around the arena."
  lindsey excited "Best day ever!"
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_talk_five:
  show lindsey laughing with Dissolve(.5)
  lindsey laughing "That's a bit forward of you, but no. I've never had a boyfriend and I'm not looking for one!"
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_talk_six:
  show lindsey neutral with Dissolve(.5)
  lindsey neutral "The school administration wants me to compete for Newfall High, but my parents and my coach agree that it's best to stay independent."
  lindsey smile "That's why none of my trophies are displayed outside the gym."
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_talk_seven:
  show lindsey neutral with Dissolve(.5)
  lindsey neutral "Have you ever been out on a yacht? My dad owns the second largest one in Newfall."
  lindsey "I miss going on cruises with [maxine] and a couple of close friends."
  $lindsey["talk_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_flirt_one:
  show lindsey excited with Dissolve(.5)
  lindsey excited "I started competing at a very young age."
  lindsey excited "Do you know why?"
  show lindsey excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Racing is in your blood.\"":
      show lindsey excited at move_to(.5)
      mc "Racing is in your blood."
      lindsey thinking "Well, my mom likes to say that, but I was actually pretty slow when I first started."
    "\"Your parents got you into it.\"":
      show lindsey excited at move_to(.5)
      mc "Your parents got you into it."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.love+=1
      lindsey neutral "They did! My mom bought me my first pair of running shoes when I was three."
    "\"You wanted to get a good scholarship.\"":
      show lindsey excited at move_to(.5)
      mc "You wanted to get a good scholarship."
      lindsey smile "Not really... my parents are pretty loaded, so I'll get into whatever college I want as long as I keep my grades up."
    "\"You've always admired champions.\"":
      show lindsey excited at move_to(.5)
      mc "You've always admired champions."
      lindsey blush "I've always loved knights and the champions of jousting tournaments!"
      lindsey neutral "But I don't care as much for racing champions."
  $lindsey["flirt_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_flirt_two:
  show lindsey smile with Dissolve(.5)
  lindsey smile "I enjoy going out with friends sometimes."
  lindsey smile "Do you know what my favorite venues are?"
  show lindsey smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Charter cruises.\"":
      show lindsey smile at move_to(.5)
      mc "Charter cruises."
      lindsey laughing "Not really! I like going on yachts, though!"
    "\"Jousting tournaments.\"":
      show lindsey smile at move_to(.5)
      mc "Jousting tournaments."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.lust+=1
      lindsey blush "Oh, yes!"
      lindsey blush "I love watching knights in full armor, charging down the lists on their mighty steeds!"
    "\"Olympic races.\"":
      show lindsey smile at move_to(.5)
      mc "Olympic races."
      lindsey laughing "You'd think so, but I honestly don't enjoy watching other people run!"
    "\"Cozy nightclubs.\"":
      show lindsey smile at move_to(.5)
      mc "Cozy nightclubs."
      lindsey thinking "I don't think I've ever even been to one. Not at all my type of venue..."
  $lindsey["flirt_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_flirt_three:
  show lindsey smile with Dissolve(.5)
  lindsey smile "Did you know I've never been on a real date with a guy? Do you know why?"
  show lindsey smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You're too fast. No one's been able to catch you yet.\"":
      show lindsey smile at move_to(.5)
      mc "You're too fast. No one's been able to catch you yet."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.love+=1
      lindsey laughing "That's sweet and funny, but not really why!"
    "\"Well, if you weren't such a self-centered primadonna, guys might be more interested.\"":
      show lindsey smile at move_to(.5)
      mc "Well, if you weren't such a self-centered primadonna, guys might be more interested."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.love-=1
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.lust-=1
      lindsey annoyed "Forget I asked."
    "\"All the dudes here are dicks. I don't blame you.\"":
      show lindsey smile at move_to(.5)
      mc "All the dudes here are dicks. I don't blame you."
      lindsey skeptical "That's not true!"
    "\"You'd like to save yourself for marriage.\"":
      show lindsey smile at move_to(.5)
      mc "You'd like to save yourself for marriage."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.lust+=1
      lindsey laughing "It's a bit weird, right?"
      lindsey laughing "But I find the thought really romantic. I'm a strong believer in true love."
  $lindsey["flirt_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_flirt_four:
  show lindsey sad with Dissolve(.5)
  lindsey sad "I used to be best friends with another student here. Do you know who?"
  show lindsey sad at move_to(.25)
  menu(side="right"):
    extend ""
    "\"[kate]. You've both always been part of the jock crowd.\"":
      show lindsey sad at move_to(.5)
      mc "[kate]. You've both always been part of the jock crowd."
      lindsey neutral "[kate] and I are friends, but I wouldn't say we're besties."
    "\"Was it [maya]? You're both so adorable, I could totally picture it.\"":
      show lindsey sad at move_to(.5)
      mc "Was it [maya]? You're both so adorable, I could totally picture it."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.love+=1
      lindsey blush "Wrong, but... aw, that's so nice of you!"
      lindsey blush "She's a real cutie! And I totally don't mind being compared to her!"
    "\"[maxine], right? You built that wooden ship together and I know you miss sailing on the yacht with her.\"":
      show lindsey sad at move_to(.5)
      mc "[maxine], right? You built that wooden ship together and I know you miss sailing on the yacht with her."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.love+=1
      lindsey sad "You're right. I do miss her a lot."
      mc "So, what happened?"
      lindsey sad "Well... I guess we grew apart. This isn't really a good time to get into the details."
    "\"[isabelle]? I don't know. I'm honestly just guessing.\"":
      show lindsey sad at move_to(.5)
      mc "[isabelle]? I don't know. I'm honestly just guessing."
      lindsey laughing "No! [isabelle] just started here. Why would you think that?"
  $lindsey["flirt_limit_today"]+=1
  hide lindsey with Dissolve(.5)
  return

label lindsey_flirt_five:
  show lindsey flirty with Dissolve(.5)
  lindsey flirty "Wanna race?"
  mc "Not really, no."
  lindsey laughing "Then how about a quiz?"
  mc "I'd be down for that."
  lindsey smile "Okay! Do you know how many trophies I have outside the gym?"
  show lindsey smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Probably many. Five at least.\"":
      show lindsey smile at move_to(.5)
      mc "Probably many. Five at least."
      lindsey laughing "Wrong! But nice try!"
    "\"Three. You've won all the high school district marathons so far.\"":
      show lindsey smile at move_to(.5)
      mc "Three. You've won all the high school district marathons so far."
      lindsey laughing "Wrong! Well, it is true that I've won all three, but those trophies aren't outside the gym."
    "\"Just one, but this year you're going for your second win.\"":
      show lindsey smile at move_to(.5)
      mc "Just one, but this year you're going for your second win."
      lindsey thinking "If I'd only won once, my parents would probably have disowned me."
    "\"None.\"":
      show lindsey smile at move_to(.5)
      mc "None."
      if not lindsey["flirt_bonuses_chosen"]:
        $lindsey.love+=1
      lindsey excited "Correct! I keep all my trophies and medals at home!"
  $lindsey["flirt_limit_today"]+=1
  $lindsey["flirt_bonuses_chosen"] = True
  hide lindsey with Dissolve(.5)
  return
