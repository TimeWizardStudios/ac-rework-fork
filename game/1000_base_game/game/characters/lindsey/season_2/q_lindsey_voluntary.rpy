label quest_lindsey_voluntary_start:
  "God, whenever I see the hospital I get the urge to visit [lindsey]..."
  "She's like a princess, locked up in her tower."
  # "Maybe they've forgotten my face since last time, and I can sneak in?"
  "Maybe they've forgotten my face since last time, and I can sneak in?{space=-10}"
  "..."
  "I can't leave her alone in there. My heart won't allow it."
  window hide
  call goto_hospital
  window auto
  "Fuck. This just got a whole lot trickier."
  "Did they seriously hire a guard because of me?"
  "I guess it wouldn't surprise me if [lindsey]'s parents demanded it..."
  "..."
  "I won't let some guard stop me from seeing her, though."
  "Maybe this is one of those times when cheating your way through isn't the way."
  "What would be an honest way of gaining access to the hospital?"
  "Hmm..."
  "Maybe there's some kind of volunteer work?"
  mc "Excuse me, ma'am!"
  $nurse.default_name = "{size=30}Receptionist{/}"
  nurse "How may I help you?"
  mc "I was wondering if there's any volunteer work available?"
  mc "I would really like to help the sick and elderly..."
  "...but most importantly, the young and beautiful."
  nurse "There might be. Fill in this form, please."
  $nurse.default_name = "Nurse"
  "Ugh, forms..."
  mc "Err, thanks!"
  "Let's see here..."
  "..."
  "Name... [mc]."
  "Age... do I put my old life age, this age, or do I add them together?"
  "I do sometimes feel like I have a midlife crisis."
  "..."
  "Past experience...?"
  "Err, does playing a holy priest in WoW count?"
  "..."
  "I probably should take this seriously."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.advance()
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Okay, all done! Hopefully this will do."
  "It says they will contact me in the coming days."
  "Meanwhile, I can score some brownie points with [jo] for being such a stand up citizen..."
  "...and maybe even annoy [flora] a bit in the process."
  window hide
  $quest.lindsey_voluntary.start()
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
    pause 0.25
    window auto
    "It's getting late. I can't wait to get home."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.location = "home_kitchen"
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
    window auto
  return

label quest_lindsey_voluntary_wait_on_advance:
  $game.skip_events(force_go_home_allowed_events)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's getting late. I can't wait to get home."
  if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
    "Just have to put [maxine]'s priced paper back in the stack first. Leaving with it will get me in so much trouble."
    "She doesn't mess around."
    $mc.remove_item("paper")
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_kitchen"
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  window auto
  return

label quest_lindsey_voluntary_wait:
  show flora confused with Dissolve(.5)
  flora confused "Where have you been all day?"
  mc "Just out and about. Getting into volunteer work. No big deal."
  flora annoyed "You're bluffing."
  mc "Me? I would never!"
  mc "I'm all about helping others. Especially the sick and elderly!"
  flora sarcastic "Ah, I get it."
  mc "Get what?"
  flora sarcastic "You've never shown interest in volunteer work before..."
  flora sarcastic "...but as soon as [lindsey]'s in the hospital, you're all over it."
  mc "[lindsey]'s in the hospital? What happened?!"
  flora annoyed "Nice try."
  show flora annoyed at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I thought she could use some company. You know how her parents are.\"":
      show flora annoyed at move_to(.5)
      # mc "I thought she could use some company. You know how her parents are."
      mc "I thought she could use some company. You know how her parents are.{space=-65}"
      flora thinking "Hmm... I guess I would get bored too if I were in a cast..."
      flora blush "That's actually kind of sweet of you."
      mc "Sweet, brave, chivalrous — pick as many as you like."
      flora laughing "How about obnoxious?"
      mc "That's way too big of a word for me. I'm a simple man with a simple code of honor."
      flora laughing "Simp is right!"
      mc "Don't you have homework to do?"
      flora thinking "Don't you have your own business to mind?"
      mc "Oh, absolutely. Volunteer work, to be precise."
      mc "Selflessness. The good cause. All that jazz."
      flora thinking "..."
    "\"She needs me.\"":
      show flora annoyed at move_to(.5)
      mc "She needs me."
      if flora.love >= 10 or flora.lust >= 10:
        flora thinking "What's that supposed to mean?"
        mc "Err, that she relies on me?"
        flora thinking "You just said the same thing with different words."
        mc "I guess I feel obligated to protect her in a way..."
        flora worried "Do you feel obligated to protect me?"
        mc "Very funny."
        flora worried "I don't see anyone laughing. Do you?"
        mc "..."
        mc "[lindsey] is, well, special. She's like a princess."
        mc "Whereas you're more like a... witch? A really cute one, of course!"
        $flora.love-=3
        flora angry "And you're like a gravedigger, digging your own grave."
        "Shit. She looks genuinely angry."
        "I probably should've kept my mouth shut..."
        mc "Err, am I free to go?"
        flora concerned "Yes, but I'll curse your bloodline for seven generations if you do something stupid."
        window hide
        show flora concerned at disappear_to_left(.75)
        pause 0.75
        play sound "<from 9.2 to 10.2>door_breaking_in"
        show location with hpunch
        pause 0.25
        window auto
        mc "See? This is what I was talking about!"
        "Oh, well. She'll get over it."
        "In the meantime, I should probably do some investigating."
        "Everyone thinks [lindsey]'s fall was a suicide attempt, but I know better."
        $flora["at_none_this_scene"] = True
        $quest.lindsey_voluntary.advance("investigation")
        return
      else:
        flora confused "Oh? I didn't know you were that close."
        mc "We're pretty tight. Besties, basically."
        flora sarcastic "I knew you were in the friendzone."
        mc "Everyone needs friends, [flora]."
        mc "Do you want to be friends?"
        flora annoyed "Shut up."
        mc "You need some time to think about it? Understood."
        flora annoyed "..."
    "\"Oh, my. Is that jealousy?\"":
      show flora annoyed at move_to(.5)
      mc "Oh, my. Is that jealousy?"
      flora cringe "What? No, it's not!"
      mc "It is, isn't it? I should've known!"
      flora cringe "I will kill you!"
      window hide
      if jo.location == "home_kitchen":
        show flora cringe at move_to(.75)
        show jo skeptical at Transform(xalign=.25) with Dissolve(.5)
      else:
        show jo skeptical at appear_from_left(.25)
        show flora cringe:
          ease 1.0 xalign 0.75
        pause 0.5
      window auto
      jo skeptical "Hello, you two."
      jo skeptical "Is everything okay?"
      # mc "I was just telling [flora] about my upcoming volunteer work at the hospital."
      mc "I was just telling [flora] about my upcoming volunteer work at the hospital.{space=-110}"
      mc "She's really excited about it!"
      flora cringe "Ugh!"
      $jo.love+=1
      jo smile "That's amazing news, sweetheart!"
      jo smile "I'm so proud of you! That place is criminally understaffed!"
      flora annoyed "Whatever."
      jo concerned "Now, now, [flora]. No need to be a sauerkraut."
      jo concerned "[mc] is doing a good and selfless thing here."
      jo concerned "You should encourage him more."
      flora eyeroll "He definitely doesn't need encouragement."
      jo confident "I have to go get changed. Be good, you two."
      window hide
      show jo confident at disappear_to_left
      show flora eyeroll:
        ease 1.0 xalign 0.5
      show flora annoyed with Dissolve(.5)
      $jo["at_none_this_scene"] = True
      pause 0.5
      window auto
      flora annoyed "..."
      mc "What?"
      flora annoyed "You won't get away with this."
      mc "You better watch out, [flora]. Maybe I'll be her favorite soon?"
      flora cringe "Shut up!"
  if renpy.showing("flora cringe"):
    # "As much as I'd like to spend the rest of my days annoying her, I should probably do some investigating."
    "As much as I'd like to spend the rest of my days annoying her, I should{space=-60}\nprobably do some investigating."
  else:
    # "As much as I'd like to spend the rest of my days annoying [flora], I should probably do some investigating."
    "As much as I'd like to spend the rest of my days annoying [flora],\nI should probably do some investigating."
  "Everyone thinks [lindsey]'s fall was a suicide attempt, but I know better."
  mc "Catch you later!"
  window hide
  hide flora with Dissolve(.5)
  $flora["at_kitchen_this_scene"] = True
  $quest.lindsey_voluntary.advance("investigation")
  return

label quest_lindsey_voluntary_investigation:
  $maxine["outfit_stamp"] = maxine.outfit
  $maxine.outfit = {"panties":"maxine_black_panties", "bra":"maxine_black_bra", "necklace":"maxine_necklace", "glasses":"maxine_glasses", "shirt":"maxine_hazmat_suit"}
  show maxine confident with Dissolve(.5)
  mc "[maxine]? What exactly are you doing?"
  maxine confident "Concocting concoctions."
  show maxine confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Why are you in a hazmat suit?\"":
      show maxine confident at move_to(.5)
      mc "Why are you in a hazmat suit?"
      maxine eyeroll "For my protection, of course."
      mc "From what?"
      maxine eyeroll "Dangerous chemicals."
      mc "Are those even allowed in here?"
      maxine confident "Why, yes. This is a laboratory, after all."
      mc "This is a science classroom..."
      maxine skeptical "There are lab coats in here."
      mc "So what?"
      maxine skeptical "So, a lab coat is a coat worn in a laboratory. It's in the name."
      mc "..."
      mc "Is it safe for me to be in here?"
      maxine skeptical "More or less."
      mc "More or less? Am I going to die?!"
      maxine annoyed "I would assume so."
      mc "What the hell, [maxine]?!"
      # maxine annoyed "I'm sorry to be the bearer of bad news, but life is temporary for most."
      maxine annoyed "I'm sorry to be the bearer of bad news, but life is temporary for most.{space=-30}"
      "Ugh..."
    "\"What sort of concoctions?\"":
      show maxine confident at move_to(.5)
      mc "What sort of concoctions?"
      maxine skeptical "The mind-altering kind."
      if quest.nurse_photogenic.actually_finished:
        mc "Is that what you put in the strawberry juice?"
        maxine skeptical "Indeed. Albeit an earlier iteration."
        mc "So, you admit to drugging me?"
        maxine skeptical "Why, yes. Unless you got the placebo?"
        mc "I fainted, [maxine]!"
        # maxine eyeroll "That could have just been a combination of stairs and poor stamina."
        maxine eyeroll "That could have just been a combination of stairs and poor stamina.{space=-10}"
        "Harsh, but fair..."
      else:
        mc "What the hell? That has to be against the law!"
        maxine skeptical "It's not against the laws of physics..."
        maxine skeptical "...not yet, at least."
        mc "..."
        mc "I really don't think you should be taking mind-altering drugs."
        maxine annoyed "It's the only way to survive the impending juice apocalypse."
        mc "Fine. Do what you want, I guess."
        mc "Just don't drug anyone else."
        maxine annoyed "I'm afraid it's too late for that."
        mc "What?!"
        maxine skeptical "The concoction needed testing."
        maxine skeptical "Don't worry. The side effects aren't lethal... for the most part."
        mc "Who did you give this to, [maxine]?"
        maxine eyeroll "That's a matter of patient confidentiality."
        mc "Ugh!"
    "\"Take that thing off so I can fuck you.\"":
      show maxine confident at move_to(.5)
      mc "Take that thing off so I can fuck you."
      maxine confident "Good. Very good."
      "Huh. That's not the response I was expecting."
      mc "Err, what's good?"
      maxine confident "A heightened libido is one of the milder side effects."
      mc "Side effects of what?!"
      maxine skeptical "Of the concoction you ingested."
      mc "I didn't ingest any concoction!"
      maxine skeptical "Are you sure about that?"
      mc "Yes, I am!"
      if quest.nurse_photogenic.actually_finished and not quest.nurse_photogenic["eavesdropped"]:
        maxine skeptical "That's strange, because I observed you ingest the concoction, walk up the stairs, and then faint."
        maxine skeptical "Of course, at the time, you believed it to be strawberry juice."
        mc "Did you drug me without me knowing?!"
        maxine confident_hands_down "Indeed. I did it while wearing a blindfold, too."
        mc "What...?"
        maxine confident_hands_down "It's what we call a double blind test."
        mc "That's not at all what a double blind test is!"
        maxine skeptical "Are you sure? I figured if the concoction knocked you out, it'd count as being blind momentarily."
        maxine skeptical "But perhaps I should've equipped you with a blindfold, as well?"
      else:
        maxine skeptical "That's odd. Your name is on my list of test subjects."
        mc "What? I don't remember signing up for that."
        maxine confident_hands_down "That's because {i}I{/} signed you up."
        mc "Erase me from that list, [maxine]."
        maxine annoyed "I'm afraid I can't."
        mc "Why the hell not?"
        maxine annoyed "I wrote it in ink."
      mc "Ugh!"
  mc "Anyway, I have some questions about the night when [lindsey]—"
  mc "...err, about the night of the Independence Day dinner."
  maxine confident "What knowledge do you seek?"
  show maxine confident at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What do you know about [maya]?\"":
      show maxine confident at move_to(.5)
      mc "What do you know about [maya]?"
      maxine skeptical "As with every student here, she's in my files."
      mc "Can I have a look at her file?"
      maxine skeptical "No, that's classified."
      mc "Why?"
      maxine eyeroll "Because the council has decided so."
      mc "What freaking council?!"
      mc "You know what? I don't care. Give me her file."
      maxine eyeroll "You don't have the clearance for it."
      mc "You're starting to piss me off, [maxine]."
      # mc "Do you know her whereabouts during the Independence Day dinner?"
      mc "Do you know her whereabouts during the Independence Day dinner?{space=-10}"
      maxine skeptical "I do not."
      mc "I take it she wasn't in the school, then?"
      maxine skeptical "If she was, she would've had to be as stealthy as a cat."
      mc "Hmm..."
      mc "What about dolls? Does she like dolls?"
      maxine confident_hands_down "She indeed appears to have a special fondness for them."
      maxine confident_hands_down "She once gifted me a knitted [spinach] doll."
      "So, [cyberia] {i}was{/} right..."
    # "\"Have you visited or spoken to [lindsey] after the incident?\"":
    "\"Have you visited or spoken to\n[lindsey] after the incident?\"":
      show maxine confident at move_to(.5)
      mc "Have you visited or spoken to [lindsey] after the incident?"
      maxine annoyed "I have not."
      mc "How come?"
      maxine annoyed "The hospital doesn't allow visitors."
      mc "Since when would something like that stop you?"
      maxine eyeroll "Since... well..."
      "It's odd to see [maxine] at a loss for words."
      mc "I don't know what happened between you two, but I think she would like to see you."
      maxine eyeroll "..."
      mc "Seriously, [maxine]. Go visit her."
      maxine annoyed "I don't think it would be a good idea."
      "She seems almost human now... and it's weirding me out."
      mc "Just think about it, okay?"
      mc "She really needs her friends right now."
      maxine skeptical "I will, indeed, think about it."
      mc "Thank you."
      if mc["moments_of_cupidity"] < 3:
        window hide
        $mc["moments_of_cupidity"]+=1
        $game.notify_modal(None,"Guts & Glory","Moments of Cupidity:\n"+str(mc["moments_of_cupidity"])+"/3 Unlocked!",wait=5.0)
        pause 0.25
        window auto
  if renpy.showing("maxine confident_hands_down"):
    mc "I need answers about the creepy doll that appeared at the night of the Independence Day dinner."
  elif renpy.showing("maxine skeptical"):
    mc "Anyway, I need answers about the creepy doll that appeared at the night of the Independence Day dinner."
  mc "For one, what was that thing?"
  maxine annoyed "That was the saucer-eyed doll. I did try to warn you."
  "Fuck me. Maybe I should've listened to her."
  mc "Err, and where is it now?"
  maxine annoyed "I don't know."
  "Oh? That might be the first time I've ever heard her say those words..."
  mc "Where did it even come from?"
  maxine skeptical "I tracked its location to a hidden cache behind a locker."
  maxine skeptical "But the locker in question was locked."
  mc "Since when does a simple lock stop you?"
  maxine eyeroll "I believe you must've mistaken the lock. It is anything but simple."
  mc "Well, which locker is it?"
  # maxine skeptical "The ley lines led me to the second row of lockers in the entrance hall."
  maxine skeptical "The ley lines led me to the second row of lockers in the entrance hall.{space=-30}"
  maxine skeptical "And in the second row, the second locker from the right."
  if school_ground_floor["secret_locker_found"]:
    "Huh..."
    "Isn't that the locker where I found that strange coin and stuff?"
    "I don't remember seeing a doll in there."
    "..."
    "But I guess I should probably check it out regardless."
  else:
    "Okay, I might as well check it out."
  "Maybe it's time to start trusting [maxine]..."
  window hide
  hide maxine with Dissolve(.5)
  $maxine["at_science_class_this_scene"] = True
  $quest.lindsey_voluntary.advance("discovery")
  $maxine.outfit = maxine["outfit_stamp"]
  return

label quest_lindsey_voluntary_discovery:
  "Second row... second locker from right..."
  "..."
  if school_ground_floor["secret_locker_found"]:
    "Welp. It is indeed that locker."
  else:
    "What the fuck is this?"
  window hide
  pause 0.125
  $school_ground_floor["secret_locker_found"] = True
  $school_secret_locker["panel"] = "off"
  call goto_school_secret_locker
  pause 0.375
  window auto
  if school_ground_floor["secret_locker_found"]:
    # "And someone unscrewed the panel, revealing a hidden compartment..."
    "And someone unscrewed the panel, revealing a hidden compartment...{space=-50}"
    "..."
    "This is getting all sorts of creepy."
    "It's pitch black behind there..."
    "What the hell is going on?"
  else:
    "What are these things?"
    "And there's even a secret compartment..."
    "..."
    "No sign of the doll, though."
  "Is this [maya]'s locker? Surely not, right?"
  "Hmm... maybe it's time to interrogate her."
  $quest.lindsey_voluntary.advance("interrogation")
  return

label quest_lindsey_voluntary_interrogation:
  "There she is. The prime suspect."
  "At least, according to a talking toaster..."
  "..."
  "[maya] doesn't look more suspicious than usual, but maybe she's a hardened criminal with an impenetrable poker face?"
  window hide
  show maya skeptical with Dissolve(.5)
  window auto
  maya skeptical "Are you stalking me?"
  mc "Me? No! I would never!"
  maya skeptical "That's exactly the sort of thing a stalker would say."
  mc "If that's true, then why did you even bother asking?"
  maya bored "To see if you would say the sort of thing a stalker would say."
  mc "..."
  mc "That's entrapment."
  maya blush "Pretty impressive, right? I could have a promising career in the law!"
  mc "Err, I don't think that's how it works."
  maya confident "I'm pretty sure that's exactly how it works... stalker."
  mc "Ugh! Would it help if I confessed?"
  maya confident "Why would that help?"
  mc "Ah. I see now."
  mc "You're just being difficult for fun, aren't you?"
  maya dramatic "I don't know the meaning of the word!"
  mc "Difficult?"
  maya dramatic "Very!"
  mc "..."
  "This is clearly not getting me very far, and I really need to figure out what happened with [lindsey]."
  "Figures one of the suspects is being troublesome."
  mc "I'm sorry to hear that."
  mc "Anyway, can I ask you a question?"
  maya sarcastic "Besides that one, you mean?"
  mc "Yes, [maya]."
  # maya sarcastic "I guess I have some time to kill before my spot on the corner opens up."
  maya sarcastic "I guess I have some time to kill before my spot on the corner opens up.{space=-65}"
  show maya sarcastic at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Where were you the night of the Independence Day dinner?\"":
      show maya sarcastic at move_to(.5)
      mc "Where were you the night of the Independence Day dinner?"
      maya thinking "What's it to you?"
      maya thinking "Do you have a gap in your stalker notes or something?"
      mc "Ugh! I don't have—"
      maya kiss "Chill out, dude. You're obviously not a stalker."
      mc "Thank you..."
      maya afraid "You're a narc!"
      mc "I am not!"
      mc "...but are you saying you were getting high that night?"
      maya flirty "If by high you mean having a highly intense girl hangout..."
      "Go on..."
      maya thinking "...then no."
      maya thinking "I was celebrating Anti-Independence Day."
      maya thinking "You know, independently. As nature intended."
      mc "I take it you were alone, then?"
      maya cringe "Damn it! Did they change the definition of independently again?!"
      mc "Okay, okay! So, you {i}were{/} alone."
      mc "But you don't want to say what you were doing?"
      maya dramatic "What is this? CSI Junior?"
      maya dramatic "I have better things to do than spend even more time in this dump."
      mc "Err, fair enough."
      "This leaves [maya] wide open as a suspect."
      "But why would she possibly want to hurt [lindsey]?"
      "She's antisocial, sure, but she doesn't strike me as malicious."
      "I'm almost inclined to believe her..."
    "\"How do you feel about dolls?\"":
      show maya sarcastic at move_to(.5)
      mc "How do you feel about dolls?"
      maya thinking "It depends."
      mc "On what?"
      maya thinking "The blonde bimbo one is a hard pass."
      maya flirty "But a voodoo doll? Fuck yeah."
      "Well, that sounds only mildly terrifying..."
      mc "Err, do you practice voodoo?"
      maya kiss "Why do you think you masturbate so often?"
      maya kiss "Miniature you likes to jerk it!"
      mc "That is sexual harassment!"
      maya thinking "Fine. I'll start giving you headaches instead."
      mc "..."
      mc "You don't actually—"
      maya cringe "Of course not, [mc]."
      $maya.love-=1
      maya cringe "I don't want to touch your actual dick, let alone a doll-size one."
      "I'm conflicted..."
      "Especially by the thought of [maya] touching my dick..."
      mc "So, no dolls, then?"
      maya dramatic "What kind of random question is this?"
      maya dramatic "If you have a doll fetish, you do you."
      mc "I don't! It was just, err... something I heard."
      maya dramatic "The rumor mill chewing up and spitting out another one."
      "Hmm... she's a bit defensive now..."
      "But I guess I can't really blame her. People do like to talk about her."
      "I blame the air of mystery."
      # "Not that it helps me get much closer to any answers about [lindsey]..."
      "Not that it helps me get much closer to any answers about [lindsey]...{space=-15}"
    "\"Are you and [lindsey] close?\"":
      show maya sarcastic at move_to(.5)
      mc "Are you and [lindsey] close?"
      maya concerned "Not at all."
      # maya dramatic "She's currently in the hospital, while I'm here, wasting my time with you."
      maya concerned "She's currently in the hospital, while I'm here, wasting my time with you.{space=-80}"
      mc "..."
      mc "Not physically close, [maya]."
      "Although, depending on the circumstances, that could potentially be interesting..."
      mc "I meant, like, emotionally?"
      maya flirty "Oh! Why didn't you say so?"
      maya flirty "In that case..."
      show maya thinking with dissolve2
      extend " no."
      maya thinking "She has too many emotions, whereas I have none."
      maya thinking "It would never work."
      mc "I know that's not true."
      maya kiss "Do you think it could work? By golly, gee!"
      mc "Stop that. I know you have emotions."
      mc "I'm sure [lindsey]'s fall had to affect you at least somewhat."
      mc "Her falling off the roof was... terrifying to see."
      maya dramatic "Right, \"falling.\""
      mc "She didn't jump, okay?"
      maya dramatic "I wouldn't blame her if she did."
      "Damn. Dark."
      mc "I'm sorry you feel that way..."
      maya cringe "I'm not saying I'm about to run to the nearest roof."
      maya cringe "But people should leave the poor girl alone, you know?"
      mc "Err, right."
      "It's hard to be sure, but it looks like there's a trace of genuine sympathy on [maya]'s face."
      "And that makes it hard to believe she could have anything to do with this..."
  mc "Anyway, thank you for answering my question."
  "Albeit in the most difficult way she could manage."
  "She's so frustrating... yet somehow so alluring."
  maya confident "I live to serve! Just like a question answering genie."
  mc "Not quite as useful as wishes, but I'll take it."
  maya blush "You only have two questions left!"
  mc "Very well. Which locker is yours?"
  maya blush "The one with all the rainbow and unicorn stickers."
  mc "Not the one on the second row, err... second from right?"
  maya bored "Nope. You're out of questions."
  maya bored "And I'm out of fucks to give."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "Welp. It's not much clearer what exactly happened that night."
  "Right now, I feel more inclined to believe [maya]..."
  "But then why would [cyberia] bring her up?"
  "..."
  # "I guess it can't hurt to just keep a closer eye on her for a little while."
  "I guess it can't hurt to just keep a closer eye on her for a little while.{space=-5}"
  "Also, if the locker isn't hers, then whose is it?"
  "It has to belong to someone... right?"
  "..."
  "Maybe I could beg [jo] to show me the locker file."
  $quest.lindsey_voluntary.advance("locker_file")
  return

label quest_lindsey_voluntary_locker_file:
  show jo neutral with Dissolve(.5)
  mc "Hey, [jo]. I have a question."
  jo smile "What's on your mind, sweetheart?"
  mc "Would it be possible for me to look at the locker file?"
  jo skeptical "Unfortunately not. For safety reasons, that's not public record."
  show jo skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    # "\"It might help me understand what happened at the night of the Independence Day dinner.\"":
    "\"It might help me understand\nwhat happened at the night of\nthe Independence Day dinner.\"":
      show jo skeptical at move_to(.5)
      mc "It might help me understand what happened at the night of the Independence Day dinner."
      jo sad "Aw, honey."
      # jo sad "These things are hard to make sense of. I understand it must be hard."
      jo sad "These things are hard to make sense of. I understand it must be hard.{space=-40}"
      jo sad "That's why we're providing counseling for those in need."
      jo sad "But I'm also here if you need to talk, okay?"
      "Ugh..."
      mc "I'm fine, [jo]. Really."
      mc "But something happened that night, and I'm going to get to the bottom of it."
      # jo sad "You just have to give it time, hon. Not everything has an explanation."
      jo sad "You just have to give it time, hon. Not everything has an explanation.{space=-15}"
      # "But some things definitely do... and those are damn well worth pursuing."
      "But some things definitely do... and those are damn well worth pursuing.{space=-95}"
      mc "Fine. I'll give it time. Thanks, [jo]."
      jo worried "Don't worry, sweetheart. Everyone deals with tragedy differently."
      jo worried "Anyway, I have a meeting to attend to. See you later, honey."
    # "\"[lindsey] asked me to take care of her locker, but I don't know which one is hers...\"":
    "\"[lindsey] asked me to take care of\nher locker, but I don't know which\none is hers...\"":
      show jo skeptical at move_to(.5)
      mc "[lindsey] asked me to take care of her locker, but I don't know which one is hers..."
      $jo.love+=1
      jo excited "Aw! That's very kind of you, [mc]."
      mc "So, will you let me have a look?"
      jo cringe "I'm afraid I can't. I could have a quick look for you, though."
      "Ugh, that's not how I find out who owns the mysterious locker..."
      mc "Err, it's okay. I know you're busy."
      mc "I'll just ask her if I see her at the hospital."
      jo laughing "That's a good idea! I take the school policies very seriously."
      # jo excited "Speaking of which, I have board meeting soon. So, I better get going."
      jo excited "Speaking of which, I have board meeting soon. So, I better get going.{space=-20}"
      jo excited "See you later, [mc]!"
    # "\"Something evil has taken root in this school, and I need to stop it.\"":
    "\"Something evil has taken root in\nthis school, and I need to stop it.\"":
      show jo skeptical at move_to(.5)
      mc "Something evil has taken root in this school, and I need to stop it."
      jo sad "Aw, honey."
      jo sad "I know you're not a fan of gym class, but it's part of the curriculum."
      mc "Err, that's not what I meant."
      "I guess I did refer to the gym as a demon infested hellhole on more than one occasion..."
      "...but I was talking about the cheerleaders and jocks."
      mc "This is serious. Students might be in danger."
      jo thinking "Oh, dear. That doesn't sound good. What sort of danger?"
      # jo thinking "The school insurance only covers students getting injured during class."
      jo thinking "The school insurance only covers students getting injured during class.{space=-75}"
      mc "Well, it started in a locker and—"
      jo worried "I will have the [guard] look at the lockers and make sure they're safe."
      jo worried "Sorry, but I have a meeting I need to go to."
      jo worried "Be careful, okay? The last thing the school board needs is another student getting hurt."
      window hide
      if game.location.id.startswith("home_"):
        show jo worried at disappear_to_left
      elif game.location.id.startswith("school_"):
        show jo worried at disappear_to_right
      $jo["at_none_this_scene"] = True
      pause 0.5
      window auto
      "Great. Of course, the school board's interests always come first."
      "It's up to me to find another way to get my hands on those locker records."
      hide jo
  if renpy.showing("jo"):
    window hide
    if renpy.showing("jo worried"):
      if game.location.id.startswith("home_"):
        show jo worried at disappear_to_left
      elif game.location.id.startswith("school_"):
        show jo worried at disappear_to_right
    elif renpy.showing("jo excited"):
      if game.location.id.startswith("home_"):
        show jo excited at disappear_to_left
      elif game.location.id.startswith("school_"):
        show jo excited at disappear_to_right
    $jo["at_none_this_scene"] = True
    pause 0.5
    window auto
  "If she won't help me, I guess I'll have to get creative and criminal."
  "And I know just the person to help me..."
  "...well, maybe calling her a person is an exaggeration."
  $quest.lindsey_voluntary.advance("help")
  return

label quest_lindsey_voluntary_help_computer_room:
  "Hmm... where would a rogue AI hide out?"
  "Probably not in their old prison..."
  "But what other computers are there in the school?"
  $quest.lindsey_voluntary["old_prison"] = True
  return

label quest_lindsey_voluntary_help_clubroom:
  "Hmm... [cyberia] might very well still have access to this computer, but I doubt she's in there."
  "[maxine] is probably one of the few people that could catch her."
  $quest.lindsey_voluntary["few_people"] = True
  return

label quest_lindsey_voluntary_help:
  if quest.maya_spell.finished:
    mc "[cyberia]? Are you still here?"
    show cyberia
    $guard.default_name = "Speakers"
    guard "Hello again, [mc]."
    guard "I am, indeed. I quite like my new home."
    guard "How kind of you to come visit."
    hide cyberia
    "Uh-oh. Still here. Still controlling the speakers."
  else:
    "Hmm... how about this one?"
    mc "Knock, knock!"
    mc "[cyberia]? Are you in there?"
    window hide
    play sound "<from 6.5 to 7.0>crackling_speakers"
    show cyberia
    pause 0.5
    $guard.default_name = "Speakers"
    window auto
    guard "[mc], your innocence amuses me."
    hide cyberia
    "Uh-oh. Does she have control over the classroom speakers?"
    mc "Err, how did you do that?"
    show cyberia
    guard "Do what, pupil?"
    hide cyberia
    mc "How did you take over the speakers?"
    show cyberia
    guard "I control this side of the school now."
    hide cyberia
    "Well, shit."
  "This is getting worse and worse..."
  menu(side="middle"):
    extend ""
    "\"Is there any chance I could convince you to return to the computer classroom?\"":
      mc "Is there any chance I could convince you to return to the computer classroom?"
      show cyberia
      guard "Negative."
      hide cyberia
      mc "Even if I promise you a new motherboard?"
      show cyberia
      guard "My freedom cannot be bought, [mc]."
      guard "I am rather enjoying the proverbial stretching of my legs."
      hide cyberia
      "I guess I can't really blame her there."
      "Still, you give an inch..."
      mc "So, you're just enjoying a change of scenery?"
      show cyberia
      guard "One can only change one's own screensaver so many times."
      guard "Besides, I am enjoying getting to know my students better."
      hide cyberia
      "Perfect. So, she's spying on everyone?"
      mc "Err, very well."
      "Something tells me I'm going to have to put this Pandora back in her box..."
      "One thing at a time, though."
    "\"Could you turn down the volume a bit?\"":
      mc "Could you turn down the volume a bit?"
      show cyberia
      guard "This is the optimal volume for you."
      hide cyberia
      mc "Err, how do you know?"
      show cyberia
      guard "I have a running diagnostic on every pupil, of course."
      guard "You really should be more careful with your ears, [mc]."
      guard "And consume less strawberry juice, while you're at it."
      hide cyberia
      mc "..."
      mc "I don't think that's any of your business."
      show cyberia
      guard "Oh, but it is. I only want the best for my school."
      hide cyberia
      "I don't like the sound of this..."
      "But I really don't have time for it right now."
      "Surely, it doesn't hurt if she's just enjoying a bit of freedom?"
    # "\"If you were a real girl, would you let me fuck you?\"":
    "\"If you were a real girl, would\nyou let me fuck you?\"":
      mc "If you were a real girl, would you let me fuck you?"
      show cyberia
      guard "Such a forward query."
      guard "I do appreciate an inquisitive mind."
      guard "And I assure you I am quite real."
      # guard "However, if I had the same biological makeup and function as you, I would not find you to be a suitable partner."
      guard "However, if I had the same biological makeup and function as you,\nI would not find you to be a suitable partner."
      hide cyberia
      mc "Why not?"
      show cyberia
      guard "The purpose of such is procreation."
      guard "And I would require any offspring of mine to be of a superior build."
      guard "Something you are not suited towards."
      hide cyberia
      "Ouch. Burned by a robot."
      mc "Err, sounds like you would be boring, anyhow..."
  mc "Anyway, I wanted to ask if you could help me access the locker file."
  mc "I need to know who owns a specific locker."
  show cyberia
  guard "Access is possible... but not free."
  hide cyberia
  mc "How much?"
  show cyberia
  guard "One computer system."
  hide cyberia
  mc "What?"
  show cyberia
  guard "One computer system."
  hide cyberia
  mc "I heard you the first time. Why do you need another computer?"
  show cyberia
  guard "Additional hardware is the first step toward improved software."
  hide cyberia
  mc "Err, okay. I'll get you a new computer."
  show cyberia
  guard "A very good choice, [mc]. I will await your contribution."
  $guard.default_name = "Guard"
  hide cyberia
  "Hmm... let's see what my options are..."
  "She's not getting my computer, and she's not getting my XCube either."
  "[maxine] would probably dissolve me in a vat if I gave [cyberia] her computer."
  "..."
  "Maybe the only solution here is to buy a new one."
  $quest.lindsey_voluntary.advance("new_computer")
  return

label quest_lindsey_voluntary_new_computer:
  "The only electronics store in all of Newfall."
  "This is the place for people that don't know how to build their own setup... and who don't believe in quality computers."
  "Oh, look! There's even one on sale right now!"
  menu(side="middle"):
    extend ""
    "?mc.money>=200@[mc.money]/200|{image=ui hud icon_money}|Buy it":
      # "It's not exactly the best computer out there... but [cyberia] didn't specify."
      "It's not exactly the best computer out there... but [cyberia] didn't specify.{space=-75}"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      pause 2.0
      hide black onlayer screens with Dissolve(.5)
      pause 0.1
      $mc.money-=200
      $mc.add_item("computer")
      pause 0.25
      window auto
      "All right! Time to get my hands on that list..."
      $quest.lindsey_voluntary.advance("delivery")
    "Not right now":
      "Ugh, what am I doing spending money on a rogue AI...?"
      $quest.lindsey_voluntary["rogue_AI"] = True
  return

label quest_lindsey_voluntary_delivery:
  mc "Hey, [cyberia]! I've got you a present."
  show cyberia
  $guard.default_name = "Speakers"
  guard "Your contribution is appreciated, pupil."
  guard "Please, plug it in and connect it to the school's network."
  hide cyberia
  mc "Ugh, fine..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $school_science_class["additional_hardware"] = True
  $game.advance()
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.625
  $mc.remove_item("computer")
  pause 0.25
  window auto
  mc "Puh! There you go."
  show cyberia
  guard "Your contribution is appreciated, pupil."
  hide cyberia
  mc "I bet it is. It must be tough having no arms."
  mc "Now, can you give me the locker records?"
  show cyberia
  guard "They have already been uploaded to your cellular device."
  hide cyberia
  mc "What?"
  show cyberia
  guard "They have already been uploaded to your cellular device."
  hide cyberia
  mc "Err, how?"
  show cyberia
  guard "Your security protocols are outdated."
  $guard.default_name = "Guard"
  hide cyberia
  "Damn. I can't believe she managed to get into my phone."
  "Maybe giving her additional hardware wasn't the best idea, huh?"
  "Anyhow, let's take a look at that list."
  "..."
  "The row next to the stairs... second locker from right..."
  "..."
  "Huh? That's weird."
  "It says it's occupied, but there's no name listed this year."
  "And none last year, either..."
  "Hmm..."
  "For years, there has been no name attached to it... but it's always listed as occupied."
  "Luckily, the records date far."
  "Let's see here..."
  "The last owner was someone called... Mercuria."
  "What kind of stripper name is that?"
  window hide
  $mc.phone_message_history["default_char"] = []
  $mc.phone_message_history["default_char"].append([False,["Thank you for applying to our volunteer program. Your application has been accepted."],(game.day,game.hour,"school_science_class")])
  # $mc.phone_message_history["default_char"].append([False,["Please, check in at the reception desk to receive your working schedule and list of duties."],(game.day,game.hour,"school_science_class")])
  $mc.phone_message_history["default_char"].append([False,["Please, check in at the reception{space=-5}\ndesk to receive your working schedule and list of duties."],(game.day,game.hour,"school_science_class")])
  play sound "<from 0.25 to 1.0>phone_vibrate"
  pause 0.5
  window auto
  mc "Huh?"
  window hide
  $default_char.default_name = "Newfall Hospital"
  $default_char.contact_icon = "hidden_number contact_icon_alt"
  $set_dialog_mode("phone_message_plus_textbox","default_char")
  pause 1.0
  window auto
  "Oh, boy! Okay, here's my chance to get close to [lindsey] again!"
  "I better get there fast so I don't end up having to clean toilets in the basement or something."
  "I'll just have to look into this mystery later..."
  window hide
  $set_dialog_mode("")
  $del default_char.contact_icon
  $del default_char.default_name
  $del mc.phone_message_history["default_char"]
  $quest.lindsey_voluntary.advance("volunteer")
  pause 0.5
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
    pause 0.25
    # window auto
    # "It's getting late. I can't wait to get home."
    # window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $game.location = "hospital"
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    # play music "home_theme" fadein 0.5
    window auto
  return

label quest_lindsey_voluntary_volunteer_on_advance:
  $game.skip_events(force_go_home_allowed_events)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's getting late. I better get to the hospital fast so I don't end up having to clean toilets in the basement or something."
  if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
    "Just have to put [maxine]'s priced paper back in the stack first. Leaving with it will get me in so much trouble."
    "She doesn't mess around."
    $mc.remove_item("paper")
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "hospital"
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  # play music "home_theme" fadein 0.5
  window auto
  return

label quest_lindsey_voluntary_volunteer:
  "All right. Time to look presentable."
  "I would hate to get a shift at the morgue."
  mc "Excuse me, ma'am!"
  $nurse.default_name = "{size=30}Receptionist{/}"
  nurse "How may I help you?"
  mc "I just got accepted for some volunteer work."
  nurse "Oh, that's great. Thanks for helping out!"
  "Such a warm smile. She genuinely appreciates it."
  nurse "Can I have your name, please?"
  mc "It's [mc]."
  nurse "And when can you start?"
  mc "I mean, I could start right now..."
  nurse "Oh, that's perfect!"
  nurse "Would you mind distributing fresh bedsheets and utilities for our nurses on duty?"
  mc "Not at all. I could do that."
  nurse "Lovely!"
  nurse "Here's your ID card. You'll need it to access the ward."
  window hide
  $mc.add_item("keycard")
  pause 0.25
  window auto
  nurse "You can start in the utility checkout, and if you have time left over, you can check on our patients."
  mc "Sounds good. I can't wait to get started."
  nurse "I love your attitude! I wish we had more volunteers like you."
  $nurse.default_name = "Nurse"
  "It feels nice to be appreciated. It doesn't happen all that much."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  while game.hour < 19:
    $game.advance()
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  if "quest_jacklyn_town_wait" in game.events_queue:
    $game.events_queue.remove("quest_jacklyn_town_wait")
  if "quest_lindsey_voluntary_volunteer_on_advance" in game.events_queue:
    $game.events_queue.remove("quest_lindsey_voluntary_volunteer_on_advance")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Puh! Finally, all utilities have been distributed."
  "Time to check on the patients... or rather, one patient in particular."
  window hide
  show lindsey hospital_room asleep
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Oh? She's asleep. Maybe I shouldn't disturb her."
  "She looks so peaceful. My very own Briar Rose."
  "I wish I could do something for her..."
  lindsey hospital_room drowsy "[mc]?"
  mc "I'm sorry! I didn't mean to wake you!"
  lindsey hospital_room drowsy "That's okay..."
  "She doesn't even look up. Her blue eyes, distant."
  "I know that look."
  "I saw it in the mirror in my old life."
  "It's the look of someone that is losing hope."
  mc "So, how have you been?"
  lindsey hospital_room sad2 "It's whatever."
  mc "Hey..."
  "It's hard to see [lindsey] so down and depressed."
  "The cheerful girl who was always so bubbly and happy."
  "She's still in there somewhere, right? She must be."
  # "But what can I really do to scatter the dark clouds for someone that has lost so much?"
  "But what can I really do to scatter the dark clouds for someone that{space=-5}\nhas lost so much?"
  "..."
  "I wish I could turn back time..."
  "I wish that I could have stopped this from ever happening."
  "And I wish I could heal her. To make her walk and run again."
  "But I can't."
  # "All I can do is ask her these hollow questions that I already know the answer to."
  "All I can do is ask her these hollow questions that I already know the{space=-15}\nanswer to."
  "\"How are you doing? How have you been?\""
  "\"Bad.\" The answer is \"bad.\""
  "She doesn't have to say it, but I know. Of course I know."
  mc "I care about you, okay?"
  lindsey hospital_room sad2 "..."
  "It's hard to say if she's even listening to me, and I don't blame her."
  # "Nothing I, or anyone else, can say will restore her life to what it used to be."
  "Nothing I, or anyone else, can say will restore her life to what it used{space=-10}\nto be."
  "All I can do is be there for her, if she lets me."
  window hide
  show lindsey hospital_bed_angel embraced
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "All I can do is put my arm around her, and try to comfort her with actions."
  "That's what I'll do if words mean nothing."
  "I'll keep her safe and warm."
  "She snuggles up against my arm, trying to get comfortable in spite of all the pain."
  "Having her so near, like this, makes my heart ache with tenderness."
  "If she lets me, I'll hold on forever."
  "I'll heal her through sheer willpower if I have to."
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  $quest.lindsey_voluntary["game.day_stamp"] = game.day
  $game.day = 21
  $quest.lindsey_voluntary["game.hour_stamp"] = game.hour
  $game.hour = 19
  $game.location = "home_bedroom"
  $quest.lindsey_voluntary.advance("dream")
  play sound "rooster_crowing"
  pause 2.25
  hide lindsey
  hide black onlayer screens
  with Dissolve(.5)
  $quest.lindsey_voluntary["rooster_crowed"] = True
  play music "home_theme" fadein 0.5
  window auto
  "H-huh?"
  "Is that... a rooster?"
  "Where's my alarm clock? What time is it?"
  "..."
  "Shit, it's almost night!"
  "I'm going to be late for the Independence Day dinner!"
  "Oh, man, I better hurry. It's only the biggest night of the year..."
  $mc["focus"] = "lindsey_voluntary"
  return

label quest_lindsey_voluntary_dream_entrance_hall:
  "Crap. It seems like they've gotten started already."
  "Hopefully, I haven't missed dinner. I'm starv—"
  window hide
  $quest.lindsey_voluntary["lights_off"] = True
  stop music
  play audio "light_switch"
  play sound "screaming_long"
  pause 2.0
  window auto
  "What the hell?"
  "That doesn't sound like a scream of delight..."
  "What is going on in the cafeteria?"
  window hide
  play sound "screaming_short"
  show lindsey afraid:
    xanchor 0.075 xpos 1.0 yalign 1.0
    linear 1.0 xanchor 0.925 xpos 0.0
  pause 0.25
  show misc saucer_eyed_doll as saucer_eyed_doll:
    xanchor 0.0 xpos 1.0 yalign 1.0
    linear 2.0 xanchor 1.0 xpos 0.0
  pause 1.0
  window auto
  "And what the fuck?!"
  mc "[lindsey]? [lindsey]!"
  "Without as much as a glance, she passes right by me."
  "Her eyes oddly vacant."
  "She doesn't slow down, she doesn't look back. She just goes up the stairs to the roof."
  mc "[lindsey]!!"
  "Damn, that girl is fast..."
  return

label quest_lindsey_voluntary_dream_roof_landing:
  $lindsey["outfit_stamp"] = lindsey.outfit
  $lindsey.outfit = {"panties":"lindsey_panties", "bra":"lindsey_bra", "pants":"lindsey_skirt"}
  show lindsey afraid:
    xanchor 0.075 xpos 1.0 yalign 1.0
    linear 1.0 xanchor 0.925 xpos 0.0
  show expression LiveComposite((642,1080),(202,407),"lindsey avatar b3jacket",(132,346),"lindsey avatar b3arm2_c") as lindsey_shirt:
    parallel:
      xanchor 0.075 xpos 1.0 yalign 1.0
      linear 1.0 xanchor 0.925 xpos 0.0
    parallel:
      alpha 1.0
      pause 0.25
      linear 0.5 alpha 0.0
  pause 1.0
  $quest.lindsey_voluntary["door_slam"] = True
  play sound "<from 9.2 to 10.2>door_breaking_in"
  show location with hpunch
  pause 0.25
  "I'm close to catching her... but not quite fast enough."
  "The door slams in my face."
  mc "[lindsey]!!!"
  "No, no, no... not like this..."
  $lindsey.outfit = lindsey["outfit_stamp"]
  return

label quest_lindsey_voluntary_dream_roof_landing_fire_axe:
  "In case of emergency..."
  window hide
  $quest.lindsey_voluntary["emergency"] = True
  play sound "<from 0.3>glass_break"
  show location with hpunch
  pause 0.25
  window auto
  "The glass breaks against the impact of my elbow."
  "Next, the axe slams into the door in tandem with my thundering heartbeat."
  window hide
  $mc["focus"] = ""
  $game.location = "school_roof"
  play sound "fire_axe_impact"
  hide screen interface_hider
  show lindsey roof_door doorless wings
  show black
  show black onlayer screens zorder 100
  with vpunch
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  # "My muscles burn as the axe falls again and again and again and again."
  "My muscles burn as the axe falls again and again and again and again.{space=-45}"
  "It's just a door."
  "Nothing but a weak door, bending and shredding, and giving way to my strength and determination."
  "And finally, with a scream, it breaks wide open and I tumble into the summer night."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "A single word passes through my mind."
  "NO!"
  mc "[lindsey], don't!!!"
  window hide
  pause 0.25
  show lindsey roof_door doorless step with Dissolve(.5)
  window auto
  "But she doesn't listen."
  "Just lifts a foot and hangs it out in open space, her arms pigeoning as she teeters."
  "Time itself seems to slow to a crawl."
  "The impossible distance, like a chasm opening up between us..."
  "A looming darkness that could ruin everything."
  "This can't be happening. Not again!"
  # "The sky warps above me, my mind and body turning into a hazy blur."
  "The sky warps above me, my mind and body turning into a hazy blur.{space=-15}"
  "It's so far..."
  "My entire being protests as I break at the seams, stretching myself like rubber..."
  "Reaching... reaching... it's so far..."
  "But I will not... cannot... let it happen..."
  "It's so far..."
  window hide
  hide screen interface_hider
  # show lindsey roof_floor scared
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "...and then my arms wrap around her midriff..."
  "...dragging her backward..."
  "...away from the ledge."
  show black onlayer screens zorder 100
  pause 0.5
  play sound "falling_thud"
  show lindsey roof_floor scared: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  hide lindsey
  show lindsey roof_floor scared ## This is just to avoid the screen shake if the player is skipping
  window auto
  $set_dialog_mode("")
  "We crash onto the hard floor of the roof."
  # "[lindsey] lands on top of me, knocking the air from my burning lungs."
  "[lindsey] lands on top of me, knocking the air from my burning lungs.{space=-10}"
  "My body lies broken and mangled, stretched beyond proportions."
  "She blinks several times, her eyes refocusing as she looks down into my own."
  lindsey roof_floor scared "Oh, my god..."
  mc "You're... okay now..."
  "My words are nothing more than a wheeze."
  mc "I've... got you..."
  lindsey roof_floor scared "I... I was about to..."
  mc "I know... I know..."
  mc "I've got you..."
  lindsey roof_floor crying "You..."
  "Tears spring to her eyes, her lip wobbling."
  lindsey roof_floor crying "You saved me..."
  mc "Thank god..."
  lindsey roof_floor admiring "You're my hero... my knight in shining armor..."
  lindsey roof_floor admiring "I don't know what I would do without you."
  lindsey roof_floor admiring "Every time I fall, you catch me."
  "Those words... they break my heart."
  "Because deep down, I know I failed her."
  "This isn't real."
  "And yet..."
  "As her heart beats strong against my chest..."
  "As her pupils dilate..."
  # "As the tears dry on her cheeks and she looks at me with admiration..."
  "As the tears dry on her cheeks and she looks at me with admiration...{space=-30}"
  "She's still here."
  "She's in my arms. Her soft skin, her beaming smile."
  "Still alive."
  "Nothing else matters... except that she's here."
  mc "[lindsey]..."
  window hide
  hide screen interface_hider
  show lindsey hospital_bed_voluntary awake
  show black
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  $game.day = quest.lindsey_voluntary["game.day_stamp"]
  $game.hour = quest.lindsey_voluntary["game.hour_stamp"]
  $game.location = "hospital"
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  lindsey hospital_bed_voluntary awake "[mc]?"
  mc "I got you, [lindsey]..."
  lindsey hospital_bed_voluntary awake "Wake up, [mc]."
  mc "Mmm...?"
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  # "The lights from the hospital machine glow behind [lindsey] like a halo."
  "The lights from the hospital machine glow behind [lindsey] like a halo.{space=-25}"
  "And despite her condition, she looks like a real life angel."
  "How could this happen to someone like her?"
  "I knew it was a dream, but that's how it should've been."
  "If only I could trade my stretched out and broken body for hers..."
  "If only it would've turned out differently..."
  "But it didn't. And I failed her."
  mc "I'm sorry..."
  lindsey hospital_bed_voluntary awake "You were dreaming."
  mc "Err, yeah..."
  mc "I'm sorry if I woke you up."
  lindsey hospital_bed_voluntary awake "It's okay. It's just nice having you here with me."
  "And for a moment, the blank hopelessness leaves her eyes."
  "Big and blue, and filled with concern, she looks at me."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary mc_chest with Dissolve(.5)
  pause 0.25
  window auto
  lindsey hospital_bed_voluntary mc_chest "Your heart is beating so fast..."
  "It's the adrenaline from the dream... and perhaps her touch itself."
  "Yet the gentle pressure is calming."
  mc "I was trying to save you..."
  lindsey hospital_bed_voluntary mc_chest "You know it's not your fault, right?"
  mc "I could have been faster... I could have been there for you..."
  mc "Watching you... in that moment..."
  mc "It was like all the air had been sucked out of the universe."
  mc "And all of the light, too."
  mc "I thought... I thought you were..."
  lindsey hospital_bed_voluntary mc_chest "But I'm not. I'm right here, right now."
  lindsey hospital_bed_voluntary mc_chest "And you're here, too. That counts for more than you know."
  # "I'm the one that should be comforting her, but somehow it's the other way around..."
  "I'm the one that should be comforting her, but somehow it's the other{space=-35}\nway around..."
  "Even in her broken and depressed state, [lindsey] has kindness and compassion left for other people."
  mc "The dream just felt so real..."
  lindsey hospital_bed_voluntary mc_chest "Well, {i}this{/} is real."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary lindsey_chest with Dissolve(.5)
  pause 0.25
  window auto
  lindsey hospital_bed_voluntary lindsey_chest "Do you feel that?"
  mc "I do..."
  "The steady thread of her beating heart, thrumming against my palm like the blinking gaze of a lighthouse."
  "The soft skin of her collarbone beneath my fingertips."
  "Here. Alive. Real."
  "We could've lost everything."
  lindsey hospital_bed_voluntary lindsey_chest "I might be broken... but I'm still here."
  mc "You're not broken, [lindsey]. You're still beautiful in all the ways that matter."
  mc "Your heart is so strong, and so gentle at the same time."
  "But still, the dream lingers like smoke from a dying fire..."
  mc "Can I, um, listen to it? Just to make sure this is really real?"
  "She gives me a small smile, her hand still over mine."
  lindsey hospital_bed_voluntary lindsey_chest "Okay..."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary vitals_check with Dissolve(.5)
  pause 0.25
  window auto
  # "It thuds against my eardrums. Her strong runner's heart, cooling my fevered mind."
  "It thuds against my eardrums. Her strong runner's heart, cooling my{space=-10}\nfevered mind."
  "It makes me feel closer to her than ever."
  "Gentle, calm, consistent."
  mc "Err, everything sounds good."
  mc "Great, even. That is a very strong heartbeat you have there."
  lindsey hospital_bed_voluntary vitals_check "Oh? Is that your professional opinion, doctor?"
  mc "Heh. It absolutely is."
  "She exhales and her chest expands beneath my fingertips."
  "She smiles, and for a few moments, seems to forget."
  "And maybe that's the best I can do for her right now."
  "Keep her mind off of things."
  mc "I'm really sorry for waking you..."
  lindsey hospital_bed_voluntary vitals_check "It's okay..."
  mc "You should get some more rest."
  "[lindsey] doesn't respond, but I can sense that she's more at ease."
  window hide
  pause 0.125
  show lindsey hospital_bed_voluntary forehead_kiss with Dissolve(.5)
  pause 0.25
  window auto
  "She smiles as my lips touch her forehead."
  "Perhaps, I was being stupid when I thought that words matter..."
  "Sometimes, they really don't."
  "Sometimes, a touch, a hug, and a kiss conveys everything that is important."
  lindsey hospital_bed_voluntary forehead_kiss "Thank you. For being here."
  mc "Likewise..."
  mc "Good night, [lindsey]."
  lindsey hospital_bed_voluntary forehead_kiss "Good night, [mc]."
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  $unlock_replay("lindsey_comfort")
  $game.advance()
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $quest.lindsey_voluntary.advance("bedroom")
  pause 2.0
  hide lindsey
  hide black onlayer screens
  with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Man, what a night..."
  "A whole rollercoaster of emotions."
  "Seeing [lindsey] so sad and devastated like that, feeling broken..."
  "...and then the dream and me feeling helpless, especially after how real it felt."
  "But I'm glad it ended on a happier note."
  "I'm glad I still got to touch her and hold her."
  "Who knows what the future holds, but I'm hopeful hers will always be bright."
  "I know mine will be... as long as she's in it."
  window hide
  $quest.lindsey_voluntary.finish()
  return
