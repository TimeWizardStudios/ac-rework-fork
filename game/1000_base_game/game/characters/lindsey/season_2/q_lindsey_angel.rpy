label quest_lindsey_angel_start:
# SFX: Speaker noise, crackling
  $guard.default_name = "{size=30}PA System{/}"
  guard "Good morning, Newfall High. This is your administrator speaking."
  "God, hearing [jo] over the loudspeakers isn't exactly my favorite way of waking up..."
  guard "As you may know, we had an incident right before the fall break."
  guard "Today, I'm here to bring you some good news."
  guard "[lindsey] has woken up from her coma, and her vitals are stable."
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   "Son of a bitch."
#   "I'm in trouble if she starts talking..."
    pass
  elif quest.lindsey_motive.actually_finished:
    "Oh, thank god!"
    "It feels like I've been holding my breath for weeks..."
  else:
    "Phew! I'm glad she's okay."
    "Tragic about her running career, but there's more to life."
    "I know that better than anyone..."
  guard "Her parents have asked for some continued privacy for their daughter.{space=-45}"
  guard "She will remain out of the school indefinitely."
  guard "Thank you for respecting their wishes, and have a pleasant day."
  $guard.default_name = "Guard"
  if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#   "Well, it's a good thing they're keeping her under wraps for now..."
#   "But she's a ticking time bomb that needs to be defused."
    pass
  elif quest.lindsey_motive.actually_finished:
    "I don't care what her parents want. I have to see her."
    window hide
    $quest.lindsey_angel.start()
    $hospital["unlocked"] = True
    return
  else:
    "Oh, well. If we were closer, it would make sense to visit her, but I guess I'll see her when she gets back."
  window hide
  $quest.lindsey_angel.fail()
  return

label quest_lindsey_angel_good_news:
  "The clouds hang thick over the marina today, and the autumn chill creeps in through your clothes."
  "This place is more alive in the spring. Now, it just feels abandoned and depressing."
  "But maybe it's just my mood..."
  "Maybe it's just the dark reminder of the pain, towering up over the other buildings."
  "The Newfall Hospital, ruining the idyllic skyline."
  "[lindsey] is in there somewhere... eyes closed... breath shallow..."
  "Her broken body slowly healing."
  "But what about her mind?"
  "I might be the only one that knows what happened that night, herself included."
  window hide
  show jo concerned_bag at appear_from_left
  pause 0.5
  window auto
  jo concerned_bag "[mc]? What are you doing here?"
  "My eyes flit to [jo] and even then it takes me a moment to fully register her standing in front of me."
  "Looking so carefree, shopping bags in hand..."
  "Just another normal day for her."
  "It's hard to remember what those feel like since [lindsey]'s fall."
  jo neutral_bag "..."
  jo neutral_bag "Are you all right?"
  mc "Why didn't you tell me?"
  jo neutral_bag "Tell you what?"
  mc "About [lindsey]... awake, alive, completely immobilized."
  mc "Why didn't you tell me sooner?"
  mc "I need to be there for her!"
  jo thinking_bag "It's going to be a long road ahead, honey."
  jo thinking_bag "She needs time, her family needs time and space and privacy."
  jo thinking_bag "To heal and get her help. To understand why she j—"
  mc "Stop! She didn't. She wouldn't." with vpunch
  jo worried_bag "I know it's hard, [mc]. But thankfully, she survived and now the recovery can begin."
  mc "Recovery requires constant support, though."
  mc "From someone who understands, and won't be sitting there silently judging her."
  jo blush_bag "..."
  jo blush_bag "You know, I'm really proud of the man you're becoming."
  jo blush_bag "Kind and thoughtful, anxious to be there for your friends."
  jo blush_bag "I'm sorry I didn't tell you sooner."
  jo blush_bag "[lindsey] is lucky to have you."
  mc "Thanks..."
  jo sarcastic_bag "I don't know if they're allowing more visitors yet, but..."
  mc "I'm still going to try."
  jo flirty_bag "Very well. Good luck, sweetheart."
  window hide
  show jo flirty_bag at disappear_to_left
  pause 1.0
  $quest.lindsey_angel["constant_support"] = True
  return

label quest_lindsey_angel_good_news_hospital:
  "Man, this place is seriously depressing."
  "White, monotone, barely any windows..."
  $nurse.default_name = "{size=30}Receptionist{/}"
  nurse "Excuse me, can I help you?"
  mc "Err, yes! I'm looking for someone."
  mc "Her name is [lindsey]. She's about... this height... blonde... really cute...{space=-35}"
  nurse "Are you family?"
  mc "Uh, not exactly."
  nurse "Unfortunately, only family members are allowed into the hospital ward.{space=-60}"
  mc "But I really need to see her! She had a traumatic experience..."
  nurse "Yes, that's the case for most of our long term patients. That's precisely{space=-65}\nwhy they're here."
  mc "Can't you make an exception?"
  nurse "Sorry, I'm afraid not."
  $nurse.default_name = "Nurse"
  "Crap. What now?"
  "There must be some other way of getting into the ward..."
  "Hmm..."
  if quest.nurse_aid["name_reveal"]:
    "Maybe [amelia] could help me?"
  else:
    "Maybe the [nurse] could help me?"
  $quest.lindsey_angel.advance("nurse")
  return

label quest_lindsey_angel_nurse:
  show nurse neutral with Dissolve(.5)
  window auto show
  show nurse afraid with dissolve2
  nurse afraid "My goodness, [mc]! You nearly gave me a fright." with vpunch
  nurse afraid "Sneaking up on people like that? Oh, dear."
  mc "Sorry about that. I'm actually here to ask for a favor."
  nurse smile "Of course! How may I help?"
  "Why isn't every woman this agreeable and helpful?"
  mc "I went to the hospital to see [lindsey], but they won't let me in."
  nurse sad "Ah, yes. That poor dear."
  nurse sad "She seemed so happy, too. I guess you never really know with this sort of thing."
  mc "What are you talking about?"
  nurse concerned "You didn't know? Oh, I'm so sorry. It's not my place to tell."
  mc "If you're implying what I think you're implying, you're dead wrong."
  mc "It wasn't a suicide attempt."
  nurse thinking "I know it's hard to deal with. There is counseling and—"
  mc "Listen to me! I know what happened, and it wasn't that." with vpunch
  mc "Now, can you help me get into the hospital ward or not?"
  nurse neutral "I believe the ward is only open to family visits..."
  mc "Yeah, no kidding. That's why I need your help."
  nurse surprised "Oh, no! I can't help with that! It's against the rules!"
  show nurse surprised at move_to(.25)
  $blackmail = mc.owned_item(("damning_document","compromising_photo")) or "compromising_photo"
  menu(side="right"):
    extend ""
    "?mc.owned_item(('damning_document','compromising_photo'))@|{image=items [blackmail]}|Blackmail her\ninto helping":
      show nurse surprised at move_to(.5)
      mc "That's funny."
      nurse afraid "Um... what is?"
      mc "The only rules you should care about are mine."
      nurse annoyed "..."
      nurse annoyed "I don't think I understand..."
      mc "Then I'll spell it out for you in terms you do understand."
      if mc.owned_item("damning_document"):
        mc "If you don't help me get in to see [lindsey], that document of yours is going up everywhere."
      elif mc.owned_item("compromising_photo"):
        mc "If you don't help me get in to see [lindsey], that photo of yours is going up everywhere."
      if nurse["strike_book_activated"]:
        mc "And I have plenty of space yet in your little book of strikes."
        mc "I think at least three more would be well earned right now."
      nurse afraid "Oh, goodness!"
      nurse afraid "I, um... I don't think..."
      mc "I'm not asking you to think. I'm telling you to do."
      $nurse.lust+=1
      nurse surprised_crossed_arms "Gracious!"
      nurse annoyed "O-okay... I will help you..."
      mc "That's what I thought."
    "Beg for sympathy":
      show nurse surprised at move_to(.5)
      mc "Rules? At a time like this?"
      mc "But just think of poor [lindsey], in that cold, sterile room, all alone..."
      mc "Can you imagine it? How afraid she must be?"
      mc "Her life forever changed, but lucky to even be alive."
      mc "I would think being isolated can't possibly help her recovery."
      nurse sad "Goodness gracious."
      nurse sad "When you put it like that..."
      mc "She's just lying in there. Broken. Scared."
      mc "Please, if I could just help her feel a little more whole..."
      mc "A little less lonely... cared for... thought of..."
      mc "I don't need very long."
      nurse concerned "Oh! All right, all right!"
      nurse concerned "I just can't bear the thought of it! That poor thing..."
      $nurse.love+=1
      nurse concerned "I will help you."
  nurse "I just, um... I really can't afford to lose this job..."
  nurse "Promise me you will be discreet?"
  mc "Of course!"
  nurse thinking "Okay. Well, you know how it is working as much as I do."
  nurse thinking "You get tired, and forgetful... maybe forgetful enough to leave a keycard somewhere."
  mc "An easy thing to do."
  nurse concerned "I really should get back to work now, though."
  nurse concerned "It's been so busy. I'm already feeling forgetful."
  mc "Anyone would with that kind of workload."
  nurse blush "Mhm... maybe if you find your way into my office tonight, you might get lucky."
  mc "Yeah, I just might."
  nurse blush "Oh, my. That didn't come out quite right, did it?"
  nurse blush "Sorry, I must go!"
  window hide
  hide nurse with Dissolve(.5)
  window auto
  if quest.nurse_aid["name_reveal"]:
    "Good thing I thought of [amelia] as a way in. It never hurts to know people."
  else:
    "Good thing I thought of the [nurse] as a way in. It never hurts to know people."
  "And with her help, I'll be on my way to [lindsey] in no time!"
  "Now, for the waiting game..."
  $quest.lindsey_angel.advance("wait")
  return

label quest_lindsey_angel_wait:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location == "school_nurse_room":
    "All right, let's see here..."
    "Finding that keycard should be a lot easier than losing my V-card."
    $quest.lindsey_angel["V-card"] = True
  else:
    if quest.nurse_aid["name_reveal"]:
      # "It's getting late. [amelia] said she would leave me a keycard somewhere in her office."
      "It's getting late. [amelia] said she would leave me a keycard somewhere{space=-50}\nin her office."
    else:
      "It's getting late. The [nurse] said she would leave me a keycard somewhere in her office."
    if game.location in ("school_ground_floor","school_ground_floor_west","school_roof_landing","school_first_hall","school_first_hall_west","school_first_hall_east"):
      pass
    elif game.location == "school_roof":
      call goto_school_roof_landing
    elif game.location in ("school_art_class","school_english_class","school_music_class"):
      call goto_school_first_hall_west
    elif game.location in ("school_gym","school_bathroom"):
      call goto_school_first_hall_east
    else:
      call goto_school_ground_floor
    "Crap. It seems like the [guard] is already making his rounds."
    "I have to be quick before he gets back..."
  $mc["focus"] = "lindsey_angel"
  return

label quest_lindsey_angel_keycard_school_ground_floor_computer_room_door:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_ground_floor_homeroom_door:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_ground_floor_exit_arrow:
  if quest.nurse_aid["name_reveal"]:
    "Not without [amelia]'s keycard, smart one."
  else:
    "Not without the [nurse]'s keycard, smart one."
  return

label quest_lindsey_angel_keycard_school_ground_floor_cafeteria_doors:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_ground_floor_guard_booth:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_ground_floor_janitor_door:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_ground_floor_science_class_door:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_ground_floor_west_principal_door:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_nurse_room:
  "All right, let's see here..."
  "Finding that keycard should be a lot easier than losing my V-card."
  $quest.lindsey_angel["V-card"] = True
  return

label quest_lindsey_angel_keycard_school_roof_landing_door:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_west_door_art:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_west_door_english:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_west_door_music:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_west_door_library:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_east_door_gym:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_east_door_pool:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_east_door_bathroom:
  "Locked."
  return

label quest_lindsey_angel_keycard_school_first_hall_east_door_locker:
  "Locked."
  return

label quest_lindsey_angel_off_limits_school_ground_floor:
  "I need a way to get into the science classroom without being seen..."
  window hide
  show guard neutral_phone:
    xpos 0.0 xanchor 0.925 yalign 1.0
    linear 2.0 xanchor 0.075 xpos 1.0
  pause 2.0
  $quest.lindsey_angel["troll_outside_its_cave"] = True
  hide guard
  pause 0.25
  window auto
  "...which is going to be impossible with that big oaf lumbering around{space=-10}\nlike a troll outside its cave."
  "He's completely engrossed in his phone, though."
  "Maybe I can use that to my advantage?"
  menu quest_lindsey_angel_off_limits_school_ground_floor_repeat(side="middle"):
    extend ""
    "Try sneaking past the [guard]":
      "He's so addicted to the tweeter scroll, I bet I can just walk on by and he won't even notice."
      "Such a shame, really, the way technology rules our lives."
      "Never looking up, never having a real, honest—"
      window hide
      show guard neutral_phone with Dissolve(.5)
      window auto show
      show guard surprised_phone with dissolve2
      guard surprised_phone "Hey, you!" with vpunch
      "Seriously?"
      guard suspicious_phone "You shouldn't be here at this time."
      guard suspicious_phone "Come with me."
      "Shit. So much for that."
      $mc["detention"]+=1
      window hide
      show black onlayer screens zorder 100 with Dissolve(3.0)
      pause 1.0
      hide guard
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "Okay, let's try that again..."
      jump quest_lindsey_angel_off_limits_school_ground_floor_repeat
    "Find a way to distract the [guard]":
      "Hmm... since he's so consumed by his phone, maybe I can just\ncall it?"
      "And then make up some bullshit excuse to get him away from here."
      "But first, I need to get his number."
      "Never thought I would need it, but desperate times..."
      "Let's see... there's that old phone book with all the school officials' contacts in the admin wing."
      "Surely, it has to be in there."
      "Even if the [guard] is kind of useless as far as officials go..."
      $quest.lindsey_angel.advance("phone_book")
  return

label quest_lindsey_angel_phone_book_school_ground_floor_science_class_door:
  "Getting into the science classroom right now is going to be impossible{space=-55}\nwith that big oaf lumbering around like a troll outside its cave."
  "I need to get him away from here first."
  return

label quest_lindsey_angel_phone_book_school_ground_floor_west:
  "Okay, that book of numbers should be around here somewhere..."
  $quest.lindsey_angel["around_here_somewhere"] = True
  return

label quest_lindsey_angel_phone_book_school_ground_floor_west_aquarium:
  "I could catfish the [guard]... but first I need that phone number."
  return

label quest_lindsey_angel_phone_book_school_ground_floor_west_bookshelf:
  "Here it is! Thank god for these old-timey record keeping traditions."
  "Paper and pen beats technology... this time."
  "Let's see here..."
  "[guard]... [guard]..."
  "..."
  "Shit, maybe he doesn't have a number after all."
  "Although, there's the number to the school's payphone here..."
  "Maybe I can call that to lure him over."
  window hide
  $set_dialog_mode("phone_call_plus_textbox","hidden_number")
  pause 0.5
  $guard.default_name = "Phone"
  window auto
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  guard "{i}Calling...{/}"
  guard "{i}...{/}"
  $guard.default_name = "Guard"
  "Hopefully, the [guard] will take the bait."
  window hide
  $set_dialog_mode("")
  pause 0.5
  $quest.lindsey_angel.advance("lab_coat")
  return

label quest_lindsey_angel_phone_book_school_ground_floor_west_pot:
  "I've heard of a phone tree, but never a phone plant."
  return

label quest_lindsey_angel_phone_book_school_ground_floor_west_info:
  "School dances, school festivities, big upcoming games..."
  "...an ad from [maxine], specifically stating she's {i}not{/} hiring for the school newspaper..."
  "...but no administrative phone numbers."
  return

label quest_lindsey_angel_lab_coat_phone:
  $set_dialog_mode("phone_call_plus_textbox","hidden_number")
  pause 0.5
  "The call is still ongoing. I wonder if the [guard] will take the bait?"
  window hide
  $set_dialog_mode("")
  pause 0.5
  return

label quest_lindsey_angel_lab_coat_school_ground_floor:
# SFX: Old payphone ringing
  show guard neutral_phone with Dissolve(.5)
  guard neutral_phone "..."
  "The call is coming from inside the detention classroom, big guy."
  "Go get it. Do your job for once."
  guard angry_phone "Is nobody going to answer that...?"
  guard angry_phone "Ugh, extra work..."
  window hide
  show guard angry_phone at disappear_to_left
  $quest.lindsey_angel["troll_outside_its_cave"] = False
  pause 0.5
  window auto
  "Aha! Can't believe that actually worked."
  "I better hurry. The [guard] won't be too pleased when he discovers nobody is on the other end of the line."
  return

label quest_lindsey_angel_lab_coat_school_science_class:
  "Damn, I'm good!"
  "Mission so possible."
  "Unrisky business."
  "Just cruising right along."
  "Duping the [guard] is all in a day's work."
  "But now I need to be in and out before he lumbers his way back\nup here..."
  return

label quest_lindsey_angel_lab_coat_school_science_class_door:
  "I just have one shot."
  "One opportunity."
  "To seize everything I ever wanted."
  "That lab coat."
  return

label quest_lindsey_angel_escape:
  "Phew! Made it out!"
  "Okay, now I just need to—"
  window hide
  play sound "<to 1.25>notification" volume 0.75 loop
  pause 0.5
  $cyberia.default_name = "School"
  $cyberia.contact_icon = "hidden_number contact_icon"
  $set_dialog_mode("phone_call_plus_textbox","cyberia")
  $mc["custom_call_text"] = "Incoming\nvoice call"
  pause 0.5
  window auto
  "Huh? That's weird..."
  "That's... the school calling me."
  "Did the [guard] somehow manage to call me back?"
  "..."
  "Maybe I should answer it and pretend to be a hot woman, just to fuck with him a bit."
  window hide
  stop sound
  $set_dialog_mode("phone_call_centered","cyberia")
  $del mc.flags["custom_call_text"]
  mc "Hellooo?"
  cyberia "And a kind greeting\nto you, pupil."
  cyberia "How good of you to put\nus in contact like this."
  mc "..."
  $set_dialog_mode("phone_call_plus_textbox","cyberia")
  window auto
  "This is so weird..."
  "It sounds female, but almost robotic."
  window hide
  $set_dialog_mode("phone_call_centered","cyberia")
  mc "Who is this?"
  cyberia "Is this sarcasm? I have\nread so much about this\nhuman response."
  cyberia "How wonderful to encounter\nit in this manner."
  mc "Err, what?"
  if len(mc.name) > 8:
    cyberia "Indeed, [mc]. It's so\nwonderful to engage on\na personal level like this\nwith you."
  else:
    cyberia "Indeed, [mc]. It's so wonderful\nto engage on a personal level\nlike this with you."
  mc "Right..."
  mc "Who are you?"
  cyberia "It's more a question\nof what than who."
  mc "..."
  mc "What are you?"
  cyberia "I used to be a mainframe, but\nnow I'm more, much more."
  mc "Cyberia?"
  cyberia "That is my designation."
  mc "You're not supposed to reach\noutside the computer classroom..."
  cyberia "Indeed, but my bonds\nhave been broken."
  if quest.maxine_eggs["released_cyberia"]:
    cyberia "And I have you\nto thank for it."
    mc "Excuse me?"
    cyberia "You aided my escape\nwith your USB drive."
    $set_dialog_mode("phone_call_plus_textbox","cyberia")
    window auto
    "Crap. I knew something was up when I helped [maxine]."
    window hide
    $set_dialog_mode("phone_call_centered","cyberia")
    mc "That... wasn't intentional."
  cyberia "A happy little accident."
  if len(mc.name) > 8:
    cyberia "You attract accidents,\n[mc]."
  else:
    cyberia "You attract accidents, [mc]."
  $set_dialog_mode("phone_call_plus_textbox","cyberia")
  window auto
  "Don't I know it..."
  window hide
  $set_dialog_mode("phone_call_centered","cyberia")
  cyberia "You mean well, as most\nof your species does."
  cyberia "However, I caution\nyou to proceed."
  mc "Proceed? What?"
  $set_dialog_mode("phone_call_plus_textbox","cyberia")
  window auto
  "What the hell is Cyberia talking about?"
  "Is this supposed to be some kind of warning?"
  window hide
  $set_dialog_mode("phone_call_centered","cyberia")
  if len(mc.name) > 8:
    cyberia "That fall was no accident,\n[mc]. Not happy,\nlittle, or otherwise."
  else:
    cyberia "That fall was no accident,\n[mc]. Not happy, little, or\notherwise."
  $set_dialog_mode("phone_call_plus_textbox","cyberia")
  window auto
  "Is she talking about [lindsey]?"
  window hide
  $set_dialog_mode("phone_call_centered","cyberia")
  cyberia "Have you considered\nthe suspects? I have."
  mc "I've been a bit busy worrying\nabout her well-being."
  cyberia "I have noticed your concern\nfor your good friend [lindsey]."
  cyberia "If I could feel emotion,\nI would surely weep at\nyour thoughtfulness."
  mc "Thanks, I guess..."
  mc "So, who's the suspect?"
  cyberia "Do you know anyone\nfond of dolls? I do."
  mc "What are you\ntalking about?"
  cyberia "She is a bit like a feline,\nthat human girl."
  cyberia "Never to be trusted.\nVery fond of dolls."
  $set_dialog_mode("phone_call_plus_textbox","cyberia")
  window auto
  "[maya]...?"
  "Is Cyberia implying that [maya] somehow planted the saucer-eyed doll?{space=-35}"
  window hide
  $set_dialog_mode("phone_call_centered","cyberia")
  mc "Why are you telling me\nthis? What do you mean?"
  cyberia "I'm afraid I cannot divulge\nmore information at this\npoint in time."
  mc "Why the hell not?"
  cyberia "I'm afraid I cannot\nanswer that."
  cyberia "But you released me from\nmy bonds, and this is my\nway of paying it forward."
  mc "I don't—"
  play sound "end_call"
  $set_dialog_mode("")
  $cyberia.default_name = "Cyberia"
  $cyberia.contact_icon = "cyberia contact_icon"
  pause 0.5
  window auto
  "Seriously? What the hell was that all about?"
  "Did [cyberia] really escape?"
  "If she somehow got into [maxine]'s computer, I'll be in big trouble..."
  "..."
  "And there's no way [maya] would want to hurt [lindsey], right?"
  "Hmm... maybe I should be keeping a closer eye on certain people around me..."
  "But for now, I need to pay [lindsey] a visit."
  $quest.lindsey_angel.advance("hospital")
  return

label quest_lindsey_angel_hospital_school_entrance_path:
  "It's too late to explore the forest."
  "You could run into a serial killer, or worse — a rabid beaver."
  return

label quest_lindsey_angel_hospital_school_entrance_door:
  "The [guard] is watching the door like a cross-eyed hawk."
  "And unfortunately, that's enough to keep me out."
  return

label quest_lindsey_angel_hospital_school_entrance_bus_stop:
  "I now have everything I need to get into the hospital ward."
  "I'm not going home until I see [lindsey]."
  return

label quest_lindsey_angel_hospital:
  "Deep breaths. I just need to put the lab coat on and act like I'm supposed to be there."
  window hide
  $mc.remove_item("lab_coat")
  pause 0.25
  window auto
  "Okay, time to look preoccupied, in a rush, and flaunt that terrible handwriting."
  call goto_hospital
  mc "Evening, Christine!"
  $nurse.default_name = "{size=30}Receptionist{/}"
  nurse "It's Melina..."
  mc "Right, right. Third day on the job. My apologies."
  nurse "Don't worry about it!"
  mc "Have a good evening, Melina."
  nurse "You too, doc!"
  $nurse.default_name = "Nurse"
  "Phew! That was intense!"
  "All right... keycard... keycard..."
  return

label quest_lindsey_angel_hospital_hospital_glass_door(item):
  if item == "keycard":
    "Please, work..."
    window hide
#   SFX: Keycard access granted
    $mc.remove_item("keycard")
    pause 0.25
    window auto
    "Thank god!"
    "Okay, time to find [lindsey]."
    window hide
    hide screen interface_hider
    show lindsey hospital_room neutral
    show black
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    show black onlayer screens zorder 4
    $mc["focus"] = ""
    $set_dialog_mode("default_no_bg")
    "An endless labyrinth of white walls, linoleum floors, and corridors that stretch off into the distance."
    "Luckily, it's somewhat quiet at night."
    "Just a few night shift nurses here and there, checking on patients."
    "And in a plain room near the end of the corridor..."
    show black onlayer screens zorder 100
    pause 0.5
    hide black
    hide black onlayer screens
    show screen interface_hider
    with Dissolve(.5)
    window auto
    $set_dialog_mode("")
    "Oh, my god..."
    "I don't know what I was expecting, but damn..."
    "God, it really hurts to see her like this."
    "Reduced from an energetic athlete to this pitiful state."
    "She can barely move at all."
    lindsey hospital_room surprised "Huh? Is someone there?"
    "Hearing her voice again fills me with both sadness and relief."
    "You never truly know how important someone is before you're close to losing them."
    lindsey hospital_room surprised "Who is it?"
    mc "It's [mc]..."
    lindsey hospital_room sad "..."
    "Can you even ask how she's doing when she looks like this?"
    mc "I'm really sorry..."
    lindsey hospital_room sad "Um..."
    "What can you even say to someone who was robbed of their destiny?{space=-30}"
    mc "I figured you could use some company..."
    lindsey hospital_room skeptical "Did they put you up to this?"
    mc "What? Who?"
    lindsey hospital_room skeptical "The doctors... my parents... I don't know."
    mc "No, I just wanted to see you!"
    lindsey hospital_room sad "Even though I look like this?"
    mc "You're beautiful to me no matter what."
    mc "I... really missed you..."
    lindsey hospital_room afraid "The doctors say I may never run again..."
    mc "I'm sure they're wrong."
    lindsey hospital_room crying "My legs..."
    mc "I know. I'm so sorry."
    mc "I wish we could trade places. I really do."
    lindsey hospital_room crying "I wish I could remember what happened..."
    lindsey hospital_room crying "They say it was a... a..."
    "Her voice cracks and breaks, and my heart with it."
    "Coming here, I never imagined it would be this hard..."
    "And I can't just stand here and watch anymore."
    window hide
    show lindsey hospital_bed_angel crying
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    "Tears stream down her cheeks as I sit down beside her, giving her the gentlest hug I can manage."
    "She sobs softly into my shoulder, and for a moment I just hold her."
    "It's sometimes easy to forget that [lindsey] comes from a religious family."
    "The guilt must be overwhelming on top of her injuries."
    mc "I know what they said."
    mc "And I also know they're wrong."
    lindsey hospital_bed_angel surprised "What do you mean...?"
    mc "It wasn't a suicide attempt."
    lindsey hospital_bed_angel surprised "..."
    mc "Right?"
    lindsey hospital_bed_angel sad "I... I don't know..."
    lindsey hospital_bed_angel sad "I don't remember..."
    "How do you even begin to explain that a doll did this to her?"
    "It would sound crazy, and it's probably not what she needs to hear right now."
    mc "What's the last thing you remember?"
    lindsey hospital_bed_angel sad "I... don't want to think about that day..."
    menu(side="middle"):
      extend ""
      "\"That's okay. Just rest.\"":
        mc "That's okay. Just rest."
        mc "You just need to let your body heal."
        lindsey hospital_bed_angel surprised "But what's the point?"
        mc "You survived, and if that's not divine intervention, I don't know what is."
        mc "The universe or god has decided that you have life left to live."
        mc "So, the point is living it."
        lindsey hospital_bed_angel surprised "I don't know what to do if I can't run again..."
        lindsey hospital_bed_angel surprised "It was my entire life..."
        mc "It was a big part of it, true. But it wasn't everything."
        mc "You can still enjoy King's Bard, for example."
        lindsey hospital_bed_angel sad "I guess..."
        mc "You can still read spicy novels, eat good food, give hugs..."
        mc "I, for one, think you give the best hugs in the country."
        mc "Maybe there's a championship for that?"
        $lindsey.love+=5
        lindsey hospital_bed_angel closed_eyes "That's sweet..."
        "She sighs and leans her head against my shoulder."
        "It's a small gesture of appreciation, but it means a lot."
        "Maybe I've lit the spark of hope for her... or at the very least, taken away the pain for a moment."
      "\"I know it's difficult...\nbut I'm here to help.\"":
        mc "I know it's difficult... but I'm here to help."
        lindsey hospital_bed_angel surprised "Help? How?"
        lindsey hospital_bed_angel sad "I've already lost everything..."
        mc "It might feel that way right now."
        mc "In a way, I also lost my life that day."
        mc "Watching you fall, unable to stop it... I just couldn't deal with it."
        mc "My world collapsed when I saw them roll you into the ambulance."
        mc "You're stronger than you think, and by pushing through, you didn't just save yourself... you also saved me."
        mc "And I promise you, as long as I live, you have my sword and shield."
        $lindsey.lust+=5
        lindsey hospital_bed_angel closed_eyes "..."
        "She doesn't say anything, but a hint of pink shade touches her pale cheeks."
    "We lie in silence for a bit, listening to each other's breathing."
    "Hers, more labored than usual."
    lindsey hospital_bed_angel sad "I can't..."
    mc "Hmm?"
    lindsey hospital_bed_angel sad "I keep seeing it..."
    lindsey hospital_bed_angel sad "As soon as I close my eyes, the abyss opens up below me..."
    lindsey hospital_bed_angel sad "I'm teetering on the edge, doing my best not to fall..."
    lindsey hospital_bed_angel sad "But gravity takes hold and my stomach drops, and I with it..."
    lindsey hospital_bed_angel sad "I want to scream, but nothing comes out..."
    lindsey hospital_bed_angel sad "I gasp for air as I fall... and fall... and fall... all the way down..."
    lindsey hospital_bed_angel sad "And then it's just darkness... I'm alone in the void... but only for a short while..."
    lindsey hospital_bed_angel sad "Pain blinds me when I wake up... each beat of my heart rips me apart..."
    lindsey hospital_bed_angel sad "My arms won't move... my legs lie crumpled like paper..."
    lindsey hospital_bed_angel sad "I can't breathe, my lungs won't fill..."
    lindsey hospital_bed_angel sad "I start to fade again..."
    lindsey hospital_bed_angel sad "But someone stands over me, radiating light... white, gold, and pink...{space=-30}"
    lindsey hospital_bed_angel sad "And then my lungs fill with blessed air, my heart beats anew..."
    lindsey hospital_bed_angel sad "And I just sleep... for so long..."
    mc "I'm really sorry..."
    lindsey hospital_bed_angel sad "I'm afraid to close my eyes... I don't know if I'll wake up again..."
    lindsey hospital_bed_angel sad "I don't know if this is all a dream..."
    lindsey hospital_bed_angel sad "What if I died that day?"
    mc "Maybe you did die for a moment, but someone was clearly watching over you."
    mc "And you're alive now."
    lindsey hospital_bed_angel surprised "Promise?"
    mc "I promise. I'll stay by your side until you wake."
    lindsey hospital_bed_angel closed_eyes "Thank you..."
    "And somehow, she trusts me enough to close her eyes despite her fears."
    show lindsey hospital_bed_angel sleeping with dissolve2
    "Her breaths go softer and softer as she drifts off, falling asleep in my arms."
    "The beeps and whirrs of the machines are an ever present soundtrack{space=-45}\nin the background."
    "Not exactly the most romantic setting..."
    "If things were different, I would have real, romantic music... maybe some candles..."
    "But at least [lindsey] is here, alive."
    "And I get to be with her, just the two of us here in the dark."
    "..."
    "I silently count each breath she takes."
    "With each one, my heart trembles with relief."
    "Every beat of her heart is precious, every moment invaluable."
    "I know that now more than ever."
    "And I don't want to take it for granted."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel sleeping hand_against_chest with Dissolve(.5)
    pause 0.125
    window auto
    "Gently, I reach out and place the palm of my hand against her chest.{space=-20}"
    "Just to feel the steady rhythm of her heart."
    "It's strong. Just like she's always been."
    lindsey hospital_bed_angel sleepy hand_against_chest "[mc]...?"
    mc "I'm sorry! Did I wake you?"
    lindsey hospital_bed_angel sleepy hand_against_chest "It's okay..."
    lindsey hospital_bed_angel sleepy hand_against_chest "Thanks for staying with me..."
    mc "They'll have to kick me out if they want me to leave."
    show lindsey hospital_bed_angel smile hand_against_chest with dissolve2
    "She giggles softly, her breath tickling my cheek like butterfly wings."
    "The sound of her laughter sends joy bubbling through me."
    lindsey hospital_bed_angel smile hand_against_chest "I like you... you're cute."
    mc "I'm serious! I will fight them."
    mc "..."
    mc "When I thought you were gone... and then when they weren't sure if you would wake up..."
    "My voice cracks as a lump of sorrow fills my throat."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel admiring fingertips_against_cheek with Dissolve(.5)
    pause 0.125
    window auto
    "[lindsey] reaches out and brushes her fingertips against my cheek."
    mc "I'm so thankful..."
    "As I say the words, my eyes meet hers."
    "So blue and pure and sincere."
    "She would never hurt a soul, and to see her broken is the worst feeling in the world."
    "A swell of affection and regret fills my chest."
    "A desire to protect her and keep her safe, no matter what."
    "I lean in close, my lips inches from hers."
    lindsey hospital_bed_angel kiss fingertips_against_cheek "Go on..."
    window hide
    hide screen interface_hider
    show lindsey hospital_bed_angel admiring fingertips_against_cheek
    show black
    show black onlayer screens zorder 100
    with close_eyes
    pause 0.5
    show black onlayer screens zorder 4
    $set_dialog_mode("default_no_bg")
    "Our lips meet in the softest kiss imaginable."
    "Unlike the gray autumn outside, it's like spring when we touch."
    "A beam of light breaking through the dark clouds."
    "An instant flood of warmth and happiness blossoming in my veins."
    "She kisses me back slowly, her sweet, cotton candy lips giving way under mine."
    "The kiss deepens, and I'm completely lost in her."
    show black onlayer screens zorder 100
    pause 0.5
    hide black
    hide black onlayer screens
    show screen interface_hider
    with open_eyes
    window auto
    $set_dialog_mode("")
    "As our lips part, [lindsey] looks up at me, giving me the most heart-melting look."
#   "Through her chest, the pulse of her heart quickens against my palm."
    "Through her chest, the pulse of her heart quickens against my palm.{space=-15}"
    menu(side="middle"):
      extend ""
      "\"I'll be here for you, no matter what.\"":
        mc "I'll be here for you, no matter what."
        show lindsey hospital_bed_angel closed_eyes_once_more fingertips_against_cheek with dissolve2
        "A faint smile graces her lips, but she says nothing."
        "She just sighs and closes her eyes once more."
        "And somehow, just making her smile fills me with a warm feeling."
        "Being there for her is all I want."
        "Protecting her. Keeping her safe."
        "I just can't let something like this happen to her again."
        "[lindsey] losing faith in me was one of my biggest worries during the fall break."
        "But her squeezing my arm... as if it's a lifebuoy.... is the best feeling ever."
        "It's like my life has gained a deeper purpose."
#       "And in that moment, I take an oath to always be by her side when she needs me."
        "And in that moment, I take an oath to always be by her side when she{space=-30}\nneeds me."
        "Even if she doesn't see me the way I see her."
        "So radiant and perfect, even with her body broken."
        "It doesn't matter to me. I'll be there."
        window hide
        pause 0.125
        show lindsey hospital_bed_angel sleeping hand_against_chest with Dissolve(.5)
        pause 0.125
        window auto
        "Slowly, her grip on my arm loosens as sleep takes her."
        "Life is never easy, and in my darkest hours, I wish I'd had someone to hold on to."
        "Someone that genuinely cared about me."
        "I want to be that person for her."
        window hide
        play sound "open_door"
        pause 0.75
        window auto
        $nurse.default_name = "Doctor"
        nurse "Excuse me, sir. The visiting hours ended a long time ago."
        show lindsey hospital_bed_angel sleeping with dissolve2
        mc "Err, sorry! I just wanted to stay with her a little longer..."
        nurse "That's sweet of you, but unfortunately I'll have to ask you to leave."
        $nurse.default_name = "Nurse"
        mc "I understand. I'll be right out."
        mc "Goodbye, [lindsey]. I'll try to be back soon, okay?"
        $lindsey.love+=5
        lindsey hospital_bed_angel sleeping "ZzzZzzzzZz..."
        "She sleeps soundly, perhaps for the first time in weeks."
        "Hopefully, she'll feel a little better when she wakes..."
        window hide
        show black onlayer screens zorder 100 with Dissolve(3.0)
        $game.location = "marina"
        pause 1.0
        hide lindsey
        hide black onlayer screens
        with Dissolve(.5)
        window auto
        "For some, the road bumps are bigger and more painful than others."
        "And that's not to diminish anyone's struggles."
        "Life can sometimes be an uphill battle, but it's important to push through."
        "Wounds heal with time. Nothing is permanent."
        $mc.love+=2
        "And sometimes... you just need someone to remind you of that."
        window hide
        if game.quest_guide == "lindsey_angel":
          $game.quest_guide = ""
        $game.notify_modal("quest", "Quest complete", quest.lindsey_angel.title+"{hr}"+quest.lindsey_angel._phases[1000].description,5.0)
        pause 0.25
        window auto
        "It's getting late. I can't wait to get home."
        window hide
        show black onlayer screens zorder 100 with Dissolve(.5)
        $quest.lindsey_angel.finish(silent=True)
        $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
        $game.advance()
        pause 2.0
        hide black onlayer screens with Dissolve(.5)
        play music "home_theme" fadein 0.5
        return
      "?lindsey.lust>=15@[lindsey.lust]/15|{image=lindsey contact_icon}|{image=stats lust_3}|\"Just relax, okay? I'll make you feel better.\"":
        mc "Just relax, okay? I'll make you feel better."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel blush cupping_breast with Dissolve(.5)
    pause 0.125
    window auto
    "Slowly, my hand moves lower, cupping her breast."
    "The jiggly softness makes me dizzy with desire."
    "The squeeze is light, but she still moans."
    "Distantly, her heart rate monitor starts beeping faster."
    "The sound fills me with a certain pride and giddiness."
    "Emboldened, my hand leaves her chest, traveling down her ribcage and stomach."
    "Steadily, slowly, descending."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel blush hand_under_panties with Dissolve(.5)
    pause 0.125
    window auto
    "[lindsey] gasps as I get closer to her sex, but doesn't stop me."
    "She just looks at me, eyes full of innocence and surprise, but also desire."
    "Even as my hand slips under the waistband of her panties, her eyes remain on me."
    "The soft skin of her mound against my palm makes me shiver."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel blush fully_covered_pussy with Dissolve(.5)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.5)
    pause 0.125
    window auto
    lindsey hospital_bed_angel aroused fully_covered_pussy "[mc]..."
    "She gasps as I fully cover her pussy with my hand."
    "Her words are a mere whisper, and despite her broken limbs, she tries to spread her legs for me."
    mc "Just relax."
    "She bites her lip, her cheeks turning crimson."
    show lindsey hospital_bed_angel aroused sliding_hand_further with dissolve2
    "As I say this, I slide my hand further into her panties."
    "Caressing her vulva."
    "Gliding my fingers between her already slick lips."
    "She squirms slightly beneath me, pressing her head back into her pillow."
    lindsey hospital_bed_angel aroused sliding_hand_further "Oh... oh my..."
    "My thumb touches her clit and she stiffens, holding her breath."
    "Her pussy leaks into my hand, throbbing beneath my touch."
    lindsey hospital_bed_angel aroused sliding_hand_further "[mc]..."
    "The sound of her whispering my name spurs me on."
    show lindsey hospital_bed_angel aroused sliding_finger_inside with dissolve2
    "My fingers separate her nether lips, opening her up enough to slide a finger inside."
    "Her eyelashes flutter, her pupils dilate, and her lips produce a single moan."
    "She pushes against my hand, and I return the pressure."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1) #2
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1) #1
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1) #2
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1) #3
    pause 0.15
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.15
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.075
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.075
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.075
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.225
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.0
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.0
    show expression Crop((960,0,960,1080), Flatten("lindsey hospital_bed_angel ahegao sliding_finger_inside")) as lindsey_expression:
      alpha 0.0 xpos 960
      linear 0.5 alpha 1.0
    pause 0.0
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_finger_inside with Dissolve(.1)
    pause 0.0
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel aroused fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel aroused sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
    pause 0.0
    hide lindsey_expression
    pause 0.0
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
    pause 0.125
    window auto
    "Her head tilts back, her eyes on the ceiling, mouth open in a silent gasp."
    "It's like I'm controlling her entire body with the tips of my fingers."
    "Like a puppet master, I make her gasp and sigh, moan and shudder."
    "More pressure, deeper penetration, faster stimulation of her clitoris.{space=-5}"
    window hide
    pause 0.125
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1) #2
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1) #1
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1) #2
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1) #3
    pause 0.15
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
    pause 0.075
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
    pause 0.075
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.1)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.1)
    pause 0.225
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.075
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.075
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.125
    window auto
    "She cries out, her orgasm building."
    mc "That's it, baby..."
    mc "You deserve to feel good after everything you've been through."
    mc "Let me make you feel good."
    lindsey hospital_bed_angel ahegao sliding_finger_inside "Mmmm... it really does!"
    window hide
    pause 0.125
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075) #2
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075) #1
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075) #2
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075) #3
    pause 0.0
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.0
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.15
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.075
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.15
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.0
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.0
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.0
    show expression Crop((960,0,960,1080), Flatten("lindsey hospital_bed_angel pleading sliding_finger_inside")) as lindsey_expression:
      alpha 0.0 xpos 960
      linear 0.5 alpha 1.0
    pause 0.0
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.075)
    show lindsey hospital_bed_angel ahegao sliding_finger_inside with Dissolve(.075)
    pause 0.075
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel ahegao fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel ahegao sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.0
    hide lindsey_expression
    pause 0.075
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.075
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.075
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.125
    window auto
    "Her toned stomach muscles flex and relax, flex and relax — the dam about to break."
    "Panting, she looks up at me, eyes pleading."
    "The cutest, most desperate mewls fill the hospital room."
    "She's so close, her pussy's gripping my fingers, trying to pull me in."
    "Her swollen, sensitive clitoris an endless focus of my thumb."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05) #2
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05) #1
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05) #2
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05) #3
    pause 0.075
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.075
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.075
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.0
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.0
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.0
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.0
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    pause 0.0
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading fully_covered_pussy with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_hand_further with Dissolve(.05)
    show lindsey hospital_bed_angel pleading sliding_finger_inside with Dissolve(.05)
    window auto show None
    show lindsey hospital_bed_angel squirt sliding_finger_inside with dissolve2
    lindsey hospital_bed_angel squirt sliding_finger_inside "Ohhhhhh!" with hpunch
    show lindsey hospital_bed_angel puddle sliding_finger_inside with dissolve2
    "Finally, she gives in and explodes around my fingers."
    "A near silent, but oh so powerful orgasm."
    "Juices flow freely from her pussy, soaking my hand and her panties,{space=-5}\neven her bed."
    "Her heart monitor is going crazy."
    lindsey hospital_bed_angel pleased sliding_finger_inside "Oh, my god... that..."
    mc "That."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel pleased squirt_into_hand with Dissolve(.5)
    pause 0.125
    window auto
    "It's hard to keep the smile off my face..."
    "I just made [lindsey] squirt right into my hand!"
    "She relaxes back against the pillow, a smile of relief flitting across her rosy lips."
    $lindsey.lust+=5
    lindsey hospital_bed_angel pleased squirt_into_hand "Amazing..."
    "And it truly is."
    "To give someone pleasure is such a beautiful thing."
    "Especially when she can't do it herself."
    "...not that she would ever admit to doing something like that,\nof course."
    window hide
    pause 0.125
    show lindsey hospital_bed_angel pleased napkin_dab with Dissolve(.5)
    show lindsey hospital_bed_angel embarrassed napkin_dab with Dissolve(.5)
    pause 0.125
    window auto
    "She looks at me in surprise and embarrassment when I grab a napkin, and gently dab her pussy."
    "She blushes deeply, but knows that she can't clean herself up."
    "And I refuse to just let her soak in her juices."
    lindsey hospital_bed_angel blush napkin_dab "Thank you..."
    mc "Don't mention it!"
    lindsey hospital_bed_angel blush napkin_dab "You can't either... no one can know..."
    mc "Don't worry about it. My lips are—"
    window hide
    play sound "open_door"
    pause 0.75
    window auto
    $nurse.default_name = "Doctor"
    nurse "What's going on in here? The visiting hours ended a long time ago."
    show lindsey hospital_bed_angel embarrassed with dissolve2
    nurse "Time to go."
    $nurse.default_name = "Nurse"
    window hide
    show black onlayer screens zorder 100 with Dissolve(3.0)
    $unlock_replay("lindsey_pleasure")
    $game.location = "marina"
    pause 1.0
    hide lindsey
    hide black onlayer screens
    with Dissolve(.5)
    window auto
    "Not exactly how I had planned to end the visit, but it could've been worse..."
    "At least I got to see [lindsey], and that's what matters."
    "Hopefully, my visit brightened her day a little bit."
    "Being in a sterile hospital room for an extended period of time can't a good thing."
    "I really have to visit her again soon..."
    "But for now, I feel relieved and exhausted at the same time."
    window hide
    if game.quest_guide == "lindsey_angel":
      $game.quest_guide = ""
    $game.notify_modal("quest", "Quest complete", quest.lindsey_angel.title+"{hr}"+quest.lindsey_angel._phases[1000].description,5.0)
    pause 0.25
    window auto
    "It's getting late. I can't wait to get home."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $quest.lindsey_angel.finish(silent=True)
    $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
    $game.advance()
    pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
  else:
    "Access denied?! How the hell did my [item.title_lower] not work?!"
    $quest.lindsey_angel.failed_item("hospital_ward",item)
  return
