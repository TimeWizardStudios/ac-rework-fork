image lindsey_piano_no_slap =  LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-201,-97), Transform(LiveComposite((1920,1080), (-12,-119), "school ground_floor background", (319,0), "lindsey afraid_slap", (540,0), "isabelle skeptical"), size=(692,389))), "ui circle_mask"), (6,6), "ui crossed_out_circle1")


label quest_lindsey_piano_start:
  show lindsey eyeroll with Dissolve(.5)
  mc "Hey, [lindsey]!"
  lindsey eyeroll "..."
  mc "Rude, but okay..."
  lindsey eyeroll "..."
  mc "Are you all right?"
  lindsey eyeroll "..."
  "Something's definitely wrong..."
  mc "Hello? Can you move at all?"
  show lindsey concerned with Dissolve(.5)
  "Hmm... she responded... maybe?"
  show lindsey eyeroll with Dissolve(.5)
  "She's in some kind of trance."
  show lindsey eyeroll at move_to(.75)
  menu(side="left"):
    extend ""
    "Shake her":
      show lindsey eyeroll at move_to(.5)
      mc "[lindsey]!{w=1.0}{nw}" with Move((15,0), (-15,0), .10, bounce=True, repeat=True, delay=.825)
      extend " Hello?"
      $lindsey.love-=1
      lindsey annoyed "Ow! What the hell, [mc]?"
      mc "You were totally spaced out."
      lindsey annoyed "What do you mean?"
      mc "Like... unresponsive?"
      lindsey annoyed "Well, you didn't have to shake me..."
    "?mc.owned_item('water_bottle')@|{image=items bottle water_bottle}|Splash her with water" if (mc.owned_item("water_bottle") and mc.owned_item("spray_water")) or not mc.owned_item("spray_water"):
      show lindsey eyeroll at move_to(.5)
      $mc.remove_item("water_bottle")
      $mc.add_item("empty_bottle")
      label quest_lindsey_piano_start_water:
      play sound "water_splash3"
      mc "Squirt, squirt!"
      lindsey concerned "Hmm?"
      mc "You're alive!"
      lindsey laughing "What?"
      mc "You were in some kind of trance. Sorry about the water."
      lindsey flirty "That's okay... I was already a little sweaty from practice."
      mc "Are you all right?"
      $lindsey.lust+=1
      lindsey flirty "Yeah, I'm peachy! Thanks for checking on me."
    "?mc.owned_item('spray_water')@|{image=items bottle spray_water}|Splash her with water" if mc.owned_item("spray_water") and not mc.owned_item("water_bottle"):
      show lindsey eyeroll at move_to(.5)
      $mc.remove_item("spray_water")
      $mc.add_item("spray_empty_bottle")
      jump quest_lindsey_piano_start_water
    "Get help":
      show lindsey eyeroll at move_to(.5)
      "Hmm... I better get the [nurse]."
      hide lindsey with Dissolve(.5)
      $quest.lindsey_piano.start("nurse")
      return
  mc "I'm worried about you."
  lindsey smile "That's sweet of you, but I'm okay. Really!"
  mc "Are you sure?"
  lindsey laughing "I'm fine, just a bit stressed."
  lindsey flirty "I need to practice... I'm already behind schedule."
  lindsey flirty "See you later!"
  if game.location == "school_gym":
    hide lindsey with dissolve2
  else:
    show lindsey flirty at disappear_to_left
  "[lindsey] never has time to rest."
  "Maybe it's the stress that's causing her problems?"
  "I once read somewhere that stress really messes with your blood pressure..."
  "Hmm..."
  "Perhaps I could get her to relax somehow?"
  "When I get really stressed out, I always put on some music."
  "I wonder if that could help her as well..."
  $quest.lindsey_piano.start("piano")
  return

label quest_lindsey_piano_nurse:
  show nurse neutral with Dissolve(.5)
  mc "Something's wrong with [lindsey]!"
  nurse afraid "Oh, no! Where is she?"
  mc "Follow me!"
  $quest.lindsey_piano["follow_me"] = True
  hide nurse with Dissolve(.5)
  return

label quest_lindsey_piano_nurse_follow:
  show lindsey smile with Dissolve(.5)
  show nurse thinking at appear_from_left(.25)
  show lindsey smile at move_to(.75,1.0)
  pause 0.0
  lindsey skeptical "Um... what's going on?"
  mc "You're okay!"
  lindsey laughing "Why wouldn't I be?"
  mc "You were completely unresponsive earlier... I went to get the [nurse]."
  lindsey smile "I'm fine!"
  nurse thinking "Maybe we should run some tests."
  lindsey skeptical "I don't know what's going on, but I need to go to practice."
  lindsey skeptical "I'm already behind schedule."
  show nurse thinking at move_to(.5,1.0)
  if game.location == "school_gym":
    hide lindsey with dissolve2
  else:
    show lindsey skeptical at disappear_to_right
  mc "But—"
  nurse sad "You didn't make this up, did you?"
  mc "I swear, she was in some kind of trance earlier!"
  nurse concerned "That's a bit worrisome, but I can't do anything if she doesn't want to get tested."
  mc "Ugh..."
  $nurse.love+=1
  nurse blush "You did well to get me. Let me know if it happens again."
  show nurse blush at disappear_to_right
  "[lindsey] seems stressed about her upcoming competitions."
  "Maybe it's the stress that's causing her problems?"
  "I once read somewhere that stress really messes with your blood pressure..."
  "Hmm..."
  "Perhaps I could get her to relax somehow?"
  "When I get really stressed out, I always put on some music."
  "I wonder if that could help her as well..."
  $quest.lindsey_piano.advance("piano",silent=True)
  return

label quest_lindsey_piano_play:
  if quest.lindsey_piano["failed"]:
    "Okay, let's try that again..."
    "I should probably focus on [lindsey] this time."
  else:
    if not quest.lindsey_piano.started:
      "[lindsey] seems stressed about her upcoming competitions."
      "Maybe it's the stress that's causing her problems?"
      "I once read somewhere that stress really messes with your blood pressure..."
      "Hmm..."
    "Maybe I could help [lindsey] relax if I filled the halls with piano music?{space=-25}"
  stop music
  window hide
  show misc piano_play bg with Dissolve(.5)
  pause 1.0
  play music "fast_and_exciting" volume 0.75
  show screen music_notes
  pause 23.0
  play music "school_theme" fadein 0.5
  hide screen music_notes
  hide misc piano_play bg
  show lindsey laughing
  with Dissolve(.5)
  window auto
  lindsey laughing "Whoa! I liked that!"
  mc "Thank you!"
  "Huh! I didn't expect this to actually bring her here..."
  "Does she look more relaxed than usual? Hmm... hard to tell."
  lindsey smile "I never took you for the musical type, but this tune gives me this feeling..."
  show lindsey smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'm typically not, but I've been\npracticing lately.\"":
      show lindsey smile at move_to(.5)
      mc "I'm typically not, but I've been practicing lately."
      lindsey flirty "It's paying off!"
      mc "Thanks!"
      "Maybe I can keep her mind off the competition for a bit..."
      mc "Do you want to hear another one?"
      lindsey laughing "I'd love to!"
      show lindsey laughing at move_to(.25)
      menu(side="right"):
        extend ""
        "Play something fast but dark":
          show lindsey laughing at move_to(.5)
          stop music
          window hide
          show misc piano_play bg with Dissolve(.5)
          pause 1.0
          play music "fast_but_dark"
          show screen music_notes
          pause 23.0
          play music "school_theme" fadein 0.5
          hide screen music_notes
          hide misc piano_play bg
          show lindsey smile
          with Dissolve(.5)
          window auto
          show lindsey smile at move_to(.25,1.0)
          show kate excited at appear_from_right(.75)
          kate excited "That wasn't terrible."
          lindsey laughing "He's quite good, isn't he?"
          kate eyeroll "Let's not get carried away here."
          kate skeptical "Half of it was decent. The rest, not so much."
          mc "Which part did you like, [kate]?"
          kate excited "The pauses between the notes."
          lindsey skeptical "I don't think it was that bad!"
          show lindsey skeptical at move_to("left")
          show kate excited at move_to("right")
          menu(side="middle"):
            extend ""
            "?not quest.lindsey_piano['failed']@|\"I do need more practice.\"":
              show lindsey skeptical at move_to(.25)
              show kate excited at move_to(.75)
              mc "I do need more practice."
              kate excited "No doubt about that."
              mc "Maybe you can teach me?"
              kate eyeroll "Why would I waste my time on you?"
              lindsey cringe "..."
              lindsey thinking "I'm going to go..."
              show lindsey thinking at disappear_to_left
              show kate eyeroll at move_to(.5,1.0)
              pause 0.0
              kate excited "Don't forget about the party this weekend!"
              lindsey "I won't!"
              kate skeptical "Don't even think about it, [mc]. You're not invited."
              mc "I know that..."
              mc "Will you at least teach me if I call you \"ma'am?\""
              $kate.lust+=3
              kate flirty "I might consider it."
              mc "Teach me, please, ma'am."
              kate neutral "Not enough enthusiasm. Besides, I have to go."
              kate confident "Keep your eyes down... I don't want you perving on my back."
              show kate confident at disappear_to_left
              "She totally cockblocked me with [lindsey], didn't she?"
              "But honestly... [kate] can do whatever she wants."
              "Hopefully, it includes me somehow..."
              $quest.lindsey_piano.fail()
              $quest.lindsey_piano["failed"] = True
              $quest.lindsey_piano.failed = False
              $quest.lindsey_piano.started = False
              $quest.lindsey_piano.phase = 0
              return
            "\"Maybe you can show us how it's done, [kate]?\"{space=-55}":
              show lindsey skeptical at move_to(.25)
              show kate excited at move_to(.75)
              mc "Maybe you can show us how it's done, [kate]?"
              lindsey laughing "Yes! I'd love to hear that."
              kate eyeroll "Dream on. I have better things to do."
              mc "I hear a lot of talking, but see very little walking..."
              kate skeptical "I have nothing to prove."
              mc "If you say so."
              $kate.lust-=2
              kate annoyed "Whatever."
              show lindsey laughing at move_to(.5,1.0)
              show kate annoyed at disappear_to_right
              "Odd that she refused..."
              "[kate] is a straight-A student, surely she knows how to play the piano.{space=-15}"
              "Maybe she got intimidated?"
              if mc["moments_of_glory"] < 3 and not mc["moments_of_glory_talent"]:
                $mc["moments_of_glory"]+=1
                $mc["moments_of_glory_talent"] = True
                $game.notify_modal(None,"Guts & Glory","Moments of Glory: Talent\n"+str(mc["moments_of_glory"])+"/3 Unlocked!",wait=5.0)
#               if mc["moments_of_glory"] == 3:
#                 $game.notify_modal(None,"Dev Note","(they still don't do\nanything yet tho)",wait=5.0)
              lindsey smile "For what it's worth, I think you did great."
              mc "I appreciate that."
              mc "Maybe one day I'll be half as good as you're at running..."
              $lindsey.love+=1
              lindsey flirty "Aw, I'm sure you will!"
              mc "So, what sort of music do you usually listen to?"
              lindsey smile "I like medieval!"
              mc "I used to love medieval music."
            "\"Thanks, [lindsey]! I'm happy you liked it.\"":
              show lindsey skeptical at move_to(.25)
              show kate excited at move_to(.75)
              mc "Thanks, [lindsey]! I'm happy you liked it."
              kate cringe "She's out of your league, weirdo."
              mc "I wasn't..."
              lindsey smile "I think he just likes to play! Right, [mc]?"
              kate laughing "You sweet summer child."
              kate smile "Guys like him only have one thing on their mind."
              show lindsey smile at move_to("left")
              show kate smile at move_to("right")
              menu(side="middle"):
                extend ""
                "\"The only thing on my mind is music.\"":
                  show lindsey smile at move_to(.25)
                  show kate smile at move_to(.75)
                  mc "The only thing on my mind is music."
                  kate eyeroll "Spare me the lies."
                  lindsey smile "I believe him!"
                  $kate.lust-=1
                  kate cringe "I think I'm going to be sick..."
                  show lindsey smile at move_to(.5,1.0)
                  show kate cringe at disappear_to_right
                  pause 0.0
                  lindsey laughing "Don't worry about her! I think it's nice that you've found a hobby."
                  "Right. Don't worry about the bullies. That's easy..."
                  mc "I'll do my best, I guess."
                  lindsey smile "That's what I tell myself before stepping up on the track."
                  lindsey smile "I know my best is really good, but it's okay to fall short of it as long as I try!"
                  show lindsey smile at move_to (.75)
                  menu(side="left"):
                    extend ""
                    "\"You never fall short, though...\"":
                      show lindsey smile at move_to (.5)
                      mc "You never fall short, though..."
                      mc "You're a star on that racing track."
                      $lindsey.love+=1
                      lindsey laughing "Thanks, but you should see me at practice!"
                      lindsey thinking "For every great lap, there's a hundred mediocre ones."
                      lindsey thinking "It's like that with everything. People only show the world their best, never the hours of failure and hardships."
                      lindsey neutral "I'm sure that goes for pianists, as well!"
                      lindsey neutral "It's those that never give up who become really good at things. Hard work always beats talent in the long run."
                      mc "Hmm... I've never thought of it like that..."
                      "How does one practice love, though?"
                      "Maybe [lindsey] will tell me if I keep talking to her?"
                      "She always seemed so dumb before, but maybe there's more to her than meets the eye..."
                      mc "So, what sort of music do you usually listen to?"
                      lindsey excited "I like medieval!"
                      mc "I used to love medieval music."
                    "\"Sounds like you need someone\nto catch you.\"":
                      show lindsey smile at move_to(.5)
                      mc "Sounds like you need someone to catch you."
                      mc "Someone strong."
                      $lindsey.lust+=1
                      lindsey flirty "I suppose that wouldn't be so bad!"
                      show lindsey flirty at move_to(.25)
                      menu(side="right"):
                        extend ""
                        "?mc.strength>=8@[mc.strength]/8|{image=stats str}|Flex":
                          show lindsey flirty at move_to(.5)
                          pause 0.0
                          lindsey laughing "Oh, my god! You're so silly!"
                          mc "It's okay, have a feel if you want."
                          lindsey smile "If you insist..."
                          show lindsey flirty with dissolve2
                          lindsey flirty "..."
                          show lindsey blush with dissolve2:
                            easein 0.175 yoffset -10
                            easeout 0.175 yoffset 0
                            easein 0.175 yoffset -4
                            easeout 0.175 yoffset 0
                          $lindsey.lust+=1
                          lindsey blush "Whoa!"
                          show lindsey blush:
                            yoffset 0
                          mc "Let me know if you want to practice trust falls..."
                          lindsey laughing "Hah! As much as I'd like that, I have to get going."
                          mc "Can you at least tell me your favorite music genre before you go?"
                          lindsey smile "I'm into medieval stuff!"
                          mc "I used to love medieval music."
                        "\"I volunteer!\"":
                          show lindsey flirty at move_to(.5)
                          mc "I volunteer!"
                          lindsey skeptical "I think I'm okay, thanks."
                          "Ouch. That was perhaps a bit too thirsty."
                          "Now she thinks [kate] was right..."
                          "Ugh, why do I always say dumb shit?"
                          mc "That was a joke..."
                          lindsey smile "No worries."
                          lindsey smile "Sorry, but I have to run."
                          mc "Can you at least tell me your favorite music genre before you go?"
                          lindsey laughing "I'm into medieval stuff!"
                          mc "I used to love medieval music."
                        "\"Not that you'd need it...\"":
                          show lindsey flirty at move_to(.5)
                          mc "Not that you'd need it..."
                          $lindsey.love+=1
                          lindsey smile "Aww!"
                          lindsey smile "I think everyone could use a good catch every now and then."
                          lindsey flirty "I at least love feeling safe and cared for."
                          mc "I think you're right."
                          mc "So, what kind of music are you into?"
                          lindsey laughing "I'm into medieval stuff!"
                          mc "I used to love medieval music."
                "?not (quest.lindsey_piano['failed'] and mc.charisma < 6)@|\"[kate] is right... I was hoping for your panties as a token of appreciation.\"":
                  show lindsey smile at move_to(.25)
                  show kate smile at move_to(.75)
                  mc "[kate] is right... I was hoping for your panties as a token of appreciation."
                  if mc.charisma >= 6:
                    lindsey laughing "Oh, my god!"
                    kate laughing "I bet he'd sniff them too like the weirdo he is."
                    show lindsey laughing at move_to("left")
                    show kate laughing at move_to("right")
                    menu(side="middle"):
                      extend ""
                      "?not quest.lindsey_piano['failed']@|\"Only to confirm that they're not\nyours, [kate].\"":
                        show lindsey laughing at move_to(.25)
                        show kate laughing at move_to(.75)
                        mc "Only to confirm that they're not yours, [kate]."
                        $mc.charisma+=1
                        mc "I hate that sulphuric smell."
                        show lindsey skeptical
                        $kate.lust-=1
                        kate neutral "At least they don't smell like a virgin loser like yours."
                        kate confident "You do wear panties, right?"
                        "Crap, [kate] is too good at this. Too much experience."
                        lindsey cringe "I'm going to go..."
                        lindsey cringe "See you around."
                        show lindsey cringe at disappear_to_left
                        show kate confident at move_to(.5,1.0)
                        "Ugh, [lindsey] left..."
                        "[kate] totally cockblocked me, didn't she?"
                        kate confident "Yeah, I'm bored with you, as well."
                        show kate confident at disappear_to_left
                        "Left with nothing. Damn it."
                        $quest.lindsey_piano.fail()
                        $quest.lindsey_piano["failed"] = True
                        $quest.lindsey_piano.failed = False
                        $quest.lindsey_piano.started = False
                        $quest.lindsey_piano.phase = 0
                        return
                      "\"Too far, [kate].\"":
                        show lindsey laughing at move_to(.25)
                        show kate laughing at move_to(.75)
                        mc "Too far, [kate]."
                        mc "You should've left the joke as it was."
                        mc "I play because I enjoy it... If others like it, that's a bonus."
                        show kate neutral
                        $lindsey.love+=1
                        lindsey smile "I like that sentiment!"
                        $kate.lust-=1
                        kate eyeroll "Of course, you do..."
                        kate skeptical "I have better things to do."
                        show lindsey smile at move_to(.5,1.0)
                        show kate skeptical at disappear_to_right
                        lindsey smile "I hope you play again sometime."
                        mc "I could! What's your favorite kind of music?"
                        lindsey flirty "I like medieval!"
                        mc "I used to love medieval music."
                  else:
                    show kate neutral
                    lindsey afraid "You're joking!"
                    mc "That was sarcasm! I obviously didn't—"
                    kate neutral "See, [lindsey]?"
                    kate neutral "Be careful with this one. A horny creep is the worst kind."
                    "Crap. That did not go as planned."
                    $lindsey.lust-=1
                    $lindsey.love-=1
                    lindsey cringe "I thought better of you, [mc]..."
                    mc "It was a joke!"
                    lindsey cringe "I don't see anyone laughing."
                    lindsey thinking "I've got to go. Bye, [kate]."
                    show lindsey thinking at disappear_to_left
                    show kate neutral at move_to(.5,1.0)
                    $lindsey["romance_disabled"] = True
                    $game.notify_modal(None,"Love or Lust","Romance with [lindsey]:\nDisabled.",wait=5.0)
                    kate laughing "That went well, I think!"
                    mc "Ugh..."
                    kate smile "You only have yourself to blame for that one."
                    kate smile "But let's be honest, that's usually the case, isn't it?"
                    mc "I guess..."
                    kate excited "Good. As long as you understand your failings, I'm happy."
                    kate excited "Anyway, I have better things to do."
                    show kate excited at disappear_to_left
                    "What's the point of even trying...?"
                    $quest.lindsey_piano.fail()
                    return
        "?not quest.isabelle_stolen['lindsey_slaped']@|{image=lindsey_piano_no_slap}|Play something exciting\nbut sweet":
          show lindsey laughing at move_to(.5)
          stop music
          window hide
          show misc piano_play bg with Dissolve(.5)
          pause 1.0
          play music "exciting_but_sweet"
          show screen music_notes
          pause 23.0
          play music "school_theme" fadein 0.5
          hide screen music_notes
          hide misc piano_play bg
          show lindsey smile
          with Dissolve(.5)
          window auto
          show lindsey smile at move_to(.25,1.0)
          show isabelle confident at appear_from_right(.75)
          isabelle confident "Nice, [mc]! I heard you from way over there and had to listen."
          isabelle confident "You're good!"
          lindsey smile "Oh, he is!"
          lindsey laughing "Hi, by the way!"
          isabelle laughing "Hey! How are you?"
          lindsey flirty "I'm just fine! I like your hair today."
          isabelle neutral "Thanks! I like your shirt. It totally matches your eyes."
          lindsey blush "It's my favorite color!"
          isabelle neutral "The blueness reminds me of the lakes in the Scottish highland."
          lindsey blush "I hope I can be good enough one day so I can travel..."
          lindsey blush "I'd really like to see the world."
          isabelle excited "I'm sure you will!"
          "It's fascinating watching girls interact. It seems so natural."
          "I wonder what they say when nobody's watching..."
          "Is it the same as now? Is it different?"
          "Do they even like each other, or is this just the default niceness?"
          show lindsey blush at move_to("left")
          show isabelle excited at move_to("right")
          menu(side="middle"):
            extend ""
            "\"I'm still here, by the way.\"":
              show lindsey blush at move_to(.25)
              show isabelle excited at move_to(.75)
              mc "I'm still here, by the way."
              show isabelle concerned
              lindsey thinking "..."
              isabelle concerned "..."
              mc "I thought you guys forgot about me..."
              lindsey cringe "What?"
              isabelle skeptical "We were just talking."
              "Crap. This just became awkward."
              "It was a stupid thing to say, but it felt like they'd forgotten about me...{space=-40}"
              "Last time around, I would've let them talk and then drift away. But not this time!"
              $mc.intellect+=1
              mc "Did you know that when Mozart died, he was buried in a common grave without a big ceremony?"
              lindsey thinking "I did not."
              mc "Well, now you do!"
              $isabelle.love-=1
              isabelle skeptical "I see what you did there, [mc], and I don't approve."
              "Can she really blame me for the change of topic?"
              isabelle neutral "Anyway, I enjoyed the tune. Good luck practicing."
              show lindsey thinking at move_to(.5,1.0)
              show isabelle neutral at disappear_to_right
              "I guess she can..."
              lindsey smile "I like [isabelle]! She seems very real."
              mc "Maybe a bit too real."
              lindsey laughing "You don't like her?"
              show lindsey laughing at move_to(.75)
              menu(side="left"):
                extend ""
                "\"I think her moral system is too rigid.\"":
                  show lindsey laughing at move_to(.5)
                  mc "I think her moral system is too rigid."
                  lindsey skeptical "What do you mean?"
                  mc "She has a hard time compromising or backing down, and it got her into a fight [kate]."
                  lindsey skeptical "I think sticking to your principles is a good thing."
                  mc "I don't know, being flexible is smarter sometimes."
                  lindsey skeptical "If you always bend with the wind, you'll get blown away sooner or later.{space=-60}"
                  mc "I just don't like confrontations..."
                  $lindsey.lust-=1
                  lindsey smile "That's too bad. I think being able to stand up for yourself and others is an admirable trait."
                  lindsey smile "Anyway, I have to run. Thanks for the double concert!"
                  mc "Wait!"
                  lindsey flirty "Yeah?"
                  mc "Can you at least tell me your favorite music genre before you go?"
                  lindsey laughing "I like medieval!"
                  mc "I used to love medieval music."
                "\"I think she's okay. Maybe a bit too English for my taste.\"":
                  show lindsey laughing at move_to(.5)
                  mc  "I think she's okay. Maybe a bit too English for my taste."
                  lindsey flirty "I like her accent!"
                  $mc.charisma+=1
                  mc "It's proper scrummy, innit?"
                  lindsey laughing "Hah! That's a great impression!"
                  mc "So, what's your favorite kind of music?"
                  lindsey smile "I like medieval!"
                  mc "I used to love medieval music."
                "\"I just meant she might be too real for her own good.\"":
                  show lindsey laughing at move_to(.5)
                  mc "I just meant she might be too real for her own good."
                  mc "I do like that about her — selfless, independent, a strong moral core.{space=-20}"
                  mc "But the world is a tough place for people like that."
                  lindsey smile "You're right, but those are all good traits!"
                  lindsey smile "She could become president someday..."
                  lindsey flirty "...or a superheroine!"
                  mc "Yeah, I still have a long way to go, but I'd like to be more like her."
                  $lindsey.lust+=1
                  lindsey smile "That is a great goal to have!"
                  mc "So, what's your favorite kind of music?"
                  lindsey laughing "I like medieval!"
                  mc "I used to love medieval music."
            "\"With your talent? You'll soon be\ntraveling the world!\"":
              show lindsey blush at move_to(.25)
              show isabelle excited at move_to(.75)
              mc "With your talent? You'll soon be traveling the world!"
              show isabelle neutral
              $lindsey.love+=1
              lindsey laughing "Oh, stop it!"
              mc "You should see her on the track, [isabelle]."
              mc "She's faster than a laser bullet!"
              isabelle laughing "I believe you! [lindsey] has the perfect body for a runner."
              isabelle confident "I'll have to come watch her compete some time."
              lindsey laughing "You guys..."
              show lindsey laughing at move_to("left")
              show isabelle confident at move_to("right")
              menu(side="middle"):
                extend ""
                "?mc.charisma>=8@[mc.charisma]/8|{image=stats cha}|\"She has the perfect body for {i}other{/} activities, too.\"":
                  show lindsey laughing at move_to(.25)
                  show isabelle confident at move_to(.75)
                  mc "She has the perfect body for {i}other{/} activities, too."
                  show isabelle skeptical
                  lindsey smile "Like what?"
                  isabelle skeptical "Yeah, like what, exactly?"
                  mc "Oh, you know... wrestling... gymnastics... swimming... and other physical activities!"
                  $lindsey.love+=1
                  lindsey flirty "Thank you!"
                  isabelle skeptical "Right... those kinds of activities... that's what you meant..."
                  mc "I'm sure she's great at whatever you're thinking of, too."
                  lindsey laughing "What were you thinking of, [isabelle]?"
                  isabelle concerned "Nothing. Forget about it."
                  isabelle concerned "[mc] is just trying to be funny."
                  mc "That is the most slanderous thing I've heard!"
                  mc "Anyway, ladies... I have to keep practicing."
                  isabelle neutral "Okay, I should get going, as well. Bye, [lindsey]!"
                  isabelle skeptical "I'm watching you, [mc]."
                  mc "Oh, and I'll be watching {i}you...{/}"
                  isabelle blush "..."
                  $isabelle.love+=1
                  isabelle blush "You're shameless."
                  window hide
                  show lindsey laughing at move_to(.5,1.0)
                  show isabelle blush at disappear_to_right
                  pause 0.5
                  window auto
                  mc "So, what's your favorite kind of music?"
                  lindsey smile "I like medieval!"
                  mc "I used to love medieval music."
                "\"Come on, [lindsey], you know you deserve all the praise!\"":
                  show lindsey laughing at move_to(.25)
                  show isabelle confident at move_to(.75)
                  mc "Come on, [lindsey], you know you deserve all the praise!"
                  lindsey blush "But my poor cheeks! Thank you!"
                  isabelle neutral "Here's my number. Text me when the next race is and I'll come cheer for you."
                  lindsey blush "Really?"
                  isabelle excited "Yes! You seem nice, unlike certain other people in this school. I'd love to support you."
                  lindsey smile "Okay, here you go! I hope I don't disappoint you..."
                  mc "You'll do great, [lindsey]. No one can compete with you."
                  $lindsey.love+=1
                  lindsey flirty "Aw, thanks!"
                  isabelle neutral "I have to go, but remember to text me!"
                  lindsey laughing "I will!"
                  show lindsey laughing at move_to(.5,1.0)
                  show isabelle neutral at disappear_to_right
                  mc "So, what's your favorite kind of music?"
                  lindsey smile "I like medieval!"
                  mc "I used to love medieval music."
                "?not quest.lindsey_piano['failed']@|\"How do you like Newfall so far, [isabelle]?\"":
                  show lindsey laughing at move_to(.25)
                  show isabelle confident at move_to(.75)
                  mc "How do you like Newfall so far, [isabelle]?"
                  isabelle neutral "It's all right. It's small and cozy. Very unlike London."
                  lindsey smile "That's true! There aren't many stores or restaurants. I usually have to go to the city to buy sports gear."
                  mc "I've lived here my entire life... to be honest, I'm kind of hating it."
                  isabelle concerned "It can't be that bad!"
                  isabelle concerned "Lots of nature, a great library, and the people seem friendly..."
                  isabelle skeptical "...well, most of them."
                  mc "It feels different after a while, I guess."
                  lindsey blush "Just one more year until college!"
                  "Ugh... just thinking of the future makes me depressed. Nothing good awaits after graduation."
                  "Just a pit of despair and self-loathing. The end of all hope."
                  lindsey thinking "I hate to do this, but I have to run... I got a busy day. Sorry, sorry!"
                  isabelle excited "Don't worry! It was nice talking to you."
                  lindsey laughing "Likewise!"
                  lindsey flirty "Bye! Thanks for the concert, [mc]!"
                  show lindsey flirty at disappear_to_left
                  show isabelle excited at move_to(.5,1.0)
                  pause 0.0
                  isabelle neutral "[lindsey] seems sweet."
                  mc "I guess."
                  isabelle concerned "You don't think so?"
                  show isabelle concerned at move_to(.25)
                  menu(side="right"):
                    extend ""
                    "\"I've always found her a bit shallow.\"":
                      show isabelle concerned at move_to(.5)
                      mc "I've always found her a bit shallow."
                      mc "I like girls who are more than just a pretty face."
                      isabelle sad "Hmm... I think I can see that. She seems a bit naive."
                      mc "Right. Real world issues fly right over her head."
                      mc "I like to have deeper conversations, you know?"
                      isabelle annoyed "Definitely. Debate is important, lest society collapses."
                      mc "And democracy."
                      $isabelle.love+=2
                      isabelle blush "Couldn't agree more! Some things are just more important than others.{space=-70}"
                      isabelle blush "I'll let you continue practicing now."
                      show isabelle blush at disappear_to_left
                      "[isabelle] is such a breath of fresh air in this place..."
                      "I like her brain, and she's cute too. A perfect combination."
                    "\"She's all right.\"":
                      show isabelle concerned at move_to(.5)
                      mc "She's all right."
                      mc "But she hangs with the wrong crowd, you know?"
                      isabelle neutral "I think the company you choose for yourself is a good indicator of your character."
                      mc "It's not really her fault, though."
                      isabelle concerned "What do you mean?"
                      mc "She's pretty and an athlete, and quite naive."
                      mc "She thinks the best of everyone."
                      isabelle sad "I see..."
                      mc "But I guess if you don't stop the crappy behavior of your friends, are you really any better?"
                      $isabelle.lust+=2
                      isabelle annoyed "No, you're not."
                      isabelle annoyed "To quote the great Edmund Burke, {i}\"All that is necessary for the triumph of evil is that good men do nothing.\"{/}"
                      isabelle annoyed "I don't think enablers aren't much better than the culprits."
                      "Damn, she's really fired up about this."
                      "There's something really hot about a passionate woman..."
                      mc "Right. I agree."
                      isabelle blush "Sorry, I get really into topics like this... I'll let you keep practicing now.{space=-35}"
                      show isabelle blush at disappear_to_left
                      "[isabelle] is just something else. Hearing her talk makes me excited."
                      "And she's so damn cute."
                  $quest.lindsey_piano.fail()
                  $quest.lindsey_piano["failed"] = True
                  $quest.lindsey_piano.failed = False
                  $quest.lindsey_piano.started = False
                  $quest.lindsey_piano.phase = 0
                  return
            "Play romantic background music\nwhile they talk":
              show lindsey blush at move_to(.25)
              show isabelle excited at move_to(.75)
              stop music
              window hide
              pause 0.25
              play music "romantic" volume 0.75
              pause 0.25
              show isabelle neutral with Dissolve(.5)
              window auto
              isabelle neutral "So, how long have you lived here?"
              lindsey thinking "Since I was really little..."
              lindsey thinking "I moved here from the city. What about you?"
              isabelle concerned "..."
              lindsey laughing "Oh, right! I forgot!"
              lindsey laughing "My mind is all over the place, sorry..."
              isabelle laughing "Don't worry!"
              isabelle confident "Listen... how about..."
              lindsey smile "What?"
              isabelle eyeroll "[mc], are you doing that on purpose?"
              window hide
              play music "school_theme" fadein 0.5
              pause 0.5
              window auto
              mc "Whoops."
              lindsey skeptical "What did you do?"
              isabelle eyeroll "He was trying to be funny."
              mc "I was merely providing thematic music."
              isabelle skeptical "Is that right?"
              lindsey laughing "I think that's nice of him!"
              lindsey laughing "I was happy when I heard a tuned piano for once!"
              mc "I was happy when you came over to listen."
              $lindsey.love+=1
              lindsey flirty "Aw! I really enjoyed it."
              isabelle concerned "..."
              isabelle concerned "I have to go..."
              show lindsey flirty at move_to(.5,1.0)
              show isabelle concerned at disappear_to_right
              pause 0.0
              lindsey skeptical "What's up with her?"
              mc "I don't know..."
              mc "Anyway, what's your favorite type of music?"
              lindsey smile "I like medieval!"
              mc "I used to love medieval music."
    "\"I never took you for someone who appreciates a good tune, either!\"":
      show lindsey smile at move_to(.5)
      mc "I never took you for someone who appreciates a good tune, either!"
      lindsey skeptical "What's that supposed to mean?"
      show lindsey skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "?not quest.lindsey_piano['failed']@|\"I've always found you a bit one-dimensional as a person.\"":
          show lindsey skeptical at move_to(.5)
          mc "I've always found you a bit one-dimensional as a person."
          $lindsey.love-=2
          lindsey eyeroll "Nice. What a nice thing to say."
          mc "You only talk about running! What am I supposed to think?"
          lindsey annoyed "Remind me to never compliment you again."
          show lindsey annoyed at disappear_to_left
          $lindsey["romance_disabled"] = True
          $game.notify_modal(None,"Love or Lust","Romance with [lindsey]:\nDisabled.",wait=5.0)
          "Typical, really. The fragile ego of a normie..."
          "People need to learn how to take criticism."
          $quest.lindsey_piano.fail()
          return
        "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"I guess I figured you were more into modern electro and dance music?\"" if not quest.lindsey_piano["failed"]:
          label quest_lindsey_piano_play_gym:
          show lindsey skeptical at move_to(.5)
          mc "I guess I figured you were more into modern electro and dance music?{space=-55}"
          mc "You know, the stuff they play at the gym."
          lindsey smile "I see! I mostly listen to medieval music."
          mc "That can be inspiring sometimes."
          mc "I used to listen to medieval music a lot when larping. Nothing like swinging a sword to an epic fantasy tune."
        "\"I guess I figured you were more into modern electro and dance music?\"" if quest.lindsey_piano["failed"]:
          jump quest_lindsey_piano_play_gym
    "?mc.strength>=6@[mc.strength]/6|{image=stats str}|\"Like a knight riding into battle\nto save a princess?\"":
      show lindsey smile at move_to(.5)
      mc "Like a knight riding into battle to save a princess?"
      lindsey laughing "Yes! That's it exactly."
      lindsey laughing "I think it was the speed and the danger..."
      mc "And they say chivalry is dead."
      $lindsey.lust+=1
      lindsey flirty "I don't think it is!"
      mc "I used to love medieval music."
    "\"Like the last stretch of a race, the contestants going head-to-head, blazing toward the finish line?\"":
      show lindsey smile at move_to(.5)
      mc "Like the last stretch of a race, the contestants going head-to-head, blazing toward the finish line?"
      lindsey flirty "Yeah!"
      mc "You probably know all about winning that race too."
      lindsey blush "..."
      $lindsey.love+=1
      lindsey blush "That's sweet."
      mc "Just a casual observation."
      mc "Crap, my plan was to take your mind off sprinting..."
      lindsey laughing "Well, good luck with that!"
      mc "One can always try. What's your favorite kind of music?"
      lindsey smile "I really like medieval music."
      mc "I used to love medieval music."
  mc "I have a first edition King's Bard album somewhere..."
  lindsey blush "Really? The one with the dragon slayer cover?"
  "Wait, [lindsey] knows about King's Bard? I thought she would laugh and call me a nerd..."
  mc "Yeah, I went to a concert once and had it signed."
  lindsey blush "Whoa! That's amazing!"
  lindsey blush "I'm so jealous!"
  show lindsey blush at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You can have it, if you want.\"":
      show lindsey blush at move_to(.5)
      mc "You can have it, if you want."
      lindsey afraid "Are you serious?!"
      mc "I don't really listen to them that much anymore, and you're clearly a big fan."
      lindsey afraid "But it's a signed original!"
      mc "It's just gathering dust in my basement. Really, it's no big deal."
      lindsey blush "I've always loved that cover..."
      mc "How about you stop by some day and pick it up?"
      lindsey blush "Are you sure?"
      mc "Yeah! I just need to find it, but it shouldn't be too hard..."
      lindsey laughing "Cool! I'm so excited!"
      lindsey laughing "Just hit me up when you do, and I'll stop by!"
      mc "O-okay."
      lindsey smile "Unless?"
      mc "No, no, that's great!"
      "I can't believe [lindsey] wants to meet up outside of school! [lindsey]!"
      "This must be a dream..."
      lindsey flirty "Great! See you later, [mc]!"
      show lindsey flirty at disappear_to_left
      "[jo] probably knows where that album is... I need to find it before [lindsey] forgets about my existence."
    "?lindsey.lust>=10@[lindsey.lust]/10|{image=lindsey contact_icon}|{image=stats lust_3}|\"If you want, you could{space=-15}\ncome over to my house and we could listen to it together.\"":
      $quest.lindsey_piano["listen"] = "bedroom"
      show lindsey blush at move_to(.5)
      mc "If you want, you could come over to my house and we could listen to it together."
      lindsey flirty "I'd like that!"
      lindsey laughing "But I really have to get back to practice now..."
      lindsey laughing "How about you call me and we'll set something up?"
      "Damn! That felt like a shot in the dark... I can't believe [lindsey] wants to come over!"
      mc "Uh, yeah! T-that would be awesome!"
      lindsey smile "All right, call me! Bye!"
      show lindsey smile at disappear_to_left
      "That's insane..."
      "[lindsey]'s actually coming over! [lindsey]!"
      "This truly is my redemption arc!"
      "Shit, okay. I need to find that album now. [jo] probably knows where it is..."
    "\"I could bring it to school some day and we could listen to it in the music classroom.\"{space=-30}":
      $quest.lindsey_piano["listen"] = "music_classroom"
      show lindsey blush at move_to(.5)
      mc "I could bring it to school some day and we could listen to it in the music classroom."
      lindsey laughing "Really? That would be sweet!"
      mc "Sure, I just need to find it..."
      lindsey smile "Cool, see you around!"
      show lindsey smile at disappear_to_left
      "[lindsey] probably thinks I'm lying, but she'll change her mind when she sees the album cover."
      "It's probably the basement or something... [jo] might know."
  if quest.lindsey_piano["failed"] or not quest.lindsey_piano.started:
    $quest.lindsey_piano.start("piano")
  $quest.lindsey_piano.advance("jo_talk")
  return

label quest_lindsey_piano_jo_talk:
  show jo smile with Dissolve(.5)
  jo smile "Ah, my big boy! How is senior year coming along?"
  mc "Okay, I guess..."
  mc "Listen, [jo], do you remember my King's Bard album? Any idea where it might be?"
  jo skeptical "King's Bard? Hmm..."
  mc "I went to a concert and had it signed, remember?"
  jo confident "Oh, you did! And you got wet from the rain, despite me telling you to bring an umbrella."
  mc "I didn't want to bring [flora]'s polka dot one to a..."
  mc "...ugh, it doesn't matter! Do you know where it is?"
  jo neutral "If it's not in your room, it's probably in the basement with your old things.{space=-110}"
  mc "It has a skull and sword-guitar on the front cover, if that helps."
  jo smile "I can go look for it later. What do you need it for?"
  show jo smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"It's a music album, what do you usually do with them?\"":
      show jo smile at move_to(.5)
      $mc.intellect+=1
      mc "It's a music album, what do you usually do with them?"
      $jo.love-=1
      jo annoyed "No need to be snarky, young man."
      mc "Can you find it for me or not?"
      if game.hour in (18,19):
        jo worried "Yes, dear. I'll go look for it first thing in the morning."
      else:
        jo worried "Yes, dear. I have to go to a meeting now, but remind me when I get home.{space=-110}"
      mc "Thanks, [jo]."
    "\"It's for a girl...\"":
      show jo smile at move_to(.5)
      mc "It's for a girl..."
      $jo.love+=1
      jo sarcastic "Oh!"
      "She's putting on an excited face, but a combination of surprise and relief accompanies her voice."
      "Makes sense, I guess..."
      jo flirty "Does she have a name?"
      show jo flirty at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I'd rather not say.\"":
          show jo flirty at move_to(.5)
          mc "I'd rather not say."
          jo laughing "That's okay! I won't pry!"
          mc "Do you think you can find it? The album, I mean."
          if game.hour in (18,19):
            jo excited "I'll go look for it first thing in the morning!"
          else:
            jo excited "I'll go look for it as soon as I get home from the meeting!"
          mc "Okay, thanks, [jo]!"
          jo excited "Of course, honey."
        "\"It's [lindsey] from the track team.\"":
          show jo flirty at move_to(.5)
          mc "It's [lindsey] from the track team."
          jo afraid "Oh!"
          jo blush "She's a lovely young lady."
          "I wish [jo] would stop looking so surprised..."
          mc "She's all right."
          jo blush "I know I don't have to say this, since you're eighteen now... but remember to be responsible and use protection, okay?"
          mc "[jo]!"
          jo laughing "Sorry, I know, I know!"
          mc "Ugh... can you find the album for me?"
          if game.hour in (18,19):
            jo excited "Of course, dear. I'll go look for it first thing in the morning!"
          else:
            jo excited "Of course, dear. I'll go look for it as soon as I get home from the meeting.{space=-110}"
  hide jo with Dissolve(.5)
  if game.hour in (18,19):
    $quest.lindsey_piano.advance("album_day")
  else:
    $quest.lindsey_piano.advance("album_night")
  return

label quest_lindsey_piano_album:
  show jo sarcastic with Dissolve(.5)
  jo sarcastic "There you are!"
  mc "Hey, [jo]! How's your day going?"
  jo eyeroll "It's a lot with the school right now..."
  jo eyeroll "The new curriculum is hard for some teachers to accept."
  show jo eyeroll at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'm sure you have it under control!\"":
      show jo eyeroll at move_to(.5)
      mc "I'm sure you have it under control!"
      jo flirty "Thank you. I do try my best to keep things running smoothly."
      jo flirty "It is a big change for everyone."
      mc "I hope you're not too stressed?"
      jo excited "It's okay!"
      jo excited "Maybe you could stop by at lunch some time and give me a hug?"
      mc "I'll think about it."
      $jo.lust+=1
      jo laughing "Okay, you do that!"
    "\"Anything I can do to help?\"":
      show jo eyeroll at move_to(.5)
      mc "Anything I can do to help?"
      jo laughing "Aw, sweetie! That's so nice of you to offer, but it's all administration business."
      $jo.love+=1
      jo excited "Just focus on your grade and I'll be more than happy."
  jo smile "Anyway, I found that album you were looking for. It was under a box of old towels that I'd been looking for."
  "Oh, crap."
  show jo smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Did you, err... wash the towels?\"":
      show jo smile at move_to(.5)
      mc "Did you, err... wash the towels?"
      jo neutral "They smelled a bit odd, so I threw them out. Why?"
      mc "No reason..."
      "I had totally forgotten about those. Not a proud moment."
      "..."
      "Fine. {i}Moments{/i}."
      jo confident "Here's the album, anyway."
    "\"Thanks for digging through the basement.\"":
      show jo smile at move_to(.5)
      mc "Thanks for digging through the basement."
      jo confident "No worries, sweetheart! I know you don't like the dark."
      mc "I was six when I said that!"
      jo neutral "Feels like it was just yesterday. How time flies..."
      jo smile "Here's the album, anyway."
  $mc.add_item("king_bard_album")
  mc "Thanks, [jo]!"
  hide jo with dissolve2
  if quest.lindsey_piano["listen"] == "music_classroom":
    "Perfect. Hopefully, [lindsey] hasn't forgotten all about me by the time I show her the album..."
    "Playing it in the music classroom is actually a great idea. The sound system there is second to none."
    $quest.lindsey_piano.advance("lindsey_talk")
  else:
    "Okay, now the only thing that remains is working up the courage to call [lindsey]..."
    "It feels weird meeting a girl after school. Forbidden, somehow."
    "Even if it's just her stopping by, it still feels awkward."
    "Maybe it would be a good idea to make a list of things that could go wrong, and plan on how to avoid them?"
    "Maybe she won't come over, and just said it to be nice? Ugh, maybe she's one of those girls."
    "Maybe she doesn't even like King's Bard?"
    "Damn it! Why are my palms so sweaty all of a sudden?"
    $quest.lindsey_piano.advance("phone_call")
  return

label quest_lindsey_piano_lindsey_talk:
  "Approaching some girls always makes me nervous..."
  "Maybe I misread the situation and she doesn't want to listen to it?"
  "..."
  "Fuck it. Just got to do it."
  "Worst case, she'll make up some excuse and leave... Nothing I haven't seen before."
  show lindsey smile with Dissolve(.5)
  mc "H-hey, [lindsey]."
  lindsey smile "Hi there! How's it going?"
  mc "I found that album we talked about. Want to go and listen to it?"
  lindsey blush "You did? That's amazing!"
  mc "Yeah, I had to dig deep for it."
  lindsey thinking "I have to go to practice now, though..."
  "Crap. I knew it."
  mc "That's too bad. Maybe some other time."
  lindsey laughing "Don't be so dramatic! Let's meet in the music classroom in an hour?{space=-10}"
  mc "Sorry, I misunderstood... That sounds good!"
  lindsey smile "No worries!"
  lindsey smile "How about a sneak peek of the cover now?"
  mc "You'll just have to wait."
  lindsey skeptical "Fine. If you want me to be distracted at practice..."
  mc "Practice is probably too easy for you, anyway."
  lindsey laughing "You big flirt!"
  lindsey flirty "See you in an hour!"
  if game.location == "school_gym":
    hide lindsey with dissolve2
  else:
    show lindsey flirty at disappear_to_right
  "Alright, that went a lot better than expected!"
  "Now for the waiting game..."
  $quest.lindsey_piano.advance("listen_music_class")
  return

label quest_lindsey_piano_listen_music_class_time_is_up:
  "Okay, time to meet up with [lindsey]!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $mc["focus"] = "lindsey_piano"
  $quest.lindsey_piano["lindsey_arrived"] = True
  call goto_school_music_class
  play music "school_theme" if_changed
  $lindsey.location = None
  $lindsey.equip("lindsey_cropped_hoodie")
  $lindsey.equip("lindsey_sweatpants")
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  play sound "open_door"
  pause 0.75
  show lindsey laughing at appear_from_right
  window auto
  lindsey laughing "Hey! Did you have to wait long?"
  "Wow, it's strange to see her in other clothes..."
  "I'm so used to her blue sweater and white skirt."
  "But she looks cute in any outfit."
  mc "I just got here, actually..."
  lindsey smile "Cool! Okay, I'm ready!"
  hide lindsey with Dissolve(.5)
  return

label quest_lindsey_piano_listen_music_class:
  "Still a little while before [lindsey] gets here. Better make sure everything is perfect."
  "It's not a date or anything, but..."
  "Ugh, why does this all make me so nervous?! We're just going to listen to some music!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $mc["focus"] = "lindsey_piano"
  $quest.lindsey_piano["lindsey_arrived"] = True
  $game.advance()
  $lindsey.location = None
  $lindsey.equip("lindsey_cropped_hoodie")
  $lindsey.equip("lindsey_sweatpants")
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play sound "open_door"
  pause 0.75
  show lindsey laughing at appear_from_right
  window auto
  "Okay, here she comes. She looks excited."
  "Wow, it's strange to see her in other clothes..."
  "I'm so used to her blue sweater and white skirt."
  "But she looks cute in any outfit."
  "Crap, I'm totally going to mess this up..."
  lindsey laughing "Hey! Did you have to wait long?"
  mc "No, not really..."
  lindsey smile "Cool! Okay, I'm ready!"
  hide lindsey with Dissolve(.5)
  return

label quest_lindsey_piano_listen_music_class_door:
  "[lindsey] is here and waiting. Bailing would kill any chances with\nher, however small they may be."
  return

label quest_lindsey_piano_listen_music_class_stereo_system_interact:
  "This thing is fired up and ready to burn."
  return

label quest_lindsey_piano_listen_music_class_stereo_system_use_item(item):
  if item == "king_bard_album":
    mc "Okay, here we go..."
    if preferences.get_volume("music"):
      $old_volume = preferences.get_volume("music")
      $preferences.set_volume("music", old_volume+0.25)
    play music "epic_medieval" fadein 0.5
    $quest.lindsey_piano["stereo_system"] = True
  else:
    "Sticking my [item.title_lower] into the stereo would probably create an entirely new genre of music."
    "People would start referring to me as Skridiot. Not a good look."
    $quest.lindsey_piano.failed_item("stereo_system",item)
  return

label quest_lindsey_piano_listen_music_class_lindsey_ready:
  show lindsey smile with Dissolve(.5)
  lindsey smile "I'm ready when you are!"
  hide lindsey with Dissolve(.5)
  return

label quest_lindsey_piano_listen_music_class_lindsey:
  show lindsey blush with Dissolve(.5)
  lindsey blush "I love this..."
  mc "Have a look at the cover, too!"
  $mc.remove_item("king_bard_album")
  lindsey excited_album "God! That's so cool!"
  lindsey excited_album "The Deathless Bard, forevermore plucking at the invisible strings of the Excalifender!"
  lindsey excited_album "And look at Johnny Rampage's signature! It's as elegant as his vocals!{space=-50}"
  lindsey confident_album "..."
  $lindsey.lust+=1
  lindsey confident_album "This is so great..."
  window hide
  show black onlayer screens zorder 100
  show lindsey dancing_on_her_own5
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  "[lindsey] is fearless. Dancing on her own like nobody's watching."
  show lindsey dancing_on_her_own7 with dissolve2
  "I wish I had half of her confidence..."
  show lindsey dancing_on_her_own8 with dissolve2
  "The way she rocks her hips, her waist, her perfect ass."
  show lindsey dancing_on_her_own6 with dissolve2
  "Her moves are so soft, so gentle and delicate!"
  show lindsey dancing_on_her_own4 with dissolve2
  "She truly is one of a kind. A real life princess."
  show lindsey dancing_on_her_own2 with dissolve2
  "So precious..."
  show lindsey dancing_on_her_own1 with dissolve2
  "I just want to keep her safe. Protect her from the world."
  show lindsey dancing_on_her_own3 with dissolve2
  "Let her live in a world where nothing bad will ever happen to her."
  show lindsey dancing_on_her_own5 with dissolve2
  "..."
  show lindsey dancing_on_her_own7 with dissolve2
  "She probably wouldn't like it if I took a picture of her, but this is a once in a lifetime moment..."
  menu(side="right"):
    extend ""
    "Take a picture":
      $mc.lust+=1
      show lindsey dancing_on_her_own8 with dissolve2
      "What the hell... I'll probably regret it later if I don't."
      window hide
      show lindsey dancing_on_her_own6 with dissolve2
      show expression "lindsey avatar events fall crashed lindseycrashed_camera" as camera
      with Dissolve(.5)
      pause 0.5
      show white onlayer screens zorder 100 with Dissolve(.15)
      play sound "camera_snap"
      hide white onlayer screens with Dissolve(.15)
      pause 0.5
      show lindsey dancing_on_her_own4
      hide camera
      with Dissolve(.5)
      $quest.lindsey_piano["lindsey_dancing_photo"] = True
      window auto
      "Phew, [lindsey] didn't seem to notice! She's too caught up in the music.{space=-55}"
      window hide
      show lindsey dancing_on_her_own2 with Dissolve(.5)
    "Let her dance in peace":
      $mc.love+=1
      show lindsey dancing_on_her_own8 with dissolve2
      "[lindsey] looks perfect like this... No need to risk it."
      show lindsey dancing_on_her_own6 with dissolve2
      "Besides, this will be the screensaver of my mind for a while."
      window hide
      show lindsey dancing_on_her_own4 with Dissolve(.5)
  $unlock_replay("lindsey_solo_dance")
  stop music fadeout 3.0
  show black onlayer screens zorder 100
  show lindsey laughing
  with Dissolve(3.0)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  if preferences.get_volume("music"):
    $preferences.set_volume("music", old_volume)
  play music "school_theme" fadein 0.5
  $mc.add_item("king_bard_album")
  window auto
  lindsey laughing "This was fantastic! Thank you so much, [mc]!"
  mc "I enjoyed it too!"
  lindsey flirty "They're releasing a new album soon. Did you know that?"
  mc "Well, they've said that for the last fourteen... err, four years..."
  lindsey laughing "True! But I think it's for real this time!"
  mc "I guess we'll see."
  lindsey smile "Anyway, I have to go change now. Practice never ends."
  lindsey smile "Thanks again!"
  mc "You're very welcome!"
  window hide
  show lindsey smile at disappear_to_right
  pause 0.5
  window auto
  "[lindsey] is pretty nice..."
  "Getting to know her better should definitely be a priority this time."
  "She seems to be okay with my imperfections and mistakes. At least, she's quick to forgive them."
  "Hopefully, she enjoyed spending some time with me and didn't just do it for the album..."
  "Time will tell."
  $quest.lindsey_piano.finish()
  if game.hour == 19:
    call event_player_force_go_home_at_night
    play music "home_theme"
  return

label quest_lindsey_piano_phone_call_same_room:
  "Calling [lindsey] when she's right there... that's just weird."
  return

label quest_lindsey_piano_phone_call:
  "Here goes nothing..."
  window hide
  $set_dialog_mode("phone_call","lindsey")
  pause 0.5
  "{i}Calling...{/}"
  "{i}...{/}"
  "{i}Calling...{/}"
  "{i}...{/}"
  $set_dialog_mode("")
  window auto
  "Somehow, the wait is always the worst. Will she even pick up?"
  "In a way, it would be a relief if she didn't. Less awkward."
  "Ugh, she probably gave me a fake number or some—"
  window hide
  $set_dialog_mode("phone_call","lindsey")
  pause 0.5
  lindsey "Hello?"
  mc "H-hey! It's me, [mc]..."
  lindsey "Oh, hi there! How's it going?"
  mc "Okay, I guess. I found\nthe King's Bard album."
  lindsey "Whoa! That's awesome!\nI'm super excited!"
  lindsey "So, do you still want me\nto come over?"
  menu(side="middle"):
    extend ""
    "\"Yes! Come whenever you want!\"":
      mc "Yes! Come whenever you want!"
      $set_dialog_mode("")
      window auto
      "Crap, that sounded way too desperate."
      window hide
      $set_dialog_mode("phone_call","lindsey")
      pause 0.5
      lindsey "Oh... okay. Hmm..."
      lindsey "How about tonight?"
      mc "Tonight is fantastic!"
      lindsey "I like your enthusiasm!"
      lindsey "See you tonight, then!"
      $set_dialog_mode("")
      window auto
      "I'm not sure how that went..."
      "According to those pick-up artist videos on YouTube, that must've been a failure."
      "But [lindsey] didn't seem grossed out... and she's actually coming over! I'd call that victory."
      if quest.lindsey_piano["listen"] == "bedroom":
        if home_bedroom["clean"]:
          "Now for the waiting game..."
        else:
          "Giving my room a once-over would probably be smart. [jo] keeps the vacuum cleaner in the hallway closet."
        $quest.lindsey_piano.advance("cleaning")
      else:
        "Now for the waiting game..."
        $quest.lindsey_piano.advance("wait")
    "\"If you want to... I know you have\na lot on your plate.\"":
      mc "If you want to... I know you\nhave a lot on your plate."
      lindsey "Thanks, but I really want this!"
      mc "Okay, just making sure.\nI know you're responsible."
      mc "How does tonight sound?"
      lindsey "That works for me!"
      mc "Great! I'll see you then."
      $lindsey.love+=1
      lindsey "Cool! Bye, [mc]!"
      $set_dialog_mode("")
      window auto
      "That felt so good! I'm not sure where that surge of confidence came from, but damn... nailed it!"
      if quest.lindsey_piano["listen"] == "bedroom":
        if home_bedroom["clean"]:
          "Now for the waiting game..."
        else:
          "Next step is cleaning my room. [jo] keeps the vacuum cleaner in the hallway closet."
        $quest.lindsey_piano.advance("cleaning")
      else:
        "Now for the waiting game..."
        $quest.lindsey_piano.advance("wait")
  return

label quest_lindsey_piano_wait_bed:
  "There's no time to take a nap. [lindsey] will be here any moment."
  return

label quest_lindsey_piano_wait_arrived:
  $game.location["night"] = False
  play sound "doorbell"
  pause 0.5
  "Oh, shit. [lindsey] is here!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $quest.lindsey_piano.advance("pick_up")
  call goto_home_kitchen
  $home_kitchen["night"] = False
  $quest.lindsey_piano["lindsey_arrived"] = True
  $mc["focus"] = "lindsey_piano"
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  return

label quest_lindsey_piano_pick_up_stairs:
  "I'm a lot of things, but a rude host isn't one of them."
  return

label quest_lindsey_piano_pick_up_door:
  "The nerves are starting to kick in..."
  "Maybe inviting [lindsey] over was a terrible idea. Shit."
  return

label quest_lindsey_piano_pick_up_lindsey:
  show lindsey laughing with Dissolve(.5)
  lindsey laughing "This is the first time I've been here, I think!"
  mc "Yeah, I would definitely have remembered it if you'd visited before."
  lindsey flirty "Aw!"
  mc "Anyway, here you go."
  $mc.remove_item("king_bard_album")
  lindsey confident_album "Oh, my god! It's amazing! The details of the Excalifender..."
  lindsey confident_album "The Deathless Bard!"
  mc "I'm glad you like it!"
  lindsey excited_album "I love it! The signatures... oh, whoa... Wilhelm Tattle's handwriting looks just like his guitar solos!"
  $lindsey.lust+=2
  $lindsey.love+=1
  lindsey excited_album "This is the coolest thing ever! Thank you so much!"
  show lindsey excited_album at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You're very welcome! The dragon slayer used to be my role model.\"":
      show lindsey excited_album at move_to(.5)
      mc "You're very welcome! The dragon slayer used to be my role model."
      $lindsey.lust+=1
      lindsey flirty "He's so badass and chivalrous at the same time!"
      show flora laughing at appear_from_left(.25)
      show lindsey flirty at move_to(.75,1.0)
#     flora laughing "You should've seen my brother running around the house in his tin foil armor..." ## Incest patch ##
      flora laughing "You should've seen [mc] running around the house in his tin foil armor..."
      mc "[flora]!"
      lindsey laughing "Hey, [flora]! How's it going?"
      flora blush "Oh, you know. Same old."
      show flora blush at move_to("left")
      show lindsey laughing at move_to("right")
      menu(side="middle"):
        extend ""
        "\"Ignore her, she's just trying to be funny.\"":
          show flora blush at move_to(.25)
          show lindsey laughing at move_to(.75)
          mc "Ignore her, she's just trying to be funny."
          flora annoyed "How very dare you!"
          flora sarcastic "But also, true... [lindsey], please rate my humor from one to five toasters.{space=-90}"
          mc "[flora]!"
          lindsey laughing "Five out of five toasters!"
          flora laughing "That's a great score, thank you!"
          mc "Ugh..."
          flora thinking "Okay, fine. I just need some cereal and I'm out."
          flora flirty "I'm allowed to get cereal in my own home, right?"
          window hide
          show flora flirty at disappear_to_left
          show lindsey laughing at move_to(.5,1.0)
          pause 0.5
          window auto
          "Finally! I swear, [flora] is trying to ruin my life..."
        "\"[flora], I'm in the middle of something here...\"{space=-35}":
          show flora blush at move_to(.25)
          show lindsey laughing at move_to(.75)
          mc "[flora], I'm in the middle of something here..."
          $flora.love-=1
          flora eyeroll "And I'm craving cereal. How will we {i}ever{/} solve this?"
          mc "Preferably, by you leaving."
          $lindsey.love-=1
          lindsey skeptical "Be nice to her, [mc]."
          mc "Ugh, you sound like [jo]..."
          flora sarcastic "Maybe you should start listening to the women in your life?"
          mc "Maybe if certain women in my life weren't so annoying, they'd get more ear-time."
          flora annoyed "[lindsey], do you see what I have to put up with here?"
          lindsey laughing "You're both adorable."
          flora flirty "Do you really think so? I'm single if you're interested."
          mc "[flora], get out!"
          flora laughing "Fine, I'm leaving!"
          window hide
          show flora laughing at disappear_to_left
          show lindsey laughing at move_to(.5,1.0)
          pause 0.5
          show lindsey flirty with Dissolve(.5)
          window auto
      lindsey flirty "[flora] is a blast!"
      mc "That's not quite the word I had in mind."
      lindsey confident "Anyway, I have to get going... I have practice quite early tomorrow."
      lindsey excited_album "Thanks again, so much! I can't wait to listen to this."
      mc "You're welcome again, so much!"
      lindsey excited_album "See you at school, [mc]!"
      show lindsey excited_album at disappear_to_left
      "Making [lindsey] happy feels so good!"
      "All these years, I worried about making myself happy, but there's something special about putting a smile on a girl's face..."
      "It's addicting."
    "?lindsey.love>=10@[lindsey.love]/10|{image=lindsey contact_icon}|{image=stats love_3}|\"I've never seen you this excited before.\nIt's cute.\"":
      show lindsey excited_album at move_to(.5)
      mc "I've never seen you this excited before. It's cute."
      $lindsey.love+=1
      lindsey laughing "Thanks! I can't believe you had a signed original of my favorite band! That's so crazy!"
      mc "It almost feels like fate."
      lindsey smile "It could very well be!"
      mc "We should hang out more and see what else fate has in store..."
      lindsey blush "Do you believe in fate, too?"
      mc "I don't know, maybe..."
      lindsey thinking "If I hadn't walked by when you played the piano..."
      lindsey thinking "...and if I hadn't loved the song, you wouldn't have had the chance to tell me about the album... and we wouldn't be here now."
      lindsey blush "Looks an awful lot like fate to me!"
      mc "It could also be a lot of coincidences."
      lindsey laughing "I think it's fate."
      mc "Maybe..."
      lindsey flirty "Maybe this will convince you?"
      window hide
      show black onlayer screens zorder 100
      show lindsey kiss_kitchen
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      mc "!!!"
      "[lindsey] gets up on her tippy-toes and leans in."
      "Our lips meet for a moment, and time seems to stop."
      "It sounds cliché, but it's what happens when a moment gets seared into memory for the rest of your life."
      "So soft, so sweet, on my upper lip..."
      "Misaligned, but still perfect. Still absolutely electric."
      "Then she moves down, taking my bottom lip between hers."
      "She blushes, smiles through the kiss, then gets it right."
      "Three kisses in one!"
      "It's one of those moments you'll never forget..."
      $unlock_replay("lindsey_kiss")
      $quest.lindsey_piano["kissed"] = True
      window hide
      show black onlayer screens zorder 100
      show lindsey flirty
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      lindsey flirty "How about now?"
      mc "I think I might need some more convincing..."
      lindsey laughing "Don't push your luck, mister!"
      mc "Oh, well."
      lindsey skeptical "Hey!"
      mc "I'm joking!"
      lindsey laughing "I knew it!"
      lindsey laughing "I like that you're so cool and relaxed about it."
      "If only she could hear my heart..."
      lindsey confident_album "Thanks so much for the album! It is by far the most badass gift I've ever received."
      lindsey confident_album "I'm going to rock out all night!"
      mc "I'm really happy you liked it. Enjoy it!"
      lindsey excited_album "I will!"
      lindsey excited_album "But it's getting late, so I should probably get going."
      lindsey excited_album "See you at school, [mc]!"
      show lindsey excited_album at disappear_to_left
      "And there it is... I kissed [lindsey]. Most jocks can't even brag about that.{space=-75}"
      "So, all of this happened just because I took the time to learn the freaking piano..."
      "It's a strange world we live in."
  $home_kitchen["night"] = True
  $quest.lindsey_piano.finish()
  return

label quest_lindsey_piano_cleaning_vacuum_cleaner:
  "First impressions are a thing... Not sure if this will help, but it's a start.{space=-50}"
  $quest.lindsey_piano["vacuum_cleaner_taken"] = True
  $mc.add_item("vacuum_cleaner")
  return

label quest_lindsey_piano_cleaning:
  "This place is a dump. [lindsey] is going to freak out as soon as she enters."
  "There's still time to give it a once-over..."
  if mc.owned_item("vacuum_cleaner"):
    "Best get to work."
    window hide
    show black onlayer screens zorder 100 with Dissolve(.5)
    $home_bedroom["clean"] = True
    $quest.lindsey_piano.advance("listen_bedroom")
    $mc["focus"] = "lindsey_piano"
    while game.hour != 20:
      $game.advance()
    $home_bedroom["night"] = False
    pause 3.0
    hide black onlayer screens with Dissolve(.5)
    window auto
    "Okay, much better! Almost looks presentable now."
    "..."
    "That took a lot longer than expected. [lindsey] will be here any moment."
  else:
    "[jo] keeps the vacuum cleaner in the hallway closet outside."
  return

label quest_lindsey_piano_cleaning_arrived:
  $game.location["night"] = False
  play sound "doorbell"
  pause 0.5
  if home_bedroom["clean"]:
    "Okay, that must be [lindsey]..."
    "Deep breaths. You got this, [mc]."
  else:
    "Oh, fuck. [lindsey] is already here!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $quest.lindsey_piano.advance("listen_bedroom")
  call goto_home_bedroom
  $home_bedroom["night"] = False
  $quest.lindsey_piano["lindsey_arrived"] = True
  $mc["focus"] = "lindsey_piano"
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  return

label quest_lindsey_piano_listen_bedroom_bed:
  "Thread count: 400."
  "Girl count: 1!!!"
  return

label quest_lindsey_piano_listen_bedroom_computer:
  "What's happening on 4chan today?"
  "Hmm... dank memes that people still think are fresh..."
  "If only I'd kept my meme folder, I'd be kingfag now."
  return

label quest_lindsey_piano_listen_bedroom_closet_first_time:
  "Meeting [lindsey] after school is a big deal. I need to pick out something to wear..."
  menu(side="middle"):
    extend ""
    "Wear the King's Bard t-shirt and\nthe old larp chainmail":
      "[lindsey] is going to fall hard, and I'll be ready to catch her."
      $lindsey.lust+=1
    "Wear something sporty":
      "[lindsey] will appreciate the workout-look."
      $lindsey.love+=1
  $quest.lindsey_piano["outfit_chosen"] = True
  return

label quest_lindsey_piano_listen_bedroom_closet_clean:
  "Okay, everything's in order. I couldn't be more prepared."
  return

label quest_lindsey_piano_listen_bedroom_closet_dirty:
  "With a room this messy, [lindsey] probably won't care about my outfit.{space=-30}"
  return

label quest_lindsey_piano_listen_bedroom_door:
  play sound "doorbell"
  pause 0.5
  "Okay, that's [lindsey] by the door..."
  "...oh, shit! She's coming up the stairs!"
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $quest.lindsey_piano["lindsey_arrived"] = True
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  return

label quest_lindsey_piano_listen_bedroom_door_leave:
  "Leaving now would result in my death or worse."
  return

label quest_lindsey_piano_listen_bedroom_lindsey:
  show lindsey smile with Dissolve(.5)
# lindsey smile "Hey, [mc]! Your sister let me in. She's quite lovely!" ## Incest patch ##
  lindsey smile "Hey, [mc]! [flora] let me in. She's quite lovely!"
  "Okay, no need to freak out. It's only one of the cutest girls in school... no big deal..."
  mc "You made it!"
  lindsey laughing "You look surprised."
  "You have no idea..."
  if home_bedroom["clean"]:
    lindsey flirty "I like your room! It's very neat and tidy."
    mc "Thanks! I like your... uh, top?"
    lindsey laughing "Aw, thanks! This is what I usually wear outside of school and practice.{space=-45}"
    mc "Well, you'd look good in anything, really."
    $lindsey.love+=1
    $lindsey.lust+=1
    lindsey smile "Thank you!"
  else:
    $lindsey.love-=1
    $lindsey.lust-=1
    lindsey skeptical "Interesting choice of decoration."
    mc "Sorry, I didn't really have time to clean up..."
    lindsey skeptical "What's with all these posters?"
    mc "Just, err... I want to support the artists."
    lindsey flirty "I bet you do."
    "Crap, I really should've cleaned. This is embarrassing..."
    "Topic change, quick!"
  mc "Are you ready to see the album?"
  lindsey confident "Yes! I've been excited since you told me about it!"
  mc "Okay, have a look."
  $mc.remove_item("king_bard_album")
  lindsey excited_album "Whoa! It's beautiful!"
  lindsey excited_album "The Deathless Bard is so badass!"
  lindsey excited_album "Did you know this is the only cover with a close up of the Excalifender?{space=-70}"
  lindsey excited_album "Oh, my god, those details! I love it!"
  lindsey excited_album "And Morgan Leeman's signature is just like his drum solos — flashy and intense!"
  mc "Yeah, he's just like that in person, too. Wore his shades over his helmet while playing."
  lindsey confident_album "Damn, that's so cool! I can't believe you met them in person!"
  lindsey confident_album "Let's play it!"
  mc "Okay, just put it in my computer."
  lindsey excited_album "Okay!"
  show lindsey excited_album at disappear_to_right
  "[lindsey] is so cute when she's this excited..."
  "I still can't believe she's here. In my room."
  stop music
  show lindsey flirty at appear_from_right
  lindsey flirty "There!"
  if preferences.get_volume("music"):
    $old_volume = preferences.get_volume("music")
    $preferences.set_volume("music", old_volume+0.25)
  play music "epic_medieval" fadein 0.5
  window hide
  pause 0.5
  show lindsey blush with Dissolve(.5):
    easein 0.175 yoffset -10
    easeout 0.175 yoffset 0
    easein 0.175 yoffset -4
    easeout 0.175 yoffset 0
  window auto
  lindsey blush "Oh, my god..."
  show lindsey blush:
    yoffset 0
  mc "Do you like it?"
  lindsey blush "I love it!"
  "I don't think I've ever seen [lindsey] this happy."
  "And that's all thanks to me..."
  "..."
  "Well, I guess the band did their part too."
  window hide
  show lindsey blush:
    parallel:
      choice:
        linear 0.5 xoffset -50
      choice:
        linear 0.5 xoffset 50
      pause 0.5
      linear 0.5 xoffset 0
      pause 0.5
      repeat
    parallel:
      linear 0.25 yoffset 25
      linear 0.25 yoffset 0
      pause 0.5
      repeat
  pause 1.0
  window auto
  "She's really getting into it. Few people have the confidence to dance on their own."
  lindsey blush "Come on, [mc]! Don't make me dance alone!"
  "Hmm... not the best idea for me. Last time I tried to dance was at a school disco. Everyone laughed."
  show lindsey blush:
    xoffset 0 yoffset 0
  show lindsey blush at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I twisted my ankle in the gym earlier...\"":
      show lindsey blush at move_to(.5)
      $mc.strength+=1
      mc "I twisted my ankle in the gym earlier..."
      lindsey laughing "That is the biggest lie I've heard!"
      lindsey laughing "Come on, I'll show you!"
      mc "I don't know..."
      lindsey flirty "Just put your hand on my waist."
      mc "Okay, fine..."
    "\"I'd much rather watch you.\"":
      show lindsey blush at move_to(.5)
      mc "I'd much rather watch you."
      lindsey thinking "And why is that?"
      mc "You look perfect on your own. I'd hate to ruin this image."
      $lindsey.love+=1
      lindsey blush "Oh..."
      "God, she's so adorable when she blushes!"
      lindsey blush "Come on! Dance with me!"
      mc "Okay, if you insist..."
      lindsey laughing "I do insist!"
      mc "Just don't expect anything amazing. I really can't dance."
      lindsey flirty "Don't worry. Put your hand on my hip."
      lindsey flirty "It's not hard!"
      mc "All right..."
  window hide
  show black onlayer screens zorder 100
  show lindsey dancing_with_the_mc_background as background behind lindsey:
    xalign 0.5 yalign 0.5 zoom 1.125
  show lindsey dancing_with_the_mc_smile
  with Dissolve(.5)
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  lindsey dancing_with_the_mc_smile "Good, just like that!"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "[lindsey] smells so nice up close... Cotton candy and spring. Her waist is tiny!"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_smile "Take it slow! Lead me around the room to the music."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Okay, this isn't too hard. As long as I don't step on her toes..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "This is nice. Thanks for inviting me over."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "My pleasure..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Uh-oh, did that sound dirty?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  mc "I mean... I enjoyed it too! You coming over, that is."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Crap, I'm making it worse."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_smile "Do you ever feel like life's too short?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Hmm..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "When you're stuck for years in the same rut..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "With no will to break the cycle..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "No ambitions. No dreams. No hope."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "When you have no energy to fulfill anything but the most basic of needs..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "Eat. Sleep. Get off."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Rinse and repeat. Day in, day out."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "When you don't even shower, cut your nails, or brush your teeth anymore..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Then no... life's not too short."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_smile:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "I don't think so."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_blush with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "I sometimes feel like I'm getting outrun by life."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "What do you mean?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "I practice so much that I don't have time for other things..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Oh..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "Everyone else is living, you know?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  mc "It feels like you're watching from the sideline?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_admiring with dissolve2:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_admiring "Exactly!"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_admiring:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Yeah, I know that feeling."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_admiring:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_admiring "Did you know I've never even had a boyfriend?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_admiring:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "You could have your pick..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "Perhaps, but I don't really have time for it."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_blush "And my family... well, they wouldn't like it."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  mc "Pretty religious?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_smile with dissolve2:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  lindsey dancing_with_the_mc_smile "Yeah..."
  menu(side="left"):
    extend ""
    "\"They probably just want to protect you.\"":
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_smile:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "They probably just want to protect you."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_smile:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      mc "There's nothing wrong with waiting."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_blush with dissolve2:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_blush "Do you really think so?"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_blush:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "Absolutely. I'm sure you'll find the right one."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_admiring with dissolve2:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      $lindsey.love+=5
      lindsey dancing_with_the_mc_admiring "That's so sweet..."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_admiring:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_admiring "My mom would like you."
    "\"Maybe you should start deciding things for yourself?\"":
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_smile:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "Maybe you should start deciding things for yourself?"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_blush with dissolve2:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_blush "I'm not very good at that."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_blush:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      mc "Well, then you need to find someone who can take care of you."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_blush:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      mc "Someone who will stand up for you and protect you, no matter what.{space=-15}"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.75
      show lindsey dancing_with_the_mc_admiring with dissolve2:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      lindsey dancing_with_the_mc_admiring "Like the dragon slayer?"
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.25
      show lindsey dancing_with_the_mc_admiring:
        linear 0.25 yoffset 25
        linear 0.25 yoffset 0
      mc "Yes. He's chivalrous and pure of heart."
      show lindsey dancing_with_the_mc_background as background:
        linear 0.5 xalign 0.5
      show lindsey dancing_with_the_mc_admiring:
        linear 0.25 yoffset 12.5
        linear 0.25 yoffset 0
      $lindsey.lust+=5
      lindsey dancing_with_the_mc_admiring "That would be the dream..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed with dissolve2:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "[lindsey] closes her eyes and lets me guide her to the music."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "She makes leading easy, matching my every move."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Her athletic body close to mine, the rise and fall of her chest..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "It's more intimate than a hug."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "My hand on the small of her back, mere inches away from her butt."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "It would be so easy to just slide it down and feel her up..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.0
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "Perhaps it's that temptation that makes it special?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Her trust that I won't abuse my position."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "It's strange how good that feels."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "That she trusts me to treat her right."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "..."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "Maybe that's my calling?"
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "To look after [lindsey] and protect her from the world."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.25
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Nothing will ever hurt her as long as I'm around."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.75
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "I'll be her biggest supporter, and her knight in shining armor."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 1.0
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 12.5
    linear 0.25 yoffset 0
  "Even if she doesn't see me as more than a friend."
  show lindsey dancing_with_the_mc_background as background:
    linear 0.5 xalign 0.5
  show lindsey dancing_with_the_mc_blush_closed:
    linear 0.25 yoffset 25
    linear 0.25 yoffset 0
  "She is a princess, after all... and that's what she deserves."
  $unlock_replay("lindsey_partner_dance")
  window hide
  stop music fadeout 3.0
  show black onlayer screens zorder 100
  show lindsey laughing
  with Dissolve(3.0)
  pause 1.0
  hide background
  hide black onlayer screens
  with Dissolve(.5)
  if preferences.get_volume("music"):
    $preferences.set_volume("music", old_volume)
  play music "home_theme" fadein 0.5
  $mc.add_item("king_bard_album")
  window auto
  lindsey laughing "I really enjoyed this!"
  mc "Same here!"
  lindsey smile "We should do it again sometime."
  mc "If you want to..."
  lindsey flirty "I'd like that very much!"
  lindsey thinking "Anyway, it's getting late and I should probably get going."
  lindsey blush "Thanks again for inviting me over!"
  show lindsey blush at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.love>=4@[mc.love]/4|{image=stats love}|\"I've been thinking... You should\nhave this album.\"":
      show lindsey blush at move_to(.5)
      mc "I've been thinking... You should have this album."
      lindsey blush "Are you serious?!"
      mc "Yeah! You're clearly a bigger fan than I am."
      $mc.remove_item("king_bard_album")
      lindsey excited_album "Oh, whoa! Thank you so much!"
      "Seeing [lindsey] happy makes my heart swell."
      "She's so pure and innocent in her ways."
      "One of the few people who hasn't been ruined by the evils of the world.{space=-65}"
      mc "Treat it as an early Christmas gift!"
      lindsey confident_album "Aw! It will be a hard one to top!"
      mc "I still have a few months to come up with something..."
      lindsey excited_album "Noo! This is enough for the next five Christmases!"
      lindsey excited_album "It is I who have to come up with something!"
      mc "I'll just settle for a stolen moment under the mistletoe."
      lindsey confident_album "Look up then, and imagine it's there..."
      window hide
      show black onlayer screens zorder 100
      show lindsey kiss_bedroom
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      mc "!!!"
      "[lindsey] gets up on her tippy-toes and leans in."
      "Our lips meet for a moment, and time seems to stop."
      "It sounds cliché, but it's what happens when a moment gets seared into memory for the rest of your life."
      "So soft, so sweet, on my upper lip..."
      "Misaligned, but still perfect. Still absolutely electric."
      "Then she moves down, taking my bottom lip between hers."
      "She blushes, smiles through the kiss, then gets it right."
      "Three kisses in one!"
      "It's one of those moments you'll never forget..."
      $unlock_replay("lindsey_kiss")
      $quest.lindsey_piano["kissed"] = True
      window hide
      show black onlayer screens zorder 100
      show lindsey excited_album
      with Dissolve(.5)
      pause 1.0
      hide black onlayer screens with Dissolve(.5)
      window auto
      lindsey excited_album "Thanks so much for the album! It is by far the most badass gift I've ever received."
      lindsey excited_album "I'm going to rock out all night!"
      mc "I'm really happy you liked it. Enjoy it!"
      lindsey confident_album "I will! See you at school, [mc]!"
      show lindsey confident_album at disappear_to_left
      "And there it is... I kissed [lindsey]. Most jocks can't even brag about that.{space=-75}"
      "So, all of this happened just because I took the time to learn the freaking piano..."
      "It's a strange world we live in."
    "\"My pleasure! I hope you have\na good night.\"":
      show lindsey blush at move_to(.5)
      mc "My pleasure! I hope you have a good night."
      lindsey flirty "You too..."
      mc "..."
      "She looks at me expectantly, somehow."
      mc "I'll... see you on the track?"
      lindsey smile "Sure."
      mc "And I guess in the corridors at school..."
      lindsey laughing "You bet."
      "Ugh, why does it have to be so awkward?!"
      lindsey smile "Okay, I'm gonna go..."
      mc "I hope you have a..."
      "Crap. I've already said that."
      mc "...pleasant and restful night of sleep."
      lindsey laughing "Thanks!"
      window hide
      show lindsey laughing at disappear_to_left
      pause 0.5
      window auto
      "Puh! She finally left."
      "Why do I always do that?!"
      "I really need to work on my social skills..."
  $home_bedroom["night"] = True
  $quest.lindsey_piano.finish()
  return
