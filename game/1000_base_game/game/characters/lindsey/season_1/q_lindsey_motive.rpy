init python:
  class Character_mer(BaseChar):

    default_name = "Mer"
    contact_icon = "hidden_number contact_icon"


label quest_lindsey_motive_start:
  "There's something about [lindsey]..."
  "It does sound cliché, but I could watch her for hours."
  "As soon as she enters the gym, she's like a magnet, attracting the eyes of everyone around."
  "And what does she do? She just runs."
  "Everyone can run."
  "I guess no one runs like her, though... and that's what makes her a joy to behold."
  "The way she flies through the gym, lap after lap, is special."
  "Her blurred legs are a marvel to behold."
  "No one can deny how fast she is. She has turned something so simple{space=-45}\nand mundane into an art form."
  "That's why people can't take their eyes off her."
  window hide
  show lindsey thinking_drinking with Dissolve(.5)
  window auto
  "And just like that, the world stops spinning."
  "She takes a sip of water, entirely unfazed by the show she just\nput on."
  "To her, it's just another day in the office."
  lindsey blush "Hey, [mc]! I didn't see you over there!"
  "And this is the best part — all the jocks looking at me in jealousy."
  "They have no idea how someone like me could get the attention of someone like [lindsey]."
  "Maybe they've been hitting on her for years without results? That's a warming thought."
  "I guess they didn't know that the key to her heart was an old music album..."
  lindsey flirty "I was just thinking about you."
  mc "All positive things, I hope."
  lindsey laughing "I'd say so!"
  show lindsey laughing at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Was it about how you're yearning\nto go to a concert with me?\"":
      show lindsey laughing at move_to(.5)
      mc "Was it about how you're yearning to go to a concert with me?"
      lindsey smile "That would depend on the band."
      mc "King's Bard, what else?"
      lindsey flirty "So, you're magically going to get the band back together?"
      mc "If that's what it takes!"
      $lindsey.lust+=1
      lindsey laughing "Okay, then. I'll go to a concert with you!"
      "Boom! And that's how it's done!"
      "Little does she know that the King's Bard will reunite in four years from now..."
    "\"Did it have something to do with jousting?\"{space=-25}":
      show lindsey laughing at move_to(.5)
      mc "Did it have something to do with jousting?"
      lindsey smile "What do you mean?"
      mc "You don't think I'd make a good knight?"
      mc "\"Sir [mc]\" has a nice ring to it."
      lindsey laughing "I'm not going to call you sir!"
      mc "Oh, we'll see about that, won't we?"
      lindsey skeptical "..."
    "\"I was actually thinking about you as well...\"{space=-15}":
      show lindsey laughing at move_to(.5)
      mc "I was actually thinking about you as well..."
      lindsey flirty "What did you think?"
      mc "That I'd like to see you race some time."
      lindsey blush "Would you really?"
      mc "Of course! I was just thinking how you make something as mundane{space=-10}\nas running seem cool and exciting."
      $lindsey.love+=1
      lindsey blush "Aw! Okay, my next big race is the week after Independence day!"
      mc "I'll mark it in my calendar."
  lindsey neutral "Anyway, what I actually had on my mind was our painting date."
  mc "Date! I very much like the sound of that!"
  lindsey excited "I thought you would!"
  mc "When?"
  lindsey excited "How about tomorrow?"
  mc "Sounds good!"
  lindsey confident "Great! I was thinking about doing an assignment for class."
  mc "Uh, okay... What assignment is that?"
  lindsey thinking_drinking "..."
  mc "..."
  lindsey thinking_drinking "..."
  mc "It's the anatomy assignment, isn't it?"
  "[lindsey] is always putting that one off because it's embarrassing to look at a nude model."
  lindsey cringe "That assignment makes me uncomfortable..."
  lindsey cringe "I was thinking that if we do it together, it'd be less embarrassing..."
  "Hmm... feels like less of a date than I expected, but if it helps her..."
  show lindsey cringe at move_to(.75)
  $nude_model = game.characters[school_art_class["nude_model"]]
  $nude_model = ("The " + nude_model.name) if nude_model == nurse else nude_model
  menu(side="left"):
    extend ""
    "\"Sorry, but painting someone naked\nisn't my idea of a good date.\"":
      show lindsey cringe at move_to(.5)
      mc "Sorry, but painting someone naked isn't my idea of a good date."
      lindsey thinking "You're probably right..."
      lindsey thinking "I'm just so low on time these days that I always try to maximize productivity."
      mc "I get that, but you also need to relax. I think an hour in nature would{space=-5}\ndo you good."
      $lindsey.love+=1
      lindsey blush "That's sweet of you. Okay, let's do it!"
      lindsey blush "I'll bring the easel and brushes. Would you mind bringing the paint?"
      mc "Not at all! See you tomorrow!"
      lindsey blush "See you!"
    "\"I don't mind. [nude_model] has volunteered\nto model.\"":
      show lindsey cringe at move_to(.5)
      mc "I don't mind. [nude_model] has volunteered to model."
      if nude_model == mrsl:
        lindsey laughing "Whoa! Really?"
      else:
        lindsey laughing "So, funny story! I did ask her to model for the assignment..."
        lindsey laughing "...but as soon as I mentioned how I was hoping to do it with you, out of the blue [mrsl] showed up and volunteered!"
      mc "You sound excited about that."
      lindsey smile "No, it's just that she used to be fairly reserved..."
      mc "Oh, right. What do you think about that? Pretty weird, right?"
      lindsey smile "I personally would never be so frivolous, but it's her choice and I'm not one to judge."
      mc "You don't think it's weird she dresses like that?"
      lindsey smile "It is a bit inappropriate, but if the school board is okay with it..."
      lindsey laughing "Besides, [jacklyn] is even worse."
      mc "Good point."
      if nude_model == mrsl:
        lindsey flirty "Anyway, I'll ask her to model for the assignment."
      else:
        lindsey flirty "Anyway, we already have a model, then."
      lindsey flirty "Would you mind bringing the paint? It's a lot for me to carry."
      mc "Not at all! See you tomorrow!"
      lindsey flirty "See you!"
      $quest.lindsey_motive["mrsl_model"] = True
  hide lindsey with Dissolve(.5)
  $quest.lindsey_motive.start()
  return

label quest_lindsey_motive_paint_supplies_outside:
  "Students aren't allowed to take school supplies outside, so I better make sure [jacklyn] isn't around to stop me."
  $quest.lindsey_motive["supplies_outside"] = True
  return

label quest_lindsey_motive_paint_jacklyn:
  "I can't really stuff my backpack full of paint while [jacklyn] is around..."
  "Got to wait for a better moment to strike."
  return

label quest_lindsey_motive_paint_shelf:
  "Okay, time to paint the town red with [lindsey]! Or at least the canvas.{space=-25}"
  $mc.add_item("tubes_of_paint")
  "Huh?"
  "..."
  "There's something back there, hidden behind the tubes."
  "..."
  "..."
  "Shit, I can't reach it. The woes of being short..."
  "It seems that, once more, an epic quest lies ahead of me."
  $quest.lindsey_motive.advance("hidden")
  $mc["focus"] = "lindsey_motive"
  return

label quest_lindsey_motive_hidden_door:
  "[jacklyn] will be back any moment. I need to be quick about this."
  return

label quest_lindsey_motive_hidden_shelf:
  "Damn, just out of reach..."
  "I must search far and wide for a tool to elevate my stature."
  return

label quest_lindsey_motive_hidden_chairs:
  "Ah, chairs — the short man's best friend."
  "Now let's see what lies hidden behind those tubes!"
  window hide
  $school_art_class["chair2_moved"] = True
  pause 0.5
  window auto
  "..."
  "Wait a minute..."
  "Isn't that..."
  $mc.add_item("chocolate_heart")
  "One of [isabelle]'s stolen chocolate hearts?"
  "What was it doing behind there?"
  "..."
  "[lindsey] said she gave the box to [jacklyn], who then threw it away."
  "But the mysterious texter said that they didn't find the box in the trash like [jacklyn] said."
  "Did [jacklyn] keep the box hidden here? And if so, why?"
  "Why even hide it?"
  "..."
  "Maybe it wasn't her?"
  "It's not like the door to this classroom is ever locked..."
  "Maybe she has seen something, though. She spends most of her time here."
  $quest.lindsey_motive.advance("jacklyn")
  $mc["focus"] = ""
  return

label quest_lindsey_motive_jacklyn:
  show jacklyn smile with Dissolve(.5)
  jacklyn smile "What's clapping, Dummy Thicc?"
  mc "You know, the usual."
  jacklyn smile "Sick. That's my type of audio."
  mc "Look, we need to talk."
  jacklyn neutral "What's with the oral fixation?"
  mc "I have to ask you about that box of chocolates."
  mc "You said [lindsey] gave it to you, and then you threw it away?"
  jacklyn thinking "Repeat and rewind much?"
  mc "Someone told me you didn't actually throw the box away."
  jacklyn angry "Who squealed?"
  mc "Doesn't matter. I just want to know why you lied."
  jacklyn annoyed "Cards on the table, okay? I didn't want you snooping."
  mc "Why not?"
  jacklyn annoyed "It's tomato cheeks, okay? People can't know."
  mc "I won't tell. I'm just trying to figure out what's going on..."
  jacklyn excited_right "Well, I kept it 'cause that sugar snap is totally adorbs."
  mc "[lindsey]?"
  jacklyn laughing "I don't dabble, but the gesture really fondled my blood pump."
  mc "Why did you throw it away, then?"
  jacklyn annoyed "I didn't. Some freebooter criminalized me."
  mc "So... someone stole it from you, and you lied because you didn't want to tell me that you'd kept it?"
  jacklyn excited "On the dough, baker-bro!"
  mc "I don't believe you."
  mc "You keep saying you don't dabble, but you don't strike me as someone{space=-50}\nwho would care if people thought you were gay."
  jacklyn thinking "That's a four-oh-four for me."
  mc "What I'm trying to say is, you wouldn't hide the box."
  mc "You'd own it."
  jacklyn neutral_hands_up "..."
  jacklyn smile "Seems like you have some horsepower in that egg."
  jacklyn smile "If you must know, someone gilded my wallet in exchange for hiding it.{space=-35}"
  mc "Who?"
  jacklyn neutral "No idea. Just got a letter and a check."
  mc "Do you still have the letter?"
  jacklyn neutral "Unfortunately not. The sender asked me to burn it."
  "Burn it? That sounds a lot like something [maxine] would do..."
  mc "All right. Thanks for your help."
  jacklyn smile_hands_down "Yeah, no big talk."
  hide jacklyn with Dissolve(.5)
  $quest.lindsey_motive.advance("maxine")
  return

label quest_lindsey_motive_maxine:
  show maxine afraid with Dissolve(.5)
  maxine afraid "I confess!"
  mc "You sent that letter?"
  maxine thinking "You're going to have to be more specific than that."
  maxine thinking "Thousands of letters pass through my mailbox every day."
  "Thousands? Really?"
  mc "[jacklyn] said she got a letter asking her to hide [isabelle]'s box of chocolates."
  mc "Do you know anything about that?"
  maxine thinking "No."
  mc "No? I thought nothing slips past you."
  maxine confident "Slippery things slip past me. Soap, for example."
  maxine skeptical "That's why I don't shower in the school — slippery soap goblins."
  mc "Right. So, what exactly did you confess to earlier?"
  maxine confident_hands_down "Oh, the experiments, of course."
  mc "..."
  mc "What experiments?"
  maxine confident_hands_down "The human experiments currently taking place in the school."
  mc "H-human... experiments...?"
  maxine skeptical "The Juice Apocalypse is coming. We need to be prepared."
  if quest.nurse_photogenic["eavesdropped"]:
    "This is what [lindsey] confronted her about earlier."
    "Man, I was so dizzy then, I thought I hallucinated that whole thing..."
  mc "What exactly is the Juice Apocalypse?"
  maxine smile "If you don't know, you're not meant to know."
  mc "Wow, that's helpful."
  maxine laughing "You're welcome!"
  mc "Ugh... what kind of experiments are you running, anyway?"
  maxine neutral "Stress testing with different types of chemical compounds."
  "What the fuck?"
  mc "Who are your victims, err... I mean, subjects?"
  maxine smile "I can't share any information other than that they're selected randomly.{space=-60}"
  if quest.nurse_photogenic["eavesdropped"]:
    mc "What about me? I know that you've tested your shit on me."
    maxine laughing "I can assure you that no excrement, mine or otherwise, was part of the experiment."
    mc "But you tested on me the other day."
    maxine smile "Due to the confidentiality of the double blind procedure, I can't discuss that with you."
    mc "You put something in my damn strawberry juice!"
    maxine smile "No comment."
    mc "Did you experiment on [lindsey] too? Is that why she's been tripping so much lately?"
  else:
    mc "Did you experiment on [lindsey]? Is that why she's been tripping so much lately?"
  maxine thinking "Side effects do not include loss of balance."
  mc "...because that's caused by the saucer-eyed doll, right?"
  "Some of these things are almost too dumb to say out loud."
  maxine sad "That's been taken care of."
  mc "So, you're saying if she trips now, it has nothing to do with that doll or your experiment?"
  maxine thinking "That would be correct."
  mc "And what exactly are the side effects of this drug?"
  maxine thinking "It's not a drug."
  mc "Sorry, this {i}chemical compound...{/}"
  maxine skeptical "Side effects include dizziness, memory loss, and hypno-somnambulism.{space=-80}"
  mc "Are you serious?"
  maxine skeptical "Absolutely. Science is nothing to joke about."
  "[lindsey] didn't remember the shoe incident or stealing [isabelle]'s chocolate hearts..."
  "But what the hell does hypno-somnambulism mean?"
  mc "If you run experiments on me or [lindsey] again, I'm reporting you to the principal."
  maxine annoyed "Why?"
  mc "Because we're not your guinea pigs!"
  maxine angry "How dare you stand in the way of science?"
  mc "It's simple. I'm a daredevil."
  maxine neutral "I see. Have a good day."
  hide maxine with dissolve2
  "I should probably google hypno-whatever..."
  $quest.lindsey_motive.advance("google")
  return

label quest_lindsey_motive_google:
  $school_computer_room["screen1"] = home_bedroom["small_pc"] = "google"
  "{i}\"Hypno... somn...ambulism...\"{/}"
  $school_computer_room["screen1"] = home_bedroom["small_pc"] = "wikipedia"
  "Let's see here..."
  "{i}\"A trance-like mental state in which a person experiences increased suggestibility.\"{/}"
  "{i}\"Unlike a regular hypnotic static, the person does not experience hyper-awareness.\"{/}"
  "{i}\"Instead, the hypno-somnambulism state is more akin to sleepwalking.\"{/}{space=-70}"
  "..."
  "Son of a bitch."
  "If [lindsey] entered this state, someone could've told her to steal [isabelle]'s chocolate hearts."
  "This explains everything..."
  "Well, everything except who did it."
  "If [kate] was behind it, she would've rubbed it in [isabelle]'s face by now.{space=-35}"
  "But if [kate] didn't do this, I'm all out of leads..."
  "Does anyone else have a bone to pick with [isabelle]?"
  "She can be a bit stuck-up sometimes, but people generally like her."
  "..."
  "Damn it! I refuse to let this be a dead end."
  "Recounting the events, [lindsey] stole the box from [isabelle]'s locker before she went to practice and shared it with the team."
  "That much is fact."
  "It's also a fact that [lindsey]'s practice starts first thing in the morning.{space=-45}"
  "So, she must've stolen the box right after changing in the locker room. That's a pretty precise timeline."
  "On the surveillance footage, [isabelle] went to her locker and picked up her sports bag, and then [lindsey] showed up right after that."
  "So, if [isabelle] was heading toward the locker room and [lindsey] was heading toward the entrance hall..."
  "There's a big chance [isabelle] ran into [lindsey] on her way to the gym!{space=-25}"
  "Perhaps she even saw who talked to [lindsey]!"
  "[mc], you're a goddamn genius."
  $school_computer_room["screen1"] = home_bedroom["small_pc"] = False
  $quest.lindsey_motive.advance("isabelle")
  return

label quest_lindsey_motive_isabelle:
  show isabelle excited with Dissolve(.5)
  isabelle excited "Hey, you look excited!"
  mc "Hey! I have something to ask you, and it's important that you try your best to remember."
  isabelle concerned "This sounds serious. I'll do my best."
  mc "Think back to the day your chocolate hearts were stolen."
  mc "You came in through the entrance hall, went to your locker, and put your purse and chocolate box in."
  isabelle concerned "I remember that."
  mc "Then you grabbed your sports bag and headed up the stairs."
  isabelle sad "Yeah, I was late for gym class because my water boiler broke and I had to make my morning tea on the stove."
  mc "So, you're walking up the stairs. The school is pretty deserted at this hour."
  isabelle concerned "Yeah, that sounds right."
  mc "Do you remember seeing anyone by the window?"
  isabelle concerned_left "Hmm... I don't think so..."
  mc "What about when you entered the sports wing?"
  isabelle excited "Oh, I did see [lindsey] coming out of the locker room!"
  isabelle skeptical "I said hi to her, but she was looking at her phone. Bloody rude, if you ask me."
  mc "Was she with anyone?"
  isabelle concerned "I don't think so."
  mc "What about inside the locker room? Did you see anyone there?"
  isabelle concerned_left "Hmm... I'm pretty sure I was alone there that morning..."
  "Crap. Another dead end."
  isabelle neutral "Why do you ask?"
  mc "Never mind. I was just playing detective, but I guess I'm not very good at it."
  isabelle excited "Hey, you found the box for me! I'd say you're quite the Sherlock!"
  mc "Thanks..."
  isabelle neutral "Sorry, but I have to run. I've got a poem to write for English class."
  isabelle neutral "Don't be too hard on yourself!"
  hide isabelle with dissolve2
  "Well, that led nowhere..."
  "Anyway, big day tomorrow with [lindsey]. I better get some shut eye."
  $quest.lindsey_motive.advance("sleep")
  return

label quest_lindsey_motive_lindsey:
  show lindsey excited with Dissolve(.5)
  lindsey excited "There you are! I thought you skipped out on me."
  mc "I would never!"
  lindsey excited "Did you get the paint?"
  mc "Sure did."
  if quest.lindsey_motive["mrsl_model"]:
    lindsey confident "Awesome! I asked [mrsl] to meet us outside."
    lindsey confident "I just need to grab the easels. See you there?"
    $quest.lindsey_motive.advance("date_mrsl")
  else:
    lindsey confident "I just need to grab the easels. Meet me outside?"
    $quest.lindsey_motive.advance("date_lindsey")
  mc "Okay, see you there."
  hide lindsey with Dissolve(.5)
  return

label quest_lindsey_motive_date_lindsey_door:
  "Painting with [lindsey], at long last. Things are finally looking up."
  "Hopefully, there aren't any beavers around this time..."
  jump goto_school_entrance

label quest_lindsey_motive_date_lindsey:
  show lindsey smile with Dissolve(.5)
  lindsey smile "There you are!"
  mc "Here I am!"
  lindsey laughing "I thought you'd gotten lost."
  mc "I just had to take care of some stuff."
  lindsey smile "Well, are you ready?"
  mc "I'm so ready."
  lindsey flirty "Okay, follow me! I've found the perfect spot!"
  window hide
  show black onlayer screens zorder 100
  show lindsey painting_date_laughing
  with Dissolve(.5)
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  $mc.remove_item("tubes_of_paint")
  window auto
  lindsey painting_date_laughing "Finally, right? I can't believe a beaver stole the easel you made for me!{space=-45}"
  mc "Neither can I..."
  lindsey painting_date_smile "Well, these are a bit sturdier. I don't think we'll have that problem again."
  lindsey painting_date_smile "Anyway, what happened to that beaver?"
  "Crap. That's not the question I was hoping for."
  menu(side="middle"):
    extend ""
    "\"It chewed up the easel, but eventually escaped into the forest.\"":
      mc "It chewed up the easel, but eventually escaped into the forest."
      lindsey painting_date_laughing "Well, it'll make for a great story for many years to come."
      "As long as she doesn't learn what really happened..."
      mc "Yeah, it was definitely stranger than fiction."
    "?mc.charisma>=5@[mc.charisma]/5|{image=stats cha}|\"I drowned it in the aquarium.\"":
      mc "I drowned it in the aquarium."
      lindsey painting_date_laughing "You're joking!"
      mc "I tracked the beast down and fought it at the top of the mountain."
      lindsey painting_date_smile "Did you, now?"
      mc "Yeah, it was a fierce battle... vile fangs gnashing against steel and courage..."
      mc "The thought of you is what kept me going in my darkest hour."
      mc "Like a patron saint of light and hope, you scattered darkness and sharpened my blade."
      mc "And with your name on my lips, I vanquished my foe."
      $lindsey.lust+=2
      lindsey painting_date_blush "That is quite the story!"
      lindsey painting_date_blush "But what about the aquarium?"
      mc "Oh, uh. That's what I call my sword."
      lindsey painting_date_laughing "Oh, really? You'll have to show it to me some time!"
      mc "You want to see my sword?"
      lindsey painting_date_pouting "Hey!"
      mc "What?"
      lindsey painting_date_pouting "You made a face."
      mc "I'm sorry, this is just how I look."
      lindsey painting_date_laughing "You know what you did."
      mc "Nothing wrong with liking swords."
      lindsey painting_date_pouting "There it is again!"
      mc "No, this is just a paint brush. My sword is way bigger."
      lindsey painting_date_laughing "Stop it!"
      mc "Heh, fine."
    "\"Let's not dwell on the past.\"":
      mc "Let's not dwell on the past."
      lindsey painting_date_laughing "You're probably right."
      lindsey painting_date_smile "I always look to the future. I have so many plans."
      mc "You're going to go far."
      mc "Scholarship at some prestigious college. Taking the athlete world by storm."
      mc "National championships. The Olympic Games. Fame and fortune!"
      $lindsey.love+=1
      lindsey painting_date_blush "Aw! You're too nice!"
      mc "I'm also right."
  lindsey painting_date_smile "So, what are you going to paint?"
  mc "Oh, I don't know... maybe you?"
  lindsey painting_date_blush "Me?"
  mc "Yeah, I've been looking for a motif since we got here, but none of the flowers here can compare to you."
  $lindsey.love+=1
  lindsey painting_date_laughing "You total cheeseball!"
  mc "I mean it!"
  show lindsey painting_date_blush with dissolve2
  mc "I haven't felt this way before. Whenever I'm with you, I feel so light and happy."
  mc "I really like you, [lindsey]."
  lindsey painting_date_blank "..."
  mc "[lindsey]?"
  lindsey painting_date_blank "What...?"
  mc "Are you okay?"
  lindsey painting_date_blank "Um..."
  "What's happening?"
  lindsey painting_date_blank "..."
  "Is this the hypno-somnambulism that [maxine] talked about?"
  "Man, I really need to find a way to detox her somehow..."
  "...or maybe I should try giving her a suggestion?"
  mc "[lindsey]..."
  menu(side="middle"):
    extend ""
    "?mc.lust>=5@[mc.lust]/5|{image=stats lust}|\"Show me your boobs.\"":
      $mc.love-=1
      mc "Show me your boobs."
      lindsey painting_date_blank "Um... okay..."
      window hide
      show lindsey painting_date_boobs_blank with Dissolve(.5)
      window auto
      mc "Very nice, [lindsey]!"
      "Hold up, what if she suddenly wakes up?"
      "Uhhh..."
      mc "[lindsey], jump in the water!"
      lindsey painting_date_boobs_blank "Okay..."
      window hide
      play sound "<from 0.25>water_splash2" volume 0.5
      show lindsey painting_date_water with vpunch
      window auto
      "Oh, wow! She jumped right in, no questions asked!"
      lindsey painting_date_water "..."
      mc "Err, you can come out of the water again!"
      lindsey painting_date_water "Okay..."
      window hide
      show lindsey painting_date_boobs_wet_blank with Dissolve(.5)
      window auto
      "Perfect! This way, I can just tell her she fell into the pond if she suddenly wakes up."
      "Hmm... what next? So many possibilities..."
      mc "[lindsey]..."
      menu(side="middle"):
        extend ""
        "\"Bark like a dog.\"":
          $mc.lust+=1
          mc "Bark like a dog."
          lindsey painting_date_boobs_wet_blank "Woof. Woof."
          mc "Good girl!"
          "That's oddly hot..."
          mc "Moo like a cow."
          lindsey painting_date_boobs_wet_blank "Moo."
          mc "Again!"
          lindsey painting_date_boobs_wet_blank "Moo."
          "Okay, I should probably get the [nurse] now."
          mc "Wait here, [lindsey]."
        "\"Give me the money in your wallet.\"":
          $mc.love-=2
          mc "Give me the money in your wallet."
          lindsey painting_date_boobs_wet_blank "Okay..."
          window hide
          show lindsey painting_date_boobs_wet_armless_blank with Dissolve(.5)
          pause 0.25
          show lindsey painting_date_boobs_wet_wallet_blank with Dissolve(.5)
          window auto
          lindsey painting_date_boobs_wet_wallet_blank "..."
          "This feels so wrong, but I really need the money."
          "She'll be fine, though. She has like eleven scholarships to choose from next year."
          $mc.money+=240
          "Damn, she's loaded!"
          "..."
          "Was loaded."
          "Okay, I should probably get the [nurse] now."
          mc "Wait here, [lindsey]."
        "?mc.lust>=8@[mc.lust]/8|{image=stats lust}|\"Take your skirt and panties off.\"":
          mc "Take your skirt and panties off."
          lindsey painting_date_boobs_wet_blank "Okay..."
          window hide
          pause 0.25
          $lindsey.unequip("lindsey_skirt")
          pause 1.0
          $lindsey.unequip("lindsey_panties")
          pause 0.75
          window auto
          "Now, that's what I'm talking about!"
          "God, I love her tan lines..."
          "Most girls try to get rid of them, but [lindsey] is a little worker bee."
          "Always running in the sun."
          "And the skin of her tits and pussy is so creamy and white."
          "I bet it's the softest part of her."
          "Fuck..."
          mc "Say, \"Fondle me, daddy!\""
          lindsey painting_date_boobs_wet_blank "Fondle me, daddy..."
          "Well, the monotony in her voice kind of ruins it..."
          $mc.lust+=2
          "Still, it was worth a shot."
          "Okay, I should probably get the [nurse] now."
          mc "Wait here, [lindsey]."
        "\"Wait here while I get help.\"":
          $mc.love+=1
          mc "Wait here while I get help."
    "\"Tell me your darkest secret.\"":
      $mc.lust+=1
      mc "Tell me your darkest secret."
      lindsey painting_date_blank "I once cheated on a science test..."
      lindsey painting_date_blank "I sometimes still have nightmares about it..."
      "That's it? God, she's such a perfect princess."
      "Hmm..."
      mc "Tell me your darkest sexual fantasy."
      lindsey painting_date_blank "I'm on my knees in a rusty cage..."
      lindsey painting_date_blank "My dress is torn and ragged..."
      "Jesus Christ."
      lindsey painting_date_blank "Darkness lies thick..."
      lindsey painting_date_blank "But something is moving out of sight..."
      lindsey painting_date_blank "The sound of steel plates clinking and rustling over the rock floor..."
      lindsey painting_date_blank "A massive eye opens... glowing like a furnace... watching me... amused at my cries for help..."
      "Man, this is some dark shit. I had no idea [lindsey] was into horror porn."
      lindsey painting_date_blank "But then, a beam of silvery light shines into the massive cave, and a knight steps forth..."
      lindsey painting_date_blank "Flames burn in the reflections of his armor and shield..."
      lindsey painting_date_blank "His sword sings as he pulls it from his scabbard..."
      "Ah, of course. I should've seen this coming."
      lindsey painting_date_blank "The dragon roars and flaps its giant wings... jagged teeth... torrents of liquid fire—"
      mc "Yeah, and then the knight chops off the head of the dragon and saves you. Great stuff, [lindsey]."
      lindsey painting_date_blank "..."
      "I should probably get the [nurse]."
      mc "Wait here, [lindsey]."
    "\"Wait here while I get help.\"":
      $mc.love+=2
      mc "Wait here while I get help."
  mc "I'll be right back."
  if renpy.showing("lindsey painting_date_blank"):
    lindsey painting_date_blank "..."
  elif renpy.showing("lindsey painting_date_boobs_wet_wallet_blank"):
    lindsey painting_date_boobs_wet_wallet_blank "..."
  else:
    lindsey painting_date_boobs_wet_blank "..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  pause 0.25
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Okay, this date didn't quite turn out the way I had planned—"
  play sound "<from 0.25>water_splash2" volume 0.25
  with vpunch
  "Huh?"
  mc "[lindsey]?"
  pause 0.25
  show lindsey painting_date_float
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Oh, god. What is she doing?"
  mc "[lindsey], are you okay?"
  window hide
  show lindsey painting_date_sink with Dissolve(.5)
  window auto
  "Oh, fuck. She disappeared below the surface. That's not good."
  "Man, I'm really not cut out to be a hero..."
  mc "[lindsey]!!!"
  "..."
  "Fuck it! I'm going in!"
  window hide
  play sound "<from 0.25>water_splash2"
  show black onlayer screens zorder 100 with vpunch
  pause 0.25
  play ambient "audio/sound/water_drown.ogg" volume 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Oh, god! She's at the bottom already..."
  "..."
  "Ugh, I should have paid more attention to the swim instructor in kindergarten..."
  "Come on, [lindsey]..."
  stop ambient
  pause 0.25
  show lindsey making_out_limp
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "[lindsey]!"
  lindsey making_out_limp "..."
  mc "Come on, wake up!"
  lindsey making_out_limp "..."
  mc "Wake up, [lindsey]!"
  lindsey making_out_dazed "Mmm..."
  mc "Oh, thank god! You're alive!"
  lindsey making_out_surprised "W-what happened...?"
  mc "You fell into the pond..."
  lindsey making_out_surprised "Last thing I remember I was painting..."
  lindsey making_out_surprised "How did I end up in the water? And where did my clothes go?"
  mc "Err... I don't know, but I'm glad you're okay."
  lindsey making_out_surprised "Did you also fall in?"
  mc "Not exactly. Someone had to pull you out."
  lindsey making_out_adore "You saved me?"
  mc "I guess I did..."
  lindsey making_out_adore "You're like my guardian angel!"
  mc "I mean, I wouldn't' go that far..."
  lindsey making_out_adore "It feels like you're always there!"
  if quest.lindsey_nurse["carried"]:
    $lindsey.lust+=1
    lindsey making_out_adore "You carried me to the [nurse] when I crashed into you..."
  if quest.lindsey_wrong["swooped_in"]:
    $lindsey.lust+=1
    lindsey making_out_adore "You caught me when I was about to fall down the stairs..."
  if quest.lindsey_wrong["fetched"]:
    $lindsey.lust+=1
    lindsey making_out_adore "You helped me get dry clothes..."
  mc "I guess—"
  lindsey making_out_adore "I don't."
  window hide
  show lindsey making_out_blush with Dissolve(.5)
  window auto
  mc "!!!"
  if quest.lindsey_piano["kissed"]:
    "Whenever our lips meet, it's like an electrical current surges through me."
  else:
    "Our lips meet for the first time, and it's like nothing I've ever experienced before."
  "It's like getting a cold glass of water after walking through the desert."
  "Like jumping into a rejuvenating oasis."
  "Hopelessness, fear, self-doubt melt away."
  "And for a moment, her energy is mine. Her excitement, strength, and determination — a rush of life from her lips to mine."
  "It's one of those things that never gets old..."
  window hide
  show lindsey making_out_workaround_hand_adore with Dissolve(.5)
  window auto
  lindsey making_out_workaround_hand_adore "In fact, I'm quite sure."
  lindsey making_out_workaround_hand_adore "Kiss me again."
  mc "You don't have to say that twice."
  window hide
  show lindsey making_out_blush with Dissolve(.5)
  window auto
  "I read somewhere that human lips have over a million nerve endings..."
  if quest.lindsey_piano["kissed"]:
    "...and whenever I kiss [lindsey], they all fire at once."
  else:
    "...and this kiss proves it."
  "A million volts, right into my lips. A million stars, lighting up my brain."
  "To think that [lindsey] called me her guardian angel... maybe I'm not as useless as I thought."
  "But was it really heroics and bravado that brought me here?"
  "Maybe I'm the villain of the story..."
  window hide
  show lindsey making_out_boob_blush with Dissolve(.5)
  show lindsey making_out_boob_surprised2 with Dissolve(.5)
  window auto
  lindsey making_out_boob_surprised2 "Hey!" with vpunch
  mc "Sorry, um..."
  lindsey making_out_boob_blush "No, it's okay..."
  lindsey making_out_boob_blush "I don't mind. I was just surprised."
  "Whoa! I wasn't expecting that!"
  window hide
  show lindsey making_out_boob_hand_blush with Dissolve(.5)
  window auto
  lindsey making_out_boob_hand_blush "Mmmm..."
  "Her lips and mouth taste like cotton candy."
  "Very wet cotton candy."
  "Making out is a lot sloppier than I thought... but it's sloppy in a good way."
  "It's nothing like those hentai clips where strings of saliva stretch like telephone lines between the mouths of the kissers."
  "In fact, kissing [lindsey] is nothing like porn."
  "It's sweet and gentle, and her playful tongue engages mine in a shy game of peekaboo."
  "Her stiff nipple pokes at my palm as I squeeze her breast."
  "I can't believe she's allowing me to grab her like this. To feel her up completely."
  "But her muffled moaning says it all."
  menu(side="middle"):
    extend ""
    "Move your hand lower":
      $lindsey.love-=1
      $lindsey.lust-=1
      lindsey making_out_nervous "N-no..."
      "Crap. She didn't like that."
      mc "I thought, um..."
      lindsey making_out_nervous "I can't..."
      mc "Why?"
      lindsey making_out_nervous "It's too soon..."
      mc "I thought you liked it..."
      lindsey making_out_nervous "I did... but I'm saving myself for marriage..."
      mc "Oh, sorry..."
      mc "That's not what I had in mind, I was just..."
      lindsey making_out_nervous "It's fine. Can you let me up, please?"
      lindsey making_out_nervous "I think it's time to go back."
      "God, why did I have to be greedy?"
      "I always fuck these things up..."
      "[lindsey] looked like she enjoyed it, but I should've known better."
      "She's a princess, and if I want to have a real chance with her, I need to start treating her as one."
    "Don't push your luck":
      $lindsey.love+=3
      $lindsey.lust+=3
      lindsey making_out_adore2 "I like you, [mc]."
      "The adoration in her eyes is like a healing balm for my wretched soul."
      "Having someone like [lindsey] look at me like that..."
      "Like I'm actually worth something."
      "Maybe even a lot."
      "Well, nothing can really compare."
      "Even with my hand on her breast and my mouth sweet with her taste, it's the validation that sets me free."
      mc "I like you too, [lindsey]."
      window hide
      show lindsey making_out_boob_hand_blush with Dissolve(.5)
      window auto
      lindsey making_out_boob_hand_blush "Mmm..."
      "I could stay like this forever..."
      "Dream my life away with her."
      "But the truth is, [lindsey] has a life to get back to."
      "But maybe that's not the worst thing..."
      "I could see myself following her to running events all over the world."
      "I'm sure Chad couldn't stand not being the number one in the relationship."
      "But I don't mind being the supporting boyfriend if that's what it takes to win her heart."
      "I would make sure she'd be the best runner in the world."
      "I would be her fan, her lover, and her guardian angel."
      "Nothing bad would ever happen to her."
      "But for now, I'll just enjoy the moment..."
  $unlock_replay("lindsey_hypno_somnambulism")
  $lindsey.equip("lindsey_panties")
  $lindsey.equip("lindsey_skirt")
  window hide
  show black onlayer screens zorder 100
  show lindsey smile
  with Dissolve(.5)
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  lindsey smile "Thanks so much for painting with me. I had a great time!"
  mc "Maybe next time I'll paint you like one of my French girls."
  lindsey laughing "Please tell me that's a Titanic reference."
  mc "Who knows!"
  lindsey smile "Well, I wouldn't mind spending more time with you."
  mc "I think I've exhausted my creativity for a while, so hopefully no more painting."
  lindsey flirty "If you ask me the right question now, I might just say yes."
  show lindsey flirty at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What question is that?\"":
      show lindsey flirty at move_to(.5)
      mc "What question is that?"
      lindsey laughing "If you don't know it, maybe you're not ready to ask it."
      "Man, why do girls always do this?"
      "Always these coy games. I wish they would just tell me what they wanted..."
      lindsey skeptical "Huh?"
      lindsey skeptical_phone "..."
    "\"Will you marry me?\"":
      show lindsey flirty at move_to(.5)
      $mc.charisma+=1
      mc "Will you marry me?"
      lindsey laughing "That is {i}not{/} the question I had in mind!"
      mc "But it's a yes, right?"
      lindsey flirty "Oh, gosh... we just met... at least buy me dinner first!"
      mc "I guess I can work with that. Tomorrow night?"
      $lindsey.love+=1
      lindsey laughing "Haha! I have practice tomorrow night, but... um..."
      lindsey skeptical "Huh?"
      lindsey skeptical_phone "..."
      "Great timing as usual. The universe really hates me."
    "\"Will you let me fight\nin your honor, m'lady?\"":
      show lindsey flirty at move_to(.5)
      mc "Will you let me fight in your honor, m'lady?"
      $lindsey.lust+=1
      lindsey blush "Ohh!"
      mc "Is that a yes?"
      lindsey blush "I'm not sure if I need any fighting done in my name right now, but I'll keep your offer in mind!"
      mc "When the time comes, I'll be ready with my sword and shield."
      lindsey laughing "This wasn't what I had in mind, but I kind of like it."
      mc "At your service, m'lady."
      lindsey laughing "..."
      "Crap. Maybe that was too much."
      lindsey skeptical "Huh?"
      lindsey skeptical_phone "..."
  jump quest_lindsey_motive_date_ending

label quest_lindsey_motive_date_mrsl:
  $mrsl.equip("mrsl_bra")
  show mrsl confident at Transform(xalign=.25)
  show lindsey smile at Transform(xalign=.75)
  with Dissolve(.5)
  lindsey smile "There he is."
  mrsl confident "I heard you'll be joining us, [mc]."
  mc "Yeah, the, err... art grade is very important to me."
  mrsl laughing "That's good to hear. You've really been stepping it up this year."
  show mrsl laughing at move_to("left")
  show lindsey smile at move_to("right")
  menu(side="middle"):
    extend ""
    "\"[lindsey] is so driven, it's hard\nnot to be inspired.\"":
      show mrsl laughing at move_to(.25)
      show lindsey smile at move_to(.75)
      mc "[lindsey] is so driven, it's hard not to be inspired."
      show mrsl confident
      $lindsey.love+=2
      lindsey blush "Aw, you say the nicest things!"
      mc "Only when they're true."
      lindsey blush "Oh, stop it!"
      mrsl concerned "Sorry to interrupt, but I don't have a lot of time..."
      lindsey laughing "Oh, of course! Let's get going!"
    "\"Your assets are invaluable...\"":
      show mrsl laughing at move_to(.25)
      show lindsey smile at move_to(.75)
      mc "Your assets are invaluable..."
      mc "...as a teacher... to this school..."
      mc "I meant you're an asset!"
      $mrsl.lust+=2
      mrsl flirty "I've already agreed to take my clothes off, no need to be a flirt!"
      lindsey thinking "That's a bit inappropriate."
      mrsl embarrassed "Sorry, I was just trying to be friendly..."
      mc "Yeah, loosen up, [lindsey]."
      $lindsey.love-=1
      lindsey cringe "Let's just do this."
    "?mc.lust>=8@[mc.lust]/8|{image=stats lust}|\"I think it's time for you\nto undress, [mrsl].\"":
      show mrsl laughing at move_to(.25)
      show lindsey smile at move_to(.75)
      mc "I think it's time for you to undress, [mrsl]."
      show lindsey cringe
      mrsl flirty "Oh, my. Someone's eager to get started."
      lindsey cringe "[mc]!"
      mc "What?"
      lindsey cringe "We haven't even found the right spot yet!"
      mc "That's exactly my point. How are we supposed to find a good spot if we don't know what we're working with?"
      lindsey thinking "I mean, I guess that makes sense..."
      lindsey thinking "Would you mind undressing for us, [mrsl]?"
      mrsl flirty "Oh, not at all."
      mrsl laughing "Anything for the sake of education."
      lindsey angry "Then get to it."
      mrsl surprised "..."
      "Whoa! That's so unlike her... so aggressive and demanding."
      show mrsl surprised at move_to("left")
      show lindsey angry at move_to("right")
      menu(side="middle"):
        extend ""
        "\"Are you okay, [lindsey]?\"":
          show mrsl surprised at move_to(.25)
          show lindsey angry at move_to(.75)
          mc "Are you okay, [lindsey]?"
          lindsey afraid "D-did I say that out loud? I'm so sorry!"
          mrsl neutral "That's okay, [lindsey]. Don't worry about it."
          lindsey cringe "I'm sorry, I get really nervous when nudity is involved..."
          mc "Just try to relax, okay? It's part of the curriculum, just like math and sports."
          $lindsey.love+=2
          lindsey laughing "You're right. I don't know why it makes me so uncomfortable."
          mrsl flirty "We'll take it slow, okay?"
          lindsey smile "Okay..."
        "\"You heard her!\"":
          show mrsl surprised at move_to(.25)
          show lindsey angry at move_to(.75)
          mc "You heard her!"
          lindsey afraid "I'm so sorry! I-I don't know what came over me..."
          mc "That's okay, [lindsey]. Our grades are on the line here, and so is\nour time."
          $mrsl.lust+=2
          mrsl embarrassed "You're right. My apologies."
      window hide
      if renpy.showing("mrsl flirty"):
        show mrsl confident with Dissolve(.5)
      elif renpy.showing("mrsl embarrassed"):
        show mrsl excited with Dissolve(.5)
      pause 0.25
      $mrsl.unequip("mrsl_dress")
      pause 0.75
      window auto
      mrsl "There's the dress."
      mrsl "Would you mind holding my clothes for me, [mc]?"
      if renpy.showing("lindsey smile"):
        lindsey skeptical "..."
      elif renpy.showing("lindsey afraid"):
        lindsey cringe "..."
      "As long as you keep stripping, I wouldn't mind anything, [mrsl]!"
      mc "Not at all."
      window hide
      pause 0.25
      $mrsl.unequip("mrsl_bra")
      pause 0.75
      window auto
      "Fuck, that's hot!"
      "People are starting to look over and stare..."
      window hide
      pause 0.25
      $mrsl.unequip("mrsl_panties")
      pause 0.75
      window auto
      mrsl "There we go."
      "I can't believe we got [mrsl] to strip in the middle of the school yard.{space=-15}"
      "And now we'll march her around the school grounds in search of a good spot..."
      "No spots are quite what I had in mind, so we'll keep looking... and keep looking... and keep looking..."
      "And I'll keep looking at her perfect body."
      lindsey thinking "Let's get going?"
      mrsl laughing "Yeah, we should probably go before this becomes a spectacle."
      mc "After you."
  window hide
  show black onlayer screens zorder 100
  show mrsl painting_date_lindsey_smile
  with Dissolve(.5)
  pause 2.0
  hide lindsey
  hide black onlayer screens
  with Dissolve(.5)
  $mc.remove_item("tubes_of_paint")
  window auto
  lindsey "Okay, I guess it was worth being picky. This is a great spot!"
  "She was starting to get annoyed, so I had to settle here..."
  mc "I told you so."
  lindsey "I'm sorry I ever doubted you!"
  window hide
  show mrsl painting_date_smile_lindsey_smile with Dissolve(.5)
  window auto
  mrsl painting_date_smile_lindsey_smile "Are you guys ready to get started?"
  mc "More than ready!"
  show mrsl painting_date_smile_lindsey_worried with Dissolve(.5)
  lindsey "I guess..."
  mrsl painting_date_smile_lindsey_worried "So, what pose should I do?"
  mc "What do you think, [lindsey]?"
  lindsey "Err... I don't know..."
  mc "Maybe we should have her take a seat and spread her legs apart?"
  lindsey "N-no, let's just paint..."
  mc "But we need a good pose!"
  mc "How about having her bend over?"
  show mrsl painting_date_flirty_lindsey_skeptical with dissolve2
  lindsey worried "No! Definitely not that!"
  show mrsl painting_date_smile_lindsey_skeptical with dissolve2
  mc "Okay, okay."
  show mrsl painting_date_smile_ with Dissolve(.5)
  lindsey "I'm just going to start..."
  mc "Well, I'm not quite happy with the pose just yet."
  mc "[jacklyn] has actually taught me a few things."
  show mrsl painting_date_smile_lindsey_worried with Dissolve(.5)
  lindsey "Okay, what is it?"
  mc "[mrsl]..."
  menu(side="middle"):
    extend ""
    "\"Can you put your hands\nbehind your head?\"":
      mc "Can you put your hands behind your head?"
      window hide
      show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
      window auto
      mrsl painting_date_arms_behind_head_lindsey_worried "Like this?"
      mc "Oh, yeah. I think that's a good pose."
      show mrsl painting_date_arms_behind_head_lindsey_blush with Dissolve(.5)
      lindsey "Yes, this is good."
      mc "You're doing great, [mrsl]. Standing so perfectly still."
      mrsl painting_date_arms_behind_head_lindsey_blush "Thank you... I care deeply about the progress of my students."
      mc "Can you show me some more of that passion you have for your students?"
      show mrsl painting_date_arms_behind_head_lindsey_worried with dissolve2
      mrsl painting_date_arms_behind_head_lindsey_worried "Trying to capture the whole essence of your object?"
      "The object of my desires..."
      mc "Something like that."
      mrsl painting_date_arms_behind_head_lindsey_worried "Mhmmm..."
    "\"Can you turn around slightly?\"":
      mc "Can you turn around slightly?"
      mrsl painting_date_smile_lindsey_worried "Certainly."
      window hide
      show mrsl painting_date_standing_sideways_lindsey_worried with Dissolve(.5)
      window auto
      "She gives me a playful little smile, then does as she's told."
      "[lindsey] averts her eyes as [mrsl] slides her right hand along her naked body..."
      "...swirls a finger around her nipple briefly..."
      "...before continuing down along her stomach, circling her belly button.{space=-45}"
      "Then, she stops just above her pussy."
      show mrsl painting_date_standing_sideways_lindsey_skeptical with dissolve2
      lindsey "Hands on your head! No more moving about!" with vpunch
      show mrsl painting_date_sad_lindsey_skeptical with dissolve2
      "A sudden panicked order from [lindsey] makes [mrsl] freeze\nmid-motion."
      window hide
      show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
      window auto
      mrsl painting_date_arms_behind_head_lindsey_worried "As you wish..."
      "But then, she blinks away the surprise and submissively assumes the position."
      "There's something highly erotic about that short exchange."
      "Something old and backwards..."
      "Like the church punishing the sinners."
      "Old, young. Naked, clothed. Sinner, saint."
      $mrsl.lust+=1
      "A smirk plays on [mrsl]'s lips. She's turned on."
      $lindsey.lust+=1
      "And [lindsey]... well, she tries to block it all out."
    "?mrsl.lust>=5@[mrsl.lust]/5|{image=mrsl contact_icon}|{image=stats lust_3}|\"Can you lift your\nleg up a bit?\"":
      mc "Can you lift your leg up a bit?"
      show mrsl painting_date_flirty_lindsey_skeptical with Dissolve(.5)
      lindsey concerned "[mc]..."
      window hide
      show mrsl painting_date_right_leg_up_lindsey_embarrassed with Dissolve(.5)
      window auto
      "With uncanny grace, [mrsl] throws her leg up ballerina-style, exposing her bare pussy."
      "She cocks her head to the side, eyes bright and questioning."
      mc "Yes... just like that..."
      lindsey horrified "[mc]!"
      mc "Don't worry. It's fine."
      "I hold my paintbrush up and measure her shapely proportions, then give a nod of approval."
      $mrsl.lust+=2
      mrsl painting_date_right_leg_up_lindsey_embarrassed "How... intrepid of you, [mc]."
      mc "All in the name of art."
      mrsl painting_date_right_leg_up_lindsey_embarrassed "Mhmmm, of course."
      $lindsey.love-=1
      lindsey "Oh, my god... I don't know about any of this..."
      "[lindsey]'s fidgety embarrassment is kind of cute, but my eyes are on [mrsl]."
      "In my mind, she spreads her pussy with her fingers, pushing the lips apart and exposing her dripping pink flower."
      "Goddamn, this is why art exists in the mind of the painter! How else can one possibly capture the beauty of such a thing?"
      show mrsl painting_date_right_leg_up_lindsey_worried with Dissolve(.5)
      lindsey "Can we try a different pose, please?"
      mc "Fine, fine, fine."
      window hide
      show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
      window auto
      mrsl painting_date_arms_behind_head_lindsey_worried "How do you like this one, [lindsey]?"
      show mrsl painting_date_arms_behind_head_lindsey_blush with Dissolve(.5)
      lindsey "This is better."
  if renpy.showing("mrsl painting_date_arms_behind_head_lindsey_blush"):
    mrsl painting_date_arms_behind_head_lindsey_blush "What about you, [mc]? Are you getting what you need?"
    "I'm getting what I need and then some..."
    mc "You're such a cooperative model, [mrsl]."
    mrsl painting_date_arms_behind_head_lindsey_blush "All in the name of good grades, isn't that right?"
    mc "I take my education very seriously."
    mrsl painting_date_arms_behind_head_lindsey_blush "Oh, I know you do."
    show mrsl painting_date_arms_behind_head_lindsey_worried with Dissolve(.5)
  else:
    mrsl painting_date_arms_behind_head_lindsey_worried "How's that? Are you getting what you need?"
    "I'm getting what I need and then some..."
    mc "You're such a cooperative model, [mrsl]."
    mrsl painting_date_arms_behind_head_lindsey_worried "All in the name of good grades, isn't that right?"
    mc "I take my education very seriously."
    mrsl painting_date_arms_behind_head_lindsey_worried "Oh, I know you do."
  lindsey "Are you sure this is... appropriate?"
  mc "Why not? It's art. Just paint her like the French."
  lindsey "Okay..."
  show mrsl painting_date_arms_behind_head with dissolve2
  "We paint in silence for a while — [lindsey] red as a tomato, at first, but then getting more into it."
  show mrsl painting_date_arms_behind_head_canvas_head with dissolve2
  mc "That's perfect, [mrsl]. Stay just like that."
  mrsl "As you wish... you are the artist, after all."
  show mrsl painting_date_arms_behind_head_canvas_arms with dissolve2
  mc "Exactly right..."
  "Once again, I take her measure with my paintbrush."
  show mrsl painting_date_arms_behind_head_canvas_torso with dissolve2
  "I hold it up in front of my face, squint my eyes, and focus on getting her perfect proportions just right."
  "Her firm, round ass... her legs spread so wide her pussy glistens in the sun... her lips swollen and wet..."
  show mrsl painting_date_arms_behind_head_canvas_full with dissolve2
  "The imitation on the canvas can hardly do justice to what's in front of me."
  show mrsl painting_date_arms_behind_head_canvas_full_lindsey_blush with Dissolve(.5)
  lindsey "I think I'm done..."
  mc "It looks great!"
  window hide
  show mrsl painting_date_smile_canvas_full_lindsey_blush with Dissolve(.5)
  window auto
  mrsl painting_date_smile_canvas_full_lindsey_blush "You two are quite the artists. I must say I'm very impressed."
  mc "Thank you!"
  lindsey "Thanks..."
  $mrsl.lust+=3
  mrsl painting_date_flirty_canvas_full_lindsey_blush "Let me know if I can be of further use to your... academic careers... in the future."
  $mc.lust+=1
  mc "Oh, we will."
  $unlock_replay("mrsl_modeling")
  window hide
  show black onlayer screens zorder 100
  show lindsey thinking at center
  with Dissolve(.5)
  pause 2.0
  hide mrsl
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  lindsey thinking "I hope we get good grades now..."
  mc "Did you hate it?"
  lindsey thinking "It wasn't what I had in mind, but it also wasn't horrible."
  mc "Maybe next time we'll go on a date just the two of us."
  lindsey flirty "We can certainly hang out, at least!"
  mc "No date? I'm wounded."
  lindsey laughing "Who knows what the future holds!"
  mc "Good enough for me."
  lindsey skeptical "Huh?"
  lindsey skeptical_phone "..."

label quest_lindsey_motive_date_ending:
  lindsey skeptical_phone "Sorry, I have to go... I have to, uh..."
  lindsey skeptical_phone "..."
  mc "..."
  lindsey skeptical_phone "..."
  "[isabelle] was right, that phone really does consume all of [lindsey]'s attention."
  "Hopefully, the person she's texting is grateful for it."
  "..."
  "Wait a minute..."
  "[isabelle] said she didn't see anyone with [lindsey] that day, but she was certainly texting someone."
  mc "Who are you texting, [lindsey]?"
  lindsey skeptical_phone "..."
  mc "[lindsey]?"
  lindsey laughing "Oh, sorry! Just a fan!"
  lindsey laughing "I have to go to practice now! See you later!"
  show lindsey laughing at disappear_to_right
  "A fan?"
  "If I can get a hold of her phone, maybe I can figure out who made her steal the box of chocolates..."
  "I think she keeps it in her locker during practice."
  if not quest.lindsey_motive["mrsl_model"]:
    "It's also concerning that [lindsey] blanked out again."
    "She could've drowned."
    "If [maxine] is truly behind it, she needs to be stopped."
    $quest.lindsey_motive["stop_maxine"] = True
    $game.notify_modal(None,"Coming soon","{image=maxine contact_icon}{space=25}|"+"The Juice Apocalypse\nis currently being worked on!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image=items monkey_wrench}",5.0)
#   $quest.maxine_experiments.start() ## Placeholder ## The Juice Apocalypse ##
  $quest.lindsey_motive.advance("phone")
  return

label quest_lindsey_motive_phone:
  "One simply does not waltz into the women's bathroom in broad daylight. It's always crowded at this time."
  "Getting a hold of [lindsey]'s phone will require a special kind of tango..."
  "I need to get the girls out, somehow."
  if quest.lindsey_motive == "phone":
    $quest.lindsey_motive.advance("distraction")
  return

label quest_lindsey_motive_distraction:
  "Hmm... maybe I could remove a fuse to blow out the bathroom lights..."
  "That would surely clear it of girls."
  "But there's just one tiny problem — the security camera."
  $quest.lindsey_motive["security_camera"] = True
  return

label quest_lindsey_motive_distraction_camera:
  "A motion sensor causes the camera to track movement inside\nthis hall."
  "It would only face away from the fuse box if there were movement on the left side of the hallway..."
  $quest.lindsey_motive["movement"] = True
  return

label quest_lindsey_motive_distraction_window:
  "If I could open this window and let the butterflies in, the security camera would definitely focus on them."
  "Only [jo] and the [guard] have keys to this window, though."
  if quest.lindsey_motive == "distraction":
    $quest.lindsey_motive.advance("keys")
  return

label quest_lindsey_motive_keys_jo:
  show jo skeptical with Dissolve(.5)
  jo skeptical "Are you wearing yesterday's shirt, [mc]?"
  mc "Of course not!"
  jo neutral "I could have sworn I saw you wearing it..."
  "You did, but it's not yesterday's shirt."
  jo smile "Anyway, what's on your mind, honey?"
  mc "Is it okay to open the window upstairs?"
  jo smile "Why do you want it opened?"
  show jo smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"[lindsey] keeps tripping, and I think\nit's due to the heat.\"":
      show jo smile at move_to(.5)
      $mc.charisma+=1
      mc "[lindsey] keeps tripping, and I think it's due to the heat."
      $jo.love+=1
      jo excited "Aw, it's sweet of you to think of her."
    "\"I would like to examine the butterflies there for biology class.\"":
      show jo smile at move_to(.5)
      $mc.intellect+=1
      mc "I would like to examine the butterflies there for biology class."
      $jo.love+=1
      jo excited "I'm so glad you're finally taking your schoolwork seriously."
  jo excited "All right, I'll open the window."
  mc "Thanks, [jo]!"
  $jo["at_none_now"] = True
  window hide
  show jo excited at disappear_to_right
  pause 1.0
  window auto
  $quest.lindsey_motive.advance("window_open")
  return

label quest_lindsey_motive_keys_guard:
  mc "Hey, would you mind opening the window upstairs?"
  guard "What for?"
  menu(side="middle"):
    extend ""
    "\"To let in some fresh air.\"":
      mc "To let in some fresh air."
      guard "Nah."
      mc "Seriously?"
      guard "Scram."
      "I hate that guy so much..."
      $quest.lindsey_motive["scram"] = True
    "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|\"The windows act as a greenhouse{space=-10}\nfor the hall upstairs.\"":
      mc "The windows act as a greenhouse for the hall upstairs."
      guard "So?"
      mc "The glass lets sunlight in, but the heat has nowhere to go."
      mc "So, throughout the day, the hallway gets increasingly hot."
      guard "I don't see how that concerns me."
      mc "Well, the heat can't escape because of the broken vent."
      mc "And I know that you don't want to spend a day fixing it."
      mc "But when students start complaining about the heat, you'll have no choice."
      guard "..."
      guard "Fine, I'll open the damn window."
      $quest.lindsey_motive.advance("window_open")
  return

label quest_lindsey_motive_window_open:
  "The window is open, but the butterflies aren't budging..."
  "Maybe I need something to lure them in?"
  if quest.lindsey_motive == "window_open":
    $quest.lindsey_motive.advance("lure")
  return

label quest_lindsey_motive_lure_ground_floor_west_plant_interact:
  if quest.lindsey_motive["no_touch"]:
    if not maxine.at("school_ground_floor_west","spying"):
      $quest.lindsey_motive["maxine_watching"] = True
      pause 0.75
    show maxine skeptical with Dissolve(.5)
    maxine skeptical "What did I tell you about touching the specimen?"
    maxine skeptical "You keep your hands to yourself, [mc]."
    window hide
    hide maxine with Dissolve(.5)
    if not maxine.at("school_ground_floor_west","spying"):
      pause 0.25
    $quest.lindsey_motive["maxine_watching"] = False
    window auto
  else:
    "Hmm... maybe this plant could lure the butterflies inside and—"
    window hide
    if not maxine.at("school_ground_floor_west","spying"):
      $quest.lindsey_motive["maxine_watching"] = True
      pause 0.75
    show maxine skeptical with Dissolve(.5)
    window auto
    maxine skeptical "What do you think you're doing?"
    mc "Uh... nothing..."
    maxine skeptical "It looked like you were about to touch the specimen."
    mc "You mean the plant?"
    maxine skeptical "Do not touch the specimen. I'll be watching."
    window hide
    hide maxine with Dissolve(.5)
    if not maxine.at("school_ground_floor_west","spying"):
      pause 0.25
    $quest.lindsey_motive["maxine_watching"] = False
    $quest.lindsey_motive["no_touch"] = True
    window auto
  return

label quest_lindsey_motive_lure_nurse_room_plant_interact:
  "The [nurse]'s prized bonsai. I should probably find a less expensive plant."
  return

label quest_lindsey_motive_lure_roof_landing_plant_interact:
  "This plant doesn't bloom until spring. I need to find another."
  return

label quest_lindsey_motive_lure_roof_plants_interact:
  "These plants have a distinct lack of insectoid customers on them..."
  "Maybe they're vegan or something."
  return

label quest_lindsey_motive_lure_first_hall_plant_interact:
  "This plant is glued to the floor."
  "Too many people tripped over it while trying to get downstairs."
  return

label quest_lindsey_motive_lure_first_hall_west_plant_interact:
  "Smells fresh like a summer breeze."
  return

label quest_lindsey_motive_lure_first_hall_west_plant_take:
  "I bet the butterflies will love this plant."
  if school_first_hall_west["dollar2_spawned_today"] == True and not school_first_hall_west["dollar2_taken_today"]:
    $school_first_hall_west["dollar2_taken_today"] = True
    $mc.money+=20
  $school_first_hall_west["plant_taken"] = True
  $mc.add_item("plant")
  return

label quest_lindsey_motive_lure_english_class_plant_interact:
  "Perfect for bat-pollination. Butterflies, not so much."
  return

label quest_lindsey_motive_lure_window(item):
  if item == "plant":
    $quest.lindsey_motive.advance("power_cut")
    $mc.remove_item("plant")
    mc "Come here, little insects!"
    mc "Smells good, doesn't it?"
    window hide
    pause 0.25
    $school_first_hall["butterflies_inside"] = True
    pause 0.5
    window auto
    "Perfection."
    "That should keep big brother occupied while I cut the power to the bathroom..."
  else:
    "While my [item.title_lower] would be perfect for attracting the ladies, these butterflies are a bit more picky."
    $quest.lindsey_motive.failed_item("butterfly_attractor",item)
  return

label quest_lindsey_motive_power_cut_camera:
  "Let the eye grow dim and the heart grow bold."
  return

label quest_lindsey_motive_power_cut:
  "All right, let's just unscrew the fuse to the women's bathroom..."
  "..."
  "There!"
  kate "Seriously?! The lights in the bathroom went out again!" with vpunch
  kate "Can someone get the janitor or something?!"
  "Okay, I better hurry."
  $quest.lindsey_motive.advance("bathroom")
  return

label quest_lindsey_motive_bathroom:
  "Looks like the coast is clear."
  $quest.lindsey_motive.advance("steal")
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_bathroom"
  $mc["focus"] = "lindsey_motive"
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_lindsey_motive_steal:
  pause 0.25
  "Hmm... it's a bit hard to see in the dark, but [lindsey]'s bag should be in one of the lockers..."
  return

label quest_lindsey_motive_steal_door:
  "I can't leave before I've found [lindsey]'s phone. This might be my only shot."
  return

label quest_lindsey_motive_steal_urinal:
  "Why is there fresh urine here? Does one of the girls pee standing up?{space=-30}"
  return

label quest_lindsey_motive_steal_shower1:
  "The men's bathroom doesn't have showers..."
  "What is this blatant discrimination?"
  return

label quest_lindsey_motive_steal_locker1:
  "If there's a sudden apocalypse of baseball-tossing zombies, this could come in handy..."
  $school_bathroom["baseball_bat_taken"] = True
  $mc.add_item("baseball_bat")
  return

label quest_lindsey_motive_steal_locker2:
  if quest.lindsey_motive == "steal":
    "Ah, there it is — [lindsey]'s bag."
    "Let's see here..."
    "Shirts... shoes..."
    "Oh, a bra!"
    "Aha! Found it!"
    "Wow, even her phone is adorable."
    "And no lock? She's so trusting!"
    "Hmm... let's see here..."
    "..."
    "Damn. She has over two hundred contacts..."
    "Imagine being that popular."
    "Chad, [kate], Tanner, [stacy]... I can't believe she's friends with them..."
    "Messages... messages..."
    "Well, she doesn't text a lot, so that's helpful."
    "I guess she's more of a face-to-face type of person."
    "Let's scroll down to the day she stole the box..."
    "..."
    "Some messages from her dad..."
    "A bunch from her coach..."
    "A party invitation from [kate]... I'm jealous."
    "Hmm... here's something from someone called Mer."
    python:
      for who,message,timestamp in mc.phone_message_history["hidden_number"]:
        if "You have the chocolate box?" in message:
          break_in_day = timestamp[0] - 1
      game.ui.can_close_phone = False
      game.pc = "lindsey"
      lindsey.add_stat("energy",mc.energy,0)
      lindsey.install_phone_app("messages",True)
      lindsey.add_message_to_history("mer", mer, "Hello!", day=1, hour=19)
      lindsey.add_message_to_history("mer", mer, "I am a big fan. I just wanted to wish you good luck in your competition tomorrow!", day=1, hour=19)
      lindsey.add_message_to_history("mer", lindsey, "Aw, thanks!", day=1, hour=19)
      lindsey.add_message_to_history("mer", lindsey, "But who are you and how did you get my number?", day=1, hour=19)
      lindsey.add_message_to_history("mer", mer, "You can call me Mer! I got it from one of your teammates.", day=1, hour=19)
      lindsey.add_message_to_history("mer", lindsey, "Ah, okay! Nice to meet you, Mer!", day=1, hour=20)
      lindsey.add_message_to_history("mer", lindsey, "Are you coming out to watch me run tomorrow?", day=1, hour=20)
      lindsey.add_message_to_history("mer", mer, "Unfortunately not.", day=1, hour=22)
      lindsey.add_message_to_history("mer", lindsey, "Okay! Well, I hope you have a lovely night.", day=1, hour=22)
      lindsey.add_message_to_history("mer", mer, "Hi, [lindsey]!", day=break_in_day, hour=8)
      lindsey.add_message_to_history("mer", lindsey, "Hello.", day=break_in_day, hour=8)
      lindsey.add_message_to_history("mer", mer, "Are you okay?", day=break_in_day, hour=8)
      lindsey.add_message_to_history("mer", lindsey, "I don't know.", day=break_in_day, hour=8)
      lindsey.add_message_to_history("mer", mer, "Go to [isabelle]'s locker and take the heart-shaped box. Have a little treat and share it with your team. Maybe you'll feel better.", day=break_in_day, hour=8)
      lindsey.add_message_to_history("mer", lindsey, "Okay.", day=break_in_day, hour=8)
      lindsey.add_message_to_history("mer", mer, "You were fabulous today! I cheered you on from the bleachers!", day=game.day-2*(game.day-break_in_day)/3, hour=16)
      lindsey.add_message_to_history("mer", mer, "You race with your heart, I can see it.", day=game.day-2*(game.day-break_in_day)/3, hour=16)
      lindsey.add_message_to_history("mer", lindsey, "Thank you so much!", day=game.day-2*(game.day-break_in_day)/3, hour=19)
      lindsey.add_message_to_history("mer", mer, "Good job beating your personal best today! Your dedication is inspiring.", day=game.day-(game.day-break_in_day)/3, hour=16)
      if quest.lindsey_motive["mrsl_model"]:
        lindsey.add_message_to_history("mer", mer, "I saw you in the park earlier.", hour=game.hour-3)
        lindsey.add_message_to_history("mer", mer, "I cannot believe you painted a nude model! That is lewd if you ask me.", hour=game.hour-3)
        lindsey.add_message_to_history("mer", lindsey, "Right??? I was super uncomfortable!!", hour=game.hour-2)
      else:
        lindsey.add_message_to_history("mer", mer, "I saw you painting in the park earlier. You and that boy looked cute together.", hour=game.hour-3)
        lindsey.add_message_to_history("mer", lindsey, "Aw, thank you. I like him a lot!", hour=game.hour-2)
    while True:
      window hide
      show screen phone("messages",return_to=None)
      pause
      window auto
      "I need to get to the bottom of this... or top, if we're talking about a text convo."
    label quest_lindsey_motive_steal_locker2_out_of_loop:
    $quest.lindsey_motive["out_of_loop"] = True
    $_rollback = False
    window auto
    "Well, there it is. I knew [lindsey] was innocent!{nw}"
    $_rollback = True
    extend ""
    "Mer told her to do it... but who the hell is Mer?"
    "I've never heard of anyone called that..."
    "They seem like a wholesome stalker of some kind."
    "Although, I guess that's what a fan is..."
    "[lindsey]'s probably used to it."
    "Hmm... maybe I can send them a message to lure them out?"
    "I need to find out who they are."
    window hide
    $set_dialog_mode("phone_message_centered","mer")
    lindsey "Hey, Mer! You've been really supportive lately and I'd like to give you something in return.{nw}"
    lindsey "Meet me in the homeroom after school?{nw}"
    pause 0.25
    window auto
    $set_dialog_mode("phone_message_plus_textbox")
    "Perfect! Okay, let's catch this person."
    $mc["focus"] = ""
    window hide
    hide screen phone
    pause 0.5
    window auto
    python:
      quest.lindsey_motive.advance("hide")
      game.ui.can_close_phone = True
      game.pc = "mc"
      game.location = "school_bathroom"
      lindsey.remove_stat("energy")
  else:
    "That's enough snooping. I need to find out who Mer is."
  return

label quest_lindsey_motive_steal_locker3:
  if quest.lindsey_motive["grave_mistake"]:
    "Hmm... no more mistakes were made here..."
    "...unless you count me looking inside the locker again despite knowing fully well that it's empty."
  else:
    "Oh, someone made a grave mistake here!"
    $quest.lindsey_motive["grave_mistake"] = random.choice(["apple","banana_milk","cake_slice","empty_bottle","garlic","lollipop","onion_slice","pepelepsi","salted_cola","seven_hp","spray_empty_bottle","strawberry_juice","tide_pods","tissues","water_bottle"])
    $item = find_item(quest.lindsey_motive["grave_mistake"])
    if getattr(item,"bottled",False) and (mc.owned_item("empty_bottle") or mc.owned_item("spray_empty_bottle")):
      $bottle = mc.owned_item(("empty_bottle","spray_empty_bottle"))
      if bottle.startswith("spray_"):
        $quest.lindsey_motive["grave_mistake"] = "spray_" + (quest.lindsey_motive["grave_mistake"].strip("_bottle") if quest.lindsey_motive["grave_mistake"] == "water_bottle" else quest.lindsey_motive["grave_mistake"])
      "It's some kind of... fluid? No harm in bottling it up, I guess..."
      $mc.remove_item(bottle)
    $mc.add_item(quest.lindsey_motive["grave_mistake"])
  return

label quest_lindsey_motive_hide_advance:
  if game.hour == 18:
    "Just one hour until the end of the school day..."
  elif game.hour == 19:
    "School day is about to end..."
  "I should probably get my ass over to the homeroom now."
  jump goto_school_homeroom

label quest_lindsey_motive_hide:
  "All right, I don't know who Mer is, but I'm going to find out."
  "Just need to find a good hiding spot... I don't feel like getting beat up if it's Chad or some other muscle jock."
  $mc["focus"] = "lindsey_motive"
  return

label quest_lindsey_motive_hide_door:
  "As much as I'd like to head over to the cafeteria for a quick strawberry juice on the rocks, now is not the time."
  return

label quest_lindsey_motive_hide_rope:
  "One of these days, I'll use that rope to tie up [maxine]. But that has to wait."
  return

label quest_lindsey_motive_hide_teacher_desk:
  "Hmm..."
  "Maybe I could squeeze into the space under the desk?"
  window hide
  show black onlayer screens zorder 100
  show mrsl under_desk
  with Dissolve(.5)
  while game.hour != 19:
    $game.advance()
  $mc["focus"] = ""
  pause 2.0
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Fuck, this is tighter than [lindsey]'s butt..."
  "At least, I'm somewhat hidden."
  pause 0.25
  hide black onlayer screens with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Ugh, my knees and back hurt already."
  "The things I do in search of the truth..."
  "..."
  "..."
  "..."
  "Man, it's getting late."
  "The guard is probably going to start locking up soon..."
  window hide
  play sound "<from 2.7 to 3.3>door_breaking_in"
  pause 0.75
  play sound "<from 0.4>bedroom_door"
  pause 0.75
  window auto
  "Oh, shit! Someone's coming!"
  play sound "<to 2.2>high_heels"
  "..."
  "Wait, why are they coming over here?"
  "Are my feet sticking out or something?"
  "Fuck, they're right by the desk—"
  window hide
  show mrsl under_desk_closed_legs with Dissolve(.5)
  window auto
  "Crap, crap, crap! Not good! Not good at all!"
  "What is [mrsl] doing here?!"
  "God, her legs smell so good... like cigarettes and sex..."
  "..."
  "Wait, could it be that [mrsl] is Mer?"
  "Surely not, right?"
  "Why would she make [lindsey] steal the chocolate hearts? That makes zero sense."
  "Mer must be someone who would want to hurt [isabelle]. Nothing else really makes sense."
  window hide
  show mrsl under_desk_half_open_legs with Dissolve(.5)
  window auto
  "Whoa!"
  "You naughty bitch, [mrsl]! Where are your panties?"
  "God, her pussy is like a work of art. And that piercing... fuck!"
  "The things I would do to that pussy..."
  window hide
  play sound "<from 1.7 to 2.4>door_knock"
  pause 0.75
  play sound "<from 0.4>bedroom_door"
  pause 0.75
  show mrsl under_desk_closed_legs with Dissolve(.5)
  window auto
  mrsl under_desk_closed_legs "What do {i}you{/} want?"
  "???" "Why are you here?"
  "Huh? Who is that?"
  "I feel like I've heard her voice before."
  mrsl under_desk_closed_legs "Have you lost your last marbles, you crazy old bitch?"
  mrsl under_desk_closed_legs "I'm the homeroom teacher. This is the homeroom."
  "???" "Watch your mouth, you fiend."
  "I can't believe what I'm hearing..."
  "This is an entirely different side of [mrsl]."
  mrsl under_desk_closed_legs "You never set foot inside my classroom. Why now?"
  "???" "..."
  "???" "That does not concern you."
  "From the sound of it, this person is Mer... and they expected to find [lindsey] here, not [mrsl]."
  mrsl under_desk_closed_legs "Oh, secrets! Now, that's exciting!"
  mrsl under_desk_closed_legs "You're not breaking the rules, are you?"
  "???" "That is rich coming from you."
  mrsl under_desk_closed_legs "I don't know what you're talking about."
  window hide
  show mrsl under_desk_half_open_legs with Dissolve(.5)
  window auto
  "Oh, god."
  "Her pussy is so wet it glistens."
  "Why is she so aroused all of a sudden?"
  "Her pussy is inches away from my face... I can almost taste it..."
  menu(side="middle"):
    extend ""
    "Have a taste":
      "[mrsl] is in the middle of a serious argument with someone she hates..."
      "There is no way she would admit that there's a student under her desk, eating her pussy."
      $mc.lust+=1
      "Right...?"
      window hide
      show mrsl under_desk_tongue6 with Dissolve(.5)
      window auto
      mrsl under_desk_tongue6 "..."
      "???" "What now?"
      mrsl under_desk_tongue6 "Oh... nothing..."
      mrsl under_desk_tongue6 "I was just... thinking about how ridiculous you look..."
      "???" "You really need to take a look in the mirror."
      "She reacted, but didn't say anything..."
      "That's practically an invitation, right?"
      window hide
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue6 with Dissolve(.05)
      window auto
      mrsl "Oooh!"
      "God, [mrsl] tastes like the seven deadly sins."
      "And she's already wetter than [flora] at the squid aquarium."
      mrsl "Mmm... society is perfectly fine with how I dress... and so is the school board..."
      if quest.lindsey_motive["mrsl_model"]:
        "???" "What about your naked escapade earlier? That must be against\nthe rules."
        mrsl "Would you really deprive these young minds... mmm... of their education in art and anatomy...?"
        "???" "Debauchery! That is what it is!"
        window hide
        show mrsl under_desk_tongue1 with Dissolve(.05)
        show mrsl under_desk_tongue2 with Dissolve(.05)
        show mrsl under_desk_tongue3 with Dissolve(.05)
        show mrsl under_desk_tongue4 with Dissolve(.05)
        show mrsl under_desk_tongue5 with Dissolve(.05)
        pause 0.1
        show mrsl under_desk_tongue1 with Dissolve(.05)
        show mrsl under_desk_tongue2 with Dissolve(.05)
        show mrsl under_desk_tongue3 with Dissolve(.05)
        show mrsl under_desk_tongue4 with Dissolve(.05)
        show mrsl under_desk_tongue5 with Dissolve(.05)
        show mrsl under_desk_tongue6 with Dissolve(.05)
        window auto
        mrsl "Mmmhmm..."
        mrsl "Well, it's part of the curriculum..."
      "???" "Outrageous."
      mrsl "No... what is outrageous... mmm... is what you've been up to lately..."
      mrsl "It doesn't take a genius to figure out... that you're stirring the pot..."
      window hide
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue6 with Dissolve(.05)
      window auto
      mrsl "{i}Gasp!{/}" with vpunch
      "Oh, I hit the spot."
      mrsl "What I don't understand... is why you're doing me a favor..."
      mrsl "Could it be that you're... mmm... trying to lose...? Are you secretly a masochist...?"
      window hide
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      pause 0.15
      show mrsl under_desk_tongue1 with Dissolve(.05)
      show mrsl under_desk_tongue2 with Dissolve(.05)
      show mrsl under_desk_tongue3 with Dissolve(.05)
      show mrsl under_desk_tongue4 with Dissolve(.05)
      show mrsl under_desk_tongue5 with Dissolve(.05)
      show mrsl under_desk_tongue6 with Dissolve(.05)
      window auto
      mrsl "Hnnngh..."
      "???" "Do not look at me like that."
      $quest.lindsey_motive["taste"] = True
    "Don't be stupid":
      $mc.love+=3
      "Oh, well. You can't just lick someone's pussy and not get caught."
      mrsl "Society is perfectly fine with how I dress, and so is the school board.{space=-20}"
      if quest.lindsey_motive["mrsl_model"]:
        "???" "What about your naked escapade earlier? That must be against\nthe rules."
        mrsl "Would you really deprive these young minds of their education in art{space=-10}\nand anatomy?"
        "???" "Debauchery! That is what it is!"
        mrsl "Well, it's part of the curriculum!"
      "???" "Outrageous."
      mrsl "No, what is outrageous is what you've been up to lately."
      mrsl "It doesn't take a genius to figure out that you're stirring the pot."
      mrsl "What I don't understand is why you're doing me a favor."
      mrsl "Could it be that you're trying to lose? Are you secretly a masochist?{space=-5}"
  "???" "I admit, things did get out of hand."
  if quest.isabelle_stolen["lindsey_slaped"]:
    "???" "That slap should never have happened."
    if quest.lindsey_motive["taste"]:
      mrsl "Oh... but it was inevitable..."
    else:
      mrsl "Oh, but it was inevitable!"
  if quest.lindsey_motive["taste"]:
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "Mmmm..."
    mrsl "What exactly was your plan...? You know you can't... mmm.... influence the boy..."
  else:
    mrsl "What exactly was your plan? You know you can't influence the boy."
  "...the boy?"
  if quest.isabelle_locker.finished:
    if quest.isabelle_stolen["lindsey_slaped"]:
      "???" "Oh, please."
      "???" "Apart from the slap, and the fact that someone stole the girl's entire locker, it turned out quite well."
    else:
      "???" "Oh, please."
      "???" "Apart from the fact that someone stole the girl's entire locker,\nit turned out quite well."
  elif quest.isabelle_stolen["lindsey_slaped"]:
    "???" "Oh, please."
    "???" "Apart from the slap, it turned out quite well."
  else:
    "???" "Oh, please. It turned out quite well."
  "???" "Someone steals a precious item from the girl. The boy successfully plays the hero, and they fall in love."
  "???" "Beautiful, isn't it?"
  "Wait, is she talking about me and [isabelle]?"
  mrsl "..."
  mrsl under_desk_closed_legs "You cheating bitch!"
  "???" "Absolutely untrue. The rules specifically say, no {i}direct{/} influence."
  "???" "The boy made the choice to help the girl. He could've stared at your oversized chest all day, but he did not."
  mrsl under_desk_closed_legs "Oh, is this how you want to play it? By all means."
  mrsl under_desk_closed_legs "Now get the fuck out of my classroom!"
  "???" "Oh, stop pouting. It's going to ruin your lipstick."
  window hide
  play sound "<from 8.2 to 10>door_breaking_in" volume 0.5
  pause 1.5
  window auto
  "Seems like they left..."
  "What the hell is going on? Did this person really make [lindsey] steal [isabelle]'s chocolate hearts, knowing that I'd help her?"
  "That is so messed up."
  "But also thanks, I guess? It really did help me get closer to [isabelle]."
  "I just don't understand why..."
  "What's the motive in all of—"
  if quest.lindsey_motive["taste"]:
    window hide
    show mrsl under_desk_open_legs with Dissolve(.5)
    window auto
    "God, I was so caught up in the mystery that I almost forgot that there's a gorgeous pussy just a few inches away from my face..."
    "Does [mrsl] want me to finish what I started?"
    "..."
    "Don't mind if I do..."
    window hide
    show mrsl under_desk_tongue6 with Dissolve(.5)
    pause 0.15
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "Mmmm! Yes!"
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.1
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.15
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "F-fuck!"
    "You have a filthy mouth, [mrsl]."
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.1
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.05
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    pause 0.15
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue6 with Dissolve(.05)
    window auto
    mrsl "Fuck! Fuck! Fuck!"
    window hide
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_tongue1 with Dissolve(.05)
    show mrsl under_desk_tongue2 with Dissolve(.05)
    show mrsl under_desk_tongue3 with Dissolve(.05)
    show mrsl under_desk_tongue4 with Dissolve(.05)
    show mrsl under_desk_tongue5 with Dissolve(.05)
    show mrsl under_desk_squirt1 with vpunch
    window auto
    mrsl under_desk_squirt1 "Ohhh!"
    show mrsl under_desk_squirt2 with dissolve2
    "Damn, I actually made her climax!"
    "I'll add that to my CV."
    mrsl under_desk_squirt2 "..."
  window hide
  show mrsl under_desk_look with Dissolve(.5)
  window auto
  mrsl under_desk_look "This never happened."
  window hide
  show mrsl under_desk_closed_legs with Dissolve(.5)
  pause 0.25
  show mrsl under_desk with Dissolve(.5)
  play sound "<to 2.2>high_heels"
  pause 1.5
  play sound "<from 8.2 to 10>door_breaking_in" volume 0.5
  pause 1.5
  window auto
  "Huh..."
  "She just got up and left."
  if quest.lindsey_motive["taste"]:
    "I thought for sure she'd send me to the gulags for that..."
    "I don't know what's more bizarre — [mrsl] letting me eat her pussy or that weird conversation."
  "I don't know what to make of this..."
  "What a strange day..."
  window hide
  $unlock_replay("mrsl_argument")
  $quest.lindsey_motive.finish()
  hide mrsl with Dissolve(.5)
  window auto
  return
