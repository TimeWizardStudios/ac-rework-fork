label quest_lindsey_book_start:
  show lindsey thinking with Dissolve(.5)
  mc "Hey, [lindsey]! Your, err... your hair looks nice today!"
  "Crap. That wasn't particularly smooth."
  if lindsey["romance_disabled"]:
    lindsey cringe "Sorry, but I don't want to talk to you."
    mc "I came to apologize..."
    lindsey angry "That's just not going to cut it."
    lindsey angry "You treated me like shit!"
    show lindsey angry at move_to(.25)
    menu(side="right"):
      extend ""
      "\"Well, suck it up, princess. Life's not a bed of roses.\"":
        show lindsey angry at move_to(.5)
        mc "Well, suck it up, princess. Life's not a bed of roses."
        $lindsey.love-=3
        $lindsey.lust-=2
        lindsey cringe "If you talk to me again, I'll report you to the principal."
        hide lindsey with Dissolve(.5)
        "I can't believe it. So ungrateful! Well, if that's how she wants it to be..."
        #(can be restarted if lindsey Romance is re-enabled) #tbd
        $quest.lindsey_book.start(silent=True)
        $quest.lindsey_book.fail()
      "\"I really am sorry! Is there anything I can do to make it up to you?\"":
        show lindsey angry at move_to(.5)
        mc "I really am sorry! Is there anything I can do to make it up to you?"
        lindsey annoyed "..."
        lindsey annoyed "I don't know what that would be..."
        mc "Anything! I'd really like to fix this."
        lindsey sad "Well, it's really not that simple."
        mc "If you give me another chance, I promise you I'll show you my good side."
        lindsey eyeroll "Okay, fine... but don't expect it to be easy."
        mc "Thank you! I'll do my best!"
        "I wonder what I can do to win her trust back."
        "Hmm..."
        hide lindsey with Dissolve(.5)
        if game.quest_guide == "lindsey_book":
          $game.quest_guide = "lindsey_making_up"
        $quest.lindsey_making_up.start()
    return
  else:
    lindsey blush "Aw, thanks!"
    lindsey blush "That made me all warm inside."
    mc "You're welcome!"
    mc "How are you doing today?"
  label quest_lindsey_book_start_mid:
  lindsey thinking "Hmm... I've actually been feeling a bit ill the last few days. Do you think there's something going around?"
  mc "Possibly! [flora] was being snotty this morning, but that's not really unusual."
  lindsey thinking "I'm taking a break from outdoor practice for a bit to recuperate."
  lindsey thinking "My dad recommended reading more while I'm resting up, but I'm not sure where to start."
  lindsey cringe "The library is closed, so I can't go for my usual favorites."
  show lindsey cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"What do you usually read?\"":
      show lindsey cringe at move_to(.5)
      mc "What do you usually read?"
      lindsey smile "All sorts of things! But I love fairytales the most!"
      mc "So, you like the happily ever afters?"
      lindsey blush "Yes, and the knights!"
    "\"Do you know when it'll open?\"":
      show lindsey cringe at move_to(.5)
      mc "Do you know when it'll open?"
      lindsey thinking "I think it'll be closed until Christmas. They're renovating."
      "Okay, just wanted to check if traveling back in time changed that. Apparently not."
  if game.location != "school_english_class":
    mc "Well, there's always the bookshelf in the English classroom."
    lindsey laughing "You're right! I totally forgot about that."
  else:
    mc "What's wrong with the books here?"
    lindsey laughing "Oh, nothing!"
  lindsey smile "If I'm going to be completely honest, I always have a hard time picking."
  mc "I could choose one for you."
  lindsey excited "Oh, that would be cool! Secret book Santa!"
  mc "I guess!"
  lindsey neutral "Okay, I'm looking forward to seeing what you come up with!"
  hide lindsey with Dissolve(.5)
  $quest.lindsey_book.start()
  return

label quest_lindsey_book_apology_gift(item):
  if getattr(item,"consumable",False):
    $mc.remove_item(item)
    show lindsey excited with Dissolve(.5)
    lindsey excited "Thank you. That's very kind."
    lindsey neutral "It's not about the gift itself... but you knew that, right?"
    mc "I guess I did."
    lindsey excited "It's a good start."
    if getattr(item,"bottled",False):
      mc "Oh, um... can I have the bottle back, though?"
      lindsey angry "What?"
      mc "The plastic bottle... it's my only one."
      lindsey cringe "So, you're taking back the gift you literally just gave me?"
      mc "No, no! The gift is the [item.title_lower]! That's all yours... I just kinda need the bottle."
      lindsey cringe "Are you serious?"
      mc "I'm sorry, but we both know that plastic bottles have been in short supply since the Great War."
      mc "That's my only one... and I kinda really need it."
      lindsey annoyed "So, you're expecting me to chug down an entire bottle of [item.title_lower]?"
      mc "Well, that would be one of your options..."
      lindsey eyeroll "I can't believe this."
      lindsey concerned "You're lucky I'm extraordinarily parched right now."
      lindsey thinking "All right, [mc]. You can have your stupid bottle back, right after I..."
      show lindsey thinking_drinking with Dissolve(.5)
      mc "Chug! Chug! Chug! Chug!"
      $mc.add_item("empty_bottle")
    $quest.lindsey_making_up.advance("second_item")
    hide lindsey with Dissolve(.5)
  else:
    "My [item.title_lower] is probably not the number one thing on her wishlist right now."
    $quest.lindsey_making_up.failed_item("start",item)
  return

label quest_lindsey_book_apology_gift_two(item):
  if getattr(item,"consumable",False):
    $mc.remove_item(item)
    show lindsey smile with Dissolve(.5)
    lindsey smile "Hey, thanks!"
    lindsey smile "I like the gesture."
    lindsey flirty "I wanted to see if you could be kind and giving."
    mc "How did I do?"
    lindsey laughing "Well enough!"
    if getattr(item,"bottled",False):
      mc "Oh, um... can I have the bottle back, though?"
      lindsey skeptical "What?"
      mc "The plastic bottle... it's my only one."
      lindsey cringe "So, you're taking back the gift you literally just gave me?"
      mc "No, no! The gift is the [item.title_lower]! That's all yours... I just kinda need the bottle."
      lindsey cringe "Are you serious?"
      mc "I'm sorry, but we both know that plastic bottles have been in short supply since the Great War."
      mc "That's my only one... and I kinda really need it."
      lindsey annoyed "So, you're expecting me to chug down an entire bottle of [item.title_lower]?"
      mc "Well, that would be one of your options..."
      lindsey eyeroll "I can't believe this."
      lindsey concerned "You're lucky I'm extraordinarily parched right now."
      lindsey thinking "All right, [mc]. You can have your stupid bottle back, right after I..."
      show lindsey thinking_drinking with Dissolve(.5)
      mc "Chug! Chug! Chug! Chug!"
      $mc.add_item("empty_bottle")
    mc "So, you forgive me?"
    lindsey skeptical "I didn't say that."
    lindsey skeptical "This is a slow process. You could ask me how I'm doing, but you have to mean it."
    show lindsey skeptical at move_to(.75)
    menu(side="left"):
      extend ""
      "\"How are you doing, [lindsey]?\"":
        show lindsey skeptical at move_to(.5)
        mc "How are you doing, [lindsey]?"
        lindsey laughing "Okay, that was good!"
        lindsey smile "Not that hard, right?"
        mc "I guess not..."
        $lindsey["romance_disabled"] = False
        $quest.lindsey_making_up.finish()
        mc "So, how are you actually feeling?"
        if game.quest_guide:
          $game.quest_guide = "lindsey_book"
        jump quest_lindsey_book_start_mid
      "\"Quit playing games and be more grateful.\"":
        show lindsey skeptical at move_to(.5)
        mc "Quit playing games and be more grateful."
        $lindsey.love-=1
        $lindsey.lust-=1
        lindsey cringe "Seems like you haven't learned a thing..."
        lindsey cringe "Hopefully, you'll figure out how to be a decent person before everyone stops talking to you."
        hide lindsey with Dissolve(.5)
        $quest.lindsey_making_up.fail()
        $quest.lindsey_book.start(silent=True)
        $quest.lindsey_book.fail()
  else:
    "Come Christmas time, I doubt [lindsey] will stay up late next to the milk and cookies, waiting for Santa to give her my [item.title_lower]..."
    $quest.lindsey_making_up.failed_item("second_item",item)
  return

label quest_lindsey_book_suggest(item):
  if item in ("catch_thirty_four","atlas_plugged","snow_white"):
    show lindsey neutral with Dissolve(.5)
    mc "Ho-ho-ho, [lindsey]! It's me, your secret book Santa!"
    lindsey excited "Yes! What do you have for me?"
    mc "First things first — have you been good this year?"
    lindsey flirty "I've never had a detention... that should count for something, right?"
    mc "Yes, I guess it does. All right, here's what I have for you."
    if item == "catch_thirty_four":
      $mc.remove_item(item)
      $quest.lindsey_book["book_given"] = "catch_thirty_four"
      lindsey skeptical "What's this?"
      mc "A porno-political satire. It's a classic!"
      lindsey skeptical "It's not exactly what I had in mind..."
      show lindsey skeptical at move_to(.25)
      menu(side="right"):
        extend ""
        "\"It's one of [isabelle]'s favorites.\"":
          show lindsey skeptical at move_to(.5)
          $mc.intellect+=1
          $isabelle.lust+=1
          mc "It's one of [isabelle]'s favorites."
          lindsey laughing "Huh! Okay, I should give it a chance, then!"
          lindsey flirty "I'll tell her you recommended it on her behalf!"
        "\"It's not the easiest read, but I figured you could handle it.\"":
          show lindsey skeptical at move_to(.5)
          mc "It's not the easiest read, but I figured you could handle it."
          $lindsey.love+=1
          lindsey blush "Aw! Thank you!"
          lindsey blush "I'll give it a chance!"
    elif item == "atlas_plugged":
      $mc.remove_item(item)
      $quest.lindsey_book["book_given"] = "atlas_plugged"
      $lindsey.lust-=1
      lindsey cringe "Not exactly my type of thing... but I'll give it a try..."
      lindsey thinking "Thanks, I guess."
    elif item == "snow_white":
      $mc.remove_item(item)
      $quest.lindsey_book["book_given"] = "snow_white"
      lindsey blush "Oh! This sounds really interesting!"
      $lindsey.lust+=1
      lindsey blush "A revisualization of the original classic! Thank you!"
  else:
    "I'm not too sure about the readability of my [item.title_lower]."
    $quest.lindsey_book.failed_item("suggest",item)
    return
  lindsey smile "Okay, I'm going to find a quiet place to read this."
  mc "All right, let me know what you think!"
  lindsey smile "Will do! Thanks again!"
  $quest.lindsey_book["day_recieved"] = game.day
  $lindsey["at_none_now"] = True
  $process_event("update_state")
  show lindsey smile at disappear_to_left
  pause(1)
  $quest.lindsey_book.advance("feedback")
  return

label quest_lindsey_book_feedback:
  if game.day > quest.lindsey_book["day_recieved"]:
    show lindsey smile with Dissolve(.5)
    lindsey smile "There you are! I've been looking for you."
    mc "Hey, [lindsey]!"
    mc "How's the book?"
    lindsey flirty "It's very different from my expectations..."
    mc "Yeah? I figured it'd be something new for you."
    lindsey laughing "Thanks! It has actually tickled my inspiration."
    lindsey laughing "Together with the weather, it's perfect for creativity."
    mc "Happy to hear it!"
    lindsey thinking "I'd like to paint something out here, but [jacklyn] is pretty strict when it comes to borrowing art supplies."
    "Hmm... maybe this could be an opportunity?"
    "[lindsey] painting in the shadow of a tree, the summer breeze gently tugging at her hair — it's a nice picture."
    show lindsey thinking at move_to(.75)
    menu(side="left"):
      extend ""
      "\"Maybe I can help with that?\"":
        $quest.lindsey_book['option_a']=True
        show lindsey thinking at move_to(.5)
        mc "Maybe I can help with that?"
        $lindsey.lust+=1
        lindsey blush "Really?"
        mc "Yeah. I'll get you an easel."
        lindsey blush "Thank you! That would be fantastic. Those things are expensive as hell and I'm just a hobby painter!"
      "\"If I bring you an easel, will you let me watch?\"":
        show lindsey thinking at move_to(.5)
        mc "If I bring you an easel, will you let me watch?"
        lindsey flirty "I'm kinda shy about my art..."
        lindsey smile "It's not that good, and I don't like to be judged."
        show lindsey smile at move_to(.25)
        menu(side="right"):
          extend ""
          "\"Your model ship is the centerpiece in the art classroom!\"":
            show lindsey smile at move_to(.5)
            mc "Your model ship is the centerpiece in the art classroom!"
            lindsey laughing "True, but [maxine] did most of that!"
            mc "I'm sure you helped greatly! Everyone knows you're super talented!"
            $lindsey.love+=1
            lindsey blush "Thank you!"
            lindsey blush "Okay, if you can somehow get me an easel, we can paint something together!"
            mc "Cool! Looking forward to it!"
          "\"I could teach you.\"":
            show lindsey smile at move_to(.5)
            mc "I could teach you."
            lindsey laughing "I didn't know you were good at art."
            mc "I've been practicing a lot since... last year."
            lindsey smile "Okay!"
            lindsey flirty "I guess if you can get your hands on an easel, we can paint something together."
            mc "Sounds good!"
    hide lindsey with Dissolve(.5)
    "Hmm... perhaps that wasn't the smartest thing to promise. But she gets so excited, it compels me to help her."
    "There's no way I can afford a real easel, but I could make one."
    "[lindsey] is a firm believer that it's the thought that counts."
    if not mc.owned_item("makeshift_easel"):
      "Need to find some sticks, for starters..."
    $quest.lindsey_book.advance("easel")
  else:
    show lindsey thinking with Dissolve(.5)
    mc "So, [lindsey], how's the book?"
    lindsey cringe "It hasn't even been a day yet, [mc]!"
    if quest.lindsey_book["book_given"] == "atlas_plugged":
      lindsey blush "But I really admire Atlas for taking it so well... his problems weren't exactly small."
      "By \"problems,\" she means butt plugs."
    if quest.lindsey_book["book_given"] == "catch_thirty_four":
      lindsey thinking "But this book has been a major, major, major, major letdown so far..."
      mc "That sounds like three strikes against it from the beginning. Give it a fair chance."
    if quest.lindsey_book["book_given"] == "snow_white":
      lindsey thinking "But how come the princess settles for a group of below average normal guys when she's the prettiest girl in the land?"
      mc "They're all nice guys, maybe she figured they deserved a chance?"
      lindsey blush "Well, I hope there's a knight in shining armor later on to save her from the mediocrity!"
    hide lindsey with Dissolve(.5)
  return

label quest_lindsey_book_easel(item):
  if item == "makeshift_easel":
    show lindsey smile with Dissolve(.5)
    mc "Here's the easel! And I know it's not pretty, but it works."
    window hide
    $mc.remove_item("makeshift_easel")
    pause 0.25
    window auto show
    show lindsey laughing with dissolve2
    lindsey laughing "Aw! Thank you!"
    $lindsey.lust+=1
    lindsey laughing "It does look a little rickety, but maybe that's just the aesthetic?"
    mc "It's sturdier than it looks!"
    lindsey flirty "I appreciate it regardless!"
    if quest.lindsey_book['option_a']:
#     lindsey flirty "Since you did this for me, maybe we could paint something together?"
      lindsey flirty "Since you did this for me, maybe we could paint something together?{space=-30}"
      mc "Wait, really?"
      lindsey laughing "Absolutely!"
      "Huh! Perhaps it does pay off to do things for others..."
      "Her excitement is contagious."
  else:
    "I don't want her to paint my [item.title_lower]."
    "I wouldn't mind painting her face though..."
    $quest.lindsey_book.failed_item("easel_created",item)
    return
  lindsey smile "Are you free now? We could get started right away! No time like the present!"
  "[lindsey] is the type with infinite energy. It's really hard to keep up sometimes."
  mc "Yeah, we could."
  lindsey confident "Okay! Where should we put the easel?"
  mc "I don't know, right here seems good."
  "The longer we wait, the more chances she'll get to ditch me..."
  lindsey excited "Nice! I like the spontaneity."
  window hide
  show lindsey excited at move_to(.65)
  show misc makeshift_easel at Position(xalign=.35,yalign=.75) with Dissolve(.5)
  window auto
  "It feels good to be appreciated and useful to one of the hottest girls in the school."
  "Even if it's just to paint, this feels like something of a turning point."
  "As long as I don't mess up, maybe I have a chance with her?"
  "Maybe she'll let me paint her like in Titanic?"
  lindsey excited "Okay, let me just—"
  window hide
  play sound "squeaking_animal"
# show beaver beaver_bob at appear_from_left(.9999)
# show beaver beaver_bob at beaver(.9999)
  show beaver beaver_bob:
    xpos 0.0 xanchor 1.0 yalign 0.75
    easein 0.5 xalign 0.35
    easeout 0.5 xpos 1.0 xanchor 0.0
  pause 0.25
  show misc makeshift_easel:
    alpha 1.0
    easeout 0.25 alpha 0.0
  pause 0.25
  stop sound fadeout 0.5
# show lindsey afraid at move_to(.5)
  show lindsey excited:
    parallel:
      easein 0.5 xalign 0.5
    parallel:
      easein 0.175 yoffset -10
      easeout 0.175 yoffset 0
      easein 0.175 yoffset -4
      easeout 0.175 yoffset 0
  show lindsey afraid with Dissolve(.5)
  window auto
  lindsey afraid "Eeep!"
  show lindsey afraid:
    xalign 0.5 yoffset 0
  hide beaver beaver_bob
  $quest.lindsey_book.advance("beaver")
  mc "What the hell?!"
  lindsey afraid "W-was that a beaver?"
  lindsey cringe "Hey! It stole the easel!"
  show lindsey cringe at move_to(.25)
  menu(side="right"):
    extend ""
    "Stay":
      show lindsey cringe at move_to(.5)
      mc "Should we go after it?"
      lindsey thinking "I, um... I don't know."
      lindsey thinking "It probably mistook the easel for regular sticks."
      mc "Well, it's going to chew it up and use it for its dam!"
      lindsey sad "There's not much we can do..."
      "Hell, no! I'm not letting some critter ruin my painting date!"
      mc "I'm going after it."
      lindsey concerned "Be careful!"
      window hide
      hide lindsey with Dissolve(.5)
    "?mc.strength>=5@[mc.strength]/5|{image=stats str}|Chase it":
      show lindsey cringe at move_to(.5)
      "Here's my chance to impress [lindsey]!"
      window hide
      show black onlayer screens zorder 100 with Dissolve(.5)
      $quest.lindsey_book["beaver"] = 4
      pause 1.0
      hide lindsey
      hide black onlayer screens
      with Dissolve(.5)
      window auto
      "Puh, that sucker is fast! It ran into the school."
      "[lindsey] is watching, so I can't give up yet..."
  return
