image lindsey_wrong_not_romance_disabled = LiveComposite((128,128), (6,6), LiveComposite((116,116), (4,5), Transform("lindsey contact_icon_evil", size=(106,106))), (6,6), "ui crossed_out_circle2")

label quest_lindsey_wrong_start:
  show lindsey concerned with Dissolve(.5)
  lindsey concerned "..."
  lindsey concerned "Hmm..."
  lindsey thinking "..."
  lindsey thinking "Ehm..."
  "Not sure what's up with her. She seems out of it somehow."
  mc "[lindsey], are you okay?"
  lindsey afraid "W-what?"
  mc "I asked if you're okay."
  lindsey concerned "Oh."
  lindsey concerned "I think I lost a shoe."
  mc "A shoe? What kind of shoe?"
  lindsey thinking "Just one of my regular sneakers."
  mc "Have you... looked on your feet?"
  lindsey excited "Huh! That's odd!"
  mc "..."
  "What? I was just trying to be snarky."
  show lindsey excited at disappear_to_left()
  "What's wrong with her?"
  $lindsey["at_none_now"] = True
  $quest.lindsey_wrong.start()
  #$game.notify_modal(None,"Coming soon","{image= lindsey contact_icon}{space=25}|"+"Nothing Wrong With Me\nis out now on Patreon!\n\nPlease consider\nsupporting us.|{space=25}{image= items monkey_wrench}",5.0)
  return

label quest_lindsey_wrong_follow:
  show lindsey smile with Dissolve(.5)
  mc "Hey, what are you reading?"
  lindsey smile "The Blush of the Scarlet Starlet."
  lindsey smile "It's a modern-day classic!"
  mc "Isn't that a clothing brand or something?"
  lindsey excited "Yeah! The author was a designer before her hit novel. Her brand got a huge upswing from the success!"
  mc "Right. I knew I'd heard [flora] mention it at some point."
  mc "Did you find your shoe?"
  lindsey thinking "My shoe?"
  "Ugh, my jokes never seem to hit home."
  mc "Sorry, I was just trying to be funny about earlier."
  lindsey thinking "I'm so confused."
  mc "You said you lost your shoe and then found it on your foot. I was just trying to joke about that."
  lindsey thinking "Hmm... I guess I must've forgotten about it. When did that happen?"
  mc "..."
  "Can't tell if she's joking or not. Doesn't seem like her to gaslight someone for fun."
  show lindsey thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Do you think I'm stupid or something?\"":
      show lindsey thinking at move_to(.5)
      mc "Do you think I'm stupid or something?"
      lindsey skeptical "Did I say that?"
      mc "Well, you're clearly trying to mess with me."
      lindsey thinking "Mess with you how?"
      lindsey thinking "I'm not doing anything."
      mc "What about your shoe earlier?"
      lindsey neutral "I said I don't remember. You'll have to be more specific."
      "Not sure what's going on, but it's unsettling to put it lightly."
      "Did I remember it wrong?"
      "Perhaps she told me about the shoe back in the old timeline and not here..."
      mc "Sorry for bothering you."
    "?lindsey['romance_disabled']@|{image=lindsey contact_icon_evil}|\"Are you getting demented in your old age?\"":
      show lindsey thinking at move_to(.5)
      mc "Are you getting demented in your old age?"
      $lindsey.love-=1
      $lindsey.lust-=1
      lindsey annoyed "Who says things like that?!"
      mc "Did you forget my name as well?"
      lindsey angry "..."
      lindsey angry "You're not funny. Do you get that?"
      mc "You just have no humor."
      lindsey angry "This is why no one likes you. You're rude and obnoxious."
      lindsey angry "Stop bothering me!"
    "\"Seriously? We talked about it earlier!\"":
      show lindsey thinking at move_to(.5)
      mc "Seriously? We talked about it earlier!"
      lindsey skeptical "I'm pretty sure I would remember that."
      mc "I don't find this game particularly funny."
      lindsey annoyed "What are you talking about?"
      mc "Are you telling me that you actually don't remember?"
      lindsey neutral "I don't know what you're on about, honestly!"
      mc "Okay, hmm..."
      "No one's that good a liar."
      "Did I dream it or something?"
      lindsey smile "What?"
      mc "Nothing, I guess. See you around."
    "\"Are you okay?\"":
      show lindsey thinking at move_to(.5)
      mc "Are you okay?"
      lindsey smile "I'm feeling a little tired. Have been for a while."
      $lindsey.love+=1
      lindsey smile "Thanks for asking!"
      mc "When we talked earlier, you seemed a bit out of it."
      mc "Thought I'd check up on you."
      lindsey blush "That's really thoughtful."
      mc "Do you really not remember talking to me about your lost shoe?"
      lindsey thinking "Can't say I do."
      lindsey thinking "I'm very careful with my shoes. They're form-fitted and expensive."
      "She's too nice to be this good of a liar. Perhaps that conversation never happened."
      "Wouldn't be the first time I daydreamed about [lindsey]."
  $quest.lindsey_wrong.advance("fall")
  $process_event("update_state")
  hide lindsey with Dissolve(.5)
  return

label school_ground_floor_stairs_quest_lindsey_wrong_fall:
  "[kate]'s having a party in her house tonight."
  "Everybody's gonna have a good time."
  "And here I am about to lose my mind."
  "Every day I'm strug-gel-in', strug-gel-in'."
  $quest.lindsey_wrong["fall"] = True
  $process_event("update_state")
  "..."
  "Man, there's something about watching [lindsey] run."
  "Just look at her go!"
  "So light in the step, a morning mist sweeping across the hall."
  "Of course, she doesn't have eyes for me. When she's running it's always eyes-on-the-prize for her."
  "I wonder what it's like to always have a goal in mind. To be ambitious and hardworking."
  "To take three steps at the time when scaling a staircase."
  "To have that otherworldly grace as she—"
  show lindsey lindseyfall with Dissolve(.5)
  "Slips?!"
  "Oh, shit. She's about to crash again..."
  menu(side="far_left"):
    extend ""
    "?mc.strength>=5@[mc.strength]/5|{image=stats str}|Swoop in":
      $quest.lindsey_wrong["swooped_in"] = True
      lindsey lindseyfall "Eeep!"
      show lindsey lindseycatch with fadehold: #this gives the image both the fadehold (transition) and vpunch (transform) effects
        block:
          linear 0.05 yoffset 15
          linear 0.05 yoffset -15
          repeat 18
        linear 0.05 yoffset 0
      hide lindsey
      show lindsey lindseycatch #this is just to avoid the screen shake if the player is skipping
      lindsey lindseycatch "Oof!"
      mc "I got you."
      "That was too close."
      lindsey lindseycatch_look "[mc]? Where did you come from?"
      menu(side="left"):
        extend ""
        "\"I've been here all along.\"":
          mc "I've been here all along."
          mc "Maybe if you slowed down you'd notice the people around you."
          lindsey lindseycatch_look "I'm late for practice... thanks for catching me, though!"
          mc "You're going to hurt yourself if you keep being careless."
          lindsey lindseycatch_smile "I slipped on the stairs, that's all! I'll watch my step in the future."
          mc "This is the third time you've been in an accident this year."
          mc "Are you sure you're okay?"
          lindsey lindseycatch_smile "Never been better!"
          show lindsey smile with Dissolve(.5)
          jump school_ground_floor_stairs_quest_lindsey_wrong_fall_ending
        "?not lindsey['romance_disabled'] and mc.charisma>=4@|{image=lindsey_wrong_not_romance_disabled}|{space=10}+{space=25}[mc.charisma]/4|{image=stats cha}|\"Where did {i}you{/} come from?\"" if lindsey["romance_disabled"]:
          pass
        "?not lindsey['romance_disabled'] and mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|\"Where did {i}you{/} come from?\"" if not lindsey["romance_disabled"]:
          mc "Where did {i}you{/} come from?"
          mc "I was just walking along and you fell out of the sky."
          mc "Are you a bird? A plane? Maybe an angel?"
          $lindsey.love+=1
          lindsey lindseycatch_smile "Hey, smooth!"
          mc "I thought you were a runner. What's with the flight maneuvers?"
          lindsey lindseycatch_smile "My lifelong dream is actually to become a pilot."
          mc "Really?"
          lindsey lindseycatch_smile "No, silly! I just slipped."
          lindsey lindseycatch_smile "Thanks for catching me."
          mc "That's it? I just saved your life."
          lindsey lindseycatch_look "..."
          mc "I'm kidding. Glad you're okay."
          lindsey lindseycatch_smile "I thought I'd eat it for sure."
          mc "Me too. I thought for sure that—"
          show lindsey lindseycatch_hug with Dissolve(.5)
          "Oh."
          "Okay, then..."
          "..."
          "God, she's so warm."
          "I used to dream about hugging her, but even in my fantasy, she never hugged me back."
          "Never thought it would be so different from [jo]'s hugs."
          "When she wraps her arms around me, it feels like she's putting everything she has into it."
          "Maybe that's unique for her, being a star athlete. Maybe she puts all her effort into everything she does."
          lindsey lindseycatch_hug "Thanks so much for catching me!"
          "So, this is what true appreciation feels like."
          "Who would've thought it'd be so soft?"
        "?not lindsey['romance_disabled'] and lindsey.love>=4@|{image=lindsey_wrong_not_romance_disabled}|+{space=15}[lindsey.love]/4|{space=-15}{image=lindsey contact_icon}|{image=stats love_3}|\"We have to{space=-25}\nstop meeting{space=-40}\nlike this.\"" if lindsey["romance_disabled"]:
          pass
        "?not lindsey['romance_disabled'] and lindsey.love>=4@[lindsey.love]/4|{image=lindsey contact_icon}|{image=stats love_3}|\"We have to stop meeting like this.\"" if not lindsey["romance_disabled"]:
          mc "We have to stop meeting like this."
          lindsey lindseycatch_smile "Oh, hello there!"
          lindsey lindseycatch_smile "I don't mind it all too much."
          mc "You could seriously have hurt yourself!"
          mc "What if I wasn't around?"
          lindsey lindseycatch_look "You're serious..."
          mc "Of course I'm serious!"
          mc "I can't stand the thought of something bad happening to you."
          $lindsey.love+=2
          lindsey lindseycatch_smile "But I'm okay!"
          mc "Yes, but—"
          lindsey lindseycatch_kiss "Mwwah!"
          "It's hard to argue with a pair of lips against your cheek."
          "Her carelessness is the main reason we're even talking."
          "It has led us to this moment. So, maybe it's wrong of me to be upset with her."
          "But she's just so precious. And I don't say that just 'cause she's kissing my cheek."
          lindsey lindseycatch_hug "Thanks for catching me."
          "Okay, maybe I don't mind it that much."
          "Not when her hair carries the scent of spring — that mild breeze that heralds the summer."
          "And her arms around me whisk away the mental anguish from years of misery and self-doubt."
          "Not when her soft chest presses into me, and our hearts beat in tandem."
          "Not when she whispers words that only I can hear."
          lindsey lindseycatch_hug "My hero!"
          "And she means it too. Not a trace of sarcasm in her voice."
          "If only we could stay like this forever."
          "If only..."
      $quest.lindsey_wrong["first_hug"] = True
      jump school_ground_floor_stairs_quest_lindsey_wrong_fall_middle
    "Watch her crash":
      $mc.lust+=1
      "The girl who ran too fast for her own good."
      "Just like Icarus, she tumbles from her high perch of self-confidence and hubris."
      "Nothing to stop her fall except the hard reality of the granite floor."
      lindsey lindseyfall "Eeep!"
      show lindsey lindseycrashed_eyesclosed with fadehold: #this gives the image both the fadehold (transition) and vpunch (transform) effects
        block:
          linear 0.05 yoffset 15
          linear 0.05 yoffset -15
          repeat 18
        linear 0.05 yoffset 0
      hide lindsey
      show lindsey lindseycrashed_eyesclosed #this is just to avoid the screen shake if the player is skipping
      "That right there is perk-fection!"
      "Boobery at its very finest."
      "Titillating to say the least."
      "[lindsey]'s crashes always put her in the best positions."
      "Maybe she's trying to show me her hot little body."
      "Fuck. I can watch her like this all day long."
      lindsey lindseycrashed_waking "Ouch..."
      menu(side="far_right"):
        extend ""
        "\"Are you okay?\"":
          mc "Are you okay?"
          lindsey lindseycrashed_angry "Yeah... I think."
          lindsey lindseycrashed_angry "Probably just a few bruises..."
          lindsey lindseycrashed_angry "I need to tie my shoelaces better."
          "She looked embarrassed again."
          "I guess it doesn't look good when the star athlete keeps tripping over her own feet."
          mc "You need to be more careful. Next time you might not be so lucky."
          lindsey lindseycrashed_waking "Nngh."
          lindsey lindseycrashed_waking "I need to get to practice..."
          $lindsey["at_none_now"] = True
          $process_event("update_state")
          hide lindsey with Dissolve(.5)
          "And just like that, the show's over..."
        "?lindsey['romance_disabled']@|{image=lindsey contact_icon_evil}|Start filming her":
          "Okay, this should be good..."
          lindsey lindseycrashed_waking_rec "Oww..."
          mc "Here we have the so-called star athlete of Newfall High, once again tripping over her own feet."
          mc "For all that praise, she can't seem to remain upright."
          mc "Perhaps that prestigious scholarship should be reconsidered?"
          lindsey lindseycrashed_angry_rec "Are you filming me?"
          mc "Just happened to have the phone out."
          lindsey lindseycrashed_angry_rec "You're totally filming me."
          mc "People need to know the truth."
          lindsey lindseycrashed_angry_rec "The truth?"
          mc "Yeah, that no one is perfect."
          $lindsey.love-=2
          $lindsey.lust-=2
          lindsey lindseycrashed_angry_rec "I never claimed to be perfect! What's wrong with you?"
          mc "Yet everyone thinks you are. Well, now I have proof."
          lindsey lindseycrashed_angry_rec "I don't even know what to say... you're truly awful!"
          $lindsey["at_none_now"] = True
          $process_event("update_state")
          hide lindsey with Dissolve(.5)
          "She ran off. Probably to cry, which is good."
          "[lindsey] probably doesn't have a lot to cry about. So, it'll be a new experience for her."
      $unlock_replay("lindsey_crash")
      jump school_ground_floor_stairs_quest_lindsey_wrong_fall_maxine_scene

label school_ground_floor_stairs_quest_lindsey_wrong_fall_middle:
  show lindsey laughing with Dissolve(.5)
  lindsey laughing "I don't know what's wrong with me."
  lindsey laughing "But at least I didn't hit my head this time!"
  mc "Please be more careful!"
  $lindsey.lust+=1
  lindsey smile "I will, don't worry!"

label school_ground_floor_stairs_quest_lindsey_wrong_fall_ending:
  "I don't remember her ever tripping back in the day, but now it seems to be a daily occurrence."
  "Feels pretty good to be there for her, though."
  "Whatever's causing it has certainly helped boost my confidence."
  lindsey smile "Thanks again. I got to get to practice!"
  window hide
  $unlock_replay("lindsey_catch")
  $lindsey["at_none_now"] = True
  $process_event("update_state")
  hide lindsey with Dissolve(.5)

label school_ground_floor_stairs_quest_lindsey_wrong_fall_maxine_scene:
  pause(.5)
  show maxine thinking at appear_from_right
  window auto
  maxine thinking "Curious."
  maxine thinking "Curious, indeed."
  mc "I'm sorry, what is?"
  maxine neutral "It would seem as if the disturbance in the magnetic field is messing with [lindsey]'s balance."
  mc "The... magnetic field?"
  maxine neutral "Indeed. As it happens, our planet is enveloped in a magnetic field."
  mc "Ugh, I know that. I meant, why is there a disturbance?"
  maxine thinking "If that's what you meant, why not ask for it directly?"
  maxine thinking "I believe the disturbance is caused by the saucer-eyed doll."
  show maxine thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"The saucer-eyed doll?\"":
      show maxine thinking at move_to(.5)
      mc "The saucer-eyed doll?"
      maxine concerned "Yes. I've been led to believe that it's behind this."
      mc "What made you come to this conclusion?"
      maxine thinking "Sorry, but I'm not in the habit of giving up my sources."
      maxine thinking "However, the Ley Line Locator is very reliable."
      mc "The what?"
      maxine eyeroll "It's a recently unearthed artifact."
      mc "What? Unearthed from where?"
      maxine skeptical "From the school's basement."
      mc "Right. The basement, of course."
      mc "And what does the doll have to do with anything?"
      maxine annoyed "I told you... it's what's causing the disturbance."
      maxine annoyed "Try to listen."
    "\"How do you even know there's a disturbance?\"":
      show maxine thinking at move_to(.5)
      mc "How do you even know there's a disturbance?"
      maxine neutral "The Ley Line Locator is going off on the regular."
      mc "The Ley Line Locator?"
      maxine neutral "Indeed."
      mc "What's that?"
      maxine annoyed "A priceless artifact excavated from the depths below the school."
      mc "..."
      maxine neutral "..."
      "Ugh."
      mc "What does it do?"
      maxine annoyed "I just told you... it keeps alerting me of the disturbance in the magnetic field."
      maxine annoyed "Try to keep up."
      mc "And this is all due to the, err...  a saucer-eyed doll?"
      maxine smile "{i}The{/} saucer-eyed doll. There's only one."
    "\"I've heard enough of this nonsense.\"":
      show maxine thinking at move_to(.5)
      mc "I've heard enough of this nonsense."
      $maxine.love-=1
      $maxine.lust-=1
      maxine neutral "Ignorance always comes at a price."
      mc "What's that?"
      maxine neutral "The saucer-eyed doll would gain free reign over the magnetic field, for one."
      maxine neutral "Who knows what will happen?"
      mc "Why don't you deal with it if you're so worried?"
      maxine eyeroll "You seem to have a hard time grasping the concept of a quest giver."
      mc "What did you just say?"
      maxine neutral "I thought you were different, but maybe you're just like the other one."
      maxine neutral "In any case, the saucer-eyed doll will be happy to hear about this, I'm sure."
      mc "The other one? I have no idea what you're saying."
      maxine smile "Maybe you'll understand it at some point. Maybe you won't."
      maxine smile "The future is uncertain for all of us."
      maxine neutral "The Ley Line Locator might be our last hope. Everything hangs in the balance."
      mc "The... what?"
      maxine excited "Oh, it's just an old artifact."
      mc "Right..."
      maxine excited "I've been excavating the school's basement."
      mc "Of course you have."
  maxine neutral "Regardless, I have a newspaper to run. Please, stop wasting my time."
  show maxine neutral at disappear_to_left
  "[maxine] is the weirdest chick in the school. No idea what she just said."
  "Saucer-eyed doll? Ley Line Locator?"
  "The only thing I know for sure is that the school doesn't have a basement."
  call screen remember_to_save(_with_none=False) with Dissolve(.25)
  with Dissolve(.25)
  menu(side="middle"):
    # "?1==0@|{space=15}{image= items saucer_eyed_doll}{space=25}|The Ley of the Land is currently being worked on!\n\nPlease consider supporting us on Patreon.|{image= items monkey_wrench}":
      # pass
    "?1==1@|{image= maxine contact_icon}{space=25}|Listen to [maxine]":
      "Not sure what this is about, but I'm curious."
      "If only I could get my hands on that Ley Line Locator..."
      $quest.lindsey_wrong.start(silent=True)
      $quest.lindsey_wrong.fail()
      if quest.maxine_lines.in_progress:
        $quest.maxine_lines.advance("locator")
      else:
        $quest.maxine_lines.start("locator")
    "?not lindsey['romance_disabled']@|{image= lindsey contact_icon}|Try to help [lindsey]":
      "[lindsey]'s lapse of memory and constant loss of balance is worrying."
      "Better not waste time with [maxine]'s nonsense."
      "[lindsey] is the type to stay as far away from the [nurse]'s office as possible."
      "She'll only talk to the [nurse] if she's seriously hurt."
      $quest.maxine_lines.start(silent=True)
      $quest.maxine_lines.fail()
      if quest.lindsey_wrong.in_progress:
        $quest.lindsey_wrong.advance("nurse")
      else:
        $quest.lindsey_wrong.start("nurse")
      $school_ground_floor_west["nurse_room_locked_now"] = school_ground_floor_west["nurse_room_locked_today"] = False
    #"Do neither" if lindsey["romance_disabled"]:
      #"Sorry, ladies. I'm not some puppet for you to push around."
      #"At least, not until 'The Ley of the Land' is released..."
      #$quest.maxine_lines.fail()
      #$quest.lindsey_wrong.fail()
  return

label quest_lindsey_wrong_nurse:
  show nurse smile with Dissolve(.5)
  nurse smile "[mc], are you feeling okay?"
  mc "Hmm... let's not talk about that."
  mc "I'm looking for medical advice for a friend."
  nurse concerned "Oh my. It's best if they come talk to me themselves."
  mc "I'm not sure if they want or need help, really."
  mc "Basically, I'm just wondering what could cause memory loss and poor balance."
  nurse afraid "Those are very serious symptoms!"
  nurse afraid "My recommendation would be to seek out a medical professional as soon as possible!"
  "Hmm... the odds of that happening seems rather low."
  "After already losing a week or more of practice, [lindsey] probably won't agree to a medical examination unless her legs are falling off."
  mc "Anything you can recommend other than that?"
  nurse thinking "Hmm... drinking more. Loss of balance is a common sign of dehydration."
  nurse thinking "The memory loss part has me worried, though!"
  mc "Hmm... I guess that'll have to do for now. Thanks for the help!"
  hide nurse with Dissolve(.5)
  "All right, helping [lindsey] drink more shouldn't be a problem."
  "It might be a bit humiliating to follow her around like a personal waterboy, but her health is more important than my dignity."
  "Best place to catch her thirsty is probably in the gym."
  $lindsey["at_none_now"] = False
  $quest.lindsey_wrong.advance("drinks")
  return

label quest_lindsey_wrong_gym:
  show lindsey thinking at appear_from_left(1.5,flip=True)
  pause(1.5)
  show lindsey neutral at move_to(.5,1)
  pause(1)
  lindsey neutral "Are you okay? You seem a bit out of breath."
  show lindsey neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"You're not the easiest person to catch up to.\"":
      show lindsey neutral at move_to(.5)
      mc "You're not the easiest person to catch up to."
      $lindsey.love+=1
      lindsey flirty "You're just saying that!"
      mc "Yeah, but it's true!"
      mc "You're also not the easiest person to compliment!"
      lindsey laughing "Sorry!"
      lindsey laughing "I really meant thanks!"
      mc "You probably get this all the time, but I figured it never hurts to be nice."
      lindsey smile "That's very true!"
      lindsey smile "I like your hoodie."
      mc "I didn't know you were into death metal?"
      lindsey skeptical "That's not a band shirt!"
      mc "Yeah, I'm just kidding. It's Castlevania merch."
      lindsey smile "I like castles!"
      mc "Thanks."
    "\"Terrible shape, that's all.\"":
      show lindsey neutral at move_to(.5)
      mc "Terrible shape, that's all."
      lindsey laughing "Time to start putting in some miles then?"
      mc "I don't know about that. I've never been much for physical activities. Unless we count... you know."
      $lindsey.love-=1
      lindsey skeptical "Okay, that's pretty nasty."
      mc "Sorry, it was just a joke."
      "Man, it's so hard to flirt with some girls..."
    "\"Just fought off a dragon on the way here.\"":
      show lindsey neutral at move_to(.5)
      mc "Just fought off a dragon on the way here."
      lindsey laughing "A dragon? What kind?"
      mc "The kind that kidnaps princesses and fair maidens."
      lindsey smile "What was it doing here?"
      mc "Seemed to be setting up a trap near your locker."
      $lindsey.lust+=1
      lindsey flirty "Oh, then I guess you have my eternal gratitude for keeping me safe!"
      mc "'Tis my duty."
      lindsey laughing "My own personal Don Quixote?"
      mc "At your service!"
  mc "Anyway, you look thirsty. Want some water?"
  lindsey blush "Aw, that's really sweet of you."
  "Yes! I knew it would be easy."
  lindsey thinking "..."
  "Crap. What now?"
  lindsey thinking "I hate to sound ungrateful, 'cause I really appreciate the gesture."
  lindsey cringe "But I don't drink from other people's bottles. I can't afford to get sick."
  mc "Oh, damn."
  lindsey sad "Yeah. I'm really sorry..."
  "Typical athlete behavior, but I guess she can't be blamed for trying to stay healthy."
  lindsey smile "I'm actually out of water, though."
  mc "..."
  mc "Would you... like me to refill your bottle?"
  lindsey laughing "Would you? I'm in the middle of practice."
  $mc.add_item("lindsey_bottle")
  lindsey flirty "I'll reward you with a hug."
  mc "Fine, but it better be a good hug."
  lindsey laughing "All my hugs are elite level!"
  "Puh. I'm still exhausted from running after her..."
  "Luckily, there's a water fountain on this floor."
  hide lindsey with Dissolve(.5)
  $quest.lindsey_wrong.advance("fountain")
  return

label school_first_hall_water_fountain_use_item_lindsey_wrong(item):
# if item == "lindsey_bottle":
    $mc.remove_item(item)
    "Doing errands for popular girls. What a fate."
    "But at least [lindsey] talks to me, and every time I'm in her presence I feel invigorated."
    "She's just so full of energy!"
    "There's something truly special about—"
    window hide
    $quest.lindsey_wrong["fountain_state"] = 1
    show location with vpunch
    window auto
    "Oh, crap!"
    $quest.lindsey_wrong["fountain_state"] = 2
    "Oh, no! The thing broke!"
    $quest.lindsey_wrong["fountain_state"] = 3
    "Fuck! What would Mario have done?"
    $quest.lindsey_wrong["fountain_state"] = 4
    "If I don't fix this, I'll be in so much trouble..."
    "..."
    "Fuck me. The only way to avoid water damage is to plug the thing with something."
    "My precious hoodie will have to do for now."
    $quest.lindsey_wrong["fountain_state"] = 5
    "..."
    "There we go! As good as new."
    "...except for the small fact that it doesn't work anymore."
    $mc.add_item(item)
    "Damn. The hall is a complete mess."
    menu(side="middle"):
      "Clean up the mess":
        $mc.love+=1
        "Hmm... I better clean up this mess before detention strikes."
        "The janitor's closet must have a mop."
        $quest.lindsey_wrong.advance("mop")
        $mc["focus"] = "lindsey_wrong"
      "Ignore all responsibilities and run":
        $mc.lust+=1
        "Honestly, it's not even my fault."
        $game.disable_notifications = True
        $hidden_number.love-=1
        $game.disable_notifications = False
        "Besides, this is why we have janitors."
        "I filled [lindsey]'s bottle and that's what matters."
        "Better get out of here before someone sees the mess."
        $quest.lindsey_wrong.advance("lindseybottle")
        jump goto_school_gym
# elif item == "empty_bottle":
#   $mc.remove_item(item)
#   $mc.add_item("water_bottle")
#   "Great. It's good to stay hydrated."
#   "[lindsey] wanted water in her own bottle, though."
# else:
#   "Cleaning time for my [item.title_lower]."
#   "..."
#   "Hell yeah! My [item.title_lower]! Sparkling!"
#   $quest.lindsey_wrong.failed_item("water_fountain",item)
    return

label lindsey_wrong_school_first_hall_mop:
  "Oh, good. No angry teacher waiting for me."
  "Better mop this up quick."
  return

label lindsey_wrong_puddle_use_mop(arg_1, item):
  if item=="mop":
    $x = "" + "puddle_" + arg_1 + "_clean"
    $quest.lindsey_wrong[x] = True
    if (quest.lindsey_wrong["puddle_1_clean"] and quest.lindsey_wrong["puddle_2_clean"] and quest.lindsey_wrong["puddle_3_clean"] and quest.lindsey_wrong["puddle_4_clean"] and quest.lindsey_wrong["puddle_5_clean"] and quest.lindsey_wrong["puddle_6_clean"]):
      if quest.lindsey_wrong == "cleanforkate":
        pass
      else:
        jump lindsey_wrong_puddle_use_mop_appear
  else:
    if quest.lindsey_wrong in("verywet", "guard", "mopforkate", "cleanforkate"):
      "Mopping up the water with my [item.title_lower] — [kate]'s going to punish me."
    else:
      "Mopping up the water with my [item.title_lower] — edgy, but ineffective."
    $quest.lindsey_wrong.failed_item("puddle", item)
  return

label lindsey_wrong_puddle_use_mop_appear:
  show lindsey confident at appear_from_right
  $unlock_replay("lindsey_spurt")
  lindsey confident "Hey, how's it going?"
  mc "Err, hi there..."
  lindsey excited "I finished practice and couldn't find my bottle. Why are you cleaning?"
  mc "Long story, but here's your bottle!"
  $mc.remove_item("lindsey_bottle")
  $lindsey.love+=1
  lindsey blush "Thanks!"
  lindsey thinking "It's only half full."
  mc "Yeah, there was a tiny mishap..."
  lindsey thinking_drinking "..."
  mc "Had to bribe the [guard] with doughnuts to get this mop."
  lindsey smile "What even happened here? Where's your hoodie?"
  show lindsey smile at disappear_to_left
  mc "Like I said, there was a—"
  mc "Wait! Don't touch it!"
  hide lindsey
  show lindsey soaked_gushed with hpunch
  lindsey "Aaaaaah!"
  lindsey "{i}*Gurgle*{/}"
  show lindsey soaked_annoyed with Dissolve(.5)
  lindsey "Oh my god!"
  "Took the words right out of my mouth..."
  "The dripping hair and blush of indignation is nothing short of adorable."
  "She wipes her face on her sleeve, eyelashes sparkling with tiny wet diamonds."
  "Always thought those wet t-shirt contests were for sluts without self-esteem or dignity..."
  "[lindsey]'s the opposite of that, and maybe that's what makes this so special."
  menu(side="left"):
    "\"Are you okay?\"":
      mc "Are you okay?"
      lindsey soaked_sad "Yeah... just absolutely drenched."
      lindsey soaked_sad "Oh, god. My skirt! It looks like I peed myself!"
      lindsey soaked_sad "What's wrong with the fountain?"
      mc "I don't know. I tried to warn you, but you moved too fast as usual."
      $lindsey.love+=1
      lindsey soaked_blush "Aw, thanks for being nice about it."
      lindsey soaked_blush "I should've realized something was wrong when I saw the hoodie."
    "\"You look super cute right now.\"":
      mc "You look super cute right now."
      lindsey soaked_skeptical "I don't feel very cute."
      mc "Dry. Wet. You're always adorable and you know it!"
      if lindsey.love>=3:
        lindsey soaked_flirty "Do you really think so?"
        mc "Of course! You're the fastest and the cutest!"
        $lindsey.love+=1
        lindsey soaked_blush "Aw, you always know how to turn a bad situation around!"
      else:
        lindsey soaked_annoyed "Can you please stop making judges on my looks?"
        mc "Sorry, I didn't mean anything bad by it."
        $lindsey.love-=1
        lindsey soaked_annoyed "I know, but it's really inappropriate..."
    "?lindsey.lust>=3@[lindsey.lust]/3|{image= lindsey contact_icon}|{image=stats lust}|\"Sorry for making you wet.\"":
      mc "Sorry for making you wet."
      lindsey soaked_flirty "Let me guess... it was bound to happen at some point?"
      mc "Oh my lord. That's absolutely raunchy! What a thing to say!"
      lindsey soaked_blush "Stop pretending!"
      lindsey soaked_blush "I know that's exactly where you were going with that!"
      mc "I don't know what you're talking about!"
      $lindsey.lust+=2
      $mc.charisma+=1
      lindsey soaked_flirty "Okay, Reverend [mc]. I totally believe you!"
  mc "Do you have anything you can change into?"
  lindsey soaked_annoyed "Yeah, in the women's locker room."
  "Crap. Someone's coming up the stairs."
  lindsey soaked_sad "People are coming! Oh, god! I'll die if they see me like this..."
  mc "Quick, hide in the art classroom!"
  hide lindsey with Dissolve(.5)
  "Oh, shit. Better put the mop away too!"
  window hide
  $school_first_hall["mop_shown"] = True
  $mc.remove_item("mop")
  $process_event("update_state")
  show kate confident at appear_from_right
  pause 0.5
  window auto
  kate confident "You look awfully guilty."
  kate confident "What are you hiding?"
  show kate confident at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Just my disgust for you.\"":
      show kate confident at move_to(.5)
      mc "Just my disgust for you."
      $kate.lust-=1
      kate laughing "There's a difference between being disgusted and being disgusting. You're unfortunately the latter."
      kate smile "Now, if you'll excuse me, I have all sorts of important social obligations to attend to."
      kate smile "Being popular is a lot of hard work... but you wouldn't know anything about that."
    "\"Nothing, ma'am.\"":
      show kate confident at move_to(.5)
      mc "Nothing, ma'am."
      kate neutral "Really, now?"
      kate neutral "Then why is your face so red?"
      mc "I, err... you make me nervous, ma'am."
      kate excited "Good! I like that you know your place."
      kate excited "I could crush you under my heel at any time."
      mc "I know that..."
      kate confident "Good boy."
      "Ugh, she makes me feel so small."
  window hide
  $quest.lindsey_wrong.advance("lindseyart")
  if kate.at("school_first_hall"):
    hide kate with Dissolve(.5)
  else:
    if renpy.showing("kate smile"):
      show kate smile at disappear_to_right
    elif renpy.showing("kate confident"):
      show kate confident at disappear_to_right
    pause 1.0
  window auto
  return

label lindsey_wrong_school_art_class_enter:
  "Huh? Where did her clothes go?"
  "I don't think [lindsey] saw me come in."
  "Can't stare at her too long, though. She might notice."
  return

label quest_lindsey_wrong_lindseyart:
  $lindsey.unequip("lindsey_panties")
  $lindsey.unequip("lindsey_bra")
  $lindsey.unequip("lindsey_shirt")
  $lindsey.unequip("lindsey_skirt")
  show lindsey afraid with Dissolve(.5)
  lindsey afraid "Eeep! Don't look!"
  mc "I'm sorry, but why are you naked?"
  lindsey afraid "My clothes were soaked! I can't afford to get a cold!"
  lindsey afraid "Stop looking!"
  show lindsey afraid at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Look, I'm closing my eyes!\"":
      show lindsey afraid at move_to(.5)
      mc "Look, I'm closing my eyes!"
      $set_dialog_mode("default_no_bg")
      show black with Dissolve(.5)
      pause(.5)
      lindsey "No peeking! Promise!"
      $lindsey.love+=1
      mc "I promise."
      mc "But what are you going to do? Hide in here until your clothes dry?"
      lindsey "Um... I need you to get my clothes from the women's locker room..."
      menu(side="middle"):
        extend ""
        "Help her":
          $mc.love+=1
          mc "Sure, no problem."
          lindsey "Thank you! They're in the locker with trophy stickers on."
          mc "Are you sure you'll be okay while I'm gone?"
          lindsey "What do you mean?"
          mc "I mean, someone could come in here at any moment."
          lindsey "I don't really have any options right now..."
          mc "Hmm..."
          mc "You could borrow my shirt if you want."
          lindsey "Would you do that for me?"
          mc  "For sure. It would be rude not to."
          mc "Hold on."
          mc "..."
          mc "Here."
          lindsey "Thank you so much!"
          lindsey "..."
          lindsey "Just a moment..."
          lindsey "..."
          lindsey "Okay, you can look now."
          $lindsey.equip("lindsey_mcshirt")
          show lindsey smile
          hide black
          with Dissolve(.5)
          show lindsey smile with Dissolve(.5)
          $set_dialog_mode("")
          lindsey laughing "I look silly in this."
          mc "Looks better on you than me!"
          lindsey smile "You're really nice. Thanks for letting me borrow it!"
          mc "It's the least I can do."
          lindsey flirty "You did get me wet..."
          mc "I bet I did."
          lindsey skeptical "Hey!"
          mc "What? You started it!"
          lindsey laughing "I'm just messing with you!"
          mc "Are you sure you'll be okay?"
          $lindsey.love+=1
          $lindsey.lust+=1
          lindsey smile "Yeah, I'm okay like this. Thanks again!"
          mc "Okay, be right back."
          $quest.lindsey_wrong["privacy"] = True
          $quest.lindsey_wrong.advance("florahelp")
          $flora["hidden_now"] = flora["hidden_today"] = False
          $process_event("update_state")
          hide lindsey with Dissolve(.5)
          return
        "Open your eyes":
          show lindsey thinking
          hide black
          with Dissolve(.5)
          show lindsey afraid with Dissolve(.5)
          $lindsey.lust-=1
          $mc.lust+=1
          $set_dialog_mode("")
          lindsey afraid "You're looking!"
          mc "Oh, get over it. You have nothing to be ashamed of."
          lindsey angry "It's not about that! It's about respecting my privacy!"
          lindsey angry "I can't believe you'd take advantage of me like this!"
          mc "Hey, no one forced you to strip."
          lindsey cringe "Still not cool."
          mc "I mean, we're in the art classroom. You might as well model for me."
          lindsey skeptical "You're not funny."
    "\"Nothing I haven't seen before.\"":
      show lindsey afraid at move_to(.5)
      mc "Nothing I haven't seen before."
      $lindsey.love-=1
      $lindsey.lust-=1
      lindsey angry "That's so rude."
      mc "It wasn't meant to be!"
      lindsey skeptical "You're really not going to give me privacy?"
      mc "I was going to. I just meant that I've seen a naked girl before. It's nothing new to me."
      lindsey cringe "That's not the point..."
      mc "I'm sorry for barging in, but you did the undressing."
      lindsey cringe "You're right, but still..."
  mc "How about this? You can have my shirt for now."
  lindsey concerned "Yes, please."
  mc "Okay, hold on."
  mc "..."
  lindsey thinking "You need to work out more."
  mc "Great. Who's being rude now?"
  lindsey thinking "It's just a fact."
  mc "I know you're upset."
  mc "But do I really have to remind you that this all happened because I was trying to be helpful and bring you water?"
  lindsey concerned "Hm... you're right. I'm sorry for getting annoyed."
  mc "It's fine, really. Just felt a bit unfair. Here you go."
  show lindsey smile with Dissolve(.5)
  $lindsey.equip("lindsey_mcshirt")
  pause(.5)
  lindsey smile "Thanks, this feels better."
  mc "So, I'm forgiven?"
  lindsey skeptical "Not so fast!"
  lindsey skeptical "I still can't go around the school looking like this."
  mc "Why not? You look cute."
  lindsey laughing "'Cause it looks like we slept together and I'm wearing your shirt."
  mc "What's wrong with that?"
  lindsey skeptical "I hope you're joking."
  mc "Maybe a little."
  mc "Do you have a set of dry clothes somewhere?"
  lindsey smile "Yeah, in the women's locker room. The locker with the trophy stickers on."
  mc "Great."
  "Just great. The only place where you get instant detention from entering."
  mc "I guess I'll be right back then."
  lindsey smile "Hurry, please!"
  $quest.lindsey_wrong.advance("florahelp")
  $flora["hidden_now"] = flora["hidden_today"] = False
  $process_event("update_state")
  hide lindsey with Dissolve(.5)
  return

label quest_lindsey_wrong_hurry_up:
  show lindsey smile with Dissolve(.5)
  mc "Are you sure you'll be okay?"
  if quest.lindsey_wrong["privacy"]:
    lindsey smile "Yeah, I'm okay like this. Thanks again!"
  else:
    lindsey smile "Yeah, I'm okay like this. Hurry, please!"
  mc "Okay, be right back."
  hide lindsey with Dissolve(.5)
  return

label flora_quest_lindsey_wrong_florahelp:
  if flora.at("school_*"):
    show flora skeptical with Dissolve(.5)
    flora skeptical "..."
    flora skeptical "Why are you shirtless, you perv?"
    mc "If Chad can parade his oiled-up ass around the school, why can't I?"
    flora eyeroll "Because, unlike you, he's been the face of Newfall Fitness for three years in a row?"
    flora confused "Because his pectorals can open soda cans and his six-pack is considered a national treasure?"
    flora sarcastic "Because he's freaking Chad?"
    mc "Okay, points taken."
    flora confident "Your zipper's open, by the way."
  else:
    show flora confident with Dissolve(.5)
    flora confident "Your zipper's open!"
  mc "..."
  mc "No, it's not."
  flora excited "Made you look!"
  mc "I swear, you're the most annoying—"
  flora embarrassed "Why are you cursing me out in front of everyone?"
  flora embarrassed "You're really embarrassing me!"
  mc "Ugh. I'm sorry, I just need—"
  flora excited "Forgiven!"
  mc "Seriously?"
  flora concerned "What's wrong with my forgiveness now?"
  mc "Nothing! Just listen, please."
  flora angry "Stop cutting me off! [jo] said you need to stop talking over other people."
  mc "I didn't even cut you off!"
  flora confident "Then, all is well. Just making sure you remember your manners."
  mc "..."
  "She's impossible sometimes..."
  mc "Can you help me with something?"
  flora concerned "Gross."
  mc "I didn't even say what it was."
  flora concerned "Yeah, but knowing you, it's probably something gross."
  show flora concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I need you to help me get [lindsey]'s clothes from the women's locker room.\"":
      show flora concerned at move_to(.5)
      mc "I need you to help me get [lindsey]'s clothes from the women's locker room."
      $flora.love-=1
      flora angry "I knew it was something gross!"
      mc "What? I'm just trying to help her."
      flora skeptical "What could [lindsey] possibly need your help with?"
      mc "She's... err, she was late for practice and asked me to get them for her."
      flora skeptical "Why would she need her clothes during practice?"
      "Ugh, crap. [flora]'s too smart."
      mc "Okay, the truth is she spilled water all over her clothes and is stuck in the art classroom."
      flora annoyed "You know I have her number, right?"
      mc "Go on, then. Text her."
      flora annoyed "..."
      flora annoyed_texting "I'm busting your wicked plans..."
      flora annoyed_texting "And I'll have evidence to ground you for life..."
      "Dear god. Please let [lindsey] confirm it."
      flora annoyed_texting "..."
      flora confused "..."
      flora confused "Did you drug her or something?"
      mc "What did she say?"
      flora annoyed_texting "[lindsey] said you can be trusted."
      flora annoyed_texting "She clearly doesn't know you well enough."
      mc "Maybe I'm not as bad as you think."
      flora skeptical "I know for a fact that you're worse than I think."
      mc "Well, not to her."
      mc "Even if you don't want to help me. You could do it for her."
      if flora.at("school_first_hall_east"): #if this interaction happens at 3 PM, it'll jump straight to the next label so that the player doesn't have quest flora again
        $quest.lindsey_wrong["same_location"] = True
        jump flora_quest_lindsey_wrong_floralocker_advance
      flora skeptical "Fine. Let's go."
      $quest.lindsey_wrong.advance("floralocker")
      $process_event("update_state")
      hide flora with Dissolve(.5)
    "?mc.strength>=3@[mc.strength]/3|{image=stats str}|\"Bullies stole my sports bag and hid it in the women's locker room.\"":
      show flora concerned at move_to(.5)
      mc "Bullies stole my sports bag and hid it in the women's locker room."
      flora skeptical "Since when do you have a sports bag?"
      mc "Since [lindsey] started helping me out in the gym."
      mc "Look at these guns, [flora]! Lifting is paying off!"
      flora eyeroll "I liked you better lazy..."
      mc "Can't be lazy if you want to look like a movie star!"
      flora sarcastic "The only movie they'd let you star in is Dumb & Dumber."
      mc "And I'd get rich from it!"
      flora confused "Whatever floats your ugly makeshift raft, I guess..."
      mc "So, you'll help me?"
      if flora.at("school_first_hall_east"): #if this interaction happens at 3 PM, it'll jump straight to the next label so that the player doesn't have quest flora again
        $quest.lindsey_wrong["same_location"] = True
        jump flora_quest_lindsey_wrong_floralocker_advance
      flora annoyed "Yeah, whatever."
      mc "Okay, let's go!"
      $quest.lindsey_wrong.advance("floralocker")
      $process_event("update_state")
      hide flora with Dissolve(.5)
  return

label flora_quest_lindsey_wrong_floralocker:
  show flora annoyed with Dissolve (.5)
  mc "Hey, why do you look so happy today?"
  flora annoyed "If you're confused, this is my can't-deal-with-you-look."
  flora annoyed "Let's get this over with."
  mc "Sure! The sports bag is in the locker with the trophies stickers."
  label flora_quest_lindsey_wrong_floralocker_advance:
  flora eyeroll "Ugh. Be right back..."
  show flora eyeroll at disappear_to_right
  if quest.lindsey_wrong["same_location"]: #if the player jumps to flora_quest_lindsey_wrong_floralocker_advance, flora won't know which locker to look for
    mc "It's in the locker with the trophies stickers!"
  "Making [flora] do things for me always fills me with a strange sort of satisfaction."
  "She's such a brat that it's extra nice to see her comply."
  "Especially if it annoys her."
  show flora annoyed at appear_from_right
  pause(1)
  flora annoyed "There."
  $mc.add_item("lindsey_bag")
  mc "Thanks, [flora]. You're an angel!"
  flora eyeroll "Spare me the sarcasm."
  $quest.lindsey_wrong.advance("lindseyclothes")
  $process_event("update_state")
  hide flora with Dissolve(.5)
  "Well, better get [lindsey] her clothes."
  return

label quest_lindsey_wrong_lindseyclothes:
  $unlock_replay("lindsey_hug")
  show lindsey smile with Dissolve(.5)
  lindsey smile "Oh, nice! You found it!"
  show lindsey smile at move_to(.75)
  menu(side="left"):
    extend ""
    "?lindsey.lust>=5@[lindsey.lust]/5|{image= lindsey contact_icon}|{image=stats lust}|\"Sure did, but I kinda like you wearing my shirt.\"":
      show lindsey smile at move_to(.5)
      $mc.remove_item("lindsey_bag")
      mc "Sure did, but I kinda like you wearing my shirt."
      lindsey flirty "Yeah? Why is that?"
      "'Cause in some twisted way that makes me feel like you're my girlfriend."
      mc "You just wear it better than I do!"
      lindsey laughing "It's a bit big on me, don't you think?"
      mc "I think it looks absolutely perfect."
      lindsey flirty "Do you really think so?"
      mc "Without a doubt."
      lindsey smile "Aw, come here!"
      $lindsey.lust+=1
      show lindsey hugging_mcshirt with Dissolve(.5)
      if quest.lindsey_wrong["first_hug"]:
        "Oh, wow. Two hugs from [lindsey] in one day!"
        $achievement.teddy_bear.unlock()
      "Not how I expected my day to turn out when I woke up this morning."
      "Maybe that's the trick. Having no expectations and just doing the right thing."
      "Feeling [lindsey]'s nimble body pressing against me... it's hard not to think that's the case."
      "Her wet hair smells like spring rain."
      "Living in loneliness for so long... I sure missed out on life itself."
      "'Cause her arms around me is life. It fills me with new hope. With energy."
      "Even if I wake up now or get sent back to my old life, I'll remember this moment."
      "I'll remember her on her tippy toes, wearing my shirt."
      "I'll remember it riding up her cute butt. And I'll remember her breath on my neck."
      "Nothing will ever erase this moment."
      "..."
      show lindsey smile with Fade(.5,3,.5)
      lindsey smile "Thank you. And thanks for being so nice to me."
      mc "You're one of the first people I think of when I hear the phrase \"good people.\""
      mc "And this year, I've decided to be better as well."
      lindsey smile "Well, it's certainly working. I hope everyone sees that!"
      lindsey smile "Anyway, can you close your eyes for a moment so I can change?"
      mc "Of course."
      $set_dialog_mode("default_no_bg")
      show black with Dissolve(.5)
      pause(.5)
      "She's so precious, acting all shy."
      "Hmm... maybe it's not an act with her. Maybe I should start giving girls the benefit of the doubt."
      "On the surface, [lindsey] looks like someone who would spit in my direction, but the truth couldn't be further from that."
      "Being nice to her has certainly worked so far, so maybe it's time to update my general mindset."
      lindsey "Just a moment!"
      lindsey "..."
      lindsey "Here's your shirt back..."
      lindsey "..."
      lindsey "Okay, now you can look."
      $lindsey.equip("lindsey_pants")
      $lindsey.equip("lindsey_tshirt")
      show lindsey smile
      hide black
      with Dissolve(.5)
      show lindsey smile with Dissolve(.5)
      $set_dialog_mode("")
      lindsey laughing "What?"
      mc "I guess I'm used to the blue hoodie."
      lindsey smile "Ah, okay! I do feel like I only wear my practice outfit these days."
      mc "It's a good color for you!"
      lindsey flirty "Thanks!"
      lindsey smile "By the way, my dad got me a new easel after the beaver ruined the one you made for me."
      lindsey smile "It's not as special, but I'd like to try it out soon."
      mc "That's nice. Best of luck!"
      lindsey laughing "Thanks, but I was hoping you'd inaugurate it with me!"
      lindsey laughing "I gotta run, but here's my number."
      $mc.add_phone_contact(lindsey)
      lindsey smile "Thanks again!"
    "\"I'd like to be rewarded for my help.\"":
      show lindsey smile at move_to(.5)
      $mc.remove_item("lindsey_bag")
      mc "I'd like to be rewarded for my help."
      $lindsey.lust-=1
      lindsey skeptical "I was going to, but that's kinda rude."
      mc "Why rude?"
      lindsey skeptical "Do you know the difference between a knight and a mercenary?"
      lindsey skeptical "One fights for chivalry and what's right and good. The other, for cash and rewards."
      mc "I guess I see your point..."
      lindsey smile "Good. Maybe you're a squire in training."
      mc "Maybe this isn't the dark ages."
      lindsey smile "You know what? I can't be angry with you after you helped me."
      mc "Twice."
      lindsey flirty "Don't push it!"
      lindsey smile "Okay, let me just get dressed. Look away, please."
      mc "Fine."
      $set_dialog_mode("default_no_bg")
      show black with Dissolve(.5)
      pause(.5)
      "Maybe it was wrong to ask for a reward... it's kinda cringy now that I think of it."
      "[lindsey] is way too nice for me. She didn't have to forgive me for any of this."
      lindsey "Just a moment!"
      lindsey "..."
      lindsey "Here's your shirt back..."
      lindsey "..."
      "Man, I'm way out of my depth with her."
      lindsey "Okay, now you can look."
      $lindsey.equip("lindsey_pants")
      $lindsey.equip("lindsey_tshirt")
      show lindsey smile
      hide black
      with Dissolve(.5)
      show lindsey smile with Dissolve(.5)
      $set_dialog_mode("")
      lindsey laughing "You look surprised."
      mc "I think I've only ever seen you in your blue hoodie before."
      lindsey smile "Yeah, it's my favorite."
      lindsey flirty "Since you didn't actually bring me the water as promised, I'll withhold my hug for now."
      lindsey flirty "However! My dad finally got me an easel after he heard what happened to the last one."
      lindsey smile "So, if you want to go paint with me sometime, we could totally do that now!"
      "A bit disappointing, but considering the many ways I screwed up, maybe it's fair."
      "At least it would mean spending more time alone with her, and that's a win in my book."
      mc "Cool, sounds good to me!"
      lindsey laughing "Great. I'll get back to you about it. Let me put my number on your phone."
      $mc.add_phone_contact(lindsey)
      "Okay, that's a great reward. Now I really can't complain."
      "Hopefully she'll call or text me about it..."
      mc "I'm looking forward to it!"
      lindsey smile "Okay, gotta run!"
  $mc["focus"] = ""
  $lindsey["at_none_now"] = True
  $process_event("update_state")
  show lindsey smile at disappear_to_right
  "Okay... so in a way, I did get [lindsey] hydrated."
  "Maybe not in the right way, but it has to count for something, right?"
  "Hmm... I should probably try to remind her to drink more in the future too."
  "But at least she didn't act weird."
  "Need to keep a look on her from now on, in case things get worse."
  $lindsey.equip("lindsey_panties")
  $lindsey.equip("lindsey_bra")
  $lindsey.equip("lindsey_shirt")
  $lindsey.equip("lindsey_skirt")
  $quest.lindsey_wrong["fetched"] = True
  $quest.lindsey_wrong.finish()
  return

label lindsey_wrong_school_gym_lindseybottle_enter:
  show lindsey neutral with Dissolve (.5)
  lindsey neutral "There you are!"
  lindsey neutral "I thought you got lost somewhere along the way."
  mc "Yeah... I had a little mishap, but nothing serious."
  "I hope."
  mc "Anyway, here's your water."
  $mc.remove_item("lindsey_bottle")
  mc "Something was wrong with the fountain outside. I only managed to get it half full."
  lindsey blush "That's fine. Thanks!"
  lindsey thinking_drinking "..."
  "It's a tiny step toward getting [lindsey] hydrated, but it's a step nonetheless."
  "Hopefully, I can keep an eye on her in case something bad happens."
  "She's got such a bright future ahead of her."
  lindsey smile "Oh, by the way! My dad got me a new easel!"
  mc "Congratulations! Those aren't cheap."
  lindsey smile "Yeah, I'm quite spoiled sometimes."
  mc "Does that mean our painting date is back on the table?"
  lindsey laughing "I'm not sure I would call it a date! But yeah, that's where I was going!"
  mc "Cool, I'm game."
  lindsey smile "Okay! Let me give you my number, then."
  $mc.add_phone_contact(lindsey)
  "Man, this day couldn't get any better!"
  "Let's hope it's not a fake one..."
  mc "Thanks. And what about that hug?"
  lindsey flirty "I'll save it for the date."
  mc "I thought you said it wasn't a date?"
  lindsey laughing "We'll see what it is!"
  mc "Hmm... okay!"
  lindsey excited "Okay, great! Gotta run! Thanks for the water again!"
  $quest.lindsey_wrong.advance("splash")
  $process_event("update_state")
  show lindsey excited at disappear_to_right
  pause(1)
  return

label lindsey_wrong_school_first_hall_splash_enter:
  $kate.unequip("kate_bra")
  "Crap. Totally forgot about the surveillance camera."
  "If the [guard] is in any way competent at his job, he's got me dead to rights."
  "Luckily, he's not very competent."
  show kate confident at appear_from_right
  pause(1)
  kate confident "What are you looking at, hm?"
  mc "Nothing..."
  kate excited "Wrong! You're looking at the queen."
  kate excited "So, what's happening in loser-ville today?"
  mc "Nothing..."
  kate eyeroll "You bore me today."
  mc "..."
  kate neutral "Anyway..."
  show kate neutral at disappear_to_left
  pause(1)
  menu(side="middle"):
    "Warn her":
      mc "[kate]! Careful with the fountain!"
      show kate skeptical at appear_from_left
      kate skeptical "What's wrong with it?"
      #kate skeptical "Is that your hoodie?"
      #mc "Maybe."
      #mc "Anyway, it's broken."
      mc "It's broken."
      kate thinking "Broken how?"
      mc "It'll spray water all over the place if the hoodie is removed."
      kate confident "Really, now?"
      mc "Mhm..."
      "Don't like the look on her face."
      kate confident "And you broke it?"
      mc "It wasn't exactly—"
      kate neutral "It's a yes-or-no-question, [mc]."
      mc "Yes, ma'am. I broke it."
      kate confident "Okay, here's what's going to happen."
      kate confident "You're going to go over to the fountain. Lean in, and remove the hoodie."
      mc "I'll get drenched!"
      kate gushing "See, that's exactly the point!"
      mc "I'd rather not..."
      kate neutral "The surveillance camera caught you. I'll go straight to the principal."
      "Ugh... why did I warn her?"
      kate neutral "I'm going to count to three."
      kate neutral "One..."
      kate neutral "Two..."
      mc "Fine! I'll do it!"
      $kate.lust+=1
      kate confident "That's a good boy."
      kate confident "Afterward, you're going to go find a mop and clean up after yourself."
      kate confident "And I'm going to make sure you do a good job. Then, maybe I'll let you off the hook."
      mc "..."
      mc "Yes, ma'am."
      kate neutral "Go ahead."
      $quest.lindsey_wrong.advance("verywet")
      $mc["focus"] = "lindsey_wrong"
      if game.quest_guide != "lindsey_wrong":
        $game.quest_guide = ""
      hide kate with Dissolve(.5)
      return
    "Stay quiet":
      $unlock_replay("kate_spurt")
      kate neutral "Who the fuck splashed water all over the floor?"
      kate neutral "And left a disgusting wet hoodie on the fountain?"
      show kate neutral at move_to("left")
      kate neutral "[mc], is this your—"
      show kate soaked_wet with hpunch
      kate soaked_wet "Aaaaaaaaah!"
      "[kate] looks like a wet cat. A soaked pussy."
      "Bold of her to not wear a bra, but I guess she's confident enough in her nipple game."
      "With standards as high as hers, she doesn't have to worry about autonomous body responses."
      "She's honestly pretty cute like that."
      "Maybe that's the cure — drown the witch in her."
      "Finally, something bad happened to [kate]."
      "It's kinda hot seeing her all upset and shivering. She almost appears human."
      "I should take a picture of this. Might be useful at some point."
      show white with Dissolve(.15)
      play sound "camera_snap"
      hide white with Dissolve(.15)
      $mc.add_item("kate_wet_photo")
      "Perfect."
      show kate angry_soaked at Position(xalign=.5) with Dissolve(.5)
      kate angry_soaked "Someone's going to die for this..."
      "Oh, but not me. Not today!"
      jump goto_school_ground_floor

label lindsey_wrong_school_ground_floor_kate_escape:
  $kate.equip("kate_bra")
  $kate["at_none_now"] = True
  "Phew. Made it out!"
  "I should probably stash this photo in my locker for now."
  "[kate] knows when I'm hiding something from her."
  "She'll interrogate me into confession as long as I have it with me..."
  "Besides, it'll make for great decoration."
  $quest.lindsey_wrong.advance("katephoto")
  return

label lindsey_wrong_school_ground_floor_kate_escape_locker:
  "Let's put it right here..."
  $quest.lindsey_wrong["kate_wet_photo"] = True
  $mc.remove_item("kate_wet_photo")
  "Man, that's a nice picture."
  "So, I helped [lindsey] and managed to escape [kate]'s wrath for now. Things are looking up."
  "The [nurse] did sound serious about [lindsey]'s condition, though..."
  "Maybe it's best if I look after her for a bit."
  "Not in a creepy way, of course. Just like a guardian angel."
  "One of these days, she's going to fall and really hurt herself."
  "Hopefully, I'll be ready."
  $quest.lindsey_wrong.finish()
  return

label kate_quest_lindsey_wrong_verywet:
  show kate smile with Dissolve(.5)
  kate smile "What are you waiting for? Wet yourself."
  kate smile "There's no other way out of this."
  hide kate with Dissolve(.5)
  return

label lindsey_wrong_school_ground_first_hall_kate_clean:
  "Well, at least [kate] didn't get the principal."
  "But she's there waiting for me."
  "She probably takes great pleasure in this..."
  show kate confident with Dissolve(.5)
  kate confident "Aha! There's my little maid."
  kate confident "She's got her little mop too. Perfect!"
  mc "She?"
  kate neutral "Is there a problem?"
  mc "..."
  kate smile "Didn't think so. The only thing missing is a skimpy maid outfit."
  kate smile "Is the maid upset that she's getting bossed around?"
  mc "..."
  kate confident "Has she lost her ability to talk?"
  mc "..."
  kate neutral "That's good. I like quiet servants. Now get to it."
  hide kate with Dissolve(.5)
  return

label kate_quest_lindsey_wrong_cleanforkate:
  if (quest.lindsey_wrong["puddle_1_clean"] and quest.lindsey_wrong["puddle_2_clean"] and quest.lindsey_wrong["puddle_3_clean"] and quest.lindsey_wrong["puddle_4_clean"] and quest.lindsey_wrong["puddle_5_clean"] and quest.lindsey_wrong["puddle_6_clean"]):
    show kate confident with Dissolve(.5)
    mc "All done, ma'am."
    kate neutral "I'll be the judge of that."
    kate neutral "Stay."
    show kate neutral at disappear_to_left
    "She's gone to inspect it. Looking at it from every angle."
    "Please be good enough..."
    "..."
    show kate neutral at appear_from_left
    pause(1)
    kate neutral "Do you think you did a good job?"
    mc "Yes, ma'am."
    kate neutral "It's passable at best... but I'll let it slide for now."
    kate smile "I like you holding that mop. Maybe I'll have you scrub my basement floor at some point."
    kate smile "Wouldn't that be fun?"
    mc "I... err..."
    kate laughing "You don't have to answer now."
    kate laughing "I think you'd look adorable in a French maid outfit."
    kate confident "Maybe next time I throw a big party I'll have you come and clean up afterward."
    kate confident "Anyway. Your allotted time with me is up."
    kate neutral "Leave the mop there to remind you who's in charge here."
    $school_first_hall["mop_shown"] = True
    $mc["focus"] = ""
    $kate["at_none_now"] = True
    show kate neutral at disappear_to_right
    $mc.remove_item("mop")
    $process_event("update_state")
    "I can't believe that just happened."
    "Why do I feel such an urge to kneel down, kiss [kate]'s shoes, and thank her?"
    "She's messing with my head."
    "Ugh, need to shake it off. Her noose is tightening."
    $quest.lindsey_wrong["mc_verywet"] = False
    $renpy.transition(Dissolve(0.5)) #this is just to remove the water drops from the screen with dissolve
    "..."
    "Even though I'm slowly falling under [kate]'s spell, helping [lindsey] was a good thing."
    "Getting her water is not much, but the [nurse] said it might help."
    "It's probably best to watch over her from now on."
    $kate.equip("kate_bra")
    $quest.lindsey_wrong.finish()
  else:
    show kate neutral with Dissolve(.5)
    kate neutral "You're far from done, missy."
    kate neutral "Mop that floor."
    hide kate with Dissolve(.5)
  return
