label jacklyn_quest_art_focus_start:
  show jacklyn smile with Dissolve(.5)
  mc "I'd like to sign up for your class, please."
  jacklyn smile "What's your name, shooter?"
  mc "[mc]."
  jacklyn thinking "Let's take a look at your criminal record."
  mc "My what?"
  jacklyn thinking "Your past art. One hot second, por favor."
  show jacklyn jacklynpose01 with fade
  "Okay, that's... a good selling point for her class... not going to lie."
  "That has to be against some kind of school policy! But well, who's actually going to complain?"
  "They say that self-expression is the best art, and [jacklyn] is certainly making a point of it."
  "Sure takes confidence to wear a skirt that short. And bending over in it is like an invitation."
  "Is she the type who likes to be spanked? "
  "Hand or paddle?"
  "Hmm... she seems tough, so probably the paddle."
  "And those fishnet stockings... didn't even know I had a leg fetish until now."
  "This goes to show that not only airhead bimbos can pull off slutty."
  "It's honestly even hotter that [jacklyn] is highly intelligent and still chooses to dress this way."
  "Math and science used to be my forte... but her string theory seems way more alluring."
  $unlock_replay("jacklyn_string")
  jacklyn neutral "Sorry. That's a no-go."
  mc "Huh? Why?"
  jacklyn thinking "Your past art is nothing but petty theft and shop-lifting. I want murder in my class."
  show jacklyn thinking at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Since when can teachers deny a student their education?\"":
      show jacklyn thinking at move_to(.5)
      mc "Since when can teachers deny a student their education?"
      jacklyn neutral "Plug the saltwater fountains, weepy. You'll get yours."
      jacklyn smile "All students have access to the shop, you just won't be on my shortlist."
      jacklyn smile "You're still getting a basic education."
      mc "What about [isabelle]? You haven't even seen her work yet!"
      jacklyn smile "Exchange students aren't my dolls to dress up. The bigwigs of the board decide their fate."
      mc "Anything I can do to change your mind?"
      jacklyn laughing "See that easel with the blank canvas? Make it your crime scene and we'll talk again."
    "\"I've been practicing a lot since... err, last year. Can I show you?\"":
      show jacklyn thinking at move_to(.5)
      mc "I've been practicing a lot since... err, last year. Can I show you?"
      jacklyn thinking "Murder isn't all about practice; it's also about passion. Just keep that in your noggin."
      jacklyn smile "See that easel with the blank canvas? Make it your crime scene and we'll talk again."
    "\"Art is a stupid subject anyway. Just spatter some paint on a canvas and hang it in a gallery.\"":
      show jacklyn thinking at move_to(.5)
      mc "Art is a stupid subject anyway. Just spatter some paint on a canvas and hang it in a gallery."
      $jacklyn.love-=1
      $jacklyn.lust-=1
      jacklyn cringe "Art is definitely not for ostrich-heads."
      jacklyn annoyed "If you ever pull it out of the sand, you're free to show me murder on that empty canvas."
  hide jacklyn with Dissolve(.5)
  $quest.jacklyn_art_focus.start()
  return

label jacklyn_art_focus_easel_use(item):
  if item == "brush":
    show misc canvas canvas with Dissolve(.5)
    "Some say my strokes are among the best. Perhaps of all time."
    "Just a little bit of this and a little bit of that..."
    show misc canvas brush_stage1 with Dissolve(.5)
    "Mm... Van Gogh can gogh on my vang."
    show misc canvas brush_stage2 with Dissolve(.5)
    "Oh! Look at that!"
    "Now for the finishing touches..."
    show misc canvas brush_stage3 with Dissolve(.5)
    "Perfection."
    "..."
    "Okay, time to show [jacklyn] and get that A."
    hide misc canvas brush_stage3 with Dissolve(.5)
    $quest.jacklyn_art_focus["show_jacklyn"] = "brush"
    $quest.jacklyn_art_focus.advance("showjacklyn")
  elif getattr(item,"tiertwo",False):
    show misc canvas canvas with Dissolve(.5)
    "Not an artist."
    "Just a boy with a dream."
    "Just a tiny spark of inspiration."
    "..."
    show misc canvas tier2_stage1 with Dissolve(.5)
    "For so many years..."
    "Ugly, lazy, unwanted."
    "A social outcast filled with rage."
    show misc canvas tier2_stage2 with Dissolve(.5)
    "And it burns in my veins, in my eyes, in my brain."
    "It burns anyone who dares get too close."
    "Like a whirling inferno, raging through cities."
    "Nothing is spared. Nothing left but ashes."
    show misc canvas tier2_stage3 with Dissolve(.5)
    "\"Fuck the world\" — that's what's on this canvas."
    show jacklyn neutral at appear_from_left(.5)
    jacklyn neutral "Dude... how did you get those strokes in?"
    mc "Err..."
    "Probably shouldn't tell her I used the [item.title_lower]."
    hide misc canvas tier2_stage3 with Dissolve(.5)
    jacklyn smile "That's hot as fuck."
    mc "Seriously?"
    jacklyn smile "Yeah, the raw emotion. It's tearing me a new asshole!"
    mc "That's a good thing?"
    jacklyn excited "Shit, duckling. You're talented and don't even know it."
    jacklyn excited "Let me give you a big hush, okay? Art is an extension of our emotions."
    jacklyn laughing "There's nothing as boring as a passionless painting."
    "Can't believe she's gushing over my art, but what she's saying does make some kind of sense."
    jacklyn smile "Honestly, you've got it."
    jacklyn smile "Channeling your rage into the colors and ripping them across the canvas. Makes my left foot all moist."
    mc "Thanks, I guess. It did feel good letting it all out."
    jacklyn neutral "I know what you're saying."
    jacklyn neutral "Floodgates were meant to be shattered."
    jacklyn smile "I'll tell you what. You, me, and half a dozen spray cans..."
    if "jacklyn" in game.pc.phone_contacts:
      jacklyn excited_right "You have my number. Let's go out and make a statement!"
      "Not sure what that means, but it sounds illegal."
      "Totally my cup of tea."
      $quest.jacklyn_art_focus.finish()
    else:
      jacklyn excited_right "Give me your number and we'll make a statement together."
      $mc.add_phone_contact("jacklyn")
      "Not sure what that means, but it sounds illegal."
      "Totally my cup of tea."
      $quest.jacklyn_art_focus.finish()
    $achievement.inspired.unlock()
    hide jacklyn with Dissolve(.5)
  else:
    show misc canvas canvas with Dissolve(.5)
    "Using my [item.title_lower] instead of a brush is totally avant-garde."
    "The strokes of a brush wouldn't impress anyone. Least of all an art connoisseur like [jacklyn]."
    "True art requires finesse..."
    show misc canvas tier1_stage1 with Dissolve(.5)
    "Innovation..."
    show misc canvas tier1_stage2 with Dissolve(.5)
    "And most importantly... pretentious bullshit."
    show misc canvas tier1_stage3 with Dissolve(.5)
    "Mmm... there we are. A masterpiece."
    "[jacklyn] will surely gush like a fountain over this."
    hide misc canvas tier1_stage3 with Dissolve(.5)
    $quest.jacklyn_art_focus["show_jacklyn"] = "tierone"
    $quest.jacklyn_art_focus.advance("showjacklyn")
  return

label jacklyn_quest_art_focus_jacklyn:
  if quest.jacklyn_art_focus["show_jacklyn"] == "brush":
    show jacklyn neutral with Dissolve(.5)
    mc "Excuse me. I've completed the task."
    jacklyn excited "Wicked. Let's have a look at it!"
    mc "What do you think?"
    jacklyn annoyed "It's..."
    jacklyn annoyed "A bit plain, don't you think?"
    mc "I put in a lot of hard work."
    jacklyn cringe "If you want to be an artist with that style of yours, you need to at least learn how to draw genitals."
    jacklyn cringe "Sorry, but try again."
    $quest.jacklyn_art_focus["try_again"] = True
    $quest.jacklyn_art_focus.advance("artist")
  else:
    show jacklyn thinking with Dissolve(.5)
    mc "Hey, I painted this thing..."
    jacklyn excited "That's crack on a stick!"
    jacklyn excited "What did you paint?"
    mc "Just err... went with my gut."
    mc "I guess it's meant as avant-garde meets surrealism meets abstractism."
    jacklyn thinking "It's a powerful combination. Okay, let's see what you've got."
    mc "..."
    jacklyn thinking "..."
    jacklyn smile "It's not toilet water, that's for sure."
    mc "Thanks, I guess."
    jacklyn neutral "You've got some spark."
    jacklyn neutral "I was wrong about you, [mc]."
    jacklyn smile "You've upped your bang from last year. You're welcome to join my special class."
    show jacklyn smile at move_to(.25)
    menu(side="right"):
      extend ""
      "\"I'd like to find my inner Picasso.\"":
        show jacklyn smile at move_to(.5)
        mc "I'd like to find my inner Picasso."
        $jacklyn.lust+=1
        jacklyn excited "Sick. Okay, I'll fill you in about the assignments later."
        hide jacklyn with Dissolve(.5)
        $quest.jacklyn_art_focus.finish()
      "\"I just did it to prove you wrong. I don't really care for art.\"":
        show jacklyn smile at move_to(.5)
        mc "I just did it to prove you wrong. I don't really care for art."
        jacklyn excited "That has been the sentiment of many great artists throughout history."
        $jacklyn.love+=1
        jacklyn excited_right "Yet here we are, admiring their work."
        hide jacklyn with Dissolve(.5)
        $quest.jacklyn_art_focus.finish("done_nofocus")
  return


label jacklyn_contact_second_chance:
  #SFX: door creaking
  $flora.unequip("pants")
  show flora concerned at appear_from_left
  flora concerned "[mc]!"
  mc "What did I do now?"
  flora excited "Nothing, but I need your phone."
  mc "Why, if I may ask?"
  flora eyeroll "My battery is dead and I need to make a call."
  show flora eyeroll at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Charge it.\"":
      show flora eyeroll at move_to(.5)
      mc "Charge it."
      $flora.love-=1
      $flora.lust+=1
      flora angry "I've lost my charger!"
      mc "Not sure how that's my problem."
      flora annoyed "I'm going to poison..."
      flora confused "..."
      mc "What?"
      show flora annoyed with vpunch:
        linear .25 zoom 1.5 yoffset 750
      show flora annoyed_texting:
        linear .25 zoom 1 yoffset 0
      mc "Hey! Stealing is illegal!"
      flora annoyed_texting "Don't care. What's your password?"
      mc "You'll never be able to guess—"
      flora eyeroll_texting "Gross. I knew it was something perverted."
      flora annoyed_texting "I need to make a call. Be right back."
      show flora annoyed_texting at disappear_to_left
      mc "Damn it, [flora]!"
      #SFX: door creaking
      "Well, she better not use up all the battery. I have a Snake high score to beat."
      "What's so urgent anyway?"
      "Guy problems?"
      "Now that I think of it, didn't she start talking to her future husband during the senior year of high school?"
      "Maybe he's telling her about his stupid BMW."
      "God, that guy is... was... will be a prick."
      #SFX: door creaking
      show flora confident at appear_from_left
      flora confident "Thanks for letting me borrow your phone."
      mc "Right. That's what happened."
      flora laughing "That's how I remember it."
      mc "So, what's the good news?"
      flora smile "Good news?"
      mc "When you first got in here, you were all grumpy. Now, you're glowing."
      flora blush "I don't know what you're talking about!"
    "\"Sure thing.\"":
      show flora eyeroll at move_to(.5)
      mc "Sure thing."
      flora skeptical "You're being suspicious."
      mc "How am I being suspicious?"
      flora skeptical "Why aren't you giving me a hard time?"
      mc "Felt like being mature today."
      mc "Here you go."
      flora annoyed_texting "What did you do with it?"
      mc "Nothing."
      mc "Why would I do something to my own phone?"
      mc "I even unlocked it for you."
      flora eyeroll_texting "This is weird. You're acting weird."
      mc "Look, I'm just trying to be nice for a change."
      mc "If you don't want to borrow the phone, that's okay."
      mc "If you do, feel free to step outside for some privacy."
      $flora.love+=1
      $flora.lust-=1
      flora annoyed_texting "Okay. Thanks for being nice, but it is a bit creepy. I don't know if I like it."
      show flora annoyed_texting at disappear_to_left
      #SFX: door creaking
      "I wonder if I'll ever get her to fully trust me."
      "After everything I've put her through. Every prank and dumb joke. Every trick and childish conflict."
      "The answer is probably never."
      "That's a bit of a tough pill to swallow."
      "Hopefully, I can at least make her less resentful."
      #SFX: door creaking
      show flora confident at appear_from_left
      pause(1)
      mc "Something good happen?"
      flora confident "Maybe."
      mc "Okay, keep your secrets."
      flora blush "I think I will!"
      flora blush "Thanks for letting me borrow your phone!"
      mc "No problem."
      "She looks like she just won the lottery..."
  flora blush "Anyway, gotta go!"
  show flora blush at disappear_to_left
  #SFX: door creak
  "That's highly suspicious. I wonder who she called."
  "..."
  "Let's check the call history..."
  "..."
  "Hmm... I don't recognize the number."
  "Might as well try calling it."
  "There are quite a few people in [flora]'s life that I've always wanted to have a word with."
  "Maybe I can even stop her from falling in love with that BMW douchebag she'll call fiancé in a few years."
  $set_dialog_mode("phone_call","hidden_number")
  "..."
  "{i}Calling...{/}" #notice italics
  "..."
  "{i}Calling...{/}" #notice italics
  "..."
  $set_dialog_mode("phone_call","jacklyn")
  jacklyn "[flora]? Already squeezing your thighs for me?"
  $set_dialog_mode("")
  "Oh shit. It's [jacklyn]. I guess [flora] is still trying to impress her."
  $set_dialog_mode("phone_call","jacklyn")
  mc "Err, no... this is [mc]. [flora] just used my phone."
  jacklyn "Ah, that flipped my think-box for a moment."
  jacklyn "So, what's rustling your jimmies today, [mc]?"
  mc "Err..."
  $set_dialog_mode("")
  "Crap. Should've thought this through."
  $set_dialog_mode("phone_call","jacklyn")
  mc "I don't know... I'm sorry for calling."
  jacklyn "Hey, don't be such a cobain."
  jacklyn "Life is more than just suffering."
  jacklyn "Text me later and I'll give you a blowjob."
  jacklyn "Toodles."
  $set_dialog_mode("")
  "She hung up."
  "I'm sure she doesn't mean a real blowjob."
  "Maybe I'll try texting her later."
  $mc.add_phone_contact("jacklyn")
  return
