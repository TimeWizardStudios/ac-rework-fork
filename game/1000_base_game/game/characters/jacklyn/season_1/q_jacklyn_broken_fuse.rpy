label jacklyn_quest_jacklyn_broken_fuse_start:
  show jacklyn thinking with Dissolve(.5)
  jacklyn thinking "The definition of HD trash!"
  mc "Excuse me?"
  jacklyn angry "The lightswitch, bro. It's not working!"
  "For a moment there, I thought she was talking to me. Well, it wouldn't have been the first time someone called me trash."
  mc "Hmm... that's odd."
  jacklyn thinking "Swapped light bulbs, opened up the switch, and gave a prayer to Satan. Nothing's working."
  mc "Why do you need to turn off the lights?"
  jacklyn neutral "Simple. Light and darkness play an important role in art. I need to be able to adjust the lighting."
  "There used to be a fuse box in the big hall on this floor, maybe something's broken there."
  "At least, that was the case when I was here last time."
  mc "Have you checked the fuse box?"
  jacklyn annoyed "No, I don't feel like sticking my fingers into that thing."
  jacklyn annoyed "Normally, I'd ask the janitor to fix it, but I can't seem to find her anywhere."
  "Maybe there's a chance of earning a better grade or impressing her."
  hide jacklyn with Dissolve(.5)
  $quest.jacklyn_broken_fuse.start()
  $quest.jacklyn_broken_fuse.advance("investigate_fusebox")
  return

label jacklyn_quest_jacklyn_broken_fuse_end:
  show jacklyn excited with Dissolve(.5)
  jacklyn excited "The lightswitch is working again!"
  jacklyn excited "If you did this — sick."
  show jacklyn excited at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Yeah, I repaired a faulty wire.\"":
      show jacklyn excited at move_to(.5)
      $mc.intellect+=1
      mc "Yeah, I repaired a faulty wire."
      jacklyn smile "Back pats! Actually, gold."
    "\"I know how important lighting is.\"":
      show jacklyn excited at move_to(.5)
      mc "I know how important lighting is."
      mc "And I firmly believe that the best art exists between light and shadow."
      #$ +Art
      $jacklyn.love+=1
      jacklyn smile "Word. Without contrasts, you can't capture reality's duality."
    "\"It seems like your prayers to Satan worked.\"":
      show jacklyn excited at move_to(.5)
      mc "It seems like your prayers to Satan worked."
      $jacklyn.lust+=2
      jacklyn cringe "I knew that horny son would be after my ass."
      jacklyn cringe "Devil worship always ends in anal."
      mc "..."
      jacklyn excited "Loosen up, Eastwood. It's a joke."
      mc "You sound just like [isabelle] when you say that."
      jacklyn excited "Yeah? Well, she's hot stuff."
      jacklyn laughing "Not that I dabble or anything."
  jacklyn thinking "Anyway, since you helped me with this, maybe you can help me decide on another thing?"
  mc "I guess."
  jacklyn smile "Badass."
  jacklyn smile "I need a nude model for some of the senior classes this year. Who would you recommend for it?"
  label jacklyn_quest_jacklyn_broken_fuse_end_choice:
  if school_art_class["nude_model"]:
    show jacklyn angry at move_to(.25)
  else:
    show jacklyn smile at move_to(.25)
  $item_count = mc.owned_item_count("compromising_photo")
  menu(side="right"):
    extend ""
    "\"How about [mrsl]?\"":
      if school_art_class["nude_model"]:
        show jacklyn angry at move_to(.5)
      else:
        show jacklyn smile at move_to(.5)
      mc "How about [mrsl]?"
      jacklyn excited "She's at the top of my list too. She volunteered."
      mc "Whoa, really?"
      "Ever since she ditched those cardigans, she's become a lot more frivolous."
      jacklyn smile "Def."
      $school_art_class["nude_model"] = "mrsl"
    "\"I recommend myself.\"" if not school_art_class["nude_model"]:
      show jacklyn smile at move_to(.5)
      mc "I recommend myself."
      jacklyn thinking "No offense, hombre, but it needs to be an adult."
      mc "But I'm eighteen."
      jacklyn angry "It also needs to be aesthetically pleasing."
      "Ouch. Right in the jugular."
      $school_art_class["nude_model"] = "not_mc_lol"
      jump jacklyn_quest_jacklyn_broken_fuse_end_choice
    "?mc.owned_item('compromising_photo')@|{image=items compromising_photo}|\"I think the school nurse would be perfect for this.\"":
      if school_art_class["nude_model"]:
        show jacklyn angry at move_to(.5)
      else:
        show jacklyn smile at move_to(.5)
      mc "I think the school nurse would be perfect for this."
      jacklyn thinking "She's not on my list."
      mc "It's fine, I think she'd love to do it. Probably even for free. Just tell her I recommended it."
      jacklyn smile "Awesome, awesome. Okay, I'll ask her."
      $school_art_class["nude_model"] = "nurse"
    "?jacklyn.lust>=1@[jacklyn.lust]/1|{image= jacklyn contact_icon}|{image= stats lust_3}|\"I think you'd be the best candidate.\"":
      if school_art_class["nude_model"]:
        show jacklyn angry at move_to(.5)
      else:
        show jacklyn smile at move_to(.5)
      mc "I think you'd be the best candidate."
      jacklyn laughing "Yeah? You think?"
      mc "Yes! And it would be easier for you to grade the paintings as well."
      jacklyn excited "Word! All right, it's a done deal."
      $school_art_class["nude_model"] = "jacklyn"
  $time = "16:00 to 18:00" if persistent.time_format=="24h" else "04:00 PM to 06:00 PM"
  jacklyn smile "If you're interested in learning the human curves and anatomy, the nude model will be available here from [time]."
  hide jacklyn with Dissolve(.5)
  $quest.jacklyn_broken_fuse.finish()
  return

label jacklyn_quest_jacklyn_broken_fuse_model_mrsl:
  $mrsl["outfit_stamp"] = mrsl.outfit
  $mrsl.outfit = {}
  if mrsl["interacted_posed"]:
    show mrsl flirty with Dissolve(.5)
    jump jacklyn_quest_jacklyn_broken_fuse_model_mrsl_end
  show mrsl confident with Dissolve(.5)
  mrsl confident "Good afternoon, [mc]!"
  mrsl confident "Are you here for me?"
  show mrsl confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I'm here for the art.\"":
      show mrsl confident at move_to(.5)
      if not quest.jacklyn_broken_fuse["mrsl_stat_gained"]:
        $mc.love+=1
      mc "I'm here for the art."
      mrsl thinking "Well, that's fine! That's what I meant."
    "\"I'm always here for you, [mrsl]!\"":
      show mrsl confident at move_to(.5)
      mc "I'm always here for you, [mrsl]!"
      if not quest.jacklyn_broken_fuse["mrsl_stat_gained"]:
        $mrsl.lust+=1
      mrsl excited "I'm glad."
  mrsl excited "Let's get started?"
  mc "Yeah!"
  label jacklyn_quest_jacklyn_broken_fuse_model_mrsl_end:
  mrsl flirty "What position would you like me in?"
  "Bent over a table, doggy, missionary... anything goes."
  mc "Err... don't feel like you have to pose. I like to have a moving model for my art."
  "Not that there's going to be much art done..."
  mrsl flirty "Okay, then!"
  mrsl thinking "So, how does it feel to be back?"
  mc "Hmm?"
  mrsl excited "In school, I mean. Did you have a good summer?"
  "For a moment I thought she was talking about time travel there."
  "Need to get my head back in the game."
  mc "It felt a lot longer than just a summer. Never thought I would miss any part of it, but I guess I did."
  mrsl sad "I feel the same way at the end of each school year. Seniors leaving... it's sad."
  mrsl sad "It's tough seeing people leave, but life goes on."
  mc "I love that pose! Please hold it for a bit!"
  "Almost as much as I love her piercings..."
  hide mrsl with Dissolve(.5)
  $quest.jacklyn_broken_fuse["mrsl_stat_gained"] = True
  $mrsl["interacted_posed"] = True
  $mrsl.outfit = mrsl["outfit_stamp"]
  return

label jacklyn_quest_jacklyn_broken_fuse_model_nurse:
  $nurse["outfit_stamp"] = nurse.outfit
  $nurse.outfit = {}
  show nurse concerned with Dissolve(.5)
  if nurse["interacted_posed"]:
    jump jacklyn_quest_jacklyn_broken_fuse_model_nurse_end
  mc "What's wrong?"
  nurse concerned "Oh, goodness. I'm so embarrassed."
  mc "What do you have to be embarrassed about?"
  nurse concerned "Being naked... on display... for everyone..."
  nurse concerned "Why did you make me sign up?"
  show nurse concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"You're absolutely gorgeous.\"":
      show nurse concerned at move_to(.5)
      mc "You're absolutely gorgeous."
      if not quest.jacklyn_broken_fuse["nurse_stat_gained"]:
        $nurse.love+=1
      nurse blush "That's so kind of you to say..."
      mc "It's the truth."
      mc "You're dummy thicc, and there's no one I'd rather learn artistic anatomy from."
      nurse blush "Oh, my. Thank you, I think...?"
    "\"Isn't this what you're craving?\"":
      show nurse concerned at move_to(.5)
      mc "Isn't this what you're craving?"
      nurse blush "Oh, I don't know about that..."
      mc "Coy to the very end, huh?"
      mc "Well, enjoy it."
      nurse thinking "Are you done with me then?"
      mc "What do you mean?"
      nurse thinking "No more blackmailing?"
      mc "Done? We're just getting started."
      if not quest.jacklyn_broken_fuse["nurse_stat_gained"]:
        $nurse.lust+=1
      nurse surprised "Heavens!"
      mc "Now, on to the art..."
    "\"I didn't make you do anything... it was simply a recommendation.\"":
      show nurse concerned at move_to(.5)
      mc "I didn't make you do anything... it was simply a recommendation."
      nurse annoyed "I must've misinterpreted it..."
      mc "Probably! I didn't know you were so eager to take your clothes off and pose for the seniors."
      nurse thinking "That's not... I mean, I didn't! Oh, my."
      mc "It's fine. Everyone has their kinks."
      mc "Being an exhibitionist is nothing to be ashamed of."
      if not quest.jacklyn_broken_fuse["nurse_stat_gained"]:
        $mc.intellect+=1
      mc "Unless you're into that, as well!"
      if not quest.jacklyn_broken_fuse["nurse_stat_gained"]:
        $nurse.love+=1
      nurse afraid "I don't know what you're talking about!"
      mc "I don't believe you. But very well, let's get started on the art instead."
  nurse sad "I've never done any sort of modelling before. I don't know how to pose."
  mc "That's what I like the most. You're genuine and vulnerable in your nudity."
  mc "That's where true art can be drawn from, not some practised poses."
  nurse thinking "Oh..."
  label jacklyn_quest_jacklyn_broken_fuse_model_nurse_end:
  nurse concerned "What kind of pose would you like me to do?"
  show nurse concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I'd like you to smile.\"":
      show nurse concerned at move_to(.5)
      mc "I'd like you to smile."
      nurse smile "..."
      mc "Great! Now hold that pose, please!"
      mc "This is kinda nice, don't you think?"
      nurse smile "It's a little embarrassing, but I'll live..."
    "\"I'd like you to look like you've just been caught masturbating.\"":
      show nurse concerned at move_to(.5)
      mc "I'd like you to look like you've just been caught masturbating."
      nurse annoyed "That... is an odd request..."
      mc "Give a try!"
      nurse afraid "Like this?"
      mc "Yes! Now, stay like that, please!"
    "\"How about some of that natural vulnerability?\"":
      show nurse concerned at move_to(.5)
      mc "How about some of that natural vulnerability?"
      nurse neutral "Like this?"
      mc "Yes, that's perfect!"
      mc "Just hold still now for a bit so I can draw you..."
      nurse neutral "I'm a bit nervous."
      mc "You're doing great!"
  hide nurse with Dissolve(.5)
  $quest.jacklyn_broken_fuse["nurse_stat_gained"] = True
  $nurse["interacted_posed"] = True
  $nurse.outfit = nurse["outfit_stamp"]
  return

label jacklyn_quest_jacklyn_broken_fuse_model_jacklyn:
  $jacklyn["outfit_stamp"] = jacklyn.outfit
  $jacklyn.outfit = {}
  if jacklyn["interacted_posed"]:
    show jacklyn laughing with Dissolve(.5)
    jacklyn laughing "Which pose do you like the best?"
    jump jacklyn_quest_jacklyn_broken_fuse_model_jacklyn_end
  show jacklyn excited with Dissolve(.5)
  jacklyn excited "How's the bell ringing today?"
  mc "Pretty good, I guess."
  jacklyn excited "Wicked."
  jacklyn laughing "Have you come to do art or just look at my tits?"
  mc "Err... art, of course."
  jacklyn smile "Bien sur! All right, how about this pose?"
  jacklyn smile_right "I call it, \"Playing with your lasso, Picasso?\""
  mc "Heh, that's a good name."
  jacklyn smile_right "[isabelle] already drew me in this pose."
  mc "Did she do well?"
  jacklyn smile "Yeah, she fucked that muse good."
  mc "Aren't you the muse?"
  jacklyn thinking "I meant in the metaphorical sense. She's cute and all, but I don't dabble."
  mc "Right."
  jacklyn excited "How about this one? [maya] liked this pose."
  jacklyn laughing "I call it, \"Come and get it, Monet!\""
  jacklyn laughing "Which one do you like the best?"
  label jacklyn_quest_jacklyn_broken_fuse_model_jacklyn_end:
  show jacklyn laughing at move_to(.25)
  menu(side="right"):
    extend ""
    "\"'Playing with your lasso, Picasso?'\"":
      show jacklyn laughing at move_to(.5)
      if not quest.jacklyn_broken_fuse["jacklyn_stat_gained"]:
        $mc.intellect+=1
      mc "\"Playing with your lasso, Picasso?\""
      jacklyn smile_right "The sophisticated choice. I can dig that."
    "\"'Come and get it, Monet!'\"":
      show jacklyn laughing at move_to(.5)
      if not quest.jacklyn_broken_fuse["jacklyn_stat_gained"]:
        $mc.charisma+=1
      mc "\"Come and get it, Monet!\""
      jacklyn excited_right "Cheeky. Very well!"
    "?jacklyn.lust>=2@[jacklyn.lust]/2|{image= jacklyn contact_icon}|{image= stats lust_3}|\"Neither, actually.\"":
      show jacklyn laughing at move_to(.5)
      mc "Neither, actually."
      jacklyn annoyed "What are you suggesting?"
      mc "I'd like you to do a pose that I call, \"Squeeze the hog, Van Gogh!\""
      jacklyn excited "Okay, what's that?"
      mc "You lean over and reach for that box."
      jacklyn laughing "A true artist always follows his own vision."
      jacklyn laughing "All right, then."
      jacklyn jacklynpose01n "How's that?"
      mc "That is perfection!"
      mc "Please hold that!"
      "..."
      "Well, it's hard to focus on the art now...."
      if not quest.jacklyn_broken_fuse["jacklyn_stat_gained"]:
        $jacklyn.lust+=1
      "Hopefully, [jacklyn] doesn't check the canvas."
  hide jacklyn with Dissolve(.5)
  $quest.jacklyn_broken_fuse["jacklyn_stat_gained"] = True
  $jacklyn["interacted_posed"] = True
  $jacklyn.outfit = jacklyn["outfit_stamp"]
  return
