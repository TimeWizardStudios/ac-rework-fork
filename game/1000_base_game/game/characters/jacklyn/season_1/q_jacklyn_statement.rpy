label jacklyn_statement_text_same_room:
  "As much as I'd like to see [jacklyn]'s face when I text her, I don't want her to see mine."
  "She doesn't get embarrassed anyway, and I'm a total tomato."
  return

label jacklyn_statement_text_start:
  "It's not every day you get the hot teacher assistant's number. Better not mess this up."
  "Well, she does seem like the person who likes things messed up. Let's see here..."
  $set_dialog_mode("phone_message","jacklyn")
  menu(side="middle"):
    "\"This is a booty text.\"":
      mc "This is a booty text."
      jacklyn "Lookin' for a pegging, cowboy?"
      mc "Sorry, autocorrect got me... I meant that this is a beautiful time to sext."
      jacklyn "Are you trying to flatter my scarlett johansson?"
      $set_dialog_mode()
      "What the fuck does that even mean?"
      $set_dialog_mode("phone_message","jacklyn")
      mc "Yes?"
      jacklyn "Crystal! Meet me outside the school."
      $set_dialog_mode()
      "Not sure what's on the menu, but it's definitely not plain steak."
      $quest.jacklyn_statement["jacklyn_text"] = "booty"
      $quest.jacklyn_statement.start("meetjacklyn")
    "?jacklyn.love>=3@[jacklyn.love]/3|{image= jacklyn contact_icon}|{image=stats love}|\"Hey, wanna go on a date?\"":
      mc "Hey, wanna go on a date?"
      $jacklyn.love+=1
      jacklyn "I like the arrowhead. And I won't say no to a free filler."
      $set_dialog_mode()
      "Seriously? That easy?"
      "Never thought I'd get to stage two... what now?"
      $set_dialog_mode("phone_message","jacklyn")
      mc "Can I cook you dinner?"
      jacklyn "That's baby gold!"
      mc "Does that mean yes?"
      jacklyn "What else would it mean?"
      mc "Just making sure..."
      mc "How about my place tomorrow night?"
      jacklyn "Wicked."
      $set_dialog_mode()
      "Wow, okay. Never thought that'd work. How do I get [flora] and [jo] out of the house for a whole evening?"
      "Better get on that first!"
      $quest.jacklyn_statement.start("remove")
    "\"How's the gold standard?\"":
      mc "How's the gold standard?"
      $jacklyn.lust+=2
      jacklyn "About eleven past midnight. You?"
      $set_dialog_mode()
      "Err... I already regret trying to speak her language..."
      $set_dialog_mode("phone_message","jacklyn")
      mc "It turned silver for the fall."
      jacklyn "Ouch! My condolences. Would a blowjob improve the goings?"
      mc "Yes?"
      jacklyn "Cool. Your fashion sense is the definition of minimalism. Your haircut is a generation ahead of its time."
      $set_dialog_mode()
      "That's a nice way of putting things..."
      $set_dialog_mode("phone_message","jacklyn")
      mc "Thanks. Where do we meet?"
      jacklyn "Meet?"
      mc "For the blowjob?"
      jacklyn "Darjeeling, darling... you've fluffed your mind-juices!"
      $set_dialog_mode()
      "Hmm... she probably didn't mean a literal blowjob."
      $set_dialog_mode("phone_message","jacklyn")
      jacklyn "Let's rendezvous outside the school."
      $set_dialog_mode()
      "Or did she?"
      $quest.jacklyn_statement["jacklyn_text"] = "gold"
      $quest.jacklyn_statement.start("meetjacklyn")
    "Overthink it some more":
      pass
  $set_dialog_mode()
  return

label jacklyn_statement_remove_jo:
  show jo confident_cup with Dissolve(.5)
  jo confident_cup "Hey, kiddo! How's it going?"
  mc "I wanted to ask you for a favor."
  jo neutral_cup "I'm listening."
  "[jo] always gets super serious whenever I say that..."
  "Not sure why."
  mc "I have a date tomorrow and I was wondering if I could have the house to myself? I'm cooking her dinner."
  jo concerned_cup "A date?! What are you saying?"
  mc "Yeah..."
  "Wish she wouldn't sound so surprised, but who can blame her?"
  jo confident_cup "Can I ask who it is?"
  mc "Jack—"
  jo smile_cup "Jack! Well, let me tell you... I always suspected you liked boys."
  mc "No, it's actually—"
  jo smile_cup "I want you to know that I'm fine with that! As long as you're happy, I don't care."
  jo smile_cup "Of course, I'm a bit sad that there won't be any mini-[mc]s running around the house..."
  mc "[jo]..."
  jo confident_cup "But there's always adoption or a surrogate mother! We live in the future!"
  jo confident_cup "Bottom line is, love is blind and I fully support you."
  mc "[jo]! It's [jacklyn]! I'm not into boys!"
  jo concerned_cup "[jacklyn]? The teacher assistant? That's against school regulations."
  show jo concerned_cup at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Please!\"":
      show jo concerned_cup at move_to(.5)
      mc "Please!"
      jo neutral_cup "Absolutely not."
      mc "Please, [jo]! This might be my only chance to get a real girlfriend!"
      "She knows it's true... the outrage can't hide the pity in her eyes."
      jo neutral_cup "Hmm... I don't know..."
    "?mc.intellect>=4@[mc.intellect]/4|{image=stats int}|\"Actually, it's not.\"":
      show jo concerned_cup at move_to(.5)
      mc "Actually, it's not."
      mc "I looked it up — the rules specifically state that students can't date teachers."
      mc "It doesn't say anything about teacher's assistants, though..."
      mc "Probably because they don't grade anyone, so there's no conflict of interest."
      jo neutral_cup "I would've expected this from [flora], but not you..."
      $jo.love+=2
      jo smile_cup "I'm proud of you for doing your research."
      jo confident_cup "It's a sign of a good student and of adulthood!"
      jo neutral_cup "But hmm..."
  jo confident_cup "Isn't she a little old for you?"
  mc "She's like twenty-something!"
  jo neutral_cup "Isn't she a bit... you know?"
  show jo neutral_cup at move_to(.25)
  menu(side="right"):
    extend ""
    "\"A bit what?\"":
      show jo neutral_cup at move_to(.5)
      mc "A bit what?"
      jo confident_cup "You know. A little bit..."
      mc "What?"
      jo confident_cup "She's a little bit a free spirit, that one..."
      jo smile_cup "Her wardrobe is something else entirely."
      show jo smile_cup at move_to(.75)
      menu(side="left"):
        extend ""
        "\"I like her style! I think punk girls are cute.\"":
          show jo smile_cup at move_to(.5)
          mc "I like her style! I think punk girls are cute."
          jo neutral_cup "She's of course allowed to dress however she likes..."
          $jo.love-=1
          jo neutral_cup "I just don't think she's setting a very good example for the students."
          mc "I like how colorful she is."
          mc "She's a good example of not caring what people think."
          jo smile_cup "We just have to agree to disagree on that."
          mc "Will you at least give us some privacy tomorrow night?"
          jo confident_cup "I'll be out of the house, but I don't approve of this."
        "\"Beggars can't be choosers...\"":
          show jo smile_cup at move_to(.5)
          mc "Beggars can't be choosers..."
          jo concerned_cup "Don't sell yourself short, honey!"
          mc "I'm just being realistic."
          jo smile_cup "I think you're a catch for someone like her."
          jo neutral_cup "I'm not going to hide that I don't approve of her style and mannerisms..."
          mc "Luckily, I'm the one who has a date with her."
          $jo.love+=1
          jo smile_cup "I suppose you're right... that's a grown-up way of looking at it."
          jo smile_cup "All right, I'll be out of the house tomorrow night."
    "\"She's perfect.\"":
      show jo neutral_cup at move_to(.5)
      mc "She's perfect."
      jo smile_cup "I remember my first love too..."
      jo smile_cup "Just be careful, okay? She might be a heartbreaker."
      mc "Thanks, [jo]. It's just our first date, but I'll keep it in mind..."
      $jo.love+=1
      jo confident_cup "Good, but make sure you use protection... you know that, though!"
      mc "[jo]!"
      jo smile_cup "I know you're responsible when it comes to these things."
      jo smile_cup "I'll make sure to be out of the house tomorrow night so you guys have some privacy."
    "\"She's a total slut, and I love it!\"":
      show jo neutral_cup at move_to(.5)
      mc "She's a total slut, and I love it!"
      $jo.love-=1
      jo concerned_cup "I don't want you to talk about women like that!"
      jo concerned_cup "I may not approve of her fashion choices, but no one deserves to be called that."
      mc "I don't think she would mind being called that..."
      jo neutral_cup "I don't want to hear you say it again."
      jo neutral_cup "It's disrespectful. You should know better."
      mc "Sorry, [jo]... I won't say so again."
      jo smile_cup "Good. You better treat her well."
  $quest.jacklyn_statement["jo_removed"] = True
  if quest.jacklyn_statement["flora_removed"]:
    hide jo with Dissolve(.5)
    "Okay, with everyone out of the house tomorrow... time to start preparing the dinner!"
    $quest.jacklyn_statement.advance("cook")
  else:
    jo confident_cup "Make sure you talk to [flora] too."
    hide jo with Dissolve(.5)
  return

label jacklyn_statement_remove_flora:
  show flora skeptical with Dissolve(.5)
  flora skeptical "You have that look on your face again..."
  mc "What look?"
  flora skeptical "Like you're going to ask me for some weird favor."
  mc "It's nothing weird this time!"
  flora eyeroll "That's what you said last time when asking me to not touch the bathroom sink."
  flora eyeroll "Then you proceeded to grow a small plantation of algae."
  "Shit. She remembers that?"
  mc "In my defense, that was for a science project."
  flora annoyed "No, it wasn't! I even checked with the teacher!"
  "Crap, I forgot about that."
  mc "In my defense, that plantation created a lot of new oxygen... it was a good cause against global warming!"
  flora eyeroll "Whatever. If it's something weird, you can forget it."
  mc "I was wondering if you could stay out of the house tomorrow night?"
  mc "I've got a date."
  flora laughing "Nice joke!"
  mc "What? Is that so strange?"
  flora blush "Seriously, sometimes you say the funniest things!"
  mc "I'm serious! I have a date with [jacklyn]."
  flora worried "..."
  flora worried "No, you don't."
  "[flora] looks really angry. Angrier than usual..."
  mc "What do you mean?"
  flora skeptical "You don't have a date with her."
  mc "I do! Wanna see the texts?"
  flora embarrassed "...You're texting her?"
  mc "Yeah, we have a date tomorrow night! That's why I'd like you out of the house then."
  #if the player took the Love Route in flora_bonsai:
  $flora.love-=2
  flora sad "Fine..."
  mc "Seriously?"
  flora sad "Yes... I'll be at my internship anyway..."
  hide flora with Dissolve(.5)
  "Hmm... it's not like [flora] to agree so readily."
  "Well, maybe she's starting to grow up too?"
  $quest.jacklyn_statement["flora_removed"] = True
  if quest.jacklyn_statement["jo_removed"]:
    "Okay, with everyone out of the house tomorrow... time to start preparing the dinner!"
    $quest.jacklyn_statement.advance("cook")
  else:
    "Okay, just gotta talk to [jo] now. Hopefully, she'll be happy that I finally have a date with someone..."
  return

label jacklyn_statement_meetjacklyn:
  show jacklyn neutral with Dissolve(.5)
  if quest.jacklyn_statement["jacklyn_text"] == "booty":
    jacklyn neutral "Hand over the valuables."
    mc "..."
    mc "What?"
    jacklyn smile "You said you were trying to flatter my scarlett johansson."
    jacklyn smile "Now's your chance."
    "Fuck. What does she mean?"
    show jacklyn smile at move_to(.25)
    menu(side="right"):
      extend ""
      "\"I'm not much of a sailor, but fishnets certainly catch my attention.\"":
        show jacklyn smile at move_to(.5)
        mc "I'm not much of a sailor, but fishnets certainly catch my attention."
        jacklyn angry "Are you hitting on me?"
        "Fuck. Wasn't that what it meant?"
        mc "Err... not really. Just setting myself up to flatter your scarlett johansson..."
        jacklyn smile "Jokes. I'm listening."
        mc "I saw your scarlett johansson when you bent over to look at my past artwork."
        mc "It was quite hot."
        $jacklyn.lust-=1
        jacklyn thinking "You've put me in a maze, Daedalus."
        mc "Fine... I'm sorry. I actually don't know what it means to flatter your scarlett johansson."
        jacklyn neutral "Why didn't you just say so?"
        mc "I guess I was trying to act cool..."
        jacklyn angry "Stop acting, it's slug juice."
        mc "Sorry..."
        jacklyn angry "I'd fuck your ass if I was in the mood."
        "The... what now? Must be another metaphor... god, it's so hard to understand [jacklyn] sometimes."
        jacklyn neutral "You'd probably let me."
        jacklyn smile "Dinner at your place tomorrow night. You're cooking for me."
        mc "A date?"
        jacklyn smile_right "We'll see."
        mc "Okay!"
        jacklyn neutral "Okay. Got some buzz buzz to finish. Kisses."
        show jacklyn neutral at disappear_to_right
        "Hmm... not sure how that happened or if she's actually planning to fuck me..."
        "But I'm not going to say no to a date with a hot girl."
        "Best find a way to get [jo] and [flora] out of the house."
      "?mc.intellect>=6@[mc.intellect]/6|{image=stats int}|\"Honestly, I'd like {i}you{/} to flatter my scarlett johansson...\"":
        show jacklyn smile at move_to(.5)
        mc "Honestly, I'd like {i}you{/} to flatter my scarlett johansson..."
        jacklyn laughing "Bit of a jokester, are you?"
        $jacklyn.lust+=3
        jacklyn excited "I didn't expect that, and I'm all for abrupt chaos."
        mc "Please, go ahead."
        jacklyn excited "I think you have an unusual spark of creativity in you, and I'd like to help you foster it."
        jacklyn laughing "Let's have dinner sometime?"
        mc "I'll think about it..."
        mc "Yes."
        jacklyn excited "Nascar!"
        mc "Yeah, didn't have to think long. Since you asked me out, how about I cook you dinner?"
        jacklyn laughing "Seems balanced."
        mc "Great! Meet me at my house tomorrow night."
        jacklyn excited_right "Wicked. Buzz buzz is calling. Laters."
        show jacklyn excited_right at disappear_to_right
        "Can't believe that worked. I'm a genius!"
        "Okay, I need to convince [jo] and [flora] to leave the house tomorrow..."
  else:
    jacklyn neutral "Still need that blowjob?"
    show jacklyn neutral at move_to(.75)
    menu(side="left"):
      extend ""
      "\"I think I do, yeah...\"":
        show jacklyn neutral at move_to(.5)
        mc "I think I do, yeah..."
        jacklyn thinking "All right, listen."
        jacklyn thinking "You didn't win the genetic lottery in terms of looks... but there's more to a person than that."
        jacklyn smile "You're funny and pretty resourceful, and you're far from stupid."
        jacklyn smile "You've also got that creative spark!"
        show jacklyn smile at move_to(.25)
        menu(side="right"):
          extend ""
          "\"Thanks, I needed to hear that...\"":
            show jacklyn smile at move_to(.5)
            mc "Thanks, I needed to hear that..."
            $jacklyn.love+=1
            jacklyn smile_right "We all do sometimes."
            "Damn, maybe this is my chance?"
            mc "You seem like someone who doesn't need positive reinforcement... you're super confident."
            mc "So, maybe I can return the favor by cooking you a meal?"
            jacklyn neutral "I mean, sure."
            mc "Really?"
            jacklyn thinking "Yeah, why would I decline a free meal?"
            mc "I suppose you're right. How about tomorrow night, my place?"
            jacklyn smile "Wicked."
            jacklyn smile "All right, I got some buzzing to do. Laters!"
            show jacklyn smile at disappear_to_right
            "Does this count as a date?"
            "It totally does, doesn't it?"
            "Crap. How do I get [jo] and [flora] out of the house?"
          "\"Thanks, but what about that blowjob?\"":
            show jacklyn smile at move_to(.5)
            mc "Thanks, but what about that blowjob?"
            $jacklyn.love-=1
            jacklyn annoyed "I just gave it to you..."
            jacklyn annoyed "If you want a real one, you have to at least buy me dinner first."
            mc "Done! My place tomorrow night! I'm cooking!"
            jacklyn laughing "Nascar! Fine... I won't say no to a free meal."
            jacklyn laughing "Catch you on the flipside."
            show jacklyn laughing at disappear_to_right
            "Okay, I didn't expect that to work..."
            "Now, how do I get [jo] and [flora] out of the house?"
      "\"Actually, I'd like to give you one!\"":
        show jacklyn neutral at move_to(.5)
        mc "Actually, I'd like to give you one!"
        jacklyn smile "Oh, yeah?"
        mc "You're super confident in who you are, and that's so hot."
        mc "Hotter than the mini-skirt and fishnets... although those help, of course."
        $jacklyn.lust+=2
        jacklyn laughing "Thank you! Not too shabby!"
        mc "I'd like to propose a real blowjob later, but first I want to cook you dinner."
        jacklyn excited_right "Huh! Well, if you don't mind getting on your knees and doing a bit of headbanging..."
        mc "We'll see who does that... how about tomorrow night?"
        jacklyn excited "We will see. All right, deal!"
        jacklyn excited "Got some buzzing to do. Laters."
        show jacklyn excited at disappear_to_right
        "Phew! Can't believe I pulled that off! I even sounded confident..."
        "There's something about [jacklyn] that just makes me ready to go."
        "The only problem now is getting [jo] and [flora] out of the house... hmm..."
  $quest.jacklyn_statement.advance("remove",silent=True)
  return

label jacklyn_statement_home_kitchen_cook:
  "Cooking for [jacklyn]..."
  "Hmm... what would she like?"
  "There's [flora]'s chili recipe, but that sounds a bit risky. What if [jacklyn] doesn't like lethally spicy food?"
  "What if she's a vegan?"
  "Maybe a salad?"
  menu(side="middle"):
    extend ""
    "Make a salad":
      "It's easy and tasty! Who doesn't like a good salad?"
      "Okay, let's see what's in the fridge..."
      $quest.jacklyn_statement.advance("fridge")
    "?flora.lust>=4@[flora.lust]/4|{image= flora contact_icon}|{image=stats lust}|Get [flora] to cook for you":
      "It might be best to hire a professional chef..."
      "Compared to me, that's what [flora] is."
      "She won't do it for free, though — so, best bring some hard cash."
      $quest.jacklyn_statement.advance("chef")
    "?mc.charisma>=4@[mc.charisma]/4|{image=stats cha}|It's a problem for later":
      "Honestly, [jacklyn]'s probably the type that likes spontaneity..."
      "Let's just see what tomorrow brings."
      $quest.jacklyn_statement["problem_for_later"] = True
      $quest.jacklyn_statement["day"] = game.day
      $quest.jacklyn_statement.advance("dinner")
  return

label jacklyn_statement_flora_ingredients:
  if home_kitchen["croutons_eaten"]:
    show flora eyeroll with Dissolve(.5)
    flora eyeroll "What?"
    mc "My tummy hurts..."
    flora confused "So?"
    mc "I ate all the croutons."
    flora sarcastic "Finally some karma for your stupidity!"
    mc "I need more croutons."
    flora annoyed "That'll be $20."
    show flora annoyed at move_to(.25)
    menu(side="right"):
      extend ""
      "?mc.money>=20@[mc.money]/20|{image=ui icon_money}|\"That's outrageous! Here you go.\"":
        show flora annoyed at move_to(.5)
        mc "That's outrageous! Here you go."
        $mc.money-=20
        flora sarcastic "Thanks! I'll pick some up after my internship."
        $home_kitchen["croutons_activated"] = True
        $flora['hidden_now'] = True
        $process_event("update_state")
        hide flora with Dissolve(.5)
      "\"I'd rather starve!\"":
        show flora annoyed at move_to(.5)
        mc "I'd rather starve!"
        flora confused "Well, then..."
        hide flora with Dissolve(.5)
  else:
    show flora excited with Dissolve(.5)
    mc "What are you so excited about?"
    flora concerned "I'm less excited now that you're here."
    mc "I tend to have that effect on women."
    flora confident "That's the first true thing you've said all week."
    mc "Isn't self-deprecation enough?"
    flora confident "Nope! You can't escape my scorn so easily."
    mc "I suppose I would've done the same to you."
    flora angry "You absolutely would have."
    mc "That's what I just said."
    flora neutral "I was confirming it."
    mc "Ugh. Anyway, do you know where I could get some croutons?"
    flora confident "How about the freaking store?"
    show flora confident at move_to(.75)
    menu(side="left"):
      extend ""
      "\"I'm too lazy and cheap for that...\"":
        show flora confident at move_to(.5)
        mc "I'm too lazy and cheap for that..."
        flora laughing "Ain't that the truth!"
        flora laughing "How do you think you'll ever get a girl with that attitude?"
        show flora laughing at move_to(.25)
        menu(side="right"):
          extend ""
          "\"Look who's talking... I don't exactly see you drowning in dicks.\"":
            show flora laughing at move_to(.5)
            mc "Look who's talking... I don't exactly see you drowning in dicks."
            $flora.lust+=2
            flora flirty "Who says I'm into dicks?"
            mc "..."
            "Oh, god. I always regret getting on this topic with [flora]."
            "Who knows what's going through her head when she stares into the distance with that coy smile on her lips?"
            "Quick, think of a different topic!"
            mc "Err... anyway, what were you so excited about?"
            flora blush "They wanted me for a special interview at my internship!"
            mc "An interview about what?"
            flora blush "I don't know! Isn't that super exciting?"
            mc "I guess..."
            mc "Isn't there a store outside that place?"
            flora thinking "Yeah. I guess I could get you those damn croutons..."
            flora flirty "Unless I drown in dicks on my way there!"
            mc "Ugh."
            "Why did I say that? It's such a disturbing image."
            flora laughing "Okay, gotta run. I'll leave the croutons in the kitchen."
            flora laughing "Later, gator!"
            show flora laughing at disappear_to_right
            pause(1)
            $home_kitchen["croutons_activated"] = True
            $flora['hidden_now'] = True
            $process_event("update_state")
          "\"Expensive dinners aren't a good foundation for a relationship, anyway...\"":
            show flora laughing at move_to(.5)
            mc "Expensive dinners aren't a good foundation for a relationship, anyway..."
            $flora.love+=1
            flora worried "I mean, you're not wrong... but it doesn't hurt to put in some effort."
            mc "Hmmm... I guess you're right."
            mc "What were you so excited about earlier, by the way?"
            flora blush "Oh! I just got a text from my internship! They want me for a special interview!"
            mc "That's awesome! Congratulations."
            flora blush "Thanks!"
            flora thinking "You know what? There's a store right outside... I'll pick up some croutons for you on the way home."
            mc "Really?"
            flora flirty "Of course!"
            mc "Why are you being so nice all of a sudden?"
            flora blush "Well, you were nice to me... so I thought I'd return the favor."
            mc "Uh... thanks, I guess..."
            "Not sure what I did, but as long as I get that bread..."
            flora laughing "No problem! I'll leave it in the kitchen!"
            flora laughing "Later, gator!"
            show flora laughing at disappear_to_right
            pause(1)
            $home_kitchen["croutons_activated"] = True
            $flora['hidden_now'] = True
            $process_event("update_state")
      "?mc.money>=13@[mc.money]/13|{image=ui icon_money}|\"Can you buy me a bag?\"":
        show flora confident at move_to(.5)
        $mc.love+=1
        mc "Can you buy me a bag?"
        flora annoyed "Go buy them yourself!"
        mc "But I don't feel like it..."
        flora sarcastic "Okay... if you pay me $10, I'll do it."
        mc "Are you serious? You always pick up stuff for [jo]!"
        flora eyeroll "Yeah, but unfortunately, you're not [jo]."
        mc "Ugh, fine... here you go."
        $mc.money-=10
        flora sarcastic "Thanks, but you still need to pay for the croutons as well."
        flora sarcastic "That would be $3 more, mister!"
        mc "..."
        "She's so annoying, I swear."
        $mc.money-=3
        flora sarcastic "Pleasure doing business with you."
        mc "I need them before tomorrow."
        flora confused "Yeah, I'll put them in the kitchen."
        flora confused "I'm going to my internship now and there's a store right outside..."
        flora sarcastic "I got a text a minute ago saying that I have to come in for a special interview!"
        mc "So... why the hell did I just pay you ten bucks to pick up the croutons?"
        flora eyeroll "That's the price for not asking about my day."
        flora sarcastic "Later, gator!"
        show flora sarcastic at disappear_to_right
        pause(1)
        $home_kitchen["croutons_activated"] = True
        $flora['hidden_now'] = True
        $process_event("update_state")
  return

label jacklyn_statement_ingredients_whoo_sauce_search:
  if mc.at("home_kitchen"):
    "That elusive Whooshster™ Sauce... it's so close I can almost taste it!"
  elif mc.at("home_hall"):
    "The scent of the Whooshster™ Sauce is quite strong here..."
  elif mc.at("school_entrance"):
    "Is that a waft of Whooshster™ Sauce on the wind? Definitely."
  elif (mc.at("school_ground_floor") or mc.at("home_bedroom")):
    "The fading scent of Whooshster™ Sauce lingers here."
  elif mc.at("home_bathroom"):
    "Oh, it smells like... wait, is that Whooshster™ Sauce? Err, no... nevermind."
  elif mc.at("school_cafeteria"):
    "Hmm... as much as my nose wants to inhale the sweet and rare aromas of the Whooshster™ Sauce, the smell here is only imaginary."
  elif (mc.at("school_ground_floor_west") or mc.at("school_homeroom") or mc.at("school_first_hall")):
    "Nothing but the artificial lemon smell of the janitor's trademark soap here. Where's the Whooshster™ Sauce?"
  elif (mc.at("school_first_hall_east") or mc.at("school_first_hall_west")):
    "The long days without the Whooshster™ Sauce. Like trying suck molasses through a straw. Nothing but a pipe dream..."
  else:
    "Where have my steps led me now... so far away from all that is safe and holy."
    "The Whooshster™ Sauce taunts me like a distant fever dream. Nothing but a tiny glimmer of hope left in the storm-ridden sea of my wretched soul."
  return

label jacklyn_statement_flora_chef:
  show flora blush with Dissolve(.5)
  flora blush "Is that a hat on your head?"
  mc "Huh?"
  flora laughing "Wait, it's your ass!"
  mc "Very funny..."
  flora laughing "Get it? 'Cause you're an asshat!"
  mc "Explaining the joke doesn't make it any more funny."
  flora annoyed "Just laugh next time!"
  mc "Yeah, yeah. Anyway, I need your help with something..."
  flora sarcastic "It won't be cheap."
  mc "Ugh. Well, I need your help cooking for my date..."
  flora confused "Didn't you say you were going to cook for her?"
  mc "Yeah, but—"
  flora cringe "It's the gesture that counts! Having me cook for your date is pretty gross..."
  flora sarcastic "I will do it for $50, though."
  show flora sarcastic at move_to(.75)
  menu(side="left"):
    extend ""
    "?mc.money>=50@[mc.money]/50|{image=ui hud icon_money}|\"Deal...\"":
      show flora sarcastic at move_to(.5)
      mc "Deal..."
      $mc.money-=50
      flora laughing "Pleasure doing business with you!"
      "Ugh, what a robbery... but at least the food will be good."
      $quest.jacklyn_statement["day"] = game.day
      $quest.jacklyn_statement["salad_made"] = True
      $quest.jacklyn_statement.advance("dinner")
    "\"Forget it.\"":
      show flora sarcastic at move_to(.5)
      mc "Forget it."
      flora confused "Good luck, then."
      flora sarcastic "I might film and send it to Kitchen Nightmares."
      mc "I might film you and send it to—"
      flora cringe "I dare you to finish that sentence! [jo] will have your head if you so much as whisper about filming me again."
      mc "..."
      "She's right... trying to get a good recording for PornHub of her on the toilet probably wasn't my brightest idea."
      "All right, so I guess I'm cooking the dinner myself..."
      "I should probably check the fridge to see what we have."
      $quest.jacklyn_statement.advance("fridge")
    "?mc.money>=25 and mc.intellect>=5@[mc.money]/25|{image=ui hud icon_money}|+|[mc.intellect]/5|{image=stats int}|\"You still owe me $25 for [jo]'s present last Christmas.\"":
      show flora sarcastic at move_to(.5)
      mc "You still owe me $25 for [jo]'s present last Christmas."
      flora confused "How come you remember that, but not to clean your room?"
      mc "Cleaning my room doesn't annoy you."
      flora annoyed "Fine, I'll do it for $25..."
      $mc.money-=25
      $quest.jacklyn_statement["day"] = game.day
      $quest.jacklyn_statement["salad_made"] = True
      $quest.jacklyn_statement.advance("dinner")
  hide flora with Dissolve(.5)
  return

label jacklyn_statement_home_kitchen_dinner:
  $quest.jacklyn_statement.advance("clothes")
  while game.hour < 16:
    $game.advance()
  $flora['hidden_now'] = True
  $jo["at_none_now"] = True
  #$set_dialog_mode("default_no_bg")
  "The sun is shining, the birds are—"
  "Hold up... that's the sunset..."
  "Fuck. Slept through the day..."
  hide black with Dissolve(.5)
  "Glad my phone woke me up."
  "Oh, shit! My date with [jacklyn]!"
  "The text is from her!"
  $set_dialog_mode("phone_message","jacklyn")
  jacklyn "I'm outside. Is the doorbell broken?"
  $set_dialog_mode("")
  "Shit. Five minutes ago. Well, it is broken... no need to lie about that."
  $set_dialog_mode("phone_message","jacklyn")
  mc "Yeah, it's broken. I'll be right down!"
  $set_dialog_mode("")
  "Okay, best get dressed!"
  $mc["focus"] = "jacklyn_statement"
  return

label jacklyn_statement_home_kitchen_datedoor:
  "[jacklyn] is by the door. Okay, time to put ice in my pants and pants in my veins... or something."
  return

label jacklyn_statement_jackdate:
  show jacklyn smile with Dissolve(.5)
  "Kinda thought [jacklyn] would dress up, but maybe she likes to keep things casual?"
  "Still in her teacher's uniform though... hmmm..."
  jacklyn smile "So, did you really cook for me? I'm starving."
  show jacklyn smile at move_to(.25)
  menu(side="right"):
    extend ""
    "?quest.jacklyn_statement['salad_made']@|{image= items croutons}|\"Yeah, I made a caesar salad.\"":
      show jacklyn smile at move_to(.5)
      mc "Yeah, I made a caesar salad."
      jacklyn excited "Wicked!"
      jacklyn excited "A mean salad is better than a mean machine."
      mc "I err... I think I agree with that."
      jacklyn laughing "Since I'm a truther, I'll tell you something... cooking for me is swelters, okay?"
      mc "Swelters?"
      $jacklyn.love+=2
      jacklyn laughing "Yeah... makes my cat purr and my ticker gush."
      mc "Thanks, but it's just a salad..."
      jacklyn excited_right "Picasso said it's just paintings. It's just people."
      mc "It's probably best you have a taste first."
    "?quest.jacklyn_statement['problem_for_later']@|\"Not really... I thought we'd figure it out once you got here.\"":
      show jacklyn smile at move_to(.5)
      mc "Not really... I thought we'd figure it out once you got here."
      $jacklyn.lust+=1
      jacklyn excited "Bitch, I do like spontaneity!"
      mc "Same, same... all right, what do you feel like?"
      jacklyn laughing "Wouldn't it be totally freak show if we had milk and cereal?"
      jacklyn excited "Like, that would shatter so many social expectations in one big bang."
      show jacklyn excited at move_to(.75)
      menu(side="left"):
        extend ""
        "\"If that's what you want...\"":
          show jacklyn excited at move_to(.5)
          mc "If that's what you want..."
          jacklyn laughing "Are you this kitten in the bedroom too?"
          mc "Meow! Err... I mean, maybe!"
          "Shit, that was really cringe..."
          jacklyn cringe "Macho is so 1884, anyway."
          "Not sure what [jacklyn] means by that, but hey, anything to please your lady, right?"
          "Not that she's my lady yet or anything, but can't start the pleasing too soon."
        "\"No, I want this to be casual, but not that casual.\"":
          show jacklyn excited at move_to(.5)
          mc "No, I want this to be casual, but not that casual."
          jacklyn neutral "Honestly, I'm starving and simple sounds good."
          mc "But... I'll be remembered forever as the guy who served his date cereal..."
          jacklyn smile "Yeah, but that's flickers and sparks! As long as you own it, people will find it badass."
          jacklyn laughing "High schoolers eat that shit up."
          mc "Uhh, okay... if you insist."
      $quest.jacklyn_statement['cereal_anyway'] = True
  mc "Please, have a seat... I'll just fix the lighting and stuff..."
  jacklyn excited "Understood."
  $unlock_replay("jacklyn_statement_date")
  $set_dialog_mode("default_no_bg")
  show black with fadehold
  "Not sure how this happened, but I'm actually on a date with freaking [jacklyn]."
  "I overheard some of the girls calling her a sex symbol the other day."
  "And if they're saying that, you just know all the guys have thought it already..."
  "Messing this up is a no-go."
  $set_dialog_mode("")
  show jacklyn jacklyn_date
  hide black
  with Dissolve(.5)
  jacklyn jacklyn_date "Cozy."
  mc "I know it's not much, but I've always found overblown romantic gestures kinda cringe."
  jacklyn jacklyn_date_nervous "Romanticism is a bygone era."
  "..."
  show jacklyn jacklyn_date with Dissolve(.5)
  mc "So... err..."
  "Crap. What do you even say on dates?"
  "My mind's totally blank..."
  mc "Did you like the food?"
  if quest.jacklyn_statement["all_ingredients"]:
    $jacklyn.love+=3
    $jacklyn.lust+=3
    $jacklyn.love+=3
    $jacklyn.lust+=3
    jacklyn jacklyn_date_flirty "It's pussy dew!"
    jacklyn jacklyn_date_laughing "Four out of four toasters!"
    mc "Heh, thanks!"
    show jacklyn jacklyn_date_smile with Dissolve(.5)
    mc "What did you think of the Whooshster™ Sauce?"
    jacklyn jacklyn_date_flirty "Blew me away. Totally made me straight!"
    mc "I thought you were straight?"
    jacklyn jacklyn_date_laughing "Oh, baby boy... I meant it woke me the fuck up."
    jacklyn jacklyn_date_smile "You know I don't dabble."
    mc "Ah! Got confused there for a second!"
  else:
    jacklyn jacklyn_date_laughing "It's a pass over the bar."
    mc "Great..."
  "Hmm... I need to think of a good topic..."
  menu(side="left"):
    extend ""
    "\"What's your favorite game?\"":
      $mc.intellect+=1
      mc "What's your favorite game?"
      jacklyn jacklyn_date_laughing "I only play love games."
      show jacklyn jacklyn_date_smile with Dissolve(.5)
      mc "Err... same. Video games are for nerds."
      jacklyn jacklyn_date_flirty "If that's your passion, then shake that virtual ass!"
      mc "Ah, yeah... definitely. I do play some video games. I really like Castlevania."
      jacklyn jacklyn_date_neutral "Can't say I've heard of it..."
    "\"Why do you want to become an art teacher?\"":
      mc "Why do you want to become an art teacher?"
      jacklyn jacklyn_date_neutral "Art is my weeping heart... always has been."
      jacklyn jacklyn_date_smile "Plus it's chill as fuck. Just watch a bunch of students splash color on a canvas all day..."
      mc "Never thought of that! Getting a chill job is pretty legit."
      jacklyn jacklyn_date_laughing "Yeah, and I can dress however the fuck I want and call it part of the image."
      mc "I do like the way you dress... it's a very strong statement."
      $jacklyn.lust+=3
      jacklyn jacklyn_date_flirty "You're totally rubbing my clit right now."
      mc "Err... you're welcome?"
    "\"Would you like to hear a funny story?\"":
      mc "Would you like to hear a funny story?"
      jacklyn jacklyn_date_neutral "Hit me, shooter."
      mc "Okay, here goes..."
      mc "I once had a nightmare that felt like years."
      mc "I watched my life pass before my eyes. It slipped between my fingers..."
      mc "[flora] got engaged to some douchebag with a BMW."
      mc "I dropped out of college due to depression, and started working some dead end job."
      mc "Each night was just me playing video games and watching porn."
      mc "And then I woke up... and life seems entirely different."
      mc "In my dream, [mrsl] was this introverted loner. Mrs. Bloomer an old hag."
      mc "No girls ever wanted anything to do with me."
      mc "I haven't told anyone this, but I'm so relieved I woke up... it almost feels like I traveled back in time."
      mc "Now you're here, and honestly, you're a splash of vibrant color in my life."
      $jacklyn.love+=1
      jacklyn jacklyn_date_laughing "That's smooch as hell."
      jacklyn jacklyn_date_flirty "Excuse me for sucking your cock, but that's quite amazing."
      jacklyn jacklyn_date_smile "It's like one of those origin stories... what a relief."
      mc "Happy you liked it."
  jacklyn jacklyn_date_surprised "What was that?"
  mc "Huh?"
  jacklyn jacklyn_date_surprised "Is someone else here?"
  mc "No... [jo]'s at work and [flora] is at her internship."
  jacklyn jacklyn_date_nervous "That's pretty damn freddy kruger if you ask me..."
  mc "Wait here, I'll go have a look."
  jacklyn jacklyn_date_neutral "That's how you get kebabbed."
  mc "It's probably nothing! [flora] sometimes leaves the window open and the neighbor's cat climbs in."
  mc "But err... be ready to call the cops..."
  hide jacklyn with Dissolve(.5)
  "I'm  not sure why, but the urge to be fearless in front of [jacklyn] is too big."
  "And besides, it's probably actually nothing but an opportunity to make myself look badass."
  "The stairs creak on their own."
  $quest.jacklyn_statement.advance("intruder")
  return

label jacklyn_statement_intruder:
  show jacklyn thinking with Dissolve(.5)
  jacklyn thinking "Did you find anything yet?"
  mc "Err, no... I'm still looking."
  hide jacklyn thinking with Dissolve(.5)
  return

label jacklyn_statement_home_hall_intruder:
  "Hmm... no one here, and the window is closed. Odd..."
  return

label jacklyn_statement_home_bathroom_intruder:
  mc "Aha!"
  "Hmm... no one here. Could've sworn someone was hiding here..."
  return

label jacklyn_statement_home_bedroom_intruder:
  "Hmm... everything here looks untouched."
  "Suspiciously untouched..."
  return

label jacklyn_statement_school_entrance_statement:
  while game.hour <23:
    $game.advance()
  $school_entrance["night_sky"] = True
  return

label jacklyn_statement_statement:
  show jacklyn neutral with Dissolve(.5)
  jacklyn neutral "You look a little jell-o-legged, what's wrong?"
  mc "Just a little nervous about getting caught."
  jacklyn cringe "Stop it with the shit vibes!"
  jacklyn cringe "No one's getting caught tonight."
  mc "Okay... so what kind of thing are we doing?"
  jacklyn excited "A big statement! One that will be seen and appreciated by many."
  mc "Are you sure about the second one?"
  jacklyn thinking "Yeah? All good statements have an enjoyment factor."
  jacklyn smile "That's the difference between vandalism and art."
  mc "Right... but why are we out here sneaking around, then?"
  mc "Couldn't we just ask for a permit?"
  jacklyn angry "People don't know what they want."
  jacklyn angry "Sometimes you just have to do it."
  mc "I guess that's fair enough..."
  jacklyn neutral "All right, mask on. We're making a moving statement tonight."
  jacklyn neutral_mask "Let's get to work."
  "This mask isn't exactly comfy, but at least I don't have to breathe in the toxic vapors..."
  jacklyn neutral_mask "What kind of statement should we make?"
  show jacklyn neutral_mask at move_to(.25)
  menu(side="right"):
    extend ""
    "\"One of passion, ferocity, and vigor!\"":
      show jacklyn neutral_mask at move_to(.5)
      $mc.lust+=1
      mc "One of passion, ferocity, and vigor!"
      jacklyn laughing_mask "That's straight sparks."
      jacklyn laughing_mask "Exactly what this place needs!"
      mc "You think so?"
      jacklyn laughing_mask "The bus will roll through the streets, igniting the minds of everyone around it."
      jacklyn neutral_mask "The statement will sweep wildfire through the city."
      mc "I'm ready to leave my mark."
      jacklyn neutral_mask "Wicked! Let's do this."
      show black with Dissolve(.5)
      $school_entrance["bus_lust"] = True
    "\"One of love, tranquility, and understanding.\"":
      show jacklyn neutral_mask at move_to(.5)
      $mc.love+=1
      mc "One of love, tranquility, and understanding."
      jacklyn laughing_mask "That's an art boner, all right."
      jacklyn laughing_mask "Nothing seeps into the mind like the clear water of an oasis in the desert of stress and hardship."
      jacklyn laughing_mask "I already have a design in mind..."
      mc "Nice. We can build on each other."
      jacklyn neutral_mask "Wicked! Let's do this."
      show black with Dissolve(.5)
      $school_entrance["bus_love"] = True
    "\"You're the real artist, you decide.\"":
      show jacklyn neutral_mask at move_to(.5)
      mc "You're the real artist, you decide."
      $jacklyn.lust+=3
      jacklyn thinking_mask "Very well! I don't mind taking charge..."
      jacklyn thinking_mask "But if you don't suck my dick when we're done, I'll be pissed."
      "She means compliments, obviously."
      mc "I don't mind a quick blowjob or two."
      jacklyn neutral_mask "Wicked! Let's do this."
      show black with Dissolve(.5)
      $school_entrance["bus_jacklyn"] = True
  hide black
  show jacklyn excited
  with Fade(.5,2.5,.5)
  jacklyn excited "Trophy wives to us both. That's a fucking statement."
  jacklyn excited "What a theme!"
  mc "I didn't do much."
  jacklyn laughing "Quit the gagging. We both made a difference today!"
  jacklyn laughing "People will know that and share our vision."
  "This really shows how talented [jacklyn] is."
  "It truly is more than just paint on a bus..."
  "It has a clear message, both artistically and psychologically."
  "Didn't really understand what she meant about making a statement before, but now I do."
  jacklyn neutral "You're all zippered up... what's on your mind?"
  "She's the type that only cares about directness... no real point beating around the bush with her."
  show jacklyn neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"How you've opened my eyes to what it means to be an artist.\"":
      show jacklyn neutral at move_to(.5)
      mc "How you've opened my eyes to what it means to be an artist."
      jacklyn smile "And what does it mean?"
      mc "To turn nothing into something... chaos into order."
      mc "To create meaning!"
      $jacklyn.love+=2
      jacklyn laughing "On point, shooter!"
      jacklyn excited "People often mistake art for something higher or refined that only those with top hats and monocles can appreciate."
      jacklyn excited "Art is the stories we tell ourselves and our children."
      jacklyn excited "Art is the emotions we pour out of our souls and onto the canvas."
      jacklyn excited "Art is the composition of sounds that turns into a melody!"
      mc "I finally understand!"
      jacklyn laughing "My panties get wet every time I see the light come on for an artist..."
      jacklyn laughing "I'm happy I could help you out with that!"
      mc "We should paint together again sometime."
      jacklyn excited "We will! You can put your abacus on the line for that."
      "Feels like [jacklyn] has helped me unlock a great secret about life itself..."
      "Can't wait for the next opportunity to explore the depths of art with her!"
      "The bus will serve as a reminder for what's important."
      "Art is an expression of our humanity... that's the statement."
      hide jacklyn with Dissolve(.5)
      "I feel like a true saint."
      "I could've chosen to indulge myself in the carnal sins of mortality, but instead I chose enlightenment."
      "And enlightenment feels good!"
      "It's lasting. It's the ambrosia of all great artists. It's the burning excitement to make something more than the humdrum of everyday life."
      "Maybe I'm just trying cheer myself up because I missed out on sex once again, but honestly... it feels okay this time."
    "?jacklyn.lust>=8@[jacklyn.lust]/8|{image= jacklyn contact_icon}|{image=stats lust_2}|\"That I want to put my dick in your ass right now.\"":
      $quest.jacklyn_statement["ending"] = "anal"
      $unlock_replay("jacklyn_statement_anal")
      show jacklyn neutral at move_to(.5)
      mc "That I want to put my dick in your ass right now."
      jacklyn laughing "I can see the afterglow of the statement burning in your eyes!"
      jacklyn excited "It's the passion of the muse."
      "Is that what it's called?"
      jacklyn smile_right "Every successful artist has felt it at some point."
      show jacklyn neutral with Dissolve(.5)
      "Not sure what's going on... I thought [jacklyn] would slap me and storm off, but she's actually playing with the waistband of her panties."
      $jacklyn.unequip("jacklyn_pants")
      jacklyn neutral "Very well. Make sure you spit on your cock... it's not going in dry."
      mc "Seriously?"
      $jacklyn.unequip("jacklyn_panties")
      jacklyn smile "Yeah, seriously. Lie down... this is the prize of enlightened artistry."
      $set_dialog_mode("default_no_bg")
      show black with fadehold
      "Can't believe this is happening... anal with freaking [jacklyn]!"
      "I need to get one of those shirts that says \"I fuck on the first date.\""
      "The grass tickles my back... the night air kisses my bare skin..."
      "She slides on top, straddling me."
      "The warm skin of her thighs on my sides zaps my abdomen with excitement."
      "Her wet pussy leaves a trail of moisture just below my belly button..."
      show jacklyn jacklyn_anal_prepare
      hide black
      with Dissolve(.5)
      $set_dialog_mode("")
      "[jacklyn] grinds on top of me, up and down, teasing my cock with her ass and pussy."
      "Coaxing out a hardness I never thought possible."
      jacklyn jacklyn_anal_insert "How's that?"
      show jacklyn jacklyn_anal_thrust1_face1 with Dissolve(.5)
      mc "Yeah..."
      jacklyn jacklyn_anal_thrust1_face3 "Yeah?"
      jacklyn jacklyn_anal_thrust1_face3 "Should I put it in deeper?"
      show jacklyn jacklyn_anal_thrust1_face2 with Dissolve(.5)
      mc "Yes!"
      jacklyn jacklyn_anal_prepare "So eager..."
      "Oh, man... she lines it up, letting the tip of my cock nestle against her wrinkled asshole."
      "She winks her sphincter at me, trying to suck me inside."
      jacklyn jacklyn_anal_prepare "Okay, let's try..."
      jacklyn jacklyn_anal_thrust2_face3 "Oooh!"
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      mc "Hnngh!"
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face3 with Dissolve(.5)
      "God, she just sucked it in with her ass muscles alone!"
      "Her ass is so warm, it feels like I just put my dick in a bowl of mac 'n' cheese..."
      show jacklyn jacklyn_anal_thrust1_face1 with Dissolve(.5)
      jacklyn jacklyn_anal_thrust1_face1 "I'm going to ride you, big boy. Ease it in slowly."
      show jacklyn jacklyn_anal_thrust2_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      "She penetrates herself on me."
      mc "Fuck..."
      "Her ass is so damn tight! Her muscles keep squeezing me, choking my, trying to suck the cum out."
      show jacklyn jacklyn_anal_thrust3_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      "The muscles in her ass are trying to push me out, but she's working me deeper."
      "She wiggles around, making my cock explore the soft insides of her anal cavity."
      "It's like she's trying to find the right spot or angle to—"
      show jacklyn jacklyn_anal_thrust4_face2 with hpunch
      mc "Ohhh!"
      jacklyn jacklyn_anal_thrust4_face1 "There... we... go..."
      "She just took me even deeper! Pushed me past some inner boundary!"
      "Man, I never thought she'd take this much of me in her ass..."
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      "She rides me deeply, making me feel every inch of her juicy asshole."
      "Squeezing and massaging me. Fuck... I could come already, but I want more..."
      mc "Take me deeper!"
      jacklyn jacklyn_anal_prepare "Just let me ride you..."
      show jacklyn jacklyn_anal_insert with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_prepare with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      "Holy shit, I think she just pushed me into the opening of her colon!"
      "Fuck... I'm so close to bursting... her ass is begging for my cum..."
      mc "Keep going!"
      jacklyn jacklyn_anal_thrust3_face3 "Cunt-bagle!"
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_prepare with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      mc "Deeper!"
      jacklyn jacklyn_anal_thrust3_face1 "Dick-fister!"
      jacklyn jacklyn_anal_thrust4_face3 "Fuck me, that's deep."
      "Jesus... I swear she just took the head of my dick into her colon."
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      jacklyn jacklyn_anal_thrust4_face2 "I do like a good ass-pounding..."
      jacklyn jacklyn_anal_thrust3_face2 "Riding you is giggle-fest."
      show jacklyn jacklyn_anal_thrust2_face2 with Dissolve(.5)
      mc "Riding me is—"
      show jacklyn jacklyn_anal_thrust1_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_prepare with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust1_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face3 with Dissolve(.5)
      mc "Fuck!"
      jacklyn jacklyn_anal_thrust3_face3 "Oh, give it to me!"
      show jacklyn jacklyn_anal_thrust4_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust3_face3 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust2_face3 with hpunch
      mc "Deeper!"
      show jacklyn jacklyn_anal_thrust3_face2 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face3 with Dissolve(.5)
      jacklyn jacklyn_anal_thrust4_face3 "Ohhh!"
      show jacklyn jacklyn_anal_thrust3_face1 with Dissolve(.5)
      show jacklyn jacklyn_anal_thrust4_face2 with Dissolve(.5)
      mc "Come on, impale yourself!"
      jacklyn jacklyn_anal_thrust3_face2 "Fuck-pickles!"
      show jacklyn jacklyn_anal_thrust4_face3 with Dissolve(.5)
      mc "Fuck, I'm so close..."
      menu(side="right"):
        extend ""
        "Fill her ass with cum":
          "I've always dreamed of filling a girl's ass with semen..."
          "I want her to remember me long after I pull my dick out."
          show jacklyn jacklyn_anal_cum_cumin_dickin with hpunch
          "Fuck, that's the best feeling ever! Just shooting my load deep into her bowels!"
          "Hot damn... I've..."
          "...never cummed... so hard!"
          jacklyn jacklyn_anal_postcum "Ohhh!"
          $jacklyn.lust+=1
          jacklyn jacklyn_anal_postcum "I just had an ass-gasm!"
          jacklyn jacklyn_anal_postcum "What a ride... you've been saving up, I think..."
          show jacklyn laughing with Fade(.5,3,.5)
          jacklyn "My kitten drenched Venus..."
          mc "Couldn't have said it better myself."
          $jacklyn.equip("jacklyn_panties")
          mc "That was truly mind-blowing..."
          $jacklyn.equip("jacklyn_pants")
          "Wow."
        "Pull out":
          mc "I'm going to..."
          show jacklyn jacklyn_anal_cum_cumout_dickin with hpunch
          mc "Oh, fuck!"
          "I think I just gave the moon a facial... hot damn!"
          jacklyn "Hello, cock-geyser!"
          show jacklyn laughing with Fade(.5,3,.5)
          "That was probably the best fucking release I've ever had..."
          "[jacklyn]'s ass is truly a masterpiece."
          $jacklyn.equip("jacklyn_panties")
          jacklyn laughing "Thanks for pulling out, Chili-flakes."
          $jacklyn.equip("jacklyn_pants")
          $jacklyn.love+=1
          jacklyn laughing "Makes my ass so much easier to clean."
          jacklyn excited "Want to know a big hush?"
          mc "Yeah..."
          jacklyn laughing "I just had an ass-gasm from that ride."
          mc "Whoa, really?"
          jacklyn laughing "Do I look like a non-truther? Fuck yeah, shooter."
          "God damn! Well, she did most of the work... but that's still a fucking boost."
          "Fuck, I still can't believe this just happened..."
      $set_dialog_mode("default_no_bg")
      show black with fadehold
      "The ride back home was fifty shades of awkward... but damn, that might've been the best night of my life..."
    "?quest.jacklyn_statement['clothes'] == 'punk'@|{image=jacklyn avatar choker}|\"That I want you to fuck me.\"":
      $quest.jacklyn_statement["ending"] = "pegging"
      $unlock_replay("jacklyn_statement_peg")
      show jacklyn neutral at move_to(.5)
      mc "That I want you to fuck me."
      "Not sure what's gotten into me, but a strange desire to be ravaged flows through my veins..."
      jacklyn smile "You sure did dress the part of a punk bitch! Is that why you rock the stockings and choker?"
      mc "I guess..."
      jacklyn smile "Baller. I'm actually feeling the tingles as well."
      jacklyn smile  "Come on, let's get skin-deep..."
      $jacklyn.unequip("jacklyn_pants")
      pause(1)
      jacklyn excited "Oh, you can leave the top on — you look totally baby in it."
      jacklyn excited "And the stockings are hot-wheels."
      mc "Err... okay."
      $jacklyn.unequip("jacklyn_panties")
      pause(1)
      mc "Whoa... you're so confident..."
      jacklyn laughing "Nothing to burn your cheeks over. Art is naked."
      jacklyn laughing "Step out of your prison!"
      mc "Uh, okay..."
      jacklyn neutral "Hey, are you sure you want this?"
      mc "Yeah... I don't think you can consider yourself truly avant garde until you've taken it in the ass."
      jacklyn smile "That is fair and true."
      jacklyn smile "All right, get comfy while I strap in..."
      $set_dialog_mode("default_no_bg")
      show black with fadehold
      "This is probably going to hurt, but somehow I'm turned on by the idea of getting fucked by someone like [jacklyn]."
      "She's so cool about it, I don't know any other girl who would treat sex so casually... especially something like this."
      show jacklyn mc_pegged_prepare
      hide black
      with Dissolve(.5)
      $set_dialog_mode("")
      jacklyn mc_pegged_prepare "Spread it, punk bitch."
      mc "..."
      jacklyn mc_pegged_prepare "Relax. You're more clenched than a boxer's fist."
      mc "That thing is a lot bigger than I thought it would be..."
      jacklyn mc_pegged_prepare "I'm not going to hurt you."
      mc "Um... okay. Just don't put it all in at once."
      jacklyn mc_pegged_prepare "Don't worry... I'll be nice."
      jacklyn mc_pegged_insert "We'll start with the tip."
      "Fuck. She's lining it up..."
      mc "Wait!"
      jacklyn mc_pegged_prepare "Hmm?"
      mc "I don't know about this..."
      jacklyn mc_pegged_prepare "We don't have to do it. Would you like to stop?"
      "I've fantasized about this, but it's a whole different thing in person..."
      "Getting a large foreign object pushed inside you is a terrifying prospect."
      "Man, I think I understand now why girls are so nervous on their virgin voyage."
      "Still, I did dress up for it, and [jacklyn]'s looking absolutely gorgeous wearing that strap-on..."
      "..."
      mc "Let's keep going."
      jacklyn mc_pegged_prepare "That's fire, shooter. I'm proud of you."
      jacklyn mc_pegged_insert "Okay, relax now."
      jacklyn mc_pegged_insert "It might hurt a little at first, but it'll pass."
      show jacklyn mc_pegged_in with vpunch
      mc "Fuck!"
      jacklyn mc_pegged_in "Are you okay?"
      "Oh, god! My poor sphincter... that beast cracked me right open."
      "Holy fuck. She's keeping it inside my ass."
      jacklyn mc_pegged_in "I'll let you get used to it for a bit."
      mc "Uhhh... it's so big!"
      jacklyn mc_pegged_in "Oh, please. It's medium size."
      show jacklyn mc_pegged_deep with vpunch
      mc "Oof!"
      jacklyn mc_pegged_deep "There we go. How's that?"
      mc "Fuck me... that's deep."
      "Feels like she just pushed through my ass entirely, poking at the entrance of my colon."
      "Damn, I'm so full..."
      jacklyn mc_pegged_deep "I'm going to start fucking you at a slow pace now. Is this your first time getting pegged?"
      mc "Yes..."
      "I can't believe she asked that when she's already buried deep in my ass."
      jacklyn mc_pegged_deep "Okay, I'll go easy on you at first."
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      "Oh my god. I'm getting fucked..."
      "I can't believe it."
      "It hurts like a motherfucker, but there's also a pulsating pleasure deep inside me."
      "I don't think I've ever been this hard before..."
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      jacklyn mc_pegged_deep "Take it, punk bitch."
      mc "Oh my god..."
      "A TA talking dirty, that's just too hot..."
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with hpunch
      jacklyn mc_pegged_deep "Take it!"
      "A firestorm of passion sweeps through her eyes as she slams into me."
      "Each thrust vibrates through my core, vibrates through her jiggly parts."
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      mc "Fuck, that's so good!"
      jacklyn mc_pegged_deep "Oh yeah?"
      mc "Fuck me, [jacklyn]!"
      jacklyn mc_pegged_deep "That's what I like to hear."
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      mc "Nghhhh!"
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with vpunch
      jacklyn mc_pegged_deeper "Now, that's a pegging!"
      jacklyn mc_pegged_deeper "Full penetration for my punk bitch."
      mc "Oh, god. Oh, fuck."
      "She just fucking impaled me..."
      "That's deeper than the fucking mines of Moria! She should call her strap-on Durin's Bane!"
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with vpunch
      mc "Ughhhh!"
      jacklyn mc_pegged_deep "I think you're ready for some deep strokes."
      mc "Really? I don't know about—"
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_in with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      "Fuck, she's beating the shit out of my prostate!"
      "Oh, god! My balls have never been so close to bursting before!"
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      jacklyn mc_pegged_deep "Come on, [mc]! Take it!"
      mc "Fuck me hard!"
      jacklyn mc_pegged_deep "Oh, I'm planning on it..."
      show jacklyn mc_pegged_deeper with vpunch
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with Dissolve(.5)
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with vpunch
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with vpunch
      show jacklyn mc_pegged_deep with Dissolve(.5)
      show jacklyn mc_pegged_deeper with vpunch
      show jacklyn mc_pegged_cum with vpunch
      "Fuuuuck!"
      jacklyn mc_pegged_cum "That's a pretty picture!"
      $achievement.taken_down_a_peg.unlock()
      show jacklyn laughing with Fade(.5,3,.5)
      "And that's what getting pegged by a hot teacher's assistant is like..."
      $jacklyn.equip("jacklyn_panties")
      "Deeply fulfilling, and not at all what I thought it would be."
      $jacklyn.equip("jacklyn_pants")
      "Better... much better..."
      $set_dialog_mode("default_no_bg")
      show black with fadehold
      "The ride back home was fifty shades of awkward... but damn, that might've been the best night of my life..."
  call goto_home_kitchen
  $set_dialog_mode("")
  "[jacklyn] is such a babe and I can't stop thinking about her."
  "Is this what having a crush is like? Am I crushing?"
  "God, I sound like a silly schoolgirl."
  "Quick, think of manly things!"
  "Diesel engines! Fire trucks! Football — the American kind! Chopping wood! Throwing the GPS out the window!"
  "Ahh! Much better..."
  $quest.jacklyn_statement.finish()
  $school_entrance["night_sky"] = False
  return
