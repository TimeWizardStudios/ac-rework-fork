init python:

  class Character_jacklyn(BaseChar):
    notify_level_changed=True

    default_name="Jacklyn"

    default_stats=[
      ["lust",1,0],
      ["love",1,0],
      ]

    name_color="#16b28b"

    contact_icon="jacklyn contact_icon"

    default_outfit_slots=["choker","pin","shirt","bra","pants","fishnet","panties"]

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("jacklyn_chained_choker")
      self.add_item("jacklyn_squidpin")
      self.add_item("jacklyn_dress")
      self.add_item("jacklyn_jacket")
      self.add_item("jacklyn_shirt")
      self.add_item("jacklyn_bra")
      self.add_item("jacklyn_orange_bra")
      self.add_item("jacklyn_pants")
      self.add_item("jacklyn_shorts")
      self.add_item("jacklyn_high_waist_fishnet")
      self.add_item("jacklyn_thigh_high_fishnet")
      self.add_item("jacklyn_panties")
      self.add_item("jacklyn_orange_panties")
      self.equip("jacklyn_panties")
      self.equip("jacklyn_bra")
      self.equip("jacklyn_shirt")
      self.equip("jacklyn_pants")

    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}

      ###Forced Locations
      if quest.jo_washed.in_progress:
        if quest.jo_washed["jacklyn_location"] == "school_art_class":
          self.location = "school_art_class"
          self.activity = "standing"
        else:
          self.location = None
          self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if quest.kate_fate in ("peek","hunt","run","hunt2","deadend","rescue"):
        self.location = None
        self.alternative_locations={}
        return

      if quest.jacklyn_statement in ("meetjacklyn","statement"):
        self.location = "school_entrance"
        self.activity = "standing"
        return

      if quest.jacklyn_statement in ("jackdate","intruder"):
        self.location = "home_kitchen"
        self.activity = "standing"
        return

      if jacklyn["at_none_this_scene"] or jacklyn["at_none_now"]:
        self.location = None
        self.activity = None
        return

      if quest.maxine_lines == "helpinghand":
        self.location = None
        self.activity = None
        return

      if quest.maxine_hook in ("day","interrogation"):
        self.location = "school_cafeteria"
        self.activity = "sitting"
        return

      if quest.fall_in_newfall.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.maya_spell == "maya" and game.hour == 12:
        self.location = "school_art_class"
        self.activity = "standing"
        return

      if quest.jacklyn_town in ("marina","gallery"):
        self.location = "pancake_brothel"
        self.activity = "standing"
        return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return
      ###Forced Locations

      ###ALT Locations
      if quest.maya_witch == "english_class":
        self.alternative_locations["school_art_class"] = "standing"

      if quest.kate_moment == "jacklyn":
        self.alternative_locations["school_art_class"] = "standing"

      if quest.jacklyn_romance == "hide_and_seek" and not school_art_class["ball_of_yarn_taken"]:
        self.alternative_locations["school_art_class"] = "standing"
      ###ALT Locations

      ### Schedule
      if game.dow in (0,1,2,3,4,5,6,7):
        if game.hour in (7,8,9,10,11):
          self.location="school_art_class"
          self.activity="standing"
          return
        elif game.hour == 12:
          self.location="school_cafeteria"
          self.activity="sitting"
          return
        elif game.hour in (13,14,15):
          self.location="school_art_class"
          self.activity="standing"
          return
        elif game.hour in (16,17,18):
          if game.season == 1 and school_art_class["nude_model"] == self:
            self.location="school_art_class"
            self.activity="pose"
            return
          else:
            self.location="school_entrance"
            self.activity="standing"
            return
        #else:
        #  self.location=None
        #  self.activity=None
        #  self.alternative_locations={}
      ### Schedule

    def call_label(self):
      if quest.isabelle_buried == "callforhelp":
        return "quest_isabelle_buried_callforhelp_jacklyn"
      else:
        return None
    def message_label(self):
      if game.season == 1:
        if quest.flora_jacklyn_introduction.finished and not quest.jacklyn_statement.started:
         if game.location ==  jacklyn.location:
           return "jacklyn_statement_text_same_room"
         else:
           return "jacklyn_statement_text_start"

    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("jacklyn avatar "+state,True):
          return "jacklyn avatar "+state
      rv=[]

      if state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        if "_jacklynless" not in state:
          rv.append(("jacklyn avatar body1",36,133))
          rv.append(("jacklyn avatar face_angry",190,243))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b1orange_panties",176,752))
        elif "_panties" in state and "_clothesless" not in state:
          rv.append(("jacklyn avatar b1panty",189,710))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b1orange_bra",160,414))
        elif "_bra" in state and "_clothesless" not in state:
          rv.append(("jacklyn avatar b1bra",137,421))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b1high_waist_fishnet",117,715))
        if "_pants" in state and "_clothesless" not in state:
          rv.append(("jacklyn avatar b1fishnet",119,936))
          rv.append(("jacklyn avatar b1skirt",144,762))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b1shorts",143,750))
        if "_jacklynless" not in state:
          rv.append(("jacklyn avatar b1arm1_n",324,432))
        if "_shirt" in state and "_clothesless" not in state:
          rv.append(("jacklyn avatar b1top",86,387))
          rv.append(("jacklyn avatar b1blazer",63,403))
          rv.append(("jacklyn avatar b1topblazer",63,387))
          rv.append(("jacklyn avatar b1choker",192,387))
          if "_jacklynless" in state:
            rv.append(("jacklyn avatar b1arm1_c_handless",322,429))
          else:
            rv.append(("jacklyn avatar b1arm1_c",322,429))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b1jacket",73,437))
          rv.append(("jacklyn avatar b1arm1_jacket",324,432))
        if "_squidpin" in state and "_clothesless" not in state:
          rv.append(("jacklyn avatar b1badge",328,497))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b1chained_choker",189,398))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body1",36,133))
        rv.append(("jacklyn avatar face_neutral",192,245))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b1orange_panties",176,752))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b1panty",189,710))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b1orange_bra",160,414))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b1bra",137,421))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b1high_waist_fishnet",117,715))
        elif "_thigh_high_fishnet" in state:
          rv.append(("jacklyn avatar b1thigh_high_fishnet",117,995))
        if "_pants" in state:
          rv.append(("jacklyn avatar b1fishnet",119,936))
          rv.append(("jacklyn avatar b1skirt",144,762))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b1shorts",143,750))
        if "_hands_up" in state:
          rv.append(("jacklyn avatar b1arm2_n",303,283))
        else:
          rv.append(("jacklyn avatar b1arm1_n",324,432))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b1top",86,387))
          rv.append(("jacklyn avatar b1blazer",63,403))
          rv.append(("jacklyn avatar b1topblazer",63,387))
          rv.append(("jacklyn avatar b1choker",192,387))
          if "_hands_up" in state:
            rv.append(("jacklyn avatar b1arm2_c",304,266))
          else:
            rv.append(("jacklyn avatar b1arm1_c",322,429))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b1jacket",73,437))
          rv.append(("jacklyn avatar b1arm1_jacket",324,432))
        elif "_dress" in state:
          rv.append(("jacklyn avatar b1dress",58,419))
          rv.append(("jacklyn avatar b1arm1_dress",293,411))
        if "_squidpin" in state:
          rv.append(("jacklyn avatar b1badge",328,497))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b1chained_choker",189,398))
        if "_mask" in state:
          rv.append(("jacklyn avatar b1gasmask",180,246))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body1",36,133))
        if "_right" in state:
          rv.append(("jacklyn avatar face_smile_right",191,245))
        else:
          rv.append(("jacklyn avatar face_smile",192,245))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b1orange_panties",176,752))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b1panty",189,710))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b1orange_bra",160,414))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b1bra",137,421))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b1high_waist_fishnet",117,715))
        elif "_thigh_high_fishnet" in state:
          rv.append(("jacklyn avatar b1thigh_high_fishnet",117,995))
        if "_pants" in state:
          rv.append(("jacklyn avatar b1fishnet",119,936))
          rv.append(("jacklyn avatar b1skirt",144,762))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b1shorts",143,750))
        if "_hands_down" in state:
          rv.append(("jacklyn avatar b1arm1_n",324,432))
        else:
          rv.append(("jacklyn avatar b1arm2_n",303,283))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b1top",86,387))
          rv.append(("jacklyn avatar b1blazer",63,403))
          rv.append(("jacklyn avatar b1topblazer",63,387))
          rv.append(("jacklyn avatar b1choker",192,387))
          if "_hands_down" in state:
            rv.append(("jacklyn avatar b1arm1_c",322,429))
          else:
            rv.append(("jacklyn avatar b1arm2_c",304,266))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b1jacket",73,437))
          if "_hands_down" in state:
            rv.append(("jacklyn avatar b1arm1_jacket",324,432))
          else:
            rv.append(("jacklyn avatar b1arm2_jacket",303,280))
        elif "_dress" in state:
          rv.append(("jacklyn avatar b1dress",58,419))
          if "_hands_down" in state:
            rv.append(("jacklyn avatar b1arm1_dress",293,411))
          else:
            rv.append(("jacklyn avatar b1arm2_dress",293,282))
        if "_squidpin" in state:
          rv.append(("jacklyn avatar b1badge",328,497))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b1chained_choker",189,398))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body1",36,133))
        rv.append(("jacklyn avatar face_thinking",192,244))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b1orange_panties",176,752))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b1panty",189,710))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b1orange_bra",160,414))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b1bra",137,421))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b1high_waist_fishnet",117,715))
        elif "_thigh_high_fishnet" in state:
          rv.append(("jacklyn avatar b1thigh_high_fishnet",117,995))
        if "_pants" in state:
          rv.append(("jacklyn avatar b1fishnet",119,936))
          rv.append(("jacklyn avatar b1skirt",144,762))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b1shorts",143,750))
        rv.append(("jacklyn avatar b1arm2_n",303,283))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b1top",86,387))
          rv.append(("jacklyn avatar b1blazer",63,403))
          rv.append(("jacklyn avatar b1topblazer",63,387))
          rv.append(("jacklyn avatar b1choker",192,387))
          rv.append(("jacklyn avatar b1arm2_c",304,266))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b1jacket",73,437))
          rv.append(("jacklyn avatar b1arm2_jacket",303,280))
        elif "_dress" in state:
          rv.append(("jacklyn avatar b1dress",58,419))
          rv.append(("jacklyn avatar b1arm2_dress",293,282))
        if "_squidpin" in state:
          rv.append(("jacklyn avatar b1badge",328,497))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b1chained_choker",189,398))
        if "_mask" in state:
          rv.append(("jacklyn avatar b1gasmask",180,246))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body2",93,136))
        rv.append(("jacklyn avatar face_annoyed",226,262))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b2orange_panties",115,790))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b2panty",155,730))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b2orange_bra",173,423))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b2bra",168,419))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b2high_waist_fishnet",93,737))
        if "_pants" in state:
          rv.append(("jacklyn avatar b2fishnet",94,930))
          rv.append(("jacklyn avatar b2skirt",89,768))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b2shorts",92,769))
        if "_sponges" in state:
          rv.append(("jacklyn avatar b2arm2_n",118,411))
          rv.append(("jacklyn avatar b2sponges_n",114,384))
        else:
          rv.append(("jacklyn avatar b2arm1_n",115,573))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b2chained_choker",266,411))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b2top",139,396))
          rv.append(("jacklyn avatar b2blazer",98,400))
          rv.append(("jacklyn avatar b2topblazer",98,396))
          rv.append(("jacklyn avatar b2choker",262,403))
          if "_sponges" in state:
            rv.append(("jacklyn avatar b2arm2_c",118,411))
            if "_squidpin" in state:
              rv.append(("jacklyn avatar b2badge",374,515))
            rv.append(("jacklyn avatar b2sponges_c",114,384))
          else:
            rv.append(("jacklyn avatar b2arm1_c",115,437))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b2jacket",133,410))
          rv.append(("jacklyn avatar b2arm1_jacket",115,573))
        if "_squidpin" in state and "_sponges" not in state:
          rv.append(("jacklyn avatar b2badge",374,515))
        if "_mask" in state:
          rv.append(("jacklyn avatar b2gasmask",204,272))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body2",93,136))
        rv.append(("jacklyn avatar face_cringe",226,261))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b2orange_panties",115,790))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b2panty",155,730))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b2orange_bra",173,423))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b2bra",168,419))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b2high_waist_fishnet",93,737))
        if "_pants" in state:
          rv.append(("jacklyn avatar b2fishnet",94,930))
          rv.append(("jacklyn avatar b2skirt",89,768))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b2shorts",92,769))
        rv.append(("jacklyn avatar b2arm2_n",118,411))
        if "_sponges" in state:
          rv.append(("jacklyn avatar b2sponges_n",114,384))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b2chained_choker",266,411))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b2top",139,396))
          rv.append(("jacklyn avatar b2blazer",98,400))
          rv.append(("jacklyn avatar b2topblazer",98,396))
          rv.append(("jacklyn avatar b2choker",262,403))
          rv.append(("jacklyn avatar b2arm2_c",118,411))
          if "_sponges" in state:
            if "_squidpin" in state:
              rv.append(("jacklyn avatar b2badge",374,515))
            rv.append(("jacklyn avatar b2sponges_c",114,384))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b2jacket",133,410))
          rv.append(("jacklyn avatar b2arm2_jacket",118,411))
        if "_squidpin" in state and "_sponges" not in state:
          rv.append(("jacklyn avatar b2badge",374,515))
        if "_mask" in state:
          rv.append(("jacklyn avatar b2gasmask",204,272))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body2",93,136))
        if "_right" in state:
          rv.append(("jacklyn avatar face_excited_right",226,258))
        else:
          rv.append(("jacklyn avatar face_excited",226,258))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b2orange_panties",115,790))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b2panty",155,730))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b2orange_bra",173,423))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b2bra",168,419))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b2high_waist_fishnet",93,737))
        elif "_thigh_high_fishnet" in state:
          rv.append(("jacklyn avatar b2thigh_high_fishnet",113,985))
        if "_pants" in state:
          rv.append(("jacklyn avatar b2fishnet",94,930))
          rv.append(("jacklyn avatar b2skirt",89,768))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b2shorts",92,769))
        if "_sponges" in state:
          rv.append(("jacklyn avatar b2arm2_n",118,411))
          rv.append(("jacklyn avatar b2sponges_n",114,384))
        elif "_dress" not in state:
          rv.append(("jacklyn avatar b2arm1_n",115,573))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b2chained_choker",266,411))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b2top",139,396))
          rv.append(("jacklyn avatar b2blazer",98,400))
          rv.append(("jacklyn avatar b2topblazer",98,396))
          rv.append(("jacklyn avatar b2choker",262,403))
          if "_sponges" in state:
            rv.append(("jacklyn avatar b2arm2_c",118,411))
            if "_squidpin" in state:
              rv.append(("jacklyn avatar b2badge",374,515))
            rv.append(("jacklyn avatar b2sponges_c",114,384))
          else:
            rv.append(("jacklyn avatar b2arm1_c",115,437))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b2jacket",133,410))
          rv.append(("jacklyn avatar b2arm1_jacket",115,573))
        elif "_dress" in state:
          rv.append(("jacklyn avatar b2dress",23,399))
          rv.append(("jacklyn avatar b2arm1_n",115,573))
        if "_squidpin" in state and "_sponges" not in state:
          rv.append(("jacklyn avatar b2badge",374,515))
        if "_mask" in state:
          rv.append(("jacklyn avatar b2gasmask",204,272))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("jacklyn avatar body2",93,136))
        rv.append(("jacklyn avatar face_laughing",228,260))
        if "_orange_panties" in state:
          rv.append(("jacklyn avatar b2orange_panties",115,790))
        elif "_panties" in state:
          rv.append(("jacklyn avatar b2panty",155,730))
        if "_orange_bra" in state:
          rv.append(("jacklyn avatar b2orange_bra",173,423))
        elif "_bra" in state:
          rv.append(("jacklyn avatar b2bra",168,419))
        if "_high_waist_fishnet" in state:
          rv.append(("jacklyn avatar b2high_waist_fishnet",93,737))
        elif "_thigh_high_fishnet" in state:
          rv.append(("jacklyn avatar b2thigh_high_fishnet",113,985))
        if "_pants" in state:
          rv.append(("jacklyn avatar b2fishnet",94,930))
          rv.append(("jacklyn avatar b2skirt",89,768))
        elif "_shorts" in state:
          rv.append(("jacklyn avatar b2shorts",92,769))
        if "_dress" not in state:
          rv.append(("jacklyn avatar b2arm2_n",118,411))
        if "_sponges" in state:
          rv.append(("jacklyn avatar b2sponges_n",114,384))
        if "_chained_choker" in state:
          rv.append(("jacklyn avatar b2chained_choker",266,411))
        if "_shirt" in state:
          rv.append(("jacklyn avatar b2top",139,396))
          rv.append(("jacklyn avatar b2blazer",98,400))
          rv.append(("jacklyn avatar b2topblazer",98,396))
          rv.append(("jacklyn avatar b2choker",262,403))
          rv.append(("jacklyn avatar b2arm2_c",118,411))
          if "_sponges" in state:
            if "_squidpin" in state:
              rv.append(("jacklyn avatar b2badge",374,515))
            rv.append(("jacklyn avatar b2sponges_c",114,384))
        elif "_jacket" in state:
          rv.append(("jacklyn avatar b2jacket",133,410))
          rv.append(("jacklyn avatar b2arm2_jacket",118,411))
        elif "_dress" in state:
          rv.append(("jacklyn avatar b2dress",23,399))
          rv.append(("jacklyn avatar b2arm2_n",118,411))
        if "_squidpin" in state and "_sponges" not in state:
          rv.append(("jacklyn avatar b2badge",374,515))
        if "_mask" in state:
          rv.append(("jacklyn avatar b2gasmask",204,272))

      if state.startswith("jacklynpose01n"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events upskirt JacklynUpskirt_n",1,0))
        #rv.append(("jacklyn avatar events upskirt JacklynUpskirt_c",447,60))
      elif state.startswith("jacklynpose01"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events upskirt JacklynUpskirt_n",1,0))
        rv.append(("jacklyn avatar events upskirt JacklynUpskirt_panty",688,96))
        rv.append(("jacklyn avatar events upskirt JacklynUpskirt_bra",1034,117))
        rv.append(("jacklyn avatar events upskirt JacklynUpskirt_c",447,60))

      if state.startswith("jacklyn_date"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datekitchen jacklyndate_bg",0,0))
        rv.append(("jacklyn avatar events datekitchen jacklyndate_base",812,32))
        rv.append(("jacklyn avatar events datekitchen jacklyndate_undies",1084,393))
        rv.append(("jacklyn avatar events datekitchen jacklyndate_clothes",807,363))

        if quest.jacklyn_statement['salad_made'] and not quest.jacklyn_statement['cereal_anyway']:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_salad",73,525))
        else:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_cereal",98,539))

        if "_smile" in state:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_face_smile",1031,153))
        elif "_flirty" in state:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_face_flirty",1031,151))
        elif "_laughing" in state:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_face_laughing",1031,157))
        elif "_surprised" in state:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_face_surprised",1031,150))
        elif "_nervous" in state:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_face_nervous",1031,148))
        else:
          rv.append(("jacklyn avatar events datekitchen jacklyndate_face_neutral",1031,154))


      if state.startswith("jacklyn_anal_prepare"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_prepare-postcum",1266,596))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headup",1175,724))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare",664,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare_face",1393,82))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare_bra",1057,204))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare_top",1018,147))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_prepare",560,582))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm",1205,721))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_insert"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_insert-thrust1",1237,595))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headup",1175,724))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert",550,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert_face",1370,100))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert_bra",1036,294))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert_top",997,164))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_insert",560,571))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm_insert",1188,730))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_thrust1"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_insert-thrust1",1237,595))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headup",1175,724))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert",550,0))
        if "_face1" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity1_thrust1_face",1370,69))
        if "_face2" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity2_thrust1_face",1370,69))
        if "_face3" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity3_thrust1_face",1370,100))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert_bra",1036,294))
        rv.append(("jacklyn avatar events dateanal jacklynanal_insert_top",997,164))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_insert",560,571))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm_insert",1188,730))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_thrust2"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_thrust2",1220,613))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headup",1175,724))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust2",550,4))
        if "_face1" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity1_thrust2_face",1351,110))
        if "_face2" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity2_thrust2_face",1351,78))
        if "_face3" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity3_thrust2_face",1351,82))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust2_bra",1017,311))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust2_top",978,173))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_thrust2",560,617))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm_thrust2",1173,740))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_thrust3"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_thrust3",1215,618))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headup",1175,724))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust3",549,13))
        if "_face1" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity1_thrust3_face",1335,88))
        if "_face2" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity2_thrust3_face",1335,119))
        if "_face3" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity3_thrust3_face",1335,89))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust3_bra",1001,212))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust3_top",961,182))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_thrust3",560,646))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm_thrust3",1165,747))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_thrust4"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_thrust4",1192,624))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headup",1175,724))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust4",560,21))
        if "_face1" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity1_thrust4_face",1320,96))
        if "_face2" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity2_thrust4_face",1320,96))
        if "_face3" in state:
            rv.append(("jacklyn avatar events dateanal jacklynanal_intensity3_thrust4_face",1320,127))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust4_bra",986,246))
        rv.append(("jacklyn avatar events dateanal jacklynanal_thrust4_top",947,190))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_thrust4",560,645))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm_thrust4",1163,748))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_cum"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_cum",1197,624))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headdown",1144,814))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_cum",576,21))
        if "_dickout" in state:
          rv.append(("jacklyn avatar events dateanal jacklynanal_dickout_face",1320,125))
        if "_dickin" in state:
          rv.append(("jacklyn avatar events dateanal jacklynanal_dickin_face",1320,126))
        rv.append(("jacklyn avatar events dateanal jacklynanal_cum_bra",986,246))
        rv.append(("jacklyn avatar events dateanal jacklynanal_cum_top",947,190))
        if "_cumin" in state:
          rv.append(("jacklyn avatar events dateanal jacklynanal_dick_cumin",549,645))  ###########Fix this
        if "_cumout" in state:
          rv.append(("jacklyn avatar events dateanal jacklynanal_dick_cumout",384,304))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm_cum",1163,748))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))
      elif state.startswith("jacklyn_anal_postcum"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events dateanal jacklynanal_bg",0,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_arm_prepare-postcum",1266,596))
        rv.append(("jacklyn avatar events dateanal jacklynanal_headdown",1144,814))
        if quest.jacklyn_statement["clothes"] == "pirate":
            rv.append(("jacklyn avatar events dateanal jacklynanal_pirate",0,651))
        elif quest.jacklyn_statement["clothes"] == "punk":
            rv.append(("jacklyn avatar events dateanal jacklynanal_punk",0,651))
        else:
            rv.append(("jacklyn avatar events dateanal jacklynanal_normal",0,651))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare",664,0))
        rv.append(("jacklyn avatar events dateanal jacklynanal_postcum_face",1393,82))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare_bra",1057,204))
        rv.append(("jacklyn avatar events dateanal jacklynanal_prepare_top",1018,147))
        rv.append(("jacklyn avatar events dateanal jacklynanal_dick_postcum",559,544))
        rv.append(("jacklyn avatar events dateanal jacklynanal_mcarm",1205,721))
        rv.append(("jacklyn avatar events dateanal jacklynanal_grass",0,558))

      if state=="mc_pegged_prepare":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody",96,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg",666,58))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand",598,543))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody",804,31))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra",1321,356))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop",914,326))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
      elif state=="mc_pegged_insert":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody2",59,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg2",666,58))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand2",598,543))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody2",920,31))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra2",1321,336))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop2",1321,326))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
      elif state=="mc_pegged_in":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody2",59,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg3",661,47))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand3",599,529))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody3",823,41))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra3",1231,366))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop3",1233,335))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
      elif state=="mc_pegged_half":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody2",59,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg4",654,57))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand4",599,559))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody4",818,27))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra4",1207,351))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop4",1206,320))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
      elif state=="mc_pegged_deep":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody2",59,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg5",650,53))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand5",591,595))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody5",761,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra5",1193,322))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop5",1193,298))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
      elif state=="mc_pegged_deeper":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody2",59,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg6",625,48))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand6",565,620))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody6",709,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra6",1177,317))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop6",1163,289))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
      elif state=="mc_pegged_cum":
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events datepeg MCPegged_bg",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcbody2",59,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_mcleg6",625,48))
        rv.append(("jacklyn avatar events datepeg MCPegged_mchand6",565,620))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbody6",709,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_jbra6",1177,317))
        rv.append(("jacklyn avatar events datepeg MCPegged_jtop6",1163,289))
        rv.append(("jacklyn avatar events datepeg MCPegged_grass",0,0))
        rv.append(("jacklyn avatar events datepeg MCPegged_cum",580,117))

      elif state.startswith("handjob"):
        if "base" in state:
          rv.append((1920,1080))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bg",0,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_mc",0,44))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyn",339,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyu",860,561))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyc",544,443))
        elif "middle" in state:
          rv.append((1920,1080))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bg",0,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_mc",0,44))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyn2",339,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyu2",856,561))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyc2",468,444))
        elif "top" in state:
          rv.append((1920,1080))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bg",0,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_mc",0,44))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyn3",339,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyu3",851,561))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyc3",501,444))
        elif "cum" in state:
          rv.append((1920,1080))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bg",0,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_mc",0,44))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyn4",339,0))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyu4",856,561))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_bodyc4",510,444))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_cumshadow",818,574))
          rv.append(("jacklyn avatar events handjob jacklynhandjob_cum",774,569))

      elif state.startswith("king_of_sweets"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events king_of_sweets bg",0,0))
        if state.endswith(("inspection","approval")):
          rv.append(("jacklyn avatar events king_of_sweets mc_body1",53,0))
        elif state.endswith("zipper"):
          rv.append(("jacklyn avatar events king_of_sweets mc_body2",21,0))
        else:
          rv.append(("jacklyn avatar events king_of_sweets mc_body3",91,0))
          if state.endswith(("stroke1","taste1","taste2","taste3","deepthroat1","deepthroat2","deepthroat3","deepthroat3_cum")):
            rv.append(("jacklyn avatar events king_of_sweets mc_dick1",722,269))
          else:
            rv.append(("jacklyn avatar events king_of_sweets mc_dick2",722,235))
        if "assisted" in state:
          if state.endswith("deepthroat1"):
            rv.append(("jacklyn avatar events king_of_sweets mc_left_hand1",712,70))
          elif state.endswith(("taste2","deepthroat2")):
            rv.append(("jacklyn avatar events king_of_sweets mc_left_hand2",732,101))
          elif state.endswith(("taste3","deepthroat3","deepthroat3_cum")):
            rv.append(("jacklyn avatar events king_of_sweets mc_left_hand3",757,101))
        if state.endswith(("taste1","taste2","taste3","cum_in_mouth","cum_on_face1","cum_on_face2","selfie","selfie_cum")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_shadow1",1292,828))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs1_n",973,595))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs1_c",973,595))
          if state.endswith(("taste1","cum_in_mouth","cum_on_face1","cum_on_face2","selfie","selfie_cum")):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body1_n",971,217))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body1_u",1258,499))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body1_c",958,214))
          elif state.endswith("taste2"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body2_n",801,222))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body2_u",1244,496))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body2_c",966,220))
          elif state.endswith("taste3"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body3_n",953,209))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body3_u",1235,478))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body3_c",953,207))
        elif state.endswith(("deepthroat1","deepthroat2","deepthroat3","deepthroat3_cum")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_shadow2",1250,846))
          if state.endswith("deepthroat1"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs2_n",989,532))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs2_c",989,532))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body4_n",933,288))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body4_u",1212,510))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body4_c",929,282))
          elif state.endswith("deepthroat2"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs3_n",1014,530))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs3_c",1014,530))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body5_n",909,296))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body5_u",1198,510))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body5_c",907,287))
          elif state.endswith(("deepthroat3","deepthroat3_cum")):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs4_n",1012,527))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_legs4_c",1012,528))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body6_n",888,294))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body6_u",1177,502))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_body6_c",886,288))
        else:
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_body7_n",1162,240))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_body7_u",1476,559))
          if jacklyn.equipped_item("jacklyn_bra"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_bra",1164,245))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_body7_c",1161,242))
        if state.endswith(("inspection","approval","zipper","dick_out","amazed","lip_bite","camera","camera_flash","realization","looking_up","stroke1","stroke2","stroke3","stroke4","stroke5","stroke6","stroke7","admiration")):
          if state.endswith(("stroke1","stroke2","stroke3","stroke4","stroke5","stroke6","stroke7","admiration")):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_top1",1173,231))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_top2",1164,231))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin1_squid",1323,302))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin1_emoji",1323,302))
        if state.endswith(("inspection","zipper","dick_out")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head1",1020,32))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_face1",1051,120))
        elif state.endswith(("approval","amazed","lip_bite","camera","realization","admiration")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head2",1030,18))
          if state.endswith("approval"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face2",1057,131))
          elif state.endswith("amazed"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face3",1060,128))
          elif state.endswith(("lip_bite","camera","admiration")):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face4",1057,131))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face5",1057,131))
        elif state.endswith("camera_flash"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head3",1030,18))
        elif state.endswith(("looking_up","shirt_lift1","shirt_lift2","stroke1","stroke2","stroke3","stroke4","stroke5","stroke6","stroke7")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head4",1045,13))
          if state.endswith(("shirt_lift1","stroke6","stroke7")):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face6",1079,97))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face7",1080,96))
        elif state.endswith("taste1"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head5",838,40))
        elif state.endswith("taste2"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head6",809,50))
        elif state.endswith("taste3"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head7",779,55))
        elif state.endswith("deepthroat1"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head8",794,85))
        elif state.endswith("deepthroat2"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head9",773,103))
        elif state.endswith(("deepthroat3","deepthroat3_cum")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head10",755,102))
          if state.endswith("deepthroat3_cum"):
            rv.append(("jacklyn avatar events king_of_sweets mc_cum1",750,270))
        elif state.endswith(("cum_in_mouth","selfie","cum_on_face1","cum_on_face2","selfie_cum")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_head11",842,23))
          if state.endswith("cum_in_mouth"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face8",875,101))
          elif state.endswith("selfie"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face9",875,101))
          elif state.endswith("cum_on_face1"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face10",875,101))
          elif state.endswith("cum_on_face2"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face11",872,101))
          elif state.endswith("selfie_cum"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_face12",872,101))
        if state.endswith(("inspection","approval","zipper","dick_out","amazed","lip_bite","camera","camera_flash","realization","looking_up","shirt_lift1","shirt_lift2","stroke1","stroke2","stroke3","stroke4","stroke5","stroke6","stroke7","admiration")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_choker",1181,226))
        if state.endswith(("inspection","approval","zipper","dick_out","realization","looking_up")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms1_n",1034,244))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms1_c",1028,262))
        elif state.endswith(("amazed","lip_bite")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms2_n",1024,122))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms2_c",1097,261))
        elif state.endswith(("camera","camera_flash")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms3_n",929,149))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms3_c",951,262))
          if state.endswith("camera_flash"):
            rv.append(("jacklyn avatar events king_of_sweets camera_flash",781,186))
        elif state.endswith(("shirt_lift1","shirt_lift2")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms4_n",1022,262))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms4_c",1015,261))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_top3",1105,229))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_choker",1181,226))
        elif state.endswith(("stroke1","stroke2")):
          if state.endswith("stroke1"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms5_n",755,262))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms5_n2",750,262))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms5_c",813,262))
        elif state.endswith(("stroke3","stroke7")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms6_n",773,260))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms6_c",828,262))
        elif state.endswith(("stroke4","admiration")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms7_n",818,239))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms7_c",855,262))
        elif state.endswith(("stroke5","stroke6")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms8_n",863,222))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms8_c",884,262))
        elif state.endswith(("taste1","cum_in_mouth","cum_on_face1","cum_on_face2")):
          if state.endswith(("cum_in_mouth","cum_on_face1","cum_on_face2")):
            rv.append(("jacklyn avatar events king_of_sweets mc_dick2",722,235))
            if state.endswith("cum_in_mouth"):
              rv.append(("jacklyn avatar events king_of_sweets mc_cum2",931,244))
            elif state.endswith("cum_on_face2"):
              rv.append(("jacklyn avatar events king_of_sweets mc_cum3",881,241))
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms9_n2",755,216))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms9_n",744,216))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms9_c",823,212))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_squid",1191,310))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_emoji",1191,310))
        elif state.endswith("taste2"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms10_n",739,220))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms10_c",833,218))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_squid",1163,313))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_emoji",1163,313))
        elif state.endswith("taste3"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms11_n",761,208))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms11_c",839,207))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_squid",1149,300))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_emoji",1149,300))
        elif state.endswith("deepthroat1"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms12_n",928,288))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms12_c",926,282))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin3_squid",1109,362))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin3_emoji",1109,362))
        elif state.endswith("deepthroat2"):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms13_n",905,296))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms13_c",901,287))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin3_squid",1090,367))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin3_emoji",1090,367))
        elif state.endswith(("deepthroat3","deepthroat3_cum")):
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms14_n",882,294))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms14_c",876,287))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin3_squid",1073,361))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin3_emoji",1073,361))
        elif state.endswith(("selfie","selfie_cum")):
          rv.append(("jacklyn avatar events king_of_sweets mc_dick2",722,235))
          if state.endswith("selfie"):
            rv.append(("jacklyn avatar events king_of_sweets mc_cum2",931,244))
          elif state.endswith("selfie_cum"):
            rv.append(("jacklyn avatar events king_of_sweets mc_cum3",881,241))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms15_n",755,45))
          rv.append(("jacklyn avatar events king_of_sweets jacklyn_arms15_c",823,204))
          if jacklyn.equipped_item("jacklyn_squidpin"):
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_squid",1191,310))
          else:
            rv.append(("jacklyn avatar events king_of_sweets jacklyn_pin2_emoji",1191,310))

      elif state.startswith("music_class_masturbation"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events music_class_masturbation background",0,0))
        if "_jacklyn" in state:
          if "_stand" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_body1",908,16))
          elif "_sit" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_body2",767,323))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_underwear1",949,335))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_bottoms",703,627))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_arms1",776,352))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_tops1",821,334))
            rv.append(("jacklyn avatar events music_class_masturbation chair_outline",907,538))
          elif "_reveal" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_body2",767,323))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_underwear2",936,335))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_bottoms",703,627))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_arms2",856,351))
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_tops2",854,334))
            rv.append(("jacklyn avatar events music_class_masturbation chair_outline",907,538))
          if "_excited" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head1",940,145))
          elif "_smile_down" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head2",940,145))
          elif "_smile" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head3",940,145))
          elif "_surprised_down" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head4",940,145))
          elif "_surprised" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head5",940,145))
          elif "_flirty_down" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head6",940,145))
          elif "_flirty" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head7",940,145))
          elif "_neutral" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head8",940,145))
          elif "_annoyed" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head9",940,145))
          elif "_blush" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head10",940,145))
          elif "_confident" in state:
            rv.append(("jacklyn avatar events music_class_masturbation jacklyn_head11",940,145))
        rv.append(("jacklyn avatar events music_class_masturbation mc_body",596,789))
        if state.endswith(("stroke1","cum_proud")):
          rv.append(("jacklyn avatar events music_class_masturbation mc_arm1",882,949))
          if "_cum" in state:
            rv.append(("jacklyn avatar events music_class_masturbation cum1",883,507))
        elif state.endswith("stroke2"):
          rv.append(("jacklyn avatar events music_class_masturbation mc_arm2",882,905))
        elif state.endswith("stroke3"):
          rv.append(("jacklyn avatar events music_class_masturbation mc_arm3",882,856))
        elif state.endswith(("hide","cum_embarrassed")):
          rv.append(("jacklyn avatar events music_class_masturbation mc_arm4",641,786))
          if "_cum" in state:
            rv.append(("jacklyn avatar events music_class_masturbation cum2",869,824))

      elif state.startswith("jumping_jacks"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events jumping_jacks background",0,0))
        if state.endswith("up"):
          rv.append(("jacklyn avatar events jumping_jacks jacklyn_body1",17,0))
          rv.append(("jacklyn avatar events jumping_jacks jacklyn_underwear1",580,311))
          rv.append(("jacklyn avatar events jumping_jacks jacklyn_clothes1",18,0))
        elif state.endswith("down"):
          rv.append(("jacklyn avatar events jumping_jacks jacklyn_body2",255,144))
          rv.append(("jacklyn avatar events jumping_jacks jacklyn_underwear2",426,454))
          rv.append(("jacklyn avatar events jumping_jacks jacklyn_clothes2",257,398))

      elif state.startswith("paint_shelf_sex"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events paint_shelf_sex background",0,0))
        if state.endswith("kiss"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head1",1311,103))
        elif state.endswith(("grabbing_dick","lifting_up")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head2",1311,103))
        elif state.endswith("anticipation"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head3",1212,63))
        elif state.endswith(("grinding1","grinding1_slash_penetration1")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head4",1232,52))
        elif state.endswith("grinding2"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head5",1212,63))
        elif state.endswith("grinding3"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head6",1219,49))
        elif state.endswith("grinding4"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head7",1223,44))
        elif state.endswith("penetration1"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head8",1232,52))
        elif state.endswith("penetration2"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head9",1212,63))
        elif state.endswith("penetration3"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head10",1219,49))
        elif state.endswith("penetration4"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head11",1223,44))
        elif state.endswith("paintbrush_reveal"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head12",1282,84))
        elif state.endswith(("paintbrush_insertion","paintbrush_deeper")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head13",1282,84))
        elif state.endswith("cum"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_head14",1299,65))
        if state.endswith(("kiss","grabbing_dick","lifting_up","anticipation","grinding2","penetration2","paintbrush_reveal","paintbrush_insertion","paintbrush_deeper")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_body1",0,290))
        elif state.endswith(("grinding1","penetration1")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_body2",0,285))
        elif state.endswith(("grinding3","penetration3")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_body3",0,280))
        elif state.endswith(("grinding4","penetration4","cum")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_body4",0,290))
        if state.endswith(("kiss","grabbing_dick")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_leg1",35,659))
        elif state.endswith(("lifting_up","anticipation","grinding2","penetration2","paintbrush_reveal","paintbrush_insertion","paintbrush_deeper")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_leg2",0,0))
        elif state.endswith(("grinding1","penetration1")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_leg3",0,0))
        elif state.endswith(("grinding3","penetration3")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_leg4",0,0))
        elif state.endswith(("grinding4","penetration4")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_leg5",0,0))
        elif state.endswith("cum"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_leg6",0,0))
        if state.endswith("kiss"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm1",736,453))
        elif state.endswith("grabbing_dick"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm2",531,421))
        elif state.endswith("lifting_up"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm3",1146,161))
        elif state.endswith("anticipation"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm4",344,452))
        elif state.endswith(("grinding1","penetration1")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm5",942,0))
        elif state.endswith(("grinding2","penetration2","paintbrush_insertion","paintbrush_deeper")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm6",936,0))
        elif state.endswith(("grinding3","penetration3")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm7",950,0))
        elif state.endswith(("grinding4","penetration4","cum")):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm8",960,0))
        elif state.endswith("paintbrush_reveal"):
          rv.append(("jacklyn avatar events paint_shelf_sex jacklyn_left_arm9",924,12))
        if state.endswith("kiss"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick1",348,514))
        elif state.endswith("grabbing_dick"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick2",376,458))
        elif state.endswith("lifting_up"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick3",376,501))
        elif state.endswith("anticipation"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick4",278,594))
        elif state.endswith("grinding1"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick5",349,626))
        elif state.endswith(("grinding2","penetration2","paintbrush_reveal","paintbrush_insertion","paintbrush_deeper")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick6",350,617))
        elif state.endswith("grinding3"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick7",350,609))
        elif state.endswith("grinding4"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick8",350,599))
        elif state.endswith("penetration1"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick9",279,556))
        elif state.endswith("penetration3"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick10",380,627))
        elif state.endswith("penetration4"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick11",407,614))
        elif state.endswith("cum"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_dick12",408,614))
        if state.endswith(("kiss","grabbing_dick")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body1",0,0))
        elif state.endswith("lifting_up"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body2",0,0))
        elif state.endswith(("anticipation","grinding2")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body3",0,0))
        elif state.endswith("grinding1"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body4",0,0))
        elif state.endswith("grinding3"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body5",0,0))
        elif state.endswith("grinding4"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body6",0,0))
        elif state.endswith("penetration1"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body7",0,0))
        elif state.endswith(("penetration2","paintbrush_reveal","paintbrush_insertion","paintbrush_deeper")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body8",0,0))
        elif state.endswith("penetration3"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body9",0,0))
        elif state.endswith(("penetration4","cum")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_body10",0,0))
        if state.endswith("kiss"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm1",1073,0))
        elif state.endswith("grabbing_dick"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm2",1065,0))
        elif state.endswith("lifting_up"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm3",568,0))
        elif state.endswith(("anticipation","grinding2","penetration2","paintbrush_reveal")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm4",560,0))
        elif state.endswith(("grinding1","penetration1")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm5",550,0))
        elif state.endswith(("grinding3","penetration3")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm6",572,0))
        elif state.endswith(("grinding4","penetration4","cum")):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm7",605,0))
        elif state.endswith("paintbrush_insertion"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm8",199,0))
        elif state.endswith("paintbrush_deeper"):
          rv.append(("jacklyn avatar events paint_shelf_sex mc_right_arm9",292,0))

      elif state.startswith("paint_shelf_aftermath"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events paint_shelf_aftermath background",0,0))
        rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_body",116,0))
        if state.endswith("smile"):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_face1",210,219))
        elif state.endswith("annoyed"):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_face2",213,219))
        elif state.endswith(("licking","fingering")):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_face3",210,219))
        elif state.endswith("tasting"):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_face4",210,219))
        if state.endswith(("smile","annoyed")):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_right_arm1",726,803))
          rv.append(("jacklyn avatar events paint_shelf_aftermath cum",1264,638))
        elif state.endswith("fingering"):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_right_arm2",692,577))
        elif state.endswith("tasting"):
          rv.append(("jacklyn avatar events paint_shelf_aftermath jacklyn_right_arm3",333,366))
        if state.endswith("licking"):
          rv.append(("jacklyn avatar events paint_shelf_aftermath mc_body",655,585))

      elif state.startswith("marina_kiss"):
        rv.append((3840,1080))
        rv.append(("jacklyn avatar events marina_kiss background",0,0))
        if state.endswith("kiss"):
          rv.append(("jacklyn avatar events marina_kiss mc_head1",776,75))
          rv.append(("jacklyn avatar events marina_kiss jacklyn_head1",1074,257))
        else:
          rv.append(("jacklyn avatar events marina_kiss mc_head2",735,63))
          rv.append(("jacklyn avatar events marina_kiss jacklyn_head2",1118,266))
        rv.append(("jacklyn avatar events marina_kiss jacklyn_body",466,361))
        rv.append(("jacklyn avatar events marina_kiss jacklyn_clothes",815,464))
        if not state.endswith("floraless"):
          rv.append(("jacklyn avatar events marina_kiss flora_body",2913,295))
          rv.append(("jacklyn avatar events marina_kiss flora_underwear",2913,821))
          rv.append(("jacklyn avatar events marina_kiss flora_clothes",2793,270))

      elif state.startswith("alley_kiss"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events alley_kiss background",0,0))
        rv.append(("jacklyn avatar events alley_kiss mc_body",0,0))
        rv.append(("jacklyn avatar events alley_kiss jacklyn_body",0,69))
        rv.append(("jacklyn avatar events alley_kiss jacklyn_left_sleeve",447,230))
        if state.endswith("pinned"):
          rv.append(("jacklyn avatar events alley_kiss jacklyn_head1",179,120))
        elif state.endswith("kiss"):
          rv.append(("jacklyn avatar events alley_kiss jacklyn_head2",172,148))
        elif state.endswith(("boobs_exposed","pussy_exposed")):
          rv.append(("jacklyn avatar events alley_kiss jacklyn_head3",202,102))
        if state.endswith(("pinned","boobs_exposed","pussy_exposed")):
          rv.append(("jacklyn avatar events alley_kiss mc_head1",159,0))
        elif state.endswith("kiss"):
          rv.append(("jacklyn avatar events alley_kiss mc_head2",136,0))
        if state.endswith(("pinned","kiss")):
          rv.append(("jacklyn avatar events alley_kiss jacklyn_dress1",234,271))
          rv.append(("jacklyn avatar events alley_kiss mc_left_arm1",916,0))
        elif state.endswith("boobs_exposed"):
          rv.append(("jacklyn avatar events alley_kiss jacklyn_dress2",234,309))
          rv.append(("jacklyn avatar events alley_kiss mc_left_arm2",836,0))
        elif state.endswith("pussy_exposed"):
          rv.append(("jacklyn avatar events alley_kiss jacklyn_dress3",234,307))
          rv.append(("jacklyn avatar events alley_kiss mc_left_arm3",1160,0))

      elif state.startswith("alley_lick"):
        rv.append((1920,1080))
        rv.append(("jacklyn avatar events alley_lick background",0,0))
        if state.endswith(("licking1","hair_pull1")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head1",63,64))
          if state.endswith("licking1"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face1",305,179))
          elif state.endswith("hair_pull1"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face5",305,179))
        elif state.endswith(("licking2","hair_pull2")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head2",56,65))
          if state.endswith("licking2"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face2",296,177))
          elif state.endswith("hair_pull2"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face6",295,177))
        elif state.endswith(("licking3","hair_pull3")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head3",46,77))
          if state.endswith("licking3"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face3",290,185))
          elif state.endswith("hair_pull3"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face7",289,185))
        elif state.endswith(("licking4","hair_pull4")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head4",60,66))
          if state.endswith("licking4"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face4",303,180))
          elif state.endswith("hair_pull4"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face8",302,180))
        elif state.endswith("skirt_covered1"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head5",40,64))
          rv.append(("jacklyn avatar events alley_lick jacklyn_face9",265,163))
        elif state.endswith("skirt_covered2"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head6",34,64))
          rv.append(("jacklyn avatar events alley_lick jacklyn_face10",260,164))
        elif state.endswith(("skirt_covered3","orgasm")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head7",28,76))
          if state.endswith("skirt_covered3"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face11",252,170))
          elif state.endswith("orgasm"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_face13",252,170))
        elif state.endswith("skirt_covered4"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_head8",37,63))
          rv.append(("jacklyn avatar events alley_lick jacklyn_face12",265,163))
        if state.endswith(("licking1","hair_pull1","skirt_covered1")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_body1",218,0))
          rv.append(("jacklyn avatar events alley_lick mc_body1",853,107))
        elif state.endswith(("licking2","hair_pull2","skirt_covered2")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_body2",226,0))
          rv.append(("jacklyn avatar events alley_lick mc_body2",875,96))
        elif state.endswith(("licking3","hair_pull3","skirt_covered3","orgasm")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_body3",245,0))
          rv.append(("jacklyn avatar events alley_lick mc_body3",931,81))
          if state.endswith("orgasm"):
            rv.append(("jacklyn avatar events alley_lick jacklyn_squirt",1295,558))
        elif state.endswith(("licking4","hair_pull4","skirt_covered4")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_body4",222,0))
          rv.append(("jacklyn avatar events alley_lick mc_body4",869,112))
        if state.endswith("hair_pull1"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_left_arm1",660,76))
        elif state.endswith("hair_pull2"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_left_arm2",650,67))
        elif state.endswith("hair_pull3"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_left_arm3",678,59))
        elif state.endswith("hair_pull4"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_left_arm4",642,69))
        if state.endswith(("licking1","hair_pull1")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress1",217,128))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm1",152,266))
        elif state.endswith(("licking2","hair_pull2")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress2",223,92))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm2",157,240))
        elif state.endswith(("licking3","hair_pull3")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress3",246,87))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm3",174,237))
        elif state.endswith(("licking4","hair_pull4")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress4",209,106))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm4",153,258))
        elif state.endswith("skirt_covered1"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress5",217,123))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm5",147,515))
        elif state.endswith("skirt_covered2"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress6",223,123))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm6",151,518))
        elif state.endswith(("skirt_covered3","orgasm")):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress7",246,106))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm7",161,529))
        elif state.endswith("skirt_covered4"):
          rv.append(("jacklyn avatar events alley_lick jacklyn_dress8",209,123))
          rv.append(("jacklyn avatar events alley_lick jacklyn_right_arm8",148,520))

      return rv


  class Interactable_jacklyn(Interactable):

    def title(cls):
      return jacklyn.name

    def description(cls):
      if quest.jo_washed.in_progress:
        return random.choice(quest_jo_washed_investigate_lines)

    def actions(cls,actions):
      if jacklyn.at("school_art_class","pose") and mc.at("school_art_class"):
        actions.append(["consume","\''Paint\''","jacklyn_quest_jacklyn_broken_fuse_model_jacklyn"])

      else:
        ##########Talks####################
        if not (jacklyn.at("school_art_class","pose")
        or quest.jacklyn_statement in ("jackdate","intruder","statement")
        or quest.jo_washed.in_progress):
          if jacklyn["talk_limit_today"]<3:
            actions.append(["talk","Talk","?jacklyn_talk_one"])
            actions.append(["talk","Talk","?jacklyn_talk_two"])
            actions.append(["talk","Talk","?jacklyn_talk_three"])
            actions.append(["talk","Talk","?jacklyn_talk_four"])
            actions.append(["talk","Talk","?jacklyn_talk_five"])
            actions.append(["talk","Talk","?jacklyn_talk_six"])
            actions.append(["talk","Talk","?jacklyn_talk_seven"])
          else:
            actions.append(["talk","Talk","?jacklyn_talk_over"])
        ####################################

        #############Quests##################
        if mc["focus"]:
          if mc["focus"] == "jacklyn_statement":
            if quest.jacklyn_statement == "jackdate" and mc.at("home_kitchen"):
              actions.append(["quest","Quest","jacklyn_statement_jackdate"])
            elif quest.jacklyn_statement == "intruder":
              actions.append(["quest","Quest","?jacklyn_statement_intruder"])
            elif quest.jacklyn_statement == "statement":
              actions.append(["quest","Quest","?jacklyn_statement_statement"])
          elif mc["focus"] == "maxine_hook":
            if quest.maxine_hook == "interrogation":
              actions.append(["quest","Quest","?quest_maxine_hook_interrogation_jacklyn"])
          elif mc["focus"] == "jo_washed":
            if quest.jo_washed == "supplies" and not mc.owned_item("sponges"):
              actions.append(["quest","Quest","quest_jo_washed_supplies_jacklyn"])
            if quest.jo_washed.in_progress:
              actions.append(random.choice(quest_jo_washed_interact_lines))
          elif mc["focus"] == "kate_moment":
            if quest.kate_moment == "jacklyn":
              actions.append(["quest","Quest","?quest_kate_moment_jacklyn"])
        else:
          if game.season == 1:
            if game.location == "school_art_class":
              if quest.jacklyn_art_focus == "showjacklyn":
                actions.append(["quest","Quest","jacklyn_quest_art_focus_jacklyn"])
              elif not quest.jacklyn_art_focus.started:
                actions.append(["quest","Quest","?jacklyn_quest_art_focus_start"])
              if quest.isabelle_tour >= "art_class" and not quest.jacklyn_broken_fuse.started:
                actions.append(["quest","Quest","?jacklyn_quest_jacklyn_broken_fuse_start"])
              if quest.jacklyn_broken_fuse == "reward":
                actions.append(["quest","Quest","?jacklyn_quest_jacklyn_broken_fuse_end"])

          if quest.flora_jacklyn_introduction == "jacklyn":
            actions.append(["quest","Quest","?jacklyn_quest_flora_jacklyn_introduction"])

          if quest.isabelle_stolen == "askmaxine":
            if game.location == "school_art_class":
              actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_jacklyn_art"])
            else:
              actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_jacklyn"])

          if quest.isabelle_buried == "buried_jacklyn" and mc.at("school_art_class"):
            actions.append(["quest","Quest","?isabelle_quest_buried_jacklyn"])

          if quest.poolside_story == "decorations":
            actions.append(["quest","Quest","poolside_story_jacklyn_decoarations"])

          if quest.jacklyn_statement == "meetjacklyn" and mc.at("school_entrance"):
            actions.append(["quest","Quest","jacklyn_statement_meetjacklyn"])

          if quest.maxine_wine == "wine":
            actions.append(["quest","Quest","?quest_maxine_wine_jacklyn_deal"])
          if quest.maxine_wine == "jacklynpin":
            actions.append(["quest","Quest","?quest_maxine_wine_jacklyn_pin"])

          if quest.maxine_lines == "unscrew":
            actions.append(["quest","Quest","?quest_maxine_lines_unscrew"])

          if quest.jacklyn_sweets == "oldpromises" and quest.jacklyn_statement.finished:
            actions.append(["quest","Quest","?quest_jacklyn_sweets_oldpromises"])
          elif quest.jacklyn_sweets == "finaltouches":
            actions.append(["quest","Quest","?quest_jacklyn_sweets_finaltouches"])

          if quest.lindsey_motive == "jacklyn":
            actions.append(["quest","Quest","quest_lindsey_motive_jacklyn"])

          if quest.maya_spell == "sweat" and game.location == "school_art_class":
            actions.append(["quest","Quest","quest_maya_spell_sweat"])

          if quest.nurse_aid == "someone_else" and not quest.nurse_aid["jacklyn_on_board"]:
            actions.append(["quest","Quest","quest_nurse_aid_someone_else"])

        ####################################
          if game.season == 1:
            if (quest.isabelle_tour.finished and (quest.isabelle_over_kate.started and not quest.isabelle_over_kate.failed) and quest.clubroom_access.finished and not quest.isabelle_red.started
            and jacklyn.at("school_art_class","standing")):
              actions.append(["quest","Quest","quest_isabelle_red_start"])

          elif game.season == 2:
            if quest.jacklyn_sweets.finished and quest.fall_in_newfall.finished and quest.maya_witch.finished and not quest.jacklyn_romance.started:
              actions.append(["quest","Quest","quest_jacklyn_romance_start"])

        #############flirts#####################
        if not (jacklyn.at("school_art_class","pose")
        or quest.jacklyn_statement in ("jackdate","intruder","statement")
        or quest.jo_washed.in_progress):
          actions.append(["flirt","Flirt","?jacklyn_flirt_over"])
        ####################################


label jacklyn_flirt_over:
  show jacklyn laughing with Dissolve(.5)
  jacklyn "Sorry, baby cakes, it ain't happening."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label jacklyn_talk_one:
  show jacklyn smile with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn smile "Vision. That's the real x-factor in art."
  jacklyn smile "You can have all the mechanical skill in the world. Without a vision, you're just another wall-scrubber."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_two:
  show jacklyn excited with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn excited "Do you ever feel like the spirit of something truly great lives inside you?"
  mc "Can't say I do."
  jacklyn smile "Maybe one day."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_three:
  show jacklyn neutral with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn neutral "I don't give two fucks about social norms."
  jacklyn neutral "I'll queen the moshpit, send nudes to the president, and fucking fill my classroom with vaginal art."
  jacklyn laughing "Not that I dabble or anything."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_four:
  show jacklyn cringe with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn cringe "Minimalist bullshit can suck all three of my dicks."
  jacklyn cringe "If I'd been born in another generation, I would've pegged Judd."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_five:
  show jacklyn annoyed with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn annoyed "Did you know that my selfie-insta has more followers than my art one?"
  jacklyn annoyed "People are horny morons."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_six:
  show jacklyn neutral with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn neutral "Female artists always hunt my pussy."
  jacklyn annoyed "Even Mrs. Bloomer had a thing for these juice petals."
  jacklyn annoyed "I swear it's a fucking magnet or something."
  jacklyn neutral "Shame I don't dabble."
  hide jacklyn with Dissolve(.5)
  return

label jacklyn_talk_seven:
  show jacklyn thinking with Dissolve(.5)
  $jacklyn["talk_limit_today"]+=1
  jacklyn thinking "Symbolism is what rips open the subconscious mind."
  jacklyn thinking "Hidden phallic imagery that gives the unknowing masses a mental boner."
  jacklyn neutral "Weird how lesbians are always so into art."
  hide jacklyn with Dissolve(.5)
  return
