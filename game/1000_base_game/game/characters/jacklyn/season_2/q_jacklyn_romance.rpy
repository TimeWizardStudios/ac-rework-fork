label quest_jacklyn_romance_start:
  show jacklyn excited with Dissolve(.5)
  jacklyn excited "There he is! Mister art deco himself."
  mc "More like art dicko, am I right?"
  jacklyn laughing "That's prime recall, babylegs."
  if quest.jacklyn_sweets["jacklyn_blowjob"]:
    jacklyn excited "You know, art that enriches all the senses is totally transcendental."
    jacklyn excited "I hope you're drawing the blueprints for future murder!"
    "Heh, I bet she does."
    "[jacklyn] can probably still taste my last one..."
    "God, that was fantastic."
    "It really makes this whole art scene worth it."
    mc "I might have something brewing for my next piece..."
    mc "I hope to bring in the sense of touch a little more."
    $jacklyn.lust+=1
    jacklyn laughing "That's fully my bag."
    mc "Don't I know it."
  else:
    jacklyn excited "While tongues tango on the subject..."
    jacklyn excited "The Newfall Culinary loved your piece!"
    "Oh, I bet they loved my piece, all right."
    "That'll do it for the confidence."
    mc "Yeah? Let's see it, then!"
    jacklyn laughing "Pop your eye cherries on this crime scene."
    window hide
    show misc newfall_culinary_spread at truecenter with Dissolve(.5)
    $set_dialog_mode("default_no_bg")
    "" # This replicates the 'pause' statement, but also adds the Click-To-Continue arrow at the bottom right
    hide misc newfall_culinary_spread with Dissolve(.5)
    window auto
    $set_dialog_mode("")
    jacklyn laughing "Sickness overload, right?"
    show jacklyn laughing at move_to(.75)
    menu(side="left"):
      extend ""
      "\"As grisly as it gets.\"":
        show jacklyn laughing at move_to(.5)
        mc "As grisly as it gets."
        mc "If you're not pushing boundaries, what are you even doing, you know?{space=-45}"
        jacklyn excited "That's exactly what oils my pan too!"
        jacklyn excited "I knew you had the spark in you. It just requires some extra ignition."
        mc "Thankfully, I have you for that."
        $jacklyn.love+=1
        jacklyn smile "Exactamundo."
      "\"Oh, god. I can't believe they printed it!\"":
        show jacklyn laughing at move_to(.5)
        mc "Oh, god. I can't believe they printed it!"
        jacklyn annoyed "It's the avant of the modern art garden."
        $jacklyn.lust-=1
        jacklyn annoyed "Don't waste the body juice stressing it."
        mc "I guess..."
        mc "But it's still kind of embarrassing."
        jacklyn excited "Don't puss out on your art, my G. You have to sex those pistols!"
        mc "Err, you're right..."
        "...I think."
  jacklyn smile "So, on the subject of orgasmic freeballing art..."
  jacklyn smile "There's this art gallery showing next weekend. Are you down to join me in shooting up some muse-juice?"
  show jacklyn smile at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.charisma>=10@[mc.charisma]/10|{image=stats cha}|\"In need of some\narm candy, huh?\"":
      show jacklyn smile at move_to(.5)
      mc "In need of some arm candy, huh?"
      jacklyn laughing "Your confidence is drag racing around the corners!"
      jacklyn laughing "A surprising twist on the outside, but I'm vibing with it."
      mc "Thank you."
      mc "I think I can clear up my schedule to join you."
      jacklyn excited "Well, that just sets my moon into orbit!"
      jacklyn excited "I'll text you later with the deets, then."
      mc "Sounds good!"
      window hide
      hide jacklyn with Dissolve(.5)
      window auto
      "Sweet, unexpected weekend plans!"
      "And with [jacklyn] of all people? It should be a fun time."
      "..."
      "But I suppose I'll need something nice to wear..."
      "I can't wear my favorite hoodie to that sort of thing."
    "\"Are they displaying your work?\"":
      show jacklyn smile at move_to(.5)
      mc "Are they displaying your work?"
      jacklyn laughing "You're charming my snake right out of the basket!"
      mc "It's not a line. I mean it."
      mc "If they're not displaying your work, they will be soon."
      $jacklyn.love+=1
      jacklyn excited "That would defo light a fire under the kiln."
      jacklyn excited "We'll see how fresco the art is together. It's facts they'll have baller pieces on display. Some of the greats."
      jacklyn excited "I'll shoot you a buzz-buzz with more info when it gets closer."
      mc "I can't wait."
      jacklyn laughing "It's gonna be wicked!"
      window hide
      hide jacklyn with Dissolve(.5)
      window auto
      "Holy shit. I  can't believe that just happened!"
      "I need a game plan..."
      "Let's start with what I'm going to wear."
      "What does one wear to such a function?"
      "Definitely something nicer than one of my old hoodies."
      "..."
      "Maybe a well tailored suit?"
    "\"Oh? {i}You're{/} asking me\non a date this time?\"":
      show jacklyn smile at move_to(.5)
      mc "Oh? {i}You're{/} asking me on a date this time?"
      jacklyn laughing "Keeping it modern for the immodern times, you follow?"
      mc "For sure. A woman who's not afraid to go after what she wants is sexy as hell."
      $jacklyn.lust+=1
      jacklyn laughing "I'm down for anything that tickles the taint."
      "[jacklyn] is so free with her tongue..."
      "...in more ways than one..."
      "It really is infectious. I love her confidence and her complete lack of shits to give."
      mc "I bet you are. And I'll tickle anything you want."
      jacklyn annoyed "Easy there, fine china. Let's soak in some art first."
      jacklyn annoyed "Then, we'll see what happens."
      mc "Cool, cool, cool."
      jacklyn excited "The coolest steel!"
      jacklyn excited "I'll hit you up with the addy of the joint and whatever soon."
      mc "I'm looking forward to it."
      window hide
      hide jacklyn with Dissolve(.5)
      window auto
      "This is going to be good!"
      "..."
      "I better figure out what to wear, though."
      "Standing next to [jacklyn], it has to be something great."
  window hide
  $quest.jacklyn_romance.start()
  return

label quest_jacklyn_romance_suit_upon_entering:
  "Man, the fall air off the water really brings a sharp chill."
  "It enlivens the senses, enters your nose and lungs."
  "Sea salt under a light drizzle."
  "..."
  "Or maybe I'm just high off the anticipation of the gallery exhibit with [jacklyn]..."
  "It feels good to be invited somewhere."
  "To have something to look forward to."
  "Shattering my own loneliness and accepting that people aren't\nall bad."
  "That life is about taking risks, big and small."
  "About saying yes to the unknown."
  "In my old life, I would have thought it was a cruel prank."
  "Now, I want to look as good on the outside as I feel on the inside."
  "And what better place to get a suit than {i}Newfall Tailor and Aquarium?{/}{space=-35}"
  $quest.jacklyn_romance["what_better_place"] = True
  return

label quest_jacklyn_romance_suit:
  if quest.jacklyn_romance == "suit":
    "Okay, time to get suited up!"
    "..."
    "Huh? Why is the door locked?"
    "And what the hell is this? A note?"
  "{i}\"Dearest customer, heckler, solicitor, pervert, cross-stitching mafia member, or tree with tangled up vines...\"{/}"
  "{i}\"I regret to inform you this store has been closed.\"{/}"
  "{i}\"Please, for your own safety, do not seek answers as to why.\"{/}"
  "{i}\"Or do. I don't really give a fuck.\"{/}"
  "{i}\"I'm sorry for any inconvenience.\"{/}"
  "{i}\"Just kidding. Fuck you.\"{/}"
  "{i}\"Kind Regards, Kalvin K. Taylor.\"{/}"
  if quest.jacklyn_romance == "suit":
    "Well, that's weird..."
    "And definitely a problem for me."
    "What the hell am I supposed to do now?"
    "..."
    "I wonder if [flora] knows what happened? She's always going on and on about this place."
    $quest.jacklyn_romance.advance("flora")
  return

label quest_jacklyn_romance_flora:
  show flora skeptical with Dissolve(.5)
  flora skeptical "You're walking funny."
  mc "What?"
  flora skeptical "Why are you walking like that?"
  mc "Like what?"
  flora confused "Shoulders back, spine straight."
  mc "Err, with confidence?"
  flora sarcastic "Aha! That's it!"
  "I'm not sure how I should feel about this..."
  mc "Well, thanks for noticing, I guess."
  flora sarcastic "I almost mistook you for someone else."
  mc "Did it make you all hot and bothered?"
  flora afraid "What? Gross!"
  mc "That's so mean!"
  flora skeptical "You deserved it."
  mc "I guess that's fair."
  flora skeptical "What has you all peacocky, anyway?"
  show flora skeptical at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Why are you so interested\nin my physique?\"":
      show flora skeptical at move_to(.5)
      mc "Why are you so interested in my physique?"
      flora angry "Ugh, I'm not!"
      flora confident "And let's be honest, it's more like phys-geek."
      mc "Good one."
      flora confident "I know."
      mc "I'll be sure to keep it up, just for you."
      flora concerned "You wish I cared."
      mc "Keep telling yourself you don't."
      $flora.lust+=1
      flora concerned "Whatever, you weirdo."
    "?mc.strength>=10@[mc.strength]/10|{image=stats str}|\"It's amazing what eating right and{space=-55}\ntaking care of yourself will do.\"{space=-5}":
      show flora skeptical at move_to(.5)
      mc "It's amazing what eating right and taking care of yourself will do."
      flora confident "Since when do you do any of that?"
      if quest.kate_stepping.finished:
        mc "Dude, I spend mad time at the gym. Ask [kate]."
      elif quest.isabelle_dethroning.finished:
        mc "Dude, I spend mad time at the gym. Ask [stacy]."
      flora angry "Don't call me that! I've told you a million times!"
      mc "Sorry, bro. I'll try to remember next time."
      flora angry "I will strangle you!"
      mc "And I will like it."
      flora concerned "..."
      $mc.charisma+=1
      mc "The point is, if I feel better, I'll look better."
      flora concerned "Right..."
      $flora.lust+=1
      flora confident "I'm not sure who's been brainwashing you, but well done, I guess."
      mc "Thank you."
    "\"Heh. Cock.\"":
      show flora skeptical at move_to(.5)
      mc "Heh. Cock."
      flora angry "Just when I think you can't be any more immature..."
      mc "I prefer to call it the whimsy of youth."
      flora concerned "Soon you'll be calling it {i}alone forever.{/}"
      mc "I'll never be alone as long as I have you."
      flora concerned "Keep talking like that, and you'll be alone faster than you think."
      "Ouch. She has no idea how familiar I am with that situation..."
      mc "You're right. I'm sorry."
      mc "Having you in my life does make every day a little bit better."
      flora afraid "..."
      flora afraid "What has gotten into you?"
      flora afraid "I don't think I like it."
      mc "That's okay. I just want you to know that despite everything, I really appreciate you."
      $flora.love+=1
      flora embarrassed "Point taken! You can stop now!"
  mc "Anyway, I had a question for you."
  flora thinking "You're really pushing your luck today, aren't you?"
  mc "Well, I'm feeling lucky."
  mc "Do you know why the Newfall Tailor & Aquarium has closed?"
  flora worried "Can't say I do."
  "Huh. I was expecting a more outraged reaction from her."
  flora worried "The owner just mysteriously disappeared the other day. I hope he comes back soon."
  flora blush "I was really looking forward to getting a new pair of boots."
  mc "That's why his return is important? Not because he mysteriously disappeared?"
  flora laughing "That too! I'm not a monster, you know?"
  mc "True, you're not."
  mc "And a non-monster would know where I can get a suit now, wouldn't they?"
  flora afraid "...tactical manipulation?!"
  mc "What can I say? I learned from the best."
  flora skeptical "Why do you even need a suit?"
  mc "Err, it's nothing, really."
  mc "I just need to have one on hand, you know?"
  mc "You never know when an occasion might call for one."
  flora skeptical "Right..."
  flora skeptical "Keep this up, and your funeral could be any day now."
  mc "...threats?!"
  flora confident "Just an observation!"
  flora confident "Especially when you insist on keeping secrets. It's bad for your health.{space=-45}"
  mc "I'll risk it."
  flora angry "Fine."
  flora angry "You could always talk to [maya]. She keeps up with fashion more than{space=-5}\nanyone I know, and is good with a needle and thread."
  mc "Hey, that's a good idea! Thanks, [flora]!"
  flora concerned "I still think it's weird that you of all people want a suit."
  window hide
  hide flora with Dissolve(.5)
  $quest.jacklyn_romance.advance("maya")
  return

label quest_jacklyn_romance_maya:
  show maya neutral with Dissolve(.5)
  mc "Hello, [maya]. Looking radiant as ever today."
  maya smile "Aw, thanks!"
  maya smile "It is my one true goal in life to be appealing to your male gaze, at\nall times."
  mc "Err, that's not what I meant..."
  maya smile "No, no! It's cool. My job is done."
  maya smile "I can die happily in my bathtub now."
  show maya smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Just be quiet and take the\ncompliment, will you?\"":
      show maya smile at move_to(.5)
      mc "Just be quiet and take the compliment, will you?"
      maya sarcastic "I can do at least one of those things."
      $maya.lust+=1
      maya sarcastic "Thank you, [mc]."
      mc "That's fine. I actually like it when you talk."
      mc "And you're welcome."
      maya dramatic "Oh, you like my little throat noises? You just made my entire decade!{space=-20}"
      "Aaaand right back to the sarcasm. I thought I had her there for a moment."
    "\"I'm sorry. I didn't mean anything by it.\"":
      show maya smile at move_to(.5)
      mc "I'm sorry. I didn't mean anything by it."
      $maya.lust-=1
      maya skeptical "So, you don't think I look radiant?"
      mc "No! I mean, yes! I mean—"
      maya confident "Oh, [mc]. You precious virgin Jesus child."
      mc "I'm... not..."
      maya bored "Anyway..."
      maya bored "What do you want?"
    "\"Sounds like a good way to go out.\"":
      show maya smile at move_to(.5)
      mc "Sounds like a good way to go out."
      mc "Bottle of wine."
      mc "Making my day a little brighter because my male gaze has been satisfied.{space=-95}"
      $maya.love+=1
      maya sarcastic "Haha! You're goddamn welcome."
    "\"You shouldn't joke about that.\"":
      show maya smile at move_to(.5)
      mc "You shouldn't joke about that."
      mc "You're not dying in a tub. You're going to be around forever."
      $maya.love-=1
      maya cringe "I can joke about whatever I want."
      maya dramatic "But you're probably right about me being around forever..."
      maya dramatic "I have nine lives, after all."
      mc "That's the spirit!"
      maya sarcastic "Yes, and after my last one, I'm hoping to come back as a hissing ghost.{space=-65}"
      mc "..."
  mc "So, I have a favor to ask of you."
  maya thinking "Is it sexual?"
  mc "What? No!"
  if game.dow_str == "Tuesday":
    maya flirty "Good, because I only do those sorts of favors on Monday mornings.{space=-10}"
  else:
    maya flirty "Good, because I only do those sorts of favors on Tuesday mornings.{space=-20}"
  mc "I'm sure you do..."
  mc "Anyway! The only tailor in town is closed."
  mc "I was wondering if you could help me in acquiring a suit?"
  maya concerned "That sounds like a lot of work."
  mc "Not really..."
  maya skeptical "You don't know what I currently have on my plate."
  mc "Fair enough, I guess."
  maya confident "But!"
  maya confident "I would do it. For a price."
  show maya confident at move_to(.25)
  menu(side="right"):
    extend ""
    "\"A sexual favor?\"":
      show maya confident at move_to(.5)
      mc "A sexual favor?"
      $maya.love+=1
      maya flirty "Oh, my! He gives as well as he takes!"
      mc "I'm just trying to do my part, you know?"
      maya kiss "Such a modern man..."
      mc "A modern man in need of a modern suit."
      maya sarcastic "Hah!"
      mc "Funny and true."
      maya sarcastic "All right, fine."
      maya sarcastic "But I need you to find my balls of yarn for me first."
    "\"Name it!\"":
      show maya confident at move_to(.5)
      mc "Name it!"
      maya bored "Damn, you fold faster than an origami swan."
      mc "Could I have bargained?"
      maya bored "Not anymore."
      mc "Crap."
      maya dramatic "Relax, I don't want your soul or anything!"
      maya sarcastic "...yet."
      mc "How comforting."
      maya sarcastic "I'm nothing if not comforting and accommodating."
      maya sarcastic "I actually need you to find my balls of yarn for me."
    "?mc.owned_item('cat_tail')@|{image=items cat_tail}|\"I have something of yours\nyou may want back.\"":
      show maya confident at move_to(.5)
      mc "I have something of yours you may want back."
      maya eyeroll "I highly doubt that."
      mc "Oh, I wouldn't be so sure."
      mc "Does your first little visit to my house ring any bells?"
      mc "Particularly, your visit to the bathroom?"
      maya smile "This is getting interesting..."
      mc "You made a mess in there, and I got in trouble because [jo] blamed me for it."
      maya annoyed "Ah."
      mc "Yeah. So, basically, you owe me."
      mc "And you can have this back."
      window hide
      $mc.remove_item("cat_tail")
      pause 0.25
      window auto show
      show maya blush with dissolve2
      maya blush "Oh, all right, then."
      $maya.lust+=3
      maya blush "Deal... and, um, thank you."
      mc "No worries."
      maya blush "Give me a couple of days and I'll have what you need."
      mc "Awesome, thanks!"
      maya confident "Mention it!"
      mc "Very funny."
      maya bored "I mean it. Mention your gratitude often and sincerely."
      mc "We'll see how the suit turns out first."
      maya confident "Good move."
      maya confident "Anyway, off to the slave labor factory!"
      window hide
      hide maya with Dissolve(.5)
      $quest.jacklyn_romance["cat_tail_returned"] = True
      $quest.jacklyn_romance.advance("wait")
      return
  mc "To sew the suit?"
  maya cringe "Nope. Some dickweed took my stash and hid it."
  maya cringe "I just want it back. Do you think you can handle that?"
  mc "Finding your yarn? Yeah, I'm pretty sure I can handle that."
  maya dramatic "Great. Let me know if you find them."
  mc "You got it."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "Pfft! How hard can finding [maya]'s balls of yarn be?"
  "I'll be looking spiffy in that new suit in no time!"
  $quest.jacklyn_romance.advance("hide_and_seek")
  return

label quest_jacklyn_romance_hide_and_seek:
  if quest.jacklyn_romance["balls_of_yarn_found"] in (1,2,3,4):
    show maya neutral with Dissolve(.5)
    mc "Here are your balls of yarn back."
    maya neutral "..."
    python:
      balls_of_yarn_found = [item[0].split("_")[-1] for item in mc.inv if item[0].startswith("ball_of_yarn_")]
      balls_of_yarn = "{}...".format(balls_of_yarn_found[0]).title()
      del balls_of_yarn_found[0]
      while len(balls_of_yarn_found) > 0:
        balls_of_yarn += " {}...".format(balls_of_yarn_found[0])
        del balls_of_yarn_found[0]
    if quest.jacklyn_romance["balls_of_yarn_found"] > 1:
      maya neutral "[balls_of_yarn]"
    maya annoyed "No, no! I distinctly remember having more than that!"
  else:
    show maya cringe with Dissolve(.5)
    maya cringe "Back already?"
    mc "I can't find any balls of yarn..."
    maya dramatic "Well, then I can't find the time to help you."
  window hide
  hide maya with Dissolve(.5)
  return

label quest_jacklyn_romance_hide_and_seek_school_ground_floor_upon_entering:
  "If I were a ball of yarn, where would I hide in this big old school?"
  "There are so many options, and so little time..."
  $quest.jacklyn_romance["so_many_options"] = True
  return

label quest_jacklyn_romance_hide_and_seek_school_clubroom_upon_entering:
  "If anyone is hoarding balls of yarn besides [maya], it's got to be [spinach], right?"
  $quest.jacklyn_romance["got_to_be_spinach"] = True
  return

label quest_jacklyn_romance_hide_and_seek_school_clubroom_spinach:
  mc "Ah-ha! I knew you were a yarn thief!"
  mc "..."
  mc "[spinach]? What's wrong, girl?"
  "She looks a little... stiff?"
  window hide
  show maxine chair_back
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  mc "Huh?"
  window hide
  pause 0.125
  show maxine chair_front with Dissolve(.5)
  pause 0.125
  window auto
  maxine chair_front "Here he is."
  mc "Jesus, [maxine]!"
  maxine chair_front "Alas, no."
  maxine chair_front "Though I did have a singular success at turning wine into water."
  mc "No, I mean you scared the crap out of me!"
  maxine chair_front "Please dispense with your soiled undergarments elsewhere, then."
  mc "You know what? Nevermind."
  mc "Why the hell is there a fake [spinach] in your office?"
  maxine chair_front "We knew you would come."
  mc "..."
  mc "I need you to give me that ball of yarn."
  maxine chair_front "That, I cannot do."
  mc "I'll give you anything you ask!"
  maxine chair_front "Are you asking us for a favor?"
  mc "Err, I guess?"
  maxine "Have you come to us as a friend?"
  menu(side="middle"):
    extend ""
    "\"Yes, I have.\"":
      mc "Yes, I have."
      maxine chair_front "Mm-hmm."
      maxine chair_front "In the many years I've known you for, this is the first time you've come to me for help."
      mc "What? That's not true! I ask you for help all the time!"
      maxine chair_front "But you never invite me over to your house, or take me out for a cup of coffee."
      mc "Well, would you even want to go out for a cup of coffee? I thought you were too busy for that."
      maxine chair_front "Mm-hmm."
      maxine chair_front "But you didn't ask."
      mc "I guess that's fair..."
    "\"No, I come as a lover.\"":
      mc "No, I come as a lover."
      maxine chair_front "To whom, if I may ask?"
      mc "No, I meant that when I make love, I come."
      maxine chair_front "..."
      "How do you like your own medicine, [maxine]?"
      $maxine.lust+=1
      maxine chair_front "That is good to hear. A lot of men suffer from erectile dysfunction."
      mc "Well, that ain't me."
    "\"I'm just looking for a quick hook up.\"":
      mc "I'm just looking for a quick hook up."
      maxine chair_front "Will you provide the plug or the socket?"
      $mc.lust+=1
      mc "If you bend over, I can plug your socket."
      maxine chair_front "I had mine removed months ago."
      mc "Err, you did what?"
      maxine chair_front "It was a security risk. I didn't want to get a virus."
      mc "Just... use a condom..."
      maxine chair_front "An interesting proposition, but electricity doesn't flow well through rubber."
      mc "..."
      mc "I think we're talking about two completely different things..."
      maxine chair_front "I doubt it."
  mc "Anyway, do you mind if I take the ball of yarn?"
  maxine chair_front "Mm-hmm."
  "I'm not entirely sure what this bit is about, but I've had enough."
  mc "I'm just going to take it, then."
  maxine chair_front "If you really think that's in your best interest."
  mc "I guess I do."
  window hide
  $quest.jacklyn_romance["balls_of_yarn_found"]+=1
  $school_clubroom["ball_of_yarn_taken"] = True
  $mc.add_item("ball_of_yarn_red",silent=True)
  $game.notify_modal(None,"Inventory","{image=items ball_of_yarn_red}{space=50}|Gained\n{color=#900}Red Ball of Yarn{/color}\n("+str(quest.jacklyn_romance["balls_of_yarn_found"])+"/5)",5.0)
  pause 0.25
  window auto
  maxine chair_front "Very well."
  maxine chair_front "Just know that there may or may not come a day when this debt must be paid."
  maxine chair_front "If [spinach] calls upon you, I expect you to be ready."
  mc "Err, sure thing."
  maxine chair_front "Arrivederci now."
  window hide
  pause 0.125
  show maxine chair_back with Dissolve(.5)
  pause 0.125
  window auto
  "Well, that was fucking weird..."
  "But at least I have what I came here for."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  hide maxine
  pause 0.5
# hide maxine
  hide black onlayer screens
  with Dissolve(.5)
  return

label quest_jacklyn_romance_hide_and_seek_school_art_class_upon_entering:
  "Yarn is art, art is yarn."
  "Where are you hiding?"
  window hide
  show jacklyn annoyed with Dissolve(.5)
  window auto
  jacklyn annoyed "Your peepers seem to be working overtime."
  mc "Err, sorry! I wasn't trying to stare..."
  jacklyn laughing "If you were, it wouldn't wrinkle my foreskin."
  mc "I'm just helping a friend find something she lost."
  jacklyn excited "That's hot sauce!"
  mc "Not as hot as the sauce is going to be at the gallery."
  jacklyn thinking "..."
  mc "Because of, err... the art!"
  jacklyn smile_hands_down "Wicked."
  jacklyn smile_hands_down "Still down to light up the culture with me, I take it?"
  mc "More down than a whore in Australia!"
  jacklyn neutral "Your lyrics need work, but I can dig it."
  jacklyn neutral "I'll fire off the electric words at you later."
  mc "Sounds good!"
  window hide
  hide jacklyn with Dissolve(.5)
  window auto
  "Crap, I could have played that cooler..."
  "...but at least [jacklyn] didn't revoke the invitation."
  $quest.jacklyn_romance["revoke_the_invitation"] = True
  return

label quest_jacklyn_romance_hide_and_seek_school_gym_upon_entering:
  "God, this is more of a workout than I usually get from this place..."
  "Running around... finding all these balls of yarn..."
  "I sure hope [maya] is able to work some magic and make this all worth it."
  $quest.jacklyn_romance["work_some_magic"] = True
  return

label quest_jacklyn_romance_hide_and_seek_school_ground_floor_west_upon_leaving:
  show jo worried at appear_from_left
  pause 0.5
  window auto
  jo worried "[mc]? Why are you lurking out here?"
  "Jeez, [jo] makes me sound like such a creeper..."
  mc "I'm not lurking!"
  mc "I was, err... looking for something."
  jo thinking "And what is that?"
  show jo thinking at move_to(.25)
  menu(side="right"):
    extend ""
    "\"If you must know, it's for [maya].\"":
      show jo thinking at move_to(.5)
      mc "If you must know, it's for [maya]."
      jo skeptical "No need for that tone, mister."
      jo skeptical "I didn't know you were friends with [maya]."
      mc "Well, we are."
      mc "...kind of."
      mc "She's helping me out with something, and I'm helping her find her balls of yarn."
      $jo.love+=1
      jo smile "I see. That's kind of you, [mc]."
      jo smile "I noticed she's a bit of a loner, so it's good of you to be inclusive of everyone."
      mc "Thanks... I try."
      "...and I'm totally not focused on my own needs right now."
      jo confident "Well, I'm glad."
      jo confident "Keep that sort of thing up, honey."
    "\"I'd rather keep it to myself.\"":
      show jo thinking at move_to(.5)
      mc "I'd rather keep it to myself."
      jo concerned "You're not running around getting into trouble, are you, young man?{space=-5}"
      mc "Of course not! It's just... private matters."
      jo skeptical "Mm-hmm."
      mc "I'm growing up, and I have things I'd rather keep to myself."
      mc "But I promise it's nothing bad."
      jo smile "You're right. I should give you the freedom and trust for that."
      mc "Thank you, [jo]."
      jo smile "It's hard to see you growing up so fast, but I am proud of you."
      mc "Now you're just going to embarrass me..."
      jo confident "Hush, you! And let this old lady be proud of her young man."
      mc "Okay, okay!"
      mc "But you're not that old."
      $jo.love+=1
      jo confident "Growing up and still a sweetheart..."
  window hide
  show jo confident at disappear_to_left
  if quest.jacklyn_romance["balls_of_yarn_found"] == 5:
    pause 0.75
    jump quest_jacklyn_romance_hide_and_seek_found_them
  pause 1.0
  return

label quest_jacklyn_romance_hide_and_seek_found_them:
  pause 0.25
  window auto
  "Phew! I guess that's all of [maya]'s balls of yarn."
  "What a hassle to look good..."
  "She better not have changed her mind."
  $quest.jacklyn_romance.advance("found_them")
  return

label quest_jacklyn_romance_found_them:
  show maya neutral with Dissolve(.5)
  mc "Hello, [maya]."
  mc "I have the stuff you need and I'm ready to make the drop."
  maya smile "Oh, sweet, my heroin!"
  mc "..."
  maya annoyed "What? A girl's gotta have her vices."
  mc "I guess that's true."
  mc "Here you go."
  window hide
  $mc.remove_item("ball_of_yarn_blue")
  $mc.remove_item("ball_of_yarn_gray")
  $mc.remove_item("ball_of_yarn_pink")
  $mc.remove_item("ball_of_yarn_red")
  $mc.remove_item("ball_of_yarn_yellow")
  pause 0.25
  window auto show
  show maya smile_hands_in_pockets with dissolve2
  maya smile_hands_in_pockets "Excellent! Thank you."
  if quest.maya_sauce["bedroom_taken_over"]:
    maya smile_hands_in_pockets "I'll get something going for you, and I'll leave it by your bed when I'm done."
  else:
    maya smile_hands_in_pockets "I'll get something going for you, and I'll leave it at your place when I'm done."
  mc "What are you going to do, exactly?"
  maya thinking "I'm going to rework a vintage suit a bit..."
  mc "Vintage?"
  maya flirty "Don't worry, you'll be the belle of the ball when I'm done."
  mc "Heh."
  mc "I wasn't worried. And I appreciate it."
  maya dramatic "What do you need the suit for, anyway?"
  mc "It's for an art gallery exhibit. [jacklyn] invited me to go along with her.{space=-10}"
  maya sarcastic "Oh? I'm not sure what she owes you for, but well played."
  mc "Very funny! She doesn't owe me anything."
  maya sarcastic "Well, isn't that just the bee's tits?"
  maya sarcastic "Good for you."
  mc "Thanks, I guess."
  window hide
  hide maya with Dissolve(.5)
  window auto
  "And just like that, there goes [maya]."
  "She's not one to waste words when she doesn't feel like it, that's for sure."
  "But it's okay, I'm a man on a mission. One step closer to an evening out with [jacklyn]."
  "Now, for the waiting game..."
  $quest.jacklyn_romance.advance("wait")
  return

label quest_jacklyn_romance_wait_cat_tail:
  show maya neutral with Dissolve(.5)
  mc "There's my fairy godmother!"
  mc "How's the suit coming along?"
  maya eyeroll "If I'm a fairy godmother, then I want a hotter Cinderella."
  mc "Sorry, you're stuck with a budget Prince Charming."
  maya annoyed "Why can I never have nice things?"
  mc "Quit pouting. That tail of yours is very nice."
  mc "I bet it would look even nicer on you... or rather, in you."
  maya skeptical "Inappropriate much?"
  mc "Look who's talking."
  if not quest.jacklyn_romance["inappropriate_much"]:
    $quest.jacklyn_romance["inappropriate_much"] = True
    $maya.lust+=2
  maya blush "..."
  mc "I don't see you speechless very often. It's kind of cute."
  maya confident "Please stop before I fall hopelessly in love."
  mc "Fine, fine, fine."
  mc "So, what about the suit?"
  maya dramatic "Well, I pulled a few strings and threads. Two-hundred, to be exact."
  maya dramatic "It's not done yet, though. There are still some finishing touches left."
  if quest.maya_sauce["bedroom_taken_over"]:
    maya dramatic "I'll drop it off by your bed in one to two business days."
  else:
    maya dramatic "I'll drop it off at your place in one to two business days."
  mc "Awesome, thank you! I bet it's great."
  maya sarcastic "It's practically the Mona Lisa of suits. It even has real pockets."
  mc "Heh! I definitely came to the right person, then."
  maya sarcastic "I get that often. Especially from the tricks at the corner."
  mc "..."
  window hide
  hide maya with Dissolve(.5)
  return

label quest_jacklyn_romance_wait:
  show maya neutral with Dissolve(.5)
  mc "Hey! How's the suit coming along?"
  maya smile "Oh, yeah, I whipped something up in about an hour."
  maya smile "I went ahead and solved sex trafficking too, while I was at it."
  mc "..."
  mc "I take it it's not done yet?"
  maya eyeroll "Of course not."
  maya eyeroll "Come back in one to two business days."
  mc "Right. Sorry."
  window hide
  hide maya with Dissolve(.5)
  return

label quest_jacklyn_romance_suit_done:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "Mmm...?"
  "Oh, sweet! [maya] came through!"
  if quest.maya_sauce["bedroom_taken_over"]:
    "I'm going to be looking like an art piece myself in this bad boy!"
  else:
    "Although, I'm not sure who let her into my bedroom when I was sleeping..."
    "Hopefully, she didn't take advantage of me."
    "..."
    "Oh, who am I kidding? I wouldn't have minded if she did."
    "Either way, I'm going to be looking like an art piece myself in this bad boy!"
  "..."
  "Okay, not really."
  "But I'll definitely feel more confident next to [jacklyn] in this."
  "I just have to wait for her to give me more information now..."
  window hide
  play sound "open_door"
  pause 0.75
  show flora embarrassed at appear_from_left
  pause 0.5
  window auto
  flora embarrassed "How could you, [mc]?!"
  "Great, what have I done now?"
  "I better not admit guilt until she's more specific..."
  "And even then, I have the right to plead."
  "Not the fifth. Just in general."
  mc "What's wrong, [flora]?"
  flora embarrassed "You know what you did!"
  mc "If you're trying to frame me, it's not going to work this time."
  flora skeptical "I ran into [maya] earlier when she was dropping off your little suit!"
  "Shit."
  mc "Err, so?"
  flora sad "So, she told me what you're doing!"
  flora sad "An art gallery exhibit with [jacklyn]? You can't go, [mc]."
  if flora["madeamends"]:
    flora worried "I mean, what about all that talk about us?"
    flora thinking "I've been thinking about the pros and cons, and..."
    flora blush "...well, I think the pros might outweigh the cons."
    "Holy shit."
    "I didn't think she'd actually come to that conclusion..."
    "I thought that would be too much to hope for."
    mc "Really...?"
    flora blush "Yes, really."
    flora worried "If you go with her, I won't ever forgive you."
    show flora worried at move_to(.25)
  else:
    flora sad "I mean, how could you do that to me when you know how I feel?"
    "Damn, she went from ready to rip my head off to looking utterly defeated."
    "There really must be something going on there with [flora] and [jacklyn]..."
    "But how serious can it really be?"
    "Especially for [jacklyn], who's always talking about not dabbling?"
    flora sad "If you go with her, I won't ever forgive you."
    show flora sad at move_to(.25)
  menu(side="right"):
    extend ""
    "?flora.love>=15@[flora.love]/15|{image=flora contact_icon}|{image=stats love_3}|\"I didn't realize it\nupset you so much.\nI won't go.\"":
      if renpy.showing("flora worried"):
        show flora worried at move_to(.5)
      elif renpy.showing("flora sad"):
        show flora sad at move_to(.5)
      mc "I didn't realize it upset you so much. I won't go."
      flora thinking "Really? You mean it?"
      if flora["madeamends"]:
        mc "Yes, really. I don't want to hurt you."
        mc "And if you feel like you actually want to pursue something in secret with me, then I'm not about to ruin that for some silly art."
        flora flirty "[mc]..."
        flora flirty "That, um, that actually means a lot."
        mc "I don't know what exactly the future holds..."
        mc "...but I don't want to jeopardize the chance of a happy one."
        $flora.love+=3
        flora blush "Me neither."
      else:
        mc "Yes, I do. It's no big deal to me."
        mc "And if it's upsetting to you, then it's not a hard choice to make."
        flora blush "Oh, um... thank you..."
        flora blush "That actually means a lot to me."
        mc "Well, {i}you{/} mean a lot to me."
        flora laughing "[mc]!"
        mc "What? It's true."
        $flora.love+=5
        flora blush "It's just weird to hear you say it... but same."
      $quest.jacklyn_romance["i_wont_go"] = True
    "?flora.lust>=10@[flora.lust]/10|{image=flora contact_icon}|{image=stats lust_3}|\"Hey, [jacklyn] and I\nare strictly platonic,\nokay? I'm just going\nfor the art.\"":
      if renpy.showing("flora worried"):
        show flora worried at move_to(.5)
      elif renpy.showing("flora sad"):
        show flora sad at move_to(.5)
      mc "Hey, [jacklyn] and I are strictly platonic, okay? I'm just going for the art.{space=-50}"
      flora thinking "Do you promise?"
      mc "I'll swear on a stack of porn magazines right now."
      flora worried "..."
      flora worried "Gross. Also unnecessary."
      flora blush "But if you say it's just for the art... then I choose to believe you."
      mc "Ever since my painting, I've felt inspired."
      mc "I think this will be good for me to broaden my artistic horizons."
      flora laughing "I wouldn't push it!"
      mc "Heh."
      flora worried "You're not going to, like, resent me, though, are you?"
      "It's a surprise to see the genuine worry on [flora]'s face, but a welcome one."
      "The fact she's taking my feelings into account, despite the turbulence of her own..."
      "It means more to me than she realizes."
      mc "I don't think such a thing is possible. You're too cute to resent."
      flora blush "[mc]! Don't say things like that!"
      mc "What? The truth?"
      flora flirty "..."
      flora flirty "Is it?"
      mc "Of course it is."
      mc "Your warm smile... your cute freckles... your beautiful eyes..."
      flora laughing "Okay, now you're really embarrassing me!"
      mc "Sorry, but you should be told these things more often."
      flora blush "Well... thank you..."
    "\"Stop being such a brat. I'm going\nto the exhibit with [jacklyn].\"":
      if renpy.showing("flora worried"):
        show flora worried at move_to(.5)
      elif renpy.showing("flora sad"):
        show flora sad at move_to(.5)
      mc "Stop being such a brat. I'm going to the exhibit with [jacklyn]."
      flora angry "You're being selfish!"
      mc "Am I? You're the one acting like a dog with a bone."
      flora angry "Seriously? Calling me a dog now?!"
      mc "I didn't say that!"
      if flora["madeamends"]:
        flora angry "You've just been messing with my head all this time!"
        flora angry "And toying with my emotions!"
        mc "I didn't mean to, okay? I've just been confused."
        mc "You're both so different... but also intriguing..."
        flora angry "That's a lame excuse."
      flora confident "You know this isn't going to last, right?"
      flora confident "There's no way [jacklyn] is actually into you."
      mc "Jealousy isn't a good look for you."
      flora concerned "Yeah, well, your little suit isn't going to do much for {i}your{/} look."
      mc "We'll just have to let [jacklyn] be the judge of that, won't we?"
      flora eyeroll "If she has eyes, it'll be a low score."
      mc "Well, she does have an appreciation for beauty in unexpected places.{space=-20}"
      mc "Though I wouldn't expect you to understand."
      flora annoyed "Whatever. Good luck fitting in at the gallery."
      mc "Thank you!"
      flora cringe "Ugh!"
      window hide
      show flora cringe at disappear_to_left(.75)
      pause 0.75
      play sound "<from 9.2 to 10.2>door_breaking_in"
      show location with hpunch2
      $flora["romance_disabled"] = True
      $game.notify_modal(None,"Love or Lust","Romance with [flora]:\nDisabled.",wait=5.0)
      pause 0.25
      window auto
      "I think I really pissed [flora] off with this one..."
      "She's abnormally possessive over [jacklyn]."
      "It is kind of hot imagining the two of them together, though."
      "...especially with me there."
      "I don't think I imagined it when [flora] turned all red just at the mention of [jacklyn]'s name."
      "And it definitely wasn't just from anger, either."
      "Never say never, right?"
      "..."
      "But one thing at a time."
      "I have my suit ready. Now, I just have to wait for the green light from [jacklyn] on the day and time."
      "It feels good to be feeling good for once."
      "To grab life by the horns and revel in art and sex and rock 'n' roll."
      window hide
      $quest.jacklyn_romance.finish("done_but_sexless")
      return
  flora blush "Anyway, I think I should go now..."
  mc "Actually, I think you should stay."
  flora worried "Oh, um... I don't know, [mc]..."
  if flora["madeamends"]:
    mc "Now's as good a time as any to explore what we really want, right?"
    flora worried "It's just so risky. It makes me nervous."
    mc "Me too... but in a good way."
  flora worried "What if—"
  window hide
  show flora bedroom_hug
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Before she can continue doubting this, doubting herself, doubting us...{space=-55}\nI step in close and pull her to me."
  "Nothing sexual, nothing scandalous. Just a hug."
  "A warm embrace as I fold my arms around her and squeeze her\nto me."
  "At first, there's some tension in her body... but then she relaxes into me."
  "Her cheek pressed against my chest."
  flora bedroom_hug "You actually smell kind of nice..."
  mc "Don't sound too surprised."
  "She giggles softly and melts into me."
  flora bedroom_hug "You feel nice too..."
  mc "Eh, I guess you feel all right."
  window hide
  pause 0.125
  show flora bedroom_hug laughing with Dissolve(.5)
  pause 0.125
  window auto
  flora bedroom_hug laughing "You're a real comedian."
  show flora bedroom_hug blush with dissolve2
  "She pulls away slightly and looks up at me."
  "Her eyes shine with worry, but more than that, they're alight with something tender."
  "Something I rarely get to see, but will always treasure."
  "Especially in these safe, hidden moments."
  "Just the two of us with no pretense."
  "No walls and no lies."
  "Just the soft feel of her in my arms, and that look in her eyes."
  "The truth of what we really feel for one another."
  "Her cheeks are flushed, lips slightly parted as her breathing quickens."
  "I can feel her practically trembling in my arms."
  mc "It's okay. I got you."
  flora bedroom_hug blush "O-okay."
  window hide
  pause 0.125
  show flora bedroom_hug kiss with Dissolve(.5)
  pause 0.125
  window auto
  "And with that, the last of her resolve melts away."
  "I'm kissing her, and she's kissing me back."
  if quest.flora_squid["shower"] not in ("hug","ass"):
    "I can hardly believe it — [flora], kissing me!"
    "Giving into what we're told by society is wrong... but we feel is right.{space=-5}"
    "I feel dizzy and elated all at once."
  "Her lips soft, sweet, and the polar opposite of her usual sharp and bratty self."
  window hide
  pause 0.125
  show flora bedroom_hug concerned with Dissolve(.5)
  pause 0.125
  window auto
  "But just as I start to deepen the kiss, she pulls away."
  mc "What's wrong?"
  if quest.maya_sauce["bedroom_taken_over"]:
    flora bedroom_hug concerned "We should go to your room..."
    "She's right. Out here we're doomed if anyone walks up the stairs."
    mc "What about [maya]?"
    flora bedroom_hug concerned "I'll just text her some excuse."
    mc "Why not do it in your room instead?"
    flora bedroom_hug surprised "You're still banned, remember?"
    # "She acts all coy when she says it, but there's also a hint of seriousness in her voice."
    "She acts all coy when she says it, but there's also a hint of seriousness{space=-65}\nin her voice."
    mc "Err, fine."
    mc "You text her, and I'll try to get the door open in the meantime."
  else:
    flora bedroom_hug concerned "We should lock the door..."
    menu(side="middle"):
      extend ""
      "\"It's fine. I need you right now.\"":
        mc "It's fine. I need you right now."
        mc "I don't care about anything or anyone else."
        flora bedroom_hug surprised "[mc], I—"
        window hide
        pause 0.125
        $flora.lust+=1
        show flora bedroom_hug kiss with Dissolve(.5)
        pause 0.125
        window auto
        "Once again, my hungry mouth swallows her words."
        "With my hands on her hips, I walk her backwards to my bed."
      "\"Good idea. We can't be too careful.\"":
        mc "Good idea. We can't be too careful."
        flora bedroom_hug concerned "R-right."
        play sound "lock_click"
        "In a moment of rare excellence and sex-driven efficiency, I manage to lock the door behind me without ever letting go of her."
        flora bedroom_hug blush "Thank you..."
        mc "Of course. Whatever makes you most comfortable."
        window hide
        pause 0.125
        $flora.love+=1
        show flora bedroom_hug kiss with Dissolve(.5)
        pause 0.125
        window auto
        "She smiles at me, then presses her lips to mine and pulls me toward the bed."
  window hide
  hide screen notification_side
  hide screen interface_hider
  show flora bedroom_sex anticipation
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  if quest.maya_sauce["bedroom_taken_over"]:
    $game.location = "home_bedroom"
  $set_dialog_mode("default_no_bg")
  if quest.maya_sauce["bedroom_taken_over"]:
    "A few moments later, we're in my old room, which now smells like some kind of girly mountain flowers or something."
    $flora.love+=1
    "[flora] smiles at me, then presses her lips to mine again and pulls me toward the bed."
  "We become a tangle of limbs and fevered kisses."
  if quest.maya_sauce["bedroom_taken_over"]:
    "All I know is I need to be as close to her as possible."
  else:
    "All I know is I need to be as close to [flora] as possible."
  "She's rarely so soft, so vulnerable, and I feel privileged to have this moment right now."
  "I want everything off, our hearts laid bare."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "She looks up at me with bright, expectant eyes."
  flora bedroom_sex anticipation "Not too hard, okay...?"
  "Her words come out in a whisper and it melts my heart anew."
  mc "I'll be gentle."
  "From one heartbeat to the next, with our pulses beating in time and our breath mingling..."
  show flora bedroom_sex insertion with dissolve2
  "...I slowly push my way into her."
  "She gives a tiny gasp and her head instantly rocks back against\nthe pillow."
  show flora bedroom_sex penetration1 with dissolve2
  flora bedroom_sex penetration1 "Ooooh..."
  "Fuck. She fits me like a glove. So tight and slick."
  "I'm not even halfway in yet and already her pussy is constricting around me."
  mc "Are you okay?"
  flora bedroom_sex penetration1 "Y-yes... keep going..."
  show flora bedroom_sex penetration2 with dissolve2
  "Glad that none of this seems to be too much for her, I happily do as she says."
  "With my hands braced on the mattress on either side of her, I work my way deeper inside."
  show flora bedroom_sex wrapped_arms penetration3 with dissolve2
  "She lets go of her white-knuckled hold of the sheets and wraps her arms around me, nails digging into the back of my neck."
  mc "God, [flora]... you feel so good..."
  show flora bedroom_sex wrapped_arms penetration4 with dissolve2
  flora bedroom_sex wrapped_arms penetration4 "Mmm, yes..."
  mc "I'm going to start moving now, okay?"
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.3
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.3
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.3
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms penetration4 "Oh, fuck!"
  mc "You like that?"
  flora bedroom_sex wrapped_arms penetration4 "Mmm, yes! P-please, don't stop!"
  mc "I got you, remember?"
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.175
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.175
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.2)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.15)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms penetration4 "Mhmmmm..."
  "She lets out a little whine and bites her bottom lip and it undoes\nme completely."
  "Having [flora] here, beneath me. Taking her like this."
  "Wholesome, tender... yet a secret we have to keep."
  "How can anything this good possibly be wrong?"
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.1
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms penetration4 "Goodness, [mc]..."
  "She goes rigid beneath me and calls out desperately."
  "It's like her words unleash something within me, and I thrust my hips harder until I'm bottoming out against her cervix."
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.075
  show flora bedroom_sex wrapped_arms penetration3 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration1 with Dissolve(.175)
  show flora bedroom_sex wrapped_arms penetration2 with Dissolve(.125)
  show flora bedroom_sex wrapped_arms penetration4 with Dissolve(.125)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.5)
  pause 0.125
  window auto
  mc "F-fuck, [flora]!"
  "When she wraps her ankles around my hips and uses her legs to press my ass harder against her, I unleash a groan."
  "We both want the same thing, her feeling every last inch of me."
  "Each thrust with my whole weight behind it."
  "Hammering home that there is no going back from this."
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.05
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.125
  window auto
  "Our bodies slick with sweat. The sweet smell of her pussy juices coating my sheets."
  "And her taking my dick, her flower spread open for me."
  "Her entire being needing me, holding me tight as she begs me\nfor more."
  window hide
  pause 0.125
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.0
  show flora bedroom_sex wrapped_arms wrapped_legs penetration3 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration1 with Dissolve(.15)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration2 with Dissolve(.1)
  show flora bedroom_sex wrapped_arms wrapped_legs penetration4 with Dissolve(.1)
  pause 0.125
  window auto
  flora bedroom_sex wrapped_arms wrapped_legs penetration4 "Ahhh! [mc]! I'm about to—"
  mc "Cum with me, [flora]!"
  flora bedroom_sex wrapped_arms wrapped_legs penetration4 "God! Oh, god! P-please!"
  "The sound of her pleading for my cum, my semen, my hard cock, finally unravels me."
  menu(side="middle"):
    extend ""
    "Cum inside":
      mc "I-I'm cumming too!"
      window hide
      pause 0.125
      show flora bedroom_sex cum with hpunch
      pause 0.125
      window auto
      flora bedroom_sex cum "Ohhhhh!"
      "With one final pump I unleash my hot, sticky load deep within her."
      "She arches against me, her feet still digging into my ass."
      window hide
      pause 0.125
      show flora bedroom_sex aftermath with Dissolve(.5)
      pause 0.125
      window auto
      "There really is no going back from this."
      "It's just the two of us on this road now."
      "And whether it's to hell or heaven... I don't care."
      "All that matters is [flora] senseless with lust beneath me."
      "Her soft little mewls still escaping her parted lips."
      window hide
      show flora bedroom_aftermath tired
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "With a shudder of pleasure, my knees go weak, and I collapse against her."
      "Together, we catch our breath. Still coming down from what we've just done."
      "From what {i}I've{/} just done."
      flora bedroom_aftermath tired "Jesus, I'm a mess..."
      flora bedroom_aftermath tired "I can't believe you came inside!"
      mc "Heh! It will be okay..."
      "...and even if it isn't, maybe we could make it work somehow?"
      flora bedroom_aftermath tired "I thought we were going to be careful."
      mc "Well, they have... stuff, just in case."
      mc "I was just so caught up in the moment..."
      mc "And with the way you were squeezing me, I couldn't pull out even if I wanted to."
      $flora.lust+=5
      flora bedroom_aftermath blush "Oh, my god!"
      flora bedroom_aftermath blush "I, um... I should go get cleaned up..."
      mc "Right..."
      flora bedroom_aftermath blush "And remember, not a word of this to anyone."
      mc "My lips are sealed."
      flora bedroom_aftermath blush "They better be..."
      window hide
      pause 0.125
      show flora bedroom_aftermath with Dissolve(.5)
      pause 0.125
      window auto
      "And just like that, she's gone."
      "But I can still feel her in my arms."
      "I just hope she doesn't regret what we've done..."
      "I know I don't."
      "..."
      "Man, I'm pretty tired after that workout..."
      "Maybe I could just sleep for a bit and skip my first classes?"
      $quest.jacklyn_romance["flora_creampie"] = True
      # $flora["creampied"]+=1
    "Pull out":
      "There's no going back from this path we're on now... but we can still be careful about it."
      "Our little secret."
      "A cool oasis for just the two of us to drink each other in."
      window hide
      show flora bedroom_aftermath blush dick
      show black onlayer screens zorder 100
      with Dissolve(.5)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      "Not a second too soon, I pull my dick out of her sopping pussy."
      "It squelches as it reluctantly releases me."
      window hide
      pause 0.125
      show flora bedroom_aftermath blush cumming with vpunch
      show flora bedroom_aftermath blush dick cum with Dissolve(.5)
      pause 0.125
      window auto
      "Load after load coat her heaving stomach."
      "Her eyes flutter with exhaustion, a faint smile on her lips."
      flora bedroom_aftermath blush dick cum "Oh, my god..."
      window hide
      pause 0.125
      show flora bedroom_aftermath tired cum with Dissolve(.5)
      pause 0.125
      window auto
      "In the aftermath, we just lie there together, soaking it all in."
      "Catching our breath, our spent limbs sprawled across the mattress."
      mc "That was..."
      flora bedroom_aftermath tired cum "Yeah..."
      "She gives a little giggle."
      mc "Fuck, you're adorable when you want to be."
      flora bedroom_aftermath blush cum "When I'm not being a brat, you mean?"
      mc "You said it, not me."
      flora bedroom_aftermath blush cum "Mm-hmm."
      $flora.love+=5
      flora bedroom_aftermath blush cum "Thanks for, err... pulling out."
      mc "We can't be too careful, right?"
      flora bedroom_aftermath blush cum "Right."
      window hide
      pause 0.125
      show flora bedroom_aftermath asleep cum with Dissolve(.5)
      pause 0.125
      window auto
      "And then, in an even bigger shock to my system, [flora] snuggles up to me and shuts her eyes."
      flora bedroom_aftermath asleep cum "I'm exhausted..."
      flora bedroom_aftermath asleep cum "Maybe we could just sleep for a bit and skip our first classes?"
      "I swallow past the giddy lump in my throat."
      if quest.maya_sauce["bedroom_taken_over"]:
        "Pleased that she wants to stay like this in [maya]'s bed with me for a little while longer."
      else:
        "Pleased that she wants to stay like this in my bed with me for a little while longer."
      "Pleased I have had this effect."
      "Plus, I too am pretty tired after that workout."
      mc "Sleep well, [flora]."
      flora bedroom_aftermath asleep cum "You too, [mc]..."
  $unlock_replay("flora_secret")
  $quest.jacklyn_romance["flora_sex"] = True
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  pause 1.0
  $game.advance(hours=2)
  hide flora
  hide black onlayer screens
  with Dissolve(.5)
  if "quest_jacklyn_romance_suit_done" in game.events_queue:
    $game.events_queue.remove("quest_jacklyn_romance_suit_done")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  if quest.jacklyn_romance["flora_creampie"]:
    "Goddamn, I don't think I've slept that well in a while."
    "[flora] completely drained me."
    "And while worries linger that she'll regret it..."
    "...or worse, shut me out..."
    "...I somehow don't think it will happen."
    "Because there's no faking what we just shared."
    "All I can do is hope there's more."
    if quest.jacklyn_romance["i_wont_go"]:
      "Even if it means bailing on [jacklyn] and ruining whatever might have been brewing there."
      "I just want to keep living my new chance at life to the fullest."
    else:
      "And keep living my new chance at life to the fullest."
  else:
    "I roll awake to an empty bed... but the spot beside me is still warm."
    "I never even felt [flora] stir. I was too content and cozy."
    "But if I know her, she's probably off to school already."
    "And while I wish she were still beside me instead, it's probably best she isn't."
    "We have to stay careful."
    "But it doesn't bother me, because I have no regrets."
    "And I don't think [flora] does either."
    if quest.jacklyn_romance["i_wont_go"]:
      "Only problem is I will now have to tell [jacklyn] I can't go with her to the gallery exhibit."
      "Hopefully, she doesn't mind..."
  window hide
  $quest.jacklyn_romance.finish()
  if quest.maya_sauce["bedroom_taken_over"]:
    pause 0.25
    window auto
    "..."
    "Man, I really need to win back my room at some point."
    "I really miss sleeping and waking up with some privacy."
    "Oh, well. Time to go before [maya] gets back."
    window hide
    scene black with Dissolve(.07)
    $game.location = "home_hall"
    $renpy.pause(0.07)
    $renpy.get_registered_image("location").reset_scene()
    scene location with Dissolve(.5)
  return
