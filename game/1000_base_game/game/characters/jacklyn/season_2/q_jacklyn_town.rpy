image jacklyn_romance_kiss = LiveComposite((128,128), (6,6), AlphaMask(LiveComposite((116,116), (-213,-15), Transform("flora bedroom_hug kiss", size=(480,270))), "ui circle_mask"))


label quest_jacklyn_town_start:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5

label quest_jacklyn_town_start_quick_start:
  $mc.add_message_to_history("jacklyn", jacklyn, "Buzz, buzz! It's me.")
  play sound "<from 0.25 to 1.0>phone_vibrate"
  pause 0.5
  $set_dialog_mode("phone_message_plus_textbox","jacklyn")
  pause 1.0
  "Oh, shit! It's finally happening!"
  "I'm about to be called up to the big leagues!"
  "Professional art, adult conversation, and a night out on the town..."
  "Maybe even alcohol?"
  "I just need to play it cool."
  if quest.jacklyn_romance["i_wont_go"]:
    "..."
    "Except I totally promised [flora] I wouldn't go, didn't I?"
    "Crap. How to answer this?"
  window hide
  $set_dialog_mode("phone_message","jacklyn")
  menu(side="middle"):
    "\"Mario?\"":
      $set_dialog_mode("phone_message_centered","jacklyn")
      mc "Mario?"
      jacklyn "Not quite the baritone... but if that's what waxes your melon, I don't judge."
      window auto
      $set_dialog_mode("phone_message_plus_textbox","jacklyn")
      "Crap. She's really not much of a gamer, is she?"
      "Or maybe she just didn't think it was funny? I do get that a lot."
      "Or maybe she really thinks I'm into dudes..."
      "...which I also get."
      window hide
      $set_dialog_mode("phone_message_centered","jacklyn")
      mc "Sorry! It was just a lame attempt at a joke..."
      mc "No melon waxing going on here."
      jacklyn "Don't be a drag, be a queen!"
      mc "Err, maybe..."
      jacklyn "We'll have to fix that."
      mc "Yeah?"
      mc "I have a feeling I'm in the right hands."
      $jacklyn.lust+=1
      jacklyn "These hands mold more than just clay, baby bird!"
      mc "I knew it!"
    "?mc.love>=10@[mc.love]/10|{image=stats love}|\"Hey, you!\"":
      $set_dialog_mode("phone_message_centered","jacklyn")
      mc "Hey, you!"
      jacklyn "I know it's been a hot sec since I told you about that art exhibit."
      jacklyn "But I have the deets if you're still down to ride the purple wave?"
      mc "I'm definitely still down!"
      mc "I've really been looking forward to expanding my art horizons."
      mc "Dipping more than just a toe into the waters, you know?"
      $mc.intellect+=1
      mc "And I couldn't ask for a better Virgil to lead me on the path."
      $jacklyn.love+=1
      jacklyn "Wicked! Your enthusiasm is smoke showing my purgatory as we speak!"
    # "{image=jacklyn_romance_kiss}|\"Bad news, [jacklyn].\"" if quest.jacklyn_romance["i_wont_go"]:
    "{image=jacklyn_romance_kiss}|\"Bad news,\n[jacklyn].\"" if quest.jacklyn_romance["i_wont_go"]:
      $set_dialog_mode("phone_message_centered","jacklyn")
      mc "Bad news, [jacklyn]."
      jacklyn "What's shivering, timber?"
      mc "It turns out I actually can't go to the gallery with you..."
      mc "Something else came up."
      jacklyn "Totally unwicked!"
      jacklyn "You can't shimmy the sched?"
      mc "I'm afraid not, but thank you for the invite, anyway!"
      jacklyn "That's all right, baller. No tears in my morning juice."
      jacklyn "Talk lates!"
      window auto
      $set_dialog_mode("phone_message_plus_textbox","jacklyn")
      "Well, I guess that wasn't so bad."
      # "I couldn't exactly tell her I'm bailing on her because of [flora], but [flora] will be happy, at least."
      "I couldn't exactly tell her I'm bailing on her because of [flora], but [flora]{space=-40}\nwill be happy, at least."
      window hide
      $set_dialog_mode("")
      pause 0.75
      $quest.jacklyn_town.fail()
      return
  if persistent.time_format == "24h":
    jacklyn "So, the exhibit is tonight at 20:00."
  else:
    jacklyn "So, the exhibit is tonight at 8:00 PM."
  jacklyn "Meet me outside the pancake brothel on the marina?"
  mc "Oh? Interesting venue for an art exhibit..."
  jacklyn "It's a baller place! Just wait!"
  mc "I'm sure it's awesome. I'll see you tonight!"
  jacklyn "Aces! See you then, tiny painter."
  window auto
  $set_dialog_mode("phone_message_plus_textbox","jacklyn")
  # "Cool! I was admittedly slightly worried that these plans weren't going to happen..."
  "Cool! I was admittedly slightly worried that these plans weren't going{space=-25}\nto happen..."
  "But tonight, huh?"
  "I'm a little nervous... and a lot excited."
  "[jacklyn]'s confidence is pretty infectious, which definitely helps."
  window hide
  $set_dialog_mode("")
  pause 0.75
  if quest.jacklyn_romance["i_wont_go"]:
    window auto
    "Surely, it won't be that big of a deal to [flora], right?"
    "It's just some fun and art, after all."
    "Perfectly harmless... especially if she never finds out."
    window hide
  $quest.jacklyn_town.start()
  return

label quest_jacklyn_town_wait:
  if "quest_lindsey_voluntary_volunteer_on_advance" in game.events_queue:
    $game.events_queue.remove("quest_lindsey_voluntary_volunteer_on_advance")
  if "quest_mrsl_bot_wait" in game.events_queue:
    $quest.mrsl_bot["jacklyn_romance_on_hold"] = True
    return
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "It's almost gallery time with [jacklyn]. I better start getting ready."
  "I don't want to be late, even if tonight I can pull off the fashionably part."
  if game.location == "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom":
    "Speaking of, where did I put my suit?"
    $mc["focus"] = "jacklyn_town"
  else:
    if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
      "I just have to put [maxine]'s priced paper back in the stack first..."
      "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    if mc.owned_item("paper") and not (quest.mrsl_bot == "message_box" and quest.mrsl_bot["some_paper_and_pen"]):
      $mc.remove_item("paper")
      pause 0.25
    show black onlayer screens zorder 100 with Dissolve(.5)
    $mc["focus"] = "jacklyn_town"
    if game.location.id.startswith("home_"):
      $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
      pause 1.0
    else:
      $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
      pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "home_theme" fadein 0.5
    window auto
  $quest.jacklyn_town.advance("suit")
  return

label quest_jacklyn_town_suit_door:
  "I'm running out of time. I better get dressed."
  return

label quest_jacklyn_town_suit_stairs:
  "I'm running out of time. I better get dressed."
  return

label quest_jacklyn_town_suit_bathroom:
  "I'm running out of time. I better get dressed."
  return

label quest_jacklyn_town_suit:
  "It feels good not to scramble or panic about what to wear."
  "It really helps with the confidence."
  "[maya]'s suit sure was the right move."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $quest.jacklyn_town.advance("hairdo")
  pause 1.0
  hide black onlayer screens with Dissolve(.5)
  window auto
  "There! Feeling like a million bucks!"
  "A nice change from the usual $3.50."
  "Now, I just need to finish getting ready."
  "It's a big night, and I don't want to look like a homeless person."
  "Or smell like one..."
  return

label quest_jacklyn_town_hairdo_stairs:
  "I'm running out of time. I better brush my teeth next."
  return

label quest_jacklyn_town_hairdo_door:
  "I should at least brush my teeth before I leave."
  "What if [jacklyn] comes in close?"
  return

label quest_jacklyn_town_hairdo:
  "First things first, I need to brush my teeth."
  "..."
  "..."
  "There we go! Shiny teeth, shiny teeth, gonna make them sparkle!"
  "Hmm... what else?"
  "There's not really much I can do about my face."
  "But let's see what we're working with here..."
  menu(side="middle"):
    extend ""
    "Leave hair alone":
      "I better not fix what isn't broken, right?"
      "There's no need to get overly fancy."
      # "I don't think [jacklyn] is the type to really notice or make a fuss, anyway."
      "I don't think [jacklyn] is the type to really notice or make a fuss, anyway.{space=-60}"
      "But she will definitely appreciate me staying real."
    "Gel hair back":
      "The man needs to match the suit."
      # "And if I want to be invited to these things again in the future, I better make the effort."
      "And if I want to be invited to these things again in the future, I better{space=-15}\nmake the effort."
      "There could be some highbrow artists there tonight."
      "I want to look like I belong."
      "..."
      "..."
      "Aha! How's that for fancy?"
      $quest.jacklyn_town["fancy_hairdo"] = True
  $quest.jacklyn_town.advance("ready")
  return

label quest_jacklyn_town_ready_stairs:
  "Do I really need anything else from upstairs?"
  "I'm already wearing my lucky underwear."
  return

label quest_jacklyn_town_ready_door:
  "I better say goodbye to [jo] before I leave."
  "I don't want her sending out a search party for me..."
  "...assuming she'd even notice my absence, that is."
  return

label quest_jacklyn_town_ready:
  show jo confident with Dissolve(.5)
  jo confident "Well, look at you!"
  jo confident "You look so handsome all dressed up!"
  if quest.jacklyn_town["fancy_hairdo"]:
    $jo.lust+=1
    jo smile "And your hair! So suave!"
    jo smile "So grown up!"
  mc "Thanks, [jo]."
  jo "What's the occasion?"
  mc "I'm going to an art exhibit with [jacklyn]."
  jo concerned "Oh? Is this for school, then?"
  show jo concerned at move_to(.25)
  menu(side="right"):
    extend ""
    "\"No, it's for a date.\"":
      show jo concerned at move_to(.5)
      mc "No, it's for a date."
      jo embarrassed "A date?! Students aren't allowed to date staff!"
      mc "Since [jacklyn] is a teacher's assistant, she's technically not staff."
      mc "She doesn't grade anyone, either. So, there's no conflict of interest."
      jo thinking "..."
      jo thinking "I see you've given this some thought."
      mc "Don't worry, okay? We're both responsible adults."
      $jo.love-=2
      jo sad "I just... I didn't realize that there was something so serious between the two of you..."
      "She looks concerned."
      "Ugh, maybe I should've kept this a secret?"
      "[jo] should be happy for me, though. [jacklyn] is a total babe."
      mc "Yeah, well. It's happening."
      jo worried "I see. Good for you, sweetheart."
      jo worried "As long as you're happy."
      mc "Thank you, [jo]. I really am."
    "\"It's for my... creative soul?\"":
      show jo concerned at move_to(.5)
      mc "It's for my... creative soul?"
      jo thinking "That sounds a lot like [jacklyn] talking."
      mc "Her passion feeds my passion, you know?"
      $jo.lust-=1
      jo sad "I don't know about this, [mc]..."
      jo sad "I still think you should be careful with a girl like her and that sort of influence."
      # jo thinking "But... I suppose it's nice she's got you interested in something outside of video games."
      jo thinking "But... I suppose it's nice she's got you interested in something outside{space=-45}\nof video games."
      jo worried "Still, just be careful, will you?"
      mc "Don't sweat it, [jo]. We're chill as snowmen."
    "\"Err, kind of! I figured this sort of thing would look good for college...\"":
      show jo concerned at move_to(.5)
      # mc "Err, kind of! I figured this sort of thing would look good for college..."
      mc "Err, kind of! I figured this sort of thing would look good for college...{space=-15}"
      # mc "Broadening my horizons... knowing more about the creative greats..."
      mc "Broadening my horizons... knowing more about the creative greats...{space=-5}"
      jo excited "College! That's wonderful to hear, [mc]!"
      jo excited "Your future is very important!"
      "Don't I know it..."
      mc "It is, indeed."
      jo laughing "Gosh, am I glad you're getting out and taking your future seriously!"
      # jo laughing "I wasn't sure about [jacklyn], but it seems that she's a positive influence on you."
      jo laughing "I wasn't sure about [jacklyn], but it seems that she's a positive influence{space=-65}\non you."
      mc "She really is. It's been nice to learn a thing or two from her."
      mc "I'm very much looking forward to tonight."
      $jo.love+=1
      jo excited "I'm so happy for you, honey!"
  window hide
  show flora cringe flip at appear_from_left(.25)
  if renpy.showing("jo worried"):
    show jo worried:
      ease 1.0 xalign 0.75
  elif renpy.showing("jo excited"):
    show jo excited:
      ease 1.0 xalign 0.75
  pause 0.5
  window auto
  flora cringe flip "Are you really going to let him go out with [jacklyn]?"
  if quest.jacklyn_romance["i_wont_go"]:
    "Crap. This isn't how I wanted [flora] to find out."
    "She looks pissed, but she can't come out and say it in front of [jo]."
    "My only saving grace... though I'll most certainly pay the price later."
    "I guess I'll just have to beg her for forgiveness like never before."
  if renpy.showing("jo worried"):
    jo thinking "I suppose it is nice that he's getting out."
    jo worried "Just be back by curfew, okay, young man?"
  elif renpy.showing("jo excited"):
    jo excited "I think it's nice that he's getting out!"
    jo laughing "Just be back by curfew, okay, young man?"
  mc "Of course, ma'am!"
  if quest.jacklyn_town["fancy_hairdo"]:
    flora annoyed flip "And what did you do to your hair?"
    mc "Do you like it?"
    flora annoyed flip "You look like a butler."
    flora sarcastic flip "Emphasis on the butt."
    mc "Rude. [jo] likes it."
  mc "Actually, [jo], can you help me tie my tie?"
  show flora annoyed flip
  if renpy.showing("jo worried"):
    jo neutral "Oh? Of course, sweetheart."
  elif renpy.showing("jo laughing"):
    jo smile "Absolutely, sweetheart!"
  window hide
  show jo smile_tie with Dissolve(.5)
  window auto
  "She moves in close to me and takes the tie in her hands."
  "A smile on her lips, cheeks slightly flushed."
  jo smile_tie "Goodness, you've really grown up so much, haven't you?"
  jo smile_tie "I'm so proud of the man you're becoming."
  mc "Thank you, [jo]!"
  mc "I'm glad at least someone here can appreciate it."
  jo skeptical_tie "But remember to be back by curfew, okay?"
  jo skeptical_tie "You're still living under my roof. So, you still follow my rules."
  "...and just like that, I feel like a boy again."
  "She's always been great at cutting my legs out from under me."
  mc "You got it, [jo]."
  flora eyeroll flip "Right. You better be back for your bedtime story."
  mc "'Cause I'm the one acting childish right now, eh?"
  if quest.jacklyn_romance["i_wont_go"]:
    flora cringe flip "You said you wouldn't go!"
    mc "I'm just going for the art!"
  jo concerned_tie "That's enough, you two!"
  jo smile_tie "Be nicer to [mc], [flora]. He deserves both our support."
  flora annoyed flip "I think it's a mistake to let him go."
  # "[flora] is really letting her displeasure be known with this one, isn't she?"
  "[flora] is really letting her displeasure be known with this one, isn't she?{space=-60}"
  "But why should I hold myself back just to make her happy?"
  "My happiness matters, too."
  "And I'm tired of pretending that it doesn't."
  show flora annoyed flip at move_to("left")
  show jo smile_tie at move_to("right")
  menu(side="middle"):
    extend ""
    "\"I'm sorry you weren't invited too, [flora].\"":
      show flora annoyed flip at move_to(.25)
      show jo smile_tie at move_to(.75)
      mc "I'm sorry you weren't invited too, [flora]."
      jo smile_tie "That's so sweet of you, [mc]."
      jo smile_tie "See, [flora]? That's how you support one another!"
      flora cringe flip "I didn't want to be invited!"
      flora cringe flip "I couldn't care less about this stupid art gallery exhibit!"
      # "Anger... denial... maybe by the end of the night she'll reach acceptance?"
      "Anger... denial... maybe by the end of the night she'll reach acceptance?{space=-75}"
      "One can only hope."
      show flora annoyed flip with dissolve2
    "\"Yeah, stop being a brat, [flora].\"":
      show flora annoyed flip at move_to(.25)
      show jo smile_tie at move_to(.75)
      mc "Yeah, stop being a brat, [flora]."
      flora cringe flip "See, [jo]?! He's being rude and rubbing it in!"
      jo skeptical_tie "That's enough, you two. Play nice."
      mc "Sorry, [jo]..."
      $flora.love-=1
      flora annoyed flip "Hmph."
  jo smile_tie "Now, let me just tie the final knot..."
  window hide
  show jo blush with Dissolve(.5)
  window auto
  jo blush "There!"
  mc "How do I look?"
  jo flirty_hands_to_the_side "You look fantastic, honey."
  jo flirty_hands_to_the_side "Remember to have fun and stay out of trouble, okay?"
  jo flirty_hands_to_the_side "Make responsible decisions and don't—"
  mc "[jo]!"
  jo sarcastic "Sorry, sorry! You're right."
  jo sarcastic "I hope you have a wonderful time, sweetheart."
  mc "Thanks, [jo]."
  flora sarcastic flip "Yeah, a real sunny time."
  jo blush "That's more like it!"
  "I guess [jo] isn't fluent in sarcasm..."
  "...or chooses to overlook when it comes from her favorite."
  "But that's fine. I'm not about to let this ruin my evening."
  mc "Thanks, [flora]!"
  flora eyeroll flip "Whatever."
  window hide
  show flora eyeroll flip at disappear_to_left
  show jo blush:
    ease 1.0 xalign 0.5
  pause 0.5
  window auto
  mc "I'm off, then. Don't wait up!"
  jo eyeroll "Remember your curfew, young man!"
  mc "Right, right!"
  window hide
  hide jo with Dissolve(.5)
  $quest.jacklyn_town.advance("marina")
  return

label quest_jacklyn_town_marina:
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.advance()
  $game.location = "marina"
  $renpy.pause(1.0)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  $jacklyn["outfit_stamp"] = jacklyn.outfit
  $jacklyn.outfit = {"fishnet":"jacklyn_thigh_high_fishnet", "shirt":"jacklyn_dress", "choker":"jacklyn_chained_choker"}
  show jacklyn smile at appear_from_left
  pause 0.5
  jacklyn smile "Looking aces, [mc]!"
  "Holy shit. She looks incredible."
  "Edgy, yet classy."
  "Like she could wilt a flower with a cold, hard stare."
  "And still incredibly [jacklyn]."
  mc "H-hey, [jacklyn]..."
  jacklyn smile "I didn't keep you shimmying the seconds too long, did I?"
  mc "Err, no! I just got here too, really."
  jacklyn excited "Wicked."
  if quest.jacklyn_town["fancy_hairdo"]:
    jacklyn excited "Did you get all dolled up just for me?"
    mc "I wanted to look good standing next to you..."
    mc "Do you like it?"
    jacklyn laughing "It's dandelion fuzz."
    $jacklyn.lust-=1
    # jacklyn laughing "Though I think you should do whatever makes you feel most comfortable."
    jacklyn laughing "Though I think you should do whatever makes you feel most comfortable.{space=-120}"
    "Oof. She doesn't like it."
    mc "Err, it was no big deal!"
    mc "Shall we?"
    jacklyn excited "Yes, we shall! I can't wait for you to see this place."
  else:
    jacklyn excited "Look at you, all suited up."
    jacklyn excited "You clean up well, don't you? Fresh as a daisy!"
    mc "Heh, well. I knew you'd look stunning tonight."
    mc "I had to keep up, you know?"
    $jacklyn.lust+=1
    jacklyn laughing "You're just whipping my cream now!"
    "She's doing something to my cream, too..."
    jacklyn excited "Anyhow, wait until you see this place."
  # jacklyn excited "Pancakes and rivers of syrup by day. Music, booze, and art by night."
  jacklyn excited "Pancakes and rivers of syrup by day. Music, booze, and art by night.{space=-5}"
  jacklyn excited "It's a baller time."
  mc "That sounds amazing."
  jacklyn laughing "Come with me!"
  window hide
  show jacklyn laughing at disappear_to_left
  pause 0.5
  hide screen interface_hider
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $game.location = "pancake_brothel"
  pause 0.5
  show jacklyn laughing at appear_from_right
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  "Oh, wow! [jacklyn] wasn't kidding."
  "This place really is something else..."
  # "All my old life, I never dared to go here, but the atmosphere is infectious."
  "All my old life, I never dared to go here, but the atmosphere is infectious.{space=-100}"
  jacklyn laughing "So, how does the gavel tumble?"
  mc "Hard and heavy! This place is awesome."
  jacklyn laughing "Yeah? Does it pass the [mc] vibe check?"
  mc "I'd say so, absolutely."
  mc "Do you go to exhibits often?"
  jacklyn excited "Skinny dipping in the artistic waters is one of my favorite hobbies."
  jacklyn excited "You'll find some platinum inspo here."
  "I'm still not sure how into the actual art scene I am..."
  "...but I am definitely getting more and more into [jacklyn] and her spirit."
  "Did some paint smeared across a canvas really cause this upbeat feeling?"
  mc "I bet. Should we take a look around?"
  jacklyn neutral "Does the pussy self lubricate?"
  mc "..."
  mc "Yes?"
  jacklyn smile "Wherever you want to start, [mc]."
  jacklyn smile "The night is a fresh faced hooker, and the art is slaying."
  jacklyn smile "Where do your eyes and heart lead you?"
  "Hmm... This is almost overwhelming..."
  "It's hard not to feel out of my depth amongst all these artists."
  "And next to [jacklyn], too."
  # "But the atmosphere is so free-spirited and alive, and somehow welcoming."
  "But the atmosphere is so free-spirited and alive, and somehow welcoming.{space=-130}"
  mc "Let's see here..."
  window hide
  hide jacklyn with Dissolve(.5)
  $quest.jacklyn_town.advance("gallery")
  $quest.jacklyn_town["paintings"] = set()
  return

label quest_jacklyn_town_gallery:
  pause 0.125
  show jacklyn laughing with Dissolve(.5)
  jacklyn laughing "Is this joint totally blowing you or what?"
  "Knowing [jacklyn], she doesn't mean blowing my mind."
  "I do feel somewhat aroused after such freedom of expression..."
  mc "It's blowing me hard!"
  jacklyn excited "That's so sex—"
  jacklyn excited_right "Hey! I just saw the gallery host!"
  mc "Oh?"
  jacklyn smile_right "She's a wicked maverick. I'm going to go say what's up."
  jacklyn smile "But you keep fapping your art boner and have a look around, okay?"
  jacklyn smile "I'll be back faster than Arnie."
  mc "Err, okay. No problem."
  window hide
  show jacklyn smile at disappear_to_right
  pause 0.5
  window auto
  "Very well. Let's see what else this place has got going on..."
  $quest.jacklyn_town.advance("pancake_brothel")
  $quest.jacklyn_town["interactables"] = set()
  if not (flora.love >= 20 or flora.lust >= 15):
    $quest.jacklyn_town["interactables"].add("mystery_woman")
  return

label quest_jacklyn_town_pancake_brothel:
  show flora
  pause 0.125
  show jacklyn smile at appear_from_right
  jacklyn smile "There you are! Sorry to sauce your brownie, and then split."
  jacklyn smile "But the host is an old friend and I had to fluff her piece."
  jacklyn neutral "Not in the dabbling sense, of course."
  "Questionable."
  mc "Err, no worries."
  jacklyn neutral "Did you at least get in some extra exploring, sailor?"
  mc "You bet. I came across a thing or two that really surprised me."
  jacklyn smile "Magnum!"
  # mc "I've definitely enjoyed seeing all the pieces and learning about the art."
  mc "I've definitely enjoyed seeing all the pieces and learning about the art.{space=-35}"
  mc "I had no idea there were so many unique and vibrant styles."
  jacklyn smile "That's what I'm always saying."
  jacklyn smile "No two sticks glow the same."
  mc "I'm definitely learning that."
  jacklyn excited "Your brain-flower is about to reach June!"
  jacklyn excited "In a way, I envy you. You're like a newly born little art baby covered in the afterbirth of creation."
  mc "Err, right. Something like that."
  # mc "It's getting pretty crowded in here, though... and I think I need a breather."
  mc "It's getting pretty crowded in here, though... and I think I need a breather.{space=-110}"
  # mc "All this art is kind of heady. I'm gonna end up with a culture hangover."
  mc "All this art is kind of heady. I'm gonna end up with a culture hangover.{space=-40}"
  jacklyn laughing "That's no rouge off my cheek. Let's inhale the night?"
  window hide
  show jacklyn laughing at disappear_to_right
  pause 0.5
  hide screen interface_hider
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $game.location = "marina"
  pause 0.5
  show jacklyn laughing at appear_from_left
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  # "The doors close behind us, abruptly cutting off the music and noise."
  "The doors close behind us, abruptly cutting off the music and noise.{space=-15}"
  "The smell of approaching rain hangs heavy in the air."
  "Bloated gray clouds roil across the moon and stars."
  "As much as I enjoyed all the art and interesting people, it feels nice to be outside again."
  "Just the sound of the waves, gently licking the pier."
  "For the first time all night, it's just the two of us."
  "The skin tight dress stretches across [jacklyn]'s chest."
  "The subtle smell of sweat and licorice sticking to her skin."
  "If only I had a paintbrush and a canvas..."
  mc "Whew! Quite the scene in there, huh?"
  jacklyn laughing "A true serial killer convention."
  mc "I have to admit, it has been nice getting out of the house."
  mc "Breaking up the routine, what with everything going on."
  mc "It's all just been a bit much lately."
  jacklyn excited "Art offers the truth of reality, and the reality of truth."
  jacklyn excited "Escape to {i}and{/} from it."
  jacklyn excited "It's disco Jesus."
  mc "Heh. You make it sound a lot cooler."
  mc "Thanks for tonight, [jacklyn]."
  jacklyn smile "Your excitement is flutters, babylegs."
  mc "Oh? Flutters?"
  jacklyn smile "Fuck yeah."
  jacklyn smile "Nothing spreads faster than the inspirational STD."
  jacklyn smile "It's totally a flash in the pants."
  mc "Totally..."
  "It's hard to believe it, but I don't think tonight could have gone better."
  "An interesting conversation, a night out, and [jacklyn] by my side looking smoking hot."
  # "I was obviously the envy of the room, but she stayed focused on me."
  "I was obviously the envy of the room, but she stayed focused\non me."
  "And she genuinely seemed to enjoy showing me the pieces."
  "And the way she's looking at me right now..."
  "How can I not seize the moment?"
  window hide
  show jacklyn marina_kiss kiss:
    xalign 0.0
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  # "Thrumming with the confidence the night has given me, my feet take me closer to [jacklyn]."
  "Thrumming with the confidence the night has given me, my feet take{space=-15}\nme closer to [jacklyn]."
  "My blood rushes hot and heavy and my heart feels ready to burst."
  "My hands find her face, and my mouth finds her lips."
  "Her mouth twists in a little half-smile beneath mine."
  "For a moment, I worry I've overstepped, but then she leans into it."
  "Her lips, warm and soft, explore without reserve."
  "She bites my bottom lip, and a quiet gasp escapes my throat."
  "Her tongue suddenly touches mine, plays with it."
  "It's a hungry, passionate kiss. One that could last forever."
  if (flora.love >= 20 or flora.lust >= 15) and quest.jacklyn_romance["flora_sex"]:
    "But something behind her catches my eye..."
    window hide
    show jacklyn marina_kiss kiss:
      ease 3.0 xalign 1.0
    pause 3.0
    hide jacklyn
    show jacklyn marina_kiss kiss:
      xalign 1.0
    window auto
    "Fuck."
    # "Never once did I think my life, once so lonely and seemingly hopeless, could become so complicated."
    "Never once did I think my life, once so lonely and seemingly hopeless,{space=-40}\ncould become so complicated."
    # "Never did I think that the dream of making out with a girl like [jacklyn] could become real..."
    "Never did I think that the dream of making out with a girl like [jacklyn]{space=-15}\ncould become real..."
    "...or that doing so would risk my relationship with [flora], and send my heart into a panicked stumble."
    "Life is all about choices. Some are harder than others."
    "Especially when you can't see the road ahead."
    "All you can do is make a choice, hoping it's the right one."
    jacklyn marina_kiss kiss "[mc]?"
    window hide
    show jacklyn marina_kiss stare:
      xalign 1.0
      ease 3.0 xalign 0.0
    pause 3.0
    hide jacklyn
    show jacklyn marina_kiss stare:
      xalign 0.0
    window auto
    mc "Huh?"
    jacklyn marina_kiss stare "You prada, spaceman?"
    mc "I, err... I don't know..."
    mc "[flora]—"
    jacklyn marina_kiss stare "It's [jacklyn]."
    mc "No, I mean she's—"
    window hide
    show jacklyn marina_kiss floraless:
      xalign 0.0
      ease 3.0 xalign 1.0
    pause 3.0
    hide jacklyn
    show jacklyn marina_kiss floraless:
      xalign 1.0
    window auto
    "...gone."
    $unlock_replay("jacklyn_kiss")
    window hide
    show jacklyn neutral at center
    show black onlayer screens zorder 100
    with Dissolve(.5)
    pause 0.5
    hide black onlayer screens with Dissolve(.5)
    window auto
    mc "She was here."
    jacklyn thinking "Ah."
    jacklyn thinking "You should really electroshock your heart, [mc]."
    jacklyn thinking "Figure out what you want, and stop yanking horsedicks."
    "When she puts it like that, it sounds horribly unappealing..."
    "...but she is one hundred percent right."
    "I need to choose what and who I want."
    "No more cloudy head or uneven roads."
    if quest.jacklyn_romance["i_wont_go"]:
      "I even promised [flora] I wouldn't come..."
      "And yet here I am, fucking with her head."
      "I didn't mean to. I just got caught up."
      "But now it's time I make my decision."
    show jacklyn thinking at move_to(.25)
    menu(side="right"):
      extend ""
      "Go after [flora]":
        show jacklyn thinking at move_to(.5)
        mc "You're right. I'm sorry, [jacklyn]."
        # mc "I hate to cut such a nice night short, but I should at least check on [flora]."
        mc "I hate to cut such a nice night short, but I should at least check on [flora].{space=-95}"
        mc "She looked pretty upset, and I don't want to be the cause of that..."
        jacklyn smile_hands_down "That's totally honey crisp."
        jacklyn smile_hands_down "Bitch slaps me right in the sweet tooth."
        mc "Err, you don't mind?"
        # jacklyn excited "Hells no! Keep fine graining that sugar, and maybe we'll talk again soon."
        jacklyn excited "Hells no! Keep fine graining that sugar, and maybe we'll talk again soon.{space=-75}"
        jacklyn excited "Flap, flap, little birdie."
        mc "Thanks for being so understanding. Really."
        mc "I'll see you later, okay?"
        jacklyn laughing "The art was kosher and the company was hydra flow."
        "I think that's a good thing? It sounds like a good thing."
        jacklyn laughing "Laters!"
        window hide
        show jacklyn laughing at disappear_to_left
        pause 0.5
        window auto
        "Welp. It was still a nice night."
        "And [jacklyn] didn't seem too upset about it..."
        "I better make sure [flora] gets home and is okay."
        window hide
        show black onlayer screens zorder 100 with Dissolve(.5)
        $quest.jacklyn_town.advance("flora")
        $game.advance()
        $game.location = "home_kitchen"
        pause 2.0
        hide black onlayer screens with Dissolve(.5)
        play music "home_theme" fadein 0.5
        if "event_player_force_go_home_at_night" in game.events_queue:
          $game.events_queue.remove("event_player_force_go_home_at_night")
        if "event_show_time_passed_screen" in game.events_queue:
          $game.events_queue.remove("event_show_time_passed_screen")
        call screen time_passed
        pause 0.5
        return
      "Stay with [jacklyn]":
        show jacklyn thinking at move_to(.5)
        mc "You know what? I think that kiss electroshocked my heart enough."
        # mc "She'll be fine. I want to stay here with you and finish our amazing night."
        mc "She'll be fine. I want to stay here with you and finish our amazing night.{space=-65}"
        jacklyn laughing "A man who takes what he wants and doesn't apologize for it?"
        jacklyn laughing "Total Rockefellow!"
        mc "Heh. What I want right now is to kiss you again."
        jacklyn excited "Well, you better hurry before I turn into a pumpkin, pumpkin!"
  else:
    # "Never did I think that the dream of making out with a girl like [jacklyn] could become real."
    "Never did I think that the dream of making out with a girl like [jacklyn]{space=-15}\ncould become real."
    $unlock_replay("jacklyn_kiss")
  window hide
  hide screen notification_side
  hide screen interface_hider
  show jacklyn alley_kiss pinned
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.25
  show black onlayer screens zorder 4
  $mc["focus"] = ""
  $set_dialog_mode("default_no_bg")
  if (flora.love >= 20 or flora.lust >= 15) and quest.jacklyn_romance["flora_sex"]:
    jacklyn alley_kiss pinned "Come on!"
  else:
    jacklyn alley_kiss pinned "Come with me!"
  "She pulls me along, and it doesn't matter where..."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  "Fevered with lust, I pull her close and push her back up against the alley wall."
  "Pinning her wrists against the wall."
  "She gives me a wicked grin."
  jacklyn alley_kiss pinned "Don't be afraid to bite, tiger."
  mc "That's funny. I was gonna say the same thing."
  window hide
  pause 0.125
  show jacklyn alley_kiss kiss with Dissolve(.5)
  pause 0.25
  window auto
  "She smiles at that, then leans in and kisses me hard."
  # "She teases my lips with her tongue, the taste of her seeping into my mouth like burnt honey."
  "She teases my lips with her tongue, the taste of her seeping into my{space=-5}\nmouth like burnt honey."
  mc "Ow!" with vpunch
  "Then, without warning, she bites at my lip again."
  mc "Playful tonight, are we?"
  jacklyn alley_kiss kiss "Every night is a merry go round if you spin hard enough."
  "She's definitely got me spinning right now..."
  mc "And fast enough."
  jacklyn alley_kiss kiss "Hard and fast — just the way I like it."
  mc "I have something else you'll like, too."
  "And she has something I want."
  "Her tight waist, her full perfect tits."
  "They look magnificent in her dress... but I want to see all of them."
  window hide
  pause 0.125
  show jacklyn alley_kiss boobs_exposed with Dissolve(.5)
  pause 0.25
  window auto
  "They spill out of her dress, soft and flawless in the moonlight."
  jacklyn alley_kiss boobs_exposed "Eager in the sandbox, I see."
  mc "So eager..."
  "This is definitely the most fun I've had in a while."
  # "And as much as I'd like to keep her pinned and put my hands all over her perfect body, there's something else I want even more."
  "And as much as I'd like to keep her pinned and put my hands all over{space=-20}\nher perfect body, there's something else I want even more."
  "I want to show [jacklyn] just how thankful I am for the night out."
  mc "Did I say thank you yet for the great evening?"
  jacklyn alley_kiss boobs_exposed "You might have mentioned it..."
  mc "Well, I think my mouth hasn't expressed it properly just yet."
  window hide
  pause 0.125
  show jacklyn alley_kiss pussy_exposed with Dissolve(.5)
  pause 0.25
  window auto
  mc "Fuck me... I can smell you already..."
  jacklyn alley_kiss pussy_exposed "Don't get pink-cheeked on me now."
  mc "Oh, don't you worry about that."
  window hide
  show jacklyn alley_lick licking1
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Her wetness sticks to my nose, to my lips."
  "She smells musky with a hint of sweetness."
  "It makes my mouth water."
  window hide
  pause 0.125
  show jacklyn alley_lick licking2 with Dissolve(.125)
  show jacklyn alley_lick licking3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick licking4 with Dissolve(.25)
  show jacklyn alley_lick licking1 with Dissolve(.125)
  pause 0.25
  window auto
  "The taste of her sends me into overdrive."
  jacklyn alley_lick licking1 "Mmm! That is blinking my 182, all right!"
  window hide
  pause 0.125
  show jacklyn alley_lick licking2 with Dissolve(.125)
  show jacklyn alley_lick licking3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick licking4 with Dissolve(.25)
  show jacklyn alley_lick licking1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull1 with Dissolve(.5)
  pause 0.0
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.25
  window auto
  "She grabs hold of my hair and pulls me in deeper."
  "My rockhard cock strains in my pants... but she just tastes so good."
  "Now is not the time for my pleasure. This moment is all about her."
  window hide
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.25
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.25
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.25
  window auto
  "From ass to clit, into her folds, the taste intensifies."
  "It's a journey of exploration and self-insight, just as much as it is one of pleasure."
  window hide
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick hair_pull2 with Dissolve(.125)
  show jacklyn alley_lick hair_pull3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick hair_pull4 with Dissolve(.25)
  show jacklyn alley_lick hair_pull1 with Dissolve(.125)
  pause 0.25
  window auto show
  show jacklyn alley_lick skirt_covered1 with dissolve2
  jacklyn alley_lick skirt_covered1 "Ohhh! Fuck yeah, [mc]!"
  "Her skirt drops, plunging me in steamy darkness."
  "My mouth covers her entire pussy, sucking out the juices within."
  window hide
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.25
  window auto
  "She squeezes my head so tightly between her legs, thrusting her pussy against my mouth."
  jacklyn alley_lick skirt_covered1 "Shiiiit! Now that is the... mmm... gold standard!"
  window hide
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.0625
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.25
  window auto
  "Her hips buck further as she begins to shudder."
  jacklyn alley_lick skirt_covered1 "T-that's it, baby! I'm almost there!"
  window hide
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.25
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick skirt_covered3 with Dissolve(.25)
  pause 0.5
  show jacklyn alley_lick skirt_covered4 with Dissolve(.25)
  show jacklyn alley_lick skirt_covered1 with Dissolve(.125)
  pause 0.125
  show jacklyn alley_lick skirt_covered2 with Dissolve(.125)
  show jacklyn alley_lick orgasm with hpunch
  pause 0.25
  window auto
  jacklyn alley_lick orgasm "Oooooooh!"
  "She suddenly stops writhing and lets out a breathy moan."
  "Then she cums hard, squirting into my mouth."
  "And I drink it... by god I drink it all... like a desperate man who has just found the fountain of juice."
  window hide
  pause 0.125
  show jacklyn alley_lick skirt_covered1 with Dissolve(.5)
  pause 0.25
  window auto
  "Her head falls back against the wall and she groans softly."
  "Her pussy is still pulsating against my lips as she speaks."
  jacklyn alley_lick skirt_covered1 "Now that... was a fucking work of art."
  mc "Heh. Just stroking the canvas, baby."
  $unlock_replay("jacklyn_thank_you")
  window hide
  show jacklyn smile_right at center
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Hearts still beating hard, we hastily put ourselves back together."
  "What a night!"
  "I feel like some kind of backwards, depraved Cinderella..."
  "And I loved every moment of it."
  "This is what living is all about."
  "Not sitting all alone and playing video games, but experiencing the world!"
  "...and eating a smoking hot chick out in a back alley."
  "Art shows are criminally underrated, aren't they?"
  "I better get home, though, before midnight strikes."
  "Or else [jo] will turn into an angry witch..."
  window hide
  if game.quest_guide == "jacklyn_town":
    $game.quest_guide = ""
  $game.notify_modal("quest", "Quest complete", quest.jacklyn_town.title+"{hr}"+quest.jacklyn_town._phases[1000].description, 5.0)
  pause 0.5
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $game.advance()
  pause 2.0
  hide jacklyn
  hide black onlayer screens
  with Dissolve(.5)
  play music "home_theme" fadein 0.5
  $quest.jacklyn_town.finish("done_jacklyn", silent=True)
  return

label quest_jacklyn_town_flora_upon_entering:
  "Quiet sobs come from the bathroom."
  "The good news is that [flora] made it home."
  "The bad news is, well... where do I even start?"
  $quest.jacklyn_town["quiet_sobs"] = True
  return

label quest_jacklyn_town_flora:
  play sound "<from 1.7 to 3>door_knock"
  pause 1.5
  mc "[flora]?"
  "..."
  mc "I know you're in there."
  mc "Can I come in?"
  flora "Go away!"
  if quest.jacklyn_romance["i_wont_go"]:
    flora "I don't ever want to see you again!"
    flora "I... I hate you!"
    mc "No, you don't, okay?"
  mc "If you don't open the door, I'll be forced to break it down."
  flora "..."
  flora "You wouldn't."
  menu(side="middle"):
    extend ""
    "?mc.strength>=10@[mc.strength]/10|{image=stats str}|\"I'm counting to three.\"":
      mc "I'm counting to three."
      mc "One..."
      mc "...two..."
      mc "...three."
      window hide
      scene black with Dissolve(.07)
      $game.location = "home_bathroom"
      $home_bathroom["night"] = False
      $flora.outfit = {"panties":"flora_striped_panties", "bra":"flora_purple_bra", "shirt":"flora_trench_coat"}
      $renpy.pause(0.07)
      $renpy.get_registered_image("location").reset_scene()
      play sound "<from 9.2 to 10.2>door_breaking_in"
      play audio "<from 23.3 to 24.3>door_breaking_in"
      scene location
      show flora afraid
      with hpunch
      window auto
      flora afraid "Eeeep!"
      mc "Yes, I would."
      $flora.love+=2
      flora embarrassed "You could've hurt yourself!"
      mc "It doesn't matter. I had to get to you."
    # "?flora.love>=15@[flora.love]/15|{image=flora contact_icon}|{image=stats love_3}|\"Please, just let me in, will you?\"":
    "?flora.love>=15@[flora.love]/15|{image=flora contact_icon}|{image=stats love_3}|\"Please, just let\nme in, will you?\"":
      mc "Please, just let me in, will you?"
      mc "I'm really worried about you."
      if quest.jacklyn_romance["i_wont_go"]:
        flora "Liar! All you do is lie!"
        flora "How am I ever supposed to trust you again?!"
        mc "You're right. I should have been honest with you."
        mc "You were just so upset and I couldn't bear to be the cause of it."
        mc "I thought I could go purely for the art, and no harm would be done."
        # mc "I now realize that that was the worst decision I could have made. You deserve full honesty."
        mc "I now realize that that was the worst decision I could have made. You{space=-20}\ndeserve full honesty."
        mc "And you deserve all of my attention and effort."
        mc "I promise I won't lie to you ever again."
        flora "..."
        mc "Please, [flora]?"
      else:
        flora "Liar..."
        mc "Come on, [flora]."
      flora "..."
      window hide
      play sound "lock_click"
      pause 0.5
      scene black with Dissolve(.07)
      $game.location = "home_bathroom"
      $home_bathroom["night"] = False
      $flora.outfit = {"panties":"flora_striped_panties", "bra":"flora_purple_bra", "shirt":"flora_trench_coat"}
      $renpy.pause(0.07)
      $renpy.get_registered_image("location").reset_scene()
      scene location
      show flora crying
      with Dissolve(.5)
      window auto
      flora crying "Well, I let you in."
      flora crying "And as you can see, I'm perfectly fine."
      mc "You don't look fine."
      mc "And I can't have your ghost haunting me."
      $flora.lust+=1
      flora crying "That would be punishment for myself, too..."
      mc "Heh. True."
    "\"I guess I'll just have to get [jo], then.\"":
      mc "I guess I'll just have to get [jo], then."
      flora "..."
      "Hopefully, she doesn't call my bluff."
      flora "You wouldn't."
      "Crap."
      mc "Err, you could be in there hurting yourself, for all I know!"
      flora "Well, I'm not!"
      mc "Prove it, then!"
      flora "Jesus!"
      window hide
      play sound "lock_click"
      pause 0.5
      scene black with Dissolve(.07)
      $game.location = "home_bathroom"
      $home_bathroom["night"] = False
      $flora.outfit = {"panties":"flora_striped_panties", "bra":"flora_purple_bra", "shirt":"flora_trench_coat"}
      $renpy.pause(0.07)
      $renpy.get_registered_image("location").reset_scene()
      scene location
      show flora crying
      with Dissolve(.5)
      window auto
      flora crying "Happy now?"
  mc "I saw you on the pier..."
  flora crying "And I saw you... and Jacklyn... and... and..."
  show flora crying at move_to(.75)
  menu(side="left"):
    extend ""
    "?flora.love>=25@[flora.love]/25|{image=flora contact_icon}|{image=stats love_3}|\"Nothing happened, okay?\"":
      show flora crying at move_to(.5)
      mc "Nothing happened, okay?"
      flora crying "You kissed her!"
      mc "I know, but... nothing happened inside me."
      if quest.jacklyn_romance["i_wont_go"]:
        flora crying "So what? It was clearly not just about the art!"
        flora crying "That's twice you have lied to me!"
        mc "I was just caught up in the moment, okay?"
        mc "I know it's no excuse, but..."
      mc "I felt nothing, and it made me realize something."
      flora crying "What?"
      mc "That it's you who I want."
      mc "I know it's wrong, but... I need you."
      "More than you know..."
      flora crying "Really?"
      mc "Really."
      mc "You're worth every risk."
      mc "I want to treat you the way you deserve."
      mc "And be the one you come to, no matter what."
      flora crying "I want that, too..."
      if quest.jacklyn_romance["i_wont_go"]:
        flora crying "...but I don't know if I can give that to you anymore."
        flora crying "You broke my trust."
        mc "I know, and I am more sorry than you know."
        mc "If you'll let me, I'll spend forever trying to make it up to you."
      window hide
      show flora bathroom_hug
      show black onlayer screens zorder 100
      with Dissolve(.5)
      $mc["focus"] = ""
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      window auto
      if quest.jacklyn_romance["i_wont_go"]:
        "[flora] wipes the tears from her cheek as I take a step closer to her."
        "She rebels against me at first, trying to pull away. To escape."
        "Nonetheless, time seems to slow and the world shrinks to nothing but this bathroom."
      else:
        "She wipes the tears from her cheek and takes a step closer to me."
        # "Time seems to slow and the world shrinks to nothing but this bathroom."
        "Time seems to slow and the world shrinks to nothing but this bathroom.{space=-85}"
      "Just [flora] and me."
      "Nothing else matters. No one else matters."
      "Not when there's a chance for us."
      "A chance born out of a miraculous jump back in time."
      "But it's real, and it's here. And nothing can take that away."
      "..."
      "God, I can't believe how badly I messed up in my old life..."
      "Hugging her feels so right, even though it's wrong."
      "She's always been reluctant to show her true feelings, but her walls finally come down as she returns my embrace."
      "She curls into my chest, her arms still wrapped around herself."
      "Squeezing her tight, the scent of her shampoo fills my senses."
      "Her heartbeat against my chest, finally slowing down."
      "It won't be easy... but what good thing ever was?"
      $flora.love+=10
      flora bathroom_hug "Never let me go, okay?"
      $unlock_replay("flora_embrace")
      window hide
      show flora smile
      show black onlayer screens zorder 100
      with Dissolve(3.0)
      pause 0.5
      hide black onlayer screens with Dissolve(.5)
      pause 0.125
      show flora smile at disappear_to_left
      if game.quest_guide == "jacklyn_town":
        $game.quest_guide = ""
      $game.notify_modal("quest", "Quest complete", quest.jacklyn_town.title+"{hr}"+quest.jacklyn_town._phases[1001].description, 1.0)
      $home_bathroom["night"] = True
      $game.notify_modal("quest", "Quest complete", quest.jacklyn_town.title+"{hr}"+quest.jacklyn_town._phases[1001].description, 4.0, False)
      $quest.jacklyn_town.finish("done_flora", silent=True)
    "\"It's complicated.\"":
      show flora crying at move_to(.5)
      mc "It's complicated."
      flora crying "How is it complicated? I saw what you did!"
      if quest.jacklyn_romance["i_wont_go"]:
        flora crying "After promising you wouldn't even go, too! That's so messed up!"
        flora crying "Why would I ever forgive you for that?!"
      # mc "It's complicated because, well... I find you both attractive for different reasons."
      mc "It's complicated because, well... I find you both attractive for different{space=-35}\nreasons."
      mc "And I do want you, [flora]."
      flora crying "No, you don't!"
      mc "Yes, I do! I promise!"
      mc "And you feel something for me as well, don't you?"
      flora crying "I don't—"
      mc "Be honest."
      flora sad "..."
      flora sad "I don't know."
      mc "Well, I do. I {i}know{/} I want you."
      mc "But it's dangerous. We can't let people know."
      mc "We need a cover, so that they won't become suspicious about us."
      mc "And [jacklyn] is the perfect cover."
      flora skeptical "...and you didn't think to bring me in on the plan?"
      mc "Err, that's what I'm doing now!"
      flora angry "{i}After{/} you kissed her!"
      mc "I mean, it helps that I'm attracted to her."
      $flora.love-=5
      flora angry "Ugh!"
      mc "What? It's true."
      mc "We can really sell it that way. So, we need to make this work."
      flora concerned "Make this work?"
      mc "Indeed."
      flora concerned "Oh, I'll make it work, all right."
      flora concerned "Just you watch!"
      window hide
      show flora concerned at disappear_to_left(.75)
      pause 0.75
      play sound "<from 9.2 to 10.2>door_breaking_in"
      $home_bathroom["night"] = True
      show location with hpunch
      pause 0.25
      window auto
      "Err, what just happened?"
      "I thought she would understand..."
      "Surely, she knows how forbidden the two of us together would be?"
      "Why did she get so angry?"
      "..."
      "And why do I feel like it was a challenge?"
      "Well, shit."
      "Complicated is probably an understatement..."
      $mc["focus"] = ""
      window hide
      $quest.jacklyn_town.finish("done_challenge")
  return
