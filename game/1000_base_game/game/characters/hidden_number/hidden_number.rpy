init python:
  class Character_hidden_number(BaseChar):
    notify_level_changed=True

    default_name="Unknown"

    default_stats=[
      ["lust",3,0],
      ["love",3,0],
      ]      

    contact_icon="hidden_number contact_icon"

    def contact_notes(self):
      rv=[]
      rv.append(["Bio","???"])
      rv.append(["Likes","???"])
      rv.append(["Dislikes","???"])
      return rv

    def call_label(self):
      if quest.isabelle_buried == "callforhelp":
        return "quest_isabelle_buried_callforhelp_hidden_number"
      else:
        return None

