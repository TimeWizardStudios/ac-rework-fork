init python:
  class Character_mrsl(BaseChar):

    notify_level_changed = True

    default_name = "Mrs. L"

    default_stats = [
      ["lust",1,0],
      ["love",1,0],
      ]

    default_outfit_slots = ["hairstyle","panties","pants","bra","dress","coat"]

    name_color = "#841210"

    contact_icon = "mrsl contact_icon"

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.add_item("mrsl_ponytail")
      self.add_item("mrsl_panties")
      self.add_item("mrsl_black_panties")
      self.add_item("mrsl_workout_panties")
      self.add_item("mrsl_leggings")
      self.add_item("mrsl_bra")
      self.add_item("mrsl_black_bra")
      self.add_item("mrsl_workout_bra")
      self.add_item("mrsl_dress")
      self.add_item("mrsl_bikini")
      self.add_item("mrsl_red_dress")
      self.add_item("mrsl_coat")
      self.equip("mrsl_panties")
      self.equip("mrsl_dress")

    def ai(self):

      self.location=None
      self.activity=None
      self.alternative_locations={}

      ##############forced locations
      if mrsl["at_none_now"]:
        self.location = None
        self.alternative_locations = {}
        return

      if mrsl["at_none_today"]:
        self.location = None
        self.alternative_locations = {}
        return

      if quest.jo_washed.in_progress:
        self.location = None
        self.activity = None
        return

      if quest.kate_blowjob_dream in ("get_dressed","school","awake","alarm") or quest.mrsl_table in ("morning","hide"):
        self.location = None
        self.activity = None
        return

      if quest.kate_fate in ("peek","hunt","run","hunt2","deadend","rescue"):
        self.location = None
        self.alternative_locations={}
        return

      if quest.kate_desire in ("stargold","bathroomhelp"):
        self.location = "school_first_hall_east"
        self.activity = "standing"
        self.alternative_locations={}
        return

      if quest.isabelle_haggis in ("instructions","puzzle"):
        self.location = None
        self.activity = None
        return

      if quest.poolside_story == "tryout":
        self.location = "school_gym"
        self.activity = "sitting"
        return

      if quest.isabelle_haggis == "escape":
        self.location = "school_first_hall_west"
        self.activity = "standing"
        return

      if mrsl["at_school_locker_room_now"]:
        self.location = None
        self.activity = None
        return

      if quest.maxine_hook == "day":
        self.location = "school_ground_floor"
        self.activity = "standing"
        return

      if quest.flora_squid == "wait" and not quest.flora_squid["smell_concern"]:
        self.location = "school_first_hall_east"
        self.activity = "standing"
        return

      if quest.isabelle_hurricane in ("short_circuit","blowjob","surveillance_footage") and (quest.isabelle_hurricane["distraction"] or quest.isabelle_hurricane["backpack"] or quest.isabelle_hurricane["cocksucker"]) and game.hour in (11,12):
        self.location = "school_first_hall_west"
        self.activity = "standing"
        return

      if quest.fall_in_newfall == "stick_around":
        self.location = "school_homeroom"
        self.activity = "sitting"
        return
      elif quest.fall_in_newfall.in_progress:
        self.location = None
        self.activity = None
        return

      if (((quest.isabelle_dethroning.finished and quest.fall_in_newfall.finished and not (quest.kate_moment.started or quest.kate_moment.failed)) and game.hour in (16,17,18))
      or mrsl["flirting_this_scene"]):
        self.location = "school_ground_floor"
        self.activity = "standing"
        if quest.maya_sauce not in ("classes","dinner"):
          return

      if quest.mrsl_bot == "dream":
        self.location = None
        self.activity = None
        return
      elif quest.mrsl_bot >= "message_box" and quest.mrsl_bot.in_progress and game.hour in (7,8,9,10):
        self.location = "school_gym"
        self.activity = "sitting"
        return

      #########alt locations
      if quest.isabelle_tour.in_progress or quest.day1_take2.in_progress:
        self.alternative_locations["school_homeroom"] = "sitting"

      if quest.isabelle_haggis=="trouble":
        self.alternative_locations["school_homeroom"] = "sitting"

      if quest.maya_sauce in ("classes","dinner"):
        self.alternative_locations["school_homeroom"] = "sitting"
        if (((quest.isabelle_dethroning.finished and quest.fall_in_newfall.finished and not (quest.kate_moment.started or quest.kate_moment.failed)) and game.hour in (16,17,18))
        or mrsl["flirting_this_scene"]):
          return

      #if quest.isabelle_haggis in ("instructions","puzzle","escape"):
      #  self.alternative_locations["school_first_hall_west"]="standing"

      ########schedule
      if game.dow in (0,1,2,3,4,5,6,7):
        if game.hour in (7,8,9,10):
          self.location="school_homeroom"
          self.activity="sitting"
          return

        if game.hour in (11,12):
          self.location="school_ground_floor"
          self.activity="standing"
          return

        if game.hour in (13,14,15):
          self.location="school_gym"
          self.activity="sitting"
          return

        if game.hour in (16,17,18):
          if game.season == 1 and school_art_class["nude_model"] == self:
            self.location="school_art_class"
            self.activity="pose"
            return
          else:
            self.location="school_first_hall_west"
            self.activity="standing"
            return

        else:
          self.location=None
          self.activity=None


    def avatar(self,state=None):
      ## auto-generated optimal avatar
      if state and isinstance(state,basestring):
        if renpy.has_image("mrsl avatar "+state,True):
          return "mrsl avatar "+state
      rv=[]

      if state.startswith("confident"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((657,1080))
        if "_ponytail" in state:
          rv.append(("mrsl avatar body1_ponytail",153,124))
        else:
          rv.append(("mrsl avatar body1",49,127))
        if "_right" in state:
          rv.append(("mrsl avatar face_confident_right",259,238))
        else:
          rv.append(("mrsl avatar face_confident",256,238))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b1black_panties",166,743))
        elif "_workout_panties" in state:
          rv.append(("mrsl avatar b1workout_panties",167,761))
        elif "_panties" in state:
          rv.append(("mrsl avatar b1panty",164,742))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b1black_bra",252,516))
        elif "_workout_bra" in state:
          rv.append(("mrsl avatar b1workout_bra",250,376))
        elif "_bra" in state:
          rv.append(("mrsl avatar b1bra",254,399))
        if "_leggings" in state:
          rv.append(("mrsl avatar b1leggings",153,676))
        rv.append(("mrsl avatar b1arm2_n",14,372))
        if "_bikini" in state:
          rv.append(("mrsl avatar b1bikini",160,395))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b1red_dress",112,389))
          rv.append(("mrsl avatar b1belt",239,635))
        elif "_dress" in state:
          rv.append(("mrsl avatar b1dressbraless",151,373))
          rv.append(("mrsl avatar b1corset",219,601))
          rv.append(("mrsl avatar b1arm2_c",14,372))
        if "_coat" in state:
          rv.append(("mrsl avatar b1coat",102,367))
          rv.append(("mrsl avatar b1arm2_coat",11,392))

      elif state.startswith("cringe"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((657,1080))
        rv.append(("mrsl avatar body1",49,127))
        rv.append(("mrsl avatar face_cringe",244,230))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b1black_panties",166,743))
        elif "_panties" in state:
          rv.append(("mrsl avatar b1panty",164,742))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b1black_bra",252,516))
        elif "_bra" in state:
          rv.append(("mrsl avatar b1bra",254,399))
        if "_hands_up" in state:
          rv.append(("mrsl avatar b1arm1_n",1,306))
        else:
          rv.append(("mrsl avatar b1arm2_n",14,372))
        if "_bikini" in state:
          rv.append(("mrsl avatar b1bikini",160,395))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b1red_dress",112,389))
          rv.append(("mrsl avatar b1belt",239,635))
        elif "_dress" in state:
          rv.append(("mrsl avatar b1dressbraless",151,373))
          rv.append(("mrsl avatar b1corset",219,601))
          if "_hands_up" in state:
            rv.append(("mrsl avatar b1arm1_c",0,305))
          else:
            rv.append(("mrsl avatar b1arm2_c",14,372))
        if "_coat" in state:
          rv.append(("mrsl avatar b1coat",102,367))
          rv.append(("mrsl avatar b1arm2_coat",11,392))

      elif state.startswith("flirty"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((657,1080))
        if "_ponytail" in state:
          rv.append(("mrsl avatar body1_ponytail",153,124))
        else:
          rv.append(("mrsl avatar body1",49,127))
        rv.append(("mrsl avatar face_flirty",246,235))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b1black_panties",166,743))
        elif "_workout_panties" in state:
          rv.append(("mrsl avatar b1workout_panties",167,761))
        elif "_panties" in state:
          rv.append(("mrsl avatar b1panty",164,742))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b1black_bra",252,516))
        elif "_workout_bra" in state:
          rv.append(("mrsl avatar b1workout_bra",250,376))
        elif "_bra" in state:
          rv.append(("mrsl avatar b1bra",254,399))
        if "_leggings" in state:
          rv.append(("mrsl avatar b1leggings",153,676))
        if "_bikini" in state:
          rv.append(("mrsl avatar b1bikini",160,395))
        rv.append(("mrsl avatar b1arm1_n",1,306))
        if "_workout_bra" in state:
          rv.append(("mrsl avatar b1workout_bra_fix",282,393))
        if "_red_dress" in state:
          rv.append(("mrsl avatar b1red_dress",112,389))
          rv.append(("mrsl avatar b1belt",239,635))
        elif "_dress" in state:
          rv.append(("mrsl avatar b1dressbraless",151,373))
          rv.append(("mrsl avatar b1corset",219,601))
          rv.append(("mrsl avatar b1arm1_c",0,305))
        if "_coat" in state:
          rv.append(("mrsl avatar b1coat",102,367))
          rv.append(("mrsl avatar b1arm1_coat",1,306))

      elif state.startswith("laughing"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((657,1080))
        if "_ponytail" in state:
          rv.append(("mrsl avatar body1_ponytail",153,124))
        else:
          rv.append(("mrsl avatar body1",49,127))
        rv.append(("mrsl avatar face_laughing",254,235))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b1black_panties",166,743))
        elif "_workout_panties" in state:
          rv.append(("mrsl avatar b1workout_panties",167,761))
        elif "_panties" in state:
          rv.append(("mrsl avatar b1panty",164,742))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b1black_bra",252,516))
        elif "_workout_bra" in state:
          rv.append(("mrsl avatar b1workout_bra",250,376))
        elif "_bra" in state:
          rv.append(("mrsl avatar b1bra",254,399))
        if "_leggings" in state:
          rv.append(("mrsl avatar b1leggings",153,676))
        if "_bikini" in state:
          rv.append(("mrsl avatar b1bikini",160,395))
        rv.append(("mrsl avatar b1arm1_n",1,306))
        if "_workout_bra" in state:
          rv.append(("mrsl avatar b1workout_bra_fix",282,393))
        if "_red_dress" in state:
          rv.append(("mrsl avatar b1red_dress",112,389))
          rv.append(("mrsl avatar b1belt",239,635))
        elif "_dress" in state:
          rv.append(("mrsl avatar b1dressbraless",151,373))
          rv.append(("mrsl avatar b1corset",219,601))
          rv.append(("mrsl avatar b1arm1_c",0,305))
        if "_coat" in state:
          rv.append(("mrsl avatar b1coat",102,367))
          rv.append(("mrsl avatar b1arm1_coat",1,306))

      elif state.startswith("afraid"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body2",44,124))
        rv.append(("mrsl avatar face_afraid",157,211))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b2black_panties",149,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b2panty",149,746))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b2black_bra",96,496))
        elif "_bra" in state:
          rv.append(("mrsl avatar b2bra",96,366))
        if "_bikini" in state:
          rv.append(("mrsl avatar b2bikini",96,365))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b2arm2_n",80,379))
          rv.append(("mrsl avatar b2red_dress",96,367))
          rv.append(("mrsl avatar b2belt",172,649))
        elif "_dress" in state:
          rv.append(("mrsl avatar b2dressbraless",96,357))
          rv.append(("mrsl avatar b2corset",172,621))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b2arm2_n",80,379))
        if "_coat" in state:
          rv.append(("mrsl avatar b2coat",142,394))
          rv.append(("mrsl avatar b2arm2_coat",80,379))

      elif state.startswith("annoyed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body2",44,124))
        rv.append(("mrsl avatar face_annoyed",155,208))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b2black_panties",149,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b2panty",149,746))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b2black_bra",96,496))
        elif "_bra" in state:
          rv.append(("mrsl avatar b2bra",96,366))
        if "_bikini" in state:
          rv.append(("mrsl avatar b2bikini",96,365))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b2red_dress",96,367))
          rv.append(("mrsl avatar b2belt",172,649))
        elif "_dress" in state:
          rv.append(("mrsl avatar b2dressbraless",96,357))
          rv.append(("mrsl avatar b2corset",172,621))
        rv.append(("mrsl avatar b2arm1_n",131,631))
        if "_coat" in state:
          rv.append(("mrsl avatar b2coat",142,394))
          rv.append(("mrsl avatar b2arm1_coat",127,626))

      elif state.startswith("concerned"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        if "_ponytail" in state:
          rv.append(("mrsl avatar body2_ponytail",97,116))
        else:
          rv.append(("mrsl avatar body2",44,124))
        rv.append(("mrsl avatar face_concerned",157,210))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b2black_panties",149,741))
        elif "_workout_panties" in state:
          rv.append(("mrsl avatar b2workout_panties",152,736))
        elif "_panties" in state:
          rv.append(("mrsl avatar b2panty",149,746))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b2black_bra",96,496))
        elif "_workout_bra" in state:
          rv.append(("mrsl avatar b2workout_bra",96,358))
        elif "_bra" in state:
          rv.append(("mrsl avatar b2bra",96,366))
        if "_leggings" in state:
          rv.append(("mrsl avatar b2leggings",135,681))
        if "_bikini" in state:
          rv.append(("mrsl avatar b2bikini",96,365))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b2red_dress",96,367))
          rv.append(("mrsl avatar b2belt",172,649))
        elif "_dress" in state:
          rv.append(("mrsl avatar b2dressbraless",96,357))
          rv.append(("mrsl avatar b2corset",172,621))
        rv.append(("mrsl avatar b2arm1_n",131,631))
        if "_coat" in state:
          rv.append(("mrsl avatar b2coat",142,394))
          rv.append(("mrsl avatar b2arm1_coat",127,626))

      elif state.startswith("embarrassed"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body2",44,124))
        rv.append(("mrsl avatar face_embarrassed",136,184))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b2black_panties",149,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b2panty",149,746))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b2black_bra",96,496))
        elif "_bra" in state:
          rv.append(("mrsl avatar b2bra",96,366))
        if "_bikini" in state:
          rv.append(("mrsl avatar b2bikini",96,365))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b2red_dress",96,367))
          rv.append(("mrsl avatar b2belt",172,649))
        elif "_dress" in state:
          rv.append(("mrsl avatar b2dressbraless",96,357))
          rv.append(("mrsl avatar b2corset",172,621))
        rv.append(("mrsl avatar b2arm1_n",131,631))
        if "_coat" in state:
          rv.append(("mrsl avatar b2coat",142,394))
          rv.append(("mrsl avatar b2arm1_coat",127,626))

      elif state.startswith("excited"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        if "_ponytail" in state:
          rv.append(("mrsl avatar body2_ponytail",97,116))
        else:
          rv.append(("mrsl avatar body2",44,124))
        if "_workaround" in state: #this is just a workaround to show the textbox with dissolve after mrsl changes to her bikini (it'll dissolve between the two 'face_excited' images, but they're the exact same)
          rv.append(("mrsl avatar face_excited",128,176))
        else:
          rv.append(("mrsl avatar face_excited",128,176))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b2black_panties",149,741))
        elif "_workout_panties" in state:
          rv.append(("mrsl avatar b2workout_panties",152,736))
        elif "_panties" in state:
          rv.append(("mrsl avatar b2panty",149,746))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b2black_bra",96,496))
        elif "_workout_bra" in state:
          rv.append(("mrsl avatar b2workout_bra",96,358))
        elif "_bra" in state:
          rv.append(("mrsl avatar b2bra",96,366))
        if "_leggings" in state:
          rv.append(("mrsl avatar b2leggings",135,681))
        if "_bikini" in state:
          rv.append(("mrsl avatar b2bikini",96,365))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b2arm2_n",80,379))
          rv.append(("mrsl avatar b2red_dress",96,367))
          rv.append(("mrsl avatar b2belt",172,649))
        elif "_dress" in state:
          rv.append(("mrsl avatar b2dressbraless",96,357))
          rv.append(("mrsl avatar b2corset",172,621))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b2arm2_n",80,379))
          if "_workout_bra" in state:
            rv.append(("mrsl avatar b2workout_bra_fix",145,624))
        if "_coat" in state:
          rv.append(("mrsl avatar b2coat",142,394))
          rv.append(("mrsl avatar b2arm2_coat",80,379))

      elif state.startswith("eyeroll"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body3",59,128))
        rv.append(("mrsl avatar face_eyeroll",197,214))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b3black_panties",138,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b3panty",125,733))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b3black_bra",193,516))
        elif "_bra" in state:
          rv.append(("mrsl avatar b3bra",194,386))
        if "_bikini" in state:
          rv.append(("mrsl avatar b3bikini",141,375))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b3arm1_n",388,637))
          rv.append(("mrsl avatar b3red_dress",57,392))
          rv.append(("mrsl avatar b3belt",189,647))
        elif "_dress" in state:
          rv.append(("mrsl avatar b3dressbraless",103,388))
          rv.append(("mrsl avatar b3corset",164,634))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b3arm1_n",388,637))
        if "_coat" in state:
          rv.append(("mrsl avatar b3coat",51,471))
          rv.append(("mrsl avatar b3arm1_coat",392,648))

      elif state.startswith("neutral"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body3",59,128))
        rv.append(("mrsl avatar face_neutral",173,192))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b3black_panties",138,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b3panty",125,733))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b3black_bra",193,516))
        elif "_bra" in state:
          rv.append(("mrsl avatar b3bra",194,386))
        if "_bikini" in state:
          rv.append(("mrsl avatar b3bikini",141,375))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b3arm1_n",388,637))
          rv.append(("mrsl avatar b3red_dress",57,392))
          rv.append(("mrsl avatar b3belt",189,647))
        elif "_dress" in state:
          rv.append(("mrsl avatar b3dressbraless",103,388))
          rv.append(("mrsl avatar b3corset",164,634))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b3arm1_n",388,637))
        if "_coat" in state:
          rv.append(("mrsl avatar b3coat",51,471))
          rv.append(("mrsl avatar b3arm1_coat",392,648))
        if "_blowtorch" in state:
          rv.append(("mrsl avatar b3blowtorch",2,916))

      elif state.startswith("skeptical"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body3",59,128))
        rv.append(("mrsl avatar face_skeptical",184,208))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b3black_panties",138,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b3panty",125,733))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b3black_bra",193,516))
        elif "_bra" in state:
          rv.append(("mrsl avatar b3bra",194,386))
        if "_bikini" in state:
          rv.append(("mrsl avatar b3bikini",141,375))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b3arm2_n",286,325))
          rv.append(("mrsl avatar b3red_dress",57,392))
          rv.append(("mrsl avatar b3belt",189,647))
        elif "_dress" in state:
          rv.append(("mrsl avatar b3dressbraless",103,388))
          rv.append(("mrsl avatar b3corset",164,634))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b3arm2_n",286,325))
        if "_coat" in state:
          rv.append(("mrsl avatar b3coat",51,471))
          rv.append(("mrsl avatar b3arm2_coat",286,325))

      elif state.startswith("surprised"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body3",59,128))
        rv.append(("mrsl avatar face_surprised",213,214))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b3black_panties",138,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b3panty",125,733))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b3black_bra",193,516))
        elif "_bra" in state:
          rv.append(("mrsl avatar b3bra",194,386))
        if "_bikini" in state:
          rv.append(("mrsl avatar b3bikini",141,375))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b3arm1_n",388,637))
          rv.append(("mrsl avatar b3red_dress",57,392))
          rv.append(("mrsl avatar b3belt",189,647))
        elif "_dress" in state:
          rv.append(("mrsl avatar b3dressbraless",103,388))
          rv.append(("mrsl avatar b3corset",164,634))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b3arm1_n",388,637))
        if "_coat" in state:
          rv.append(("mrsl avatar b3coat",51,471))
          rv.append(("mrsl avatar b3arm1_coat",392,648))
        if "_blowtorch" in state:
          rv.append(("mrsl avatar b3blowtorch",2,916))

      elif state.startswith("thinking"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body3",59,128))
        rv.append(("mrsl avatar face_thinking",213,214))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b3black_panties",138,741))
        elif "_panties" in state:
          rv.append(("mrsl avatar b3panty",125,733))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b3black_bra",193,516))
        elif "_bra" in state:
          rv.append(("mrsl avatar b3bra",194,386))
        if "_bikini" in state:
          rv.append(("mrsl avatar b3bikini",141,375))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b3arm2_n",286,325))
          rv.append(("mrsl avatar b3red_dress",57,392))
          rv.append(("mrsl avatar b3belt",189,647))
        elif "_dress" in state:
          rv.append(("mrsl avatar b3dressbraless",103,388))
          rv.append(("mrsl avatar b3corset",164,634))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b3arm2_n",286,325))
        if "_coat" in state:
          rv.append(("mrsl avatar b3coat",51,471))
          rv.append(("mrsl avatar b3arm2_coat",286,325))
        if "_blowtorch" in state:
          rv.append(("mrsl avatar b3blowtorch",2,916))

      elif state.startswith("angry"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body4",55,125))
        rv.append(("mrsl avatar face_angry",226,198))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b4black_panties",187,734))
        elif "_panties" in state:
          rv.append(("mrsl avatar b4panty",187,722))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b4black_bra",161,518))
        elif "_bra" in state:
          rv.append(("mrsl avatar b4bra",160,395))
        if "_bikini" in state:
          rv.append(("mrsl avatar b4bikini",161,391))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b4red_dress",63,396))
          rv.append(("mrsl avatar b4belt",207,643))
        elif "_dress" in state:
          rv.append(("mrsl avatar b4dressbraless",128,395))
          rv.append(("mrsl avatar b4corset",195,623))
        rv.append(("mrsl avatar b4arm2_n",131,568))
        if "_red_dress" in state:
          rv.append(("mrsl avatar b4coat",106,513))
          rv.append(("mrsl avatar b4arm2_coat",131,568))

      elif state.startswith("blush"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body4",55,125))
        rv.append(("mrsl avatar face_blush",212,206))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b4black_panties",187,734))
        elif "_panties" in state:
          rv.append(("mrsl avatar b4panty",187,722))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b4black_bra",161,518))
        elif "_bra" in state:
          rv.append(("mrsl avatar b4bra",160,395))
        if "_bikini" in state:
          rv.append(("mrsl avatar b4bikini",161,391))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b4arm1_n",108,640))
          rv.append(("mrsl avatar b4red_dress",63,396))
          rv.append(("mrsl avatar b4belt",207,643))
        elif "_dress" in state:
          rv.append(("mrsl avatar b4dressbraless",128,395))
          rv.append(("mrsl avatar b4corset",195,623))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b4arm1_n",108,640))
        if "_coat" in state:
          rv.append(("mrsl avatar b4coat",106,513))
          rv.append(("mrsl avatar b4arm1_coat",108,643))

      elif state.startswith("sad"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body4",55,125))
        rv.append(("mrsl avatar face_sad",241,218))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b4black_panties",187,734))
        elif "_panties" in state:
          rv.append(("mrsl avatar b4panty",187,722))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b4black_bra",161,518))
        elif "_bra" in state:
          rv.append(("mrsl avatar b4bra",160,395))
        if "_bikini" in state:
          rv.append(("mrsl avatar b4bikini",161,391))
        elif "red_dress" in state:
          rv.append(("mrsl avatar b4arm1_n",108,640))
          rv.append(("mrsl avatar b4red_dress",63,396))
          rv.append(("mrsl avatar b4belt",207,643))
        elif "_dress" in state:
          rv.append(("mrsl avatar b4dressbraless",128,395))
          rv.append(("mrsl avatar b4corset",195,623))
        if "red_dress" not in state:
          rv.append(("mrsl avatar b4arm1_n",108,640))
        if "_coat" in state:
          rv.append(("mrsl avatar b4coat",106,513))
          rv.append(("mrsl avatar b4arm1_coat",108,643))

      elif state.startswith("sarcastic"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body4",55,125))
        rv.append(("mrsl avatar face_sarcastic",231,218))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b4black_panties",187,734))
        elif "_panties" in state:
          rv.append(("mrsl avatar b4panty",187,722))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b4black_bra",161,518))
        elif "_bra" in state:
          rv.append(("mrsl avatar b4bra",160,395))
        if "_bikini" in state:
          rv.append(("mrsl avatar b4bikini",161,391))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b4arm1_n",108,640))
          rv.append(("mrsl avatar b4red_dress",63,396))
          rv.append(("mrsl avatar b4belt",207,643))
        elif "_dress" in state:
          rv.append(("mrsl avatar b4dressbraless",128,395))
          rv.append(("mrsl avatar b4corset",195,623))
        if "_red_dress" not in state:
          rv.append(("mrsl avatar b4arm1_n",108,640))
        if "_coat" in state:
          rv.append(("mrsl avatar b4coat",106,513))
          rv.append(("mrsl avatar b4arm1_coat",108,643))

      elif state.startswith("smile"):
        for slot in self.outfit_slots:
          state+=getattr(self.equipped(slot),"state_suffix","")
        rv.append((569,1080))
        rv.append(("mrsl avatar body4",55,125))
        rv.append(("mrsl avatar face_smile",230,206))
        if "_black_panties" in state:
          rv.append(("mrsl avatar b4black_panties",187,734))
        elif "_panties" in state:
          rv.append(("mrsl avatar b4panty",187,722))
        if "_black_bra" in state:
          rv.append(("mrsl avatar b4black_bra",161,518))
        elif "_bra" in state:
          rv.append(("mrsl avatar b4bra",160,395))
        if "_bikini" in state:
          rv.append(("mrsl avatar b4bikini",161,391))
        elif "_red_dress" in state:
          rv.append(("mrsl avatar b4red_dress",63,396))
          rv.append(("mrsl avatar b4belt",207,643))
        elif "_dress" in state:
          rv.append(("mrsl avatar b4dressbraless",128,395))
          rv.append(("mrsl avatar b4corset",195,623))
        rv.append(("mrsl avatar b4arm2_n",131,568))
        if "_coat" in state:
          rv.append(("mrsl avatar b4coat",106,513))
          rv.append(("mrsl avatar b4arm2_coat",131,568))

      if state=="write1":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events write MrsLBlackboard_BG",0,0))
        rv.append(("mrsl avatar events write MrsLBlackboard_Head1",1152,113))
        rv.append(("mrsl avatar events write MrsLBlackboard_Body",1081,355))
        rv.append(("mrsl avatar events write MrsLBlackboard_Panty",1440,757))
        rv.append(("mrsl avatar events write MrsLBlackboard_Dress",1081,383))
        rv.append(("mrsl avatar events write MrsLBlackboard_Hair1",1398,343))
        rv.append(("mrsl avatar events write MrsLBlackboard_Desk",840,905))
      elif state=="write2":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events write MrsLBlackboard_BG",0,0))
        rv.append(("mrsl avatar events write MrsLBlackboard_Head2",1152,123))
        rv.append(("mrsl avatar events write MrsLBlackboard_Body",1081,355))
        rv.append(("mrsl avatar events write MrsLBlackboard_Panty",1440,757))
        rv.append(("mrsl avatar events write MrsLBlackboard_Dress",1081,383))
        rv.append(("mrsl avatar events write MrsLBlackboard_Hair1",1398,343))
        rv.append(("mrsl avatar events write MrsLBlackboard_Desk",840,905))

        #-------------- couchmast
      if state=="surveillance_one":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signup",511,680))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))
      elif state=="surveillance_two":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signup",511,680))
        rv.append(("mrsl avatar events surveillance camera_lindseyrun_xray",235,212))
        rv.append(("mrsl avatar events surveillance camera_lindseyrun_undies",235,212))
        rv.append(("mrsl avatar events surveillance camera_lindseyrun_c",235,212))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))
      elif state=="surveillance_three":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signdown",586,724))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))
      elif state=="surveillance_four":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_isabelle_xray",1131,529))
        rv.append(("mrsl avatar events surveillance camera_isabelle_undies",1131,529))
        rv.append(("mrsl avatar events surveillance camera_isabelle_c",1131,529))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signdown",586,724))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))
      elif state=="surveillance_five":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signdown",586,724))
        rv.append(("mrsl avatar events surveillance camera_mrsl_xray",1339,581))
        rv.append(("mrsl avatar events surveillance camera_mrsl_undies",1338,580))
        rv.append(("mrsl avatar events surveillance camera_mrsl_c",1337,581))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))

      #pole scene
      elif state=="mrsl_pole_ready_frame_one":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance frames camera1_mrsl_poleready",0,0))

      elif state=="mrsl_pole_ready_frame_two":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance frames camera2_mrsl_poleready",0,0))

      elif state=="mrsl_pole_ready_frame_three":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance frames camera3_mrsl_poleready",0,0))

      elif state=="mrsl_pole_bot_frame_one":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance frames camera1_mrsl_polebot",0,0))

      elif state=="mrsl_pole_bot_frame_two":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance frames camera2_mrsl_polebot",0,0))

      elif state=="mrsl_pole_bot_frame_three":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance frames camera3_mrsl_polebot",0,0))


      elif state=="mrsl_poleready":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance Pole_Dance_BG",0,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_leg1",819,410))
        rv.append(("mrsl avatar events surveillance Pole_Dance_base1",234,28))
        rv.append(("mrsl avatar events surveillance Pole_Dance_undies1",592,239))
        rv.append(("mrsl avatar events surveillance Pole_Dance_arms1",587,208))
        rv.append(("mrsl avatar events surveillance Pole_Dance_hair1",629,107))
        rv.append(("mrsl avatar events surveillance Pole_Dance_mop1",525,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_face1",490,135))
      elif state=="mrsl_poletop":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance Pole_Dance_BG",0,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_leg2",609,499))
        rv.append(("mrsl avatar events surveillance Pole_Dance_base2",234,28))
        rv.append(("mrsl avatar events surveillance Pole_Dance_undies2",592,239))
        rv.append(("mrsl avatar events surveillance Pole_Dance_arms2",434,4))
        rv.append(("mrsl avatar events surveillance Pole_Dance_hair2",596,90))
        rv.append(("mrsl avatar events surveillance Pole_Dance_mop2",512,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_face2",490,136))
      elif state=="mrsl_polemid":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance Pole_Dance_BG",0,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_leg3",878,603))
        rv.append(("mrsl avatar events surveillance Pole_Dance_base3",308,123))
        rv.append(("mrsl avatar events surveillance Pole_Dance_undies3",687,325))
        rv.append(("mrsl avatar events surveillance Pole_Dance_arms3",457,44))
        rv.append(("mrsl avatar events surveillance Pole_Dance_hair3",763,204))
        rv.append(("mrsl avatar events surveillance Pole_Dance_mop3",512,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_face3",620,197))
      elif state=="mrsl_polebot":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance Pole_Dance_BG",0,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_base4",422,366))
        rv.append(("mrsl avatar events surveillance Pole_Dance_leg4",974,803))
        rv.append(("mrsl avatar events surveillance Pole_Dance_undies4",845,554))
        rv.append(("mrsl avatar events surveillance Pole_Dance_arms4",697,252))
        rv.append(("mrsl avatar events surveillance Pole_Dance_hair4",817,346))
        rv.append(("mrsl avatar events surveillance Pole_Dance_mop4",512,0))
        rv.append(("mrsl avatar events surveillance Pole_Dance_face4",750,445))

      #couch scene
      elif state=="mrsl_couchmastpre":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy1",938,687))
        rv.append(("mrsl avatar events surveillance MopMast_bra1",329,353))
        rv.append(("mrsl avatar events surveillance MopMast_panty1",725,620))
        rv.append(("mrsl avatar events surveillance MopMast_couchmastpre",226,0))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmastpre",317,158))
      elif state=="mrsl_couchmastready":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy1",938,687))
        rv.append(("mrsl avatar events surveillance MopMast_bra1",329,353))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmastready",226,440))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmastready",297,157))
        rv.append(("mrsl avatar events surveillance MopMast_hand1",366,694))
      elif state=="mrsl_couchmasttip":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy2",947,693))
        rv.append(("mrsl avatar events surveillance MopMast_bra1",329,353))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmasttip",226,427))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmastready",297,157))
        rv.append(("mrsl avatar events surveillance MopMast_hand1",366,694))
      elif state=="mrsl_couchmasttip_eyes":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy2",947,693))
        rv.append(("mrsl avatar events surveillance MopMast_bra1",329,353))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmasttip",226,427))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmast",287,116))
        rv.append(("mrsl avatar events surveillance MopMast_hand1",366,694))
      elif state=="mrsl_couchmasthalf_nipple":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy2",947,693))
        rv.append(("mrsl avatar events surveillance MopMast_bra2",329,355))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmasthalf",226,427))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmast2",280,116))
        rv.append(("mrsl avatar events surveillance MopMast_hand3",366,357))
      elif state=="mrsl_couchmasthalf":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy2",947,693))
        rv.append(("mrsl avatar events surveillance MopMast_bra1",329,353))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmasthalf",226,427))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmast",287,116))
        rv.append(("mrsl avatar events surveillance MopMast_hand1",366,694))
      elif state=="mrsl_couchmastfull":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy2",947,693))
        rv.append(("mrsl avatar events surveillance MopMast_bra2",329,355))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmastfull",226,427))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmast2",280,116))
        rv.append(("mrsl avatar events surveillance MopMast_hand4",366,356))
      elif state=="mrsl_couchmastsquirt":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance MopMast_bg",0,0))
        rv.append(("mrsl avatar events surveillance MopMast_base",0,24))
        rv.append(("mrsl avatar events surveillance MopMast_pussy2",947,693))
        rv.append(("mrsl avatar events surveillance MopMast_bra2",329,355))
        rv.append(("mrsl avatar events surveillance MopMast_panty2",725,619))
        rv.append(("mrsl avatar events surveillance MopMast_couchmastsquirt",226,319))
        rv.append(("mrsl avatar events surveillance MopMast_face_couchmastsquirt",287,116))
        rv.append(("mrsl avatar events surveillance MopMast_hand4",366,356))
        rv.append(("mrsl avatar events surveillance MopMast_squirt",1001,404))

      #stealing
      elif state=="lindsey_stealing1":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signdown",586,724))
        rv.append(("mrsl avatar events surveillance camera_lindseylook_xray",413,552))
        rv.append(("mrsl avatar events surveillance camera_lindseylook_undies",413,552))
        rv.append(("mrsl avatar events surveillance camera_lindseylook_c",413,552))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))
      elif state=="lindsey_stealing2":
        rv.append((1920,1080))
        rv.append(("mrsl avatar events surveillance camera_bg",0,0))
        rv.append(("mrsl avatar events surveillance camera_couch",695,693))
        rv.append(("mrsl avatar events surveillance camera_signdown",586,724))
        rv.append(("mrsl avatar events surveillance camera_lindseylookback_xray",413,552))
        rv.append(("mrsl avatar events surveillance camera_lindseylookback_undies",413,552))
        rv.append(("mrsl avatar events surveillance camera_lindseylookback_c",413,552))
        rv.append(("mrsl avatar events surveillance camera_overlay",0,0))

      elif state.startswith("mrsl_tryout"):
        rv.append(("mrsl avatar events tryout mrsltryout_bg",0,0))
        rv.append(("mrsl avatar events tryout mrsltryout_n",487,16))
        rv.append(("mrsl avatar events tryout mrsltryout_bottom",469,336))
        if "_top" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_top",469,327))
        if "_laughing" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_face_laughing",519,146))
        elif "_flirty" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_face_flirty",731,147))
        elif "_surprised" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_face_surprised",735,145))
        elif "_fgrab" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_face_fgrab",733,146))
        elif "_cum" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_face_cum",731,147))
        else:
          rv.append(("mrsl avatar events tryout mrsltryout_face_start",735,145))
        if "_start" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_arm_start",673,402)) #this has to go above the mc
        if "_boner" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_mc",0,334))
          rv.append(("mrsl avatar events tryout mrsltryout_shorts2",747,617))
        elif "_noboxers" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_dick",859,516))
          rv.append(("mrsl avatar events tryout mrsltryout_mc",0,334))
        else:
          rv.append(("mrsl avatar events tryout mrsltryout_mc",0,334))
          rv.append(("mrsl avatar events tryout mrsltryout_shorts1",747,617))
        if "_grab" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_hand_grab",602,402))
        elif "_stroke1" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_hand_stroke1",635,401))
        elif "_stroke2" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_hand_stroke2",653,401))
        else:
          rv.append(("mrsl avatar events tryout mrsltryout_hand_start",385,805))
        if "_cum" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_cum",1036,814))
        rv.append(("mrsl avatar events tryout mrsltryout_water",0,469))
        if "_noboxers" in state:
          rv.append(("mrsl avatar events tryout mrsltryout_shorts3",0,882))

      elif state.startswith("painting_date"):
        rv.append((1920,1080))
        rv.append(("mrsl avatar events painting_date background",0,0))
        if "_canvas_head" in state:
          rv.append(("mrsl avatar events painting_date canvas1",526,297))
        elif "_canvas_arms" in state:
          rv.append(("mrsl avatar events painting_date canvas2",526,297))
        elif "_canvas_torso" in state:
          rv.append(("mrsl avatar events painting_date canvas3",526,297))
        elif "_canvas_full" in state:
          rv.append(("mrsl avatar events painting_date canvas4",526,297))
        else:
          rv.append(("mrsl avatar events painting_date canvas5",526,297))
        if any(x in state for x in ("smile_","_flirty","_neutral","_laughing","_sad")):
          rv.append(("mrsl avatar events painting_date mrsl_body1",1163,103))
          if "smile_" in state:
            rv.append(("mrsl avatar events painting_date mrsl_face1",1339,174))
          elif "_flirty" in state:
            rv.append(("mrsl avatar events painting_date mrsl_face2",1339,179))
          elif "_neutral" in state:
            rv.append(("mrsl avatar events painting_date mrsl_face3",1339,175))
          elif "_laughing" in state:
            rv.append(("mrsl avatar events painting_date mrsl_face4",1338,169))
          elif "_sad" in state:
            rv.append(("mrsl avatar events painting_date mrsl_face5",1339,169))
        elif "_hands_on_hips" in state:
          rv.append(("mrsl avatar events painting_date mrsl_body2",1163,101))
        elif "_arms_behind_head" in state:
          rv.append(("mrsl avatar events painting_date mrsl_body3",1150,118))
        elif "_standing_sideways" in state:
          rv.append(("mrsl avatar events painting_date mrsl_body4",1161,98))
        elif "_right_leg_up" in state:
          rv.append(("mrsl avatar events painting_date mrsl_body5",1196,0))
        rv.append(("mrsl avatar events painting_date lindsey_body",233,337))
        rv.append(("mrsl avatar events painting_date lindsey_underwear",302,349))
        rv.append(("mrsl avatar events painting_date lindsey_clothes",198,339))
        if state.endswith(("_lindsey_smile","_lindsey_blush","_lindsey_skeptical","_lindsey_worried")):
          rv.append(("mrsl avatar events painting_date lindsey_right_arm1_n",403,350))
          rv.append(("mrsl avatar events painting_date lindsey_right_arm1_c",387,349))
          if state.endswith("_lindsey_smile"):
            rv.append(("mrsl avatar events painting_date lindsey_head1",358,110))
          elif state.endswith("_lindsey_blush"):
            rv.append(("mrsl avatar events painting_date lindsey_head2",358,110))
          elif state.endswith("_lindsey_skeptical"):
            rv.append(("mrsl avatar events painting_date lindsey_head3",358,110))
          elif state.endswith("_lindsey_worried"):
            rv.append(("mrsl avatar events painting_date lindsey_head4",358,110))
        elif state.endswith("_lindsey_embarrassed"):
          rv.append(("mrsl avatar events painting_date lindsey_head5",359,116))
          rv.append(("mrsl avatar events painting_date lindsey_right_arm2_n",415,241))
          rv.append(("mrsl avatar events painting_date lindsey_right_arm2_c",414,350))
        else:
          rv.append(("mrsl avatar events painting_date lindsey_right_arm3_n",413,350))
          rv.append(("mrsl avatar events painting_date lindsey_right_arm3_c",412,349))
          rv.append(("mrsl avatar events painting_date lindsey_head6",419,129))

      elif state.startswith("under_desk"):
        rv.append((1920,1080))
        rv.append(("mrsl avatar events under_desk background",0,0))
        if state.endswith(("_closed_legs","_half_open_legs","_open_legs")):
          rv.append(("mrsl avatar events under_desk mrsl_body1_n",577,156))
          rv.append(("mrsl avatar events under_desk mrsl_body1_u",777,174))
          rv.append(("mrsl avatar events under_desk mrsl_body1_c",746,173))
          if state.endswith("_closed_legs"):
            rv.append(("mrsl avatar events under_desk mrsl_legs1_n",661,409))
            rv.append(("mrsl avatar events under_desk mrsl_legs1_fix",896,418))
            rv.append(("mrsl avatar events under_desk mrsl_legs1_c",697,398))
          elif state.endswith("_half_open_legs"):
            rv.append(("mrsl avatar events under_desk mrsl_legs2_n",581,404))
            rv.append(("mrsl avatar events under_desk mrsl_legs2_fix",877,446))
            rv.append(("mrsl avatar events under_desk mrsl_legs2_c",788,400))
          elif state.endswith("_open_legs"):
            rv.append(("mrsl avatar events under_desk mrsl_legs3_n",326,429))
            rv.append(("mrsl avatar events under_desk mrsl_legs3_fix",789,440))
            rv.append(("mrsl avatar events under_desk mrsl_legs3_c",732,423))
        elif state.endswith(("_tongue1","_tongue2","_tongue3","_tongue4","_tongue5","_tongue6","_squirt1","_squirt2")):
          rv.append(("mrsl avatar events under_desk mrsl_body2_n",577,172))
          rv.append(("mrsl avatar events under_desk mrsl_body2_u",778,176))
          rv.append(("mrsl avatar events under_desk mrsl_body2_c",743,154))
          if state.endswith(("_tongue1","_tongue2","_tongue3","_tongue4","_tongue5","_tongue6")):
            rv.append(("mrsl avatar events under_desk mrsl_legs4_n",216,377))
            rv.append(("mrsl avatar events under_desk mrsl_legs4_c",813,381))
            rv.append(("mrsl avatar events under_desk mc_hands1",391,396))
            if state.endswith("_tongue1"):
              rv.append(("mrsl avatar events under_desk mc_tongue1",951,576))
            elif state.endswith("_tongue2"):
              rv.append(("mrsl avatar events under_desk mc_tongue2",937,552))
            elif state.endswith("_tongue3"):
              rv.append(("mrsl avatar events under_desk mc_tongue3",969,498))
            elif state.endswith("_tongue4"):
              rv.append(("mrsl avatar events under_desk mc_tongue4",976,471))
            elif state.endswith("_tongue5"):
              rv.append(("mrsl avatar events under_desk mc_tongue5",976,465))
          elif state.endswith(("_squirt1","_squirt2")):
            rv.append(("mrsl avatar events under_desk mrsl_legs5_n",295,291))
            rv.append(("mrsl avatar events under_desk mrsl_legs5_c",813,377))
            if state.endswith("_squirt1"):
              rv.append(("mrsl avatar events under_desk squirt",788,164))
            rv.append(("mrsl avatar events under_desk mc_hands2",371,485))
        elif state.endswith("_look"):
          rv.append(("mrsl avatar events under_desk mrsl_body3_n",718,109))
          rv.append(("mrsl avatar events under_desk mrsl_body3_u",974,176))
          rv.append(("mrsl avatar events under_desk mrsl_body3_c",718,109))
          rv.append(("mrsl avatar events under_desk mrsl_legs6_n",648,394))
          rv.append(("mrsl avatar events under_desk mrsl_legs6_fix",816,387))
          rv.append(("mrsl avatar events under_desk mrsl_legs6_c",702,384))
        rv.append(("mrsl avatar events under_desk table",0,0))

      elif state.startswith("bent_forward"):
        rv.append((1920,1080))
        rv.append(("mrsl avatar events bent_forward background",0,0))
        rv.append(("mrsl avatar events bent_forward mrsl_body",144,57))
        rv.append(("mrsl avatar events bent_forward mrsl_underwear",427,202))
        rv.append(("mrsl avatar events bent_forward mrsl_bottoms",755,114))
        if state.endswith("mc_help"):
          rv.append(("mrsl avatar events bent_forward mc_hands",871,426))

      elif state.startswith("leg_stretch"):
        rv.append((1920,1080))
        rv.append(("mrsl avatar events leg_stretch background",0,0))
        rv.append(("mrsl avatar events leg_stretch mrsl_body",15,120))
        rv.append(("mrsl avatar events leg_stretch mrsl_underwear",449,379))
        rv.append(("mrsl avatar events leg_stretch mrsl_bottoms",883,525))
        rv.append(("mrsl avatar events leg_stretch mc_body",280,0))

      elif state.startswith("workout"):
        rv.append((1920,1080))
        rv.append(("mrsl avatar events workout background",0,0))
        if state.endswith("knee_up_jumps"):
          rv.append(("mrsl avatar events workout mrsl_body1",744,31))
          rv.append(("mrsl avatar events workout mrsl_underwear1",867,200))
          rv.append(("mrsl avatar events workout mrsl_bottoms1",865,374))
        elif state.endswith("jumping_jacks"):
          rv.append(("mrsl avatar events workout mrsl_body2",681,0))
          rv.append(("mrsl avatar events workout mrsl_underwear2",849,226))
          rv.append(("mrsl avatar events workout mrsl_bottoms2",676,408))
        elif state.endswith("side_lunges"):
          rv.append(("mrsl avatar events workout mrsl_body3",713,0))
          rv.append(("mrsl avatar events workout mrsl_underwear3",807,346))
          rv.append(("mrsl avatar events workout mrsl_bottoms3",721,530))
        elif state.endswith("leg_raise"):
          rv.append(("mrsl avatar events workout mrsl_body4",706,0))
          rv.append(("mrsl avatar events workout mrsl_underwear4",857,174))
          rv.append(("mrsl avatar events workout mrsl_bottoms4",713,40))
        elif state.endswith("squats"):
          rv.append(("mrsl avatar events workout mrsl_body5",744,290))
          rv.append(("mrsl avatar events workout mrsl_underwear5",873,450))
          rv.append(("mrsl avatar events workout mrsl_bottoms5",739,621))
        elif state.endswith("downward_facing_dog"):
          rv.append(("mrsl avatar events workout mrsl_body6",576,491))
          rv.append(("mrsl avatar events workout mrsl_underwear6",904,510))
          rv.append(("mrsl avatar events workout mrsl_bottoms6",830,490))

      elif state.startswith("clothed_titjob"):
        rv.append((1920,1080))
        rv.append(("mrsl avatar events clothed_titjob background",0,0))
        rv.append(("mrsl avatar events clothed_titjob mc_body",1049,591))
        if state.endswith(("anticipation","titjob1","lick1")):
          rv.append(("mrsl avatar events clothed_titjob mrsl_body1",399,0))
        elif state.endswith(("titjob2","lick2")):
          rv.append(("mrsl avatar events clothed_titjob mrsl_body2",399,0))
        elif state.endswith(("titjob3","lick3","orgasm")):
          rv.append(("mrsl avatar events clothed_titjob mrsl_body3",399,0))
        elif state.endswith(("titjob4","lick4")):
          rv.append(("mrsl avatar events clothed_titjob mrsl_body4",399,0))
        if state.endswith("anticipation"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face1",945,298))
        elif state.endswith("titjob1"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face2",945,295))
        elif state.endswith("titjob2"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face3",948,285))
        elif state.endswith("titjob3"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face4",943,267))
        elif state.endswith("titjob4"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face5",943,274))
        elif state.endswith("lick1"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face6",945,297))
        elif state.endswith("lick2"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face7",948,288))
        elif state.endswith("lick3"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face8",943,269))
        elif state.endswith("lick4"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face9",943,277))
        elif state.endswith("orgasm"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face10",943,267))
        if state.endswith(("anticipation","titjob1","lick1")):
          rv.append(("mrsl avatar events clothed_titjob mc_dick1",1037,559))
          if state.endswith("anticipation"):
            rv.append(("mrsl avatar events clothed_titjob mrsl_arms1",573,497))
            rv.append(("mrsl avatar events clothed_titjob mrsl_underwear1",768,492))
            rv.append(("mrsl avatar events clothed_titjob mrsl_bottoms1",400,780))
          elif state.endswith(("titjob1","lick1")):
            rv.append(("mrsl avatar events clothed_titjob mrsl_underwear2",596,492))
            rv.append(("mrsl avatar events clothed_titjob mrsl_bottoms2",400,779))
            rv.append(("mrsl avatar events clothed_titjob mrsl_arms2",573,595))
        elif state.endswith(("titjob2","lick2")):
          rv.append(("mrsl avatar events clothed_titjob mc_dick2",1047,543))
          rv.append(("mrsl avatar events clothed_titjob mrsl_underwear3",597,491))
          rv.append(("mrsl avatar events clothed_titjob mrsl_bottoms3",400,772))
          rv.append(("mrsl avatar events clothed_titjob mrsl_arms3",573,584))
        elif state.endswith(("titjob3","lick3","orgasm")):
          if state.endswith(("titjob3","lick3")):
            rv.append(("mrsl avatar events clothed_titjob mc_dick3",1036,507))
          elif state.endswith("orgasm"):
            rv.append(("mrsl avatar events clothed_titjob mc_dick5",1036,467))
          rv.append(("mrsl avatar events clothed_titjob mrsl_underwear4",597,483))
          rv.append(("mrsl avatar events clothed_titjob mrsl_bottoms4",400,767))
          rv.append(("mrsl avatar events clothed_titjob mrsl_arms4",532,563))
        elif state.endswith(("titjob4","lick4")):
          rv.append(("mrsl avatar events clothed_titjob mc_dick4",1060,516))
          rv.append(("mrsl avatar events clothed_titjob mrsl_underwear5",597,488))
          rv.append(("mrsl avatar events clothed_titjob mrsl_bottoms5",400,772))
          rv.append(("mrsl avatar events clothed_titjob mrsl_arms5",556,575))
        if state.endswith("lick1"):
          rv.append(("mrsl avatar events clothed_titjob mrsl_face6_fix",1104,522))

      return rv


  class Interactable_mrsl(Interactable):
    def title(cls):
      return mrsl.name

    def actions(cls,actions):

      if mrsl.at("school_art_class","pose"):
        if game.location=="school_art_class":
          actions.append(["consume","\''Paint\''","jacklyn_quest_jacklyn_broken_fuse_model_mrsl"])
      else:

        ############talk#####################
        if quest.fall_in_newfall.in_progress:
          pass
        elif quest.day1_take2.in_progress:
          actions.append(["talk","Talk","?mrsl_talk_day1_take2"])
        else:
          if mrsl["talk_limit_today"]<3:
            actions.append(["talk","Talk","?mrsl_talk_one"])
            actions.append(["talk","Talk","?mrsl_talk_two"])
            actions.append(["talk","Talk","?mrsl_talk_three"])
            actions.append(["talk","Talk","?mrsl_talk_four"])
            actions.append(["talk","Talk","?mrsl_talk_five"])
          else:
            actions.append(["talk","Talk","?mrsl_talk_over"])

        ################Quests###################
        if mc["focus"]:
          if mc["focus"] == "isabelle_haggis":
            if quest.isabelle_haggis == "escape":
              actions.append(["quest","Quest","?isabelle_quest_haggis_mrsl_caught"])
          elif mc["focus"] == "mrsl@poolside_story":
            if quest.poolside_story == "tryout":
              actions.append(["quest","Quest","mrsl_quest_poolside_story_tryout"])
          elif mc["focus"] == "kate_desire":
            if quest.kate_desire in ("stargold","bathroomhelp"):
              actions.append(["quest","Quest","quest_kate_desire_mrsl_talk"])
          elif mc["focus"] == "flora_squid":
            if quest.flora_squid == "wait" and not quest.flora_squid["smell_concern"]:
              actions.append(["quest","Quest","quest_flora_squid_wait_mrsl"])
        else:
          ##########stolen hearts###############
          if quest.isabelle_stolen == "askmaxine":
            actions.append(["quest","Quest","?isabelle_quest_stolen_askmaxine_mrsl"])

          ###############mrsl_HOT##############
          if quest.mrsl_HOT.in_progress:
            if quest.mrsl_HOT=="start":
              if mrsl.at("school_gym","sitting"):
                if quest.mrsl_HOT["broke_af"]:
                  actions.append(["quest","Quest","mrsl_quest_HOT_start_deal"])
                else:
                  actions.append(["quest","Quest","mrsl_quest_HOT_start"])

          ###########poolside story############
          if quest.poolside_story == "newplan":
            actions.append(["quest","Quest","mrsl_quest_poolside_story_newplan"])
          elif quest.poolside_story == "convince":
            actions.append(["quest","Quest","mrsl_quest_poolside_story_convince"])

          ###########gathering storm###########
          if quest.isabelle_locker == "interrogate" and not quest.isabelle_locker["mrsl_interrogated"] and not quest.isabelle_locker["might_have_an_idea"]:
            actions.append(["quest","Quest","quest_isabelle_locker_interrogate_mrsl"])

          ###########fall in newfall###########
          if quest.fall_in_newfall == "stick_around":
            actions.append(["quest","Quest","quest_fall_in_newfall_stick_around"])

          #############hot my bot##############
          if quest.mrsl_bot == "phrase":
            actions.append(["quest","Quest","quest_mrsl_bot_phrase"])
          elif quest.mrsl_bot == "confrontation":
            actions.append(["quest","Quest","quest_mrsl_bot_confrontation"])

          ########## quest interactions for picking up new quests go down here ############
          ########## so they don't pop up while another quest is in_progress ##############

          if game.season == 1:
            if not quest.mrsl_HOT.started and quest.lindsey_nurse.ended:
              if mrsl.at("school_gym","sitting"):
                actions.append(["quest","Quest","?mrsl_quest_HOT_start"])

            if not quest.the_key.started:
              actions.append(["quest","Quest","?mrsl_quest_the_key_start"])

            if not quest.poolside_story.started:
              actions.append(["quest","Quest","?mrsl_quest_poolside_story_start"])

            if quest.isabelle_tour.ended and quest.kate_blowjob_dream.ended and quest.poolside_story.ended and not quest.mrsl_table.started and mrsl.at("school_homeroom","sitting"):
              actions.append(["quest","Quest","quest_mrsl_table_start"])

        ############Flirt###################
        if quest.fall_in_newfall.in_progress:
          pass
        else:
          if mrsl["flirt_limit_today"]<3:
            actions.append(["flirt","Flirt","?mrsl_flirt_one"])
            actions.append(["flirt","Flirt","?mrsl_flirt_two"])
            actions.append(["flirt","Flirt","?mrsl_flirt_three"])
          else:
            actions.append(["flirt","Flirt","?mrsl_flirt_over"])
        ##################################


label mrsl_talk_day1_take2:
  show mrsl smile with Dissolve(.5)
  mrsl smile "Hi, [mc]!"
  show mrsl smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I like your new style, [mrsl]!\"":
      show mrsl smile at move_to(.5)
      mc "I like your new style, [mrsl]!"
      #$mrsl.lust+=1
      mrsl blush "Thank you. I decided to switch it up this year. I hope it's not too much..."
      mc "I'd say it's just right."
    "\"Do you give private swimming lessons, by any chance?\"":
      show mrsl smile at move_to(.5)
      mc "Do you give private swimming lessons, by any chance?"
      #$mrsl.lust+=1
      mrsl excited "Only to the most promising students on the team..."
      mc "I'll start practicing my breaststroke right away!"
    "\"How long have you worked at Newfall High?\"":
      show mrsl smile at move_to(.5)
      mc "How long have you worked at Newfall High?"
      mrsl thinking "For a long time. I've always been drawn to the passion and lust for knowledge."
      mrsl smile "I'll be sad to see you all go by the end of the year."
      "Why does it feel like she's not being sincere?"
  hide mrsl with Dissolve(.5)
  return

label mrsl_flirt_over:
  "She must be sick of my poor attempts at flirting by now..."
  return

label mrsl_talk_over:
  "She's probably had enough of my chitchat for today."
  return

label mrsl_talk_one:
  show mrsl neutral with Dissolve(.5)
  $mrsl["talk_limit_today"]+=1
  mrsl neutral "The passion of the students is what fuels me."
  mrsl excited "I think this is going to be a great year."
  hide mrsl with Dissolve(.5)
  return

label mrsl_talk_two:
  show mrsl smile with Dissolve(.5)
  $mrsl["talk_limit_today"]+=1
  mrsl smile "I'm glad to be out of those old rags."
  mrsl smile "I feel... invigorated."
  mrsl sad "Sometimes you have to make tough choices in life to break out of a bad rut."
  mrsl sad "And it doesn't always come without a price."
  hide mrsl with Dissolve(.5)
  return

label mrsl_talk_three:
  show mrsl annoyed with Dissolve(.5)
  $mrsl["talk_limit_today"]+=1
  mrsl annoyed "I almost slipped just there."
  mrsl annoyed "Cleaning the floor without putting up a sign isn't very bright..."
  mrsl concerned "But I'm sure the janitor has a lot of other talents."
  hide mrsl with Dissolve(.5)
  return

label mrsl_talk_four:
  show mrsl confident with Dissolve(.5)
  $mrsl["talk_limit_today"]+=1
  mrsl confident "I've really enjoyed teaching here and the students have always been a great source of excitement."
  mrsl confident "But I think this is my last year."
  mrsl laughing "Sometimes you have to take a leap of faith and spread your wings."
  hide mrsl with Dissolve(.5)
  return

label mrsl_talk_five:
  show mrsl cringe with Dissolve(.5)
  $mrsl["talk_limit_today"]+=1
  mrsl cringe "Curses! What is that smell?"
  mc "Sorry, I didn't shower this morning."
  mrsl flirty "Oh, it's not you, dear!"
  mrsl cringe "Did the janitor change the soap formula?"
  mc "I don't think so. Smells the same as always."
  mrsl laughing "You're probably right. I've just never gotten used to it."
  hide mrsl with Dissolve(.5)
  return

label mrsl_flirt_one:
  show mrsl sad with Dissolve(.5)
  mrsl sad "Everyone's going through their own struggles. Do you know what fuels me to keep going every day?"
  show mrsl sad at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Getting out of your old clothes.\"":
      show mrsl sad at move_to(.5)
      mc "Getting out of your old clothes."
      mrsl smile "That did help, but it's never been what drives me."
    "\"The passion of the students here.\"":
      show mrsl sad at move_to(.5)
      mc "The passion of the students here."
      if not mrsl["flirt_bonuses_chosen"]:
        $mrsl.lust+=1
      mrsl smile "Correct. That's why I'm still here after all these years."
    "\"Two Duracell batteries and a magic wand.\"":
      show mrsl sad at move_to(.5)
      mc "Two Duracell batteries and a magic wand."
      mrsl smile "Only on slow nights."
      mrsl embarrassed "Sorry, that was a joke."
      mrsl angry "Highly inappropriate by both of us."
    "\"Drinking a glass of lemon juice every morning.\"":
      show mrsl sad at move_to(.5)
      mc "Drinking a glass of lemon juice every morning."
      mrsl cringe "For some reason, I've grown to hate lemons over the years."
      mrsl cringe "Might be because of that infernal soap."
  $mrsl["flirt_limit_today"]+=1
  hide mrsl with Dissolve(.5)
  return



label mrsl_flirt_two:
  show mrsl confident with Dissolve(.5)
  mrsl confident "Keeping your head up is important, but some days are worse than others."
  mrsl cringe "There's one thing that always sours my mood. Do you know what?"
  show mrsl cringe at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Excited students.\"":
      show mrsl cringe at move_to(.5)
      mc "Excited students."
      mrsl neutral "No, I love excitement! It really gets my blood flowing."
    "\"People who don't shower.\"":
      show mrsl cringe at move_to(.5)
      mc "People who don't shower."
      mrsl annoyed "Since I used to be one of them, I can't really blame them too much."
    "\"Old clothes.\"":
      show mrsl cringe at move_to(.5)
      mc "Old clothes."
      mrsl laughing "I did hate my old clothes, but they were so comfy at the same time."
      mrsl cringe "There are, however, things I hate more."
    "\"Lemon soap.\"":
      show mrsl cringe at move_to(.5)
      mc "Lemon soap."
      if not mrsl["flirt_bonuses_chosen"]:
        $mrsl.lust+=1
      mrsl angry "Yes! I can't stand it. It kills my nostrils every morning."
  $mrsl["flirt_limit_today"]+=1
  hide mrsl with Dissolve(.5)
  return



label mrsl_flirt_three:
  show mrsl neutral with Dissolve(.5)
  mrsl neutral "What's something I consider important in order to get through life?"
  show mrsl neutral at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Wet floor signs.\"":
      show mrsl neutral at move_to(.5)
      mc "Wet floor signs."
      mrsl angry "Wet floors are death traps if you're wearing heels."
      mrsl smile "They're important, but that's not really what I had in mind."
    "\"Sexy clothes.\"":
      show mrsl neutral at move_to(.5)
      mc "Sexy clothes."
      mrsl laughing "Well, for some that might be true."
      mrsl flirty "I'm sure Miss [jacklyn] would agree."
    "\"Breaking out of bad ruts.\"":
      show mrsl neutral at move_to(.5)
      mc "Breaking out of bad ruts."
      if not mrsl["flirt_bonuses_chosen"]:
        $mrsl.lust+=1
      mrsl excited "Correct! If you keep making the same poor choices over and over, you'll never get anywhere."
    "\"Spreading your wings.\"":
      show mrsl neutral at move_to(.5)
      mc "Spreading your wings."
      mrsl laughing "Spreading your wings is important. They tend to get stiff otherwise."
      mrsl confident "But that's not really what I had in mind."
  $mrsl["flirt_limit_today"]+=1
  $mrsl["flirt_bonuses_chosen"] = True
  hide mrsl with Dissolve(.5)
  return
