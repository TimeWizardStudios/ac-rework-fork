label mrsl_quest_HOT_start:
  show mrsl thinking with Dissolve(.5)
  "[mrsl] seems lost in thought. Her eyes wander slowly over the gym."
  "She used to be reserved and introverted before her sexual awakening, and this look reminds me of that time."
  "Perhaps her old self is still hiding beneath the surface."
  mc "Excuse me."
  mrsl surprised "Hm?"
  mc "A penny for your thoughts."
  mrsl confident "Hello there, [mc]! I didn't see you come in."
  mrsl flirty "I'm afraid my thoughts are pricier than that."
  show mrsl flirty at move_to(.75)
  menu(side="left"):
    extend ""
    "\"How much?\"":
      show mrsl flirty at move_to(.5)
      $mc.intellect+=1
      mc "How much?"
      mrsl confident "A businessman, eh?"
      mrsl confident "All right, then."
    "\"I don't have a lot of money, but maybe I can pay you in favors?\"":
      show mrsl flirty at move_to(.5)
      $mc.charisma+=1
      mc "I don't have a lot of money, but maybe I can pay you in favors?"
      mrsl laughing "That's adorable."
      mrsl laughing "You just paid me in humor; that's good enough!"
      "Glad she thinks it's humorous and adorable..."
      "I hate not being taken seriously."
      "You say all the right things and still get a crappy response."
      "Whose fault is that? Mine or the women?"
      mc "Well... what's on your mind, then?"
      jump mrsl_quest_HOT_start_end
    "\"I'm thinking about getting in shape, and you clearly take good care of your body. Do you have any tips?\"":
      show mrsl flirty at move_to(.5)
      $mc.strength+=1
      mc "I'm thinking about getting in shape, and you clearly take good care of your body. Do you have any tips?"
      mrsl blush "That's nice of you."
      mrsl blush "A good diet is always step one, but when it comes to gym activities there are many ways."
      mc "What, err... do you usually eat?"
      mrsl smile "A diet is a personal thing. It's never one size fits all."
      mrsl smile "I usually like to keep my meals hot. Lately, I've been going for small portions of junk food."
      mc "You can diet on junk food?"
      mrsl thinking "Some can, if they know what they're doing. And if you keep up the right training regimen."
      mc "I like the sound of that. What kind of training do you usually do, then?"
      jump mrsl_quest_HOT_start_end

label mrsl_quest_HOT_start_deal:
  if quest.mrsl_HOT["broke_af"]:
    show mrsl laughing with Dissolve(.5)
    mrsl laughing "These thoughts will cost you $5."
  else:
    mrsl confident "These thoughts will cost you $5."
    mc "Really?"
    mrsl laughing "Really."
    "It's a bit odd that a teacher is soliciting money from their students..."
    "Surely, that has to be against some kind of school regulation."
    "On the other hand, [mrsl] is a total babe. Is any other logic really needed?"
  show mrsl laughing at move_to(.25)
  menu(side="right"):
    extend ""
    "?mc.money>=5@[mc.money]/5|{image=ui icon_money}|Pay up":
      show mrsl laughing at move_to(.5)
      $mc.money-=5
      mc "Here you go."
      $mc.lust+=1
      $unlock_stat_perk("lust19")
      "The only thing missing is stuffing the cash into the waistband of her panties."
      $mrsl.lust+=1
      mrsl blush "Okay, you're serious about picking my brain!"
      mrsl concerned "This is a little embarrassing, but I only wanted to make sure you were interested."
      $mc.money+=5
      mrsl concerned "It's my poor self-esteem acting up again. Here, have the money back."
      mc "Err... okay."
      mrsl sad "Please, don't tell anyone about this. It was a lapse of common sense."
      mrsl sad "Sometimes, I forget that I'm a teacher."
      mc "Everyone makes mistakes. Don't worry."
    "No deal":
      show mrsl laughing at move_to(.5)
      $mc.love+=1
      mc "Sorry, but asking a student for money seems inappropriate to me."
      $mrsl.lust-=1
      $unlock_stat_perk("love18")
      mrsl afraid "You're totally right! I'm so sorry!"
      mrsl afraid "It's my shitty self-esteem acting up."
      mrsl afraid "I didn't really think you were interested in me... or rather, my thoughts. So, I wanted to put it to the test."
    "Come back later":
      hide mrsl with Dissolve(.5)
      $quest.mrsl_HOT.start()
      $quest.mrsl_HOT["broke_af"] = True
      return
  "How can bad self-esteem even be a thing for a woman like her?"
  "Also, she was totally flirting with me before..."
  "Is she being fishy or just overcompensating?"
  mc "So, about those thoughts?"
  mrsl excited "Oh, right!"

label mrsl_quest_HOT_start_end:
  mrsl excited "I used to follow a series of calisthenics videotapes."
  mrsl excited "I was really happy with the results, and I'm thinking of picking it up again."
  mc "Videotapes?"
  mrsl thinking "Yes, it's a box set called High-intensity Omni-flex Training."
  mc "But why VHS?"
  mrsl embarrassed "Oh, uh, I couldn't find it in another format."
  mrsl embarrassed "It's a very special kind of program."
  "That seems a bit suspicious."
  "[mrsl] always was a little old-fashioned, but who the hell uses VHS in this day and age?"
  mrsl sarcastic "Anyway, I should get going."
  show mrsl sarcastic at disappear_to_right
  $mrsl["at_school_locker_room_now"] = True
  #$process_event("update_state")
  "High-intensity Omni-flex Training? Is that what she said?"
  "The whole VHS business feels odd."
  "Then again, there's been a lot of weirdness since I returned..."
  $quest.mrsl_HOT.start()
  $quest.mrsl_HOT.advance("eavesdrop")
  return

label mrsl_quest_HOT_eavesdrop:
  "???" "What exactly do you think you're doing?"
  mrsl "I'm... not doing anything."
  "???" "Please, I know your tricks. Don't think you can fool me."
  mrsl "I promise I wasn't doing anything."
  "The other voice belongs to a woman. It seems somewhat familiar, but I can't remember where I've heard it."
  "???" "Promise? Now, that's funny."
  "???" "What exactly do you call flirting with him? Is that doing nothing?"
  mrsl "I... I've not been doing it on purpose."
  "???" "Ha! For a natural born liar, you're not very good at it."
  mrsl "I don't know what to tell you."
  mrsl "I've just been going about my business."
  "???" "And we both know what your business is..."
  mrsl "It's not my fault if he's interested in me."
  "???" "Wearing that skimpy dress? I'd say it is."
  mrsl "Dressing like a nun wasn't part of the deal."
  "???" "You've always been a snake! I can't wait to be rid of you!"
  mrsl "I'm sorry you feel that way..."
  "???" "A crocodile snake!"
  "???" "I suppose telling him about those videotapes is also nothing?"
  mrsl "I mean, he asked. And it's just calisthenics..."
  "Wait. They're actually talking about me?"
  "???" "..."
  "???" "You're planning something."
  mrsl "I'm not."
  "..."
  "It went very quiet all of a sudden. I should probably get going before they find me eavesdropping."
  "Who is that person? What deal are they talking about?"
  "Fuck. It feels like they know something that I don't."
  "Also, those videotapes came up again..."
  "Maybe [lindsey] could tell me more. Training programs are her specialty."
  $quest.mrsl_HOT.advance("lindsey_talk")
  return

label mrsl_quest_lindsey_talk:
  show lindsey annoyed with Dissolve(.5)
  "She looks a bit frustrated and worn down."
  mc "You look tired."
  lindsey eyeroll "Gee, thanks."
  show lindsey eyeroll at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I didn't mean anything bad by it.\"":
      show lindsey eyeroll at move_to(.5)
      mc "I didn't mean anything bad by it."
      $lindsey.love+=1
      lindsey sad "Sorry for being snappy..."
      lindsey sad "It's just one of those days when the legs just aren't going as fast as they're meant to."
      mc "Ah, that's how I've felt for years."
      lindsey thinking "Really? Maybe you should get that checked out."
      "Oh, I already know what the problem is. Bad fucking genes."
    "\"Too much training?\"":
      show lindsey eyeroll at move_to(.5)
      $mc.charisma+=1
      mc "Too much training?"
      lindsey thinking "That's right."
      lindsey cringe "I don't know what I'm doing and accidentally trained too much..."
      mc "Is that sarcasm?"
      lindsey flirty "It might've been."
      mc "Now I'm really worried."
      lindsey laughing "Thanks for caring!"
    "\"That time of the month, eh?\"":
      show lindsey eyeroll at move_to(.5)
      mc "That time of the month, eh?"
      $lindsey.love-=1
      $lindsey.lust-=1
      lindsey cringe "What did you just say?"
      mc "I'm talking about your period."
      lindsey thinking "That's weird. Just drop it, okay?"
      mc "I guess I'm okay with that."
  lindsey eyeroll "Believe it or not, I'm actually a bit stressed out over the district marathon..."
  "That's odd. Last time around, [lindsey] blew everyone out of the water."
  mc "How come?"
  lindsey concerned "Ever since I came back to school, I've had a hard time trusting my own feet."
  show lindsey concerned at move_to(.25)
  show flora annoyed at appear_from_right(.75)
  flora annoyed "Sorry to barge in, but I overheard [mc]'s lack of support."
  flora confused "I think you're going to do great, [lindsey]. None of the other runners are near your level!"
  lindsey laughing "Thank you! You're a ray of sunshine."
  mc "I... I was getting to it."
  flora eyeroll "Sure you were."
  show lindsey laughing at move_to("left")
  show flora eyeroll at move_to("right")
  menu(side="middle"):
    extend ""
    "\"I agree with [flora]. You have nothing to worry about, [lindsey]!\"":
      show lindsey laughing at move_to(.25)
      show flora eyeroll at move_to(.75)
      mc "I agree with [flora]. You have nothing to worry about, [lindsey]!"
      $lindsey.love+=1
      lindsey smile "Hey, thanks! I appreciate that."
      flora excited "You should do that more often, [mc]."
      flora excited "For example, I think you could've spent more time on your hair this morning."
      flora confident "Unless frizzle-mop is the look you went for. Ten out of ten, if so."
      mc "Thanks..."
    "\"Thanks for making me look bad, [flora]...\"":
      show lindsey laughing at move_to(.25)
      show flora eyeroll at move_to(.75)
      mc "Thanks for making me look bad, [flora]..."
      flora excited "Anytime!"
      flora confident "Should I tell her about the time you skipped school to sneak into my room?"
      show lindsey laughing at move_to("left")
      show flora confident at move_to("right")
      menu(side="middle"):
        extend ""
        "\"Which time? I didn't know we were keeping track.\"":
          show lindsey laughing at move_to(.25)
          show flora confident at move_to(.75)
          $mc.charisma+=1
          mc "Which time? I didn't know we were keeping track."
          $lindsey.lust-=1
          show lindsey skeptical
          flora angry "Ugh, you have no shame."
        "\"Don't say anything you might regret.\"":
          show lindsey laughing at move_to(.25)
          show flora confident at move_to(.75)
          mc "Don't say anything you might regret."
          $lindsey.lust+=1
          show lindsey smile
          flora eyeroll "Oh, I'm so scared."
          flora confused "Basically, [mc] has a thing for my—"
          mc "Okay! Point taken! Moving on!"
          flora sarcastic "Just this time."
  mc "Anyway... has either of you heard of something called \"High-intensity Omni-flex Training?\""
  lindsey thinking "Hmm... that sounds like one of those 90s training programs."
  lindsey thinking "I only know because my track coach was into them."
  flora thinking "Like yoga?"
  mc "No, I think it's more intense..."
  flora worried "Didn't [jo] use to do something like that?"
  "Now that [flora] mentions it, [jo] was into all sorts of training programs. Totally forgot about that."
  lindsey smile "Well, I got to get my bearings straight for practice. See you around!"
  show flora worried at move_to(.5)
  hide lindsey with moveoutleft
  flora worried "Are you finally going to get in shape, or what's the deal?"
  mc "Err... yes?"
  flora eyeroll "It's something perverted, isn't it?"
  flora eyeroll "I don't want to hear it."
  hide flora with Dissolve(.5)
  $quest.mrsl_HOT.advance("jo_talk")
  return

label mrsl_quest_HOT_jo_talk:
  show jo neutral with Dissolve(.5)
  mc "[jo]? Do you remember those training programs you used to do?"
  jo smile "Yes! They worked wonders. You should try one some time."
  mc "I'm okay, thanks."
  mc "I was wondering if we still have those tapes somewhere?"
  jo thinking "I've gone through a lot of those programs over the years. Most of them are down in the basement."
  mc  "Does High-intensity Omni-flex Training ring any bells?"
  jo sad "I don't remember the names. I'd have to go down there and check."
  jo sad "Can you take care of the laundry in the meantime?"
  show jo sad at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Sure, [jo]!\"":
      show jo sad at move_to(.5)
      mc "Sure, [jo]!"
      $jo.love+=1
      jo laughing "Thank you, honey!"
      jo laughing "I appreciate you helping out."
      jo excited "There's a list of laundry instructions in the bathroom."
      mc "Anytime, [jo]!"
      $quest.mrsl_HOT.advance("laundry")
    "\"Err... it's not that urgent.\"":
      show jo sad at move_to(.5)
      mc "Err... it's not that urgent."
      $jo.love-=1
      jo eyeroll "Very well, I was hoping you could help out a bit more at home."
      jo sarcastic "I'll go look for the videotapes once you've completed a batch of laundry."
      jo sarcastic "I've left a list of instructions in the bathroom."
      "This is basically blackmail. Why can't she just be helpful and reasonable?"
      $quest.mrsl_HOT.advance("laundry")
    "?(mc.intellect>=6)@[mc.intellect]/6|{image=stats int}|\"Remember what happened the last time I did the laundry?\"":
      show jo sad at move_to(.5)
      mc "Remember what happened the last time I did the laundry?"
      jo neutral "You do have a point..."
      jo concerned "Can you talk to [flora] and remind her to do it?"
      mc "Absolutely."
      "Ah, nothing better than pushing chores on [flora]!"
      "However, this has to be a very delicate operation..."
      "A direct approach would likely spell my annihilation."
      "No... this requires wits and guile. Guerilla warfare."
      "[flora] is much stronger than she looks. She can overpower me both physically and mentally."
      "Plus, I get flustered easily when I have to speak to someone face to face."
      "Eye contact is for the birds. A stealth-bombing of text messages is the winning strategy here."
      $quest.mrsl_HOT.advance("text_flora")
  hide jo with Dissolve(.5)
  return

label mrsl_quest_HOT_flora_text_same_room:
  "Texting [flora] in the same room is risky."
  "She's within striking distance. She might be overcome with rage and rush me."
  "Gotta put at least fifty yards between us."
  return

label mrsl_quest_HOT_flora_text:
  $set_dialog_mode("phone_message","flora")
  "Guess what day it is today."
  flora "I thought I blocked you."
  "You did. I went on your phone and unblocked myself."
  flora "I will end you!"
  "Sure you will. Now, guess what day it is!"
  flora "Bitchmas Eve."
  "Almost. It's Flaundry Day!"
  flora "Nope. I did it last week!"
  "Tough luck. [jo] told me to remind you."
  flora "How the hell did you convince her?"
  "Through sharp wit and incompetence."
  flora "That does sound like you."
  "Thank you."
  flora "Whatever."
  flora "I'll give you $10 to do the laundry."
  menu(side="right"):
    "Accept":
      "I'm actually in need of cash right now. Laundry can't be too hard..."
      "Deal!"
      $mc.money+=10
      flora "Great."
      flora "Also, if you send me hentai again I will block you."
      "I'll keep that in mind."
      $set_dialog_mode()
      "She didn't say anything about regular porn, though!"
      $quest.mrsl_HOT.advance("laundry")
    "Decline":
      #mc "I like putting you to work, sis. No amount of money will change that."
      mc "I like putting you to work, [flora]. No amount of money will change that!"
      $flora.lust+=1
      $flora.love-=1
      flora "Fuck off."
      mc "Better get it done soon. [jo] is already annoyed."
      flora "I hate you."
      $set_dialog_mode()
      "Great. With [flora] on laundry duty, I have time to do other things. Watching porn, for example."
      $quest.mrsl_HOT.advance("jo_talk_laundry_done")
  return

label mrsl_quest_HOT_laundry_reset:
  $home_bathroom["sink_detergent"] = False
  $home_bathroom["sink_water"] = False
  $home_bathroom["washing"] = False
  return

label mrsl_quest_HOT_flora_entered_bathroom:
  "Knowing [jo], she won't go look for those videotapes until the laundry is done."
  "Ugh... might as well take a look at the instructions."
  $home_bathroom["mrsl_quest_HOT_flora_entered_bathroom"] = True
  return

label mrsl_quest_HOT_flora_entered_school_first_hall_east:
  "Huh?"
  "Raised voices coming from the locker room. Sounds like an argument..."
  $school_first_hall_east["mrsl_quest_HOT_flora_entered_school_first_hall_east"] = True
  return

label mrsl_quest_HOT_jo_talk_laundry_done:
  show jo smile with Dissolve(.5)
  mc "The laundry situation has been taken care of."
  jo confident "Thank you, honey! I really appreciate that you're giving this old lady a hand."
  jo confident "I went to the basement and looked for those videotapes."
  mc "Did you find anything?"
  jo thinking "I found a lot of old tapes, but not the ones you asked for..."
  jo sad "Sorry, hon."
  "Crap. [jo] used to be a fitness freak. It's honestly weird that she didn't have them."
  "The tapes [mrsl] talked about must be ultra rare..."
  "Maybe there's some information about them on the internet."
  if mc.owned_item("clean_laundry"):
    $mc.remove_item("clean_laundry")
  $quest.mrsl_HOT.advance("research")
  hide jo with Dissolve(.5)
  return

label mrsl_quest_HOT_delivery_arrive_talk_jo:
  show jo confident with Dissolve(.5)
  jo confident "[mc], have you cleaned your room yet?"
  mc "What?"
  jo neutral "You heard me, young man."
  mc "I've cleaned... a little bit?"
  "Eating those week-old cheetos totally counts as cleaning."
  jo skeptical "Okay, what about your homework?"
  mc "We don't have homework with this new system..."
  jo concerned "You still need to study! The new system is all about responsibility. You have to give yourself homework."
  "Man, if only [jo] would get off my back. This is basically asphyxiation through chores."
  mc "I'll... assign myself some homework... I guess."
  jo smile "That's what I like to hear!"
  jo smile "[flora] is already working hard. We've had a couple of other students ask for her homework schedule."
  mc "Great. Good for her."
  jo worried "Maybe you could ask her for some points?"
  "Ugh, [flora] this, [flora] that. The perfect little angel. Why does she have to be such a tryhard?"
  show jo worried at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Life is more than just grades, [jo].\"":
      show jo worried at move_to(.5)
      mc "Life is more than just grades, [jo]."
      $jo.love-=1
      jo annoyed "Good grades lead to a good life. You know that."
      "I wish that I could prove her wrong somehow, but my grades sucked last time around."
      mc "Whatever."
      jo angry "Don't give me that! I care a lot about your future."
      "Highly debatable..."
    "\"Did you know that [flora] skipped school the other day?\"":
      show jo worried at move_to(.5)
      $flora.love-=1
      mc "Did you know that [flora] skipped school the other day?"
      jo thinking "Are you sure? With the new system in place, maybe she's given herself a day off?"
      jo sad "I know she's keeping up her grades. She deserves a day off every now and then!"
      mc "I'm thinking of taking a day off tomorrow."
      jo annoyed "Don't even start with me."
      "Hmm... better not push it when she gets cross. Might lose my internet privileges."
    "\"Whatever. Fine. I'll ask her for advice.\"":
      show jo worried at move_to(.5)
      mc "Whatever. Fine. I'll ask her for advice."
      "Not."
      $jo.love+=1
      jo sarcastic "Very good! While you're at it, tell her I'm making her favorite chili tonight."
      mc "Great. What's the occasion?"
      jo flirty "She's been doing very well at her internship and I wanted to celebrate her success."
      mc "I've had successes as well..."
      jo sarcastic "Oh, what did you do?"
      mc "I showed the new girl around the school."
      jo blush "That was very kind of you."
      mc "So, can we have pizza?"
      jo eyeroll "You know that junk isn't good for you."
      mc "Please!"
      jo eyeroll "The answer is no."
  jo concerned "Anyway... did you order something? I found this package in the mail."
  "Could it be the videotape...?"
  mc "Err... yes, I ordered an instruction video for school!"
  jo smile "That's great to hear! Very good use of your money."
  jo smile "Here you go."
  $mc.add_item("vcr_tape")
  "It's badly wrapped, but it definitely has the shape of a videotape."
  "Looks like an amateur videotape. It doesn't even have a real label."
  "Damn, I can't wait to watch it!"
  "It's a shame we don't have a VHS player at home... but maybe [maxine] has one in the clubroom? She has all sorts of equipment from her investigations."
  hide jo with Dissolve(.5)
  $quest.mrsl_HOT.advance("vhs")
  # $quest.mrsl_HOT.finish(silent=True)
  # $game.notify_modal(None,"Coming soon","{image= mrsl contact_icon}{space=25}|"+"You have reached the end of\nThe Missing Tapes for now!\n\nPlease consider supporting\nus on Patreon.|{space=25}{image= items monkey_wrench}",5.0)
  return

label mrsl_quest_HOT_jo_talk_capture_card:
  show jo smile with Dissolve(.5)
  jo smile "Hey, honey! How's your day so far?"
  mc "It's... a bit weird. I dreamt that [kate] cut a hole in my head and tried to eat my soul with a spoon."
  jo concerned "That doesn't sound like [kate]."
  mc "You're right..."
  "She wouldn't settle for just my soul."
  mc "Anyways, do you know—"
  jo neutral "{i}Anyway.{/}"
  mc "Sorry, [jo]..."
  mc "{i}Anyway{/}, do you know if we have any capture cards down in the basement?"
  jo thinking "I know there's a stack of your old pocketmon cards down there."
  "What does that even have to do with anything?"
  mc "{i}Pokémon.{/}"
  jo annoyed "Don't correct me, [mc]. It's very rude!"
  "Of course..."
  "Not sure why I thought she'd know. Electronic devices are way above her pay grade."
  show jo annoyed at move_to(.25)
  show flora sarcastic at appear_from_right(.75)
  pause(1)
  jo laughing "[flora], sweetheart! There you are."
  jo excited "The Newfall Independence Parade is right around the corner."
  jo excited "How about you and I take the car into the city and pick out an outfit for you?"
  flora confused "It's fine. No one really dresses up for the NIP."
  "Why isn't [jo] asking me? I need new clothes too."
  "And why is she letting [flora] say nip?"
  jo confident "Back in my day, all the girls on the swim team used to dress up and join the parade."
  jo confident "We always had a great time!"
  flora laughing "Thanks, [jo], but I think I'll pass!"
  jo smile "Very well."
  flora flirty "Is [mc] in trouble again?"
  show jo smile at move_to("left")
  show flora flirty at move_to("right")
  menu(side="middle"):
    extend ""
    "\"Why do you always assume I'm in trouble when talking to [jo]?\"":
      show jo smile at move_to(.25)
      show flora flirty at move_to(.75)
      mc "Why do you always assume I'm in trouble when talking to [jo]?"
      flora laughing "I always assume you're in trouble, period."
      jo laughing "He's not in trouble right now, but the day isn't over yet!"
      flora flirty "Did you tell [jo] about... you know what?"
      mc "What?"
      jo displeased "What is she talking about, [mc]?"
      mc "I swear I didn't do anything!"
      "Why is [flora] always getting me in trouble?"
      flora thinking "That's what you said when you filled the head of my plushie unicorn with corn."
      mc "Come on, that was funny!"
      jo annoyed "What did you do this time, [mc]?"
      mc "She's making things up! I've literally been an angel!"
      flora worried "That's what you said when you tried to climb to the top of the Christmas tree."
      mc "That was ages ago!"
      jo annoyed "It's better you come clean now. I don't like finding things out later."
      mc "She's making it up!"
      jo thinking "Why would she do that?"
      $flora.lust+=1
      mc "Because she's an evil genius!"
      jo sad "[flora] is smart, but hardly evil."
      $jo.love-=1
      jo sad "Very well... it'll only be worse for you if I find out what you did later."
      show flora worried at move_to(.5)
      hide jo with moveoutleft
    "?mc.intellect>=5@[mc.intellect]/5|{image=stats int}|\"Don't try to divert the attention to me. Why aren't you going to dress up, [flora]?\"":
      show jo smile at move_to(.25)
      show flora flirty at move_to(.75)
      mc "Don't try to divert the attention to me. Why aren't you going to dress up, [flora]?"
      flora eyeroll "I've never dressed up for this event."
      mc "Kinda rude to the men and women who died for our independence."
      $jo.love+=1
      jo thinking "He does have a point, [flora]."
      flora confused "He does?"
      "I do?"
      flora confused "No one dresses up for that except [mrsl]..."
      jo sad "Well, maybe it's time someone reinstated the tradition."
      mc "I, a hundred percent, agree."
      flora annoyed "Of course you do."
      jo laughing "Okay, [flora], that settles it. You're wearing a dress for the parade."
      jo excited "And we're going shopping tomorrow."
      "Ah! This is perfect! [flora] hates to wear dresses."
      "Finally, something's going my way. Or rather... not going her way."
      "Small victories."
      #Parade day: equip flora dress
      jo excited "Okay. I need to go look up the latest dresses. Be good."
      show flora annoyed at move_to(.5)
      hide jo with moveoutleft
    "Let [jo] answer":
      show jo smile at move_to(.25)
      show flora flirty at move_to(.75)
      jo smile "Not that I know of."
      jo confident "He's been quite good lately... I must say I'm happily surprised."
      flora laughing "That just means you haven't found out about his latest misdeeds!"
      jo confident "I'd like to keep a positive mindset about it."
      jo neutral "You're not going to disappoint me, are you?"
      mc "Err... no, of course not. I'm totally clean."
      flora thinking "Weird, 'cause your bathroom towel has been dry for at least a week."
      mc "That's... not true!"
      "Shit, has it really been a week since my last shower?"
      jo skeptical "Your hygiene is important, [mc]."
      mc "I know that!"
      flora flirty "He totally doesn't know that."
      jo neutral "Go take a shower. I need to get some work stuff done."
      show flora flirty at move_to(.5)
      hide jo with moveoutleft
  flora sarcastic "So, what are you doing today?"
  mc "I'm looking for a capture card for VHS."
  flora confused "What for?"
  show flora confused at move_to(.25)
  menu(side="right"):
    extend ""
    "\"None of your goddamn business.\"":
      show flora confused at move_to(.5)
      mc "None of your goddamn business."
      flora angry "I made cake and now you won't get any!"
      mc "..."
      flora angry "..."
      mc "Fine. I'm sorry."
      flora concerned "No, you're not."
      mc "Ah, you got me."
      flora skeptical "I knew it."
      flora skeptical "Why won't you tell me?"
      mc "Because you can't keep your mouth shut."
      mc "And also because it pleases me to know things you don't."
      flora smile "I get that. It's such a rare thing."
      mc "That was a compliment."
      $flora.love+=1
      flora laughing "Noted."
    "\"For a school project.\"":
      show flora confused at move_to(.5)
      $mc.intellect+=1
      mc "For a school project."
      flora sarcastic "Since when do you do school projects?"
      flora sarcastic "What subject?"
      mc "It's for... gym class."
      flora sarcastic "Why are you lying?"
      mc "I'm not!"
      flora eyeroll "There aren't any projects in gym class this year."
      "Shit."
      mc "It's a special assignment. Got it from [mrsl]."
      flora confused "Why would she be giving out gym class projects?"
      mc "Fine. There's no project."
      flora sarcastic "You're such a bad liar!"
      flora sarcastic "So, what's it really for?"
      mc "I've been following a rabbit hole mystery surrounding a set of missing video tapes."
      flora confused "Why?"
      "Because uncovering this mystery might help me understand the whole time traveling thing."
      "At the very least there might be some really juicy porn on that tape..."
      mc "I've always been curious."
      flora annoyed "You've never been curious."
      mc "Well, this time I am!"
    "\"I need it to watch vintage porn.\"":
      show flora confused at move_to(.5)
      mc "I need it to watch vintage porn."
      flora cringe "Gross."
      flora eyeroll "Why do I even ask?"
      mc "Gross? Old school porn was quite classy."
      flora angry "I don't need to know this!"
      mc "Oh, please. Stop pretending like you're some princess of purity."
      flora concerned "I didn't say I was."
      mc "Sure sounded like it."
      mc "Besides, I know what kind of stuff you're into."
      flora angry "I'm not having this discussion with you."
      mc "Y' ymg' kadishtu n'ghft gotha!"
      #$im ??
      flora laughing "Stop it!"
      mc "Are you impressed?"
      flora flirty "Where did you learn that?"
      mc "During a dark sermon with Cthulhu himself."
      flora laughing "No, you didn't."
      mc "Fine, I used R'Lyehian translator."
      flora confident "Ymg' mgvulgtlagln, uh'enah!"
      mc "That's kinda cute."
      flora angry"It's not meant to be cute!"
  mc "Anyway... do you have any idea if we have an adaptor or capture card somewhere?"
  flora thinking "Isn't there a box of old electronics in the attic?"
  mc "The ladder is broken and you know it."
  flora laughing "Hehe!"
  flora blush "Good luck!"
  hide flora with Dissolve(.5)
  "Hmm... now that she mentioned it, I actually remember that box too."
  "Getting up into the attic is, however, an entirely different matter..."
  $quest.mrsl_HOT.advance("attic")
  return

label home_bedroom_computer_interact_mrsl_HOT_capture_card(item):
  if item == "capture_card":
    $home_bedroom["capture_card"] = True
    $mc.remove_item(item)
    "Okay, perfect. Now let's put the tape into the VHS player and let's see what all the fuss is about!"
    $quest.mrsl_HOT.advance("insert")
  else:
    $quest.mrsl_HOT.failed_item("computer_capture",item)
    "Hooking my [item.title_lower] to the mainframe would be the ideal if my goal was to fry the circuit board."
  return

label home_bedroom_computer_interact_mrsl_HOT_watch:
  "Okay. Long I have waited for this."
  "It better not be a fucking rickroll..."
  show misc ocean as background
  $mrsl.unequip("mrsl_panties")
  $mrsl.equip("mrsl_bikini")
  show mrsl smile behind computer_screen
  show misc computer_screen
  with fadehold
  mrsl smile "Do you ever feel like your jaw aches and your throat is sore?"
  "Holy shit, it's actually [mrsl]!"
  "But it's not an instruction video..."
  "This is some kind of commercial."
  mrsl blush "Do you ever have a hard time sitting down due to pain?"
  mrsl laughing "Worry not, for the new High-intensity Omni-flex Training program is here!"
  mrsl flirty "This brand new program consists of several different levels of intensity and difficulty!"
  mrsl flirty "Our personal trainers are ready to help you get started!"
  mrsl flirty "Sign up now!"
  mrsl confident "And remember, the first lesson is always free if you say: \"HOT my bot!\""
  mrsl laughing "And that is short for bottom!"
  hide background
  hide mrsl
  hide misc computer_screen
  show misc computer_screen_ad
  with Dissolve(.5)
  "That is one odd commercial..."
  "But the first lesson is {i}always{/} free?"
  "Hmm... maybe, maybe, maybe..."
  $mrsl.equip("mrsl_dress")
  $mrsl.equip("mrsl_panties")
  $quest.mrsl_HOT.finish()
  hide misc computer_screen_ad with Dissolve(.5)
  return
