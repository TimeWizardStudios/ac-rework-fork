label mrsl_quest_the_key_start:
  show mrsl smile with Dissolve(.5)
  mrsl smile "Hi, [mc]! How can I help you today?"
  mc "How do I get the key to my locker?"
  mrsl neutral "Since you're eighteen now, you'll have to sign a form at the [guard]'s booth."
  mc "Oh, right. I totally forgot about that."
  mrsl flirty "Here's the permission slip for the [guard]."
  $mc.add_item("permission_slip")
  hide mrsl with Dissolve(.5)
  $quest.the_key.start()
  return
