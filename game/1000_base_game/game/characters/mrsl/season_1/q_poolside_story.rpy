label mrsl_quest_poolside_story_start:
  show mrsl smile with Dissolve(.5)
  mrsl smile "Hi, [mc]! How can I help you today?"
  mc "I'd like to try out for the swim team."
  if not quest.poolside_story["started"]:
    $mc.strength+=1
    $mrsl.lust+=1
  mrsl laughing "Stop by the pool later, and we'll see what happens..."
  "Did she just? No..."
  "Must be my imagination."
  hide mrsl with Dissolve(.5)
  $quest.poolside_story.start()
  $quest.poolside_story["started"] = True
  return

label mrsl_quest_poolside_story_newplan:
  show mrsl neutral with Dissolve(.5)
  mrsl neutral "How can I help you, [mc]?"
  mc "The pool is closed until after the Newfall Independence Day festivities..."
  mrsl surprised "Oh, that's right! I forgot to tell you about that."
  mc "What about my swim team tryout?"
  mrsl concerned "Unfortunately, you'll have to wait until after Christmas."
  mrsl concerned "The season is starting and the school's roster must be finalized before the Newfall Independence Day."
  "Great. So, me trying to ruin the festivities last year really only ruined it for myself? Fuck."
  "Well, I guess it ruined it for any freshmen swimmers too..."
  "Not that my goal was to actually join the swim team. I just wanted to see [mrsl] in a swimsuit."
  mc "All right. That's too bad."
  mrsl excited "I hope you come and cheer for us when the season starts still."
  hide mrsl excited with Dissolve(.5)
  "Hmm... maybe there's a way to get around this. Surely, the freshmen won't be happy about this."
  "If I get enough signatures, maybe the board will reverse the decision..."
  "Getting wet with [mrsl] is something worth fighting for."
  "The newbies always check the bulletin board in the entrance hall."
  "Putting a petition there would be ideal."
  "[jo] knows everything about petitions, but she also knows I wouldn't petition for anything."
  "[isabelle] on the other hand would probably see this as a great injustice..."
  $quest.poolside_story.advance("petition")
  return

label poolside_story_isabelle_petition:
  show isabelle smile with Dissolve(.5)
  mc "Hey, [isabelle]!"
  isabelle smile "Hello!"
  isabelle laughing "Did you see that [mrsl] looked extra excited earlier?"
  mc "Hmm... not really?"
  isabelle concerned_left "All right, maybe it was just me then."
  mc "What do you think she was excited about?"
  isabelle excited "Oh, I was hoping you could tell me... since you spoke to her the moment prior."
  mc "Hmm... no, I didn't notice it. She seemed quite sad when I left."
  isabelle concerned "Sad? Why sad?"
  show isabelle concerned at move_to(.75)
  menu(side="left"):
    extend ""
    "\"She probably saw my grades.\"":
      show isabelle concerned at move_to(.5)
      mc "She probably saw my grades."
      isabelle thinking "Are your grades that bad?"
      mc "They're not good, no."
      mc "I'm especially struggling with English and gym class."
      isabelle afraid "That's really not good, [mc]!"
      isabelle afraid "Your grades are your future!"
      "Ugh, she sounds just like [jo]."
      mc "I was wondering if you could help me out?"
      isabelle laughing "I'm not super good at sports myself, but English is my favorite subject!"
      mc "I've just never been able to wrap my head around the literature."
      isabelle confident "It can be a bit tricky sometimes, but I can definitely help."
      mc "That would be nice of you."
      $mc.intellect+=1
      $isabelle.love+=1
      isabelle laughing "Okay, I'll give you some tips and book recs later!"
    "\"Her mouth wasn't big enough.\"":
      show isabelle concerned at move_to(.5)
      mc "Her mouth wasn't big enough."
      isabelle skeptical "Big enough for what?"
      mc "For my dic...tionary. She couldn't fit in the big words."
      isabelle thinking "I feel like you've used that joke before."
      mc "Nope. Definitely the first time."
      $isabelle.love-=1
      isabelle cringe "Bit inappropriate, innit?"
      mc "I thought it was funny."
      isabelle eyeroll "Agree to disagree."
      "Yikes. I really need to update my joke supply, 'cause that shit's running low."
      isabelle smile "So, was she actually sad?"
      mc "I don't know. I have a hard time reading her."
      isabelle laughing "Fair."
    "\"She seems to be in trouble.\"" if quest.mrsl_HOT > "eavesdrop": #if the player overheard the conversation in Act 1, Scene X27b
      show isabelle concerned at move_to(.5)
      mc "She seems to be in trouble."
      isabelle afraid "In trouble?"
      mc "Yeah. I overheard a conversation in the women's locker room earlier."
      mc "Someone really told her off. And there was something about a deal."
      isabelle thinking "Who?"
      mc "I don't know. The voices were pretty muted."
      isabelle thinking "What kind of deal?"
      mc "I think it was something about me..."
      isabelle thinking "About you? Are you sure?"
      mc "I think the other person accused [mrsl] of being flirtatious with me."
      isabelle cringe "That's really inappropriate."
      isabelle cringe "Have you felt that's the case?"
      mc "Hmm... maybe. Probably more than last time around."
      isabelle concerned "Last time around?"
      mc "Last year I mean."
      isabelle skeptical "Mhm..."
      isabelle neutral "Do you reckon it was the principal?"
      mc "No, it wasn't. I'd definitely recognize [jo]'s voice even if muted."
      isabelle concerned "Maybe you just misunderstood the situation."
      isabelle concerned_left "Eavesdropping isn't exactly reliable all of the time."
      mc "I'm not sure..."
      isabelle excited "Maybe you should report it to the principal?"
      "Knowing [jo], I'd just get in trouble somehow."
      mc "Hmm... maybe later."
  isabelle smile "So, what have you been up to today?"
  mc "I'm actually trying to write a petition to open the pool early."
  isabelle thinking "I did hear that it was closed the first week..."
  mc "It's actually closed longer than that this year. Up until after the Newfall Independence Day."
  mc "No one will have the chance to try out for the swim team before the season starts."
  mc "Freshmen will be robbed of their chance to get a good start on the team."
  isabelle cringe "Right. That is a proper mistake by the administration."
  mc "Think you can help me out?"
  isabelle confident "Absolutely can."
  isabelle confident "But the festivities are right around the corner, and I promised [mrsl] to help decorate."
  isabelle laughing "Maybe we can help each other?"
  "[isabelle] has been here for a few weeks and she's already doing more for the school than I did in four full years."
  "Maybe it's time to step up my game."
  mc "I guess that sounds fair."
  isabelle excited "Right! We'll need confetti and decorations."
  isabelle excited "Take care of that for me and I'll handle the petition."
  "Great. Once again in charge of the confetti."
  "She has no idea that I'm known as the pool clogger."
  mc "Confetti and decorations. Got it."
  hide isabelle with Dissolve(.5)
  "The logical place to start would be the art classroom."
  $quest.poolside_story.advance("confetti")
  return

label poolside_story_enter_art_class:
  "Okay, confetti, confetti, confetti..."
  "With the sheer drought of paper in this school, it's probably easier to go with mylar."
  "That should be somewhere in here..."
  $school_art_class["poolside_story_entered_art_class"] = True
  return

label poolside_story_making_confetti_glass_shard(item1,item2):
  if "glass_shard" in (item1,item2):
    "This glass shard is technically sharp enough for the job, but it'll take me two aeons to shred a bag of confetti."
  elif "locker_key" in (item1,item2):
    "My locker key is technically sharp enough for the job, but it'll take me two aeons to shred a bag of confetti."
  "Need to find something chompier... something chuckier. Maybe a wood chipper?"
  return

label poolside_story_making_confetti(item1,item2):
  $mc.remove_item("old_artwork")
  "There's nothing sharper and more shredding than a pair of beaver teeth."
  "Time to show those woodchucks how it's done..."
  "Chuck, chuck, chuck, chuck, chuck...."
  "Chuck, chuck, chuck..."
  "Chuck, chuck, chuck, chuck..."
  "And there it is. The finest confetti in town!"
  $mc.add_item("confetti")
  "Now for the decorations..."
  "Who knows better how to decorate than the art teacher?"
  $quest.poolside_story.advance("decorations")
  return

label poolside_story_jacklyn_decoarations:
  show jacklyn smile with Dissolve(.5)
  jacklyn smile "Lookin' good there, shooter."
  jacklyn smile "What do you call that shirt? Some kind of spatter job?"
  "Crap. I forgot I spilled mustard on myself last night."
  show jacklyn smile at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I ran out of shirts and the washing machine is broken.\"":
      show jacklyn smile at move_to(.5)
      mc "I ran out of shirts and the washing machine is broken."
      jacklyn neutral "Unfortunately, this ain't the laundry room."
      mc "You're right, I should take better care of my clothes."
      jacklyn thinking "Jokes. I don't remember that."
      jacklyn smile "But how you dress is certainly an extension of your artistic self."
      jacklyn smile "Polished is cream paint, but ragged is only good if you own it."
      mc "I'll... keep that in mind."
    "\"I call it 'Mustard Skies Over Gray Chapels.'\"":
      show jacklyn smile at move_to(.5)
      mc "I call it \"Mustard Skies Over Gray Chapels.\""
      $jacklyn.lust+=1
      jacklyn laughing "How very impressionist!"
      mc "I always like to impress."
    "?mc.intellect>=2@[mc.intellect]/2|{image=stats int}|\"It's a work in progress. Do you have any ideas?\"":
      show jacklyn smile at move_to(.5)
      mc "It's a work in progress. Do you have any ideas?"
      jacklyn excited "How about \"Questionable Substance?\""
      mc "A Pollock reference?"
      jacklyn laughing "Freaky! That's a straight up combustion!"
      mc "Thanks. It's due to my teacher, I think."
      $jacklyn.love+=1
      jacklyn excited_right "Okay, now you're just shining my shoes."
  mc "Listen, I promised [isabelle] to help her out with the decorations for the Newfall Independence Day."
  mc "I was wondering if you could help me out?"
  jacklyn thinking "I can give you the latitudes and maybe the longitudes, but you're steering the ship."
  mc "Anything would be helpful... I've never made decorations before."
  jacklyn smile "It's not exactly exhibit art. All you need is some string, paint, and pennants."
  "Hmm... she's right. It shouldn't be too hard."
  $quest.poolside_story.advance("makedecor")
  hide jacklyn with Dissolve(.5)
  return

label poolside_story_making_decor(item1,item2):
  if mc.owned_item("discarded_art1") and mc.owned_item("discarded_art2") and mc.owned_item("discarded_art3"):
    $mc.remove_item("discarded_art1")
    "Just dip it like a chip! I'm something of an expert in the field."
    $mc.remove_item("discarded_art2")
    "Dip, dip, dip..."
    $mc.remove_item("discarded_art3")
    "..."
    "And there it is."
    $mc.add_item("colored_paper")
    "Haters, where you at?"
  else:
    "I left collectibles behind. Banjo would be disappointed."
  return

label combine_string_pennants(item1,item2):
  $mc.remove_item("pennants")
  $mc.remove_item("string")
  "String 'em up. Like a band of outlaws at the gallows."
  "..."
  mc "Fantastic work!"
  "Had to say it 'cause no one else would."
  $mc.add_item("independence_day_decorations")
  $quest.poolside_story.advance("isadec")
  "All right, time to brag about it to [isabelle]."
  return

label combine_colored_paper_beaver(item1,item2):
  "The ferocious teeth of the beaver are too powerful for this job... it'd be like slicing bread with a chainsaw."
  return

label combine_colored_paper_glass_shard(item1,item2):
  $mc.remove_item("colored_paper")
  "Slice 'em up."
  "Iced-out blade. Glass in my hand."
  "No one's ever seen pennants this grand."
  $mc.add_item("pennants")
  return

label combine_colored_paper_machete(item1,item2):
  $mc.remove_item("colored_paper")
  "When someone asks for a slice of the action, this is what I show them."
  "Fifteen inches of carbon spring steel."
  "Chops paper decorations just as well as it tears through the heart of Africa."
  $mc.add_item("pennants")
  return

label poolside_story_isabelle_isadec:
  show isabelle smile with Dissolve(.5)
  isabelle smile "I see you got the decorations done!"
  isabelle smile "Bloody well done!"
  isabelle smile_left "I reckon I looked for paper for an hour without much luck..."
  mc "Sometimes you need to know where to look."
  isabelle laughing "I suppose you're right!"
  $mc.remove_item("confetti")
  $mc.remove_item("independence_day_decorations")
  isabelle laughing "Thanks for helping me out with this."
  mc "You're welcome!"
  mc "Did you manage to write up the pool petition?"
  isabelle neutral "Yeah, I just put it up on the bulletin board at the entrance."
  isabelle neutral "Give it a few hours to let people add their signatures."
  isabelle excited "Thanks again!"
  hide isabelle with Dissolve(.5)
  "The shameful wait begins..."
  $quest.poolside_story.advance("waitpet")
  $quest.poolside_story["isadecday"] = game.day
  $quest.poolside_story["isadechour"] = game.hour
  return

label mrsl_quest_poolside_story_convince:
  show mrsl smile with Dissolve(.5)
  mrsl smile "Hello there, [mc]."
  mrsl smile "Are you sleeping well?"
  mc "Hmm... apart from one strange dream, I'd say so."
  mc "Do I look tired?"
  mrsl sad "I'm sorry to hear about the dream."
  mrsl sarcastic "You do look a little tired, but maybe you're used to worse?"
  mc "I guess I am."
  "Always slept like shit in my old timeline."
  mrsl blush "In that case, don't mind me!"
  "It's hard not to mind her. I mean, just look at her!"
  "[mrsl] never used to give me a second thought and now she's asking about my sleep..."
  "This timeline is definitely an improvement to the old one."
  "She doesn't look like she knows what's going on, but maybe it's time to start asking questions."
  mc "Random question but..."
  show mrsl blush at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Have you noticed anything odd recently?\"":
      show mrsl blush at move_to(.5)
      mc "Have you noticed anything odd recently?"
      mrsl concerned "Odd? What would that be?"
      mc "Oh, I don't know. Things that seem out of place somehow."
      mrsl excited "Well, you seem to have a bigger joy for life than last year!"
      mrsl excited "It seems we've both managed to work through some stuff."
      mc "What about strange texts? Have you had any of those?"
      mrsl thinking "Can't say I have..."
      mrsl skeptical "I usually block numbers I don't recognize."
      mrsl skeptical "Maybe you should do the same?"
      mc "Hmm, that might be a good idea..."
      mrsl flirty "I think it'd be smart!"
      mrsl cringe "Nothing worse than getting dragged down by pranksters. Or woken up by unwanted ads."
      "She does have a point, and I probably would've blocked that number if they weren't the only person that seems to know what's going on."
      mc "I'll consider doing so, thanks!"
    "\"Do you know what happened to Mrs. Bloomer?\"":
      show mrsl blush at move_to(.5)
      mc "Do you know what happened to Mrs. Bloomer?"
      mrsl thinking "Mrs. Bloomer, hmm..."
      mrsl thinking "I believe she had a heart attack in her sleep..."
      mrsl surprised "The poor lady has been weak for years."
      "Undoubtedly due to decades of chain smoking."
      mc "Very unfortunate."
      mrsl excited "Yeah, it's sad... but we were lucky to get such a competent replacement!"
      "She doesn't seem all too heartbroken about it, but nobody really liked Mrs. Bloomer, so I guess that makes sense."
      "[jacklyn] is definitely a splash of well-needed color in this place."
      mrsl concerned "Why do you ask?"
      "Just trying to figure out why there are anomalies from the old timeline."
      mc "Just curious."
    "\"Is the program for exchange students new?\"":
      show mrsl blush at move_to(.5)
      mc "Is the program for exchange students new?"
      mrsl thinking "Yeah, I believe it's new."
      mrsl skeptical "It went through quite unexpectedly, if I do say so myself..."
      mrsl skeptical "Newfall High is a small school with limited resources."
      mrsl surprised "Hopefully, it won't become a problem."
      "The school board that won't serve proper food to save money, suddenly decides to open up an exchange program?"
      "It seems odd to say the least."
      mc "Do you know who initiated it?"
      mrsl eyeroll "Unfortunately, I don't. I'm not part of the school board."
      mc "Hmm, okay... I might look into it."
  mc "Anyway, on a completely different note. A lot of the students aren't happy about the closed pool."
  mc "I managed to get a list of signatures."
  mrsl surprised "Huh!"
  mc "Basically, we'd like you to open the pool before the independence day."
  mc "So that everyone has a chance to try out before the roster is locked for the season."
  mrsl thinking "That would seem fair..."
  mrsl neutral "However, the sign up list is empty this year."
  mc "Err... but what about me? I signed up and would like to try out."
  mrsl smile "You did, didn't you?"
  mrsl smile "I can bring the list of signatures to the board, but I don't think they'll open it for one student."
  mrsl sad "The clogged filters cost the school a lot to replace last year."
  "Damn it. I really want to see her in a swimsuit."
  mrsl concerned "I see that you're disappointed."
  mrsl excited "I'll tell you what... meet me at the pool and we'll figure out an alternative."
  $quest.poolside_story.advance("meetmrsl")
  hide mrsl with Dissolve(.5)
  return

label school_first_hall_east_door_pool_poolside_story_meetmrsl:
  show mrsl confident at appear_from_right
  mrsl confident "There you are!"
  mc "Here I am."
  mrsl laughing "I came up with an alternative."
  mc "An alternative?"
  mrsl laughing "Yeah, since we can't use the pool."
  mrsl flirty "I booked the gym for the next hour, so we won't be disturbed."
  mrsl flirty "But you have to promise to keep this a secret!"
  mrsl confident "I'm not usually in the habit of making exceptions for the swim team..."
  mrsl confident "But since you collected all those signatures, I think you deserve a chance."
  mc "Err... okay. But if we can't use the pool?"
  mrsl smile "Well, like I said, I came up with an alternative."
  "Not sure how we're going to conduct a swim team tryout in the gym, but.... I can't say I'm not intrigued."
  mrsl blush "Follow me!"
  window hide
  show mrsl blush at disappear_to_left
  pause 0.5
  scene black
  show black onlayer screens zorder 100
  with Dissolve(.07)
  $game.location = "school_gym"
  $school_gym["exclusive"] = "mrsl"
  $mc["focus"] = "mrsl@poolside_story"
  $quest.poolside_story.advance("tryout")
  $renpy.pause(0.25)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  hide black onlayer screens
  with Dissolve(.5)
  return

label mrsl_quest_poolside_story_tryout:
  show mrsl smile with Dissolve(.5)
  mrsl smile "Just one moment, [mc]. I should put on appropriate sports attire."
  $set_dialog_mode("default_no_bg")
  show black with Dissolve(.5)
  pause(1)
  $mrsl.equip("mrsl_bikini")
  $mrsl.unequip("mrsl_panties")
  show mrsl excited_workaround #same exact sprite as mrsl excited, but this forces a dissolve transition next time mrsl speaks
  hide black
  with Dissolve(.5)
  show mrsl excited_workaround with Dissolve(.5)
  $set_dialog_mode("")
  $mrsl.default_name=None
  mrsl excited "Damn." #this line is now said by mrsl to show the textbox with dissolve (set_dialog_mode seems to disable it?); changed her default_name to None momentarily just to hide the namebox
  $mrsl.default_name="Mrs. L"
  "The swimming suit hugs [mrsl]'s curves, stretches across her boobs — skin-tight delight."
  "Takes everything in my power not to reach out, cup and squeeze and fondle. Mmmm..."
  mc "What... err... what's going on?"
  mrsl flirty "Since the pool is closed, but you really wanted a tryout..."
  mrsl laughing "Well, I figured this would be a good alternative."
  mc "A kiddie pool?"
  mrsl confident "It'll be fine!"
  mrsl flirty "And definitely more intimate than a usual tryout."
  "Not sure what's going on here, but she seems to be serious about it."
  "Perhaps this isn't something too unusual..."
  "But it does seem strange."
  "The part about not telling anyone about it also felt a bit suspicious..."
  mrsl flirty "Ready to get started?"
  show mrsl flirty at move_to(.25)
  menu(side="right"):
    extend ""
    "\"I was born ready.\"":
      show mrsl flirty at move_to(.5)
      mc "I was born ready."
      $mrsl.lust+=2
      mrsl laughing "I love the enthusiasm!"
      mrsl confident "Have you done any competitive swimming in the past?"
      mc "No, just surfing..."
      "The internet."
      mrsl confident "That's okay. Perhaps you have the talent for it!"
      jump mrsl_quest_poolside_story_tryout_end
    "\"I don't know... I'm not really sure anymore.\"":
      show mrsl flirty at move_to(.5)
      mc "I don't know... I'm not really sure anymore."
      mrsl embarrassed "You seemed excited just a moment ago! What changed?"
      mc "The kiddie pool. The secrecy. I'm not sure it's a good idea..."
      mrsl excited "It is a little unorthodox, but I promise you it'll be fine!"
      show mrsl excited at move_to(.75)
      menu(side="left"):
        extend ""
        "\"Okay, let's give it a go...\"":
          show mrsl excited at move_to(.5)
          mc "Okay, let's give it a go..."
          $mrsl.lust+=1
          mrsl laughing "Perfect!"
          mrsl confident "Now, let's get into the water, shall we?"
          jump mrsl_quest_poolside_story_tryout_end
        "\"I'm sorry, but I've changed my mind.\"":
          show mrsl excited at move_to(.5)
          mc "I'm sorry, but I've changed my mind."
          mrsl cringe "Are you telling me I took time out of my busy schedule and filled up this pool for nothing?"
          "[mrsl] seems genuinely upset. I don't think I've seen her like this before."
          mc "I'm sorry, I just don't see how it's going to work... and frankly, it'd be a bit humiliating to step into that pool."
          mrsl annoyed "I said it'd be fine!"
          mc "I think I'll just do my tryout in the real pool..."
          mrsl annoyed "There won't be any new tryouts until Christmas."
          $mc.love+=2
          mc "That's okay. I don't mind waiting."
          $mrsl.lust-=2
          mrsl concerned "Very well. I'll see you in class later, [mc]."
          window hide
          $quest.poolside_story.advance("text")
          pause 0.75
          $mrsl.unequip("mrsl_bikini")
          $mrsl.equip("mrsl_panties")
          $mrsl.equip("mrsl_dress")
          pause 0.75
          $school_gym["exclusive"] = 0
          $mc["focus"] = ""
          $mrsl["at_none_now"] = True
          $process_event("update_state")
          show mrsl concerned at disappear_to_right
          pause 1.0
          return

label mrsl_quest_poolside_story_tryout_end:
  $unlock_replay("poolside_story")
  mc "I didn't actually bring my swim trunks."
  mrsl laughing "That's okay. You can just... stay in your underwear."
  "I thought she'd tell me to do it nude there for a moment."
  "Oh, well. Let's do this."
  $set_dialog_mode("default_no_bg")
  show black with Dissolve(.5)
  #SFX: splashing water
  "The pool is tiny. [mrsl]'s sizzling hot thighs press against my back."
  mrsl "Okay, just lean forward onto your stomach."
  $set_dialog_mode("")
  show mrsl mrsl_tryout_start_top
  hide black with Dissolve(.5)
  mrsl "I'm going to hold you, and you're going to show me your butterfly stroke." #smile
  "Butterfly stroke? Uh, damn. I had no idea that was a requirement..."
  "Fuck. How am I supposed to breathe like this?"
  mrsl "Go ahead, I'll hold you." #smile
  show mrsl mrsl_tryout_grab_top_laughing with Dissolve(.5)
  "Her left hand just brushed right up the inside of my thigh!"
  "Err... this position feels a bit—"
  show mrsl mrsl_tryout_start_top_laughing with Dissolve(.5)
  "Shit. Okay, better swim!"
  show mrsl mrsl_tryout_start_top with Dissolve(.5)
  show mrsl mrsl_tryout_grab_top with Dissolve(.5)
  show mrsl mrsl_tryout_start_top with Dissolve(.5)
  "Fuck, her wrist just touched my balls..."
  "This is not the time to get an erection..."
  "Ugh, I can't breathe. Focus. Focus!"
  show mrsl mrsl_tryout_start_top_boner with Dissolve(.5)
  show mrsl mrsl_tryout_start_top_boner_surprised with Dissolve(.5)
  mrsl "That is not a butterfly stroke, [mc]." #surprised
  mrsl "You must be nervous." #surprised
  show mrsl mrsl_tryout_start_top_boner_flirty with Dissolve(.5)
  mrsl "Maybe this will help?" #flirty
  show mrsl mrsl_tryout_start_boner_flirty with Dissolve(.5)#_topless
  mrsl "Okay, let's try something easier. Give me a good breaststroke." #smile
  "Breaststroke, shit. Okay."
  mrsl "Don't worry, I've got you." #flirty
  show mrsl mrsl_tryout_grab_boner_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_start_noboxers_flirty with Dissolve(.5)#_topless
  "What the hell? Where did my boxers go?"
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  "Fuck me!"
  mrsl "There you go. That got you started." #flirty
  "Her hand is like a vice around the base of my cock."
  mrsl "Nice. That's what I'm talking about." #flirty
  mrsl "Long hard strokes!" #flirty
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  mc "{i}*Gurgle*{/}"
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  "Her sharp nails threaten to dig into my flesh, urging me to swim."
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  mrsl "Faster! Faster!" #laughing
  show mrsl mrsl_tryout_grab_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  "Fuck! This is too—"
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  mrsl "This is an old Japanese technique." #flirty
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_grab_noboxers_fgrab with Dissolve(.5)
  "Oh, god. She's squeezing me, her hand wrestling my dick."
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  "Son of a bitch, I'm actually going to cum."
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  mrsl "Your motivation is showing!" #flirty
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  mrsl "Keep going!" #flirty
  show mrsl mrsl_tryout_stroke1_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_laughing with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_fgrab with Dissolve(.5)
  show mrsl mrsl_tryout_stroke1_noboxers_flirty with Dissolve(.5)
  show mrsl mrsl_tryout_stroke2_noboxers_flirty with Dissolve(.5)
  "Oh fuck!"
  show mrsl mrsl_tryout_stroke2_noboxers_cum with Dissolve(.5)
  "God! That's heaven."
  "I can fucking see the light!"
  window hide None
  $mc["focus"] = ""
  show black with fadehold
  $set_dialog_mode("default_no_bg")
  pause(4)
  mrsl "[mc]! Can you hear me?"
  "Uhhh... what the hell?"
  $mrsl.unequip("mrsl_bikini")
  $mrsl.equip("mrsl_panties")
  $mrsl.equip("mrsl_dress")
  hide mrsl with Dissolve(.5)
  hide black with fadehold
  show mrsl afraid with Dissolve(.5) #back in her swimsuit, regular dialogue
  window auto
  $set_dialog_mode("")
  mrsl afraid "Are you okay?"
  "Fuck. Why is my mouth tasting like ashes and salted cola?"
  mrsl concerned "Goodness! Talk to me, [mc]!"
  mc "I don't feel so good..."
  mrsl embarrassed "I'm sorry, you passed out during the test."
  mrsl embarrassed "You got water in your lungs and stopped breathing."
  mrsl embarrassed "I had to give you CPR to bring you back."
  "Huh? I do remember having trouble breathing..."
  mc "How long was I out for?"
  mrsl concerned "Oh, I don't think too long. We were going to do breaststrokes when you went limp."
  mc "Limp? I only remember going hard as a fucking lightning rod."
  mrsl cringe "[mc]! That is highly inappropriate."
  "Huh? Didn't she just give me the best fucking handjob in the west?"
  mc "Didn't you just... grab me?"
  mrsl thinking "Grab you?"
  mc "As in, grab my junk."
  mrsl surprised "I most certainly did not! Why would you even say that?"
  "Seriously? Did my oxygen-deprived brain just make that whole thing up?"
  "I'm still semi-erect... I feel like I just came..."
  mrsl cringe "Your head doesn't seem to be quite right."
  mrsl cringe "I think we need to take you to the [nurse]."
  "I honestly can't tell if she's gaslighting me or if this was just fantasy."
  "She'd be one hell of an actress faking that concern..."
  "Hmm... my head's dizzy though. Better go see the [nurse] like she says."
  show black with fadehold
  call goto_school_ground_floor_west
  hide black with Dissolve(.5)
  "Well, the good news is the [nurse] cleared me."
  "The bad news is that... no matter how ridiculous it sounds, I still feel like [mrsl] gave me that handjob in the pool."
  "Not sure how I'll prove it, though..."
  $school_gym["exclusive"] = 0
  $quest.poolside_story.finish()
  return

label poolside_story_enter_sports_text:
  "Damn. [mrsl] seemed pissed, but I can't really blame her..."
  "She did blow up and fill that pool. She also made an exception for me."
  "No wonder she got annoyed..."
  "But truth be told, the whole situation gave me this weird feeling."
  "Another thing to add to the list."
  "Perhaps it's time to send a text to the one person who seems to know what's going on?"
  $set_dialog_mode("phone_message","hidden_number")
  menu(side="right"):
    "\"Weird things keep happening. I need help!\"":
      mc "Weird things keep happening. I need help!"
      $quest.poolside_story["text"] = 'weird'
      $quest.poolside_story.advance("text2")
    "\"What do you know about [mrsl]?\"":
      mc "What do you know about [mrsl]?"
      $quest.poolside_story["text"] = 'know'
      $quest.poolside_story.advance("text2")
  $set_dialog_mode()
  "Now to wait for a reply..."
  return

label poolside_story_enter_sports_text2:
  if quest.poolside_story["text"] == 'weird':
    $set_dialog_mode("phone_message","hidden_number")
    hidden_number "I am sorry, but I cannot."
    mc "What do you mean? I need answers."
    hidden_number "I have said too much already. I am not allowed to sway you one way or another."
    $set_dialog_mode()
    "Sway me? What the hell is this person on about?"
    "Well, still useless. Not sure what I expected."
  elif quest.poolside_story["text"] == 'know':
    $set_dialog_mode("phone_message","hidden_number")
    hidden_number "Why do you ask that?"
    mc "Something weird happened."
    hidden_number "Weird in what way?"
    $set_dialog_mode()
    "Fuck. How do I even explain this?"
    $set_dialog_mode("phone_message","hidden_number")
    mc "I was meant to try out for the swim team, and I got this weird feeling."
    hidden_number "Remember what I told you."
    $set_dialog_mode()
    "Hmm... what did they tell me? Trust my heart?"
    "Of course it's some cryptic bullshit."
  $quest.poolside_story.finish()
  return
