image poolside_story_swim_tryout_drowned = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-56,-10),Transform("mrsl mrsl_tryout_top_surprised_start",size=(240,135))),"ui circle_mask"))
image poolside_story_swim_tryout_handjob = LiveComposite((128,128),(6,6),AlphaMask(LiveComposite((116,116),(-56,-10),Transform("mrsl mrsl_tryout_flirty_noboxers_grab",size=(240,135))),"ui circle_mask"))


label quest_mrsl_bot_start:
  menu(side="middle"):
    "What time is it?"
    "Sleeping time":
      label quest_mrsl_bot_start_quick_start:
        "God, I'm so tired..."
        "I feel like the semester has barely started and I'm always dealing with so much."
        "I really just need a good night's sleep."
        "That's all I ask."
        window hide
        call home_bedroom_bed_interact_force_sleep
        if "quest_jacklyn_town_start" in game.events_queue:
          $game.events_queue.remove("quest_jacklyn_town_start")
          $quest.mrsl_bot["jacklyn_romance_on_hold"] = True
        if "event_show_time_passed_screen" in game.events_queue:
          $game.events_queue.remove("event_show_time_passed_screen")
        call screen time_passed
        pause 0.5
        window auto
        "Oh, man! Now that was a refreshing sleep!"
        "I feel so much better..."
        "Like I could run a mile."
        "Maybe I'll stop by the gym this morning, and let one good habit roll into another?"
        "Practically professional adulting."
        $mc["focus"] = "mrsl_bot"
        window hide
        $quest.mrsl_bot.start()
    "Brooding time":
      "Rest is for the dead."
      "And those who are afraid of energy drinks."
  return

label quest_mrsl_bot_dream:
  "Huh? That's weird."
  "There must be a leak in the roof or something..."
  "I should probably tell the janitor before someone gets hurt—"
  window hide
  play sound "<from 0.4>poof"
  show expression LiveComposite((335,153),(0,0),"school gym pool",(24,31),"school gym water") as kiddie_pool:
    alpha 0.0 xanchor 0.5 xpos 431+168 yanchor 0.5 ypos 676+77 zoom 0.0
    easein_bounce 0.25 alpha 1.0 zoom 1.0
  pause 0.5
  window auto
  "Wait a second! Where did that come from?"
  window hide
  $mrsl["outfit_stamp"] = mrsl.outfit
  $mrsl.outfit = {"dress":"mrsl_bikini"}
  show mrsl confident at appear_from_left
  pause 0.5
  window auto
  mrsl confident "There you are, [mc]!"
  mc "Err, hi?"
  "Why the hell is she wearing that?"
  "Not that I'm complaining, of course."
  if quest.poolside_story.actually_finished and not quest.poolside_story["text"]:
    "..."
    "Maybe it's time for my swim tryout?"
    "But I thought I already did that..."
  mrsl flirty "I have been looking all over for you."
  mrsl flirty "I have such a wonderful surprise."
  mc "Oh? For me?"
  mrsl laughing "Of course it's for you, silly boy!"
  mrsl laughing "Do you want to take a guess at what it is?"
  if quest.poolside_story.actually_finished and not quest.poolside_story["text"]:
    show mrsl laughing at move_to(.25)
    menu(side="right"):
      extend ""
      # "?quest.poolside_story.actually_finished and not quest.poolside_story['text']@|{image=poolside_story_swim_tryout_drowned}|\"Did you change your mind about the swim team?\"":
      "?quest.poolside_story.actually_finished and not quest.poolside_story['text']@|{image=poolside_story_swim_tryout_drowned}|\"Did you change your mind\nabout the swim team?\"":
        show mrsl laughing at move_to(.5)
        mc "Did you change your mind about the swim team?"
        mrsl cringe "Oh, absolutely not. You almost drowned, remember?"
        mrsl cringe "That's not exactly swim team material."
        "Gee, thanks."
        mc "Well, why is the pool here, then?"
        mrsl confident "I was just getting in a quick workout."
        mrsl confident "I have to keep this body nice and tight for you, after all."
        "Hold up..."
        mc "Err, for me?"
        mrsl laughing "Of course!"
      "\"Is [lindsey] finally out of the hospital?\"":
        show mrsl laughing at move_to(.5)
        label quest_mrsl_bot_dream_lindsey:
          mc "Is [lindsey] finally out of the hospital?"
          if quest.lindsey_angel.finished:
            "Wishful thinking more than anything..."
          mrsl concerned "Oh, um. I'm afraid not."
          mrsl excited "But I love how big your heart is!"
          mrsl excited "It's almost as big as... {i}other{/} parts of you..."
          "Wait, what? That seems like a super inappropriate thing to say, especially to my question."
          if quest.poolside_story.actually_finished and not quest.poolside_story["text"]:
            "But if she's implying what I think she is... then I knew I wasn't crazy!"
          mc "Err, thanks?"
          mrsl laughing "Oh, don't thank me just yet!"
      # "?quest.poolside_story.actually_finished and not quest.poolside_story['text']@|{image=poolside_story_swim_tryout_handjob}|\"Are you going to give me another handjob?\"":
      "?quest.poolside_story.actually_finished and not quest.poolside_story['text']@|{image=poolside_story_swim_tryout_handjob}|\"Are you going to give\nme another handjob?\"":
        show mrsl laughing at move_to(.5)
        mc "Are you going to give me another handjob?"
        mrsl confident "Oh, my! I have {i}no{/} idea what you're talking about!"
        mc "Sure you don't."
        mc "It was right here, in this very gym. In that very pool."
        mc "And now you're back for round two."
        mrsl flirty "Mmm! Aren't you self-assured?"
        mc "I bet you have been dying to get me alone again."
        mrsl flirty "You have no idea..."
  else:
    jump quest_mrsl_bot_dream_lindsey
  mrsl flirty "Now, are you ready for your surprise or not?"
  "Fuck me. The way she's looking at me..."
  "With those sultry eyes... and those full pouty lips..."
  "It hardens me in an instant."
  # "It sends a fog creeping through my brain, eliminating all other senses."
  "It sends a fog creeping through my brain, eliminating all other senses.{space=-40}"
  mc "I'm so ready."
  mrsl flirty "Close your eyes, then."
  "She absolutely does not have to tell me twice."
  mc "Anything you say..."
  window hide
  hide screen interface_hider
  show mrsl confident:
    zoom 5.0 xoffset -72 yoffset 2727
  show black
  show black onlayer screens zorder 100
  show teary_eyes2 as half_open_eyes:
    ysize 1080
  with close_eyes
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  mrsl confident "Good. Very good."
  play sound "zipper"
  "A faint smell of cigarettes passes through my senses, followed by a gentle pressure against my crotch as my fly is undone."
  "My breath hitches in my throat."
  mc "Oh, f-fuck!"
  "Her voice is but a whisper in my ear."
  mrsl confident "{i}Language.{/}"
  mc "S-sorry..."
  show black onlayer screens zorder 100
  pause 0.5
  $mc["focus"] = ""
  $game.ui.hide_hud = True
  hide black
  hide black onlayer screens
  with open_eyes
  window auto
  $set_dialog_mode("")
  "By reflex, my eyes start to open."
  mrsl confident "Nuh-uh! Keep them closed."
  mc "Sorry!"
  window hide
  show black
  show black onlayer screens zorder 100
  with close_eyes
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "A low involuntary groan escapes my throat."
  "Her warm, velvety hands free my throbbing cock."
  mrsl confident "There he is! My big boy."
  "Her words come out in a sultry purr."
  "It takes everything in my power not to cum instantly in her grip."
  "Thankfully, I don't, because in the next second her wet, plump lips are kissing the head of my dick."
  mc "H-holy..."
  mc "Oh, god. Oh, god!"
  mrsl confident "Mmm... far from it..."
  mc "Err, what?"
  "Fuck, it doesn't even matter what she's saying."
  "All that matters is the suction of her mouth as she wraps it around my shaft."
  "Slowly, painstakingly, swallowing my dick."
  mrsl confident "Mmmm!"
  mc "Oooh..."
  mc "F-fuck! I'm going to—"
  show black onlayer screens zorder 100
  pause 0.25
  $quest.mrsl_bot.advance("phrase")
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  $game.ui.hide_hud = False
  play sound "audio/music/alarm.ogg"
  pause 0.75
  hide kiddie_pool
  hide mrsl
  hide half_open_eyes
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  play music "home_theme" fadein 0.5
  pause 0.125
  window auto
  $set_dialog_mode("")
  "..."
  "With a choking gasp, I sit up in bed."
  "Struggling to suck in air."
  "My sheets wet."
  "The dream comes crashing back to me in a tingling wave."
  "It felt so damn real..."
  "I haven't had a dream like that in a while."
  "But it tickled something that had been lingering at the edge of my memory."
  "[mrsl] and those tapes."
  "With everything that has been going on lately, what with [lindsey] falling, and the school feeling more and more chaotic..."
  "I had totally forgotten about them."
  "But now it's brought to the forefront of my mind."
  "...specifically, that phrase from the commercial."
  "I think it's time I find out what that is all about."
  $mrsl.outfit = mrsl["outfit_stamp"]
  if quest.mrsl_bot["jacklyn_romance_on_hold"]:
    window hide
    $game.events_queue.append("quest_jacklyn_town_start_quick_start")
    $del quest.mrsl_bot.flags["jacklyn_romance_on_hold"]
    window auto
  return

label quest_mrsl_bot_phrase:
  show mrsl excited with Dissolve(.5)
  mrsl excited "Good morning, [mc]!"
  mrsl excited "You look well-rested."
  mc "Err, I do?"
  mc "I actually had some interesting dreams last night..."
  mrsl concerned "Oh? Such a busy mind you have, even in sleep."
  mc "I guess you could say that."
  mrsl concerned "Is everything all right?"
  "Okay. This is my chance."
  "It's time to test out my theory."
  "If I pussy out now, I may never work up the courage again."
  mrsl excited "Hmm?"
  "She watches me expectantly, her dark eyes curious."
  "A small smile playing on her lips."
  "I take a deep breath."
  mc "..."
  mc "Hot my bot."
  mrsl surprised "What did you just say?"
  show mrsl surprised at move_to(.25)
  menu(side="right"):
    extend ""
    "?quest.poolside_story.actually_finished and not quest.poolside_story['text']@|{image=poolside_story_swim_tryout_handjob}|\"You heard me.\"":
      show mrsl surprised at move_to(.5)
      mc "You heard me."
      mrsl thinking "I don't know what you are getting at."
      mc "I think you do."
      mc "Just like how we got at it during my swim tryout, remember?"
      mrsl flirty "Do you mean how we got at your... {i}calisthenics?{/}"
      mc "Sure. Something like that."
      mc "I definitely got a full body workout in that pool... and I think you got a pretty good arm workout, too."
      $mrsl.lust+=1
      mrsl laughing "Holding you up, yes!"
    "\"Err, you know, as in 'hot my bottom?'\"":
      show mrsl surprised at move_to(.5)
      mc "Err, you know, as in \"hot my bottom?\""
      mrsl thinking "Oh, goodness."
      mrsl thinking "Now, there's a phrase I have not heard in a long time..."
      mc "But you have heard it, right?"
      mrsl annoyed "Well, yes."
      mrsl annoyed "There was a time when I had to do what I could to make ends meet."
      "...and I would love to make our ends meet..."
      "Goddamn, that dream is really sticking with me."
      mc "Right. Of course."
    # "\"I have taken a sudden interest in working out.\"":
    "\"I have taken a sudden\ninterest in working out.\"":
      show mrsl surprised at move_to(.5)
      mc "I have taken a sudden interest in working out."
      mrsl thinking "Is that so?"
      mrsl thinking "Well, I'm afraid I cannot help you with your new little interest."
      mc "Why not?"
      # mc "I was hoping for the best workout I can get, and yours looks pretty great."
      mc "I was hoping for the best workout I can get, and yours looks pretty great.{space=-100}"
      mc "I think it would really help me get fit and stay fit."
      $mc.strength+=1
      mc "And have some muscle definition, too."
      mrsl skeptical "You're very adamant, aren't you?"
  if renpy.showing("mrsl laughing"):
    mrsl cringe "But I do not work for the High-intensity Omni-flex Training Program anymore."
    mrsl cringe "So, I'm afraid I really cannot help you."
  else:
    mrsl "But I do not work for the High-intensity Omni-flex Training Program anymore."
    mrsl "So, I'm afraid I really cannot help you."
  mc "I am well aware that you don't work there anymore."
  # mc "But the phrase is supposed to be honored for up to thirty years, and I know you're not that old."
  mc "But the phrase is supposed to be honored for up to thirty years, and{space=-10}\nI know you're not that old."
  mc "I mean, you look like you made that video yesterday!"
  mrsl blush "Trying to charm our way into it, are we, [mc]?"
  mc "I'm just stating facts... and looking for that beneficial free lesson."
  mrsl blush "Oh, but of course."
  mrsl blush "Still, I am not one to turn my nose up at valid loopholes..."
  "Seriously? Is this actually about to work?"
  mrsl smile "Very well. You have the special offer code and I will honor it."
  mc "That's—"
  mrsl smile "But! It needs to be private."
  mrsl smile "I can't have that offer running rampant through the school."
  mc "I understand."
  mrsl smile "Good. Meet me outside the school after dark, then."
  mrsl blush "And until then, not another word about this, okay?"
  window hide
  hide mrsl with Dissolve(.5)
  window auto
  "Holy crap. I can't believe that actually worked!"
  "Now, I just have to get through the rest of the day..."
  $quest.mrsl_bot.advance("wait")
  return

label quest_mrsl_bot_wait:
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  $quest.mrsl_bot.advance("meeting")
  call screen time_passed
  pause 0.5
  if game.location != "school_entrance":
    "It's been hard to focus since [mrsl] agreed to meet me tonight."
    "But the hour is fast approaching now..."
    "Just a little while longer and I'll be getting a full body workout with the hottest instructor ever!"
    "..."
    "..."
    "..."
    "Okay. Surely, it's been long enough?"
    if mc.owned_item("paper"):
      "I just have to put [maxine]'s priced paper back in the stack first."
      "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    if mc.owned_item("paper"):
      $mc.remove_item("paper")
      pause 0.25
    show black onlayer screens zorder 100 with Dissolve(.5)
    if game.location.id.startswith("school_"):
      $game.location = "school_entrance"
      pause 1.0
    else:
      $game.location = "school_entrance"
      pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "<from 35>outdoor_wind"
    window auto
  "The sand crunches under my sneakers as I enter the dimly lit schoolyard."
  "Adrenaline of anticipation courses through me already."
  "Me and [mrsl], one on one..."
  if quest.poolside_story.actually_finished and not quest.poolside_story["text"]:
    "Just like last time with the swim tryout."
    "Maybe this time we'll take things even further?"
  "..."
  "That is, if she shows up."
  "I thought I would be late... but I don't see any sign of her."
  "Oh, man. I hope she's not having second thoughts about this."
  "..."
  "Maybe she just got caught up with something inside?"
  $mc["focus"] = "mrsl_bot"
  return

label quest_mrsl_bot_meeting_path:
  "Not right now. Getting lost in the middle of the forest at night is an activity for another time."
  return

label quest_mrsl_bot_meeting:
  scene black with Dissolve(.07)
  $game.location = "school_ground_floor"
  $renpy.pause(0.07)
  $renpy.get_registered_image("location").reset_scene()
  scene location
  show guard neutral
  with Dissolve(.5)
  window auto show
  show guard angry_flashlight with dissolve2
  guard angry_flashlight "Hey! Just what do you think you're doing out here?" with vpunch
  "Crap."
  # "I swear the [guard] is only good at his job when it comes to busting me..."
  "I swear the [guard] is only good at his job when it comes to busting me...{space=-75}"
  "He can't be bothered when you actually need him."
  show guard angry_flashlight at move_to(.75)
  menu(side="left"):
    extend ""
    "\"Err, I was just leaving...\"":
      show guard angry_flashlight at move_to(.5)
      mc "Err, I was just leaving..."
      guard suspicious_flashlight "A likely story."
      $mc.charisma+=1
      mc "It's true! I stayed late for some, um, extra credit work?"
      guard suspicious_flashlight "Mm-hmm."
      mc "You can ask [mrsl] if you don't believe me."
      mc "She'll back up my story, though."
      guard suspicious_flashlight "I just might."
      mc "Oh, I'm sure she would love to be interrupted right now."
      mc "It's not like she has a mountain of papers to grade..."
      guard suspicious_flashlight "Regardless, you better get out of here now, kid. It's late."
    "\"I'm watching the meteor shower.\"":
      show guard angry_flashlight at move_to(.5)
      mc "I'm watching the meteor shower."
      guard suspicious_flashlight "The what, now?"
      mc "It's a rare cosmic phenomenon that happens once every hundred years or so!"
      mc "You should look it up. Tonight is a very special night."
      guard neutral_flashlight "Err, I don't know much about that space stuff..."
      $mc.intellect+=1
      mc "Oh, well. You're missing out!"
      guard neutral_flashlight "Regardless, you better get out of here now, kid. It's late."
    # "\"Hey! Just what do you think you're doing out here?\"":
    "\"Hey! Just what do you think\nyou're doing out here?\"":
      show guard angry_flashlight at move_to(.5)
      mc "Hey! Just what do you think you're doing out here?"
      guard neutral_flashlight "Excuse me?"
      mc "Excuse me?"
      guard surprised_flashlight "Are you mocking me?!"
      mc "Are you mocking me?!"
      guard angry_flashlight "Knock that off!"
      mc "Knock that off!"
      guard angry_flashlight "I'm warning you..."
      mc "I'm warning y—"
      guard angry_flashlight "That's it! That's a detention for you!" with vpunch
      "Welp. I don't know why I had to go and poke the bear."
      "It never ends well..."
      mc "Fine. I'm sorry."
      $mc["detention"]+=1
      guard angry_flashlight "You better get out of here now, kid. It's late."
  mc "I'm going, I'm going..."
  "He stands there expectantly, waiting."
  "So, I have no choice but to turn and walk away."
  "Slowly, into the night, feeling his eyes bore into my back."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $game.location = "school_entrance"
  pause 1.0
  hide guard
  hide black onlayer screens
  with Dissolve(.5)
  window auto
  "A few minutes pass as I wait for him to leave."
  "There's no way I'm missing out on this."
  "I'll just have to find a different way inside..."
  "..."
  "Hmm... what about through one of the windows?"
  "I'll just climb up [maxine]'s rope ladder."
  # "As long as I'm fast, I should be able to beat the [guard]'s loop around."
  "As long as I'm fast, I should be able to beat the [guard]'s loop around.{space=-20}"
  # "I knew there were more practical reasons to work on my upper body strength!"
  "I knew there were more practical reasons to work on my upper body{space=-5}\nstrength!"
  "I'll just have to make due with what I have, though."
  "..."
  "Wait a second..."
  "Why is the rope ladder gone?!"
  "Damn it, [maxine]! Why tonight of all nights?"
  "Maybe if I—"
  "???" "...know better..."
  "Huh?"
  "Hang on. I recognize that voice."
  mrsl "...lecture me."
  "[mrsl]? Is this why she didn't meet me outside?"
  "I wonder what they're talking about."
  "Maybe if I just... get a little closer..."
  "???" "I mean it! I know you have something planned."
  "???" "You cannot go through with whatever it is."
  mrsl "Yes, yes. It's all perfectly innocent, okay?"
  mrsl "I promise you."
  "???" "Pfft! Knowing you? I highly doubt that."
  "???" "I am serious. Do {i}not{/} push it."
  mrsl "Is that all? This conversation is boring me now."
  "???" "Well, tonight is about to get even more boring for you."
  "..."
  "Welp. Whoever it is, they sure are bossy."
  "And clearly keeping tabs on [mrsl]..."
  "The question is, why?"
  "And why does [mrsl] put up with it?"
  "Anyway, it seems like I won't be meeting up with [mrsl] tonight, after all."
  "Damn it! I was so looking forward to it!"
  "I guess I'll have to ask [mrsl] about it tomorrow..."
  "For now, I should get home before the [guard] shows up again."
  window hide
  show black onlayer screens zorder 100 with Dissolve(.5)
  $mc["focus"] = ""
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  if not quest.mrsl_bot["jacklyn_romance_on_hold"]:
    $game.advance()
  pause 2.0
  hide black onlayer screens with Dissolve(.5)
  play music "home_theme" fadein 0.5
  if quest.mrsl_bot["jacklyn_romance_on_hold"]:
    $game.events_queue.append("quest_jacklyn_town_wait")
    $del quest.mrsl_bot.flags["jacklyn_romance_on_hold"]
    $quest.mrsl_bot.advance("sleep")
    window auto
    return
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "What a night. Cockblocked by some mysterious busybody."
  "And all I wanted to do was get busy with my body..."
  "Oh, well."
  "Tomorrow, I will have to see if I can find some answers."
  $quest.mrsl_bot.advance("sleep")
  return

label quest_mrsl_bot_meeting_bus_stop:
  "Not yet. If I bail now, I may never get to experience that hot bot."
  return

label quest_mrsl_bot_meeting_right_path:
  "A walk in the park? I don't expect this training session to be."
  return

label quest_mrsl_bot_sleep:
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  "Ugh... not a very restful sleep..."
  "But no dreams about [mrsl] last night, at least."
  "Today, I ask her what the deal is."
  $quest.mrsl_bot.advance("confrontation")
  return

label quest_mrsl_bot_confrontation:
  show mrsl neutral with Dissolve(.5)
  mc "You never showed up last night."
  mrsl surprised "Excuse me?"
  mc "You know, for my private workout lesson? You never showed up."
  mrsl thinking "Oh, right. That."
  mrsl thinking "I'm afraid I cannot go through with the lesson, after all."
  mc "Why not?"
  mrsl eyeroll "I had... something else come up."
  show mrsl eyeroll at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Is someone blackmailing you?\"":
      show mrsl eyeroll at move_to(.5)
      mc "Is someone blackmailing you?"
      mrsl afraid "I don't know what you're talking about."
      mc "Isn't that why you didn't show up last night?"
      mrsl afraid "This is hardly an appropriate conversation right now."
      mc "I was just thinking that maybe I can help..."
      mrsl concerned "..."
      mrsl concerned "I really think it's best if you stay out of it."
      mc "I mean it. I heard that conversation last night."
    # "\"That's funny. I had {i}something come up{/}, too.\"":
    "\"That's funny. I had {i}something\ncome up{/}, too.\"":
      show mrsl eyeroll at move_to(.5)
      mc "That's funny. I had {i}something come up{/}, too."
      mc "And I was left all alone to deal with it."
      mrsl laughing "My, oh my, [mc]!"
      mrsl flirty "That sounds like {i}quite{/} the problem."
      mrsl flirty "But I'm not sure how I could have helped you?"
      mc "The workout would have done wonders."
      mc "It would surely have brought my adrenaline right back down."
      $mrsl.lust+=1
      mrsl laughing "Oh, but of course! Your... adrenaline."
      mc "Seriously, though. I heard that conversation last night."
    "\"Oh, I know.\"":
      show mrsl eyeroll at move_to(.5)
      mc "Oh, I know."
      mrsl skeptical "What do you know?"
      mc "Well, I thought you were still in the school last night..."
      # mc "...and I might have overheard you talking to someone in the homeroom."
      mc "...and I might have overheard you talking to someone in the homeroom.{space=-65}"
      mrsl surprised "Eavesdropping, [mc]? Really?"
      mc "Err, it was kind of an accident..."
      mrsl thinking "I'm sure it was."
      mc "Seriously, though. That conversation sounded really bad."
  mc "Who is this person, and why are they keeping tabs on you?"
  if renpy.showing("mrsl concerned"):
    mrsl angry "Look, I really don't know what you are talking about."
  else:
    mrsl angry "I really don't know what you are talking about."
  mrsl angry "But this topic needs to be dropped right now."
  mrsl sad "As I said, I unfortunately cannot go through with the lesson."
  mrsl sad "Now, get to class, will you?"
  window hide
  hide mrsl with Dissolve(.5)
  window auto
  "Damn. [mrsl] sounded super serious about this."
  "That person must have really gotten to her..."
  "..."
  "But the idea of forbidden fruit only makes the goal of tasting it that much sweeter."
  "Not if she's going to start avoiding me, though."
  "Hmm... I wonder if I can come up with a secret way for [mrsl] and me to communicate?"
  "And what better place to ask for help than from a reluctant, bratty party?"
  $quest.mrsl_bot.advance("help")
  return

label quest_mrsl_bot_help:
  show flora smile with Dissolve(.5)
  mc "[flora]! I need—"
  flora skeptical "Why do you always need something from me?"
  mc "What? That's not true."
  flora skeptical "Yes, it is."
  # mc "No, it's not! Sometimes I just need to see your beautiful, smiling face."
  mc "No, it's not! Sometimes I just need to see your beautiful, smiling face.{space=-35}"
  flora skeptical "..."
  mc "Ah. That's right."
  mc "You're almost never smiling."
  flora skeptical "Maybe not around you."
  flora skeptical "Maybe because you're always pestering me."
  mc "[flora]?! You wound me!"
  flora afraid "Oh, no!"
  show flora smile with dissolve2
  extend " Anyway."
  flora smile "So, what is it that you want?"
  show flora smile at move_to(.75)
  menu(side="left"):
    extend ""
    "\"I just told you.\"":
      show flora smile at move_to(.5)
      mc "I just told you."
      mc "I wanted to see your beautiful face."
      flora embarrassed "You're just doing it on purpose, aren't you?"
      mc "Doing what?!"
      flora embarrassed "Being annoying {i}and{/} weird!"
      mc "I'm sorry. I wasn't trying to be."
      mc "I know how rarely you actually smile, but it does makes me happy when you do it."
      mc "It's like a bubbly excitement in my chest."
      mc "Especially when you think no one is looking, and it lights up your whole face."
      flora afraid "..."
      flora afraid "Okay, this really is weird now."
      flora afraid "Go back to being annoying, will you?"
      mc "Fine, fine!"
      mc "What's the magic word?"
      $flora.love+=1
      flora smile "That's already better."
    "\"Fine. You got me.\"":
      show flora smile at move_to(.5)
      mc "Fine. You got me."
      mc "I do need something from you."
      flora laughing "Oh, wow! What a surprise!"
      mc "You don't have to be such a brat about it, okay?"
      mc "I do a lot for you, too."
      flora thinking "You do? Since when?"
      mc "Since, well..."
      mc "..."
      flora thinking "Go on. I'm waiting."
      mc "Err, just because I can't think of something off the top of my head right now, it doesn't mean it's not true!"
      $flora.love-=1
      flora laughing "I knew you couldn't name a single thing!"
      "Welp. I guess she does have a point. I don't do a whole lot for her."
      "But that's like eighty percent her fault, really."
      "..."
      "Okay, eight percent."
      mc "Whatever."
    "\"I want what anyone wants.\"":
      show flora smile at move_to(.5)
      mc "I want what anyone wants."
      mc "Money, fame, fortune. A hot wife. A fast car."
      flora laughing "Ah, of course! I definitely want a hot wife!"
      "Mmm... [flora] and another girl..."
      if quest.isabelle_haggis["flora_kissed"]:
        "Like [isabelle], for instance."
        "She's had a taste once. Does she want more?"
      "Damn, that's a nice thought."
      mc "And!"
      mc "To be abducted by ancient aliens and probed."
      flora thinking "What? Why would you say that?"
      flora thinking "That's not a thing that people want."
      mc "Really? Not even you?"
      $flora.lust+=1
      flora blush "Err, of course not!"
      mc "Huh."
  mc "Anyway, a shoebox."
  flora thinking "Come again?"
  mc "I need an empty shoebox."
  flora thinking "Is this some weird foot fetish thing?"
  mc "What? No, it's not!"
  mc "It's for, err... a project?"
  flora laughing "Of course it is."
  mc "Please, [flora]?"
  flora worried "I don't know, [mc]..."
  mc "Pretty please with a cherry on top?"
  flora laughing "Oh, my god! You're desperate!"
  flora blush "Fine. But I get to play your xCube for a month."
  "All for a lousy shoebox..."
  "This better be worth it."
  mc "Err, deal."
  flora blush "Great! I'll leave it for you outside your bedroom."
  if quest.maya_sauce["bedroom_taken_over"]:
    flora laughing "Oh, wait! You don't own a bedroom anymore!"
    flora laughing "I'll leave it outside your old room, I mean."
  flora laughing "Pleasure doing business with you!"
  mc "Likewise..."
  window hide
  if game.location in ("home_kitchen","home_bedroom"):
    show flora laughing at disappear_to_left
  else:
    show flora laughing at disappear_to_right
  pause 1.0
  $flora["at_none_this_scene"] = True
  $quest.mrsl_bot.advance("shoebox")
  return

label quest_mrsl_bot_shoebox_machete:
  $home_hall["machete_taken"] = True
  $mc.add_item("machete")
  return

label quest_mrsl_bot_shoebox(item1,item2):
  $mc.remove_item(item1)
  $mc.remove_item(item2)
  pause 0.25
  "It cuts like butter... or like cardboard."
  "..."
  "..."
  "Welp. It's not the straightest opening."
  "But it's beautiful and unique and will make the perfect message depository."
  window hide
  $process_event("items_combined", item1,item2, items_by_id["message_box"])
  $mc.add_item("message_box", silent=True)
  if not item1 == "shoebox":
    $mc.add_item(item1, silent=True)
  elif not item2 == "shoebox":
    $mc.add_item(item2, silent=True)
  pause 0.25
  window auto
  "Now, I just need a secret message to leave for [mrsl]..."
  $quest.mrsl_bot.advance("message_box")
  return

label quest_mrsl_bot_message_box_teachers_desk:
  "Ah, perfect! Some paper and pen."
  window hide
  $mc.add_item("paper")
  $mc.add_item("pen")
  pause 0.25
  window auto
  "Good old fashioned communication..."
  "No pixels, no instant gratification, no quick back and forth. "
  "This will take time, but it will also make the grind and the eventual prize that much better."
  "What to write, though?"
  $quest.mrsl_bot["some_paper_and_pen"] = True
  return

label quest_mrsl_bot_message_box_pen(item):
  "I honestly don't know what's so special about a pen."
  "I would much rather have a sword."
  return True

label quest_mrsl_bot_message_box(item1,item2):
  if quest.mrsl_bot == "message_box":
    $mc.remove_item(item1)
    $mc.remove_item(item2)
    pause 0.25
    "Hmm... let's see here..."
    "..."
    "{i}\"These buns are still in need of toasting.\"{/}"
    "{i}\"Please allow me to use your grill.\"{/}"
    window hide
    $process_event("items_combined", item1,item2, items_by_id["note_to_mrsl"])
    $mc.add_item("note_to_mrsl", silent=True)
    if item1 in ("pen","nurse_pen"):
      $mc.add_item(item1, silent=True)
    elif item2 in ("pen","nurse_pen"):
      $mc.add_item(item2, silent=True)
    pause 0.25
    window auto
    "There we go! Hopefully, that's subtle enough."
  else:
    $game.notify_modal(None,"Combine","Combining {color=#900}[item1]{/color} with {color=#900}[item2]{/color} is not only futile, it is dangerous.",10.0)
  return

label quest_mrsl_bot_message_box_teacher_desk(item):
  if item == "message_box":
    "It might be a little obvious, but sometimes the best ruse takes place right under the nose."
    "It's just an innocent looking suggestion box, after all."
    "Nothing scandalous to see here, folks!"
    window hide
    $school_homeroom["message_box"] = True
    $mc.remove_item("message_box")
  else:
    "As much as I'd love to show [mrsl] my [item.title_lower], she would probably not appreciate it."
    $quest.mrsl_bot.failed_item("message_box",item)
  return

label quest_mrsl_bot_message_box_message_box(item):
  if item == "note_to_mrsl":
    $mc.remove_item("note_to_mrsl")
    pause 0.25
    "Mmm... the paper fits into the slit so nicely..."
    "Maybe it's a sign of what's to come? One can only hope."
    "For now, all that's left to do is wait for a reply."
    $quest.mrsl_bot.advance("back_and_forth")
  else:
    "I can't desecrate the message box with my [item.title_lower]. It's too pedestrian."
    $quest.mrsl_bot.failed_item("note_to_mrsl",item)
  return

label quest_mrsl_bot_back_and_forth_upon_entering:
  "Okay, let's see if my message trap has caught any messages..."
  $quest.mrsl_bot["message_trap"] = True
  return

label quest_mrsl_bot_back_and_forth_note_from_mrsl:
  "I'll just stick my hand inside the slit..."
  "Oh, yeah, just like that—"
  "..."
  "Holy crap, there's actually something in here!"
  window hide
  $quest.mrsl_bot["note_from_mrsl_retrieved"] = True
  $mc.add_item("note_from_mrsl")
  return

label quest_mrsl_bot_back_and_forth_note_from_mrsl_pen_combine(item1,item2):
  if quest.mrsl_bot["note_from_mrsl_interacted_with"]:
    $mc.remove_item(item1)
    $mc.remove_item(item2)
    pause 0.25
    "How about a more direct approach this time?"
    "..."
    "{i}\"Art, gym — I do it all.\"{/}"
    "{i}\"I just need someone to really push me to the edge.\"{/}"
    window hide
    $process_event("items_combined", item1,item2, items_by_id["second_note_to_mrsl"])
    $mc.add_item("second_note_to_mrsl", silent=True)
    if item1 in ("pen","nurse_pen"):
      $mc.add_item(item1, silent=True)
    elif item2 in ("pen","nurse_pen"):
      $mc.add_item(item2, silent=True)
    pause 0.25
    window auto
  else:
    $game.notify_modal(None,"Combine","Combining {color=#900}[item1]{/color} with {color=#900}[item2]{/color} is not only futile, it is dangerous.",10.0)
  return

label quest_mrsl_bot_back_and_forth_message_box(item):
  if item == "second_note_to_mrsl":
    $mc.remove_item("second_note_to_mrsl", silent=True)
    $game.notify_modal(None,"Inventory","{image=items second_note_to_mrsl}{space=50}|Lost\n{color=#900}Second Note to\n[mrsl]{/}",5.0)
    pause 0.25
    "I'll just drop it in, and there we go!"
    "The opening advancement has been made and parried."
    "A delicate war of words."
    "Now, for the waiting game..."
    $quest.mrsl_bot["second_note_to_mrsl_delivered"] = True
  elif item == "third_note_to_mrsl":
    $mc.remove_item("third_note_to_mrsl", silent=True)
    $game.notify_modal(None,"Inventory","{image=items second_note_to_mrsl}{space=50}|Lost\n{color=#900}Third Note to\n[mrsl]{/}",5.0)
    pause 0.25
    "Boom! Another drop has been made."
    "I love the sneakiness of it all. It's so exciting."
    "..."
    "The wait for a reply sure isn't, though."
    $quest.mrsl_bot["third_note_to_mrsl_delivered"] = True
  elif item == "fourth_note_to_mrsl":
    $mc.remove_item("fourth_note_to_mrsl", silent=True)
    $game.notify_modal(None,"Inventory","{image=items second_note_to_mrsl}{space=50}|Lost\n{color=#900}Fourth Note to\n[mrsl]{/}",5.0)
    pause 0.25
    "Let's see how she answers that one..."
    "...and if she does it in a timely manner this time."
    $quest.mrsl_bot["fourth_note_to_mrsl_delivered"] = True
  elif item == "ball_of_yarn" and (quest.mrsl_bot["note_from_maya_interacted_with"] and not quest.mrsl_bot["ball_of_yarn_delivered"]):
    $mc.remove_item("ball_of_yarn")
    pause 0.25
    "I don't have sex toys or cocaine, but I do have a ball of yarn..."
    "The ball is in [maya]'s court, now. Literally."
    $quest.mrsl_bot["ball_of_yarn_delivered"] = True
  else:
    "As much as do I love a tight fit, I probably shouldn't put my [item.title_lower] in here."
    $quest.mrsl_bot.failed_item("further_notes_to_mrsl",item)
  return

label quest_mrsl_bot_back_and_forth_second_note_from_mrsl:
  "Let's see if [mrsl] has left me on read..."
  "..."
  "..."
  "Aha! There's something in here!"
  window hide
  $quest.mrsl_bot["second_note_from_mrsl_retrieved"] = True
  $mc.add_item("second_note_from_mrsl")
  return

label quest_mrsl_bot_back_and_forth_second_note_from_mrsl_pen_combine(item1,item2):
  if quest.mrsl_bot["second_note_from_mrsl_interacted_with"]:
    $mc.remove_item(item1)
    $mc.remove_item(item2)
    pause 0.25
    "If not a more direct approach, what else?"
    "..."
    "{i}\"I am really enjoying talking to you.\"{/}"
    "{i}\"You push me to be better.\"{/}"
    window hide
    $process_event("items_combined", item1,item2, items_by_id["third_note_to_mrsl"])
    $mc.add_item("third_note_to_mrsl", silent=True)
    if item1 in ("pen","nurse_pen"):
      $mc.add_item(item1, silent=True)
    elif item2 in ("pen","nurse_pen"):
      $mc.add_item(item2, silent=True)
    pause 0.25
    window auto
  else:
    $game.notify_modal(None,"Combine","Combining {color=#900}[item1]{/color} with {color=#900}[item2]{/color} is not only futile, it is dangerous.",10.0)
  return

label quest_mrsl_bot_back_and_forth_note_from_maxine:
  "Come on, letter fairy..."
  "..."
  "..."
  "Yes! She's still into it!"
  window hide
  $quest.mrsl_bot["note_from_maxine_retrieved"] = True
  $mc.add_item("note_from_maxine", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_maxine}{space=50}|Gained\n{color=#900}Third Note from\n[mrsl]{/}",5.0)
  return

label quest_mrsl_bot_back_and_forth_note_from_maya:
  "Hopefully, no outlandish notes from randoms today."
  "..."
  "..."
  "Uh-oh. What's this?"
  window hide
  $quest.mrsl_bot["note_from_maya_retrieved"] = True
  $mc.add_item("note_from_maya", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_maya}{space=50}|Gained\n{color=#900}Third Note from\n[mrsl]?{/}",5.0)
  return

label quest_mrsl_bot_back_and_forth_second_note_from_maya:
  "Come on... [mrsl], [mrsl], [mrsl]..."
  "..."
  "..."
  "Oh? There's another pink note in here."
  "And the ball of yarn is gone, too."
  "Could it be...?"
  window hide
  $quest.mrsl_bot["second_note_from_maya_retrieved"] = True
  $mc.add_item("second_note_from_maya")
  return

label quest_mrsl_bot_back_and_forth_third_note_from_mrsl:
  "My excitement for more from [mrsl] starts to dwindle, but hope dies last..."
  "..."
  "..."
  "Oh! It looks like her handwriting!"
  "Thank god!"
  window hide
  $quest.mrsl_bot["third_note_from_mrsl_retrieved"] = True
  $mc.add_item("third_note_from_mrsl", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_mrsl}{space=50}|Gained\n{color=#900}Third Note from\n[mrsl]{/}",5.0)
  return

label quest_mrsl_bot_back_and_forth_third_note_from_mrsl_pen_combine(item1,item2):
  if quest.mrsl_bot["third_note_from_mrsl_interacted_with"]:
    if item1 == "third_note_from_mrsl":
      $mc.remove_item(item1, silent=True)
      $game.notify_modal(None,"Inventory","{image=items note_from_mrsl}{space=50}|Lost\n{color=#900}Third Note from\n[mrsl]{/}",5.0)
    else:
      $mc.remove_item(item1)
    if item2 == "third_note_from_mrsl":
      $mc.remove_item(item2, silent=True)
      $game.notify_modal(None,"Inventory","{image=items note_from_mrsl}{space=50}|Lost\n{color=#900}Third Note from\n[mrsl]{/}",5.0)
    else:
      $mc.remove_item(item2)
    pause 0.25
    "Come on, [mc]! Think!"
    "..."
    "{i}\"Popular box for a popular guy.\"{/}"
    "{i}\"I'm not giving up hope just yet.\"{/}"
    window hide
    $process_event("items_combined", item1,item2, items_by_id["fourth_note_to_mrsl"])
    $mc.add_item("fourth_note_to_mrsl", silent=True)
    if item1 in ("pen","nurse_pen"):
      $mc.add_item(item1, silent=True)
    elif item2 in ("pen","nurse_pen"):
      $mc.add_item(item2, silent=True)
    pause 0.25
    window auto
  else:
    $game.notify_modal(None,"Combine","Combining {color=#900}[item1]{/color} with {color=#900}[item2]{/color} is not only futile, it is dangerous.",10.0)
  return

label quest_mrsl_bot_back_and_forth_fourth_note_from_mrsl:
  "Please, let there be something in here..."
  "..."
  "..."
  "Aha!"
  "..."
  "Oh? There are two somethings."
  window hide
  $quest.mrsl_bot["fourth_note_from_mrsl_retrieved"] = True
  $mc.add_item("fourth_note_from_mrsl", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_mrsl}{space=50}|Gained\n{color=#900}Fourth Note from\n[mrsl]{/}",5.0)
  $quest.mrsl_bot["note_from_flora_retrieved"] = True
  $mc.add_item("note_from_flora", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_flora}{space=50}|Gained\n{color=#900}Fifth Note from\n[mrsl]?{/}",5.0)
  return

label quest_mrsl_bot_help_2_electric_boogaloo:
  show maya bored with Dissolve(.5)
  mc "[maya]! I need your help!"
  maya skeptical "That's odd."
  mc "What is?"
  maya skeptical "I don't remember being added to the payroll as [mc]'s personal assistant."
  maya skeptical "Sorry, it must be an oversight."
  # maya confident "I would assist with that, but we just established that I'm not being paid."
  maya confident "I would assist with that, but we just established that I'm not being paid.{space=-65}"
  maya confident "And, oh, yeah!"
  show maya bored with dissolve2
  extend " I don't care."
  show maya bored at move_to(.25)
  menu(side="right"):
    extend ""
    "\"Well, you should care.\"":
      show maya bored at move_to(.5)
      mc "Well, you should care."
      maya cringe "Uh-oh. That's not going to end well."
      mc "What? You caring?"
      maya cringe "You telling me what to think or feel."
      maya dramatic "But yes, that, too. The day I start to care is the day the world ends."
      maya dramatic "Fiery flames and everything."
      mc "I knew it!"
      mc "Seriously, though, you should care because it's a chance to cause some chaos."
      $maya.love+=1
      maya sarcastic "Oh? Now you have my half-hearted attention."
      mc "I need your help creating a distraction."
    # "\"Right, of course. You don't care about anything.\"":
    "\"Right, of course. You don't\ncare about anything.\"":
      show maya bored at move_to(.5)
      mc "Right, of course. You don't care about anything."
      mc "You're so secretly edgy, aren't you?"
      maya cringe "Excuse me?! That's not true!"
      maya dramatic "It's not a secret at all. I dance along the edge, day in and day out."
      # maya dramatic "It's all I can do to feel anything through the searing numbness of life."
      maya dramatic "It's all I can do to feel anything through the searing numbness of life.{space=-20}"
      mc "Err, okay."
      mc "Now that you got that out of your system, can you please listen to what I need?"
      maya sarcastic "It's always your needs, isn't it, [mc]?"
      mc "Maybe one of these days I'll throw you a fishbone."
      $maya.lust+=1
      maya sarcastic "Always so magnanimous!"
      mc "So, I need your help creating a distraction."
    "\"Not even if I beg?\"":
      show maya bored at move_to(.5)
      mc "Not even if I beg?"
      maya cringe "{i}Especially{/} if you beg."
      maya cringe "The more you beg, the more care credits you lose."
      mc "Oh? I have care credits?"
      maya dramatic "Not anymore."
      mc "Please, oh, please—"
      $maya.lust-=1
      maya cringe "That's negative one billion now."
      mc "Err, I'll just tell you, anyway..."
      mc "I need your help creating a distraction."
  mc "And since you're, well, kind of good at getting attention, whether you want it or not..."
  maya bored "Just like right now, you mean?"
  mc "...I figured you would be the best person to ask."
  # mc "Plus, a bit of chaos would definitely liven this place up, don't you think?"
  mc "Plus, a bit of chaos would definitely liven this place up, don't you think?{space=-75}"
  maya bored "What kind of chaos are we talking about here?"
  mc "I figured I would leave that part up to you."
  mc "If you're in for some fun, that is."
  maya confident "Sure, why not? This place is a drag, otherwise."
  "Bingo! I knew that would get her!"
  maya confident "I'm going to need some red paint for this."
  mc "Red paint?"
  maya annoyed "Oh? Did I dream up you asking me for help?"
  maya annoyed "Silly little me! Too girl-brained to hold a thought for more than a few seconds."
  mc "Okay, okay! Red paint! Got it!"
  mc "Anything else?"
  maya eyeroll "Yes. A sacrificial virgin."
  maya smile_hands_in_pockets "Unless you'd like to volunteer?"
  mc "I'm not! And, err... I won't?"
  maya smile_hands_in_pockets "Very well. Meet me in the entrance hall when you have the paint."
  window hide
  hide maya with Dissolve(.5)
  window auto
  # "Oh, well. She's actually agreed to help, so I guess I won't question it..."
  "Oh, well. She's actually agreed to help, so I guess I won't question it...{space=-35}"
  "Paint it is."
  $quest.mrsl_bot.advance("paint")
  return

label quest_mrsl_bot_paint_upon_entering:
  "Surely, [jacklyn] won't mind if I take just one bucket, right?"
  "Especially if it means unleashing [maya]'s artistic expression."
  "..."
  "Whatever that might be..."
  $quest.mrsl_bot["just_one_bucket"] = True
  return

label quest_mrsl_bot_paint:
  "All right... paint, paint, paint..."
  "And [maya] said red, specifically."
  # "Hopefully, there's at least one bucket here. I would't want her to use my blood instead."
  "Hopefully, there's at least one bucket here. I would't want her to use{space=-5}\nmy blood instead."
  "..."
  "Aha! Found it!"
  $mc["focus"] = "mrsl_bot"
  window hide
  $mc.add_item("red_paint")
  $quest.mrsl_bot.advance("diversion")
  return

label quest_mrsl_bot_diversion_red_paint(item):
  "Red — the color of angry [jo] will be once [maya] uses this for her dastardly deeds."
  return True

label quest_mrsl_bot_diversion:
  show maya bored with Dissolve(.5)
  maya bored "Well?"
  mc "I have the paint right here."
  maya blush "Oh, wow! You did exactly as I asked!"
  maya blush "Shall I applaud you now or later?"
  mc "Err, later is fine."
  maya bored "Cool. Now, hand over the goods."
  window hide
  $mc.remove_item("red_paint")
  pause 0.25
  window auto
  mc "Where is everyone? The [guard]?"
  maya dramatic "Outside, looking for a pervert."
  mc "Excuse me?"
  maya dramatic "I told him I was flashed by a pervert who ran into the woods."
  mc "Huh. That's clever."
  maya sarcastic "The girl who cried pervert."
  mc "I bet that happens a lot, eh?"
  maya sarcastic "You have no idea."
  window hide
  show maya pentagram_drawing
  show black onlayer screens zorder 100
  with Dissolve(.5)
  $mc["focus"] = ""
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "She takes the paint from me and sets to work immediately."
  "Drawing thick red lines right there on the floor."
  "..."
  "Goddamn. How's that for a nice view?"
  "Her short skirt showing off her bare thighs and just a small tease of her pert ass..."
  "Mmmm..."
  mc "Err, what exactly are you making?"
  maya pentagram_drawing "Patience, unvirtuous one. You will see."
  # "She goes about her drawing with a meticulous care I rarely see from her."
  "She goes about her drawing with a meticulous care I rarely see from{space=-5}\nher."
  "Probably because of how much she doesn't give a shit most of the time."
  "But she genuinely seems into this."
  "...and I am very into watching her."
  window hide
  hide screen interface_hider
  show maya smile
  show black
  show black onlayer screens zorder 100
  with Dissolve(3.0)
  $unlock_replay("maya_diversion")
  $school_ground_floor["pentagram"] = True
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Finally, she connects her last lines and the whole picture comes together."
  "And of course it's something edgy."
  show black onlayer screens zorder 100
  pause 0.5
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  window auto
  $set_dialog_mode("")
  mc "Heh. Well done."
  maya smile "Do you like it?"
  mc "It's a genuine work of art."
  maya smile "I have many years of practice drawing ones just like it."
  "I have so many questions about that..."
  "But I don't have time right now."
  mc "So, do you think this will cause enough uproar?"
  maya neutral "It will certainly keep the honchos busy for a few days while looking for the culprit."
  maya neutral "Plus, cleaning it up, too."
  # maya smile_hands_in_pockets "I don't know what you want it for, but I'm looking forward to watching them panic about devil worship for a bit."
  maya smile_hands_in_pockets "I don't know what you want it for, but I'm looking forward to watching{space=-30}\nthem panic about devil worship for a bit."
  mc "Diabolical."
  maya eyeroll "Just bored."
  mc "What about the surveillance footage? Can't they just watch it and bust you?"
  maya thinking "That's true. If you'll excuse me, I need to go suck someone's dick and make it disappear."
  mc "Are you talking about the [guard]? That's... ew."
  maya flirty "I was talking about [maxine]."
  mc "What?"
  maya thinking "What?"
  mc "..."
  mc "Anyway, thanks for the help, [maya]."
  maya thinking "..."
  window hide
  show maya thinking at disappear_to_left
  pause 0.5
  window auto
  "She looks at me for a moment, and then shrugs."
  "Welp. I better get out of here too before I'm caught at the scene."
  $quest.mrsl_bot.advance("wait_2_electric_boogaloo")
  return

label quest_mrsl_bot_wait_2_electric_boogaloo:
  "Hopefully, [maya]'s little drawing keeps people talking and distracted for a few days..."
  "I guess we'll see what kind of commotion it stirs up."
  "Until then, nothing to do but wait some more."
  $quest.mrsl_bot["wait_some_more"] = True
  $school_ground_floor["exclusive"] = ""
  return

label quest_mrsl_bot_back_and_forth_fifth_note_from_mrsl:
  "Surely, I have waited long enough."
  "Let's see if my little diversion has come through..."
  "..."
  "..."
  "Yes! Jackpot!"
  window hide
  $quest.mrsl_bot["fifth_note_from_mrsl_retrieved"] = True
  $mc.add_item("fifth_note_from_mrsl", silent=True)
  $game.notify_modal(None,"Inventory","{image=items note_from_mrsl}{space=50}|Gained\n{color=#900}Fifth Note from\n[mrsl]{/}",5.0)
  return

label quest_mrsl_bot_meeting_2_electric_boogaloo:
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  if game.location != "school_gym":
    "It's getting late. [mrsl] told me to meet up with her in the gym."
    # "Time to get a full body workout with the hottest instructor ever, at long last!"
    "Time to get a full body workout with the hottest instructor ever,\nat long last!"
    if mc.owned_item("paper"):
      "I just have to put [maxine]'s priced paper back in the stack first."
      "Leaving with it will get me in so much trouble. She doesn't mess around."
    window hide
    if mc.owned_item("paper"):
      $mc.remove_item("paper")
      pause 0.25
    show black onlayer screens zorder 100 with Dissolve(.5)
    if game.location.id.startswith("school_"):
      $game.location = "school_gym"
      pause 1.0
    else:
      $game.location = "school_gym"
      pause 2.0
    hide black onlayer screens with Dissolve(.5)
    play music "school_theme" fadein 0.5
    window auto
  "It's crazy how much louder the smallest sounds seem when no one else is in here..."
  # "The sound of my thudding heart amplified with each echoing footstep I take."
  "The sound of my thudding heart amplified with each echoing footstep{space=-40}\nI take."
  window hide
  $mrsl["outfit_stamp"] = mrsl.outfit
  $mrsl.outfit = {"hairstyle":"mrsl_ponytail", "panties":"mrsl_workout_panties", "pants":"mrsl_leggings", "bra":"mrsl_workout_bra"}
  show mrsl laughing at appear_from_right
  pause 0.5
  window auto
  mrsl laughing "Well, well, well, if it isn't Mr. Determination himself!"
  mc "Heh! You know what they say about good things..."
  "Man, just look at her exposed skin, her hair up and showing off the curve of her neck."
  "She looks regal and sophisticated even in gym clothes."
  mrsl flirty "And what is it they say?"
  show mrsl flirty at move_to(.75)
  menu(side="left"):
    extend ""
    # "\"I don't know. I was hoping you could tell me.\"":
    "\"I don't know. I was hoping\nyou could tell me?\"":
      show mrsl flirty at move_to(.5)
      mc "I don't know. I was hoping you could tell me?"
      mrsl laughing "Oh, my! What a... playful... little thing you are."
      mc "I do know the one they say about all work and no play."
      mrsl confident "Is that so?"
      mc "Yes, and I think you have been working too hard."
      mrsl confident "You just might have a point there."
      $mrsl.lust+=1
      mrsl flirty "A nice, {i}hard{/} workout would loosen me right up."
      mc "I thought so, too."
      mrsl excited "Okay! First, we must stretch."
    "\"That they come to those who wait.\"":
      show mrsl flirty at move_to(.5)
      mc "That they come to those who wait."
      mrsl laughing "And you have been waiting a very long time, haven't you?"
      mc "Such a long time..."
      $mc.charisma+=1
      mc "So, I guess that means this will be extra good, right?"
      mrsl excited "I guess we're about to find out!"
      mrsl excited "First, we must stretch."
    "\"I'm not surprised you don't know.\"":
      show mrsl flirty at move_to(.5)
      mc "I'm not surprised you don't know."
      mrsl flirty "Oh? And why is that?"
      mc "Because you probably never have to wait for anything."
      mc "I bet people are begging to give you what you want."
      mrsl laughing "Oh, I don't know about that!"
      "She plays coy, but I can see a flash of amusement behind her eyes."
      mc "I would definitely give you whatever you want."
      mrsl confident "And what is it {i}you{/} want, [mc]?"
      mc "To have this lesson with you."
      mrsl excited "Very well. Let's do it, then!"
      mrsl excited "First, we must stretch."
  # mrsl excited "It's very important to make sure you're plenty {i}limber{/} and, well... loose."
  mrsl excited "It's very important to make sure you're plenty {i}limber{/} and, well... loose.{space=-45}"
  # "The way those words leave her mouth, all slow and sensual, automatically accelerates my heart rate."
  "The way those words leave her mouth, all slow and sensual, automatically{space=-120}\naccelerates my heart rate."
  mc "Very important, indeed..."
  mrsl excited "Come a little closer and help me out, will you?"
  window hide
  show mrsl bent_forward
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Just as she says this, she bends forward, reaching down to touch her toes."
  "Goddamn! It looks like she's still just as flexible as in that video."
  mc "That's, err... very impressive."
  mrsl bent_forward "Oh, you haven't seen anything yet!"
  "Mmm... I really want to see it all..."
  mrsl bent_forward "Can you help me stretch my thighs?"
  mrsl bent_forward "Just put your hands on them and apply some pressure, okay?"
  mc "O-okay."
  window hide
  pause 0.125
  show mrsl bent_forward mc_help with Dissolve(.5)
  pause 0.125
  window auto
  "This is exhilarating."
  # "Coming this close, smelling her fiery scent and placing my hands on the backs of her thighs while she continues to bend forward."
  "Coming this close, smelling her fiery scent and placing my hands on{space=-5}\nthe backs of her thighs while she continues to bend forward."
  "Her round, firm ass resting right there in my face."
  mrsl bent_forward mc_help "That's it, [mc]!"
  mrsl bent_forward mc_help "Mmm! That feels good!"
  "God, it really does..."
  "The feel of her muscular, toned legs shifting beneath my fingers..."
  "Her ass so close I could just slide my hand up to have a touch..."
  mrsl bent_forward mc_help "Okay! It's your turn, now."
  mrsl bent_forward mc_help "Lie back, will you?"
  window hide
  show mrsl leg_stretch
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  # "With my back flat on the floor, she towers over me, like some sort of sex goddess above her devout follower."
  "With my back flat on the floor, she towers over me, like some sort of{space=-15}\nsex goddess above her devout follower."
  "And then she moves in, pressing one of my legs back."
  "I have never been particularly flexible, but her presence seems to bolster me, somehow."
  "A smile plays on her lips as her crimson nails dig into the skin of my leg."
  "Despite my best efforts, a groan of pleasure escapes me."
  "It's like her hands have a magical current of electrical discharge into my body, and my mind starts to wander."
  "And my dick immediately stiffens in my shorts."
  mrsl leg_stretch "That's better already."
  mrsl leg_stretch "You're very tight. Too much strain in that young body of yours."
  mc "Err, right..."
  mrsl leg_stretch "That's exactly what this is for, though!"
  mrsl leg_stretch "Are you feeling better?"
  mc "Much better..."
  mrsl leg_stretch "Perfect! Up you get, then."
  window hide
  show mrsl excited
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Oh, boy! It's finally time!"
  "All my hard work has led me to this workout."
  # "But not just any workout. A workout with the hottest teacher in school."
  "But not just any workout. A workout with the hottest teacher in school.{space=-60}"
  mrsl excited "Ready?"
  mc "I have never been more ready."
  mrsl excited "Okay! Let's hot your bottom!"
  window hide
  show mrsl workout knee_up_jumps
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "She takes me through a much more grueling regimen than I expected."
  "But watching her do all the moves makes all the sweat and pain worthwhile."
  "To see [mrsl] there across from me, boobs bouncing in her tight, sweaty top..."
  "Beads of sweat clinging to her heaving chest."
  show mrsl workout jumping_jacks with dissolve2
  mrsl workout jumping_jacks "You're doing great, [mc]!"
  mrsl workout jumping_jacks "So strong! And determined!"
  mc "Th-thanks!"
  show mrsl workout side_lunges with dissolve2
  "The word wheezes out past my lips as her movements turn into a blur."
  "She moves with an otherworldly grace that leaves my mouth open and watering."
  "Like some sort of goddess, her body moves in impossible ways."
  show mrsl workout leg_raise with dissolve2
  "She bends in ways I never knew a person could bend."
  "She's in complete control of her own body."
  "Moving like a lithe, graceful dancer. Sensual, sexy, dangerous."
  show mrsl workout squats with dissolve2
  "Her powerful ass and thighs leave nothing to the imagination in that skin-tight outfit."
  "It makes my dick ache in my shorts even as I sweat from the routine."
  "Sweat from her fiery eyes watching me just as I watch her."
  show mrsl workout downward_facing_dog with dissolve2
  mrsl workout downward_facing_dog "One last push, [mc]! We're nearly done!"
  mrsl workout downward_facing_dog "You're going to have the hottest bottom, all right!"
  "I had no idea a body could be as flexible as hers..."
  "Never in my decades of porn abuse have I ever come across something so incredible."
  "Her body truly is a work of art, and a dizzying passion overcomes me as I marvel at her divine perfection."
  window hide
  show mrsl excited
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  hide black onlayer screens with Dissolve(.5)
  window auto
  "Finally, just when I think I can't take anymore, we stop."
  mrsl excited "Well? What did you think?"
  mc "That was... {i}huff...{/} quite the... {i}puff...{/} workout..."
  "She just laughs and beams..."
  show mrsl concerned with dissolve2
  extend " then turns serious."
  mrsl concerned "Lie back on the mat."
  mrsl concerned "I will massage out your thighs so that they don't cramp."
  mc "Err, okay."
  window hide
  hide screen interface_hider
  # show mrsl clothed_titjob anticipation
  show black
  show black onlayer screens zorder 100
  with Dissolve(.5)
  pause 0.5
  show black onlayer screens zorder 4
  $set_dialog_mode("default_no_bg")
  "Oh, man. Lying down and relaxing my muscles is such a blessing after that murderous workout..."
  "Eyes closed, my ragged breath slowly returns to normal."
  # "But my mind keeps swimming in a cocktail of endorphins, dopamine, and perhaps something else entirely."
  "But my mind keeps swimming in a cocktail of endorphins, dopamine,{space=-15}\nand perhaps something else entirely."
  "The world keeps spinning behind my eyes."
  "They say that workout is a powerful drug, but this is nothing like I've ever felt before."
  "Then, [mrsl]'s hands start to massage my thigh."
  "Her fingers dig in. So very close to my groin."
  mrsl concerned "Does that feel good?"
  mc "Mmm... yes..."
  mc "It feels so good... and relaxing..."
  "It puts my mind and body at ease."
  "A calm, post-workout fog settles over me."
  "The world moves slowly, drifts past me. I'm drifting."
  "My shorts roll down my legs."
  "My semi-erect dick springs free, hardening under her gentle touch."
  mc "Wha—"
  show black onlayer screens zorder 100
  pause 0.5
  show mrsl clothed_titjob anticipation: ## This gives the image both the dissolve (transition) and vpunch (transform) effects
    block:
      linear 0.05 yoffset 15
      linear 0.05 yoffset -15
      repeat 3
    linear 0.05 yoffset 0
  pause 0.0
  hide black
  hide black onlayer screens
  show screen interface_hider
  with Dissolve(.5)
  hide mrsl
  show mrsl clothed_titjob anticipation ## This is just to avoid the screen shake if the player is skipping
  window auto
  $set_dialog_mode("")
  mc "Oh, f-fuck!"
  # "In a haze of blurry passion, she lifts her top, taking my dick between her breasts."
  "In a haze of blurry passion, she lifts her top, taking my dick between{space=-10}\nher breasts."
  "They envelop me in a valley of silky bliss, softer than anything I've ever experienced."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob1 with Dissolve(.5)
  pause 0.25
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  # "With her hands on the sides of her boobs, she pushes them together around my cock."
  "With her hands on the sides of her boobs, she pushes them together{space=-20}\naround my cock."
  # "They sweat from the workout mixes with my sticky precum, creating a perfect lubrication."
  "The sweat from the workout mixes with my sticky precum, creating{space=-5}\na perfect lubrication."
  "The sensation sends a new hit of euphoria and released tension kindling through me."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  mc "Oooooooh!"
  mc "That really... does feel... so gooood!"
  "The last word leaves me in a desperate groan."
  "The sensation almost knocks me out."
  "My head keeps spinning and my eyes water."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  # "It feels like a dream when she starts to slide her boobs up and down my shaft."
  "It feels like a dream when she starts to slide her boobs up and down{space=-15}\nmy shaft."
  "Pushing them together even more to create the feeling of a vagina."
  "But it feels nothing like one."
  # "It's almost like fucking warm jelly... warm perfect jelly that fits perfectly around me."
  "It's almost like fucking warm jelly... warm perfect jelly that fits perfectly{space=-70}\naround me."
  window hide
  pause 0.125
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.0625
  show mrsl clothed_titjob titjob2 with Dissolve(.125)
  show mrsl clothed_titjob titjob3 with Dissolve(.25)
  show mrsl clothed_titjob titjob4 with Dissolve(.125)
  show mrsl clothed_titjob titjob1 with Dissolve(.25)
  pause 0.25
  window auto
  mc "God... y-yes..."
  mc "This is just... like my dream..."
  mrsl clothed_titjob titjob1 "Hmm?"
  mc "Err, nothing! Don't stop!"
  window hide
  pause 0.125
  show mrsl clothed_titjob lick1 with Dissolve(.5)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.1
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.25
  window auto
  "Thankfully, she does the exact opposite of stopping."
  "She twirls her tongue around the tip of my dick, teasing the hole with her tongue."
  "Tasting the clear sticky fluid that oozes out."
  "She licks and sucks it up as it comes out, slurping loudly."
  "Sounds I've never heard before. Not even in porn."
  window hide
  pause 0.125
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob lick3 with Dissolve(.2)
  pause 0.05
  show mrsl clothed_titjob lick4 with Dissolve(.1)
  show mrsl clothed_titjob lick1 with Dissolve(.2)
  pause 0.25
  window auto
  "Sweat rolls down my brow, burning into my eyes."
  "This feels too good to be true."
  "It's like some sort of perfect wet dream, but one where you don't wake up just before finishing."
  "It almost feels like a compulsion. I need to finish."
  "I need to finish before I wake up!"
  window hide
  pause 0.125
  show mrsl clothed_titjob lick2 with Dissolve(.1)
  show mrsl clothed_titjob orgasm with Move((0,15), (0,-15), .075, bounce=True, repeat=True, delay=.275)
  pause 0.25
  window auto
  mc "Ohhhhhhh!"
  "After everything... the workout... this..."
  "It sends me into outer fucking space."
  "Stars explode across my eyes, and I explode in her mouth."
  "Shooting my cum straight onto her tongue."
  "She tastes my hot, sticky semen. Savors it."
  show mrsl clothed_titjob titjob3 with dissolve2
  "Swirls it around in her mouth, before finally swallowing."
  "Careful to drink every last drop."
  mc "Oh... my god..."
  mc "That was... amazing..."
  window hide
  show black onlayer screens zorder 100 with Dissolve(3.0)
  $unlock_replay("mrsl_workout")
  $game.advance(hours=2)
  $game.location = "home_hall" if quest.maya_sauce["bedroom_taken_over"] else "home_bedroom"
  pause 2.0
  hide mrsl
  hide black onlayer screens
  with Dissolve(.5)
  play music "home_theme" fadein 0.5
  if "event_player_force_go_home_at_night" in game.events_queue:
    $game.events_queue.remove("event_player_force_go_home_at_night")
  if "event_show_time_passed_screen" in game.events_queue:
    $game.events_queue.remove("event_show_time_passed_screen")
  call screen time_passed
  pause 0.5
  window auto
  "Hours later, and I still can't get [mrsl] out of my head."
  "The scene keeps repeating as soon as I close my eyes."
  "Her boobs around my dick."
  if quest.jo_day.actually_finished:
    "The technique... it's nothing like I've ever experienced before."
  "And where did she learn that thing with her tongue?"
  "Even though I know it happened, I can't help but doubt my senses."
  "Maybe I did dream it? Surely, sex can't feel this good?"
  "The sensation of her perfect breasts gives me shivers even now."
  "..."
  "It already feels like an obsession."
  "The sexual version of [flora]'s forbidden cake."
  "I just know one thing..."
  "I need more."
  window hide
  $quest.mrsl_bot.finish()
  $mrsl.outfit = mrsl["outfit_stamp"]
  return
