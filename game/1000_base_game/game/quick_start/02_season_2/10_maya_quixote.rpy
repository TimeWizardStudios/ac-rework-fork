init python:
  quick_starts_by_order["season_2"].append(["Don Juan Quixote","quest_maya_quixote_quick_start",True,"quick_start maya_quixote"])


label quest_maya_quixote_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables (for the introduction)
  call intro_variables(input=True)
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=False)
  call quest_the_key_variables
  call quest_isabelle_tour_variables(choices=True)
  call quest_lindsey_nurse_variables(choices=True, exclusive="Go get the [nurse]")
  call quest_kate_blowjob_dream_variables(choices=False)
  call quest_act_one_variables

  ## Create a backstory (for the introduction)
  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 1)
  call quest_lindsey_book_variables
  call quest_jacklyn_art_focus_variables
  call quest_flora_cooking_chilli_variables
  call quest_flora_jacklyn_introduction_variables(reversible=True)
  if quest.isabelle_over_kate.finished:
    call quest_jacklyn_broken_fuse_variables(choices=False, reversible=True)
  call quest_kate_search_for_nurse_variables(choices=False)
  call quest_isabelle_stolen_variables(choices=False)
  call quest_berb_fight_variables
  if quest.isabelle_over_kate.finished:
    call quest_isabelle_buried_variables(reversible=True)
  call quest_poolside_story_variables
  call quest_clubroom_access_variables
  call quest_spinach_seek_variables
  call quest_flora_bonsai_variables(choices=False)
  if quest.isabelle_over_kate.finished:
    call quest_lindsey_wrong_variables(choices=False, reversible=True)
  call quest_maxine_eggs_variables
  call quest_mrsl_table_variables
  if quest.isabelle_over_kate.finished:
    call quest_lindsey_piano_variables(reversible=True)
    call quest_lindsey_motive_variables(reversible=True)
  if quest.kate_over_isabelle.finished:
    call quest_isabelle_piano_variables
    call quest_nurse_photogenic_variables
    call quest_flora_handcuffs_variables
    call quest_kate_trick_variables
    call quest_kate_stepping_variables(choices=True)
  elif quest.isabelle_over_kate.finished:
    call quest_isabelle_red_variables
    call quest_nurse_venting_variables
    call quest_kate_wicked_variables
    call quest_isabelle_dethroning_variables(choices=True)
  call quest_jo_washed_variables

  ## Create a backstory (for season 1)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 2)
  call quest_fall_in_newfall_variables
  call quest_maya_witch_variables(choices=False)
  call quest_maya_spell_variables

  python:
    quest.fall_in_newfall["finished_today"] = False

  ## Create a backstory (for season 2)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    flora.love = 8 ## for the "It's okay. I'm sure I can fix it." dialogue choice in phase_3_xCube
                   ## and the "Very well. You're in." dialogue line in phase_7_power_outage
    mc.intellect = 6 ## for the "You just have to twist the handle a bit." dialogue line in phase_4_shower
    maya.love = 8 ## for the "Do you use sarcasm to mask your true feelings?" dialogue choice in phase_9_board_game
                  ## and the "Did you have to do that a lot?" dialogue choice in phase_9_board_game
    flora.lust = 6 ## for the "Nope. The innkeep grabs you and kisses you hard." dialogue choice in phase_9_board_game
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)
    if mc.owned_item("compromising_photo") and mc.inv.index(["compromising_photo", mc.owned_item_count("compromising_photo")]) > 1:
        mc.remove_item("compromising_photo", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 19
    game.location = "home_hall"
    game.quest_guide = "maya_quixote"

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"
  else:
    stop music

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  call quest_maya_quixote_start
  jump main_loop
