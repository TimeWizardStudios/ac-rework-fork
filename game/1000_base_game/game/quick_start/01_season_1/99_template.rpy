init python:
  pass ## quick_starts_by_order["season_1"].append(["Template","quest_template_quick_start",True,"quick_start template"])


label quest_template_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables (for the introduction)
  call intro_variables(input=True) ## "In the introduction, a mysterious texter asked your name in exchange for life-changing information. You told them it's..."
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=False) ## "In 'Day 1, Take 2', Mrs. L introduced Isabelle, the new student from London, before asking someone to show her around. You decided to..."
  call quest_the_key_variables
  call quest_isabelle_tour_variables(choices=True) ## "In 'Tour de School', Isabelle confronted Kate about her bad behavior. You decided to side with..."
  call quest_lindsey_nurse_variables(choices=True, exclusive="Go get the [nurse]") ## "Still in 'Tour de School', Lindsey accidentally ran into you while doing laps in the gym. You decided to..."
  call quest_kate_blowjob_dream_variables(choices=False) ## "In 'Make a Wish and Blow', Kate got on her knees to apologize for how she had treated you the past few years. You decided to..."
  call quest_act_one_variables

  ## Create a backstory (for the introduction)
  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 1) ## Placeholder ## Template ##
# call quest_lindsey_book_variables
# call quest_jacklyn_art_focus_variables
# call quest_flora_cooking_chilli_variables
# call quest_isabelle_haggis_variables(choices=False) ## "In 'No True Scotswoman', Mrs. L locked you in the homeroom with Isabelle and Flora. The three of you decided the reward should go to..."
# call quest_flora_jacklyn_introduction_variables
# call quest_jacklyn_broken_fuse_variables(choices=True) ## "In 'A Short Fuse', Jacklyn told you she needed a nude model for some of the senior classes. You recommended..."
# call quest_kate_search_for_nurse_variables(choices=False) ## "In 'Search & Rescue', the Nurse hid from an appointment with Kate, before getting found out. You decided to..."
# call quest_mrsl_HOT_variables
# call quest_isabelle_stolen_variables(choices=False) ## "In 'Stolen Hearts', Isabelle revealed someone broke into her locker and stole a box of chocolates. You told her the thief is..."
# call quest_berb_fight_variables
# call quest_jo_potted_variables
# call quest_isabelle_buried_variables
# call quest_poolside_story_variables
# call quest_clubroom_access_variables
# call quest_spinach_seek_variables
# call quest_flora_bonsai_variables(choices=False) ## "In 'Tiny Thirsty Tree', Flora knocked on your door late at night claiming she needed more cake. You decided to..."
# call quest_flora_bonsai_variables(choices=False) ## "Still in 'Tiny Thirsty Tree', a tree dragged Flora to the Nurse's office, before attempting to have its way with her. You decided to..."
# call quest_jacklyn_statement_variables(choices=True) ## "In 'The Statement', Flora hid in your closet to spy on you and Jacklyn, before getting caught and storming out of the room. You decided to..."
# call quest_lindsey_wrong_variables(choices=False) ## "In 'Nothing Wrong With Me', Maxine claimed that the disturbance in the magnetic field caused by the saucer-eyed doll is messing with Lindsey's balance. You decided to..."
# call quest_maxine_wine_variables
# call quest_maxine_eggs_variables
# call quest_mrsl_table_variables
# call quest_maxine_lines_variables(choices=False) ## "In 'The Ley of the Land', Maxine claimed that the disturbance in the magnetic field caused by the saucer-eyed doll is messing with Lindsey's balance. You decided to..."
# call quest_kate_fate_variables(choices=False) ## "In 'Twisted Fate', Kate tied you up and kept her dirty socks in your mouth for a whole night, before Isabelle managed to set you free. You decided to..."
# call quest_isabelle_locker_variables(choices=False) ## "In 'Gathering Storm', Isabelle angrily proposed sending Maxine her cat's bloody head. You decided to..."
# call quest_kate_desire_variables(choices=False) ## "In 'Twisted Desire', you confronted Kate about her blackmailing the Nurse, before proposing a small property wager. Should you win the ongoing challenge, she would have to..."
# call quest_maxine_hook_variables
# call quest_jacklyn_sweets_variables(choices=False) ## "In 'King of Sweets', Jacklyn was left mesmerized by the centerpiece reveal in your 'King of Sweets' piece. Following her reaction, you decided to ask her to..."
# call quest_flora_squid_variables
# call quest_lindsey_piano_variables
# call quest_isabelle_hurricane_variables
# call quest_lindsey_motive_variables
# call quest_jo_day_variables
# if quest.kate_over_isabelle.finished:
#   call quest_isabelle_piano_variables
#   call quest_nurse_photogenic_variables
#   call quest_flora_handcuffs_variables
#   call quest_kate_trick_variables
#   call quest_kate_stepping_variables(choices=True) ## "In 'Stepping on the Rose', Lindsey climbed onto the edge of the school roof, seemingly in a complete trance. You decided..."
# elif quest.isabelle_over_kate.finished:
#   call quest_isabelle_red_variables
#   call quest_nurse_venting_variables
#   call quest_kate_wicked_variables
#   call quest_isabelle_dethroning_variables(choices=True) ## "In 'Dethroning the Queen', Lindsey climbed onto the edge of the school roof, seemingly in a complete trance. You decided..."
# call quest_jo_washed_variables

  ## Create a backstory (for season 1)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)
    if mc.owned_item("compromising_photo") and mc.inv.index(["compromising_photo", mc.owned_item_count("compromising_photo")]) > 1: ## Placeholder ## Template ##
        mc.remove_item("compromising_photo", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 7 ## Placeholder ## Template ##
    game.location = "home_bedroom" ## Placeholder ## Template ##
    game.quest_guide = "template"

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  call quest_template_start
  jump main_loop
