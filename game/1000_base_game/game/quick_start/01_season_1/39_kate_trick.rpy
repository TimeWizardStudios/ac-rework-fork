init python:
  quick_starts_by_order["season_1"].append(["The Dog Trick","quest_kate_trick_quick_start",True,"quick_start kate_trick"])


label quest_kate_trick_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables (for the introduction)
  call intro_variables(input=True)
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=False)
  call quest_the_key_variables
  call quest_isabelle_tour_variables(choices=True, exclusive="[kate]")
  call quest_lindsey_nurse_variables(choices=True)
  call quest_kate_blowjob_dream_variables(choices=False)
  call quest_act_one_variables

  ## Create a backstory (for the introduction)
  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 1)
  call quest_isabelle_stolen_variables(choices=False)
  call quest_clubroom_access_variables
  call quest_isabelle_piano_variables
  call quest_nurse_photogenic_variables
  call quest_flora_handcuffs_variables

  python:
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn",5, silent=True)
    if not school_nurse_room["got_pin"]:
      school_nurse_room["got_pin"] = True
      mc.add_item("safety_pin", silent=True)
    if not mc.owned_item("high_tech_lockpick"):
      mc.remove_item("ball_of_yarn", silent=True)
      mc.remove_item("safety_pin", silent=True)
      mc.add_item("high_tech_lockpick", silent=True)

  ## Create a backstory (for season 1)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    mc.lust = 4 ## for the "We'll have her leashed and potty trained in no time." dialogue choice in phase_0_not_started
    kate.lust = 4 ## for the "That you need to bow before the queen." dialogue choice in phase_0_not_started
    mc.intellect = 4 ## for the "I've been trying to take gym classes, but the gym seems to be off-limits." dialogue choice in phase_20_jo
    mc.money = 50 ## for the "Pay up" dialogue choice in phase_50_flora
    nurse.love = 5 ## for the "Just a moment, I'm not quite finished yet." dialogue choice in phase_80_workout
    mc.strength = 8 ## for the "Flex" dialogue choice in phase_90_report
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)
    if mc.owned_item("compromising_photo") and mc.inv.index(["compromising_photo", mc.owned_item_count("compromising_photo")]) > 1:
        mc.remove_item("compromising_photo", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 14
    game.location = "school_first_hall"
    game.quest_guide = "kate_trick"

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
# call quest_kate_trick_start ## Taken care of automatically by the 'on_location_changed' event in \quests\season_1\kate_trick.rpy
  jump main_loop
