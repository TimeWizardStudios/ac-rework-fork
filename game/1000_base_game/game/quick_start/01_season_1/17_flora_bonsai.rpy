init python:
  quick_starts_by_order["season_1"].append(["Tiny Thirsty Tree","quest_flora_bonsai_quick_start",True,"quick_start flora_bonsai"])


label quest_flora_bonsai_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables (for the introduction)
  call intro_variables(input=True)
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=False)
  call quest_the_key_variables
  call quest_isabelle_tour_variables(choices=True)
  call quest_lindsey_nurse_variables(choices=True)
  call quest_kate_blowjob_dream_variables(choices=False)
  call quest_act_one_variables

  ## Create a backstory (for the introduction)
  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 1)
  call quest_flora_cooking_chilli_variables
  call quest_flora_jacklyn_introduction_variables
  call quest_kate_search_for_nurse_variables(choices=False)
  call quest_isabelle_stolen_variables(choices=False)
  call quest_clubroom_access_variables
  call quest_spinach_seek_variables

  python:
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
    if not mc.owned_item("spray_water"):
      mc.remove_item("water_bottle", silent=True)
      mc.remove_item("spray_cap", silent=True)
      mc.add_item("spray_water", silent=True)
    mc.remove_item("spray_water", silent=True)
    mc.add_item("spray_empty_bottle", silent=True)
    mc.remove_item("spray_empty_bottle", silent=True)
    mc.add_item("spray_strawberry_juice", silent=True)
    mc.remove_item("spray_strawberry_juice", silent=True)
    mc.add_item("spray_empty_bottle", silent=True)

  ## Create a backstory (for season 1)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    flora.lust = 4
    flora.love = 4
    mc.money = 2
    mc.intellect = 3
    mc.charisma = 5
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 10
    game.location = "school_nurse_room"
    game.quest_guide = "flora_bonsai"

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  call quest_flora_bonsai_spaghetti
  jump main_loop
