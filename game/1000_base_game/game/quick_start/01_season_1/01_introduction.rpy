init python:
  quick_starts_by_order["season_1"].append(["Introduction","intro_quick_start",True,"quick_start intro"])


label intro_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables
  python:
    backstory = []
    skip_to = None

    mc.add_phone_contact("jo", silent=True)
    mc.add_phone_contact("flora", silent=True)
    mc.add_phone_contact("hidden_number", silent=True)

  ## Create a backstory
  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = True
    game.hour = 7
    game.location = "home_bedroom"
    game.quest_guide = "smash_or_pass"

  play music "<loop 30.2279>intro"

  ## Render new scene
  scene intro bg
  show screen hud
  show screen skip_to_target
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  call intro_start
  jump main_loop
