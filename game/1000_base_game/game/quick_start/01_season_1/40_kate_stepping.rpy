init python:
  quick_starts_by_order["season_1"].append(["{k=-2}Stepping on the Rose{/}","quest_kate_stepping_quick_start",True,"quick_start kate_stepping"])


label quest_kate_stepping_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables (for the introduction)
  call intro_variables(input=True)
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=False)
  call quest_the_key_variables
  call quest_isabelle_tour_variables(choices=True, exclusive="[kate]")
  call quest_lindsey_nurse_variables(choices=True, exclusive="Go get the [nurse]")
  call quest_kate_blowjob_dream_variables(choices=False)
  call quest_act_one_variables

  ## Create a backstory (for the introduction)
  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 1)
  call quest_lindsey_book_variables
  call quest_isabelle_stolen_variables(choices=False)
  call quest_berb_fight_variables
  call quest_clubroom_access_variables
  call quest_maxine_eggs_variables
  call quest_isabelle_piano_variables
  call quest_nurse_photogenic_variables
  call quest_flora_handcuffs_variables
  call quest_kate_trick_variables

  ## Create a backstory (for season 1)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    mc.strength = 6 ## for the "Allow me to clear a path for you." dialogue choice in phase_0_not_started  AND  the "{i}Your{/} hand is on the doorknob..." dialogue choice in phase_10_magazine
    mc.intellect = 5 ## for the "You absolutely shouldn't, but this isn't my opinion." dialogue choice in phase_0_not_started  AND  the "Could you help me find a good book?" dialogue choice in phase_60_isabelle
    mc.lust = 6 ## for the "A slap in the face, please." dialogue choice in phase_0_not_started  AND  the "I want to fuck [isabelle] too." dialogue choice in phase_30_gym
    kate.lust = 8 ## for the "I want you as my mistress." dialogue choice in phase_30_gym
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)
    if mc.owned_item("compromising_photo") and mc.inv.index(["compromising_photo", mc.owned_item_count("compromising_photo")]) > 1:
        mc.remove_item("compromising_photo", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 8
    game.location = "school_homeroom"
    game.quest_guide = "kate_stepping"

    game.day+=14

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  call quest_kate_stepping_start
  jump main_loop
