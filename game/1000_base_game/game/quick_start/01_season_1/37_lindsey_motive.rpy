init python:
  quick_starts_by_order["season_1"].append(["A Beautiful Motive","quest_lindsey_motive_quick_start",True,"quick_start lindsey_motive"])


label quest_lindsey_motive_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables (for the introduction)
  call intro_variables(input=True)
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=False)
  call quest_the_key_variables
  call quest_isabelle_tour_variables(choices=True, exclusive="[isabelle]")
  call quest_lindsey_nurse_variables(choices=True, exclusive="Go get the [nurse]")
  call quest_kate_blowjob_dream_variables(choices=False)
  call quest_act_one_variables

  ## Create a backstory (for the introduction)
  while len(backstory) > 0:
    call screen backstory_creation

  ## Set prerequisite variables (for season 1)
  call quest_lindsey_book_variables
  call quest_jacklyn_art_focus_variables
  call quest_flora_jacklyn_introduction_variables
  call quest_jacklyn_broken_fuse_variables(choices=True)
  call quest_isabelle_stolen_variables(choices=False)
  call quest_berb_fight_variables
  call quest_isabelle_buried_variables
  call quest_clubroom_access_variables
  call quest_lindsey_wrong_variables(choices=True)
  call quest_maxine_eggs_variables
  call quest_lindsey_piano_variables

  python:
    quest.lindsey_nurse["carried"] = True
    quest.lindsey_wrong["swooped_in"] = True
    quest.lindsey_wrong["fetched"] = True

  ## Create a backstory (for season 1)
  if game.skipped_backstory_choices:
    $skip_backstory_choices()

  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    mc.charisma = 5 ## for the "I drowned it in the aquarium." dialogue choice in phase_90_date_lindsey
    mc.lust = 8 ## for the "Show me your boobs." and "Take your skirt and panties off." dialogue choices in phase_90_date_lindsey, and the "I think it's time for you to undress, [mrsl]." dialogue choice in phase_91_date_mrsl
    mrsl.lust = 5 ## for the "Can you lift your leg up a bit?" dialogue choice in phase_91_date_mrsl
    mc.intellect = 5 ## for the "The windows act as a greenhouse for the hall upstairs." dialogue choice in phase_12_keys
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 11
    game.quest_guide = "lindsey_motive" ## Had to swap these two lines to prevent Twisted Fate from
    game.location = "school_gym" ######### starting automatically after the game jumps to main_loop

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  call quest_lindsey_motive_start
  jump main_loop
