init python:
  quick_starts_by_order["season_1"].append(["Tour de School","quest_isabelle_tour_quick_start",True,"quick_start isabelle_tour"])


label quest_isabelle_tour_quick_start:
  $renpy.block_rollback()
  $_rollback = mouse_visible = True

  ## Hide visible screens
  python:
    renpy.scene("master")
    renpy.scene("screens")
    store._window = False

  ## Initialize the engine
  call init_game

  ## Set prerequisite variables
  call intro_variables(input=True)
  call quest_smash_or_pass_variables
  call quest_natures_call_variables
  call quest_wash_hands_variables
  call quest_dress_to_the_nine_variables
  call quest_back_to_school_special_variables
  call quest_day1_take2_variables(choices=True)
  call quest_the_key_variables
  call quest_act_one_variables

  python:
    kate["at_none_today"] = True
    if school_ground_floor["sign_fallen"]:
      del school_ground_floor.flags["sign_fallen"]
    if quest.day1_take2.finished:
      quest.day1_take2.finished = False

  ## Create a backstory
  while len(backstory) > 0:
    call screen backstory_creation
  show black
  pause 0.5

  ## Provide essential stat points
  python:
    game.disable_notifications = 1
    mc.strength = 3
    mc.charisma = 4
    mc.intellect = 4
    game.disable_notifications = 0

  ## Remove duplicate inventory items
  python:
    if mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("lollipop_2", silent=True)
    if mc.owned_item("spray_empty_bottle"):
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle"), silent=True)
    if mc.owned_item_count("empty_bottle") > 1:
      mc.remove_item("empty_bottle", mc.owned_item_count("empty_bottle")-1, silent=True)
    if mc.owned_item("high_tech_lockpick"):
      mc.remove_item("safety_pin", silent=True)

  ## Choose environment settings
  python:
    game.ui.inventory_page = max(0, (len(mc.inv)+11) // 12-1)
    game.ui.hide_hud = False
    game.hour = 8
    game.location = "school_homeroom"
    game.quest_guide = "isabelle_tour"

  if game.location == "school_entrance":
    play music "<from 35>outdoor_wind"
  elif game.location.id.startswith("school_"):
    play music "school_theme"
  elif game.location.id.startswith("home_"):
    play music "home_theme"

  ## Render new scene
  scene location
  show screen hud
  show screen interface_hider
  with Dissolve(.5)
  window auto

  ## Start the quest
  if quest.day1_take2 == "isabelle_volunteer":
    call school_homeroom_door_isabelle_volunteer
  elif quest.day1_take2 == "isabelle_pass":
    call school_homeroom_door_isabelle_pass
  jump main_loop
