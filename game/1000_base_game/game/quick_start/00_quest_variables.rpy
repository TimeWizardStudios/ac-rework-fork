
init python:
  def skip_backstory_choices():

    default_functions = {
      quest_day1_take2_backstory_volunteer: None, ## Volunteer to show Isabelle around "in Day 1, Take 2"
      quest_isabelle_tour_backstory_dethroning: ## Side with Isabelle in "Tour de School"
        quest_isabelle_tour_backstory_humiliation, ## If unavailable, side with Kate instead
      quest_lindsey_nurse_backstory_help: None, ## Get the Nurse for Lindsey in "Tour de School"
      quest_lindsey_nurse_backstory_photo: ## Take a compromising photo of the Nurse in "Tour de School"
        quest_lindsey_nurse_backstory_kind, ##  If unavailable, be kind and understanding instead
      quest_kate_blowjob_dream_backstory_accept: None, ## Accept Kate's apology in "Make a Wish and Blow"
      quest_isabelle_haggis_backstory_isabelle: None, ## Give the reward to Isabelle in "No True Scotswoman"
      quest_jacklyn_broken_fuse_backstory_mrsl: None, ## Recommend Mrs. L to Jacklyn in "A Short Fuse"
      quest_kate_search_for_nurse_backstory_take: None, ## Take the Nurse to Kate in "Search & Rescue"
      quest_kate_search_for_nurse_backstory_venus_vagenis: None, ## Receive the venus vagenis after letting the Nurse go in "Search & Rescue"
      quest_isabelle_stolen_backstory_lindsey: None, ## Tell Isabelle the thief is Lindsey in "Stolen Hearts"
      quest_flora_bonsai_backstory_help_her: ## Help Flora bake in "Tiny Thirsty Tree"
        quest_flora_bonsai_backstory_stop_her, ## If unavailable, stop her from baking instead
      quest_flora_bonsai_backstory_help_vines: None, ## Help the vines assault Flora in "Tiny Thirsty Tree"
      quest_jacklyn_statement_backstory_get_out: None, ## Make a statement with Jacklyn in "The Statement"
      quest_jacklyn_statement_backstory_bus_jacklyn: None, ## Let Jacklyn decide the paint job in "The Statement"
      quest_jacklyn_statement_backstory_anal: None, ## Have anal sex with Jacklyn in "The Statement"
      quest_lindsey_wrong_backstory_lindsey: None, ## Try to help Lindsey in "Nothing Wrong With Me"
      quest_maxine_lines_backstory_maxine: None, ## Listen to Maxine in "The Ley of the Land"
      quest_kate_fate_backstory_spit: None, ## Spit out Kate's socks in "Twisted Fate"
      quest_isabelle_locker_backstory_enough: ## Calm Isabelle down in "Gathering Storm"
        quest_isabelle_locker_backstory_justice, ## If unavailable, add fuel to her fire instead
      quest_kate_desire_backstory_release: None, ## Release the Nurse from Kate in "Twisted Desire"
      quest_jacklyn_sweets_backstory_photo: None, ## Ask Jacklyn to take a photo of your "King of Sweets" piece for Newfall Culinary in "King of Sweets"
      quest_kate_stepping_backstory_what_i_could: None, ## Concede that you did what you could in "Stepping on the Rose"
      quest_isabelle_dethroning_backstory_insist: ## Insist on not pushing your luck in "Dethroning the Queen"
        quest_isabelle_dethroning_backstory_give_in, ## If unavailable, give in and go after Kate instead
      quest_isabelle_dethroning_backstory_oink: ## Oink to scare Kate in "Dethroning the Queen"
        quest_isabelle_dethroning_backstory_grunt, ## If unavailable, grunt to arouse her instead
      quest_isabelle_dethroning_backstory_not_waiting: ## Don't wait for Lindsey to fall down in "Dethroning the Queen"
        quest_isabelle_dethroning_backstory_what_i_could, ## If unavailable, concede that you did what you could instead
      quest_maya_witch_backstory_internet: None, ## Ask the internet for help with Maya's curse problem in "Big Witch Energy"
      quest_jacklyn_romance_backstory_art: None, ## Assure Flora that you're just going to the art gallery exhibit with Jacklyn for the art in "Suitable Romance"
    }

    if ["backstory intro","In the introduction, a mysterious texter asked your name\nin exchange for life-changing information. You told them it's..."] in backstory:
      mc.name = "Anon"
      intro_backstory()

    while len(backstory) > 0:
      icon,event,choices = backstory[0]
      for text,function in choices:
        if function in default_functions:
          if text.startswith("#"):
            function = default_functions[function]
          function()

    game.skipped_backstory_choices = True




## v1.0 — Introduction (January, 2020) ##

label intro_variables(input=True):
  python:
    mc.add_phone_contact("jo", silent=True)
    mc.add_phone_contact("flora", silent=True)
    mc.add_phone_contact("hidden_number", silent=True)
    mc.add_message_to_history("hidden_number", hidden_number, "I know what you did last weekend.")
    mc.add_message_to_history("hidden_number", mc, "And what was that?")
    mc.add_message_to_history("hidden_number", hidden_number, "The answer is... absolutely nothing.")
    mc.add_message_to_history("hidden_number", mc, "Who is this? Also, that's not true!")
    mc.add_message_to_history("hidden_number", hidden_number, "Completing Castlevania for the 167th time and attempting to make an origami cat out of an old pizza box doesn't count.")
    mc.add_message_to_history("hidden_number", mc, "How do you know this?")
    mc.add_message_to_history("hidden_number", hidden_number, "Doesn't matter. You're lazy and self-destructive. That's why you're here now, contemplating your failures.")
    mc.add_message_to_history("hidden_number", hidden_number, "You've had no significant impact on anyone's life. [flora] hates you. You have no special talents. You've never even had an interesting science fair project!")
    mc.add_message_to_history("hidden_number", hidden_number, "Some of it is bad luck. Most of it is your own doing.")
    mc.add_message_to_history("hidden_number", hidden_number, "I've never seen anyone make so many poor choices in life. It saddens me.")
    mc.add_message_to_history("hidden_number", mc, "[jo], is that you?")
    mc.add_message_to_history("hidden_number", hidden_number, "No, I'm not. But I have something to tell you about those choices. Something life-changing!")
    mc.add_message_to_history("hidden_number", hidden_number, "All you need to do is tell me your name.")
    mc.add_message_to_history("hidden_number", mc, "I asked first!")
    mc.add_message_to_history("hidden_number", hidden_number, "My name isn't important. If you want to know what I have to say, tell me yours.")
    if not input:
      mc.name = "Anon"
    mc.add_message_to_history("hidden_number", mc, "My name is [mc]. There you go! Now tell me!")
    mc.add_message_to_history("hidden_number", mc, "Hello?")
    mc.add_message_to_history("hidden_number", mc, "That's really funny...")
  if input:
    call intro_backstory
  return

label intro_backstory:
  python:
    backstory = []
    backstory.append([
      "backstory intro",
      "In the introduction, a mysterious texter asked your name\nin exchange for life-changing information. You told them it's..."
      ])
  return


init python:
  def intro_backstory():
    del backstory[0]




## v1.0 — "Smash or Pass" (January, 2020) ##

label quest_smash_or_pass_variables:
  python:
    quest.smash_or_pass.start(silent=True)
    home_bedroom["alarm"] = "off"
#   home_bedroom["alarm"] = "smashed"
    home_bedroom["alarm_smash_combo"]-=1
#   home_bedroom["alarm_smash_combo"]+=1
    quest.smash_or_pass.finish(silent=True)
  return




## v1.0 — "Nature's Call" (January, 2020) ##

label quest_natures_call_variables:
  python:
    quest.natures_call.start(silent=True)
    quest.natures_call.finish(silent=True)
  return




## v1.0 — "Wash Hands" (January, 2020) ##

label quest_wash_hands_variables:
  python:
    quest.wash_hands.start(silent=True)
    quest.wash_hands.finish(silent=True)
  return




## v1.0 — "Dress to the Nine" (January, 2020) ##

label quest_dress_to_the_nine_variables:
  python:
    quest.dress_to_the_nine.start(silent=True)
    mc["first_choice"] = "hoodie"
#   mc["first_choice"] = "striped vest"
#   mc["first_choice"] = "tank top"
    quest.dress_to_the_nine.finish(silent=True)
  return




## v1.0 — "Back to School Special" (January, 2020) ##

label quest_back_to_school_special_variables:
  python:
    quest.back_to_school_special.start(silent=True)
    quest.back_to_school_special.finish(silent=True)
  return




## v1.0 — "Day 1, Take 2" (January, 2020) ##

label quest_day1_take2_variables(choices=False):
  python:
    quest.day1_take2.start(silent=True)
    mc.add_message_to_history("hidden_number", hidden_number, "Blasted technology! Apologies for earlier.")
    mc.add_message_to_history("hidden_number", hidden_number, "I have something important to tell you.")
    mc.add_message_to_history("hidden_number", mc, "Seriously? Just out with it!")
    mc.add_message_to_history("hidden_number", hidden_number, "Your choices in life matter. Even the smallest ones.")
    mc.add_message_to_history("hidden_number", hidden_number, "Sometimes, they have consequences. Not just for yourself, but also for others. Keep that in mind, [mc].")
    mc.add_message_to_history("hidden_number", mc, "Thanks. I guess that is important. Now more than ever. What else?")
    mc.add_message_to_history("hidden_number", hidden_number, "Follow your heart, and you'll be okay!")
    if not choices:
      isabelle["volunteered"] = True
#     isabelle["volunteered"] = False
    school_ground_floor["sign_fallen"] = True
    quest.day1_take2.finish(silent=True)
  if choices:
    call quest_day1_take2_backstory
  return

label quest_day1_take2_backstory:
  python:
    backstory.append([
      "backstory day1_take2",
      "In {color=#48F}Day 1, Take 2{/}, [mrsl] introduced [isabelle], the new student from\nLondon, before asking someone to show her around. You decided to...",
      [[
        "Volunteer",
        quest_day1_take2_backstory_volunteer
        ],
      [
        "Pass",
        quest_day1_take2_backstory_pass
        ]]
      ])
  return


init python:
  def quest_day1_take2_backstory_volunteer():
    isabelle["volunteered"] = True
    quest.day1_take2.advance("isabelle_volunteer")
    del backstory[0]

  def quest_day1_take2_backstory_pass():
    quest.day1_take2.advance("isabelle_pass")
    del backstory[0]




## v1.0 — "The Key pt. I" (January, 2020)  &  v1.1 — "The Key pt. II" (May, 2020) ##

label quest_the_key_variables:
  python:
    quest.the_key.start(silent=True)
    mc.add_item("permission_slip", silent=True)
    mc.remove_item("permission_slip", silent=True)
    mc.add_item("locker_key", silent=True)
    quest.the_key.finish(silent=True)
  return




## v1.0 — "Poolside Story pt. I" (January, 2020)  &  v1.6 — "Poolside Story pt. II" (March, 2021) ##

label quest_poolside_story_variables:
  python:
    quest.poolside_story.start(silent=True)
    mc.add_item("old_artwork", silent=True)
    mc.remove_item("old_artwork", silent=True)
    mc.add_item("confetti", silent=True)
    if not school_art_class["discarded_art_1_taken"]:
      school_art_class["discarded_art_1_taken"] = True
      mc.add_item("discarded_art1", silent=True)
    if not school_art_class["discarded_art_2_taken"]:
      school_art_class["discarded_art_2_taken"] = True
      mc.add_item("discarded_art2", silent=True)
    if not school_art_class["discarded_art_3_taken"]:
      school_art_class["discarded_art_3_taken"] = True
      mc.add_item("discarded_art3", silent=True)
    mc.add_item("paint_buckets", silent=True)
    mc.remove_item("discarded_art1", silent=True)
    mc.remove_item("discarded_art2", silent=True)
    mc.remove_item("discarded_art3", silent=True)
    mc.add_item("colored_paper", silent=True)
    if not school_gym["light_smashed"]:
      if not school_homeroom["got_book"]:
        school_homeroom["got_book"] = True
        mc.add_item("bayonets_etiquettes", silent=True)
      if not quest.isabelle_haggis["got_wrench"]:
        quest.isabelle_haggis["got_wrench"] = True
        mc.add_item("monkey_wrench", silent=True)
      school_gym["light_smashed"] = True
      mc.remove_item("monkey_wrench", silent=True)
      mc.add_item("glass_shard", silent=True)
      mc.add_item("monkey_wrench", silent=True)
    mc.remove_item("colored_paper", silent=True)
    mc.add_item("pennants", silent=True)
    mc.add_item("string", silent=True)
    mc.remove_item("pennants", silent=True)
    mc.remove_item("string", silent=True)
    mc.add_item("independence_day_decorations", silent=True)
    mc.remove_item("confetti", silent=True)
    mc.remove_item("independence_day_decorations", silent=True)
    quest.poolside_story.finish(silent=True)
  return




## v1.0 — "Tour de School pt. I" (January, 2020)  &  v1.1 — "Tour de School pt. II" (May, 2020) ##

label quest_isabelle_tour_variables(choices=False, exclusive=None):
  python:
    quest.isabelle_tour.start(silent=True)
    if not choices:
#     quest.kate_over_isabelle.start(silent=True)
      quest.isabelle_over_kate.start(silent=True)
#     quest.kate_over_isabelle.finish(silent=True)
      quest.isabelle_over_kate.finish(silent=True)
    mc.add_phone_contact("isabelle", silent=True)
    quest.isabelle_tour.finish(silent=True)
  if choices:
    call quest_isabelle_tour_backstory(exclusive=exclusive)
  return

label quest_isabelle_tour_backstory(exclusive=None):
  python:
    backstory.append([
      "backstory isabelle_tour",
      "In {color=#48F}Tour de School{/}, [isabelle] confronted [kate]\nabout her bad behavior. You decided to side with...",
      [[
        "#[kate]" if exclusive == "[isabelle]" else "[kate]",
        quest_isabelle_tour_backstory_humiliation
        ],
      [
        "#[isabelle]" if exclusive == "[kate]" else "[isabelle]",
        quest_isabelle_tour_backstory_dethroning
        ]]
      ])
  return


init python:
  def quest_isabelle_tour_backstory_humiliation():
    quest.kate_over_isabelle.start(silent=True)
    quest.kate_over_isabelle.finish(silent=True)
    del backstory[0]

  def quest_isabelle_tour_backstory_dethroning():
    quest.isabelle_over_kate.start(silent=True)
    quest.isabelle_over_kate.finish(silent=True)
    del backstory[0]




## v1.1 — "Art Through Suffering pt. I" (May, 2020)  &  v1.4 — "Art Through Suffering pt. II" (January, 2021) ##

label quest_jacklyn_art_focus_variables(reversible=False):
  python:
    quest.jacklyn_art_focus.start(silent=True)
    school_art_class["easel_paint_buckets"] = True
    if not school_art_class["brush_taken"]:
      school_art_class["brush_taken"] = True
      mc.add_item("brush", silent=True)
    mc.add_phone_contact("jacklyn", silent=True)
    quest.jacklyn_art_focus.finish(silent=True)
    if reversible:
      quest.jacklyn_art_focus["revert"] = True
  return




## v1.1 — "A Short Fuse" (May, 2020) ##

label quest_jacklyn_broken_fuse_variables(choices=False, reversible=False):
  python:
    quest.jacklyn_broken_fuse.start(silent=True)
    if not school_nurse_room["lollipop_taken"]:
      school_nurse_room["lollipop_taken"] = True
      mc.add_item("lollipop_2", silent=True)
    if not mc.owned_item("specialized_fuse_cable_replacement_tool"):
      mc.remove_item("plastic_fork", silent=True)
      mc.remove_item("lollipop_2", silent=True)
      mc.add_item("specialized_fuse_cable_replacement_tool", silent=True)
    mc.add_item("broken_cable", silent=True)
    school_ground_floor["locker_unlocked"] = True
    if not quest.jo_potted["got_soup"]:
      quest.jo_potted["got_soup"] = True
      mc.add_item("soup_can", silent=True)
      game.day+=1
    if not school_locker["gotten_lunch_today"]:
      school_locker["gotten_lunch_today"] = True
      mc.add_item("tide_pods", 3, silent=True)
    mc.remove_item("tide_pods", silent=True)
    school_english_class["desk_one_trade_count"]+=1
    game.day+=1
    mc.add_item("electrical_tape", silent=True)
    mc.remove_item("broken_cable", silent=True)
    mc.remove_item("electrical_tape", silent=True)
    mc.add_item("fixed_cable", silent=True)
    mc.remove_item("fixed_cable", silent=True)
    if not choices:
      school_art_class["nude_model"] = "mrsl"
#     school_art_class["nude_model"] = "nurse"
#     school_art_class["nude_model"] = "jacklyn"
    quest.jacklyn_broken_fuse.finish(silent=True)
    if reversible:
      quest.jacklyn_broken_fuse["revert"] = True
  if choices:
    call quest_jacklyn_broken_fuse_backstory
  return

label quest_jacklyn_broken_fuse_backstory:
  python:
    backstory.append([
      "backstory jacklyn_broken_fuse",
      "In {color=#48F}A Short Fuse{/}, [jacklyn] told you she needed a nude model\nfor some of the senior classes. You recommended...",
      [[
        "[mrsl]",
        quest_jacklyn_broken_fuse_backstory_mrsl
        ],
      [
        "The [nurse]",
        quest_jacklyn_broken_fuse_backstory_nurse
        ],
      [
        "[jacklyn] herself",
        quest_jacklyn_broken_fuse_backstory_jacklyn
        ]]
      ])

    if not mc.owned_item("compromising_photo"):
      backstory[backstory.index(["backstory jacklyn_broken_fuse","In {color=#48F}A Short Fuse{/}, [jacklyn] told you she needed a nude model\nfor some of the senior classes. You recommended...",[["[mrsl]",quest_jacklyn_broken_fuse_backstory_mrsl],["The [nurse]",quest_jacklyn_broken_fuse_backstory_nurse],["[jacklyn] herself",quest_jacklyn_broken_fuse_backstory_jacklyn]]])] = ["backstory jacklyn_broken_fuse","In {color=#48F}A Short Fuse{/}, [jacklyn] told you she needed a nude model\nfor some of the senior classes. You recommended...",[["[mrsl]",quest_jacklyn_broken_fuse_backstory_mrsl],["[jacklyn] herself",quest_jacklyn_broken_fuse_backstory_jacklyn]]]
  return


init python:
  def quest_jacklyn_broken_fuse_backstory_mrsl():
    school_art_class["nude_model"] = "mrsl"
    del backstory[0]

  def quest_jacklyn_broken_fuse_backstory_nurse():
    school_art_class["nude_model"] = "nurse"
    del backstory[0]

  def quest_jacklyn_broken_fuse_backstory_jacklyn():
    school_art_class["nude_model"] = "jacklyn"
    del backstory[0]




## v1.1 — "Fully Booked" (May, 2020) ##

label quest_lindsey_book_variables:
  python:
    quest.lindsey_book.start(silent=True)
    if not school_english_class["bookshelf_interact_first_time"]:
      school_english_class["bookshelf_interact_first_time"] = True
      mc.add_item("marilyn_the_mason_drawing", silent=True)
    mc.add_item("snow_white", silent=True)
#   mc.add_item("catch_thirty_four", silent=True)
#   mc.add_item("atlas_plugged", silent=True)
    mc.remove_item("snow_white", silent=True)
#   mc.remove_item("catch_thirty_four", silent=True)
#   mc.remove_item("atlas_plugged", silent=True)
    game.day+=1
    if not school_entrance["bush_interacted"]:
      school_entrance["bush_interacted"] =  True
      mc.add_item("stick", silent=True)
    if not school_homeroom["stick_interacted"]:
      school_homeroom["stick_interacted"] = True
      mc.add_item("stick", silent=True)
    if not school_gym["stick_interacted"]:
      school_gym["stick_interacted"] = True
      mc.add_item("stick", silent=True)
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    mc.remove_item("stick", silent=True)
    mc.remove_item("stick", silent=True)
    mc.add_item("stick_two", silent=True)
    mc.remove_item("stick_two", silent=True)
    mc.remove_item("stick", silent=True)
    mc.add_item("stick_three", silent=True)
    mc.remove_item("stick_three", silent=True)
    mc.remove_item("ball_of_yarn", silent=True)
    mc.add_item("makeshift_easel", silent=True)
    mc.remove_item("makeshift_easel", silent=True)
    if not school_homeroom["got_book"]:
      school_homeroom["got_book"] = True
      mc.add_item("bayonets_etiquettes", silent=True)
    if not quest.isabelle_haggis["got_wrench"]:
      quest.isabelle_haggis["got_wrench"] = True
      mc.add_item("monkey_wrench", silent=True)
    mc.add_item("beaver_carcass", silent=True)
    quest.lindsey_book.finish(silent=True)
  return




## v1.1 — "Loser to the Rescue" (May, 2020) ##

label quest_lindsey_nurse_variables(choices=False, exclusive=None, exclusive1=None, exclusive2=None):
  python:
    quest.lindsey_nurse.start(silent=True)
    if not choices:
      quest.lindsey_nurse["carried"] = True
      mc.add_item("compromising_photo", silent=True)
#     lindsey["romance_disabled"] = True
    school_nurse_room["curtain_off"] = True
    quest.lindsey_nurse.finish(silent=True)
  if choices:
    call quest_lindsey_nurse_backstory(exclusive=exclusive, exclusive1=exclusive1, exclusive2=exclusive2)
  return

label quest_lindsey_nurse_backstory(exclusive=None, exclusive1=None, exclusive2=None):
  python:
    quest.lindsey_nurse["carried"] = True
    mc.add_item("compromising_photo", silent=True)
    lindsey["romance_disabled"] = True
    backstory.append([
      "backstory lindsey_nurse1",
      "Still in {color=#48F}Tour de School{/}, [lindsey] accidentally ran into you\nwhile doing laps in the gym. You decided to...",
      [[
        "Go get the [nurse]",
        quest_lindsey_nurse_backstory_help
        ],
      [
        "#Pass up on helping her" if exclusive == "Go get the [nurse]" or exclusive1 == "Go get the [nurse]" else "Pass up on helping her",
        quest_lindsey_nurse_backstory_pass
        ]]
      ])
    backstory.append([
      "backstory lindsey_nurse2",
      "Still in {color=#48F}Tour de School{/}, you walked into what seemed to be the [nurse] masturbating\nbehind the curtain. After pulling it back to confirm your suspicion, you decided to...",
      [[
        "#Take a compromising photo of her" if exclusive2 == "Be kind and understanding" else "Take a compromising photo of her",
        quest_lindsey_nurse_backstory_photo
        ],
      [
        "Be kind and understanding",
        quest_lindsey_nurse_backstory_kind
        ]]
      ])
  return


init python:
  def quest_lindsey_nurse_backstory_help():
    del lindsey.flags["romance_disabled"]
    del backstory[0]

  def quest_lindsey_nurse_backstory_pass():
    del quest.lindsey_nurse.flags["carried"]
    mc.remove_item("compromising_photo", silent=True)
    del backstory[:2]

  def quest_lindsey_nurse_backstory_photo():
    del backstory[0]

  def quest_lindsey_nurse_backstory_kind():
    mc.remove_item("compromising_photo", silent=True)
    del backstory[0]




## v1.1 — "Make a Wish and Blow" (May, 2020) ##

label quest_kate_blowjob_dream_variables(choices=False):
  python:
    quest.kate_blowjob_dream.start(silent=True)
    mc.add_item("courage_badge", silent=True)
    home_bedroom["courage_badge"] = True
    mc.remove_item("courage_badge", silent=True)
    mc.add_message_to_history("hidden_number", hidden_number, "I really shouldn't be messaging you, but I just wanted to say that you've done well today.")
    mc.add_message_to_history("hidden_number", hidden_number, "Some mistakes, granted. But you're still learning.")
    mc.add_message_to_history("hidden_number", mc, "Who are you? Seriously? Why do you keep spying on me?")
    mc.add_message_to_history("hidden_number", hidden_number, "I'm no one special, but I am proud of you.")
    mc.add_message_to_history("hidden_number", mc, "Can we meet? I'd like to talk to you about a few things.")
    mc.add_message_to_history("hidden_number", mc, "...")
    game.day+=1
    if not choices:
      quest.kate_blowjob_dream["bj_received"] = "yes"
#     quest.kate_blowjob_dream["bj_received"] = "no"
#     unlock_stat_perk("love14")
    home_bedroom["alarm"] = "off_again"
#   home_bedroom["alarm"] = "smashed_again"
    home_bedroom["alarm_smash_combo"]-=1
#   home_bedroom["alarm_smash_combo"]+=1
    quest.kate_blowjob_dream.finish(silent=True)
  if choices:
    call quest_kate_blowjob_dream_backstory
  return

label quest_kate_blowjob_dream_backstory:
  python:
    backstory.append([
      "backstory kate_blowjob_dream",
      "In {color=#48F}Make a Wish and Blow{/}, [kate] got on her knees to apologize\nfor how she had treated you the past few years. You decided to...",
      [[
        "Accept her apology",
        quest_kate_blowjob_dream_backstory_accept
        ],
      [
        "Turn down her apology",
        quest_kate_blowjob_dream_backstory_turn_down
        ]]
      ])
  return


init python:
  def quest_kate_blowjob_dream_backstory_accept():
    quest.kate_blowjob_dream["bj_received"] = "yes"
    del backstory[0]

  def quest_kate_blowjob_dream_backstory_turn_down():
    quest.kate_blowjob_dream["bj_received"] = "no"
    unlock_stat_perk("love14")
    del backstory[0]




## v1.1 — "Punked" (May, 2020) ##

label quest_flora_jacklyn_introduction_variables(reversible=False):
  python:
    quest.flora_jacklyn_introduction.start(silent=True)
    mc.add_item("cake_slice", silent=True)
    mc.add_item("plastic_fork", silent=True)
    quest.flora_jacklyn_introduction.finish(silent=True)
    if reversible:
      quest.flora_jacklyn_introduction["revert"] = True
  return




## v1.1 — "Chili Con Carnal Sin" (May, 2020) ##

label quest_flora_cooking_chilli_variables:
  python:
    quest.flora_cooking_chilli.start(silent=True)
    if not school_cafeteria["cooking_pot_taken"]:
      school_cafeteria["cooking_pot_taken"] = True
      mc.add_item("cooking_pot", silent=True)
    home_kitchen["cooking_pot"] = True
    mc.remove_item("cooking_pot", silent=True)
    mc.add_item("onion_slice", silent=True)
    mc.remove_item("onion_slice", silent=True)
    mc.add_item("spice", silent=True)
    mc.add_item("spoon", silent=True)
    mc.remove_item("spice", silent=True)
    mc.remove_item("spoon", silent=True)
    mc.add_item("spoonful_of_spice", silent=True)
    mc.remove_item("spoonful_of_spice", silent=True)
    mc.add_item("flora_phone", silent=True)
    quest.flora_cooking_chilli["distraction_method"] = "done"
    mc.remove_item("flora_phone", silent=True)
    quest.flora_cooking_chilli.finish(silent=True)
  return




## v1.1 — "No True Scotswoman" (May, 2020) ##

label quest_isabelle_haggis_variables(choices=False):
  python:
    quest.isabelle_haggis.start(silent=True)
    mc.add_item("haggis_lunchbox", silent=True)
    mc.remove_item("haggis_lunchbox", silent=True)
    if not school_homeroom["got_book"]:
      school_homeroom["got_book"] = True
      mc.add_item("bayonets_etiquettes", silent=True)
    if not quest.isabelle_haggis["got_wrench"]:
      quest.isabelle_haggis["got_wrench"] = True
      mc.add_item("monkey_wrench", silent=True)
    mc.add_item("globe", silent=True)
    mc.remove_item("globe", silent=True)
    quest.isabelle_haggis["greasy_bolt_acquired"] = True
    mc.add_item("greasy_bolt", silent=True)
    mc.add_item("globe", silent=True)
    school_homeroom["globe_fixed"] = True
    mc.remove_item("globe", silent=True)
    mc.remove_item("greasy_bolt", silent=True)
    mc.add_item("bolt", silent=True)
    school_homeroom["open_window"] = True
    mc.remove_item("bolt", silent=True)
    school_gym["ladder_taken"] = True
    school_entrance["window_shortcut"] = True
    if not choices:
#     flora["brooch"] = True
      mc.add_item("flora_bracelet", silent=True)
      mc.add_item("mrsl_brooch", silent=True)
#     isabelle["brooch"] = True
    quest.isabelle_haggis.finish(silent=True)
  if choices:
    call quest_isabelle_haggis_backstory
  return

label quest_isabelle_haggis_backstory:
  python:
    mc.add_item("flora_bracelet", silent=True)
    mc.add_item("mrsl_brooch", silent=True)
    backstory.append([
      "backstory isabelle_haggis",
      "In {color=#48F}No True Scotswoman{/}, [mrsl] locked you in the homeroom with\n[isabelle] and [flora]. The three of you decided the reward should go to...",
      [[
        "[flora]",
        quest_isabelle_haggis_backstory_flora
        ],
      [
        "You",
        quest_isabelle_haggis_backstory_mc
        ],
      [
        "[isabelle]",
        quest_isabelle_haggis_backstory_isabelle
        ]]
      ])
  return


init python:
  def quest_isabelle_haggis_backstory_flora():
    mc.remove_item("flora_bracelet", silent=True)
    mc.remove_item("mrsl_brooch", silent=True)
    flora["brooch"] = True
    del backstory[0]

  def quest_isabelle_haggis_backstory_mc():
    del backstory[0]

  def quest_isabelle_haggis_backstory_isabelle():
    mc.remove_item("flora_bracelet", silent=True)
    mc.remove_item("mrsl_brooch", silent=True)
    isabelle["brooch"] = True
    del backstory[0]




## v1.1 — "Search & Rescue" (May, 2020) ##

label quest_kate_search_for_nurse_variables(choices=False):
  python:
    quest.kate_search_for_nurse.start(silent=True)
    mc.add_item("wrapper", 22, silent=True)
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    if not school_nurse_room["got_pin"]:
      school_nurse_room["got_pin"] = True
      mc.add_item("safety_pin", silent=True)
    if not mc.owned_item("high_tech_lockpick"):
      mc.remove_item("ball_of_yarn", silent=True)
      mc.remove_item("safety_pin", silent=True)
      mc.add_item("high_tech_lockpick", silent=True)
    mc.add_phone_contact("nurse", silent=True)
    if not choices:
#     mc.add_item("nymphoria_heartus", silent=True)
      mc.add_item("venus_vagenis", silent=True)
      unlock_stat_perk("love20")
#     mc.add_item("lollipop_2", silent=True)
    quest.kate_search_for_nurse.finish(silent=True)
  if choices:
    call quest_kate_search_for_nurse_backstory
  return

label quest_kate_search_for_nurse_backstory:
  python:
    mc.add_item("nymphoria_heartus", silent=True)
    mc.add_item("venus_vagenis", silent=True)
    mc.add_item("lollipop_2", silent=True)
    backstory.append([
      "backstory kate_search_for_nurse1",
      "In {color=#48F}Search & Rescue{/}, the [nurse] hid from an appointment\nwith [kate], before getting found out. You decided to...",
      [[
        "Take her to [kate]",
        quest_kate_search_for_nurse_backstory_take
        ],
      [
        "Let her go",
        quest_kate_search_for_nurse_backstory_let_go
        ]]
      ])
    backstory.append([
      "backstory kate_search_for_nurse2"
      "Still in {color=#48F}Search & Rescue{/}, the [nurse] then gave you a choice between\ntwo rare herbs as thanks for letting her go. You chose..."
      [[
        "Nymphoria Heartus",
        quest_kate_search_for_nurse_backstory_nymphoria_heartus
        ],
      [
        "Venus Vagenis",
        quest_kate_search_for_nurse_backstory_venus_vagenis
        ]]
      ])
  return


init python:
  def quest_kate_search_for_nurse_backstory_take():
    mc.remove_item("nymphoria_heartus", silent=True)
    mc.remove_item("venus_vagenis", silent=True)
    del backstory[:2]

  def quest_kate_search_for_nurse_backstory_let_go():
    mc.remove_item("lollipop_2", silent=True)
    unlock_stat_perk("love20")
    del backstory[0]

  def quest_kate_search_for_nurse_backstory_nymphoria_heartus():
    mc.remove_item("venus_vagenis", silent=True)
    del backstory[0]

  def quest_kate_search_for_nurse_backstory_venus_vagenis():
    mc.remove_item("nymphoria_heartus", silent=True)
    del backstory[0]




## v1.1 — "The Missing Tapes pt. I" (May, 2020)  &  v1.10 — "The Missing Tapes pt. II" (July, 2021) ##

label quest_mrsl_HOT_variables:
  python:
    quest.mrsl_HOT.start(silent=True)
    school_ground_floor["locker_unlocked"] = True
    if not quest.jo_potted["got_soup"]:
      quest.jo_potted["got_soup"] = True
      mc.add_item("soup_can", silent=True)
      game.day+=1
    if not school_locker["gotten_lunch_today"]:
      school_locker["gotten_lunch_today"] = True
      mc.add_item("tide_pods", 3, silent=True)
    mc.remove_item("tide_pods", silent=True)
    home_bathroom["dirty_clothes_taken"] = True
    mc.add_item("dirty_laundry", silent=True)
    mc.remove_item("dirty_laundry", silent=True)
    mc.add_item("clean_laundry", silent=True)
    mc.remove_item("clean_laundry", silent=True)
    game.day+=2
    mc.add_item("vcr_tape", silent=True)
    mc.add_item("vhs_player", silent=True)
    home_bedroom["vhs"] = True
    mc.remove_item("vhs_player", silent=True)
    if not home_bedroom["controller_taken"]:
      home_bedroom["controller_taken"] = True
      mc.add_item("xcube_controller", silent=True)
    mc.add_item("capture_card", silent=True)
    home_bedroom["capture_card"] = True
    mc.remove_item("capture_card", silent=True)
    mc.remove_item("vcr_tape", silent=True)
    quest.mrsl_HOT.finish(silent=True)
  return




## v1.2 — Unstarted Quests (September, 2020) ##

label quest_act_one_variables:
  python:
    quest.act_one.start(silent=True)
  return




## v1.3 — "Stolen Hearts" (October, 2020) ##

label quest_isabelle_stolen_variables(choices=False):
  python:
    quest.isabelle_stolen.start(silent=True)
    mc.add_item("pen", silent=True)
    mc.add_item("piece_of_paper", silent=True)
    mc.remove_item("pen", silent=True)
    mc.remove_item("piece_of_paper", silent=True)
    mc.add_item("maxine_letter", silent=True)
    mc.remove_item("maxine_letter", silent=True)
    mc.add_item("letter_from_maxine", silent=True)
    mc.remove_item("letter_from_maxine", silent=True)
    school_ground_floor["locker_unlocked"] = True
    mc.add_item("doughnut", silent=True)
    mc.add_item("doughnut", 10, silent=True)
    nurse["strike_book_activated"] = True
    nurse["strike_book"]+=1
    mc.remove_item("doughnut", silent=True)
    if not home_bedroom["flash_drive_taken"]:
      home_bedroom["flash_drive_taken"] = True
      mc.add_item("flash_drive", silent=True)
    quest.mrsl_number.start(silent=True)
    if not school_cafeteria["got_coffee"]:
      school_cafeteria["got_coffee"] = True
      mc.add_item("cup_of_coffee", silent=True)
      mc.remove_item("cup_of_coffee", silent=True)
      mc.add_item("coffee_cup", silent=True)
    mc.add_phone_contact("mrsl", silent=True)
    quest.mrsl_number.finish(silent=True)
    quest.isabelle_stolen["lindsey_talk"] = True
    if not choices:
#     quest.isabelle_stolen["accused"] = "kate"
      quest.isabelle_stolen["accused"] = "lindsey"
      quest.isabelle_stolen["lindsey_slaped"] = True
    quest.isabelle_stolen.finish(silent=True)
  if choices:
    call quest_isabelle_stolen_backstory
  return

label quest_isabelle_stolen_backstory:
  python:
    backstory.append([
      "backstory isabelle_stolen",
      "In {color=#48F}Stolen Hearts{/}, [isabelle] revealed someone broke into her locker\nand stole a box of chocolates. You told her the thief is...",
      [[
        "[kate]",
        quest_isabelle_stolen_backstory_kate
        ],
      [
        "[lindsey]",
        quest_isabelle_stolen_backstory_lindsey
        ]]
      ])
  return


init python:
  def quest_isabelle_stolen_backstory_kate():
    quest.isabelle_stolen["accused"] = "kate"
    del backstory[0]

  def quest_isabelle_stolen_backstory_lindsey():
    quest.isabelle_stolen["accused"] = "lindsey"
    quest.isabelle_stolen["lindsey_slaped"] = True
    del backstory[0]




## v1.4 — "Operation B.O.B." (January, 2021) ##

label quest_berb_fight_variables:
  python:
    quest.berb_fight.start(silent=True)
    mc.add_item("book_of_the_dammed", silent=True)
    if not school_english_class["desk_three_first"]:
      school_english_class["desk_three_first"] = True
      mc.add_item("baseball", silent=True)
    mc.remove_item("baseball", silent=True)
    school_entrance["baseball_picked_up"] = True
    mc.add_item("baseball", silent=True)
    school_forest_glade["unlocked"] = True
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("pepelepsi", silent=True)
    school_forest_glade["pollution"]+=1
    mc.remove_item("pepelepsi", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("pepelepsi", silent=True)
    school_forest_glade["pollution"]+=1
    mc.remove_item("pepelepsi", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("pepelepsi", silent=True)
    school_forest_glade["pollution"]+=1
    mc.remove_item("pepelepsi", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.add_item("lollipop", 5, silent=True)
    mc.remove_item("lollipop", 5, silent=True)
    mc.add_item("wrapper", 5, silent=True)
    school_forest_glade["wrappers"] = True
    mc.remove_item("wrapper", 5, silent=True)
    if not school_english_class["desk_four_interact_first"]:
      school_english_class["desk_four_interact_first"] = True
      mc.add_item("apple", silent=True)
    school_ground_floor["locker_unlocked"] = True
    if not quest.jo_potted["got_soup"]:
      quest.jo_potted["got_soup"] = True
      mc.add_item("soup_can", silent=True)
      game.day+=1
    if not school_locker["gotten_lunch_today"]:
      school_locker["gotten_lunch_today"] = True
      mc.add_item("tide_pods", 3, silent=True)
    mc.remove_item("apple", silent=True)
    mc.remove_item("tide_pods", silent=True)
    mc.add_item("poisoned_apple", silent=True)
    mc.remove_item("poisoned_apple", silent=True)
    if not school_gym["light_smashed"]:
      school_gym["light_smashed"] = True
      mc.remove_item("baseball", silent=True)
      mc.add_item("glass_shard", silent=True)
      mc.add_item("baseball", silent=True)
    if not berb["scalped"]:
      berb["scalped"] = True
      mc.add_item("beaver_pelt", silent=True)
    quest.berb_fight.finish(silent=True)
  return




## v1.4 — "Potted Weeds" (January, 2021) ##

label quest_jo_potted_variables:
  python:
    quest.jo_potted.start(silent=True)
    game.day+=2
    mc.add_item("package_gigglypuff_seeds", silent=True)
    mc.remove_item("package_gigglypuff_seeds", silent=True)
    mc.add_item("gigglypuff_seeds", silent=True)
    if not home_hall["umbrella_taken"]:
      home_hall["umbrella_taken"] = True
      mc.add_item("umbrella", silent=True)
    mc.add_item("power_plower", silent=True)
    school_forest_glade["farm"] = "farm"
    school_forest_glade["birds"] = {k:"roof" for k in range(6)}
    mc.remove_item("gigglypuff_seeds", silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    game.day+=1
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    game.day+=1
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    game.day+=1
    mc.add_item("gigglypuff_leaves", silent=True)
    if not home_kitchen["kettle"]:
      mc.add_item("kettle", silent=True)
    mc.remove_item("kettle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.remove_item("gigglypuff_leaves", silent=True)
    if not home_hall["lamp_taken"]:
      home_hall["lamp_taken"] = True
      mc.add_item("lamp", silent=True)
    home_kitchen["kettle"] = "lamp"
    mc.remove_item("lamp", silent=True)
    mc.add_item("gigglypuff_tea", silent=True)
    mc.remove_item("gigglypuff_tea", silent=True)
    mc.add_item("key_jo_room", silent=True)
    quest.jo_potted.finish(silent=True)
  return




## v1.5 — "Buried Truths" (February, 2021) ##

label quest_isabelle_buried_variables(reversible=False):
  python:
    quest.isabelle_buried.start(silent=True)
    if not school_first_hall["trash_bin_interact"]:
      school_first_hall["trash_bin_interact"] = True
      mc.add_item("rock", silent=True)
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    school_ground_floor["secret_locker_found"] = True
    if not school_secret_locker["coin_taken"]:
      school_secret_locker["coin_taken"] = True
      mc.add_item("mysterious_coin", silent=True)
    if not mc.owned_item("mysterious_medallion"):
      mc.remove_item("ball_of_yarn", silent=True)
      mc.remove_item("mysterious_coin", silent=True)
      mc.add_item("mysterious_medallion", silent=True)
    mc.add_message_to_history("hidden_number", hidden_number, "It is a blessing seeing your name on my screen! At long last, you have come to your senses.")
    mc.add_message_to_history("hidden_number", hidden_number, "I have followed your first steps and I have what you seek.")
    mc.add_message_to_history("hidden_number", mc, "You have the chocolate box?")
    mc.add_message_to_history("hidden_number", hidden_number, "Indeed.")
    mc.add_message_to_history("hidden_number", mc, "Can I have it?")
    mc.add_message_to_history("hidden_number", hidden_number, "You can. I will leave it in your locker.")
    mc.add_message_to_history("hidden_number", hidden_number, "However, you should know that I did not find it in the trash.")
    mc.add_message_to_history("hidden_number", mc, "What do you mean? Where did you find it?")
    mc.add_message_to_history("hidden_number", hidden_number, "Hidden.")
    mc.add_message_to_history("hidden_number", mc, "Hidden? Why?")
    mc.add_message_to_history("hidden_number", mc, "HeIIo?")
    school_ground_floor["locker_unlocked"] = True
    mc.add_item("isabelle_box", silent=True)
    mc.remove_item("isabelle_box", silent=True)
    if not quest.jo_potted["got_soup"]:
      quest.jo_potted["got_soup"] = True
      mc.add_item("soup_can", silent=True)
    mc.remove_item("soup_can", silent=True)
    mc.add_item("empty_can", silent=True)
    if not school_forest_glade["dam_stick_retrieved"]:
      school_forest_glade["dam_stick_retrieved"] = True
      mc.add_item("stick", silent=True)
    if not mc.owned_item("shovel"):
      mc.remove_item("empty_can", silent=True)
      mc.remove_item("stick", silent=True)
      mc.add_item("shovel", silent=True)
    if not quest.maxine_hook.finished:
      mc.add_item("locked_box_forest", silent=True)
    quest.isabelle_buried["hole_state"] = 5
    game.day+=1
    quest.isabelle_buried.finish(silent=True)
    if reversible:
      quest.isabelle_buried["revert"] = True
  return




## v1.7 — "The Statement" (April, 2021) ##

label quest_jacklyn_statement_variables(choices=False):
  python:
    quest.jacklyn_statement.start("remove", silent=True)
    mc.add_message_to_history("jacklyn", mc, "Hey, wanna go on a date?")
    mc.add_message_to_history("jacklyn", jacklyn, "I like the arrowhead. And I won't say no to a free filler.")
    mc.add_message_to_history("jacklyn", mc, "Can I cook you dinner?")
    mc.add_message_to_history("jacklyn", jacklyn, "That's baby gold!")
    mc.add_message_to_history("jacklyn", mc, "Does that mean yes?")
    mc.add_message_to_history("jacklyn", jacklyn, "What else would it mean?")
    mc.add_message_to_history("jacklyn", mc, "Just making sure...")
    mc.add_message_to_history("jacklyn", mc, "How about my place tomorrow night?")
    mc.add_message_to_history("jacklyn", jacklyn, "Wicked.")
    school_cafeteria["garlic_count"]+=1
    mc.add_item("garlic", silent=True)
    mc.add_item("croutons", silent=True)
    home_kitchen["plant_right"] = True
    home_kitchen["whoo_sauce_taken"] = True
    mc.add_item("whoo_sauce", silent=True)
    mc.remove_item("garlic", silent=True)
    mc.remove_item("croutons", silent=True)
    game.day+=1
    mc.add_message_to_history("jacklyn", jacklyn, "I'm outside. Is the doorbell broken?")
    mc.add_message_to_history("jacklyn", mc, "Yeah, it's broken. I'll be right down!")
    if not home_hall["machete_taken"]:
      home_hall["machete_taken"] = True
      mc.add_item("machete", silent=True)
#   if not choices:
#     flora["madeamends"] = True
    if not choices:
#     school_entrance["bus_lust"] = True
#     school_entrance["bus_love"] = True
      school_entrance["bus_jacklyn"] = True
    if not choices:
      quest.jacklyn_statement["ending"] = "anal"
#     quest.jacklyn_statement["ending"] = "pegging"
    game.day+=1
    quest.jacklyn_statement.finish(silent=True)
  if choices:
    call quest_jacklyn_statement_backstory
  return

label quest_jacklyn_statement_backstory:
  python:
    backstory.append([
      "backstory jacklyn_statement1",
      "In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",
      [[
        "Leave and make a statement with [jacklyn]",
        quest_jacklyn_statement_backstory_get_out
        ],
      [
        "Have [jacklyn] go after her",
        quest_jacklyn_statement_backstory_step_back
        ]]
      ])
    backstory.append([
      "backstory jacklyn_statement2",
      "Still in {color=#48F}The Statement{/}, [jacklyn] then asked you what kind\nof statement the two of you should make. You chose...",
      [[
        "One of passion, ferocity, and vigor",
        quest_jacklyn_statement_backstory_bus_lust
        ],
      [
        "One of love, tranquility, and understanding",
        quest_jacklyn_statement_backstory_bus_love
        ],
      [
        "To let [jacklyn] decide",
        quest_jacklyn_statement_backstory_bus_jacklyn
        ]]
      ])
    backstory.append([
      "backstory jacklyn_statement3",
      "Still in {color=#48F}The Statement{/}, [jacklyn] helped you make a statement on the\nschool bus, before asking what's on your mind. You told her it's...",
      [[
        "Enlightenment",
        quest_jacklyn_statement_backstory_enlightenment
        ],
      [
        "Your dick in her ass",
        quest_jacklyn_statement_backstory_anal
        ],
      [
        "{size=21}{color=#000}*not a real one{/}\n\n\n{/}Her dick* in your ass{size=21}\n\n\n{color=#fefefe}*not a real one{/}{/}",
        quest_jacklyn_statement_backstory_pegging
        ]]
      ])
  return


init python:
  def quest_jacklyn_statement_backstory_get_out():
    del backstory[0]

  def quest_jacklyn_statement_backstory_step_back():
    del backstory[:3]

  def quest_jacklyn_statement_backstory_stay_home():
    flora["madeamends"] = True
    del backstory[:3]

  def quest_jacklyn_statement_backstory_bus_lust():
    school_entrance["bus_lust"] = True
    del backstory[0]

  def quest_jacklyn_statement_backstory_bus_love():
    school_entrance["bus_love"] = True
    del backstory[0]

  def quest_jacklyn_statement_backstory_bus_jacklyn():
    school_entrance["bus_jacklyn"] = True
    del backstory[0]

  def quest_jacklyn_statement_backstory_enlightenment():
    del backstory[0]

  def quest_jacklyn_statement_backstory_anal():
    quest.jacklyn_statement["ending"] = "anal"
    del backstory[0]

  def quest_jacklyn_statement_backstory_pegging():
    quest.jacklyn_statement["ending"] = "pegging"
    del backstory[0]




## v1.8 — "Oxygen for Oxymoron" (May, 2021) ##

label quest_clubroom_access_variables:
  python:
    quest.clubroom_access.start(silent=True)
    if not school_homeroom["got_book"]:
      school_homeroom["got_book"] = True
      mc.add_item("bayonets_etiquettes", silent=True)
    if not quest.isabelle_haggis["got_wrench"]:
      quest.isabelle_haggis["got_wrench"] = True
      mc.add_item("monkey_wrench", silent=True)
    school_first_hall_east["vent_open"] = True
    mc.add_item("grappling_hook", silent=True)
    mc.add_item("globe", silent=True)
    mc.remove_item("globe", silent=True)
    quest.isabelle_haggis["greasy_bolt_acquired"] = True
    mc.add_item("greasy_bolt", silent=True)
    mc.add_item("globe", silent=True)
    school_homeroom["globe_fixed"] = True
    mc.remove_item("globe", silent=True)
    if not home_bathroom["tissues_taken_today"]:
      home_bathroom["tissues_taken_today"] = True
      mc.add_item("tissues", 5, silent=True)
    mc.remove_item("greasy_bolt", silent=True)
    mc.remove_item("tissues", silent=True)
    mc.add_item("bolt", silent=True)
    school_homeroom["open_window"] = True
    mc.remove_item("bolt", silent=True)
    school_homeroom["rope"] = True
    mc.remove_item("grappling_hook", silent=True)
    school_homeroom["rope_first_climb"] = True
    quest.maxine_eggs["first_clubroom"] = True
    quest.clubroom_access.finish(silent=True)
  return




## v1.8 — "Hide and Seek" (May, 2021) ##

label quest_spinach_seek_variables:
  python:
    quest.spinach_seek.start(silent=True)
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    mc.remove_item("ball_of_yarn", silent=True)
    mc.add_item("spray_cap", silent=True)
    quest.spinach_seek.finish(silent=True)
  return




## v1.8 — "Tiny Thirsty Tree" (May, 2021) ##

label quest_flora_bonsai_variables(choices=False):
  python:
    quest.flora_bonsai.start(silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
    if not mc.owned_item("spray_water"):
      if mc.owned_item("empty_bottle"):
        mc.remove_item("empty_bottle", silent=True)
        mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.remove_item("spray_cap", silent=True)
      mc.add_item("spray_water", silent=True)
    mc.remove_item("spray_water", silent=True)
    mc.add_item("spray_empty_bottle", silent=True)
    mc.remove_item("spray_empty_bottle", silent=True)
    mc.add_item("spray_strawberry_juice", silent=True)
    mc.remove_item("spray_strawberry_juice", silent=True)
    mc.add_item("spray_empty_bottle", silent=True)
    mc.remove_item("cake_slice", silent=True)
    if not choices:
#     quest.flora_bonsai["handholding"] = True
      quest.flora_bonsai["handholding"] = False
    game.day+=1
    mc.remove_item("spray_empty_bottle", silent=True)
    mc.add_item("spray_strawberry_juice", silent=True)
    mc.remove_item("spray_strawberry_juice", silent=True)
    mc.add_item("spray_empty_bottle", silent=True)
    school_nurse_room["pads_taken"] = True
    mc.add_item("pad", 2, silent=True)
    mc.remove_item("pad", silent=True)
    mc.add_message_to_history("flora", flora, "Bring me sweets!")
    mc.add_item("lollipop", silent=True)
    mc.remove_item("lollipop", silent=True)
    mc.add_message_to_history("flora", flora, "I'm thirsty now! Bring me a drink!")
    if not school_cafeteria["got_coffee"]:
      school_cafeteria["got_coffee"] = True
      mc.add_item("cup_of_coffee", silent=True)
      mc.remove_item("cup_of_coffee", silent=True)
      mc.add_item("coffee_cup", silent=True)
    mc.add_message_to_history("flora", flora, "Help me to the bathroom!")
    mc.add_message_to_history("flora", flora, "Bring me a book from the English classroom!")
    mc.add_message_to_history("flora", mc, "Why? How are you supposed to read it?")
    mc.add_item("catch_thirty_four", silent=True)
    mc.remove_item("catch_thirty_four", silent=True)
    mc.add_message_to_history("flora", flora, "I need you to cut onions for me.")
    game.day+=1
    school_ground_floor_west["lifted_glass"] = True
    mc.add_item("stethoscope", silent=True)
    mc.remove_item("stethoscope", silent=True)
    mc.add_item("nurse_pen", silent=True)
    mc.remove_item("nurse_pen", silent=True)
    if not school_nurse_room["meds_taken"]:
      school_nurse_room["meds_taken"] = True
      mc.add_item("meds", silent=True)
    mc.remove_item("meds", silent=True)
    mc.remove_item("spray_empty_bottle", silent=True)
    mc.add_item("spray_water", silent=True)
    mc.remove_item("spray_water", silent=True)
    mc.add_item("spray_empty_bottle", silent=True)
    if not choices:
#     quest.flora_bonsai["hug"] = True
      quest.flora_bonsai["hug"] = False
    quest.flora_bonsai.finish(silent=True)
  if choices:
    call quest_flora_bonsai_backstory
  return

label quest_flora_bonsai_backstory:
  python:
    backstory.append([
      "backstory flora_bonsai1",
      "In {color=#48F}Tiny Thirsty Tree{/}, [flora] knocked on your door late at night\nclaiming she needed more cake. You decided to...",
      [[
        "Help her bake",
        quest_flora_bonsai_backstory_help_her
        ],
      [
        "Stop her from baking",
        quest_flora_bonsai_backstory_stop_her
        ]]
      ])
    backstory.append([
      "backstory flora_bonsai2",
      "Still in {color=#48F}Tiny Thirsty Tree{/}, a tree dragged [flora] to the [nurse]'s office,\nbefore attempting to have its way with her. You decided to...",
      [[
        "Help its vines assault her",
        quest_flora_bonsai_backstory_help_vines
        ],
      [
        "Stop its vines from assaulting her",
        quest_flora_bonsai_backstory_stop_vines
        ]]
      ])
  return


init python:
  def quest_flora_bonsai_backstory_help_her():
    if ["backstory jacklyn_statement1","In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",[["Leave and make a statement with [jacklyn]",quest_jacklyn_statement_backstory_get_out],["Have [jacklyn] go after her",quest_jacklyn_statement_backstory_step_back]]] in backstory:
      backstory[backstory.index(["backstory jacklyn_statement1","In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",[["Leave and make a statement with [jacklyn]",quest_jacklyn_statement_backstory_get_out],["Have [jacklyn] go after her",quest_jacklyn_statement_backstory_step_back]]])] = ["backstory jacklyn_statement1","In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",[["Leave and make a statement with [jacklyn]",quest_jacklyn_statement_backstory_get_out],["Look for her",quest_jacklyn_statement_backstory_stay_home]]]
    del backstory[0]

  def quest_flora_bonsai_backstory_stop_her():
    quest.flora_bonsai["handholding"] = True
    if ["backstory jacklyn_statement1","In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",[["Leave and make a statement with [jacklyn]",quest_jacklyn_statement_backstory_get_out],["Have [jacklyn] go after her",quest_jacklyn_statement_backstory_step_back]]] in backstory:
      backstory[backstory.index(["backstory jacklyn_statement1","In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",[["Leave and make a statement with [jacklyn]",quest_jacklyn_statement_backstory_get_out],["Have [jacklyn] go after her",quest_jacklyn_statement_backstory_step_back]]])] = ["backstory jacklyn_statement1","In {color=#48F}The Statement{/}, [flora] hid in your closet to spy on you and [jacklyn],\nbefore getting caught and storming out of the room. You decided to...",[["Leave and make a statement with [jacklyn]",quest_jacklyn_statement_backstory_get_out],["Look for her",quest_jacklyn_statement_backstory_stay_home]]]
    del backstory[0]

  def quest_flora_bonsai_backstory_help_vines():
    del backstory[0]

  def quest_flora_bonsai_backstory_stop_vines():
    quest.flora_bonsai["hug"] = True
    del backstory[0]




## v1.9 — "Nothing Wrong With Me" (June, 2021) ##

label quest_lindsey_wrong_variables(choices=False, reversible=False):
  python:
    quest.lindsey_wrong.start(silent=True)
    quest.maxine_lines.start(silent=True)
    quest.maxine_lines.fail(silent=True)
    mc.add_item("lindsey_bottle", silent=True)
    mc.remove_item("lindsey_bottle", silent=True)
    quest.lindsey_wrong["fountain_state"] = 5
    mc.add_item("lindsey_bottle", silent=True)
    mc.add_item("doughnut", 2, silent=True)
    mc.remove_item("doughnut", 2, silent=True)
    mc.add_item("mop", silent=True)
    quest.lindsey_wrong["puddle_1_clean"] = True
    quest.lindsey_wrong["puddle_2_clean"] = True
    quest.lindsey_wrong["puddle_3_clean"] = True
    quest.lindsey_wrong["puddle_4_clean"] = True
    quest.lindsey_wrong["puddle_5_clean"] = True
    quest.lindsey_wrong["puddle_6_clean"] = True
    mc.remove_item("lindsey_bottle", silent=True)
    school_first_hall["mop_shown"] = True
    mc.remove_item("mop", silent=True)
    mc.add_item("lindsey_bag", silent=True)
    mc.remove_item("lindsey_bag", silent=True)
    mc.add_phone_contact("lindsey", silent=True)
    quest.lindsey_wrong.finish(silent=True)
    if reversible:
      quest.lindsey_wrong["revert"] = True
  if choices:
    call quest_lindsey_wrong_backstory
  return

label quest_lindsey_wrong_backstory:
  python:
    backstory.append([
      "backstory lindsey_wrong",
      "In {color=#48F}Nothing Wrong With Me{/}, [maxine] claimed that the disturbance in the magnetic field\ncaused by the saucer-eyed doll is messing with [lindsey]'s balance. You decided to...",
      [[
        "#Listen to [maxine]",
        quest_lindsey_wrong_backstory_maxine
        ],
      [
        "Try to help [lindsey]",
        quest_lindsey_wrong_backstory_lindsey
        ]]
      ])
  return


init python:
  def quest_lindsey_wrong_backstory_maxine():
    del backstory[0]

  def quest_lindsey_wrong_backstory_lindsey():
    del backstory[0]




## v1.10 — "Strawberries, Cherries" (July, 2021) ##

label quest_maxine_wine_variables:
  python:
    quest.maxine_wine.start(silent=True)
    school_ground_floor["locker_unlocked"] = True
    mc.add_item("flora_poster", 5, silent=True)
    quest.maxine_wine["entrance_hall_poster"] = True
    mc.remove_item("flora_poster", silent=True)
    quest.maxine_wine["1f_hall_poster"] = True
    mc.remove_item("flora_poster", silent=True)
    quest.maxine_wine["sports_wing_poster"] = True
    mc.remove_item("flora_poster", silent=True)
    quest.maxine_wine["admin_wing_poster"] = True
    mc.remove_item("flora_poster", silent=True)
    quest.maxine_wine["homeroom_poster"] = True
    mc.remove_item("flora_poster", silent=True)
    mc.add_item("flora_pin", silent=True)
    mc.remove_item("flora_pin", silent=True)
    jacklyn.equip("jacklyn_squidpin")
    mc.add_item("summer_wine", silent=True)
    mc.remove_item("summer_wine", silent=True)
    maxine["flirt_unlocked"] = True
    school_locker["maxine_nude_taken"] = True
    mc.add_item("maxine_nude", silent=True)
    mc.remove_item("maxine_nude", silent=True)
    mc.add_item("maxine_other_nude", silent=True)
    quest.maxine_wine.finish(silent=True)
  return




## v1.11 — "The Glowing Spider Eggs" (August, 2021) ##

label quest_maxine_eggs_variables:
  python:
    quest.maxine_eggs.start(silent=True)
    school_computer_room["unlocked"] = True
    school_computer_room["welcome_back"] = True
    if not home_bedroom["flash_drive_taken"]:
      home_bedroom["flash_drive_taken"] = True
      mc.add_item("flash_drive", silent=True)
    quest.maxine_eggs["released_cyberia"] = True
    mc.add_item("maxine_camera", silent=True)
    mc.add_item("silver_paint", silent=True)
    mc.remove_item("silver_paint", silent=True)
    school_bathroom["utter_disappointment"] = True
    if not school_bathroom["baseball_bat_taken"]:
      school_bathroom["baseball_bat_taken"] = True
      mc.add_item("baseball_bat", silent=True)
#   quest.maxine_eggs["lindsey_photo"] = True
    quest.maxine_eggs["lindsey_photo"] = False
    game.day+=1
    mc.remove_item("maxine_camera", silent=True)
    quest.maxine_eggs.finish(silent=True)
  return




## v1.12 — "Table Manners" (September, 2021) ##

label quest_mrsl_table_variables:
  python:
    quest.mrsl_table.start(silent=True)
    mc.add_item("letter_for_jo", silent=True)
    mc.remove_item("letter_for_jo", silent=True)
    if not home_bathroom["tissues_taken_today"]:
      home_bathroom["tissues_taken_today"] = True
      mc.add_item("tissues", 5, silent=True)
    mc.remove_item("tissues", silent=True)
    mc.add_item("wet_tissue", silent=True)
    mc.remove_item("wet_tissue", silent=True)
    mc.remove_item("tissues", silent=True)
    if not home_hall["umbrella_taken"]:
      home_hall["umbrella_taken"] = True
      mc.add_item("umbrella", silent=True)
    mc.add_item("cat_tail", silent=True)
    mc.add_item("mop2", silent=True)
    mc.remove_item("mop2", silent=True)
    game.day+=1
    quest.mrsl_table.finish(silent=True)
  return




## v1.13 — "The Ley of the Land" (October, 2021) ##

label quest_maxine_lines_variables(choices=False):
  python:
    quest.maxine_lines.start(silent=True)
    quest.lindsey_wrong.start(silent=True)
    quest.lindsey_wrong.fail(silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("seven_hp", silent=True)
    mc.remove_item("seven_hp", silent=True)
    mc.add_item("empty_bottle", silent=True)
    school_gym["ball_3_hidden"] = True
    school_gym["ball_7_shown"] = "six_pointer"
    school_clubroom["locator_taken"] = True
    mc.add_item("leyline_locator", silent=True)
    school_computer_room["unlocked"] = True
    school_computer_room["welcome_back"] = True
    mc.add_item("charger_cable", silent=True)
    mc.remove_item("charger_cable", silent=True)
    mc.add_item("sliced_cable", silent=True)
    mc.remove_item("sliced_cable", silent=True)
    mc.add_item("magnet", silent=True)
    mc.add_item("magnet", silent=True)
    game.day+=1
    mc.add_item("package_magnet", silent=True)
    mc.remove_item("package_magnet", silent=True)
    mc.add_item("magnet", silent=True)
    mc.remove_item("magnet", silent=True)
    mc.remove_item("magnet", silent=True)
    mc.remove_item("magnet", silent=True)
    school_ground_floor["secret_locker_found"] = True
    school_secret_locker["panel"] = "off"
    mc.add_item("saucer_eyed_doll", silent=True)
    school_clubroom["doll"] = True
    mc.remove_item("saucer_eyed_doll", silent=True)
    mc.add_item("magnet", silent=True)
    mc.add_item("magnet", silent=True)
    mc.add_item("magnet", silent=True)
    school_clubroom["magnets"] = True
    mc.remove_item("magnet", 3, silent=True)
    home_bathroom["got_pocket_mirror"] = True
    mc.add_item("pocket_mirror", silent=True)
    school_clubroom["microplastics_taken"] = True
    mc.add_item("microplastics", silent=True)
    school_clubroom["books_moved"] = True
    mc.remove_item("pocket_mirror", silent=True)
    school_clubroom["doll"] = "eyeless"
    mc.add_item("pocket_mirror", silent=True)
    school_entrance["crop"] = True
    quest.maxine_lines.finish(silent=True)
  if choices:
    call quest_maxine_lines_backstory
  return

label quest_maxine_lines_backstory:
  python:
    backstory.append([
      "backstory lindsey_wrong",
      "In {color=#48F}The Ley of the Land{/}, [maxine] claimed that the disturbance in the magnetic field\ncaused by the saucer-eyed doll is messing with [lindsey]'s balance. You decided to...",
      [[
        "Listen to [maxine]",
        quest_maxine_lines_backstory_maxine
        ],
      [
        "#Try to help [lindsey]",
        quest_maxine_lines_backstory_lindsey
        ]]
      ])
  return


init python:
  def quest_maxine_lines_backstory_maxine():
    del backstory[0]

  def quest_maxine_lines_backstory_lindsey():
    del backstory[0]




## v1.14 — "Twisted Fate" (November, 2021) ##

label quest_kate_fate_variables(choices=False):
  python:
    quest.kate_fate.start(silent=True)
    mc.add_phone_contact("kate", silent=True)
    if not choices:
      quest.kate_fate["spit_sock"] = True
#     mc.add_item("kate_socks_dirty", silent=True)
#     mc.remove_item("kate_socks_dirty", silent=True)
#     mc.add_item("kate_socks_clean", silent=True)
    quest.kate_fate.finish(silent=True)
  if choices:
    call quest_kate_fate_backstory
  return

label quest_kate_fate_backstory:
  python:
    mc.add_item("kate_socks_dirty", silent=True)
    mc.remove_item("kate_socks_dirty", silent=True)
    mc.add_item("kate_socks_clean", silent=True)
    backstory.append([
      "backstory kate_fate",
      "In {color=#48F}Twisted Fate{/}, [kate] tied you up and kept her dirty socks in your mouth\nfor a whole night, before [isabelle] managed to set you free. You decided to...",
      [[
        "Spit out [kate]'s socks",
        quest_kate_fate_backstory_spit
        ],
      [
        "Hide the socks from her",
        quest_kate_fate_backstory_hide
        ]]
      ])
  return


init python:
  def quest_kate_fate_backstory_spit():
    mc.remove_item("kate_socks_clean", silent=True)
    quest.kate_fate["spit_sock"] = True
    del backstory[0]

  def quest_kate_fate_backstory_hide():
    del backstory[0]




## v1.15 — "Gathering Storm" (December, 2021) ##

label quest_isabelle_locker_variables(choices=False, exclusive=None):
  python:
    quest.isabelle_locker.start(silent=True)
    mc.add_item("isabelle_panties_inv", silent=True)
    mc.remove_item("isabelle_panties_inv", silent=True)
    mc.add_item("isabelle_panties_inv", silent=True)
    mc.remove_item("isabelle_panties_inv", silent=True)
    if not choices:
#     quest.isabelle_locker["time_for_a_change"] = True
      quest.isabelle_locker["time_for_a_change"] = False
    quest.isabelle_locker.finish(silent=True)
  if choices:
    call quest_isabelle_locker_backstory(exclusive=exclusive)
  return

label quest_isabelle_locker_backstory(exclusive=None):
  python:
    backstory.append([
      "backstory isabelle_locker",
      "In {color=#48F}Gathering Storm{/}, [isabelle] angrily proposed sending\n[maxine] her cat's bloody head. You decided to...",
      [[
        "#Calm her down" if exclusive == "Add fuel to her fire" else "Calm her down",
        quest_isabelle_locker_backstory_enough
        ],
      [
        "#Add fuel to her fire" if exclusive == "Calm her down" else "Add fuel to her fire",
        quest_isabelle_locker_backstory_justice
        ]]
      ])
  return


init python:
  def quest_isabelle_locker_backstory_enough():
#   quest.isabelle_hurricane.phase = 0
#   quest.isabelle_hurricane.started = False
#   if quest.isabelle_hurricane["darkness"]:
#     del quest.isabelle_hurricane.flags["darkness"]
#   if not quest.kate_wicked.finished:
#     if mc.check_phone_contact("maxine"):
#       mc.phone_contacts.remove("maxine")
#   quest.isabelle_hurricane.finished = False
    del backstory[0]

  def quest_isabelle_locker_backstory_justice():
    quest.isabelle_locker["time_for_a_change"] = True
    del backstory[0]




## v1.16 — "Twisted Desire" (February, 2022) ##

label quest_kate_desire_variables(choices=False):
  python:
    quest.kate_desire.start(silent=True)
    game.day+=1
    mc.add_item("paper_star_blue", silent=True)
    mc.add_item("paper_star_red", silent=True)
    mc.add_item("paper_star_green", silent=True)
    mc.add_item("paper_star_black", silent=True)
    mc.add_item("paper_star_gold", silent=True)
    quest.kate_desire["nude_model"] = True
    if not choices:
#     quest.kate_desire["stakes"] = "release"
      mc.add_item("damning_document", silent=True)
    quest.kate_desire.finish(silent=True)
  if choices:
    call quest_kate_desire_backstory
  return

label quest_kate_desire_backstory:
  python:
    quest.kate_desire["stakes"] = "release"
    mc.add_item("damning_document", silent=True)
    backstory.append([
      "backstory kate_desire",
      "In {color=#48F}Twisted Desire{/}, you confronted [kate] about her blackmailing the [nurse], before proposing\na small property wager. Should you win the ongoing challenge, she would have to...",
      [[
        "Release the [nurse]",
        quest_kate_desire_backstory_release
        ],
      [
        "#Give you the dirt she has on her",
        quest_kate_desire_backstory_dirt
        ]]
      ])
  return


init python:
  def quest_kate_desire_backstory_release():
    mc.remove_item("damning_document", silent=True)
    del backstory[0]

  def quest_kate_desire_backstory_dirt():
    del quest.kate_desire.flags["stakes"]
    del backstory[0]




## v1.17 — "Hooking Up" (March, 2022) ##

label quest_maxine_hook_variables:
  python:
    quest.maxine_hook.start(silent=True)
    game.day+=1
    school_computer_room["unlocked"] = True
    school_computer_room["welcome_back"] = True
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("strawberry_juice", silent=True)
    mc.remove_item("strawberry_juice", silent=True)
    mc.add_item("empty_bottle", silent=True)
    school_ground_floor["locker_unlocked"] = True
    if not quest.jo_potted["got_soup"]:
      quest.jo_potted["got_soup"] = True
      mc.add_item("soup_can", silent=True)
    mc.remove_item("soup_can", silent=True)
    mc.add_item("empty_can", silent=True)
    if not school_forest_glade["dam_stick_retrieved"]:
      school_forest_glade["dam_stick_retrieved"] = True
      mc.add_item("stick", silent=True)
    if not mc.owned_item("shovel"):
      mc.remove_item("empty_can", silent=True)
      mc.remove_item("stick", silent=True)
      mc.add_item("shovel", silent=True)
    mc.remove_item("shovel", silent=True)
    mc.add_item("shovel", silent=True)
    if not quest.isabelle_buried.finished:
      mc.add_item("locked_box_forest", silent=True)
      quest.isabelle_buried["hole_state"] = 3
    game.day+=1
    quest.maxine_hook.finish(silent=True)
  return




## v1.18 — "King of Sweets" (April, 2022) ##

label quest_jacklyn_sweets_variables(choices=False):
  python:
    quest.jacklyn_sweets.start(silent=True)
    mc.add_item("doughnut", 10, silent=True)
    mc.remove_item("doughnut", silent=True)
    nurse["strike_book_activated"] = True
    nurse["strike_book"] = 1
    mc.remove_item("doughnut", 5, silent=True)
    mc.add_message_to_history("flora", flora, "Doughnut. Glazing. The circle of life.")
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    if not school_nurse_room["got_pin"]:
      school_nurse_room["got_pin"] = True
      mc.add_item("safety_pin", silent=True)
    if not mc.owned_item("high_tech_lockpick"):
      mc.remove_item("ball_of_yarn", silent=True)
      mc.remove_item("safety_pin", silent=True)
      mc.add_item("high_tech_lockpick", silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("pepelepsi", silent=True)
    mc.remove_item("pepelepsi", silent=True)
    mc.add_item("empty_bottle", silent=True)
    mc.add_item("lollipop", 3, silent=True)
    mc.remove_item("lollipop", 3, silent=True)
    mc.add_item("wrapper", 3, silent=True)
    mc.remove_item("doughnut", 3, silent=True)
#   if not choices:
#     quest.jacklyn_sweets["jacklyn_blowjob"] = True
    mc.add_item("king_of_sweets", silent=True)
    home_bedroom["poster_removed"] = True
    home_bedroom["king_of_sweets"] = True
    mc.remove_item("king_of_sweets", silent=True)
    quest.jacklyn_sweets.finish(silent=True)
  if choices:
    call quest_jacklyn_sweets_backstory
  return

label quest_jacklyn_sweets_backstory:
  python:
    quest.jacklyn_sweets["jacklyn_blowjob"] = True
    backstory.append([
      "backstory jacklyn_sweets",
      "In {color=#48F}King of Sweets{/}, [jacklyn] was left mesmerized by the centerpiece reveal in your \"King of Sweets\" piece.\nFollowing her reaction, you decided to ask her to...",
      [[
        "Take a photo for Newfall Culinary",
        quest_jacklyn_sweets_backstory_photo
        ],
      [
        "Taste the King of Sweets",
        quest_jacklyn_sweets_backstory_taste
        ]]
      ])
  return


init python:
  def quest_jacklyn_sweets_backstory_photo():
    del quest.jacklyn_sweets.flags["jacklyn_blowjob"]
    del backstory[0]

  def quest_jacklyn_sweets_backstory_taste():
    del backstory[0]




## v1.19 — "Chops and Nocs" (May, 2022) ##

label quest_isabelle_piano_variables:
  python:
    quest.isabelle_piano.start("piano", silent=True)
    if not quest.piano_tuning.finished:
      quest.piano_tuning.start(silent=True)
      school_music_class["got_conductor_baton"] = True
      mc.add_item("conductor_baton", silent=True)
      school_music_class["middle_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      school_music_class["bottom_compartment"] = None
      mc.add_item("tuning_hammer", silent=True)
      mc.remove_item("tuning_hammer", silent=True)
      school_first_hall_west["tuning_hammer"] = True
      mc.add_item("tuning_hammer", silent=True)
      school_music_class["bottom_compartment"] = "tuning_hammer"
      mc.remove_item("tuning_hammer", silent=True)
      school_music_class["middle_compartment"] = None
      mc.add_item("conductor_baton", silent=True)
      school_music_class["top_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      mc.add_item("tuning_fork", silent=True)
      mc.remove_item("tuning_fork", silent=True)
      school_first_hall_west["tuning_fork"] = True
      mc.add_item("tuning_fork", silent=True)
      school_music_class["top_compartment"] = "tuning_fork"
      mc.remove_item("tuning_fork", silent=True)
      mc.add_item("conductor_baton", silent=True)
      school_music_class["middle_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      quest.piano_tuning.finish(silent=True)
    school_computer_room["unlocked"] = True
    school_computer_room["welcome_back"] = True
    mc.add_item("power_cable_blue", silent=True)
    mc.remove_item("power_cable_blue", silent=True)
    mc.add_item("chopin_music_score", silent=True)
    school_first_hall_west["music_score"] = "chopin"
    mc.remove_item("chopin_music_score", silent=True)
    mc.add_item("ice_tea", silent=True)
    quest.isabelle_piano.finish(silent=True)
  return




## v1.20 — "Squid Game" (June, 2022) ##

label quest_flora_squid_variables:
  python:
    quest.flora_squid.start(silent=True)
    game.day+=1
    school_ground_floor["sign_fallen"] = False
    if not school_roof_landing["fire_axe_taken"]:
      school_roof_landing["fire_axe_taken"] = True
      mc.add_item("fire_axe", silent=True)
    quest.flora_squid["shower"] = "hug"
#   quest.flora_squid["shower"] = "ass"
    quest.flora_squid.finish(silent=True)
  return




## v1.21 — "Dead Girl's Score" (July, 2022) ##

label quest_lindsey_piano_variables(reversible=False):
  python:
    quest.lindsey_piano.start("nurse", silent=True)
    if not quest.piano_tuning.finished:
      quest.piano_tuning.start(silent=True)
      school_music_class["got_conductor_baton"] = True
      mc.add_item("conductor_baton", silent=True)
      school_music_class["middle_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      school_music_class["bottom_compartment"] = None
      mc.add_item("tuning_hammer", silent=True)
      mc.remove_item("tuning_hammer", silent=True)
      school_first_hall_west["tuning_hammer"] = True
      mc.add_item("tuning_hammer", silent=True)
      school_music_class["bottom_compartment"] = "tuning_hammer"
      mc.remove_item("tuning_hammer", silent=True)
      school_music_class["middle_compartment"] = None
      mc.add_item("conductor_baton", silent=True)
      school_music_class["top_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      mc.add_item("tuning_fork", silent=True)
      mc.remove_item("tuning_fork", silent=True)
      school_first_hall_west["tuning_fork"] = True
      mc.add_item("tuning_fork", silent=True)
      school_music_class["top_compartment"] = "tuning_fork"
      mc.remove_item("tuning_fork", silent=True)
      mc.add_item("conductor_baton", silent=True)
      school_music_class["middle_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      quest.piano_tuning.finish(silent=True)
    mc.add_item("king_bard_album", silent=True)
    if not mc.owned_item("vacuum_cleaner"):
      mc.add_item("vacuum_cleaner", silent=True)
    home_bedroom["clean"] = True
    mc.remove_item("king_bard_album", silent=True)
    mc.add_item("king_bard_album", silent=True)
    quest.lindsey_piano.finish(silent=True)
    if reversible:
      quest.lindsey_piano["revert"] = True
  return




## v1.22 — "Paint It Red" (August, 2022) ##

label quest_isabelle_red_variables:
  python:
    quest.isabelle_red.start(silent=True)
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    if not school_nurse_room["got_pin"]:
      school_nurse_room["got_pin"] = True
      mc.add_item("safety_pin", silent=True)
    if not mc.owned_item("high_tech_lockpick"):
      mc.remove_item("ball_of_yarn", silent=True)
      mc.remove_item("safety_pin", silent=True)
      mc.add_item("high_tech_lockpick", silent=True)
    mc.remove_item("high_tech_lockpick", silent=True)
    mc.add_item("high_tech_lockpick", silent=True)
    quest.isabelle_red["sneaked_a_peek"] = True
    mc.add_item("red_paint", silent=True)
    quest.isabelle_red.finish(silent=True)
  return




## v1.22 — "Photogenic" (August, 2022) ##

label quest_nurse_photogenic_variables:
  python:
    quest.nurse_photogenic.start(silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("strawberry_juice", silent=True)
    mc.remove_item("strawberry_juice", silent=True)
    mc.add_item("empty_bottle", silent=True)
    quest.nurse_photogenic["eavesdropped"] = True
    if not mc.owned_item("compromising_photo"):
      school_nurse_room["curtain_off"] = True
      mc.add_item("compromising_photo", silent=True)
    mc.add_item("sleeping_pills", silent=True)
    quest.nurse_photogenic.finish(silent=True)
  return




## v1.23 — "Stainless Steal" (September, 2022) ##

label quest_flora_handcuffs_variables:
  python:
    quest.flora_handcuffs.start(silent=True)
    home_computer["visited"]+=1
    black_market.remove_item("cutie_harlot")
    game.day+=1
    del home_computer.flags["cutie_harlot_ordered_today"]
    home_computer["cutie_harlot_received"] = True
    mc.add_item("package_cutie_harlot", silent=True)
    mc.remove_item("package_cutie_harlot", silent=True)
    mc.add_item("cutie_harlot", silent=True)
    mc.remove_item("cutie_harlot", silent=True)
    flora.equip("flora_skirt")
#   jacklyn["prom_disabled"] = True
    mc.add_message_to_history("flora", flora, "The snail has left its shell! I repeat, the snail has left its shell!")
    mc.add_item("handcuffs", silent=True)
    quest.flora_handcuffs.finish(silent=True)
  return




## v1.24 — "Venting Frustration" (October, 2022) ##

label quest_nurse_venting_variables:
  python:
    quest.nurse_venting.start(silent=True)
    mc.add_item("walkie_talkie", 2, silent=True)
    mc.add_item("maxine_camera", silent=True)
    mc.remove_item("walkie_talkie", silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("walkie_talkie", silent=True)
    mc.remove_item("walkie_talkie", 2, silent=True)
    mc.remove_item("maxine_camera", silent=True)
    mc.add_item("fishing_hook", silent=True)
    mc.add_item("fishing_line", silent=True)
    mc.add_item("empty_bottle", silent=True)
    quest.nurse_venting.finish(silent=True)
  return




## v1.25 — "Hurricane Isabelle" (November, 2022) ##

label quest_isabelle_hurricane_variables:
  python:
    quest.isabelle_hurricane.start("maxine_office", silent=True)
    mc.add_item("spinach", silent=True)
    mc.remove_item("spinach", silent=True)
    quest.isabelle_hurricane["darkness"] = True
    mc.remove_item("flash_drive", silent=True)
    mc.add_item("flash_drive", silent=True)
    mc.remove_item("flash_drive", silent=True)
    mc.add_item("flash_drive", silent=True)
    mc.add_phone_contact("maxine", silent=True)
    quest.isabelle_hurricane.finish(silent=True)
  return




## v1.26 — "A Beautiful Motive" (January, 2023) ##

label quest_lindsey_motive_variables(reversible=False):
  python:
    quest.lindsey_motive.start(silent=True)
    mc.add_item("tubes_of_paint", silent=True)
    mc.add_item("chocolate_heart", silent=True)
    game.day+=1
    mc.remove_item("tubes_of_paint", silent=True)
    quest.lindsey_motive["stop_maxine"] = True
    school_first_hall_west["plant_taken"] = True
    mc.add_item("plant", silent=True)
    mc.remove_item("plant", silent=True)
    school_first_hall["butterflies_inside"] = True
    if not school_bathroom["baseball_bat_taken"]:
      school_bathroom["baseball_bat_taken"] = True
      mc.add_item("baseball_bat", silent=True)
    break_in_day = sum([timestamp for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [True,["You have the chocolate box?"]]],())[0]-1
    lindsey.install_phone_app("messages", silent=True)
    lindsey.add_message_to_history("mer", mer, "Hello!", day=1, hour=19)
    lindsey.add_message_to_history("mer", mer, "I am a big fan. I just wanted to wish you good luck in your competition tomorrow!", day=1, hour=19)
    lindsey.add_message_to_history("mer", lindsey, "Aw, thanks!", day=1, hour=19)
    lindsey.add_message_to_history("mer", lindsey, "But who are you and how did you get my number?", day=1, hour=19)
    lindsey.add_message_to_history("mer", mer, "You can call me Mer! I got it from one of your teammates.", day=1, hour=19)
    lindsey.add_message_to_history("mer", lindsey, "Ah, okay! Nice to meet you, Mer!", day=1, hour=20)
    lindsey.add_message_to_history("mer", lindsey, "Are you coming out to watch me run tomorrow?", day=1, hour=20)
    lindsey.add_message_to_history("mer", mer, "Unfortunately not.", day=1, hour=22)
    lindsey.add_message_to_history("mer", lindsey, "Okay! Well, I hope you have a lovely night.", day=1, hour=22)
    lindsey.add_message_to_history("mer", mer, "Hi, [lindsey]!", day=break_in_day, hour=8)
    lindsey.add_message_to_history("mer", lindsey, "Hello.", day=break_in_day, hour=8)
    lindsey.add_message_to_history("mer", mer, "Are you okay?", day=break_in_day, hour=8)
    lindsey.add_message_to_history("mer", lindsey, "I don't know.", day=break_in_day, hour=8)
    lindsey.add_message_to_history("mer", mer, "Go to [isabelle]'s locker and take the heart-shaped box. Have a little treat and share it with your team. Maybe you'll feel better.", day=break_in_day, hour=8)
    lindsey.add_message_to_history("mer", lindsey, "Okay.", day=break_in_day, hour=8)
    lindsey.add_message_to_history("mer", mer, "You were fabulous today! I cheered you on from the bleachers!", day=game.day-2*(game.day-break_in_day)/3, hour=16)
    lindsey.add_message_to_history("mer", mer, "You race with your heart, I can see it.", day=game.day-2*(game.day-break_in_day)/3, hour=16)
    lindsey.add_message_to_history("mer", lindsey, "Thank you so much!", day=game.day-2*(game.day-break_in_day)/3, hour=19)
    lindsey.add_message_to_history("mer", mer, "Good job beating your personal best today! Your dedication is inspiring.", day=game.day-(game.day-break_in_day)/3, hour=16)
    lindsey.add_message_to_history("mer", mer, "I saw you painting in the park earlier. You and that boy looked cute together.", hour=game.hour-3)
    lindsey.add_message_to_history("mer", lindsey, "Aw, thank you. I like him a lot!", hour=game.hour-2)
    lindsey.add_message_to_history("mer", lindsey, "Hey, Mer! You've been really supportive lately and I'd like to give you something in return.")
    lindsey.add_message_to_history("mer", lindsey, "Meet me in the homeroom after school?")
    game.day+=1
    quest.lindsey_motive.finish(silent=True)
    if reversible:
      quest.lindsey_motive["revert"] = True
  return




## v1.27 — "Jo's Day" (February, 2023) ##

label quest_jo_day_variables:
  python:
    quest.jo_day.start(silent=True)
    if not mc.owned_item("vacuum_cleaner"):
      mc.add_item("vacuum_cleaner", silent=True)
    home_bedroom["clean"] = True
    mc.add_item("picnic_blanket", silent=True)
    mc.add_item("jo_secret_wine", silent=True)
    mc.add_item("mug", silent=True)
    school_english_class["cactus_taken"] = True
    mc.add_item("cactus", silent=True)
    mc.add_item("paint_jar", silent=True)
    mc.remove_item("cactus", silent=True)
    mc.remove_item("paint_jar", silent=True)
    mc.add_item("cactus_in_a_pot", silent=True)
    school_forest_glade["picnic_blanket"] = True
    mc.remove_item("picnic_blanket", silent=True)
    school_forest_glade["jo_secret_wine"] = True
    mc.remove_item("jo_secret_wine", silent=True)
    school_forest_glade["cactus_in_a_pot"] = True
    mc.remove_item("cactus_in_a_pot", silent=True)
    mc.add_item("queen_rue", silent=True)
    mc.remove_item("queen_rue", silent=True)
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
    mc.remove_item("water_bottle", silent=True)
    mc.add_item("love_potion_droplet", silent=True)
    mc.add_item("empty_bottle", silent=True)
    school_forest_glade["love_potion_droplet"] = True
    mc.remove_item("love_potion_droplet", silent=True)
    mc.remove_item("mug", silent=True)
    quest.jo_day.finish(silent=True)
  return




## v1.28 — "The Dog Trick" (March, 2023) ##

label quest_kate_trick_variables:
  python:
    quest.kate_trick.start("jo", silent=True)
    mc.add_item("dog_collar", silent=True)
    quest.kate_trick.finish(silent=True)
  return




## v1.29 — "Stepping on the Rose" (April, 2023) ##

label quest_kate_stepping_variables(choices=False):
  python:
    quest.kate_stepping.start("magazine", silent=True)
    mc.add_item("kate_fashion_poster", silent=True)
    mc.remove_item("kate_fashion_poster", silent=True)
    school_first_hall["kate_fashion_poster"] = True
    mc.add_phone_contact("kate", silent=True)
    mc.remove_item("handcuffs", silent=True)
    mc.remove_item("dog_collar", silent=True)
#   quest.kate_stepping["fuck_isabelle_too"] = True
#   quest.kate_stepping["mistress"] = True
    while game.day < 21:
      game.day+=1
    school_ground_floor["independence_day_banner"] = True
    mc.remove_item("ice_tea", silent=True)
    mc.remove_item("sleeping_pills", silent=True)
    mc.add_item("spiked_ice_tea", silent=True)
    mc.remove_item("spiked_ice_tea", silent=True)
    quest.kate_stepping.finish(silent=True)
  if choices:
    call quest_kate_stepping_backstory
  return

label quest_kate_stepping_backstory:
  python:
    backstory.append([
      "backstory isabelle_dethroning3",
      "In {color=#48F}Stepping on the Rose{/}, [lindsey] climbed onto the edge\nof the school roof, seemingly in a complete trance. You decided...",
      [[
        "#Not to wait for her to fall down",
        quest_kate_stepping_backstory_not_waiting
        ],
      [
        "That you had done what you could",
        quest_kate_stepping_backstory_what_i_could
        ]]
      ])
  return


init python:
  def quest_kate_stepping_backstory_not_waiting():
    del backstory[0]

  def quest_kate_stepping_backstory_what_i_could():
    del backstory[0]




## v1.30 — "Wicked Game" (May, 2023) ##

label quest_kate_wicked_variables:
  python:
    quest.kate_wicked.start(silent=True)
    if not quest.piano_tuning.finished:
      quest.piano_tuning.start(silent=True)
      school_music_class["got_conductor_baton"] = True
      mc.add_item("conductor_baton", silent=True)
      school_music_class["middle_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      school_music_class["bottom_compartment"] = None
      mc.add_item("tuning_hammer", silent=True)
      mc.remove_item("tuning_hammer", silent=True)
      school_first_hall_west["tuning_hammer"] = True
      mc.add_item("tuning_hammer", silent=True)
      school_music_class["bottom_compartment"] = "tuning_hammer"
      mc.remove_item("tuning_hammer", silent=True)
      school_music_class["middle_compartment"] = None
      mc.add_item("conductor_baton", silent=True)
      school_music_class["top_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      mc.add_item("tuning_fork", silent=True)
      mc.remove_item("tuning_fork", silent=True)
      school_first_hall_west["tuning_fork"] = True
      mc.add_item("tuning_fork", silent=True)
      school_music_class["top_compartment"] = "tuning_fork"
      mc.remove_item("tuning_fork", silent=True)
      mc.add_item("conductor_baton", silent=True)
      school_music_class["middle_compartment"] = "conductor_baton"
      mc.remove_item("conductor_baton", silent=True)
      quest.piano_tuning.finish(silent=True)
    mc.add_item("key_music_class", silent=True)
    mc.add_item("costume_shop_voucher", silent=True)
    quest.kate_wicked["party_date"] = game.day
    mc.remove_item("costume_shop_voucher", silent=True)
    home_computer["pig_mask_ordered"] = True
    game.day+=2
    home_computer["pig_mask_received"] = True
    mc.add_item("package_pig_mask", silent=True)
    mc.remove_item("package_pig_mask", silent=True)
    mc.add_item("pig_mask", silent=True)
    kate["outfit_stamp"] = kate.outfit
    mc.remove_item("key_music_class", silent=True)
    mc.add_message_to_history("flora", flora, maxine.contact_number)
    mc.add_phone_contact("maxine", silent=True)
    quest.kate_wicked["cheques"] = "bouncy"
#   quest.kate_wicked["cheques"] = "flat"
#   quest.kate_wicked["cheques"] = "medium"
#   home_kitchen["stones"] = 1
    school_nurse_room["curtain_off"] = True
    mc.add_item("ghost_sheet", silent=True)
    mc.remove_item("ghost_sheet", silent=True)
#   quest.kate_wicked["next_level"] = True
    lindsey["outfit_stamp"] = lindsey.outfit
    game.day+=1
    quest.kate_wicked.finish(silent=True)
  return




## v1.31 — "Dethroning the Queen" (July, 2023) ##

label quest_isabelle_dethroning_variables(choices=False, exclusive=None, exclusive1=None, exclusive2=None, exclusive3=None):
  python:
    quest.isabelle_dethroning.start(silent=True)
    game.day+=1
    school_first_hall["newspaper_taken"] = True
    mc.add_item("newspaper", silent=True)
    school_first_hall_east["newspaper"] = True
    mc.remove_item("newspaper", silent=True)
    while game.day < 21:
      game.day+=1
    school_ground_floor["independence_day_banner"] = True
    isabelle["outfit_stamp"] = isabelle.outfit
    mc.add_item("maxine_camera", silent=True)
    quest.isabelle_dethroning["dinner_picture"] = "isabelle"
    mc.remove_item("maxine_camera", silent=True)
    mc.remove_item("pig_mask", silent=True)
    school_first_hall["red_paint"] = True
    mc.remove_item("red_paint", silent=True)
    school_first_hall["fishing_hook"] = True
    mc.remove_item("fishing_hook", silent=True)
    school_first_hall["fishing_line"] = True
    mc.remove_item("fishing_line", silent=True)
    school_first_hall["red_paint"] = False
    school_first_hall["fishing_hook"] = False
    school_first_hall["fishing_line"] = False
    school_first_hall["paint_splash"] = True
    mc.add_item("pig_mask", silent=True)
    if not mc.owned_item("safety_pin"):
      mc.add_item("safety_pin", silent=True)
    mc.remove_item("safety_pin", silent=True)
    if not mc.owned_item("high_tech_lockpick"):
      mc.add_item("safety_pin", silent=True)
    school_first_hall["paint_splash"] = False
    school_first_hall_west["scorch_marks"] = True
    if not school_roof_landing["fire_axe_taken"]:
      school_roof_landing["fire_axe_taken"] = True
      mc.add_item("fire_axe", silent=True)
    mc.remove_item("fire_axe", silent=True)
    school_roof_landing["axe_hole"]+=1
    school_roof_landing["axe_hole"]+=1
    school_roof_landing["axe_hole"]+=1
    mc.add_item("fire_axe", silent=True)
    quest.isabelle_dethroning.finish(silent=True)
  if choices:
    call quest_isabelle_dethroning_backstory(exclusive=exclusive, exclusive1=exclusive1, exclusive2=exclusive2, exclusive3=exclusive3)
  return

label quest_isabelle_dethroning_backstory(exclusive=None, exclusive1=None, exclusive2=None, exclusive3=None):
  if exclusive1 or exclusive2:
    python:
      backstory.append([
        "backstory isabelle_dethroning1",
        "In {color=#48F}Dethroning the Queen{/}, [kate] locked herself inside the art classroom after being chased down\nby the ghost pig. Having initially failed to talk [isabelle] out of tormenting her further, you decided to...",
        [[
          "#Insist on not pushing your luck" if exclusive1 == "Give in and go after [kate]" else "Insist on not pushing your luck",
          quest_isabelle_dethroning_backstory_insist
          ],
        [
          "Give in and go after [kate]",
          quest_isabelle_dethroning_backstory_give_in
          ]]
        ])
      backstory.append([
        "backstory isabelle_dethroning2",
        "Still in {color=#48F}Dethroning the Queen{/}, unlocking the supply closet revealed a cowering [kate],\nher eyes glued shut by the drying paint. Using that to your advantage, you decided to...",
        [[
          "#Oink to scare her" if exclusive2 == "Grunt to arouse her" else "Oink to scare her",
          quest_isabelle_dethroning_backstory_oink
          ],
        [
          "#Grunt to arouse her" if exclusive2 == "Oink to scare her" else "Grunt to arouse her",
          quest_isabelle_dethroning_backstory_grunt
          ]]
        ])
      backstory.append([
        "backstory isabelle_dethroning3",
        "Still in {color=#48F}Dethroning the Queen{/}, [lindsey] climbed onto the edge\nof the school roof, seemingly in a complete trance. You decided...",
        [[
          "#Not to wait for her to fall down" if exclusive3 == "That you had done what you could" else "Not to wait for her to fall down",
          quest_isabelle_dethroning_backstory_not_waiting
          ],
        [
          "#That you had done what you could" if exclusive3 == "Not to wait for her to fall down" else "That you had done what you could",
          quest_isabelle_dethroning_backstory_what_i_could
          ]]
        ])
  else:
    python:
      backstory.append([
        "backstory isabelle_dethroning3",
        "In {color=#48F}Dethroning the Queen{/}, [lindsey] climbed onto the edge\nof the school roof, seemingly in a complete trance. You decided...",
        [[
          "#Not to wait for her to fall down" if exclusive == "That you had done what you could" else "Not to wait for her to fall down",
          quest_isabelle_dethroning_backstory_not_waiting
          ],
        [
          "#That you had done what you could" if exclusive == "Not to wait for her to fall down" else "That you had done what you could",
          quest_isabelle_dethroning_backstory_what_i_could
          ]]
        ])
  return


init python:
  def quest_isabelle_dethroning_backstory_insist():
    del backstory[:2]

  def quest_isabelle_dethroning_backstory_give_in():
    del backstory[0]

  def quest_isabelle_dethroning_backstory_oink():
    del backstory[0]

  def quest_isabelle_dethroning_backstory_grunt():
    quest.isabelle_dethroning["kate_sex"] = "pussy"
    # quest.isabelle_dethroning["kate_sex"] = "ass"
    quest.isabelle_dethroning["kate_cum"] = "outside"
    # quest.isabelle_dethroning["kate_cum"] = "inside"
    quest.isabelle_dethroning["isabelle_caught"] = "sad"
    # quest.isabelle_dethroning["isabelle_caught"] = "disgusted"
    isabelle["romance_disabled"] = True
    del backstory[0]

  def quest_isabelle_dethroning_backstory_not_waiting():
    del backstory[0]

  def quest_isabelle_dethroning_backstory_what_i_could():
    if quest.jacklyn_art_focus["revert"]:
      quest.jacklyn_art_focus.phase = 0
      quest.jacklyn_art_focus.started = False
      if school_art_class["easel_paint_buckets"]:
        del school_art_class.flags["easel_paint_buckets"]
      if school_art_class["brush_taken"]:
        del school_art_class.flags["brush_taken"]
        mc.remove_item("brush", silent=True)
      if mc.check_phone_contact("jacklyn"):
        mc.phone_contacts.remove("jacklyn")
      quest.jacklyn_art_focus.finished = False

    if quest.flora_jacklyn_introduction["revert"]:
      quest.flora_jacklyn_introduction.phase = 0
      quest.flora_jacklyn_introduction.started = False
      mc.remove_item("cake_slice", silent=True)
      mc.remove_item("plastic_fork", silent=True)
      quest.flora_jacklyn_introduction.finished = False

    if quest.jacklyn_broken_fuse["revert"]:
      quest.jacklyn_broken_fuse.phase = 0
      quest.jacklyn_broken_fuse.started = False
      if quest.flora_jacklyn_introduction.finished:
        mc.add_item("plastic_fork", silent=True)
      if school_nurse_room["lollipop_taken"]:
        del school_nurse_room.flags["lollipop_taken"]
      mc.remove_item("specialized_fuse_cable_replacement_tool", silent=True)
      if not (quest.mrsl_HOT.finished or quest.isabelle_stolen.finished or quest.berb_fight.finished or quest.maxine_wine.finished or quest.maxine_hook.finished):
        if school_ground_floor["locker_unlocked"]:
          del school_ground_floor.flags["locker_unlocked"]
      if not (quest.mrsl_HOT.finished or quest.berb_fight.finished or quest.isabelle_buried.finished or quest.maxine_hook.finished):
        if quest.jo_potted["got_soup"]:
          del quest.jo_potted.flags["got_soup"]
        mc.remove_item("soup_can", silent=True)
      if not (quest.jacklyn_broken_fuse.finished or quest.mrsl_HOT.finished or quest.berb_fight.finished):
        if school_locker["gotten_lunch_today"]:
          del school_locker.flags["gotten_lunch_today"]
        mc.remove_item("tide_pods", 3, silent=True)
      school_english_class["desk_one_trade_count"]-=1
      if school_art_class["nude_model"]:
        del school_art_class.flags["nude_model"]
      quest.jacklyn_broken_fuse.finished = False

    if quest.isabelle_buried["revert"]:
      quest.isabelle_buried.phase = 0
      quest.isabelle_buried.started = False
      if school_first_hall["trash_bin_interact"]:
        del school_first_hall.flags["trash_bin_interact"]
        mc.remove_item("rock", silent=True)
      if not (quest.lindsey_book.finished or quest.kate_search_for_nurse.finished or quest.isabelle_buried.finished or quest.spinach_seek.finished or quest.jacklyn_sweets.finished or quest.isabelle_red.finished):
        if school_homeroom["ball_of_yarn_taken"]:
          del school_homeroom.flags["ball_of_yarn_taken"]
        mc.remove_item("ball_of_yarn", 5, silent=True)
      if not quest.maxine_lines.finished:
        if school_ground_floor["secret_locker_found"]:
          del school_ground_floor.flags["secret_locker_found"]
      if school_secret_locker["coin_taken"]:
        del school_secret_locker.flags["coin_taken"]
      mc.remove_item("mysterious_medallion", silent=True)
      if [False,["It is a blessing seeing your name on my screen! At long last, you have come to your senses."]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [False,["It is a blessing seeing your name on my screen! At long last, you have come to your senses."]]],[]))
      if [False,["I have followed your first steps and I have what you seek."]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [False,["I have followed your first steps and I have what you seek."]]],[]))
      if [True,["You have the chocolate box?"]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [True,["You have the chocolate box?"]]],[]))
      if [False,["Indeed."]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [False,["Indeed."]]],[]))
      if [True,["Can I have it?"]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [True,["Can I have it?"]]],[]))
      if [False,["You can. I will leave it in your locker."]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [False,["You can. I will leave it in your locker."]]],[]))
      if [False,["However, you should know that I did not find it in the trash."]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [False,["However, you should know that I did not find it in the trash."]]],[]))
      if [True,["What do you mean? Where did you find it?"]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [True,["What do you mean? Where did you find it?"]]],[]))
      if [False,["Hidden."]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [False,["Hidden."]]],[]))
      if [True,["Hidden? Why?"]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [True,["Hidden? Why?"]]],[]))
      if [True,["HeIIo?"]] in [[who,what] for who,what,timestamp in mc.phone_message_history["hidden_number"]]:
        mc.phone_message_history["hidden_number"].remove(sum([[who,what,timestamp] for who,what,timestamp in mc.phone_message_history["hidden_number"] if [who,what] == [True,["HeIIo?"]]],[]))
      if not (quest.mrsl_HOT.finished or quest.isabelle_stolen.finished or quest.berb_fight.finished or quest.maxine_wine.finished or quest.maxine_hook.finished):
        if school_ground_floor["locker_unlocked"]:
          del school_ground_floor.flags["locker_unlocked"]
      if not (quest.mrsl_HOT.finished or quest.berb_fight.finished or quest.isabelle_buried.finished or quest.maxine_hook.finished):
        if quest.jo_potted["got_soup"]:
          del quest.jo_potted.flags["got_soup"]
      if not quest.maxine_hook.finished:
        if school_forest_glade["dam_stick_retrieved"]:
          del school_forest_glade.flags["dam_stick_retrieved"]
        mc.remove_item("shovel", silent=True)
        if quest.jo_potted["got_soup"]:
          mc.add_item("soup_can", silent=True)
        mc.remove_item("locked_box_forest", silent=True)
        if quest.isabelle_buried["hole_state"]:
          del quest.isabelle_buried.flags["hole_state"]
      quest.isabelle_buried.finished = False

    if quest.lindsey_wrong["revert"]:
      quest.lindsey_wrong.phase = 0
      quest.lindsey_wrong.started = False
      quest.maxine_lines.phase = 0
      quest.maxine_lines.started = False
      quest.maxine_lines.failed = False
      if quest.lindsey_wrong["fountain_state"]:
        del quest.lindsey_wrong.flags["fountain_state"]
      if quest.lindsey_wrong["puddle_1_clean"]:
        del quest.lindsey_wrong.flags["puddle_1_clean"]
      if quest.lindsey_wrong["puddle_2_clean"]:
        del quest.lindsey_wrong.flags["puddle_2_clean"]
      if quest.lindsey_wrong["puddle_3_clean"]:
        del quest.lindsey_wrong.flags["puddle_3_clean"]
      if quest.lindsey_wrong["puddle_4_clean"]:
        del quest.lindsey_wrong.flags["puddle_4_clean"]
      if quest.lindsey_wrong["puddle_5_clean"]:
        del quest.lindsey_wrong.flags["puddle_5_clean"]
      if quest.lindsey_wrong["puddle_6_clean"]:
        del quest.lindsey_wrong.flags["puddle_6_clean"]
      if school_first_hall["mop_shown"]:
        del school_first_hall.flags["mop_shown"]
      if mc.check_phone_contact("lindsey"):
        mc.phone_contacts.remove("lindsey")
      quest.lindsey_wrong.finished = False

    if quest.lindsey_piano["revert"]:
      quest.lindsey_piano.phase = 0
      quest.lindsey_piano.started = False
      if not (quest.isabelle_piano.finished or quest.kate_wicked.finished):
        quest.piano_tuning.phase = 0
        quest.piano_tuning.started = False
        if school_music_class["got_conductor_baton"]:
          del school_music_class.flags["got_conductor_baton"]
        if school_first_hall_west["tuning_hammer"]:
          del school_first_hall_west.flags["tuning_hammer"]
        if school_music_class["bottom_compartment"]:
          del school_music_class.flags["bottom_compartment"]
        if school_first_hall_west["tuning_fork"]:
          del school_first_hall_west.flags["tuning_fork"]
        if school_music_class["top_compartment"]:
          del school_music_class.flags["top_compartment"]
        if school_music_class["middle_compartment"]:
          del school_music_class.flags["middle_compartment"]
        quest.piano_tuning.finished = False
      if not quest.jo_day.finished:
        mc.remove_item("vacuum_cleaner", silent=True)
        if home_bedroom["clean"]:
          del home_bedroom.flags["clean"]
      mc.remove_item("king_bard_album", silent=True)
      quest.lindsey_piano.finished = False

    if quest.lindsey_motive["revert"]:
      quest.lindsey_motive.phase = 0
      quest.lindsey_motive.started = False
      mc.remove_item("chocolate_heart", silent=True)
      if quest.lindsey_motive["stop_maxine"]:
        del quest.lindsey_motive.flags["stop_maxine"]
      if school_first_hall_west["plant_taken"]:
        del school_first_hall_west.flags["plant_taken"]
      if school_first_hall["butterflies_inside"]:
        del school_first_hall.flags["butterflies_inside"]
      if not quest.maxine_eggs.finished:
        if school_bathroom["baseball_bat_taken"]:
          del school_bathroom.flags["baseball_bat_taken"]
        mc.remove_item("baseball_bat", silent=True)
      if hasattr(store,"break_in_day"):
        global break_in_day
        del break_in_day
      lindsey.uninstall_phone_app("messages",True)
      if [False,["Hello!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["Hello!"]]],[]))
      if [False,["I am a big fan. I just wanted to wish you good luck in your competition tomorrow!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["I am a big fan. I just wanted to wish you good luck in your competition tomorrow!"]]],[]))
      if [True,["Aw, thanks!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Aw, thanks!"]]],[]))
      if [True,["But who are you and how did you get my number?"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["But who are you and how did you get my number?"]]],[]))
      if [False,["You can call me Mer! I got it from one of your teammates."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["You can call me Mer! I got it from one of your teammates."]]],[]))
      if [True,["Ah, okay! Nice to meet you, Mer!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Ah, okay! Nice to meet you, Mer!"]]],[]))
      if [True,["Are you coming out to watch me run tomorrow?"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Are you coming out to watch me run tomorrow?"]]],[]))
      if [False,["Unfortunately not."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["Unfortunately not."]]],[]))
      if [True,["Okay! Well, I hope you have a lovely night."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Okay! Well, I hope you have a lovely night."]]],[]))
      if [False,["Hi, [lindsey]!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["Hi, [lindsey]!"]]],[]))
      if [True,["Hello."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Hello."]]],[]))
      if [False,["Are you okay?"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["Are you okay?"]]],[]))
      if [True,["I don't know."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["I don't know."]]],[]))
      if [False,["Go to [isabelle]'s locker and take the heart-shaped box. Have a little treat and share it with your team. Maybe you'll feel better."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["Go to [isabelle]'s locker and take the heart-shaped box. Have a little treat and share it with your team. Maybe you'll feel better."]]],[]))
      if [True,["Okay."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Okay."]]],[]))
      if [False,["You were fabulous today! I cheered you on from the bleachers!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["You were fabulous today! I cheered you on from the bleachers!"]]],[]))
      if [False,["You race with your heart, I can see it."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["You race with your heart, I can see it."]]],[]))
      if [True,["Thank you so much!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Thank you so much!"]]],[]))
      if [False,["Good job beating your personal best today! Your dedication is inspiring."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["Good job beating your personal best today! Your dedication is inspiring."]]],[]))
      if [False,["I saw you painting in the park earlier. You and that boy looked cute together."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [False,["I saw you painting in the park earlier. You and that boy looked cute together."]]],[]))
      if [True,["Aw, thank you. I like him a lot!"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Aw, thank you. I like him a lot!"]]],[]))
      if [True,["Hey, Mer! You've been really supportive lately and I'd like to give you something in return."]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Hey, Mer! You've been really supportive lately and I'd like to give you something in return."]]],[]))
      if [True,["Meet me in the homeroom after school?"]] in [[who,what] for who,what,timestamp in lindsey.phone_message_history["mer"]]:
        lindsey.phone_message_history["mer"].remove(sum([[who,what,timestamp] for who,what,timestamp in lindsey.phone_message_history["mer"] if [who,what] == [True,["Meet me in the homeroom after school?"]]],[]))
      quest.lindsey_motive.finished = False

    if not quest.flora_squid.finished:
      if school_roof_landing["fire_axe_taken"]:
        del school_roof_landing.flags["fire_axe_taken"]
        mc.remove_item("fire_axe", silent=True)
    del school_roof_landing.flags["axe_hole"]
    del backstory[0]




## v1.32 — "Washed Up" (August, 2023) ##

label quest_jo_washed_variables:
  python:
    quest.jo_washed.start(silent=True)
    mc.add_item("bottle_of_soap", silent=True)
    mc.add_item("garden_hose", silent=True)
    mc.add_item("sponges", silent=True)
    mc.add_item("car_wash_sign", silent=True)
    mc.remove_item("bottle_of_soap", silent=True)
    mc.remove_item("garden_hose", silent=True)
    mc.remove_item("sponges", silent=True)
    mc.remove_item("car_wash_sign", silent=True)
    quest.jo_washed.finish(silent=True)
  return




## v1.33 — "Fall in Newfall" (October, 2023) ##

label quest_fall_in_newfall_variables:
  python:
    game.season = 2
    game.day+=14
    flora.outfit = {"bra": "flora_purple_bra", "panties": "flora_striped_panties", "pants": "flora_dress", "shirt": "flora_blouse"}
    isabelle.outfit = {"hat": "isabelle_tiara", "collar": "isabelle_collar" if quest.kate_stepping.finished else None, "jacket": "isabelle_jacket", "shirt": "isabelle_top", "bra": "isabelle_green_bra", "pants": "isabelle_skirt", "panties": "isabelle_green_panties"}
    jacklyn.outfit = {"choker": "jacklyn_chained_choker", "shirt": "jacklyn_jacket", "bra": "jacklyn_orange_bra", "pants": "jacklyn_shorts", "fishnet": "jacklyn_high_waist_fishnet", "panties": "jacklyn_orange_panties"}
    jo.outfit = {"glasses": "jo_glasses", "coat": "jo_coat", "shirt": "jo_shirt", "bra": "jo_white_bra", "pants": "jo_pants", "panties": "jo_white_panties"}
    kate.outfit = {"necklace": "kate_choker", "shirt": "kate_bardot_top", "bra": "kate_blue_bra", "pants": "kate_skirt", "panties": "kate_blue_panties", "boots": "kate_knee_high_boots"}
    maxine.outfit = {"hat":"maxine_hat", "glasses":"maxine_glasses", "necklace":"maxine_necklace", "shirt":"maxine_sweater", "bra":"maxine_black_bra", "pants":"maxine_skirt", "panties":"maxine_black_panties"}
    mrsl.outfit = {"coat":"mrsl_coat", "dress":"mrsl_red_dress", "bra":"mrsl_black_bra", "panties":"mrsl_black_panties"}
    nurse.outfit = {"hat":"nurse_hat", "shirt":"nurse_dress", "bra":"nurse_green_bra", "panties":"nurse_green_panties"}
    school_computer_room["unlocked"] = True
    school_computer_room["welcome_back"] = True
    school_first_hall_west["scorch_marks"] = True
    [game.timed_out_quests.add(quest_id) for quest_title,quest_id in get_available_quest_guides() if quest_id not in ("","act_one")]
    [game.quests[quest_id].finish(silent=True) for quest_title,quest_id in get_available_quest_guides() if quest_id]
    quest.act_two.start(silent=True)
    quest.fall_in_newfall.start(silent=True)
    if False: ## Placeholder ## The player has played through Lindsey's hate route ##
#     mc.add_message_to_history("hidden_number", hidden_number, "The path you are on is one of regret.", day=game.day-1, hour=23)
#     mc.add_message_to_history("hidden_number", hidden_number, "Change course now, or end up worse than before.", day=game.day-1, hour=23)
#     mc.add_message_to_history("hidden_number", mc, "I forgot the part where I asked for your opinion.", day=game.day-1, hour=23)
      pass
    elif quest.lindsey_motive.finished:
      mc.add_message_to_history("hidden_number", hidden_number, "Do not give up.", day=game.day, hour=23)
    else:
      mc.add_message_to_history("hidden_number", hidden_number, "She needs you.", day=game.day, hour=23)
    game.day+=1
    if quest.kate_stepping.finished:
      mc.add_message_to_history("kate", kate, "{image=misc isabelle_picture}", day=game.day-15, hour=20)
      mc.add_message_to_history("kate", kate, "A little thanks for the help to make this bitch heel.", day=game.day-15, hour=20)
    elif quest.isabelle_dethroning.finished and not quest.isabelle_dethroning["isabelle_caught"]:
      mc.add_phone_contact("isabelle", silent=True)
      mc.add_message_to_history("isabelle", isabelle, "Hey, how are you doing?", day=game.day-14, hour=21)
      mc.add_message_to_history("isabelle", isabelle, "Just thought I'd check in.", day=game.day-14, hour=21)
      mc.add_message_to_history("isabelle", isabelle, "Hey, I'm worried about you.", day=game.day-10, hour=12)
      mc.add_message_to_history("isabelle", isabelle, "Can we talk?", day=game.day-10, hour=12)
      mc.add_message_to_history("isabelle", isabelle, "I know what you're going through. Shutting out the world isn't the answer.", day=game.day-7, hour=19)
      mc.add_message_to_history("isabelle", isabelle, "You need to talk to someone, even if it's not me.", day=game.day-7, hour=19)
      mc.add_message_to_history("isabelle", isabelle, "Don't be too hard on yourself, okay?", day=game.day-5, hour=22)
      mc.add_message_to_history("isabelle", isabelle, "Message me back, please!", day=game.day-3, hour=8)
    quest.fall_in_newfall.finish(silent=True)
    quest.fall_in_newfall["finished_today"] = True
    quest.fall_in_newfall["finished_this_week"] = True
  return




## v1.33 — "Touched by an Angel" (October, 2023) ##

label quest_lindsey_angel_variables:
  python:
    quest.lindsey_angel.start(silent=True)
    hospital["unlocked"] = True
    mc.add_item("keycard", silent=True)
    school_science_class["lab_coat_taken"] = True
    mc.add_item("lab_coat", silent=True)
    mc.remove_item("lab_coat", silent=True)
    mc.remove_item("keycard", silent=True)
    game.day+=1
    quest.lindsey_angel.finish(silent=True)
  return




## v1.34 — "Big Witch Energy" (November, 2023) ##

label quest_maya_witch_variables(choices=False):
  python:
    quest.maya_witch.start(silent=True)
    if not school_homeroom["ball_of_yarn_taken"]:
      school_homeroom["ball_of_yarn_taken"] = True
      mc.add_item("ball_of_yarn", 5, silent=True)
    if not school_nurse_room["got_pin"]:
      school_nurse_room["got_pin"] = True
      mc.add_item("safety_pin", silent=True)
    if not mc.owned_item("high_tech_lockpick"):
      mc.remove_item("ball_of_yarn", silent=True)
      mc.remove_item("safety_pin", silent=True)
      mc.add_item("high_tech_lockpick", silent=True)
    mc.remove_item("high_tech_lockpick", silent=True)
    mc.add_item("high_tech_lockpick", silent=True)
    mc.add_item("maya_schedule", silent=True)
    school_computer_room["burns"] = True
    school_computer_room["ashes"] = True
    school_computer_room["screen1"] = False
    school_computer_room["screen2"] = False
    if not choices:
      home_computer["visited"]+=1
      black_market.remove_item("hex_vex_and_texmex")
      home_computer["hex_vex_and_texmex_ordered"] = True
      game.day+=1
      home_computer["hex_vex_and_texmex_received"] = True
      mc.add_item("package_hex_vex_and_texmex", silent=True)
      mc.remove_item("package_hex_vex_and_texmex", silent=True)
      mc.add_item("hex_vex_and_texmex", silent=True)
#     mc.add_item("bippity_boppity_bitch", silent=True)
    quest.maya_witch.finish(silent=True)
  if choices:
    call quest_maya_witch_backstory
  return

label quest_maya_witch_backstory:
  python:
    home_computer["visited"]+=1
    black_market.remove_item("hex_vex_and_texmex")
    home_computer["hex_vex_and_texmex_ordered"] = True
    game.day+=1
    home_computer["hex_vex_and_texmex_received"] = True
    mc.add_item("package_hex_vex_and_texmex", silent=True)
    mc.remove_item("package_hex_vex_and_texmex", silent=True)
    mc.add_item("hex_vex_and_texmex", silent=True)
    mc.add_item("bippity_boppity_bitch", silent=True)
    backstory.append([
      "backstory maya_witch",
      "In {color=#48F}Big Witch Energy{/}, [maxine] agreed to help you with [maya]'s curse problem on the condition\nthat you would first generate the power required to bring her computer to life. You decided to...",
      [[
        "Do physical labor in exchange for curse help",
        quest_maya_witch_backstory_maxine
        ],
      [
        "Ask the internet for help instead",
        quest_maya_witch_backstory_internet
        ]]
      ])
  return


init python:
  def quest_maya_witch_backstory_maxine():
    home_computer["visited"]-=1
    del home_computer.flags["hex_vex_and_texmex_ordered"]
    game.day-=1
    del home_computer.flags["hex_vex_and_texmex_received"]
    mc.remove_item("hex_vex_and_texmex", silent=True)
    del backstory[0]

  def quest_maya_witch_backstory_internet():
    mc.remove_item("bippity_boppity_bitch", silent=True)
    del backstory[0]




## v1.35 — "A Single Moment" (December, 2023) ##

label quest_kate_moment_variables:
  python:
    quest.kate_moment.start(silent=True)
    mc.add_item("kate_pills", silent=True)
    if nurse["strike_book_activated"]:
      nurse["strike_book"]+=4
    if not school_first_hall["trash_bin_interact"]:
      school_first_hall["trash_bin_interact"] = True
      mc.add_item("rock", silent=True)
    mc.remove_item("rock", silent=True)
    mc.add_item("rock", silent=True)
    game.day+=1
    quest.kate_moment.finish(silent=True)
  return




## v1.36 — "Suitable Romance" (January, 2024) ##

label quest_jacklyn_romance_variables(choices=False):
  python:
    quest.jacklyn_romance.start(silent=True)
    mc.add_item("ball_of_yarn_blue", silent=True)
    mc.add_item("ball_of_yarn_gray", silent=True)
    mc.add_item("ball_of_yarn_pink", silent=True)
    mc.add_item("ball_of_yarn_red", silent=True)
    mc.add_item("ball_of_yarn_yellow", silent=True)
    mc.remove_item("ball_of_yarn_blue", silent=True)
    mc.remove_item("ball_of_yarn_gray", silent=True)
    mc.remove_item("ball_of_yarn_pink", silent=True)
    mc.remove_item("ball_of_yarn_red", silent=True)
    mc.remove_item("ball_of_yarn_yellow", silent=True)
    game.day+=2
    if not choices:
#     quest.jacklyn_romance["i_wont_go"] = True
      quest.jacklyn_romance["flora_sex"] = True
#     flora["romance_disabled"] = True
#     quest.jacklyn_romance.finished = False
#     quest.jacklyn_romance.finish("done_but_sexless", silent=True)
    quest.jacklyn_romance.finish(silent=True)
  if choices:
    call quest_jacklyn_romance_backstory
  return

label quest_jacklyn_romance_backstory:
  python:
    quest.jacklyn_romance["i_wont_go"] = True
    quest.jacklyn_romance["flora_sex"] = True
    flora["romance_disabled"] = True
    quest.jacklyn_romance.finished = False
    quest.jacklyn_romance.finish("done_but_sexless", silent=True)
    backstory.append([
      "backstory jacklyn_romance",
      "In {color=#48F}Suitable Romance{/}, [flora] found out you were going to an art gallery exhibit with [jacklyn],\nbefore claiming that she wouldn't ever forgive you if you did. You decided to...",
      [[
        "Promise her not to go",
        quest_jacklyn_romance_backstory_promise
        ],
      [
        "Assure her that you're just going for the art",
        quest_jacklyn_romance_backstory_art
        ],
      [
        "Tell her to stop being such a brat",
        quest_jacklyn_romance_backstory_brat
        ]]
      ])
  return


init python:
  def quest_jacklyn_romance_backstory_promise():
    del flora.flags["romance_disabled"]
    quest.jacklyn_romance.finished = False
    quest.jacklyn_romance.finish(silent=True)
    del backstory[0]

  def quest_jacklyn_romance_backstory_art():
    del quest.jacklyn_romance.flags["i_wont_go"]
    del flora.flags["romance_disabled"]
    quest.jacklyn_romance.finished = False
    quest.jacklyn_romance.finish(silent=True)
    del backstory[0]

  def quest_jacklyn_romance_backstory_brat():
    del quest.jacklyn_romance.flags["i_wont_go"]
    del quest.jacklyn_romance.flags["flora_sex"]
    del backstory[0]




## v1.37 — "Spell You Later" (February, 2024) ##

label quest_maya_spell_variables:
  python:
    quest.maya_spell.start("book", silent=True)
    mc.add_item("kate_hair", silent=True)
#   mc.add_item("isabelle_hair", silent=True)
    mc.add_item("jacklyn_sweat", silent=True)
    if home_kitchen["cooking_pot"]:
      home_kitchen["cooking_pot_taken"] = True
    mc.add_item("cooking_pot", silent=True)
    mc.add_item("frog", silent=True)
    mc.add_item("frog", silent=True)
    mc.add_item("frog", silent=True)
    mc.add_item("frog", silent=True)
    mc.add_item("frog", silent=True)
    mc.remove_item("cooking_pot", silent=True)
    mc.remove_item("frog", 5, silent=True)
    mc.add_item("frog_cage", silent=True)
    school_cafeteria["burns"] = True
    mc.remove_item("frog_cage", silent=True)
    mc.remove_item("kate_hair", silent=True)
#   mc.remove_item("isabelle_hair", silent=True)
    mc.remove_item("jacklyn_sweat", silent=True)
    if mc.owned_item("bippity_boppity_bitch"):
      mc.remove_item("bippity_boppity_bitch", silent=True)
    elif mc.owned_item("hex_vex_and_texmex"):
      mc.remove_item("hex_vex_and_texmex", silent=True)
    game.day+=1
    quest.maya_spell.finish(silent=True)
  return




## v1.38 — "A Walk in the Park" (March, 2024) ##

label quest_flora_walk_variables:
  python:
    quest.flora_walk.start(silent=True)
    game.day+=1
    school_forest_glade["pollution"] = 0
    school_forest_glade["wrappers"] = False
    game.day+=1
    quest.flora_walk.finish(silent=True)
  return




## v1.39 — "Vicious Intrigue" (April, 2024) ##

label quest_kate_intrigue_variables:
  python:
    quest.kate_intrigue.start(silent=True)
    game.day+=1
    quest.kate_intrigue.finish(silent=True)
  return




## v1.40 — "The Grand Gesture" (May, 2024) ##

label quest_isabelle_gesture_variables:
  python:
    quest.isabelle_gesture.start(silent=True)
    mc.add_item("letter_to_isabelle", silent=True)
    mc.add_item("wildflowers", silent=True)
    quest.isabelle_gesture["manic_obsession"] = True
    game.day+=1
    mc.add_item("package_hearshey_shesay", silent=True)
#   mc.add_item("package_tob_le_bone", silent=True)
#   mc.add_item("package_dick_wonky", silent=True)
    mc.remove_item("package_hearshey_shesay", silent=True)
#   mc.remove_item("package_tob_le_bone", silent=True)
#   mc.remove_item("package_dick_wonky", silent=True)
    mc.add_item("hearshey_shesay", silent=True)
#   mc.add_item("tob_le_bone", silent=True)
#   mc.add_item("dick_wonky", silent=True)
    mc.remove_item("wildflowers", silent=True)
    mc.remove_item("hearshey_shesay", silent=True)
#   mc.remove_item("tob_le_bone", silent=True)
#   mc.remove_item("dick_wonky", silent=True)
    mc.remove_item("letter_to_isabelle", silent=True)
    quest.isabelle_gesture["girlfriend"] = isabelle["girlfriend"] = True
#   quest.isabelle_gesture["revenge"] = True
    isabelle["romance_disabled"] = False
    quest.isabelle_gesture.finish(silent=True)
  return




## v1.41 — "Don Juan Quixote" (June, 2024) ##

label quest_maya_quixote_variables:
  python:
    quest.maya_quixote.start(silent=True)
    game.day+=1
    game.day+=1
    if not home_bedroom["controller_taken"]:
      home_bedroom["controller_taken"] = True
      mc.add_item("xcube_controller", silent=True)
    if not home_hall["hole_hat"]:
      home_hall["hole_hat"] = True
      mc.add_item("cowboy_hat", silent=True)
    if not home_hall["hole_comic"]:
      home_hall["hole_comic"] = True
      mc.add_item("comic_book", silent=True)
    if not home_hall["hole_game"]:
      home_hall["hole_game"] = True
      mc.add_item("dungeons_and_damsels", silent=True)
    quest.maya_quixote.finish(silent=True)
  return




## v1.42 — "Voluntary Good" (July, 2024) ##

label quest_lindsey_voluntary_variables:
  python:
    quest.lindsey_voluntary.start(silent=True)
    school_ground_floor["secret_locker_found"] = True
    school_secret_locker["panel"] = "off"
    mc.add_item("computer", silent=True)
    school_science_class["additional_hardware"] = True
    mc.remove_item("computer", silent=True)
    mc.add_item("keycard", silent=True)
    game.day+=1
    quest.lindsey_voluntary.finish(silent=True)
  return




## v1.43 — "Paint the Town" (August, 2024) ##

label quest_jacklyn_town_variables:
  python:
    game.day+=1
    quest.jacklyn_town.start(silent=True)
    mc.add_message_to_history("jacklyn", jacklyn, "Buzz, buzz! It's me.")
    mc.add_message_to_history("jacklyn", mc, "Mario?")
    mc.add_message_to_history("jacklyn", jacklyn, "Not quite the baritone... but if that's what waxes your melon, I don't judge.")
    mc.add_message_to_history("jacklyn", mc, "Sorry! It was just a lame attempt at a joke...")
    mc.add_message_to_history("jacklyn", mc, "No melon waxing going on here.")
    mc.add_message_to_history("jacklyn", jacklyn, "Don't be a drag, be a queen!")
    mc.add_message_to_history("jacklyn", mc, "Err, maybe...")
    mc.add_message_to_history("jacklyn", jacklyn, "We'll have to fix that.")
    mc.add_message_to_history("jacklyn", mc, "Yeah?")
    mc.add_message_to_history("jacklyn", mc, "I have a feeling I'm in the right hands.")
    mc.add_message_to_history("jacklyn", jacklyn, "These hands mold more than just clay, baby bird!")
    mc.add_message_to_history("jacklyn", mc, "I knew it!")
    if persistent.time_format == "24h":
      mc.add_message_to_history("jacklyn", jacklyn, "So, the exhibit is tonight at 20:00.")
    else:
      mc.add_message_to_history("jacklyn", jacklyn, "So, the exhibit is tonight at 8:00 PM.")
    mc.add_message_to_history("jacklyn", jacklyn, "Meet me outside the pancake brothel on the marina?")
    mc.add_message_to_history("jacklyn", mc, "Oh? Interesting venue for an art exhibit...")
    mc.add_message_to_history("jacklyn", jacklyn, "It's a baller place! Just wait!")
    mc.add_message_to_history("jacklyn", mc, "I'm sure it's awesome. I'll see you tonight!")
    mc.add_message_to_history("jacklyn", jacklyn, "Aces! See you then, tiny painter.")
    jacklyn["outfit_stamp"] = jacklyn.outfit
    flora["outfit_stamp"] = flora.outfit
    quest.jacklyn_town.finish("done_jacklyn", silent=True)
    # quest.jacklyn_town.finish("done_flora", silent=True)
    # quest.jacklyn_town.finish("done_challenge", silent=True)
  return




## v1.44 — "Hot My Bot" (September, 2024) ##

label quest_mrsl_bot_variables:
  python:
    game.day+=1
    quest.mrsl_bot.start(silent=True)
    game.day+=1
    home_hall["shoebox"] = True
    home_hall["shoebox_taken"] = True
    mc.add_item("shoebox", silent=True)
    mc.remove_item("shoebox", silent=True)
    mc.remove_item("beaver_carcass", silent=True)
    mc.add_item("message_box", silent=True)
    mc.add_item("beaver_carcass", silent=True)
    mc.add_item("paper", silent=True)
    mc.add_item("pen", silent=True)
    mc.remove_item("paper", silent=True)
    mc.remove_item("pen", silent=True)
    mc.add_item("note_to_mrsl", silent=True)
    mc.add_item("pen", silent=True)
    school_homeroom["message_box"] = True
    mc.remove_item("message_box", silent=True)
    mc.remove_item("note_to_mrsl", silent=True)
    game.day+=1
    mc.add_item("note_from_mrsl", silent=True)
    mc.remove_item("note_from_mrsl", silent=True)
    mc.remove_item("pen", silent=True)
    mc.add_item("second_note_to_mrsl", silent=True)
    mc.add_item("pen", silent=True)
    mc.remove_item("second_note_to_mrsl", silent=True)
    game.day+=1
    mc.add_item("second_note_from_mrsl", silent=True)
    mc.remove_item("second_note_from_mrsl", silent=True)
    mc.remove_item("pen", silent=True)
    mc.add_item("third_note_to_mrsl", silent=True)
    mc.add_item("pen", silent=True)
    mc.remove_item("third_note_to_mrsl", silent=True)
    game.day+=1
    mc.add_item("note_from_maxine", silent=True)
    mc.remove_item("note_from_maxine", silent=True)
    game.day+=1
    mc.add_item("note_from_maya", silent=True)
    mc.remove_item("note_from_maya", silent=True)
    game.day+=1
    mc.add_item("third_note_from_mrsl", silent=True)
    mc.remove_item("third_note_from_mrsl", silent=True)
    mc.remove_item("pen", silent=True)
    mc.add_item("fourth_note_to_mrsl", silent=True)
    mc.add_item("pen", silent=True)
    mc.remove_item("fourth_note_to_mrsl", silent=True)
    game.day+=1
    mc.add_item("fourth_note_from_mrsl", silent=True)
    mc.add_item("note_from_flora", silent=True)
    mc.remove_item("note_from_flora", silent=True)
    mc.remove_item("fourth_note_from_mrsl", silent=True)
    mc.add_item("red_paint", silent=True)
    mc.remove_item("red_paint", silent=True)
    school_ground_floor["pentagram"] = True
    game.day+=1
    mc.add_item("fifth_note_from_mrsl", silent=True)
    quest.mrsl_bot.finish(silent=True)
    game.day+=1
  return




## v1.45 — "The Dive" (October, 2024) ##

label quest_maxine_dive_variables:
  python:
    quest.maxine_dive.start(silent=True)
    if not school_roof_landing["fire_axe_taken"]:
      school_roof_landing["fire_axe_taken"] = True
      mc.add_item("fire_axe", silent=True)
    if not home_hall["machete_taken"]:
      home_hall["machete_taken"] = True
      mc.add_item("machete", silent=True)
      if "quest_maxine_dive_weapons_taken" in game.events_queue:
        game.events_queue.remove("quest_maxine_dive_weapons_taken")
    if home_kitchen["cooking_pot"]:
      home_kitchen["cooking_pot_taken"] = True
      mc.add_item("cooking_pot", silent=True)
    home_kitchen["pizza_boxes"] = 4
    home_kitchen["pizza_boxes"]-=4
    mc.add_item("cardboard", silent=True)
    game.day+=1
    mc.add_item("duct_tape", silent=True)
    mc.remove_item("cardboard", silent=True)
    mc.remove_item("duct_tape", silent=True)
    mc.add_item("armor", silent=True)
    mc.remove_item("armor", silent=True)
    game.day+=1
    # quest.maxine_dive["kitchen_milk"] = True
    # quest.maxine_dive["cafeteria_milk"] = True
    quest.maxine_dive["black_market_milk"] = True
    home_computer["visited"]+=1
    black_market.remove_item("goat")
    home_computer["goat_ordered"] = True
    game.day+=1
    shrub.default_name = "Shrub"
    quest.maxine_dive["hard_part"] = True
    if not home_kitchen["water_bottle_taken"]:
      home_kitchen["water_bottle_taken"] = True
      mc.add_item("water_bottle", silent=True)
      mc.remove_item("water_bottle", silent=True)
      mc.add_item("empty_bottle", silent=True)
    mc.remove_item("empty_bottle", silent=True)
    mc.add_item("milk", silent=True)
    mc.add_item("ball_of_yarn", silent=True)
    mc.remove_item("milk", silent=True)
    mc.remove_item("fire_axe", silent=True)
    if "quest_maxine_dive_weapons_taken" in game.events_queue:
      game.events_queue.remove("quest_maxine_dive_weapons_taken")
    mc.remove_item("ball_of_yarn", silent=True)
    if not school_cave["ancient_chastity_belt_taken"]:
      school_cave["ancient_chastity_belt_taken"] = True
      mc.add_item("ancient_chastity_belt", silent=True)
    if not school_cave["locked_box_taken"]:
      school_cave["locked_box_taken"] = True
      mc.add_item("locked_box_cave", silent=True)
    quest.maxine_dive["well_earned_outcome"] = True
    # quest.maxine_dive["crab_rocks_infestation"] = False
    quest.maxine_dive["crab_rocks_infestation"] = True
    quest.maxine_dive.finish(silent=True)
    game.day+=1
  return




## v1.46 — "Service Aid" (November, 2024) ##

label quest_nurse_aid_variables:
  python:
    quest.nurse_aid.start(silent=True)
    mc.add_item("fishing_pole", silent=True)
    mc.add_item("worms", silent=True)
    mc.remove_item("fishing_pole", silent=True)
    mc.remove_item("worms", silent=True)
    mc.add_item("baited_pole", silent=True)
    mc.remove_item("baited_pole", silent=True)
    game.day+=1
    game.day+=1
    # quest.nurse_aid["dress_chosen"] = "revealing_dress"
    # quest.nurse_aid["dress_chosen"] = "sophisticated_dress"
    quest.nurse_aid["dress_chosen"] = "nice_dress"
    game.day+=1
    nurse.default_name = "Amelia"
    amelia = nurse
    quest.nurse_aid["girlfriend"] = nurse["girlfriend"] = True
    quest.nurse_aid["name_reveal"] = True
    quest.nurse_aid.finish(silent=True)
    # quest.nurse_aid.finish("done_jacklyn", silent=True)
  return




## v1.47 — "Under the Stars" (December, 2024) ##

label quest_isabelle_stars_variables:
  python:
    quest.isabelle_stars.start(silent=True)
    if not school_roof["telescope_taken"]:
      school_roof["telescope_taken"] = True
      mc.add_item("telescope", silent=True)
    if not school_roof_landing["fire_axe_taken"]:
      school_roof_landing["fire_axe_taken"] = True
      mc.add_item("fire_axe", silent=True)
    mc.remove_item("fire_axe", silent=True)
    mc.add_item("small_pile_of_wood", silent=True)
    # mc.add_item("medium_pile_of_wood", silent=True)
    # mc.add_item("big_pile_of_wood", silent=True)
    mc.add_item("fire_axe", silent=True)
    # mc.add_item("fish_and_chips", silent=True)
    # mc.add_item("shrimp_and_lobster", silent=True)
    # mc.add_item("hotdog", silent=True)
    quest.isabelle_stars["meal"] = "spaghetti"
    mc.add_item("spaghetti", silent=True)
    # quest.isabelle_stars["meal"] = "sandwich"
    # mc.add_item("sandwich", silent=True)
    # quest.isabelle_stars["meal"] = "fruit"
    # mc.add_item("grapes", silent=True)
    mc.add_message_to_history("isabelle", mc, "As promised, your evening out awaits, m'lady.")
    mc.add_message_to_history("isabelle", mc, "Meet me in the park after school?")
    mc.add_message_to_history("isabelle", isabelle, "The park? Interesting choice of venue this time of the year.")
    mc.add_message_to_history("isabelle", mc, "I promise it won't disappoint!")
    mc.add_message_to_history("isabelle", isabelle, "Exciting! I'll be there!")
    mc.add_message_to_history("isabelle", isabelle, "{image=phone emojis relaxed}")
    school_park["telescope"] = True
    mc.remove_item("telescope", silent=True)
    school_park["blanket"] = True
    school_park["small_pile_of_wood"] = True
    mc.remove_item("small_pile_of_wood", silent=True)
    # school_park["medium_pile_of_wood"] = True
    # mc.remove_item("medium_pile_of_wood", silent=True)
    # school_park["big_pile_of_wood"] = True
    # mc.remove_item("big_pile_of_wood", silent=True)
    # mc.remove_item("fish_and_chips", silent=True)
    # mc.remove_item("shrimp_and_lobster", silent=True)
    # mc.remove_item("hotdog", silent=True)
    mc.remove_item("spaghetti", silent=True)
    # mc.remove_item("sandwich", silent=True)
    # mc.remove_item("grapes", silent=True)
    quest.isabelle_stars["girlfriend"] = isabelle["girlfriend"] = True
    quest.isabelle_stars.finish(silent=True)
    # quest.isabelle_stars.finish("done_feelings_rejected", silent=True)
    game.day+=1
  return




## v1.48 — "Sauce/Off" (January, 2025) ##

label quest_maya_sauce_variables:
  python:
    game.day+=1
    quest.maya_sauce.start(silent=True)
    game.day+=1
    quest.maya_sauce["bedroom_taken_over"] = True
    # quest.maya_sauce.finish(silent=True)
    quest.maya_sauce.finish("done_but_bedroom_taken_over", silent=True)
  return
