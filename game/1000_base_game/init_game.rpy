label init_game:
  ## This label called when player click "New Game" at main menu.
  ## We setup important variables and inform all interested by sending "init" event.
  ## During processing of this "init" event all locations, characters and quests are actually created.
  ## See engine/system_events/on_init.rpy
  python:
    automatically_update_state=0
    game_version=current_game_version
    game=Game()
    game.activity_selector="roaming"
    game.hour=7
    process_event("init")
    game.pc=mc
    game.location=home_bedroom
    automatically_update_state=1
  return
