﻿screen todo_note():
  layer "master"
  add "misc todolist"
  if renpy.seen_label("isabelle_quest_isabelle_tour_cafeteria_ending"):
    text "{s}1) Be the tour guide from the underworld.{/s}" style "to_do_text" pos (730,70) at Transform(rotate=10)    
  else: 
    text "1)Be the tour guide from the underworld." style "to_do_text" pos (730,70) at Transform(rotate=10)    
  
  if renpy.seen_label("kate_kiss_shoe_scene"):
    text "{s}2) Study the sole.{/s}" style "to_do_text" pos (730,240) at Transform(rotate=20)    
  else: 
    text "2)Study the sole." style "to_do_text" pos (730,240) at Transform(rotate=20)    
  
  if renpy.seen_label("jacklyn_quest_art_focus_start"):
    text "{s}3) Take a class in string theory.{/s}" style "to_do_text" pos (700,200) at Transform(rotate=10)    
  else: 
    text "3)Take a class in string theory." style "to_do_text" pos (700,200) at Transform(rotate=10)    
  
  if renpy.seen_label("isabelle_quest_isabelle_tour_peak_lindsey"):
    text "{s}4) Introduced myself to the twins.{/s}" style "to_do_text" pos (700,220) at Transform(rotate=9)    
  else: 
    text "4) Introduced myself to the twins." style "to_do_text" pos (700,220) at Transform(rotate=9)    
  
  if renpy.seen_label("isabelle_quest_isabelle_tour_nurse_blackmail"):
    text "{s}5) Commit a casual felony.{/s}" style "to_do_text" pos (700,300) at Transform(rotate=10)    
  else: 
    text "5)Commit a casual felony." style "to_do_text" pos (700,300) at Transform(rotate=10)    
  
  if renpy.seen_label("flora_quest_flora_cooking_got_liquid"):
    text "{s}6) Find out if a flower can bloom\nunder extremely moist conditions.{/s}" style "to_do_text" pos (700,325) at Transform(rotate=10)    
  else: 
    text "6)Find out if a flower can bloom\nunder extremely moist conditions." style "to_do_text" pos (700,325) at Transform(rotate=10)    
  
  if renpy.seen_label("quest_flora_jacklyn_introduction_outside"):
    text "{s}7) Find out if she dabbles.{/s}" style "to_do_text" pos (700,450) at Transform(rotate=10)    
  else: 
    text "7)Find out if she dabbles." style "to_do_text" pos (700,450) at Transform(rotate=10)    
  
  if renpy.seen_label("isabelle_quest_haggis_flora_isabelle_kiss"):
    text "{s}8) Participate in a team-building exercise.{/s}" style "to_do_text" pos (700,425) at Transform(rotate=10)    
  else: 
    text "8)Participate in a team-building exercise." style "to_do_text" pos (700,425) at Transform(rotate=10)    
  
  if renpy.seen_label("kate_quest_search_for_nurse_meet_kate"):
    text "{s}9) Go spelunking with a buddy\nand a medical professional.{/s}" style "to_do_text" pos (700,540) at Transform(rotate=10)    
  else: 
    text "9)Go spelunking with a buddy\nand a medical professional." style "to_do_text" pos (700,540) at Transform(rotate=10)    
  
  if renpy.seen_label("quest_lindsey_book_easel_end"):
    text "{s}10) Join a book club.{/s}" style "to_do_text" pos (700,660) at Transform(rotate=10)    
  else: 
    text "10)Join a book club." style "to_do_text" pos (700,660) at Transform(rotate=10)    
  
  if renpy.seen_label("home_bedroom_computer_interact_mrsl_HOT_research"):
    text "{s}11) Be kind. Rewind.{/s}" style "to_do_text" pos (700,720) at Transform(rotate=10)    
  else: 
    text "11)Be kind. Rewind." style "to_do_text" pos (700,720) at Transform(rotate=10)    

  if renpy.seen_label("isabelle_quest_stolen_slap"):
    text "{s}12) Full Skin on Skin contact.{/s}" style "to_do_text" pos (700,720) at Transform(rotate=10)    
  else: 
    text "12)Full Skin on Skin contact." style "to_do_text" pos (700,720) at Transform(rotate=10)    


  #if not quest.smash_or_pass.finished:
  #  add "items mysterious_coin" pos (700,300)
  #else:
  #  add "items bottle water_bottle" pos (700,300)
  #if not quest.isabelle_tour.finished:
  #  text "I need to do something with Kate" style "to_do_text" pos (650,400) at Transform(rotate=15)
  #if quest.isabelle_tour.finished:
  #    text "{s}I need to do something with Kate{/s}" style "to_do_text" at Transform(rotate=15)    

  button:
    action Return()
  key "game_menu" action Return()
