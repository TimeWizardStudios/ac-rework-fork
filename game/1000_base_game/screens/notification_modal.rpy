style notification_modal_frame_default is frame:
  background Frame("ui frame_popup",220,160,240,150)
  padding (80,60,80,100)
  align (0.5,0.5)
  xminimum 500
  xmaximum 900

style notification_modal_frame_achievement is frame:
  background Frame("ui frame_popup",220,160,240,150)
#  background Frame("ui notification modal_frame_achievement",220,160,240,150)
  padding (80,60,80,100)
  align (0.5,0.5)
  xminimum 500
  xmaximum 800

style notification_modal_frame_quest is frame:
  background Frame("ui notification modal_frame_quest",220,156,240,174)
  padding (80,60,80,100)
  align (0.5,0.5)
  yminimum 350
  xminimum 500
  xmaximum 800

style notification_modal_title_frame is frame:
  background Frame("ui notification modal_frame_title",128,0,64,0)
  padding (128,36,64,36)
  xminimum 200
  ysize 130
  xalign 0.5

style notification_modal_title is ui_text:
  align (0.5,0.5)
  text_align 0.5

style notification_modal_text is ui_text:
  xalign 0.5
  text_align 0.5

transform tf_notification_modal:
  on show,replace:
    alpha 0.0
    easein 0.25 alpha 1.0
  on hide,replaced:
    easeout 0.25 alpha 0.0

transform tf_transformless:
  on hide,replaced:
    easeout 0.25 alpha 0.0

screen notification_modal(bg_type,title,content,wait=None,_transform=True):
  zorder 98
  style_prefix "notification_modal"
  key "game_menu" action Return()
  if wait:
    timer wait action Return()
  button:
    action Return()
  add "#0008"
  $frame_style="notification_modal_frame_"+(bg_type if bg_type else "default")
  vbox:
    if _transform:
      at tf_notification_modal
    else:
      at tf_transformless
    align (0.5,0.4)
    spacing 16
    if title:
      frame style "notification_modal_title_frame":
        text title style "notification_modal_title"
    frame style frame_style:
      hbox:
        xalign 0.5
        for part in content.split("|"):
          if part.startswith("$"):
            add part[1:] yalign 0.5
          else:
            vbox:
              yalign 0.5
              spacing 8
              for n,part in enumerate(part.split("{hr}")):
                if n:
                  fixed:
                    xfill True
                    ysize 5
                    add Solid("#ab835e")
                text part xalign 0.5:
                  if title == "Inventory":
                    xmaximum 370
