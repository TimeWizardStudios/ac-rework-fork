image lindsey_phone:
  xsize 515 ysize 1021
  contains:
    "phone case"
    xoffset -15 yoffset -15
  contains:
    "phone bg"
  contains:
    "phone keychains"
    xoffset -138 yoffset 42


screen phone(app, *app_args, phone_place=None, char=None, **kwargs):
  default show = False

  zorder 5
  tag game_mode

  python:
    char = char or game.pc

    if isinstance(char, basestring):
      char = game.characters[char]

    pos, anchor, offset = normalize_place(phone_place or [], 0.5, 0.5, 0.5, 0.5, 0, 0)

    if isinstance(app, basestring):
      for installed_app in char.phone_apps:
        if installed_app == app:
          app = installed_app

          break

  add "#0008" at phone_backdrop_fade(phone_is_showing)

  ## hiders
  if game.ui.can_close_phone:
    if phone_is_showing:
      key "game_menu" action [SetVariable("phone_clicked_to_hide", True), Hide("phone")]

      button:
        action [SetVariable("phone_clicked_to_hide", True), Hide("phone")]

    else:
      key "game_menu" action NullAction()

  fixed:
    pos pos
    anchor anchor
    offset offset
    fit_first True

    add ("lindsey_phone" if game.pc == "lindsey" else "phone bg")

    ## Supresses the hiders
    button:
      action NullAction()

    ## current app area
    fixed:
      pos (30, 148)
      fit_first True

      add "phone app_bg"

      ## app content
      ## Viewport for a possible slide transition.
      viewport:
        xfill True
        yfill True

        if app.screen:
          use expression app.screen pass (*app_args, **kwargs)

    at phone_slide_in_and_out(phone_is_showing)

default phone_is_showing = False
default phone_clicked_to_hide = False

init python:
  def phoneSlideInFunction(trans, st, at):
    store.phone_is_showing = True
    renpy.restart_interaction()

  def phoneSlideOutFunction(trans, st, at):
    store.phone_is_showing = False
    renpy.restart_interaction()

    if store.phone_clicked_to_hide:
      renpy.return_statement(setattr(store, "phone_clicked_to_hide", False))

transform phone_slide_in_and_out(phone_is_showing_):
  on show:
    (phone_slide_in(phone_is_showing_) if not phone_is_showing_ is None else 0.0)

  on hide:
    (phone_slide_out(phone_is_showing_) if phone_is_showing_ else 0.0)

transform phone_slide_in(phone_is_showing_):
  yoffset (config.screen_height if not phone_is_showing_ else 0)
  easein 0.50 yoffset 0
  function phoneSlideInFunction

transform phone_slide_out(phone_is_showing_):
  easeout 0.50 yoffset config.screen_height
  function phoneSlideOutFunction

transform phone_backdrop_fade(phone_is_showing_, fade_duration=0.5):
  on show:
    alpha (0.0 if not phone_is_showing_ else 1.0)
    easein fade_duration alpha 1.0

  on hide:
    easeout fade_duration alpha 0.0
