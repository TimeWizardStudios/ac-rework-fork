﻿style quest_guide_marker_text is ui_text_small:
  align (0.5,0.5)
  text_align 0.5
  color "#FFF"
  outlines [(2,"#000")]
  layout "nobreak"

style quest_guide_select_frame is frame:
  background Frame("ui dialog choice",100,0,150,0)
  hover_background Frame("ui dialog choice_hover",100,0,150,0)
  insensitive_background Frame(ElementDisplayable("ui dialog choice","inactive"),100,0,150,0)
  padding (100,30,140,30)
  xsize 640
  size_group "menu_choice"

style quest_guide_select_title is ui_text_small:
  font "fonts/ComicHelvetic_Medium.otf"
  color "#fefefe"
  size 28
  xalign 0.5
  text_align 0.5
  yalign 0.5
  outlines [(2,"#432a12",0,0)]
  line_spacing 3

transform tf_quest_guide_marker(anchor,rotate,offset,hbox,ofs_per_element):
  anchor anchor
  easein 0.35 offset offset
  easeout 0.35 offset (0,0)
  repeat

define quest_guide_marker_places={
  "left_top": ((0.0,0.0),135,(30,30),True,0),
  "mid_top": ((0.5,0.0),180,(0,30),True,0),
  "right_top": ((1.0,0.0),225,(-30,30),True,0),
  "left_mid": ((0.0,0.5),90,(30,0),False,0),
  "mid_mid": ((1.0,1.0),315,(-30,-30),True,0),
  "right_mid": ((1.0,0.5),270,(-30,0),False,0),
  "left_bottom": ((0.0,1.0),45,(30,-30),True,0),
  "mid_bottom": ((0.5,1.0),0,(0,-30),True,0),
  "right_bottom": ((1.0,1.0),315,(-30,-30),True,0),
  }

screen quest_guide_select():
  zorder 10
  button:
    action Return("$cancel")
  key "game_menu" action Return("$cancel")
  default default_per_col=8
  fixed:
    ysize 960
    yalign 1.0
    viewport:
      mousewheel True
      pagekeys True
      xfill False
      yfill False
      ymaximum 960
      align (0.5,0.5)
      $guides=get_available_quest_guides()
      $columns=min(3,(len(guides)+default_per_col-1)//default_per_col)
      $real_per_col=(len(guides)+columns-1)//columns
      hbox:
        for col in range(columns):
          vbox:
            for n in range(real_per_col):
              $n=col*real_per_col+n
              if n<len(guides):
                button:
                  ypadding 4
                  frame style "quest_guide_select_frame":
                    text guides[n][0] style "quest_guide_select_title"
                  if quest.lindsey_nurse.in_progress:
                    if guides[n][0] not in ("Tour de School","Dethroning the Queen","Stepping on the Rose"):
                      action Return(guides[n][1])
                  elif quest.isabelle_haggis in ("instructions","puzzle","escape","choice"):
                    if guides[n][0] in ("Off","No True Scotswoman"):
                      action Return(guides[n][1])
                  elif quest.flora_bonsai.in_progress:
                    if guides[n][0] in ("Off","Tiny Thirsty Tree"):
                      action Return(guides[n][1])
                  elif quest.lindsey_wrong in ("mop","doughnuts","clean","lindseyart","florahelp","floralocker","lindseyclothes","verywet","guard","mopforkate","cleanforkate"):
                    if guides[n][0] in ("Off","Nothing Wrong With Me"):
                      action Return(guides[n][1])
                  elif "endnurse" > quest.kate_desire > "gym":
                    if guides[n][0] in ("Off","Twisted Desire"):
                      action Return(guides[n][1])
                  elif quest.maxine_hook == "shovel":
                    if guides[n][0] in ("Off","Hooking Up"):
                      action Return(guides[n][1])
                  elif quest.flora_squid in ("search","choice","wait","bathroom","forest_glade"):
                    if guides[n][0] in ("Off","Squid Game"):
                      action Return(guides[n][1])
                  elif quest.nurse_venting > "ready" and quest.nurse_venting.in_progress:
                    if guides[n][0] in ("Off","Venting Frustration"):
                      action Return(guides[n][1])
                  elif quest.isabelle_hurricane == "short_circuit" and quest.isabelle_hurricane["darkness"]:
                    if guides[n][0] in ("Off","Hurricane Isabelle"):
                      action Return(guides[n][1])
                  elif quest.kate_trick in ("school","gym","workout"):
                    if guides[n][0] in ("Off","The Dog Trick"):
                      action Return(guides[n][1])
                  elif quest.isabelle_dethroning in ("riddle","trap","revenge","revenge_done","panik","fly_to_the_moon") or (quest.isabelle_dethroning == "dinner" and game.hour == 19):
                    if guides[n][0] in ("Off","Dethroning the Queen"):
                      action Return(guides[n][1])
                  elif quest.kate_stepping == "fly_to_the_moon":
                    if guides[n][0] in ("Off","Stepping on the Rose"):
                      action Return(guides[n][1])
                  elif quest.jo_washed.in_progress:
                    if guides[n][0] in ("Off","Washed Up"):
                      action Return(guides[n][1])
                  elif quest.lindsey_angel in ("keycard","off_limits","phone_book","lab_coat","escape","hospital"):
                    if guides[n][0] in ("Off","Touched by an Angel"):
                      action Return(guides[n][1])
                  elif quest.maya_witch in ("locked","phone_call","wait","meet_up"):
                    if guides[n][0] in ("Off","Big Witch Energy"):
                      action Return(guides[n][1])
                  elif quest.kate_moment.in_progress and quest.isabelle_gesture.in_progress:
                    if guides[n][0] in ("Off","A Single Moment","The Grand Gesture"):
                      action Return(guides[n][1])
                  elif quest.kate_moment.in_progress:
                    if guides[n][0] in ("Off","A Single Moment"):
                      action Return(guides[n][1])
                  elif quest.maya_spell in ("paint","statue"):
                    if guides[n][0] in ("Off","Spell You Later"):
                      action Return(guides[n][1])
                  elif quest.flora_walk.in_progress:
                    if guides[n][0] in ("Off","A Walk in the Park"):
                      action Return(guides[n][1])
                  elif quest.kate_intrigue == "lion_den":
                    if guides[n][0] in ("Off","Vicious Intrigue"):
                      action Return(guides[n][1])
                  elif quest.isabelle_gesture in ("wait","revenge"):
                    if guides[n][0] in ("Off","The Grand Gesture"):
                      action Return(guides[n][1])
                  elif quest.maya_quixote in ("text","power_outage","attic","board_game"):
                    if guides[n][0] in ("Off","Don Juan Quixote"):
                      action Return(guides[n][1])
                  elif quest.lindsey_voluntary == "dream":
                    if guides[n][0] in ("Off","Voluntary Good"):
                      action Return(guides[n][1])
                  elif quest.mrsl_bot in ("dream","meeting","diversion"):
                    if guides[n][0] in ("Off","Hot My Bot"):
                      action Return(guides[n][1])
                  elif quest.maxine_dive == "cave":
                    if guides[n][0] in ("Off","The Dive"):
                      action Return(guides[n][1])
                  elif quest.nurse_aid == "shopping" and game.location == "dress_shop":
                    if guides[n][0] in ("Off","Service Aid"):
                      action Return(guides[n][1])
                  else:
                    action Return(guides[n][1])

screen quest_guide():
  if game.quest_guide:
    if mc["focus"] and game.quest_guide != (mc["focus"].split("@")[1] if "@" in mc["focus"] else mc["focus"]) and not (quest.kate_moment == "coming_soon" and quest.isabelle_gesture.in_progress):
      $marker=process_event("quest_guide_act_one",default_rv=(None,None))
    else:
      $marker=process_event("quest_guide_"+game.quest_guide,default_rv=(None,None))
    if marker:
      python:
        if len(marker)>1:
          if not isinstance(marker[1],(list,tuple)) or (isinstance(marker[1][0],(list,tuple)) and isinstance(marker[1][0][0],int)):
            marker=[marker]
        unique_markers={}
        for m in marker:
          if m[1]:
            if m[0] or m[1][0] not in unique_markers:
              unique_markers[m[1][0]]=m
        marker=[m for m in unique_markers.values()]
      for marker_img,marker_pos in marker:
        if marker_pos:
          python:
            marker_pos,marker_sector=marker_pos
            if not marker_sector:
              marker_sector="left_" if marker_pos[0]<250 else "right_" if marker_pos[0]>config.screen_width-250 else "mid_"
              marker_sector+="top" if marker_pos[1]<250 else "bottom" if marker_pos[1]>config.screen_height-250 else "mid"
            place=quest_guide_marker_places[marker_sector]
            if marker_img is None:
              marker_img=[]
            elif isinstance(marker_img,basestring):
              marker_img=[marker_img]
          fixed:
            pos marker_pos
            at tf_quest_guide_marker(*place)
            xysize (125,125)
            #if not show_arrow:
            add "ui quest_guide_marker" anchor (0.5,0.5) pos place[0] rotate place[1]
            if place[3]:
              hbox:
                spacing 8
                align (0.5,0.5)
                for n,img in enumerate(marker_img):
                  python:
                    img,_,img_mod=img.partition("@")
                    img_mod=img_mod.split(",")
                    img_zoom=float(img_mod[0]) if len(img_mod)>0 and img_mod[0] else 1.0
                    img_rotate=float(img_mod[1]) if len(img_mod)>1 and img_mod[1] else 0
                  if img.startswith("$"):
                    text img[1:] style "quest_guide_marker_text" yoffset place[4]*n:
                      at Transform(zoom=img_zoom,rotate=img_rotate)
                  else:
                    add img align(0.5,0.5) yoffset place[4]*n:
                      at Transform(zoom=img_zoom,rotate=img_rotate)
            else:
              vbox:
                spacing 8
                align (0.5,0.5)
                for n,img in enumerate(marker_img):
                  $img,_,img_zoom=img.partition("@")
                  if img.startswith("$"):
                    text img[1:] style "quest_guide_marker_text" xoffset place[4]*n:
                      if img_zoom:
                        at Transform(zoom=float(img_zoom))
                  else:
                    add img align(0.5,0.5) xoffset place[4]*n:
                      if img_zoom:
                        at Transform(zoom=float(img_zoom))
