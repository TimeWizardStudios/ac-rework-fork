image backstory_caret:
  HBox(Solid("0000",xsize=3,ysize=33), Solid("#fefefe",xsize=2,ysize=33), Solid("0000",xsize=3,ysize=33))
  linear 0.1 alpha 0.0
  pause 0.5
  linear 0.1 alpha 1.0
  pause 0.5
  repeat


screen backstory_creation:
  zorder 9
  add "#000"

  default clickable = False

  fixed:
    if not clickable:
      at transform:
        alpha 0.0 xoffset 50
        pause 0.25
        easein 0.25 alpha 1.0 xoffset 0

    python:
      icon = backstory[0][0]
      event = backstory[0][1]
      intro = "intro" in icon
      choices = None if intro else backstory[0][2]

    vbox spacing 70 xalign 0.5 yalign (0.4125 if intro else 0.45):
      text "YOUR STORY SO FAR" font "fonts/Fresca-Regular.ttf" color "#fefefe" size 56 xalign 0.5
      add icon xalign 0.5
      text event font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      if intro:
        input value FieldInputValue(mc,"name") font "fonts/ComicHelvetic_Medium.otf" color "#09CD4D" size 42 caret "backstory_caret" pixel_width 400 allow "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -'`" xalign 0.5
      else:
        hbox spacing 20 xalign 0.5:
          for text,function in choices:
            textbutton text.strip("#") text_font "fonts/ComicHelvetic_Medium.otf" text_color ("#222" if text.startswith("#") else "#fefefe") text_hover_color "#000" text_size 28 background (AlphaMask("#222", Frame("backstory _idle",20,20,20,20)) if text.startswith("#") else Frame("backstory _idle",20,20,20,20)) hover_background Frame("backstory _hover",20,20,20,20) xminimum 350 xpadding 70 ysize 70 action If(clickable and not text.startswith("#"), [Function(function), Return()])

    if intro:
      add Solid("#fefefe",xsize=350,ysize=2) xalign 0.5 yalign 0.825
      key "K_RETURN" action If(clickable and mc.name.strip(), [Function(intro_backstory), Return()])
      textbutton "Continue" text_font "fonts/ComicHelvetic_Medium.otf" text_color ("#fefefe69" if clickable and mc.name.strip() else "#000") text_hover_color "#fefefe" text_size 21 xalign 0.5 yalign 0.95 action If(clickable and mc.name.strip(), [Function(intro_backstory), Return()])
    else:
      textbutton "Back" text_font "fonts/ComicHelvetic_Medium.otf" text_color "#fefefe69" text_hover_color "#fefefe" text_size 21 xalign 0.5 yalign 0.95 action If(clickable, Rollback())
    textbutton "...or skip remaining choices!\n(we'll choose them for you)" text_font "fonts/ComicHelvetic_Medium.otf" text_color "#fefefe69" text_hover_color "#fefefe" text_size 21 xalign 0.95 yalign 0.95 action If(clickable, [Function(skip_backstory_choices), Return()])

    timer 0.5 action SetScreenVariable("clickable",True)
