init -500 python:
  class ElementDisplayable(renpy.Displayable):
    def __init__(self,img,mode=None,*args,**kwargs):
      self.mode_extra=kwargs.pop("mode_extra",None)
      self.no_render=kwargs.pop("no_render",None)
      super().__init__(*args,**kwargs)
      self.img=img
      self.mode=mode
    # controls how all the items are rendered (bed,computer, flash_drive, etc)
    def render(self,width,height,st,at):
      img=renpy.easy.displayable(self.img)
      while not isinstance(img,(renpy.display.im.Image,renpy.display.transform.ATLTransform,Text)):
        while not isinstance(img,(renpy.display.im.Image,DynamicImage,DynamicDisplayable,renpy.display.transform.ATLTransform)):
          img_target=img._target()
          if img_target==img:
            break
          img=img_target
        if isinstance(img,DynamicImage):
          img=renpy.easy.dynamic_image(img.name)
        elif isinstance(img,DynamicDisplayable):
          img,redraw=img.function(st,at,*img.args,**img.kwargs)
          img=renpy.easy.displayable(img)
      if isinstance(img,(renpy.display.transform.ATLTransform,Text)):
        imgr=img.render(width,height,st,at)
        placement=img.get_placement()
        ofsx,ofsy=renpy.display.core.place(width,height,imgr.width,imgr.height,placement)
        rv=renpy.Render(imgr.width,imgr.height)
        rv.blit(imgr,(ofsx,ofsy))
        return rv
      else:
        if self.mode=="hover":
          hover_level=1.0 if self.mode_extra is None else self.mode_extra
          img=im.MatrixColor(img,im.matrix.brightness(0.15*hover_level))
        elif self.mode in ("gray","grey"):
          img=im.MatrixColor(img,im.matrix.saturation(0.15)*im.matrix.contrast(0.33))
        elif self.mode=="inactive":
          img=im.MatrixColor(img,im.matrix.saturation(0.15)*im.matrix.contrast(0.33))
        elif self.mode=="colourise":
          img=im.MatrixColor(img,im.matrix.colorize("#917046","#fffbfb"))##You can recolour black and white images as you like here "black_color","white_color"
        elif self.mode=="colourise_hover":
          hover_level=1.0 if self.mode_extra is None else self.mode_extra
          img=im.MatrixColor(img,im.matrix.colorize("#967041","#fffbfb")*im.matrix.brightness(0.15*hover_level))
      if isinstance(img,im.MatrixColor) or not self.no_render:
        img=renpy.render(img,width,height,st,at)
      return img
