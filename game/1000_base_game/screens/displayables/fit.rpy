﻿init python:
  class FitDisplayable(renpy.Displayable):
    def __init__(self,img,size=None,align=None,padding=None,allow_upscale=True,bg=None,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.img=img
      self.isize=size
      self.ialign=align
      self.ipadding=padding
      self.allow_upscale=allow_upscale
      self.bg=bg
    def render(self,width,height,st,at):
      rw,rh=w,h=self.isize or (width,height)
      align=self.ialign or (0.0,0.0)
      padding=self.ipadding or (0,0,0,0)
      if len(padding)==2:
        padding=(padding[0],padding[1],padding[0],padding[1])
      w-=padding[0]+padding[2]
      h-=padding[1]+padding[3]
      img=renpy.displayable(self.img)
      imgr=img.render(w,h,st,at)
      iw,ih=imgr.get_size()
      if iw and ih:
        zoom=min(float(w)/iw,float(h)/ih)
        if zoom!=1.0:
          if self.allow_upscale or zoom<1.0:
            img=Transform(img,zoom=zoom)
            imgr=img.render(w,h,st,at)
            iw,ih=imgr.get_size()
      x,y=renpy.display.core.place(w,h,iw,ih,(align[0],align[1],align[0],align[1],padding[0],padding[1],True))
      rv=renpy.Render(rw,rh)
      if self.bg:
        bg=renpy.displayable(self.bg).render(rw,rh,st,at)
        rv.blit(bg,(0,0))
      rv.blit(imgr,(x,y))
      renpy.redraw(self,0)
      return rv
