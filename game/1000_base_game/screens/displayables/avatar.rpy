init -500 python:

  class Event_avatar_after_load(GameEvent):
    def on_init(event):
      for char_id in game.characters:
        renpy.get_registered_image(char_id).reset_layers()
    def on_after_load(event):
      for char_id in game.characters:
        renpy.get_registered_image(char_id).reset_layers()

  class LayeredDisplayable(renpy.Displayable):
    def __init__(self,layers,w=None,h=None,render_vars=None,**kwargs):
      super().__init__(**kwargs)
      self.layers=[layers] if isinstance(layers,basestring) else layers
      self.render_size=None if None in (w,h) else (w,h)
      self.render_vars=render_vars or {}
    def render(self,width,height,st,at):
      vars={}
      for var,val in self.render_vars.items():
        vars[var]=get_variable_at(var)
        set_variable_at(var,val)
      layers=[]
      rv_w=0
      rv_h=0
      for layer in self.layers:
        if isinstance(layer,basestring):
          layer=[layer,0,0]
        elif len(layer)==2:
          rv_w,rv_h=layer
          continue
        img,x,y=layer
        img=renpy.easy.displayable(img)
        img=img.render(width,height,st,at)
        w,h=img.get_size()
        layers.append([x,y,w,h,img])
        rv_w=max(rv_w,x+w)
        rv_h=max(rv_h,y+h)
      if self.render_size:
        rv_w,rv_h=self.render_size
      rv=renpy.Render(rv_w,rv_h)
      for x,y,w,h,img in layers:
        rv.blit(img,(x,y))
      for var in self.render_vars:
        val=vars[var]
        if val is not NotFound:
          set_variable_at(var,val)
      return rv

  class AvatarDisplayable(renpy.Displayable):
    def __init__(self,char_id,char_state=None,**kwargs):
      super().__init__(**kwargs)
      self.char_id=char_id
      self.char_state=char_state
      self.mouse_pos=(0,0)
      self.last_time=0.0
      self.alpha_per_sec=2.0
      self.layers=[]
    _duplicatable=True
    def _duplicate(self,img_args,*args,**kwargs):
      attributes=[attr for attr in img_args.args if not attr.startswith("-")]
      return AvatarDisplayable(self.char_id," ".join(attributes),**kwargs)
    def reset_layers(self):
      self.layers=[]
    def render(self,width,height,st,at):
      if hasattr(store,"game"):
        ## get current avatar layers
        char=game.characters[self.char_id]
        char_state,char_flipped,char_state_extra=self.char_state.partition(" flip")
        char_state+=char_state_extra
        char_state,char_video,char_state_extra=self.char_state.partition(" video")
        char_state+=char_state_extra
        avatar_layers=char.avatar(char_state)
        if char_video: ## separate, unlayered logic for video sprites because they lack transparency. (unless done in editing)
          rv_w,rv_h=0,0
          renders=[]
          if isinstance(avatar_layers[-1],basestring):
            layer=[avatar_layers[-1],0,0]#checking if pos is supplies
          else:
            layer=avatar_layers[-1]
          video,x,y=layer
          ## putting together the filename
          video = str("images/characters/"+"/".join(video.split())+xray_fn_suffix[game.xray_mode]+".webm")
          video = video if renpy.loadable(video) else video.replace("_xray","").replace("_full","") ## if there are no additional "_xray" and "_xray_full" files, keep displaying the original video
          states={}
          lrn = Movie(fps=24,channel=u'sprite',play=video,side_mask=False, loop=True)
          if char_flipped: ## flip the movie if the flip flag is there
            lrn=Transform(lrn,xzoom=-1.0)
          lrn=lrn.render(width,height,st,at) ## render object
          states[False] = lrn
          if game.xray_mode:
            lrx = Movie(fps=24,channel=u'sprite',play=video,side_mask=False, loop=True)
            if char_flipped:
              lrx=Transform(lrx,xzoom=-1.0)
            lrx=lrx.render(width,height,st,at)
            states[True] = lrx
          s_w,s_h=lrn.get_size()
          rv_w=max(rv_w,s_w)
          rv_h=max(rv_h,s_h)
          renders.append(states)
          rv=renpy.Render(rv_w,rv_h,opaque=False)
          rv.blit(renders[-1][bool(game.xray_mode)],(x,y),focus=True, main=False)
        else: ## regular layered logic that calls LayeredDisplayable
          ## add new layers if currently empty or changed
          if not self.layers:
            self.layers.append([1.0,avatar_layers])
          elif self.layers[-1][1]!=avatar_layers:
            self.layers.append([0.0,avatar_layers])
          ## update internal time counter
          time_delta=min(0.1,max(0.0,st-self.last_time))
          alpha_delta=time_delta*self.alpha_per_sec
          self.last_time=st
          ## update layers alpha levels, remove if invisible
          if alpha_delta>0:
            self.layers[-1][0]=min(1.0,self.layers[-1][0]+alpha_delta)
            found_opaque=False
            for n,(alpha,layers) in enumerate(reversed(self.layers)):
              if found_opaque:
                self.layers[len(self.layers)-n-1][0]=max(0.0,alpha-alpha_delta)
              elif alpha>=1.0:
                found_opaque=True
            self.layers=[[alpha,layers] for n,(alpha,layers) in enumerate(self.layers) if n==len(self.layers)-1 or alpha>0]
          ## prepare renders
          rv_w,rv_h=0,0
          renders=[]
          for n,(alpha,layers) in enumerate(self.layers):
            states={}
            lrn=LayeredDisplayable(layers,render_vars={"game.xray_mode":0})
            if char_flipped:
              lrn=Transform(lrn,xzoom=-1.0)
            lrn=lrn.render(width,height,st,at)
            s_w,s_h=lrn.get_size()
            states[False]=lrn
            rv_w=max(rv_w,s_w)
            rv_h=max(rv_h,s_h)
            if game.xray_mode:
              lrx=LayeredDisplayable(layers,render_vars={"game.xray_mode":game.xray_mode})
              if char_flipped:
                lrx=Transform(lrx,xzoom=-1.0)
              lrx=lrx.render(width,height,st,at)
              xr=renpy.Render(s_w,s_h,opaque=False)
              mask=renpy.easy.displayable("ui xray_mask").render(s_w,s_h,st,at)
              m_w,m_h=mask.get_size()
              mask=LayeredDisplayable([["ui xray_mask",self.mouse_pos[0]-m_w//2,self.mouse_pos[1]-m_h//2]],s_w,s_h)
              mask=renpy.display.render.render(mask,s_w,s_h,st,at)
              xr.operation=renpy.display.render.IMAGEDISSOLVE
              xr.operation_alpha=1.0
              xr.operation_complete=256.0 / (256.0 + 256.0)
              xr.operation_parameter=256
              xr.blit(mask,(0,0),focus=False,main=False)
              xr.blit(lrn,(0,0),focus=False,main=False)
              xr.blit(lrx,(0,0))
              states[True]=xr
            renders.append(states)
          rv=renpy.Render(rv_w,rv_h,opaque=False)
          if len(renders)>1:
            rv.operation=renpy.display.render.DISSOLVE
            rv.operation_alpha=1.0
            rv.operation_complete=self.layers[-1][0]
            rv.blit(renders[-2][bool(game.xray_mode)],(0,0),focus=False, main=False)
          rv.blit(renders[-1][bool(game.xray_mode)],(0,0),focus=True, main=True)
      else:
        rv=renpy.Render(width,height)
      renpy.redraw(self,0)
      return rv
    def event(self,ev,x,y,st):
      self.mouse_pos=(x,y)

  ## should we have a place to put things like this?
  renpy.music.register_channel("sprite","voice",loop=True,stop_on_mute=False,buffer_queue=False,movie=True,framedrop=True)
