style skip_to_target_button is textbutton:
  pos (50,300)

screen skip_to_target():
  style_prefix "skip_to_target"
  zorder 15
  if skip_to is not None:
    python:
      if isinstance(skip_to,basestring):
        caption="Skip"
        target=skip_to
        place=None
      else:
        caption=skip_to[0]
        target=skip_to[1]
        place=skip_to[2] if len(skip_to)>2 else None
      if place is not None:
        pos,anchor,offset=normalize_place(place)
    fixed:
      textbutton caption:
        if place is not None:
          pos pos
          anchor anchor
          offset offset
        action Jump(target)
