#define fast_mode_action_circle_hint="o o o"
define fast_mode_action_circle_hint="{image=ui fast_mode_action_circle_hint}"


init python:
  class Event_location_after_load(GameEvent):
    def on_init(event):
      renpy.get_registered_image("location").reset_scene()
    def on_after_load(event):
      renpy.get_registered_image("location").reset_scene()

  class LocationDisplayable(renpy.Displayable):
    def __init__(self,**properties):
      super().__init__(**properties)
      self.last_time=0.0
      self.alpha_per_sec=4.0
      self.hover_per_sec=4.0
      self.scenes=[]
      self.mouse_pos=(0,0)
      self.hovered_obj=None
      self.internals=None
      self.render_cache={}
    def reset_scene(self):
      self.scenes=[]
    def prepare_renders(self,scene):
      width,height,st,at=self.internals
      alpha,elements,hover_levels=scene
      el_renders=[]
      for n,el in enumerate(elements):
        el_pos,el_image,el_obj=el
        el_mode,el_mode_extra="idle",None
        if el_obj is None:
          if game.ui.hint_active_elements:
            el_mode="inactive"
        else:
#         jotapê was here
          el=interactables_by_id[(el_obj[0] if isinstance(el_obj,tuple) else el_obj)]
          el_actions=[]
          el.actions(el_actions)
          el_actions=el.finalize_actions(el_actions)
          if game.ui.hint_active_elements and not el_actions:
            el_mode="inactive"
#           jotapê was here
          if hover_levels[n]>0:
            el_mode,el_mode_extra="hover",hover_levels[n]
        el_image_renders=self.render_cache.get(el_image,{})
        cache_id=(el_mode,el_mode_extra)
        el_image_render=el_image_renders.get(cache_id)
        if not el_image_render:
          img=renpy.displayable(el_image.strip("#"))
          if el_mode!="idle":
            img=ElementDisplayable(img,el_mode,mode_extra=el_mode_extra)
          el_image_render=renpy.render(img,width,height,st,at)
        el_image_renders[cache_id]=el_image_render
        self.render_cache[el_image]=el_image_renders
        el_renders.append(el_image_render)
      return el_renders
    def update_hovered(self,pos,scene):
      rv=None
      if renpy.get_screen("location"):
        if not renpy.get_screen("action_circle") and not renpy.get_screen("investigate"):
          width,height,st,at=self.internals
          alpha,elements,hover_levels=scene
          elements=elements[:]
#         jotapê was here
          # while elements and not elements[0][2]:
            # elements.pop(0)
#           jotapê was here
          for n,el in enumerate(reversed(elements)):
            el_pos,el_image,el_obj=el
            if (el_image.startswith("#")
            or game.hud_hint):
              continue
#           jotapê was here
            if el_obj is None:
              elements.pop(elements.index(el))
            else:
              el=interactables_by_id[(el_obj[0] if isinstance(el_obj,tuple) else el_obj)]
              el_actions=[]
              el.actions(el_actions)
              el_actions=el.finalize_actions(el_actions)
#             jotapê was here
            x=pos[0]-el_pos[0]
            y=pos[1]-el_pos[1]
            img=self.render_cache.get(el_image).values()[0]
            w,h=img.get_size()
            if 0<=x<w and 0<=y<h:
              if bool(img.is_pixel_opaque(x,y)):
                if isinstance(el_obj,basestring):
                  el_obj=(el_obj,0,0)
#               jotapê was here
                if el_obj and el_actions:
#               jotapê was here
                  rv=(el_pos,el_image,el_obj[0],el_obj[1],el_obj[2])
                break
        else:
          rv=self.hovered_obj
      self.hovered_obj=rv
      return rv
    def render(self,width,height,st,at):
      renpy.display.render.render_cache.clear() ## memory leak dirty fix
      self.internals=width,height,st,at
      self.render_cache={}
      if game.location is not None:
        elements=[]
        game.location.scene(elements)
        game.location.prepare_images(elements)
        ## normalize elements
        for n,el in enumerate(elements):
          if isinstance(el,basestring):
            el=[(0,0),el]
          elements[n]=list(el)+[None]*(3-len(el))
        ## add scene if first or changed
        if self.scenes:
          if elements!=self.scenes[-1][1]:
            self.scenes.append([0.0,elements,[0.0]*len(elements)])
        else:
          self.scenes.append([1.0,elements,[0.0]*len(elements)])
      ## update internal time counter
      time_delta=min(0.1,max(0.0,st-self.last_time))
      hover_delta=time_delta*self.hover_per_sec
      alpha_delta=time_delta*self.alpha_per_sec
      self.last_time=st
      ## update current scene alpha, once fully visible remove old scenes
      if alpha_delta>0:
        self.scenes[-1][0]=min(1.0,self.scenes[-1][0]+alpha_delta)
        if self.scenes[-1][0]==1.0:
          self.scenes=[self.scenes[-1]]
      ## prepare scenes
      scene_elements_renders=[]
      for scene_n,scene in enumerate(self.scenes):
        scene_elements_renders.append(self.prepare_renders(scene))
      ## update hover states
      self.update_hovered(self.mouse_pos,self.scenes[-1])
      alpha,elements,hover_levels=self.scenes[-1]
      for n,el in enumerate(elements):
        el_pos,el_image,el_obj=el
        if el_obj:
          if not isinstance(el_obj,basestring):
            el_obj=el_obj[0]
          if self.hovered_obj and el_obj==self.hovered_obj[2]:
            hover_levels[n]+=hover_delta
          else:
            hover_levels[n]-=hover_delta
          hover_levels[n]=min(1.0,max(0.0,hover_levels[n]))
      ## render scenes
      scene_renders=[]
      for scene_n,(alpha,elements,hover_levels) in enumerate(self.scenes):
        el_renders=scene_elements_renders[scene_n]
        scene_render=renpy.Render(width,height)
        for n,el in enumerate(elements):
          if el_renders[n] is not None:
            scene_render.blit(el_renders[n],el[0])
        scene_renders.append(scene_render)
      ## render final scene
      rv=renpy.Render(width,height)
      if len(self.scenes)>1:
        rv.operation=renpy.display.render.DISSOLVE
        rv.operation_alpha=1.0
        rv.operation_complete=self.scenes[-1][0]
        rv.blit(scene_renders[-2],(0,0),focus=False, main=False)
      rv.blit(scene_renders[-1],(0,0),focus=True, main=True)
      ## done
      renpy.redraw(self,0)
      self.render_cache={}
      return rv
    def event(self,ev,x,y,st):
      if renpy.get_screen("location"):
        if ev.type==pygame.MOUSEMOTION:
          self.mouse_pos=(x,y)
          hobj=self.hovered_obj
          if hobj:
            hint=renpy.get_screen("element_hint")
            if not hint or hint.scope["el"].id!=hobj[2]:
              el=interactables_by_id[hobj[2]]
              action_hint=None
              if game.fast_mode:
                el_actions=[]
                el.actions(el_actions)
                el_actions=el.finalize_actions(el_actions)
                action_id=None
                for el_action in el_actions:
                  if action_id in (None,"investigate"):
                    action_id=el_action[0][0]
                    action_hint="{#"+action_id+"}"+el_action[1]
                  else:
                    action_id=el_action[0][0]
                    action_hint=fast_mode_action_circle_hint
              Show("element_hint",None,renpy.displayable(hobj[1]),hobj[0],el,hobj[3],hobj[4],action_hint=action_hint)()
          elif renpy.get_screen("element_hint"):
            Hide("element_hint")()
        elif ev.type==pygame.MOUSEBUTTONUP and ev.button==1:
          if self.scenes and self.internals is not None:
            hobj=self.hovered_obj
            if hobj:
              el=interactables_by_id[hobj[2]]
              rv_action=process_event("location_element_click",el)
              if rv_action is not None:
                rv_action=Return(rv_action)
              else:
                if game.fast_mode:
                  el_actions=[]
                  el.actions(el_actions)
                  el_actions=el.finalize_actions(el_actions)
                  for el_action in el_actions:
                    if rv_action in (None,"investigate"):
                      rv_action=el_action[0][0]
                    else:
                      rv_action=False
                rv_action=Show("action_circle",None,renpy.displayable(hobj[1]),hobj[0],el,hobj[3],hobj[4],force_action=rv_action)
              renpy.sound.play("interact_popup"+str(random.randint(1, 6)))
              rv_action()
          raise renpy.IgnoreEvent()

image location=LocationDisplayable()

screen location():
  tag game_mode
  key "`" action ToggleVariable("game.ui.hint_active_elements")
  use quest_guide()
  $pygame.event.post(pygame.event.Event(pygame.MOUSEMOTION,{"pos":pygame.mouse.get_pos(),"rel":None,"buttons":None}))
