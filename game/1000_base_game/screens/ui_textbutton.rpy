style textbutton:
  background Frame("ui frame_button",32,8)
  hover_background Frame(ElementDisplayable("ui frame_button","hover"),32,8)
  padding (32,27)
  xminimum 120

style button_text:
  font "fonts/ComicHelvetic_Heavy.otf"
  color "#6a431e"
  size 32  
  hover_color "#841"
  align (0.5,0.5)
  text_align 0.5
