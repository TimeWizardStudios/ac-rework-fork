style choice_btn:
  background Frame("ui dialog choice",100,0,150,0)
  hover_background Frame("ui dialog choice_hover",100,0,150,0)
  insensitive_background Frame(ElementDisplayable("ui dialog choice","inactive"),100,0,150,0)
  padding (100,30,140,30)
  xminimum 500
  xmaximum 850
  size_group "menu_choice"

style choice_btn_message:
  background Frame("ui dialog choice_message",20,56,40,20)
  hover_background Frame(ElementDisplayable("ui dialog choice_message","hover"),20,56,40,20)
  insensitive_background Frame(ElementDisplayable("ui dialog choice_message","inactive"),20,56,40,20)
  padding (20,20,80,20)
  xminimum 500
  xmaximum 800
  size_group "menu_choice"

style choice_caption is ui_text_small:
  font "fonts/ComicHelvetic_Medium.otf"
  color "#fefefe"
  size 28
  text_align 0.0
  yalign 0.5
  #if not no_textbox:
  outlines [(2,"#432a12",0,0)]
  line_spacing 3

style choice_caption_message is ui_text_small:
  yalign 0.5
  text_align 0.5
  color "#000"
  size 28

style choice_marker is choice_caption:
  text_align 0.0

style choice_marker_message is choice_caption_message:
  text_align 0.0

#The next three are for the entire menu box.
transform tf_choice_box_far_left:
  yalign 0.5
  xpos 0.15
  xanchor 0.5

transform tf_choice_box_left:
  yalign 0.5
  xpos 0.25
  xanchor 0.5

transform tf_choice_box_right:
  yalign 0.5
  xpos 0.75
  xanchor 0.5

transform tf_choice_box_far_right:
  yalign 0.5
  xpos 0.85
  xanchor 0.5


transform tf_choice_box_top:
  align (0.5,0.5)

#The next three are for the individual menu choice.

transform tf_choice_choice_far_left(total,n):
  xoffset -00
  xanchor 1.0
  xpos 0.0
  pause n*0.075
  parallel:
    easein 0.35 xanchor 0.0
  parallel:
    easein 0.35 xoffset 0

transform tf_choice_choice_left(total,n):
  xoffset -100
  xanchor 1.0
  xpos 0.0
  pause n*0.075
  parallel:
    easein 0.35 xanchor 0.0
  parallel:
    easein 0.35 xoffset 0

transform tf_choice_choice_right(total,n):
  xoffset 100
  xanchor 0.0
  xpos 1.0
  pause n*0.075
  parallel:
    easein 0.35 xanchor 1.0
  parallel:
    easein 0.35 xoffset 0

transform tf_choice_choice_far_right(total,n):
  xoffset 100
  xanchor 0.0
  xpos 1.0
  pause n*0.075
  parallel:
    easein 0.35 xanchor 1.0
  parallel:
    easein 0.35 xoffset 0



transform tf_choice_choice_top(total,n):
  xalign 0.5
  yoffset -1080
  pause (total-n-1)*0.1
  easein 0.35 yoffset 0

#The screen for the menu choice.

#For condotionals:
# icon should be inside {image=icon id} tag.
#you can have more than one icon or text, separate them with "|".
#Format is "?condition@marker 1|marker 2|marker n|caption".
#Each part is optional, you can skip condition - "marker1|marker2|caption", you can skip markers "?condition|caption".
#Or just normal "caption".

# "?mc.strength>=5@[mc.strength]/5|{image=stats str}|Done":
# "{image=stats str}|Increase strength":
#"?mc.intellect>=1@[mc.intellect]/1|{image=stats int}|Who the hell are you?":
# "{image=stats love}|It was a pretty good origami cat, though. Right?":
#Text inside [] will be interpolated, text inside {} is text tags.
#Normal RenPy string handling applies.
#https://www.renpy.org/doc/html/text.html
#Eh, no, if you just want text marker you write:
#menu:
#  "marker|caption":

screen choice(items,side=None):
  zorder 7
  python:
    if side not in ("left","right","top","far_right", "far_left"):
      side="top"
    if game.ui.dialog_mode in ("phone_call","phone_message"):
      side="right"
    if game.ui.dialog_mode=="phone_message":
      choice_style="choice_btn_message"
      choice_caption_style="choice_caption_message"
      choice_marker_style="choice_marker_message"
    else:
      choice_style="choice_btn"
      choice_caption_style="choice_caption"
      choice_marker_style="choice_marker"
  vbox:
    at getattr(store,"tf_choice_box_"+side)
    for n,item in enumerate(items):
      python:
        marker,_,caption=(s.strip() for s in item[0].rpartition("|"))
        if marker.startswith("?"):
          if "@" in marker:
            condition,_,marker=(s.strip() for s in marker[1:].rpartition("@"))
          else:
            condition,marker=marker[1:],""
        else:
          condition=""
        condition=eval(condition) if condition else True
        marker=[s.strip() for s in marker.split("|")]
      button:
        ypadding 4
        at getattr(store,"tf_choice_choice_"+side)(len(items),n)
        if condition:
          action item[1]
        frame style choice_style:
          hbox:
            align (0.5,0.5)
            if marker:
              spacing 32
              hbox:
                spacing 8
                yalign 0.5
                for marker_part in marker:
                  text marker_part style choice_marker_style:
                    color ("#0C0" if condition else "#C00")
                    if game.ui.dialog_mode == "phone_message" and condition:
                      outlines [(2,"#30F273",0,0)]
            text caption style choice_caption_style

screen remember_to_save:
    modal True
    zorder 200
    add "#0008"
    frame style "notification_modal_title_frame" yalign 0.23:
      text "Warning" style "notification_modal_title"
    frame style_prefix "vine_instructions"yalign 0.52:
      vbox:
        spacing 32
        text "The choice you're about to make will impact the remainder of your playthrough. Remember to save!"
        textbutton "Okay" xalign 0.5 action Return()
    key "rollforward" action NullAction()
