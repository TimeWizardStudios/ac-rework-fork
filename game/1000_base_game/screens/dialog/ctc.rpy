image ctc:
  "ui dialog ctc"
  easein 0.5 xoffset 30
  easeout 0.5 xoffset 0
  repeat

screen ctc:
  zorder 9
  add "ctc" align(1.0,1.0) offset (-60,-30)
