style say_frame:
  xfill True
  yalign 1.0
  background Frame("ui dialog frame_say_bg",0,0)
  padding (300,64)
  yminimum 220

style say_phone_frame:
  pos (0.525,100)
  background Frame("ui dialog frame_phone_say_bg",45,58,30,22)
  padding (65,20,50,25)

style say_phone_pc_frame:
  xpos 0.525
  yalign 1.0
  yoffset -100
  background Frame("ui dialog frame_phone_say_bg",45,58,30,22)
  padding (65,20,50,25)

style say_name_frame is frame:
  background Frame("ui dialog frame_name",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200

###Jo Frame####
style say_name_frame_jo is frame:
  background Frame("ui dialog frame_name_jo",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###Jo Frame####

###lindsey Frame####
style say_name_frame_lindsey is frame:
  background Frame("ui dialog frame_name_lindsey",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###lindsey Frame####

###kate Frame####
style say_name_frame_kate is frame:
  background Frame("ui dialog frame_name_kate",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###kate Frame####

###kate's retinue Frame####
style say_name_frame_casey is say_name_frame_berb
style say_name_frame_lacey is say_name_frame_berb
style say_name_frame_stacy is say_name_frame_berb
###kate's retinue Frame####

###isabelle Frame####
style say_name_frame_isabelle is frame:
  background Frame("ui dialog frame_name_isabelle",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###isabelle Frame####

###nurse Frame####
style say_name_frame_nurse is frame:
  background Frame("ui dialog frame_name_nurse",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###nurse Frame####

###jacklyn Frame####
style say_name_frame_jacklyn is frame:
  background Frame("ui dialog frame_name_jacklyn",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###jacklyn Frame####

###mrsl Frame####
style say_name_frame_mrsl is frame:
  background Frame("ui dialog frame_name_mrsl",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###mrsl Frame####

###flora Frame####
style say_name_frame_flora is frame:
  background Frame("ui dialog frame_name_flora",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###flora Frame####

###guard Frame####
style say_name_frame_guard is frame:
  background Frame("ui dialog frame_name_guard",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###guard Frame####

###maxine Frame####
style say_name_frame_maxine is frame:
  background Frame("ui dialog frame_name_maxine",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###maxine Frame####

###maya Frame####
style say_name_frame_maya is frame:
  background Frame("ui dialog frame_name_maya",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###maya Frame####

###berb Frame####
style say_name_frame_berb is frame:
  background Frame("ui dialog frame_name_berb",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###berb Frame####

###ria Frame####
style say_name_frame_ria is frame:
  background Frame("ui dialog frame_name_ria",45,10,45,30)
  padding (30,15,30,25)
  pos (270,900)
  anchor (1.0,0)
  xminimum 200
###ria Frame####

###hati Frame####
style say_name_frame_hati is say_name_frame_guard
###hati Frame####

###cuthbert Frame####
style say_name_frame_cuthbert is say_name_frame_jacklyn
###cuthbert Frame####

###mrbrown Frame####
style say_name_frame_mrbrown is say_name_frame_lindsey
###mrbrown Frame####

style say_name is ui_text:
  size 36
  #color "#432a12"
  color "#fefefe"
  outlines [(2,"#432a12",0,0)]
  align(.5,.5)
  kerning 1
  font "fonts/ComicHelvetic_Heavy.otf"

style say_dialogue is ui_text:
  font "fonts/ComicHelvetic_Medium.otf"
  #font "fonts/MonkeyIsland.ttf"
  color "#fefefe"
  line_leading 10
  line_spacing 10
  size 40
  text_align 0.0
  xmaximum 1302
  align (0.0,0.0)
  #outlines [(1, "#000000", 0, 0)]
  #if not no_textbox:
  outlines [(2,"#432a12",0,0)]
  #kerning 1

style say_thought is say_dialogue

transform hidden:
  alpha 0.0

screen say(who,what):
  zorder 6
  style_prefix "say"
  if not renpy.get_screen("choice"):
    key "dismiss" action Return()
  fixed:
    if game.ui.dialog_mode in ("phone_call","phone_call_centered","phone_message","phone_message_centered","walkie_talkie","walkie_talkie_left_aligned"):
      $say_frame_style="say_phone_pc_frame" if game._speaker==game.pc else "say_phone_frame"
      frame style say_frame_style:
        if game.ui.dialog_mode == "phone_call_centered":
          xpos 1265
        elif game.ui.dialog_mode in ("walkie_talkie","walkie_talkie_left_aligned"):
          xpos (1165 if game.ui.dialog_mode == "walkie_talkie" else 965)
          yoffset (-110 if game._speaker == game.pc else 220)
        if game.ui.dialog_mode in ("phone_message","phone_message_centered"):
          at hidden
        text what id "what" color "#151515" font "fonts/ComicHelvetic_Light.otf" size 32 outlines []
    else:
      frame:
        if game.ui.dialog_mode=="default_no_bg":
          background None
        text what id "what"
      if who is not None:
        python:
          who_frame_style="say_name_frame_"+getattr(game._speaker,"id","default")
          if not style.exists(who_frame_style):
            who_frame_style="say_name_frame"
        frame style who_frame_style:
          python:
            who_style="say_name_"+getattr(game._speaker,"id","default")
            if not style.exists(who_style):
              who_style="say_name"
          text who style who_style
      if hasattr(game._speaker,"side_image"):
        $side_image=game._speaker.side_image(game._speaker_state)
        if side_image:
          add side_image align (0,1.0)
