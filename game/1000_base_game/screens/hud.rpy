define xray_key="x"

style hud_text is ui_text:
  color "#000"
style hud_text_small is ui_text_small:
  color "#000"

style hud_location is hud_text
style hud_date is hud_text_small
style hud_time is hud_text_small
style hud_money is hud_text_small

style ui_btn_hint_hud_bottom is ui_btn_hint_bottom:
  yoffset 0

style hud_energy_text is ui_text_small:
  size 20
  align (0.5,0.5)

style hud_money_text is ui_text:
  color "#FFF"
  outlines [(2,"#000")]

style hud_btn_leyline_locator:
  background "misc leyline top"
  hover_background "misc leyline top_selected"
  align (0.95,1.05)

screen hud():
  zorder 3
  if xray_key:
    key xray_key action SetVariable("game.xray_mode",(int(game.xray_mode)+1)%len(xray_fn_suffix))
    #Caps Lock support - natan
    key "X" action SetVariable("game.xray_mode",(int(game.xray_mode)+1)%len(xray_fn_suffix))
  $char=game.pc
  $energy_bar_size=(250,50)
  if game.ui.hide_hud <= 0:
    if quest.lindsey_wrong["mc_verywet"]:
      add "water_drops"
    if quest.kate_wicked["ghost_costume"]:
      add "ghost_costume"
    if quest.maxine_lines['lll_charged'] and game.ui.hud_active and not game.location in ("school_leyline_locator","school_locker","school_secret_locker"):
      fixed:
        pos (0.6,0.91)
        button style "hud_btn_leyline_locator":
          #add "misc leyline top"
          action Call("goto_school_leyline_locator")
    add "ui hud top_gradient"
    vbox:
      hbox:
        offset 30,30
        spacing 8
        use ui_btn(("ui hud btn_map",(64,8,16,8),(64,8,24,8),(250,50,600,50)),None,title="{size=20}[game.location]{/size}"+("\n{size=18}(XRAY: "+xray_mode_name[int(game.xray_mode)]+"){/}" if game.xray_mode else ""))
        hbox:
          use ui_btn(("ui hud btn_time",(16,8,16,8),(24,8,24,8),(250,50,600,50)),(Return("advance_time") if game.can_advance_time else None),title="{size=20}Day [game.day] - [game.dow_short] - [game.time_str]{/size}",hint="Advance time",hint_place="hud_bottom")
          #add "ui tod "+game.tod yalign 0.5
        fixed:
          fit_first True
          yalign 0.5
          bar:
            left_bar Frame("ui hud bar_front_energy",0,0)
            right_bar Frame("ui hud bar_back_energy",0,0)
            xysize energy_bar_size
            value char.energy
            range char.max_energy
          add "ui hud bar_border_energy" size energy_bar_size
          text "[char.energy]/[char.max_energy]" style "hud_energy_text"
        hbox:
          yalign 0.5
          spacing 0
          add "ui hud icon_money" yalign 0.5
          text "[mc.money]" yalign 0.5 style "hud_money_text"
      vbox:
        text game.hud_hint style "ui_btn_hint_top" anchor 0,0 pos 30,60
        hbox:
          for char_id,char in game.characters.items():
            if mc["focus"] and (char.id+"_" in mc["focus"] or char.id+"@" in mc["focus"]):
              imagebutton idle LiveComposite((82,88),(0,0),Transform("ui notification icon_bg",xzoom=.58,yzoom=.56),(5,5),Transform(str(char.id)+" contact_icon",zoom=.56),(40,0),"ui hud icon_focus") hover LiveComposite((82,88),(0,0),Transform(ElementDisplayable("ui notification icon_bg","hover"),xzoom=.58,yzoom=.56),(5,5),Transform(ElementDisplayable(str(char.id)+" contact_icon","hover"),zoom=.56),(40,0),ElementDisplayable("ui hud icon_focus","hover")) hovered SetVariable("game.hud_hint",("The " if char.id in ("nurse","guard") else "")+str(char)+"'s {color=#48F}"+(quests_by_id[mc["focus"].split("@")[1]].title if "@" in mc["focus"] else quests_by_id[mc["focus"]].title)+"{/} quest is currently blocking you from progressing other quests.") unhovered SetVariable("game.hud_hint","") action (NullAction() if game.ui.hud_active else None) xpos 30 ypos 60
          for char_id,char in game.characters.items():
            if char["at_none_now"] or char["hidden_now"] or char["at_none_today"] or char["hidden_today"] or char["at_none"]:
              imagebutton idle LiveComposite((82,88),(0,0),Transform("ui notification icon_bg",xzoom=.58,yzoom=.56),(5,5),Transform(str(char.id)+" contact_icon",zoom=.56),(40,0),"ui hud icon_at_none") hover LiveComposite((82,88),(0,0),Transform(ElementDisplayable("ui notification icon_bg","hover"),xzoom=.58,yzoom=.56),(5,5),Transform(ElementDisplayable(str(char.id)+" contact_icon","hover"),zoom=.56),(40,0),ElementDisplayable("ui hud icon_at_none","hover")) hovered SetVariable("game.hud_hint",("The " if char.id in ("nurse","guard") else "")+str(char)+" is unavailable " + ("indefinitely" if char["at_none"] else "until "+(str(time_str(game.hour+1,0)) if (char["at_none_now"] or char["hidden_now"]) and not game.hour == 23 else "tomorrow"))+".") unhovered SetVariable("game.hud_hint","") action (NullAction() if game.ui.hud_active else None) xpos 30 ypos 60
    if "(Trailer Edition)" in config.window_title:
      add "ui hud censor" xpos 295 ypos 34
    hbox:
      xalign 1.0
      offset -30,30
      spacing 8
      $guided_quest=game.quests.get(game.quest_guide,None)
      use ui_btn(("ui hud btn_quest_guide",(16,8,16,8),(24,8,24,8),(250,50,600,50)),(Return("select_quest_guide") if game.ui.hud_active else None),title="{size=20}Quest guide:\n"+(guided_quest.title if guided_quest else "{color=#888}Off{/color}")+"{/size}",hint="Change quest guide",hint_place="hud_bottom",place=(0,0.5,0,0.5))
      use ui_btn(("ui hud btn_game_mode",(16,8,16,8),(24,8,24,8),(125,50,600,50)),(ToggleVariable("game.fast_mode") if game.ui.hud_active else None),title="{size=20}Mode:\n"+("Fast" if game.fast_mode else "Explore")+"{/size}",hint="Switch to\n"+("Explore" if game.fast_mode else "Fast")+" mode",hint_place="hud_bottom",place=(0,0.5,0,0.5))
      use ui_btn("ui hud btn_inventory",(Return("inventory") if (game.ui.hud_active and not quest.lindsey_voluntary == "dream") else None),hint="Inventory",hint_place="hud_bottom")
      if quest.isabelle_hurricane == "short_circuit" and quest.isabelle_hurricane["darkness"]:
        use ui_btn("ui hud btn_phone",(Return("quest_isabelle_hurricane_short_circuit_phone") if game.ui.hud_active else None),hint="Phone",hint_place="hud_bottom")
      elif quest.lindsey_angel == "lab_coat" and quest.lindsey_angel["troll_outside_its_cave"]:
        use ui_btn("ui hud btn_phone",(Return("quest_lindsey_angel_lab_coat_phone") if game.ui.hud_active else None),hint="Phone",hint_place="hud_bottom")
      else:
        use ui_btn("ui hud btn_phone",(Return("phone") if (game.ui.hud_active and not quest.lindsey_voluntary == "dream") else None),hint="Phone",hint_place="hud_bottom")
