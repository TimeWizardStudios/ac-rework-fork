﻿style quick_start_button is textbutton

style quick_start_header is ui_text:
  color "#FC4"
  size 64
  text_align 0.5
  xalign 0.5

style quick_start_page is quick_start_header:
#  size 32
  yalign 0.5

style quick_start_slot_info is ui_text:
  ysize 32
  text_align 0.5
  color "#FFF"
  outlines [(2,"#000")]
  align (0.5,0.5)

style quick_start_title is quick_start_slot_info:
  yalign 1.0

style quick_start_locked is quick_start_slot_info

style quick_start_empty is quick_start_slot_info

screen quick_start():
  style_prefix "quick_start"
  tag menu
  default quick_starts = quick_starts_by_order["season_1"]
  default current_page=0
  default max_page=max(0,(len(quick_starts)+5)//6)
  use game_menu:
    vbox:
      align (0.5,0.5)
      text "What point would you like to start from?" style "quick_start_header"
      grid 3 2:
        xalign 0.5
        for n in range(0,6):
          if len(quick_starts)>current_page*6+n:
            $title,start_label,condition,image=quick_starts[current_page*6+n]
            $image=image if renpy.loadable("images/"+image.replace(" ","/")+".webp") else "quick_start placeholder"
          else:
            $title,start_label,condition,image="",None,False,None
          if callable(condition):
            $condition=condition()
          button:
            style "button"
            xysize (399,320)
            vbox:
              fixed:
                ysize 90
                text title style "quick_start_title"
              fixed:
                xysize (399,230)
                if image:
                  add im.AlphaMask(renpy.displayable(image),renpy.displayable("ui game_menu slot_mask"))
                  if not condition:
                    text "Locked" style "quick_start_locked"
                else:
                  text "Empty" style "quick_start_empty"
                frame:
                  background "ui game_menu slot_idle"
                  hover_background "ui game_menu slot_hover"
            if start_label and condition:
              action If(main_menu, Start(start_label), Confirm("Selecting a chapter will lose\nunsaved progress.\nAre you sure you want to do this?", Start(start_label)))
      null height 32
      fixed:
        ysize 64
        xfill True
        if max_page>1:
          use ui_btn("ui prev_page",SetScreenVariable("current_page",(current_page-1)%max_page),hint="Prev",place=(0.3,0.5,0.0,0.5))
        text "Page {} of {}".format(current_page+1,max_page) style "quick_start_page"
        if max_page>1:
          use ui_btn("ui next_page",SetScreenVariable("current_page",(current_page+1)%max_page),hint="Next",place=(0.7,0.5,1.0,0.5),hint_place="right")
      null height 16
      hbox xalign 0.5:
        textbutton "Season 1" action If(quick_starts != quick_starts_by_order["season_1"], [SetScreenVariable("quick_starts",quick_starts_by_order["season_1"]),SetScreenVariable("current_page",0),SetScreenVariable("max_page",max(0,(len(quick_starts_by_order["season_1"])+5)//6))], NullAction())
        textbutton "Season 2" action If(quick_starts != quick_starts_by_order["season_2"], [SetScreenVariable("quick_starts",quick_starts_by_order["season_2"]),SetScreenVariable("current_page",0),SetScreenVariable("max_page",max(0,(len(quick_starts_by_order["season_2"])+5)//6))], NullAction())
        textbutton "Season 3" background Frame(Transform("ui frame_button",alpha=0.16),32,8) text_color "#1A1613"
        textbutton "Season 4" background Frame(Transform("ui frame_button",alpha=0.16),32,8) text_color "#1A1613"
