style confirmscreen_frame is frame:
  background Frame("ui frame_popup",220,160,240,150)
  padding (80,60)
  align (0.5,0.5)
  xmaximum 800

style confirmscreen_text is ui_text:
  xalign 0.5
  text_align 0.5

style confirmscreen_button is textbutton:
  xminimum 250
  size_group "confirm_buttons"

screen confirm(message, yes_action, no_action):
  modal True
  zorder 200
  style_prefix "confirmscreen"
  add "#0008"
  frame:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Yes" action yes_action
        textbutton "No" action no_action
  key "game_menu" action no_action
