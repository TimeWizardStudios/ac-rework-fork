style pref_label_text is ui_text:
  size 48
  color "#FC4"
  layout "nobreak"

style pref_button:
  selected_foreground Text("{image=ui game_menu on}",pos=(0.0,0.5),anchor=(1.0,0.5),xoffset=-16)

style pref_button_text is ui_text:
  color "#888"
  hover_color "#FFF"
  selected_color "#FFF"
  layout "nobreak"

style pref_slider:
  xysize (450,54)
  left_gutter 8
  right_gutter 16
  left_bar Frame("ui game_menu bar_full",32,0,)
  right_bar Frame("ui game_menu bar_empty",32,0)
  thumb_offset 27
  thumb "ui game_menu bar_thumb"
  hover_thumb ElementDisplayable("ui game_menu bar_thumb","hover")

screen preferences():
  tag menu
  use game_menu:
    style_prefix "pref"
    hbox:
      align (0.5,0.5)
      spacing 64
      vbox:
        spacing 32
        vbox:
          if renpy.variant("pc"):
            label "Display"
            textbutton "Windowed" action Preference("display","any window")
            textbutton "Fullscreen" action Preference("display","fullscreen")
        vbox:
          label "Skip"
          textbutton "Unseen Text" action Preference("skip","toggle")
          textbutton "After Choices" action Preference("after choices","toggle")
          textbutton "Transitions" action InvertSelected(Preference("transitions","toggle"))
        vbox:
          label "Time format"
          textbutton "12h (05:35PM)" action SetVariable("persistent.time_format",None)
          textbutton "24h (17:35)" action SetVariable("persistent.time_format","24h")
      vbox:
        spacing 32
        vbox:
          label "Text Speed"
          bar value Preference("text speed")
        vbox:
          label "Auto-Forward Time"
          bar value Preference("auto-forward time")
        vbox:
          label "Audio Volume"
          vbox:
            spacing 4
            for channel in ("sound","music","voice"):
              if getattr(config,"has_"+channel,False):
                hbox:
                  xsize 450
                  text channel.capitalize()
                  bar value Preference(channel+" volume") xalign 1.0 xsize 300
          null height 6
          textbutton "Mute All" action Preference("all mute","toggle")
