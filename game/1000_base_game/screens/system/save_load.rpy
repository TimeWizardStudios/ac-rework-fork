style fileslots_page is ui_text:
  color "#FC4"
  size 64
  text_align 0.5
  xalign 0.5

style fileslots_button is textbutton

style fileslots_text is ui_text:
  color "#FFF"
  text_align 0.5
  layout "nobreak"

style fileslots_slot_info is fileslots_text:
  color "#888"
  hover_color "#FFF"
  xalign 0.5

style fileslots_slot_time is fileslots_slot_info:
  size 32

screen save():
  tag menu
  python:
    if not renpy.predicting(): ## add this line
      try:
        int(persistent._file_page)
      except:
        persistent._file_page="1"
  use file_slots("Save")

screen load():
  tag menu
  use file_slots("Load",loading=True)

screen file_slots(title,loading=False):
  style_prefix "fileslots"
  use game_menu(title):
    default page_name_value=FilePageNameInputValue(pattern="Page {}",auto="Automatic saves",quick="Quick saves")
    vbox:
      align (0.5,0.5)
      spacing 32
      text page_name_value.get_text() style "fileslots_page"
      grid 3 2:
        xalign 0.5
        spacing 32
        for slot in range(1,7):
          python:
            if FileLoadable(slot):
              if FileJson(slot,"game_save_id")!=game_save_id or FileJson(slot,"game_version",[0,0,0],[0,0,0])<supported_game_version:
                slot_status="invalid"
              else:
                slot_status="valid"
            else:
              slot_status=None
          button:
            style "button"
            xysize (399,300)
            vbox:
              text FileTime(slot,format="{#file_time}%B %d %Y, %H:%M",empty="") style "fileslots_slot_time"
              fixed:
                xysize (399,230)
                if slot_status:
                  if slot_status=="valid":
                    add im.AlphaMask(FileScreenshot(slot),renpy.displayable("ui game_menu slot_mask"))
                  else:
                    text "Unsupported\nsave game\nversion" align (0.5,0.5) text_align 0.5
                else:
                  text "Empty" align (0.5,0.5) text_align 0.5
                frame:
                  background "ui game_menu slot_idle"
                  hover_background "ui game_menu slot_hover"
              if FileJson(slot,"save_info","","") and FileJson(slot,"save_info","","").split(" - ")[1].strip() == "Dungeons and Damsels":
                text FileJson(slot,"save_info","","").replace(FileJson(slot,"save_info","","").split(" - ")[1],"DnD") style "fileslots_slot_info"
              elif FileJson(slot,"save_info","","") and FileJson(slot,"save_info","","").split(" - ")[1].strip() == "Tam's Pancake Brothel":
                text FileJson(slot,"save_info","","").replace(FileJson(slot,"save_info","","").split(" - ")[1],"Tam's") style "fileslots_slot_info"
              else:
                text FileJson(slot,"save_info","","") style "fileslots_slot_info"
            if slot_status is not None:
              key "save_delete" action FileDelete(slot)
            if (loading and slot_status=="valid") or not loading:
              action FileAction(slot)
            elif slot_status is not None:
              action NullAction()
      hbox:
        xalign 0.5
        textbutton "<" action FilePagePrevious(max=100,wrap=True,auto=loading,quick=False)
        if loading:
          if config.has_autosave:
            textbutton "{#auto_page}Auto" action FilePage("auto")
          #if config.has_quicksave:
            #textbutton "{#quick_page}Quick" action FilePage("quick")
        for page in range(1, 7):
          textbutton "[page]" action FilePage(page)
        textbutton ">" action FilePageNext(max=100,wrap=True,auto=loading,quick=False)
