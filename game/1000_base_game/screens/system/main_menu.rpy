style mainmenu_button is textbutton:
    xanchor 0.5
    xalign 0.5
    xsize 300
    focus_mask True

style mainmenu_version is ui_text_small:
    size 27
    color "#8fdef1"
    outlines [(3,"#2e5676")]
    pos (0.5, 0.5)
    anchor (1.0, 1.0)
    xoffset -120
    yoffset 50
    font "fonts/ComicHelvetic_Heavy.otf"
    kerning 5

screen main_menu():
    default demo_xray = DemoXRay("sprite")
    default is_showing = False
    default show_support_tab = False
    default demo_xray_hovered = False

    default textbuttons = [
        {
            "text": _("New Game"), "action": Start()
        },
        {
            "text": _("Chapter Select"), "action": ShowMenu("quick_start")
        },
        {
            "text": _("Tips & Tricks"), "action": ShowMenu("how_to_play")
        },
        {
            "text": _("Load Game"), "action": ShowMenu("load")
        },
        {
            "text": _("Settings"), "action": ShowMenu("preferences")
        },
        {
            "text": _("Credits"), "action": ShowMenu("credits")
        },
        {
            "text": _("Quit"), "action": Quit(confirm=False), "condition": renpy.variant("pc")
        }
    ]
    default support_buttons = [
        {
            "icon": "Digital-Patreon-Logo_FieryCoral",
            "tooltip": "Patreon",
            "url": patreon_url
        },
        {
            "icon": "Discord-Logo-Color",
            "tooltip": "Discord",
            "url": discord_url
        }
    ]

    tag menu
    style_prefix "mainmenu"

    timer 0.01 action SetScreenVariable("is_showing", True)

    add "#fff"
    #add AlphaMask(Fixed("main_menu bg slideshow", Solid("#15151500")), "ui main_menu redesign bg_mask")
    add Fixed("main_menu bg slideshow")

    showif is_showing:
        add "ui main_menu redesign ray_overlay":
            at main_menu_ray_overlay

        add Transform("ui main_menu redesign strip_top", yalign=0.0, xoffset=75):
            at main_menu_strip(-1)

        add Transform("ui main_menu redesign strip_bottom", yalign=1.0, xoffset=75):
            at main_menu_strip(1)

        add Transform(
#               "ui main_menu redesign team_and_game_logos_cut",
                "ui main_menu redesign team_and_game_logos_cut2",
#               align=(0.0, 0.0), yoffset=-160, xoffset=-50
                align=(0.0, 0.0), yoffset=-171, xoffset=-48
            ):

            at main_menu_team_and_game_logos

        add Transform(
                AlphaMask(
                    Fixed(
#                       "ui main_menu redesign team_and_game_logos_cut", "#fff", fit_first=True
                        "ui main_menu redesign team_and_game_logos_cut2", "#fff", fit_first=True
                    ),
#                   "ui main_menu redesign team_and_game_logos_cut"
                    "ui main_menu redesign team_and_game_logos_cut2",
                ),
#               align=(0.0, 0.0), yoffset=-160, xoffset=-50
                align=(0.0, 0.0), yoffset=-171, xoffset=-48
            ):

            at main_menu_team_and_game_logos_alt

        text config.version.replace(".", "·"):
#           style "mainmenu_version"
            style "mainmenu_version" color "#DBA05F" outlines [(3,"#5B2B0D")]

            at main_menu_version

    fixed:
        ysize 0.5
        anchor (0.0, 1.0)
        pos (0.0, 1.0)

        vbox:
            xsize 350
            align (0.0, 0.5)
            spacing -12
            xoffset 0

            showif is_showing:

                for index, txtbtn in enumerate(textbuttons):
                    if txtbtn.get("condition", True):
                        textbutton txtbtn["text"]:
                            action txtbtn["action"]

                            at main_menu_textbutton(0.05 * index)

    button:
        action SetScreenVariable("show_support_tab", True)
        hovered SetScreenVariable("show_support_tab", True)
        unhovered SetScreenVariable("show_support_tab", False)

        align (1.0, 0.85)
        background None
        focus_mask None
        padding (0, 0)
        margin (0, 0)
        ysize 257
        xoffset 250

        at main_menu_support_tab

        hbox:
            spacing 0

            frame:
                background "#fff"
                yfill True
                padding (5, 5)

                vbox:
                    spacing -8
                    yalign 0.5

                    for char in reversed("SUPPORT US"):
                        text char:
                            font "fonts/ComicHelvetic_Heavy.otf"
                            size 26
                            xalign 0.5
                            color "#151515"
                            xoffset 2

                            at transform:
                                rotate -90.0

            frame:
                padding (10, 10)
                yalign 0.5
                background "#151515"

                vbox:
                    spacing 7
                    yalign 0.0

                    for index, li in enumerate(support_buttons):
                        button:
                            action OpenURL(li["url"])
                            hovered SetScreenVariable("show_support_tab", True)
                            unhovered SetScreenVariable("show_support_tab", False)

                            background AlphaMask("#fff1", Frame("ui mask_rounded_50px"))
                            hover_background AlphaMask("#fff3", Frame("ui mask_rounded_50px"))
                            xsize 100
                            ysize 100
                            focus_mask None
                            padding (5, 5)

                            tooltip li["tooltip"]

                            add Frame("ui main_menu redesign {}".format(li["icon"]))

                    $ tooltip = GetTooltip() or ""
                    showif tooltip:
                        text tooltip.upper():
                            yalign 1.0
                            font "fonts/ComicHelvetic_Medium.otf"
                            size 20

                            at transform:
                                on show:
                                    alpha 0.0
                                    easein 0.15 alpha 1.0

                                on hide:
                                    easeout 0.15 alpha 0.0

    showif demo_xray_hovered:
        add "#15151599":
            at main_menu_demo_xray_overlay

    showif is_showing:
        button:
            #action NullAction()
            hovered SetScreenVariable("demo_xray_hovered", True)
            unhovered SetScreenVariable("demo_xray_hovered", False)

            background None
            padding (0, 0)
            margin (0, 0)
            focus_mask True
            style_prefix ""

            add demo_xray ## xpos 100 ## only if there are two sprites

            at main_menu_demo_xray

    showif is_showing and not demo_xray_hovered:
        frame:
            anchor (0.0, 1.0)
            pos (0.73, 0.23) ## yoffset 160 ## only if maya ## xpos 1600 ## only if there are two sprites
            background Frame("ui dialog frame_phone_say_bg")
            padding (27, 12)

            vbox:
                align (0.0, 0.0)

                for txt in ["Hey, you!", "Over here!"]:
                    text txt:
                        xalign 0.0
                        color "#151515"
                        font "fonts/ComicHelvetic_Light.otf"
                        size 26

            at main_menu_demo_xray_bubble

transform main_menu_demo_xray_overlay():
    subpixel True

    on show:
        alpha 0.0
        easein 0.5 alpha 1.0

    on hide:
        easeout 0.5 alpha 0.0

transform main_menu_demo_xray_bubble():
    subpixel True

    on show:
        zoom 0.0 rotate 45.0
        pause 0.5
        easein 0.15 zoom 1.0 rotate 0.0

        block:
            easein_bounce 0.35 zoom 1.05
            easeout_bounce 0.35 zoom 1.0
            repeat

    on hide:
        easeout 0.15 zoom 0.0 rotate 45.0

transform main_menu_version():
    subpixel True

    on show:
        alpha 0.0
        pause 1.15
        easein 0.50 alpha 0.85

transform main_menu_ray_overlay():
    on show:
        alpha 0.0
        pause 0.40
        easein 0.15 alpha 1.0
        pause 1.5

        block:
            easein 1.5 alpha 0.60
            easeout 1.5 alpha 1.0
            repeat

transform main_menu_support_tab():
    subpixel True

    on idle:
        easeout 0.5 xoffset 0

    on hover, selected_hover, selected_idle:
        easein 0.5 xoffset -110

transform main_menu_demo_xray():
    subpixel True

    on show:
        alpha 0.0 yoffset config.screen_height align (0.7, 0.5)
        easein 0.50 yoffset 0 alpha 1.0

transform main_menu_strip(direction=1):
    subpixel True

    on show:
        yoffset (700 * direction)
        easein 1.0 yoffset 0

transform main_menu_team_and_game_logos():
    subpixel True

    on show:
        zoom 0.0 rotate 35.0

        parallel:
            easein_back 1.0 zoom 1.0 rotate 0.0

transform main_menu_team_and_game_logos_alt():
    subpixel True

    on show:
        zoom 0.0 rotate 35.0 alpha 0.0

        parallel:
            easein_back 1.0 zoom 1.0 rotate 0.0

        parallel:
            pause 0.40
            easein 0.10 alpha 1.0
            easeout 0.15 alpha 0.0


transform main_menu_textbutton(delay=0.0):
    subpixel True

    on show:
        xoffset -500
        pause 0.10
        pause delay
        easein_elastic 1.20 xoffset 0

init python:
    class DemoXRay(renpy.Displayable):
        def __init__(self, file_name, *args, **kwargs):
            renpy.Displayable.__init__(self, *args, **kwargs)

            file_location_prefix = "ui main_menu redesign demo_xray_sprite {}{}"

            self.file_name = file_name
            self.xray_image = file_location_prefix.format(file_name,"_nude")
            self.normal_image = file_location_prefix.format(file_name,"_clothed")
            self.mask_image = "ui xray_mask"
            self.mouse_pos = (0, 0)
            self.child = None
            self.child_size = (1, 1)
            self.kwargs = kwargs
            self.args = args

        def render(self, w, h, st, at):
            fixed = Fixed(
                self.normal_image,
                AlphaMask(
                    self.xray_image,
                    Transform(self.mask_image, align=(0.5, 0.5), pos=(self.mouse_pos[0] / self.child_size[0], self.mouse_pos[1] / self.child_size[1])),
                ),
                fit_first=True
            )

            self.child = fixed
            child_render = renpy.render(self.child, w, h, st, at)
            self.child_size = child_render.get_size()

            renpy.redraw(self, 0)

            return child_render

        def event(self, e, x, y, st):
            self.mouse_pos = (x, y)
            renpy.timeout(0)

            return

        def visit(self):
            return [self.child]


image main_menu bg slideshow:
    "ui main_menu redesign bg main_menu_0" with dissolve
    pause 4.0
    "ui main_menu redesign bg main_menu_1" with dissolve
    pause 4.0
    "ui main_menu redesign bg main_menu_2" with dissolve
    pause 4.0
    "ui main_menu redesign bg main_menu_3" with dissolve
    pause 4.0
    "ui main_menu redesign bg main_menu_4" with dissolve
    pause 4.0
    repeat
