style gamemenu_button:
  background None
  selected_background Frame("ui game_menu navtab_selected",0,0,64,0)
  padding (64,8,80,8)

style gamemenu_button_text is ui_text:
  size 48
  color "#888"
  hover_color "#FFF"
  selected_color "#440"
  yalign 0.5

screen game_menu(*args,**kwargs):
  style_prefix "gamemenu"
  add "ui game_menu bg"
  hbox:
    fixed:
      xsize 450
#     add Transform("ui main_menu logo",zoom=0.5) xalign 0.5 offset (24,65)
      add Transform("ui main_menu logo2",zoom=0.5) xalign 0.5 offset (24+15,65+5)
      add Crop((459,0,11,1080),"ui game_menu bg") xoffset 459
      text config.version.replace(".", "·"):
#       style "mainmenu_version" size 13 kerning 1 outlines [(1,"#2e5676")] xpos 545 ypos 265
        style "mainmenu_version" size 13 kerning 1 color "#DBA05F" outlines [(1,"#5B2B0D")] xpos 545 ypos 265
      vbox:
        ypos 350
        if not main_menu:
          textbutton "Save Game" action ShowMenu("save")
        textbutton "Chapter Select" action ShowMenu("quick_start")
        textbutton "Tips & Tricks" action ShowMenu("how_to_play")
        textbutton "Load Game" action ShowMenu("load")
        textbutton "Settings" action ShowMenu("preferences")
        textbutton "Credits" action ShowMenu("credits")
        textbutton "Main Menu" action (Return() if main_menu else MainMenu())
        if renpy.variant("pc"):
          textbutton "Quit" action Quit(confirm=not main_menu)
      textbutton "Return" action Return() align (0,0.97)
      key "game_menu" action Return()
    frame:
      background None
      padding (32,32,48,32)
      fixed:
        transclude
