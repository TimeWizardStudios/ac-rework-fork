style ui_text is text:
  font "assets/fonts/Fresca-Regular.ttf" #Old Font
  #font "assets/fonts/DejaVuSans.ttf"
  size 42
  color "#420"

style to_do_text is text:
  font "assets/fonts/MergedFont.ttf" #Old Font
  #font "assets/fonts/DejaVuSans.ttf"
  size 36
  color "#420"  

style ui_text_small is ui_text:
  size 32

style ui_text_large is ui_text:
  size 64

style frame:
  padding (0,0)

style button:
  padding (0,0)
