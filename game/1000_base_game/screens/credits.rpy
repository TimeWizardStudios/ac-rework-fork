define credits_text = [

"""
{=quick_start_header}Development Team{/}
"""
,

"""
{=credits_title}Project Lead, Director, Producer{/}
Time Wizard
"""

,

"""
{=credits_title}Lead Writer, Story Design{/}
Lilwa Dexel
"""

,

"""
{=credits_title}Lead Artist, Character Design{/}
Minteut
"""

,

"""
{=credits_title}Lead Developer{/}
jotapê
"""

,

"""
{=credits_title}Art{/}
krawgr
Fabrizio
RialynKV
"""

,

"""
{=credits_title}Code{/}
Tamarisk
drKlauz
"""

,

"""
{=credits_title}Music{/}
Tama
"""

,

"""
{=credits_title}Marketing{/}
Aizen-Sama
"""

,

"""
{=credits_title}Special Thanks{/}
Our Patreon supporters and Discord moderators
{image=credits_links}


PyTom and Ren'Py contributors
"""

]


screen credits_links():
  hbox:
    xalign 0.5
    use ui_btn("ui main_menu patreon",OpenURL(patreon_url),hint="Support us on Patreon!",hint_place="left")
    null width 10
    use ui_btn("ui main_menu discord",OpenURL(discord_url),hint="Join us on Discord!",hint_place="right")
## this is outright hack, which will work only here, i think
image credits_links=renpy.display.screen.ScreenDisplayable(renpy.display.screen.screens[("credits_links",None)],None,None)


style credits_text is ui_text:
  xalign 0.5
  yalign 0.5
  text_align 0.5
  color "#FFF"


style credits_title is ui_text:
  color "#888"
  size 28
  italic True


screen credits():
  tag menu
  use game_menu:
    viewport:
      xfill True
      yalign 0.5
      draggable True
      mousewheel True
      pagekeys True
      vbox:
        xfill True
        null height 48
        spacing 38
        for credits_row in credits_text:
          if isinstance(credits_row,basestring):
            text credits_row.strip() style "credits_text"
          else:
            grid len(credits_row) 1:
              xfill True
              for credits_col in credits_row:
                text credits_col.strip() style "credits_text"
        null height 48
    add Solid("#1A1613", xsize=1390, ysize=21, ypos=995)
