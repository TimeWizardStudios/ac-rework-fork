﻿init python:
    how_to_play_slides=[
        [
            "The Sands of Time",
            "1000_base_game/assets/images/help/hint_schedule.webm",
            "As time passes, characters move around according to their schedule.\nOne hour passes every time you run out of energy.\nYou can also make an hour pass manually by clicking the time button.\nBut don't worry!\nThere aren't any time sensitive events.\nSo feel free to click and explore to your heart's content."
        ],
        [
            "A Walkthrough the Park",
            "1000_base_game/assets/images/help/hint_quest_guide.webm",
            "Toggle the Quest Guide™ for any open quest you have and let our trusty arrows point you in the right direction."
        ],
        [
            "Tilde Vision",
            "1000_base_game/assets/images/help/hint_tilde_vision.webm",
            "Press the Tilde Key (~) to see all the possible Interactables in your current location."
        ],
        [
            "Trinkets and Baubles",
            "1000_base_game/assets/images/help/hint_greyed_used_item.webm",
            "Item interactions galore!\nWhen Using or Giving an item, our Inventory system reminds you what's already been tried."
        ],
        [
            "Deep Lore",
            "1000_base_game/assets/images/help/hint_dull_interaction.webm",
            "After you perform an action, the Action Circle™ dulls for repeat attempts.\nThis lets you know there aren't any new lines for this Interactable. When you have read all of the available current lines, the Hover Box goes grey.\nAs you progress through the story, more lines and actions get added."
        ],
        [
            "Vroom Vroom",
            "1000_base_game/assets/images/help/hint_fast_mode.webm",
            "Enable Fast Mode™ to play through the game faster.\nFast Mode™ no longer brings up the Action Circle™, instead it chooses the default option whenever you click."
        ],
        [
            "Fill Your Eyes...",
            "1000_base_game/assets/images/help/hint_xray.webm",
            "Press the X key at any time to activate X-ray mode.\nThe free version allows you to see through outerwear (jackets, shirts, pants, etc).\nBut our Chad and Incel supporters get to see one layer deeper!\nSupport us on Patreon to be able to see through bras and undies!"
        ],
    ]

    for htp in how_to_play_slides:
        htp[1] = AlphaMask(Movie(play=htp[1]), Frame("ui mask_rounded_50px", 50, 50, 50, 50))

style how_to_play_title is ui_text:
    xalign 0.5
    text_align 0.5
    color "#FC4"
    bold True

style how_to_play_text is ui_text:
    xalign 0.5
    yalign 0.5
    text_align 0.5
    color "#FFF"

screen how_to_play():
    tag menu

    default current_page=0
    default max_page=len(how_to_play_slides)

    use game_menu:
        vbox:
            $ slide = how_to_play_slides[current_page]

            null height 20

            if slide[0]:
                text slide[0].upper() style "how_to_play_title"

            null height 40

            if slide[1]:
                add slide[1] xalign 0.5 zoom 0.5

            null height 40

            viewport:
                mousewheel True
                pagekeys True
                xysize (1200, 230)
                align (0.5,0.0)

                vbox:
                    xfill True
                    spacing 64

                    if slide[2]:
                        text slide[2] style "how_to_play_text"

            fixed:
                yfit True
                xfill True
                xalign 0.5

                if max_page>1:
                    use ui_btn("ui prev_page",SetScreenVariable("current_page",(current_page-1)%max_page),hint="Prev",place=(0.3,0.5,0.0,0.5))

                text "Page {} of {}".format(current_page+1,max_page) style "quick_start_page"

                if max_page>1:
                    use ui_btn("ui next_page",SetScreenVariable("current_page",(current_page+1)%max_page),hint="Next",place=(0.7,0.5,1.0,0.5),hint_place="right")
