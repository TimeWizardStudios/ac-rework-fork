﻿transform tf_time_passed:
  on show,replace:
    alpha 0.0
    easein 0.5 alpha 1.0
  on hide,replaced:
    easeout 0.5 alpha 0.0

screen time_passed():
  zorder 99
  timer 0.2 action Return()
  fixed:
    at tf_time_passed
    add "#000"
    add "ui time_passed" align (0.5,0.5)
  button:
    action Return()
