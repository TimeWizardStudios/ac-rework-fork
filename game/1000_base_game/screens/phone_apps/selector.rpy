screen phone_app_selector(*args, **kwargs):
  python:
    contact = game.characters[game.pc.last_phone_message_contact]
    contact_message_label = contact.message_label()

    app_n = 0
    apps = []

    for app in game.pc.phone_apps:
      if not app.hidden:
        x = app_n % 3 * 152
        y = app_n // 3 * (8 + 113 + 8 + 24 + 8) + 16
        apps.append([(x, y), app.id, app.icon, app.title])
        app_n += 1

  viewport:
    xfill True
    mousewheel True
    pagekeys True

    frame:
      xfill True
      padding (10, 20)
      background None

      hbox:
        # xfill True
        align (0.5, 0.5)
        spacing 20
        box_wrap True
        box_wrap_spacing 20

        for pos, id, icon, title in apps:
          button:
            # xalign 0.5
            xysize (152, 8 + 113 + 8 + 24 + 8)
            focus_mask True

            vbox:
              xalign 0.5
              spacing 20

              frame:
                xysize (113,113)
                xalign 0.5
                background icon
                hover_background ElementDisplayable(icon,"hover")

              text (title if len(title) <= 10 else title[:7] + "..."):
                xalign 0.5
                text_align 0.5
                size 24
                color "#fffa"
                hover_color "#FFF"
                font "fonts/ComicHelvetic_Medium.otf"

            action (([SetVariable("game.ui.current_phone_app",None),Return(contact_message_label)] if contact_message_label else Return(["phone_app", [id]])) if id == "messages" else Return(["phone_app", [id]]))

            at transform:
              zoom 0.80
