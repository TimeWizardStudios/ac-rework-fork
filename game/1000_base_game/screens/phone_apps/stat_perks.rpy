style phone_app_stat_perks_frame is frame:
  background Frame("phone apps messages frame_left_message",10,20,30,30)
  padding (8,16,32,16)
  xfill True
style phone_app_stat_perks_title is ui_text:
  color "#DB2"
style phone_app_stat_perks_description is ui_text_small:
  color "#FFF"

screen phone_app_stat_perks(stat_id,return_to="stats",*args,**kwargs):
  python:
    stat=stats_by_id[stat_id]
    perks=stat_perks_by_stat_id[stat.id]
    unlocked=[perk_id for perk_id in perks if perk_id in game.unlocked_stat_perks]
    title="Unlocked\n{} perks".format(stat.title)
  vbox:
    box_reverse True

    viewport:
      xfill True
      mousewheel True
      pagekeys True
      vbox:
        spacing 16
        for perk_id in unlocked:
          $perk=stat_perks_by_id[perk_id]
          if not perk.hidden:
            frame style "phone_app_stat_perks_frame":
              vbox:
                text perk.title style "phone_app_stat_perks_title"
                text perk.description style "phone_app_stat_perks_description"

    use phone_app_default_header(title,return_to)
