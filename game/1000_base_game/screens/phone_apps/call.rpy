style phone_app_call_name is ui_text:
  color "#FC4"
  size 64
  xalign 0.5
  text_align 0.5

style phone_app_call_status is ui_text:
  color "#FFF"
  xalign 0.5
  text_align 0.5

screen phone_app_call(contact,*args,**kwargs):
  python:
    if isinstance(contact,basestring):
      contact=game.characters[contact]
  vbox:
    align (0.5,0.5)
    spacing 16
    text str(contact) style "phone_app_call_name" size (56 if contact.default_name == "Master Splinter's Pizza" else 64)
    fixed:
      xalign 0.5
      fit_first True
      add "phone portrait_bg"
      add contact.contact_icon
    text (mc["custom_call_text"] if mc["custom_call_text"] else "Voice call\nin progress") style "phone_app_call_status"
  null height 0 ## Without this line the vbox above doesn't get centered for whatever reason
