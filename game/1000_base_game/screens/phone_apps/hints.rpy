style phone_app_hints_quest_frame is frame:
  background Frame("phone apps messages frame_left_message",10,20,30,30)
  padding (8,16,32,16)
  xfill True
style phone_app_hints_quest_title is ui_text:
  color "#DB2"
style phone_app_hints_quest_hint is ui_text_small:
  color "#FFF"
style phone_app_hints_quest_guide_hint is ui_text_small:
  color "#AAA"

screen phone_app_hints(*args,**kwargs):
  vbox:
    box_reverse True

    viewport:
      xfill True
      mousewheel True
      pagekeys True
      vbox:
        spacing 16
        for quest_id in game.quest_started_order:
          $quest=game.quests[quest_id]
          if not quest.hidden:
            if quest.hint:
              frame style "phone_app_hints_quest_frame":
                vbox:
                  text quest.title style "phone_app_hints_quest_title"
                  text quest.hint style "phone_app_hints_quest_hint"
                  if game.quest_guide==quest.id:
                    if quest.quest_guide_hint:
                      text quest.quest_guide_hint style "phone_app_hints_quest_guide_hint"

    use phone_app_default_header("Hints","selector")
