screen phone_app_contact_info(contact,*args,**kwargs):
  python:
    if isinstance(contact,basestring):
      contact=game.characters[contact]
    contact_call_label=contact.call_label()
    contact_message_label=contact.message_label()
  vbox:
    box_reverse True

    viewport:
      xfill True
      mousewheel True
      pagekeys True
      frame:
        background None
        padding (16,16)
        vbox:
          xfill True
          spacing 16
          frame:
            xalign 0.5
            background "phone portrait_bg"
            add contact.contact_icon
          text "[contact]":
            style "ui_text_large"
            xalign 0.5
            color "#FFF"
            if contact.default_name == "Master Splinter's Pizza":
              size 56
              text_align 0.5
          hbox:
            xalign 0.5
            spacing 64
            imagebutton:
              idle "phone apps contact_info call"
              hover ElementDisplayable("phone apps contact_info call","hover")
              if game.ui.dialog_mode == "contact_info":
                pass
              elif contact_call_label:
                action [SetVariable("game.ui.current_phone_app",None),Return(contact_call_label)]
              else:
                action Return("no_phone_call_label")
            imagebutton:
              idle "phone apps contact_info text"
              hover ElementDisplayable("phone apps contact_info text","hover")
              if game.ui.dialog_mode == "contact_info":
                pass
              elif contact_message_label:
                action [SetVariable("game.ui.current_phone_app",None),Return(contact_message_label)]
              else:
                action Return(["phone_app",["messages",contact.id,["contact_info",contact.id]]])
          vbox:
            spacing 8
            for stat in contact.stats_order:
              $stat=stats_by_id[stat]
              if not stat.hidden:
                $level=getattr(contact,stat.id)
                vbox:
                  text "[stat]"
                  use phone_stat_bar(level,stat.max_level,stat.color)
          vbox:
            $notes=contact.contact_notes()
            if notes:
              for note_title,note_content in notes:
                if not isinstance(note_content,basestring):
                  $note_content="\n".join(note_content)
                text note_title
                text note_content size 24

    use phone_app_default_header("Contact\n{}".format(contact)+("{space=-85}" if contact.default_name == "Master Splinter's Pizza" else ""),"contacts")
