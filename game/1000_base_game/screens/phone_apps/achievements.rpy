style phone_app_achievements_entry_frame is frame:
  background Frame("phone apps messages frame_left_message",10,20,30,30)
  padding (8,16,32,16)
  xfill True
style phone_app_achievements_title is ui_text:
  color "#DB2"
style phone_app_achievements_desc is ui_text_small:
  color "#FFF"
style phone_app_achievements_status is ui_text_small:
  color "#FFF"
  align (0.0,0.5)
style phone_app_achievements_button is textbutton:
  xalign 0.5
  color "#420"

init python:
  def reset_all_progress(confirm=True):
    if confirm:
      Confirm("Reset all achievements and progress?",Function(reset_all_progress,False))()
    else:
      reset_achievements(False)
      reset_progress(False)

screen phone_app_achievements(*args,**kwargs):
  style_prefix "phone_app_achievements"
  vbox:
    box_reverse True

    viewport:
      xfill True
      mousewheel True
      pagekeys True
      vbox:
        spacing 16
        for achievement in achievements_by_order:
          frame style "phone_app_achievements_entry_frame":
            vbox:
              hbox:
                xfill True
                text achievement.title() style "phone_app_achievements_title"
              text ("UNLOCKED" if achievement.unlocked else "LOCKED") style "phone_app_achievements_status"
              text achievement.description() style "phone_app_achievements_desc"
#       frame:
#         background None
#         padding (16,8)
#         vbox:
#           text "Reading Progress" style "phone_app_achievements_title" xalign 0.5
#           python:
#             seen_labels,total_labels=calculate_progress()
#             caption=progress_str()
#           use phone_stat_bar(seen_labels,total_labels,"#08F",caption)
#       textbutton "Reset" action Function(reset_all_progress)

    use phone_app_default_header("Feats","selector")
