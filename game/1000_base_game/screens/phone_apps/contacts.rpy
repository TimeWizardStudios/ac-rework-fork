screen phone_app_contacts(*args, **kwargs):
    default contacts = [
        (game.characters[contact] if isinstance(contact, basestring) else contact)
        for contact in game.pc.phone_contacts #+ [jacklyn, lindsey, mrsl, nurse, kate]
    ]
    default sorted_contacts = sorted(contacts, key=(lambda c: c.name.lower()))
    default contact_name_finder = StringFinder([c.name for c in sorted_contacts])
    default search_input_value = FieldInputValue(contact_name_finder, "query")

    vbox:
        box_reverse True

        frame:
            xfill True
            background "#fff"

            viewport:
                xfill True
                mousewheel True
                pagekeys True

                vbox:
                    xfill True

                    python:
                        result = contact_name_finder.result
                        name_result = [(r["searchable"] if (r["matched"] if contact_name_finder.query else True) else None) for r in result]
                        name_letters = [(n[0].lower() if n is not None else None) for n in name_result]
                        name_strings = [r["characters"] for r in result]

                    for index, contact in enumerate(sorted_contacts):
                        $ name_letter = contact.name[0].lower()

                        if not contact_name_finder.query or (contact.name in name_result and contact_name_finder.result[index]["matched"]):
                            $ name_string = name_strings[index]

                            if name_letter in name_letters and name_letters.index(name_letter) == index:
                                frame:
                                    xfill True
                                    background "#eee"
                                    padding (15, 10)

                                    hbox:
                                        spacing 10

                                        text contact.name[0].upper():
                                            font "fonts/ComicHelvetic_Heavy.otf"
                                            size 28
                                            yalign 0.5
                                            xalign 0.0
                                            color "#b540ff"

                                        add Solid("#b540ff77", ysize=2, yalign=0.5, xalign=0.0)

                            frame:
                                padding (10, 5)
                                background None

                                button:
                                    xfill True
                                    padding (16, 8)
                                    background AlphaMask("#aaa1", Frame("phone apps contacts contact_hover_background"))
                                    hover_background AlphaMask("#aaa9", Frame("phone apps contacts contact_hover_background"))

                                    hbox:
                                        spacing 16

                                        frame:
                                            yalign 0.5
                                            background "phone portrait_bg"

                                            add contact.contact_icon

                                            at transform:
                                                zoom 0.60

                                        hbox:
                                            yalign 0.5

                                            for char in name_string:
                                                frame:
                                                    background ("#d9b800" if char["matched"] else None)
                                                    padding (0, 2)

                                                    text "{}{}{}".format(
                                                        "{color=#151515}" if char["matched"] else "",
                                                        char["character"],
                                                        "{/color}" if char["matched"] else "",
                                                    ):
                                                        xalign 0.0
                                                        font "fonts/ComicHelvetic_Medium.otf"
                                                        size 24
                                                        color "#15151599"
                                                        hover_color "#151515"

                                    action Return(["phone_app", ["contact_info", contact.id]])
                        

                            if name_result.index(contact.name) != len(name_result) - 1:
                                $ next_name_letter = name_letters[index + 1]
                                
                                if next_name_letter is not None and name_letters.index(next_name_letter) != index + 1:
                                    add Window("#ccca", style="default", margin=(10, 0), ysize=3, yalign=0.5, xalign=0.0)
                
                if contact_name_finder.query and not contact_name_finder.match_count:
                    frame:
                        xfill True
                        background "#eee"
                        padding (15, 10)

                        hbox:
                            spacing 10

                            text "No Matches":
                                font "fonts/ComicHelvetic_Heavy.otf"
                                size 28
                                yalign 0.5
                                xalign 0.0
                                color "#b540ff"

                            add Solid("#b540ff77", ysize=2, yalign=0.5, xalign=0.0)
        vbox:
            xfill True

            frame:
                xfill True
                ysize 90
                padding (20, 20, 20, 20)
                background "#fff"

                button:
                    action search_input_value.Toggle()

                    background Transform(
                        Fixed(
                            Solid("#ddde"),
                            Solid("#15151555", xsize=2, align=(0.0, 0.5)),
                            Solid("#15151555", ysize=2, align=(0.5, 0.0)),
                            Solid("#15151555", xsize=2, align=(1.0, 0.5)),
                            Solid("#15151555", ysize=2, align=(0.5, 1.0)),
                            Transform("phone apps search_icon", zoom=0.40, align=(1.0, 0.5), xoffset=-13, alpha=0.75)
                        ),
                        alpha=0.5
                    )
                    selected_background Transform(
                        Fixed(
                            Solid("#ddde"),
                            Solid("#15151555", xsize=2, align=(0.0, 0.5)),
                            Solid("#15151555", ysize=2, align=(0.5, 0.0)),
                            Solid("#15151555", xsize=2, align=(1.0, 0.5)),
                            Solid("#15151555", ysize=2, align=(0.5, 1.0)),
                            Transform("phone apps search_icon", zoom=0.40, align=(1.0, 0.5), xoffset=-13, alpha=0.75)
                        ),
                        alpha=1.0
                    )
                    hover_background Transform(
                        Fixed(
                            Solid("#ddde"),
                            Solid("#15151555", xsize=2, align=(0.0, 0.5)),
                            Solid("#15151555", ysize=2, align=(0.5, 0.0)),
                            Solid("#15151555", xsize=2, align=(1.0, 0.5)),
                            Solid("#15151555", ysize=2, align=(0.5, 1.0)),
                            Transform("phone apps search_icon", zoom=0.40, align=(1.0, 0.5), xoffset=-13, alpha=0.75)
                        ),
                        alpha=0.75
                    )

                frame:
                    xfill True
                    padding (20, 13)
                    background None

                    input:
                        value search_input_value

                        font "fonts/ComicHelvetic_Light.otf"
                        size 24
                        color "#151515"
                        pixel_width 300
                        caret "search_caret"

                    if not contact_name_finder.query:
                        text "Search":
                            font "fonts/ComicHelvetic_Light.otf"
                            size 24
                            color "#15151577"

            add Solid("#151515", ysize=1)

        if search_input_value.editable:
            key "input_enter" action search_input_value.Toggle()

        use phone_app_default_header("Contacts", "selector", header_color="#b540ff", header_text_color="#151515")

image search_caret:
    Solid("#151515", xsize=2)

    block:
        alpha 1.0
        pause 0.50
        alpha 0.0
        pause 0.50
        repeat

init python:
    def LocalVariableInputValue(variable, default=True, returnable=False):
        return DictInputValue(sys._getframe(1).f_locals, variable, default, returnable)

    class StringFinder(object):
        def __init__(self, searchables, case_sensitive=False):
            self.searchables = searchables if (searchables, (list, tuple)) else [searchables]
            self.case_sensitive = case_sensitive
            self.result = []
            self.matched_result = []
            self.match_count = 0
            self.query = ""

        @property
        def query(self):
            return self._query

        @query.setter
        def query(self, other):
            self._query = other

            self.find(self._query)

        def find(self, query):
            self.result.clear()
            self._query = query
            
            query = self.query.lower() if not self.case_sensitive else self.query

            for index, searchable in enumerate(self.searchables):
                original_searchable = self.searchables[index]

                if not self.case_sensitive:
                    searchable = searchable.lower()

                in_searchable = query and query in searchable

                self.result.append({"searchable": original_searchable, "characters": [], "matched": False})

                if in_searchable:
                    self.result[index]["matched"] = True
                    
                continue_index = None

                for index_, char in enumerate(original_searchable):
                    if query and searchable[index_:index_ + len(query)] == query:
                        continue_index = index_ + len(query)

                    elif continue_index is not None and index_ == continue_index:
                        continue_index = None

                    if continue_index is not None:
                        self.result[index]["characters"].append(
                            {
                                "character": char,
                                "matched": True
                            }
                        )

                    else:
                        self.result[index]["characters"].append(
                            {
                                "character": char,
                                "matched": False
                            }
                        )

            self.matched_result = [r for r in self.result if r["matched"]]
            self.match_count = len(self.matched_result)

            renpy.restart_interaction()