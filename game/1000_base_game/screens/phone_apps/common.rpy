style phone_app_default_header_frame is frame:
  background VBox(
    Solid("#eee"),
    Solid("#15151599", ysize=5)
  )
  padding (10, 5)
  # xminimum 450
  xfill True
  ysize 62
  xalign 0.5

screen phone_app_default_header(title,return_to=None, **kwargs):
  if isinstance(return_to,basestring):
    $return_to=[return_to]
    
  vbox:
    frame style "phone_app_default_header_frame":
      if kwargs.get("header_color"):
        background VBox(
          Solid(kwargs.get("header_color")),
          Solid("#15151599", ysize=5)
        )

      $ contact = kwargs.get("contact")

      if contact:
        hbox:
          xsize 0.5
          align (1.0, 0.5)

          vbox:
            yalign 0.5
            xalign 1.0

            text contact.name:
              color "#151515"
              font "fonts/ComicHelvetic_Medium.otf"
              text_align 1.0
              xalign 1.0
              yalign 0.0
              size 22
            
            text contact.contact_number:
              color "#151515bb"
              font "fonts/ComicHelvetic_Light.otf"
              text_align 1.0
              xalign 1.0
              yalign 1.0
              size 16

          null width 10

          add contact.contact_icon zoom 0.35

          null width 10

      hbox:
        xsize 0.5
        yfill True
        align (0.0, 0.5)
        spacing 5

        imagebutton:
          yalign 0.5
          idle "phone apps return_alt"
          hover ElementDisplayable("phone apps return_alt","hover")
          action (Return(["phone_app",return_to]) if return_to else NullAction())
          sensitive return_to

          at transform:
            zoom 0.40 xoffset -5

        text title:
          yalign 0.5
          color "#151515"
          size 24
          font "fonts/ComicHelvetic_Medium.otf"

          if kwargs.get("header_text_color"):
            color kwargs.get("header_text_color")

    # null height 8

screen phone_stat_bar(level,max_level,color,caption=None):
  python:
    img=renpy.easy.displayable("phone stats_bar_left")
    while not isinstance(img,(renpy.display.im.Image,DynamicImage)):
      img=img._target()
    left_img=im.MatrixColor(img,im.matrix.colorize("#000",color))
    img=renpy.easy.displayable("phone stats_bar_right")
    while not isinstance(img,(renpy.display.im.Image,DynamicImage)):
      img=img._target()
    right_img=im.MatrixColor(img,im.matrix.colorize("#000",color))
    if caption is None:
      caption="[level]/[max_level]"
  fixed:
    ysize 50
    add Frame("phone stats_bg",24,12)
    bar:
      ysize 50
      left_bar Frame(left_img,24,12)
      right_bar Frame(right_img,24,12)
#      left_gutter 0#24
#      right_gutter 0#24
      value level
      range max_level
    text caption:
      style "ui_text"
      align (0.5,0.9)
      size 32
      color "#FFF"
      outlines [(4,"#440")]
