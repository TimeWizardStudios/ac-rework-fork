style phone_app_stats_button is textbutton

screen phone_app_stats(*args,**kwargs):
  style_prefix "phone_app_stats"
  vbox:
    box_reverse True

    frame:
      background None
      padding (16,8)
      vbox:
        spacing 8
        for stat in game.pc.stats_order:
          $stat=stats_by_id[stat]
          if not stat.hidden:
            $level=getattr(game.pc,stat.id)
            vbox:
              if stat.id in stat_perks_by_stat_id:
                hbox:
                  xfill True
                  text "[stat]" align (0.0,0.5)
                  textbutton "Perks" action Return(["phone_app",["stat_perks",stat.id,"stats"]]) align (1.0,0.5)
              else:
                text "[stat]"
              use phone_stat_bar(level,stat.max_level,stat.color)

    use phone_app_default_header("Stats","selector")
