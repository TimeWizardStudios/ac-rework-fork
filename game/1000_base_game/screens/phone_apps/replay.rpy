style phone_app_replay_title is ui_text:
  color "#FFF"
  outlines [(2,"#000")]
  align (0.5,0.5)
  text_align 0.5
  size style.ui_text.size * 1.35
  bold True

style phone_app_replay_locked is phone_app_replay_title:
  color "#AAA"

screen phone_app_replay(*args,**kwargs):
  vbox:
    box_reverse True

    viewport:
      xfill True
      mousewheel True
      pagekeys True
      #draggable True #tbd?
      #edgescroll (120,1000) #tbd?  (pixels from edge, scroll speed) third optional value: acceleration -1 to 1
      vbox:
        xfill True
        spacing 16
        for replay_id,replay_title,replay_icon,replay_label in replays_by_order:
          $unlocked=is_replay_unlocked(replay_id)
          $replay_icon=replay_icon if renpy.loadable("images/"+replay_icon.replace(" ","/")+".webp") else "quick_start placeholder"
          button:
            style "button"
            xysize (399,230)
            xalign 0.5

            # sensitive unlocked

            fixed:
                # add Fixed(im.AlphaMask(renpy.displayable(replay_icon),renpy.displayable("ui game_menu slot_mask")), AlphaMask(Null() if unlocked else "#151515f0", "ui game_menu slot_mask"))
                add Fixed(AlphaMask((im.Blur(renpy.displayable(replay_icon), 2.5) if not unlocked else renpy.displayable(replay_icon)), renpy.displayable("ui game_menu slot_mask")), AlphaMask(Null() if unlocked else "#15151555", "ui game_menu slot_mask"))
                frame:
                  background "ui game_menu slot_idle"
                  hover_background "ui game_menu slot_hover"
            if unlocked:
              text _(replay_title).upper() style "phone_app_replay_title"
              action Return(replay_label)
            else:
              text _("Locked").upper() style "phone_app_replay_locked"
              action NullAction()

    use phone_app_default_header("Replay","selector")
