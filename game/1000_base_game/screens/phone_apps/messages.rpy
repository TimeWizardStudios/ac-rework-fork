style phone_app_message_left is frame:
  background Frame("ui dialog frame_phone_say_bg_alt_3", 15, 15, 20, 20)
  padding (15, 15, 20, 20)
  left_margin 10
  top_margin 23
  xmaximum 300

style phone_app_message_right is frame:
  background Frame("ui dialog frame_phone_say_bg_mc", 20, 15, 15, 20)
  padding (20, 15, 15, 20)
  right_margin 10
  top_margin 23
  xmaximum 300
  xalign 1.0

style phone_app_message_right_lindsey is phone_app_message_right:
  background Frame("ui dialog frame_phone_say_bg_lindsey", 20, 15, 15, 20)

style phone_app_message_text_left is ui_text_small:
  color "#000"
  size 20

style phone_app_message_text_right is ui_text_small:
  color "#fff"
  size 20
  text_align 1.0

style phone_app_message_timestamp is ui_text_small:
  color "#151515"
  size 22
  xsize 300
  bold True
  align (0.0, 0.5)

default messages_vp_status=(None,0.0)

init python:
  from functools import partial

  class MessagesVPController(renpy.Displayable):
    def render(self,width,height,st,at):
      vp=renpy.get_widget("phone","messages_vp")
      if vp:
        adj=vp.yadjustment
        mode,range=store.messages_vp_status
        if range!=adj.range:
          mode="scroll"
          range=adj.range
        if mode=="scroll":
          if adj.value!=adj.range:
            delta=adj.range-adj.value
            adj.change(adj.value+min(max(3.0,delta/10.0),delta))
          else:
            mode=None
        store.messages_vp_status=(mode,range)
      renpy.redraw(self,0)
      rv=renpy.Render(1,1)
      return rv

screen phone_app_messages(contact=None,return_to="selector",*args,**kwargs):
  python:
    contact=contact or game.pc.last_phone_message_contact
    title="Messages"
    if isinstance(contact,basestring):
      contact=game.characters[contact]
    if contact is not None:
      # title+="\nfrom "+str(contact)
      message_history=game.pc.phone_message_history.get(contact.id,[])

  vbox:
    box_reverse True
    align (0.5, 0.5)

    vbox:
      xfill True

      add Solid("#151515", ysize=1)

      frame:
        ysize 60
        xfill True
        background "#fff"
        padding (15, 10)

        hbox:
          align (0.5, 0.5)
          spacing 10

          # add Frame("#ddd", xsize=350)
          button:
            action NullAction()

            xsize 350
            background Transform(
              Fixed(
                  Solid("#ddde"),
                  Solid("#15151555", xsize=2, align=(0.0, 0.5)),
                  Solid("#15151555", ysize=2, align=(0.5, 0.0)),
                  Solid("#15151555", xsize=2, align=(1.0, 0.5)),
                  Solid("#15151555", ysize=2, align=(0.5, 1.0))
              ),
              alpha=0.5
            )
            selected_background Transform(
              Fixed(
                  Solid("#ddde"),
                  Solid("#15151555", xsize=2, align=(0.0, 0.5)),
                  Solid("#15151555", ysize=2, align=(0.5, 0.0)),
                  Solid("#15151555", xsize=2, align=(1.0, 0.5)),
                  Solid("#15151555", ysize=2, align=(0.5, 1.0))
              ),
              alpha=1.0
            )
            hover_background Transform(
              Fixed(
                  Solid("#ddde"),
                  Solid("#15151555", xsize=2, align=(0.0, 0.5)),
                  Solid("#15151555", ysize=2, align=(0.5, 0.0)),
                  Solid("#15151555", xsize=2, align=(1.0, 0.5)),
                  Solid("#15151555", ysize=2, align=(0.5, 1.0))
              ),
              alpha=0.75
            )

          button:
            action NullAction()

            xfill True
            yfill True
            background AlphaMask(("#1B90F9" if game.pc == "lindsey" else "#0acc4a"), Frame("phone apps contacts contact_hover_background", 20, 20, 20, 20))
            hover_foreground "#fff5"
            add "phone apps messages send_icon" zoom 0.35 align (0.5, 0.5) xoffset 3
          # add Solid("#0af", xsize=50, ysize=30)

    frame:
      xfill True
      background ("phone wallpaper" if game.pc == "lindsey" else "#eee")
      
      if game.pc == "lindsey":
        viewport_custom:
          id "messages_vp"
          mousewheel True
          pagekeys True
          yinitial 1.0
          ysize 650
          frame:
            background None
            padding (20, 10)
            vbox:
              xfill True
              spacing 8
              if contact is None:
                text "No recent messages":
                  xalign 0.5
              else:
                if not message_history:
                  fixed:
                    ysize 36
                    hbox:
                      align (0.5, 0.5)
                      spacing 10

                      add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)
                      text "Empty Conversation" style "phone_app_message_timestamp"
                      add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)

                      at transform:
                        alpha 0.50


                for msg_n,msg_info in enumerate(message_history):
                  $msg_side=msg_info[0]
                  $msg=msg_info[1]
                  $timestamp=msg_info[2] if len(msg_info)>2 else None
                  ## show mc messages
                  if msg_n==0 or message_history[msg_n-1][-1]!=timestamp:
                    if timestamp:
                      fixed:
                        ysize 36
                        hbox:
                          align (0.5, 0.5)
                          spacing 10

                          add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)
                          text "Day {} - {}".format(timestamp[0],time_str(timestamp[1],0)) style "phone_app_message_timestamp"
                          add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)

                          at transform:
                            alpha 0.50

                  if msg_side:
                    frame style ("phone_app_message_right_lindsey" if game.pc == "lindsey" else "phone_app_message_right"):
                      if len(message_history) > msg_n - 1 >= 0 and message_history[msg_n - 1][0]:
                        top_margin 0

                      if isinstance(msg,basestring):
                        $msg=[msg]
                      vbox:
                        for msg_part in msg:
                          if msg_part.startswith("$"):
                            add msg_part[1:] xalign 0.5
                          else:
                            text msg_part style "phone_app_message_text_right"
                  ## show npc messages
                  else:
                    $ show_contact_icon = (len(message_history) > msg_n - 1 >= 0 and (message_history[msg_n - 1][0])) or (((not msg_n or message_history[msg_n - 1][-1] != timestamp) and timestamp))

                    hbox:
                      if contact:
                        fixed:
                          fit_first True
                          add "phone portrait_bg"
                          add contact.contact_icon align (0.5,0.5)
                          at transform:
                            zoom 0.35 alpha (1.0 if show_contact_icon else 0.0)

                      frame style "phone_app_message_left":
                        if not show_contact_icon:
                          top_margin 0

                        if isinstance(msg,basestring):
                          $msg=[msg]
                        vbox:
                          for msg_part in msg:
                            if msg_part.startswith("$"):
                              add msg_part[1:] xalign 0.5
                            else:
                              text msg_part style "phone_app_message_text_left"
      else:
        viewport:
          id "messages_vp"
          mousewheel True
          pagekeys True
          yinitial 1.0
          ysize 650
          frame:
            background None
            padding (20, 10)
            vbox:
              xfill True
              spacing 8
              if contact is None:
                text "No recent messages":
                  xalign 0.5
              else:
                if not message_history:
                  fixed:
                    ysize 36
                    hbox:
                      align (0.5, 0.5)
                      spacing 10

                      add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)
                      text "Empty Conversation" style "phone_app_message_timestamp"
                      add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)

                      at transform:
                        alpha 0.50


                for msg_n,msg_info in enumerate(message_history):
                  $msg_side=msg_info[0]
                  $msg=msg_info[1]
                  $timestamp=msg_info[2] if len(msg_info)>2 else None
                  ## show mc messages
                  if msg_n==0 or message_history[msg_n-1][-1]!=timestamp:
                    if timestamp:
                      fixed:
                        ysize 36
                        hbox:
                          align (0.5, 0.5)
                          spacing 10

                          add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)
                          text "Day {} - {}".format(timestamp[0],time_str(timestamp[1],0)) style "phone_app_message_timestamp"
                          add Frame("#151515", align=(0.0, 0.5), xsize=100, ysize=2)

                          at transform:
                            alpha 0.50

                  if msg_side:
                    frame style ("phone_app_message_right_lindsey" if game.pc == "lindsey" else "phone_app_message_right"):
                      if len(message_history) > msg_n - 1 >= 0 and message_history[msg_n - 1][0]:
                        top_margin 0

                      if isinstance(msg,basestring):
                        $msg=[msg]
                      vbox:
                        for msg_part in msg:
                          if msg_part.startswith("$"):
                            add msg_part[1:] xalign 0.5
                          else:
                            text msg_part style "phone_app_message_text_right"
                  ## show npc messages
                  else:
                    $ show_contact_icon = (len(message_history) > msg_n - 1 >= 0 and (message_history[msg_n - 1][0])) or (((not msg_n or message_history[msg_n - 1][-1] != timestamp) and timestamp))

                    hbox:
                      if contact:
                        fixed:
                          fit_first True
                          add "phone portrait_bg"
                          add contact.contact_icon align (0.5,0.5)
                          at transform:
                            zoom 0.35 alpha (1.0 if show_contact_icon else 0.0)

                      frame style "phone_app_message_left":
                        if not show_contact_icon:
                          top_margin 0

                        if isinstance(msg,basestring):
                          $msg=[msg]
                        vbox:
                          for msg_part in msg:
                            if msg_part.startswith("$"):
                              add msg_part[1:] xalign 0.5
                            else:
                              text msg_part style "phone_app_message_text_left"

    use phone_app_default_header(
      title,
      return_to,
      contact=contact,
      header_color=("#1B90F9" if game.pc == "lindsey" else "#0acc4a"),
      header_text_color="#151515")

  add MessagesVPController()
