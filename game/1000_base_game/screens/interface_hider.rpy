﻿init python:
  class Event_interface_hider(GameEvent):
    def on_after_load(event):
      if not renpy.get_screen("interface_hider"):
        renpy.show_screen("interface_hider")

style ui_btn_hint_eye_right is ui_btn_hint_right:
  color "#fefefe"
  outlines [(2,"#432a12",0,0)]
  size 28

screen interface_hider():
  zorder 99
  default old_hud_counter=None
  if renpy.get_screen("location"):
    if old_hud_counter is None or old_hud_counter!=game.ui.hide_hud-1:
      use ui_btn("ui hud eye_open",[SetScreenVariable("old_hud_counter",game.ui.hide_hud),SetVariable("game.ui.hide_hud",game.ui.hide_hud+1)],place=(0.0,1.0,0.0,1.0,30,-30),hint="Hide HUD",hint_place="eye_right")
    else:
      use ui_btn("ui hud eye_open",[SetScreenVariable("old_hud_counter",None),SetVariable("game.ui.hide_hud",game.ui.hide_hud-1)],place=(0.0,1.0,0.0,1.0,30,-30),hint="Show HUD",hint_place="eye_right")
  else:
    use ui_btn("ui hud eye_open",HideInterface(),place=(0.0,1.0,0.0,1.0,30,-30),hint="Hide UI",hint_place="eye_right")
