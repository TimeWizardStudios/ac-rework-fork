init python:
  class NotificationHider(BaseClass):
    def __init__(self,n):
      self.n=n
    def __call__(self,*args,**kwargs):
      game.notifications[self.n]=None
      renpy.hide_screen("notification_side_"+str(self.n))
      game.process_delayed_notifications()
      renpy.restart_interaction()

transform tf_notification(tf_tag,n,wait):
  alpha 1.0
  pos (1.0,n*160+160)
  anchor (0.0,0.5)
  xoffset 0
  easein 0.5 xanchor 1.0
  pause wait
  easeout 0.5 xanchor 0.0 xoffset 50
  function NotificationHider(n)

style notification_side_text is ui_text:
  xalign 0.5
  text_align 0.5

screen notification_side(n,tf_tag,icon,msg,wait):
  zorder 99
  style_prefix "notification_side"
  fixed:
    fit_first True
    frame:
      xminimum 200
      background Frame("ui notification message_with_icon_bg",40,16,0,16)
      padding (150,16,16,16)
      hbox:
        xalign 0.5
        spacing 16
        for msg_part in msg.split("|"):
          text msg_part yalign 0.5
    fixed:
      fit_first True
      yalign 0.5
      xoffset -20
      if icon:
        add "ui notification icon_bg" align (0.5,0.5)
        if icon.endswith("contact_icon"):
          add icon align (0.4,0.4)
        else:
          add icon align (0.5,0.5)
      else:
        add "ui notification icon_bg_statgain" align (0.5,0.5)
    at tf_notification(tf_tag,n,wait)
