image ask_name_caret:
  alpha 0
  Solid("#888",xmaximum=4)

  block:
    linear 0.5 alpha 1.0
    linear 0.5 alpha 0.0
    repeat

style ask_name_frame is frame:
  background Frame("ui ask_name frame_bg",20,20,30,30)
  padding (20,20,30,30)
  xminimum 500

style ask_name_button is textbutton:
  xalign 0.5

style ask_name_title is ui_text:
  color "#000"
  xalign 0.5

style ask_name_input is ui_text_large:
  xalign 0.5
  caret "ask_name_caret"

init python:
  # def ask_name_update(name, char):
  #   char.name = name
    # char=renpy.current_screen().scope["char"]
    # name=" ".join(name.split())
    # char.name=name

  # class CheckName(Action):
  #   def __call__(self):
  #     cs=renpy.current_screen().scope
  #     char=cs["char"]
  #     if char.name:
  #       return True
  #     cs["show_need_name"]=True
  #     renpy.restart_interaction()

  def show_need_name_done(tf,st,at):
    cs=renpy.current_screen().scope
    if "show_need_name" in cs:
      cs["show_need_name"]=False
      cs["show_need_name_tag"]+=1

transform tf_show_need_name(tag):
  alpha 0.0
  easein 0.15 alpha 1.0
  easeout 0.5 alpha 0.0
  function show_need_name_done

screen ask_name(title,char):
  style_prefix "ask_name" 
  zorder 7
  default show_need_name=False
  default show_need_name_tag=0
  default char = game.characters[char_] if isinstance(char_, basestring) else char_
  # if isinstance(char,basestring):
  #   $char=game.characters[char]
  vbox:
    align (0.5,0.5)
    frame style "ask_name_frame":
      vbox:
        align (0.5,0.5)
        spacing 16
        fixed:
          xalign 0.5
          fit_first True
          text title style "ask_name_title"
          if show_need_name:
            text title style "ask_name_title":
              color "#F00"
              at tf_show_need_name(0)
        input:
          style "ask_name_input"
          default char.default_name
          value FieldInputValue(char, "name", True, False)
          # changed ask_name_update
          pixel_width 400
          allow "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -'`"
          
    key "K_RETURN" action (Return(True) if char.name.strip() else SetScreenVariable("show_need_name", True))
    textbutton "Continue" action (Return(True) if char.name.strip() else SetScreenVariable("show_need_name", True)) xoffset -10
