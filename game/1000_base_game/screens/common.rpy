init python:
  def normalize_place(place,pos_x=0,pos_y=0,anchor_x=0.0,anchor_y=0.0,offset_x=0,offset_y=0):
    place={n:val for n,val in enumerate(place)} if place else {}
    pos=(place.get(0,pos_x),place.get(1,pos_y))
    anchor=(place.get(2,anchor_x),place.get(3,anchor_y))
    offset=(place.get(4,offset_x),place.get(5,offset_y))
    return pos,anchor,offset

style title_frame_default is frame:
  background Frame("ui frame_objname_gold",45,10,45,30)
  padding (45,14,45,34)
  xminimum 300

style title_frame_nothing_new is title_frame_default:
  background Frame("ui frame_objname",45,10,45,30)

style title_frame_active_nothing_new is title_frame_default:
  background Frame("ui frame_objname_selected",45,10,45,30)

style title_frame_active is title_frame_default:
  background Frame("ui frame_objname_selected_gold",45,10,45,30)

#Controls the text in the title when clicking on an interactable.
style title_frame_content is ui_text:
  align (0.5,0.5)
  text_align 0.5
  color "#EEEEEE"
  #bold True  
  font "fonts/ComicHelvetic_Light.otf"
  size 32
  offset (0,3)

screen title_frame(content,place,style="default"):
  python:
    pos,anchor,offset=normalize_place(place)
    frame_style="title_frame_"+style
  frame style frame_style:
    pos pos
    anchor anchor
    offset offset
    text content style "title_frame_content"

style hint_frame is frame:
  background Frame("ui frame_int_text",45,10,45,30)
  padding (45,10,45,30)

style hint_frame_content is ui_text_small:
  align (0.5,0.5)
  text_align 0.5
  color "#6a431e"
  bold True  
  font "fonts/ComicHelvetic_Light.otf"
  size 24
  offset (0,10)

screen hint_frame(content,place):
  $pos,anchor,offset=normalize_place(place)
  frame style "hint_frame":
    pos pos
    anchor anchor
    offset offset
    text content style "hint_frame_content"
