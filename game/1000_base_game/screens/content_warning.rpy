screen content_warning:

  default clickable = False

  fixed:
    if not clickable:
      at transform:
        alpha 0.0 xoffset 50
        pause 0.25
        easein 0.25 alpha 1.0 xoffset 0

    vbox spacing 70 xalign 0.5 yalign 0.5:
      text "CONTENT WARNING" font "fonts/Fresca-Regular.ttf" color "#fefefe" size 56 xalign 0.5
      # text "Another Chance contains sexually explicit material intended and suitable for adult audiences only. All characters depicted in this work of fiction are aged 18 or above and not biologically related." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      text "Another Chance contains sexually explicit material intended and suitable for adult audiences only.\nAll characters depicted in this work of fiction are aged 18 or above and not biologically related." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      text "You, too, must be at least 18 years old to play it." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      # text "Furthemore, the game features sensitive themes such as bullying, drug use and abuse, humiliation, violence, and suicide — none of which are endorsed nor encouraged by us." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      text "Furthemore, the game features sensitive themes such as bullying, drug use and abuse,\nhumiliation, violence, and suicide — none of which are endorsed nor encouraged by us." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      text "If such content offends you, or you are easily disturbed, please leave right now." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      # text "Lastly, Another Chance is under constant development, with new updates every month. This current release is a work in progress and does not represent the final product." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      text "Lastly, Another Chance is under constant development, with new updates every month.\nThis current release is a work in progress and does not represent the final product." font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      text "Do you understand and want to continue?" font "fonts/ComicHelvetic_Medium.otf" color "#fefefe" size 28 line_spacing 5 text_align 0.5 xalign 0.5
      hbox spacing 20 xalign 0.5:
        textbutton "Yes, continue" text_font "fonts/ComicHelvetic_Medium.otf" text_color "#fefefe" text_hover_color "#000" text_size 28 background Frame("backstory _idle",20,20,20,20) hover_background Frame("backstory _hover",20,20,20,20) xminimum 350 xpadding 70 ysize 70 action If(clickable, Return())
        textbutton "No, leave" text_font "fonts/ComicHelvetic_Medium.otf" text_color "#fefefe" text_hover_color "#000" text_size 28 background Frame("backstory _idle",20,20,20,20) hover_background Frame("backstory _hover",20,20,20,20) xminimum 350 xpadding 70 ysize 70 action If(clickable, Quit())

    timer 0.5 action SetScreenVariable("clickable",True)
