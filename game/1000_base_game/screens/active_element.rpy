define active_element_offset=(0,-65)
define investigate_offset=(0,70)

transform tf_active_element:
  on show,replace:
    alpha 0.0
    easein 0.15 alpha 1.0
  on hide,replaced:
    easeout 0.15 alpha 0.0

init python:
  def is_new_interaction_available(el,action_hint=None):
    new_interaction=False
    el_actions=[]
    el.actions(el_actions)
    el_actions=el.finalize_actions(el_actions)
    if action_hint and action_hint.startswith("{#"):
      action=action_hint[2:].split("}")[0]
      for el_action in el_actions:
        if el_action[0][0]==action:
          el_action=(el_action[2].strip("?!"),el_action[3])
          new_interaction=el_action not in game.seen_actions
    else:
      for el_action in el_actions:
        el_action=(el_action[2].strip("?!"),el_action[3])
        new_interaction=new_interaction or el_action not in game.seen_actions
    return new_interaction

screen element_hint(img,pos,el,ofs_x=0,ofs_y=0,action_hint=None):
  zorder 8
  python:
    if img is not None:
      imgr=img.render(1920,1080,0,0)
      ofs_x+=int(imgr.width//2)
      imgr=None
    x0=pos[0]+ofs_x+active_element_offset[0]
    y0=pos[1]+ofs_y+active_element_offset[1]
  fixed:
    at tf_active_element
    use title_frame(el.title(),(x0,y0,0.5,1.0),style="default" if is_new_interaction_available(el,action_hint) else "nothing_new")
    if action_hint and not renpy.get_screen("active_element"):
      use hint_frame(action_hint,(x0,y0,0.5,0))

init python:
  class InteractableDo(Action):
    def __init__(self,interactable_id,action_type,energy_cost,mark_seen,action_args):
      super().__init__()
      self.interactable_id=interactable_id
      self.action_type=action_type
      self.energy_cost=energy_cost
      self.mark_seen=mark_seen
      self.action_args=action_args
    def __call__(self):
      mc.energy-=process_event("energy_cost",self.interactable_id,self.energy_cost,default_rv=self.energy_cost)
      if self.interactable_id not in game.last_actions:
        game.last_actions[self.interactable_id]={}
      game.last_actions[self.interactable_id][self.action_type]=self.action_args
      if self.mark_seen:
        game.seen_actions.add(self.action_args)
      renpy.retain_after_load()
    def get_sensitive(self):
      return True

screen action_circle(img,pos,el,ofs_x=0,ofs_y=0,forced_args=None,force_action=None):
  default action_hint=""
  default forced_action=force_action
  tag active_element
  zorder 9
  if forced_action and not isinstance(forced_action,basestring):
    timer 0.01 action forced_action
  python:
    el_actions=[]
    el.actions(el_actions)
    el_actions=el.finalize_actions(el_actions)
    if img is not None:
      imgr=img.render(1920,1080,0,0)
      ofs_x+=int(imgr.width//2)
      imgr=None
    x0=pos[0]+ofs_x+active_element_offset[0]
    y0=pos[1]+ofs_y+active_element_offset[1]
    circle=el.circle(el_actions,x0,y0)
    cx0=x0+circle.center[0]
    cy0=y0+circle.center[1]
    angle=circle.start_angle
    to_radians=math.pi/180.0
    actions=[]
    for act in el_actions:
      x=int(cx0-circle.radius*math.sin(-angle*to_radians))
      y=int(cy0-circle.radius*math.cos(-angle*to_radians))
      angle+=circle.angle_per_icon
      act_place=(x,y,0.5,0.5)
      act_type=act[0]
      if isinstance(act_type,basestring):
        act_energy=0
      else:
        act_energy=act_type[1]
        act_type=act_type[0]
      act_icon="actions "+act_type
      act_hint=act[1]
      act_action=act[2]
      act_action_args=list(act[3])
      dull_seen="?" in act_action
      if "!" in act_action:
        act_action_args.insert(0,[el,x0,y0])
      act_action=act_action.replace("?","").replace("!","")
      if forced_args:
        act_action_args.extend(forced_args)
      act_action_args=tuple(act_action_args)
      actions.append([act_place,act_type,act_energy,act_icon,act_hint,dull_seen,act_action,act_action_args])
  button:
    if forced_action:
      action NullAction()
    else:
      action [Hide("action_circle"),Hide("element_hint")]
  fixed:
    at tf_active_element
    if not forced_action:
      use title_frame(el.title(),(x0,y0,0.5,1.0),style="active" if is_new_interaction_available(el) else "active_nothing_new")
      if action_hint:
        use hint_frame(action_hint,(x0,y0,0.5,0))
    for act_place,act_type,act_energy,act_icon,act_hint,dull_seen,act_action,act_action_args in actions:
      $pos,anchor,offset=normalize_place(act_place)
      if act_action:
        python:
          if act_action=="investigate":
            action=Show("investigate",None,*act_action_args)
          else:
            action=Return([act_action,act_action_args])
          act_action_args=(act_action,tuple([v for v in act_action_args if not isinstance(v,list)]))
          action=[InteractableDo(el.id,act_type,act_energy,dull_seen,act_action_args),action]
          if forced_action and act_type==forced_action:
            forced_action=action
            renpy.restart_interaction()
        if not forced_action:
          imagebutton:
            pos pos
            anchor anchor
            offset offset
            focus_mask act_icon
            if dull_seen and act_action_args in game.seen_actions:
              idle Transform(act_icon,alpha=0.5)
            else:
              idle act_icon
            hover ElementDisplayable(act_icon,"hover")
            action action
            hovered SetScreenVariable("action_hint",act_hint)
            unhovered SetScreenVariable("action_hint","")
      else:
        if not forced_action:
          add ElementDisplayable(act_icon,"inactive"):
            pos pos
            anchor anchor
            offset offset

style investigate_frame is frame:
  background Frame("ui frame_popup")
  padding (50,30,50,40)
  xsize 500

style investigate_frame_content is ui_text_small:
  align (0.5,0.5)
  text_align 0.5
  color "#6a431e"
  #xmaximum 440
  font "fonts/ComicHelvetic_Medium.otf"
  #font "fonts/dinomouse.regular.otf"
  size 24
  line_spacing 2
  kerning -.01

init python:
  class InvestigateHide(Action):
    def __call__(self):
      cs=renpy.current_screen().scope
      if "obj_id" in cs:
        process_event("investigate_shown",cs["obj_id"],cs["description"])
      Hide("investigate")()
      if renpy.get_screen("inventory"):
        Hide("element_hint")()
      if game.events_queue:
        return True

  def investigate_fix_position(tf,st,at):
    r=tf.child.render(config.screen_width,config.screen_height,st,at)
    rw,rh=r.get_size()
    x,y=renpy.display.core.place(config.screen_width,config.screen_height,rw,rh,tf.child.get_placement())
    if x<0:
      tf.xalign=0.0
    if y<0:
      tf.yalign=0.0
    if x+rw+investigate_offset[0]>=config.screen_width:
      tf.xalign=1.0
    if y+rh+investigate_offset[1]>=config.screen_height:
      tf.yalign=1.0

transform tf_investigate_frame:
  function investigate_fix_position

screen investigate(target,obj_id,description,*args,**kwargs):
  tag active_element
  zorder 9
  $pos=(target[1],target[2])
  $target=target[0]
  timer 3.0+(0.05*len(description)) action InvestigateHide()
  fixed:
    at tf_active_element
    frame style "investigate_frame":
      at tf_investigate_frame
      xpos pos[0] + investigate_offset[0]
      ypos pos[1] + investigate_offset[1]
      anchor (0.5,0)
      text description style "investigate_frame_content"
  button:
    action InvestigateHide()
