style inventory_title_frame is frame:
  background Frame("ui inventory frame_title",130,20,40,20)
  ysize 100
  padding (130,0,40,0)
  xalign 0.5

style inventory_title_text is ui_text:
  align (0.5,0.5)
  text_align 0.5
  font "fonts/ComicHelvetic_Light.otf"

style inventory_frame is frame:
  background Frame("ui inventory bg",400,280)
  padding(60,40,60,120)
  xysize (1350,870)
  xalign 0.5

style inventory_item_frame is frame:
  background "ui inventory item_bg"
  hover_background ElementDisplayable("ui inventory item_bg","hover")
  xysize (132,107)
  align (0.5,0.5)

style inventory_item_count_frame is frame:
  background Frame("ui inventory frame_count",20,8,20,15)
  padding (15,5)
  align (1.0,1.0)
  offset (-30,-15)

style inventory_item_count_text is ui_text:
  align (0.5,0.5)

style inventory_page is ui_text

screen inventory(title=None,char=None):
  tag game_mode
  zorder 5
  key "game_menu" action Return()
  default current_page=0
  default slot_w=300
  default slot_h=210
  default slot_bg_zoom=0.75
  python:
    if isinstance(title,basestring) and title.startswith("$"):
      item_filter,title=title[1:].split("|",1)
      if ":" in item_filter:
        item_filter,item_filter_arg=item_filter.split(":",1)
      else:
        item_filter_arg=None
      item_filter=getattr(store,item_filter,None)
    else:
      item_filter,item_filter_arg=None,None
    char=char or game.pc
    if isinstance(char,basestring):
      char=game.characters[char]
    inv=char.inv
    max_page=max(0,(len(inv)+11)//12-1)
    current_page=min(max_page,game.ui.inventory_page)
  button:
    add "#0008"
    action Return()
  vbox:
    align (0.5,1.0)
    spacing 16
    frame style "inventory_title_frame":
      text title or "Inventory" style "inventory_title_text"
    frame style "inventory_frame":
      button:
        action NullAction()
      fixed:
        xysize (slot_w*4,slot_h*3)
        align (0.5,0.0)
        for i in range(12):
          python:
            x=(i%4)*slot_w+slot_w//2
            y=(i//4)*slot_h+slot_h//2
          if len(inv)>(current_page*12+i):
            python:
              item,item_count=inv[current_page*12+i]
              item=items_by_id[item]
              icon=item.icon if renpy.loadable("images/"+item.icon.replace(" ","/")+".webp") else "items placeholder"
              if callable(item_filter):
                if item_filter_arg:
                  item_mode=item_filter(item_filter_arg,item.id)
                else:
                  item_mode=item_filter(item.id)
              else:
                item_mode=None
            button:
              xysize (slot_w,slot_h)
              pos (x,y)
              anchor (0.5,0.5)
              frame:
                background Frame("ui inventory item_bg",73,53,56,53)
                hover_background Frame(ElementDisplayable("ui inventory item_bg","hover"),73,53,56,53)
                xysize (int(slot_w*slot_bg_zoom),int(slot_h*slot_bg_zoom))
                align (0.5,0.5)
              if item_mode=="disabled":
                add ElementDisplayable(icon,"inactive") align (0.5,0.5)
                action NullAction()
              else:
                if item_mode in ("grey","gray"):
                  add ElementDisplayable(icon,item_mode) align (0.5,0.5)
                else:
                  add icon align (0.5,0.5)
                if item_count>1:
                  frame style "inventory_item_count_frame":
                    text str(item_count) style "inventory_item_count_text"
                if title:
                  action Return(item.id)
                else:
                  action Show("action_circle",None,None,(x+360,y+230),item,forced_args=[item.id])
                if not renpy.get_screen("active_element"):
                  hovered Show("element_hint",None,None,(x+360,y+230),item)
                  unhovered Hide("element_hint")
          else:
            add Frame("ui inventory item_bg_empty",40,50):
              size (int(slot_w*slot_bg_zoom),int(slot_h*slot_bg_zoom))
              pos (x,y)
              anchor (0.5,0.5)
      fixed:
        yfit True
        xfill True
        align (0.5,1.0)
        if max_page>0:
          use ui_btn("ui prev_page",SetVariable("game.ui.inventory_page",(current_page-1)%(max_page+1)),hint="Prev",place=(0.1,0.5,0.0,0.5))
        text "Page {} of {}".format(current_page+1,max_page+1) align (0.5,0.5) style "inventory_page"
        if max_page>0:
          use ui_btn("ui next_page",SetVariable("game.ui.inventory_page",(current_page+1)%(max_page+1)),hint="Next",place=(0.9,0.5,1.0,0.5),hint_place="right")
  if quest.nurse_aid == "fishing_pole" and (quest.isabelle_tour.actually_finished and not mc.owned_item("compromising_photo") and quest.isabelle_dethroning.finished) and mc.owned_item("fishing_pole"):
    button:
      action Return()
