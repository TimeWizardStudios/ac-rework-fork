style ui_btn_hint is ui_text:
  layout "nobreak"
  color "#fefefe"
  outlines [(2,"#432a12",0,0)]
  size 28

style ui_btn_hint_left is ui_btn_hint:
  pos (0.0,0.5)
  anchor (1.0,0.5)
  xoffset -16
  text_align 1.0

style ui_btn_hint_right is ui_btn_hint:
  pos (1.0,0.5)
  anchor (0.0,0.5)
  xoffset 16
  text_align 0.0

style ui_btn_hint_top is ui_btn_hint:
  pos (0.5,0.0)
  anchor (0.5,1.0)
  yoffset -16
  text_align 0.5

style ui_btn_hint_bottom is ui_btn_hint:
  pos (0.5,1.0)
  anchor (0.5,0.0)
  yoffset 16
  text_align 0.5

style ui_btn_title is ui_text_small:
  layout "nobreak"
  align (0.5,0.5)
  text_align 0.5

screen ui_btn(bg,action,title=None,hint=None,place=None,hint_place="left"):
  python:
    if place:
      pos,anchor,offset=normalize_place(place)
  if title:
    button:
      if place:
        pos pos
        anchor anchor
        offset offset
      frame:
        background Frame(bg[0],*bg[1])
        hover_background Frame(ElementDisplayable(bg[0],"hover"),*bg[1])
        if len(bg)>2:
          padding bg[2]
        if len(bg)>3:
          xminimum bg[3][0]
          if len(bg[3])>1:
            yminimum bg[3][1]
          if len(bg[3])>2:
            xmaximum bg[3][2]
          if len(bg[3])>3:
            ymaximum bg[3][3]
        if hint:
          hover_foreground Text(hint,style="ui_btn_hint_"+hint_place)
        text title style "ui_btn_title"
      action action
  else:
    imagebutton:
      idle bg
      hover ElementDisplayable(bg,"hover")
      if place:
        pos pos
        anchor anchor
        offset offset
      if hint:
        hover_foreground Text(hint,style="ui_btn_hint_"+hint_place)
      action action
