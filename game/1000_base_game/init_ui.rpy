label init_screens:
  ## Reseting some variables, showing some always shown screens.
  $quick_menu=False
  show screen hud
  show screen interface_hider
  $skip_to=None
  show screen skip_to_target
  return

init python:
  ## Setting default values for RenPy internals
  config.thumbnail_width=399
  config.thumbnail_height=230

  config.autosave_slots=6
  config.quicksave_slots=6

  config.optimize_texture_bounds=True

  config.mouse_hide_time=None

  config.end_splash_transition=dissolve

  config.narrator_menu=True

  config.fade_music=1.0

  config.main_menu_music="<loop 8.0>main_menu"

  renpy.start_predict("actions *")
  renpy.start_predict("ui *")
  renpy.start_predict("phone *")
  renpy.start_predict("* contact_icon")
  renpy.start_predict("stats *")

init python:
  ## This needed to handle btn_idle.png, not just btn_idle_.png
  ## Unused in current codebase, migrated from older to fix some issues.
  ## If all state images are generated using ElementDisplayable then can be removed.
  for v in renpy.styledata.stylesets.prefix_search.values():
    for i,s in enumerate(v):
      if s.endswith("_"):
        v.insert(i+1,"_"+s[:-1])
