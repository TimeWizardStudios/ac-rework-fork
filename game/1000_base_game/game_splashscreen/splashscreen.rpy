label splashscreen:
  call screen content_warning
  pause 0.25
  scene ui splashscreen studio_logo_bg with Dissolve(.5)
  show ui splashscreen studio_logo as logo:
    xalign 0.5
    ypos 1.0
    yanchor 0.0
    parallel:
      linear 1.0 ypos 0.5
    parallel:
      linear 1.0 yanchor 0.5
  $renpy.pause(4.0)
  return
