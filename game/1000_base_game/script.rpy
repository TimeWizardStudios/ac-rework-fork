default intro_label = "intro"

label start:
  ## init new game
  call init_game
  call init_screens
  ## show intro
  call expression intro_label
  ## actual interactive game
  call main_loop
  ## if left, show ending
  call ending
  return
