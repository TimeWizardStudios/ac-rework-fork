label intro:
  scene black
  with fadehold

  $mc.add_phone_contact(hidden_number,silent=True)
  $mc.add_phone_contact(jo,silent=True)
  $mc.add_phone_contact(flora,silent=True)

  play music "<loop 30.2279>intro"
  scene intro bg with dissolve

  label intro_start:
  if renpy.seen_label("intro_enter_name"):
    $skip_to=("Skip intro","intro_skip",(50,50))

  "Newfall is the perfect town, but if you think you can have any of it, you're an idiot."
  "Not the girls with sun-kissed legs, rollerblading down the streets."
  "Not the million-dollar yachts, bobbing lazily in the bright-blue harbor."
  "Not the cheerful laughter of the families, picnicking in the park."
  "None of it will ever be yours."
  "And who's to blame? Society and shitty genetics?"
  "To have winners, you need losers. And in life's zero-sum game... sorry, kid — snake eyes."
  "..."
  "The days all bleed together in a diarrhea of misery and self-loathing."
  "Every morning, you sleep in like a bum. Get yelled at for being a disappointment."
  "You work a pointless job, earning a few pointless bucks that you spend on pointless pizzas that taste like pepperoni and ballsack."
  "Every night, you eat alone, trying not to cry."
  "And then you cry. What else is there?"
  play sound "train_passing" fadein 3.0
  #$renpy.pause(3.0,hard=True)
  "Train-smashed tomato sauce is the only way out... but not if you're a pussy."
  "..."
  "Growing up, you expect life to be a fairytale."
  "You'll make it through school, get a good job, meet the girl of your dreams."
  "That's what you tell yourself as you stuff your promposal into the garbage because you're too much of a wimp to ask."
  "There's more fish in the sea. More opportunities."
  "That's what goes through your head as you stay home and play video games instead of going to that party..."
  "There's always another chance!"
  "..."
  "Then you're suddenly halfway to thirty, and your Cinderella story has turned to ash."
  "You're forced to bite life's poisoned apple and realize that you have nothing. You're nobody."
  #"You still live with your mother. All your friends have long since moved on, and the only girl who wants to talk to you is called Alexa."
  "You're all alone."
  "All your friends have long since moved on, and the only girl who wants to talk to you is called Alexa."
  "All those chances you thought you had — gone."
  "You're stuck in a pool with no ladder, hoping you drown before..."
  "Before life hooks its marionette strings into your flesh once more, and you pray that one of them will catch your neck in a tight noose."
  "..."
  #$renpy.pause(1.0,hard=True)

label intro_phone_call:
  play sound "phone_vibrate"

  "There my phone goes again, taunting me with a flicker of hope."
  "Well... better check the latest promotional message from my dear mobile carrier..."

  $set_dialog_mode("phone_message","hidden_number")
  hidden_number "I know what you did last weekend."
  $set_dialog_mode()
  "Who the hell is this?"
  "Hmm... hidden number."
  "It's probably some idiot claiming to have a webcam recording of me masturbating."
  "Sorry, man. No money. No friends. No one who cares."
  "All I have is a need for human interaction and this UNO reverse card..."
  $set_dialog_mode("phone_message","hidden_number")
  menu(side="right"):
    "\"And what was that?\"":
      "And what was that?"
    "\"Budget horror much?\"":
      "Budget horror much?"
    "\"I've already repressed it.\"":
      "I've already repressed it."
  hidden_number "The answer is... absolutely nothing."
  "Who is this? Also, that's not true!"
  hidden_number "Completing Castlevania for the 167th time and attempting to make an origami cat out of an old pizza box doesn't count."
  $set_dialog_mode()
  "Shit. That's odd, but I did tweet that. Always thought my three followers were bots."
  $set_dialog_mode("phone_message","hidden_number")
  menu(side="right"):
    "\"How do you know this?\"":
      "How do you know this?"
    "\"Who the hell are you?\"":
      "Who the hell are you?"
    "\"It was a pretty good origami cat, though. Right?\"":
      "It was a pretty good origami cat, though. Right?"
  hidden_number "Doesn't matter. You're lazy and self-destructive."
  hidden_number "That's why you're here now, contemplating your failures."
  hidden_number "You've had no significant impact on anyone's life. [flora] hates you. You have no special talents. You've never even had an interesting science fair project!"
  hidden_number "Some of it is bad luck. Most of it is your own doing."
  hidden_number "I've never seen anyone make so many poor choices in life. It saddens me."
  menu(side="right"):
    "\"[jo], is that you?\"":
      "[jo], is that you?"
      hidden_number "No, I'm not. But I have something to tell you about those choices. Something life-changing!"
    "\"Is this the NSA? I can explain the browser history...\"":
      "Is this the NSA? I can explain the browser history..."
      hidden_number "No. But I have something to tell you about those choices. Something life-changing!"
    "\"And all those choices have led me to you. Are you the woman I've been waiting for all my life?\"":
      "And all those choices have led me to you. Are you the woman I've been waiting for all my life?"
      hidden_number "No, I'm not. But I have something to tell you about those choices. Something life-changing!"
  hidden_number "All you need to do is tell me your name."
  menu(side="right"):
    "\"I asked first!\"":
      "I asked first!"
      hidden_number "My name isn't important. If you want to know what I have to say, tell me yours."
    "\"Are you also after my credit card number? You're going to be disappointed.\"":
      "Are you also after my credit card number? You're going to be disappointed."
      hidden_number "What? No! If you want to know what I have to say, tell me your name."
    "\"Hold up. Are you a Nigerian prince, by any chance?\"":
      "Hold up. Are you a Nigerian prince, by any chance?"
      hidden_number "What? No! If you want to know what I have to say, tell me your name."
  $set_dialog_mode()
  "It's probably a scammer, but what do I have to lose?"

label intro_enter_name:
  $skip_to=None
  $set_dialog_mode()
  call screen ask_name("What is your name?",mc)
  $set_dialog_mode("phone_message","hidden_number")
  "My name is [mc]. There you go! Now tell me!"
  "Hello?"
  "That's really funny..."
  $set_dialog_mode()
  "Most people ghost me after I send them a picture. Ghosted after a name... that's a first."
  "Oh well, what's the point of staying awake, anyway?"

label intro_wake_up_in_past:
  stop music fadeout 1.0
  scene black with dissolve
  $renpy.pause(3.0)
  play music "alarm"
  scene location with dissolve
  $game.ui.hide_hud=0
  "Ugh, why the fuck is the alarm going off? I haven't set one in years."
  "That dream was a good one, too. Obviously, something had to ruin it for me..."
  "This thing is driving me nuts!"
  $quest.act_one.start(silent = True)
  $quest.smash_or_pass.start()
  return

label quick_start_defaults_intro_1:
  $mc.add_phone_contact(hidden_number,silent=True)
  $mc.add_phone_contact(jo,silent=True)
  $mc.add_phone_contact(flora,silent=True)
  ## set default phone message history if player skipped intro
  ## already selected choices are ignored, we assume first choice selected
  $mc.phone_message_history[hidden_number.id]=[]
  $mc.add_message_to_history(hidden_number,hidden_number,"I know what you did last weekend.")
  $mc.add_message_to_history(hidden_number,mc,"And what was that?")
  $mc.add_message_to_history(hidden_number,hidden_number,"The answer is... absolutely nothing.")
  $mc.add_message_to_history(hidden_number,mc,"Who is this? Also, that's not true!")
  $mc.add_message_to_history(hidden_number,hidden_number,"Completing Castlevania for the 167th time and attempting to make an origami cat out of an old pizza box doesn't count.")
  $mc.add_message_to_history(hidden_number,mc,"How do you know this?")
  $mc.add_message_to_history(hidden_number,hidden_number,"Doesn't matter. You're lazy and self-destructive. That's why you're here now, contemplating your failures.")
  $mc.add_message_to_history(hidden_number,hidden_number,"You've had no significant impact on anyone's life. [flora] hates you. You have no special talents. You've never even had an interesting science fair project!")
  $mc.add_message_to_history(hidden_number,hidden_number,"Some of it is bad luck. Most of it is your own doing.")
  $mc.add_message_to_history(hidden_number,hidden_number,"I've never seen anyone make so many poor choices in life. It saddens me.")
  $mc.add_message_to_history(hidden_number,mc,"[jo], is that you?")
  $mc.add_message_to_history(hidden_number,hidden_number,"No, I'm not. But I have something to tell you about those choices. Something life-changing!")
  $mc.add_message_to_history(hidden_number,hidden_number,"All you need to do is tell me your name.")
  $mc.add_message_to_history(hidden_number,mc,"I asked first!")
  $mc.add_message_to_history(hidden_number,hidden_number,"My name isn't important. If you want to know what I have to say, tell me yours.")
  return

label quick_start_defaults_intro_2:
  $mc.add_message_to_history(hidden_number,mc,"My name is [mc]. There you go! Now tell me!")
  $mc.add_message_to_history(hidden_number,mc,"Hello?")
  $mc.add_message_to_history(hidden_number,mc,"That's really funny...")
  return

label intro_skip:
  call quick_start_defaults_intro_1
  jump intro_enter_name
