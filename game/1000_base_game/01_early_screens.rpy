python early:
  ##Code for additional screen objects.
  ##Note: this file breaks naming convention as it has to be loaded before other files.
  ## Code by Tama using SleepKirby's fix to allow action on scroll in a window with scroll bars
  ## Subclass of Adjustment Class in order to override "change"
  class Custom_Adjustment(renpy.display.behavior.Adjustment):
    def __init__(self,*args,**kwargs):
      super(Custom_Adjustment,self).__init__(*args,**kwargs)
    def change(self, value):
      #trying globals for now, but could call functions from here if needed
      if value > self._range and self._value == self._range:
        #If value is over range (scrolling down)
        pass
      elif value < 0 and self._value == 0:
        #If value is less than 0 (scrolling up)
        if not quest.lindsey_motive["out_of_loop"]:
          renpy.jump("quest_lindsey_motive_steal_locker2_out_of_loop")
      # Otherwise, just do what the Adjustment normally does
      return renpy.display.behavior.Adjustment.change(self, value)

  ##Define custom viewport class here
  ##Taken from renpy\display\viewport.py
  class Custom_Viewport_Class(renpy.display.viewport.Viewport):
    __version__ = 5
    arrowkeys = False
    pagekeys = False
    def after_upgrade(self, version):#don't actually think I need to override this method, but doing it anyway just in case
      if version < 1:
        self.xadjustment = Custom_Adjustment(1, 0)#replacing renpy adjustment class with
        self.yadjustment = Custom_Adjustment(1, 0)#the custom one
        self.set_adjustments = False
        self.mousewheel = False
        self.draggable = False
        self.width = 0
        self.height = 0

      if version < 2:
        self.drag_position = None

      if version < 3:
        self.edge_size = False
        self.edge_speed = False
        self.edge_function = None
        self.edge_xspeed = 0
        self.edge_yspeed = 0
        self.edge_last_st = None

      if version < 4:
        self.xadjustment_param = None
        self.yadjustment_param = None
        self.offsets_param = (None, None)
        self.set_adjustments_param = True
        self.xinitial_param = None
        self.yinitial_param = None

      if version < 5:
        self.focusable = self.draggable

    def __init__(self,*args,**kwargs):
      super(Custom_Viewport_Class,self).__init__(*args,**kwargs)

    def _show(self):
      if self.xadjustment_param is None:
        self.xadjustment = Custom_Adjustment(1, 0)#Custom adjustment here
      else:
        self.xadjustment = self.xadjustment_param
      if self.yadjustment_param is None:
        self.yadjustment = Custom_Adjustment(1, 0)#And here too.
      else:
        self.yadjustment = self.yadjustment_param

      if self.xadjustment.adjustable is None:
        self.xadjustment.adjustable = True

      if self.yadjustment.adjustable is None:
        self.yadjustment.adjustable = True

      self.set_adjustments = self.set_adjustments_param

      offsets = self.offsets_param
      self.xoffset = offsets[0] if (offsets[0] is not None) else self.xinitial_param
      self.yoffset = offsets[1] if (offsets[1] is not None) else self.yinitial_param
  ## Use renpy.register_sl_displayable()
  ## To create my custom Screen Language version of viewport for use in screens in a python early block
  renpy.register_sl_displayable("viewport_custom", Custom_Viewport_Class,replaces=True,nchildren=1,style="viewport").add_property("ui_properties").add_property("position_properties").add_property("side_position_properties").add_property("pagekeys").add_property("child_size").add_property("mousewheel").add_property("arrowkeys").add_property("draggable").add_property("edgescroll").add_property("xadjustment").add_property("yadjustment").add_property("xinitial").add_property("yinitial").add_property("scrollbars").add_property("replaces")
#######################################
##Fun side notes:
##When adding new custom Screen Language statements, you can look in renpy\screenlang.py to see what properties/keywords similar statements use
##Along with what renpy function that word calls
##You can then look in renpy\ui.py for that function for example and see if it calls a class which you could override.

##leftover function that wasn't actually needed:
  # def viewport_custom(**properties):## taken from renpy ui.py
    # vpfunc = renpy.ui.Wrapper(Custom_Viewport_Class, one=True, replaces=True, style='viewport')
    # _spacing_to_side = True
    # return vpfunc(**properties)