#minigame
init -100 python:
##############################################################
######### Custom Displayable ########################################
##############################################################
  #This displayable shows the weapon following the mouse and could be used for
  #other features such as a crosshair
  class vineWeaponDisplay(renpy.Displayable):
    def __init__(self, *args, **kwargs):
      renpy.Displayable.__init__(self, *args, **kwargs)
      if mc.owned_item("machete"):#assuming quest doesn't get to this phase unless mc has atleast 1 weapon
        self.weapon_image = "minigames vine_attack machete_main"
        self.weapon_align = (0.24, 0.35)
      else:#else use axe
        self.weapon_image = "minigames vine_attack axe_main"
        self.weapon_align = (0.2, 0.45)
      self.mouse_pos = (0, 0)
      self.child = None
      self.child_size = (1, 1)
      self.kwargs = kwargs
      self.args = args

    def render(self, w, h, st, at):
      fixed = Fixed(
            Bar(#cooldown timer bar
              value = vine_attack_minigame.cooldown_ticker,
              range = vine_attack_minigame.cooldown_time,
              xmaximum=200,
              ysize=25,
              offset=(-60,-130),
              left_bar=Frame("ui game_menu bar_full",32,0,),#just using the bar images we have, but could be anything
              right_bar=Frame("ui game_menu bar_empty",32,0),
              pos=(vine_attack_minigame.mouse_pos[0] / self.child_size[0], vine_attack_minigame.mouse_pos[1] / self.child_size[1]),
              keyboard_focus=False
              ),
            Transform(#weapon image
              self.weapon_image,
              align=self.weapon_align,
              pos=(vine_attack_minigame.mouse_pos[0] / self.child_size[0], vine_attack_minigame.mouse_pos[1] / self.child_size[1])
              ),
            )
      self.child = fixed
      child_render = renpy.render(self.child, w, h, st, at)
      self.child_size = child_render.get_size()
      renpy.redraw(self, 0)
      return child_render

    def event(self, e, x, y, st):
      if renpy.get_screen("vine_attack_turn"):#make sure this is only checked on the correct screen
        vine_attack_minigame.mouse_pos = (x, y)#update mouse pos
        if not vine_attack_minigame.cooldown_active:
          if mc.owned_item("machete"):#reset image if not swinging
            self.weapon_image = "minigames vine_attack machete_main"
          else:#else use axe
            self.weapon_image = "minigames vine_attack axe_main"
          if e.type==pygame.MOUSEBUTTONUP and e.button==1:#mouseclick occurred
              if mc.owned_item("machete"):#trigger swinging animation
                self.weapon_image = "vine_attack_machete_swipe"
              else:
                self.weapon_image = "vine_attack_axe_swipe"
              vine_attack_minigame.cooldown_active = True
              vine_attack_minigame.cooldown_started = st
              vine_attack_minigame.cooldown_ticker = 0.0
        else:
          vine_attack_minigame.update_cooldown(st)
          # renpy.redraw(self,0)
        renpy.timeout(0)
      return
    # def visit(self):
      # return [self.child]

##############################################################
######### Vines templates###########################################
##############################################################
  vines_by_id = {}
  class BaseVineMeta(type):
    def __init__(cls,name,bases,dct):
      super(BaseVineMeta,cls).__init__(name,bases,dct)
      if cls.id is not None:
        vines_by_id[cls.id]=cls
  #represents a vine on the screen, it's end/start position, image and pos
  class BaseVine(BaseClass):
    __metaclass__=BaseVineMeta
    id = None
    def __init__(self,*args,**kwargs):
      super(BaseVine,self).__init__(*args,**kwargs)
    def update(self,tf,st,at):#this method is run during transforms to update the pos of a vine
      self.pos = (tf.xoffset,tf.yoffset)
  class VineOne(BaseVine):
    id = "vine_one"
    vine_image = "minigames vine_attack vine1_reaching"
    grab_image = "minigames vine_attack vine1_grabbing"
    start_pos = [521,-800]
    end_pos = [521,0]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"  #States:  waiting reaching grabbing beaten
      self.previous_state = "waiting" #used to check if the state has changed
      self.pos = self.start_pos #tracks pos so transforms can be done part-way between turns

  class VineTwo(BaseVine):
    id = "vine_two"
    vine_image = "minigames vine_attack vine2_reaching"
    grab_image = "minigames vine_attack vine2_grabbing"
    start_pos = [954,-900]
    end_pos = [954,0]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos

  class VineThree(BaseVine):
    id = "vine_three"
    vine_image = "minigames vine_attack vine3_reaching"
    grab_image = "minigames vine_attack vine3_grabbing"
    start_pos = [1323,-900]
    end_pos = [1323,0]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos

  class VineFour(BaseVine):
    id = "vine_four"
    vine_image = "minigames vine_attack vine4_reaching"
    grab_image = "minigames vine_attack vine4_grabbing"
    start_pos = [950,1080]
    end_pos = [925,310]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos

#mouse object, create displayable class based on xray example and add to screen as a default displayable!
#############################################################
########### State class ############################################
#############################################################
  class VineAttackState(BaseClass):
    default_vines = ["vine_one","vine_two","vine_three","vine_four"]
    def __init__(self,attack_vines,panties=True,bra=True,speed=3.0,max_rounds=20,max_score=10,squid=False,attack_freq=2,grabbed_limit=3,**kwargs):
      self.hits = [] #list of (x,y) where hits have taken place
      self.vines = [] #this container holds the instantiated vines
      self.waiters = [] #holds list position of vines that are ready to attack
      self.mouse_pos = (0, 0) #tracks mouse pos for weapon and hit-marker
      self.round = 0 #current round
      self.score = 0 #number of successful hits
      self.grabbed = 0 #number of vines latched on
      self.lose = False
      self.win = False
      self.flora_surprise = False #switch for facial expressions
      self.cooldown_active = False
      self.cooldown_started = 0.0
      self.cooldown_ticker = 0.0
      self.cooldown_time = 0.5

      self.bra = bra #does she have bra
      self.panties = panties #does she have panties
      self.speed = speed #seconds per round
      self.max_rounds = max_rounds #win condition 1
      self.max_score = max_score #win condition 2
      self.squid = squid #Is Flora's friend there to help?
      self.attack_freq = attack_freq #number of vines attacking at once (can get a lil buggy [I fixed the bug])
      self.grabbed_limit = grabbed_limit #lose condition. number of vines allowed to latch on before losing
      self.attack_vines = default_vines if attack_vines is None else attack_vines
      for vine in self.attack_vines:
        self.vines.append(vines_by_id[vine]())
    def round_advance(self):
      self.grabbed = 0
      self.round += 1 #advance round
      self.hits = []
      self.waiters = []
      vine_attack_minigame.flora_surprise = False
      for vine in self.vines:#advance state of each vine
        if vine.state == "reaching":
          vine.previous_state = "reaching"
          vine.state = "grabbing"
          vine_attack_minigame.flora_surprise = True
        elif vine.state == "grabbing":
          vine_attack_minigame.flora_surprise = True
          vine.previous_state = "grabbing"
          self.grabbed +=1
          if self.grabbed >= self.grabbed_limit:
            self.lose = True
        elif vine.state == "beaten":
          vine.previous_state = "beaten"
          vine.state = "waiting"
        elif vine.state == "waiting":#if a vine is waiting to attack, add it to the attacker list
          vine.previous_state = "waiting"
          self.waiters.append(self.vines.index(vine))
      if self.waiters:
        attackers = random.sample(self.waiters,self.attack_freq if self.attack_freq<=len(self.waiters) else 1)
        for attacker in attackers:#select x random attackers to attack
          if self.vines[attacker].state == "waiting":
            self.vines[attacker].state = "reaching"
        if self.score>self.max_score:
          self.win = True
    def hit(self,vine):
      if not self.cooldown_ticker>0.05:
        if vine.state == "grabbing":
          vine.state = "reaching"#hitting vine unattaches it
        else:
          vine.state = "beaten"
          self.score += 1 ## jotapê was here
        self.hits.append((self.mouse_pos[0],self.mouse_pos[1]))
        #print("Vine "+vine.id+" has been hit.")
#       self.score += 1 ## jotapê was here
    def update_cooldown(self,st):#updates the attack cooldown timer [calculated using the screen's shown_timebase]
      if self.cooldown_active:
        if self.cooldown_ticker < self.cooldown_time:
          if st < self.cooldown_started:
            self.cooldown_started = -(self.speed-self.cooldown_started)
            self.cooldown_ticker = st-self.cooldown_started#self.cooldown_ticker = self.cooldown_ticker+(st-self.cooldown_started)
          else:
            self.cooldown_ticker = st-self.cooldown_started
        else:
          #print("cooldown over")
          self.cooldown_active = False
          self.cooldown_ticker = 0.0
          self.cooldown_started = 0.0
#############################################################
##Styles#######################################################
#############################################################
style vine_instructions_frame is frame:
  background Frame("ui frame_popup",220,160,240,150)
  padding (80,60)
  align (0.5,0.5)
# xmaximum 800 ## jotapê was here
  xmaximum 820 ## jotapê was here

style vine_instructions_text is ui_text:
  xalign 0.5
  text_align 0.5

style vine_instructions_button is textbutton:
  xminimum 250
  size_group "confirm_buttons"
##############################################################
#####labels #####################################################
##############################################################
#This is the main label where vine attack game is always called from
#Acceps win and lose label names so it can redirect to a specific label based on outcome
#and accepts difficulty
#label start_vine_attack_minigame(winlabel='vine_attack_win',loselabel='vine_attack_lose',difficulty='medium',skip_instructions=True): ## jotapê was here
label start_vine_attack_minigame(winlabel='vine_attack_win',loselabel='vine_attack_lose',difficulty='medium',headstart=False,skip_instructions=True): ## jotapê was here
  if not skip_instructions:
#   call screen vine_instructions("Slash the vines to help [flora]!") ## jotapê was here
    call screen vine_instructions("The vines have latched onto [flora]!\nUse your "+mc.owned_item(("machete","fire_axe")).replace("_"," ")+" to slash them away.") ## jotapê was here
  if difficulty == 'easy':#Difficulties here are just an example, see the state class for full list of attributes
    python:
#     vines = ["vine_one","vine_two"] ## jotapê was here
      vines = ["vine_one","vine_two","vine_three","vine_four"] ## jotapê was here
#     vine_attack_minigame=VineAttackState(attack_vines=vines,speed=4.0,attack_freq=1) ## jotapê was here
      vine_attack_minigame=VineAttackState(attack_vines=vines,attack_freq=0,max_score=4) ## jotapê was here
  elif difficulty == 'medium':
    $vines = ["vine_one","vine_two","vine_three","vine_four"]
#   $vine_attack_minigame=VineAttackState(attack_vines=vines) ## jotapê was here
    $vine_attack_minigame=VineAttackState(attack_vines=vines,max_score=8) ## jotapê was here
  elif difficulty == 'hard':#(impossible)
    python:
      vines = ["vine_one","vine_two","vine_three","vine_four"]
#     vine_attack_minigame=VineAttackState(attack_vines=vines,speed=1.5,attack_freq=4,max_score=100,max_round=100) ## jotapê was here
      vine_attack_minigame=VineAttackState(attack_vines=vines,speed=1.5,attack_freq=4,max_score=16) ## jotapê was here
  elif difficulty == 'medium_squid':
    $vines = ["vine_one","vine_two","vine_three"]
#   $vine_attack_minigame=VineAttackState(attack_vines=vines,bra=False,squid=True) ## jotapê was here
    $vine_attack_minigame=VineAttackState(attack_vines=vines,speed=1.5,attack_freq=3,bra=False,squid=True,max_score=8-(8/4)) ## jotapê was here
# elif difficulty == 'easy_squid':#can't lose ## jotapê was here
  elif difficulty == 'hard_squid': ## jotapê was here
    python:
#     vines = ["vine_one","vine_three"] ## jotapê was here
      vines = ["vine_one","vine_two","vine_three"] ## jotapê was here
#     vine_attack_minigame=VineAttackState(attack_vines=vines,speed=4.0,attack_freq=1,bra=False,squid=True) ## jotapê was here
      vine_attack_minigame=VineAttackState(attack_vines=vines,speed=1.5,attack_freq=3,bra=False,squid=True,max_score=16-(16/4)) ## jotapê was here
  $mouse_visible = False
  $_rollback = False
# $game.ui.hide_hud+=1 ## jotapê was here
  $game.ui.hide_hud=1 ## jotapê was here
  hide screen interface_hider ## jotapê was here
  show screen kitchen_minigame_skip ## jotapê was here
  if headstart: ## jotapê was here
    python: ## jotapê was here
      for vine in vine_attack_minigame.vines: ## jotapê was here
        if vine.id in headstart: ## jotapê was here
          vine.state = "grabbing" ## jotapê was here
          vine.previous_state = "grabbing" ## jotapê was here
  call vine_attack_loop from _call_vine_attack_loop
  $mouse_visible = True
  $_rollback = True
# $game.ui.hide_hud-=1 ## jotapê was here
  $game.ui.hide_hud=0 ## jotapê was here
  show screen interface_hider ## jotapê was here
  hide screen kitchen_minigame_skip ## jotapê was here

# if _return == 'lose':#these labels can be changed for each call depending on the winlabel/loselabel arguments ## jotapê was here
#   call expression loselabel from _call_expression_vine_attack_minigame_lose ## jotapê was here
# else: ## jotapê was here
#   call expression winlabel from _call_expression_vine_attack_minigame_win ## jotapê was here
  return

label vine_attack_loop:#main gameplay loop
# while (vine_attack_minigame.round<vine_attack_minigame.max_rounds): ## jotapê was here
  while (vine_attack_minigame.score<vine_attack_minigame.max_score): ## jotapê was here
    #advance round
    $vine_attack_minigame.round_advance()
#   if vine_attack_minigame.lose: ## jotapê was here
#     return 'lose' ## jotapê was here
#   elif vine_attack_minigame.win: ## jotapê was here
#     return "win" ## jotapê was here
    call screen vine_attack_turn
  return "win"

label vine_attack_win:
  "you win"
  return

label vine_attack_lose:
  "you lose"
  return

###############################################
###############Transforms#########################
###############################################

transform transform_vine_waiting(vine):
  xoffset vine.start_pos[0]
  yoffset vine.start_pos[0]

transform transform_vine_reaching(vine):
  xoffset vine.pos[0]
  yoffset vine.pos[1]
  parallel:
    linear vine_attack_minigame.speed/1.15 xoffset vine.end_pos[0]
  parallel:
    linear vine_attack_minigame.speed/1.15 yoffset vine.end_pos[1]
  parallel:
    block:#updates the pos of vine for smooth transitions between
      function vine.update#transforms
      0.01
      repeat

transform transform_vine_reaching_disappear(vine):
  xoffset vine.pos[0]
  yoffset vine.pos[1]
  linear vine_attack_minigame.speed/10 alpha 0.0
transform transform_vine_beaten(vine):
  xoffset vine.pos[0]
  yoffset vine.pos[1]
  parallel:
    linear vine_attack_minigame.speed/10 xoffset vine.start_pos[0]
  parallel:
    linear vine_attack_minigame.speed/10 yoffset vine.start_pos[1]
  parallel:
    block:
      function vine.update
      0.01
      repeat

transform transform_vine_grabbing(vine):
  xoffset vine.end_pos[0]
  yoffset vine.end_pos[1]
  alpha 0.0
  parallel:
    linear vine_attack_minigame.speed/10 alpha 1.0
  parallel:
    block:
      function vine.update
      0.01
      repeat
transform transform_vine_grabbed(vine):
  xoffset vine.end_pos[0]
  yoffset vine.end_pos[1]
  alpha 1.0
  block:
    function vine.update
    0.01
    repeat
###############################################
###############Screens###########################
###############################################
image vine_attack_machete_swipe:#animations for weapon swings
  "minigames vine_attack machete_smear"
  0.05
  "minigames vine_attack machete_hit"
  0.3
  "minigames vine_attack machete_smear"
  0.05
  "minigames vine_attack machete_main"
  0.2
  repeat
image vine_attack_axe_swipe:#they glitch occasionally, but without repeat they just play once and then don't work :s
  "minigames vine_attack axe_smear"
  0.05
  "minigames vine_attack axe_hit"
  0.3
  "minigames vine_attack axe_smear"
  0.05
  "minigames vine_attack axe_main"
  0.2
  repeat
screen vine_attack_turn():
  default cursor = vineWeaponDisplay()#cursor object
  #background and flora
  add 'minigames vine_attack background'
  add 'minigames vine_attack flora' pos (255,312)
  if vine_attack_minigame.flora_surprise:#show surprise when grabbed
    add 'minigames vine_attack face3' pos (518,418)
  else:
    add 'minigames vine_attack face2' pos (519,419)
  if vine_attack_minigame.panties:#hide panties
    add 'minigames vine_attack panties' pos (1097,331)
  if vine_attack_minigame.bra:#or bra
    add 'minigames vine_attack bra' pos (651,425)
  if vine_attack_minigame.squid:#wet if squid
    add 'minigames vine_attack wet' pos (308,345)
  add 'minigames vine_attack grass' pos (639,507)#touch
# text "Score: " + str(vine_attack_minigame.score) + "/" + str(vine_attack_minigame.max_score) xalign 0.5 yalign 0.05 ## jotapê was here

  for vine in vine_attack_minigame.vines:
    if vine.state == "grabbing":#separate loop to make sure this appears under other vines
      if vine.state == vine.previous_state:#was it already grabbing, then don't redo animation
        imagebutton:
          focus_mask vine.grab_image#Displayable
          idle vine.grab_image
          keyboard_focus False
          #activate_sound = ""
          activate_sound ("machete_hit" if mc.owned_item("machete") else "fire_axe_hit") ## jotapê was here
          action Function(vine_attack_minigame.hit,vine)
          at transform_vine_grabbed(vine)
      else:#else fade from reaching to grabbing image
        add vine.vine_image:
          at transform_vine_reaching_disappear(vine)
        imagebutton:
          focus_mask vine.grab_image#Displayable
          idle vine.grab_image
          keyboard_focus False
          #activate_sound = "" Could be used to add hitting sound fx tbd
          activate_sound ("machete_hit" if mc.owned_item("machete") else "fire_axe_hit") ## jotapê was here
          action Function(vine_attack_minigame.hit,vine)
          at transform_vine_grabbing(vine)
  for vine in vine_attack_minigame.vines:
    if vine.state == "waiting":
      pass #had an image here but it was buggy so just deleted it
    elif vine.state == "reaching":
        imagebutton:
          focus_mask vine.vine_image#Displayable mask so you have to click on the vine
          idle vine.vine_image
          keyboard_focus False#no cheating
          #activate_sound = ""
          activate_sound ("machete_hit" if mc.owned_item("machete") else "fire_axe_hit") ## jotapê was here
          action Function(vine_attack_minigame.hit,vine) #successful hit
          at transform_vine_reaching(vine)
    elif vine.state == "beaten":
      add vine.vine_image:
        at transform_vine_beaten(vine)
  if vine_attack_minigame.squid:#squid? c:
    if difficulty == "hard_squid": ## jotapê was here
      add "minigames vine_attack vine4_chopped": ## jotapê was here
        pos (923,483) ## jotapê was here
      add "minigames vine_attack squid2": ## jotapê was here
        pos (894,470) ## jotapê was here
    else: ## jotapê was here
      add "minigames vine_attack vine4_reaching":
        pos (925,310)
      add "minigames vine_attack squid":#reminder: hide vine4 if showing squid
        pos (1003,429)
  add 'minigames vine_attack foreground' #this is where vines go to hide
  for hit in vine_attack_minigame.hits:#show all hitmarkers triggered
    add "minigames vine_attack hitmarker":#resets each turn
      pos hit
      anchor (0.5,0.5)
  add cursor #custom displayable that shows weapon following cursor
  timer vine_attack_minigame.speed action Return('')
screen vine_instructions(message):
  modal True
  zorder 200
  style_prefix "vine_instructions"
  add "#0008"
  frame style "notification_modal_title_frame" yalign 0.25: ## jotapê was here
    text "Warning" style "notification_modal_title" ## jotapê was here
  frame:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Start" action Return('')
  key "game_menu" action NullAction()
