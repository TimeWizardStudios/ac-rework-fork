
## Functions

init python:
  floraTime = renpy.random.random() * 4 + 2
  isabelleTime = renpy.random.random() * 4 + 2
  kateTime = renpy.random.random() * 4 + 2
  lindseyTime = renpy.random.random() * 4 + 2
  maxineTime = renpy.random.random() * 4 + 2
  floraPos = 0
  isabellePos = 0
  katePos = 0
  lindseyPos = 0
  maxinePos = 0
  floraOffset = 0
  isabelleOffset = 0
  kateOffset = 0
  lindseyOffset = 0
  maxineOffset = 0
  floraZoom = 1
  isabelleZoom = 1
  kateZoom = 1
  lindseyZoom = 1
  maxineZoom = 1


  def randomPauseFlora(trans,st,at):
    if st > floraTime:
      global floraTime
      floraTime = renpy.random.random() * 4 + 2
      return None
    return 0

  def randomMoveFlora(trans,st,at):
    global floraPos
    global floraOffset
    global floraZoom
    if st > .16:
      if floraPos > 0:
        floraPos = renpy.random.randint(-1,0)
      elif floraPos < 0:
        floraPos = renpy.random.randint(0,1)
      else:
        floraPos = renpy.random.randint(-1,1)
      if trans.xoffset * floraPos > 5:
        floraPos *= -1
      return None
    if floraPos > 0:
      trans.xzoom = -1
    elif floraPos < 0:
      trans.xzoom = 1
    trans.xoffset += .16 * 10 * floraPos
    floraOffset = trans.xoffset
    floraZoom = trans.xzoom
    return 0


  def randomPauseIsabelle(trans,st,at):
    if st > isabelleTime:
      global isabelleTime
      isabelleTime = renpy.random.random() * 4 + 2
      return None
    return 0

  def randomMoveIsabelle(trans,st,at):
    global isabellePos
    global isabelleOffset
    global isabelleZoom
    if st > .16:
      if isabellePos > 0:
        isabellePos = renpy.random.randint(-1,0)
      elif isabellePos < 0:
        isabellePos = renpy.random.randint(0,1)
      else:
        isabellePos = renpy.random.randint(-1,1)
      if trans.xoffset * isabellePos > 5:
        isabellePos *= -1
      return None
    if isabellePos > 0:
      trans.xzoom = -1
    elif isabellePos < 0:
      trans.xzoom = 1
    trans.xoffset += .16 * 10 * isabellePos
    isabelleOffset = trans.xoffset
    isabelleZoom = trans.xzoom
    return 0


  def randomPauseKate(trans,st,at):
    if st > kateTime:
      global kateTime
      kateTime = renpy.random.random() * 4 + 2
      return None
    return 0

  def randomMoveKate(trans,st,at):
    global katePos
    global kateOffset
    global kateZoom
    if st > .16:
      if katePos > 0:
        katePos = renpy.random.randint(-1,0)
      elif katePos < 0:
        katePos = renpy.random.randint(0,1)
      else:
        katePos = renpy.random.randint(-1,1)
      if trans.xoffset * katePos > 5:
        katePos *= -1
      return None
    if katePos > 0:
      trans.xzoom = -1
    elif katePos < 0:
      trans.xzoom = 1
    trans.xoffset += .16 * 10 * katePos
    kateOffset = trans.xoffset
    kateZoom = trans.xzoom
    return 0


  def randomPauseLindsey(trans,st,at):
    if st > lindseyTime:
      global lindseyTime
      lindseyTime = renpy.random.random() * 4 + 2
      return None
    return 0

  def randomMoveLindsey(trans,st,at):
    global lindseyPos
    global lindseyOffset
    global lindseyZoom
    if st > .16:
      if lindseyPos > 0:
        lindseyPos = renpy.random.randint(-1,0)
      elif lindseyPos < 0:
        lindseyPos = renpy.random.randint(0,1)
      else:
        lindseyPos = renpy.random.randint(-1,1)
      if trans.xoffset * lindseyPos > 5:
        lindseyPos *= -1
      return None
    if lindseyPos > 0:
      trans.xzoom = -1
    elif lindseyPos < 0:
      trans.xzoom = 1
    trans.xoffset += .16 * 10 * lindseyPos
    lindseyOffset = trans.xoffset
    lindseyZoom = trans.xzoom
    return 0


  def randomPauseMaxine(trans,st,at):
    if st > maxineTime:
      global maxineTime
      maxineTime = renpy.random.random() * 4 + 2
      return None
    return 0

  def randomMoveMaxine(trans,st,at):
    global maxinePos
    global maxineOffset
    global maxineZoom
    if st > .16:
      if maxinePos > 0:
        maxinePos = renpy.random.randint(-1,0)
      elif maxinePos < 0:
        maxinePos = renpy.random.randint(0,1)
      else:
        maxinePos = renpy.random.randint(-1,1)
      if trans.xoffset * maxinePos > 5:
        maxinePos *= -1
      return None
    if maxinePos > 0:
      trans.xzoom = -1
    elif maxinePos < 0:
      trans.xzoom = 1
    trans.xoffset += .16 * 10 * maxinePos
    maxineOffset = trans.xoffset
    maxineZoom = trans.xzoom
    return 0




## Transforms

transform sticker_leftmost:
  xcenter 100 yalign 0.905 subpixel True

transform sticker_left:
  xcenter 200 yalign 0.905 subpixel True

transform sticker_mid:
  xcenter 300 yalign 0.905 subpixel True

transform sticker_right:
  xcenter 400 yalign 0.905 subpixel True

transform sticker_rightmost:
  xcenter 500 yalign 0.905 subpixel True

transform sticker_move_n:
  easein_quad .08 yoffset -15
  easeout_quad .08 yoffset 0

transform sticker_hop:
  easein_quad .18 yoffset -80
  easeout_quad .18 yoffset 0
  easein_quad .18 yoffset -80
  easeout_quad .18 yoffset 0




## Images

image flora_sticker:
  "minigames poem flora_sticker"
  xoffset floraOffset xzoom floraZoom
  block:
    function randomPauseFlora
    parallel:
      sticker_move_n
    parallel:
      function randomMoveFlora
    repeat

image flora_sticker hop:
  "minigames poem flora_sticker"
  xoffset floraOffset xzoom floraZoom
  sticker_hop
  xoffset 0 xzoom 1
  "flora_sticker"


image isabelle_sticker:
  "minigames poem isabelle_sticker"
  xoffset isabelleOffset xzoom isabelleZoom
  block:
    function randomPauseIsabelle
    parallel:
      sticker_move_n
    parallel:
      function randomMoveIsabelle
    repeat

image isabelle_sticker hop:
  "minigames poem isabelle_sticker"
  xoffset isabelleOffset xzoom isabelleZoom
  sticker_hop
  xoffset 0 xzoom 1
  "isabelle_sticker"


image isabelle_autumn_sticker_neutral:
  "minigames poem isabelle_autumn_sticker_neutral"
  xoffset isabelleOffset xzoom isabelleZoom
  block:
    function randomPauseIsabelle
    parallel:
      sticker_move_n
    parallel:
      function randomMoveIsabelle
    repeat

image isabelle_autumn_sticker_happy:
  "minigames poem isabelle_autumn_sticker_happy"
  xoffset isabelleOffset xzoom isabelleZoom
  sticker_hop
  xoffset 0 xzoom 1
  "isabelle_autumn_sticker_neutral"

image isabelle_autumn_sticker_angry:
  "minigames poem isabelle_autumn_sticker_angry"
  xoffset isabelleOffset xzoom isabelleZoom
  sticker_hop
  xoffset 0 xzoom 1
  "isabelle_autumn_sticker_neutral"


image kate_sticker:
  "minigames poem kate_sticker"
  xoffset kateOffset xzoom kateZoom
  block:
    function randomPauseKate
    parallel:
      sticker_move_n
    parallel:
      function randomMoveKate
    repeat

image kate_sticker hop:
  "minigames poem kate_sticker"
  xoffset kateOffset xzoom kateZoom
  sticker_hop
  xoffset 0 xzoom 1
  "kate_sticker"


image lindsey_sticker:
  "minigames poem lindsey_sticker"
  xoffset lindseyOffset xzoom lindseyZoom
  block:
    function randomPauseLindsey
    parallel:
      sticker_move_n
    parallel:
      function randomMoveLindsey
    repeat

image lindsey_sticker hop:
  "minigames poem lindsey_sticker"
  xoffset lindseyOffset xzoom lindseyZoom
  sticker_hop
  xoffset 0 xzoom 1
  "lindsey_sticker"


image maxine_sticker:
  "minigames poem maxine_sticker"
  xoffset maxineOffset xzoom maxineZoom
  block:
    function randomPauseMaxine
    parallel:
      sticker_move_n
    parallel:
      function randomMoveMaxine
    repeat

image maxine_sticker hop:
  "minigames poem maxine_sticker"
  xoffset maxineOffset xzoom maxineZoom
  sticker_hop
  xoffset 0 xzoom 1
  "maxine_sticker"




## Screens

screen poem_minigame(hop=None):
  add "minigames poem background"
  textbutton "Skip minigame" style_prefix "kitchen_minigame_skip" xpos 30 ypos 30 action [SetVariable("hop",None), Jump("kitchen_minigame_done")]

  add ("flora_sticker hop" if hop == flora else "flora_sticker") at sticker_leftmost
  add ("kate_sticker hop" if hop == kate else "kate_sticker") at sticker_mid
  add ("maxine_sticker hop" if hop == maxine else "maxine_sticker") at sticker_rightmost
  add ("isabelle_sticker hop" if hop == isabelle else "isabelle_sticker") at sticker_left
  add ("lindsey_sticker hop" if hop == lindsey else "lindsey_sticker") at sticker_right

  python:
    wordlist = [
      ["flora:octopus", "isabelle:camping", "kate:tiger", "isabelle:owl", "lindsey:gazelle ", "maxine:toad", "kate:sunbathing", "maxine:investigating", "flora:picnicking", "lindsey:swimming"],
      ["lindsey:pride", "kate:vanity", "maxine:sorrow", "flora:envy", "kate:diligence", "flora:charity", "isabelle:wrath", "isabelle:kindness", "lindsey:chastity", "maxine:temperance"],
      ["maxine:enlightened", "flora:nature", "isabelle:mature", "kate:assertive", "lindsey:cheerful", "maxine:time", "lindsey:sunset", "flora:playful", "kate:ice", "isabelle:fire"],
      ["isabelle:trust", "lindsey:protect", "lindsey:support", "maxine:believe", "flora:forbidden", "isabelle:sympathy", "maxine:entrust", "kate:submit", "flora:penitence", "kate:resilience"],
      ["kate:social media", "maxine:puzzles", "flora:cooking", "lindsey:crop top", "isabelle:sundress", "maxine:shorts", "kate:high heels", "lindsey:music", "isabelle:literature", "flora:bikini"],
    ]

  text "[progress]/5" color "#000" font "fonts/ComicHelvetic_Light.otf" size 40 xalign 1.0 xpos 1434 ypos 203
  vbox spacing 97 xpos 780 ypos 337:
    for word in wordlist[progress-1][:5]:
      python:
        girl,word = word.split(":")
        word = Text(word, color="#000", font="fonts/ComicHelvetic_Light.otf", size=40)
        word_xsize,word_ysize = tuple(int(x) for x in word.size())
      button:
        xsize word_xsize ysize word_ysize
        text word
        frame:
          xalign 0.5 xsize 20 + word_xsize + 20 yalign 0.5 ysize 20 + word_ysize + 20
          background None hover_background Frame(("minigames poem highlight" + str(random.randint(1,4))))
        activate_sound renpy.random.choice(["<from 0.2 to 0.6>scribble","<from 0.4 to 0.8>scribble","<from 0.8 to 1.15>scribble","<from 1.0>scribble"])
        action [SetDict(winning_words,girl,winning_words[girl]+1), SetVariable("hop",girl), Return()]

  vbox spacing 97 xpos 1140 ypos 337:
    for word in wordlist[progress-1][5:]:
      python:
        girl,word = word.split(":")
        word = Text(word, color="#000", font="fonts/ComicHelvetic_Light.otf", size=40)
        word_xsize,word_ysize = tuple(int(x) for x in word.size())
      button:
        xsize word_xsize ysize word_ysize
        text word
        frame:
          xalign 0.5 xsize 20 + word_xsize + 20 yalign 0.5 ysize 20 + word_ysize + 20
          background None hover_background Frame(("minigames poem highlight" + str(random.randint(1,4))))
        activate_sound renpy.random.choice(["<from 0.2 to 0.6>scribble","<from 0.4 to 0.8>scribble","<from 0.8 to 1.15>scribble","<from 1.0>scribble"])
        action [SetDict(winning_words,girl,winning_words[girl]+1), SetVariable("hop",girl), Return()]


screen poem_instructions(message):
  modal True
  zorder 200
  style_prefix "stepping_instructions"
  add "#0008"
  frame style "notification_modal_title_frame" yalign 0.19:
    text "Warning" style "notification_modal_title"
  frame:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Start" action Return()
  key "game_menu" action NullAction()


screen letter_minigame(happy=False, angry=False):
  add "minigames poem background"
  textbutton "Skip minigame" style_prefix "kitchen_minigame_skip" xpos 30 ypos 30 action [SetVariable("happy",False), SetDict(isabelle_words,"happy",5), SetVariable("angry",False), SetDict(isabelle_words,"angry",0), Jump("kitchen_minigame_done")]

  if happy:
    add "isabelle_autumn_sticker_happy" at sticker_right
  elif angry:
    add "isabelle_autumn_sticker_angry" at sticker_right
  else:
    add "isabelle_autumn_sticker_neutral" at sticker_right

  python:
    wordlist = [
      ["happy:hurricane", "happy:earthquake", "angry:frost", "angry:drought", "happy:wildfire", "angry:flood", "happy:avalanche", "angry:heatwave", "angry:whirlpool", "happy:thunderstorm"],
      ["happy:ranger", "angry:monk", "happy:fighter", "angry:sorceress", "angry:rogue", "happy:druid", "happy:bard", "angry:cleric", "happy:barbarian", "angry:warlock"],
      ["angry:spa day", "happy:{k=-3}amusement park{/}", "angry:cinema", "happy:dinner", "happy:art gallery", "happy:poetry reading", "angry:bar crawl", "angry:beach day", "happy:starlight stroll", "angry:nightclub"],
      ["angry:friends", "angry:success", "happy:freedom", "happy:love", "happy:family", "happy:justice", "angry:popularity", "angry:possessions", "happy:kindness", "angry:leisure"],
      ["angry:kate", "happy:lindsey", "angry:maya", "happy:nurse", "angry:mrs. l", "happy:jo", "happy:flora", "angry:jacklyn", "angry:maxine", "happy:ria"],
    ]

  text "[progress]/5" color "#000" font "fonts/ComicHelvetic_Light.otf" size 40 xalign 1.0 xpos 1434 ypos 203
  vbox spacing 97 xpos 780 ypos 337:
    for word in wordlist[progress-1][:5]:
      python:
        outcome,word = word.split(":")
        word = Text(word, color="#000", font="fonts/ComicHelvetic_Light.otf", size=40)
        word_xsize,word_ysize = tuple(int(x) for x in word.size())
      button:
        xsize word_xsize ysize word_ysize
        text word
        frame:
          xalign 0.5 xsize 20 + word_xsize + 20 yalign 0.5 ysize 20 + word_ysize + 20
          background None hover_background Frame(("minigames poem highlight" + str(random.randint(1,4))))
        activate_sound renpy.random.choice(["<from 0.2 to 0.6>scribble","<from 0.4 to 0.8>scribble","<from 0.8 to 1.15>scribble","<from 1.0>scribble"])
        action [SetDict(isabelle_words,outcome,isabelle_words[outcome]+1), SetVariable("happy",False), SetVariable("angry",False), SetVariable(outcome,True), Return()]

  vbox spacing 97 xpos 1140 ypos 337:
    for word in wordlist[progress-1][5:]:
      python:
        outcome,word = word.split(":")
        word = Text(word, color="#000", font="fonts/ComicHelvetic_Light.otf", size=40)
        word_xsize,word_ysize = tuple(int(x) for x in word.size())
      button:
        xsize word_xsize ysize word_ysize
        text word
        frame:
          xalign 0.5 xsize 20 + word_xsize + 20 yalign 0.5 ysize 20 + word_ysize + 20
          background None hover_background Frame(("minigames poem highlight" + str(random.randint(1,4))))
        activate_sound renpy.random.choice(["<from 0.2 to 0.6>scribble","<from 0.4 to 0.8>scribble","<from 0.8 to 1.15>scribble","<from 1.0>scribble"])
        action [SetDict(isabelle_words,outcome,isabelle_words[outcome]+1), SetVariable("happy",False), SetVariable("angry",False), SetVariable(outcome,True), Return()]


screen letter_instructions(message):
  modal True
  zorder 200
  style_prefix "stepping_instructions"
  add "#0008"
  frame style "notification_modal_title_frame" yalign 0.17:
    text "Warning" style "notification_modal_title"
  frame:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Start" action Return()
  key "game_menu" action NullAction()




## Labels

label start_poem_minigame(skip_instructions=False):
  if not skip_instructions:
    call screen poem_instructions("It's time to write a poem!\n\nPick words you think your\nfavorite girl would like.")
  $_rollback = False
  $game.ui.hide_hud = True
  call poem_loop
  $_rollback = True
  $game.ui.hide_hud = False
  return


label poem_loop:
  $winning_words = {"flora": 0, "isabelle": 0, "kate": 0, "lindsey": 0, "maxine": 0}
  $hop = None
  $progress = 1
  while not progress > 5:
    call screen poem_minigame(hop=hop)
    $progress+=1
  return


label start_letter_minigame(skip_instructions=False):
  if not skip_instructions:
    call screen letter_instructions("It's time to write a\nheartfelt letter!\n\nPick words you think\n[isabelle] would like.")
  $_rollback = False
  $game.ui.hide_hud = True
  call letter_loop
  $_rollback = True
  $game.ui.hide_hud = False
  return


label letter_loop:
  $isabelle_words = {"happy": 0, "angry": 0}
  $happy = angry = False
  $progress = 1
  while not progress > 5:
    call screen letter_minigame(happy=happy, angry=angry)
    $progress+=1
  return
