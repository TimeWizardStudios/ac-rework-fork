init -100 python:
#####################################################################
####################Cursor Displayable goes here############################
#####################################################################
  class LockDisplay(renpy.Displayable):
    def __init__(self, *args, **kwargs):
      renpy.Displayable.__init__(self, *args, **kwargs)
      self.mouse_pos = (0, 0)
      self.child = None
      self.child_size = (1, 1)
      self.kwargs = kwargs
      self.args = args

    def render(self, w, h, st, at):#This doesn't actually render anything
      fixed = Fixed()
      self.child = fixed
      child_render = renpy.render(self.child, w, h, st, at)
      self.child_size = child_render.get_size()
      renpy.redraw(self, 0)
      return child_render

    def event(self, e, x, y, st):
      if renpy.get_screen("lock_picking_lesson_turn"):#make sure this is only checked on the correct screen
        if e.type==pygame.MOUSEBUTTONDOWN and e.button==1:
          lock_picking_minigame.pin.state = 'active'
        if e.type==pygame.MOUSEBUTTONUP and e.button==1:
          if lock_picking_minigame.pin.state == 'active':
            if (lock_picking_minigame.pin.yoffset-lock_picking_minigame.pin.win_offset) < -lock_picking_minigame.delta_win:
              lock_picking_minigame.pin.state = 'inactive'
              return 'lose_reset'
            elif ((lock_picking_minigame.pin.yoffset-lock_picking_minigame.pin.win_offset) > lock_picking_minigame.delta_win):
              lock_picking_minigame.pin.state = 'inactive'
              return 'lose'
            else:
              lock_picking_minigame.pin.state = 'done'
              return 'win'
        renpy.timeout(0)
      return

#####################################################################
####################Templates for pins go here##########################
#####################################################################
  pins_by_id = {}
  class BasePinMeta(type):
    def __init__(cls,name,bases,dct):
      super(BasePinMeta,cls).__init__(name,bases,dct)
      if cls.id is not None:
        pins_by_id[cls.id]=cls

  class BasePin(BaseClass):
    __metaclass__=BasePinMeta
    id = None
    def __init__(self,*args,**kwargs):
      super(BasePin,self).__init__(*args,**kwargs)
#     jotapê was here
#     self.sound_played= False
#     jotapê was here
    def update(self,tf,st,at):#this method is run during transforms to update position
      self.yoffset = tf.yoffset
#     jotapê was here
#     if (self.yoffset <= (self.win_offset+(lock_picking_minigame.delta_win))) and not self.sound_played and self.state=='active':
#       self.sound_played = True
#       jotapê was here
        # renpy.play(filename, channel=None, **kwargs)
        #Went with line because it's snappier than the lock sound
#       jotapê was here
#       renpy.play('line_snapping')
#       jotapê was here
    def reset(self):
      # self.yoffset = 0 ##Maybe leave this be for smoother transition
      self.state = "inactive"
#     jotapê was here
#     self.sound_played = False
#     jotapê was here

  class Pin_1(BasePin):
    id = "pin_1"
    def __init__(self,*args,**kwargs):
      super(Pin_1,self).__init__(*args,**kwargs)
      self.pin_image = "minigames lock_picking_lesson pin1"
      self.start_position = (819,469)
      self.end_position = (819,350)
      self.win_offset = -89
      self.yoffset = 0
      self.offset_speed = 1
      self.state = 'inactive' ## inactive/active/done
  class Pin_2(BasePin):
    id = "pin_2"
    def __init__(self,*args,**kwargs):
      super(Pin_2,self).__init__(*args,**kwargs)
      self.pin_image = "minigames lock_picking_lesson pin2"
      self.start_position = (943,469)
      self.end_position = (943,350)
      self.win_offset = -54
      self.yoffset = 0
      self.offset_speed = 1
      self.state = 'inactive' ## inactive/active/done

  class Pin_3(BasePin):
    id = "pin_3"
    def __init__(self,*args,**kwargs):
      super(Pin_3,self).__init__(*args,**kwargs)
#     jotapê was here
      self.pin_image = "minigames lock_picking_lesson pin1"
#     jotapê was here
      self.start_position = (1079,469)
      self.end_position = (1079,350)
      self.win_offset = -89
      self.yoffset = 0
      self.offset_speed = 1
      self.state = 'inactive' ## inactive/active/done

  class Pin_4(BasePin):
    id = "pin_4"
    def __init__(self,*args,**kwargs):
      super(Pin_4,self).__init__(*args,**kwargs)
#     jotapê was here
      self.pin_image = "minigames lock_picking_lesson pin3"
#     jotapê was here
      self.start_position = (1205,469)
      self.end_position = (1205,350)
      self.win_offset = -18
      self.yoffset = 0
      self.offset_speed = 1
      self.state = 'inactive' ## inactive/active/done

#####################################################################
####################State class goes here##################################
#####################################################################

  class lock_picking_state(BaseClass):
    default_pins = ['pin_1','pin_2','pin_3','pin_4']
    def __init__(self, current_pin = 0, **kwargs):
#     jotapê was here
      self.speeds = [1.5,2,1,2.5]
      self.speed = self.speeds[current_pin]
#     jotapê was here
      self.current_pin = current_pin
      self.win = False
      self.delta_win = 5

#     jotapê was here
#     if mc.owned_item("high_tech_lockpick"):#pin
      self.tool = "minigames lock_picking_lesson safety_pin"
#     else:#paperclip
#       self.tool="minigames lock_picking_lesson clip"
#       jotapê was here
      self.tool_offset = (0,0)
      self.pins = []
      for pin in self.default_pins:
        self.pins.append(pins_by_id[pin]())
      self.pin = self.pins[self.current_pin]

    def round_advance(self):
      self.current_pin+=1
      if self.current_pin>3:
        self.win = True
        return 'win'
      self.pin = self.pins[self.current_pin]
#     jotapê was here
      self.speed = self.speeds[self.current_pin]
#     jotapê was here

    def update_tool(self,tf,st,at):
      self.tool_offset = (tf.xoffset,tf.yoffset)

    def game_reset(self):
      for pin in self.pins:
        pin.reset()
      self.current_pin = 0
      self.pin = self.pins[self.current_pin]
#     jotapê was here
      self.speed = self.speeds[self.current_pin]
#     jotapê was here

  class custom_mouseup(Action):
    def __call__(self):
      pass

  class custom_mousedown(Action):
    def __call__(self):
      pass

#####################################################################
####################Labels#############################################
#####################################################################

# jotapê was here
label start_lock_picking_lesson_minigame(item="high_tech_lockpick",skip_instructions=False):
# jotapê was here
  if not skip_instructions:
#   jotapê was here
    call screen lock_picking_lesson_instructions("Click and hold to raise each pin\ninto position and pick the lock.\nBe careful not to hold too long!")
#   jotapê was here
  python:
    lock_picking_minigame=lock_picking_state()
  # $mouse_visible = False
  $_rollback = False
  $game.ui.hide_hud=1
# hide screen interface_hider
  show screen LPL_minigame_skip
# jotapê was here
  hide minigame_background onlayer screens
  hide minigame_pin1 onlayer screens
  hide minigame_pin2 onlayer screens
  hide minigame_pin3 onlayer screens
  hide minigame_pin4 onlayer screens
  hide minigame_safety_pin onlayer screens
  hide minigame_workaround onlayer screens
  call lock_picking_loop(item)
# jotapê was here
  # $mouse_visible = True
  $_rollback = True
  $game.ui.hide_hud=0
# show screen interface_hider
  hide screen LPL_minigame_skip
# jotapê was here
  hide screen lock_picking_lesson_inactive
# jotapê was here
# hide screen hint_frame
  return

# jotapê was here
label lock_picking_loop(item=item):
# jotapê was here
  show screen lock_picking_lesson_inactive
  while not lock_picking_minigame.win:
    call screen lock_picking_lesson_turn()
    if _return == 'lose':
      $statement_pin = lock_picking_minigame.current_pin+1
      pass #reset current pin
      # if statement_pin == 1 or statement_pin == 3:
        # mc"[statement_pin] is binding"
      # else:
        # mc "nothing on [statement_pin]"
      pause 0.25
    elif _return == 'lose_reset':
#     jotapê was here
      $failure_line = lock_picking_minigame.current_pin
#     jotapê was here
      $statement_pin = lock_picking_minigame.current_pin+1
      $lock_picking_minigame.game_reset() #reset all pins and start over
#     jotapê was here
      if item == "high_tech_lockpick" and failure_line:
        window auto
        "Damn it! I hate technology!"
        window hide
      elif item == "safety_pin":
        play sound "line_snapping"
        $lock_picking_minigame.tool = "minigames lock_picking_lesson safety_pin_broken"
        hide screen LPL_minigame_skip
        window auto show None
        "Crap! I broke it." with hpunch
        window hide
        $lock_picking_minigame.pin.state = "broken"
        $mc.remove_item("safety_pin")
        pause 0.25
        hide screen lock_picking_lesson_inactive
        return
#       jotapê was here
      pause 0.25
    elif _return == 'win':
      if not lock_picking_minigame.win:
        #next pin
#       jotapê was here
        play sound "shutter"
#       jotapê was here
        $statement_pin = lock_picking_minigame.current_pin+1
        $lock_picking_minigame.round_advance()
#       jotapê was here
#       if statement_pin == 1:
#         mc "Click out of [statement_pin]"
#       elif statement_pin == 2:
#         mc "Nice little click on [statement_pin]."
#         jotapê was here
        pause 0.25
  play audio "lock_click"
  hide screen LPL_minigame_skip
# jotapê was here
  window auto
  if item == "high_tech_lockpick":
    "Easy as that! This is why technology is so important."
  elif item == "safety_pin":
    "Yes! I'm something of a Chad myself!"
    "At least when it comes to breaking and entering."
  window hide
# jotapê was here
  hide screen lock_picking_lesson_inactive
  return#winlabel

#####################################################################
#####################Transforms#########################################
#####################################################################
transform tf_pin_active(pin):
  parallel:
    yoffset pin.yoffset
    linear lock_picking_minigame.speed yoffset -118
  parallel:
    block:
      function pin.update
      0.01
      repeat

transform tf_pin_rest_static(pin):
  parallel:
    yoffset pin.yoffset
  parallel:
    block:
      function pin.update
      0.01
      repeat

transform tf_pin_rest(pin):
  parallel:
    yoffset pin.yoffset
    linear lock_picking_minigame.speed/4 yoffset 0
  parallel:
    block:
      function pin.update
      0.01
      repeat

# jotapê was here
# transform tf_clip_rest():
# parallel:
#   xoffset lock_picking_minigame.tool_offset[0]
#   linear lock_picking_minigame.speed/8 xoffset lock_picking_minigame.current_pin*128
# parallel:
#   yoffset lock_picking_minigame.tool_offset[1]
#   linear lock_picking_minigame.speed/4 yoffset 0
# parallel:
#   block:
#     function lock_picking_minigame.update_tool
#     0.01
#     repeat

# transform tf_clip_static():
# parallel:
#   xoffset lock_picking_minigame.tool_offset[0]
#   linear lock_picking_minigame.speed/8 xoffset lock_picking_minigame.current_pin*128
# parallel:
#   yoffset lock_picking_minigame.tool_offset[1]
#   linear lock_picking_minigame.speed/4 yoffset 0
# parallel:
#   block:
#     function lock_picking_minigame.update_tool
#     0.01
#     repeat

# transform tf_clip_use():
# parallel:
#   xoffset lock_picking_minigame.tool_offset[0]
#   linear lock_picking_minigame.speed/8 xoffset lock_picking_minigame.current_pin*128
# parallel:
#   yoffset lock_picking_minigame.tool_offset[1]
#   linear lock_picking_minigame.speed yoffset -118
# parallel:
#   block:
#     function lock_picking_minigame.update_tool
#     0.01
#     repeat
# jotapê was here

transform tf_safetypin_rest():
  parallel:
    xoffset lock_picking_minigame.tool_offset[0]
    linear lock_picking_minigame.speed/8 xoffset lock_picking_minigame.current_pin*128
  parallel:
    yoffset lock_picking_minigame.tool_offset[1]
    linear lock_picking_minigame.speed/4 yoffset 0
  parallel:
    block:
      function lock_picking_minigame.update_tool
      0.01
      repeat

transform tf_safetypin_static():
  parallel:
    xoffset lock_picking_minigame.tool_offset[0]
    linear lock_picking_minigame.speed/8 xoffset lock_picking_minigame.current_pin*128
  parallel:
    yoffset lock_picking_minigame.tool_offset[1]
    linear lock_picking_minigame.speed/4 yoffset 0
  parallel:
    block:
      function lock_picking_minigame.update_tool
      0.01
      repeat

transform tf_safetypin_use():
  parallel:
    xoffset lock_picking_minigame.tool_offset[0]
    linear lock_picking_minigame.speed/8 xoffset lock_picking_minigame.current_pin*128
  parallel:
    yoffset lock_picking_minigame.tool_offset[1]
    linear lock_picking_minigame.speed yoffset -118
  parallel:
    block:
      function lock_picking_minigame.update_tool
      0.01
      repeat

#####################################################################
#######################Screens#########################################
#####################################################################
screen lock_picking_lesson_turn():
  default cursor = LockDisplay()
# jotapê was here
# add 'minigames lock_picking_lesson background'
# add 'minigames lock_picking_lesson darkoverlay'
# add 'minigames lock_picking_lesson shadow' pos (385,207)
# add 'minigames lock_picking_lesson popup' pos (435,270)
# add 'minigames lock_picking_lesson handleoverlay' pos (440,273)
# add 'minigames lock_picking_lesson lock' pos (733,326)
  add "location"
  add LiveComposite((106,106),(0,0),Crop((904,921,106,106),"school first_hall_west 1fwcorridor"),(0,0),Crop((904,921,106,106),"school first_hall_west night_overlay")) xpos 904 ypos 921
  add "#0008"
  add "minigames lock_picking_lesson background" xpos 436 ypos 270
# jotapê was here
  key 'mouseup_1' action custom_mouseup #capture True #might work but is only available in renpy 7.5 +
  key 'mousedown_1' action custom_mousedown #noooot working, fixed with custom event in LockDisplay
  ###Pins###
  for pin in lock_picking_minigame.pins:
    add pin.pin_image:
      pos pin.start_position
      if pin.state=='active':
        at tf_pin_active(pin)
      elif pin.state=='done':
        at tf_pin_rest_static(pin)
      else:
        at tf_pin_rest(pin)
  ####Picks####
# jotapê was here
# if mc.owned_item("high_tech_lockpick"):#pin
# jotapê was here
  add lock_picking_minigame.tool:
    # pos (48,648)
#   jotapê was here
    pos (105,645)
#   anchor (1.0,0.0)
#   rotate -5
#   jotapê was here
    if lock_picking_minigame.pin.state == 'active':
      at tf_safetypin_use()
    elif lock_picking_minigame.pin.state == 'done':
      at tf_safetypin_static()
    else:
      at tf_safetypin_rest()
# jotapê was here
# else:#clip
#   add lock_picking_minigame.tool:
#     pos (-214,585)
#     if lock_picking_minigame.pin.state == 'active':
#       at tf_clip_use()
#     elif lock_picking_minigame.pin.state == 'done':
#       at tf_clip_static()
#     else:
#       at tf_clip_rest()
#       jotapê was here
  add cursor ##Cursor for catching mouse events that renpy mysteriously ignores

screen lock_picking_lesson_inactive():#copy of main screen but without active components
  #So that the onclick events aren't accidentally triggered by dismissing dialogue
# jotapê was here
# add 'minigames lock_picking_lesson background'
# add 'minigames lock_picking_lesson darkoverlay'
# add 'minigames lock_picking_lesson shadow' pos (385,207)
# add 'minigames lock_picking_lesson popup' pos (435,270)
# add 'minigames lock_picking_lesson handleoverlay' pos (440,273)
# add 'minigames lock_picking_lesson lock' pos (733,326)
  add LiveComposite((106,106),(0,0),Crop((904,921,106,106),"school first_hall_west 1fwcorridor"),(0,0),Crop((904,921,106,106),"school first_hall_west night_overlay")) xpos 904 ypos 921
  add "#0008"
  add "minigames lock_picking_lesson background" xpos 436 ypos 270
# jotapê was here
  ###Pins###
  for pin in lock_picking_minigame.pins:
    add pin.pin_image:
      pos pin.start_position
      if pin.state=='active':
        at tf_pin_active(pin)
      elif pin.state=='done':
        at tf_pin_rest_static(pin)
      else:
        at tf_pin_rest(pin)
  ####Picks####
# jotapê was here
# if mc.owned_item("high_tech_lockpick"):#pin
# jotapê was here
  add lock_picking_minigame.tool:
    # pos (48,648)
#   jotapê was here
    pos (105,645)
#   anchor (1.0,0.0)
#   rotate -5
#   jotapê was here
    if lock_picking_minigame.pin.state == 'active':
      at tf_safetypin_use()
    elif lock_picking_minigame.pin.state == 'done':
      at tf_safetypin_static()
#   jotapê was here
    elif lock_picking_minigame.pin.state == 'broken':
      at transform:
        xoffset lock_picking_minigame.tool_offset[0] yoffset lock_picking_minigame.tool_offset[1] alpha 1.0
        easein 0.25 alpha 0.0
#       jotapê was here
    else:
      at tf_safetypin_rest()
# jotapê was here
# else:#clip
#   add lock_picking_minigame.tool:
#     pos (-214,585)
#     if lock_picking_minigame.pin.state == 'active':
#       at tf_clip_use()
#     elif lock_picking_minigame.pin.state == 'done':
#       at tf_clip_static()
#     else:
#       at tf_clip_rest()
#       jotapê was here


screen lock_picking_lesson_instructions(message):
  modal True
  zorder 200
  style_prefix "stepping_instructions"
  add "#0008"
  frame style "notification_modal_title_frame" yalign 0.225:
    text "Warning" style "notification_modal_title"
  frame:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Start" action Return('')
  key "game_menu" action NullAction()

screen LPL_minigame_skip():
  style_prefix "kitchen_minigame_skip"
  zorder 99
  fixed:
    textbutton "Skip Minigame":
#     jotapê was here
      action SetVariable("lock_picking_minigame.win",True),Jump("kitchen_minigame_done")
      xpos 30
      ypos 30
#     jotapê was here
