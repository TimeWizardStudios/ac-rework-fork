define kitchen_minigame_win_threshold=125
define kitchen_minigame_epic_fail_threshold=400
define kitchen_minigame_ingredient_speed_range=(0.75,1.25)
define kitchen_minigame_knife_speed_range=(0.5,1.0)

init python:
  class KitchenMinigameState(BaseClass):
    def __init__(self,max_rounds=3,**kwargs):
      self.max_rounds=max_rounds
      self.current_round=0
      self.reset_ingredients()
    def reset_ingredients(self):
      self.ingredients=[
        ["Beet Root","minigames kitchen veg_beetroot"],
        ["Garlic","minigames kitchen veg_garlic"],
        ["Red Onion","minigames kitchen veg_red_onion"],
        ["White Onion","minigames kitchen veg_white_onion"],
        ["Yellow Onion","minigames kitchen veg_yellow_onion"],
        ]
      renpy.random.shuffle(self.ingredients)
      self.ingredients.pop()
      for n in range(len(self.ingredients)):
        speed_min=kitchen_minigame_ingredient_speed_range[0]
        speed_range=kitchen_minigame_ingredient_speed_range[1]-speed_min
        ingredient_speed=renpy.random.random()*speed_range+speed_min
        speed_min=kitchen_minigame_knife_speed_range[0]
        speed_range=kitchen_minigame_knife_speed_range[1]-speed_min
        knife_speed=renpy.random.random()*speed_range+speed_min
        dir=[1.0,-1.0][renpy.random.randint(0,1)]
        self.ingredients[n]+=[ingredient_speed,dir,0.0,knife_speed,0.0]
      self.current_ingredient=renpy.random.randint(0,len(self.ingredients)-1)

  class KitchenMinigameUpdateIngredientPos(BaseClass):
    def __init__(self,n):
      self.n=n
    def __call__(self,tf,st,at):
      kitchen_minigame.ingredients[self.n][4]=tf.xoffset

  class KitchenMinigameUpdateKnifePos(BaseClass):
    def __init__(self,n):
      self.n=n
    def __call__(self,tf,st,at):
      kitchen_minigame.ingredients[self.n][6]=tf.xoffset

  class KitchenMinigameClick(Action):
    def __call__(self):
      ingredient=kitchen_minigame.ingredients[kitchen_minigame.current_ingredient]
      delta=abs(ingredient[4]-ingredient[6])
      if delta<kitchen_minigame_win_threshold:
        return "win"
      elif delta<kitchen_minigame_epic_fail_threshold:
        return "lose1"
      else:
        return "lose2"

label start_kitchen_minigame:
  $kitchen_minigame=KitchenMinigameState(max_rounds=3)
  call kitchen_minigame_loop
  return

label kitchen_minigame_loop:
  $game.ui.hide_hud=1
  show minigames kitchen bg
  show screen kitchen_minigame_skip
  while kitchen_minigame.current_round<kitchen_minigame.max_rounds:
    $kitchen_minigame.reset_ingredients()
    call screen kitchen_minigame_init
    call screen kitchen_minigame_moving
    if _return=="win":
      $kitchen_minigame.current_round+=1
    call screen kitchen_minigame_result("minigames kitchen result_"+_return)
    $renpy.pause(0.5)
  $game.ui.hide_hud=0
label kitchen_minigame_done:
  hide screen kitchen_minigame_skip
  hide minigames kitchen bg
  return

transform tf_kitchen_minigame_ingredients_appear:
  yoffset -1500
  easein 0.5 yoffset 0

transform tf_kitchen_minigame_ingredients_disappear:
  1.5
  easein 0.5 yoffset -1500

transform tf_kitchen_minigame_ingredient_init:
  zoom 0.5
  0.5

transform tf_kitchen_minigame_ingredient_init_shake:
  zoom 0.5
  0.5
  block:
    easein 0.07 xoffset -50
    linear 0.15 xoffset 50
    easeout 0.07 xoffset 0
    repeat 3

transform tf_kitchen_minigame_knife_init:
  rotate 45
  zoom 0.5
  xoffset -500

transform tf_kitchen_minigame_ingredient_moving(n,speed,dir):
  zoom 0.5
  parallel:
    block:
      linear speed xoffset -500*dir
      linear speed xoffset 0
      linear speed xoffset 500*dir
      linear speed xoffset 0
      repeat
  parallel:
    block:
      function KitchenMinigameUpdateIngredientPos(n)
      0.01
      repeat

transform tf_kitchen_minigame_knife_moving(n,speed):
  zoom 0.5
  rotate 45
  xoffset -500
  parallel:
    block:
      linear speed xoffset 0
      linear speed xoffset 500
      linear speed xoffset 0
      linear speed xoffset -500
      repeat
  parallel:
    block:
      function KitchenMinigameUpdateKnifePos(n)
      0.01
      repeat

transform tf_kitchen_minigame_ingredient_result:
  zoom 0.5

transform tf_kitchen_minigame_knife_result:
  zoom 0.5
  rotate 45

transform tf_kitchen_minigame_knife_result_chop:
  zoom 0.5
  rotate 45

transform tf_kitchen_minigame_result:
  alpha 0.0
  0.5
  easein 0.25 alpha 1.0
  0.5
  easeout 0.25 alpha 0.0

screen kitchen_minigame_init():
  fixed:
    at tf_kitchen_minigame_ingredients_appear
    for n,ingredient in enumerate(kitchen_minigame.ingredients):
      add ingredient[1] pos (1000,150+n*250) anchor (0.5,0.5):
        if n==kitchen_minigame.current_ingredient:
          at tf_kitchen_minigame_ingredient_init_shake
        else:
          at tf_kitchen_minigame_ingredient_init
      add "minigames kitchen knife_ready" pos (1000,150+n*250) anchor (0.5,0.5):
        at tf_kitchen_minigame_knife_init
  timer 1.0 action Return()

screen kitchen_minigame_moving():
  for n,ingredient in enumerate(kitchen_minigame.ingredients):
    add ingredient[1] pos (1000,150+n*250) anchor (0.5,0.5):
      at tf_kitchen_minigame_ingredient_moving(n,ingredient[2],ingredient[3])
    add "minigames kitchen knife_ready" pos (1000,150+n*250) anchor (0.5,0.5):
      at tf_kitchen_minigame_knife_moving(n,ingredient[5])
  button:
    action KitchenMinigameClick()

screen kitchen_minigame_result(img):
  fixed:
    at tf_kitchen_minigame_ingredients_disappear
    for n,ingredient in enumerate(kitchen_minigame.ingredients):
      add ingredient[1] pos (1000,150+n*250) anchor (0.5,0.5) xoffset ingredient[4]:
        at tf_kitchen_minigame_ingredient_result
      if n==kitchen_minigame.current_ingredient:
        add "minigames kitchen knife_chop" pos (1000,150+n*250) anchor (0.5,0.5) xoffset ingredient[6]-50:
          at tf_kitchen_minigame_knife_result_chop
      else:
        add "minigames kitchen knife_ready" pos (1000,150+n*250) anchor (0.5,0.5) xoffset ingredient[6]:
          at tf_kitchen_minigame_knife_result
  add img align (0.5,0.5):
    at tf_kitchen_minigame_result
  timer 2.0 action Return()

style kitchen_minigame_skip_button is textbutton

screen kitchen_minigame_skip():
  style_prefix "kitchen_minigame_skip"
  zorder 99
  fixed:
    textbutton "Skip Minigame" action Jump("kitchen_minigame_done") xpos 20 ypos 20
