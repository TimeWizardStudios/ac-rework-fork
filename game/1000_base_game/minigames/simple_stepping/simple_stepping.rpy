init -100 python:
#####################################################################
####################Cursor Displayable goes here############################
#####################################################################
  class ActionDisplay(renpy.Displayable):
    def __init__(self, *args, **kwargs):
      renpy.Displayable.__init__(self, *args, **kwargs)
      self.mouse_pos = (0, 0)
      self.child = None
      self.child_size = (1, 1)
      self.kwargs = kwargs
      self.args = args
      self.action = "minigames simple_stepping mouse_" + simple_stepping_minigame.current_action
      self.icon_align = (0.5, 0.5)

    def render(self, w, h, st, at):#This does render something now
      fixed = Fixed(
            Transform(
              self.action,
              align=self.icon_align,
              pos=(simple_stepping_minigame.mouse_pos[0] / self.child_size[0], simple_stepping_minigame.mouse_pos[1] / self.child_size[1]),
            )
      )
      self.child = fixed
      child_render = renpy.render(self.child, w, h, st, at)
      self.child_size = child_render.get_size()
      renpy.redraw(self, 0)
      return child_render

    def event(self, e, x, y, st):
      if renpy.get_screen("simple_stepping_turn"):#make sure this is only checked on the correct screen
        simple_stepping_minigame.mouse_pos = (x, y)#update mouse pos
        if e.type==pygame.MOUSEBUTTONUP and e.button==1:
          self.action = "minigames simple_stepping mouse_" + simple_stepping_minigame.current_action
        renpy.timeout(0)
      return
    def click(self):
      self.action = "minigames simple_stepping mouse_" + simple_stepping_minigame.current_action
#####################################################################
####################Templates for foot types go here##########################
#####################################################################
  feet_by_id = {}
  class BaseFeetMeta(type):
    def __init__(cls,name,bases,dct):
      super(BaseFeetMeta,cls).__init__(name,bases,dct)
      if cls.id is not None:
        feet_by_id[cls.id]=cls
  class BaseFeet(BaseClass):
    __metaclass__=BaseFeetMeta
    id = None
    def __init__(self,*args,**kwargs):
      super(BaseFeet,self).__init__(*args,**kwargs)
    def update(self,tf,st,at):#this method is run during transforms to update position
      self.pos = (tf.xoffset,tf.yoffset)

  class FootLeft(BaseFeet):
    id = "foot_left"
    foot_image = "minigames simple_stepping feet l_idle_pale" #default image
    tense_image = "minigames simple_stepping feet l_curled_pale" #Image to show near max satisfaction
    start_request = "lick" # lick/kiss/bite
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [-750,280]
    end_pos = [0,523]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"  #States:  waiting reaching active done
      self.previous_state = "waiting" #used to check if the state has changed
      self.pos = self.start_pos #tracks pos so transforms can be done part-way between turns
      self.miss_count = 0

  class FootRight(BaseFeet):
    id = "foot_right"
    foot_image = "minigames simple_stepping feet r_idle_pale"
    tense_image = "minigames simple_stepping feet r_curled_pale"
    start_request = "bite" # lick/kiss/bite
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [1925,280]
    end_pos = [1226,510]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.miss_count = 0

  class FootLindsey(BaseFeet):
    id = "foot_lindsey"
    foot_image = "minigames simple_stepping feet lindsey_idle"
    tense_image = "minigames simple_stepping feet lindsey_curled"
    start_request = "lick"
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [1200,-50]
    end_pos = [818,402]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.miss_count = 0

  class FootCenter(BaseFeet):
    id = "foot_center"
    foot_image = "minigames simple_stepping feet idle_mid"
    tense_image = "minigames simple_stepping feet curled_mid"
    start_request = "kiss"
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [1200,-50]
    end_pos = [818,402]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.miss_count = 0

  class FootStacy(BaseFeet):
    id = "foot_stacy"
    foot_image = "minigames simple_stepping feet stacy_idle"
    tense_image = "minigames simple_stepping feet stacy_curled"
    start_request = "lick" # lick/kiss/bite
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [-750,280]
    end_pos = [0,523]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.miss_count = 0

  class FootCasey(BaseFeet):
    id = "foot_casey"
    foot_image = "minigames simple_stepping feet casey_idle"
    tense_image = "minigames simple_stepping feet casey_curled"
    start_request = "lick"
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [-750,280]
    end_pos = [0,523]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.miss_count = 0

  class FootLacey(BaseFeet):
    id = "foot_lacey"
    foot_image = "minigames simple_stepping feet lacey_idle"
    tense_image = "minigames simple_stepping feet lacey_curled"
    start_request = "kiss"
    start_satisfaction = 0
    max_satisfaction = 100
    start_pos = [1925,280]
    end_pos = [1226,510]
    def __init__(self,*args,**kwargs):
      self.satisfaction = self.start_satisfaction
      self.request = self.start_request
      self.state = "reaching"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.miss_count = 0

#####################################################################
####################State class goes here##################################
#####################################################################

  class simple_stepping_state(BaseClass):
    default_feet = ['foot_left','foot_right','foot_center']
    def __init__(self,attack_feet,speed=3.0,max_rounds=20,max_score=10,handy=0.3,previous_action=None,**kwargs):
      self.hits = [] #list of (x,y) where hits have taken place
      self.misses = [] #list of (x,y) where misses have taken place
      self.feet = [] #this container holds the instantiated feet
      self.waiters = [] #holds list position of feet that are ready to attack
      self.mouse_pos = (0, 0) #tracks mouse pos for weapon and hit-marker
      self.round = 0 #current round
      self.score = 0 #Do we need a score?
      self.lose = False
      self.win = False
      self.miss_threshhold = 2 #Number of turns without an action before satisfaction ticks backwards
      self.current_action = previous_action or 'lick' #the player's current action
      self.default_action = ""
      self.available_actions = ['lick','kiss','bite']
      self.speed = speed #seconds per round
      self.max_rounds = max_rounds #win condition 1
      self.max_score = max_score #win condition 2
      self.attack_feet = default_feet if attack_feet is None else attack_feet
      self.max_feet = 1 ##Setting this higher than 1 could result in 2 feet sharing a hole.
      self.handy = handy
      for foot in self.attack_feet:
        self.feet.append(feet_by_id[foot]())
      ##need to move all feet to wait list
    def round_advance(self):
      self.round+=1
      self.active_count = 0
      self.hits=[]
      self.misses = []
      self.waiters = []
      for foot in self.feet:
        if foot.state == "reaching":
          self.active_count += 1
          foot.state = "arriving"
        elif foot.state=="arriving":
          self.active_count += 1
          foot.state = "active"
        elif foot.state == "active":
          self.active_count += 1
          foot.miss_count +=1
          if foot.satisfaction<foot.max_satisfaction:
            foot.request = random.sample(self.available_actions,1)[0]
            if foot.miss_count >= self.miss_threshhold:
              foot.miss_count = 0
              foot.satisfaction = max((foot.satisfaction-20),0)
          else:
            foot.state = "done"
        elif foot.state == "waiting":
          foot.previous_state = "waiting"
          self.waiters.append(self.feet.index(foot))

      ##Select a random foot
      if self.waiters and (self.active_count<self.max_feet):
        reacher = self.waiters.pop(0) #random.sample(self.waiters,1)[0]
        self.feet[reacher].state = "reaching"
      else:
        for foot in self.feet:
          if foot.state == "done":
            pass#Once all feet are done waiting,
          else:#check if any feet are still out/occupied.
            return
        self.win = True#Win if all feet are done.

    def set_action(self,new_action):
      self.current_action = new_action

    def focus_action(self,new_focus):
      self.default_action = new_focus

    def interact(self,foot):
      if foot.state == "active":
        if self.current_action == foot.request:
          self.hits.append((self.mouse_pos[0],self.mouse_pos[1]))
          foot.satisfaction+=20
          if foot.miss_count:
            foot.miss_count = 0#reset on good interaction
          if foot.satisfaction>=foot.max_satisfaction:
            foot.state = "done"
            foot.previous_state="active"
          foot.request = ""
        else:
          self.misses.append((self.mouse_pos[0],self.mouse_pos[1]))
          foot.miss_count+=1 #incorrect action
          foot.request = ""
      else:#if not active nothing should happen yet
        pass


#####################################################################
####################Styles#############################################
#####################################################################
style stepping_instructions_frame is frame:
  background Frame("ui frame_popup",220,160,240,150)
  padding (80,60)
  align (0.5,0.5)
# xmaximum 820

style stepping_instructions_text is ui_text:
  xalign 0.5
  text_align 0.5

style stepping_instructions_button is textbutton:
  xminimum 250
  size_group "confirm_buttons"


#####################################################################
####################Labels#############################################
#####################################################################

label start_simple_stepping_minigame(difficulty='story',feet=None,action=simple_stepping_minigame.current_action if hasattr(store,'simple_stepping_minigame') else None,skip_instructions=True):
  if not skip_instructions:
    call screen simple_stepping_instructions("You're the entertainment of the party!\nChoose the appropriate actions to\nsatisfy each guest.")
  if difficulty == 'story':#Only one route here, and can't really fail
    python:
      feet = feet or ["foot_lacey","foot_center","foot_casey","foot_stacy"]
      simple_stepping_minigame=simple_stepping_state(attack_feet=feet,previous_action=action)
    $mouse_visible = False
    $_rollback = False
    $game.ui.hide_hud=1
    hide screen interface_hider
    show screen kitchen_minigame_skip
    call simple_stepping_loop from _call_simple_stepping_loop
    #todo: Possibly call to some story label here before lindsey gets her turn?
#   python:#gotta make sure to save the best for last
#     feet = ["foot_lindsey"]
#     simple_stepping_minigame=simple_stepping_state(attack_feet=feet,handy=0)
#   call simple_stepping_loop from _call_simple_stepping_loop_lindsey
    $mouse_visible = True
    $_rollback = True
    $game.ui.hide_hud=0
    show screen interface_hider
    hide screen kitchen_minigame_skip
    hide screen hint_frame
  return

label simple_stepping_loop:
  while not simple_stepping_minigame.win:
    $simple_stepping_minigame.round_advance()
    if not simple_stepping_minigame.win:
      call screen simple_stepping_turn
  return

#####################################################################
#####################Transforms#########################################
#####################################################################
transform transform_foot_active(foot):
  xoffset foot.end_pos[0]
  yoffset foot.end_pos[1]
  function foot.update

transform transform_foot_active_curled(foot):
  # xoffset foot.end_pos[0]
  # yoffset foot.end_pos[1]
  # alpha 0.0
  # parallel:
    # linear simple_stepping_minigame.speed/12 alpha 1.0
  xoffset foot.end_pos[0]
  yoffset foot.end_pos[1]
  function foot.update

transform transform_foot_reaching_left(foot):
  xoffset foot.pos[0]
  yoffset foot.pos[1]
  # xanchor 0.5 #rotation messes with offset if anchor is not set to 0.5
  # yanchor 0.5 #but setting anchor to 0.5 messes with the default x/y offset provided by psd.
  # rotate_pad True  #So I gave up on rotating.
  # rotate 5 #but it's here if you want to give it a try. I think it would make it a bit more realistic.

  # parallel:
    # linear simple_stepping_minigame.speed/6 rotate 0
    # linear simple_stepping_minigame.speed/6 anchor (0,0)
  parallel:
    linear simple_stepping_minigame.speed/3 xoffset (foot.end_pos[0]+foot.start_pos[0])/3
    pause simple_stepping_minigame.speed/15
    easein_quad simple_stepping_minigame.speed/6 xoffset foot.end_pos[0]
  parallel:
    ease_back simple_stepping_minigame.speed/1.2 yoffset foot.end_pos[1]
  parallel:
    block:#updates the pos of foot for smooth transitions between
      function foot.update
      0.01
      repeat

transform transform_foot_defeated_left(foot):
  xoffset foot.pos[0]
  yoffset foot.pos[1]
  parallel:
    easeout simple_stepping_minigame.speed/6 xoffset foot.start_pos[0]
  parallel:
    easein simple_stepping_minigame.speed/6 yoffset foot.start_pos[1]
  parallel:
    block:
      function foot.update
      0.01
      repeat

transform transform_foot_reaching_right(foot):
  xoffset foot.pos[0]
  yoffset foot.pos[1]
  parallel:
    linear simple_stepping_minigame.speed/3 xoffset (foot.start_pos[0]-(foot.start_pos[0]-foot.end_pos[0])/1.5)
    pause simple_stepping_minigame.speed/15
    easein_quad simple_stepping_minigame.speed/6 xoffset foot.end_pos[0]
  parallel:
    ease_back simple_stepping_minigame.speed/1.2 yoffset foot.end_pos[1]
  parallel:
    block:
      function foot.update
      0.01
      repeat

transform transform_foot_defeated_right(foot):
  xoffset foot.pos[0]
  yoffset foot.pos[1]
  parallel:
    easeout simple_stepping_minigame.speed/6 xoffset foot.start_pos[0]
  parallel:
    easein simple_stepping_minigame.speed/6 yoffset foot.start_pos[1]
  parallel:
    block:
      function foot.update
      0.01
      repeat

transform transform_foot_reaching(foot):
  xoffset foot.pos[0]
  yoffset foot.pos[1]
  zoom 0.75#Center gets zoom to show distance
  parallel:
    easeout simple_stepping_minigame.speed/1.5 zoom 1.0
  parallel:
    easein_circ simple_stepping_minigame.speed/1.2 xoffset foot.end_pos[0]
  parallel:
    easein_expo simple_stepping_minigame.speed/1.2 yoffset foot.end_pos[1]
  parallel:
    block:
      function foot.update
      0.01
      repeat

transform transform_foot_defeated(foot):
  xoffset foot.pos[0]
  yoffset foot.pos[1]
  parallel:
    easeout simple_stepping_minigame.speed/6 xoffset foot.start_pos[0]
  parallel:
    easein simple_stepping_minigame.speed/6 yoffset foot.start_pos[1]
  parallel:
    block:
      function foot.update
      0.01
      repeat

transform transform_bubble_appear(foot):
  xoffset 0
  yoffset -1000
  easein_back simple_stepping_minigame.speed/12 yoffset 0

transform transform_bubble_disappear(foot):
  xoffset 0
  yoffset 0
  ease_back simple_stepping_minigame.speed/14 yoffset -1000

image simple_stepping_heart:#Animated heart image
  'minigames simple_stepping heart1'
  pause 0.05
  'minigames simple_stepping heart2'
  pause 0.05
  'minigames simple_stepping heart3'
  pause 0.10
  repeat

image simple_stepping_cross:#Animated angry image
  'minigames simple_stepping anger1'
  pause 0.05
  'minigames simple_stepping anger2'
  pause 0.05
  'minigames simple_stepping anger3'
  pause 0.10
  repeat

#####################################################################
#######################Screens#########################################
#####################################################################
screen simple_stepping_turn():
  default cursor = ActionDisplay() #cursor object (used for hearts and crosses)
  add 'minigames simple_stepping background'
  for foot in simple_stepping_minigame.feet:#Loop for feet
    if foot.end_pos[0] > 1200:#right
      if foot.state== "waiting":
        pass #just incase I need to do something here
      elif foot.state == "arriving":#stepping into the box
        imagebutton:
          focus_mask foot.foot_image#Displayable mask so you have to click on the foot
          idle foot.foot_image
          keyboard_focus False#no cheating
          #activate_sound = ""#idk what sound you could use here, but you found something last time.
          action NullAction() #Function(simple_stepping_minigame.hit,foot) #successful hit
          at transform_foot_reaching_right(foot)
      elif foot.state == "active":
        #this state does not move and is when the foot is active and requests are made.
        if foot.satisfaction >=(foot.max_satisfaction-((foot.max_satisfaction/100)*20)):
          imagebutton:
            focus_mask foot.tense_image
            idle foot.tense_image
            keyboard_focus False
            if simple_stepping_minigame.current_action == "kiss":
              activate_sound "<from 0.6 to 0.9>kiss"
            elif simple_stepping_minigame.current_action == "lick":
              activate_sound "<from 0.5>lick"
            elif simple_stepping_minigame.current_action == "bite":
              activate_sound "bite"
            action Function(simple_stepping_minigame.interact,foot)
            at transform_foot_active_curled(foot)
        else:
          imagebutton:
            focus_mask foot.foot_image
            idle foot.foot_image
            keyboard_focus False
            if simple_stepping_minigame.current_action == "kiss":
              activate_sound "<from 0.6 to 0.9>kiss"
            elif simple_stepping_minigame.current_action == "lick":
              activate_sound "<from 0.5>lick"
            elif simple_stepping_minigame.current_action == "bite":
              activate_sound "bite"
            action Function(simple_stepping_minigame.interact,foot)
            at transform_foot_active(foot)
      elif foot.state == "done":#animation for removing the foot from the box
        add foot.foot_image:
          if foot.previous_state == "done":
            at transform_foot_gone_right(foot)
          else:
            at transform_foot_defeated_right(foot)
    elif foot.end_pos[0] > 800:#center
      if foot.state== "waiting":
        pass
      elif foot.state == "arriving":
        imagebutton:
          focus_mask foot.foot_image
          idle foot.foot_image
          keyboard_focus False
          #activate_sound = ""
          action NullAction()
          at transform_foot_reaching(foot)
      elif foot.state == "active":
        if foot.satisfaction >=(foot.max_satisfaction-((foot.max_satisfaction/100)*20)):
          imagebutton:
            focus_mask foot.tense_image
            idle foot.tense_image
            keyboard_focus False
            if simple_stepping_minigame.current_action == "kiss":
              activate_sound "<from 0.6 to 0.9>kiss"
            elif simple_stepping_minigame.current_action == "lick":
              activate_sound "<from 0.5>lick"
            elif simple_stepping_minigame.current_action == "bite":
              activate_sound "bite"
            action Function(simple_stepping_minigame.interact,foot)
            at transform_foot_active_curled(foot)
        else:
          imagebutton:
            focus_mask foot.foot_image
            idle foot.foot_image
            keyboard_focus False
            if simple_stepping_minigame.current_action == "kiss":
              activate_sound "<from 0.6 to 0.9>kiss"
            elif simple_stepping_minigame.current_action == "lick":
              activate_sound "<from 0.5>lick"
            elif simple_stepping_minigame.current_action == "bite":
              activate_sound "bite"
            action Function(simple_stepping_minigame.interact,foot)
            at transform_foot_active(foot)
      elif foot.state == "done":
        add foot.foot_image:
          if foot.previous_state == "done":
            at transform_foot_gone(foot)
          else:
            at transform_foot_defeated(foot)
    else:#left
      if foot.state== "waiting":
        pass
      elif foot.state == "arriving":
        imagebutton:
          focus_mask foot.foot_image
          idle foot.foot_image
          keyboard_focus False
          #activate_sound = ""
          action NullAction()
          at transform_foot_reaching_left(foot)
      elif foot.state == "active":
        if foot.satisfaction >=(foot.max_satisfaction-((foot.max_satisfaction/100)*20)):
          imagebutton:
            focus_mask foot.tense_image
            idle foot.tense_image
            keyboard_focus False
            if simple_stepping_minigame.current_action == "kiss":
              activate_sound "<from 0.6 to 0.9>kiss"
            elif simple_stepping_minigame.current_action == "lick":
              activate_sound "<from 0.5>lick"
            elif simple_stepping_minigame.current_action == "bite":
              activate_sound "bite"
            action Function(simple_stepping_minigame.interact,foot)
            at transform_foot_active_curled(foot)
        else:
          imagebutton:
            focus_mask foot.foot_image
            idle foot.foot_image
            keyboard_focus False
            if simple_stepping_minigame.current_action == "kiss":
              activate_sound "<from 0.6 to 0.9>kiss"
            elif simple_stepping_minigame.current_action == "lick":
              activate_sound "<from 0.5>lick"
            elif simple_stepping_minigame.current_action == "bite":
              activate_sound "bite"
            action Function(simple_stepping_minigame.interact,foot)
            at transform_foot_active(foot)
      elif foot.state == "done":
        add foot.foot_image:
          if foot.previous_state == "done":
            at transform_foot_gone_left(foot)
          else:
            at transform_foot_defeated_left(foot)
  add 'minigames simple_stepping foreground' #hides center foot
  ## UI ##
  ##Bubbles##
  for foot in simple_stepping_minigame.feet:
    if foot.state == "active":
      if foot.end_pos[0] > 1200:#right
        fixed:
          xmaximum 450
          ymaximum 350
          xpos 1393
          ypos 218
          if foot.satisfaction>=(foot.max_satisfaction-((foot.max_satisfaction/100)*20)):
            add "minigames simple_stepping balloon1_blush":
              zoom 0.5
          else:
            add "minigames simple_stepping balloon1":
              zoom 0.5
          if foot.request:
            if foot.request =="lick":
              add "minigames simple_stepping balloon_lick" xpos 156 ypos 78
            elif foot.request =="kiss":
              add "minigames simple_stepping balloon_kiss" xpos 156 ypos 75
            elif foot.request =="bite":
              add "minigames simple_stepping balloon_bite" xpos 158 ypos 74
            at transform_bubble_appear(foot)
          else:
            at transform_bubble_disappear(foot)
      elif foot.end_pos[0] > 800:#center
        fixed:
          xmaximum 450
          ymaximum 350
          xpos 1075
          ypos 67
          if foot.satisfaction>=(foot.max_satisfaction-((foot.max_satisfaction/100)*20)):
            add "minigames simple_stepping balloon2_blush":
              zoom 0.5
          else:
            add "minigames simple_stepping balloon2":
              zoom 0.5
          if foot.request:
            if foot.request =="lick":
              add "minigames simple_stepping balloon_lick" xpos 156 ypos 78
            elif foot.request =="kiss":
              add "minigames simple_stepping balloon_kiss" xpos 156 ypos 75
            elif foot.request =="bite":
              add "minigames simple_stepping balloon_bite" xpos 158 ypos 74
            at transform_bubble_appear(foot)
          else:
            at transform_bubble_disappear(foot)
      else:#left
        fixed:
          xmaximum 450
          ymaximum 350
          xpos 55
          ypos 218
          if foot.satisfaction>=(foot.max_satisfaction-((foot.max_satisfaction/100)*20)):
            add "minigames simple_stepping balloon2_blush":
              zoom 0.5
          else:
            add "minigames simple_stepping balloon2":
              zoom 0.5
          if foot.request:
            if foot.request =="lick":
              add "minigames simple_stepping balloon_lick" xpos 156 ypos 78
            elif foot.request =="kiss":
              add "minigames simple_stepping balloon_kiss" xpos 156 ypos 75
            elif foot.request =="bite":
              add "minigames simple_stepping balloon_bite" xpos 158 ypos 74
            at transform_bubble_appear(foot)
          else:
            at transform_bubble_disappear(foot)

  ##Buttons##
  fixed:
    imagebutton:
      xpos 755 ypos 894
      idle LiveComposite((100,106),(0,0),"minigames simple_stepping button_idle",(0,0),"minigames simple_stepping button_kiss")
      hover LiveComposite((100,106),(0,0),ElementDisplayable("minigames simple_stepping button_idle","hover"),(0,0),ElementDisplayable("minigames simple_stepping button_kiss","hover"))
      hovered Show("hint_frame",content="Kiss",place=(954,824,0.5,0)), Function(simple_stepping_minigame.focus_action,"kiss")
      unhovered Hide("hint_frame"), Function(simple_stepping_minigame.focus_action,"")
      selected_idle LiveComposite((100,106),(0,0),"minigames simple_stepping button_selected",(0,0),"minigames simple_stepping button_kiss")
      selected_hover LiveComposite((100,106),(0,0),ElementDisplayable("minigames simple_stepping button_selected","hover"),(0,0),ElementDisplayable("minigames simple_stepping button_kiss","hover"))
      selected (simple_stepping_minigame.current_action == "kiss")
      activate_sound "interact_popup" + str(random.randint(1,6))
      if simple_stepping_minigame.default_action == "kiss":
        default_focus True
      action [Function(simple_stepping_minigame.set_action,"kiss"), Function(cursor.click)]
    imagebutton:
      xpos 904 ypos 924
      idle LiveComposite((100,106),(0,0),"minigames simple_stepping button_idle",(0,0),"minigames simple_stepping button_lick")
      hover LiveComposite((100,106),(0,0),ElementDisplayable("minigames simple_stepping button_idle","hover"),(0,0),ElementDisplayable("minigames simple_stepping button_lick","hover"))
      hovered Show("hint_frame",content="Lick",place=(954,824,0.5,0)), Function(simple_stepping_minigame.focus_action,"lick")
      unhovered Hide("hint_frame"), Function(simple_stepping_minigame.focus_action,"")
      selected_idle LiveComposite((100,106),(0,0),"minigames simple_stepping button_selected",(0,0),"minigames simple_stepping button_lick")
      selected_hover LiveComposite((100,106),(0,0),ElementDisplayable("minigames simple_stepping button_selected","hover"),(0,0),ElementDisplayable("minigames simple_stepping button_lick","hover"))
      selected (simple_stepping_minigame.current_action == "lick")
      activate_sound "interact_popup" + str(random.randint(1,6))
      if simple_stepping_minigame.default_action == "lick":
        default_focus True
      action [Function(simple_stepping_minigame.set_action,"lick"), Function(cursor.click)]
    imagebutton:
      xpos 1053 ypos 894
      idle LiveComposite((100,106),(0,0),"minigames simple_stepping button_idle",(0,0),"minigames simple_stepping button_bite")
      hover LiveComposite((100,106),(0,0),ElementDisplayable("minigames simple_stepping button_idle","hover"),(0,0),ElementDisplayable("minigames simple_stepping button_bite","hover"))
      hovered Show("hint_frame",content="Bite",place=(954,824,0.5,0)), Function(simple_stepping_minigame.focus_action,"bite")
      unhovered Hide("hint_frame"), Function(simple_stepping_minigame.focus_action,"")
      selected_idle LiveComposite((100,106),(0,0),"minigames simple_stepping button_selected",(0,0),"minigames simple_stepping button_bite")
      selected_hover LiveComposite((100,106),(0,0),ElementDisplayable("minigames simple_stepping button_selected","hover"),(0,0),ElementDisplayable("minigames simple_stepping button_bite","hover"))
      selected (simple_stepping_minigame.current_action == "bite")
      activate_sound "interact_popup" + str(random.randint(1,6))
      if simple_stepping_minigame.default_action == "bite":
        default_focus True
      action [Function(simple_stepping_minigame.set_action,"bite"), Function(cursor.click)]

  ##Progress bars##
  #mostly here for testing, they don't have to stay
  for foot in simple_stepping_minigame.feet:
    if foot.state == 'active':
      if foot.end_pos[0] > 1200:#right
        bar:
          xpos 1161 xsize 200 ypos 752 ysize 20
          left_bar Frame("minigames simple_stepping bar_full",10,10,10,10) right_bar Frame("minigames simple_stepping bar_empty",10,10,10,10)
          value AnimatedValue(foot.satisfaction,foot.max_satisfaction)
      elif foot.end_pos[0] > 800:#center
        bar:
          xpos 815 xsize 200 ypos 363 ysize 20
          left_bar Frame("minigames simple_stepping bar_full",10,10,10,10) right_bar Frame("minigames simple_stepping bar_empty",10,10,10,10)
          value AnimatedValue(foot.satisfaction,foot.max_satisfaction)
      else:#left
        bar:
          xpos 543 xsize 200 ypos 748 ysize 20
          left_bar Frame("minigames simple_stepping bar_full",10,10,10,10) right_bar Frame("minigames simple_stepping bar_empty",10,10,10,10)
          value AnimatedValue(foot.satisfaction,foot.max_satisfaction)

  ## Hearts and Crosses ##
  for hit in simple_stepping_minigame.hits:#show all hitmarkers triggered
    add "simple_stepping_heart":#resets each turn
      pos hit
      anchor (0.5,0.5)
  for miss in simple_stepping_minigame.misses:
    add "simple_stepping_cross":#resets each turn
      pos miss
      anchor (0.5,0.5)
  add cursor #cursor for hearts and crosses.
  timer simple_stepping_minigame.speed+max((9-simple_stepping_minigame.round),0)*simple_stepping_minigame.handy action Return('')

screen simple_stepping_instructions(message):
  modal True
  zorder 200
  style_prefix "stepping_instructions"
  add "#0008"
  frame style "notification_modal_title_frame" yalign 0.225:
    text "Warning" style "notification_modal_title"
  frame:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Start" action Return('')
  key "game_menu" action NullAction()
