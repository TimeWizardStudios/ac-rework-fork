#cleaning minigame by tama
init -100 python:
#############################################################
########### State class ############################################
#############################################################
  class NetDisplay(renpy.Displayable):
    def __init__(self, *args, **kwargs):
      renpy.Displayable.__init__(self, *args, **kwargs)
      self.child = None
      self.child_size = (1, 1)
      self.kwargs = kwargs
      self.args = args
      self.image = "minigames cleaning net_background"
      self.icon_align = (0,0)
      self.position = (840/self.child_size[0],915/self.child_size[1])
    def render(self, w, h, st, at):
      if (cleaning_minigame.drags):
        self.position = ((cleaning_minigame.drags[0].x+420) / self.child_size[0], (cleaning_minigame.drags[0].y+910) / self.child_size[1])
      else:
        self.position = (840/self.child_size[0],915/self.child_size[1])#Default position if drags has not been made yet
      fixed = Fixed(
            Transform(
              self.image,
              align=self.icon_align,
              pos=self.position,
            )
      )
      self.child = fixed
      child_render = renpy.render(self.child, w, h, st, at)
      self.child_size = child_render.get_size()
      renpy.redraw(self, 0)
      return child_render

  class CleaningState(BaseClass):
    default_trash = ['plastic_bag','beaver']
    def __init__(self,trashpile,speed=3.0,max_rounds=16,max_score=10,max_simultaneous=1,**kwargs):
      self.lane_positions = [(900,250)]#[(660,450),(935,450),(1210,450)] #Where trash starts its turn
      self.end_positions =  [(570-80,850),(935-75,850),(1300-100,850)]  #Where it ends up
      self.capture_zones = [(0,300),(310,690),(700,1200)] ##List of x boundaries within which the net must be to catch an object?
      self.snap_zones = [40,420,750]##quick snaps for net
      self.drags = [] ##Keeps track of draggable objects
      self.trash = []
      self.score = 0
      self.round = 0
      self.oopsnt = False
      self.oops = False
      self.speed = speed
      self.state = "dirty"
      self.previous_state = "dirty"
      self.max_rounds=max_rounds
      self.max_score=max_score
      self.max_simultaneous = max_simultaneous
      self.trashpile = default_trash if trashpile is None else trashpile
      for garbo in self.trashpile:
        self.trash.append(trash_by_id[garbo]())

    def round_advance(self):
      self.round+=1
      self.moving = 0
      self.drags[0].at = 0
      if [trash for trash in cleaning_minigame.trash if trash.state != "finished"]:
        self.oopsnt = False
        self.oops = False
      if self.state == "dirty":
        if self.score > 3:
          self.state = "dirtyish"
          self.previous_state = "dirty"
        else:
          self.previous_state = "dirty"
      elif self.state == "dirtyish":
        if 4 > self.score:
          self.state = "dirty"
          self.previous_state = "dirtyish"
        elif 7 > self.score > 3:
          self.previous_state = "dirtyish"
        elif self.score > 6:
          self.state = "cleanish"
          self.previous_state = "dirtyish"
      elif self.state == "cleanish":
        if 7 > self.score:
          self.state = "dirtyish"
          self.previous_state = "cleanish"
        if 10 > self.score > 6:
          self.previous_state = "cleanish"
        elif self.score > 9:
          self.state = "clean"
          self.previous_state = "cleanish"
      elif self.state == "clean":
        if self.score > 9:
          self.previous_state = "clean"
        else:
          self.state = "cleanish"
          self.previous_state = "clean"
      for item in self.trash:
        if item.state == "waiting":
          if (self.moving < self.max_simultaneous):
            self.moving+=1
            item.state = "starting"
            item.previous_state = "waiting"
            item.start_lane = 0#random.randint(0,2)
            item.end_pos = self.lane_positions[item.start_lane]
          else:#ignore waiting if enough have been moved already
            pass
        elif item.state == "starting":
          item.state = "flowing"
          item.previous_state = "starting"
          item.end_lane = random.randint(0,2)
          item.end_pos = self.end_positions[item.end_lane]
        elif item.state == "flowing":
          item.state = "finished"
          item.previous_state = "flowing"
#          print self.drags[0].x
          if self.drags[0].x>=self.capture_zones[item.end_lane][0] and self.drags[0].x<=self.capture_zones[item.end_lane][1]:
            item.captured = True
            if item.type == "trash":
              self.score+=2
              self.oopsnt = True
              if 7 > self.score > 3:
                self.state = "dirtyish"
              elif 10 > self.score > 6:
                self.state = "cleanish"
              elif self.score > 9:
                self.state = "clean"
            else:
              self.score-=3
              self.oops = True
              if 10 > self.score > 6:
                self.state = "cleanish"
              elif 7 > self.score > 3:
                self.state = "dirtyish"
              if 4 > self.score:
                self.state = "dirty"
          else:#missed
            item.end_pos = (item.pos[0],1100)
        elif item.state == "finished":
          item.previous_state = "finished"
        if ([trash for trash in cleaning_minigame.trash if trash.state != "waiting"]
        and [trash for trash in cleaning_minigame.trash if trash.state != "finished"]):
          self.speed = renpy.random.choice((2.5,3.0,3.5,4.0))
    def item_dragged(self,draggable,drop):
      if not drop:#snap to nearest location if dropped on nothing
        if draggable[0].x<self.capture_zones[0][1]:
          snap_x = self.snap_zones[0]
        elif draggable[0].x>self.capture_zones[2][0]:
          snap_x = self.snap_zones[2]
        else:
          snap_x = self.snap_zones[1]
        snap_y = draggable[0].y
        draggable[0].snap(snap_x,snap_y,0.05)#xy relative to draggroup container
        return
      self.drags = []
      return True
    def left_arrow(self):
      if self.drags[0].x > self.capture_zones[2][0]:
        self.drags[0].snap(self.snap_zones[1],self.drags[0].y,0.05)
      elif self.drags[0].x > 0:
        self.drags[0].snap(self.snap_zones[0],self.drags[0].y,0.05)
      else:
        pass
    def right_arrow(self):
      if self.drags[0].x < self.snap_zones[1]:
        self.drags[0].snap(self.snap_zones[1],self.drags[0].y,0.05)
      elif self.drags[0].x < self.snap_zones[2]:
        self.drags[0].snap(self.snap_zones[2],self.drags[0].y,0.05)
      else:
        pass
#############################################################
########## Objects  ############################################
#############################################################
  trash_by_id = {}
  class BaseTrashMeta(type):
    def __init__(cls,name,bases,dct):
      super(BaseTrashMeta,cls).__init__(name,bases,dct)
      if cls.id is not None:
        trash_by_id[cls.id]=cls

  class BaseTrash(BaseClass):
    __metaclass__=BaseTrashMeta
    id = None
    start_pos = [875,-250]
    def __init__(self,*args,**kwargs):
      super(BaseTrash,self).__init__(*args,**kwargs)
    def update(self,tf,st,at):
      self.pos = (tf.xoffset,tf.yoffset)
  class Tincan(BaseTrash):
    id = 'tin_can'
    object_image = "minigames cleaning garbo6"
    type="trash"
    # start_pos = [875,-250]
    def __init__(self,*args,**kwargs):
      self.state = "waiting" ##States: waiting, starting, flowing, finished
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False
  class PlasticBag(BaseTrash):
    id = 'plastic_bag'
    object_image = "minigames cleaning garbo2"
    type="trash"
    # start_pos = [775,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False
  class Hamburger(BaseTrash):
    id = 'hamburger'
    object_image = "minigames cleaning garbo1"
    type="trash"
    # start_pos = [875,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False
  class Bottle(BaseTrash):
    id = 'bottle'
    object_image = "minigames cleaning garbo3"
    type="trash"
    # start_pos = [875,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False
  class Bottle2(BaseTrash):
    id = 'bottle2'
    object_image = "minigames cleaning garbo5"
    type="trash"
    # start_pos = [875,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False

  class Cup(BaseTrash):
    id = 'cup'
    object_image = "minigames cleaning garbo4"
    type="trash"
    # start_pos = [875,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False

  class Beaver(BaseTrash):
    id = 'beaver'
    object_image = "minigames cleaning adv1"
    type="adversary"
    # start_pos = [975,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False
  class Squid(BaseTrash):
    id = 'squid'
    object_image = "minigames cleaning adv2"
    type="adversary"
    # start_pos = [950,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False
  class Frog(BaseTrash):
    id = 'frog'
    object_image = "minigames cleaning adv3"
    type="adversary"
    # start_pos = [960,50]
    def __init__(self,*args,**kwargs):
      self.state = "waiting"
      self.previous_state = "waiting"
      self.pos = self.start_pos
      self.end_pos = [0,0]
      self.captured = False


#############################################################
##Styles#######################################################
#############################################################
style cleaning_button_net:
  yalign 0.5
  background 'minigames cleaning net_foreground'
  # insensitive_background 'minigames cleaning net_insensitive'
  # hover_background 'minigames cleaning net_hover'
  # selected_idle_background 'minigames cleaning net_idle'
  # selected_hover_background 'minigames cleaning net_hover'
  # selected_insensitive_background 'minigames cleaning net_insensitive'

##############################################################
#####labels #####################################################
##############################################################
label start_cleaning_minigame(winlabel='cleaning_win',loselabel='cleaning_lose',difficulty='medium',headstart=False,skip_instructions=True if quest.flora_walk["cleaning_minigame_failed"] else False):
  if not skip_instructions:
    call screen cleaning_instructions("Click on the arrows or\ndrag and drop/hold the net\nto position it and catch as\nmuch garbage as you can,\nwhile avoiding the wildlife!")
  python:
    trash = ['frog','bottle','cup','beaver','tin_can','hamburger','plastic_bag','squid','tin_can','bottle2','plastic_bag']
    cleaning_minigame = CleaningState(trashpile=trash)
  $_rollback = False
  $game.ui.hide_hud=1
  hide screen interface_hider
  show screen kitchen_minigame_skip
  call cleaning_loop from _call_cleaning_loop
  $_rollback = True
  $game.ui.hide_hud=0
# show screen interface_hider
# hide screen kitchen_minigame_skip
# if _return == 'lose':
#   call expression loselabel from _call_expression_vine_attack_minigame_lose
# else:
#   call expression winlabel from _call_expression_vine_attack_minigame_win
  return

label cleaning_loop:#main gameplay loop
  while (cleaning_minigame.round<cleaning_minigame.max_rounds):
    if cleaning_minigame.round < 14:
      call screen cleaning_turn(cleaning_minigame.drags)
    if isinstance(_return,list):
      $cleaning_minigame.drags = _return
    $cleaning_minigame.round_advance()
# if cleaning_minigame.state != "clean":
#   return "lose"
# else:
#   return "win"
  return

# label cleaning_win:
# show minigames cleaning background_clean as minigame_cleaning_background
# show flora excited at appear_from_right(.5)
# flora excited "I did it!"
# mc "You mean I did it?"
# show flora skeptical at move_to(.75)
# show isabelle neutral flip at appear_from_left(.25)
# isabelle neutral flip "It was a group effort."
# hide flora with Dissolve(.5)
# hide isabelle with Dissolve(.5)
# hide minigame_cleaning_background
# return

# label cleaning_lose:
# "you lose"
# show minigames cleaning background as minigame_cleaning_background_dirty
# show flora neutral at appear_from_right(.5)
# flora neutral "[mc], get your head in the game!"
# flora angry "Think of the animals!"
# hide flora with Dissolve(.5)
# hide minigame_cleaning_background_dirty
# return

###############################################
###############Transforms#########################
###############################################
##---------Trash transforms##
transform transform_item_waiting(item):
  xoffset item.start_pos[0]
  yoffset item.start_pos[1]
  zoom 0.2

transform transform_item_starting(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.2
  parallel:
    easeout_circ cleaning_minigame.speed/2 zoom 0.35
  parallel:
    easein_quint cleaning_minigame.speed/2 xoffset item.end_pos[0]
  parallel:
    linear cleaning_minigame.speed yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_item_flowing(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.35
  parallel:
    easein_quad cleaning_minigame.speed zoom 0.65
  parallel:
    easeout_back 1.5 xoffset item.end_pos[0]
  parallel:
    easein_quad cleaning_minigame.speed yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_item_flowing_towards_left_lane(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.35
  parallel:
    easein_quad cleaning_minigame.speed zoom 0.65
  parallel:
    easeout_cubic 0.9 xoffset item.end_pos[0]+110
    linear cleaning_minigame.speed-0.9 xoffset item.end_pos[0]
  parallel:
    easein_quad cleaning_minigame.speed yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_item_finish_catch(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  rotate_pad True
  parallel:
    easein_expo cleaning_minigame.speed/4 xoffset cleaning_minigame.drags[0].x+420#move to relative location of draggable
  parallel:
    easeout_back cleaning_minigame.speed/4 yoffset cleaning_minigame.drags[0].y+910
  parallel:
    linear cleaning_minigame.speed/4 rotate 720
  parallel:
    linear cleaning_minigame.speed/4 zoom 0.35
  parallel:
    pause cleaning_minigame.speed/6
    linear cleaning_minigame.speed/8 alpha 0.0
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_item_finish_catch_back(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  rotate_pad True
  parallel:
    easein_expo cleaning_minigame.speed/4 xoffset cleaning_minigame.drags[0].x+420#move to relative location of draggable
  parallel:
    easeout_back cleaning_minigame.speed/4 yoffset cleaning_minigame.drags[0].y+910
  parallel:
    linear cleaning_minigame.speed/4 rotate -720
  parallel:
    linear cleaning_minigame.speed/4 zoom 0.35
  parallel:
    pause cleaning_minigame.speed/6
    linear cleaning_minigame.speed/8 alpha 0.0
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_item_finish_miss(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.65
  parallel:
    linear cleaning_minigame.speed/4 zoom 0.75
  parallel:
    linear cleaning_minigame.speed/4 xoffset item.end_pos[0]
  parallel:
    linear cleaning_minigame.speed/4 yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat
##----------Adversary transforms----------##

transform transform_adv_waiting(item):
  xoffset item.start_pos[0]
  yoffset item.start_pos[1]
  zoom 0.05

transform transform_adv_starting(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.05
  parallel:
    easeout_circ cleaning_minigame.speed/2 zoom 0.15
  parallel:
    easein_quint cleaning_minigame.speed/2 xoffset item.end_pos[0]
  parallel:
    linear cleaning_minigame.speed yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_adv_flowing(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.15
  parallel:
    easeout_quad cleaning_minigame.speed zoom 0.25
  parallel:
    easeout_back 1.5 xoffset item.end_pos[0]
  parallel:
    easein_quad cleaning_minigame.speed yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_adv_flowing_towards_left_lane(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.15
  parallel:
    easeout_quad cleaning_minigame.speed zoom 0.25
  parallel:
    easeout_cubic 0.9 xoffset item.end_pos[0]+110
    linear cleaning_minigame.speed-0.9 xoffset item.end_pos[0]
  parallel:
    easein_quad cleaning_minigame.speed yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_adv_finish_catch(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  rotate_pad True
  parallel:
    easein_expo cleaning_minigame.speed/4 xoffset cleaning_minigame.drags[0].x+420#move to relative location of draggable
  parallel:
    easeout_back cleaning_minigame.speed/4 yoffset cleaning_minigame.drags[0].y+910
  parallel:
    linear cleaning_minigame.speed/4 rotate 720
  parallel:
    linear cleaning_minigame.speed/4 zoom 0.05
  parallel:
    pause cleaning_minigame.speed/6
    linear cleaning_minigame.speed/8 alpha 0.0
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_adv_finish_catch_back(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  rotate_pad True
  parallel:
    easein_expo cleaning_minigame.speed/4 xoffset cleaning_minigame.drags[0].x+420#move to relative location of draggable
  parallel:
    easeout_back cleaning_minigame.speed/4 yoffset cleaning_minigame.drags[0].y+910
  parallel:
    linear cleaning_minigame.speed/4 rotate -720
  parallel:
    linear cleaning_minigame.speed/4 zoom 0.05
  parallel:
    pause cleaning_minigame.speed/6
    linear cleaning_minigame.speed/8 alpha 0.0
  parallel:
    block:
      function item.update
      0.01
      repeat

transform transform_adv_finish_miss(item):
  xoffset item.pos[0]
  yoffset item.pos[1]
  zoom 0.25
  parallel:
    linear cleaning_minigame.speed/4 zoom 0.3
  parallel:
    linear cleaning_minigame.speed/4 xoffset item.end_pos[0]
  parallel:
    linear cleaning_minigame.speed/4 yoffset item.end_pos[1]
  parallel:
    block:
      function item.update
      0.01
      repeat
#---------------------------------
transform transform_dirty_from_clean:
  alpha 1.0
  linear 1.0 alpha 0.0

transform transform_clean_from_dirty:
  alpha 0.0
  linear 1.0 alpha 1.0

# transform transform_angry_x:
# xoffset 1560
# yoffset 110
# zoom 0.8


###############################################
###############Screens###########################
###############################################

screen cleaning_turn(drags):
  default cursor = NetDisplay()

  if cleaning_minigame.state == "dirty":#dirty river
    if cleaning_minigame.previous_state == "dirty":
      add "minigames cleaning background_dirty"
    else:
      add "minigames cleaning background_dirty"
      add "minigames cleaning background_dirtyish":
        at transform_dirty_from_clean
  elif cleaning_minigame.state == "dirtyish":
    if cleaning_minigame.previous_state == "dirty":
      add "minigames cleaning background_dirtyish"
      add "minigames cleaning background_dirty":
        at transform_dirty_from_clean
    elif cleaning_minigame.previous_state == "dirtyish":
      add "minigames cleaning background_dirtyish"
    elif cleaning_minigame.previous_state == "cleanish":
      add "minigames cleaning background_dirtyish"
      add "minigames cleaning background_cleanish":
        at transform_dirty_from_clean
  elif cleaning_minigame.state == "cleanish":
    if cleaning_minigame.previous_state == "dirtyish":
      add "minigames cleaning background_cleanish"
      add "minigames cleaning background_dirtyish":
        at transform_dirty_from_clean
    elif cleaning_minigame.previous_state == "cleanish":
      add "minigames cleaning background_cleanish"
    elif cleaning_minigame.previous_state == "clean":
      add "minigames cleaning background_cleanish"
      add "minigames cleaning background_clean":
        at transform_dirty_from_clean
  else:#clean river
    if cleaning_minigame.previous_state == "clean":
      add "minigames cleaning background_clean"
    else:
      add "minigames cleaning background_cleanish"
      add "minigames cleaning background_clean":
        at transform_clean_from_dirty

  if quest.flora_walk["uno_reverse_card"]:
    add "minigames cleaning flora" xpos 1551 ypos 0
    if cleaning_minigame.oopsnt:
      add "minigames cleaning speech_bubble_bottom" xpos 1130 ypos 170
      add "minigames cleaning heart" xpos 1272 ypos 272
    elif cleaning_minigame.oops:
      add "minigames cleaning speech_bubble_bottom" xpos 1130 ypos 170
      add "minigames cleaning anger" xpos 1279 ypos 264
  else:
    add "minigames cleaning flora_xray" xpos 1111 ypos 200
    if cleaning_minigame.oopsnt:
      add "minigames cleaning face_laughing" xpos 1197 ypos 284
      add "minigames cleaning speech_bubble_top" xpos 1400 ypos 40
      add "minigames cleaning heart" xpos 1542 ypos 104
    elif cleaning_minigame.oops:#Speech bubble angry
      add "minigames cleaning face_angry" xpos 1196 ypos 289
      add "minigames cleaning speech_bubble_top" xpos 1400 ypos 40
      add "minigames cleaning anger" xpos 1549 ypos 96
    else:#regular expression
      add "minigames cleaning face_neutral" xpos 1197 ypos 287

  if quest.flora_walk["uno_reverse_card"]:
    add "minigames cleaning isabelle_xray_full" xpos 700 ypos 50
  else:
    add "minigames cleaning isabelle_xray" xpos 700 ypos 50
    if isabelle.equipped_item("isabelle_collar"):
      add "minigames cleaning collar" xpos 756 ypos 160

  add cursor

############Drag can go here if you want it to clip under trash instead of over
  # draggroup:#Container for draggable object
    # xpos 420 ypos 910
    # xsize 1150 ysize 230
    # if drags:#if draggroup already declared
      # for drag in drags:
        # add drag
    # else:
      # drag:
        # as draggable_net
        # drag_name "net"
        # droppable False #can a draggable be dropped onto this
        # draggable True #can be dragged
        # drag_raise True #To top when dragged
        ##activated #Callback for mousedown on the drag
        # dragged cleaning_minigame.item_dragged #Callback when a drag has been dropped
        ##dragging #callback for actively being dragged
        # xpos 357 ypos 5

        # frame style 'cleaning_button_net':
          # xysize(393,211)
##################################

  for item in cleaning_minigame.trash:
    if item.state == "waiting":
      add item.object_image:
        if item.type == 'trash':
          at transform_item_waiting(item)
        else:
          at transform_adv_waiting(item)
    elif item.state == "starting":
      add item.object_image:#move to start position
        if item.type == 'trash':
          at transform_item_starting(item)
        else:
          at transform_adv_starting(item)
    elif item.state == "flowing":
      add item.object_image:
        if item.type == 'trash' and item.end_lane:
          at transform_item_flowing(item)
        elif item.type == 'trash':
          at transform_item_flowing_towards_left_lane(item)
        elif item.end_lane:
          at transform_adv_flowing(item)
        else:
          at transform_adv_flowing_towards_left_lane(item)
    elif item.state == "finished":
      if item.previous_state == "flowing":
        if item.captured:
          #Fall into net
          if (item.pos[0] > cleaning_minigame.drags[0].x+420):
            add item.object_image:
              if item.type == 'trash':
                at transform_item_finish_catch_back(item)
              else:
                at transform_adv_finish_catch_back(item)
          else:
            add item.object_image:
              if item.type == 'trash':
                at transform_item_finish_catch(item)
              else:
                at transform_adv_finish_catch(item)
        else:#miss
          add item.object_image:
            if item.type == 'trash':
              at transform_item_finish_miss(item)
            else:
              at transform_adv_finish_miss(item)
      else:
        #transition for staying finished
        pass
  draggroup:#Container for draggable object
    xpos 420 ypos 910
    xsize 1150 ysize 230
    if drags:#if draggroup already declared
      for drag in drags:
        add drag
    else:
      drag:
        as draggable_net
        drag_name "net"
        droppable False #can a draggable be dropped onto this
        draggable True #can be dragged
        drag_raise True #To top when dragged
        #activated #Callback for mousedown on the drag
        dragged cleaning_minigame.item_dragged #Callback when a drag has been dropped
        #dragging #callback for actively being dragged
        xpos 420 ypos 5

        frame style 'cleaning_button_net':
          xysize(393,211)

  #UI
  # Maybe we don't have to explicitly show score and just let the context clue of the river being clean show the score is high enough
  # text "Score: " + str(cleaning_minigame.score) + "/" + str(cleaning_minigame.max_score) xalign 0.5 yalign 0.05
# text "Score: " + str(cleaning_minigame.score) + "/" + str(cleaning_minigame.max_score) + " (" + str(len([trash for trash in cleaning_minigame.trash if trash.state != "finished"])) + " left)" xalign 0.5 yalign 0.05
# text "Round: " + str(cleaning_minigame.round) xalign 0.5 yalign 0.1
# text "Speed: " + str(cleaning_minigame.speed) xalign 0.5 yalign 0.15
# text "State: " + str(cleaning_minigame.state) xalign 0.5 yalign 0.2
# text "Prev.: " + str(cleaning_minigame.previous_state) xalign 0.5 yalign 0.25
# text "Diff?: " + ("False" if cleaning_minigame.state == cleaning_minigame.previous_state else "True") xalign 0.5 yalign 0.3

  imagebutton:
    xoffset 450
    yoffset 800
    focus_mask 'minigames cleaning left_arrow'
    idle 'minigames cleaning left_arrow'
    hover ElementDisplayable("minigames cleaning left_arrow","hover")
    keyboard_focus False
    #activate_sound = ""
    if drags:#workaround to prevent crash from moving using arrow during first round
      action Function(cleaning_minigame.left_arrow)
  imagebutton:
    xoffset 1400
    yoffset 800
    focus_mask 'minigames cleaning right_arrow'
    idle 'minigames cleaning right_arrow'
    hover ElementDisplayable("minigames cleaning right_arrow","hover")
    keyboard_focus False
    #activate_sound = ""
    if drags:
      action Function(cleaning_minigame.right_arrow)
  if drags:
    if ([trash for trash in cleaning_minigame.trash if trash.state != "waiting"]
    and [trash for trash in cleaning_minigame.trash if trash.state != "finished"]):
      timer cleaning_minigame.speed action Return('')
    elif cleaning_minigame.state != cleaning_minigame.previous_state:
      timer 1.0 action Return('')
    elif not [trash for trash in cleaning_minigame.trash if trash.state != "finished"]:
      timer cleaning_minigame.speed/4 action Return('')
    else:
      timer 0.01 action Return('')
  else:
    if ([trash for trash in cleaning_minigame.trash if trash.state != "waiting"]
    and [trash for trash in cleaning_minigame.trash if trash.state != "finished"]):
      timer cleaning_minigame.speed action Return([draggable_net])
    elif cleaning_minigame.state != cleaning_minigame.previous_state:
      timer 1.0 action Return([draggable_net])
    elif not [trash for trash in cleaning_minigame.trash if trash.state != "finished"]:
      timer cleaning_minigame.speed/4 action Return([draggable_net])
    else:
      timer 0.01 action Return([draggable_net])
  key "game_menu" action NullAction()
  key "hide_windows" action NullAction()

screen cleaning_instructions(message):
  modal True
  zorder 200
  style_prefix "stepping_instructions"
  add "#0008"
  frame style "notification_modal_title_frame" yalign 0.225:
    text "Warning" style "notification_modal_title"
  frame yoffset 64:
    vbox:
      spacing 32
      text message
      hbox:
        xalign 0.5
        spacing 100
        textbutton "Start" action Return('')
  key "game_menu" action NullAction()
  key "hide_windows" action NullAction()
