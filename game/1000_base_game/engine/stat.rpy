init -100 python:
  stats_by_id={}

  class StatMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[5:] if name.lower().startswith("stat_") else name
      if not dct.get("do_not_register",False):
        stats_by_id[cls.id]=cls
        for alt_id in cls.alt_id:
          stats_by_id[alt_id]=cls
      for k,v in dct.items():
        if callable(v):
          force_classmethod(cls,k)
    def __eq__(cls,other):
      return cls is other or cls.id==other or other in cls.alt_id
    def __ne__(cls,other):
      return not cls.__eq__(other)
    def __str__(cls):
      return str(cls.title)

  class BaseStat(BaseClass):
    __metaclass__=StatMeta
    do_not_register=True
    id=None
    alt_id=[]
    title="- generic stat -"
    color="#0F0"
    icon=None
    min_level=1
    max_level=5
    notify_xp_granted=True
    notify_level_changed=True
    hidden=False
    xp_to_next_level=[
      1000, # 1->2
      2000, # 2->3
      3000, # 3->4
      4000, # 4->5
      9999, # 5, needed to allow losing xp and down-leveling
      ]
