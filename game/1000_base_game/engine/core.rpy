label main_loop_process_events:
  while len(game.events_queue)>0:
    python:
      event=game.events_queue.pop(0)
      if isinstance(event,basestring):
        if renpy.has_label(event):
          event_label=event
          event_args=[]
        else:
          event=game_events_by_id[event]
          event_label=event.default_label
          event_args=event.default_args
      else:
        event_label=event[1]
        event_args=event[2:]
    call expression event_label pass (*event_args) from _call_expression_7
  return

label main_loop:
  $exit_main_loop=None
  while exit_main_loop is None:
    call main_loop_process_events from _call_main_loop_process_events
    if game.location.main_loop_label is not None:
      call expression game.location.main_loop_label from _call_expression_8
    if len(game.events_queue)==0 and _return is None:
      if game.pc.main_loop_label is not None:
        call expression game.pc.main_loop_label from _call_expression_9
      if len(game.events_queue)==0 and _return is None:
        if game.activity_selector is not None:
          call expression game.activity_selector from _call_expression_10
          if isinstance(_return,basestring):
            call expression _return from _call_expression_11
          elif isinstance(_return,(list,tuple)):
            call expression _return[0] pass (*_return[1]) from _call_expression_12
  jump expression exit_main_loop
