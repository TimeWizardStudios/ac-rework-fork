init python:
  def print_game_debug_info():
    print("## PERSISTENT VARIABLES ##".ljust(79,"#"))
#    print(persistent.__dict__.keys())
    for k,v in sorted(persistent.__dict__.items()):
      if not k.startswith("_"):
        print(k,"=",v)
    print("## CHARACTERS ##".ljust(79,"#"))
    for k,v in sorted(characters_by_id.items()):
      print(k)
    print("## CHARACTER STATS ##".ljust(79,"#"))
    for k,v in sorted(stats_by_id.items()):
      print(k)
    print("## LOCATIONS ##".ljust(79,"#"))
    for k,v in sorted(locations_by_id.items()):
      print(k)
    print("## LOCATION INTERACTABLES ##".ljust(79,"#"))
    for k,v in sorted(interactables_by_id.items()):
      print(k)
    print("## ITEMS ##".ljust(79,"#"))
    for k,v in sorted(items_by_id.items()):
      print(k)
    print("## COMBINE RECIPES ##".ljust(79,"#"))
    for k,v in sorted(recipes_by_ingredients.items()):
      print(k,"->",v)
    print("## QUESTS ##".ljust(79,"#"))
    for k,v in sorted(quests_by_id.items()):
      print(k)
    print("## ACHIEVEMENTS ##".ljust(79,"#"))
    for k,v in sorted(achievements_by_id.items()):
      print(k)
    print("## PHONE APPS ##".ljust(79,"#"))
    for k,v in sorted(phone_apps_by_id.items()):
      print(k)
    print("## GAME EVENTS ##".ljust(79,"#"))
    for k,v in sorted(game_events_by_id.items()):
      print(k)
    print("## GAME EVENT HANDLERS ##".ljust(79,"#"))
    for k,v in sorted(game_events_by_handler_id.items()):
      print(k)
    print("####".ljust(79,"#"))
