﻿init python:
  class HookedImages(dict):
    def get(self,key,default=None):
      rv=super(HookedImages,self).get(key,default)
      if rv is None:
        key=" ".join(key)
        if key.endswith("_small"):
          key=tuple(key[:-6].split(" "))
          rv=super(HookedImages,self).get(key)
          if rv is not None:
            rv=Transform(rv,zoom=0.5)
        elif key.endswith("_large"):
          key=tuple(key[:-6].split(" "))
          rv=super(HookedImages,self).get(key)
          if rv is not None:
            rv=Transform(rv,zoom=2.0)
      return rv

  def setup_images_hook():
    new_images=HookedImages()
    new_images.update(renpy.display.image.images)
    renpy.display.image.images=new_images

  setup_images_hook()

  
image white = Solid('#ffffff')