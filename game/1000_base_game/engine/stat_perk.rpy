init -100 python:
  stat_perks_by_id={}
  stat_perks_by_stat_id={}

  class StatPerkMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[9:] if name.lower().startswith("statperk_") else name
      if not dct.get("do_not_register",False):
        stat_perks_by_id[cls.id]=cls
        if cls.parent_stat:
          if cls.parent_stat not in stat_perks_by_stat_id:
            stat_perks_by_stat_id[cls.parent_stat]=[]
          stat_perks_by_stat_id[cls.parent_stat].append(cls.id)

  class StatPerk(BaseClass):
    __metaclass__=StatPerkMeta
    do_not_register=True
    id=None
    parent_stat=None
    title="- stat perk -"
    description=" - stat perk -"
    hidden=False

  def unlock_stat_perk(perk_id,silent=False):
    if isinstance(perk_id,basestring):
      perk=stat_perks_by_id[perk_id]
    if perk.id not in game.unlocked_stat_perks:
      game.unlocked_stat_perks.add(perk.id)
      process_event("stat_perk_unlocked",perk,silent=silent)
