init python:
  def time_str(hours,minutes):
    if persistent.time_format=="24h":
      return "{:02}:{:02}".format(hours,minutes)
    else:
      return "{:02}:{:02} {}".format((hours+11)%12+1,minutes,("AM" if hours<12 else "PM"))
