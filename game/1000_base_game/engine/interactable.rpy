init -100 python:
  interactables_by_id={}

  class InteractableMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[13:] if name.lower().startswith("interactable_") else name
      if not dct.get("do_not_register",False):
        interactables_by_id[cls.id]=cls
      for k,v in dct.items():
        if callable(v):
          force_classmethod(cls,k)

  class Interactable(BaseClass):
    __metaclass__=InteractableMeta
    do_not_register=True
    id=None
    investigate_id=None
    def title(cls):
      return ""
    def description(cls):
      return ""
    def circle(cls,actions,x,y):
      rv=BaseClass()
      edge_threshold=230
      sector="left" if x<edge_threshold else "right" if x>config.screen_width-edge_threshold else "mid"
#      print(sector)
      sector+="_"+("top" if y<edge_threshold else "bottom" if y>config.screen_height-edge_threshold else "mid")
#      print(sector)

      rv.center=(0,50)
      rv.radius=200
      rv.start_angle=340
      rv.angle_per_icon= 40

      if len(actions) == 1:
        
        rv.start_angle-=((len(actions)-2)*rv.angle_per_icon)//2
        if sector=="left_top" or sector=="mid_top" or sector=="right_top":
          rv.center=(0,320)
      
      elif len(actions) == 2:

        if sector=="left_top" or sector=="mid_top" or sector=="right_top":
          rv.center=(0,310)

      elif len(actions) == 3:

        rv.start_angle-=((len(actions)-2)*rv.angle_per_icon)//2
        rv.center=(0,0)

        if sector=="left_top" or sector=="mid_top" or sector=="right_top":
          rv.center=(0,310)

      elif len(actions) >= 4:

        rv.start_angle-=((len(actions)-2)*rv.angle_per_icon)//2
        rv.center=(0,-20)

        if sector=="left_top" or sector=="mid_top" or sector=="right_top":
          rv.center=(0,310)

      return rv

    def actions(cls,actions):
      ## action_icon,action_hint,action_label,optional action_label arguments
      ## if action label starts with "!" first arg will be clicked element info
      ## if action label starts with "?" then dull seen action mechanics will be used
      pass
    def finalize_actions(cls,actions):
      ## can add actions what should be present in almost every interactable
      description=cls.description()
      if description:
        investigate_id=cls.investigate_id or cls.id
        actions.insert(0,["investigate","Investigate","?!investigate",investigate_id,description])
      ## if more than one of same type actions present, select only one
      actions=prepare_interactable_actions(cls,actions)
      return actions

  def prepare_interactable_actions(interactable,actions):
    order=[]
    by_type={}
    for action in actions:
      if isinstance(action,basestring):
        action=["interact","Interact",action]
      action_type=action[0]
      if isinstance(action_type,basestring):
        action_energy=default_energy_costs.get(action_type,default_energy_cost)
      else:
        action_energy=action_type[1]
        action_type=action_type[0]
      action_hint=action[1]
      action_args=tuple(action[3:])
      action=action[2]
      if action_type not in by_type:
        if action_type.endswith("_default"):
          action_type_id=action_type[:-8]
        else:
          action_type_id=action_type
        if action_type_id not in order:
          order.append(action_type_id)
        by_type[action_type]=[]
      by_type[action_type].append([action_energy,action_hint,action,action_args])
    rv={}
    for action_type in order:
      actions=by_type.get(action_type)
      if not actions:
        actions=by_type.get(action_type+"_default")
      rv_action=None
      if actions:
        ## find unseen action
        for action_energy,action_hint,action,action_args in actions:
          action_id=action.replace("?","").replace("!","")
          if "?" not in action or (action_id,action_args) not in game.seen_actions:
            rv_action=[action_energy,action_hint,action,action_args]
            break
        ## if not found and last_action is not None, find last_action and use next
        if not rv_action:
          last_action=game.last_actions.get(interactable.id,{}).get(action_type)
          if last_action:
            for n,(action_energy,action_hint,action,action_args) in enumerate(actions):
              action_id=action.replace("?","").replace("!","")
              if last_action==(action_id,action_args):
                rv_action=actions[(n+1)%len(actions)]
                break
        if not rv_action:
          ## if still not found any applicable, use first
          rv_action=actions[0]
      rv[action_type]=rv_action
    rv=[[(action_type,rv[action_type][0])]+rv[action_type][1:] for action_type in order if action_type in rv]
    return rv
