init -1000 python:
  class BaseClass(renpy.python.RevertableObject):
    def __getstate__(self):
      return vars(self).copy()
    def __setstate__(self,new_dict):
      self.__dict__.update(new_dict)
