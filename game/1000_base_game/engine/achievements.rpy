init -100 python:
  achievements_by_id={}
  achievements_by_order=[]

  class AchievementMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[12:] if name.lower().startswith("achievement_") else name
      if not dct.get("do_not_register",False):
        achievements_by_id[cls.id]=cls
        achievements_by_order.append(cls)
      for k,v in dct.items():
        if callable(v):
          force_classmethod(cls,k)
    ## allow testing "if achievement.fixer:" instead of "if achievement.fixed.unlocked"
    def __nonzero__(cls):
      return cls.unlocked
    @property
    def unlocked(cls):
      return bool(cls.is_unlocked())
    @property
    def value(cls):
      if persistent.achievements is None:
        persistent.achievements={}
      rv=persistent.achievements.get(cls.id,cls.default_value)
      value_type=type(cls.default_value)
      rv=value_type(rv)
      return rv
    @value.setter
    def value(cls,value):
      unlocked=cls.unlocked
      value_type=type(cls.default_value)
      value=value_type(value)
      if persistent.achievements is None:
        persistent.achievements={}
      persistent.achievements[cls.id]=value
      if not unlocked and cls.unlocked:
        process_event("achievement_unlocked",cls)

  class Achievement(BaseClass):
    __metaclass__=AchievementMeta
    do_not_register=True
    id=None
    default_value=0
    unlock_value=1
    def title(self):
      ## check value and decide what title should be used
      return "-achievement-"
    def description(self):
      ## check value and decide what description should be used
      return "-achievement-"
    def unlocked_message(self):
      ## custom unlocked notification message or None for default
      return None
    def unlock(self):
      ## set persistent value to something meaning unlock
      self.value=self.unlock_value
    def is_unlocked(self):
      ## read persistent value and decide if achievement considered unlocked
      if isinstance(self.unlock_value,int):
        return self.value>=self.unlock_value
      else:
        return self.value==self.unlock_value
    def reset(self):
      ## clear achievement
      self.value=self.default_value

  def reset_achievements(confirm=True):
    if confirm:
      Confirm("Reset all achievements?",Function(reset_achievements,False))()
    else:
      for achievement in achievements_by_order:
        achievement.reset()

  class AchievementsManager(BaseClass):
    def __getattr__(self,name):
      return achievements_by_id[name]

  achievement=AchievementsManager()
