init -100 python:
  phone_apps_by_id={}

  class PhoneAppMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[9:] if name.lower().startswith("phoneapp_") else name
      if not dct.get("do_not_register",False):
        phone_apps_by_id[cls.id]=cls
      for k,v in dct.items():
        if callable(v):
          force_classmethod(cls,k)

  class PhoneApp(BaseClass):
    __metaclass__=PhoneAppMeta
    do_not_register=True
    hidden=False
    icon="phone dummy_app_icon"
    title="-title-"
    screen=None
    notify_app_installed=True
    notify_app_uninstalled=True
    def __eq__(self,other):
      return self is other or self.id==other
    def __ne__(self,other):
      return not self.__eq__(other)
    def __str__(self):
      return str(self.title)
