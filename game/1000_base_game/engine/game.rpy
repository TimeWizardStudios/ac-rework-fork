init -100 python:
  class GameUI(BaseClass):
    def __init__(self):
      super().__init__()
      self.hint_active_elements=False
      self.hide_hud=1
      self.hud_active=False
      self.dialog_mode=None
      self.dialog_mode_args=[]
      self.can_close_phone=True
      self.inventory_page=0

  class BaseGame(BaseClass):
    starting_season=1
    starting_day=1
    starting_hour=0
    def __init__(self):
      super().__init__()
      self._speaker=None
      self._speaker_state=None
      self.ui=GameUI()
      self.events_queue=[]
      self.season=self.starting_season
      self.day=self.starting_day
      self.hour=self.starting_hour
      self._pc=None
      self.activity_selector=None
      self.seen_actions=set()
      self.last_actions={}
      self.investigate_shown=set()
      self.timed_out_quests=set()
      self.skipped_backstory_choices=False
      self.hud_hint=""
    @property
    def pc(self):
      return self.characters.get(self._pc)
    @pc.setter
    def pc(self,pc):
      if isinstance(pc,basestring):
        pc=self.characters[pc]
      old_pc=self.characters.get(self._pc)
      self._pc=pc.id
      if pc!=old_pc:
        process_event("pc_changed",old_pc,pc)
    @property
    def location(self):
      if self.pc:
        return self.pc.location
    @location.setter
    def location(self,location):
      if self.pc:
        self.pc.location=location
      process_event("update_state")
    def skip_events(self,filter=None):
      skipped_events=self.events_queue[:]
      self.events_queue=[]
      for event in skipped_events:
        if filter:
          if isinstance(filter,(list,tuple)) and event in filter:
            self.events_queue.append(event)
            continue
          elif callable(filter) and filter(event):
            self.events_queue.append(event)
            continue
        if isinstance(event,basestring):
          if renpy.has_label(event):
            event=None
          else:
            event=game_events_by_id[event]
            event_label=event.default_label
            event_args=event.default_args
        else:
          event_label=event[1]
          event_args=event[2:]
          event=game_events_by_id[event[0]]
        if event and event.process("missed",event_label,*event_args):
          self.events_queue.append(event)
    def advance(self,hours=1):
      self.skip_events()
      self.hour+=hours
      if self.hour>=24:
        self.hour=0
        self.day+=1
      process_event("advance")
      process_event("update_state")
    @property
    def can_advance_time(self):
      rv=bool(game.ui.hud_active)
      if rv:
        rv=process_event("can_advance_time",default_rv=rv)
      return rv
