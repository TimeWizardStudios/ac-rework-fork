init -100 python:
  locations_by_id={}

  class LocationMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[9:] if name.lower().startswith("location_") else name
      if not dct.get("do_not_register",False):
        locations_by_id[cls.id]=cls

  class Location(FlagsMixin,EventProcessorMixin,BaseClass):
    __metaclass__=LocationMeta
    do_not_register=True
    id=None
    main_loop_label=None
    default_title="- location -"
    default_unlocked=True
    notify_unlocked=True
    def __init__(self):
      super().__init__()
      self._unlocked=self.default_unlocked
    def remove(self):
      if game.locations.get(self.id) is self:
        game.locations.pop(self.id)
    def __eq__(self,other):
      return self is other or self.id==other
    def __ne__(self,other):
      return not self.__eq__(other)
    def __str__(self):
      return str(self.title)
    @property
    def title(self):
      return self.default_title
    @property
    def unlocked(self):
      return self._unlocked
    @unlocked.setter
    def unlocked(self,unlocked):
      if not self._unlocked:
        self._unlocked=unlocked
        if unlocked:
          process_event("location_unlocked",self)
    def visit(self):
      self["visited_today"]+=1
      self["visited_now"]+=1
      self["visited"]+=1
    @property
    def first_visit(self):
      return self["visited"]<1
    @property
    def first_visit_today(self):
      return self["visited_today"]<1
    @property
    def first_visit_now(self):
      return self["visited_now"]<1
    def scene(self,scene):
      pass
    def advance(self):
      self.process_event("on_advance")
    def prepare_images(self,scene):
      pass
    def find_path(self,target_location):
      return None
