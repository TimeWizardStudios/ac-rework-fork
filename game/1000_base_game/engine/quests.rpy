init -100 python:
  quests_by_id={}

  class QuestMeta(type):
    def __new__(cls,name,bases,dct):
      phases={}
      new_dct={}
      for base in bases:
        if hasattr(base,"_phases"):
          phases.update(base._phases)
      for k,v in dct.items():
        if k.startswith("phase_"):
          num_id,str_id=k[6:].split("_",1)
          num_id=int(num_id)
          v.id=str_id
          v.num_id=num_id
          v.original_id=k
          for id in (num_id,str_id):
            if id in phases:
              raise Exception("Duplicate quest phase id: {}, {} clashes with {}".format(name,k,phases[id].original_id))
          force_staticmethod(v,"description")
          force_staticmethod(v,"hint")
          force_staticmethod(v,"quest_guide_hint")
          phases[num_id]=v
          phases[str_id]=v
        else:
          new_dct[k]=v
      new_dct["_phases"]=phases
      if "id" not in new_dct:
        new_dct["id"]=name[6:] if name.lower().startswith("quest_") else name
      return type.__new__(cls,name,bases,new_dct)
    def __init__(cls,name,bases,dct):
      if not dct.get("do_not_register",False):
        quests_by_id[cls.id]=cls

  class Quest(FlagsMixin,BaseClass):
    __metaclass__=QuestMeta
    do_not_register=True
    id=None
    title="- generic quest -"
    hidden=False
    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.phase=0
      self.started=False
      self.finished=False
      self.failed=False
      self._failed_items={}
    def __cmp__(self,other):
      other_phase=self._phases.get(other).num_id
      current_phase=self.phase.num_id
      return current_phase-other_phase
    def __str__(self):
      description=getattr(self.phase,"description","")
      if callable(description):
        description=description(self)
      return description
    @property
    def phase(self):
      return self._phases[self._phase]
    @phase.setter
    def phase(self,phase):
      self._phase=self._phases.get(phase,phase).num_id
    @property
    def description(self):
      return str(self)
    @property
    def hint(self):
      hint=getattr(self.phase,"hint","")
      if callable(hint):
        hint=hint(self)
      return hint
    @property
    def quest_guide_hint(self):
      hint=getattr(self.phase,"quest_guide_hint","")
      if callable(hint):
        hint=hint(self)
      return hint
    def start(self,start_phase=1,silent=False):
      if not self.started:
        self.started=True
        self.phase=start_phase
        process_event("update_state")
        process_event("quest_started",self,silent=silent)
    def finish(self,finish_phase=1000,silent=False):
      if not self.finished:
        self.finished=True
        self.phase=finish_phase
        process_event("update_state")
        process_event("quest_finished",self,silent=silent)
    def fail(self,fail_phase=2000,silent=False):
      if not self.failed:
        self.failed=True
        self.phase=fail_phase
        process_event("update_state")
        process_event("quest_failed",self,silent=silent)
    @property
    def in_progress(self):
      return self.started and not self.ended
    @property
    def ended(self):
      return self.finished or self.failed
    @property
    def actually_finished(self):
      return self.finished and self.id not in game.timed_out_quests
    def advance(self,new_phase=None,silent=False):
      if new_phase is None:
        phases=sorted([v for v in self._phases if isinstance(v,(int,long))])
        cur_phase=phases.index(self.phase.num_id)
        if cur_phase+1<len(phases):
          new_phase=phases[cur_phase+1]
        else:
          return
      elif isinstance(new_phase,basestring):
        new_phase=self._phases[new_phase].num_id
      elif isinstance(new_phase,int):
        new_phase= self.phase.num_id+new_phase
      if self.phase.num_id!=new_phase:
        self.phase=new_phase
        process_event("update_state")
        process_event("quest_advanced",self,silent=silent)
    def failed_item(self,stage,item=None):
      if item is None:
        item=stage
        stage=None
      if isinstance(item,basestring):
        item=items_by_id[item]
      if stage not in self._failed_items:
        self._failed_items[stage]=set()
      self._failed_items[stage].add(item.id)
    class phase_0_not_started:
      description=""
      hint=""
      quest_guide_hint=""

  def quest_item_filter(quest_info,item):
    if isinstance(item,basestring):
      item=items_by_id[item]
    quest_id,_,stage=quest_info.partition(",")
    quest=game.quests[quest_id]
    if item.id in quest._failed_items.get(stage or None,[]):
      return "grey"

  class QuestsManager(BaseClass):
    def __getattr__(self,name):
      return game.quests[name]
