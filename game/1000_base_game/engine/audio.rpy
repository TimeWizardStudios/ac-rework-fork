init python:
  class AudioChannelPrefix(object):
    def __init__(self,prefix=""):
      if prefix and prefix[-1]!=" ":
        prefix+=" "
      self.prefix=prefix
    def __add__(self,other):
      if isinstance(other,basestring):
        rv=getattr(audio,self.prefix+other,None)
        if rv is not None:
          return rv
      return other

  def prepare_audio_channels():
    for channel_id,channel in renpy.audio.audio.channels.items():
      prefix=channel_id if isinstance(channel_id,basestring) else "sound"
      channel.file_prefix=AudioChannelPrefix(prefix)
    config.auto_channels={id:(mixer,AudioChannelPrefix(prefix or "sound"),suffix) for id,(mixer,prefix,suffix) in config.auto_channels.items()}

  prepare_audio_channels()
