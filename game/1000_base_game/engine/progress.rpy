init -100 python:
  progress_labels_to_track=set()

  if not isinstance(persistent.progress_seen,set):
    persistent.progress_seen=set()

  def reset_progress(confirm=True):
    if confirm:
      Confirm("Reset all progress?",Function(reset_progress,False))()
    else:
      persistent.progress_seen.clear()

  def progress_on_label_callback(label,reason):
    if label in progress_labels_to_track:
      persistent.progress_seen.add(label)

  config.label_callback=progress_on_label_callback

  def calculate_progress():
    total_labels=len(progress_labels_to_track)
    seen_labels=len(persistent.progress_seen)
    return seen_labels,total_labels

  def progress_str():
    seen_labels,total_labels=calculate_progress()
    rv="{:.2f}%".format(seen_labels*100.0/total_labels)
    return rv
