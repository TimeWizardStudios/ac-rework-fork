init -800 python:
  game_events_by_id={}
  game_events_by_handler_id={}

  class GameEventMeta(type):
    def __init__(cls,name,bases,dct):
      id=dct.get("id",None)
      if id is None:
        cls.id=name[6:] if name.lower().startswith("event_") else name
      if not dct.get("do_not_register",False):
        game_events_by_id[cls.id]=cls
      for k,v in dct.items():
        if k.startswith("on_") and callable(v):
          force_classmethod(cls,k)

  class GameEvent:
    __metaclass__=GameEventMeta
    do_not_register=True
    id=None
    priority=0
    default_label=None
    default_args=[]
    @classmethod
    def process(event,handler_id,*args,**kwargs):
      handler=getattr(event,"on_"+handler_id,None)
      if callable(handler):
        return handler(*args,**kwargs)

  def process_event(handler_id,*args,**kwargs):
    if kwargs.pop("all_rv",False):
      default_rv=kwargs.pop("default_rv",NotFound)
      #print(handler_id)
      rv=[]
      #print(game_events_by_handler_id)
      if handler_id in game_events_by_handler_id:
        #print("handler_id in game_events_by_handler_id")
        for event in game_events_by_handler_id[handler_id]:
          rv.append(event.process(handler_id,*args,**kwargs))
      if not rv and default_rv is not NotFound:
        rv=[default_rv]
    else:
      rv=kwargs.pop("default_rv",None)
      if handler_id in game_events_by_handler_id:
        for event in game_events_by_handler_id[handler_id]:
          handler_rv=event.process(handler_id,*args,**kwargs)
          if handler_rv is not None:
            rv=handler_rv
    return rv

init 9999 python:
  def finalize_game_events():
    global game_events_by_handler_id
    for event in game_events_by_id.values():
      for handler_id in dir(event):
        if handler_id.startswith("on_"):
          handler=getattr(event,handler_id)
          if callable(handler):
            handler_id=handler_id[3:]
            if handler_id not in game_events_by_handler_id:
              game_events_by_handler_id[handler_id]=[]
            game_events_by_handler_id[handler_id].append(event)
    game_events_by_handler_id={handler_id:sorted(handlers,key=lambda event: event.priority) for handler_id,handlers in game_events_by_handler_id.items()}

  finalize_game_events()
