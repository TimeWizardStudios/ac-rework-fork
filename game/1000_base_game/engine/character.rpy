init -100 python:
  from fnmatch import fnmatchcase

  characters_by_id={}

  class BaseCharMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[10:] if name.lower().startswith("character_") else name
      if not dct.get("do_not_register",False):
        characters_by_id[cls.id]=cls
        renpy.image(cls.id,AvatarDisplayable(cls.id))

  class BaseChar(StatsMixin,PhoneMixin,MoneyMixin,InventoryMixin,FlagsMixin,EventProcessorMixin,BaseClass):
    __metaclass__=BaseCharMeta
    do_not_register=True
    id=None
    main_loop_label=None
    default_name=None
    default_stats=[]
    default_outfit_slots=[]
    record_say=True

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self._name=None
      self._char_properties=kwargs
      self._char_properties.setdefault("image",self.id)
      self._location=None
      self.activity=None
      self.alternative_locations={}
    def remove(self):
      if game.characters.get(self.id) is self:
        game.characters.pop(self.id)
    def __call__(self,what,*args,**kwargs):
      game._speaker=self
      if "|" in what:
        state,what=what.split("|",1)
        state=state.strip()
        if state:
          if state.startswith("@"):
            renpy.game.context().temporary_attributes=state[1:].split()
          else:
            renpy.game.context().say_attributes=state.split()
      state=renpy.game.context().temporary_attributes
      if state is None:
        state=renpy.game.context().say_attributes
      if state is None:
        state=renpy.get_attributes(self.id)
      if state is not None:
        state=" ".join(state)
      if state is not None:
        state=renpy.substitutions.substitute(state.strip())[0].strip()
        if state=="":
          state=None
      game._speaker_state=state
      dialog_line_hook(self,what)
      Character(self.name,**self._char_properties)(what,*args,**kwargs)
      self.talked()
    def do_extend(self,*args,**kwargs):
      Character(self.name,**self._char_properties).do_extend(*args,**kwargs)
    def predict(self,what):
      pass
    def __str__(self):
      return str(self.name)
    def __eq__(self,other):
      return self is other or self.id==other
    def __ne__(self,other):
      return not self.__eq__(other)
    @property
    def name(self):
      return self._name if self._name else self.default_name
    @name.setter
    def name(self,new_name):
      self._name=new_name
    def name_by(self,who):
      return self.name
    def __getattr__(self,key):
      if key.startswith("name_by_"):
        return self.name_by(key[8:])
      else:
        return super().__getattr__(key)
    def side_image(self,state):
      rv="{} side_images {}".format(self.id,state)
      if not renpy.has_image(rv,True):
        rv="{} side_images default".format(self.id)
      if not renpy.has_image(rv,True):
        rv=None
      return rv
    def status_image(self):
      rv="{} status".format(self.id)
      if not renpy.has_image(rv):
        rv=None
      return rv
    def avatar(self,state=None):
      if state and isinstance(state,basestring):
        if renpy.has_image("{} avatar {}".format(self.id,state),True):
          return "{} avatar {}".format(self.id,state)
      return "{} avatar default".format(self.id)
    @property
    def location(self):
      return game.locations.get(self._location)
    @location.setter
    def location(self,location):
      if isinstance(location,basestring):
        location=game.locations[location]
      old_location=self.location
      if location!=old_location:
        if location is not None:
          self._location=location.id
          if self==game.pc:
            location.visit()
        else:
          self._location=None
        if self==game.pc:
          if location!=old_location:
            process_event("location_changed",old_location,location)
    def talked(self):
      self["talked_today"]+=1
      self["talked_now"]+=1
      self["talked"]+=1
    @property
    def talking(self):
      return renpy.showing(self.id) or self["talking"]
    @talking.setter
    def talking(self,talking):
      self["talking"]=talking
    def at(self,location,activity=None):
      if location:
        location_pattern=location if isinstance(location,basestring) else location.id
        if self._location and fnmatchcase(self._location,location_pattern):
          if not activity or activity==self.activity:
            return True
        for alt_location,alt_activity in self.alternative_locations.items():
          if alt_location and fnmatchcase(alt_location,location_pattern):
            if not activity or activity==alt_activity:
              return True
      return False
    def effective_stat(self,stat_id):
      stat=stats_by_id[stat_id]
      level=getattr(self,stat.id,stat.min_level)
      if level is not None:
        level=self.process_event("calc_effective_stat:1",stat,level)
      return level
    def effective_xp(self,stat_id,xp):
      stat=stats_by_id[stat_id]
      xp=self.process_event("calc_effective_xp:1",stat,xp)
      return xp
    def advance(self):
      self.process_event("on_advance")
    def ai(self):
      self.location=None
      self.activity=None
      self.alternative_locations={}
    @property
    def contact_number(self):
      cls = self.__class__
      if not hasattr(cls, "_contact_number"):
        contact_number = "+63"
        for i in range(10):
          contact_number += (" " if i == 0 or i == 3 or i == 6 else "") + str(renpy.random.choice(range(10)))
        cls._contact_number = contact_number
      return cls._contact_number
    @property
    def statement_name(self):
      return "say"

  class Character_narrator(BaseChar):
    pass

  class Character_default_char(BaseChar):
    def call_label(self):
      if quest.maxine_dive == "armor":
        return "quest_maxine_dive_armor"
      return None

  def say(who,what,*args,**kwargs):
    default_char=game.characters["default_char"]
    default_char.name=who
    default_char(what,*args,**kwargs)
