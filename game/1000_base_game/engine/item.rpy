init -100 python:
  items_by_id={}

  def find_item(item):
    return item if isinstance(item,ItemMeta) else items_by_id.get(item)

  class ItemMeta(type):
    def __init__(cls,name,bases,dct):
      if dct.get("id") is None:
        cls.id=name[5:] if name.lower().startswith("item_") else name
      if not dct.get("do_not_register",False):
        items_by_id[cls.id]=cls
      for k,v in dct.items():
        if callable(v):
          force_classmethod(cls,k)
    def __eq__(cls,other):
      return cls is other or cls.id==other
    def __ne__(cls,other):
      return not cls.__eq__(other)
    def __str__(cls):
      return str(cls.title())

  class Item(BaseClass):
    __metaclass__=ItemMeta
    do_not_register=True
    id=None
    investigate_id=None
    price=10
    icon=None
    slot=None
    def title(item):
      return "-title-"
    @classproperty
    def title_lower(item):
      if getattr(item,"retain_capital",False):
        return item.title()
      else:
        #return item.title().lower()
        s=item.title().lower()
        for i in ("maxine","flora","lindsey","mrs. l","nurse","berb","guard","isabelle","jacklyn","jo","kate","maya"):
            if i in s:
                s=s[:s.find(i)]+i[0].upper()+i[1:-1]+i[-1].upper()+s[s.find(i)+len(i):] if i in "mrs. l" else s[:s.find(i)]+i[0].upper()+i[1:]+s[s.find(i)+len(i):]
        return s
    def description(item):
      return ""
    def circle(cls,actions,x,y):
      rv=BaseClass()
      rv.center=(0,70)
      rv.radius=180
      rv.start_angle=90
      rv.angle_per_icon=36
      return rv
    def actions(item,actions):
      pass
    def finalize_actions(item,actions):
      ## can add actions what should be present in every item
      description=item.description()
      if description:
        investigate_id=item.investigate_id or item.id
        actions.insert(0,["investigate","Investigate","?!investigate",investigate_id,description])
      actions.append(["combine","Combine","select_inventory_item","$combine_filter:"+item.id+"|Combine "+item.title()+" with what?","combine_items",(item.id,),None])
      actions=prepare_interactable_actions(item,actions)
      return actions
