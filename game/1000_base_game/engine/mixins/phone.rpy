init -700 python:
  class PhoneMixin(BaseClass):
    ## character phone, installed apps, contacts, messages history
    notify_phone_app_installed=False
    notify_phone_app_uninstalled=False
    notify_phone_contact_added=False

    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.phone_apps=[]
      self.last_phone_message_contact=None
      self.phone_message_history={}
      self.phone_contacts=[]
    ## return app if installed or None
    def phone_app(self,app):
      for installed_app in self.phone_apps:
        if installed_app==app:
          return installed_app
      return None
    ## create and add app to phone
    ## generate "phone_app_installed" event
    def install_phone_app(self,app,silent=False):
      if isinstance(app,basestring):
        app=phone_apps_by_id[app]
      self.phone_apps.append(app())
      process_event("phone_app_installed",self,app,silent=silent)
    ## remove app from phone
    ## generate "phone_app_uninstalled" event
    def uninstall_phone_app(self,app,silent=False):
      for n,installed_app in enumerate(self.phone_apps):
        if installed_app==app:
          self.phone_apps.pop(n)
          process_event("phone_app_uninstalled",self,installed_app,silent=silent)
    ## add message <what> from <who> to history of <history_char>
    ## if <who> is narrator it is added as if <who> is character
    ## updates latest contact
    def add_message_to_history(self,history_char,who,what,day=None,hour=None,location=None):
      if isinstance(history_char,basestring):
        history_char=game.characters[history_char]
      if isinstance(who,basestring):
        who=game.characters[who]
      if history_char.id not in self.phone_message_history:
        self.phone_message_history[history_char.id]=[]
      day=day or game.day
      hour=hour or game.hour
      location=location or game.location.id
      timestamp=(day,hour,location)
      self.phone_message_history[history_char.id].append([who in (self,narrator),what.split("\n"),timestamp])
      self.last_phone_message_contact=history_char.id
    ## add contact to phone contacts
    ## generates "phone_contact_added" event
    def add_phone_contact(self,contact,silent=False):
      if isinstance(contact,basestring):
        contact=game.characters[contact]
      if contact.id not in self.phone_contacts:
        #print (self.phone_contacts)
        self.phone_contacts.append(contact.id)
        process_event("phone_contact_added",self,contact,silent=silent)
    ## Check if a phone number is known ##
    def check_phone_contact(self,contact):
      if isinstance(contact,basestring):
        contact=game.characters[contact]
      if contact.id in self.phone_contacts:
        return True
      else:
        return False
    ## returns label what should be called when clicked "Call Character"
    ## or None if default "no one answer" should be shown
    def call_label(self):
      return None
    ## returns label what should be called when clicked "Message Character"
    ## or None to open character messages history
    def message_label(self):
      return None
    ## returns string/list of strings to show in contact info screen
    ## or None if nothing to show
    def contact_notes(self):
      return None
