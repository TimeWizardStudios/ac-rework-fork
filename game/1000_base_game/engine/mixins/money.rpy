init -700 python:
  class MoneyMixin(BaseClass):
    ## simple money mixin for characters, main reason to generate notifications
    ## if changed, generate "money_changed" event
    notify_money_changed=False
    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self._money=0
    @property
    def money(self):
      return self._money
    @money.setter
    def money(self,money):
      if money!=self._money:
        old_money=self._money
        self._money=money
        if old_money!=money:
          process_event("money_changed",self,old_money,money)
