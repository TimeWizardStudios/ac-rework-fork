init -700 python:
  class InventoryMixin(BaseClass):
    ## generic inventory mixin, supporting having and wearing items.
    ## items can be stacked.
    ## object can have multiple slots, only items for these slots can be worn there.
    default_outfit_slots=[]
    notify_inventory_changed=False
    notify_outfit_changed=False
    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.outfit_slots=self.default_outfit_slots[:]
      self.inv=[]
      self.outfit={}
    ## check if object can equip item, item can be item class or string id
    ## we check does object have outfit slot used by item and does it not occupied by this item already
    def can_equip_item(self,item):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      return item.slot in self.outfit_slots and self.outfit.get(item.slot)!=item_id
    ## check if object can unequip item, item can be item class or string id
    ## we check if item actually equipped
    def can_unequip_item(self,item):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      return self.outfit.get(item.slot)==item_id
    ## return count of items with this id owned, but not equipped
    def owned_item_count(self,item):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      for owned_item_id,count in self.inv:
        if owned_item_id==item_id:
          return count-self.equipped_item_count(item)
      return 0
    ## return count of currently equipped items with this id
    def equipped_item_count(self,item):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      count=0
      for slot in self.outfit_slots:
        equipped_item_id=self.outfit.get(slot)
        if equipped_item_id==item_id:
          count+=1
      return count
    ## return count of owned+equipped items with this id
    def owned_or_equipped_item_count(self,item):
      return self.owned_item_count(item)+self.equipped_item_count(item)
    ## return True/False, if owned count is more or equal to requested
    ## if item is list or tuple, each element is item in question
    ##   if item is list or tuple then first element is item, second is count to check
    ##   otherwise checked if have item at all
    def owned_item(self,item,count=1):
      if isinstance(item,(list,tuple)):
        items=item
        for item in items:
          if not isinstance(item,(list,tuple)):
            item=(item,1)
          item,count=item
          owned_count=self.owned_item_count(item)
          if owned_count>=count:
            #return item,owned_count
            return item #changed from above line, to only return the item, not the count
      else:
        return self.owned_item_count(item)>=count
    ## return True/False, if equipped this item
    def equipped_item(self,item):
      return self.equipped_item_count(item)>0
    ## return True/False, if owned or equipped requested count
    def owned_or_equipped_item(self,item,count=1):
      return self.owned_or_equipped_item_count(item)>=count
    ## return item equipped in slot, or None if nothing there
    def equipped(self,slot):
      item=self.outfit.get(slot)
      if item:
        item=items_by_id[item]
      return item
    ## add item or count items to inventory
    ## generate "inventory_changed" event
    def add_item(self,item,count=1,silent=False):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      if count>0:
        for n,(owned_item_id,old_count) in enumerate(self.inv):
          if owned_item_id==item_id:
            self.inv[n][1]+=count
            process_event("inventory_changed",self,item,old_count,old_count+count,silent=silent)
            return self.owned_item_count(item)
        self.inv.append([item_id,count])
        process_event("inventory_changed",self,item,0,count,silent=silent)
      return self.owned_item_count(item)
    ## remove item or count items from inventory
    ## generate "inventory_changed" event
    def remove_item(self,item,count=1,silent=False):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      for n,(owned_item_id,old_count) in enumerate(self.inv):
        if owned_item_id==item_id:
          old_count=self.owned_item_count(item)
          if count>=old_count:
            count=old_count
            self.inv.pop(n)
          else:
            self.inv[n][1]-=count
          process_event("inventory_changed",self,item,old_count,old_count-count,silent=silent)
      return self.owned_item_count(item)
    #returns a random consumable that is contained in the inventory of the parent class (mc)  
    #false if no consumables are owned
    # E.G: mc.remove_item(mc.random_consumable())
    def random_consumable(self):
      consumables = []
      for item,count in self.inv:
        if getattr(items_by_id[item],"consumable",False):
          consumables.append(item)
      if consumables:
        return consumables[renpy.random.randint(0,len(consumables)-1)]#item
      return False 
    ## equips owned item in item's slot
    ## if slot already occupied old item it unequipped
    ## generate "outfit_changed" event
    def equip(self,item,silent=False):
      if isinstance(item,basestring):
        item=items_by_id[item]
      item_id=item.id
      if item.slot is not None:
        if self.owned_item(item):
          worn_item_id=self.outfit.get(item.slot)
          if worn_item_id!=item_id:
            self.unequip(item.slot)
            self.outfit[item.slot]=item_id
            worn_item=items_by_id.get(worn_item_id,None)
            process_event("outfit_changed",self,item.slot,worn_item,item,silent=silent)
            return worn_item_id
    ## unequips slot or equipped item
    ## if slot then unequips any item from it
    ## if item then unequips only if item is actually equipped
    ## generate "outfit_changed" event
    def unequip(self,slot_or_item,silent=False):
      if slot_or_item in self.outfit_slots:
        slot=slot_or_item
        worn_item_id=self.outfit.get(slot)
        if worn_item_id is not None:
          self.outfit[slot]=None
          worn_item=items_by_id[worn_item_id]
          process_event("outfit_changed",self,slot,worn_item,None,silent=silent)
          return worn_item_id
      else:
        if isinstance(slot_or_item,basestring):
          item=items_by_id[slot_or_item]
        else:
          item=slot_or_item
        worn_item_id=self.outfit.get(item.slot)
        if worn_item_id==item.id:
          self.outfit[item.slot]=None
          process_event("outfit_changed",self,item.slot,item,None,silent=silent)
          return item.id
    ## add bonus to stat from owned and/or equipped items
    def calc_effective_stat(self,stat,level):
      for item_id,count in self.inv:
        item=items_by_id[item_id]
        handler=getattr(item,"calc_effective_stat",None)
        if callable(handler):
          owned=self.owned_item_count(item_id)
          equipped=self.equipped_item_count(item_id)
          level=handler(self,stat,level,owned,equipped)
      return level
    ## add bonus to xp gained from owned and/or equipped items
    def calc_effective_xp(self,stat,xp):
      for item_id,count in self.inv:
        item=items_by_id[item_id]
        handler=getattr(item,"calc_effective_xp",None)
        if callable(handler):
          owned=self.owned_item_count(item_id)
          equipped=self.equipped_item_count(item_id)
          xp=handler(self,stat,xp,owned,equipped)
      return xp
