init -700 python:
  class StatsMixin(BaseClass):
    default_stats=[]
    notify_xp_granted=False
    notify_level_changed=False
    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.stats={}
      self.stats_order=[]
      for stat_info in self.default_stats:
        self.add_stat(*stat_info)
    def give_xp(self,stat_id,xp=None,silent=False):
      if xp is None:
        xp=stat_id
        stat_id=self.default_stat
      stat=stats_by_id[stat_id]
      stat_id=stat.id
      if hasattr(self,"effective_xp"):
        xp=self.effective_xp(stat_id,xp)
      if xp!=0:
        process_event("stat_xp_granted",self,stat,xp,silent=silent)
      old_level,old_xp=self.stats[stat_id]
      level=old_level
      if xp>0:
        xp+=old_xp
        while level<stat.max_level:
          needed_xp=stat.xp_to_next_level[level-stat.min_level]
          if xp>=needed_xp:
            level+=1
            xp-=needed_xp
          else:
            break
      elif xp<0:
        xp+=old_xp
        while level>stat.min_level:
          if xp<0:
            level-=1
            xp+=stat.xp_to_next_level[level-stat.min_level]
          else:
            break
      xp=min(max(xp,0),stat.xp_to_next_level[level-stat.min_level])
      self.stats[stat_id]=[level,xp]
      if level!=old_level:
        process_event("stat_level_changed",self,stat,old_level,level,silent=silent)
    def __setattr__(self,name,value):
      stats=getattr(self,"stats",None)
      if stats is not None:
        stat=stats_by_id.get(name)
        if stat is not None:
          stat_id=stat.id
          if stat_id in stats:
            old_level,old_xp=stats[stat_id]
            level=min(max(value,stat.min_level),stat.max_level)
            if level!=old_level:
              stats[stat_id]=[level,0]
              process_event("stat_level_changed",self,stat,old_level,level)
            return
      super().__setattr__(name,value)
    def __getattr__(self,name):
      if hasattr(self,"stats"):
        stats=getattr(self,"stats",None)
        if stats is not None:
          if name.startswith("effective_"):
            stat_id=stats_by_id[name[10:]].id
            if stat_id in stats:
              return self.effective_stat(stat_id)
          elif name.startswith("max_"):
            stat_id=stats_by_id[name[4:]].id
            if stat_id in stats:
              return stats_by_id[stat_id].max_level
          elif name.startswith("min_"):
            stat_id=stats_by_id[name[4:]].id
            if stat_id in stats:
              return stats_by_id[stat_id].min_level
          elif name.endswith("_xp"):
            stat_id=stats_by_id[name[:-3]].id
            if stat_id in stats:
              return stats[stat_id][1]
          else:
            stat_id=stats_by_id[name].id
            if stat_id in stats:
              return stats[stat_id][0]
      raise AttributeError("Entity \"{}\" has no defined \"{}\" stat".format(self.id,name))
    def add_stat(self,stat_id,level,xp):
      stat_id=stats_by_id[stat_id].id
      if stat_id not in self.stats:
        stat=stats_by_id[stat_id]
        level=min(stat.max_level,max(stat.min_level,level))
        xp=min(stat.xp_to_next_level[level],max(0,xp))
        self.stats[stat_id]=[level,xp]
        self.stats_order.append(stat_id)
    def remove_stat(self,stat_id):
      stat_id=stats_by_id[stat_id].id
      if stat_id in self.stats:
        self.stats.pop(stat_id)
        self.stats_order.pop(self.stats_order.index(stat_id))
    def calc_effective_stat(self,stat,level):
      for char_stat_id in self.stats_order:
        char_stat=stats_by_id[char_stat_id]
        handler=getattr(char_stat,"calc_effective_stat",None)
        if callable(handler):
          char_stat_level=self.stats[char_stat.id][0]
          level=handler(self,stat,level,char_stat_level)
      return level
    def calc_effective_xp(self,stat,xp):
      for char_stat_id in self.stats_order:
        char_stat=stats_by_id[char_stat_id]
        handler=getattr(char_stat,"calc_effective_xp",None)
        if callable(handler):
          char_stat_level=self.stats[char_stat.id][0]
          xp=handler(self,stat,xp,char_stat_level)
      return xp
