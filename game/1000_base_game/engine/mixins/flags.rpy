init -700 python:
  class FlagsMixin(BaseClass):
    ## Class using flags mixin have flags dict what can be accessed Bob["flag1"] etc.
    ## Basically this is used to contain all assorted small variables related to object.
    ## It have extra special functionality, if flag name ends with _now or _today it is cleared automatically on advance.
    ## You can force flag type by accessing it using type modifier Bob["flag2:bool"] - this will return True/False, not 0/1/"abc".
    default_flag_value=0
    def __init__(self,*args,**kwargs):
      super().__init__(*args,**kwargs)
      self.flags={}
    def __getitem__(self,name):
      if ":" in name:
        name,normalizer=name.split(":",1)
        normalizer=eval(normalizer)
        return normalizer(self.flags.get(name,self.default_flag_value))
      else:
        return self.flags.get(name,self.default_flag_value)
    def __setitem__(self,name,value):
      if ":" in name:
        name,normalizer=name.split(":",1)
        normalizer=eval(normalizer)
        self.flags[name]=normalizer(value)
      else:
        self.flags[name]=value
        if name.endswith("_this_week"):
          self.flags[name+"_day_tracker"]=game.day
        if automatically_update_state:
          process_event("update_state")
    def on_advance(self):
      for name,value in self.flags.items():
        if name.endswith("_now"):
          del self.flags[name]
        elif name.endswith("_today") and game.hour==0:
          del self.flags[name]
        elif name.endswith("_this_week_day_tracker") and game.day==value+7 and game.hour==0:
          del self.flags[name]
          del self.flags[name.replace("_day_tracker","")]

      for id,quest_cls in quests_by_id.items(): ## Adds the automatic clear functionality to quest flags as well
        for name,value in game.quests[id].flags.items():
          if name.endswith("_now"):
            del game.quests[id].flags[name]
          elif name.endswith("_today") and game.hour==0:
            del game.quests[id].flags[name]
          elif name.endswith("_this_week_day_tracker") and game.day==value+7 and game.hour==0:
            del game.quests[id].flags[name]
            del game.quests[id].flags[name.replace("_day_tracker","")]
