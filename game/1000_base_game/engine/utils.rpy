init -950 python:
  import sys,math,pygame,random

  class NotFound:
    pass

  class classproperty(object):
    def __init__(self, fget):
      self.fget=fget
    def __get__(self, owner, cls):
      return self.fget(cls)

  def force_classproperty(cls,f_name):
    f=getattr(cls,f_name,None)
    if f is not None:
      im_func=getattr(f,"im_func",NotFound)
      if im_func is NotFound:
        setattr(cls,f_name,classproperty(f))
      elif f.im_self is None:
        setattr(cls,f_name,classproperty(im_func))

  def force_classmethod(cls,f_name):
    f=getattr(cls,f_name,None)
    if f is not None:
      im_func=getattr(f,"im_func",NotFound)
      if im_func is NotFound:
        setattr(cls,f_name,classmethod(f))
      elif f.im_self is None:
        setattr(cls,f_name,classmethod(im_func))

  def force_staticmethod(cls,f_name):
    f=getattr(cls,f_name,None)
    if f is not None:
      im_func=getattr(f,"im_func",NotFound)
      if im_func is not NotFound:
        setattr(cls,f_name,staticmethod(im_func))

  ## allow skipping super arguments like in Python3
  ## support methods and classmethods, but not staticmethods
  from inspect import currentframe
  system_super=super
  def super(*args):
    if args:
      return system_super(*args)
    f=currentframe().f_back
    first_arg=f.f_locals[f.f_code.co_varnames[0]]
    cm=isinstance(first_arg,type)
    caller=f.f_code
    method_name=caller.co_name
    for cls in (first_arg if cm else first_arg.__class__).__mro__:
      method=cls.__dict__.get(method_name)
      if method is not None:
        method=getattr(method,"__func__",method)
        if method.func_code==caller:
          break
    return system_super(cls,(cls if cm else first_arg))

  def get_variable_at(path):
    path=path.split(".")
    root=store
    while len(path)>0 and root is not NotFound:
      root=getattr(root,path.pop(0),NotFound)
    return root

  def set_variable_at(path,value):
    path=path.split(".")
    root=store
    while len(path)>1 and root is not NotFound:
      root=getattr(root,path.pop(0),NotFound)
    if root is not NotFound:
      setattr(root,path[0],value)
