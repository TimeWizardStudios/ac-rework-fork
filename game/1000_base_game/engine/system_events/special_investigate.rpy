﻿init python:
  class Event_investigate_shown(GameEvent):
    def on_investigate_shown(event,obj_id,description):
      if description.startswith("{#"):
        marker=description[2:].split("}")[0]
        game.investigate_shown.add(marker)
