init python:
  class Event_init_locations(GameEvent):
    priority=-9
    def on_init(event):
      game.locations={}
      for id,loc_cls in locations_by_id.items():
        if id is not None:
          loc=loc_cls()
          setattr(store,id,loc)
          game.locations[id]=loc

  class Event_init_characters(GameEvent):
    priority=-5
    def on_init(event):
      game.characters={}
      for id,char_cls in characters_by_id.items():
        if id is not None:
          char=char_cls()
          setattr(store,id,char)
          game.characters[id]=char

  class Event_init_quests(GameEvent):
    priority=-7
    def on_init(event):
      game.quests={}
      store.quest=QuestsManager()
      for id,quest_cls in quests_by_id.items():
        if id is not None:
          quest=quest_cls()
          game.quests[id]=quest

  class Event_init_stat_perks(GameEvent):
    priority=-3
    def on_init(event):
      game.unlocked_stat_perks=set()

  class Event_init_game(GameEvent):
    priority=-1
    def on_init(event):
      process_event("update_state")
