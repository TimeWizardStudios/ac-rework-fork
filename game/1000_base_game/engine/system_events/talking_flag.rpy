init python:
  class Event_clear_characters_talking_flag(GameEvent):
    priority=-1
    def on_location_changed(event,old_location,new_location,silent=False):
      for char_id,char in game.characters.items():
        char["talking"]=False
    def on_enter_roaming_mode(event):
      for char_id,char in game.characters.items():
        char["talking"]=False
