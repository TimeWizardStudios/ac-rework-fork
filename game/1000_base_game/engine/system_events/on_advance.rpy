init python:
  class Event_process_locations_advance(GameEvent):
    priority=-9
    def on_advance(event):
      for loc_id,location in game.locations.items():
        location.advance()

  class Event_process_characters_advance(GameEvent):
    priority=-5
    def on_advance(event):
      for char_id,char in game.characters.items():
        char.advance()

  class Event_show_time_passed_screen(GameEvent):
    priority=99
    def on_advance(event):
      ## put some condition here to avoid showing time passed screen more than necessary
      game.events_queue.append("event_show_time_passed_screen")

label event_show_time_passed_screen:
  call screen time_passed
  return
