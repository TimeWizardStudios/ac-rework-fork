﻿init python:
  class Event_update_game_state(GameEvent):
    priority=-9
    def on_update_state(event):
      ## update characters locations/activities
      for char_id,char in game.characters.items():
        if char!=game.pc:
          if not hasattr(game,"season"):
            game.season=1
          char.ai()
        else:
          char.activity=None
          char.alternative_locations={}
