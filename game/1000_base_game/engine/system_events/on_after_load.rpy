init python:
  class Event_init_new_locations(GameEvent):
    priority=-9
    def on_after_load(event):
      for id,loc_cls in locations_by_id.items():
        if id and id not in game.locations:
          loc=loc_cls()
          setattr(store,id,loc)
          game.locations[id]=loc

  class Event_init_new_characters(GameEvent):
    priority=-5
    def on_after_load(event):
      for id,char_cls in characters_by_id.items():
        if id and id not in game.characters:
          char=char_cls()
          setattr(store,id,char)
          game.characters[id]=char

  class Event_init_new_quests(GameEvent):
    priority=-4
    def on_after_load(event):
      for id,quest_cls in quests_by_id.items():
        if id and id not in game.quests:
          quest=quest_cls()
          game.quests[id]=quest

  class Event_update_state_after_load(GameEvent):
    priority=-1
    def on_after_load(event):
      process_event("update_state")
