python early:
  config.ignore_duplicate_labels=True

  def load_mods():
    import os
    for mod in sorted(os.listdir(config.gamedir)):
      if "_" in mod and (mod.endswith(".rpa") or os.path.isdir(config.gamedir+"/"+mod)):
        if mod.endswith(".rpa"):
          mod=mod[:-4]
        renpy.config.search_prefixes.insert(0,mod+"/")
        renpy.config.search_prefixes.insert(0,mod+"/assets/")
    
  load_mods()

init -1800 python:
  config.audio_directory=None

  def scan_audio_files():
    for fn in renpy.list_files():
      if fn.lower().endswith((".wav",".mp2",".mp3",".ogg",".opus")):
        parts=fn.split("/") if "/" in fn else [fn]
        if "assets" in parts:
          parts=parts[parts.index("assets")+1:]
        if parts and parts[0] in ["audio"]:
          parts.pop(0)
          if parts:
            parts[-1]=parts[-1].rsplit(".",1)[0]
            name=" ".join(parts)
            audio.__dict__[name]=fn

  scan_audio_files()

init -1800 python:
  config.images_directory=None

  xray_fn_suffix=["","_xray","_xray_full"]
  xray_mode_name=["","ON","FULL"]

  xray_image_versions={}

  def XRayImage(st,at,image_name):
    rv=Null()
    if hasattr(store,"game"):
      if image_name in xray_image_versions:
        for fn_suffix in reversed(xray_fn_suffix[:game.xray_mode+1]):
          xray_fn=xray_image_versions[image_name].get(fn_suffix)
          if xray_fn and renpy.loadable(xray_fn):
            rv=xray_fn
            break
    rv=renpy.easy.displayable(rv)
    return rv,0.05

  def scan_image_files():
    images={}
    xray_image_versions.clear()
    for fn in renpy.list_files():
      if fn.lower().endswith((".png",".jpg",".webp")):
        parts=fn.split("/") if "/" in fn else [fn]
        if "assets" in parts:
          parts=parts[parts.index("assets")+1:]
        if parts and parts[0] in ["images"]:
          parts.pop(0)
          while parts and parts[0] in ["characters","locations"]:
            parts.pop(0)
          if parts:
            parts[-1]=parts[-1].rsplit(".",1)[0]
            name=" ".join(parts)
            images[name]=fn
    xray_images=[name for name in images if "_xray" in name]
    for xray_name in xray_images:
      name=xray_name.partition("_xray")[0]
      if name in images:
        fn=images.get(name)
        if fn:
          xray_fn=images[xray_name]
          images[name]=DynamicDisplayable(XRayImage,name)
          if name not in xray_image_versions:
            xray_image_versions[name]={}
            xray_image_versions[name][""]=fn
          xray_image_versions[name][xray_name[len(name):]]=xray_fn
    for name,fn in images.items():
      renpy.image(name,fn)

  scan_image_files()
