init -999 python:
  def module_filter(mod):
    return True

init python:
  build.classify("**~",None)
  build.classify("**.~*",None)
  build.classify("**.bak",None)
  build.classify("**.cmd",None)
  build.classify("**/.**",None)
  build.classify("**/#**",None)
  build.classify("**/thumbs.db",None)
  build.classify("**.bat",None)
  build.classify("**.cmd",None)
  build.classify("saves/**",None)
  build.classify("cache/**",None)
  build.classify("tools/**.**",None)
  build.classify("doc/**.**",None)
  build.classify("**.rpy",None)
  build.classify("**/build_config.*",None)
  build.classify("**/local_config.*",None)

  import os
  for mod in os.listdir(config.gamedir):
    if os.path.isdir(config.gamedir+"/"+mod):
      if "_" in mod:
        if module_filter(mod):
          build.archive(mod)
          build.classify("game/"+mod+"/**",mod)
        else:
          build.classify("game/"+mod+"/**",None)

#  build.documentation("*.html")
#  build.documentation("*.txt")
