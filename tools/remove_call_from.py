﻿do_fix=True

###############################################################################

import os

def fix_rpy(path):
  with open(path,"rb") as f:
    data_in=f.read()
  lines=data_in.decode().splitlines()
  for line_n,line in enumerate(lines):
    if line.strip().startswith("call ") and " from " in line:
      lines[line_n]=line.partition(" from ")[0]
  if lines and lines[-1]!="":
    lines.append("")
  data_out="\r\n".join(lines).encode()
  if data_out!=data_in:
    if do_fix:
      print("fixed:",path)
      with open(path,"wb") as f:
        f.write(data_out)

def fix_dir(root):
  for path in os.listdir(root):
    path=os.path.join(root,path)
    if path.lower().endswith(".rpy"):
      fix_rpy(path)
    elif os.path.isdir(path):
      fix_dir(path)

fix_dir("../game")

print("done.")
