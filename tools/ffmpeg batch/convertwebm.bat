for /r %%F in (*.mp4, *.mkv, *.mov) do (
    ffmpeg -y -i "%%F" -c:v libvpx -qmin 0 -qmax 63 -crf 63 -b:v 1M -an "%%~dpnF.webm"
)
