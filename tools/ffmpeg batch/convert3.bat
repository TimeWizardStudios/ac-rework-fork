for /r %%F in (*.png) do (
    ffmpeg.exe -y -i "%%F" -compression_level 3 "%%~dpnF.webp"
    if not errorlevel 1 if exist "%%~dpnF.webp" del /q "%%F"
)

