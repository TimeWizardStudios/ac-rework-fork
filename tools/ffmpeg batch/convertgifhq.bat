for /r %%F in (*.mp4, *.mkv, *.mov) do (
    ffmpeg.exe -y -i "%%F" -vf fps=18,scale=720:-1,smartblur=ls=-0.5,crop=iw:ih-2:0:0 -loop 0 "%%~dpnF.gif"
)
