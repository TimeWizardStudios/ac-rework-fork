for /r %%F in (*.mp4, *.mkv, *.mov) do (
    ffmpeg.exe -y -i "%%F" -vf fps=14,scale=640:-1,smartblur=ls=-0.5,crop=iw:ih-2:0:0 -loop 0 "%%~dpnF.gif"
    if not errorlevel 1 if exist "%%~dpnF.mp4" del /q "%%F"
)
