#########################################
## The generate anything utility made by Tama to help write code blocks for javascript engine.
## Based on DrKlauz's script
##
## Tested with Python 3.7.4, psd-tools 1.9.13
## Tested with Python 3.7.4 and Pillow 9.3.0
## On Windows 10
##
## 1. Read avatar description from txt file.
## 2. Load avatar assets from folders specified in txt file.
## 3. Resize/rename avatar assets.
## 4. Generate prerendered avatar images as a visual aid
## 6. Generate optimal avatar images and code to be copy-pasted.
## OR
## 1. Read Image and Layer info from PSD photoshop file
## 2. Unpack layers from photoshop file into individual images
## 3. Generate code for location and interactables.
########################################

from PIL import Image
from psd_tools import PSDImage
import sys
import os
import shutil

###############################
default_equipment=["panties","bra","shirt","pants"]
process_as_interactables = True  ##Set to false to make a background of only static components instead. 
crop_layers=False
zoom=1
layer_id_replaces=[
  ["\\","_"],
  ["/", "_"],
  [" ", "_"],
  ["\"","_"],
  ["'", ""],
  ["(", "" ],
  [")", "" ],
  [":",""],
  ["\r",""],
  ["\n",""]
  ]  

########### Methods ############################
###########Tama's special method####################
def to_title_case(under_score):
  return "".join(x for x in under_score.title() if not x=="_") ##Returns TitleCase for any _ seperated term
############################For Text files###################################
def load_sprites_description(path):
  global zoom
  global char_id,char_id_up
  global prerendered_out_dir,optimal_out_dir
  global images_roots,image_prefixes,image_replaces
  global sprites,sprites_order
  images_roots=[]
  image_prefixes=[]
  image_replaces=[]
  sprites={}
  sprites_order=[]
  char_id=os.path.splitext(os.path.basename(path))[0]
  char_id_up=char_id[0].upper()+char_id[1:]
  prerendered_out_dir=os.path.join(char_id,"avatar_prerendered")
  optimal_out_dir=os.path.join(char_id,"avatar_optimal")
  with open(path,"rb") as f:
    data=f.read().decode().splitlines()
  current_sprite=None
  for line in data:
    line=line.strip()
    if line:
      if line.startswith("#"):
        continue
      elif line.startswith("*"):
        zoom=float(line[1:])
      elif line.startswith("@"):
        images_roots.append(line[1:])
      elif line.startswith("-"):
        image_prefixes.append(line[1:])
      elif line.startswith("?"):
        image_replaces.append(line[1:].split(":",1))
      elif line.endswith(":"):
        current_sprite=line[:-1]
        sprites_order.append(current_sprite)
        sprites[current_sprite]=[]
      elif current_sprite:
        sprites[current_sprite].append(line)

def do_search_images(path,images_root):
  for fn in os.listdir(path):
    if path:
      fn=os.path.join(path,fn)
    if os.path.isdir(fn):
      do_search_images(fn,images_root)
    elif fn.lower().endswith((".png",".jpg",".webp")):
      img_fn=fn
      parts=[]
      while True:
        fn,part=os.path.split(fn)
        if not part:
          break
        parts.insert(0,part)
      parts[-1]=os.path.splitext(parts[-1])[0]
      if parts[-1].endswith("_naked"):
        parts[-1]=parts[-1][:-6]+"_xray"
      for n,part in enumerate(parts):
        if n:
          if part.startswith(parts[n-1]):
            part=part[len(parts[n-1]):]
            while part.startswith("_"):
              part=part[1:]
            parts[n]=part
      img_id=" ".join(parts)
      for prefix in image_prefixes:
        if img_id.startswith(prefix):
          img_id=img_id[len(prefix):]
      for replace_from,replace_to in image_replaces:
        img_id=img_id.replace(replace_from,replace_to)
      if images_root:
        img_fn=os.path.join(images_root,img_fn)
      images[img_id]=img_fn

def search_images():
  global images
  images={}
  if images_roots:
    current_dir=os.getcwd()
    for images_root in images_roots:
      os.chdir(images_root)
      do_search_images(None,images_root)
      os.chdir(current_dir)
  else:
    do_search_images(None,None)

def load_images():
  global images
  loaded_images={}
  for img_id,img_fn in images.items():
    img=Image.open(img_fn).convert("RGBA")
    if zoom not in (1.0,1,None,False):
      img=img.resize((int(img.width*zoom),int(img.height*zoom)),Image.BICUBIC)
    loaded_images[img_id]=img
  images=loaded_images

def load_image(img_id,xray):
  if xray and img_id+"_xray" in images:
    img_id=img_id+"_xray"
  img=images[img_id]
  return img

def render_sprite(sprite_id,layers,xray):
  sprite=None
  for layer in layers:
    layer=load_image(layer,xray)
    if not sprite:
      sprite=Image.new("RGBA",(layer.width,layer.height))
    sprite.alpha_composite(layer,(0,0))
  return sprite

def save_prerendered_avatar():
  shutil.rmtree(prerendered_out_dir,ignore_errors=True)
  os.makedirs(prerendered_out_dir)
  renders={}
  for sprite_id,layers in sprites.items():
    renders[sprite_id]=render_sprite(sprite_id,layers,False)
    ##renders[sprite_id+"_xray"]=render_sprite(sprite_id,layers,True) no more xray?
  for sprite_id,render in renders.items():
    render.save(os.path.join(prerendered_out_dir,sprite_id+".png"))

def save_optimal_avatar():
  shutil.rmtree(optimal_out_dir,ignore_errors=True)
  os.makedirs(optimal_out_dir) ##uppercase character name folder goes in here
  optimal_root_dir = os.path.join(optimal_out_dir,char_id_up) ##data.json and intex.tsx go in here
  os.makedirs(optimal_root_dir)
  optimal_expressions_dir = os.path.join(optimal_root_dir,"expressions/") ##expression.tsx files go in here
  os.makedirs(optimal_expressions_dir)
  optimal_sequences_dir = os.path.join(optimal_root_dir,"sequences/") ##sequence.ts files go in here
  os.makedirs(optimal_sequences_dir)
  optimal_hooks_dir = os.path.join(optimal_root_dir,"hooks/")##useAction/Location/Render files go in here
  os.makedirs(optimal_hooks_dir)
  optimal_images={img_id:img for img_id,img in images.items()}
  xray_images={img_id[:-5] for img_id in optimal_images if img_id.endswith("_xray") and img_id[:-5] in optimal_images}
  xray_images={img_id:(optimal_images.pop(img_id),optimal_images.pop(img_id+"_xray")) for img_id in xray_images}
  optimal_images.update(xray_images)
  images_info={}
  for img_id,img in optimal_images.items():
    if isinstance(img,(list,tuple)):
      x=99999
      y=99999
      for img_var in img:
        bbox=img_var.getbbox()
        if bbox is not None:
          x=min(x,bbox[0])
          y=min(y,bbox[1])
      for n,img_var in enumerate(img):
        bbox=img_var.getbbox()
        if bbox is not None:
          bbox=(x,y,bbox[2],bbox[3])
          if n==0:
            images_info[img_id]=(img_var.width,img_var.height,bbox[0],bbox[1])
          img_var=img_var.crop(bbox)
        if n:
          pass ##Don't bother saving xray images because JS version does not use them?
          ## img_var.save(os.path.join(optimal_out_dir,img_id+"_xray.png"))
        else:
          img_var.save(os.path.join(optimal_out_dir,img_id+".png"))
    else:
      bbox=img.getbbox()
      images_info[img_id]=(img.width,img.height,bbox[0],bbox[1])
      img=img.crop(bbox)
      img.save(os.path.join(optimal_out_dir,img_id+".png"))

##code to generate here: 
## character/data.json character/index.tsx character/expressions/expression.tsx
## things to be done manually: src/components/index.tsx src/components/TextDisplay/tags.json src/state/features/character/index.ts
## character/index.tsx will be looped a second time seperately because easier that way.
##Code prefix#############
  code=[]  ##index.tsx
  code_jason=[]  ##data.json
  code_hooks_index=[]
  code_action=[]
  code_location=[]
  code_render=[]
  base_layers={}
  
  code_hooks_index.append("export * from './useAction';")
  code_hooks_index.append("export * from './useRender';")
  
  code_action.append("// Libraries")
  code_action.append("import { useAppSelector } from 'state/hooks';")
  code_action.append("")
  code_action.append("// Sequences")
  code_action.append("export const useAction = () => {")
  code_action.append("  return (function* () {})();")
  code_action.append("};")
  
  code_location.append("// Libraries")
  code_location.append("import { useAppSelector } from 'state/hooks';")
  code_location.append("")
  code_location.append("// Calculate Character's current location from state")
  code_location.append("export const useLocation = () => {")
  code_location.append("  return {")
  code_location.append("    location: null,")
  code_location.append("  };")
  code_location.append("};")

  code_render.append("// Libraries")
  code_render.append("import { useAppSelector } from 'state/hooks';")
  code_render.append("import { useLocation } from './useLocation';")
  code_render.append("")
  code_render.append("type RenderHook = (overrides: () => boolean) => boolean;")
  code_render.append("")
  code_render.append("export const useRender: RenderHook = (overrides = () => true) => {")
  code_render.append("  const { isSpeaking, currentLocation } = useAppSelector((state) => ({")
  code_render.append("    isSpeaking: state.character.current.includes('{}'),".format(char_id))
  code_render.append("    currentLocation: state.display.scene,")
  code_render.append("  }));")
  code_render.append("  const { location } = useLocation();")
  code_render.append("")
  code_render.append("  // Run overrides, and exit if they return false")
  code_render.append("  if (!overrides()) return false;")
  code_render.append("")
  code_render.append("  // Don't show if character is speaking")
  code_render.append("  if (isSpeaking) return false;")
  code_render.append("")
  code_render.append("  // Don't show if the current location doesn't match character location")
  code_render.append("  if (currentLocation !== location) return false;")
  code_render.append("")
  code_render.append("  // If we haven't failed out at this point, go ahead and render")
  code_render.append("  return true;")
  code_render.append("};")
  
  code.append("// Libraries")
  code.append("import React from 'react';")
  code.append("")
  code.append("// Components")
  code.append("import Character from 'components/Character';")
  code.append("")
  code.append("// Expressions")
  code_jason.append("{")
  
##Code per layer###########  
  for n,sprite_id in enumerate(sprites_order): ##Loop through character expressions:sprite_id
    sprite_id_up = sprite_id[0].upper()+sprite_id[1:]
    code.append("import {}{} from './expressions/{}';".format(char_id_up,sprite_id_up,sprite_id_up))
    code_expression=[] ##expression.tsx
    code_expression.append("// Libraries")
    code_expression.append("import React from 'react';")
    code_expression.append("import { useAppSelector } from 'state/hooks';")
    code_expression.append("import { getEquipment } from 'state/features/character';")
    code_expression.append("")
    code_expression.append("// Components")
    code_expression.append("import Expression, { Asset } from 'components/Expression';")
    code_expression.append("")
    code_expression.append("// Data")
    code_expression.append("import data from '../data.json';")
    code_expression.append("")
    code_expression.append("export const {}{}: React.FC = () => {{".format(char_id_up,sprite_id_up))
    code_expression.append("  const {")
    for item in default_equipment:
      code_expression.append("    {},".format(item))
    code_expression.append("  }} = useAppSelector((state) => getEquipment(state.character.{}));".format(char_id))
    code_expression.append("")
    code_expression.append("  return (")
    for layer_n,layer in enumerate(sprites[sprite_id]):##Loop through sublayers of expression
      info=images_info[layer]
      ##info 0 1 2 3 : height width xpos ypos
      if layer_n==0:##base layer, used in data.json to group parts by what they fit on
        base_layer = layer
        if base_layer not in base_layers:
          base_layers[base_layer] = {}
        code_expression.append("    <Expression id=\"{}\" definitions={{data.{}}}>".format(char_id,base_layer))
      base_layers[base_layer][layer] = [info[2],info[3]]
      code_expression.append("      <Asset name=\"{}\" layer=\"base\" />".format(layer))##defaulting all to base layer, as this must be manually decided on
    code_expression.append("    </Expression>")
    code_expression.append("  );")
    code_expression.append("};")
    code_expression.append("")
    code_expression.append("export default {}{};".format(char_id_up,sprite_id_up))
    code_expression="\n".join(code_expression)
    expression_filename = sprite_id_up+".tsx"
    with open(os.path.join(optimal_expressions_dir,expression_filename),"w") as f:
      f.write(code_expression)
      
##Code suffix#############      
  code.append("")
  code.append("export const {}: React.FC = () => {{".format(char_id_up))
  code.append("  return (")
  code.append("    <Character")
  code.append("      id=\"{}\"".format(char_id))
  code.append("      expressions={{")
  for n,sprite_id in enumerate(sprites_order): ##Loop through character expressions:sprite_id
    sprite_id_up = sprite_id[0].upper()+sprite_id[1:]
    code.append("        {}: <{}{} />,".format(sprite_id,char_id_up,sprite_id_up))
  code.append("      }}")
  code.append("    />")
  code.append("  );")
  code.append("};")
  code.append("")
  code.append("export default {};".format(char_id_up))
  code="\n".join(code)
  with open(os.path.join(optimal_root_dir,"index.tsx"),"w") as f:
    f.write(code)
  
  code_hooks_index="\n".join(code_hooks_index)
  with open(os.path.join(optimal_hooks_dir,"index.tsx"),"w") as f:
    f.write(code_hooks_index)
  code_action="\n".join(code_action)
  with open(os.path.join(optimal_hooks_dir,"useAction.tsx"),"w") as f:
    f.write(code_action)
  code_location="\n".join(code_location)
  with open(os.path.join(optimal_hooks_dir,"useLocation.tsx"),"w") as f:
    f.write(code_location)
  code_render="\n".join(code_render)
  with open(os.path.join(optimal_hooks_dir,"useRender.tsx"),"w") as f:
    f.write(code_render)
  
  for base_layer,sublayers in base_layers.items():
    code_jason.append("  \"{}\": {{".format(base_layer))
    layer_counter = 0
    for sublayer,values in sublayers.items():
      code_jason.append("    \"{}\": {{ \"x\": {}, \"y\": {}, \"z\": {} }},".format(sublayer,values[0],values[1],layer_counter))
      layer_counter += 1
    code_jason.append("  },")
  code_jason.append("}")
  code_jason="\n".join(code_jason)
  with open(os.path.join(optimal_root_dir,"data.json"),"w") as f:
    f.write(code_jason)
  
#########################################################################################
#############################  For Images ###################################################
#########################################################################################
def process_static(interactable):
  int_id = interactable[0]
  int_id_up = to_title_case(int_id)
  dir_int = interactable[1]
  dir_int_hooks = interactable[2]
  asset_location = interactable[3]
  ofsx = interactable[4]
  ofsy = interactable[5]
  code_index.append("      <Static")
  code_index.append("        asset=\"{}\"".format(asset_location))
  code_index.append("        offset={{{{ x: {}, y: {} }}}}".format(ofsx,ofsy))
  code_index.append("      />")
  
def process_interactable(interactable):
  code_interact=[]
  code_interact_Action=[]
  code_interact_Description=[]
  code_interact_Title=[]
  int_id = interactable[0]
  int_id_up = to_title_case(int_id)
  dir_int = interactable[1]
  dir_int_hooks = interactable[2]
  asset_location = interactable[3]
  ofsx = interactable[4]
  ofsy = interactable[5]
  code_index.append("      <{} offset={{{{ x: {}, y: {} }}}} />".format(int_id_up,ofsx,ofsy))
  
  code_interact.append("// Libraries")
  code_interact.append("import React from 'react';")
  code_interact.append("")
  code_interact.append("// Components")
  code_interact.append("import Interactable, { InteractableConfig } from 'components/Interactable';")
  code_interact.append("")
  code_interact.append("// Definitions")
  code_interact.append("import { useAction } from './hooks/useAction';")
  code_interact.append("import { useDescription } from './hooks/useDescription';")
  code_interact.append("import { useTitle } from './hooks/useTitle';")
  code_interact.append("")
  code_interact.append("export const {}: React.FC<InteractableConfig> = ({{ offset }}) => {{".format(int_id_up))
  code_interact.append("  const title = useTitle();")
  code_interact.append("  const description = useDescription();")
  code_interact.append("  const action = useAction();")
  code_interact.append("")
  code_interact.append("  return (")
  code_interact.append("    <Interactable")
  code_interact.append("      id=\"{}\"".format(int_id))
  code_interact.append("      asset=\"{}\"".format(asset_location))
  code_interact.append("      title={title}")
  code_interact.append("      description={description}")
  code_interact.append("      action={action}")
  code_interact.append("      offset={offset}")
  code_interact.append("      render={true}")
  code_interact.append("    />")
  code_interact.append("  );")
  code_interact.append("};")
  code_interact.append("")
  code_interact.append("export default {};".format(int_id_up))

  code_interact_Action.append("// Libraries")
  code_interact_Action.append("import { useAppSelector } from 'state/hooks';")
  code_interact_Action.append("")
  code_interact_Action.append("// Sequences")
  code_interact_Action.append("")
  code_interact_Action.append("export const useAction = () => {")
  code_interact_Action.append("  const state = useAppSelector((state) => ({")
  code_interact_Action.append("    // TODO: Pull out the state you need here")
  code_interact_Action.append("  }));")
  code_interact_Action.append("  // This is just for type safety, we should never reach this point")
  code_interact_Action.append("  return (function* () {})();")
  code_interact_Action.append("};")

  code_interact_Description.append("import { useAppSelector } from 'state/hooks';")
  code_interact_Description.append("")
  code_interact_Description.append("export const useDescription = () => {")
  code_interact_Description.append("  const state = useAppSelector((state) => ({")
  code_interact_Description.append("    // TODO: Pull out the state you need here")
  code_interact_Description.append("  }));")
  code_interact_Description.append("  // TODO: Return a string for the description")
  code_interact_Description.append("  return 'Lorem ipsum dolor sit amet.';")
  code_interact_Description.append("};")

  code_interact_Title.append("import { useAppSelector } from 'state/hooks';")
  code_interact_Title.append("")
  code_interact_Title.append("export const useTitle = () => {")
  code_interact_Title.append("  const state = useAppSelector((state) => ({")
  code_interact_Title.append("    // TODO: Pull out the state you need here")
  code_interact_Title.append("  }));")
  code_interact_Title.append("  // TODO: Return a string to display as the title")
  code_interact_Title.append("  return 'Title';")
  code_interact_Title.append("};")
  
  code_interact="\n".join(code_interact)
  with open(os.path.join(dir_int,"index.tsx"),"w") as f:
    f.write(code_interact)
  code_interact_Action="\n".join(code_interact_Action)
  with open(os.path.join(dir_int_hooks,"useAction.tsx"),"w") as f:
    f.write(code_interact_Action)
  code_interact_Description="\n".join(code_interact_Description)
  with open(os.path.join(dir_int_hooks,"useDescription.tsx"),"w") as f:
    f.write(code_interact_Description)
  code_interact_Title="\n".join(code_interact_Title)
  with open(os.path.join(dir_int_hooks,"useTitle.tsx"),"w") as f:
    f.write(code_interact_Title)

def process_layer(layer_id,layer):
  ofsx=layer.bbox[0]
  ofsy=layer.bbox[1]
  w=layer.bbox[2]-layer.bbox[0]
  h=layer.bbox[3]-layer.bbox[1]
  print("Found layer {}, size: {}x{}, pos: {}x{}".format(layer_id,w,h,ofsx,ofsy))
  bbox=[0,0,w,h]
  if crop_layers:
    if ofsx<0:
      bbox[0]=-ofsx
      ofsx=0
    bbox[2]=min(1920+bbox[0],bbox[2])
    if ofsy<0:
      bbox[1]=-ofsy
      ofsy=0
    bbox[3]=min(1080+bbox[1],bbox[3])
  fn=layer_id.lower().strip()
  for replace_from,replace_to in layer_id_replaces:
    fn=fn.replace(replace_from,replace_to)
  el_image=out_dir_old+" "+fn
  el_interactable=el_image.replace(" ","_")
  fn_up = to_title_case(fn)
  this_out_dir_int = os.path.join(out_dir_int,fn_up)
  out_dir_int_hooks = os.path.join(this_out_dir_int,"hooks/")##useAction/Description/Title files go in here
  if process_as_interactables:
    code_index.append("import {} from './interactables/{}';".format(fn_up,fn_up))
    os.makedirs(this_out_dir_int)
    os.makedirs(out_dir_int_hooks)
  ##interactables.append(el_interactable)
  ##new code for interacts
  asset_location = "assets/locations/"+el_image.replace(" ","/")+".webp"
  this_interactable = [fn,this_out_dir_int,out_dir_int_hooks,asset_location,ofsx,ofsy]
  interactables.append(this_interactable)
  fn=os.path.join(out_dir,fn+".png")
  os.makedirs(os.path.dirname(fn),exist_ok=True)
  img=layer.composite()
  if crop_layers:
    img=img.crop(bbox)
  img.save(fn)

######################################################Main program flow######################################################

if len(sys.argv)<2:
  print("Usage: generate_anything.py <filename.txt> or <filename.psd>")
  print("This code is currently being run in python version: "+sys.version) ##Check to see which python instance is handling drag and drop
  os.system("pause")
  exit()

if sys.argv[1].endswith('.txt'):##Sequence for Characters
  print("Text file provided, parsing text file to generate character files.")
  print("loading avatar description from {}...".format(sys.argv[1]))
  load_sprites_description(sys.argv[1])
  print("searching layer images...")
  search_images()
  print("loading layer images...")
  load_images()
  print("generating prerendered avatar sprites for reference")
  save_prerendered_avatar()
  print("generating optimal avatar layer images and code...")
  save_optimal_avatar()
  print("Code generated for new character.")
  print("Note this does NOT include changes to src directory such as:")
  print("-src/components/index.tsx")
  print("-src/components/TextDisplay/tags.json")
  print("-src/state/features/character/index.ts")
  print("")
  print("For additional changes required to implement character, please see documentation.")
  print("Don't forget that you can change the default equipment for each character at the top of the script by using the global variable")
  os.system("pause")
elif sys.argv[1].endswith('.psd'):##Sequence for Locations
  print("PSD provided, parsing psd image to generate location files.")
  src_fn=sys.argv[1]
  print("Processing: {}...".format(src_fn))
  psd_id=os.path.splitext(os.path.basename(src_fn))[0]
  loc_id=psd_id.lower().split(' ')[1]
  loc_id_old=psd_id.lower().replace(" ","_")
  loc_id_up = to_title_case(loc_id)
  out_dir_old = psd_id.lower()
  out_dir=  loc_id_up
  out_dir_int = os.path.join(out_dir,"interactables/")
  
  src=PSDImage.open(src_fn)
  w,h=src.width,src.height

  print("{} ({}x{}) -> {}".format(src_fn,w,h,out_dir))
  
  ##Make parent directory here using TitleCase
  shutil.rmtree(out_dir,ignore_errors=True)
  os.makedirs(out_dir) 
  os.makedirs(out_dir_int) 
  code_index=[]
  interactables=[]
  
  ##Prior to loop

  code_index.append("// Libraries")
  code_index.append("import React, { useEffect } from 'react';")
  code_index.append("")
  code_index.append("// Components")
  code_index.append("import Scene from 'components/Scene';")
  code_index.append("import Static from 'components/Static';")
  code_index.append("")
  code_index.append("// Interactables")
  
  for layer in src:##Loop to add interactables
    if layer.is_group():
      for sub_layer in layer:
        process_layer("{}_{}".format(layer.name,sub_layer.name),sub_layer)
    else:
      process_layer(layer.name,layer)

  ##Code continues for index.tsx
  code_index.append("")
  code_index.append("export const Scene{}: React.FC = () => {{".format(loc_id_up))
  code_index.append("  useEffect(() => {")
  code_index.append("    // On enter")
  code_index.append("    return () => {")
  code_index.append("      // On exit")
  code_index.append("    };")
  code_index.append("  }, []);")
  code_index.append("")
  code_index.append("  // TODO: Update background asset path and offset")
  code_index.append("  return (")
  code_index.append("    <Scene")
  code_index.append("      background=\"{}\"".format(interactables[0][3]))
  code_index.append("      offset={{{{ x: {}, y: {} }}}}".format(interactables[0][4],interactables[0][5]))
  code_index.append("    >")
  
  for interactable in interactables[1:]:##and make and exception here not to process the background.
    ##process_as_interactables
    if process_as_interactables:
      process_interactable(interactable)
    else:##static
      process_static(interactable)
  
  ##code suffix for index.tsx
  code_index.append("    </Scene>")
  code_index.append("  );")
  code_index.append("};")
  code_index.append("")
  code_index.append("export default Scene{};".format(loc_id_up))
  
  code_index="\n".join(code_index)
  with open(os.path.join(out_dir,"index.tsx"),"w") as f:
    f.write(code_index)
  
  print("Location generated")
  print("Please not that this only generates code for src/scenes/Location folder.")
  print("Be sure to place the Location folder in the correct area (home/school)")
  print("And follow the documentation to complete setup.")
  print("Don't forget that you can default to create all location items as either")
  print("Interactables or Static elements by changing the global variable at the top.")
  os.system("pause")
else:
  print("Usage: generate_anything.py <filename.txt> or <filename.psd>")
  os.system("pause")


