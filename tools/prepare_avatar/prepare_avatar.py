#zoom=None
zoom=0.115

###############################################################################

from PIL import Image
import sys
import os
import shutil

if len(sys.argv)<2:
  print("Usage: prepare_avatar.py <folder>")
  exit()

in_dir=sys.argv[1]

if not os.path.isdir(in_dir):
  print("Error: folder required, not file - {}".format(in_dir))
  exit()

out_dir=in_dir+".out"

images={}

def search_images():
  for fn in os.listdir(in_dir):
    path=os.path.join(in_dir,fn)
    if os.path.isdir(path):
      pass
    elif fn.lower().endswith((".png",".jpg",".webp")):
      img_fn=fn
      parts=[]
      while True:
        fn,part=os.path.split(fn)
        if not part:
          break
        parts.insert(0,part)
      parts[-1]=os.path.splitext(parts[-1])[0]
      if parts[-1].endswith("_naked"):
        parts[-1]=parts[-1][:-6]+"_xray"
      for n,part in enumerate(parts):
        if n:
          if part.startswith(parts[n-1]):
            part=part[len(parts[n-1]):]
            while part.startswith("_"):
              part=part[1:]
            parts[n]=part
      parts=" ".join(parts)
      parts=parts.split("_")
      for n,part in enumerate(parts):
        if part.lower().startswith("pose") and part.endswith(("0","1","2","3","4","5","6","7","8","9")):
          parts=parts[n+1:]
          break
      img_id="_".join(parts)
      images[img_id]=path

def prepare_images():
  shutil.rmtree(out_dir,ignore_errors=True)
  os.makedirs(out_dir)
  for img_id,img_fn in images.items():
    img=Image.open(img_fn)
    if zoom not in (1.0,None):
      img=img.resize((int(img.width*zoom),int(img.height*zoom)),Image.BICUBIC)
    img.save(os.path.join(out_dir,img_id+".png"))

search_images()
prepare_images()
