do_fix=True
report_ignored_chars=False

replace_chars=[
  [chr(65279), ""  ], ## BOM
  [chr(65534), ""  ], ## BOM
  [chr(8217),  '"' ],
  [chr(8212),  "-" ],
  ["\r\n",     "\n"],
  ["\r",       "\n"],
  ]

ignore_chars=[
  9656,
  ]

###############################################################################

import os

def fix_rpy(path):
  with open(path,"rb") as f:
    data_in=f.read()
  try:
    data=data_in.decode()
  except:
    print("ERROR: can't decode - {}".format(path))
    raise
  for c_from,c_to in replace_chars:
    data=data.replace(c_from,c_to)
  data=data.split("\n")
  for line_n,line in enumerate(data):
    for c_n,c in enumerate(line):
      if ord(c)<32 or ord(c)>127:
        if ord(c) in ignore_chars:
          if report_ignored_chars:
            print(path,"expected invalid char",ord(c),"at line",line_n,"at pos",c_n)
        else:
          print(path,"invalid char",ord(c),"at line",line_n,"at pos",c_n)
          print("fix, then run again")
          exit()
  data="\r\n".join(data)
  data_out=data.encode()
  if data_out!=data_in:
    if do_fix:
      print("fixed:",path)
      with open(path+".fix","wb") as f:
        f.write(data_out)
    else:
      print("need fixing:",path)

def fix_dir(root):
  for path in os.listdir(root):
    path=os.path.join(root,path)
    if path.lower().endswith(".rpy"):
      fix_rpy(path)
    elif os.path.isdir(path):
      fix_dir(path)

fix_dir("../game")

print("done.")
