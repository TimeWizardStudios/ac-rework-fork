## Tested with Python 3.8, psd-tools 1.9.9

crop_layers=False

layer_id_replaces=[
  ["\\","_"],
  ["/", "_"],
  [" ", "_"],
  ["\"","_"],
  ["'", ""],
  ["(", "" ],
  [")", "" ],
  [":",""],
  ["\r",""],
  ["\n",""]
  ]

###############################################################################

from psd_tools import PSDImage
import sys
import os
import shutil

if len(sys.argv)<2:
  print("Usage: generate_location.py <filename.psd>")
  exit()

src_fn=sys.argv[1]
print("Processing: {}...".format(src_fn))
psd_id=os.path.splitext(os.path.basename(src_fn))[0]
loc_id=psd_id.lower().replace(" ","_")
out_dir=psd_id.lower()
src=PSDImage.open(src_fn)
w,h=src.width,src.height

print("{} ({}x{}) -> {}".format(src_fn,w,h,out_dir))

shutil.rmtree(out_dir,ignore_errors=True)

def process_layer(layer_id,layer):
  ofsx=layer.bbox[0]
  ofsy=layer.bbox[1]
  w=layer.bbox[2]-layer.bbox[0]
  h=layer.bbox[3]-layer.bbox[1]
  print("Found layer {}, size: {}x{}, pos: {}x{}".format(layer_id,w,h,ofsx,ofsy))
  bbox=[0,0,w,h]
  if crop_layers:
    if ofsx<0:
      bbox[0]=-ofsx
      ofsx=0
    bbox[2]=min(1920+bbox[0],bbox[2])
    if ofsy<0:
      bbox[1]=-ofsy
      ofsy=0
    bbox[3]=min(1080+bbox[1],bbox[3])
  fn=layer_id.lower().strip()
  for replace_from,replace_to in layer_id_replaces:
    fn=fn.replace(replace_from,replace_to)
  el_image=out_dir+" "+fn
  el_interactable=el_image.replace(" ","_")
  scene_info.append("      scene.append([({},{}),\"{}\",\"{}\"])".format(ofsx,ofsy,el_image,el_interactable))
  interactables.append(el_interactable)
  fn=os.path.join(out_dir,fn+".png")
  os.makedirs(os.path.dirname(fn),exist_ok=True)
  img=layer.composite()
  if crop_layers:
    img=img.crop(bbox)
  img.save(fn)

scene_info=[]
interactables=[]

scene_info.append("init python:")
scene_info.append("  class Location_{}(Location):".format(loc_id))
scene_info.append("    default_title=\"{}\"".format(psd_id))
scene_info.append("    def __init__(self):")
scene_info.append("      super().__init__()")
scene_info.append("    def scene(self,scene):")

for layer in src:
  if layer.is_group():
    for sub_layer in layer:
      process_layer("{}_{}".format(layer.name,sub_layer.name),sub_layer)
  else:
    process_layer(layer.name,layer)

scene_info.append("")
scene_info.append("label goto_{}:".format(loc_id))
scene_info.append("  $game.location={}".format(loc_id))
scene_info.append("  return")
scene_info.append("")

for interactable in interactables:
  scene_info.append("### {} interactable ###".format(interactable).ljust(79,"#"))
  scene_info.append("")
  scene_info.append("init python:")
  scene_info.append("  class Interactable_"+interactable+"(Interactable):")
  scene_info.append("    def title(cls):")
  scene_info.append("      return \"\"")
  scene_info.append("    def description(cls):")
  scene_info.append("      return \"\"")
  scene_info.append("    def actions(cls,actions):")
  scene_info.append("      actions.append(\"?"+interactable+"_interact\")")
  scene_info.append("")
  scene_info.append("label "+interactable+"_interact:")
  scene_info.append("  \"\"")
  scene_info.append("  return")
  scene_info.append("")

scene_info="\n".join(scene_info)

with open(os.path.join(out_dir,"_location.rpy"),"w") as f:
  f.write(scene_info)
