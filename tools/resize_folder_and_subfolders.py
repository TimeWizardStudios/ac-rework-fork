############################################################
##I made this to resize large folders of similar images with many subfolders based on#######
##the prepare_avatar.py tool. It's a bit messy so use with caution if you do ###############
############################################################
##############################zoom=None
zoom=0.6

###############################################################################

from PIL import Image
import sys
import os
import shutil

if len(sys.argv)<2:
  print("Usage: resize_folder_and_subfolders.py <folder>")
  exit()

in_dir=sys.argv[1]

if not os.path.isdir(in_dir):
  print("Error: folder required, not file - {}".format(in_dir))
  exit()

#out_dir=in_dir+".out"

def resize_images(directory):
  im_dir = directory
  out_dir = im_dir+".out"
  shutil.rmtree(out_dir,ignore_errors=True)
  os.makedirs(out_dir)
  print("resizing images")
  print("in directory: "+im_dir)
  print("out directory: "+out_dir)
  for fn in os.listdir(im_dir):
    path=os.path.join(im_dir,fn)
    if os.path.isdir(path):
      resize_images(path)
    elif fn.lower().endswith((".png",".jpg",".webp")):
      img_fn=fn
      img=Image.open(path)
      if zoom not in (1.0,None):
        img=img.resize((int(img.width*zoom),int(img.height*zoom)),Image.BICUBIC)
      img.save(os.path.join(out_dir,img_fn))#+".png"))extra .png not needed?

resize_images(in_dir)
#prepare_images()
