zoom=0.088
###############################################################################

from PIL import Image
import sys
import os

if len(sys.argv)<2:
  print("Usage: resize_image.py <imagefn.ext>")
  exit()

src_fn=sys.argv[1]
dest_fn=os.path.splitext(src_fn)[0]+"_resized.png"

img=Image.open(src_fn)
img=img.resize((int(img.width*zoom),int(img.height*zoom)),Image.BICUBIC)
img.save(dest_fn)

print("done.")
