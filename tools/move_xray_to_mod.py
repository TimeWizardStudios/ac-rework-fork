﻿base_game="../game/1000_base_game"
xray_mod="../game/1100_xray_mode"

import os

def process_file(path):
  base_dir=os.path.dirname(path)
  fn=os.path.basename(path)
  base_dir=base_dir[len(base_game)+1:]
  with open(path,"rb") as f:
    data=f.read()
  target_dir=os.path.join(xray_mod,base_dir)
  os.makedirs(target_dir,exist_ok=True)
  target_path=os.path.join(target_dir,fn)
  with open(target_path,"wb") as f:
    f.write(data)
  os.unlink(path)
  print("Moved",os.path.join(base_dir,fn))

def process_dir(root):
  for path in os.listdir(root):
    path=os.path.join(root,path)
    if "_xray." in path.lower():
      process_file(path)
    elif os.path.isdir(path):
      process_dir(path)

process_dir(base_game)

print("done.")
