zoom=1

###############################################################################
# Tool for automatic avatar generation.

# 1. Read avatar description from txt file.
# 2. Load avatar assets from folders specified in txt file.
# 3. Resize/rename avatar assets.
# 4. Generate prerendered avatar images and code to be copy-pasted.
# 5. Generate simple avatar images and code to be copy-pasted.
# 6. Generate optimal avatar images and code to be copy-pasted.

# Prerendered avatar are simple character sprites, one per expression.
# Simple avatar are uncropped layers which combined in run-time.
# Optimal avatar are cropped layers with offset coordinates, combined.

# Tested with Python 3.4 and Pillow 5.3.0
# Should work on newer versions, but no promises

###############################################################################

from PIL import Image
import sys
import os
import shutil

if len(sys.argv)<2:
  print("Usage: generate_avatar.py <filename.txt>")
  exit()

def load_sprites_description(path):
  global zoom
  global char_id
  global prerendered_out_dir,simple_out_dir,optimal_out_dir
  global images_roots,image_prefixes,image_replaces
  global sprites,sprites_order
  images_roots=[]
  image_prefixes=[]
  image_replaces=[]
  sprites={}
  sprites_order=[]
  char_id=os.path.splitext(os.path.basename(path))[0]
  prerendered_out_dir=os.path.join(char_id,"avatar_prerendered")
  simple_out_dir=os.path.join(char_id,"avatar_simple")
  optimal_out_dir=os.path.join(char_id,"avatar_optimal")
  with open(path,"rb") as f:
    data=f.read().decode().splitlines()
  current_sprite=None
  for line in data:
    line=line.strip()
    if line:
      if line.startswith("#"):
        continue
      elif line.startswith("*"):
        zoom=float(line[1:])
      elif line.startswith("@"):
        images_roots.append(line[1:])
      elif line.startswith("-"):
        image_prefixes.append(line[1:])
      elif line.startswith("?"):
        image_replaces.append(line[1:].split(":",1))
      elif line.endswith(":"):
        current_sprite=line[:-1]
        sprites_order.append(current_sprite)
        sprites[current_sprite]=[]
      elif current_sprite:
        sprites[current_sprite].append(line)

def do_search_images(path,images_root):
  for fn in os.listdir(path):
    if path:
      fn=os.path.join(path,fn)
    if os.path.isdir(fn):
      do_search_images(fn,images_root)
    elif fn.lower().endswith((".png",".jpg",".webp")):
      img_fn=fn
      parts=[]
      while True:
        fn,part=os.path.split(fn)
        if not part:
          break
        parts.insert(0,part)
      parts[-1]=os.path.splitext(parts[-1])[0]
      if parts[-1].endswith("_naked"):
        parts[-1]=parts[-1][:-6]+"_xray"
      for n,part in enumerate(parts):
        if n:
          if part.startswith(parts[n-1]):
            part=part[len(parts[n-1]):]
            while part.startswith("_"):
              part=part[1:]
            parts[n]=part
      img_id=" ".join(parts)
      for prefix in image_prefixes:
        if img_id.startswith(prefix):
          img_id=img_id[len(prefix):]
      for replace_from,replace_to in image_replaces:
        img_id=img_id.replace(replace_from,replace_to)
      if images_root:
        img_fn=os.path.join(images_root,img_fn)
      images[img_id]=img_fn

def search_images():
  global images
  images={}
  if images_roots:
    current_dir=os.getcwd()
    for images_root in images_roots:
      os.chdir(images_root)
      do_search_images(None,images_root)
      os.chdir(current_dir)
  else:
    do_search_images(None,None)

def load_images():
  global images
  loaded_images={}
  for img_id,img_fn in images.items():
    img=Image.open(img_fn).convert("RGBA")
    if zoom not in (1.0,1,None,False):
      img=img.resize((int(img.width*zoom),int(img.height*zoom)),Image.BICUBIC)
    loaded_images[img_id]=img
  images=loaded_images

def load_image(img_id,xray):
  if xray and img_id+"_xray" in images:
    img_id=img_id+"_xray"
  img=images[img_id]
  return img

def render_sprite(sprite_id,layers,xray):
  sprite=None
  for layer in layers:
    layer=load_image(layer,xray)
    if not sprite:
      sprite=Image.new("RGBA",(layer.width,layer.height))
    sprite.alpha_composite(layer,(0,0))
  return sprite

def save_prerendered_avatar():
  shutil.rmtree(prerendered_out_dir,ignore_errors=True)
  os.makedirs(prerendered_out_dir)

  renders={}
  for sprite_id,layers in sprites.items():
    renders[sprite_id]=render_sprite(sprite_id,layers,False)
    renders[sprite_id+"_xray"]=render_sprite(sprite_id,layers,True)
  for sprite_id,render in renders.items():
    render.save(os.path.join(prerendered_out_dir,sprite_id+".png"))

  code=[]
  code.append("init python:")
  code.append("  class Character_{}(BaseChar):".format(char_id))
  code.append("    def avatar(self,state=None):")
  code.append("      ## auto-generated prerendered avatar")
  code.append("      if state and isinstance(state,basestring):")
  code.append("        if renpy.has_image(\"{} avatar \"+state,True):".format(char_id))
  code.append("          return \"{} avatar \"+state".format(char_id))
  code.append("      return \"{} avatar default\"".format(char_id))
  code.append("")
  code="\n".join(code)
  with open(os.path.join(char_id,"avatar_prerendered.rpy"),"w") as f:
    f.write(code)

def save_simple_avatar():
  shutil.rmtree(simple_out_dir,ignore_errors=True)
  os.makedirs(simple_out_dir)

  for img_id,img in images.items():
    img=load_image(img_id,False)
    img.save(os.path.join(simple_out_dir,img_id+".png"))

  code=[]
  code.append("init python:")
  code.append("  class Character_{}(BaseChar):".format(char_id))
  code.append("    def avatar(self,state=None):")
  code.append("      ## auto-generated simple avatar")
  code.append("      rv=[]")
  if "default" in sprites_order:
    sprites_order.remove("default")
    sprites_order.append("default")
  for n,sprite_id in enumerate(sprites_order):
    if len(sprites_order)>1:
      if sprite_id=="default":
        code.append("      else:")
      else:
        code.append("      "+("elif" if n else "if")+" state==\""+sprite_id+"\":")
    for layer in sprites[sprite_id]:
      img_id="{} avatar {}".format(char_id,layer)
      tab=8 if len(sprites_order)>1 else 6
      code.append(" "*tab+"rv.append(\""+img_id+"\")")
  code.append("      return rv")
  code.append("")
  code="\n".join(code)
  with open(os.path.join(char_id,"avatar_simple.rpy"),"w") as f:
    f.write(code)

def save_optimal_avatar():
  shutil.rmtree(optimal_out_dir,ignore_errors=True)
  os.makedirs(optimal_out_dir)

  optimal_images={img_id:img for img_id,img in images.items()}
  xray_images={img_id[:-5] for img_id in optimal_images if img_id.endswith("_xray") and img_id[:-5] in optimal_images}
  xray_images={img_id:(optimal_images.pop(img_id),optimal_images.pop(img_id+"_xray")) for img_id in xray_images}
  optimal_images.update(xray_images)

  images_info={}

  for img_id,img in optimal_images.items():
    if isinstance(img,(list,tuple)):
      x=99999
      y=99999
      for img_var in img:
        bbox=img_var.getbbox()
        if bbox is not None:
          x=min(x,bbox[0])
          y=min(y,bbox[1])
      for n,img_var in enumerate(img):
        bbox=img_var.getbbox()
        if bbox is not None:
          bbox=(x,y,bbox[2],bbox[3])
          if n==0:
            images_info[img_id]=(img_var.width,img_var.height,bbox[0],bbox[1])
          img_var=img_var.crop(bbox)
        img_var.save(os.path.join(optimal_out_dir,img_id+("_xray.png" if n else ".png")))
    else:
      bbox=img.getbbox()
      images_info[img_id]=(img.width,img.height,bbox[0],bbox[1])
      img=img.crop(bbox)
      img.save(os.path.join(optimal_out_dir,img_id+".png"))

  code=[]
  code.append("init python:")
  code.append("  class Character_{}(BaseChar):".format(char_id))
  code.append("    def avatar(self,state=None):")
  code.append("      ## auto-generated optimal avatar")
  code.append("      if state and isinstance(state,basestring):")
  code.append("        if renpy.has_image(\"{} avatar \"+state,True):".format(char_id))
  code.append("          return \"{} avatar \"+state".format(char_id))
  code.append("      rv=[]")
  if "default" in sprites_order:
    sprites_order.remove("default")
    sprites_order.append("default")
  for n,sprite_id in enumerate(sprites_order):
    if len(sprites_order)>1:
      if sprite_id=="default":
        code.append("      else:")
      else:
        code.append("      "+("elif" if n else "if")+" state==\""+sprite_id+"\":")
    for layer_n,layer in enumerate(sprites[sprite_id]):
      info=images_info[layer]
      img_id="{} avatar {}".format(char_id,layer)
      tab=8 if len(sprites_order)>1 else 6
      if layer_n==0:
        code.append(" "*tab+"rv.append(({},{}))".format(info[0],info[1]))
      code.append(" "*tab+"rv.append((\"{}\",{},{}))".format(img_id,info[2],info[3]))
  code.append("      return rv")
  code.append("")
  code="\n".join(code)
  with open(os.path.join(char_id,"avatar_optimal.rpy"),"w") as f:
    f.write(code)

print("loading avatar description from {}...".format(sys.argv[1]))
load_sprites_description(sys.argv[1])
print("searching layer images...")
search_images()
print("loading layer images...")
load_images()
print("generating prerendered avatar sprites and code...")
save_prerendered_avatar()
print("generating simple avatar layer images and code...")
save_simple_avatar()
print("generating optimal avatar layer images and code...")
save_optimal_avatar()
print("done.")
